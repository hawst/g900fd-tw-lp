.class public final Lmyc;
.super Loxq;
.source "PG"


# instance fields
.field public a:[I

.field public b:[I

.field public c:Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 648
    invoke-direct {p0}, Loxq;-><init>()V

    .line 651
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lmyc;->d:[Ljava/lang/String;

    .line 654
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmyc;->a:[I

    .line 657
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmyc;->b:[I

    .line 648
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 693
    .line 694
    iget-object v0, p0, Lmyc;->d:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmyc;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 696
    iget-object v3, p0, Lmyc;->d:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 698
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 696
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 701
    :cond_0
    iget-object v0, p0, Lmyc;->d:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 703
    :goto_1
    iget-object v2, p0, Lmyc;->a:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmyc;->a:[I

    array-length v2, v2

    if-lez v2, :cond_2

    .line 705
    iget-object v4, p0, Lmyc;->a:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_1

    aget v6, v4, v2

    .line 707
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 705
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 709
    :cond_1
    add-int/2addr v0, v3

    .line 710
    iget-object v2, p0, Lmyc;->a:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 712
    :cond_2
    iget-object v2, p0, Lmyc;->b:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Lmyc;->b:[I

    array-length v2, v2

    if-lez v2, :cond_4

    .line 714
    iget-object v3, p0, Lmyc;->b:[I

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_3

    aget v5, v3, v1

    .line 716
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 714
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 718
    :cond_3
    add-int/2addr v0, v2

    .line 719
    iget-object v1, p0, Lmyc;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 721
    :cond_4
    iget-object v1, p0, Lmyc;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 722
    const/4 v1, 0x4

    iget-object v2, p0, Lmyc;->e:Ljava/lang/Integer;

    .line 723
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 725
    :cond_5
    iget-object v1, p0, Lmyc;->c:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 726
    const/4 v1, 0x5

    iget-object v2, p0, Lmyc;->c:Ljava/lang/String;

    .line 727
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 729
    :cond_6
    iget-object v1, p0, Lmyc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 730
    iput v0, p0, Lmyc;->ai:I

    .line 731
    return v0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lmyc;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 739
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 740
    sparse-switch v0, :sswitch_data_0

    .line 744
    iget-object v1, p0, Lmyc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 745
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyc;->ah:Ljava/util/List;

    .line 748
    :cond_1
    iget-object v1, p0, Lmyc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 750
    :sswitch_0
    return-object p0

    .line 755
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 756
    iget-object v0, p0, Lmyc;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 757
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 758
    iget-object v2, p0, Lmyc;->d:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 759
    iput-object v1, p0, Lmyc;->d:[Ljava/lang/String;

    .line 760
    :goto_1
    iget-object v1, p0, Lmyc;->d:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 761
    iget-object v1, p0, Lmyc;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 762
    invoke-virtual {p1}, Loxn;->a()I

    .line 760
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 765
    :cond_2
    iget-object v1, p0, Lmyc;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 769
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 770
    iget-object v0, p0, Lmyc;->a:[I

    array-length v0, v0

    .line 771
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 772
    iget-object v2, p0, Lmyc;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 773
    iput-object v1, p0, Lmyc;->a:[I

    .line 774
    :goto_2
    iget-object v1, p0, Lmyc;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 775
    iget-object v1, p0, Lmyc;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 776
    invoke-virtual {p1}, Loxn;->a()I

    .line 774
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 779
    :cond_3
    iget-object v1, p0, Lmyc;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 783
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 784
    iget-object v0, p0, Lmyc;->b:[I

    array-length v0, v0

    .line 785
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 786
    iget-object v2, p0, Lmyc;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 787
    iput-object v1, p0, Lmyc;->b:[I

    .line 788
    :goto_3
    iget-object v1, p0, Lmyc;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 789
    iget-object v1, p0, Lmyc;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 790
    invoke-virtual {p1}, Loxn;->a()I

    .line 788
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 793
    :cond_4
    iget-object v1, p0, Lmyc;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 797
    :sswitch_4
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmyc;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 801
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmyc;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 740
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 666
    iget-object v1, p0, Lmyc;->d:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 667
    iget-object v2, p0, Lmyc;->d:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 668
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 667
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 671
    :cond_0
    iget-object v1, p0, Lmyc;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmyc;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 672
    iget-object v2, p0, Lmyc;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget v4, v2, v1

    .line 673
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 672
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 676
    :cond_1
    iget-object v1, p0, Lmyc;->b:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmyc;->b:[I

    array-length v1, v1

    if-lez v1, :cond_2

    .line 677
    iget-object v1, p0, Lmyc;->b:[I

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget v3, v1, v0

    .line 678
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 677
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 681
    :cond_2
    iget-object v0, p0, Lmyc;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 682
    const/4 v0, 0x4

    iget-object v1, p0, Lmyc;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 684
    :cond_3
    iget-object v0, p0, Lmyc;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 685
    const/4 v0, 0x5

    iget-object v1, p0, Lmyc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 687
    :cond_4
    iget-object v0, p0, Lmyc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 689
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 644
    invoke-virtual {p0, p1}, Lmyc;->a(Loxn;)Lmyc;

    move-result-object v0

    return-object v0
.end method
