.class public final Llam;
.super Lllq;
.source "PG"

# interfaces
.implements Lhts;


# instance fields
.field private a:[Ljava/lang/String;

.field private b:[Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:[Landroid/text/Spanned;

.field private e:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lllq;-><init>()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Lllq;-><init>()V

    .line 70
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Llam;->e:I

    .line 72
    iget v0, p0, Llam;->e:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Llam;->a:[Ljava/lang/String;

    .line 73
    iget v0, p0, Llam;->e:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Llam;->b:[Ljava/lang/String;

    .line 74
    iget v0, p0, Llam;->e:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Llam;->c:[Ljava/lang/String;

    .line 75
    iget v0, p0, Llam;->e:I

    new-array v0, v0, [Landroid/text/Spanned;

    iput-object v0, p0, Llam;->d:[Landroid/text/Spanned;

    .line 78
    invoke-interface {p1}, Landroid/database/Cursor;->moveToLast()Z

    move v0, v1

    .line 79
    :goto_0
    iget v2, p0, Llam;->e:I

    if-ge v0, v2, :cond_0

    .line 80
    iget-object v2, p0, Llam;->a:[Ljava/lang/String;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 81
    iget-object v2, p0, Llam;->b:[Ljava/lang/String;

    const/4 v3, 0x1

    .line 82
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 83
    iget-object v2, p0, Llam;->c:[Ljava/lang/String;

    const/4 v3, 0x2

    .line 84
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 83
    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 85
    iget-object v2, p0, Llam;->d:[Landroid/text/Spanned;

    const/4 v3, 0x3

    .line 86
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 85
    invoke-static {v3}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    aput-object v3, v2, v0

    .line 87
    invoke-interface {p1}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/util/List;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lofa;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Lllq;-><init>()V

    .line 43
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 44
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Llam;->e:I

    .line 46
    iget v1, p0, Llam;->e:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Llam;->a:[Ljava/lang/String;

    .line 47
    iget v1, p0, Llam;->e:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Llam;->b:[Ljava/lang/String;

    .line 48
    iget v1, p0, Llam;->e:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Llam;->c:[Ljava/lang/String;

    .line 49
    iget v1, p0, Llam;->e:I

    new-array v1, v1, [Landroid/text/Spanned;

    iput-object v1, p0, Llam;->d:[Landroid/text/Spanned;

    .line 51
    iget v1, p0, Llam;->e:I

    sub-int v8, v0, v1

    .line 53
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    iget v0, p0, Llam;->e:I

    if-ge v7, v0, :cond_1

    .line 54
    iget-object v1, p0, Llam;->a:[Ljava/lang/String;

    add-int v0, v8, v7

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lofa;

    iget-object v0, v0, Lofa;->g:Ljava/lang/String;

    aput-object v0, v1, v7

    .line 55
    iget-object v1, p0, Llam;->b:[Ljava/lang/String;

    add-int v0, v8, v7

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lofa;

    iget-object v0, v0, Lofa;->b:Ljava/lang/String;

    aput-object v0, v1, v7

    .line 56
    iget-object v1, p0, Llam;->c:[Ljava/lang/String;

    add-int v0, v8, v7

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lofa;

    iget-object v0, v0, Lofa;->m:Ljava/lang/String;

    aput-object v0, v1, v7

    .line 57
    iget-object v9, p0, Llam;->d:[Landroid/text/Spanned;

    add-int v0, v8, v7

    .line 58
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lofa;

    iget-object v0, v0, Lofa;->n:Lpeo;

    .line 59
    invoke-static {}, Lhwr;->b()Lhxa;

    move-result-object v1

    .line 60
    sget-object v2, Lhwr;->b:Lhxa;

    if-nez v2, :cond_0

    new-instance v2, Lhwy;

    invoke-direct {v2}, Lhwy;-><init>()V

    sput-object v2, Lhwr;->b:Lhxa;

    :cond_0
    sget-object v2, Lhwr;->b:Lhxa;

    .line 61
    invoke-static {}, Lhwr;->b()Lhxa;

    move-result-object v3

    .line 62
    invoke-static {}, Lhwr;->a()Lhxa;

    move-result-object v4

    .line 63
    invoke-static {}, Lhwr;->b()Lhxa;

    move-result-object v5

    .line 64
    invoke-static {}, Lhwr;->e()Lhxa;

    move-result-object v6

    .line 57
    invoke-static/range {v0 .. v6}, Lhwr;->a(Lpeo;Lhxa;Lhxa;Lhxa;Lhxa;Lhxa;Lhxa;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    aput-object v0, v9, v7

    .line 53
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 67
    :cond_1
    return-void
.end method

.method public static a([B)Llam;
    .locals 5

    .prologue
    .line 112
    if-nez p0, :cond_0

    .line 113
    const/4 v0, 0x0

    .line 133
    :goto_0
    return-object v0

    .line 116
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 117
    new-instance v1, Llam;

    invoke-direct {v1}, Llam;-><init>()V

    .line 119
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, v1, Llam;->e:I

    .line 121
    iget v0, v1, Llam;->e:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v1, Llam;->a:[Ljava/lang/String;

    .line 122
    iget v0, v1, Llam;->e:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v1, Llam;->b:[Ljava/lang/String;

    .line 123
    iget v0, v1, Llam;->e:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v1, Llam;->c:[Ljava/lang/String;

    .line 124
    iget v0, v1, Llam;->e:I

    new-array v0, v0, [Landroid/text/Spanned;

    iput-object v0, v1, Llam;->d:[Landroid/text/Spanned;

    .line 126
    const/4 v0, 0x0

    :goto_1
    iget v3, v1, Llam;->e:I

    if-ge v0, v3, :cond_1

    .line 127
    iget-object v3, v1, Llam;->a:[Ljava/lang/String;

    invoke-static {v2}, Llam;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 128
    iget-object v3, v1, Llam;->b:[Ljava/lang/String;

    invoke-static {v2}, Llam;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 129
    iget-object v3, v1, Llam;->c:[Ljava/lang/String;

    invoke-static {v2}, Llam;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 130
    iget-object v3, v1, Llam;->d:[Landroid/text/Spanned;

    invoke-static {v2}, Llht;->a(Ljava/nio/ByteBuffer;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    aput-object v4, v3, v0

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 133
    goto :goto_0
.end method

.method public static a(Llam;)[B
    .locals 7

    .prologue
    const/16 v6, 0x400

    const/4 v1, 0x0

    .line 92
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x100

    invoke-direct {v4, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 93
    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 95
    iget v0, p0, Llam;->e:I

    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v0, v1

    .line 97
    :goto_0
    iget v2, p0, Llam;->e:I

    if-ge v0, v2, :cond_1

    .line 98
    iget-object v2, p0, Llam;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v5, v2}, Llam;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 99
    iget-object v2, p0, Llam;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v5, v2}, Llam;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 100
    iget-object v2, p0, Llam;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v5, v2}, Llam;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 101
    iget-object v2, p0, Llam;->d:[Landroid/text/Spanned;

    aget-object v2, v2, v0

    .line 102
    invoke-interface {v2}, Landroid/text/Spanned;->length()I

    move-result v3

    if-gt v3, v6, :cond_0

    :goto_1
    invoke-static {v2}, Llht;->a(Landroid/text/Spanned;)[B

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/io/DataOutputStream;->write([B)V

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_0
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v2, v1, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;II)V

    move-object v2, v3

    goto :goto_1

    .line 106
    :cond_1
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 107
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V

    .line 108
    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Llam;->e:I

    return v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Llam;->c:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Llam;->a:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Llam;->b:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public d(I)Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Llam;->d:[Landroid/text/Spanned;

    aget-object v0, v0, p1

    return-object v0
.end method
