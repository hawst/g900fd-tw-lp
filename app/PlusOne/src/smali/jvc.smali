.class public Ljvc;
.super Lhye;
.source "PG"


# instance fields
.field private final b:[Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Z

.field private final h:Ljava/lang/String;

.field private final i:[Ljava/lang/String;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZ)V
    .locals 10

    .prologue
    .line 221
    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    invoke-direct/range {v0 .. v9}, Ljvc;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    .line 223
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 13

    .prologue
    .line 228
    invoke-static/range {p3 .. p3}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 229
    iput p2, p0, Ljvc;->d:I

    .line 230
    move-object/from16 v0, p3

    iput-object v0, p0, Ljvc;->c:Ljava/lang/String;

    .line 231
    move-object/from16 v0, p4

    iput-object v0, p0, Ljvc;->b:[Ljava/lang/String;

    .line 232
    move-object/from16 v0, p6

    iput-object v0, p0, Ljvc;->f:Ljava/lang/String;

    .line 234
    move/from16 v0, p7

    iput-boolean v0, p0, Ljvc;->g:Z

    .line 235
    invoke-static/range {p9 .. p9}, Ljva;->b(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 236
    invoke-static/range {p9 .. p9}, Ljva;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 237
    invoke-static/range {p9 .. p9}, Ljva;->a(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 239
    if-nez p6, :cond_0

    if-eqz p7, :cond_2

    :cond_0
    const/4 v2, 0x1

    move v3, v2

    .line 241
    :goto_1
    if-nez p5, :cond_5

    .line 242
    const-string v2, "all_tiles"

    iput-object v2, p0, Ljvc;->h:Ljava/lang/String;

    .line 243
    if-eqz v3, :cond_4

    iget-boolean v2, p0, Ljvc;->g:Z

    if-eqz v2, :cond_3

    const-string v2, "view_id = ? AND (parent_id IS NULL OR media_attr & ? != 0 OR ?) AND media_attr & ? == 0 AND media_attr & 512 != 0"

    .line 253
    :goto_2
    new-instance v7, Ljvl;

    invoke-virtual {p0}, Ljvc;->n()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Ljvl;-><init>(Landroid/content/Context;)V

    .line 256
    const-wide/32 v8, 0xc350

    iget v7, v7, Ljvl;->c:I

    int-to-long v10, v7

    add-long/2addr v8, v10

    .line 257
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "((view_order >= 50000 AND view_order <= %d) OR type = \'110\' OR view_order > 50100)"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v11, v12

    invoke-static {v7, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 258
    if-eqz p8, :cond_8

    :goto_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x5

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " AND "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ljvc;->e:Ljava/lang/String;

    .line 261
    if-eqz v3, :cond_9

    iget-boolean v2, p0, Ljvc;->g:Z

    if-nez v2, :cond_9

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Ljvc;->c:Ljava/lang/String;

    aput-object v8, v2, v7

    const/4 v7, 0x1

    aput-object v4, v2, v7

    const/4 v4, 0x2

    aput-object v5, v2, v4

    const/4 v4, 0x3

    aput-object v6, v2, v4

    const/4 v4, 0x4

    const/4 v5, 0x0

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const/4 v5, 0x0

    aput-object v5, v2, v4

    :goto_4
    iput-object v2, p0, Ljvc;->i:[Ljava/lang/String;

    .line 265
    if-eqz v3, :cond_a

    const-string v2, "SELECT tile_id FROM all_tiles WHERE view_id = ?  AND photo_id = ? AND media_attr & 512 != 0"

    :goto_5
    iput-object v2, p0, Ljvc;->j:Ljava/lang/String;

    .line 266
    return-void

    .line 236
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 239
    :cond_2
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_1

    .line 243
    :cond_3
    const-string v2, "view_id = ? AND (parent_id IS NULL OR media_attr & ? != 0 OR ?) AND media_attr & ? == 0 AND ( ( tile_id = ?  AND media_attr & 512 != 0 )  OR ( tile_id != ?  AND media_attr & 512 == 0 ) )"

    goto/16 :goto_2

    :cond_4
    const-string v2, "view_id = ? AND (parent_id IS NULL OR media_attr & ? != 0 OR ?) AND media_attr & ? == 0 AND media_attr & 512 == 0"

    goto/16 :goto_2

    .line 246
    :cond_5
    const-string v2, "all_tiles"

    iput-object v2, p0, Ljvc;->h:Ljava/lang/String;

    .line 247
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    if-eqz v3, :cond_7

    iget-boolean v2, p0, Ljvc;->g:Z

    if-eqz v2, :cond_6

    const-string v2, "view_id = ? AND (parent_id IS NULL OR media_attr & ? != 0 OR ?) AND media_attr & ? == 0 AND media_attr & 512 != 0 AND %s"

    :goto_6
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p5, v8, v9

    invoke-static {v7, v2, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_6
    const-string v2, "view_id = ? AND (parent_id IS NULL OR media_attr & ? != 0 OR ?) AND media_attr & ? == 0 AND ( ( tile_id = ?  AND media_attr & 512 != 0 )  OR ( tile_id != ?  AND media_attr & 512 == 0 ) ) AND %s"

    goto :goto_6

    :cond_7
    const-string v2, "view_id = ? AND (parent_id IS NULL OR media_attr & ? != 0 OR ?) AND media_attr & ? == 0 AND media_attr & 512 == 0 AND %s"

    goto :goto_6

    .line 258
    :cond_8
    const-string v8, "view_order > 50100"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x5

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " AND "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 261
    :cond_9
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Ljvc;->c:Ljava/lang/String;

    aput-object v8, v2, v7

    const/4 v7, 0x1

    aput-object v4, v2, v7

    const/4 v4, 0x2

    aput-object v5, v2, v4

    const/4 v4, 0x3

    aput-object v6, v2, v4

    goto :goto_4

    .line 265
    :cond_a
    const-string v2, "SELECT tile_id FROM all_tiles WHERE view_id = ?  AND photo_id = ? AND media_attr & 512 == 0"

    goto :goto_5
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 313
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 315
    if-eqz p1, :cond_1

    .line 316
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 317
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {v0}, Lizu;->a()Ljava/lang/String;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_0

    .line 319
    invoke-static {v2, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 320
    const/16 v0, 0x2c

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 316
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 325
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 326
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 327
    if-nez p0, :cond_3

    .line 328
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "tile_id NOT IN (%s)"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 333
    :cond_2
    :goto_1
    return-object p0

    .line 330
    :cond_3
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "( %s AND tile_id NOT IN (%s) )"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 339
    iget v9, p0, Ljvc;->d:I

    .line 340
    invoke-virtual {p0}, Ljvc;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v9}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 343
    iget-object v1, p0, Ljvc;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Ljvc;->g:Z

    if-nez v1, :cond_1

    .line 344
    iget-object v1, p0, Ljvc;->f:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Ljvc;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 345
    if-nez v1, :cond_0

    .line 362
    :goto_0
    return-object v5

    .line 348
    :cond_0
    iget-object v2, p0, Ljvc;->i:[Ljava/lang/String;

    iget-object v3, p0, Ljvc;->i:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x2

    aput-object v1, v2, v3

    .line 349
    iget-object v2, p0, Ljvc;->i:[Ljava/lang/String;

    iget-object v3, p0, Ljvc;->i:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aput-object v1, v2, v3

    .line 352
    :cond_1
    iget-object v1, p0, Ljvc;->h:Ljava/lang/String;

    iget-object v2, p0, Ljvc;->b:[Ljava/lang/String;

    iget-object v3, p0, Ljvc;->e:Ljava/lang/String;

    iget-object v4, p0, Ljvc;->i:[Ljava/lang/String;

    const-string v7, "view_order ASC"

    move-object v6, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 355
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 357
    invoke-virtual {p0}, Ljvc;->n()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Ljvc;->c:Ljava/lang/String;

    invoke-static {v0, v9, v3}, Ljvj;->c(Landroid/content/Context;ILjava/lang/String;)Landroid/util/Pair;

    move-result-object v3

    .line 358
    if-eqz v3, :cond_2

    .line 359
    const-string v4, "resume_token"

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v4, "last_refresh_time"

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 362
    :cond_2
    invoke-static {v1, v2}, Lhyc;->a(Landroid/database/Cursor;Landroid/os/Bundle;)Lhyc;

    move-result-object v5

    goto :goto_0
.end method

.method public a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 296
    if-nez p2, :cond_0

    .line 305
    :goto_0
    return-object v0

    .line 300
    :cond_0
    :try_start_0
    iget-object v1, p0, Ljvc;->j:Ljava/lang/String;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Ljvc;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {p1, v1, v2}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 305
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public l()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Ljvc;->i:[Ljava/lang/String;

    return-object v0
.end method
