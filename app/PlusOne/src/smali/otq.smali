.class public final Lotq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Loto;

.field public b:Lory;

.field public c:Loro;

.field private d:Ljava/lang/Boolean;

.field private e:Lotp;

.field private f:Loti;

.field private g:Lotl;

.field private h:Lotk;

.field private i:Lotm;

.field private j:Lotr;

.field private k:Loru;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    iput-object v0, p0, Lotq;->a:Loto;

    .line 18
    iput-object v0, p0, Lotq;->e:Lotp;

    .line 21
    iput-object v0, p0, Lotq;->f:Loti;

    .line 24
    iput-object v0, p0, Lotq;->b:Lory;

    .line 27
    iput-object v0, p0, Lotq;->c:Loro;

    .line 30
    iput-object v0, p0, Lotq;->g:Lotl;

    .line 33
    iput-object v0, p0, Lotq;->h:Lotk;

    .line 36
    iput-object v0, p0, Lotq;->i:Lotm;

    .line 39
    iput-object v0, p0, Lotq;->j:Lotr;

    .line 42
    iput-object v0, p0, Lotq;->k:Loru;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lotq;->a:Loto;

    if-eqz v1, :cond_0

    .line 88
    const/4 v0, 0x1

    iget-object v1, p0, Lotq;->a:Loto;

    .line 89
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 91
    :cond_0
    iget-object v1, p0, Lotq;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 92
    const/4 v1, 0x2

    iget-object v2, p0, Lotq;->d:Ljava/lang/Boolean;

    .line 93
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 95
    :cond_1
    iget-object v1, p0, Lotq;->b:Lory;

    if-eqz v1, :cond_2

    .line 96
    const/4 v1, 0x3

    iget-object v2, p0, Lotq;->b:Lory;

    .line 97
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_2
    iget-object v1, p0, Lotq;->c:Loro;

    if-eqz v1, :cond_3

    .line 100
    const/4 v1, 0x4

    iget-object v2, p0, Lotq;->c:Loro;

    .line 101
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_3
    iget-object v1, p0, Lotq;->e:Lotp;

    if-eqz v1, :cond_4

    .line 104
    const/4 v1, 0x5

    iget-object v2, p0, Lotq;->e:Lotp;

    .line 105
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_4
    iget-object v1, p0, Lotq;->f:Loti;

    if-eqz v1, :cond_5

    .line 108
    const/4 v1, 0x6

    iget-object v2, p0, Lotq;->f:Loti;

    .line 109
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_5
    iget-object v1, p0, Lotq;->g:Lotl;

    if-eqz v1, :cond_6

    .line 112
    const/4 v1, 0x7

    iget-object v2, p0, Lotq;->g:Lotl;

    .line 113
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_6
    iget-object v1, p0, Lotq;->h:Lotk;

    if-eqz v1, :cond_7

    .line 116
    const/16 v1, 0x8

    iget-object v2, p0, Lotq;->h:Lotk;

    .line 117
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_7
    iget-object v1, p0, Lotq;->i:Lotm;

    if-eqz v1, :cond_8

    .line 120
    const/16 v1, 0x9

    iget-object v2, p0, Lotq;->i:Lotm;

    .line 121
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_8
    iget-object v1, p0, Lotq;->j:Lotr;

    if-eqz v1, :cond_9

    .line 124
    const/16 v1, 0xa

    iget-object v2, p0, Lotq;->j:Lotr;

    .line 125
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_9
    iget-object v1, p0, Lotq;->k:Loru;

    if-eqz v1, :cond_a

    .line 128
    const/16 v1, 0xb

    iget-object v2, p0, Lotq;->k:Loru;

    .line 129
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_a
    iget-object v1, p0, Lotq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    iput v0, p0, Lotq;->ai:I

    .line 133
    return v0
.end method

.method public a(Loxn;)Lotq;
    .locals 2

    .prologue
    .line 141
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 142
    sparse-switch v0, :sswitch_data_0

    .line 146
    iget-object v1, p0, Lotq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 147
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lotq;->ah:Ljava/util/List;

    .line 150
    :cond_1
    iget-object v1, p0, Lotq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :sswitch_0
    return-object p0

    .line 157
    :sswitch_1
    iget-object v0, p0, Lotq;->a:Loto;

    if-nez v0, :cond_2

    .line 158
    new-instance v0, Loto;

    invoke-direct {v0}, Loto;-><init>()V

    iput-object v0, p0, Lotq;->a:Loto;

    .line 160
    :cond_2
    iget-object v0, p0, Lotq;->a:Loto;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 164
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lotq;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 168
    :sswitch_3
    iget-object v0, p0, Lotq;->b:Lory;

    if-nez v0, :cond_3

    .line 169
    new-instance v0, Lory;

    invoke-direct {v0}, Lory;-><init>()V

    iput-object v0, p0, Lotq;->b:Lory;

    .line 171
    :cond_3
    iget-object v0, p0, Lotq;->b:Lory;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 175
    :sswitch_4
    iget-object v0, p0, Lotq;->c:Loro;

    if-nez v0, :cond_4

    .line 176
    new-instance v0, Loro;

    invoke-direct {v0}, Loro;-><init>()V

    iput-object v0, p0, Lotq;->c:Loro;

    .line 178
    :cond_4
    iget-object v0, p0, Lotq;->c:Loro;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 182
    :sswitch_5
    iget-object v0, p0, Lotq;->e:Lotp;

    if-nez v0, :cond_5

    .line 183
    new-instance v0, Lotp;

    invoke-direct {v0}, Lotp;-><init>()V

    iput-object v0, p0, Lotq;->e:Lotp;

    .line 185
    :cond_5
    iget-object v0, p0, Lotq;->e:Lotp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 189
    :sswitch_6
    iget-object v0, p0, Lotq;->f:Loti;

    if-nez v0, :cond_6

    .line 190
    new-instance v0, Loti;

    invoke-direct {v0}, Loti;-><init>()V

    iput-object v0, p0, Lotq;->f:Loti;

    .line 192
    :cond_6
    iget-object v0, p0, Lotq;->f:Loti;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 196
    :sswitch_7
    iget-object v0, p0, Lotq;->g:Lotl;

    if-nez v0, :cond_7

    .line 197
    new-instance v0, Lotl;

    invoke-direct {v0}, Lotl;-><init>()V

    iput-object v0, p0, Lotq;->g:Lotl;

    .line 199
    :cond_7
    iget-object v0, p0, Lotq;->g:Lotl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 203
    :sswitch_8
    iget-object v0, p0, Lotq;->h:Lotk;

    if-nez v0, :cond_8

    .line 204
    new-instance v0, Lotk;

    invoke-direct {v0}, Lotk;-><init>()V

    iput-object v0, p0, Lotq;->h:Lotk;

    .line 206
    :cond_8
    iget-object v0, p0, Lotq;->h:Lotk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 210
    :sswitch_9
    iget-object v0, p0, Lotq;->i:Lotm;

    if-nez v0, :cond_9

    .line 211
    new-instance v0, Lotm;

    invoke-direct {v0}, Lotm;-><init>()V

    iput-object v0, p0, Lotq;->i:Lotm;

    .line 213
    :cond_9
    iget-object v0, p0, Lotq;->i:Lotm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 217
    :sswitch_a
    iget-object v0, p0, Lotq;->j:Lotr;

    if-nez v0, :cond_a

    .line 218
    new-instance v0, Lotr;

    invoke-direct {v0}, Lotr;-><init>()V

    iput-object v0, p0, Lotq;->j:Lotr;

    .line 220
    :cond_a
    iget-object v0, p0, Lotq;->j:Lotr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 224
    :sswitch_b
    iget-object v0, p0, Lotq;->k:Loru;

    if-nez v0, :cond_b

    .line 225
    new-instance v0, Loru;

    invoke-direct {v0}, Loru;-><init>()V

    iput-object v0, p0, Lotq;->k:Loru;

    .line 227
    :cond_b
    iget-object v0, p0, Lotq;->k:Loru;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 142
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lotq;->a:Loto;

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    iget-object v1, p0, Lotq;->a:Loto;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 50
    :cond_0
    iget-object v0, p0, Lotq;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    iget-object v1, p0, Lotq;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 53
    :cond_1
    iget-object v0, p0, Lotq;->b:Lory;

    if-eqz v0, :cond_2

    .line 54
    const/4 v0, 0x3

    iget-object v1, p0, Lotq;->b:Lory;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 56
    :cond_2
    iget-object v0, p0, Lotq;->c:Loro;

    if-eqz v0, :cond_3

    .line 57
    const/4 v0, 0x4

    iget-object v1, p0, Lotq;->c:Loro;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 59
    :cond_3
    iget-object v0, p0, Lotq;->e:Lotp;

    if-eqz v0, :cond_4

    .line 60
    const/4 v0, 0x5

    iget-object v1, p0, Lotq;->e:Lotp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 62
    :cond_4
    iget-object v0, p0, Lotq;->f:Loti;

    if-eqz v0, :cond_5

    .line 63
    const/4 v0, 0x6

    iget-object v1, p0, Lotq;->f:Loti;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 65
    :cond_5
    iget-object v0, p0, Lotq;->g:Lotl;

    if-eqz v0, :cond_6

    .line 66
    const/4 v0, 0x7

    iget-object v1, p0, Lotq;->g:Lotl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 68
    :cond_6
    iget-object v0, p0, Lotq;->h:Lotk;

    if-eqz v0, :cond_7

    .line 69
    const/16 v0, 0x8

    iget-object v1, p0, Lotq;->h:Lotk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 71
    :cond_7
    iget-object v0, p0, Lotq;->i:Lotm;

    if-eqz v0, :cond_8

    .line 72
    const/16 v0, 0x9

    iget-object v1, p0, Lotq;->i:Lotm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 74
    :cond_8
    iget-object v0, p0, Lotq;->j:Lotr;

    if-eqz v0, :cond_9

    .line 75
    const/16 v0, 0xa

    iget-object v1, p0, Lotq;->j:Lotr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 77
    :cond_9
    iget-object v0, p0, Lotq;->k:Loru;

    if-eqz v0, :cond_a

    .line 78
    const/16 v0, 0xb

    iget-object v1, p0, Lotq;->k:Loru;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 80
    :cond_a
    iget-object v0, p0, Lotq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 82
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lotq;->a(Loxn;)Lotq;

    move-result-object v0

    return-object v0
.end method
