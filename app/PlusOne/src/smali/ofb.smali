.class public final Lofb;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:[Ljava/lang/String;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2006
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2011
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lofb;->b:[Ljava/lang/String;

    .line 2006
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2040
    .line 2041
    iget-object v0, p0, Lofb;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2042
    const/4 v0, 0x1

    iget-object v2, p0, Lofb;->a:Ljava/lang/Integer;

    .line 2043
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2045
    :goto_0
    iget-object v2, p0, Lofb;->b:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lofb;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 2047
    iget-object v3, p0, Lofb;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 2049
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 2047
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2051
    :cond_0
    add-int/2addr v0, v2

    .line 2052
    iget-object v1, p0, Lofb;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2054
    :cond_1
    iget-object v1, p0, Lofb;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2055
    const/4 v1, 0x3

    iget-object v2, p0, Lofb;->c:Ljava/lang/Long;

    .line 2056
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2058
    :cond_2
    iget-object v1, p0, Lofb;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 2059
    const/4 v1, 0x4

    iget-object v2, p0, Lofb;->d:Ljava/lang/Long;

    .line 2060
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2062
    :cond_3
    iget-object v1, p0, Lofb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2063
    iput v0, p0, Lofb;->ai:I

    .line 2064
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lofb;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2072
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2073
    sparse-switch v0, :sswitch_data_0

    .line 2077
    iget-object v1, p0, Lofb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2078
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofb;->ah:Ljava/util/List;

    .line 2081
    :cond_1
    iget-object v1, p0, Lofb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2083
    :sswitch_0
    return-object p0

    .line 2088
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofb;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 2092
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 2093
    iget-object v0, p0, Lofb;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 2094
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 2095
    iget-object v2, p0, Lofb;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2096
    iput-object v1, p0, Lofb;->b:[Ljava/lang/String;

    .line 2097
    :goto_1
    iget-object v1, p0, Lofb;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 2098
    iget-object v1, p0, Lofb;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2099
    invoke-virtual {p1}, Loxn;->a()I

    .line 2097
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2102
    :cond_2
    iget-object v1, p0, Lofb;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 2106
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lofb;->c:Ljava/lang/Long;

    goto :goto_0

    .line 2110
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lofb;->d:Ljava/lang/Long;

    goto :goto_0

    .line 2073
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2020
    iget-object v0, p0, Lofb;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2021
    const/4 v0, 0x1

    iget-object v1, p0, Lofb;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2023
    :cond_0
    iget-object v0, p0, Lofb;->b:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2024
    iget-object v1, p0, Lofb;->b:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2025
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 2024
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2028
    :cond_1
    iget-object v0, p0, Lofb;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2029
    const/4 v0, 0x3

    iget-object v1, p0, Lofb;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2031
    :cond_2
    iget-object v0, p0, Lofb;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2032
    const/4 v0, 0x4

    iget-object v1, p0, Lofb;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2034
    :cond_3
    iget-object v0, p0, Lofb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2036
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2002
    invoke-virtual {p0, p1}, Lofb;->a(Loxn;)Lofb;

    move-result-object v0

    return-object v0
.end method
