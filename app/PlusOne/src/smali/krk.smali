.class public final Lkrk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkrl;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lkrp;

.field private c:Lbs;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkrp;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 47
    iput-object p1, p0, Lkrk;->a:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lkrk;->b:Lkrp;

    .line 51
    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lkrk;->a(Z)V

    .line 52
    return-void
.end method

.method private a(Landroid/widget/RemoteViews;)V
    .locals 4

    .prologue
    const v3, 0x7f1001b1

    const/4 v0, 0x0

    .line 254
    .line 255
    iget-object v1, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v1}, Lkrp;->j()Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 257
    iget-object v1, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v1}, Lkrp;->j()Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 258
    const/4 v1, 0x1

    .line 259
    iget-object v2, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v2}, Lkrp;->j()Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p1, v3, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 262
    :goto_0
    if-eqz v1, :cond_0

    :goto_1
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 263
    return-void

    .line 262
    :cond_0
    const/4 v0, 0x4

    goto :goto_1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private a(Landroid/widget/RemoteViews;ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 243
    iget-object v0, p0, Lkrk;->a:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 245
    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 246
    return-void
.end method

.method private a(Z)V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const v11, 0x7f1001e2

    const/16 v1, 0x8

    const v10, 0x7f1001df

    const/4 v2, 0x0

    const v9, 0x7f1001e4

    .line 79
    const-string v0, "CastNotification"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x2b

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Showing cast notification, connected: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 83
    :cond_0
    invoke-direct {p0}, Lkrk;->e()Lbs;

    move-result-object v3

    .line 84
    new-instance v4, Landroid/widget/RemoteViews;

    iget-object v0, p0, Lkrk;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v5, 0x7f040060

    invoke-direct {v4, v0, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v5, 0x7f100185

    if-eqz p1, :cond_5

    iget-object v0, p0, Lkrk;->a:Landroid/content/Context;

    const v6, 0x7f0a0250

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v8}, Lkrp;->p()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v4, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v5, 0x7f1001e0

    if-eqz p1, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {v4, v5, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    if-eqz p1, :cond_7

    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    :goto_2
    invoke-virtual {v4, v9, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f0204e6

    invoke-virtual {v4, v10, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_3
    if-eqz p1, :cond_1

    invoke-direct {p0, v4}, Lkrk;->a(Landroid/widget/RemoteViews;)V

    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7f02054c

    :goto_4
    invoke-virtual {v4, v9, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :cond_1
    :goto_5
    const v0, 0x7f1001e3

    const-string v5, "com.google.android.libraries.social.socialcast.action.toggle_playpause"

    invoke-direct {p0, v4, v0, v5}, Lkrk;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    const-string v0, "com.google.android.libraries.social.socialcast.action.next"

    invoke-direct {p0, v4, v9, v0}, Lkrk;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    const-string v0, "com.google.android.libraries.social.socialcast.action.stop"

    invoke-direct {p0, v4, v10, v0}, Lkrk;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    invoke-virtual {v3, v4}, Lbs;->a(Landroid/widget/RemoteViews;)Lbs;

    .line 85
    invoke-virtual {v3}, Lbs;->c()Landroid/app/Notification;

    move-result-object v3

    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v0, v4, :cond_4

    .line 88
    new-instance v4, Landroid/widget/RemoteViews;

    iget-object v0, p0, Lkrk;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v5, 0x7f04005f

    invoke-direct {v4, v0, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v5, 0x7f100185

    if-eqz p1, :cond_c

    iget-object v0, p0, Lkrk;->a:Landroid/content/Context;

    const v6, 0x7f0a0250

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v8}, Lkrp;->p()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v4, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v5, 0x7f1001e0

    if-eqz p1, :cond_d

    move v0, v1

    :goto_7
    invoke-virtual {v4, v5, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f1001e1

    if-eqz p1, :cond_2

    move v1, v2

    :cond_2
    invoke-virtual {v4, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x7f0204e6

    invoke-virtual {v4, v10, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_8
    if-eqz p1, :cond_3

    invoke-direct {p0, v4}, Lkrk;->a(Landroid/widget/RemoteViews;)V

    const-string v0, "setEnabled"

    iget-object v1, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v1}, Lkrp;->f()Z

    move-result v1

    invoke-virtual {v4, v11, v0, v1}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    const-string v0, "setEnabled"

    iget-object v1, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v1}, Lkrp;->e()Z

    move-result v1

    invoke-virtual {v4, v9, v0, v1}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_12

    const v1, 0x7f1001e3

    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->d()Z

    move-result v0

    if-eqz v0, :cond_f

    const v0, 0x7f02052a

    :goto_9
    invoke-virtual {v4, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->f()Z

    move-result v0

    if-eqz v0, :cond_10

    const v0, 0x7f020550

    :goto_a
    invoke-virtual {v4, v11, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->e()Z

    move-result v0

    if-eqz v0, :cond_11

    const v0, 0x7f02054c

    :goto_b
    invoke-virtual {v4, v9, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :cond_3
    :goto_c
    const-string v0, "com.google.android.libraries.social.socialcast.action.prev"

    invoke-direct {p0, v4, v11, v0}, Lkrk;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    const v0, 0x7f1001e3

    const-string v1, "com.google.android.libraries.social.socialcast.action.toggle_playpause"

    invoke-direct {p0, v4, v0, v1}, Lkrk;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    const-string v0, "com.google.android.libraries.social.socialcast.action.next"

    invoke-direct {p0, v4, v9, v0}, Lkrk;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    const-string v0, "com.google.android.libraries.social.socialcast.action.stop"

    invoke-direct {p0, v4, v10, v0}, Lkrk;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    iput-object v4, v3, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 91
    :cond_4
    iget-object v0, p0, Lkrk;->a:Landroid/content/Context;

    const-string v1, "notification"

    .line 92
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 93
    invoke-virtual {v0, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 94
    return-void

    .line 84
    :cond_5
    iget-object v0, p0, Lkrk;->a:Landroid/content/Context;

    const v6, 0x7f0a057b

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto/16 :goto_1

    :cond_7
    move v0, v1

    goto/16 :goto_2

    :cond_8
    const v0, 0x7f0204e7

    invoke-virtual {v4, v10, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_3

    :cond_9
    const v0, 0x7f02054b

    goto/16 :goto_4

    :cond_a
    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->e()Z

    move-result v0

    if-eqz v0, :cond_b

    const v0, 0x7f02054d

    :goto_d
    invoke-virtual {v4, v9, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_5

    :cond_b
    const v0, 0x7f02054e

    goto :goto_d

    .line 88
    :cond_c
    iget-object v0, p0, Lkrk;->a:Landroid/content/Context;

    const v6, 0x7f0a057b

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    :cond_d
    move v0, v2

    goto/16 :goto_7

    :cond_e
    const v0, 0x7f0204e7

    invoke-virtual {v4, v10, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_8

    :cond_f
    const v0, 0x7f020536

    goto/16 :goto_9

    :cond_10
    const v0, 0x7f02054f

    goto/16 :goto_a

    :cond_11
    const v0, 0x7f02054b

    goto :goto_b

    :cond_12
    const v1, 0x7f1001e3

    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->d()Z

    move-result v0

    if-eqz v0, :cond_13

    const v0, 0x7f02052b

    :goto_e
    invoke-virtual {v4, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->f()Z

    move-result v0

    if-eqz v0, :cond_14

    const v0, 0x7f020551

    :goto_f
    invoke-virtual {v4, v11, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    iget-object v0, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v0}, Lkrp;->e()Z

    move-result v0

    if-eqz v0, :cond_15

    const v0, 0x7f02054d

    :goto_10
    invoke-virtual {v4, v9, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_c

    :cond_13
    const v0, 0x7f020537

    goto :goto_e

    :cond_14
    const v0, 0x7f02054e

    goto :goto_f

    :cond_15
    const v0, 0x7f02054e

    goto :goto_10
.end method

.method private e()Lbs;
    .locals 4

    .prologue
    .line 97
    iget-object v0, p0, Lkrk;->c:Lbs;

    if-nez v0, :cond_0

    .line 98
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lkrk;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    const-string v1, "account_id"

    iget-object v2, p0, Lkrk;->b:Lkrp;

    invoke-virtual {v2}, Lkrp;->o()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 100
    iget-object v1, p0, Lkrk;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 103
    new-instance v1, Lbs;

    iget-object v2, p0, Lkrk;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lbs;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0202dd

    .line 104
    invoke-virtual {v1, v2}, Lbs;->a(I)Lbs;

    move-result-object v1

    const/4 v2, 0x1

    .line 105
    invoke-virtual {v1, v2}, Lbs;->a(Z)Lbs;

    move-result-object v1

    .line 106
    invoke-virtual {v1, v0}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    move-result-object v0

    const-string v1, "social"

    .line 107
    invoke-virtual {v0, v1}, Lbs;->a(Ljava/lang/String;)Lbs;

    move-result-object v0

    iput-object v0, p0, Lkrk;->c:Lbs;

    .line 109
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lkrk;->c:Lbs;

    iget-object v1, p0, Lkrk;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbs;->d(I)Lbs;

    .line 113
    :cond_0
    iget-object v0, p0, Lkrk;->c:Lbs;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lkrk;->e()Lbs;

    move-result-object v0

    invoke-virtual {v0}, Lbs;->c()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkrk;->a(Z)V

    .line 65
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkrk;->a(Z)V

    .line 70
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lkrk;->a:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 75
    return-void
.end method
