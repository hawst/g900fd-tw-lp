.class public abstract Llde;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;
.implements Lljh;


# static fields
.field public static a:Llct;


# instance fields
.field public b:Landroid/graphics/Rect;

.field public final c:Landroid/graphics/Paint;

.field public d:J

.field private e:Lizu;

.field private f:Lcom/google/android/libraries/social/resources/images/ImageResource;

.field private g:Landroid/graphics/Rect;

.field private h:Landroid/graphics/Rect;

.field private i:I

.field private j:I

.field private k:F

.field private l:Ljava/lang/Runnable;

.field private m:Z

.field private n:Landroid/text/StaticLayout;

.field private o:Landroid/text/StaticLayout;

.field private p:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Llde;->k:F

    .line 46
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Llde;->c:Landroid/graphics/Paint;

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Llde;->m:Z

    .line 65
    sget-object v0, Llde;->a:Llct;

    if-nez v0, :cond_0

    .line 66
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Llde;->a:Llct;

    .line 69
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llde;->g:Landroid/graphics/Rect;

    .line 70
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llde;->h:Landroid/graphics/Rect;

    .line 71
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llde;->b:Landroid/graphics/Rect;

    .line 73
    iget-object v0, p0, Llde;->c:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 74
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Llde;->d:J

    .line 75
    return-void
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/text/StaticLayout;II)I
    .locals 2

    .prologue
    .line 385
    if-eqz p1, :cond_0

    .line 386
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 387
    invoke-virtual {p1, p0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 388
    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 389
    invoke-virtual {p1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p3, v0

    .line 391
    :cond_0
    return p3
.end method

.method private c(II)I
    .locals 2

    .prologue
    .line 261
    invoke-virtual {p0}, Llde;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Llde;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, p2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 268
    :goto_0
    return p1

    .line 264
    :cond_0
    iget-object v0, p0, Llde;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, p2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 265
    add-int/2addr p1, p2

    goto :goto_0
.end method


# virtual methods
.method public a(II)I
    .locals 3

    .prologue
    .line 290
    invoke-virtual {p0}, Llde;->e()Ljava/lang/String;

    move-result-object v0

    .line 291
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 292
    invoke-virtual {p0}, Llde;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x25

    invoke-static {v1, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    .line 294
    sget-object v2, Llde;->a:Llct;

    iget v2, v2, Llct;->Z:I

    invoke-static {v1, v0, p2, v2}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Llde;->o:Landroid/text/StaticLayout;

    .line 296
    iget-object v0, p0, Llde;->o:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    .line 297
    invoke-virtual {p0}, Llde;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 298
    iget-object v1, p0, Llde;->b:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    sub-int v0, v2, v0

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 305
    :cond_0
    :goto_0
    return p1

    .line 300
    :cond_1
    iget-object v1, p0, Llde;->b:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 301
    add-int/2addr p1, v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;II)I
    .locals 0

    .prologue
    .line 428
    return p2
.end method

.method public a(Landroid/graphics/Canvas;II)I
    .locals 0

    .prologue
    .line 432
    return p3
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 233
    iput-object v2, p0, Llde;->n:Landroid/text/StaticLayout;

    .line 234
    iput-object v2, p0, Llde;->o:Landroid/text/StaticLayout;

    .line 235
    iput-object v2, p0, Llde;->p:Landroid/text/StaticLayout;

    .line 237
    iput-object v2, p0, Llde;->e:Lizu;

    .line 238
    iget-object v0, p0, Llde;->g:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 239
    iget-object v0, p0, Llde;->h:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 240
    iget-object v0, p0, Llde;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 241
    iput v1, p0, Llde;->i:I

    .line 242
    iput v1, p0, Llde;->j:I

    .line 243
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Llde;->k:F

    .line 245
    iget-object v0, p0, Llde;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 246
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Llde;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 247
    iput-object v2, p0, Llde;->l:Ljava/lang/Runnable;

    .line 249
    :cond_0
    iget-object v0, p0, Llde;->c:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 250
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Llde;->d:J

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Llde;->m:Z

    .line 252
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 440
    invoke-virtual {p0, p1, p1}, Llde;->b(II)V

    .line 441
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 402
    invoke-virtual {p0}, Llde;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    sget-object v0, Llde;->a:Llct;

    iget-object v0, v0, Llct;->ad:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v1, p0, Llde;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Llde;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sget-object v3, Llde;->a:Llct;

    iget v3, v3, Llct;->m:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Llde;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Llde;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 407
    sget-object v0, Llde;->a:Llct;

    iget-object v0, v0, Llct;->ad:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 411
    :goto_0
    return-void

    .line 409
    :cond_0
    iget-object v0, p0, Llde;->b:Landroid/graphics/Rect;

    sget-object v1, Llde;->a:Llct;

    iget-object v1, v1, Llct;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public a(Lizu;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Llde;->e:Lizu;

    .line 450
    return-void
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 381
    invoke-virtual {p0}, Llde;->invalidate()V

    .line 382
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 363
    iget-object v0, p0, Llde;->e:Lizu;

    if-eqz v0, :cond_0

    .line 364
    sget-object v0, Llde;->a:Llct;

    iget-object v0, v0, Llct;->d:Lizs;

    iget-object v1, p0, Llde;->e:Lizu;

    const/4 v2, 0x3

    const/16 v3, 0x40

    invoke-virtual {v0, v1, v2, v3, p0}, Lizs;->a(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 367
    iget-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getResource()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Llde;->m:Z

    .line 369
    :cond_0
    return-void

    .line 367
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(II)V
    .locals 0

    .prologue
    .line 444
    iput p1, p0, Llde;->i:I

    .line 445
    iput p2, p0, Llde;->j:I

    .line 446
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->unregister(Lkdd;)V

    .line 375
    const/4 v0, 0x0

    iput-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    .line 377
    :cond_0
    return-void
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 256
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 257
    invoke-virtual {p0}, Llde;->invalidate()V

    .line 258
    return-void
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 436
    iget v0, p0, Llde;->j:I

    sget-object v1, Llde;->a:Llct;

    iget v1, v1, Llct;->E:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lizu;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Llde;->e:Lizu;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v12, 0x0

    const/4 v2, 0x0

    .line 114
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 121
    invoke-virtual {p0}, Llde;->getMeasuredWidth()I

    move-result v7

    .line 122
    iget-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    if-eqz v0, :cond_18

    .line 123
    iget-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 124
    iget-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getWidth()I

    move-result v3

    .line 125
    iget-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getHeight()I

    move-result v0

    move-object v8, v4

    .line 127
    :goto_0
    if-eqz v8, :cond_15

    .line 128
    iget-boolean v4, p0, Llde;->m:Z

    if-eqz v4, :cond_0

    .line 129
    iget-boolean v4, p0, Llde;->m:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Llde;->c:Landroid/graphics/Paint;

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v10, 0xfa

    add-long/2addr v4, v10

    iput-wide v4, p0, Llde;->d:J

    iput-boolean v2, p0, Llde;->m:Z

    iget-object v4, p0, Llde;->l:Ljava/lang/Runnable;

    if-eqz v4, :cond_c

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Llde;->l:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :goto_1
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Llde;->l:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 132
    :cond_0
    const/high16 v4, 0x3f800000    # 1.0f

    int-to-float v5, v0

    mul-float/2addr v4, v5

    int-to-float v5, v3

    div-float v5, v4, v5

    .line 133
    iget-object v4, p0, Llde;->g:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 134
    iget-object v4, p0, Llde;->g:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v2, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 137
    :cond_1
    iget v0, p0, Llde;->k:F

    sub-float/2addr v0, v5

    .line 138
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v3, 0x3c23d70a    # 0.01f

    cmpg-float v0, v0, v3

    if-gez v0, :cond_d

    move v0, v1

    .line 140
    :goto_2
    iget-object v3, p0, Llde;->h:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 141
    if-eqz v0, :cond_e

    .line 142
    iget v3, p0, Llde;->i:I

    sub-int v3, v7, v3

    div-int/lit8 v3, v3, 0x2

    .line 143
    iget-object v4, p0, Llde;->h:Landroid/graphics/Rect;

    sub-int v5, v7, v3

    iget v6, p0, Llde;->j:I

    invoke-virtual {v4, v3, v2, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 163
    :cond_2
    :goto_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 164
    iget v3, p0, Llde;->j:I

    add-int/lit8 v3, v3, 0x0

    invoke-virtual {p1, v2, v2, v7, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 165
    invoke-virtual {p1, v12, v12}, Landroid/graphics/Canvas;->translate(FF)V

    .line 167
    if-nez v0, :cond_3

    .line 171
    iget-object v0, p0, Llde;->h:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v3, p0, Llde;->j:I

    if-ge v0, v3, :cond_10

    .line 172
    :goto_4
    sget-object v0, Llde;->a:Llct;

    iget-object v4, v0, Llct;->H:Landroid/graphics/Rect;

    if-eqz v1, :cond_11

    move v3, v7

    :goto_5
    if-eqz v1, :cond_12

    iget-object v0, p0, Llde;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    :goto_6
    invoke-virtual {v4, v2, v2, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 174
    sget-object v0, Llde;->a:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    sget-object v3, Llde;->a:Llct;

    iget-object v3, v3, Llct;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 176
    sget-object v0, Llde;->a:Llct;

    iget-object v4, v0, Llct;->H:Landroid/graphics/Rect;

    if-eqz v1, :cond_13

    move v3, v2

    :goto_7
    if-eqz v1, :cond_14

    iget-object v0, p0, Llde;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    :goto_8
    iget v1, p0, Llde;->j:I

    invoke-virtual {v4, v3, v0, v7, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 178
    sget-object v0, Llde;->a:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    sget-object v1, Llde;->a:Llct;

    iget-object v1, v1, Llct;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 181
    :cond_3
    iget-object v0, p0, Llde;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Llde;->h:Landroid/graphics/Rect;

    iget-object v3, p0, Llde;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 182
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 195
    :cond_4
    :goto_9
    iget v0, p0, Llde;->j:I

    add-int/lit8 v0, v0, 0x0

    .line 197
    invoke-virtual {p0}, Llde;->g()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 198
    iget-object v1, p0, Llde;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v0, v1

    .line 200
    :cond_5
    invoke-virtual {p0, p1}, Llde;->a(Landroid/graphics/Canvas;)V

    .line 202
    sget-object v1, Llde;->a:Llct;

    iget v1, v1, Llct;->m:I

    add-int/lit8 v1, v1, 0x0

    .line 204
    iget-object v3, p0, Llde;->n:Landroid/text/StaticLayout;

    if-eqz v3, :cond_6

    .line 205
    sget-object v3, Llde;->a:Llct;

    iget v3, v3, Llct;->m:I

    add-int/2addr v0, v3

    .line 206
    iget-object v3, p0, Llde;->n:Landroid/text/StaticLayout;

    invoke-static {p1, v3, v1, v0}, Llde;->a(Landroid/graphics/Canvas;Landroid/text/StaticLayout;II)I

    move-result v0

    .line 209
    :cond_6
    iget-object v3, p0, Llde;->o:Landroid/text/StaticLayout;

    if-nez v3, :cond_7

    iget-object v3, p0, Llde;->p:Landroid/text/StaticLayout;

    if-eqz v3, :cond_8

    .line 210
    :cond_7
    sget-object v3, Llde;->a:Llct;

    iget v3, v3, Llct;->m:I

    add-int/2addr v0, v3

    .line 213
    :cond_8
    iget-object v3, p0, Llde;->o:Landroid/text/StaticLayout;

    invoke-static {p1, v3, v1, v0}, Llde;->a(Landroid/graphics/Canvas;Landroid/text/StaticLayout;II)I

    move-result v0

    .line 214
    iget-object v3, p0, Llde;->p:Landroid/text/StaticLayout;

    invoke-static {p1, v3, v1, v0}, Llde;->a(Landroid/graphics/Canvas;Landroid/text/StaticLayout;II)I

    move-result v3

    .line 216
    sget-object v0, Llde;->a:Llct;

    iget v0, v0, Llct;->m:I

    sub-int/2addr v1, v0

    .line 218
    iget-object v0, p0, Llde;->o:Landroid/text/StaticLayout;

    if-nez v0, :cond_9

    iget-object v0, p0, Llde;->p:Landroid/text/StaticLayout;

    if-eqz v0, :cond_17

    :cond_9
    sget-object v0, Llde;->a:Llct;

    iget v0, v0, Llct;->o:I

    :goto_a
    add-int/2addr v0, v3

    .line 220
    invoke-virtual {p0, p1, v1, v0}, Llde;->a(Landroid/graphics/Canvas;II)I

    .line 222
    sget-object v0, Llde;->a:Llct;

    iget v0, v0, Llct;->m:I

    .line 224
    invoke-virtual {p0}, Llde;->isPressed()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0}, Llde;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 225
    :cond_a
    sget-object v0, Llde;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Llde;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Llde;->getHeight()I

    move-result v3

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 226
    sget-object v0, Llde;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 229
    :cond_b
    return-void

    .line 129
    :cond_c
    new-instance v4, Lldf;

    invoke-direct {v4, p0}, Lldf;-><init>(Llde;)V

    iput-object v4, p0, Llde;->l:Ljava/lang/Runnable;

    goto/16 :goto_1

    :cond_d
    move v0, v2

    .line 138
    goto/16 :goto_2

    .line 146
    :cond_e
    int-to-float v3, v7

    mul-float/2addr v3, v5

    float-to-int v4, v3

    .line 150
    iget v3, p0, Llde;->j:I

    if-gt v4, v3, :cond_f

    .line 151
    iget v3, p0, Llde;->j:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    move v5, v4

    move v6, v7

    move v4, v2

    .line 157
    :goto_b
    iget-object v9, p0, Llde;->h:Landroid/graphics/Rect;

    add-int/2addr v6, v4

    add-int/2addr v5, v3

    invoke-virtual {v9, v4, v3, v6, v5}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_3

    .line 153
    :cond_f
    iget v4, p0, Llde;->j:I

    .line 154
    iget v3, p0, Llde;->j:I

    int-to-float v3, v3

    div-float/2addr v3, v5

    float-to-int v5, v3

    .line 155
    sub-int v3, v7, v5

    div-int/lit8 v3, v3, 0x2

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v2

    goto :goto_b

    :cond_10
    move v1, v2

    .line 171
    goto/16 :goto_4

    .line 172
    :cond_11
    iget-object v0, p0, Llde;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move v3, v0

    goto/16 :goto_5

    :cond_12
    iget v0, p0, Llde;->j:I

    goto/16 :goto_6

    .line 176
    :cond_13
    iget-object v0, p0, Llde;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move v3, v0

    goto/16 :goto_7

    :cond_14
    move v0, v2

    goto/16 :goto_8

    .line 183
    :cond_15
    iget-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    if-eqz v0, :cond_4

    .line 184
    iget-object v0, p0, Llde;->f:Lcom/google/android/libraries/social/resources/images/ImageResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getStatus()I

    move-result v0

    .line 185
    sget-object v1, Llde;->a:Llct;

    iget-object v1, v1, Llct;->bc:Landroid/graphics/Bitmap;

    .line 186
    const/4 v3, 0x5

    if-eq v0, v3, :cond_16

    const/4 v3, 0x3

    if-ne v0, v3, :cond_4

    .line 189
    :cond_16
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int v0, v7, v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    iget v3, p0, Llde;->j:I

    .line 190
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    .line 188
    invoke-virtual {p1, v1, v0, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_9

    .line 218
    :cond_17
    sget-object v0, Llde;->a:Llct;

    iget v0, v0, Llct;->m:I

    goto/16 :goto_a

    :cond_18
    move v0, v2

    move v3, v2

    move-object v8, v5

    goto/16 :goto_0
.end method

.method public onMeasure(II)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 81
    iget v4, p0, Llde;->j:I

    .line 82
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 84
    iget-object v0, p0, Llde;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v2, v4, v5, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 86
    sget-object v0, Llde;->a:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v6, v5, v0

    .line 88
    invoke-virtual {p0}, Llde;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 89
    :goto_0
    invoke-virtual {p0}, Llde;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    move v3, v1

    .line 90
    :goto_1
    invoke-virtual {p0}, Llde;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    move v2, v1

    .line 92
    :cond_0
    if-eqz v0, :cond_c

    .line 93
    sget-object v0, Llde;->a:Llct;

    iget v0, v0, Llct;->m:I

    invoke-direct {p0, v4, v0}, Llde;->c(II)I

    move-result v0

    .line 94
    invoke-virtual {p0}, Llde;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p0}, Llde;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v7, 0x22

    invoke-static {v4, v7}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sget-object v7, Llde;->a:Llct;

    iget v7, v7, Llct;->Y:I

    invoke-static {v4, v1, v6, v7}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Llde;->n:Landroid/text/StaticLayout;

    iget-object v1, p0, Llde;->n:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Llde;->g()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Llde;->b:Landroid/graphics/Rect;

    iget v7, v4, Landroid/graphics/Rect;->top:I

    sub-int v1, v7, v1

    iput v1, v4, Landroid/graphics/Rect;->top:I

    .line 97
    :cond_1
    :goto_2
    if-nez v2, :cond_2

    if-eqz v3, :cond_b

    .line 98
    :cond_2
    sget-object v1, Llde;->a:Llct;

    iget v1, v1, Llct;->m:I

    invoke-direct {p0, v0, v1}, Llde;->c(II)I

    move-result v0

    .line 99
    invoke-virtual {p0, v0, v6}, Llde;->a(II)I

    move-result v0

    .line 100
    invoke-virtual {p0}, Llde;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {p0}, Llde;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v7, 0xb

    invoke-static {v4, v7}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sget-object v7, Llde;->a:Llct;

    iget v7, v7, Llct;->Z:I

    invoke-static {v4, v1, v6, v7}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Llde;->p:Landroid/text/StaticLayout;

    iget-object v1, p0, Llde;->p:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Llde;->g()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Llde;->b:Landroid/graphics/Rect;

    iget v6, v4, Landroid/graphics/Rect;->top:I

    sub-int v1, v6, v1

    iput v1, v4, Landroid/graphics/Rect;->top:I

    :cond_3
    :goto_3
    move v1, v0

    .line 103
    :goto_4
    if-nez v2, :cond_4

    if-eqz v3, :cond_9

    :cond_4
    sget-object v0, Llde;->a:Llct;

    iget v0, v0, Llct;->o:I

    .line 105
    :goto_5
    invoke-direct {p0, v1, v0}, Llde;->c(II)I

    move-result v0

    .line 106
    invoke-virtual {p0}, Llde;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, v0, v5}, Llde;->a(Landroid/content/Context;II)I

    move-result v1

    sub-int/2addr v1, v0

    invoke-virtual {p0}, Llde;->g()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Llde;->b:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    sub-int v1, v3, v1

    iput v1, v2, Landroid/graphics/Rect;->top:I

    .line 107
    :goto_6
    sget-object v1, Llde;->a:Llct;

    iget v1, v1, Llct;->m:I

    invoke-direct {p0, v0, v1}, Llde;->c(II)I

    move-result v0

    .line 109
    invoke-virtual {p0, v5, v0}, Llde;->setMeasuredDimension(II)V

    .line 110
    return-void

    :cond_5
    move v0, v2

    .line 88
    goto/16 :goto_0

    :cond_6
    move v3, v2

    .line 89
    goto/16 :goto_1

    .line 94
    :cond_7
    iget-object v4, p0, Llde;->b:Landroid/graphics/Rect;

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v1

    iput v7, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    goto :goto_2

    .line 100
    :cond_8
    iget-object v4, p0, Llde;->b:Landroid/graphics/Rect;

    iget v6, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v1

    iput v6, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    goto :goto_3

    .line 103
    :cond_9
    sget-object v0, Llde;->a:Llct;

    iget v0, v0, Llct;->m:I

    goto :goto_5

    .line 106
    :cond_a
    iget-object v2, p0, Llde;->b:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    goto :goto_6

    :cond_b
    move v1, v0

    goto :goto_4

    :cond_c
    move v0, v4

    goto/16 :goto_2
.end method
