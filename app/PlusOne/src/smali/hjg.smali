.class public final Lhjg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhjf;
.implements Llnx;
.implements Llpr;
.implements Llpv;
.implements Llpx;
.implements Llre;
.implements Llrg;


# instance fields
.field private final a:Los;

.field private b:I

.field private c:Z

.field private d:Landroid/os/Handler;

.field private e:Ljava/lang/Runnable;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhjj;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhjj;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhjj;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lhjc;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lhji;


# direct methods
.method public constructor <init>(Los;Llqr;I)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lhjg;->d:Landroid/os/Handler;

    .line 41
    new-instance v0, Lhjh;

    invoke-direct {v0, p0}, Lhjh;-><init>(Lhjg;)V

    iput-object v0, p0, Lhjg;->e:Ljava/lang/Runnable;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhjg;->f:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhjg;->g:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhjg;->h:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lhjg;->i:Landroid/util/SparseArray;

    .line 59
    iput-object p1, p0, Lhjg;->a:Los;

    .line 60
    iput p3, p0, Lhjg;->b:I

    .line 61
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 62
    return-void
.end method

.method static synthetic a(Lhjg;)Los;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lhjg;->a:Los;

    return-object v0
.end method

.method static synthetic b(Lhjg;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lhjg;->i:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic c(Lhjg;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lhjg;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lhjg;)Loo;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lhjg;->a:Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 166
    iget-boolean v0, p0, Lhjg;->c:Z

    if-nez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 170
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 171
    iget-object v0, p0, Lhjg;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 172
    iget-object v0, p0, Lhjg;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Lhjg;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    :goto_1
    iget-object v0, p0, Lhjg;->a:Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v4

    .line 179
    iget-object v5, p0, Lhjg;->h:Ljava/util/ArrayList;

    move v1, v2

    .line 180
    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 181
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjj;

    .line 182
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 183
    invoke-interface {v0, v4}, Lhjj;->b(Loo;)V

    .line 180
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 174
    :cond_2
    iget-object v0, p0, Lhjg;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 187
    :cond_3
    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 188
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjj;

    .line 189
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 190
    invoke-interface {v0, v4}, Lhjj;->a(Loo;)V

    .line 187
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 194
    :cond_5
    iput-object v3, p0, Lhjg;->h:Ljava/util/ArrayList;

    .line 195
    invoke-virtual {p0}, Lhjg;->b()V

    goto :goto_0
.end method


# virtual methods
.method public synthetic a(Lhjj;)Lhjf;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lhjg;->e(Lhjj;)Lhjg;

    move-result-object v0

    return-object v0
.end method

.method public a(Llnh;)Lhjg;
    .locals 1

    .prologue
    .line 65
    const-class v0, Lhjf;

    invoke-virtual {p1, v0, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 66
    return-object p0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhjg;->c:Z

    .line 84
    invoke-direct {p0}, Lhjg;->d()V

    .line 85
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 71
    const-class v0, Lhjd;

    invoke-virtual {p2, v0}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjd;

    .line 72
    invoke-interface {v0}, Lhjd;->a()I

    move-result v2

    .line 73
    iget-object v3, p0, Lhjg;->i:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 74
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Multiple ActionBarController: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :cond_0
    iget-object v3, p0, Lhjg;->a:Los;

    invoke-interface {v0, v3}, Lhjd;->a(Landroid/app/Activity;)Lhjc;

    move-result-object v0

    .line 77
    iget-object v3, p0, Lhjg;->i:Landroid/util/SparseArray;

    invoke-virtual {v3, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 79
    :cond_1
    return-void
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lhjg;->a:Los;

    invoke-virtual {v0}, Los;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget v1, p0, Lhjg;->b:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 90
    new-instance v0, Lhji;

    invoke-direct {v0, p0, p1}, Lhji;-><init>(Lhjg;Landroid/view/Menu;)V

    iput-object v0, p0, Lhjg;->j:Lhji;

    .line 91
    iget-object v0, p0, Lhjg;->j:Lhji;

    invoke-virtual {v0}, Lhji;->a()V

    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 102
    iget-object v0, p0, Lhjg;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_1

    .line 103
    iget-object v0, p0, Lhjg;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjj;

    .line 104
    invoke-interface {v0, p1}, Lhjj;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 118
    :goto_1
    return v0

    .line 102
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 109
    :cond_1
    iget-object v0, p0, Lhjg;->j:Lhji;

    iget-object v0, v0, Lhji;->a:Landroid/util/SparseArray;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 110
    if-eqz v0, :cond_3

    .line 111
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjx;

    .line 112
    iget-object v3, p0, Lhjg;->a:Los;

    invoke-interface {v0, v3}, Lhjx;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 113
    goto :goto_1

    .line 118
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public synthetic b(Lhjj;)Lhjf;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lhjg;->f(Lhjj;)Lhjg;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lhjg;->d:Landroid/os/Handler;

    iget-object v1, p0, Lhjg;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 127
    iget-object v0, p0, Lhjg;->d:Landroid/os/Handler;

    iget-object v1, p0, Lhjg;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 128
    return-void
.end method

.method public c(Lhjj;)V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lhjg;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    invoke-direct {p0}, Lhjg;->d()V

    .line 157
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    return v0
.end method

.method public d(Lhjj;)V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lhjg;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 162
    invoke-direct {p0}, Lhjg;->d()V

    .line 163
    return-void
.end method

.method public e(Lhjj;)Lhjg;
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lhjg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to add ActionBarListener twice"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    iget-object v0, p0, Lhjg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-direct {p0}, Lhjg;->d()V

    .line 139
    return-object p0
.end method

.method public f(Lhjj;)Lhjg;
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lhjg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to remove non-present ActionBarListener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_0
    iget-object v0, p0, Lhjg;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 149
    invoke-direct {p0}, Lhjg;->d()V

    .line 150
    return-object p0
.end method
