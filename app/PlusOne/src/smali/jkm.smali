.class public final Ljkm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjm;
.implements Ljjp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "com.google.android.libraries.social.notifications.SYSTEM_NOTIFICATION_DISMISSED"

    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 12

    .prologue
    .line 22
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->a(Landroid/content/Intent;)Lkfo;

    move-result-object v1

    .line 23
    if-nez v1, :cond_0

    .line 42
    :goto_0
    return-void

    .line 27
    :cond_0
    const-string v0, "notification_key_list"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 31
    invoke-virtual {v1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v3

    .line 30
    invoke-static {p2, v0, v3, v2}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)[Ljiu;

    move-result-object v7

    .line 33
    const-string v0, "notification_event_type"

    .line 34
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljir;

    .line 36
    const-string v0, "view_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 38
    const-string v0, "AST"

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x0

    invoke-static {p2}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v8

    invoke-virtual {v8, v3, v5}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v8

    if-nez v8, :cond_2

    const-string v0, "GunsSyncer"

    const-string v3, "Cannot find database helper for account."

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    const/4 v3, 0x5

    move-object v0, p2

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Ljkh;->a(Landroid/content/Context;Lkfo;[Ljava/lang/String;ILjava/lang/String;Ljjp;)V

    .line 41
    invoke-static {p2, v7, v6}, Ljkh;->a(Landroid/content/Context;[Ljiu;Ljir;)V

    goto :goto_0

    .line 38
    :cond_2
    invoke-virtual {v8}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    :try_start_0
    new-instance v9, Landroid/content/ContentValues;

    const/4 v10, 0x1

    invoke-direct {v9, v10}, Landroid/content/ContentValues;-><init>(I)V

    const-string v10, "push_enabled"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v9, v10, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v0, "key"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    array-length v0, v2

    invoke-static {v0}, Ljla;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {v10, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const-string v10, "notifications"

    invoke-virtual {v8, v10, v9, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v0, Ljkn;

    invoke-direct {v0}, Ljkn;-><init>()V

    invoke-virtual {v0, p2, v3, v5}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_1

    :cond_3
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v0
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Ljkm;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
