.class public final Lkry;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljgg;
.implements Llrb;
.implements Llrc;
.implements Llrg;


# static fields
.field private static final j:Lloy;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lvq;

.field private c:Landroid/support/v7/app/MediaRouteButton;

.field private d:Landroid/view/View;

.field private e:Lvn;

.field private f:Lvp;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lloy;

    const-string v1, "enable_socialcast"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkry;->j:Lloy;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 113
    iget-object v0, p0, Lkry;->f:Lvp;

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-boolean v0, p0, Lkry;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkry;->f:Lvp;

    iget-object v2, p0, Lkry;->e:Lvn;

    invoke-virtual {v0, v2, v1}, Lvp;->a(Lvn;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 119
    :goto_1
    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lkry;->c:Landroid/support/v7/app/MediaRouteButton;

    const/4 v2, -0x1

    invoke-static {v0, v2}, Lhly;->a(Landroid/view/View;I)V

    .line 121
    iget-object v0, p0, Lkry;->c:Landroid/support/v7/app/MediaRouteButton;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lkry;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 117
    goto :goto_1

    .line 124
    :cond_2
    iget-object v0, p0, Lkry;->c:Landroid/support/v7/app/MediaRouteButton;

    invoke-virtual {v0, v3}, Landroid/support/v7/app/MediaRouteButton;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lkry;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lkry;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lkry;->a()V

    return-void
.end method

.method static synthetic b(Lkry;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lkry;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lkry;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lkry;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lkry;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lkry;->i:I

    return v0
.end method


# virtual methods
.method public a(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 91
    iput-boolean p1, p0, Lkry;->g:Z

    .line 92
    iput-object p2, p0, Lkry;->h:Ljava/lang/String;

    .line 94
    invoke-direct {p0}, Lkry;->a()V

    .line 95
    return-void
.end method

.method public a(Landroid/content/Context;ILandroid/support/v7/app/MediaRouteButton;Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 55
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 57
    sget-object v1, Lkry;->j:Lloy;

    sget-object v1, Lkrx;->a:Lief;

    .line 58
    invoke-interface {v0, v1, p2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    iput-object p1, p0, Lkry;->a:Landroid/content/Context;

    .line 60
    iput-object p3, p0, Lkry;->c:Landroid/support/v7/app/MediaRouteButton;

    .line 61
    iput-object p4, p0, Lkry;->d:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lkry;->a:Landroid/content/Context;

    invoke-static {v0}, Lvp;->a(Landroid/content/Context;)Lvp;

    move-result-object v0

    iput-object v0, p0, Lkry;->f:Lvp;

    .line 63
    iput p2, p0, Lkry;->i:I

    .line 65
    iget-object v0, p0, Lkry;->a:Landroid/content/Context;

    .line 66
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Liig;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liig;

    .line 67
    new-instance v1, Lvo;

    invoke-direct {v1}, Lvo;-><init>()V

    .line 68
    invoke-static {p1, p2}, Lksd;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Liig;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-virtual {v1, v0}, Lvo;->a(Ljava/lang/String;)Lvo;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lvo;->a()Lvn;

    move-result-object v0

    iput-object v0, p0, Lkry;->e:Lvn;

    .line 71
    new-instance v0, Lksa;

    invoke-direct {v0, p0}, Lksa;-><init>(Lkry;)V

    iput-object v0, p0, Lkry;->b:Lvq;

    .line 73
    iget-object v0, p0, Lkry;->c:Landroid/support/v7/app/MediaRouteButton;

    new-instance v1, Lhmk;

    sget-object v2, Lonk;->a:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 75
    iget-object v0, p0, Lkry;->c:Landroid/support/v7/app/MediaRouteButton;

    new-instance v1, Lhmi;

    new-instance v2, Lkrz;

    invoke-direct {v2}, Lkrz;-><init>()V

    invoke-direct {v1, v2}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v0, p0, Lkry;->c:Landroid/support/v7/app/MediaRouteButton;

    iget-object v1, p0, Lkry;->e:Lvn;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->a(Lvn;)V

    .line 82
    invoke-direct {p0}, Lkry;->a()V

    .line 83
    const/4 v0, 0x1

    .line 85
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Lkry;->f:Lvp;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lkry;->f:Lvp;

    iget-object v1, p0, Lkry;->e:Lvn;

    iget-object v2, p0, Lkry;->b:Lvq;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lvp;->a(Lvn;Lvq;I)V

    .line 103
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lkry;->f:Lvp;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lkry;->f:Lvp;

    iget-object v1, p0, Lkry;->b:Lvq;

    invoke-virtual {v0, v1}, Lvp;->a(Lvq;)V

    .line 110
    :cond_0
    return-void
.end method
