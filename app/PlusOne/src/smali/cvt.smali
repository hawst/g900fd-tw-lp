.class final Lcvt;
.super Lfhh;
.source "PG"


# instance fields
.field private synthetic a:Lcvq;


# direct methods
.method constructor <init>(Lcvq;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcvt;->a:Lcvq;

    invoke-direct {p0}, Lfhh;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/io/File;ZLjava/lang/String;Ljava/lang/String;Lfib;)V
    .locals 4

    .prologue
    .line 213
    iget-object v0, p0, Lcvt;->a:Lcvq;

    invoke-static {v0}, Lcvq;->b(Lcvq;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvt;->a:Lcvq;

    invoke-static {v0}, Lcvq;->b(Lcvq;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    iget-object v0, p0, Lcvt;->a:Lcvq;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcvq;->a(Lcvq;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 218
    iget-object v0, p0, Lcvt;->a:Lcvq;

    invoke-virtual {v0}, Lcvq;->p()Lae;

    move-result-object v0

    invoke-static {v0}, Ldhh;->a(Lae;)V

    .line 220
    if-eqz p6, :cond_4

    invoke-virtual {p6}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 221
    const-string v0, "PhotoDownloadFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    invoke-virtual {p6}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 224
    const-string v0, "PhotoDownloadFragment"

    const-string v1, "Could not download image"

    invoke-virtual {p6}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 230
    :cond_2
    :goto_1
    iget-object v0, p0, Lcvt;->a:Lcvq;

    invoke-static {v0, p3}, Lcvq;->a(Lcvq;Z)V

    goto :goto_0

    .line 226
    :cond_3
    const-string v0, "PhotoDownloadFragment"

    invoke-virtual {p6}, Lfib;->c()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not download image: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 232
    :cond_4
    iget-object v0, p0, Lcvt;->a:Lcvq;

    invoke-virtual {v0}, Lcvq;->n()Lz;

    move-result-object v0

    .line 233
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 234
    invoke-static {v0, p2, p4, p5}, Lcvq;->a(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_5
    const v1, 0x7f0a05f1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
