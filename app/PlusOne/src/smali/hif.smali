.class public final Lhif;
.super Llol;
.source "PG"

# interfaces
.implements Lhiw;
.implements Lhja;
.implements Lkc;


# instance fields
.field private N:Landroid/support/v4/view/ViewPager;

.field private O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhzd;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private T:Lhhx;

.field private U:Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;

.field private V:Landroid/view/ViewGroup;

.field private W:Lhzl;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Llol;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhif;->P:Ljava/util/List;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhif;->Q:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhif;->R:Ljava/util/List;

    .line 92
    new-instance v0, Lhip;

    iget-object v1, p0, Lhif;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhip;-><init>(Lu;Llqr;)V

    .line 93
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    .line 126
    const v0, 0x7f040028

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 128
    new-instance v5, Lhjb;

    .line 129
    invoke-virtual {p0}, Lhif;->q()Lae;

    move-result-object v0

    invoke-direct {v5, v0}, Lhjb;-><init>(Lae;)V

    .line 131
    const v0, 0x7f100164

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lhif;->N:Landroid/support/v4/view/ViewPager;

    .line 132
    iget-object v0, p0, Lhif;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->a(Lkc;)V

    .line 136
    invoke-virtual {p0}, Lhif;->k()Landroid/os/Bundle;

    move-result-object v6

    .line 138
    if-eqz v6, :cond_0

    .line 139
    const-string v0, "AclPickerFragment.SHAREOUSEL_ORDER"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lhif;->S:Ljava/util/ArrayList;

    .line 140
    iget-object v0, p0, Lhif;->S:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 141
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lhif;->O:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lhif;->O:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzd;

    invoke-interface {v0}, Lhzd;->a()Ljava/lang/String;

    move-result-object v0

    const-string v7, ".search"

    invoke-virtual {v0, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhif;->O:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzd;

    invoke-interface {v0}, Lhzd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iput-object v2, p0, Lhif;->S:Ljava/util/ArrayList;

    .line 149
    :cond_3
    iget-object v0, p0, Lhif;->S:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 150
    const/4 v2, 0x0

    .line 151
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v7, :cond_4

    .line 152
    iget-object v0, p0, Lhif;->S:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 153
    iget-object v1, p0, Lhif;->Q:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 154
    const-string v8, "index %s detected for %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v0, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 155
    const/4 v8, -0x1

    if-eq v1, v8, :cond_7

    .line 156
    iget-object v8, p0, Lhif;->O:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhzd;

    .line 159
    iget-object v8, p0, Lhif;->S:Ljava/util/ArrayList;

    invoke-interface {v1, v8}, Lhzd;->a(Ljava/util/ArrayList;)Lu;

    move-result-object v8

    .line 160
    if-eqz v8, :cond_7

    .line 161
    const-string v9, "Adding %s to Shareousel"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lhif;->P:Ljava/util/List;

    invoke-interface {v1}, Lhzd;->e()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lhif;->R:Ljava/util/List;

    invoke-interface {v1}, Lhzd;->b()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    invoke-interface {v1}, Lhzd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v8}, Lhjb;->a(Ljava/lang/String;Lu;)V

    .line 166
    add-int/lit8 v0, v2, 0x1

    .line 151
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_1

    .line 171
    :cond_4
    iget-object v0, p0, Lhif;->N:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v1, v2, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->b(I)V

    .line 172
    iget-object v0, p0, Lhif;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->a(Lip;)V

    .line 175
    const v0, 0x7f100161

    .line 176
    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lhif;->V:Landroid/view/ViewGroup;

    .line 177
    const v0, 0x7f100162

    .line 178
    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;

    iput-object v0, p0, Lhif;->U:Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;

    .line 179
    iget-object v0, p0, Lhif;->U:Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a(I)V

    .line 182
    if-eqz v6, :cond_6

    .line 183
    const-string v0, "AclPickerFragment.INITIAL_SELECTION_SLIDE"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 184
    const-string v1, "selecting slide: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 185
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 186
    iget-object v1, p0, Lhif;->S:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 187
    iget-object v1, p0, Lhif;->N:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 188
    iget-object v1, p0, Lhif;->N:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v1

    if-ne v1, v0, :cond_5

    .line 189
    invoke-virtual {p0, v0}, Lhif;->g_(I)V

    .line 191
    :cond_5
    iget-object v1, p0, Lhif;->U:Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b(I)V

    .line 195
    :cond_6
    return-object v4

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public a(IFI)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 112
    if-nez p1, :cond_0

    .line 113
    invoke-virtual {p0}, Lhif;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_0

    .line 115
    const-string v1, "AclPickerFragment.INITIAL_SELECTION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_0

    .line 117
    iget-object v1, p0, Lhif;->W:Lhzl;

    invoke-interface {v1, v0}, Lhzl;->a(Ljava/util/Collection;)V

    .line 121
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 226
    iget-object v0, p0, Lhif;->S:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 227
    if-ltz v0, :cond_0

    iget-object v1, p0, Lhif;->S:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 228
    iget-object v1, p0, Lhif;->N:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 230
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lhif;->V:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 220
    iget-object v1, p0, Lhif;->V:Landroid/view/ViewGroup;

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 222
    :cond_0
    return-void

    .line 220
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 97
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 98
    iget-object v0, p0, Lhif;->au:Llnh;

    const-class v1, Lhhx;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhx;

    iput-object v0, p0, Lhif;->T:Lhhx;

    .line 99
    iget-object v0, p0, Lhif;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Lhif;->W:Lhzl;

    .line 100
    iget-object v0, p0, Lhif;->au:Llnh;

    const-class v1, Lhzd;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lhif;->O:Ljava/util/List;

    .line 101
    iget-object v0, p0, Lhif;->au:Llnh;

    const-class v1, Lhiv;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhiv;

    .line 102
    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {v0, p0}, Lhiv;->a(Lhiw;)V

    .line 105
    :cond_0
    iget-object v0, p0, Lhif;->O:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const-string v0, "%s factories found."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lhif;->O:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzd;

    invoke-interface {v0}, Lhzd;->a()Ljava/lang/String;

    move-result-object v0

    const-string v4, "indexing %s as %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v4, p0, Lhif;->Q:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 106
    :cond_1
    return-void
.end method

.method public g_(I)V
    .locals 2

    .prologue
    .line 200
    iget-object v1, p0, Lhif;->T:Lhhx;

    iget-object v0, p0, Lhif;->P:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lhhx;->a(Z)V

    .line 201
    iget-object v1, p0, Lhif;->T:Lhhx;

    iget-object v0, p0, Lhif;->R:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lhhx;->a(I)V

    .line 202
    iget-object v0, p0, Lhif;->U:Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lhif;->U:Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b(I)V

    .line 205
    :cond_0
    return-void
.end method
