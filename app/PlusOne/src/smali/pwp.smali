.class public final Lpwp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpwp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Float;

.field public e:Ljava/lang/Float;

.field public f:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x2624fdb

    new-instance v1, Lpwq;

    invoke-direct {v1}, Lpwq;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpwp;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x0

    .line 51
    iget-object v1, p0, Lpwp;->b:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 52
    const/4 v0, 0x1

    iget-object v1, p0, Lpwp;->b:Ljava/lang/Float;

    .line 53
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 55
    :cond_0
    iget-object v1, p0, Lpwp;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 56
    const/4 v1, 0x2

    iget-object v2, p0, Lpwp;->c:Ljava/lang/Float;

    .line 57
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 59
    :cond_1
    iget-object v1, p0, Lpwp;->d:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 60
    const/4 v1, 0x3

    iget-object v2, p0, Lpwp;->d:Ljava/lang/Float;

    .line 61
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 63
    :cond_2
    iget-object v1, p0, Lpwp;->e:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 64
    const/4 v1, 0x4

    iget-object v2, p0, Lpwp;->e:Ljava/lang/Float;

    .line 65
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 67
    :cond_3
    iget-object v1, p0, Lpwp;->f:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 68
    const/4 v1, 0x5

    iget-object v2, p0, Lpwp;->f:Ljava/lang/Float;

    .line 69
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 71
    :cond_4
    iget-object v1, p0, Lpwp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    iput v0, p0, Lpwp;->ai:I

    .line 73
    return v0
.end method

.method public a(Loxn;)Lpwp;
    .locals 2

    .prologue
    .line 81
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 82
    sparse-switch v0, :sswitch_data_0

    .line 86
    iget-object v1, p0, Lpwp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpwp;->ah:Ljava/util/List;

    .line 90
    :cond_1
    iget-object v1, p0, Lpwp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    :sswitch_0
    return-object p0

    .line 97
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpwp;->b:Ljava/lang/Float;

    goto :goto_0

    .line 101
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpwp;->c:Ljava/lang/Float;

    goto :goto_0

    .line 105
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpwp;->d:Ljava/lang/Float;

    goto :goto_0

    .line 109
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpwp;->e:Ljava/lang/Float;

    goto :goto_0

    .line 113
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpwp;->f:Ljava/lang/Float;

    goto :goto_0

    .line 82
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lpwp;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x1

    iget-object v1, p0, Lpwp;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 32
    :cond_0
    iget-object v0, p0, Lpwp;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 33
    const/4 v0, 0x2

    iget-object v1, p0, Lpwp;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 35
    :cond_1
    iget-object v0, p0, Lpwp;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 36
    const/4 v0, 0x3

    iget-object v1, p0, Lpwp;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 38
    :cond_2
    iget-object v0, p0, Lpwp;->e:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 39
    const/4 v0, 0x4

    iget-object v1, p0, Lpwp;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 41
    :cond_3
    iget-object v0, p0, Lpwp;->f:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 42
    const/4 v0, 0x5

    iget-object v1, p0, Lpwp;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 44
    :cond_4
    iget-object v0, p0, Lpwp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 46
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpwp;->a(Loxn;)Lpwp;

    move-result-object v0

    return-object v0
.end method
