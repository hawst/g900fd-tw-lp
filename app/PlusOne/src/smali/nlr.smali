.class public final Lnlr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnig;

.field public b:Lnfq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 642
    invoke-direct {p0}, Loxq;-><init>()V

    .line 645
    iput-object v0, p0, Lnlr;->a:Lnig;

    .line 648
    iput-object v0, p0, Lnlr;->b:Lnfq;

    .line 642
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 665
    const/4 v0, 0x0

    .line 666
    iget-object v1, p0, Lnlr;->a:Lnig;

    if-eqz v1, :cond_0

    .line 667
    const/4 v0, 0x1

    iget-object v1, p0, Lnlr;->a:Lnig;

    .line 668
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 670
    :cond_0
    iget-object v1, p0, Lnlr;->b:Lnfq;

    if-eqz v1, :cond_1

    .line 671
    const/4 v1, 0x2

    iget-object v2, p0, Lnlr;->b:Lnfq;

    .line 672
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 674
    :cond_1
    iget-object v1, p0, Lnlr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 675
    iput v0, p0, Lnlr;->ai:I

    .line 676
    return v0
.end method

.method public a(Loxn;)Lnlr;
    .locals 2

    .prologue
    .line 684
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 685
    sparse-switch v0, :sswitch_data_0

    .line 689
    iget-object v1, p0, Lnlr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 690
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnlr;->ah:Ljava/util/List;

    .line 693
    :cond_1
    iget-object v1, p0, Lnlr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 695
    :sswitch_0
    return-object p0

    .line 700
    :sswitch_1
    iget-object v0, p0, Lnlr;->a:Lnig;

    if-nez v0, :cond_2

    .line 701
    new-instance v0, Lnig;

    invoke-direct {v0}, Lnig;-><init>()V

    iput-object v0, p0, Lnlr;->a:Lnig;

    .line 703
    :cond_2
    iget-object v0, p0, Lnlr;->a:Lnig;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 707
    :sswitch_2
    iget-object v0, p0, Lnlr;->b:Lnfq;

    if-nez v0, :cond_3

    .line 708
    new-instance v0, Lnfq;

    invoke-direct {v0}, Lnfq;-><init>()V

    iput-object v0, p0, Lnlr;->b:Lnfq;

    .line 710
    :cond_3
    iget-object v0, p0, Lnlr;->b:Lnfq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 685
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 653
    iget-object v0, p0, Lnlr;->a:Lnig;

    if-eqz v0, :cond_0

    .line 654
    const/4 v0, 0x1

    iget-object v1, p0, Lnlr;->a:Lnig;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 656
    :cond_0
    iget-object v0, p0, Lnlr;->b:Lnfq;

    if-eqz v0, :cond_1

    .line 657
    const/4 v0, 0x2

    iget-object v1, p0, Lnlr;->b:Lnfq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 659
    :cond_1
    iget-object v0, p0, Lnlr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 661
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 638
    invoke-virtual {p0, p1}, Lnlr;->a(Loxn;)Lnlr;

    move-result-object v0

    return-object v0
.end method
