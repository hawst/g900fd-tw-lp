.class final Lfae;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 82
    iput-object p1, p0, Lfae;->a:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lfae;->b:Ljava/util/ArrayList;

    .line 84
    return-void
.end method


# virtual methods
.method public a(I)Ldui;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lfae;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldui;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfae;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lfae;->a(I)Ldui;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 98
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lfae;->a(I)Ldui;

    move-result-object v1

    .line 104
    if-nez p2, :cond_0

    .line 105
    iget-object v0, p0, Lfae;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040105

    const/4 v3, 0x0

    .line 106
    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 108
    :cond_0
    invoke-interface {v1}, Ldui;->c()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setId(I)V

    .line 109
    const v0, 0x7f100364

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 110
    invoke-interface {v1}, Ldui;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 111
    iget-object v2, p0, Lfae;->a:Landroid/content/Context;

    .line 112
    invoke-interface {v1}, Ldui;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljac;->a:Ljac;

    .line 111
    invoke-static {v2, v3, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 118
    :goto_0
    const v0, 0x7f100365

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 119
    iget-object v2, p0, Lfae;->a:Landroid/content/Context;

    invoke-interface {v1, v2}, Ldui;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    return-object p2

    .line 114
    :cond_1
    invoke-interface {v1}, Ldui;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(I)V

    .line 115
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Z)V

    goto :goto_0
.end method
