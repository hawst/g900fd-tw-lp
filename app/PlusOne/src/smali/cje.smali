.class public abstract Lcje;
.super Llol;
.source "PG"

# interfaces
.implements Lhmm;


# instance fields
.field public N:Lcjk;

.field private O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

.field private P:Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;

.field private Q:Landroid/view/View;

.field private R:Z

.field private S:Lcmi;

.field private T:Landroid/view/View;

.field private U:Landroid/view/View;

.field private V:Landroid/view/View;

.field private W:Landroid/view/View;

.field private X:Landroid/graphics/drawable/TransitionDrawable;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0}, Llol;-><init>()V

    .line 80
    new-instance v0, Lhmf;

    iget-object v1, p0, Lcje;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 789
    return-void
.end method

.method private X()V
    .locals 2

    .prologue
    .line 700
    iget-object v0, p0, Lcje;->S:Lcmi;

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcje;->S:Lcmi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcmi;->a(Z)V

    .line 702
    const/4 v0, 0x0

    iput-object v0, p0, Lcje;->S:Lcmi;

    .line 704
    :cond_0
    return-void
.end method

.method static synthetic a(Lcje;)Llnl;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcje;->at:Llnl;

    return-object v0
.end method

.method static synthetic b(Lcje;)Llnl;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcje;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Lcje;)Llnl;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcje;->at:Llnl;

    return-object v0
.end method

.method static synthetic d(Lcje;)Llnl;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcje;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Lcje;)Llnl;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcje;->at:Llnl;

    return-object v0
.end method

.method static synthetic f(Lcje;)Llnl;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcje;->at:Llnl;

    return-object v0
.end method

.method static synthetic g(Lcje;)Llnl;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcje;->at:Llnl;

    return-object v0
.end method

.method static synthetic h(Lcje;)Llnl;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcje;->at:Llnl;

    return-object v0
.end method

.method static synthetic i(Lcje;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    return-object v0
.end method

.method private k(Z)V
    .locals 2

    .prologue
    .line 645
    iget-boolean v0, p0, Lcje;->R:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0, p1}, Lcje;->a(ZZ)I

    move-result v0

    .line 647
    iget-object v1, p0, Lcje;->V:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 648
    iget-boolean v1, p0, Lcje;->R:Z

    if-eqz v1, :cond_1

    .line 649
    iget-object v1, p0, Lcje;->X:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    .line 653
    :goto_1
    return-void

    .line 645
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 651
    :cond_1
    iget-object v1, p0, Lcje;->X:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_1
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 234
    invoke-super {p0}, Llol;->A()V

    .line 236
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v0

    check-cast v0, Lcky;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcky;->a(Lcmn;)V

    .line 237
    return-void
.end method

.method protected B_()Z
    .locals 1

    .prologue
    .line 480
    const/4 v0, 0x1

    return v0
.end method

.method protected U()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0}, Lcje;->c()I

    move-result v0

    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    return-object v0
.end method

.method protected V()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 733
    invoke-virtual {p0}, Lcje;->af()Lhbj;

    move-result-object v0

    .line 734
    if-nez v0, :cond_0

    .line 735
    iget-object v0, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-virtual {p0, v0, v4}, Lcje;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V

    .line 743
    :goto_0
    return-void

    .line 739
    :cond_0
    invoke-virtual {p0, v1, v1}, Lcje;->a(ZZ)I

    .line 742
    invoke-virtual {p0}, Lcje;->W()Lhbi;

    move-result-object v1

    iget-object v2, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    new-instance v3, Lcjl;

    invoke-direct {v3, p0}, Lcjl;-><init>(Lcje;)V

    .line 741
    invoke-interface {v0, v1, v4, v2, v3}, Lhbj;->a(Lhbi;Landroid/graphics/Rect;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Lhbm;)Z

    goto :goto_0
.end method

.method protected W()Lhbi;
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(ZZ)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 656
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v1

    if-nez v1, :cond_0

    .line 696
    :goto_0
    return v0

    .line 660
    :cond_0
    if-eqz p2, :cond_2

    .line 661
    invoke-virtual {p0}, Lcje;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    move v1, v0

    .line 663
    :goto_1
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v3

    .line 666
    if-eqz p1, :cond_3

    .line 667
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v0

    check-cast v0, Lcky;

    sget-object v2, Lcmn;->a:Lcmn;

    invoke-interface {v0, v2}, Lcky;->a(Lcmn;)V

    .line 669
    invoke-static {v3}, Lhdn;->a(Landroid/app/Activity;)V

    .line 671
    const v0, 0x7f050018

    invoke-static {v3, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 672
    const v0, 0x7f050019

    invoke-static {v3, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 682
    :goto_2
    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 683
    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 685
    iget-object v3, p0, Lcje;->T:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 686
    iget-object v3, p0, Lcje;->W:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 687
    iget-object v3, p0, Lcje;->V:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 688
    iget-object v3, p0, Lcje;->U:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 690
    iget-object v2, p0, Lcje;->Q:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 692
    iget-object v2, p0, Lcje;->Q:Landroid/view/View;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 693
    iget-object v2, p0, Lcje;->Q:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    move v0, v1

    .line 696
    goto :goto_0

    :cond_2
    move v1, v0

    .line 661
    goto :goto_1

    .line 674
    :cond_3
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v0

    check-cast v0, Lcky;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcky;->a(Lcmn;)V

    .line 676
    invoke-static {v3}, Lhdn;->b(Landroid/app/Activity;)V

    .line 678
    const v0, 0x7f050017

    invoke-static {v3, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 679
    const v0, 0x7f05001a

    invoke-static {v3, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_2
.end method

.method public a(ILjava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 464
    invoke-virtual {p0, p1, p2}, Lcje;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 96
    if-eqz p1, :cond_0

    .line 97
    const-string v0, "filter_parameter"

    .line 98
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lham;->a(Ljava/lang/String;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iput-object v0, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 103
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcje;->e(Z)V

    .line 104
    return-void

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcje;->U()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iput-object v0, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 130
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 132
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 133
    const v1, 0x7f040036

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 135
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 138
    const v1, 0x7f10017c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcje;->T:Landroid/view/View;

    .line 139
    iget-object v1, p0, Lcje;->T:Landroid/view/View;

    new-instance v2, Lcjf;

    invoke-direct {v2, p0}, Lcjf;-><init>(Lcje;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    const v1, 0x7f100178

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcje;->W:Landroid/view/View;

    .line 152
    iget-object v1, p0, Lcje;->W:Landroid/view/View;

    new-instance v2, Lcjg;

    invoke-direct {v2, p0}, Lcjg;-><init>(Lcje;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    const v1, 0x7f10017a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcje;->V:Landroid/view/View;

    .line 165
    invoke-virtual {p0}, Lcje;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcje;->V:Landroid/view/View;

    new-instance v2, Lcjh;

    invoke-direct {v2, p0}, Lcjh;-><init>(Lcje;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    iget-object v1, p0, Lcje;->V:Landroid/view/View;

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v1, p0, Lcje;->X:Landroid/graphics/drawable/TransitionDrawable;

    .line 177
    iget-object v1, p0, Lcje;->X:Landroid/graphics/drawable/TransitionDrawable;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 186
    :goto_0
    const v1, 0x7f100177

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcje;->U:Landroid/view/View;

    .line 187
    invoke-virtual {p0}, Lcje;->B_()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    iget-object v0, p0, Lcje;->U:Landroid/view/View;

    new-instance v1, Lcji;

    invoke-direct {v1, p0}, Lcji;-><init>(Lcje;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 223
    :goto_1
    return-void

    .line 179
    :cond_0
    iget-object v1, p0, Lcje;->V:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 181
    const v1, 0x7f100179

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 182
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 218
    :cond_1
    iget-object v1, p0, Lcje;->U:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 220
    const v1, 0x7f10017b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 221
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Lcjk;)V
    .locals 0

    .prologue
    .line 746
    iput-object p1, p0, Lcje;->N:Lcjk;

    .line 747
    return-void
.end method

.method protected declared-synchronized a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 751
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcje;->a(ZZ)I

    .line 753
    iget-object v0, p0, Lcje;->N:Lcjk;

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lcje;->N:Lcjk;

    invoke-interface {v0, p1, p2}, Lcjk;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 756
    :cond_0
    monitor-exit p0

    return-void

    .line 751
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Z)V
    .locals 0

    .prologue
    .line 627
    return-void
.end method

.method protected a(ILjava/lang/Object;Z)Z
    .locals 1

    .prologue
    .line 515
    invoke-virtual {p0}, Lcje;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v0

    .line 517
    if-eqz p3, :cond_0

    if-eqz v0, :cond_0

    .line 518
    invoke-virtual {p0}, Lcje;->ak()V

    .line 521
    :cond_0
    return v0
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 108
    invoke-super {p0}, Llol;->aO_()V

    .line 110
    invoke-virtual {p0}, Lcje;->am()V

    .line 117
    invoke-virtual {p0}, Lcje;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    iget-object v1, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    const/16 v2, 0x268

    .line 120
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v3

    .line 119
    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 121
    iget-object v1, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    const/16 v2, 0x269

    .line 122
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getImageWidth()I

    move-result v3

    .line 121
    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 123
    iget-object v1, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    const/16 v2, 0x26a

    .line 124
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getImageHeight()I

    move-result v0

    .line 123
    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 126
    :cond_0
    return-void
.end method

.method protected ad()Lhbn;
    .locals 1

    .prologue
    .line 484
    const/4 v0, 0x0

    return-object v0
.end method

.method protected ae()Lhbp;
    .locals 1

    .prologue
    .line 488
    const/4 v0, 0x0

    return-object v0
.end method

.method protected af()Lhbj;
    .locals 1

    .prologue
    .line 492
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final ah()Lchp;
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v0

    check-cast v0, Lcky;

    .line 241
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcky;->i()Lchp;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ai()Lhbi;
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x0

    return-object v0
.end method

.method protected aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    return-object v0
.end method

.method protected ak()V
    .locals 1

    .prologue
    .line 537
    invoke-virtual {p0}, Lcje;->ad()Lhbn;

    move-result-object v0

    .line 538
    if-eqz v0, :cond_0

    .line 539
    invoke-interface {v0}, Lhbn;->a()Z

    .line 541
    :cond_0
    return-void
.end method

.method public al()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 552
    iget-boolean v1, p0, Lcje;->R:Z

    if-eqz v1, :cond_0

    .line 553
    invoke-virtual {p0, v0}, Lcje;->j(Z)V

    .line 556
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected am()V
    .locals 2

    .prologue
    .line 572
    invoke-virtual {p0}, Lcje;->x()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 575
    :cond_1
    iget-object v0, p0, Lcje;->P:Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;

    if-nez v0, :cond_2

    .line 577
    invoke-virtual {p0}, Lcje;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1002d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;

    iput-object v0, p0, Lcje;->P:Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;

    .line 579
    :cond_2
    iget-object v0, p0, Lcje;->Q:Landroid/view/View;

    if-nez v0, :cond_0

    .line 580
    invoke-virtual {p0}, Lcje;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1002d5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcje;->Q:Landroid/view/View;

    goto :goto_0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 408
    sparse-switch p1, :sswitch_data_0

    .line 447
    const-string v0, "*UNKNOWN*"

    :goto_0
    return-object v0

    .line 410
    :sswitch_0
    const v0, 0x7f0a008a

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 413
    :sswitch_1
    const-string v0, "%s %d"

    new-array v1, v1, [Ljava/lang/Object;

    .line 414
    invoke-virtual {p0, p1}, Lcje;->f(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 413
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 443
    :sswitch_2
    check-cast p2, Ljava/lang/Number;

    .line 444
    const-string v0, "%s %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcje;->f(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 408
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0x8 -> :sswitch_2
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xb -> :sswitch_2
        0xc -> :sswitch_2
        0xd -> :sswitch_2
        0xe -> :sswitch_2
        0xf -> :sswitch_2
        0x10 -> :sswitch_2
        0x11 -> :sswitch_2
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_2
        0x2a -> :sswitch_0
        0x68 -> :sswitch_2
        0xdd -> :sswitch_2
        0xde -> :sswitch_2
        0xe7 -> :sswitch_2
        0xe8 -> :sswitch_2
        0xe9 -> :sswitch_2
        0x28e -> :sswitch_2
    .end sparse-switch
.end method

.method protected b(Z)V
    .locals 0

    .prologue
    .line 642
    return-void
.end method

.method public abstract c()I
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 86
    iget-object v0, p0, Lcje;->au:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 87
    return-void
.end method

.method protected c(Z)V
    .locals 0

    .prologue
    .line 544
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 716
    const/4 v0, 0x0

    return v0
.end method

.method protected e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 729
    const/4 v0, 0x0

    return-object v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 249
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 251
    const-string v0, "filter_parameter"

    iget-object v1, p0, Lcje;->O:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-static {v1}, Lham;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public f(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    sparse-switch p1, :sswitch_data_0

    .line 396
    const-string v0, "*UNKNOWN*"

    :goto_0
    return-object v0

    .line 282
    :sswitch_0
    const v0, 0x7f0a0087

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 285
    :sswitch_1
    const v0, 0x7f0a0082

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 288
    :sswitch_2
    const v0, 0x7f0a009e

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 291
    :sswitch_3
    const v0, 0x7f0a009b

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 294
    :sswitch_4
    const v0, 0x7f0a008e

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 297
    :sswitch_5
    const v0, 0x7f0a0086

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 300
    :sswitch_6
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 303
    :sswitch_7
    const v0, 0x7f0a009a

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 306
    :sswitch_8
    const v0, 0x7f0a0083

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 309
    :sswitch_9
    const v0, 0x7f0a0084

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 312
    :sswitch_a
    const v0, 0x7f0a0092

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 315
    :sswitch_b
    const v0, 0x7f0a0093

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 318
    :sswitch_c
    const v0, 0x7f0a008a

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 321
    :sswitch_d
    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 324
    :sswitch_e
    const v0, 0x7f0a008c

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 327
    :sswitch_f
    const v0, 0x7f0a0095

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 330
    :sswitch_10
    const v0, 0x7f0a007e

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 333
    :sswitch_11
    const v0, 0x7f0a00a1

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 336
    :sswitch_12
    const v0, 0x7f0a0081

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 339
    :sswitch_13
    const v0, 0x7f0a0088

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 342
    :sswitch_14
    const v0, 0x7f0a0085

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 345
    :sswitch_15
    const v0, 0x7f0a0094

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 348
    :sswitch_16
    const v0, 0x7f0a0099

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 351
    :sswitch_17
    const v0, 0x7f0a0098

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 354
    :sswitch_18
    const v0, 0x7f0a0096

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 357
    :sswitch_19
    const v0, 0x7f0a0097

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 360
    :sswitch_1a
    const v0, 0x7f0a009f

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 363
    :sswitch_1b
    const v0, 0x7f0a0091

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 366
    :sswitch_1c
    const v0, 0x7f0a00a3

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 369
    :sswitch_1d
    const v0, 0x7f0a00a2

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 372
    :sswitch_1e
    const v0, 0x7f0a008f

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 375
    :sswitch_1f
    const v0, 0x7f0a0090

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 378
    :sswitch_20
    const v0, 0x7f0a007f

    invoke-virtual {p0, v0}, Lcje;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 381
    :sswitch_21
    const-string v0, "[ALPHA]"

    goto/16 :goto_0

    .line 384
    :sswitch_22
    const-string v0, "[BETA]"

    goto/16 :goto_0

    .line 387
    :sswitch_23
    const-string v0, "[BLACKS]"

    goto/16 :goto_0

    .line 390
    :sswitch_24
    const-string v0, "[WHITES]"

    goto/16 :goto_0

    .line 393
    :sswitch_25
    const-string v0, ""

    goto/16 :goto_0

    .line 280
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_14
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_8
        0x8 -> :sswitch_9
        0x9 -> :sswitch_f
        0xa -> :sswitch_10
        0xb -> :sswitch_11
        0xc -> :sswitch_12
        0xd -> :sswitch_13
        0xe -> :sswitch_15
        0xf -> :sswitch_16
        0x10 -> :sswitch_17
        0x11 -> :sswitch_18
        0x13 -> :sswitch_19
        0x14 -> :sswitch_1a
        0x16 -> :sswitch_1d
        0x17 -> :sswitch_1c
        0x26 -> :sswitch_7
        0x28 -> :sswitch_e
        0x29 -> :sswitch_d
        0x2a -> :sswitch_c
        0x68 -> :sswitch_6
        0xdd -> :sswitch_a
        0xde -> :sswitch_b
        0xe7 -> :sswitch_1b
        0xe8 -> :sswitch_1e
        0xe9 -> :sswitch_1f
        0x28a -> :sswitch_21
        0x28b -> :sswitch_22
        0x28c -> :sswitch_23
        0x28d -> :sswitch_24
        0x28e -> :sswitch_20
        0x3e8 -> :sswitch_25
    .end sparse-switch
.end method

.method protected final g(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 472
    const-string v0, "%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final i(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 585
    iget-boolean v0, p0, Lcje;->R:Z

    if-eqz v0, :cond_1

    .line 586
    invoke-virtual {p0, p1}, Lcje;->j(Z)V

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    iget-boolean v0, p0, Lcje;->R:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcje;->P:Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcje;->k(Z)V

    new-instance v1, Lcmm;

    iget-object v0, p0, Lcje;->P:Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;

    invoke-direct {v1, v0, v4}, Lcmm;-><init>(Lcmn;Z)V

    iget-object v0, p0, Lcje;->P:Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;

    sget v2, Lcmi;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->a(Ljava/lang/Integer;)V

    sget v0, Lcmi;->a:I

    invoke-virtual {v1, v0}, Lcmm;->a(I)V

    iget-object v0, p0, Lcje;->V:Landroid/view/View;

    invoke-static {v0}, Lcmp;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcmm;->a(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-interface {v0, v1}, Lcky;->a(Lcmn;)V

    invoke-direct {p0}, Lcje;->X()V

    invoke-static {}, Lcmi;->a()Lcmi;

    move-result-object v0

    iput-object v0, p0, Lcje;->S:Lcmi;

    iget-object v0, p0, Lcje;->S:Lcmi;

    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v1

    new-instance v2, Lcjj;

    invoke-direct {v2, p0}, Lcjj;-><init>(Lcje;)V

    invoke-virtual {p0}, Lcje;->e()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcmi;->a(Landroid/app/Activity;Lcmj;Ljava/util/List;)V

    iput-boolean v4, p0, Lcje;->R:Z

    invoke-virtual {p0, p1}, Lcje;->a(Z)V

    goto :goto_0
.end method

.method protected final j(Z)V
    .locals 1

    .prologue
    .line 630
    iget-boolean v0, p0, Lcje;->R:Z

    if-nez v0, :cond_0

    .line 639
    :goto_0
    return-void

    .line 634
    :cond_0
    invoke-direct {p0, p1}, Lcje;->k(Z)V

    .line 636
    invoke-direct {p0}, Lcje;->X()V

    .line 637
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcje;->R:Z

    .line 638
    invoke-virtual {p0, p1}, Lcje;->b(Z)V

    goto :goto_0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 227
    invoke-super {p0}, Llol;->z()V

    .line 229
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcje;->j(Z)V

    .line 230
    return-void
.end method
