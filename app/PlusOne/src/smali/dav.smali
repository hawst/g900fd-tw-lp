.class public final Ldav;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lctm;


# instance fields
.field a:Lhsn;

.field b:Lljv;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:J

.field private h:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Ldav;->h:Landroid/content/Context;

    .line 31
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 35
    check-cast p1, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;

    .line 36
    if-nez p1, :cond_0

    .line 37
    iget-object v0, p0, Ldav;->h:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 38
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 39
    const v1, 0x7f040181

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;

    .line 42
    :goto_0
    iget-object v1, p0, Ldav;->c:Ljava/lang/String;

    iget-object v2, p0, Ldav;->d:Ljava/lang/String;

    iget-object v3, p0, Ldav;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    iget-object v1, p0, Ldav;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->a(Ljava/lang/String;)V

    .line 44
    iget-wide v2, p0, Ldav;->g:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->a(J)V

    .line 45
    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->requestLayout()V

    .line 46
    iget-object v1, p0, Ldav;->a:Lhsn;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->a(Lhsn;)V

    .line 47
    iget-object v1, p0, Ldav;->b:Lljv;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsView;->a(Lljv;)V

    .line 49
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public a(Lctn;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method
