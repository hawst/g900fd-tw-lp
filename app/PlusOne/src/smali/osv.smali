.class public final Losv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:I

.field private d:Lost;

.field private e:Losu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1748
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1772
    const/high16 v0, -0x80000000

    iput v0, p0, Losv;->c:I

    .line 1775
    iput-object v1, p0, Losv;->d:Lost;

    .line 1778
    iput-object v1, p0, Losv;->e:Losu;

    .line 1748
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1804
    const/4 v0, 0x0

    .line 1805
    iget-object v1, p0, Losv;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1806
    const/4 v0, 0x1

    iget-object v1, p0, Losv;->a:Ljava/lang/Integer;

    .line 1807
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1809
    :cond_0
    iget-object v1, p0, Losv;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1810
    const/4 v1, 0x2

    iget-object v2, p0, Losv;->b:Ljava/lang/Integer;

    .line 1811
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1813
    :cond_1
    iget v1, p0, Losv;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 1814
    const/4 v1, 0x3

    iget v2, p0, Losv;->c:I

    .line 1815
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1817
    :cond_2
    iget-object v1, p0, Losv;->d:Lost;

    if-eqz v1, :cond_3

    .line 1818
    const/4 v1, 0x4

    iget-object v2, p0, Losv;->d:Lost;

    .line 1819
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1821
    :cond_3
    iget-object v1, p0, Losv;->e:Losu;

    if-eqz v1, :cond_4

    .line 1822
    const/4 v1, 0x5

    iget-object v2, p0, Losv;->e:Losu;

    .line 1823
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1825
    :cond_4
    iget-object v1, p0, Losv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1826
    iput v0, p0, Losv;->ai:I

    .line 1827
    return v0
.end method

.method public a(Loxn;)Losv;
    .locals 2

    .prologue
    .line 1835
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1836
    sparse-switch v0, :sswitch_data_0

    .line 1840
    iget-object v1, p0, Losv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1841
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Losv;->ah:Ljava/util/List;

    .line 1844
    :cond_1
    iget-object v1, p0, Losv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1846
    :sswitch_0
    return-object p0

    .line 1851
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Losv;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 1855
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Losv;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1859
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1860
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-ne v0, v1, :cond_3

    .line 1874
    :cond_2
    iput v0, p0, Losv;->c:I

    goto :goto_0

    .line 1876
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Losv;->c:I

    goto :goto_0

    .line 1881
    :sswitch_4
    iget-object v0, p0, Losv;->d:Lost;

    if-nez v0, :cond_4

    .line 1882
    new-instance v0, Lost;

    invoke-direct {v0}, Lost;-><init>()V

    iput-object v0, p0, Losv;->d:Lost;

    .line 1884
    :cond_4
    iget-object v0, p0, Losv;->d:Lost;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1888
    :sswitch_5
    iget-object v0, p0, Losv;->e:Losu;

    if-nez v0, :cond_5

    .line 1889
    new-instance v0, Losu;

    invoke-direct {v0}, Losu;-><init>()V

    iput-object v0, p0, Losv;->e:Losu;

    .line 1891
    :cond_5
    iget-object v0, p0, Losv;->e:Losu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1836
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1783
    iget-object v0, p0, Losv;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1784
    const/4 v0, 0x1

    iget-object v1, p0, Losv;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1786
    :cond_0
    iget-object v0, p0, Losv;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1787
    const/4 v0, 0x2

    iget-object v1, p0, Losv;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1789
    :cond_1
    iget v0, p0, Losv;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 1790
    const/4 v0, 0x3

    iget v1, p0, Losv;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1792
    :cond_2
    iget-object v0, p0, Losv;->d:Lost;

    if-eqz v0, :cond_3

    .line 1793
    const/4 v0, 0x4

    iget-object v1, p0, Losv;->d:Lost;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1795
    :cond_3
    iget-object v0, p0, Losv;->e:Losu;

    if-eqz v0, :cond_4

    .line 1796
    const/4 v0, 0x5

    iget-object v1, p0, Losv;->e:Losu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1798
    :cond_4
    iget-object v0, p0, Losv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1800
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1744
    invoke-virtual {p0, p1}, Losv;->a(Loxn;)Losv;

    move-result-object v0

    return-object v0
.end method
