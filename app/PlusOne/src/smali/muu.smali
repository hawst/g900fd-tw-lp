.class public final Lmuu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmuu;


# instance fields
.field private b:I

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Integer;

.field private g:[Lmug;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13436
    const/4 v0, 0x0

    new-array v0, v0, [Lmuu;

    sput-object v0, Lmuu;->a:[Lmuu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13437
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13440
    const/high16 v0, -0x80000000

    iput v0, p0, Lmuu;->b:I

    .line 13451
    sget-object v0, Lmug;->a:[Lmug;

    iput-object v0, p0, Lmuu;->g:[Lmug;

    .line 13437
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 13494
    .line 13495
    iget v0, p0, Lmuu;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_8

    .line 13496
    const/4 v0, 0x1

    iget v2, p0, Lmuu;->b:I

    .line 13497
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 13499
    :goto_0
    iget-object v2, p0, Lmuu;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 13500
    const/4 v2, 0x2

    iget-object v3, p0, Lmuu;->c:Ljava/lang/Boolean;

    .line 13501
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 13503
    :cond_0
    iget-object v2, p0, Lmuu;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 13504
    const/4 v2, 0x3

    iget-object v3, p0, Lmuu;->d:Ljava/lang/Boolean;

    .line 13505
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 13507
    :cond_1
    iget-object v2, p0, Lmuu;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 13508
    const/4 v2, 0x4

    iget-object v3, p0, Lmuu;->e:Ljava/lang/Boolean;

    .line 13509
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 13511
    :cond_2
    iget-object v2, p0, Lmuu;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 13512
    const/4 v2, 0x5

    iget-object v3, p0, Lmuu;->f:Ljava/lang/Integer;

    .line 13513
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 13515
    :cond_3
    iget-object v2, p0, Lmuu;->g:[Lmug;

    if-eqz v2, :cond_5

    .line 13516
    iget-object v2, p0, Lmuu;->g:[Lmug;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 13517
    if-eqz v4, :cond_4

    .line 13518
    const/4 v5, 0x6

    .line 13519
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 13516
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 13523
    :cond_5
    iget-object v1, p0, Lmuu;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 13524
    const/4 v1, 0x7

    iget-object v2, p0, Lmuu;->h:Ljava/lang/String;

    .line 13525
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13527
    :cond_6
    iget-object v1, p0, Lmuu;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 13528
    const/16 v1, 0x8

    iget-object v2, p0, Lmuu;->i:Ljava/lang/Integer;

    .line 13529
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13531
    :cond_7
    iget-object v1, p0, Lmuu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13532
    iput v0, p0, Lmuu;->ai:I

    .line 13533
    return v0

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmuu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13541
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 13542
    sparse-switch v0, :sswitch_data_0

    .line 13546
    iget-object v2, p0, Lmuu;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 13547
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmuu;->ah:Ljava/util/List;

    .line 13550
    :cond_1
    iget-object v2, p0, Lmuu;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13552
    :sswitch_0
    return-object p0

    .line 13557
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 13558
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd

    if-eq v0, v2, :cond_2

    const/16 v2, 0xe

    if-eq v0, v2, :cond_2

    const/16 v2, 0x50

    if-eq v0, v2, :cond_2

    const/16 v2, 0x51

    if-eq v0, v2, :cond_2

    const/16 v2, 0x64

    if-eq v0, v2, :cond_2

    const/16 v2, 0x65

    if-eq v0, v2, :cond_2

    const/16 v2, 0x66

    if-eq v0, v2, :cond_2

    const/16 v2, 0x67

    if-eq v0, v2, :cond_2

    const/16 v2, 0x68

    if-eq v0, v2, :cond_2

    const/16 v2, 0x77

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa0

    if-eq v0, v2, :cond_2

    const/16 v2, 0x69

    if-eq v0, v2, :cond_2

    const/16 v2, 0x81

    if-eq v0, v2, :cond_2

    const/16 v2, 0x6a

    if-eq v0, v2, :cond_2

    const/16 v2, 0x6b

    if-eq v0, v2, :cond_2

    const/16 v2, 0x6c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x6d

    if-eq v0, v2, :cond_2

    const/16 v2, 0x6e

    if-eq v0, v2, :cond_2

    const/16 v2, 0x6f

    if-eq v0, v2, :cond_2

    const/16 v2, 0x70

    if-eq v0, v2, :cond_2

    const/16 v2, 0x71

    if-eq v0, v2, :cond_2

    const/16 v2, 0x72

    if-eq v0, v2, :cond_2

    const/16 v2, 0x73

    if-eq v0, v2, :cond_2

    const/16 v2, 0x74

    if-eq v0, v2, :cond_2

    const/16 v2, 0x76

    if-eq v0, v2, :cond_2

    const/16 v2, 0x78

    if-eq v0, v2, :cond_2

    const/16 v2, 0x79

    if-eq v0, v2, :cond_2

    const/16 v2, 0x7a

    if-eq v0, v2, :cond_2

    const/16 v2, 0x7b

    if-eq v0, v2, :cond_2

    const/16 v2, 0x7c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x7d

    if-eq v0, v2, :cond_2

    const/16 v2, 0x7e

    if-eq v0, v2, :cond_2

    const/16 v2, 0x7f

    if-eq v0, v2, :cond_2

    const/16 v2, 0x80

    if-eq v0, v2, :cond_2

    const/16 v2, 0x82

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x83

    if-eq v0, v2, :cond_2

    const/16 v2, 0x84

    if-eq v0, v2, :cond_2

    const/16 v2, 0x85

    if-eq v0, v2, :cond_2

    const/16 v2, 0x86

    if-eq v0, v2, :cond_2

    const/16 v2, 0x87

    if-eq v0, v2, :cond_2

    const/16 v2, 0x88

    if-eq v0, v2, :cond_2

    const/16 v2, 0x89

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8a

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8b

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8d

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8e

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8f

    if-eq v0, v2, :cond_2

    const/16 v2, 0x90

    if-eq v0, v2, :cond_2

    const/16 v2, 0x91

    if-eq v0, v2, :cond_2

    const/16 v2, 0x92

    if-eq v0, v2, :cond_2

    const/16 v2, 0x93

    if-eq v0, v2, :cond_2

    const/16 v2, 0x94

    if-eq v0, v2, :cond_2

    const/16 v2, 0x95

    if-eq v0, v2, :cond_2

    const/16 v2, 0x96

    if-eq v0, v2, :cond_2

    const/16 v2, 0x97

    if-eq v0, v2, :cond_2

    const/16 v2, 0x98

    if-eq v0, v2, :cond_2

    const/16 v2, 0x99

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9a

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9b

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9d

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9e

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9f

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa1

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa2

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa3

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa4

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa5

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa6

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa7

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xaa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xab

    if-eq v0, v2, :cond_2

    const/16 v2, 0xac

    if-eq v0, v2, :cond_2

    const/16 v2, 0xad

    if-eq v0, v2, :cond_2

    const/16 v2, 0xae

    if-eq v0, v2, :cond_2

    const/16 v2, 0xaf

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb0

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb1

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb2

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb3

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb4

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb5

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb6

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb7

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc8

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xca

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcd

    if-eq v0, v2, :cond_2

    const/16 v2, 0xce

    if-eq v0, v2, :cond_2

    const/16 v2, 0xcf

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd0

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd1

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd2

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd3

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd4

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd5

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd6

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x12c

    if-eq v0, v2, :cond_2

    const/16 v2, 0x12d

    if-eq v0, v2, :cond_2

    const/16 v2, 0x75

    if-eq v0, v2, :cond_2

    const/16 v2, 0x190

    if-eq v0, v2, :cond_2

    const/16 v2, 0x191

    if-eq v0, v2, :cond_2

    const/16 v2, 0x192

    if-eq v0, v2, :cond_2

    const/16 v2, 0x193

    if-eq v0, v2, :cond_2

    const/16 v2, 0x194

    if-eq v0, v2, :cond_2

    const/16 v2, 0x195

    if-eq v0, v2, :cond_2

    const/16 v2, 0x196

    if-eq v0, v2, :cond_2

    const/16 v2, 0x199

    if-eq v0, v2, :cond_2

    const/16 v2, 0x197

    if-eq v0, v2, :cond_2

    const/16 v2, 0x198

    if-eq v0, v2, :cond_2

    const/16 v2, 0x3e8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x3e9

    if-eq v0, v2, :cond_2

    const/16 v2, 0x3ea

    if-eq v0, v2, :cond_2

    const/16 v2, 0x3eb

    if-eq v0, v2, :cond_2

    const/16 v2, 0x3ec

    if-eq v0, v2, :cond_2

    const/16 v2, 0x3ed

    if-eq v0, v2, :cond_2

    const/16 v2, 0x3ee

    if-ne v0, v2, :cond_3

    .line 13694
    :cond_2
    iput v0, p0, Lmuu;->b:I

    goto/16 :goto_0

    .line 13696
    :cond_3
    iput v1, p0, Lmuu;->b:I

    goto/16 :goto_0

    .line 13701
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmuu;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 13705
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmuu;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 13709
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmuu;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 13713
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmuu;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 13717
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 13718
    iget-object v0, p0, Lmuu;->g:[Lmug;

    if-nez v0, :cond_5

    move v0, v1

    .line 13719
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmug;

    .line 13720
    iget-object v3, p0, Lmuu;->g:[Lmug;

    if-eqz v3, :cond_4

    .line 13721
    iget-object v3, p0, Lmuu;->g:[Lmug;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 13723
    :cond_4
    iput-object v2, p0, Lmuu;->g:[Lmug;

    .line 13724
    :goto_2
    iget-object v2, p0, Lmuu;->g:[Lmug;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 13725
    iget-object v2, p0, Lmuu;->g:[Lmug;

    new-instance v3, Lmug;

    invoke-direct {v3}, Lmug;-><init>()V

    aput-object v3, v2, v0

    .line 13726
    iget-object v2, p0, Lmuu;->g:[Lmug;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 13727
    invoke-virtual {p1}, Loxn;->a()I

    .line 13724
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 13718
    :cond_5
    iget-object v0, p0, Lmuu;->g:[Lmug;

    array-length v0, v0

    goto :goto_1

    .line 13730
    :cond_6
    iget-object v2, p0, Lmuu;->g:[Lmug;

    new-instance v3, Lmug;

    invoke-direct {v3}, Lmug;-><init>()V

    aput-object v3, v2, v0

    .line 13731
    iget-object v2, p0, Lmuu;->g:[Lmug;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 13735
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmuu;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 13739
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmuu;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 13542
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 13460
    iget v0, p0, Lmuu;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 13461
    const/4 v0, 0x1

    iget v1, p0, Lmuu;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 13463
    :cond_0
    iget-object v0, p0, Lmuu;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 13464
    const/4 v0, 0x2

    iget-object v1, p0, Lmuu;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 13466
    :cond_1
    iget-object v0, p0, Lmuu;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 13467
    const/4 v0, 0x3

    iget-object v1, p0, Lmuu;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 13469
    :cond_2
    iget-object v0, p0, Lmuu;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 13470
    const/4 v0, 0x4

    iget-object v1, p0, Lmuu;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 13472
    :cond_3
    iget-object v0, p0, Lmuu;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 13473
    const/4 v0, 0x5

    iget-object v1, p0, Lmuu;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 13475
    :cond_4
    iget-object v0, p0, Lmuu;->g:[Lmug;

    if-eqz v0, :cond_6

    .line 13476
    iget-object v1, p0, Lmuu;->g:[Lmug;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 13477
    if-eqz v3, :cond_5

    .line 13478
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 13476
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13482
    :cond_6
    iget-object v0, p0, Lmuu;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 13483
    const/4 v0, 0x7

    iget-object v1, p0, Lmuu;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 13485
    :cond_7
    iget-object v0, p0, Lmuu;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 13486
    const/16 v0, 0x8

    iget-object v1, p0, Lmuu;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 13488
    :cond_8
    iget-object v0, p0, Lmuu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 13490
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 13433
    invoke-virtual {p0, p1}, Lmuu;->a(Loxn;)Lmuu;

    move-result-object v0

    return-object v0
.end method
