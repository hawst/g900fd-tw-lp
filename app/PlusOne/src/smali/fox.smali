.class final Lfox;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lfos;


# direct methods
.method constructor <init>(Lfos;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lfox;->a:Lfos;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413
    new-instance v0, Lfpc;

    iget-object v1, p0, Lfox;->a:Lfos;

    invoke-static {v1}, Lfos;->y(Lfos;)Llnl;

    move-result-object v1

    iget-object v2, p0, Lfox;->a:Lfos;

    invoke-static {v2}, Lfos;->c(Lfos;)Lhee;

    move-result-object v2

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lfox;->a:Lfos;

    .line 414
    invoke-static {v3}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v3

    iget-object v3, v3, Lmmq;->g:Lmlz;

    iget-object v3, v3, Lmlz;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lfpc;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const v10, 0x7f0a01d2

    const v9, 0x7f0a01c5

    const/16 v2, 0xff

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 422
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_6

    .line 423
    :cond_0
    iget-object v0, p0, Lfox;->a:Lfos;

    invoke-static {v0}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfox;->a:Lfos;

    invoke-static {v0}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v0

    iget-object v0, v0, Lmmq;->g:Lmlz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfox;->a:Lfos;

    invoke-static {v0}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v0

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    if-eqz v0, :cond_1

    .line 425
    iget-object v0, p0, Lfox;->a:Lfos;

    invoke-static {v0}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v0

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    iget-object v0, v0, Logr;->K:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 426
    iget-object v0, p0, Lfox;->a:Lfos;

    invoke-virtual {v0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100227

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 427
    if-nez v4, :cond_2

    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 428
    iget-object v1, p0, Lfox;->a:Lfos;

    const v5, 0x7f0a01d5

    new-array v6, v8, [Ljava/lang/Object;

    .line 429
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-virtual {v1, v5, v6}, Lfos;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 428
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 431
    iget-object v0, p0, Lfox;->a:Lfos;

    invoke-static {v0}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v0

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    iget-object v0, v0, Logr;->C:Loae;

    if-eqz v0, :cond_1

    .line 432
    iget-object v0, p0, Lfox;->a:Lfos;

    iget-object v1, p0, Lfox;->a:Lfos;

    .line 433
    invoke-static {v1}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v1

    iget-object v1, v1, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    iget-object v1, v1, Logr;->C:Loae;

    iget-object v1, v1, Loae;->c:Ljava/lang/Boolean;

    .line 432
    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    invoke-static {v0, v1}, Lfos;->e(Lfos;Z)Z

    .line 434
    iget-object v0, p0, Lfox;->a:Lfos;

    .line 435
    invoke-static {v0}, Lfos;->m(Lfos;)Lmmq;

    move-result-object v0

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    iget-object v0, v0, Logr;->C:Loae;

    iget-object v0, v0, Loae;->e:Ljava/lang/Integer;

    .line 434
    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v4

    .line 436
    iget-object v0, p0, Lfox;->a:Lfos;

    invoke-virtual {v0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1004a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 437
    invoke-virtual {v0}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget-object v1, p0, Lfox;->a:Lfos;

    invoke-static {v1}, Lfos;->z(Lfos;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v5, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 438
    iget-object v1, p0, Lfox;->a:Lfos;

    new-array v2, v8, [Ljava/lang/Object;

    .line 439
    invoke-static {v8, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    .line 438
    invoke-virtual {v1, v9, v2}, Lfos;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    iget-object v1, p0, Lfox;->a:Lfos;

    new-array v2, v8, [Ljava/lang/Object;

    .line 441
    invoke-static {v8, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 440
    invoke-virtual {v1, v10, v2}, Lfos;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lfox;->a:Lfos;

    .line 442
    invoke-static {v1}, Lfos;->z(Lfos;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lfox;->a:Lfos;

    const v3, 0x7f0a01d3

    invoke-virtual {v1, v3}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 443
    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 440
    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 468
    :cond_1
    :goto_4
    return-void

    .line 427
    :cond_2
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_3
    move v1, v3

    .line 437
    goto :goto_1

    .line 442
    :cond_4
    iget-object v1, p0, Lfox;->a:Lfos;

    const v3, 0x7f0a01d4

    .line 443
    invoke-virtual {v1, v3}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 448
    :cond_6
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 449
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 450
    const/4 v0, 0x2

    .line 451
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 450
    invoke-static {v0}, Llah;->a([B)Llah;

    move-result-object v0

    .line 452
    invoke-virtual {v0}, Llah;->b()I

    move-result v1

    .line 454
    iget-object v5, p0, Lfox;->a:Lfos;

    invoke-virtual {v0}, Llah;->c()Z

    move-result v0

    invoke-static {v5, v0}, Lfos;->e(Lfos;Z)Z

    .line 455
    iget-object v0, p0, Lfox;->a:Lfos;

    invoke-virtual {v0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v5, 0x7f1004a1

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 456
    iget-object v5, p0, Lfox;->a:Lfos;

    new-array v6, v8, [Ljava/lang/Object;

    .line 457
    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    .line 456
    invoke-virtual {v5, v9, v6}, Lfos;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 458
    invoke-virtual {v0}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget-object v6, p0, Lfox;->a:Lfos;

    invoke-static {v6}, Lfos;->z(Lfos;)Z

    move-result v6

    if-eqz v6, :cond_7

    :goto_5
    invoke-virtual {v5, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 459
    iget-object v2, p0, Lfox;->a:Lfos;

    new-array v5, v8, [Ljava/lang/Object;

    .line 460
    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v3

    .line 459
    invoke-virtual {v2, v10, v5}, Lfos;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lfox;->a:Lfos;

    .line 461
    invoke-static {v1}, Lfos;->z(Lfos;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lfox;->a:Lfos;

    const v5, 0x7f0a01d3

    invoke-virtual {v1, v5}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 462
    :goto_6
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 459
    :goto_7
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 464
    iget-object v0, p0, Lfox;->a:Lfos;

    invoke-virtual {v0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100227

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 465
    if-nez v4, :cond_a

    const-string v1, ""

    :goto_8
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 466
    iget-object v1, p0, Lfox;->a:Lfos;

    const v2, 0x7f0a01d5

    new-array v5, v8, [Ljava/lang/Object;

    .line 467
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v3

    invoke-virtual {v1, v2, v5}, Lfos;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 466
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_7
    move v2, v3

    .line 458
    goto :goto_5

    .line 461
    :cond_8
    iget-object v1, p0, Lfox;->a:Lfos;

    const v5, 0x7f0a01d4

    .line 462
    invoke-virtual {v1, v5}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    :cond_9
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_7

    .line 465
    :cond_a
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_8
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 472
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 409
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lfox;->a(Landroid/database/Cursor;)V

    return-void
.end method
