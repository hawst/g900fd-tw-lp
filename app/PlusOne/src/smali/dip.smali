.class public final Ldip;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmhu;",
        "Lmhv;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Z

.field private final r:Lkzl;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 47
    const-string v3, "collectionread"

    new-instance v4, Lmhu;

    invoke-direct {v4}, Lmhu;-><init>()V

    new-instance v5, Lmhv;

    invoke-direct {v5}, Lmhv;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 50
    iget-object v0, p0, Ldip;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Ldip;->r:Lkzl;

    .line 51
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CollectionReadOperation: clusterId is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    iput-object p3, p0, Ldip;->a:Ljava/lang/String;

    .line 55
    iput-object p4, p0, Ldip;->b:Ljava/lang/String;

    .line 56
    iput-object p5, p0, Ldip;->p:Ljava/lang/String;

    .line 57
    iput-boolean p6, p0, Ldip;->q:Z

    .line 58
    return-void
.end method

.method private static a(Landroid/content/Context;I[Logr;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 147
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    .line 148
    :cond_0
    const/4 v0, 0x0

    .line 161
    :goto_0
    return-object v0

    .line 151
    :cond_1
    new-array v0, v1, [Logr;

    aget-object v1, p2, v4

    aput-object v1, v0, v4

    .line 153
    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, p1, v0, v1, v2}, Llap;->b(Landroid/content/Context;I[Logr;IZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_2
    :goto_1
    aget-object v0, v0, v4

    iget-object v0, v0, Logr;->i:Ljava/lang/String;

    goto :goto_0

    .line 155
    :catch_0
    move-exception v1

    .line 156
    const-string v2, "HttpOperation"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 157
    const-string v2, "HttpOperation"

    const-string v3, "Error inserting album activity"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 84
    const/16 v0, 0x194

    if-ne p1, v0, :cond_0

    .line 85
    iget-object v0, p0, Ldip;->f:Landroid/content/Context;

    iget v1, p0, Ldip;->c:I

    iget-object v2, p0, Ldip;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 87
    :cond_0
    return-void
.end method

.method protected a(Lmhu;)V
    .locals 6

    .prologue
    .line 62
    new-instance v0, Lmzu;

    invoke-direct {v0}, Lmzu;-><init>()V

    iput-object v0, p1, Lmhu;->a:Lmzu;

    .line 63
    iget-object v0, p1, Lmhu;->a:Lmzu;

    .line 64
    new-instance v1, Lmzt;

    invoke-direct {v1}, Lmzt;-><init>()V

    .line 65
    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lmzt;->a:Ljava/lang/Integer;

    .line 66
    const/4 v2, 0x1

    iput v2, v1, Lmzt;->b:I

    .line 68
    new-instance v2, Loxz;

    invoke-direct {v2}, Loxz;-><init>()V

    iput-object v2, v0, Lmzu;->h:Loxz;

    .line 69
    iget-object v2, v0, Lmzu;->h:Loxz;

    iget-object v3, p0, Ldip;->r:Lkzl;

    iget-object v4, p0, Ldip;->f:Landroid/content/Context;

    iget v5, p0, Ldip;->c:I

    invoke-interface {v3, v4, v5}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v3

    iput-object v3, v2, Loxz;->a:[I

    .line 71
    iget-object v2, p0, Ldip;->a:Ljava/lang/String;

    iget-object v3, p0, Ldip;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;)Lmzo;

    move-result-object v2

    iput-object v2, v0, Lmzu;->a:Lmzo;

    .line 72
    iput-object v1, v0, Lmzu;->b:Lmzt;

    .line 73
    iget-object v1, p0, Ldip;->p:Ljava/lang/String;

    iput-object v1, v0, Lmzu;->c:Ljava/lang/String;

    .line 74
    iget-boolean v1, p0, Ldip;->q:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmzu;->d:Ljava/lang/Boolean;

    .line 75
    iget-boolean v1, p0, Ldip;->q:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmzu;->e:Ljava/lang/Boolean;

    .line 76
    iget-boolean v1, p0, Ldip;->q:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmzu;->g:Ljava/lang/Boolean;

    .line 77
    iget-boolean v1, p0, Ldip;->q:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmzu;->f:Ljava/lang/Boolean;

    .line 79
    const/4 v1, 0x2

    iput v1, v0, Lmzu;->i:I

    .line 80
    return-void
.end method

.method protected a(Lmhv;)V
    .locals 8

    .prologue
    const/high16 v3, -0x80000000

    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 91
    iget-object v4, p1, Lmhv;->a:Lmzv;

    .line 100
    iget-object v0, p0, Ldip;->a:Ljava/lang/String;

    invoke-static {v0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 101
    iget-object v0, v4, Lmzv;->a:Lnxr;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lmzv;->a:Lnxr;

    iget-object v0, v0, Lnxr;->c:Ljava/lang/String;

    if-nez v0, :cond_7

    :cond_0
    iget-object v0, p0, Ldip;->a:Ljava/lang/String;

    .line 102
    invoke-static {v0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    :goto_0
    iget-object v2, p0, Ldip;->a:Ljava/lang/String;

    invoke-static {v2}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 107
    iget-object v2, v4, Lmzv;->a:Lnxr;

    if-eqz v2, :cond_1

    iget-object v2, v4, Lmzv;->a:Lnxr;

    iget-object v2, v2, Lnxr;->f:Ljava/lang/String;

    if-nez v2, :cond_8

    :cond_1
    iget-object v2, p0, Ldip;->a:Ljava/lang/String;

    .line 108
    invoke-static {v2}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 112
    :goto_1
    iget-object v6, p0, Ldip;->a:Ljava/lang/String;

    invoke-static {v6}, Ljvj;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 113
    iget-object v1, v4, Lmzv;->a:Lnxr;

    if-eqz v1, :cond_2

    iget-object v1, v4, Lmzv;->a:Lnxr;

    iget-object v1, v1, Lnxr;->b:Ljava/lang/String;

    if-nez v1, :cond_9

    :cond_2
    iget-object v1, p0, Ldip;->a:Ljava/lang/String;

    .line 114
    invoke-static {v1}, Ljvj;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 119
    :cond_3
    :goto_2
    iget-object v6, p0, Ldip;->a:Ljava/lang/String;

    invoke-static {v6}, Ljvj;->f(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_5

    .line 121
    iget-object v6, v4, Lmzv;->a:Lnxr;

    if-eqz v6, :cond_4

    iget-object v6, v4, Lmzv;->a:Lnxr;

    iget v6, v6, Lnxr;->d:I

    if-ne v6, v3, :cond_a

    :cond_4
    iget-object v3, p0, Ldip;->a:Ljava/lang/String;

    .line 122
    invoke-static {v3}, Ljvj;->f(Ljava/lang/String;)I

    move-result v3

    .line 126
    :cond_5
    :goto_3
    invoke-static {v1, v2, v0, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 127
    const/4 v1, 0x3

    new-array v2, v7, [Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 129
    iget-object v0, p0, Ldip;->f:Landroid/content/Context;

    iget v1, p0, Ldip;->c:I

    iget-object v3, v4, Lmzv;->b:Ljava/lang/String;

    iget-boolean v6, p0, Ldip;->q:Z

    invoke-static {v0, v1, v2, v3, v6}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 132
    iget-boolean v0, p0, Ldip;->q:Z

    if-eqz v0, :cond_b

    iget-object v0, v4, Lmzv;->d:Lnzx;

    if-eqz v0, :cond_b

    iget-object v0, v4, Lmzv;->c:[Lnzx;

    if-eqz v0, :cond_b

    iget-object v0, v4, Lmzv;->c:[Lnzx;

    array-length v0, v0

    if-lez v0, :cond_b

    .line 134
    iget-object v0, v4, Lmzv;->d:Lnzx;

    iget-object v1, v4, Lmzv;->c:[Lnzx;

    iput-object v1, v0, Lnzx;->j:[Lnzx;

    iget-object v3, v0, Lnzx;->f:Lnyl;

    if-nez v3, :cond_6

    aget-object v1, v1, v5

    iget-object v1, v1, Lnzx;->f:Lnyl;

    iput-object v1, v0, Lnzx;->f:Lnyl;

    .line 135
    :cond_6
    new-array v3, v7, [Lnzx;

    iget-object v0, v4, Lmzv;->d:Lnzx;

    aput-object v0, v3, v5

    .line 139
    :goto_4
    iget-object v0, p0, Ldip;->f:Landroid/content/Context;

    iget v1, p0, Ldip;->c:I

    iget-object v4, v4, Lmzv;->e:[Logr;

    invoke-static {v0, v1, v4}, Ldip;->a(Landroid/content/Context;I[Logr;)Ljava/lang/String;

    move-result-object v6

    .line 140
    iget-object v0, p0, Ldip;->f:Landroid/content/Context;

    iget v1, p0, Ldip;->c:I

    iget-boolean v4, p0, Ldip;->q:Z

    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 142
    return-void

    .line 102
    :cond_7
    iget-object v0, v4, Lmzv;->a:Lnxr;

    iget-object v0, v0, Lnxr;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 108
    :cond_8
    iget-object v2, v4, Lmzv;->a:Lnxr;

    iget-object v2, v2, Lnxr;->f:Ljava/lang/String;

    goto/16 :goto_1

    .line 114
    :cond_9
    iget-object v1, v4, Lmzv;->a:Lnxr;

    iget-object v1, v1, Lnxr;->b:Ljava/lang/String;

    goto :goto_2

    .line 122
    :cond_a
    iget-object v3, v4, Lmzv;->a:Lnxr;

    iget v3, v3, Lnxr;->d:I

    goto :goto_3

    .line 137
    :cond_b
    iget-object v3, v4, Lmzv;->c:[Lnzx;

    goto :goto_4

    :cond_c
    move-object v2, v1

    goto/16 :goto_1

    :cond_d
    move-object v0, v1

    goto/16 :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lmhu;

    invoke-virtual {p0, p1}, Ldip;->a(Lmhu;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lmhv;

    invoke-virtual {p0, p1}, Ldip;->a(Lmhv;)V

    return-void
.end method
