.class public final Lgaa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdd;


# instance fields
.field a:Z

.field b:Llez;

.field c:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Llez;",
            ">;"
        }
    .end annotation
.end field

.field d:I

.field final synthetic e:Lcom/google/android/apps/plus/views/PhotoView;

.field private f:Lcom/google/android/libraries/social/media/MediaResource;

.field private g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

.field private h:Z

.field private i:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/PhotoView;)V
    .locals 1

    .prologue
    .line 2071
    iput-object p1, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgaa;->h:Z

    .line 2079
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lgaa;->i:Landroid/graphics/Matrix;

    .line 2264
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2100
    iget-object v0, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgaa;->b:Llez;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2101
    iget-object v0, p0, Lgaa;->i:Landroid/graphics/Matrix;

    iget-object v1, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/PhotoView;->g(Lcom/google/android/apps/plus/views/PhotoView;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 2102
    iget-object v0, p0, Lgaa;->b:Llez;

    invoke-virtual {v0}, Llez;->e()I

    move-result v0

    rem-int/lit16 v0, v0, 0xb4

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2103
    :goto_0
    iget-object v1, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-float v1, v1

    .line 2104
    if-eqz v0, :cond_2

    iget-object v0, p0, Lgaa;->b:Llez;

    invoke-virtual {v0}, Llez;->c()I

    move-result v0

    int-to-float v0, v0

    .line 2106
    :goto_1
    div-float v0, v1, v0

    .line 2107
    iget-object v1, p0, Lgaa;->i:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 2108
    iget-object v0, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    iget-object v1, p0, Lgaa;->i:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a(Landroid/graphics/Matrix;)V

    .line 2110
    :cond_0
    return-void

    .line 2102
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2104
    :cond_2
    iget-object v0, p0, Lgaa;->b:Llez;

    .line 2105
    invoke-virtual {v0}, Llez;->b()I

    move-result v0

    int-to-float v0, v0

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)V
    .locals 1

    .prologue
    .line 2204
    iget-object v0, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    if-eq v0, p1, :cond_0

    .line 2205
    iput-object p1, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    .line 2206
    iget-object v0, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    if-eqz v0, :cond_1

    .line 2207
    invoke-virtual {p0}, Lgaa;->e()V

    .line 2212
    :cond_0
    :goto_0
    return-void

    .line 2209
    :cond_1
    invoke-virtual {p0}, Lgaa;->c()V

    goto :goto_0
.end method

.method public a(Lkda;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2148
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2154
    :goto_0
    return-void

    .line 2150
    :pswitch_0
    invoke-virtual {p1}, Lkda;->getCachedFile()Ljava/io/File;

    move-result-object v0

    .line 2151
    iget-object v1, p0, Lgaa;->c:Landroid/os/AsyncTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgaa;->c:Landroid/os/AsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    new-instance v1, Lgab;

    invoke-direct {v1, p0, v0}, Lgab;-><init>(Lgaa;Ljava/io/File;)V

    iput-object v1, p0, Lgaa;->c:Landroid/os/AsyncTask;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lgaa;->c:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgaa;->c:Landroid/os/AsyncTask;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 2148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 2234
    iget-boolean v0, p0, Lgaa;->h:Z

    if-eq p1, v0, :cond_0

    .line 2235
    iput-boolean p1, p0, Lgaa;->h:Z

    .line 2236
    iget-boolean v0, p0, Lgaa;->h:Z

    if-eqz v0, :cond_1

    .line 2237
    invoke-virtual {p0}, Lgaa;->e()V

    .line 2243
    :cond_0
    :goto_0
    return-void

    .line 2239
    :cond_1
    invoke-virtual {p0}, Lgaa;->f()V

    .line 2240
    iget-object v0, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 2117
    iget-object v0, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgaa;->b:Llez;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2118
    invoke-direct {p0}, Lgaa;->h()V

    .line 2119
    invoke-virtual {p0}, Lgaa;->d()Z

    move-result v0

    .line 2122
    :goto_0
    return v0

    .line 2121
    :cond_0
    invoke-virtual {p0}, Lgaa;->e()V

    .line 2122
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 6

    .prologue
    .line 2085
    invoke-static {}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgaa;->f:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_1

    .line 2097
    :cond_0
    :goto_0
    return-void

    .line 2089
    :cond_1
    iget-object v0, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/PhotoView;->e(Lcom/google/android/apps/plus/views/PhotoView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2090
    invoke-static {}, Lcom/google/android/apps/plus/views/PhotoView;->p()Lizs;

    move-result-object v0

    iget-object v1, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/PhotoView;->b:Lizu;

    const/4 v2, 0x1

    iget-object v3, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    .line 2092
    invoke-static {v3}, Lcom/google/android/apps/plus/views/PhotoView;->f(Lcom/google/android/apps/plus/views/PhotoView;)Lnzi;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    :goto_1
    const/16 v4, 0x2022

    move-object v5, p0

    .line 2090
    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;ILizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lgaa;->f:Lcom/google/android/libraries/social/media/MediaResource;

    goto :goto_0

    .line 2092
    :cond_2
    new-instance v3, Ljub;

    iget-object v4, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-static {v4}, Lcom/google/android/apps/plus/views/PhotoView;->f(Lcom/google/android/apps/plus/views/PhotoView;)Lnzi;

    move-result-object v4

    invoke-direct {v3, v4}, Ljub;-><init>(Lnzi;)V

    goto :goto_1
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2127
    invoke-virtual {p0}, Lgaa;->d()Z

    move-result v0

    .line 2128
    const/4 v1, 0x0

    iput-boolean v1, p0, Lgaa;->a:Z

    .line 2129
    iget-object v1, p0, Lgaa;->f:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v1, :cond_0

    .line 2130
    iget-object v1, p0, Lgaa;->f:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v1, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 2131
    iput-object v3, p0, Lgaa;->f:Lcom/google/android/libraries/social/media/MediaResource;

    .line 2133
    :cond_0
    iget-object v1, p0, Lgaa;->c:Landroid/os/AsyncTask;

    if-eqz v1, :cond_1

    .line 2134
    iget-object v1, p0, Lgaa;->c:Landroid/os/AsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 2135
    iput-object v3, p0, Lgaa;->c:Landroid/os/AsyncTask;

    .line 2137
    :cond_1
    iget-object v1, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    if-eqz v1, :cond_2

    .line 2138
    iget-object v1, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-virtual {v1, v3, v3}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a(Llfi;Ljava/lang/Runnable;)V

    .line 2140
    :cond_2
    iput-object v3, p0, Lgaa;->b:Llez;

    .line 2141
    if-eqz v0, :cond_3

    .line 2142
    iget-object v0, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 2144
    :cond_3
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 2200
    iget-boolean v0, p0, Lgaa;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgaa;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 2215
    invoke-virtual {p0}, Lgaa;->f()V

    .line 2216
    iget-boolean v0, p0, Lgaa;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 2231
    :cond_0
    :goto_0
    return-void

    .line 2219
    :cond_1
    iget-object v0, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    .line 2220
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljgn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    .line 2221
    iget-object v1, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoView;->m()F

    move-result v1

    iget-object v2, p0, Lgaa;->e:Lcom/google/android/apps/plus/views/PhotoView;

    iget v2, v2, Lcom/google/android/apps/plus/views/PhotoView;->d:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    invoke-interface {v0}, Ljgn;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2224
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgaa;->a:Z

    .line 2225
    iget v0, p0, Lgaa;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgaa;->d:I

    .line 2226
    iget-object v0, p0, Lgaa;->b:Llez;

    if-eqz v0, :cond_3

    .line 2227
    invoke-virtual {p0}, Lgaa;->g()V

    goto :goto_0

    .line 2229
    :cond_3
    invoke-virtual {p0}, Lgaa;->b()V

    goto :goto_0
.end method

.method f()V
    .locals 2

    .prologue
    .line 2246
    iget-object v0, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    if-eqz v0, :cond_0

    .line 2247
    iget-object v1, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    invoke-virtual {p0}, Lgaa;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->setVisibility(I)V

    .line 2249
    :cond_0
    return-void

    .line 2247
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method g()V
    .locals 4

    .prologue
    .line 2252
    iget-object v0, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgaa;->b:Llez;

    invoke-virtual {v0}, Llez;->f()Landroid/graphics/BitmapRegionDecoder;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2253
    iget-boolean v0, p0, Lgaa;->a:Z

    if-nez v0, :cond_0

    .line 2254
    iget-object v0, p0, Lgaa;->g:Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;

    iget-object v1, p0, Lgaa;->b:Llez;

    new-instance v2, Lgac;

    iget v3, p0, Lgaa;->d:I

    invoke-direct {v2, p0, v3}, Lgac;-><init>(Lgaa;I)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a(Llfi;Ljava/lang/Runnable;)V

    .line 2257
    :cond_0
    invoke-direct {p0}, Lgaa;->h()V

    .line 2259
    :cond_1
    return-void
.end method
