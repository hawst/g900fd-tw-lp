.class public final Lkuv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkve;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Lkuu;

.field private b:Landroid/content/Context;

.field private c:I

.field private d:Lktc;

.field private e:Lktb;


# direct methods
.method public constructor <init>(Llqr;Lkuu;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p2, p0, Lkuv;->a:Lkuu;

    .line 43
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 48
    iput-object p1, p0, Lkuv;->b:Landroid/content/Context;

    .line 49
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lkuv;->c:I

    .line 50
    const-class v0, Lktc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktc;

    iput-object v0, p0, Lkuv;->d:Lktc;

    .line 51
    const-class v0, Lktb;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktb;

    iput-object v0, p0, Lkuv;->e:Lktb;

    .line 52
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 63
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 64
    iget-object v0, p0, Lkuv;->b:Landroid/content/Context;

    const-class v1, Lkte;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkte;

    iget v1, p0, Lkuv;->c:I

    invoke-interface {v0, v1, p1}, Lkte;->a(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 65
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v0, v2, :cond_0

    .line 66
    iget-object v0, p0, Lkuv;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 75
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lkuv;->b:Landroid/content/Context;

    const-class v2, Lhkr;

    .line 69
    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    invoke-virtual {v0}, Lhkr;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 70
    iget-object v2, p0, Lkuv;->b:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0

    .line 73
    :cond_1
    const-string v0, "MemberListActionsMixin"

    const-string v1, "Got empty gaiaId for member."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 56
    iget-object v0, p0, Lkuv;->a:Lkuu;

    iget-object v1, p0, Lkuv;->d:Lktc;

    invoke-interface {v1}, Lktc;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lkuv;->e:Lktb;

    .line 57
    invoke-interface {v1}, Lktb;->c()I

    move-result v5

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    .line 56
    invoke-virtual/range {v0 .. v5}, Lkuu;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V

    .line 58
    return-void
.end method
