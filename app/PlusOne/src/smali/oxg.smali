.class public final Loxg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loxg;


# instance fields
.field private A:Loxk;

.field private b:Loxh;

.field private c:Loxh;

.field private d:Loxl;

.field private e:[Loxj;

.field private f:[Loxi;

.field private g:Ljava/lang/Float;

.field private h:Ljava/lang/Float;

.field private i:Ljava/lang/Float;

.field private j:Ljava/lang/Float;

.field private k:Ljava/lang/Float;

.field private l:Ljava/lang/Float;

.field private m:Ljava/lang/Float;

.field private n:Ljava/lang/Float;

.field private o:Ljava/lang/Float;

.field private p:Ljava/lang/Float;

.field private q:Ljava/lang/Float;

.field private r:Ljava/lang/Float;

.field private s:Ljava/lang/Float;

.field private t:Ljava/lang/Float;

.field private u:Ljava/lang/Float;

.field private v:Ljava/lang/Float;

.field private w:Ljava/lang/Float;

.field private x:[B

.field private y:[Loxm;

.field private z:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    new-array v0, v0, [Loxg;

    sput-object v0, Loxg;->a:[Loxg;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 114
    invoke-direct {p0}, Loxq;-><init>()V

    .line 692
    iput-object v1, p0, Loxg;->b:Loxh;

    .line 695
    iput-object v1, p0, Loxg;->c:Loxh;

    .line 698
    iput-object v1, p0, Loxg;->d:Loxl;

    .line 701
    sget-object v0, Loxj;->a:[Loxj;

    iput-object v0, p0, Loxg;->e:[Loxj;

    .line 704
    sget-object v0, Loxi;->a:[Loxi;

    iput-object v0, p0, Loxg;->f:[Loxi;

    .line 743
    sget-object v0, Loxm;->a:[Loxm;

    iput-object v0, p0, Loxg;->y:[Loxm;

    .line 748
    iput-object v1, p0, Loxg;->A:Loxk;

    .line 114
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 849
    .line 850
    iget-object v0, p0, Loxg;->b:Loxh;

    if-eqz v0, :cond_1c

    .line 851
    const/4 v0, 0x1

    iget-object v2, p0, Loxg;->b:Loxh;

    .line 852
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 854
    :goto_0
    iget-object v2, p0, Loxg;->e:[Loxj;

    if-eqz v2, :cond_1

    .line 855
    iget-object v3, p0, Loxg;->e:[Loxj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 856
    if-eqz v5, :cond_0

    .line 857
    const/4 v6, 0x2

    .line 858
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 855
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 862
    :cond_1
    iget-object v2, p0, Loxg;->g:Ljava/lang/Float;

    if-eqz v2, :cond_2

    .line 863
    const/4 v2, 0x3

    iget-object v3, p0, Loxg;->g:Ljava/lang/Float;

    .line 864
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 866
    :cond_2
    iget-object v2, p0, Loxg;->h:Ljava/lang/Float;

    if-eqz v2, :cond_3

    .line 867
    const/4 v2, 0x4

    iget-object v3, p0, Loxg;->h:Ljava/lang/Float;

    .line 868
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 870
    :cond_3
    iget-object v2, p0, Loxg;->i:Ljava/lang/Float;

    if-eqz v2, :cond_4

    .line 871
    const/4 v2, 0x5

    iget-object v3, p0, Loxg;->i:Ljava/lang/Float;

    .line 872
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 874
    :cond_4
    iget-object v2, p0, Loxg;->k:Ljava/lang/Float;

    if-eqz v2, :cond_5

    .line 875
    const/4 v2, 0x6

    iget-object v3, p0, Loxg;->k:Ljava/lang/Float;

    .line 876
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 878
    :cond_5
    iget-object v2, p0, Loxg;->l:Ljava/lang/Float;

    if-eqz v2, :cond_6

    .line 879
    const/4 v2, 0x7

    iget-object v3, p0, Loxg;->l:Ljava/lang/Float;

    .line 880
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 882
    :cond_6
    iget-object v2, p0, Loxg;->x:[B

    if-eqz v2, :cond_7

    .line 883
    const/16 v2, 0x8

    iget-object v3, p0, Loxg;->x:[B

    .line 884
    invoke-static {v2, v3}, Loxo;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 886
    :cond_7
    iget-object v2, p0, Loxg;->j:Ljava/lang/Float;

    if-eqz v2, :cond_8

    .line 887
    const/16 v2, 0x9

    iget-object v3, p0, Loxg;->j:Ljava/lang/Float;

    .line 888
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 890
    :cond_8
    iget-object v2, p0, Loxg;->m:Ljava/lang/Float;

    if-eqz v2, :cond_9

    .line 891
    const/16 v2, 0xa

    iget-object v3, p0, Loxg;->m:Ljava/lang/Float;

    .line 892
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 894
    :cond_9
    iget-object v2, p0, Loxg;->n:Ljava/lang/Float;

    if-eqz v2, :cond_a

    .line 895
    const/16 v2, 0xb

    iget-object v3, p0, Loxg;->n:Ljava/lang/Float;

    .line 896
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 898
    :cond_a
    iget-object v2, p0, Loxg;->o:Ljava/lang/Float;

    if-eqz v2, :cond_b

    .line 899
    const/16 v2, 0xc

    iget-object v3, p0, Loxg;->o:Ljava/lang/Float;

    .line 900
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 902
    :cond_b
    iget-object v2, p0, Loxg;->p:Ljava/lang/Float;

    if-eqz v2, :cond_c

    .line 903
    const/16 v2, 0xd

    iget-object v3, p0, Loxg;->p:Ljava/lang/Float;

    .line 904
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 906
    :cond_c
    iget-object v2, p0, Loxg;->q:Ljava/lang/Float;

    if-eqz v2, :cond_d

    .line 907
    const/16 v2, 0xe

    iget-object v3, p0, Loxg;->q:Ljava/lang/Float;

    .line 908
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 910
    :cond_d
    iget-object v2, p0, Loxg;->r:Ljava/lang/Float;

    if-eqz v2, :cond_e

    .line 911
    const/16 v2, 0xf

    iget-object v3, p0, Loxg;->r:Ljava/lang/Float;

    .line 912
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 914
    :cond_e
    iget-object v2, p0, Loxg;->s:Ljava/lang/Float;

    if-eqz v2, :cond_f

    .line 915
    const/16 v2, 0x10

    iget-object v3, p0, Loxg;->s:Ljava/lang/Float;

    .line 916
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 918
    :cond_f
    iget-object v2, p0, Loxg;->t:Ljava/lang/Float;

    if-eqz v2, :cond_10

    .line 919
    const/16 v2, 0x11

    iget-object v3, p0, Loxg;->t:Ljava/lang/Float;

    .line 920
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 922
    :cond_10
    iget-object v2, p0, Loxg;->u:Ljava/lang/Float;

    if-eqz v2, :cond_11

    .line 923
    const/16 v2, 0x12

    iget-object v3, p0, Loxg;->u:Ljava/lang/Float;

    .line 924
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 926
    :cond_11
    iget-object v2, p0, Loxg;->v:Ljava/lang/Float;

    if-eqz v2, :cond_12

    .line 927
    const/16 v2, 0x13

    iget-object v3, p0, Loxg;->v:Ljava/lang/Float;

    .line 928
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 930
    :cond_12
    iget-object v2, p0, Loxg;->z:[B

    if-eqz v2, :cond_13

    .line 931
    const/16 v2, 0x14

    iget-object v3, p0, Loxg;->z:[B

    .line 932
    invoke-static {v2, v3}, Loxo;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 934
    :cond_13
    iget-object v2, p0, Loxg;->f:[Loxi;

    if-eqz v2, :cond_15

    .line 935
    iget-object v3, p0, Loxg;->f:[Loxi;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_15

    aget-object v5, v3, v2

    .line 936
    if-eqz v5, :cond_14

    .line 937
    const/16 v6, 0x15

    .line 938
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 935
    :cond_14
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 942
    :cond_15
    iget-object v2, p0, Loxg;->c:Loxh;

    if-eqz v2, :cond_16

    .line 943
    const/16 v2, 0x16

    iget-object v3, p0, Loxg;->c:Loxh;

    .line 944
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 946
    :cond_16
    iget-object v2, p0, Loxg;->y:[Loxm;

    if-eqz v2, :cond_18

    .line 947
    iget-object v2, p0, Loxg;->y:[Loxm;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_18

    aget-object v4, v2, v1

    .line 948
    if-eqz v4, :cond_17

    .line 949
    const/16 v5, 0x17

    .line 950
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 947
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 954
    :cond_18
    iget-object v1, p0, Loxg;->w:Ljava/lang/Float;

    if-eqz v1, :cond_19

    .line 955
    const/16 v1, 0x18

    iget-object v2, p0, Loxg;->w:Ljava/lang/Float;

    .line 956
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 958
    :cond_19
    iget-object v1, p0, Loxg;->d:Loxl;

    if-eqz v1, :cond_1a

    .line 959
    const/16 v1, 0x19

    iget-object v2, p0, Loxg;->d:Loxl;

    .line 960
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 962
    :cond_1a
    iget-object v1, p0, Loxg;->A:Loxk;

    if-eqz v1, :cond_1b

    .line 963
    const/16 v1, 0x1a

    iget-object v2, p0, Loxg;->A:Loxk;

    .line 964
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 966
    :cond_1b
    iget-object v1, p0, Loxg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 967
    iput v0, p0, Loxg;->ai:I

    .line 968
    return v0

    :cond_1c
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loxg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 976
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 977
    sparse-switch v0, :sswitch_data_0

    .line 981
    iget-object v2, p0, Loxg;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 982
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loxg;->ah:Ljava/util/List;

    .line 985
    :cond_1
    iget-object v2, p0, Loxg;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 987
    :sswitch_0
    return-object p0

    .line 992
    :sswitch_1
    iget-object v0, p0, Loxg;->b:Loxh;

    if-nez v0, :cond_2

    .line 993
    new-instance v0, Loxh;

    invoke-direct {v0}, Loxh;-><init>()V

    iput-object v0, p0, Loxg;->b:Loxh;

    .line 995
    :cond_2
    iget-object v0, p0, Loxg;->b:Loxh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 999
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1000
    iget-object v0, p0, Loxg;->e:[Loxj;

    if-nez v0, :cond_4

    move v0, v1

    .line 1001
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loxj;

    .line 1002
    iget-object v3, p0, Loxg;->e:[Loxj;

    if-eqz v3, :cond_3

    .line 1003
    iget-object v3, p0, Loxg;->e:[Loxj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1005
    :cond_3
    iput-object v2, p0, Loxg;->e:[Loxj;

    .line 1006
    :goto_2
    iget-object v2, p0, Loxg;->e:[Loxj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1007
    iget-object v2, p0, Loxg;->e:[Loxj;

    new-instance v3, Loxj;

    invoke-direct {v3}, Loxj;-><init>()V

    aput-object v3, v2, v0

    .line 1008
    iget-object v2, p0, Loxg;->e:[Loxj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1009
    invoke-virtual {p1}, Loxn;->a()I

    .line 1006
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1000
    :cond_4
    iget-object v0, p0, Loxg;->e:[Loxj;

    array-length v0, v0

    goto :goto_1

    .line 1012
    :cond_5
    iget-object v2, p0, Loxg;->e:[Loxj;

    new-instance v3, Loxj;

    invoke-direct {v3}, Loxj;-><init>()V

    aput-object v3, v2, v0

    .line 1013
    iget-object v2, p0, Loxg;->e:[Loxj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1017
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->g:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1021
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->h:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1025
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->i:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1029
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->k:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1033
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->l:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1037
    :sswitch_8
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Loxg;->x:[B

    goto/16 :goto_0

    .line 1041
    :sswitch_9
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->j:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1045
    :sswitch_a
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->m:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1049
    :sswitch_b
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->n:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1053
    :sswitch_c
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->o:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1057
    :sswitch_d
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->p:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1061
    :sswitch_e
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->q:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1065
    :sswitch_f
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->r:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1069
    :sswitch_10
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->s:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1073
    :sswitch_11
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->t:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1077
    :sswitch_12
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->u:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1081
    :sswitch_13
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->v:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1085
    :sswitch_14
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Loxg;->z:[B

    goto/16 :goto_0

    .line 1089
    :sswitch_15
    const/16 v0, 0xaa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1090
    iget-object v0, p0, Loxg;->f:[Loxi;

    if-nez v0, :cond_7

    move v0, v1

    .line 1091
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loxi;

    .line 1092
    iget-object v3, p0, Loxg;->f:[Loxi;

    if-eqz v3, :cond_6

    .line 1093
    iget-object v3, p0, Loxg;->f:[Loxi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1095
    :cond_6
    iput-object v2, p0, Loxg;->f:[Loxi;

    .line 1096
    :goto_4
    iget-object v2, p0, Loxg;->f:[Loxi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 1097
    iget-object v2, p0, Loxg;->f:[Loxi;

    new-instance v3, Loxi;

    invoke-direct {v3}, Loxi;-><init>()V

    aput-object v3, v2, v0

    .line 1098
    iget-object v2, p0, Loxg;->f:[Loxi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1099
    invoke-virtual {p1}, Loxn;->a()I

    .line 1096
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1090
    :cond_7
    iget-object v0, p0, Loxg;->f:[Loxi;

    array-length v0, v0

    goto :goto_3

    .line 1102
    :cond_8
    iget-object v2, p0, Loxg;->f:[Loxi;

    new-instance v3, Loxi;

    invoke-direct {v3}, Loxi;-><init>()V

    aput-object v3, v2, v0

    .line 1103
    iget-object v2, p0, Loxg;->f:[Loxi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1107
    :sswitch_16
    iget-object v0, p0, Loxg;->c:Loxh;

    if-nez v0, :cond_9

    .line 1108
    new-instance v0, Loxh;

    invoke-direct {v0}, Loxh;-><init>()V

    iput-object v0, p0, Loxg;->c:Loxh;

    .line 1110
    :cond_9
    iget-object v0, p0, Loxg;->c:Loxh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1114
    :sswitch_17
    const/16 v0, 0xba

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1115
    iget-object v0, p0, Loxg;->y:[Loxm;

    if-nez v0, :cond_b

    move v0, v1

    .line 1116
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loxm;

    .line 1117
    iget-object v3, p0, Loxg;->y:[Loxm;

    if-eqz v3, :cond_a

    .line 1118
    iget-object v3, p0, Loxg;->y:[Loxm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1120
    :cond_a
    iput-object v2, p0, Loxg;->y:[Loxm;

    .line 1121
    :goto_6
    iget-object v2, p0, Loxg;->y:[Loxm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    .line 1122
    iget-object v2, p0, Loxg;->y:[Loxm;

    new-instance v3, Loxm;

    invoke-direct {v3}, Loxm;-><init>()V

    aput-object v3, v2, v0

    .line 1123
    iget-object v2, p0, Loxg;->y:[Loxm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1124
    invoke-virtual {p1}, Loxn;->a()I

    .line 1121
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1115
    :cond_b
    iget-object v0, p0, Loxg;->y:[Loxm;

    array-length v0, v0

    goto :goto_5

    .line 1127
    :cond_c
    iget-object v2, p0, Loxg;->y:[Loxm;

    new-instance v3, Loxm;

    invoke-direct {v3}, Loxm;-><init>()V

    aput-object v3, v2, v0

    .line 1128
    iget-object v2, p0, Loxg;->y:[Loxm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1132
    :sswitch_18
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxg;->w:Ljava/lang/Float;

    goto/16 :goto_0

    .line 1136
    :sswitch_19
    iget-object v0, p0, Loxg;->d:Loxl;

    if-nez v0, :cond_d

    .line 1137
    new-instance v0, Loxl;

    invoke-direct {v0}, Loxl;-><init>()V

    iput-object v0, p0, Loxg;->d:Loxl;

    .line 1139
    :cond_d
    iget-object v0, p0, Loxg;->d:Loxl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1143
    :sswitch_1a
    iget-object v0, p0, Loxg;->A:Loxk;

    if-nez v0, :cond_e

    .line 1144
    new-instance v0, Loxk;

    invoke-direct {v0}, Loxk;-><init>()V

    iput-object v0, p0, Loxg;->A:Loxk;

    .line 1146
    :cond_e
    iget-object v0, p0, Loxg;->A:Loxk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 977
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x42 -> :sswitch_8
        0x4d -> :sswitch_9
        0x55 -> :sswitch_a
        0x5d -> :sswitch_b
        0x65 -> :sswitch_c
        0x6d -> :sswitch_d
        0x75 -> :sswitch_e
        0x7d -> :sswitch_f
        0x85 -> :sswitch_10
        0x8d -> :sswitch_11
        0x95 -> :sswitch_12
        0x9d -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc5 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 753
    iget-object v1, p0, Loxg;->b:Loxh;

    if-eqz v1, :cond_0

    .line 754
    const/4 v1, 0x1

    iget-object v2, p0, Loxg;->b:Loxh;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 756
    :cond_0
    iget-object v1, p0, Loxg;->e:[Loxj;

    if-eqz v1, :cond_2

    .line 757
    iget-object v2, p0, Loxg;->e:[Loxj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 758
    if-eqz v4, :cond_1

    .line 759
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 757
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 763
    :cond_2
    iget-object v1, p0, Loxg;->g:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 764
    const/4 v1, 0x3

    iget-object v2, p0, Loxg;->g:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 766
    :cond_3
    iget-object v1, p0, Loxg;->h:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 767
    const/4 v1, 0x4

    iget-object v2, p0, Loxg;->h:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 769
    :cond_4
    iget-object v1, p0, Loxg;->i:Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 770
    const/4 v1, 0x5

    iget-object v2, p0, Loxg;->i:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 772
    :cond_5
    iget-object v1, p0, Loxg;->k:Ljava/lang/Float;

    if-eqz v1, :cond_6

    .line 773
    const/4 v1, 0x6

    iget-object v2, p0, Loxg;->k:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 775
    :cond_6
    iget-object v1, p0, Loxg;->l:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 776
    const/4 v1, 0x7

    iget-object v2, p0, Loxg;->l:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 778
    :cond_7
    iget-object v1, p0, Loxg;->x:[B

    if-eqz v1, :cond_8

    .line 779
    const/16 v1, 0x8

    iget-object v2, p0, Loxg;->x:[B

    invoke-virtual {p1, v1, v2}, Loxo;->a(I[B)V

    .line 781
    :cond_8
    iget-object v1, p0, Loxg;->j:Ljava/lang/Float;

    if-eqz v1, :cond_9

    .line 782
    const/16 v1, 0x9

    iget-object v2, p0, Loxg;->j:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 784
    :cond_9
    iget-object v1, p0, Loxg;->m:Ljava/lang/Float;

    if-eqz v1, :cond_a

    .line 785
    const/16 v1, 0xa

    iget-object v2, p0, Loxg;->m:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 787
    :cond_a
    iget-object v1, p0, Loxg;->n:Ljava/lang/Float;

    if-eqz v1, :cond_b

    .line 788
    const/16 v1, 0xb

    iget-object v2, p0, Loxg;->n:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 790
    :cond_b
    iget-object v1, p0, Loxg;->o:Ljava/lang/Float;

    if-eqz v1, :cond_c

    .line 791
    const/16 v1, 0xc

    iget-object v2, p0, Loxg;->o:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 793
    :cond_c
    iget-object v1, p0, Loxg;->p:Ljava/lang/Float;

    if-eqz v1, :cond_d

    .line 794
    const/16 v1, 0xd

    iget-object v2, p0, Loxg;->p:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 796
    :cond_d
    iget-object v1, p0, Loxg;->q:Ljava/lang/Float;

    if-eqz v1, :cond_e

    .line 797
    const/16 v1, 0xe

    iget-object v2, p0, Loxg;->q:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 799
    :cond_e
    iget-object v1, p0, Loxg;->r:Ljava/lang/Float;

    if-eqz v1, :cond_f

    .line 800
    const/16 v1, 0xf

    iget-object v2, p0, Loxg;->r:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 802
    :cond_f
    iget-object v1, p0, Loxg;->s:Ljava/lang/Float;

    if-eqz v1, :cond_10

    .line 803
    const/16 v1, 0x10

    iget-object v2, p0, Loxg;->s:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 805
    :cond_10
    iget-object v1, p0, Loxg;->t:Ljava/lang/Float;

    if-eqz v1, :cond_11

    .line 806
    const/16 v1, 0x11

    iget-object v2, p0, Loxg;->t:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 808
    :cond_11
    iget-object v1, p0, Loxg;->u:Ljava/lang/Float;

    if-eqz v1, :cond_12

    .line 809
    const/16 v1, 0x12

    iget-object v2, p0, Loxg;->u:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 811
    :cond_12
    iget-object v1, p0, Loxg;->v:Ljava/lang/Float;

    if-eqz v1, :cond_13

    .line 812
    const/16 v1, 0x13

    iget-object v2, p0, Loxg;->v:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IF)V

    .line 814
    :cond_13
    iget-object v1, p0, Loxg;->z:[B

    if-eqz v1, :cond_14

    .line 815
    const/16 v1, 0x14

    iget-object v2, p0, Loxg;->z:[B

    invoke-virtual {p1, v1, v2}, Loxo;->a(I[B)V

    .line 817
    :cond_14
    iget-object v1, p0, Loxg;->f:[Loxi;

    if-eqz v1, :cond_16

    .line 818
    iget-object v2, p0, Loxg;->f:[Loxi;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_16

    aget-object v4, v2, v1

    .line 819
    if-eqz v4, :cond_15

    .line 820
    const/16 v5, 0x15

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 818
    :cond_15
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 824
    :cond_16
    iget-object v1, p0, Loxg;->c:Loxh;

    if-eqz v1, :cond_17

    .line 825
    const/16 v1, 0x16

    iget-object v2, p0, Loxg;->c:Loxh;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 827
    :cond_17
    iget-object v1, p0, Loxg;->y:[Loxm;

    if-eqz v1, :cond_19

    .line 828
    iget-object v1, p0, Loxg;->y:[Loxm;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_19

    aget-object v3, v1, v0

    .line 829
    if-eqz v3, :cond_18

    .line 830
    const/16 v4, 0x17

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 828
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 834
    :cond_19
    iget-object v0, p0, Loxg;->w:Ljava/lang/Float;

    if-eqz v0, :cond_1a

    .line 835
    const/16 v0, 0x18

    iget-object v1, p0, Loxg;->w:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 837
    :cond_1a
    iget-object v0, p0, Loxg;->d:Loxl;

    if-eqz v0, :cond_1b

    .line 838
    const/16 v0, 0x19

    iget-object v1, p0, Loxg;->d:Loxl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 840
    :cond_1b
    iget-object v0, p0, Loxg;->A:Loxk;

    if-eqz v0, :cond_1c

    .line 841
    const/16 v0, 0x1a

    iget-object v1, p0, Loxg;->A:Loxk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 843
    :cond_1c
    iget-object v0, p0, Loxg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 845
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Loxg;->a(Loxn;)Loxg;

    move-result-object v0

    return-object v0
.end method
