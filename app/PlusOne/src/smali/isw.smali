.class public final Lisw;
.super Landroid/os/Handler;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 237
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 238
    return-void
.end method

.method public static a()Lisw;
    .locals 2

    .prologue
    .line 241
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MtpImageView Fetch"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 242
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 243
    new-instance v1, Lisw;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Lisw;-><init>(Landroid/os/Looper;)V

    return-object v1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 249
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;

    .line 250
    if-nez v0, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 256
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v0, v2}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;Z)Z

    .line 257
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->b(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Landroid/mtp/MtpDevice;

    move-result-object v2

    .line 258
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->c(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Lisj;

    move-result-object v3

    .line 259
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 260
    if-eqz v2, :cond_0

    .line 263
    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a(Landroid/mtp/MtpDevice;Lisj;)Ljava/lang/Object;

    move-result-object v1

    .line 264
    if-eqz v1, :cond_0

    .line 267
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 268
    :try_start_1
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->c(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Lisj;

    move-result-object v4

    if-eq v4, v3, :cond_2

    .line 269
    monitor-exit v2

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 259
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 271
    :cond_2
    :try_start_3
    invoke-static {v0, v1}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;Landroid/mtp/MtpDevice;)Landroid/mtp/MtpDevice;

    .line 273
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;Lisj;)Lisj;

    .line 274
    invoke-static {}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->b()Lisx;

    move-result-object v1

    .line 275
    invoke-static {}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->b()Lisx;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->d(Lcom/google/android/libraries/social/ingest/ui/MtpImageView;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lisx;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 274
    invoke-virtual {v1, v0}, Lisx;->sendMessage(Landroid/os/Message;)Z

    .line 276
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
