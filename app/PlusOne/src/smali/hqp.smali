.class public final Lhqp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhqo;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;


# instance fields
.field private final f:Landroid/content/Context;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lhqs;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lhrj;

.field private i:Z

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Landroid/os/Handler;

.field private final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/lang/Object;

.field private final n:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 56
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, "bucket_id"

    aput-object v1, v0, v7

    const-string v1, "_data"

    aput-object v1, v0, v8

    const-string v1, "date_added"

    .line 61
    invoke-static {v1}, Lhql;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date_modified"

    .line 62
    invoke-static {v2}, Lhql;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 60
    invoke-static {v1, v2}, Lhql;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "corrected_added_modified"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " as "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    const-string v1, "mime_type"

    aput-object v1, v0, v10

    const/4 v1, 0x5

    const-string v2, "0 as orientation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    sput-object v0, Lhqp;->a:[Ljava/lang/String;

    .line 68
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v6

    const-string v1, "bucket_id"

    aput-object v1, v0, v7

    const-string v1, "_data"

    aput-object v1, v0, v8

    const-string v1, "date_added"

    .line 73
    invoke-static {v1}, Lhql;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date_modified"

    .line 74
    invoke-static {v2}, Lhql;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-static {v1, v2}, Lhql;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "corrected_added_modified"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " as "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    const-string v1, "mime_type"

    aput-object v1, v0, v10

    const/4 v1, 0x5

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    sput-object v0, Lhqp;->b:[Ljava/lang/String;

    .line 110
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "bucket_id"

    aput-object v1, v0, v6

    sput-object v0, Lhqp;->c:[Ljava/lang/String;

    .line 134
    new-array v0, v9, [Ljava/lang/String;

    const-string v1, "media_type"

    aput-object v1, v0, v6

    const-string v1, "volume_name"

    aput-object v1, v0, v7

    const-string v1, "last_media_id"

    aput-object v1, v0, v8

    sput-object v0, Lhqp;->d:[Ljava/lang/String;

    .line 153
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "bucket_id"

    aput-object v1, v0, v6

    sput-object v0, Lhqp;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhqp;->g:Ljava/util/Map;

    .line 172
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhqp;->j:Ljava/util/Set;

    .line 176
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhqp;->l:Ljava/util/Set;

    .line 181
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhqp;->m:Ljava/lang/Object;

    .line 183
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhqp;->n:Ljava/lang/Object;

    .line 186
    iput-object p1, p0, Lhqp;->f:Landroid/content/Context;

    .line 187
    const-class v0, Lhrj;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrj;

    iput-object v0, p0, Lhqp;->h:Lhrj;

    .line 188
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MediaTracker bucket changes"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lhqp;->k:Landroid/os/Handler;

    .line 189
    invoke-direct {p0}, Lhqp;->i()V

    .line 190
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 694
    const-class v0, Lhrj;

    .line 695
    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrj;

    invoke-virtual {v0}, Lhrj;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 699
    :try_start_0
    const-string v1, "media_tracker"

    sget-object v2, Lhqp;->d:[Ljava/lang/String;

    const-string v3, "volume_name = ? AND media_type = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 705
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 706
    const-string v0, "last_media_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    .line 709
    if-eqz v2, :cond_0

    .line 710
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 713
    :cond_0
    :goto_0
    return-wide v0

    .line 709
    :cond_1
    if-eqz v2, :cond_2

    .line 710
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 713
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 709
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_3

    .line 710
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 709
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method static synthetic a(Lhqp;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lhqp;->n:Ljava/lang/Object;

    return-object v0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 94
    const-string v0, "CREATE TABLE media_tracker (_id INTEGER PRIMARY KEY, volume_name TEXT NOT NULL, media_type TEXT NOT NULL,last_media_id INTEGER NOT NULL DEFAULT(0))"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 95
    const-string v0, "CREATE TABLE exclude_bucket (_id INTEGER PRIMARY KEY, bucket_id TEXT UNIQUE NOT NULL)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 96
    const-string v0, "CREATE TABLE local_folders (bucket_id TEXT UNIQUE NOT NULL)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method static synthetic b(Lhqp;)Lhrj;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lhqp;->h:Lhrj;

    return-object v0
.end method

.method static synthetic c(Lhqp;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lhqp;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lhqp;->b:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lhqp;->a:[Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 603
    new-instance v1, Lhqu;

    iget-object v0, p0, Lhqp;->f:Landroid/content/Context;

    const-string v2, "AUTO_BACKUP_MEDIA_TRACKER_INCLUDED_BUCKET_IDS"

    invoke-direct {v1, v0, v2}, Lhqu;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 604
    invoke-virtual {v1}, Lhqu;->a()Ljava/util/Set;

    move-result-object v0

    .line 605
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 606
    invoke-virtual {p0, v0}, Lhqp;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 608
    :cond_0
    invoke-virtual {v1}, Lhqu;->b()V

    .line 609
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    .line 615
    iget-object v0, p0, Lhqp;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqs;

    .line 616
    iget-object v3, p0, Lhqp;->h:Lhrj;

    iget-object v1, p0, Lhqp;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Lhqs;->a(Lhrj;J)V

    goto :goto_0

    .line 618
    :cond_0
    return-void
.end method

.method private i()V
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    .line 624
    iget-object v0, p0, Lhqp;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 627
    iget-object v0, p0, Lhqp;->g:Ljava/util/Map;

    new-instance v1, Lhqs;

    const-string v2, "photo"

    const-string v4, "external"

    invoke-direct {v1, v2, v4}, Lhqs;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 628
    iget-object v0, p0, Lhqp;->g:Ljava/util/Map;

    new-instance v1, Lhqs;

    const-string v2, "photo"

    const-string v4, "phoneStorage"

    invoke-direct {v1, v2, v4}, Lhqs;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    iget-object v0, p0, Lhqp;->g:Ljava/util/Map;

    new-instance v1, Lhqs;

    const-string v2, "video"

    const-string v4, "external"

    invoke-direct {v1, v2, v4}, Lhqs;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 630
    iget-object v0, p0, Lhqp;->g:Ljava/util/Map;

    new-instance v1, Lhqs;

    const-string v2, "video"

    const-string v4, "phoneStorage"

    invoke-direct {v1, v2, v4}, Lhqs;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 633
    iget-object v0, p0, Lhqp;->h:Lhrj;

    invoke-virtual {v0}, Lhrj;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 634
    const-string v1, "media_tracker"

    sget-object v2, Lhqp;->d:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 638
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 639
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 640
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 641
    const/4 v5, 0x2

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 642
    iget-object v5, p0, Lhqp;->g:Ljava/util/Map;

    new-instance v8, Lhqs;

    invoke-direct {v8, v2, v4}, Lhqs;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 645
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 648
    iget-object v1, p0, Lhqp;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 649
    :try_start_1
    iget-object v2, p0, Lhqp;->j:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 651
    const-string v5, "exclude_bucket"

    sget-object v6, Lhqp;->c:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v4, v0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v2

    .line 655
    :goto_1
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 656
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 657
    iget-object v5, p0, Lhqp;->j:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 660
    :catchall_1
    move-exception v0

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 662
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0

    .line 660
    :cond_1
    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 662
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 664
    const-string v1, "local_folders"

    sget-object v2, Lhqp;->e:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 668
    :goto_2
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 669
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 670
    iget-object v2, p0, Lhqp;->l:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_2

    .line 673
    :catchall_3
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 674
    return-void
.end method


# virtual methods
.method public a()I
    .locals 28

    .prologue
    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lhqp;->n:Ljava/lang/Object;

    move-object/from16 v18, v0

    monitor-enter v18

    .line 292
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lhqp;->m:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 294
    :try_start_1
    new-instance v19, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->j:Ljava/util/Set;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 295
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhqp;->i:Z

    if-eqz v2, :cond_0

    .line 298
    const/4 v2, 0x0

    monitor-exit v18
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 450
    :goto_0
    return v2

    .line 295
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    .line 451
    :catchall_1
    move-exception v2

    monitor-exit v18
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    .line 301
    :cond_0
    :try_start_5
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 302
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    invoke-static {v2}, Lhre;->a(Landroid/content/Context;)Lhre;

    move-result-object v4

    .line 303
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 304
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->h:Lhrj;

    invoke-virtual {v2}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 305
    const/4 v7, 0x0

    .line 306
    const/4 v15, 0x0

    .line 307
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    .line 310
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v22

    .line 311
    const-string v2, "exclusion_scanner.has_run"

    const/4 v8, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v2, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v23

    .line 316
    if-nez v23, :cond_1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v17, v2

    .line 318
    :goto_1
    if-nez v23, :cond_2

    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhqs;

    .line 324
    invoke-static {v2, v3}, Lhqs;->a(Lhqs;Landroid/content/ContentResolver;)Lhqt;

    move-result-object v9

    iget-wide v10, v9, Lhqt;->a:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-interface {v0, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 316
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v17, v2

    goto :goto_1

    .line 328
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_3
    :goto_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lhqs;

    move-object v14, v0

    .line 329
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhqp;->i:Z

    if-nez v2, :cond_d

    .line 330
    invoke-virtual {v14, v3}, Lhqs;->a(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 334
    const-string v2, "iu.UploadsManager"

    const/4 v8, 0x3

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 335
    iget-object v2, v14, Lhqs;->b:Ljava/lang/String;

    iget-object v8, v14, Lhqs;->a:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x9

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "SKIP; "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " ["

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, "]"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 340
    :cond_4
    const-string v2, "iu.UploadsManager"

    const/4 v8, 0x3

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 341
    iget-object v2, v14, Lhqs;->b:Ljava/lang/String;

    iget-object v8, v14, Lhqs;->a:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0xa

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "START; "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " ["

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, "]"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    move/from16 v16, v7

    .line 345
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->g:Ljava/util/Map;

    .line 346
    invoke-interface {v2, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v14, v3, v8, v9}, Lhqs;->a(Lhqs;Landroid/content/ContentResolver;J)Lhqt;

    move-result-object v12

    .line 347
    iget-wide v8, v12, Lhqt;->a:J

    .line 348
    iget-object v7, v12, Lhqt;->b:Ljava/lang/String;

    .line 349
    const-wide/16 v10, -0x1

    cmp-long v2, v8, v10

    if-eqz v2, :cond_15

    .line 350
    if-eqz v7, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->l:Ljava/util/Set;

    invoke-interface {v2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->l:Ljava/util/Set;

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->h:Lhrj;

    invoke-virtual {v2}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    new-instance v10, Landroid/content/ContentValues;

    const/4 v11, 0x1

    invoke-direct {v10, v11}, Landroid/content/ContentValues;-><init>(I)V

    const-string v11, "bucket_id"

    invoke-virtual {v10, v11, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "local_folders"

    const/4 v13, 0x0

    invoke-virtual {v2, v11, v13, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 356
    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 360
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lhqp;->b(Ljava/lang/String;)V

    .line 361
    if-eqz v23, :cond_6

    .line 362
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    const-class v10, Lhpu;

    invoke-static {v2, v10}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhpu;

    .line 364
    invoke-virtual {v2}, Lhpu;->e()Ljava/util/List;

    move-result-object v10

    .line 365
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_6

    .line 366
    invoke-virtual {v2}, Lhpu;->e()Ljava/util/List;

    move-result-object v2

    const/4 v10, 0x0

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 368
    move-object/from16 v0, p0

    iget-object v10, v0, Lhqp;->f:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v11, v12, Lhqt;->c:Ljava/lang/String;

    invoke-static {v10, v2, v11, v7}, Lhsb;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->g:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v2, v14, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    invoke-direct/range {p0 .. p0}, Lhqp;->h()V

    .line 377
    iget-object v2, v14, Lhqs;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 378
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    .line 379
    const-string v2, "photo"

    invoke-virtual {v14, v2}, Lhqs;->a(Ljava/lang/String;)Z

    move-result v11

    .line 387
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    .line 389
    iget-object v2, v12, Lhqt;->d:Ljava/lang/String;

    .line 390
    if-nez v23, :cond_7

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    cmp-long v2, v26, v8

    if-gez v2, :cond_b

    :cond_7
    if-nez v13, :cond_8

    .line 391
    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_8
    const/4 v12, 0x1

    .line 392
    :goto_5
    if-nez v23, :cond_9

    .line 393
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    cmp-long v2, v26, v8

    if-gez v2, :cond_c

    :cond_9
    if-eqz v13, :cond_c

    const/4 v13, 0x1

    .line 395
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    .line 396
    invoke-static/range {v2 .. v13}, Lhqn;->a(Landroid/content/Context;Landroid/content/ContentResolver;Lhrd;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;JLandroid/net/Uri;ZZZ)Z

    move-result v2

    .line 399
    add-int/lit8 v7, v16, 0x1

    .line 400
    if-eqz v2, :cond_14

    .line 401
    add-int/lit8 v2, v15, 0x1

    .line 403
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lhqp;->i:Z

    if-eqz v8, :cond_13

    .line 405
    :goto_8
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 406
    iget-object v8, v14, Lhqs;->b:Ljava/lang/String;

    iget-object v9, v14, Lhqs;->a:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x18

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "DONE; no more media; "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " ["

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    move v15, v2

    .line 409
    goto/16 :goto_3

    .line 391
    :cond_b
    const/4 v12, 0x0

    goto :goto_5

    .line 393
    :cond_c
    const/4 v13, 0x0

    goto :goto_6

    .line 411
    :cond_d
    const-string v2, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 414
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v20

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v6, 0x58

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "End new media; added: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", uploading: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", time: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ms"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    :cond_e
    invoke-interface/range {v22 .. v22}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "exclusion_scanner.has_run"

    const/4 v5, 0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 421
    invoke-direct/range {p0 .. p0}, Lhqp;->g()V

    .line 423
    if-lez v7, :cond_f

    .line 424
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    const-class v4, Lieh;

    invoke-static {v2, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lieh;

    .line 425
    move-object/from16 v0, p0

    iget-object v4, v0, Lhqp;->f:Landroid/content/Context;

    const-class v5, Lhei;

    invoke-static {v4, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lhei;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "logged_in"

    aput-object v8, v5, v6

    .line 426
    invoke-interface {v4, v5}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 428
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_11

    .line 431
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    const-class v4, Lhsc;

    .line 432
    invoke-static {v2, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhsc;

    .line 433
    invoke-virtual {v2}, Lhsc;->b()Z

    move-result v2

    .line 439
    :goto_9
    if-eqz v2, :cond_f

    .line 440
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    invoke-static {v2}, Lhsb;->e(Landroid/content/Context;)V

    .line 444
    :cond_f
    if-lez v15, :cond_10

    .line 445
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    invoke-static {v2}, Lhqv;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 446
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    .line 447
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v3, 0x0

    .line 446
    invoke-static {v2, v4, v5, v3}, Lhsb;->a(Landroid/content/Context;JZ)V

    .line 450
    :cond_10
    monitor-exit v18

    move v2, v7

    goto/16 :goto_0

    .line 436
    :cond_11
    sget-object v5, Lhpi;->g:Lief;

    const/4 v6, 0x0

    .line 437
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 436
    invoke-interface {v2, v5, v4}, Lieh;->b(Lief;I)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v2

    if-nez v2, :cond_12

    const/4 v2, 0x1

    goto :goto_9

    :cond_12
    const/4 v2, 0x0

    goto :goto_9

    :cond_13
    move v15, v2

    move/from16 v16, v7

    goto/16 :goto_4

    :cond_14
    move v2, v15

    goto/16 :goto_7

    :cond_15
    move v2, v15

    move/from16 v7, v16

    goto/16 :goto_8
.end method

.method public a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 572
    iget-object v1, p0, Lhqp;->n:Ljava/lang/Object;

    monitor-enter v1

    .line 573
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lhqp;->i:Z

    .line 574
    iget-object v0, p0, Lhqp;->h:Lhrj;

    invoke-virtual {v0}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 575
    const-string v2, "media_tracker"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 576
    const-string v2, "local_folders"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 577
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "exclusion_scanner.has_run"

    .line 578
    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 579
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 580
    invoke-direct {p0}, Lhqp;->i()V

    .line 581
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhqp;->i:Z

    .line 582
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 201
    iget-object v1, p0, Lhqp;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 204
    :try_start_0
    iget-object v0, p0, Lhqp;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Lhqu;

    iget-object v2, p0, Lhqp;->f:Landroid/content/Context;

    const-string v3, "AUTO_BACKUP_MEDIA_TRACKER_INCLUDED_BUCKET_IDS"

    invoke-direct {v0, v2, v3}, Lhqu;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 206
    invoke-virtual {v0, p1}, Lhqu;->a(Ljava/lang/String;)V

    .line 210
    :cond_0
    iget-object v0, p0, Lhqp;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 211
    monitor-exit v1

    .line 248
    :goto_0
    return-void

    .line 215
    :cond_1
    iget-object v0, p0, Lhqp;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 216
    iget-object v2, p0, Lhqp;->f:Landroid/content/Context;

    invoke-static {v2}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 218
    iget-object v0, p0, Lhqp;->k:Landroid/os/Handler;

    new-instance v2, Lhqq;

    invoke-direct {v2, p0, p1}, Lhqq;-><init>(Lhqp;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 248
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()I
    .locals 22

    .prologue
    .line 456
    move-object/from16 v0, p0

    iget-object v0, v0, Lhqp;->n:Ljava/lang/Object;

    move-object/from16 v17, v0

    monitor-enter v17

    .line 458
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lhqp;->m:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 460
    :try_start_1
    new-instance v18, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->j:Ljava/util/Set;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 461
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhqp;->i:Z

    if-eqz v2, :cond_0

    .line 464
    const/4 v2, 0x0

    monitor-exit v17
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 535
    :goto_0
    return v2

    .line 461
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    .line 536
    :catchall_1
    move-exception v2

    monitor-exit v17
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    .line 467
    :cond_0
    :try_start_5
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 468
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 469
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->h:Lhrj;

    invoke-virtual {v2}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 470
    const/4 v4, 0x0

    .line 471
    const/4 v15, 0x0

    .line 473
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 475
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_1
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lhqs;

    move-object v14, v0

    .line 480
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhqp;->i:Z

    if-nez v2, :cond_8

    .line 481
    invoke-virtual {v14, v3}, Lhqs;->a(Landroid/content/ContentResolver;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 485
    const-string v2, "iu.UploadsManager"

    const/4 v7, 0x3

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 486
    const-string v2, "SKIP; no storage for config: "

    invoke-virtual {v14}, Lhqs;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v2, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_2
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 491
    :cond_3
    const-wide/16 v8, -0x1

    move-wide v10, v8

    move/from16 v16, v4

    .line 493
    :goto_2
    invoke-static {v14, v3, v10, v11}, Lhqs;->a(Lhqs;Landroid/content/ContentResolver;J)Lhqt;

    move-result-object v2

    .line 494
    iget-wide v8, v2, Lhqt;->a:J

    .line 495
    iget-object v7, v2, Lhqt;->b:Ljava/lang/String;

    .line 496
    const-wide/16 v12, -0x1

    cmp-long v4, v8, v12

    if-nez v4, :cond_6

    .line 497
    const-string v2, "iu.UploadsManager"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 498
    const-string v2, "DONE; no more media of type: "

    invoke-virtual {v14}, Lhqs;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 501
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->g:Ljava/util/Map;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v14, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    invoke-direct/range {p0 .. p0}, Lhqp;->h()V

    move/from16 v4, v16

    .line 503
    goto :goto_1

    .line 498
    :cond_5
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 507
    :cond_6
    iget-object v4, v14, Lhqs;->c:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 508
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    .line 509
    const-string v4, "photo"

    invoke-virtual {v14, v4}, Lhqs;->a(Ljava/lang/String;)Z

    move-result v11

    .line 510
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 512
    iget-object v2, v2, Lhqt;->d:Ljava/lang/String;

    .line 513
    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    if-nez v4, :cond_7

    const/4 v12, 0x1

    .line 514
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v13, 0x0

    .line 515
    invoke-static/range {v2 .. v13}, Lhqn;->a(Landroid/content/Context;Landroid/content/ContentResolver;Lhrd;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;JLandroid/net/Uri;ZZZ)Z

    move-result v2

    .line 518
    add-int/lit8 v4, v16, 0x1

    .line 519
    if-eqz v2, :cond_c

    .line 520
    add-int/lit8 v2, v15, 0x1

    .line 522
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lhqp;->i:Z

    if-eqz v7, :cond_b

    move v15, v2

    .line 523
    goto/16 :goto_1

    .line 513
    :cond_7
    const/4 v12, 0x0

    goto :goto_4

    .line 525
    :cond_8
    const-string v2, "iu.UploadsManager"

    const/4 v5, 0x4

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 528
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v20

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v5, 0x58

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "End all media; added: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", uploading: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", time: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " ms"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    :cond_9
    if-lez v15, :cond_a

    .line 532
    move-object/from16 v0, p0

    iget-object v2, v0, Lhqp;->f:Landroid/content/Context;

    invoke-static {v2}, Lhqv;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 535
    :cond_a
    monitor-exit v17
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move v2, v4

    goto/16 :goto_0

    :cond_b
    move-wide v10, v8

    move v15, v2

    move/from16 v16, v4

    goto/16 :goto_2

    :cond_c
    move v2, v15

    goto :goto_5
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 253
    iget-object v1, p0, Lhqp;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 255
    :try_start_0
    iget-object v0, p0, Lhqp;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    monitor-exit v1

    .line 285
    :goto_0
    return-void

    .line 260
    :cond_0
    iget-object v0, p0, Lhqp;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 261
    iget-object v2, p0, Lhqp;->f:Landroid/content/Context;

    invoke-static {v2}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 263
    iget-object v0, p0, Lhqp;->k:Landroid/os/Handler;

    new-instance v2, Lhqr;

    invoke-direct {v2, p0, p1}, Lhqr;-><init>(Lhqp;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 285
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Z
    .locals 3

    .prologue
    .line 541
    iget-object v0, p0, Lhqp;->f:Landroid/content/Context;

    .line 542
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 543
    const-string v1, "exclusion_scanner.has_run"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 194
    iget-object v1, p0, Lhqp;->m:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_0
    iget-object v0, p0, Lhqp;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lhqp;->j:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 592
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 593
    const-string v0, "MediaTracker:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    iget-object v0, p0, Lhqp;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqs;

    .line 595
    const-string v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lhqs;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lhqp;->g:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 598
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
