.class public final Lnrb;
.super Loxq;
.source "PG"


# instance fields
.field private a:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 652
    invoke-direct {p0}, Loxq;-><init>()V

    .line 655
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnrb;->a:[I

    .line 652
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 671
    .line 672
    iget-object v1, p0, Lnrb;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnrb;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 674
    iget-object v2, p0, Lnrb;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 676
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 674
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 679
    :cond_0
    iget-object v0, p0, Lnrb;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 681
    :cond_1
    iget-object v1, p0, Lnrb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 682
    iput v0, p0, Lnrb;->ai:I

    .line 683
    return v0
.end method

.method public a(Loxn;)Lnrb;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 691
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 692
    sparse-switch v0, :sswitch_data_0

    .line 696
    iget-object v1, p0, Lnrb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 697
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnrb;->ah:Ljava/util/List;

    .line 700
    :cond_1
    iget-object v1, p0, Lnrb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 702
    :sswitch_0
    return-object p0

    .line 707
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 708
    iget-object v0, p0, Lnrb;->a:[I

    array-length v0, v0

    .line 709
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 710
    iget-object v2, p0, Lnrb;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 711
    iput-object v1, p0, Lnrb;->a:[I

    .line 712
    :goto_1
    iget-object v1, p0, Lnrb;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 713
    iget-object v1, p0, Lnrb;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 714
    invoke-virtual {p1}, Loxn;->a()I

    .line 712
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 717
    :cond_2
    iget-object v1, p0, Lnrb;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 692
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 660
    iget-object v0, p0, Lnrb;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnrb;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 661
    iget-object v1, p0, Lnrb;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 662
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 661
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 665
    :cond_0
    iget-object v0, p0, Lnrb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 667
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 648
    invoke-virtual {p0, p1}, Lnrb;->a(Loxn;)Lnrb;

    move-result-object v0

    return-object v0
.end method
