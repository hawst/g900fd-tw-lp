.class public final Llec;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lkdd;
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private A:Landroid/graphics/Paint;

.field private B:Landroid/graphics/Path;

.field private C:Z

.field private D:Z

.field private E:I

.field private F:Ljaz;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lkzs;

.field private f:Lkzs;

.field private g:Lled;

.field private h:Landroid/text/StaticLayout;

.field private i:Landroid/text/StaticLayout;

.field private j:Landroid/widget/Button;

.field private k:I

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Landroid/text/StaticLayout;

.field private o:Lizu;

.field private p:Lcom/google/android/libraries/social/media/MediaResource;

.field private q:Landroid/graphics/Rect;

.field private r:Landroid/graphics/Rect;

.field private s:Landroid/graphics/Rect;

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Llec;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 120
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Llec;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 127
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 111
    iput-boolean v1, p0, Llec;->D:Z

    .line 129
    sget-object v0, Llec;->a:Llct;

    if-nez v0, :cond_0

    .line 130
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Llec;->a:Llct;

    .line 133
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llec;->q:Landroid/graphics/Rect;

    .line 134
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llec;->r:Landroid/graphics/Rect;

    .line 135
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llec;->s:Landroid/graphics/Rect;

    .line 137
    invoke-virtual {p0, v1}, Llec;->setWillNotDraw(Z)V

    .line 138
    invoke-virtual {p0, v2}, Llec;->setFocusable(Z)V

    .line 139
    invoke-virtual {p0, v2}, Llec;->setClickable(Z)V

    .line 140
    invoke-virtual {p0, p0}, Llec;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    return-void
.end method

.method private a(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 618
    iget-boolean v0, p0, Llec;->D:Z

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Llec;->s:Landroid/graphics/Rect;

    invoke-virtual {v0, v3, v3, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 630
    :goto_0
    return-void

    .line 624
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 625
    if-le p1, v0, :cond_1

    .line 626
    iget-object v1, p0, Llec;->s:Landroid/graphics/Rect;

    sub-int v2, p1, v0

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, p1

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1, v2, v3, v0, p2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 628
    :cond_1
    iget-object v1, p0, Llec;->s:Landroid/graphics/Rect;

    sub-int v2, p2, v0

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1, v3, v2, p1, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkzs;Lkzs;IIZLled;II)V
    .locals 8

    .prologue
    .line 171
    invoke-direct {p0}, Llec;->e()V

    .line 172
    iput-object p1, p0, Llec;->b:Ljava/lang/String;

    .line 173
    iput-object p3, p0, Llec;->c:Ljava/lang/String;

    .line 174
    iput-object p6, p0, Llec;->f:Lkzs;

    .line 175
    if-eqz p7, :cond_7

    :goto_0
    iput-object p7, p0, Llec;->e:Lkzs;

    .line 177
    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 179
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 180
    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v3, p2, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    iput-object v2, p0, Llec;->o:Lizu;

    .line 183
    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v4, Ljaa;

    invoke-static {v2, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljaa;

    invoke-interface {v2}, Ljaa;->a()Z

    move-result v4

    .line 185
    if-lez p9, :cond_8

    if-eqz v4, :cond_8

    .line 187
    invoke-static {v3}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v2

    const-class v5, Llcq;

    invoke-virtual {v2, v5}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llcq;

    .line 188
    move/from16 v0, p9

    invoke-virtual {v2, v0}, Llcq;->a(I)I

    move-result v2

    iput v2, p0, Llec;->y:I

    .line 193
    :goto_1
    if-eqz v4, :cond_9

    .line 194
    iget v2, p0, Llec;->y:I

    div-int/lit8 v2, v2, 0x4

    iput v2, p0, Llec;->t:I

    .line 199
    :goto_2
    move/from16 v0, p12

    iput v0, p0, Llec;->u:I

    .line 200
    move/from16 v0, p13

    iput v0, p0, Llec;->v:I

    .line 202
    iget v2, p0, Llec;->u:I

    iget v4, p0, Llec;->v:I

    const/16 v5, 0x190

    if-lt v2, v5, :cond_a

    int-to-double v6, v2

    int-to-double v4, v4

    div-double v4, v6, v4

    const-wide/high16 v6, 0x4004000000000000L    # 2.5

    cmpg-double v2, v4, v6

    if-gtz v2, :cond_a

    const-wide v6, 0x3fe1c71c71c71c72L    # 0.5555555555555556

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_a

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_b

    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v4, Lhee;

    invoke-static {v2, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v4

    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v5, Lkzl;

    invoke-static {v2, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkzl;

    invoke-interface {v2, v4}, Lkzl;->d(I)Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, p0, Llec;->f:Lkzs;

    if-nez v2, :cond_b

    const/4 v2, 0x1

    :goto_4
    iput-boolean v2, p0, Llec;->D:Z

    .line 205
    iget-boolean v2, p0, Llec;->D:Z

    if-eqz v2, :cond_1

    .line 208
    iget v2, p0, Llec;->u:I

    int-to-double v4, v2

    iget v2, p0, Llec;->v:I

    int-to-double v6, v2

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x3fe2000000000000L    # 0.5625

    cmpg-double v2, v4, v6

    if-gez v2, :cond_0

    .line 209
    iget v2, p0, Llec;->u:I

    int-to-double v4, v2

    const-wide/high16 v6, 0x3fe2000000000000L    # 0.5625

    div-double/2addr v4, v6

    double-to-int v2, v4

    iput v2, p0, Llec;->v:I

    .line 210
    iget v2, p0, Llec;->y:I

    int-to-double v4, v2

    const-wide/high16 v6, 0x3fe2000000000000L    # 0.5625

    div-double/2addr v4, v6

    double-to-int v2, v4

    iput v2, p0, Llec;->z:I

    .line 212
    :cond_0
    iput-object p5, p0, Llec;->d:Ljava/lang/String;

    .line 215
    :cond_1
    invoke-virtual {p0}, Llec;->b()V

    .line 221
    :goto_5
    iput-object p4, p0, Llec;->m:Ljava/lang/String;

    .line 223
    move-object/from16 v0, p11

    iput-object v0, p0, Llec;->g:Lled;

    .line 227
    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    if-eqz v2, :cond_2

    .line 228
    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    invoke-virtual {p0, v2}, Llec;->removeView(Landroid/view/View;)V

    .line 230
    :cond_2
    iget-object v2, p0, Llec;->f:Lkzs;

    if-eqz v2, :cond_3

    .line 231
    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    if-eqz v2, :cond_d

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    :goto_6
    invoke-virtual {p0, v2}, Llec;->addView(Landroid/view/View;)V

    .line 232
    iget-object v4, p0, Llec;->j:Landroid/widget/Button;

    if-eqz p11, :cond_e

    const/4 v2, 0x1

    :goto_7
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 237
    :cond_3
    if-nez p10, :cond_4

    iget-object v2, p0, Llec;->f:Lkzs;

    if-eqz v2, :cond_5

    .line 238
    :cond_4
    iput-object p5, p0, Llec;->d:Ljava/lang/String;

    .line 241
    :cond_5
    const-class v2, Lizp;

    invoke-static {v3, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizp;

    .line 243
    if-eqz v2, :cond_f

    invoke-interface {v2}, Lizp;->a()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 244
    const-class v2, Ljba;

    .line 245
    invoke-static {v3, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljba;

    .line 246
    if-eqz v2, :cond_6

    .line 247
    invoke-interface {v2, v3}, Ljba;->a(Landroid/content/Context;)Ljaz;

    move-result-object v2

    iput-object v2, p0, Llec;->F:Ljaz;

    .line 253
    :cond_6
    :goto_8
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 254
    invoke-virtual {p0}, Llec;->requestLayout()V

    .line 255
    return-void

    :cond_7
    move-object p7, p6

    .line 175
    goto/16 :goto_0

    .line 190
    :cond_8
    move/from16 v0, p8

    iput v0, p0, Llec;->y:I

    goto/16 :goto_1

    .line 196
    :cond_9
    div-int/lit8 v2, p8, 0x4

    iput v2, p0, Llec;->t:I

    goto/16 :goto_2

    .line 202
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 217
    :cond_c
    const/4 v2, 0x0

    iput v2, p0, Llec;->t:I

    .line 218
    const/4 v2, 0x0

    iput v2, p0, Llec;->y:I

    .line 219
    invoke-virtual {p0}, Llec;->c()V

    goto :goto_5

    .line 231
    :cond_d
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Llec;->j:Landroid/widget/Button;

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setSingleLine(Z)V

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Llec;->j:Landroid/widget/Button;

    const/16 v5, 0xb

    invoke-static {v2, v4, v5}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    const/16 v4, 0x10

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setGravity(I)V

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    const v4, 0x7f0205ab

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    const v4, 0x7f0201c4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    sget-object v4, Llec;->a:Llct;

    iget v4, v4, Llct;->n:I

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setCompoundDrawablePadding(I)V

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    sget-object v4, Llec;->a:Llct;

    iget v4, v4, Llct;->ba:I

    const/4 v5, 0x0

    sget-object v6, Llec;->a:Llct;

    iget v6, v6, Llct;->ba:I

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/widget/Button;->setPadding(IIII)V

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Llec;->j:Landroid/widget/Button;

    goto/16 :goto_6

    .line 232
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 250
    :cond_f
    const/4 v2, 0x0

    iput-object v2, p0, Llec;->F:Ljaz;

    goto/16 :goto_8
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 859
    invoke-virtual {p0}, Llec;->c()V

    .line 860
    invoke-virtual {p0}, Llec;->removeAllViews()V

    .line 862
    iput-object v1, p0, Llec;->b:Ljava/lang/String;

    .line 863
    iput-object v1, p0, Llec;->d:Ljava/lang/String;

    .line 864
    iput-object v1, p0, Llec;->c:Ljava/lang/String;

    .line 865
    iput-object v1, p0, Llec;->e:Lkzs;

    .line 866
    iput-object v1, p0, Llec;->f:Lkzs;

    .line 867
    iput-object v1, p0, Llec;->g:Lled;

    .line 871
    iput-object v1, p0, Llec;->h:Landroid/text/StaticLayout;

    .line 872
    iput-object v1, p0, Llec;->i:Landroid/text/StaticLayout;

    .line 873
    iput v2, p0, Llec;->k:I

    .line 874
    iput v2, p0, Llec;->l:I

    .line 875
    iput-object v1, p0, Llec;->m:Ljava/lang/String;

    .line 876
    iput-object v1, p0, Llec;->n:Landroid/text/StaticLayout;

    .line 877
    iput-object v1, p0, Llec;->o:Lizu;

    .line 878
    iget-object v0, p0, Llec;->s:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 879
    iget-object v0, p0, Llec;->q:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 880
    iget-object v0, p0, Llec;->r:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 881
    iput v2, p0, Llec;->u:I

    .line 882
    iput v2, p0, Llec;->v:I

    .line 883
    iput v2, p0, Llec;->t:I

    .line 884
    iput v2, p0, Llec;->E:I

    .line 885
    iput-object v1, p0, Llec;->j:Landroid/widget/Button;

    .line 886
    iput v2, p0, Llec;->w:I

    .line 887
    iput v2, p0, Llec;->x:I

    .line 889
    iput-object v1, p0, Llec;->A:Landroid/graphics/Paint;

    .line 890
    iput-object v1, p0, Llec;->B:Landroid/graphics/Path;

    .line 891
    iput-boolean v2, p0, Llec;->D:Z

    .line 892
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 289
    invoke-direct {p0}, Llec;->e()V

    .line 290
    iget-object v0, p0, Llec;->F:Ljaz;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Llec;->F:Ljaz;

    invoke-interface {v0}, Ljaz;->a()V

    .line 293
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 590
    iget-object v0, p0, Llec;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 591
    const-string v0, "link_title"

    iget-object v1, p0, Llec;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 594
    :cond_0
    iget-object v0, p0, Llec;->f:Lkzs;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 596
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 597
    const-string v1, "deep_link_label"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 600
    :cond_1
    iget-object v0, p0, Llec;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 601
    const-string v0, "link_url"

    iget-object v1, p0, Llec;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 603
    :cond_2
    return-void

    .line 594
    :cond_3
    iget-object v0, p0, Llec;->f:Lkzs;

    .line 595
    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkzs;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 559
    invoke-virtual {p0}, Llec;->invalidate()V

    .line 560
    return-void
.end method

.method public a(Lkzv;Lkzs;Lkzs;Ljava/lang/String;Ljava/lang/String;IIZLled;)V
    .locals 14

    .prologue
    .line 146
    invoke-virtual {p1}, Lkzv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lkzv;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lkzv;->c()Ljava/lang/String;

    move-result-object v3

    .line 147
    invoke-virtual {p1}, Lkzv;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lkzv;->b()Ljava/lang/String;

    move-result-object v5

    .line 149
    invoke-virtual {p1}, Lkzv;->k()S

    move-result v12

    .line 150
    invoke-virtual {p1}, Lkzv;->l()S

    move-result v13

    move-object v0, p0

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move-object/from16 v11, p9

    .line 146
    invoke-direct/range {v0 .. v13}, Llec;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkzs;Lkzs;IIZLled;II)V

    .line 151
    return-void
.end method

.method public a(Lkzy;I)V
    .locals 15

    .prologue
    .line 154
    invoke-virtual/range {p1 .. p1}, Lkzy;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lkzy;->c()Ljava/lang/String;

    move-result-object v2

    .line 155
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lkzy;->b()Ljava/lang/String;

    move-result-object v6

    .line 157
    invoke-virtual/range {p1 .. p1}, Lkzy;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lkzy;->f()Ljava/lang/String;

    move-result-object v4

    .line 158
    invoke-virtual/range {p1 .. p1}, Lkzy;->e()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, -0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    move/from16 v0, p2

    int-to-short v13, v0

    move/from16 v0, p2

    int-to-short v14, v0

    move-object v1, p0

    move/from16 v9, p2

    .line 157
    invoke-direct/range {v1 .. v14}, Llec;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkzs;Lkzs;IIZLled;II)V

    .line 162
    return-void

    .line 154
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lkzy;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public b()V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/16 v5, 0x40

    .line 497
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 546
    :cond_0
    :goto_0
    return-void

    .line 501
    :cond_1
    iget-object v0, p0, Llec;->o:Lizu;

    if-eqz v0, :cond_0

    .line 506
    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljaa;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljaa;

    invoke-interface {v0}, Ljaa;->a()Z

    move-result v0

    .line 508
    iget-boolean v1, p0, Llec;->D:Z

    if-eqz v1, :cond_5

    .line 512
    iget v1, p0, Llec;->w:I

    if-lez v1, :cond_2

    iget v1, p0, Llec;->w:I

    if-lez v1, :cond_2

    .line 513
    if-eqz v0, :cond_3

    .line 514
    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iget-object v1, p0, Llec;->o:Lizu;

    iget v2, p0, Llec;->y:I

    iget v3, p0, Llec;->z:I

    move-object v6, p0

    .line 515
    invoke-virtual/range {v0 .. v6}, Lizs;->a(Lizu;IIIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    .line 543
    :cond_2
    :goto_1
    iget-object v0, p0, Llec;->F:Ljaz;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Llec;->F:Ljaz;

    iget-object v1, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-interface {v0, v1}, Ljaz;->a(Lcom/google/android/libraries/social/media/MediaResource;)V

    goto :goto_0

    .line 519
    :cond_3
    iget v3, p0, Llec;->u:I

    .line 520
    iget v4, p0, Llec;->v:I

    .line 521
    iget v0, p0, Llec;->w:I

    if-le v3, v0, :cond_4

    .line 522
    iget v3, p0, Llec;->w:I

    .line 523
    iget v4, p0, Llec;->x:I

    .line 525
    :cond_4
    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizs;

    iget-object v2, p0, Llec;->o:Lizu;

    move-object v6, p0

    .line 526
    invoke-virtual/range {v1 .. v6}, Lizs;->a(Lizu;IIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    goto :goto_1

    .line 533
    :cond_5
    iget v1, p0, Llec;->t:I

    if-eqz v1, :cond_2

    .line 534
    if-eqz v0, :cond_6

    .line 537
    :goto_2
    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iget-object v1, p0, Llec;->o:Lizu;

    iget v2, p0, Llec;->t:I

    iget v3, p0, Llec;->t:I

    move-object v6, p0

    .line 538
    invoke-virtual/range {v0 .. v6}, Lizs;->a(Lizu;IIIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    goto :goto_1

    .line 534
    :cond_6
    const/4 v4, -0x1

    goto :goto_2
.end method

.method public c()V
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 552
    const/4 v0, 0x0

    iput-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    .line 554
    :cond_0
    iget-object v0, p0, Llec;->s:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 555
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 580
    iget-boolean v0, p0, Llec;->D:Z

    return v0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 585
    invoke-virtual {p0}, Llec;->invalidate()V

    .line 586
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 587
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 5
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 260
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 261
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Llec;->b:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 262
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Llec;->d:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 263
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 471
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 476
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Llec;->C:Z

    .line 477
    invoke-virtual {p0}, Llec;->b()V

    .line 478
    return-void

    .line 476
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 564
    iget-object v0, p0, Llec;->g:Lled;

    if-nez v0, :cond_1

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    if-ne p1, p0, :cond_2

    .line 569
    iget-object v0, p0, Llec;->g:Lled;

    iget-object v1, p0, Llec;->c:Ljava/lang/String;

    iget-object v2, p0, Llec;->e:Lkzs;

    invoke-interface {v0, v1, v2}, Lled;->a(Ljava/lang/String;Lkzs;)V

    goto :goto_0

    .line 570
    :cond_2
    iget-object v0, p0, Llec;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 571
    iget-object v0, p0, Llec;->g:Lled;

    iget-object v1, p0, Llec;->f:Lkzs;

    invoke-interface {v0, v1}, Lled;->a(Lkzs;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 482
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 483
    invoke-virtual {p0}, Llec;->c()V

    .line 484
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 403
    iget-boolean v1, p0, Llec;->D:Z

    if-eqz v1, :cond_9

    .line 404
    iget-object v1, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v1, :cond_13

    iget-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getWidth()I

    move-result v1

    iget-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getHeight()I

    move-result v0

    :goto_0
    if-eqz v2, :cond_2

    iget-object v3, p0, Llec;->s:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, v1, v0}, Llec;->a(II)V

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-boolean v0, p0, Llec;->C:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Llec;->B:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    :cond_1
    iget-object v0, p0, Llec;->q:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Llec;->s:Landroid/graphics/Rect;

    iget-object v1, p0, Llec;->q:Landroid/graphics/Rect;

    sget-object v3, Llec;->a:Llct;

    iget-object v3, v3, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Llec;->q:Landroid/graphics/Rect;

    iget-object v1, p0, Llec;->A:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-object v0, p0, Llec;->r:Landroid/graphics/Rect;

    sget-object v1, Llec;->a:Llct;

    iget-object v1, v1, Llct;->bi:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    invoke-virtual {p0}, Llec;->getWidth()I

    move-result v1

    sget-object v0, Llec;->a:Llct;

    iget v2, v0, Llct;->bl:I

    iget v0, p0, Llec;->x:I

    sget-object v3, Llec;->a:Llct;

    iget v3, v3, Llct;->bj:I

    add-int/2addr v0, v3

    iget-object v3, p0, Llec;->h:Landroid/text/StaticLayout;

    if-eqz v3, :cond_3

    int-to-float v3, v2

    int-to-float v4, v0

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Llec;->h:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v3, v2

    int-to-float v3, v3

    neg-int v4, v0

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Llec;->h:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget-object v4, Llec;->a:Llct;

    iget v4, v4, Llct;->bj:I

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    :cond_3
    iget-object v3, p0, Llec;->n:Landroid/text/StaticLayout;

    if-eqz v3, :cond_4

    int-to-float v3, v2

    int-to-float v4, v0

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Llec;->n:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v3, v2

    int-to-float v3, v3

    neg-int v4, v0

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Llec;->n:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget-object v4, Llec;->a:Llct;

    iget v4, v4, Llct;->bn:I

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    :cond_4
    iget-object v3, p0, Llec;->i:Landroid/text/StaticLayout;

    if-eqz v3, :cond_5

    int-to-float v3, v2

    int-to-float v4, v0

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Llec;->i:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v2, v2

    int-to-float v2, v2

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Llec;->i:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->bj:I

    :cond_5
    iget-object v0, p0, Llec;->g:Lled;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Llec;->isPressed()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Llec;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    sget-object v0, Llec;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Llec;->getHeight()I

    move-result v2

    invoke-virtual {v0, v8, v8, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v0, Llec;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_7
    iget-object v0, p0, Llec;->F:Ljaz;

    if-eqz v0, :cond_8

    iget-object v0, p0, Llec;->F:Ljaz;

    iget v2, p0, Llec;->x:I

    invoke-interface {v0, p1, v1, v2}, Ljaz;->a(Landroid/graphics/Canvas;II)V

    .line 467
    :cond_8
    :goto_1
    return-void

    .line 411
    :cond_9
    iget-object v1, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v1, :cond_12

    .line 412
    iget-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 413
    iget-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getWidth()I

    move-result v1

    .line 414
    iget-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getHeight()I

    move-result v0

    .line 416
    :goto_2
    if-eqz v2, :cond_b

    .line 417
    iget-object v3, p0, Llec;->s:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 418
    invoke-direct {p0, v1, v0}, Llec;->a(II)V

    .line 420
    :cond_a
    iget-object v0, p0, Llec;->s:Landroid/graphics/Rect;

    iget-object v1, p0, Llec;->q:Landroid/graphics/Rect;

    sget-object v3, Llec;->a:Llct;

    iget-object v3, v3, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 421
    iget-object v0, p0, Llec;->r:Landroid/graphics/Rect;

    sget-object v1, Llec;->a:Llct;

    iget-object v1, v1, Llct;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 424
    :cond_b
    invoke-virtual {p0}, Llec;->getWidth()I

    move-result v9

    .line 425
    invoke-virtual {p0}, Llec;->getHeight()I

    move-result v10

    .line 427
    sget-object v0, Llec;->a:Llct;

    iget v6, v0, Llct;->m:I

    .line 428
    sget-object v0, Llec;->a:Llct;

    iget v7, v0, Llct;->m:I

    .line 431
    sget-object v0, Llec;->a:Llct;

    iget-object v0, v0, Llct;->w:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v2, v0, v1

    .line 432
    int-to-float v1, v6

    sub-int v0, v9, v6

    int-to-float v3, v0

    sget-object v0, Llec;->a:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 434
    iget-object v0, p0, Llec;->p:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_11

    .line 435
    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->m:I

    iget v1, p0, Llec;->t:I

    add-int/2addr v0, v1

    add-int/2addr v0, v6

    .line 438
    :goto_3
    iget-object v1, p0, Llec;->h:Landroid/text/StaticLayout;

    if-eqz v1, :cond_10

    .line 439
    int-to-float v1, v0

    int-to-float v2, v7

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 440
    iget-object v1, p0, Llec;->h:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 441
    neg-int v1, v0

    int-to-float v1, v1

    neg-int v2, v7

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 442
    iget-object v1, p0, Llec;->h:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget-object v2, Llec;->a:Llct;

    iget v2, v2, Llct;->m:I

    add-int/2addr v1, v2

    add-int/2addr v1, v7

    .line 445
    :goto_4
    iget-object v2, p0, Llec;->i:Landroid/text/StaticLayout;

    if-eqz v2, :cond_c

    .line 446
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 447
    iget-object v2, p0, Llec;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 448
    neg-int v2, v0

    int-to-float v2, v2

    neg-int v3, v1

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 449
    iget-object v2, p0, Llec;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Llec;->a:Llct;

    iget v3, v3, Llct;->m:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 452
    :cond_c
    iget-object v2, p0, Llec;->n:Landroid/text/StaticLayout;

    if-eqz v2, :cond_d

    .line 453
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 454
    iget-object v2, p0, Llec;->n:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 455
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 456
    iget-object v0, p0, Llec;->n:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->m:I

    .line 459
    :cond_d
    iget-object v0, p0, Llec;->g:Lled;

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Llec;->isPressed()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Llec;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 460
    :cond_e
    sget-object v0, Llec;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8, v8, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 461
    sget-object v0, Llec;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 464
    :cond_f
    iget-object v0, p0, Llec;->F:Ljaz;

    if-eqz v0, :cond_8

    .line 465
    iget-object v0, p0, Llec;->F:Ljaz;

    iget v1, p0, Llec;->x:I

    invoke-interface {v0, p1, v9, v1}, Ljaz;->a(Landroid/graphics/Canvas;II)V

    goto/16 :goto_1

    :cond_10
    move v1, v7

    goto :goto_4

    :cond_11
    move v0, v6

    goto/16 :goto_3

    :cond_12
    move v1, v8

    move-object v2, v0

    move v0, v8

    goto/16 :goto_2

    :cond_13
    move v1, v8

    move-object v2, v0

    move v0, v8

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 297
    iget-object v0, p0, Llec;->j:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llec;->j:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 298
    iget-object v0, p0, Llec;->j:Landroid/widget/Button;

    iget v1, p0, Llec;->k:I

    iget v2, p0, Llec;->l:I

    iget v3, p0, Llec;->k:I

    iget-object v4, p0, Llec;->j:Landroid/widget/Button;

    .line 299
    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Llec;->l:I

    iget-object v5, p0, Llec;->j:Landroid/widget/Button;

    .line 300
    invoke-virtual {v5}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 298
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Button;->layout(IIII)V

    .line 302
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    .line 306
    iget-boolean v0, p0, Llec;->D:Z

    if-eqz v0, :cond_9

    .line 307
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->bl:I

    sub-int v0, v9, v0

    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->bm:I

    sub-int v3, v0, v1

    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v0, p0, Llec;->f:Lkzs;

    if-eqz v0, :cond_2

    iget-object v0, p0, Llec;->j:Landroid/widget/Button;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v2, v0

    :goto_0
    const/4 v1, 0x0

    if-eqz v2, :cond_3

    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->X:I

    :goto_1
    iget-object v5, p0, Llec;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_16

    const/16 v1, 0x14

    invoke-static {v4, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    iget-object v5, p0, Llec;->b:Ljava/lang/String;

    invoke-static {v1, v5, v3, v0}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Llec;->h:Landroid/text/StaticLayout;

    iget-object v0, p0, Llec;->h:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->bj:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    :goto_2
    iget-object v1, p0, Llec;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v5, 0x5

    invoke-static {v1, v5}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    iget-object v5, p0, Llec;->m:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-static {v1, v5, v3, v6}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Llec;->n:Landroid/text/StaticLayout;

    iget-object v1, p0, Llec;->n:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget-object v5, Llec;->a:Llct;

    iget v5, v5, Llct;->bn:I

    add-int/2addr v1, v5

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Llec;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->bp:I

    const/16 v5, 0xa

    invoke-static {v4, v5}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    iget-object v6, p0, Llec;->d:Ljava/lang/String;

    invoke-static {v5, v6, v3, v1}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Llec;->i:Landroid/text/StaticLayout;

    iget-object v1, p0, Llec;->i:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget-object v5, Llec;->a:Llct;

    iget v5, v5, Llct;->bj:I

    add-int/2addr v1, v5

    add-int/2addr v0, v1

    :goto_3
    if-eqz v2, :cond_15

    iget-object v1, p0, Llec;->j:Landroid/widget/Button;

    iget-object v2, p0, Llec;->f:Lkzs;

    invoke-virtual {v2, v4}, Lkzs;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Llec;->j:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setMaxWidth(I)V

    iget-object v1, p0, Llec;->j:Landroid/widget/Button;

    const/high16 v2, -0x80000000

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/Button;->measure(II)V

    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Llec;->t:I

    add-int/2addr v1, v2

    iput v1, p0, Llec;->k:I

    iput v0, p0, Llec;->l:I

    iget-object v1, p0, Llec;->j:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v1

    sget-object v2, Llec;->a:Llct;

    iget v2, v2, Llct;->m:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    move v8, v0

    :goto_4
    iget v0, p0, Llec;->u:I

    if-eqz v0, :cond_1

    iget v0, p0, Llec;->v:I

    if-nez v0, :cond_5

    :cond_1
    invoke-virtual {p0, v9, v8}, Llec;->setMeasuredDimension(II)V

    .line 399
    :goto_5
    return-void

    .line 307
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_0

    :cond_3
    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->bo:I

    goto/16 :goto_1

    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, Llec;->i:Landroid/text/StaticLayout;

    goto :goto_3

    :cond_5
    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->bk:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v9, v0

    iget v1, p0, Llec;->v:I

    mul-int/2addr v1, v0

    iget v2, p0, Llec;->u:I

    div-int/2addr v1, v2

    iget v2, p0, Llec;->w:I

    if-ne v0, v2, :cond_6

    iget v2, p0, Llec;->x:I

    if-ne v1, v2, :cond_6

    iget-object v2, p0, Llec;->B:Landroid/graphics/Path;

    if-eqz v2, :cond_6

    iget-object v2, p0, Llec;->A:Landroid/graphics/Paint;

    if-nez v2, :cond_8

    :cond_6
    iput v0, p0, Llec;->w:I

    iput v1, p0, Llec;->x:I

    iget v2, p0, Llec;->w:I

    iget v0, p0, Llec;->x:I

    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->bk:I

    add-int v3, v0, v1

    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->bk:I

    int-to-float v0, v0

    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->bk:I

    int-to-float v1, v1

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->bk:I

    int-to-float v0, v0

    int-to-float v1, v3

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    sget-object v0, Llec;->a:Llct;

    iget v1, v0, Llct;->bk:I

    sget-object v0, Llec;->a:Llct;

    iget v5, v0, Llct;->bg:I

    const/4 v0, 0x0

    :goto_6
    shl-int/lit8 v6, v5, 0x1

    div-int v6, v2, v6

    if-gt v0, v6, :cond_7

    add-int/2addr v1, v5

    int-to-float v6, v1

    sub-int v7, v3, v5

    int-to-float v7, v7

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/2addr v1, v5

    int-to-float v6, v1

    int-to-float v7, v3

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_7
    int-to-float v0, v1

    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->bk:I

    int-to-float v1, v1

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    iput-object v4, p0, Llec;->B:Landroid/graphics/Path;

    iget v3, p0, Llec;->w:I

    iget v4, p0, Llec;->x:I

    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v1, v3

    int-to-float v2, v4

    int-to-float v3, v3

    sget-object v5, Llec;->a:Llct;

    iget v5, v5, Llct;->bj:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    sget-object v5, Llec;->a:Llct;

    iget v5, v5, Llct;->bh:I

    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iput-object v1, p0, Llec;->A:Landroid/graphics/Paint;

    invoke-virtual {p0}, Llec;->b()V

    :cond_8
    iget-object v0, p0, Llec;->q:Landroid/graphics/Rect;

    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->bk:I

    sget-object v2, Llec;->a:Llct;

    iget v2, v2, Llct;->bk:I

    sget-object v3, Llec;->a:Llct;

    iget v3, v3, Llct;->bk:I

    iget v4, p0, Llec;->w:I

    add-int/2addr v3, v4

    sget-object v4, Llec;->a:Llct;

    iget v4, v4, Llct;->bk:I

    iget v5, p0, Llec;->x:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->bj:I

    iget v1, p0, Llec;->x:I

    add-int/2addr v0, v1

    add-int/2addr v0, v8

    sget-object v1, Llec;->a:Llct;

    iget-object v1, v1, Llct;->v:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Llec;->r:Landroid/graphics/Rect;

    iget-object v3, p0, Llec;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Llec;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Llec;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int v1, v0, v1

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p0, v9, v0}, Llec;->setMeasuredDimension(II)V

    goto/16 :goto_5

    .line 310
    :cond_9
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 311
    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v6, v0

    .line 313
    iget v1, p0, Llec;->t:I

    if-lez v1, :cond_a

    .line 314
    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->m:I

    iget v2, p0, Llec;->t:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 317
    :cond_a
    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 319
    iget-object v1, p0, Llec;->f:Lkzs;

    if-eqz v1, :cond_f

    iget-object v1, p0, Llec;->j:Landroid/widget/Button;

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    .line 320
    :goto_7
    iget-object v2, p0, Llec;->m:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    const/4 v2, 0x1

    move v5, v2

    .line 322
    :goto_8
    if-eqz v1, :cond_11

    sget-object v2, Llec;->a:Llct;

    iget v2, v2, Llct;->X:I

    .line 325
    :goto_9
    sget-object v3, Llec;->a:Llct;

    iget v3, v3, Llct;->m:I

    mul-int/lit8 v3, v3, 0x2

    .line 328
    iget-object v4, p0, Llec;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 329
    const/16 v4, 0x1a

    .line 330
    invoke-static {v7, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    .line 331
    iget-object v8, p0, Llec;->b:Ljava/lang/String;

    invoke-static {v4, v8, v0, v2}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v4

    iput-object v4, p0, Llec;->h:Landroid/text/StaticLayout;

    .line 333
    iget-object v4, p0, Llec;->h:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    sget-object v8, Llec;->a:Llct;

    iget v8, v8, Llct;->m:I

    add-int/2addr v4, v8

    add-int/2addr v3, v4

    .line 338
    :cond_b
    sget-object v4, Llec;->a:Llct;

    iget v4, v4, Llct;->W:I

    .line 339
    iget-object v8, p0, Llec;->h:Landroid/text/StaticLayout;

    if-eqz v8, :cond_14

    iget-object v8, p0, Llec;->h:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v8

    if-ne v2, v8, :cond_14

    .line 340
    const/4 v2, 0x1

    add-int/lit8 v4, v4, -0x1

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 344
    :goto_a
    iget-object v4, p0, Llec;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 345
    const/4 v4, 0x7

    .line 346
    invoke-static {v7, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    .line 347
    iget-object v8, p0, Llec;->d:Ljava/lang/String;

    invoke-static {v4, v8, v0, v2}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Llec;->i:Landroid/text/StaticLayout;

    .line 349
    iget-object v2, p0, Llec;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v4, Llec;->a:Llct;

    iget v4, v4, Llct;->m:I

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    .line 354
    :goto_b
    if-eqz v5, :cond_c

    .line 356
    invoke-virtual {p0}, Llec;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0xa

    invoke-static {v3, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    .line 357
    iget-object v4, p0, Llec;->m:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v3, v4, v0, v5}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v3

    iput-object v3, p0, Llec;->n:Landroid/text/StaticLayout;

    .line 359
    iget-object v3, p0, Llec;->n:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget-object v4, Llec;->a:Llct;

    iget v4, v4, Llct;->m:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 362
    :cond_c
    if-eqz v1, :cond_13

    .line 363
    iget-object v1, p0, Llec;->j:Landroid/widget/Button;

    iget-object v3, p0, Llec;->f:Lkzs;

    invoke-virtual {v3, v7}, Lkzs;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v1, p0, Llec;->j:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setMaxWidth(I)V

    .line 365
    iget-object v1, p0, Llec;->j:Landroid/widget/Button;

    const/high16 v3, -0x80000000

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 366
    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 365
    invoke-virtual {v1, v0, v3}, Landroid/widget/Button;->measure(II)V

    .line 367
    sget-object v0, Llec;->a:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    iget v1, p0, Llec;->t:I

    add-int/2addr v0, v1

    iput v0, p0, Llec;->k:I

    .line 368
    iput v2, p0, Llec;->l:I

    .line 369
    iget-object v0, p0, Llec;->j:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v0

    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 372
    :goto_c
    iget v1, p0, Llec;->t:I

    if-nez v1, :cond_d

    .line 373
    sget-object v1, Llec;->a:Llct;

    iget v1, v1, Llct;->ae:I

    sget-object v2, Llec;->a:Llct;

    iget v2, v2, Llct;->m:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 375
    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    sget-object v2, Llec;->a:Llct;

    iget v2, v2, Llct;->m:I

    .line 376
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Llec;->E:I

    .line 378
    iget-object v1, p0, Llec;->j:Landroid/widget/Button;

    if-eqz v1, :cond_d

    .line 379
    iget v1, p0, Llec;->l:I

    iget v2, p0, Llec;->E:I

    add-int/2addr v1, v2

    iput v1, p0, Llec;->l:I

    .line 383
    :cond_d
    iget v1, p0, Llec;->E:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Llec;->t:I

    sget-object v2, Llec;->a:Llct;

    iget v2, v2, Llct;->m:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 386
    iget v1, p0, Llec;->t:I

    if-lez v1, :cond_e

    .line 387
    iget-object v1, p0, Llec;->q:Landroid/graphics/Rect;

    sget-object v2, Llec;->a:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Llec;->a:Llct;

    iget v3, v3, Llct;->m:I

    sget-object v4, Llec;->a:Llct;

    iget v4, v4, Llct;->m:I

    iget v5, p0, Llec;->t:I

    add-int/2addr v4, v5

    sget-object v5, Llec;->a:Llct;

    iget v5, v5, Llct;->m:I

    iget v7, p0, Llec;->t:I

    add-int/2addr v5, v7

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 391
    sget-object v1, Llec;->a:Llct;

    iget-object v1, v1, Llct;->v:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    float-to-int v1, v1

    .line 392
    iget-object v2, p0, Llec;->r:Landroid/graphics/Rect;

    iget-object v3, p0, Llec;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iget-object v4, p0, Llec;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v1

    iget-object v5, p0, Llec;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v1

    iget-object v7, p0, Llec;->q:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    sub-int v1, v7, v1

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 398
    :cond_e
    invoke-virtual {p0, v6, v0}, Llec;->setMeasuredDimension(II)V

    goto/16 :goto_5

    .line 319
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_7

    .line 320
    :cond_10
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_8

    .line 322
    :cond_11
    sget-object v2, Llec;->a:Llct;

    iget v2, v2, Llct;->V:I

    goto/16 :goto_9

    .line 351
    :cond_12
    const/4 v2, 0x0

    iput-object v2, p0, Llec;->i:Landroid/text/StaticLayout;

    move v2, v3

    goto/16 :goto_b

    :cond_13
    move v0, v2

    goto/16 :goto_c

    :cond_14
    move v2, v4

    goto/16 :goto_a

    :cond_15
    move v8, v0

    goto/16 :goto_4

    :cond_16
    move v0, v1

    goto/16 :goto_2
.end method
