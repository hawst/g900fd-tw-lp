.class public final Lmbk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvg;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7531
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7534
    iput-object v0, p0, Lmbk;->apiHeader:Llyq;

    .line 7537
    iput-object v0, p0, Lmbk;->a:Lnvg;

    .line 7531
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7554
    const/4 v0, 0x0

    .line 7555
    iget-object v1, p0, Lmbk;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 7556
    const/4 v0, 0x1

    iget-object v1, p0, Lmbk;->apiHeader:Llyq;

    .line 7557
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7559
    :cond_0
    iget-object v1, p0, Lmbk;->a:Lnvg;

    if-eqz v1, :cond_1

    .line 7560
    const/4 v1, 0x2

    iget-object v2, p0, Lmbk;->a:Lnvg;

    .line 7561
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7563
    :cond_1
    iget-object v1, p0, Lmbk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7564
    iput v0, p0, Lmbk;->ai:I

    .line 7565
    return v0
.end method

.method public a(Loxn;)Lmbk;
    .locals 2

    .prologue
    .line 7573
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7574
    sparse-switch v0, :sswitch_data_0

    .line 7578
    iget-object v1, p0, Lmbk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7579
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbk;->ah:Ljava/util/List;

    .line 7582
    :cond_1
    iget-object v1, p0, Lmbk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7584
    :sswitch_0
    return-object p0

    .line 7589
    :sswitch_1
    iget-object v0, p0, Lmbk;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 7590
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmbk;->apiHeader:Llyq;

    .line 7592
    :cond_2
    iget-object v0, p0, Lmbk;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7596
    :sswitch_2
    iget-object v0, p0, Lmbk;->a:Lnvg;

    if-nez v0, :cond_3

    .line 7597
    new-instance v0, Lnvg;

    invoke-direct {v0}, Lnvg;-><init>()V

    iput-object v0, p0, Lmbk;->a:Lnvg;

    .line 7599
    :cond_3
    iget-object v0, p0, Lmbk;->a:Lnvg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7574
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7542
    iget-object v0, p0, Lmbk;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 7543
    const/4 v0, 0x1

    iget-object v1, p0, Lmbk;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7545
    :cond_0
    iget-object v0, p0, Lmbk;->a:Lnvg;

    if-eqz v0, :cond_1

    .line 7546
    const/4 v0, 0x2

    iget-object v1, p0, Lmbk;->a:Lnvg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7548
    :cond_1
    iget-object v0, p0, Lmbk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7550
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7527
    invoke-virtual {p0, p1}, Lmbk;->a(Loxn;)Lmbk;

    move-result-object v0

    return-object v0
.end method
