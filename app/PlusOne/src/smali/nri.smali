.class public final Lnri;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnri;


# instance fields
.field public b:I

.field public c:[Lnrp;

.field public d:Lohl;

.field public e:Lnrm;

.field public f:Lnrg;

.field private g:[Llxo;

.field private h:Ljava/lang/String;

.field private i:Lnrd;

.field private j:Lnro;

.field private k:Lnrf;

.field private l:Ljava/lang/String;

.field private m:Lnrh;

.field private n:Lnre;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 358
    const/4 v0, 0x0

    new-array v0, v0, [Lnri;

    sput-object v0, Lnri;->a:[Lnri;

    .line 362
    const v0, 0x3039d0a

    new-instance v1, Lnrj;

    invoke-direct {v1}, Lnrj;-><init>()V

    .line 363
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 362
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 359
    invoke-direct {p0}, Loxq;-><init>()V

    .line 366
    const/high16 v0, -0x80000000

    iput v0, p0, Lnri;->b:I

    .line 369
    sget-object v0, Lnrp;->a:[Lnrp;

    iput-object v0, p0, Lnri;->c:[Lnrp;

    .line 372
    sget-object v0, Llxo;->a:[Llxo;

    iput-object v0, p0, Lnri;->g:[Llxo;

    .line 375
    iput-object v1, p0, Lnri;->d:Lohl;

    .line 380
    iput-object v1, p0, Lnri;->i:Lnrd;

    .line 383
    iput-object v1, p0, Lnri;->j:Lnro;

    .line 386
    iput-object v1, p0, Lnri;->k:Lnrf;

    .line 391
    iput-object v1, p0, Lnri;->m:Lnrh;

    .line 394
    iput-object v1, p0, Lnri;->n:Lnre;

    .line 397
    iput-object v1, p0, Lnri;->e:Lnrm;

    .line 400
    iput-object v1, p0, Lnri;->f:Lnrg;

    .line 359
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 458
    .line 459
    iget v0, p0, Lnri;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_e

    .line 460
    const/4 v0, 0x1

    iget v2, p0, Lnri;->b:I

    .line 461
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 463
    :goto_0
    iget-object v2, p0, Lnri;->c:[Lnrp;

    if-eqz v2, :cond_1

    .line 464
    iget-object v3, p0, Lnri;->c:[Lnrp;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 465
    if-eqz v5, :cond_0

    .line 466
    const/4 v6, 0x2

    .line 467
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 464
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 471
    :cond_1
    iget-object v2, p0, Lnri;->d:Lohl;

    if-eqz v2, :cond_2

    .line 472
    const/4 v2, 0x3

    iget-object v3, p0, Lnri;->d:Lohl;

    .line 473
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 475
    :cond_2
    iget-object v2, p0, Lnri;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 476
    const/4 v2, 0x6

    iget-object v3, p0, Lnri;->h:Ljava/lang/String;

    .line 477
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 479
    :cond_3
    iget-object v2, p0, Lnri;->i:Lnrd;

    if-eqz v2, :cond_4

    .line 480
    const/16 v2, 0x8

    iget-object v3, p0, Lnri;->i:Lnrd;

    .line 481
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 483
    :cond_4
    iget-object v2, p0, Lnri;->j:Lnro;

    if-eqz v2, :cond_5

    .line 484
    const/16 v2, 0x9

    iget-object v3, p0, Lnri;->j:Lnro;

    .line 485
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 487
    :cond_5
    iget-object v2, p0, Lnri;->k:Lnrf;

    if-eqz v2, :cond_6

    .line 488
    const/16 v2, 0xa

    iget-object v3, p0, Lnri;->k:Lnrf;

    .line 489
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 491
    :cond_6
    iget-object v2, p0, Lnri;->l:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 492
    const/16 v2, 0xb

    iget-object v3, p0, Lnri;->l:Ljava/lang/String;

    .line 493
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 495
    :cond_7
    iget-object v2, p0, Lnri;->m:Lnrh;

    if-eqz v2, :cond_8

    .line 496
    const/16 v2, 0xc

    iget-object v3, p0, Lnri;->m:Lnrh;

    .line 497
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 499
    :cond_8
    iget-object v2, p0, Lnri;->n:Lnre;

    if-eqz v2, :cond_9

    .line 500
    const/16 v2, 0xd

    iget-object v3, p0, Lnri;->n:Lnre;

    .line 501
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 503
    :cond_9
    iget-object v2, p0, Lnri;->e:Lnrm;

    if-eqz v2, :cond_a

    .line 504
    const/16 v2, 0xf

    iget-object v3, p0, Lnri;->e:Lnrm;

    .line 505
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 507
    :cond_a
    iget-object v2, p0, Lnri;->f:Lnrg;

    if-eqz v2, :cond_b

    .line 508
    const/16 v2, 0x10

    iget-object v3, p0, Lnri;->f:Lnrg;

    .line 509
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 511
    :cond_b
    iget-object v2, p0, Lnri;->g:[Llxo;

    if-eqz v2, :cond_d

    .line 512
    iget-object v2, p0, Lnri;->g:[Llxo;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    .line 513
    if-eqz v4, :cond_c

    .line 514
    const/16 v5, 0x11

    .line 515
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 512
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 519
    :cond_d
    iget-object v1, p0, Lnri;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 520
    iput v0, p0, Lnri;->ai:I

    .line 521
    return v0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnri;
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 529
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 530
    sparse-switch v0, :sswitch_data_0

    .line 534
    iget-object v2, p0, Lnri;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 535
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnri;->ah:Ljava/util/List;

    .line 538
    :cond_1
    iget-object v2, p0, Lnri;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 540
    :sswitch_0
    return-object p0

    .line 545
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 546
    if-eq v0, v4, :cond_2

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd

    if-eq v0, v2, :cond_2

    const/16 v2, 0xe

    if-eq v0, v2, :cond_2

    const/16 v2, 0xf

    if-ne v0, v2, :cond_3

    .line 563
    :cond_2
    iput v0, p0, Lnri;->b:I

    goto :goto_0

    .line 565
    :cond_3
    iput v4, p0, Lnri;->b:I

    goto :goto_0

    .line 570
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 571
    iget-object v0, p0, Lnri;->c:[Lnrp;

    if-nez v0, :cond_5

    move v0, v1

    .line 572
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnrp;

    .line 573
    iget-object v3, p0, Lnri;->c:[Lnrp;

    if-eqz v3, :cond_4

    .line 574
    iget-object v3, p0, Lnri;->c:[Lnrp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 576
    :cond_4
    iput-object v2, p0, Lnri;->c:[Lnrp;

    .line 577
    :goto_2
    iget-object v2, p0, Lnri;->c:[Lnrp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 578
    iget-object v2, p0, Lnri;->c:[Lnrp;

    new-instance v3, Lnrp;

    invoke-direct {v3}, Lnrp;-><init>()V

    aput-object v3, v2, v0

    .line 579
    iget-object v2, p0, Lnri;->c:[Lnrp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 580
    invoke-virtual {p1}, Loxn;->a()I

    .line 577
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 571
    :cond_5
    iget-object v0, p0, Lnri;->c:[Lnrp;

    array-length v0, v0

    goto :goto_1

    .line 583
    :cond_6
    iget-object v2, p0, Lnri;->c:[Lnrp;

    new-instance v3, Lnrp;

    invoke-direct {v3}, Lnrp;-><init>()V

    aput-object v3, v2, v0

    .line 584
    iget-object v2, p0, Lnri;->c:[Lnrp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 588
    :sswitch_3
    iget-object v0, p0, Lnri;->d:Lohl;

    if-nez v0, :cond_7

    .line 589
    new-instance v0, Lohl;

    invoke-direct {v0}, Lohl;-><init>()V

    iput-object v0, p0, Lnri;->d:Lohl;

    .line 591
    :cond_7
    iget-object v0, p0, Lnri;->d:Lohl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 595
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnri;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 599
    :sswitch_5
    iget-object v0, p0, Lnri;->i:Lnrd;

    if-nez v0, :cond_8

    .line 600
    new-instance v0, Lnrd;

    invoke-direct {v0}, Lnrd;-><init>()V

    iput-object v0, p0, Lnri;->i:Lnrd;

    .line 602
    :cond_8
    iget-object v0, p0, Lnri;->i:Lnrd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 606
    :sswitch_6
    iget-object v0, p0, Lnri;->j:Lnro;

    if-nez v0, :cond_9

    .line 607
    new-instance v0, Lnro;

    invoke-direct {v0}, Lnro;-><init>()V

    iput-object v0, p0, Lnri;->j:Lnro;

    .line 609
    :cond_9
    iget-object v0, p0, Lnri;->j:Lnro;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 613
    :sswitch_7
    iget-object v0, p0, Lnri;->k:Lnrf;

    if-nez v0, :cond_a

    .line 614
    new-instance v0, Lnrf;

    invoke-direct {v0}, Lnrf;-><init>()V

    iput-object v0, p0, Lnri;->k:Lnrf;

    .line 616
    :cond_a
    iget-object v0, p0, Lnri;->k:Lnrf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 620
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnri;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 624
    :sswitch_9
    iget-object v0, p0, Lnri;->m:Lnrh;

    if-nez v0, :cond_b

    .line 625
    new-instance v0, Lnrh;

    invoke-direct {v0}, Lnrh;-><init>()V

    iput-object v0, p0, Lnri;->m:Lnrh;

    .line 627
    :cond_b
    iget-object v0, p0, Lnri;->m:Lnrh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 631
    :sswitch_a
    iget-object v0, p0, Lnri;->n:Lnre;

    if-nez v0, :cond_c

    .line 632
    new-instance v0, Lnre;

    invoke-direct {v0}, Lnre;-><init>()V

    iput-object v0, p0, Lnri;->n:Lnre;

    .line 634
    :cond_c
    iget-object v0, p0, Lnri;->n:Lnre;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 638
    :sswitch_b
    iget-object v0, p0, Lnri;->e:Lnrm;

    if-nez v0, :cond_d

    .line 639
    new-instance v0, Lnrm;

    invoke-direct {v0}, Lnrm;-><init>()V

    iput-object v0, p0, Lnri;->e:Lnrm;

    .line 641
    :cond_d
    iget-object v0, p0, Lnri;->e:Lnrm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 645
    :sswitch_c
    iget-object v0, p0, Lnri;->f:Lnrg;

    if-nez v0, :cond_e

    .line 646
    new-instance v0, Lnrg;

    invoke-direct {v0}, Lnrg;-><init>()V

    iput-object v0, p0, Lnri;->f:Lnrg;

    .line 648
    :cond_e
    iget-object v0, p0, Lnri;->f:Lnrg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 652
    :sswitch_d
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 653
    iget-object v0, p0, Lnri;->g:[Llxo;

    if-nez v0, :cond_10

    move v0, v1

    .line 654
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Llxo;

    .line 655
    iget-object v3, p0, Lnri;->g:[Llxo;

    if-eqz v3, :cond_f

    .line 656
    iget-object v3, p0, Lnri;->g:[Llxo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 658
    :cond_f
    iput-object v2, p0, Lnri;->g:[Llxo;

    .line 659
    :goto_4
    iget-object v2, p0, Lnri;->g:[Llxo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    .line 660
    iget-object v2, p0, Lnri;->g:[Llxo;

    new-instance v3, Llxo;

    invoke-direct {v3}, Llxo;-><init>()V

    aput-object v3, v2, v0

    .line 661
    iget-object v2, p0, Lnri;->g:[Llxo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 662
    invoke-virtual {p1}, Loxn;->a()I

    .line 659
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 653
    :cond_10
    iget-object v0, p0, Lnri;->g:[Llxo;

    array-length v0, v0

    goto :goto_3

    .line 665
    :cond_11
    iget-object v2, p0, Lnri;->g:[Llxo;

    new-instance v3, Llxo;

    invoke-direct {v3}, Llxo;-><init>()V

    aput-object v3, v2, v0

    .line 666
    iget-object v2, p0, Lnri;->g:[Llxo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 530
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x32 -> :sswitch_4
        0x42 -> :sswitch_5
        0x4a -> :sswitch_6
        0x52 -> :sswitch_7
        0x5a -> :sswitch_8
        0x62 -> :sswitch_9
        0x6a -> :sswitch_a
        0x7a -> :sswitch_b
        0x82 -> :sswitch_c
        0x8a -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 405
    iget v1, p0, Lnri;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 406
    const/4 v1, 0x1

    iget v2, p0, Lnri;->b:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 408
    :cond_0
    iget-object v1, p0, Lnri;->c:[Lnrp;

    if-eqz v1, :cond_2

    .line 409
    iget-object v2, p0, Lnri;->c:[Lnrp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 410
    if-eqz v4, :cond_1

    .line 411
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 409
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 415
    :cond_2
    iget-object v1, p0, Lnri;->d:Lohl;

    if-eqz v1, :cond_3

    .line 416
    const/4 v1, 0x3

    iget-object v2, p0, Lnri;->d:Lohl;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 418
    :cond_3
    iget-object v1, p0, Lnri;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 419
    const/4 v1, 0x6

    iget-object v2, p0, Lnri;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 421
    :cond_4
    iget-object v1, p0, Lnri;->i:Lnrd;

    if-eqz v1, :cond_5

    .line 422
    const/16 v1, 0x8

    iget-object v2, p0, Lnri;->i:Lnrd;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 424
    :cond_5
    iget-object v1, p0, Lnri;->j:Lnro;

    if-eqz v1, :cond_6

    .line 425
    const/16 v1, 0x9

    iget-object v2, p0, Lnri;->j:Lnro;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 427
    :cond_6
    iget-object v1, p0, Lnri;->k:Lnrf;

    if-eqz v1, :cond_7

    .line 428
    const/16 v1, 0xa

    iget-object v2, p0, Lnri;->k:Lnrf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 430
    :cond_7
    iget-object v1, p0, Lnri;->l:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 431
    const/16 v1, 0xb

    iget-object v2, p0, Lnri;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 433
    :cond_8
    iget-object v1, p0, Lnri;->m:Lnrh;

    if-eqz v1, :cond_9

    .line 434
    const/16 v1, 0xc

    iget-object v2, p0, Lnri;->m:Lnrh;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 436
    :cond_9
    iget-object v1, p0, Lnri;->n:Lnre;

    if-eqz v1, :cond_a

    .line 437
    const/16 v1, 0xd

    iget-object v2, p0, Lnri;->n:Lnre;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 439
    :cond_a
    iget-object v1, p0, Lnri;->e:Lnrm;

    if-eqz v1, :cond_b

    .line 440
    const/16 v1, 0xf

    iget-object v2, p0, Lnri;->e:Lnrm;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 442
    :cond_b
    iget-object v1, p0, Lnri;->f:Lnrg;

    if-eqz v1, :cond_c

    .line 443
    const/16 v1, 0x10

    iget-object v2, p0, Lnri;->f:Lnrg;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 445
    :cond_c
    iget-object v1, p0, Lnri;->g:[Llxo;

    if-eqz v1, :cond_e

    .line 446
    iget-object v1, p0, Lnri;->g:[Llxo;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 447
    if-eqz v3, :cond_d

    .line 448
    const/16 v4, 0x11

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 446
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 452
    :cond_e
    iget-object v0, p0, Lnri;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 454
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 355
    invoke-virtual {p0, p1}, Lnri;->a(Loxn;)Lnri;

    move-result-object v0

    return-object v0
.end method
