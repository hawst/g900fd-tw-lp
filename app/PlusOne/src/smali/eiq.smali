.class public final Leiq;
.super Llol;
.source "PG"

# interfaces
.implements Lgae;
.implements Lgaf;
.implements Lhjj;
.implements Lhmq;
.implements Lllm;


# static fields
.field private static N:Ljava/lang/Integer;

.field private static O:I

.field private static P:I

.field private static Q:I

.field private static R:I


# instance fields
.field private final S:Lhje;

.field private T:Lhee;

.field private U:I

.field private V:Lizu;

.field private W:Lcom/google/android/apps/plus/views/PhotoView;

.field private X:Lcom/google/android/apps/plus/views/PhotoCropOverlay;

.field private Y:Z

.field private Z:Z

.field private aa:Z

.field private ab:I

.field private ac:I

.field private ad:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Llol;-><init>()V

    .line 94
    new-instance v0, Lllo;

    iget-object v1, p0, Leiq;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lllo;-><init>(Llqr;Lllm;)V

    .line 97
    new-instance v0, Lhje;

    iget-object v1, p0, Leiq;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Leiq;->S:Lhje;

    .line 333
    return-void
.end method

.method static synthetic a(Leiq;I)I
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Leiq;->ab:I

    return p1
.end method

.method static synthetic a(Leiq;)Lcom/google/android/apps/plus/views/PhotoView;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Leiq;->W:Lcom/google/android/apps/plus/views/PhotoView;

    return-object v0
.end method

.method private a(II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 220
    iget v0, p0, Leiq;->ac:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Leiq;->ad:I

    if-ge p2, v0, :cond_1

    .line 221
    :cond_0
    invoke-virtual {p0}, Leiq;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0ada

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Leiq;->ac:I

    .line 222
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget v4, p0, Leiq;->ad:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 221
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 223
    invoke-virtual {p0}, Leiq;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 225
    invoke-virtual {p0}, Leiq;->n()Lz;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 226
    invoke-virtual {p0}, Leiq;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 228
    :cond_1
    return-void
.end method

.method static synthetic a(Leiq;II)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Leiq;->a(II)V

    return-void
.end method

.method static synthetic a(Leiq;Z)Z
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Leiq;->Z:Z

    return p1
.end method

.method static synthetic b(Leiq;)Llnl;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Leiq;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Leiq;)Lhee;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Leiq;->T:Lhee;

    return-object v0
.end method

.method static synthetic d(Leiq;)Lhje;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Leiq;->S:Lhje;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lhmw;->ah:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/16 v10, 0x1e

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 142
    const v0, 0x7f04018d

    invoke-virtual {p1, v0, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 145
    const v0, 0x7f1004a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoView;

    iput-object v0, p0, Leiq;->W:Lcom/google/android/apps/plus/views/PhotoView;

    .line 146
    iget-object v2, p0, Leiq;->W:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v3, p0, Leiq;->V:Lizu;

    const/4 v4, 0x0

    iget v0, p0, Leiq;->U:I

    .line 148
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown crop mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    new-instance v0, Lgad;

    sget v5, Leiq;->R:I

    sget v6, Leiq;->R:I

    const/16 v7, 0x322

    invoke-direct {v0, v5, v6, v7, v9}, Lgad;-><init>(IIIZ)V

    .line 146
    :goto_0
    invoke-virtual {v2, v3, v4, v0, p0}, Lcom/google/android/apps/plus/views/PhotoView;->a(Lizu;Lnzi;Lgad;Lgae;)V

    .line 150
    iget-object v0, p0, Leiq;->W:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoView;->a(Lgaf;)V

    .line 151
    iget-object v0, p0, Leiq;->W:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/PhotoView;->h(Z)V

    .line 153
    const v0, 0x7f1004c8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoCropOverlay;

    iput-object v0, p0, Leiq;->X:Lcom/google/android/apps/plus/views/PhotoCropOverlay;

    .line 154
    iget-object v0, p0, Leiq;->X:Lcom/google/android/apps/plus/views/PhotoCropOverlay;

    new-instance v2, Leir;

    invoke-direct {v2, p0}, Leir;-><init>(Leiq;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a(Lfzo;)V

    .line 160
    iget-object v2, p0, Leiq;->X:Lcom/google/android/apps/plus/views/PhotoCropOverlay;

    iget v0, p0, Leiq;->U:I

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown crop mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 148
    :pswitch_3
    new-instance v0, Lgad;

    const/16 v5, 0x3ac

    const/16 v6, 0x10

    invoke-direct {v0, v5, v9, v6, v9}, Lgad;-><init>(IIIZ)V

    goto :goto_0

    .line 160
    :pswitch_4
    sget v0, Leiq;->Q:I

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a(I)V

    .line 161
    iget-object v2, p0, Leiq;->X:Lcom/google/android/apps/plus/views/PhotoCropOverlay;

    iget v0, p0, Leiq;->U:I

    packed-switch v0, :pswitch_data_2

    :pswitch_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown crop mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 160
    :pswitch_6
    sget v0, Leiq;->P:I

    goto :goto_1

    .line 161
    :pswitch_7
    const v0, 0x3fe38e39

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a(F)V

    .line 163
    iget v0, p0, Leiq;->U:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    iget v0, p0, Leiq;->U:I

    if-ne v0, v8, :cond_1

    :cond_0
    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "rotation"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "rotation"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Leiq;->ab:I

    :cond_1
    iput-boolean v8, p0, Leiq;->Z:Z

    .line 165
    :goto_3
    return-object v1

    .line 161
    :pswitch_8
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_2

    .line 163
    :cond_2
    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "view_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "tile_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Leiq;->w()Lbb;

    move-result-object v0

    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Leis;

    invoke-direct {v3, p0}, Leis;-><init>(Leiq;)V

    invoke-virtual {v0, v8, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_3

    :cond_3
    iput-boolean v8, p0, Leiq;->Z:Z

    goto :goto_3

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 160
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 161
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public a(FF)V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Leiq;->X:Lcom/google/android/apps/plus/views/PhotoCropOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/views/PhotoCropOverlay;->a(FF)V

    .line 376
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 122
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 124
    sget-object v0, Leiq;->N:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 125
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 126
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 127
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 128
    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Leiq;->N:Ljava/lang/Integer;

    .line 130
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 132
    const v1, 0x7f0b02e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Leiq;->O:I

    .line 133
    const v1, 0x7f0d02a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Leiq;->P:I

    .line 134
    const v1, 0x7f0d02a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Leiq;->Q:I

    .line 135
    const v1, 0x7f0d0331

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Leiq;->R:I

    .line 137
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 314
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 316
    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 317
    const-string v0, "photo_min_width"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leiq;->ac:I

    .line 318
    const-string v0, "photo_min_height"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leiq;->ad:I

    .line 320
    const-string v0, "photo_ref"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    iput-object v0, p0, Leiq;->V:Lizu;

    .line 321
    const-string v0, "photo_picker_crop_mode"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leiq;->U:I

    .line 322
    return-void
.end method

.method public a(Lcom/google/android/apps/plus/views/PhotoView;Lkda;)V
    .locals 3

    .prologue
    .line 195
    instance-of v0, p2, Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leiq;->V:Lizu;

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 196
    check-cast v0, Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getWidth()I

    move-result v0

    check-cast p2, Lcom/google/android/libraries/social/media/MediaResource;

    .line 197
    invoke-virtual {p2}, Lcom/google/android/libraries/social/media/MediaResource;->getHeight()I

    move-result v1

    .line 196
    invoke-direct {p0, v0, v1}, Leiq;->a(II)V

    .line 200
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leiq;->Y:Z

    .line 201
    iget-object v0, p0, Leiq;->S:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 203
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoView;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Leiq;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leiq;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->em:Lhmv;

    .line 205
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 204
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 207
    :cond_1
    return-void
.end method

.method public a(Lhjk;)V
    .locals 2

    .prologue
    .line 241
    iget v0, p0, Leiq;->U:I

    if-eqz v0, :cond_2

    .line 242
    const v0, 0x7f0a0ab6

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 247
    :cond_0
    :goto_0
    iget-boolean v0, p0, Leiq;->aa:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Leiq;->Y:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Leiq;->Z:Z

    if-eqz v0, :cond_1

    .line 248
    const v0, 0x7f10068d

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 250
    :cond_1
    return-void

    .line 243
    :cond_2
    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 232
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0}, Leiq;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 370
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 13

    .prologue
    .line 254
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 255
    const v1, 0x7f10068d

    if-ne v0, v1, :cond_5

    .line 256
    invoke-virtual {p0}, Leiq;->n()Lz;

    move-result-object v2

    .line 257
    iget-object v0, p0, Leiq;->T:Lhee;

    .line 258
    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iget-object v1, p0, Leiq;->V:Lizu;

    .line 257
    invoke-static {v0, v1}, Legi;->a(ILizu;)Landroid/content/Intent;

    move-result-object v3

    .line 261
    iget v0, p0, Leiq;->U:I

    if-eqz v0, :cond_0

    .line 262
    const-string v0, "photo_picker_crop_mode"

    iget v1, p0, Leiq;->U:I

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 263
    const-string v0, "photo_picker_rotation"

    iget v1, p0, Leiq;->ab:I

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 265
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 266
    iget-object v0, p0, Leiq;->W:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/PhotoView;->a(Landroid/graphics/RectF;)V

    .line 271
    iget v0, p0, Leiq;->U:I

    packed-switch v0, :pswitch_data_0

    .line 292
    :goto_0
    :pswitch_0
    iget-object v0, p0, Leiq;->V:Lizu;

    invoke-virtual {v0}, Lizu;->b()Ljava/lang/String;

    move-result-object v0

    .line 293
    if-eqz v0, :cond_0

    const-string v1, "115239603441691718952"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    const-string v0, "is_gallery_photo"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 298
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {v2, v0, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 299
    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 300
    const/4 v0, 0x1

    .line 302
    :goto_1
    return v0

    .line 273
    :pswitch_1
    iget-object v0, p0, Leiq;->W:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView;->k()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 274
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    if-lez v1, :cond_1

    .line 275
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-gtz v0, :cond_2

    .line 276
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 279
    :cond_2
    iget-object v0, p0, Leiq;->W:Lcom/google/android/apps/plus/views/PhotoView;

    .line 280
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView;->k()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 279
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v0, v6

    float-to-int v0, v0

    const/16 v6, 0x4b0

    if-gt v1, v6, :cond_3

    const/16 v6, 0x4b0

    if-le v0, v6, :cond_4

    :cond_3
    const/16 v6, 0x4b0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v7

    div-int/2addr v6, v7

    int-to-float v6, v6

    int-to-float v1, v1

    mul-float/2addr v1, v6

    float-to-int v1, v1

    int-to-float v0, v0

    mul-float/2addr v0, v6

    float-to-int v0, v0

    :cond_4
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v8, Landroid/graphics/RectF;

    iget v9, v4, Landroid/graphics/RectF;->left:F

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v9, v10

    iget v10, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, v4, Landroid/graphics/RectF;->right:F

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v11, v12

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v4, v12

    invoke-direct {v8, v9, v10, v11, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    new-instance v9, Landroid/graphics/RectF;

    const/4 v10, 0x0

    const/4 v11, 0x0

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-direct {v9, v10, v11, v1, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v0, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v4, v8, v9, v0}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    sget v0, Leiq;->O:I

    invoke-virtual {v7, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    invoke-virtual {v7, v4}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    invoke-virtual {v5, v7}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 282
    const/16 v0, 0x5a

    const/4 v1, 0x0

    invoke-static {v6, v0, v1}, Llrw;->a(Landroid/graphics/Bitmap;IZ)[B

    move-result-object v0

    .line 284
    const-string v1, "data"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    goto/16 :goto_0

    .line 288
    :pswitch_2
    const-string v0, "coordinates"

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 302
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 271
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public aj_()V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 212
    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "coordinates"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v1, p0, Leiq;->W:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {p0}, Leiq;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "coordinates"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2, v4, v6}, Lcom/google/android/apps/plus/views/PhotoView;->a(FFF)F

    move-result v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v3, v4, v6}, Lcom/google/android/apps/plus/views/PhotoView;->a(FFF)F

    move-result v3

    iget v0, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v0, v4, v6}, Lcom/google/android/apps/plus/views/PhotoView;->a(FFF)F

    move-result v0

    iget-object v4, v1, Lcom/google/android/apps/plus/views/PhotoView;->c:Landroid/graphics/Matrix;

    iget-object v5, v1, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->getValues([F)V

    sub-float/2addr v0, v2

    div-float v0, v6, v0

    iget-object v4, v1, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v0, v4

    iget-object v4, v1, Lcom/google/android/apps/plus/views/PhotoView;->c:Landroid/graphics/Matrix;

    invoke-virtual {v4, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v4, v1, Lcom/google/android/apps/plus/views/PhotoView;->c:Landroid/graphics/Matrix;

    iget-object v5, v1, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    iget-object v6, v1, Lcom/google/android/apps/plus/views/PhotoView;->e:[F

    const/4 v7, 0x5

    aget v6, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v4, v1, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget-object v5, v1, Lcom/google/android/apps/plus/views/PhotoView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    int-to-float v4, v4

    mul-float/2addr v4, v0

    mul-float/2addr v2, v4

    int-to-float v4, v5

    mul-float/2addr v0, v4

    mul-float/2addr v0, v3

    iget-object v3, v1, Lcom/google/android/apps/plus/views/PhotoView;->c:Landroid/graphics/Matrix;

    neg-float v2, v2

    neg-float v0, v0

    invoke-virtual {v3, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    .line 215
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leiq;->aa:Z

    .line 216
    iget-object v0, p0, Leiq;->S:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 217
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 331
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 236
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 307
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 308
    iget-object v0, p0, Leiq;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 309
    iget-object v0, p0, Leiq;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Leiq;->T:Lhee;

    .line 310
    return-void
.end method
