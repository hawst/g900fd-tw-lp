.class public final Lnsb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lnsc;

.field public c:Lnsf;

.field public d:I

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Loxq;-><init>()V

    .line 625
    iput-object v0, p0, Lnsb;->b:Lnsc;

    .line 628
    iput-object v0, p0, Lnsb;->c:Lnsf;

    .line 631
    const/high16 v0, -0x80000000

    iput v0, p0, Lnsb;->d:I

    .line 62
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 659
    const/4 v0, 0x0

    .line 660
    iget-object v1, p0, Lnsb;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 661
    const/4 v0, 0x1

    iget-object v1, p0, Lnsb;->a:Ljava/lang/String;

    .line 662
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 664
    :cond_0
    iget-object v1, p0, Lnsb;->b:Lnsc;

    if-eqz v1, :cond_1

    .line 665
    const/4 v1, 0x2

    iget-object v2, p0, Lnsb;->b:Lnsc;

    .line 666
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 668
    :cond_1
    iget-object v1, p0, Lnsb;->c:Lnsf;

    if-eqz v1, :cond_2

    .line 669
    const/4 v1, 0x3

    iget-object v2, p0, Lnsb;->c:Lnsf;

    .line 670
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 672
    :cond_2
    iget v1, p0, Lnsb;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 673
    const/4 v1, 0x4

    iget v2, p0, Lnsb;->d:I

    .line 674
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 676
    :cond_3
    iget-object v1, p0, Lnsb;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 677
    const/4 v1, 0x6

    iget-object v2, p0, Lnsb;->e:Ljava/lang/Boolean;

    .line 678
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 680
    :cond_4
    iget-object v1, p0, Lnsb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 681
    iput v0, p0, Lnsb;->ai:I

    .line 682
    return v0
.end method

.method public a(Loxn;)Lnsb;
    .locals 2

    .prologue
    .line 690
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 691
    sparse-switch v0, :sswitch_data_0

    .line 695
    iget-object v1, p0, Lnsb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 696
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnsb;->ah:Ljava/util/List;

    .line 699
    :cond_1
    iget-object v1, p0, Lnsb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 701
    :sswitch_0
    return-object p0

    .line 706
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsb;->a:Ljava/lang/String;

    goto :goto_0

    .line 710
    :sswitch_2
    iget-object v0, p0, Lnsb;->b:Lnsc;

    if-nez v0, :cond_2

    .line 711
    new-instance v0, Lnsc;

    invoke-direct {v0}, Lnsc;-><init>()V

    iput-object v0, p0, Lnsb;->b:Lnsc;

    .line 713
    :cond_2
    iget-object v0, p0, Lnsb;->b:Lnsc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 717
    :sswitch_3
    iget-object v0, p0, Lnsb;->c:Lnsf;

    if-nez v0, :cond_3

    .line 718
    new-instance v0, Lnsf;

    invoke-direct {v0}, Lnsf;-><init>()V

    iput-object v0, p0, Lnsb;->c:Lnsf;

    .line 720
    :cond_3
    iget-object v0, p0, Lnsb;->c:Lnsf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 724
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 725
    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 728
    :cond_4
    iput v0, p0, Lnsb;->d:I

    goto :goto_0

    .line 730
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Lnsb;->d:I

    goto :goto_0

    .line 735
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnsb;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 691
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Lnsb;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 639
    const/4 v0, 0x1

    iget-object v1, p0, Lnsb;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 641
    :cond_0
    iget-object v0, p0, Lnsb;->b:Lnsc;

    if-eqz v0, :cond_1

    .line 642
    const/4 v0, 0x2

    iget-object v1, p0, Lnsb;->b:Lnsc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 644
    :cond_1
    iget-object v0, p0, Lnsb;->c:Lnsf;

    if-eqz v0, :cond_2

    .line 645
    const/4 v0, 0x3

    iget-object v1, p0, Lnsb;->c:Lnsf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 647
    :cond_2
    iget v0, p0, Lnsb;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 648
    const/4 v0, 0x4

    iget v1, p0, Lnsb;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 650
    :cond_3
    iget-object v0, p0, Lnsb;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 651
    const/4 v0, 0x6

    iget-object v1, p0, Lnsb;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 653
    :cond_4
    iget-object v0, p0, Lnsb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 655
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lnsb;->a(Loxn;)Lnsb;

    move-result-object v0

    return-object v0
.end method
