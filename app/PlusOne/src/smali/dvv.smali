.class public Ldvv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Ldvv;->a:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method public a(I)Lhny;
    .locals 3

    .prologue
    .line 82
    new-instance v0, Ldpe;

    iget-object v1, p0, Ldvv;->a:Landroid/content/Context;

    const-string v2, "LoadCirclesTask"

    invoke-direct {v0, v1, v2, p1}, Ldpe;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    return-object v0
.end method

.method public a(ILjava/lang/String;)Lhny;
    .locals 3

    .prologue
    .line 66
    const-string v0, "f."

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 67
    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    :goto_0
    iget-object v1, p0, Ldvv;->a:Landroid/content/Context;

    invoke-static {v1}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    new-instance v1, Ljqj;

    iget-object v2, p0, Ldvv;->a:Landroid/content/Context;

    invoke-direct {v1, v2, p1, v0}, Ljqj;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    move-object v0, v1

    .line 78
    :goto_1
    return-object v0

    :cond_0
    move-object v0, p2

    .line 67
    goto :goto_0

    .line 72
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 73
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v0, Ldpw;

    iget-object v1, p0, Ldvv;->a:Landroid/content/Context;

    const-string v2, "RemoveCircleTaskLegacy"

    invoke-direct {v0, v1, v2, p1, p2}, Ldpw;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_1
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lhny;
    .locals 7

    .prologue
    .line 50
    const-string v0, "f."

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 51
    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 53
    :goto_0
    iget-object v0, p0, Ldvv;->a:Landroid/content/Context;

    invoke-static {v0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    new-instance v0, Ljqm;

    iget-object v1, p0, Ldvv;->a:Landroid/content/Context;

    move v2, p1

    move-object v4, p3

    move v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Ljqm;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 61
    :goto_1
    return-object v0

    :cond_0
    move-object v3, p2

    .line 51
    goto :goto_0

    .line 57
    :cond_1
    new-instance v0, Ldqg;

    iget-object v1, p0, Ldvv;->a:Landroid/content/Context;

    const-string v2, "UpdateCircleTaskLegacy"

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p5

    invoke-direct/range {v0 .. v6}, Ldqg;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Z)Lhny;
    .locals 6

    .prologue
    .line 35
    iget-object v0, p0, Ldvv;->a:Landroid/content/Context;

    invoke-static {v0}, Ldsm;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    new-instance v0, Ljqi;

    iget-object v1, p0, Ldvv;->a:Landroid/content/Context;

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Ljqi;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 43
    :goto_0
    return-object v0

    .line 39
    :cond_0
    new-instance v0, Ldok;

    iget-object v1, p0, Ldvv;->a:Landroid/content/Context;

    const-string v2, "AddCircleTaskLegacy"

    move v3, p1

    move-object v4, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Ldok;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Z)V

    goto :goto_0
.end method
