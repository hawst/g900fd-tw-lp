.class public final Lqbd;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2590
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2593
    const/high16 v0, -0x80000000

    iput v0, p0, Lqbd;->a:I

    .line 2590
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2612
    const/4 v0, 0x0

    .line 2613
    iget v1, p0, Lqbd;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 2614
    const/4 v0, 0x1

    iget v1, p0, Lqbd;->a:I

    .line 2615
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2617
    :cond_0
    iget-object v1, p0, Lqbd;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2618
    const/4 v1, 0x2

    iget-object v2, p0, Lqbd;->b:Ljava/lang/Integer;

    .line 2619
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2621
    :cond_1
    iget-object v1, p0, Lqbd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2622
    iput v0, p0, Lqbd;->ai:I

    .line 2623
    return v0
.end method

.method public a(Loxn;)Lqbd;
    .locals 2

    .prologue
    .line 2631
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2632
    sparse-switch v0, :sswitch_data_0

    .line 2636
    iget-object v1, p0, Lqbd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2637
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbd;->ah:Ljava/util/List;

    .line 2640
    :cond_1
    iget-object v1, p0, Lqbd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2642
    :sswitch_0
    return-object p0

    .line 2647
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2648
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 2654
    :cond_2
    iput v0, p0, Lqbd;->a:I

    goto :goto_0

    .line 2656
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lqbd;->a:I

    goto :goto_0

    .line 2661
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbd;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2632
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2600
    iget v0, p0, Lqbd;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2601
    const/4 v0, 0x1

    iget v1, p0, Lqbd;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2603
    :cond_0
    iget-object v0, p0, Lqbd;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2604
    const/4 v0, 0x2

    iget-object v1, p0, Lqbd;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2606
    :cond_1
    iget-object v0, p0, Lqbd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2608
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2586
    invoke-virtual {p0, p1}, Lqbd;->a(Loxn;)Lqbd;

    move-result-object v0

    return-object v0
.end method
