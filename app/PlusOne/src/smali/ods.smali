.class public final Lods;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lods;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    new-array v0, v0, [Lods;

    sput-object v0, Lods;->a:[Lods;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Loxq;-><init>()V

    .line 40
    const/high16 v0, -0x80000000

    iput v0, p0, Lods;->b:I

    .line 37
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 67
    const/4 v0, 0x1

    iget v1, p0, Lods;->b:I

    .line 69
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 70
    iget-object v1, p0, Lods;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 71
    const/4 v1, 0x2

    iget-object v2, p0, Lods;->c:Ljava/lang/String;

    .line 72
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_0
    iget-object v1, p0, Lods;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 75
    const/4 v1, 0x3

    iget-object v2, p0, Lods;->d:Ljava/lang/String;

    .line 76
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_1
    iget-object v1, p0, Lods;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 79
    const/4 v1, 0x4

    iget-object v2, p0, Lods;->e:Ljava/lang/String;

    .line 80
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_2
    iget-object v1, p0, Lods;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    iput v0, p0, Lods;->ai:I

    .line 84
    return v0
.end method

.method public a(Loxn;)Lods;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 92
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 93
    sparse-switch v0, :sswitch_data_0

    .line 97
    iget-object v1, p0, Lods;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lods;->ah:Ljava/util/List;

    .line 101
    :cond_1
    iget-object v1, p0, Lods;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    :sswitch_0
    return-object p0

    .line 108
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 109
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 113
    :cond_2
    iput v0, p0, Lods;->b:I

    goto :goto_0

    .line 115
    :cond_3
    iput v2, p0, Lods;->b:I

    goto :goto_0

    .line 120
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lods;->c:Ljava/lang/String;

    goto :goto_0

    .line 124
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lods;->d:Ljava/lang/String;

    goto :goto_0

    .line 128
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lods;->e:Ljava/lang/String;

    goto :goto_0

    .line 93
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 51
    const/4 v0, 0x1

    iget v1, p0, Lods;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 52
    iget-object v0, p0, Lods;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x2

    iget-object v1, p0, Lods;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 55
    :cond_0
    iget-object v0, p0, Lods;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 56
    const/4 v0, 0x3

    iget-object v1, p0, Lods;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 58
    :cond_1
    iget-object v0, p0, Lods;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 59
    const/4 v0, 0x4

    iget-object v1, p0, Lods;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 61
    :cond_2
    iget-object v0, p0, Lods;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 63
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lods;->a(Loxn;)Lods;

    move-result-object v0

    return-object v0
.end method
