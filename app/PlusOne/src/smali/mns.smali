.class public final Lmns;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmns;


# instance fields
.field public b:[Lmnf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3341
    const/4 v0, 0x0

    new-array v0, v0, [Lmns;

    sput-object v0, Lmns;->a:[Lmns;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3342
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3345
    sget-object v0, Lmnf;->a:[Lmnf;

    iput-object v0, p0, Lmns;->b:[Lmnf;

    .line 3342
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3363
    .line 3364
    iget-object v1, p0, Lmns;->b:[Lmnf;

    if-eqz v1, :cond_1

    .line 3365
    iget-object v2, p0, Lmns;->b:[Lmnf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3366
    if-eqz v4, :cond_0

    .line 3367
    const/4 v5, 0x1

    .line 3368
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3365
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3372
    :cond_1
    iget-object v1, p0, Lmns;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3373
    iput v0, p0, Lmns;->ai:I

    .line 3374
    return v0
.end method

.method public a(Loxn;)Lmns;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3382
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3383
    sparse-switch v0, :sswitch_data_0

    .line 3387
    iget-object v2, p0, Lmns;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 3388
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmns;->ah:Ljava/util/List;

    .line 3391
    :cond_1
    iget-object v2, p0, Lmns;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3393
    :sswitch_0
    return-object p0

    .line 3398
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3399
    iget-object v0, p0, Lmns;->b:[Lmnf;

    if-nez v0, :cond_3

    move v0, v1

    .line 3400
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmnf;

    .line 3401
    iget-object v3, p0, Lmns;->b:[Lmnf;

    if-eqz v3, :cond_2

    .line 3402
    iget-object v3, p0, Lmns;->b:[Lmnf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3404
    :cond_2
    iput-object v2, p0, Lmns;->b:[Lmnf;

    .line 3405
    :goto_2
    iget-object v2, p0, Lmns;->b:[Lmnf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 3406
    iget-object v2, p0, Lmns;->b:[Lmnf;

    new-instance v3, Lmnf;

    invoke-direct {v3}, Lmnf;-><init>()V

    aput-object v3, v2, v0

    .line 3407
    iget-object v2, p0, Lmns;->b:[Lmnf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3408
    invoke-virtual {p1}, Loxn;->a()I

    .line 3405
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3399
    :cond_3
    iget-object v0, p0, Lmns;->b:[Lmnf;

    array-length v0, v0

    goto :goto_1

    .line 3411
    :cond_4
    iget-object v2, p0, Lmns;->b:[Lmnf;

    new-instance v3, Lmnf;

    invoke-direct {v3}, Lmnf;-><init>()V

    aput-object v3, v2, v0

    .line 3412
    iget-object v2, p0, Lmns;->b:[Lmnf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3383
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3350
    iget-object v0, p0, Lmns;->b:[Lmnf;

    if-eqz v0, :cond_1

    .line 3351
    iget-object v1, p0, Lmns;->b:[Lmnf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 3352
    if-eqz v3, :cond_0

    .line 3353
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 3351
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3357
    :cond_1
    iget-object v0, p0, Lmns;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3359
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3338
    invoke-virtual {p0, p1}, Lmns;->a(Loxn;)Lmns;

    move-result-object v0

    return-object v0
.end method
