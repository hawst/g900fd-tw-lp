.class public final Ljke;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:I

.field private static b:[I

.field private static c:[I

.field private static d:Ljke;


# instance fields
.field private final e:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 22
    const/4 v0, -0x1

    sput v0, Ljke;->a:I

    .line 24
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Ljke;->b:[I

    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v2, v0, v1

    sput-object v0, Ljke;->c:[I

    return-void

    .line 24
    :array_0
    .array-data 4
        0x3
        0x4
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Ljke;->e:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Ljke;
    .locals 2

    .prologue
    .line 39
    const-class v1, Ljke;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljke;->d:Ljke;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Ljke;

    invoke-direct {v0, p0}, Ljke;-><init>(Landroid/content/Context;)V

    sput-object v0, Ljke;->d:Ljke;

    .line 42
    :cond_0
    sget-object v0, Ljke;->d:Ljke;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()Lmyk;
    .locals 3

    .prologue
    .line 46
    new-instance v1, Lmyk;

    invoke-direct {v1}, Lmyk;-><init>()V

    .line 48
    new-instance v2, Lmxz;

    invoke-direct {v2}, Lmxz;-><init>()V

    .line 49
    sget v0, Ljke;->a:I

    if-gez v0, :cond_0

    iget-object v0, p0, Ljke;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    sput v0, Ljke;->a:I

    :cond_0
    sget v0, Ljke;->a:I

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    iput v0, v2, Lmxz;->a:I

    .line 51
    iput-object v2, v1, Lmyk;->b:Lmxz;

    .line 52
    iget-object v0, p0, Ljke;->e:Landroid/content/Context;

    const-class v2, Ljiy;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljiy;

    invoke-interface {v0}, Ljiy;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lmyk;->a:Ljava/lang/String;

    .line 54
    return-object v1

    .line 49
    :sswitch_0
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x4

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x5

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x9

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xa0 -> :sswitch_0
        0xf0 -> :sswitch_1
        0x140 -> :sswitch_2
        0x1e0 -> :sswitch_3
        0x280 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Ljin;)[I
    .locals 4

    .prologue
    .line 62
    sget-object v0, Ljkf;->a:[I

    invoke-virtual {p1}, Ljin;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 69
    const-string v0, "RequestCreatorHelper"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown fetch category: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    sget-object v0, Ljke;->b:[I

    :goto_0
    return-object v0

    .line 64
    :pswitch_0
    sget-object v0, Ljke;->b:[I

    goto :goto_0

    .line 66
    :pswitch_1
    sget-object v0, Ljke;->c:[I

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Ljke;->e:Landroid/content/Context;

    const-class v1, Ljiy;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljiy;

    invoke-interface {v0}, Ljiy;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
