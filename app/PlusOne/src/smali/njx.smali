.class public final Lnjx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnjx;


# instance fields
.field public b:Lnja;

.field public c:Lnie;

.field public d:Ljava/lang/String;

.field private e:Lnir;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7040
    const/4 v0, 0x0

    new-array v0, v0, [Lnjx;

    sput-object v0, Lnjx;->a:[Lnjx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7041
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7044
    iput-object v0, p0, Lnjx;->b:Lnja;

    .line 7047
    iput-object v0, p0, Lnjx;->c:Lnie;

    .line 7052
    iput-object v0, p0, Lnjx;->e:Lnir;

    .line 7041
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7075
    const/4 v0, 0x0

    .line 7076
    iget-object v1, p0, Lnjx;->b:Lnja;

    if-eqz v1, :cond_0

    .line 7077
    const/4 v0, 0x1

    iget-object v1, p0, Lnjx;->b:Lnja;

    .line 7078
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7080
    :cond_0
    iget-object v1, p0, Lnjx;->c:Lnie;

    if-eqz v1, :cond_1

    .line 7081
    const/4 v1, 0x2

    iget-object v2, p0, Lnjx;->c:Lnie;

    .line 7082
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7084
    :cond_1
    iget-object v1, p0, Lnjx;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 7085
    const/4 v1, 0x3

    iget-object v2, p0, Lnjx;->d:Ljava/lang/String;

    .line 7086
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7088
    :cond_2
    iget-object v1, p0, Lnjx;->e:Lnir;

    if-eqz v1, :cond_3

    .line 7089
    const/4 v1, 0x4

    iget-object v2, p0, Lnjx;->e:Lnir;

    .line 7090
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7092
    :cond_3
    iget-object v1, p0, Lnjx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7093
    iput v0, p0, Lnjx;->ai:I

    .line 7094
    return v0
.end method

.method public a(Loxn;)Lnjx;
    .locals 2

    .prologue
    .line 7102
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7103
    sparse-switch v0, :sswitch_data_0

    .line 7107
    iget-object v1, p0, Lnjx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7108
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjx;->ah:Ljava/util/List;

    .line 7111
    :cond_1
    iget-object v1, p0, Lnjx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7113
    :sswitch_0
    return-object p0

    .line 7118
    :sswitch_1
    iget-object v0, p0, Lnjx;->b:Lnja;

    if-nez v0, :cond_2

    .line 7119
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnjx;->b:Lnja;

    .line 7121
    :cond_2
    iget-object v0, p0, Lnjx;->b:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7125
    :sswitch_2
    iget-object v0, p0, Lnjx;->c:Lnie;

    if-nez v0, :cond_3

    .line 7126
    new-instance v0, Lnie;

    invoke-direct {v0}, Lnie;-><init>()V

    iput-object v0, p0, Lnjx;->c:Lnie;

    .line 7128
    :cond_3
    iget-object v0, p0, Lnjx;->c:Lnie;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7132
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnjx;->d:Ljava/lang/String;

    goto :goto_0

    .line 7136
    :sswitch_4
    iget-object v0, p0, Lnjx;->e:Lnir;

    if-nez v0, :cond_4

    .line 7137
    new-instance v0, Lnir;

    invoke-direct {v0}, Lnir;-><init>()V

    iput-object v0, p0, Lnjx;->e:Lnir;

    .line 7139
    :cond_4
    iget-object v0, p0, Lnjx;->e:Lnir;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7103
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7057
    iget-object v0, p0, Lnjx;->b:Lnja;

    if-eqz v0, :cond_0

    .line 7058
    const/4 v0, 0x1

    iget-object v1, p0, Lnjx;->b:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7060
    :cond_0
    iget-object v0, p0, Lnjx;->c:Lnie;

    if-eqz v0, :cond_1

    .line 7061
    const/4 v0, 0x2

    iget-object v1, p0, Lnjx;->c:Lnie;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7063
    :cond_1
    iget-object v0, p0, Lnjx;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 7064
    const/4 v0, 0x3

    iget-object v1, p0, Lnjx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7066
    :cond_2
    iget-object v0, p0, Lnjx;->e:Lnir;

    if-eqz v0, :cond_3

    .line 7067
    const/4 v0, 0x4

    iget-object v1, p0, Lnjx;->e:Lnir;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7069
    :cond_3
    iget-object v0, p0, Lnjx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7071
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7037
    invoke-virtual {p0, p1}, Lnjx;->a(Loxn;)Lnjx;

    move-result-object v0

    return-object v0
.end method
