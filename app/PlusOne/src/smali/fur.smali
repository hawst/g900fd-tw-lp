.class public Lfur;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfur;->b:Z

    .line 50
    iput-object p1, p0, Lfur;->a:Landroid/content/ContentResolver;

    .line 51
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lfur;->b:Z

    if-nez v0, :cond_0

    invoke-static {}, Llsx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 98
    :goto_0
    return v0

    .line 97
    :cond_0
    const/4 v0, 0x0

    .line 98
    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 38
    invoke-static {}, Llsx;->c()V

    .line 41
    iget-object v0, p0, Lfur;->a:Landroid/content/ContentResolver;

    const-string v1, "dummy"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfur;->b:Z

    .line 43
    return-void
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Lfur;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lfur;->a:Landroid/content/ContentResolver;

    const-string v2, "plusone:psyncho_launch_strings_enabled"

    invoke-static {v1, v2, v0}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0}, Lfur;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lfur;->a:Landroid/content/ContentResolver;

    const-string v2, "plusone:disable_p_components_in_gplus_app"

    invoke-static {v1, v2, v0}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method
