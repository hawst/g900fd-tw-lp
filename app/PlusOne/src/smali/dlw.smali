.class public final Ldlw;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmiy;",
        "Lmiz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:[Lnmx;

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 29
    const-string v3, "searchquery"

    new-instance v4, Lmiy;

    invoke-direct {v4}, Lmiy;-><init>()V

    new-instance v5, Lmiz;

    invoke-direct {v5}, Lmiz;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 31
    iput-object p3, p0, Ldlw;->a:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Ldlw;->b:Ljava/lang/String;

    .line 33
    iput-boolean p5, p0, Ldlw;->r:Z

    .line 34
    return-void
.end method


# virtual methods
.method protected a(Lmiy;)V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lnnq;

    invoke-direct {v0}, Lnnq;-><init>()V

    iput-object v0, p1, Lmiy;->a:Lnnq;

    .line 39
    iget-object v0, p1, Lmiy;->a:Lnnq;

    new-instance v1, Loej;

    invoke-direct {v1}, Loej;-><init>()V

    iput-object v1, v0, Lnnq;->a:Loej;

    .line 40
    iget-object v0, p1, Lmiy;->a:Lnnq;

    iget-object v0, v0, Lnnq;->a:Loej;

    iget-object v1, p0, Ldlw;->a:Ljava/lang/String;

    iput-object v1, v0, Loej;->a:Ljava/lang/String;

    .line 43
    iget-object v0, p1, Lmiy;->a:Lnnq;

    iget-object v1, v0, Lnnq;->a:Loej;

    iget-boolean v0, p0, Ldlw;->r:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    :goto_0
    iput v0, v1, Loej;->b:I

    .line 46
    iget-object v0, p0, Ldlw;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p1, Lmiy;->a:Lnnq;

    new-instance v1, Lnmw;

    invoke-direct {v1}, Lnmw;-><init>()V

    iput-object v1, v0, Lnnq;->b:Lnmw;

    .line 48
    iget-object v0, p1, Lmiy;->a:Lnnq;

    iget-object v0, v0, Lnnq;->b:Lnmw;

    iget-object v1, p0, Ldlw;->b:Ljava/lang/String;

    iput-object v1, v0, Lnmw;->a:Ljava/lang/String;

    .line 50
    :cond_0
    return-void

    .line 43
    :cond_1
    const/16 v0, 0x9

    goto :goto_0
.end method

.method protected a(Lmiz;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p1, Lmiz;->a:Lnnu;

    .line 55
    iget-object v1, v0, Lnnu;->a:Lnna;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lnnu;->a:Lnna;

    iget-object v1, v1, Lnna;->a:Lnmy;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, v0, Lnnu;->a:Lnna;

    iget-object v1, v1, Lnna;->a:Lnmy;

    iget-object v1, v1, Lnmy;->b:Ljava/lang/String;

    iput-object v1, p0, Ldlw;->p:Ljava/lang/String;

    .line 57
    iget-object v0, v0, Lnnu;->a:Lnna;

    iget-object v0, v0, Lnna;->a:Lnmy;

    iget-object v0, v0, Lnmy;->a:[Lnmx;

    iput-object v0, p0, Ldlw;->q:[Lnmx;

    .line 59
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmiy;

    invoke-virtual {p0, p1}, Ldlw;->a(Lmiy;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Ldlw;->p:Ljava/lang/String;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmiz;

    invoke-virtual {p0, p1}, Ldlw;->a(Lmiz;)V

    return-void
.end method

.method public c()[Lnmx;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ldlw;->q:[Lnmx;

    return-object v0
.end method
