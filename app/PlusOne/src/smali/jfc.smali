.class final Ljfc;
.super Ljfb;
.source "PG"


# static fields
.field private static final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 436
    const-class v0, Ljfc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ljfc;->c:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 435
    invoke-direct {p0}, Ljfb;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 528
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "G+ must call MovieMakerProvider.setInstance() to provide the actual implementation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 518
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "G+ must call MovieMakerProvider.setInstance() to provide the actual implementation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljdz;Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 523
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "G+ must call MovieMakerProvider.setInstance() to provide the actual implementation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 507
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "G+ must call MovieMakerProvider.setInstance() to provide the actual implementation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Long;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 599
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 502
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/content/Intent;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 512
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 513
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljeb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 464
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 465
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljed;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljed;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 470
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 471
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Iterable;Lkfd;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljdz;",
            ">;",
            "Lkfd;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljdz;",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 489
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    const-string v0, "Dummy: getMediaUris(%s, %s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 490
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;J)Ljfd;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 495
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    const-string v0, "Dummy: getStoryboard(%s, %s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 496
    new-instance v0, Ljfd;

    sget-object v1, Ljff;->c:Ljff;

    const/16 v2, 0x190

    invoke-direct {v0, v1, v2, v4, v4}, Ljfd;-><init>(Ljff;ILood;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljdz;Lllx;)Lllt;
    .locals 1

    .prologue
    .line 589
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(ILjeo;)V
    .locals 1

    .prologue
    .line 548
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 549
    return-void
.end method

.method public a(Ljdz;)V
    .locals 1

    .prologue
    .line 594
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 595
    return-void
.end method

.method public a(Ljed;Z)V
    .locals 1

    .prologue
    .line 448
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 449
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 453
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 573
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 458
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 459
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljed;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljed;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 476
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 477
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 443
    sget-object v0, Ljfc;->c:Ljava/lang/String;

    .line 444
    return-void
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 553
    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 538
    const/4 v0, 0x0

    return v0
.end method

.method public c(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 558
    const/4 v0, 0x0

    return v0
.end method

.method public d(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 563
    return-object p1
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 543
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 568
    return-object p1
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 583
    const-string v0, ""

    return-object v0
.end method

.method public f(Landroid/net/Uri;)J
    .locals 2

    .prologue
    .line 578
    const-wide/16 v0, -0x1

    return-wide v0
.end method
