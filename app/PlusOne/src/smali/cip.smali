.class public final Lcip;
.super Llol;
.source "PG"


# instance fields
.field private final N:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

.field private O:Landroid/view/View;

.field private P:Landroid/view/View;

.field private Q:Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

.field private R:Lcis;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Llol;-><init>()V

    .line 37
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iput-object v0, p0, Lcip;->N:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 115
    return-void
.end method

.method static synthetic a(Lcip;)Llnl;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcip;->at:Llnl;

    return-object v0
.end method

.method static synthetic a(Lcip;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcip;->d(I)V

    return-void
.end method

.method static synthetic b(Lcip;)Llnl;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcip;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Lcip;)Lcis;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcip;->R:Lcis;

    return-object v0
.end method

.method private d()F
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 120
    invoke-virtual {p0}, Lcip;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 121
    const-string v0, "aspectX"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 122
    const-string v0, "aspectY"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 123
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 124
    :cond_0
    const-string v0, "outputX"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 125
    const-string v0, "outputY"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 128
    :cond_1
    if-lez v1, :cond_2

    if-lez v0, :cond_2

    .line 129
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 131
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 165
    packed-switch p1, :pswitch_data_0

    .line 180
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid aspect ratio index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :pswitch_0
    iget-object v0, p0, Lcip;->Q:Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(Z)V

    :goto_0
    move v0, v1

    .line 183
    :goto_1
    iget-object v2, p0, Lcip;->N:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    .line 184
    iget-object v2, p0, Lcip;->N:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    aget-object v4, v2, v0

    if-ne v0, p1, :cond_0

    move v2, v3

    :goto_2
    invoke-virtual {v4, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 171
    :pswitch_1
    iget-object v0, p0, Lcip;->Q:Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->b()F

    move-result v0

    .line 172
    iget-object v2, p0, Lcip;->Q:Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FZ)V

    goto :goto_0

    .line 176
    :pswitch_2
    iget-object v0, p0, Lcip;->Q:Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FZ)V

    goto :goto_0

    :cond_0
    move v2, v1

    .line 184
    goto :goto_2

    .line 186
    :cond_1
    return-void

    .line 165
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcip;->Q:Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 47
    iget-object v0, p0, Lcip;->at:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 49
    const v1, 0x7f040078

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcip;->O:Landroid/view/View;

    .line 51
    iget-object v0, p0, Lcip;->O:Landroid/view/View;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to inflate the filter fragment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    iget-object v0, p0, Lcip;->O:Landroid/view/View;

    const v1, 0x7f10023a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    iput-object v0, p0, Lcip;->Q:Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    .line 57
    iget-object v0, p0, Lcip;->O:Landroid/view/View;

    const v1, 0x7f10023f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcip;->P:Landroid/view/View;

    .line 58
    invoke-virtual {p0, v2}, Lcip;->c(I)V

    .line 60
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcip;->e(Z)V

    .line 62
    iget-object v0, p0, Lcip;->O:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v2, 0x0

    .line 90
    invoke-direct {p0}, Lcip;->d()F

    move-result v0

    cmpg-float v0, v0, v8

    if-gtz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcip;->O:Landroid/view/View;

    const v1, 0x7f10023b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-array v3, v7, [I

    fill-array-data v3, :array_0

    new-array v4, v7, [I

    fill-array-data v4, :array_1

    move v1, v2

    :goto_0
    if-ge v1, v7, :cond_0

    iget-object v5, p0, Lcip;->N:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iget-object v0, p0, Lcip;->O:Landroid/view/View;

    aget v6, v3, v1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    aput-object v0, v5, v1

    iget-object v0, p0, Lcip;->N:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    aget-object v0, v0, v1

    new-instance v5, Lcir;

    invoke-direct {v5, p0, v4, v1}, Lcir;-><init>(Lcip;[II)V

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v2}, Lcip;->d(I)V

    .line 94
    :cond_1
    iget-object v0, p0, Lcip;->Q:Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(Landroid/graphics/Bitmap;)V

    .line 96
    invoke-direct {p0}, Lcip;->d()F

    move-result v0

    cmpl-float v1, v0, v8

    if-lez v1, :cond_2

    iget-object v1, p0, Lcip;->Q:Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FZ)V

    .line 97
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcip;->c(I)V

    .line 98
    return-void

    .line 91
    nop

    :array_0
    .array-data 4
        0x7f10023c
        0x7f10023d
        0x7f10023e
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x1
        0x2
    .end array-data
.end method

.method public a(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0, p1}, Llol;->a(Landroid/view/Menu;)V

    .line 69
    invoke-virtual {p0}, Lcip;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 70
    if-nez v0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Could not get action bar"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    .line 75
    new-instance v1, Lciq;

    invoke-direct {v1, p0}, Lciq;-><init>(Lcip;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-void
.end method

.method public a(Lcis;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcip;->R:Lcis;

    .line 106
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcip;->P:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 102
    return-void
.end method
