.class public Legy;
.super Lehh;
.source "PG"

# interfaces
.implements Lze;


# instance fields
.field public N:I

.field public O:Ljava/lang/String;

.field private aw:Ljava/lang/String;

.field private ax:Ljava/lang/String;

.field private ay:Z

.field private az:Landroid/support/v7/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lehh;-><init>()V

    return-void
.end method

.method static synthetic a(Legy;)Landroid/support/v7/widget/SearchView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method


# virtual methods
.method protected V()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method protected W()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 291
    iput-boolean v3, p0, Legy;->ai:Z

    .line 292
    invoke-virtual {p0}, Legy;->af_()V

    .line 293
    invoke-virtual {p0}, Legy;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 294
    invoke-virtual {p0, v3}, Legy;->a(Z)V

    .line 295
    return-void
.end method

.method protected X()V
    .locals 0

    .prologue
    .line 298
    invoke-virtual {p0}, Legy;->W()V

    .line 299
    return-void
.end method

.method protected a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;
    .locals 9

    .prologue
    .line 164
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v8, p8

    invoke-super/range {v0 .. v8}, Lehh;->a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 79
    invoke-super {p0, p1}, Lehh;->a(Landroid/os/Bundle;)V

    .line 80
    if-eqz p1, :cond_0

    .line 81
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legy;->O:Ljava/lang/String;

    .line 82
    const-string v0, "delayed_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legy;->aw:Ljava/lang/String;

    .line 83
    const-string v0, "search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Legy;->N:I

    .line 84
    const-string v0, "injected_item_blob"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legy;->ax:Ljava/lang/String;

    .line 85
    const-string v0, "show_search_view"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Legy;->ay:Z

    .line 86
    invoke-virtual {p0}, Legy;->af_()V

    .line 87
    invoke-virtual {p0}, Legy;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 93
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-virtual {p0}, Legy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legy;->aw:Ljava/lang/String;

    .line 90
    invoke-virtual {p0}, Legy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "injected_item_blob"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legy;->ax:Ljava/lang/String;

    .line 91
    invoke-virtual {p0}, Legy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "show_search_view"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Legy;->ay:Z

    goto :goto_0
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 214
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 216
    :pswitch_0
    invoke-super {p0, p1, p2}, Lehh;->a(Ldo;Landroid/database/Cursor;)V

    .line 221
    iget-object v0, p0, Legy;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Legy;->O:Ljava/lang/String;

    .line 222
    invoke-virtual {p0}, Legy;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "query"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {p0}, Legy;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 224
    iget-object v0, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    goto :goto_0

    .line 232
    :pswitch_1
    iget-object v0, p0, Legy;->W:Lfdj;

    invoke-virtual {v0, v2}, Lfdj;->c(Z)V

    .line 233
    iget-object v0, p0, Legy;->W:Lfdj;

    const/4 v1, -0x1

    invoke-virtual {v0, p2, v1}, Lfdj;->b(Landroid/database/Cursor;I)V

    .line 234
    invoke-virtual {p0}, Legy;->aA()V

    .line 236
    iput-boolean v2, p0, Legy;->aq:Z

    .line 237
    iput-boolean v2, p0, Legy;->ar:Z

    .line 239
    invoke-virtual {p0}, Legy;->x()Landroid/view/View;

    .line 240
    iget-boolean v0, p0, Legy;->ak:Z

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Legy;->U:Licq;

    const v1, 0x7f0a058f

    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    .line 242
    iget-object v0, p0, Legy;->U:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 266
    :goto_1
    invoke-virtual {p0}, Legy;->ay()V

    goto :goto_0

    .line 244
    :cond_1
    iget-object v0, p0, Legy;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 245
    if-eqz p2, :cond_3

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 246
    iget-object v0, p0, Legy;->U:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 247
    iget-object v0, p0, Legy;->Y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Legy;->aq:Z

    .line 263
    :cond_2
    :goto_2
    iput-boolean v2, p0, Legy;->ai:Z

    goto :goto_1

    .line 248
    :cond_3
    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "fetch_older"

    .line 249
    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 250
    :cond_4
    invoke-virtual {p0}, Legy;->az()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    iget-object v0, p0, Legy;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 253
    :cond_5
    iget-object v0, p0, Legy;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 254
    iget-boolean v0, p0, Legy;->ai:Z

    if-eqz v0, :cond_6

    .line 255
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Legy;->a(Z)V

    goto :goto_2

    .line 256
    :cond_6
    invoke-virtual {p0}, Legy;->az()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    iget-object v0, p0, Legy;->U:Licq;

    const v1, 0x7f0a06f6

    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    .line 258
    iget-object v0, p0, Legy;->U:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 261
    :cond_7
    iget-object v0, p0, Legy;->U:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 214
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 47
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Legy;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 200
    invoke-super {p0, p1}, Lehh;->a(Lhjk;)V

    .line 201
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 67
    const-string v0, "fetch_newer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fetch_older"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lhos;->a(Z)V

    .line 69
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    iput-boolean v0, p0, Legy;->ak:Z

    .line 70
    invoke-virtual {p0}, Legy;->aD()V

    .line 71
    invoke-virtual {p0}, Legy;->aa()V

    .line 75
    :goto_0
    return-void

    .line 73
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lehh;->a(Ljava/lang/String;Lhoz;Lhos;)V

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 171
    iget-boolean v0, p0, Legy;->ay:Z

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p0}, Legy;->n()Lz;

    move-result-object v0

    .line 173
    new-instance v1, Landroid/support/v7/widget/SearchView;

    invoke-virtual {p1}, Loo;->i()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 174
    invoke-static {v0, v1}, Lley;->a(Landroid/content/Context;Landroid/support/v7/widget/SearchView;)V

    .line 175
    invoke-virtual {p0}, Legy;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0818

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;)V

    .line 176
    invoke-virtual {v1, v3}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 177
    iget-object v0, p0, Legy;->O:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 178
    invoke-virtual {v1, p0}, Landroid/support/v7/widget/SearchView;->a(Lze;)V

    .line 179
    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->requestFocus()Z

    .line 181
    iput-object v1, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    .line 183
    invoke-static {p1, v4}, Lley;->a(Loo;Z)V

    .line 184
    invoke-virtual {p1, v4}, Loo;->c(Z)V

    .line 185
    invoke-virtual {p1, v1}, Loo;->a(Landroid/view/View;)V

    .line 186
    invoke-virtual {p1, v4}, Loo;->e(Z)V

    .line 187
    invoke-virtual {p1, v3}, Loo;->d(Z)V

    .line 189
    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 7

    .prologue
    .line 308
    iget-object v0, p0, Legy;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    invoke-virtual {p0}, Legy;->ay()V

    .line 332
    :goto_0
    return-void

    .line 313
    :cond_0
    iget-object v0, p0, Legy;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 314
    const-string v0, "extra_search_query"

    iget-object v2, p0, Legy;->O:Ljava/lang/String;

    invoke-static {v0, v2}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 315
    iget-object v0, p0, Legy;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Legy;->at:Llnl;

    invoke-direct {v3, v4, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->cE:Lhmv;

    .line 317
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 318
    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 315
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 321
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Legy;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Legy;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 325
    :cond_1
    invoke-virtual {p0}, Legy;->n()Lz;

    move-result-object v1

    .line 326
    new-instance v0, Ldqc;

    iget-object v2, p0, Legy;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Legy;->O:Ljava/lang/String;

    iget-object v4, p0, Legy;->ax:Ljava/lang/String;

    iget v5, p0, Legy;->N:I

    move v6, p1

    invoke-direct/range {v0 .. v6}, Ldqc;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 328
    if-eqz p1, :cond_2

    const-string v1, "fetch_newer"

    :goto_1
    invoke-virtual {v0, v1}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 329
    iget-object v1, p0, Legy;->au:Llnh;

    const-class v2, Lhoc;

    invoke-virtual {v1, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 331
    invoke-virtual {p0}, Legy;->ay()V

    goto :goto_0

    .line 328
    :cond_2
    const-string v1, "fetch_older"

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 278
    invoke-virtual {p0}, Legy;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 279
    iget-object v0, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 281
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 282
    iget-object v1, p0, Legy;->O:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 283
    iput-boolean v2, p0, Legy;->al:Z

    .line 285
    :cond_0
    iput-object v0, p0, Legy;->O:Ljava/lang/String;

    .line 286
    invoke-virtual {p0}, Legy;->X()V

    .line 287
    return v2
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 126
    invoke-super {p0}, Lehh;->aO_()V

    .line 128
    iget-object v0, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Legy;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    new-instance v1, Legz;

    invoke-direct {v1, p0}, Legz;-><init>(Legy;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/SearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 139
    :cond_0
    return-void
.end method

.method public aa()V
    .locals 3

    .prologue
    .line 338
    invoke-virtual {p0}, Legy;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 339
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 357
    new-instance v0, Lhmk;

    sget-object v1, Lonh;->a:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method protected ad_()Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method

.method protected af_()V
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Legy;->O:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    const-string v0, "com.google.android.apps.plus.INVALID_SEARCH_QUERY"

    iput-object v0, p0, Legy;->ag:Ljava/lang/String;

    .line 348
    :goto_0
    return-void

    .line 346
    :cond_0
    iget-object v0, p0, Legy;->O:Ljava/lang/String;

    iget v1, p0, Legy;->N:I

    invoke-static {v0, v1}, Lfvd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legy;->ag:Ljava/lang/String;

    goto :goto_0
.end method

.method protected ag_()V
    .locals 0

    .prologue
    .line 353
    return-void
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 194
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 195
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 196
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    return v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Legy;->N:I

    if-eq p1, v0, :cond_0

    .line 106
    iput p1, p0, Legy;->N:I

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Legy;->al:Z

    .line 108
    invoke-virtual {p0}, Legy;->W()V

    .line 110
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1}, Lehh;->e(Landroid/os/Bundle;)V

    .line 98
    const-string v0, "query"

    iget-object v1, p0, Legy;->O:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v0, "delayed_query"

    iget-object v1, p0, Legy;->aw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v0, "search_mode"

    iget v1, p0, Legy;->N:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    const-string v0, "show_search_view"

    iget-boolean v1, p0, Legy;->ay:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 102
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Legy;->aw:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Legy;->aw:Ljava/lang/String;

    iput-object v0, p0, Legy;->O:Ljava/lang/String;

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Legy;->aw:Ljava/lang/String;

    .line 118
    invoke-virtual {p0}, Legy;->X()V

    .line 121
    :cond_0
    invoke-super {p0}, Lehh;->g()V

    .line 122
    return-void
.end method

.method public l(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 303
    invoke-super {p0, p1}, Lehh;->l(Landroid/os/Bundle;)V

    .line 304
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 143
    invoke-super {p0}, Lehh;->z()V

    .line 145
    iget-object v0, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Legy;->az:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 148
    :cond_0
    return-void
.end method
