.class final Ldrd;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/os/Bundle;

.field private b:Lizu;

.field private c:Landroid/net/Uri;

.field private d:I

.field private e:Landroid/graphics/Bitmap$CompressFormat;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/io/ByteArrayOutputStream;

.field private i:Landroid/graphics/Bitmap;

.field private j:Landroid/graphics/Bitmap;

.field private final k:Ldxa;

.field private synthetic l:Ldqx;


# direct methods
.method public constructor <init>(Ldqx;Ldxa;Lizu;)V
    .locals 1

    .prologue
    .line 654
    iput-object p1, p0, Ldrd;->l:Ldqx;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 655
    iput-object p2, p0, Ldrd;->k:Ldxa;

    .line 656
    iput-object p3, p0, Ldrd;->b:Lizu;

    .line 658
    iget-object v0, p1, Ldqx;->c:Landroid/content/Intent;

    .line 659
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Ldrd;->a:Landroid/os/Bundle;

    .line 661
    iget-object v0, p0, Ldrd;->k:Ldxa;

    invoke-virtual {v0}, Ldxa;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Ldrd;->j:Landroid/graphics/Bitmap;

    .line 662
    return-void

    .line 659
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 686
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 687
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 689
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 690
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v3, v4, :cond_0

    .line 695
    :goto_1
    return v0

    .line 689
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 695
    goto :goto_1
.end method

.method private a(Landroid/net/Uri;)Ljava/io/File;
    .locals 11

    .prologue
    const/16 v10, 0x2e

    const/4 v9, -0x1

    const/4 v6, 0x0

    .line 787
    invoke-static {p1}, Llsb;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Ldrd;->l:Ldqx;

    iget-object v0, v0, Ldqx;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1}, Llsb;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 797
    :goto_0
    invoke-virtual {v0, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 798
    if-eq v1, v9, :cond_2

    .line 799
    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 804
    :goto_1
    invoke-direct {p0, v1}, Ldrd;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 805
    invoke-direct {p0, v1}, Ldrd;->a(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 808
    :goto_2
    return-object v0

    .line 789
    :cond_0
    invoke-static {p1}, Llsb;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 790
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 792
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move-object v1, v0

    .line 801
    goto :goto_1

    .line 808
    :cond_3
    iget-object v1, p0, Ldrd;->l:Ldqx;

    iget-object v1, v1, Ldqx;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-direct {p0, v4, v0}, Ldrd;->a(Ljava/io/File;Ljava/lang/String;)I

    move-result v2

    iget-object v1, p0, Ldrd;->l:Ldqx;

    iget-object v1, v1, Ldqx;->a:Landroid/content/Context;

    sget-object v3, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v1, v3}, Ldh;->a(Landroid/content/Context;Ljava/lang/String;)[Ljava/io/File;

    move-result-object v7

    array-length v8, v7

    move v5, v6

    :goto_3
    if-ge v5, v8, :cond_4

    aget-object v3, v7, v5

    invoke-direct {p0, v3, v0}, Ldrd;->a(Ljava/io/File;Ljava/lang/String;)I

    move-result v1

    if-le v1, v2, :cond_6

    move-object v2, v3

    :goto_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v4, v2

    move v2, v1

    goto :goto_3

    :cond_4
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-eq v2, v9, :cond_5

    invoke-virtual {v1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-direct {p0, v0}, Ldrd;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_2

    :cond_5
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_6
    move v1, v2

    move-object v2, v4

    goto :goto_4
.end method

.method private a(Ljava/lang/String;)Ljava/io/File;
    .locals 5

    .prologue
    .line 720
    const/4 v0, 0x2

    .line 723
    const/16 v1, 0x7e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 724
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 726
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 728
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 729
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 737
    :cond_0
    :goto_0
    iget-object v1, p0, Ldrd;->f:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 738
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 739
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 740
    add-int/lit8 v0, v0, 0x1

    .line 743
    goto :goto_0

    .line 745
    :cond_1
    return-object v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 837
    iget-object v0, p0, Ldrd;->l:Ldqx;

    iget-object v0, v0, Ldqx;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 839
    iget-object v2, p0, Ldrd;->e:Landroid/graphics/Bitmap$CompressFormat;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    if-ne v2, v3, :cond_5

    .line 840
    iget-object v2, p0, Ldrd;->b:Lizu;

    invoke-virtual {v2}, Lizu;->e()Landroid/net/Uri;

    move-result-object v2

    .line 844
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 845
    :try_start_1
    iget-object v3, p0, Ldrd;->c:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v1

    .line 846
    iget-boolean v0, p0, Ldrd;->g:Z

    if-eqz v0, :cond_2

    .line 847
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x800

    invoke-direct {v0, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    .line 848
    iget-object v0, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    invoke-direct {p0, v2, v0, p1}, Ldrd;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Landroid/graphics/Bitmap;)V

    .line 849
    iget-object v0, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 854
    :goto_0
    if-eqz v2, :cond_0

    .line 855
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 857
    :cond_0
    if-eqz v1, :cond_1

    .line 858
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 881
    :cond_1
    :goto_1
    return-void

    .line 851
    :cond_2
    :try_start_2
    invoke-direct {p0, v2, v1, p1}, Ldrd;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Landroid/graphics/Bitmap;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 854
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v2, :cond_3

    .line 855
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 857
    :cond_3
    if-eqz v1, :cond_4

    .line 858
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_4
    throw v0

    .line 864
    :cond_5
    :try_start_3
    const-string v2, "DefaultEditorProvider"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 865
    const-string v2, "Saving to: "

    iget-object v3, p0, Ldrd;->c:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 867
    :cond_6
    :goto_3
    iget-object v2, p0, Ldrd;->c:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v1

    .line 868
    iget-boolean v0, p0, Ldrd;->g:Z

    if-eqz v0, :cond_9

    .line 869
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x800

    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    .line 870
    iget-object v0, p0, Ldrd;->e:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x5a

    iget-object v3, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p1, v0, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 871
    iget-object v0, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 876
    :goto_4
    if-eqz v1, :cond_1

    .line 877
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    goto :goto_1

    .line 865
    :cond_7
    :try_start_4
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    .line 876
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_8

    .line 877
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_8
    throw v0

    .line 873
    :cond_9
    :try_start_5
    iget-object v0, p0, Ldrd;->e:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x5a

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_4

    .line 854
    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_2
.end method

.method private a(Ljava/io/InputStream;Ljava/io/OutputStream;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 814
    new-instance v0, Lidp;

    invoke-direct {v0}, Lidp;-><init>()V

    .line 816
    :try_start_0
    invoke-virtual {v0, p1}, Lidp;->a(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 823
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lidp;->b()V

    .line 824
    sget v1, Lidp;->a:I

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lidp;->b(ILjava/lang/Object;)Z

    .line 825
    sget v1, Lidp;->b:I

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lidp;->b(ILjava/lang/Object;)Z

    .line 826
    sget v1, Lidp;->i:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 827
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    .line 826
    invoke-virtual {v0, v1, v2, v3, v4}, Lidp;->a(IJLjava/util/TimeZone;)Z

    .line 832
    sget v1, Lidp;->f:I

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lidp;->b(ILjava/lang/Object;)Z

    .line 833
    invoke-virtual {v0, p3, p2}, Lidp;->a(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)V

    .line 834
    return-void

    .line 817
    :catch_0
    move-exception v1

    .line 818
    const-string v2, "DefaultEditorProvider"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 819
    const-string v2, "DefaultEditorProvider"

    const-string v3, "Failed to read Exif data."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private a(Ljava/io/File;)Z
    .locals 2

    .prologue
    .line 749
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 750
    iget-object v1, p0, Ldrd;->l:Ldqx;

    iget-object v1, v1, Ldqx;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 752
    :try_start_0
    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 758
    if-eqz v0, :cond_0

    .line 759
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 762
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 763
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 767
    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 756
    :catch_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 763
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 758
    :catchall_0
    move-exception v0

    .line 759
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 763
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    :cond_3
    throw v0
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Boolean;
    .locals 14

    .prologue
    const v9, 0xb71b0

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1005
    iget-object v0, p0, Ldrd;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 1006
    iget-object v0, p0, Ldrd;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 1007
    iget-object v0, p0, Ldrd;->l:Ldqx;

    iget-object v0, v0, Ldqx;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 1008
    if-eqz v8, :cond_0

    const-string v0, "outputX"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "outputY"

    .line 1009
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010
    const-string v0, "outputX"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1011
    const-string v2, "outputY"

    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1012
    const-string v5, "scale"

    invoke-virtual {v8, v5, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "scaleUpIfNeeded"

    .line 1013
    invoke-virtual {v8, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v7

    .line 1015
    :goto_0
    if-le v0, v1, :cond_4

    .line 1016
    if-eqz v5, :cond_3

    :goto_1
    move v1, v0

    .line 1021
    :goto_2
    if-le v2, v3, :cond_6

    .line 1022
    if-eqz v5, :cond_5

    move v0, v2

    .line 1027
    :goto_3
    iget-object v2, p0, Ldrd;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Ldrd;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1028
    iget-object v2, p0, Ldrd;->j:Landroid/graphics/Bitmap;

    invoke-static {v2, v1, v0, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Ldrd;->j:Landroid/graphics/Bitmap;

    .line 1033
    :cond_0
    if-eqz v8, :cond_b

    :try_start_0
    const-string v0, "return-data"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1034
    iget-object v2, p0, Ldrd;->j:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-nez v0, :cond_7

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad argument to getDownsampledBitmap()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1041
    :catch_0
    move-exception v0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1048
    :goto_4
    return-object v0

    :cond_2
    move v5, v6

    .line 1013
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1016
    goto :goto_1

    :cond_4
    move v1, v0

    .line 1018
    goto :goto_2

    :cond_5
    move v0, v3

    .line 1022
    goto :goto_3

    :cond_6
    move v0, v2

    .line 1024
    goto :goto_3

    .line 1034
    :cond_7
    :try_start_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    move v1, v6

    :goto_5
    if-le v0, v9, :cond_8

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v0, v0, 0x4

    goto :goto_5

    :cond_8
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    shr-int/2addr v0, v1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    shr-int v1, v3, v1

    const/4 v3, 0x1

    invoke-static {v2, v0, v1, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_a

    move-object v0, v4

    :cond_9
    :goto_6
    iput-object v0, p0, Ldrd;->i:Landroid/graphics/Bitmap;

    .line 1035
    iget-object v0, p0, Ldrd;->i:Landroid/graphics/Bitmap;

    if-nez v0, :cond_b

    .line 1036
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_4

    .line 1034
    :cond_a
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int/2addr v1, v2

    if-le v1, v9, :cond_9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    shr-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    shr-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_6

    .line 1039
    :cond_b
    iget-object v8, p0, Ldrd;->j:Landroid/graphics/Bitmap;

    iget-object v0, p0, Ldrd;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_e

    iget-object v0, p0, Ldrd;->a:Landroid/os/Bundle;

    const-string v1, "outputFormat"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Ldrd;->a:Landroid/os/Bundle;

    const-string v1, "outputFormat"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "png"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    iput-object v1, p0, Ldrd;->e:Landroid/graphics/Bitmap$CompressFormat;

    iput-object v0, p0, Ldrd;->f:Ljava/lang/String;

    :goto_7
    iget-object v0, p0, Ldrd;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_13

    iget-object v0, p0, Ldrd;->a:Landroid/os/Bundle;

    const-string v1, "output"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Ldrd;->a:Landroid/os/Bundle;

    const-string v1, "output"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Ldrd;->c:Landroid/net/Uri;

    iget-object v0, p0, Ldrd;->c:Landroid/net/Uri;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No output Uri provided!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1044
    :catch_1
    move-exception v0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    .line 1039
    :cond_c
    :try_start_2
    const-string v1, "webp"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->WEBP:Landroid/graphics/Bitmap$CompressFormat;

    iput-object v1, p0, Ldrd;->e:Landroid/graphics/Bitmap$CompressFormat;

    iput-object v0, p0, Ldrd;->f:Ljava/lang/String;

    goto :goto_7

    :cond_d
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    iput-object v0, p0, Ldrd;->e:Landroid/graphics/Bitmap$CompressFormat;

    const-string v0, "jpg"

    iput-object v0, p0, Ldrd;->f:Ljava/lang/String;

    goto :goto_7

    :cond_e
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    iput-object v0, p0, Ldrd;->e:Landroid/graphics/Bitmap$CompressFormat;

    const-string v0, "jpg"

    iput-object v0, p0, Ldrd;->f:Ljava/lang/String;

    goto :goto_7

    :cond_f
    invoke-direct {p0, v8}, Ldrd;->a(Landroid/graphics/Bitmap;)V

    :cond_10
    :goto_8
    iget-boolean v0, p0, Ldrd;->g:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    if-nez v0, :cond_11

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x800

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    iget-object v0, p0, Ldrd;->e:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x5a

    iget-object v2, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    :cond_11
    iget-object v0, p0, Ldrd;->l:Ldqx;

    iget-object v0, v0, Ldqx;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    if-eqz v0, :cond_12

    :try_start_3
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Ldrd;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v1}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 1048
    :cond_12
    :goto_9
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    .line 1039
    :cond_13
    :try_start_4
    iget-boolean v0, p0, Ldrd;->g:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Ldrd;->b:Lizu;

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p0, v2}, Ldrd;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v3

    if-nez v3, :cond_14

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Source files specified with content URI must also specify an output URI via the \"output\" extra."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Ldrd;->c:Landroid/net/Uri;

    invoke-direct {p0, v8}, Ldrd;->a(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Ldrd;->l:Ldqx;

    iget-object v0, v0, Ldqx;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    new-instance v4, Lidp;

    invoke-direct {v4}, Lidp;-><init>()V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v4, v1}, Lidp;->a(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :goto_a
    :try_start_6
    sget v1, Lidp;->f:I

    invoke-virtual {v4, v1}, Lidp;->d(I)Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_18

    move v1, v6

    :goto_b
    invoke-static {v1}, Lidp;->b(S)I

    move-result v5

    invoke-static {v0, v2}, Lhsf;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v10

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v9, 0x2e

    invoke-virtual {v1, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    const/4 v12, -0x1

    if-eq v9, v12, :cond_15

    const/4 v12, 0x0

    invoke-virtual {v1, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_15
    invoke-virtual {v4}, Lidp;->c()[D

    move-result-object v4

    new-instance v9, Landroid/content/ContentValues;

    const/4 v12, 0x5

    invoke-direct {v9, v12}, Landroid/content/ContentValues;-><init>(I)V

    const-string v12, "title"

    invoke-virtual {v9, v12, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "_display_name"

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "datetaken"

    const-wide/16 v12, 0x5

    add-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "date_modified"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "mime_type"

    const-string v10, "image/"

    iget-object v1, p0, Ldrd;->e:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v1}, Landroid/graphics/Bitmap$CompressFormat;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v11

    if-eqz v11, :cond_19

    invoke-virtual {v10, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_c
    invoke-virtual {v9, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "orientation"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-eqz v4, :cond_16

    const-string v1, "latitude"

    const/4 v2, 0x0

    aget-wide v10, v4, v2

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v1, "longitude"

    const/4 v2, 0x1

    aget-wide v4, v4, v2

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    :cond_16
    const-string v1, "_data"

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {v0, v9, v1}, Lhsf;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Z)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Ldrd;->c:Landroid/net/Uri;

    iget-object v1, p0, Ldrd;->c:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "bucket_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_17

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_17

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Ldrd;->d:I

    :cond_17
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Ldrd;->c:Landroid/net/Uri;

    if-nez v0, :cond_1a

    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    :cond_18
    invoke-virtual {v1}, Ljava/lang/Integer;->shortValue()S

    move-result v1

    goto/16 :goto_b

    :cond_19
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_c

    :cond_1a
    iget-object v0, p0, Ldrd;->c:Landroid/net/Uri;

    invoke-static {v0}, Ljcq;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Ldrd;->l:Ldqx;

    iget-object v0, v0, Ldqx;->a:Landroid/content/Context;

    iget-object v1, p0, Ldrd;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Ljcq;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_8

    :catch_2
    move-exception v0

    goto/16 :goto_9

    :catch_3
    move-exception v1

    goto/16 :goto_a
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 1060
    iget-object v0, p0, Ldrd;->l:Ldqx;

    invoke-virtual {v0}, Ldqx;->c()V

    .line 1061
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1062
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1063
    iget-object v1, p0, Ldrd;->i:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 1064
    const-string v1, "data"

    iget-object v2, p0, Ldrd;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1066
    :cond_0
    const-string v1, "bucket_id"

    iget v2, p0, Ldrd;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1067
    iget-object v1, p0, Ldrd;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1068
    iget-object v1, p0, Ldrd;->l:Ldqx;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Ldqx;->a(ILandroid/content/Intent;)V

    .line 1074
    :goto_0
    return-void

    .line 1070
    :cond_1
    iget-object v0, p0, Ldrd;->l:Ldqx;

    .line 1072
    iget-object v0, p0, Ldrd;->l:Ldqx;

    iget-object v0, v0, Ldqx;->a:Landroid/content/Context;

    const v1, 0x7f0a0aca

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 641
    invoke-virtual {p0}, Ldrd;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 641
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Ldrd;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 1053
    iget-object v0, p0, Ldrd;->l:Ldqx;

    iget-object v0, v0, Ldqx;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1054
    if-eqz v0, :cond_0

    const-string v1, "set-as-wallpaper"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Ldrd;->g:Z

    .line 1056
    return-void

    .line 1054
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
