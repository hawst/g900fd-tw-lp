.class public final Lmdd;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lntq;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17332
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17335
    iput-object v0, p0, Lmdd;->apiHeader:Llyr;

    .line 17338
    iput-object v0, p0, Lmdd;->a:Lntq;

    .line 17332
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 17355
    const/4 v0, 0x0

    .line 17356
    iget-object v1, p0, Lmdd;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 17357
    const/4 v0, 0x1

    iget-object v1, p0, Lmdd;->apiHeader:Llyr;

    .line 17358
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17360
    :cond_0
    iget-object v1, p0, Lmdd;->a:Lntq;

    if-eqz v1, :cond_1

    .line 17361
    const/4 v1, 0x2

    iget-object v2, p0, Lmdd;->a:Lntq;

    .line 17362
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17364
    :cond_1
    iget-object v1, p0, Lmdd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17365
    iput v0, p0, Lmdd;->ai:I

    .line 17366
    return v0
.end method

.method public a(Loxn;)Lmdd;
    .locals 2

    .prologue
    .line 17374
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 17375
    sparse-switch v0, :sswitch_data_0

    .line 17379
    iget-object v1, p0, Lmdd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 17380
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdd;->ah:Ljava/util/List;

    .line 17383
    :cond_1
    iget-object v1, p0, Lmdd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17385
    :sswitch_0
    return-object p0

    .line 17390
    :sswitch_1
    iget-object v0, p0, Lmdd;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 17391
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmdd;->apiHeader:Llyr;

    .line 17393
    :cond_2
    iget-object v0, p0, Lmdd;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17397
    :sswitch_2
    iget-object v0, p0, Lmdd;->a:Lntq;

    if-nez v0, :cond_3

    .line 17398
    new-instance v0, Lntq;

    invoke-direct {v0}, Lntq;-><init>()V

    iput-object v0, p0, Lmdd;->a:Lntq;

    .line 17400
    :cond_3
    iget-object v0, p0, Lmdd;->a:Lntq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17375
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 17343
    iget-object v0, p0, Lmdd;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 17344
    const/4 v0, 0x1

    iget-object v1, p0, Lmdd;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17346
    :cond_0
    iget-object v0, p0, Lmdd;->a:Lntq;

    if-eqz v0, :cond_1

    .line 17347
    const/4 v0, 0x2

    iget-object v1, p0, Lmdd;->a:Lntq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17349
    :cond_1
    iget-object v0, p0, Lmdd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 17351
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 17328
    invoke-virtual {p0, p1}, Lmdd;->a(Loxn;)Lmdd;

    move-result-object v0

    return-object v0
.end method
