.class public final Lesy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Leth;

.field final b:Landroid/content/Context;

.field c:Lnhl;

.field d:Lnjt;

.field e:Z

.field f:I

.field g:Z

.field h:Z

.field private final i:Landroid/view/View;

.field private final j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;ILeth;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p1, p0, Lesy;->b:Landroid/content/Context;

    .line 106
    iput-object p2, p0, Lesy;->i:Landroid/view/View;

    .line 107
    iput p3, p0, Lesy;->j:I

    .line 108
    iput-object p4, p0, Lesy;->a:Leth;

    .line 109
    const/4 v0, 0x1

    iput v0, p0, Lesy;->p:I

    .line 110
    return-void
.end method

.method private a(Landroid/widget/TextView;I)V
    .locals 4

    .prologue
    .line 577
    iget-object v0, p0, Lesy;->b:Landroid/content/Context;

    const-string v1, "plus_location"

    const-string v2, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v0, v1, v2}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 578
    iget-object v1, p0, Lesy;->b:Landroid/content/Context;

    .line 579
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, p2, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Llif;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 580
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 601
    iget-object v1, p0, Lesy;->c:Lnhl;

    iget-object v1, v1, Lnhl;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lesy;->g:Z

    if-nez v1, :cond_0

    .line 602
    iput v0, p0, Lesy;->f:I

    .line 604
    :cond_0
    iget-object v1, p0, Lesy;->i:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 605
    iget-object v1, p0, Lesy;->a:Leth;

    iget-object v2, p0, Lesy;->d:Lnjt;

    iget-object v2, v2, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->a:Lnjb;

    iget-object v2, v2, Lnjb;->a:Ljava/lang/String;

    iget v3, p0, Lesy;->f:I

    iget-boolean v4, p0, Lesy;->g:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lesy;->o:Z

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-interface {v1, v2, v3, v0}, Leth;->a(Ljava/lang/String;IZ)V

    .line 607
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 621
    iget-object v0, p0, Lesy;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 622
    iget-object v0, p0, Lesy;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 623
    return-void
.end method


# virtual methods
.method a()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/16 v12, 0x8

    const/4 v5, 0x1

    const/4 v1, 0x2

    const/4 v6, 0x0

    .line 188
    iget v4, p0, Lesy;->p:I

    .line 189
    iget v0, p0, Lesy;->p:I

    sget-object v3, Letg;->a:[I

    add-int/lit8 v0, v0, -0x1

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x6

    move v3, v0

    .line 191
    :goto_0
    const/4 v0, 0x0

    .line 192
    if-ne v4, v1, :cond_7

    .line 193
    iget-object v0, p0, Lesy;->i:Landroid/view/View;

    const v4, 0x7f1002c8

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    .line 197
    :goto_1
    if-eqz v4, :cond_0

    .line 198
    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    .line 200
    :cond_0
    iput v3, p0, Lesy;->p:I

    .line 201
    sget-object v0, Letg;->a:[I

    add-int/lit8 v3, v3, -0x1

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_1

    .line 218
    :goto_2
    return-void

    .line 189
    :pswitch_1
    iget-boolean v0, p0, Lesy;->k:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lesy;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "last_seen_onboarding"

    const-wide/16 v8, -0x1

    invoke-interface {v0, v3, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v8, v10, v8

    const-wide v10, 0x9a7ec800L

    cmp-long v0, v8, v10

    if-gtz v0, :cond_1

    move v0, v5

    :goto_3
    if-nez v0, :cond_2

    move v3, v1

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_3

    :cond_2
    :pswitch_2
    iget-boolean v0, p0, Lesy;->n:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    move v3, v0

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lesy;->l:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    move v3, v0

    goto :goto_0

    :cond_4
    const/4 v0, 0x4

    move v3, v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lesy;->d()Z

    move-result v0

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lesy;->m:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    move v3, v0

    goto :goto_0

    :cond_5
    move v3, v1

    goto :goto_0

    :cond_6
    const/4 v0, 0x6

    move v3, v0

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    move v3, v0

    goto :goto_0

    .line 194
    :cond_7
    const/4 v7, 0x3

    if-ne v4, v7, :cond_d

    .line 195
    iget-object v0, p0, Lesy;->i:Landroid/view/View;

    const v4, 0x7f1000fb

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    .line 203
    :pswitch_5
    sget-object v0, Lhmv;->dp:Lhmv;

    sget-object v1, Lhmw;->W:Lhmw;

    invoke-virtual {p0, v0, v1}, Lesy;->a(Lhmv;Lhmw;)V

    iput-boolean v5, p0, Lesy;->m:Z

    invoke-direct {p0}, Lesy;->f()V

    iget-object v0, p0, Lesy;->i:Landroid/view/View;

    const v1, 0x7f1002c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f1002c4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a0a94

    invoke-direct {p0, v0, v2}, Lesy;->a(Landroid/widget/TextView;I)V

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f1002c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lesz;

    invoke-direct {v2, v1}, Lesz;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f1002c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Leta;

    invoke-direct {v1, p0}, Leta;-><init>(Lesy;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 206
    :pswitch_6
    iput-boolean v5, p0, Lesy;->n:Z

    if-eqz v4, :cond_8

    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    invoke-direct {p0}, Lesy;->f()V

    iget-object v0, p0, Lesy;->i:Landroid/view/View;

    const v2, 0x7f1000fb

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f1002b3

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lesy;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v2, p0, Lesy;->d:Lnjt;

    iget-object v2, v2, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->d:Lnip;

    iget v2, v2, Lnip;->b:I

    packed-switch v2, :pswitch_data_2

    const v2, 0x7f0a0aa5

    :goto_4
    new-array v7, v5, [Ljava/lang/Object;

    iget-object v8, p0, Lesy;->d:Lnjt;

    iget-object v8, v8, Lnjt;->g:Ljava/lang/String;

    aput-object v8, v7, v6

    invoke-virtual {v3, v2, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f1002b8

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lesy;->d:Lnjt;

    iget-object v2, v2, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->m:Lnik;

    iget-object v2, v2, Lnik;->a:[Lnij;

    aget-object v2, v2, v6

    iget-object v2, v2, Lnij;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lesy;->c:Lnhl;

    iget-object v0, v0, Lnhl;->a:[Lnij;

    array-length v0, v0

    if-lez v0, :cond_9

    const v0, 0x7f1002b9

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lesy;->c:Lnhl;

    iget-object v2, v2, Lnhl;->a:[Lnij;

    aget-object v2, v2, v6

    iget-object v2, v2, Lnij;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    const v0, 0x7f1002b4

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    iget-object v2, p0, Lesy;->b:Landroid/content/Context;

    const-class v3, Lhei;

    invoke-static {v2, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhei;

    iget v3, p0, Lesy;->j:I

    invoke-interface {v2, v3}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v3, "profile_photo_url"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const v2, 0x7f1002b5

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/location/MarkerIconView;

    const v3, 0x7f1002b6

    invoke-virtual {v0, v3}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/social/location/MarkerIconView;

    invoke-virtual {v2, v5, v6}, Lcom/google/android/libraries/social/location/MarkerIconView;->a(ILjava/lang/String;)V

    invoke-virtual {v3, v1, v6}, Lcom/google/android/libraries/social/location/MarkerIconView;->a(ILjava/lang/String;)V

    const v1, 0x7f1002ba

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/locations/LocationSharingRadioGroup;

    iget-object v2, p0, Lesy;->d:Lnjt;

    iget-object v2, v2, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->a:Lnjb;

    iget-object v2, v2, Lnjb;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/locations/LocationSharingRadioGroup;->a(Ljava/lang/String;)V

    new-instance v2, Letb;

    invoke-direct {v2, p0, v0, v4}, Letb;-><init>(Lesy;Landroid/widget/ViewFlipper;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/locations/LocationSharingRadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v0, p0, Lesy;->d:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->m:Lnik;

    iget-object v0, v0, Lnik;->a:[Lnij;

    invoke-static {v0}, Liuo;->a([Lnij;)Lnij;

    move-result-object v0

    iget v0, v0, Lnij;->b:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/locations/LocationSharingRadioGroup;->check(I)V

    const v0, 0x7f1002a4

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Letc;

    invoke-direct {v1, p0}, Letc;-><init>(Lesy;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f1002a6

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Letd;

    invoke-direct {v1, p0, v4}, Letd;-><init>(Lesy;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    :pswitch_7
    const v2, 0x7f0a0aa3

    goto/16 :goto_4

    :pswitch_8
    const v2, 0x7f0a0aa4

    goto/16 :goto_4

    .line 209
    :pswitch_9
    iget-object v0, p0, Lesy;->i:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 212
    :pswitch_a
    sget-object v0, Lhmv;->dq:Lhmv;

    sget-object v3, Lhmw;->X:Lhmw;

    invoke-virtual {p0, v0, v3}, Lesy;->a(Lhmv;Lhmw;)V

    iget-object v0, p0, Lesy;->i:Landroid/view/View;

    const v3, 0x7f1002c9

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v0, 0x7f100118

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v3, p0, Lesy;->e:Z

    if-eqz v3, :cond_a

    const v3, 0x7f0a0a9a

    :goto_5
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-boolean v0, p0, Lesy;->e:Z

    if-eqz v0, :cond_b

    const v0, 0x7f0a0a9c

    move v3, v0

    :goto_6
    const v0, 0x7f1002c6

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0, v3}, Lesy;->a(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lesy;->b:Landroid/content/Context;

    const-class v3, Lhei;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v3, p0, Lesy;->j:I

    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lesy;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v8, 0x7f0a0a9d

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v8, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f1002c7

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Letf;

    invoke-direct {v0, p0}, Letf;-><init>(Lesy;)V

    const v3, 0x7f1002a6

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f1002a4

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    if-nez v4, :cond_c

    invoke-direct {p0}, Lesy;->f()V

    iget-object v9, p0, Lesy;->i:Landroid/view/View;

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v6, -0x40800000    # -1.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setStartOffset(J)V

    invoke-virtual {v9, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_2

    :cond_a
    const v3, 0x7f0a0a99

    goto/16 :goto_5

    :cond_b
    const v0, 0x7f0a0a9b

    move v3, v0

    goto/16 :goto_6

    :cond_c
    invoke-virtual {v4, v12}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 215
    :pswitch_b
    invoke-virtual {p0}, Lesy;->b()V

    goto/16 :goto_2

    :cond_d
    move-object v4, v0

    goto/16 :goto_1

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 201
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 206
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method a(Lhmv;Lhmw;)V
    .locals 4

    .prologue
    .line 630
    iget-object v0, p0, Lesy;->b:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lesy;->b:Landroid/content/Context;

    iget v3, p0, Lesy;->j:I

    invoke-direct {v1, v2, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 631
    invoke-virtual {v1, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-virtual {v1, p2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 632
    return-void
.end method

.method public a(Llwa;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 116
    iget-object v0, p0, Lesy;->a:Leth;

    invoke-interface {v0}, Leth;->a()V

    .line 117
    sget-object v0, Lhmv;->dg:Lhmv;

    sget-object v3, Lhmw;->V:Lhmw;

    invoke-virtual {p0, v0, v3}, Lesy;->a(Lhmv;Lhmw;)V

    .line 118
    if-eqz p1, :cond_6

    .line 119
    iget-object v4, p1, Llwa;->a:[Llwc;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_6

    aget-object v0, v4, v3

    .line 120
    iget-object v6, v0, Llwc;->b:Ljava/lang/String;

    .line 121
    const-string v7, "locationSharingType"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 122
    iget-object v0, v0, Llwc;->c:Llwb;

    iget-object v0, v0, Llwb;->d:Llwa;

    iget-object v0, v0, Llwa;->a:[Llwc;

    aget-object v0, v0, v2

    iget-object v0, v0, Llwc;->c:Llwb;

    iget-object v0, v0, Llwb;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lesy;->k:Z

    .line 119
    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 122
    goto :goto_1

    .line 123
    :cond_2
    const-string v7, "emptyAcl"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 124
    iget-object v0, v0, Llwc;->c:Llwb;

    iget-object v0, v0, Llwb;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lesy;->o:Z

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    .line 125
    :cond_4
    const-string v7, "recipientToSenderSharingType"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 126
    iget-object v0, v0, Llwc;->c:Llwb;

    iget-object v0, v0, Llwb;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lesy;->f:I

    .line 127
    iget v0, p0, Lesy;->f:I

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lesy;->h:Z

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_4

    .line 132
    :cond_6
    invoke-virtual {p0}, Lesy;->a()V

    .line 133
    return-void
.end method

.method public a(Lnhl;Lnjt;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 143
    iget v2, p0, Lesy;->p:I

    if-ne v2, v3, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    iget-object v2, p2, Lnjt;->e:Lnkd;

    if-eqz v2, :cond_2

    iget-object v2, p2, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->m:Lnik;

    if-eqz v2, :cond_2

    iget-object v2, p2, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->m:Lnik;

    iget-object v2, v2, Lnik;->a:[Lnij;

    if-eqz v2, :cond_2

    iget-object v2, p2, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->m:Lnik;

    iget-object v2, v2, Lnik;->a:[Lnij;

    array-length v2, v2

    if-eqz v2, :cond_2

    iget-object v2, p2, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->a:Lnjb;

    if-eqz v2, :cond_2

    iget-object v2, p2, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->a:Lnjb;

    iget-object v2, v2, Lnjb;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    :cond_2
    move v2, v0

    :goto_1
    if-nez v2, :cond_5

    move v2, v0

    :goto_2
    if-nez v2, :cond_6

    .line 149
    :cond_3
    iput v3, p0, Lesy;->p:I

    iget-object v0, p0, Lesy;->i:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lesy;->a:Leth;

    const/4 v2, 0x0

    invoke-interface {v0, v2, v1, v1}, Leth;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_4
    move v2, v1

    .line 148
    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_2

    .line 153
    :cond_6
    iput-object p1, p0, Lesy;->c:Lnhl;

    .line 154
    iput-object p2, p0, Lesy;->d:Lnjt;

    .line 155
    iput-boolean p3, p0, Lesy;->e:Z

    .line 156
    iput-boolean v0, p0, Lesy;->l:Z

    .line 159
    iget v2, p0, Lesy;->p:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p0}, Lesy;->a()V

    goto :goto_0

    :cond_7
    move v0, v1

    .line 159
    goto :goto_3
.end method

.method public b()V
    .locals 4

    .prologue
    .line 587
    iget-object v0, p0, Lesy;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_seen_onboarding"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 588
    invoke-direct {p0}, Lesy;->e()V

    .line 589
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 597
    invoke-direct {p0}, Lesy;->e()V

    .line 598
    return-void
.end method

.method d()Z
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lesy;->c:Lnhl;

    iget-object v0, v0, Lnhl;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lesy;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
