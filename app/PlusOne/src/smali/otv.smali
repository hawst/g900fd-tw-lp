.class public final Lotv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 640
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 661
    const/4 v0, 0x0

    .line 662
    iget-object v1, p0, Lotv;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 663
    const/4 v0, 0x1

    iget-object v1, p0, Lotv;->a:Ljava/lang/String;

    .line 664
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 666
    :cond_0
    iget-object v1, p0, Lotv;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 667
    const/4 v1, 0x2

    iget-object v2, p0, Lotv;->b:Ljava/lang/Float;

    .line 668
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 670
    :cond_1
    iget-object v1, p0, Lotv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 671
    iput v0, p0, Lotv;->ai:I

    .line 672
    return v0
.end method

.method public a(Loxn;)Lotv;
    .locals 2

    .prologue
    .line 680
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 681
    sparse-switch v0, :sswitch_data_0

    .line 685
    iget-object v1, p0, Lotv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 686
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lotv;->ah:Ljava/util/List;

    .line 689
    :cond_1
    iget-object v1, p0, Lotv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 691
    :sswitch_0
    return-object p0

    .line 696
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lotv;->a:Ljava/lang/String;

    goto :goto_0

    .line 700
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lotv;->b:Ljava/lang/Float;

    goto :goto_0

    .line 681
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 649
    iget-object v0, p0, Lotv;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 650
    const/4 v0, 0x1

    iget-object v1, p0, Lotv;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 652
    :cond_0
    iget-object v0, p0, Lotv;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 653
    const/4 v0, 0x2

    iget-object v1, p0, Lotv;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 655
    :cond_1
    iget-object v0, p0, Lotv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 657
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 636
    invoke-virtual {p0, p1}, Lotv;->a(Loxn;)Lotv;

    move-result-object v0

    return-object v0
.end method
