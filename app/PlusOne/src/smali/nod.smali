.class public final Lnod;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnoc;

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 404
    invoke-direct {p0}, Loxq;-><init>()V

    .line 414
    sget-object v0, Lnoc;->a:[Lnoc;

    iput-object v0, p0, Lnod;->a:[Lnoc;

    .line 417
    const/high16 v0, -0x80000000

    iput v0, p0, Lnod;->b:I

    .line 404
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 438
    .line 439
    iget-object v1, p0, Lnod;->a:[Lnoc;

    if-eqz v1, :cond_1

    .line 440
    iget-object v2, p0, Lnod;->a:[Lnoc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 441
    if-eqz v4, :cond_0

    .line 442
    const/4 v5, 0x1

    .line 443
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 440
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 447
    :cond_1
    iget v1, p0, Lnod;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 448
    const/4 v1, 0x2

    iget v2, p0, Lnod;->b:I

    .line 449
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 451
    :cond_2
    iget-object v1, p0, Lnod;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 452
    iput v0, p0, Lnod;->ai:I

    .line 453
    return v0
.end method

.method public a(Loxn;)Lnod;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 461
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 462
    sparse-switch v0, :sswitch_data_0

    .line 466
    iget-object v2, p0, Lnod;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 467
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnod;->ah:Ljava/util/List;

    .line 470
    :cond_1
    iget-object v2, p0, Lnod;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    :sswitch_0
    return-object p0

    .line 477
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 478
    iget-object v0, p0, Lnod;->a:[Lnoc;

    if-nez v0, :cond_3

    move v0, v1

    .line 479
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnoc;

    .line 480
    iget-object v3, p0, Lnod;->a:[Lnoc;

    if-eqz v3, :cond_2

    .line 481
    iget-object v3, p0, Lnod;->a:[Lnoc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 483
    :cond_2
    iput-object v2, p0, Lnod;->a:[Lnoc;

    .line 484
    :goto_2
    iget-object v2, p0, Lnod;->a:[Lnoc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 485
    iget-object v2, p0, Lnod;->a:[Lnoc;

    new-instance v3, Lnoc;

    invoke-direct {v3}, Lnoc;-><init>()V

    aput-object v3, v2, v0

    .line 486
    iget-object v2, p0, Lnod;->a:[Lnoc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 487
    invoke-virtual {p1}, Loxn;->a()I

    .line 484
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 478
    :cond_3
    iget-object v0, p0, Lnod;->a:[Lnoc;

    array-length v0, v0

    goto :goto_1

    .line 490
    :cond_4
    iget-object v2, p0, Lnod;->a:[Lnoc;

    new-instance v3, Lnoc;

    invoke-direct {v3}, Lnoc;-><init>()V

    aput-object v3, v2, v0

    .line 491
    iget-object v2, p0, Lnod;->a:[Lnoc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 495
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 496
    if-eq v0, v4, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-ne v0, v2, :cond_6

    .line 500
    :cond_5
    iput v0, p0, Lnod;->b:I

    goto :goto_0

    .line 502
    :cond_6
    iput v4, p0, Lnod;->b:I

    goto :goto_0

    .line 462
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 422
    iget-object v0, p0, Lnod;->a:[Lnoc;

    if-eqz v0, :cond_1

    .line 423
    iget-object v1, p0, Lnod;->a:[Lnoc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 424
    if-eqz v3, :cond_0

    .line 425
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 423
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 429
    :cond_1
    iget v0, p0, Lnod;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 430
    const/4 v0, 0x2

    iget v1, p0, Lnod;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 432
    :cond_2
    iget-object v0, p0, Lnod;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 434
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 400
    invoke-virtual {p0, p1}, Lnod;->a(Loxn;)Lnod;

    move-result-object v0

    return-object v0
.end method
