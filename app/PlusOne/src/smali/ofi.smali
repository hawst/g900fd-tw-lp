.class public final Lofi;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lofi;


# instance fields
.field public b:Lofv;

.field public c:[Lltf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8771
    const/4 v0, 0x0

    new-array v0, v0, [Lofi;

    sput-object v0, Lofi;->a:[Lofi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8772
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8775
    const/4 v0, 0x0

    iput-object v0, p0, Lofi;->b:Lofv;

    .line 8778
    sget-object v0, Lltf;->a:[Lltf;

    iput-object v0, p0, Lofi;->c:[Lltf;

    .line 8772
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8799
    .line 8800
    iget-object v0, p0, Lofi;->b:Lofv;

    if-eqz v0, :cond_2

    .line 8801
    const/4 v0, 0x1

    iget-object v2, p0, Lofi;->b:Lofv;

    .line 8802
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8804
    :goto_0
    iget-object v2, p0, Lofi;->c:[Lltf;

    if-eqz v2, :cond_1

    .line 8805
    iget-object v2, p0, Lofi;->c:[Lltf;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 8806
    if-eqz v4, :cond_0

    .line 8807
    const/4 v5, 0x2

    .line 8808
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 8805
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8812
    :cond_1
    iget-object v1, p0, Lofi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8813
    iput v0, p0, Lofi;->ai:I

    .line 8814
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lofi;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8822
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8823
    sparse-switch v0, :sswitch_data_0

    .line 8827
    iget-object v2, p0, Lofi;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 8828
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lofi;->ah:Ljava/util/List;

    .line 8831
    :cond_1
    iget-object v2, p0, Lofi;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8833
    :sswitch_0
    return-object p0

    .line 8838
    :sswitch_1
    iget-object v0, p0, Lofi;->b:Lofv;

    if-nez v0, :cond_2

    .line 8839
    new-instance v0, Lofv;

    invoke-direct {v0}, Lofv;-><init>()V

    iput-object v0, p0, Lofi;->b:Lofv;

    .line 8841
    :cond_2
    iget-object v0, p0, Lofi;->b:Lofv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8845
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 8846
    iget-object v0, p0, Lofi;->c:[Lltf;

    if-nez v0, :cond_4

    move v0, v1

    .line 8847
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lltf;

    .line 8848
    iget-object v3, p0, Lofi;->c:[Lltf;

    if-eqz v3, :cond_3

    .line 8849
    iget-object v3, p0, Lofi;->c:[Lltf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 8851
    :cond_3
    iput-object v2, p0, Lofi;->c:[Lltf;

    .line 8852
    :goto_2
    iget-object v2, p0, Lofi;->c:[Lltf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 8853
    iget-object v2, p0, Lofi;->c:[Lltf;

    new-instance v3, Lltf;

    invoke-direct {v3}, Lltf;-><init>()V

    aput-object v3, v2, v0

    .line 8854
    iget-object v2, p0, Lofi;->c:[Lltf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 8855
    invoke-virtual {p1}, Loxn;->a()I

    .line 8852
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 8846
    :cond_4
    iget-object v0, p0, Lofi;->c:[Lltf;

    array-length v0, v0

    goto :goto_1

    .line 8858
    :cond_5
    iget-object v2, p0, Lofi;->c:[Lltf;

    new-instance v3, Lltf;

    invoke-direct {v3}, Lltf;-><init>()V

    aput-object v3, v2, v0

    .line 8859
    iget-object v2, p0, Lofi;->c:[Lltf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8823
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 8783
    iget-object v0, p0, Lofi;->b:Lofv;

    if-eqz v0, :cond_0

    .line 8784
    const/4 v0, 0x1

    iget-object v1, p0, Lofi;->b:Lofv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8786
    :cond_0
    iget-object v0, p0, Lofi;->c:[Lltf;

    if-eqz v0, :cond_2

    .line 8787
    iget-object v1, p0, Lofi;->c:[Lltf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 8788
    if-eqz v3, :cond_1

    .line 8789
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 8787
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8793
    :cond_2
    iget-object v0, p0, Lofi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8795
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8768
    invoke-virtual {p0, p1}, Lofi;->a(Loxn;)Lofi;

    move-result-object v0

    return-object v0
.end method
