.class public final Lmqg;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 629
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 650
    const/4 v0, 0x0

    .line 651
    iget-object v1, p0, Lmqg;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 652
    const/4 v0, 0x1

    iget-object v1, p0, Lmqg;->a:Ljava/lang/Boolean;

    .line 653
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 655
    :cond_0
    iget-object v1, p0, Lmqg;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 656
    const/4 v1, 0x2

    iget-object v2, p0, Lmqg;->b:Ljava/lang/Boolean;

    .line 657
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 659
    :cond_1
    iget-object v1, p0, Lmqg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 660
    iput v0, p0, Lmqg;->ai:I

    .line 661
    return v0
.end method

.method public a(Loxn;)Lmqg;
    .locals 2

    .prologue
    .line 669
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 670
    sparse-switch v0, :sswitch_data_0

    .line 674
    iget-object v1, p0, Lmqg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 675
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmqg;->ah:Ljava/util/List;

    .line 678
    :cond_1
    iget-object v1, p0, Lmqg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 680
    :sswitch_0
    return-object p0

    .line 685
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqg;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 689
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqg;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 670
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Lmqg;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 639
    const/4 v0, 0x1

    iget-object v1, p0, Lmqg;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 641
    :cond_0
    iget-object v0, p0, Lmqg;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 642
    const/4 v0, 0x2

    iget-object v1, p0, Lmqg;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 644
    :cond_1
    iget-object v0, p0, Lmqg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 646
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 625
    invoke-virtual {p0, p1}, Lmqg;->a(Loxn;)Lmqg;

    move-result-object v0

    return-object v0
.end method
