.class public final enum Lcdz;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcdz;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcdz;

.field public static final enum b:Lcdz;

.field public static final enum c:Lcdz;

.field public static final enum d:Lcdz;

.field public static final enum e:Lcdz;

.field public static final enum f:Lcdz;

.field public static final enum g:Lcdz;

.field public static final enum h:Lcdz;

.field public static final enum i:Lcdz;

.field public static final enum j:Lcdz;

.field public static final enum k:Lcdz;

.field public static final enum l:Lcdz;

.field private static final synthetic n:[Lcdz;


# instance fields
.field private final m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 43
    new-instance v0, Lcdz;

    const-string v1, "MAIN"

    const-string v2, "Main"

    invoke-direct {v0, v1, v4, v2}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->a:Lcdz;

    .line 44
    new-instance v0, Lcdz;

    const-string v1, "PICKER"

    const-string v2, "Picker"

    invoke-direct {v0, v1, v5, v2}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->b:Lcdz;

    .line 45
    new-instance v0, Lcdz;

    const-string v1, "SAVE_VIDEO_SIZE_DIALOG"

    const-string v2, "SaveVideoSizeDialog"

    invoke-direct {v0, v1, v6, v2}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->c:Lcdz;

    .line 46
    new-instance v0, Lcdz;

    const-string v1, "DURATION_DIALOG"

    const-string v2, "DurationDialog"

    invoke-direct {v0, v1, v7, v2}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->d:Lcdz;

    .line 47
    new-instance v0, Lcdz;

    const-string v1, "ADD_FROM_GALLERY"

    const-string v2, "AddFromGallery"

    invoke-direct {v0, v1, v8, v2}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->e:Lcdz;

    .line 48
    new-instance v0, Lcdz;

    const-string v1, "SETTINGS"

    const/4 v2, 0x5

    const-string v3, "Settings"

    invoke-direct {v0, v1, v2, v3}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->f:Lcdz;

    .line 49
    new-instance v0, Lcdz;

    const-string v1, "STORYBOARD"

    const/4 v2, 0x6

    const-string v3, "Storyboard"

    invoke-direct {v0, v1, v2, v3}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->g:Lcdz;

    .line 50
    new-instance v0, Lcdz;

    const-string v1, "CLIP"

    const/4 v2, 0x7

    const-string v3, "Clip"

    invoke-direct {v0, v1, v2, v3}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->h:Lcdz;

    .line 51
    new-instance v0, Lcdz;

    const-string v1, "LATE_ANALYSIS_DIALOG"

    const/16 v2, 0x8

    const-string v3, "LateAnalysis"

    invoke-direct {v0, v1, v2, v3}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->i:Lcdz;

    .line 52
    new-instance v0, Lcdz;

    const-string v1, "UNSAVED_CHANGES_DIALOG"

    const/16 v2, 0x9

    const-string v3, "UnsavedChangesDialog"

    invoke-direct {v0, v1, v2, v3}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->j:Lcdz;

    .line 53
    new-instance v0, Lcdz;

    const-string v1, "CLOUD_OVERWRITE_CONFIRMATION"

    const/16 v2, 0xa

    const-string v3, "CloudOverwrite"

    invoke-direct {v0, v1, v2, v3}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->k:Lcdz;

    .line 54
    new-instance v0, Lcdz;

    const-string v1, "DOWNLOAD_RETRY_DIALOG"

    const/16 v2, 0xb

    const-string v3, "DownloadRetryDialog"

    invoke-direct {v0, v1, v2, v3}, Lcdz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcdz;->l:Lcdz;

    .line 42
    const/16 v0, 0xc

    new-array v0, v0, [Lcdz;

    sget-object v1, Lcdz;->a:Lcdz;

    aput-object v1, v0, v4

    sget-object v1, Lcdz;->b:Lcdz;

    aput-object v1, v0, v5

    sget-object v1, Lcdz;->c:Lcdz;

    aput-object v1, v0, v6

    sget-object v1, Lcdz;->d:Lcdz;

    aput-object v1, v0, v7

    sget-object v1, Lcdz;->e:Lcdz;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcdz;->f:Lcdz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcdz;->g:Lcdz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcdz;->h:Lcdz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcdz;->i:Lcdz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcdz;->j:Lcdz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcdz;->k:Lcdz;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcdz;->l:Lcdz;

    aput-object v2, v0, v1

    sput-object v0, Lcdz;->n:[Lcdz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    iput-object p3, p0, Lcdz;->m:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcdz;
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcdz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcdz;

    return-object v0
.end method

.method public static values()[Lcdz;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcdz;->n:[Lcdz;

    invoke-virtual {v0}, [Lcdz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcdz;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcdz;->m:Ljava/lang/String;

    return-object v0
.end method
