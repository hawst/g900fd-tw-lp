.class public final Liwk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Liwi;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Liwk;->a:Ljava/util/List;

    .line 22
    iput-object p1, p0, Liwk;->b:Landroid/content/Context;

    .line 23
    iput p2, p0, Liwk;->c:I

    .line 24
    return-void
.end method

.method private c()Liwg;
    .locals 3

    .prologue
    .line 56
    new-instance v0, Liwg;

    invoke-direct {v0}, Liwg;-><init>()V

    iget v1, p0, Liwk;->c:I

    invoke-virtual {v0, v1}, Liwg;->a(I)Liwg;

    move-result-object v2

    .line 57
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Liwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 58
    iget-object v0, p0, Liwk;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v2, v0}, Liwg;->a(Ljava/lang/Class;)Liwg;

    .line 57
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 60
    :cond_0
    return-object v2
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Liwk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Liwi;",
            ">;)",
            "Liwk;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Liwk;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    return-object p0
.end method

.method public a()Z
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Liwk;->b:Landroid/content/Context;

    const-class v1, Liwc;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwc;

    .line 39
    invoke-direct {p0}, Liwk;->c()Liwg;

    move-result-object v1

    iget v2, p0, Liwk;->c:I

    invoke-interface {v0, v1, v2}, Liwc;->a(Liwg;I)Z

    move-result v0

    return v0
.end method

.method public b()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 46
    new-instance v1, Livy;

    iget-object v0, p0, Liwk;->b:Landroid/content/Context;

    invoke-direct {v1, v0}, Livy;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-direct {p0}, Liwk;->c()Liwg;

    move-result-object v0

    invoke-virtual {v1, v0}, Livy;->a(Liwg;)Livy;

    .line 48
    iget-object v0, p0, Liwk;->b:Landroid/content/Context;

    const-class v2, Lkex;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkex;

    iget-object v2, p0, Liwk;->b:Landroid/content/Context;

    iget v3, p0, Liwk;->c:I

    .line 49
    invoke-interface {v0, v2, v3}, Lkex;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 50
    const v2, 0x18000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 51
    invoke-virtual {v1, v0}, Livy;->a(Landroid/content/Intent;)Livy;

    .line 52
    invoke-virtual {v1}, Livy;->a()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
