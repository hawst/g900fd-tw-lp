.class public final Lltz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpec;

.field public b:Lpzw;

.field public c:Lqbp;

.field private d:[Ljava/lang/Integer;

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/high16 v1, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v0, p0, Lltz;->a:Lpec;

    .line 16
    iput-object v0, p0, Lltz;->b:Lpzw;

    .line 19
    iput-object v0, p0, Lltz;->c:Lqbp;

    .line 22
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lltz;->d:[Ljava/lang/Integer;

    .line 25
    iput v1, p0, Lltz;->e:I

    .line 28
    iput v1, p0, Lltz;->f:I

    .line 31
    iput v1, p0, Lltz;->g:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v4, -0x80000000

    .line 65
    .line 66
    iget v0, p0, Lltz;->e:I

    if-eq v0, v4, :cond_7

    .line 67
    const/4 v0, 0x1

    iget v2, p0, Lltz;->e:I

    .line 68
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 70
    :goto_0
    iget v2, p0, Lltz;->f:I

    if-eq v2, v4, :cond_0

    .line 71
    const/4 v2, 0x2

    iget v3, p0, Lltz;->f:I

    .line 72
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 74
    :cond_0
    iget v2, p0, Lltz;->g:I

    if-eq v2, v4, :cond_1

    .line 75
    const/4 v2, 0x3

    iget v3, p0, Lltz;->g:I

    .line 76
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 78
    :cond_1
    iget-object v2, p0, Lltz;->a:Lpec;

    if-eqz v2, :cond_2

    .line 79
    const/4 v2, 0x4

    iget-object v3, p0, Lltz;->a:Lpec;

    .line 80
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    :cond_2
    iget-object v2, p0, Lltz;->b:Lpzw;

    if-eqz v2, :cond_3

    .line 83
    const/4 v2, 0x5

    iget-object v3, p0, Lltz;->b:Lpzw;

    .line 84
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    :cond_3
    iget-object v2, p0, Lltz;->c:Lqbp;

    if-eqz v2, :cond_4

    .line 87
    const/4 v2, 0x6

    iget-object v3, p0, Lltz;->c:Lqbp;

    .line 88
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 90
    :cond_4
    iget-object v2, p0, Lltz;->d:[Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lltz;->d:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 92
    iget-object v3, p0, Lltz;->d:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 94
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 92
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 96
    :cond_5
    add-int/2addr v0, v2

    .line 97
    iget-object v1, p0, Lltz;->d:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 99
    :cond_6
    iget-object v1, p0, Lltz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    iput v0, p0, Lltz;->ai:I

    .line 101
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lltz;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 109
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 110
    sparse-switch v0, :sswitch_data_0

    .line 114
    iget-object v1, p0, Lltz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 115
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lltz;->ah:Ljava/util/List;

    .line 118
    :cond_1
    iget-object v1, p0, Lltz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    :sswitch_0
    return-object p0

    .line 125
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 126
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-eq v0, v7, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x64

    if-ne v0, v1, :cond_3

    .line 137
    :cond_2
    iput v0, p0, Lltz;->e:I

    goto :goto_0

    .line 139
    :cond_3
    iput v3, p0, Lltz;->e:I

    goto :goto_0

    .line 144
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 145
    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    if-eq v0, v7, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0x50

    if-eq v0, v1, :cond_4

    const/16 v1, 0x51

    if-eq v0, v1, :cond_4

    const/16 v1, 0x64

    if-eq v0, v1, :cond_4

    const/16 v1, 0x65

    if-eq v0, v1, :cond_4

    const/16 v1, 0x66

    if-eq v0, v1, :cond_4

    const/16 v1, 0x67

    if-eq v0, v1, :cond_4

    const/16 v1, 0x68

    if-eq v0, v1, :cond_4

    const/16 v1, 0x77

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_4

    const/16 v1, 0x69

    if-eq v0, v1, :cond_4

    const/16 v1, 0x81

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x70

    if-eq v0, v1, :cond_4

    const/16 v1, 0x71

    if-eq v0, v1, :cond_4

    const/16 v1, 0x72

    if-eq v0, v1, :cond_4

    const/16 v1, 0x73

    if-eq v0, v1, :cond_4

    const/16 v1, 0x74

    if-eq v0, v1, :cond_4

    const/16 v1, 0x76

    if-eq v0, v1, :cond_4

    const/16 v1, 0x78

    if-eq v0, v1, :cond_4

    const/16 v1, 0x79

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x80

    if-eq v0, v1, :cond_4

    const/16 v1, 0x82

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x83

    if-eq v0, v1, :cond_4

    const/16 v1, 0x84

    if-eq v0, v1, :cond_4

    const/16 v1, 0x85

    if-eq v0, v1, :cond_4

    const/16 v1, 0x86

    if-eq v0, v1, :cond_4

    const/16 v1, 0x87

    if-eq v0, v1, :cond_4

    const/16 v1, 0x88

    if-eq v0, v1, :cond_4

    const/16 v1, 0x89

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x90

    if-eq v0, v1, :cond_4

    const/16 v1, 0x91

    if-eq v0, v1, :cond_4

    const/16 v1, 0x92

    if-eq v0, v1, :cond_4

    const/16 v1, 0x93

    if-eq v0, v1, :cond_4

    const/16 v1, 0x94

    if-eq v0, v1, :cond_4

    const/16 v1, 0x95

    if-eq v0, v1, :cond_4

    const/16 v1, 0x96

    if-eq v0, v1, :cond_4

    const/16 v1, 0x97

    if-eq v0, v1, :cond_4

    const/16 v1, 0x98

    if-eq v0, v1, :cond_4

    const/16 v1, 0x99

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xaa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xab

    if-eq v0, v1, :cond_4

    const/16 v1, 0xac

    if-eq v0, v1, :cond_4

    const/16 v1, 0xad

    if-eq v0, v1, :cond_4

    const/16 v1, 0xae

    if-eq v0, v1, :cond_4

    const/16 v1, 0xaf

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb0

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xca

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xce

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd0

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x75

    if-eq v0, v1, :cond_4

    const/16 v1, 0x190

    if-eq v0, v1, :cond_4

    const/16 v1, 0x191

    if-eq v0, v1, :cond_4

    const/16 v1, 0x192

    if-eq v0, v1, :cond_4

    const/16 v1, 0x193

    if-eq v0, v1, :cond_4

    const/16 v1, 0x194

    if-eq v0, v1, :cond_4

    const/16 v1, 0x195

    if-eq v0, v1, :cond_4

    const/16 v1, 0x196

    if-eq v0, v1, :cond_4

    const/16 v1, 0x199

    if-eq v0, v1, :cond_4

    const/16 v1, 0x197

    if-eq v0, v1, :cond_4

    const/16 v1, 0x198

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3e9

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ea

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ec

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ed

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ee

    if-ne v0, v1, :cond_5

    .line 281
    :cond_4
    iput v0, p0, Lltz;->f:I

    goto/16 :goto_0

    .line 283
    :cond_5
    iput v3, p0, Lltz;->f:I

    goto/16 :goto_0

    .line 288
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 289
    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    const/16 v1, 0xa

    if-ne v0, v1, :cond_7

    .line 293
    :cond_6
    iput v0, p0, Lltz;->g:I

    goto/16 :goto_0

    .line 295
    :cond_7
    iput v3, p0, Lltz;->g:I

    goto/16 :goto_0

    .line 300
    :sswitch_4
    iget-object v0, p0, Lltz;->a:Lpec;

    if-nez v0, :cond_8

    .line 301
    new-instance v0, Lpec;

    invoke-direct {v0}, Lpec;-><init>()V

    iput-object v0, p0, Lltz;->a:Lpec;

    .line 303
    :cond_8
    iget-object v0, p0, Lltz;->a:Lpec;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 307
    :sswitch_5
    iget-object v0, p0, Lltz;->b:Lpzw;

    if-nez v0, :cond_9

    .line 308
    new-instance v0, Lpzw;

    invoke-direct {v0}, Lpzw;-><init>()V

    iput-object v0, p0, Lltz;->b:Lpzw;

    .line 310
    :cond_9
    iget-object v0, p0, Lltz;->b:Lpzw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 314
    :sswitch_6
    iget-object v0, p0, Lltz;->c:Lqbp;

    if-nez v0, :cond_a

    .line 315
    new-instance v0, Lqbp;

    invoke-direct {v0}, Lqbp;-><init>()V

    iput-object v0, p0, Lltz;->c:Lqbp;

    .line 317
    :cond_a
    iget-object v0, p0, Lltz;->c:Lqbp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 321
    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 322
    iget-object v0, p0, Lltz;->d:[Ljava/lang/Integer;

    array-length v0, v0

    .line 323
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 324
    iget-object v2, p0, Lltz;->d:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 325
    iput-object v1, p0, Lltz;->d:[Ljava/lang/Integer;

    .line 326
    :goto_1
    iget-object v1, p0, Lltz;->d:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_b

    .line 327
    iget-object v1, p0, Lltz;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 328
    invoke-virtual {p1}, Loxn;->a()I

    .line 326
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 331
    :cond_b
    iget-object v1, p0, Lltz;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 110
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v2, -0x80000000

    .line 36
    iget v0, p0, Lltz;->e:I

    if-eq v0, v2, :cond_0

    .line 37
    const/4 v0, 0x1

    iget v1, p0, Lltz;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 39
    :cond_0
    iget v0, p0, Lltz;->f:I

    if-eq v0, v2, :cond_1

    .line 40
    const/4 v0, 0x2

    iget v1, p0, Lltz;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 42
    :cond_1
    iget v0, p0, Lltz;->g:I

    if-eq v0, v2, :cond_2

    .line 43
    const/4 v0, 0x3

    iget v1, p0, Lltz;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 45
    :cond_2
    iget-object v0, p0, Lltz;->a:Lpec;

    if-eqz v0, :cond_3

    .line 46
    const/4 v0, 0x4

    iget-object v1, p0, Lltz;->a:Lpec;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 48
    :cond_3
    iget-object v0, p0, Lltz;->b:Lpzw;

    if-eqz v0, :cond_4

    .line 49
    const/4 v0, 0x5

    iget-object v1, p0, Lltz;->b:Lpzw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 51
    :cond_4
    iget-object v0, p0, Lltz;->c:Lqbp;

    if-eqz v0, :cond_5

    .line 52
    const/4 v0, 0x6

    iget-object v1, p0, Lltz;->c:Lqbp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 54
    :cond_5
    iget-object v0, p0, Lltz;->d:[Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 55
    iget-object v1, p0, Lltz;->d:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 56
    const/4 v4, 0x7

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_6
    iget-object v0, p0, Lltz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 61
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lltz;->a(Loxn;)Lltz;

    move-result-object v0

    return-object v0
.end method
