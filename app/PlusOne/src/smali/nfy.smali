.class public final Lnfy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field private b:Lnyb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1201
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1206
    const/4 v0, 0x0

    iput-object v0, p0, Lnfy;->b:Lnyb;

    .line 1201
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1221
    const/4 v0, 0x1

    iget-object v1, p0, Lnfy;->a:Ljava/lang/Boolean;

    .line 1223
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1224
    iget-object v1, p0, Lnfy;->b:Lnyb;

    if-eqz v1, :cond_0

    .line 1225
    const/4 v1, 0x2

    iget-object v2, p0, Lnfy;->b:Lnyb;

    .line 1226
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1228
    :cond_0
    iget-object v1, p0, Lnfy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1229
    iput v0, p0, Lnfy;->ai:I

    .line 1230
    return v0
.end method

.method public a(Loxn;)Lnfy;
    .locals 2

    .prologue
    .line 1238
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1239
    sparse-switch v0, :sswitch_data_0

    .line 1243
    iget-object v1, p0, Lnfy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1244
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnfy;->ah:Ljava/util/List;

    .line 1247
    :cond_1
    iget-object v1, p0, Lnfy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1249
    :sswitch_0
    return-object p0

    .line 1254
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnfy;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 1258
    :sswitch_2
    iget-object v0, p0, Lnfy;->b:Lnyb;

    if-nez v0, :cond_2

    .line 1259
    new-instance v0, Lnyb;

    invoke-direct {v0}, Lnyb;-><init>()V

    iput-object v0, p0, Lnfy;->b:Lnyb;

    .line 1261
    :cond_2
    iget-object v0, p0, Lnfy;->b:Lnyb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1239
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1211
    const/4 v0, 0x1

    iget-object v1, p0, Lnfy;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1212
    iget-object v0, p0, Lnfy;->b:Lnyb;

    if-eqz v0, :cond_0

    .line 1213
    const/4 v0, 0x2

    iget-object v1, p0, Lnfy;->b:Lnyb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1215
    :cond_0
    iget-object v0, p0, Lnfy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1217
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1197
    invoke-virtual {p0, p1}, Lnfy;->a(Loxn;)Lnfy;

    move-result-object v0

    return-object v0
.end method
