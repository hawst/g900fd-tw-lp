.class public final Lnwb;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lofi;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 408
    invoke-direct {p0}, Loxq;-><init>()V

    .line 413
    sget-object v0, Lofi;->a:[Lofi;

    iput-object v0, p0, Lnwb;->a:[Lofi;

    .line 408
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    .line 432
    const/4 v0, 0x1

    iget-object v1, p0, Lnwb;->b:Ljava/lang/String;

    .line 434
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 435
    iget-object v1, p0, Lnwb;->a:[Lofi;

    if-eqz v1, :cond_1

    .line 436
    iget-object v2, p0, Lnwb;->a:[Lofi;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 437
    if-eqz v4, :cond_0

    .line 438
    const/4 v5, 0x2

    .line 439
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 436
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 443
    :cond_1
    iget-object v1, p0, Lnwb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 444
    iput v0, p0, Lnwb;->ai:I

    .line 445
    return v0
.end method

.method public a(Loxn;)Lnwb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 453
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 454
    sparse-switch v0, :sswitch_data_0

    .line 458
    iget-object v2, p0, Lnwb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 459
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnwb;->ah:Ljava/util/List;

    .line 462
    :cond_1
    iget-object v2, p0, Lnwb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    :sswitch_0
    return-object p0

    .line 469
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnwb;->b:Ljava/lang/String;

    goto :goto_0

    .line 473
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 474
    iget-object v0, p0, Lnwb;->a:[Lofi;

    if-nez v0, :cond_3

    move v0, v1

    .line 475
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lofi;

    .line 476
    iget-object v3, p0, Lnwb;->a:[Lofi;

    if-eqz v3, :cond_2

    .line 477
    iget-object v3, p0, Lnwb;->a:[Lofi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 479
    :cond_2
    iput-object v2, p0, Lnwb;->a:[Lofi;

    .line 480
    :goto_2
    iget-object v2, p0, Lnwb;->a:[Lofi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 481
    iget-object v2, p0, Lnwb;->a:[Lofi;

    new-instance v3, Lofi;

    invoke-direct {v3}, Lofi;-><init>()V

    aput-object v3, v2, v0

    .line 482
    iget-object v2, p0, Lnwb;->a:[Lofi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 483
    invoke-virtual {p1}, Loxn;->a()I

    .line 480
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 474
    :cond_3
    iget-object v0, p0, Lnwb;->a:[Lofi;

    array-length v0, v0

    goto :goto_1

    .line 486
    :cond_4
    iget-object v2, p0, Lnwb;->a:[Lofi;

    new-instance v3, Lofi;

    invoke-direct {v3}, Lofi;-><init>()V

    aput-object v3, v2, v0

    .line 487
    iget-object v2, p0, Lnwb;->a:[Lofi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 454
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 418
    const/4 v0, 0x1

    iget-object v1, p0, Lnwb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 419
    iget-object v0, p0, Lnwb;->a:[Lofi;

    if-eqz v0, :cond_1

    .line 420
    iget-object v1, p0, Lnwb;->a:[Lofi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 421
    if-eqz v3, :cond_0

    .line 422
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 420
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 426
    :cond_1
    iget-object v0, p0, Lnwb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 428
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 404
    invoke-virtual {p0, p1}, Lnwb;->a(Loxn;)Lnwb;

    move-result-object v0

    return-object v0
.end method
