.class final Ldcy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Lcpg;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Ldcw;


# direct methods
.method constructor <init>(Ldcw;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Ldcy;->a:Ldcw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lcpg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v0}, Ldcw;->n()Lz;

    .line 94
    iget-object v0, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v0}, Ldcw;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 96
    iget-object v1, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v1}, Ldcw;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "all_photos_row_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 97
    new-instance v1, Levc;

    iget-object v4, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v4}, Ldcw;->n()Lz;

    move-result-object v4

    invoke-direct {v1, v4, v0, v2, v3}, Levc;-><init>(Landroid/content/Context;IJ)V

    return-object v1
.end method

.method public a(Lcpg;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcpg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    if-nez p1, :cond_0

    .line 103
    iget-object v0, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v0}, Ldcw;->U()V

    .line 136
    :goto_0
    return-void

    .line 107
    :cond_0
    new-instance v9, Lddp;

    invoke-direct {v9}, Lddp;-><init>()V

    .line 108
    iget-object v0, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v0}, Ldcw;->V()Lddl;

    move-result-object v0

    invoke-virtual {v9, v0}, Lddp;->a(Lddl;)Ldds;

    .line 109
    iget-wide v0, p1, Lcpg;->b:J

    invoke-virtual {v9, v0, v1}, Lddp;->a(J)Lddp;

    .line 110
    iget-object v0, p1, Lcpg;->f:Lizu;

    .line 111
    invoke-virtual {v9, v0}, Lddp;->a(Lizu;)Ldds;

    move-result-object v0

    iget-wide v2, p1, Lcpg;->d:J

    .line 112
    invoke-virtual {v0, v2, v3}, Ldds;->b(J)Ldds;

    move-result-object v0

    iget-wide v2, p1, Lcpg;->c:J

    .line 113
    invoke-virtual {v0, v2, v3}, Ldds;->c(J)Ldds;

    move-result-object v0

    iget-object v1, p1, Lcpg;->g:Lnym;

    .line 114
    invoke-virtual {v0, v1}, Ldds;->a(Lnym;)Ldds;

    .line 116
    iget-object v0, p1, Lcpg;->g:Lnym;

    if-eqz v0, :cond_1

    .line 119
    new-instance v0, Lfdg;

    iget-object v1, p1, Lcpg;->g:Lnym;

    iget-wide v2, p1, Lcpg;->d:J

    iget-wide v4, p1, Lcpg;->c:J

    .line 121
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lfdg;-><init>(Lnym;JJJZ)V

    .line 122
    iget-object v1, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v1}, Ldcw;->n()Lz;

    move-result-object v1

    invoke-virtual {v9, v1, v0}, Lddp;->a(Landroid/content/Context;Lfdg;)Ldds;

    .line 125
    :cond_1
    iget-object v0, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v9}, Lddp;->a()Lddl;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcw;->a(Lddl;)V

    .line 128
    iget-object v0, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v0}, Ldcw;->V()Lddl;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldcy;->a:Ldcw;

    .line 129
    invoke-virtual {v0}, Ldcw;->V()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->Q()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldcy;->a:Ldcw;

    .line 130
    invoke-virtual {v0}, Ldcw;->V()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->a()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 131
    iget-object v0, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v0}, Ldcw;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Ldcy;->a:Ldcw;

    .line 132
    invoke-virtual {v2}, Ldcw;->k()Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Ldcx;

    iget-object v4, p0, Ldcy;->a:Ldcw;

    invoke-direct {v3, v4}, Ldcx;-><init>(Ldcw;)V

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 135
    :cond_2
    iget-object v0, p0, Ldcy;->a:Ldcw;

    iget-object v1, p0, Ldcy;->a:Ldcw;

    invoke-virtual {v1}, Ldcw;->V()Lddl;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcw;->a(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lcpg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 90
    check-cast p2, Lcpg;

    invoke-virtual {p0, p2}, Ldcy;->a(Lcpg;)V

    return-void
.end method
