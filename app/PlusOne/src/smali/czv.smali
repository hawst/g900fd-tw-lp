.class public final Lczv;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ldax;

.field private final c:Lldh;

.field private final d:Ldeo;

.field private final e:Ldec;

.field private final f:Ldeb;

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lctm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldax;Lldh;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lczv;->g:Ljava/util/ArrayList;

    .line 42
    iput-object p1, p0, Lczv;->a:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lczv;->b:Ldax;

    .line 44
    iput-object p3, p0, Lczv;->c:Lldh;

    .line 46
    const-class v0, Ldeo;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lczv;->d:Ldeo;

    .line 47
    const-class v0, Ldec;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldec;

    iput-object v0, p0, Lczv;->e:Ldec;

    .line 48
    const-class v0, Ldeb;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeb;

    iput-object v0, p0, Lczv;->f:Ldeb;

    .line 50
    iget-object v0, p0, Lczv;->d:Ldeo;

    invoke-virtual {v0}, Ldeo;->b()Ljlx;

    move-result-object v0

    new-instance v1, Lczw;

    invoke-direct {v1, p0}, Lczw;-><init>(Lczv;)V

    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 57
    iget-object v0, p0, Lczv;->e:Ldec;

    invoke-virtual {v0}, Ldec;->b()Ljlx;

    move-result-object v0

    new-instance v1, Lczx;

    invoke-direct {v1, p0}, Lczx;-><init>(Lczv;)V

    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 64
    iget-object v0, p0, Lczv;->f:Ldeb;

    invoke-virtual {v0}, Ldeb;->b()Ljlx;

    move-result-object v0

    new-instance v1, Lczy;

    invoke-direct {v1, p0}, Lczy;-><init>(Lczv;)V

    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 70
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 74
    iget-object v0, p0, Lczv;->d:Ldeo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczv;->d:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-nez v0, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    new-instance v0, Ldaw;

    iget-object v2, p0, Lczv;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, Ldaw;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lczv;->c:Lldh;

    .line 79
    invoke-virtual {v0, v2}, Ldaw;->a(Lhsn;)Ldaw;

    move-result-object v0

    iget-object v2, p0, Lczv;->c:Lldh;

    .line 80
    invoke-virtual {v0, v2}, Ldaw;->a(Lljv;)Ldaw;

    move-result-object v0

    iget-object v2, p0, Lczv;->e:Ldec;

    .line 81
    invoke-virtual {v2}, Ldec;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldaw;->a(Ljava/lang/String;)Ldaw;

    move-result-object v0

    iget-object v2, p0, Lczv;->e:Ldec;

    .line 82
    invoke-virtual {v2}, Ldec;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldaw;->b(Ljava/lang/String;)Ldaw;

    move-result-object v0

    iget-object v2, p0, Lczv;->e:Ldec;

    .line 83
    invoke-virtual {v2}, Ldec;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldaw;->c(Ljava/lang/String;)Ldaw;

    move-result-object v0

    iget-object v2, p0, Lczv;->e:Ldec;

    .line 84
    invoke-virtual {v2}, Ldec;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldaw;->d(Ljava/lang/String;)Ldaw;

    move-result-object v0

    iget-object v2, p0, Lczv;->e:Ldec;

    .line 85
    invoke-virtual {v2}, Ldec;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ldaw;->a(J)Ldaw;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Ldaw;->a()Ldav;

    move-result-object v2

    .line 87
    new-instance v0, Ldaz;

    iget-object v3, p0, Lczv;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Ldaz;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lczv;->b:Ldax;

    .line 88
    invoke-virtual {v0, v3}, Ldaz;->a(Ldax;)Ldaz;

    move-result-object v0

    iget-object v3, p0, Lczv;->e:Ldec;

    .line 89
    invoke-virtual {v3}, Ldec;->g()I

    move-result v3

    invoke-virtual {v0, v3}, Ldaz;->a(I)Ldaz;

    move-result-object v0

    iget-object v3, p0, Lczv;->d:Ldeo;

    .line 90
    invoke-virtual {v3}, Ldeo;->a()Lddl;

    move-result-object v3

    invoke-interface {v3}, Lddl;->w()Z

    move-result v3

    invoke-virtual {v0, v3}, Ldaz;->a(Z)Ldaz;

    move-result-object v0

    iget-object v3, p0, Lczv;->d:Ldeo;

    .line 91
    invoke-virtual {v3}, Ldeo;->a()Lddl;

    move-result-object v3

    invoke-interface {v3}, Lddl;->B()Z

    move-result v3

    invoke-virtual {v0, v3}, Ldaz;->b(Z)Ldaz;

    move-result-object v0

    iget-object v3, p0, Lczv;->e:Ldec;

    .line 92
    invoke-virtual {v3}, Ldec;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldaz;->a(Ljava/lang/String;)Ldaz;

    move-result-object v0

    iget-object v3, p0, Lczv;->e:Ldec;

    .line 93
    invoke-virtual {v3}, Ldec;->i()I

    move-result v3

    invoke-virtual {v0, v3}, Ldaz;->b(I)Ldaz;

    move-result-object v0

    iget-object v3, p0, Lczv;->e:Ldec;

    .line 94
    invoke-virtual {v3}, Ldec;->j()Z

    move-result v3

    invoke-virtual {v0, v3}, Ldaz;->c(Z)Ldaz;

    move-result-object v3

    iget-object v0, p0, Lczv;->d:Ldeo;

    .line 95
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lczv;->d:Ldeo;

    .line 96
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->t()Z

    move-result v0

    .line 95
    :goto_1
    invoke-virtual {v3, v0}, Ldaz;->d(Z)Ldaz;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Ldaz;->a()Lday;

    move-result-object v0

    .line 99
    iget-object v3, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 101
    iget-object v3, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 102
    iget-object v3, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 106
    :cond_2
    iget-object v3, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 108
    iget-object v1, p0, Lczv;->g:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 96
    goto :goto_1
.end method

.method static synthetic a(Lczv;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lczv;->a()V

    return-void
.end method

.method static synthetic b(Lczv;)V
    .locals 6

    .prologue
    .line 26
    iget-object v0, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lczv;->e:Ldec;

    invoke-virtual {v0}, Ldec;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczv;->e:Ldec;

    invoke-virtual {v0}, Ldec;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczv;->e:Ldec;

    invoke-virtual {v0}, Ldec;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lczv;->a()V

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lczv;->f:Ldeb;

    invoke-virtual {v1}, Ldeb;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lczv;->f:Ldeb;

    invoke-virtual {v1, v0}, Ldeb;->a(I)Ldea;

    move-result-object v1

    new-instance v2, Lczu;

    iget-object v3, p0, Lczv;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lczu;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lczv;->c:Lldh;

    invoke-virtual {v2, v3}, Lczu;->a(Lldh;)Lczu;

    move-result-object v2

    invoke-virtual {v1}, Ldea;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lczu;->a(Ljava/lang/String;)Lczu;

    move-result-object v2

    invoke-virtual {v1}, Ldea;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lczu;->b(Ljava/lang/String;)Lczu;

    move-result-object v2

    invoke-virtual {v1}, Ldea;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lczu;->c(Ljava/lang/String;)Lczu;

    move-result-object v2

    invoke-virtual {v1}, Ldea;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lczu;->d(Ljava/lang/String;)Lczu;

    move-result-object v2

    invoke-virtual {v1}, Ldea;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lczu;->e(Ljava/lang/String;)Lczu;

    move-result-object v2

    invoke-virtual {v1}, Ldea;->f()Z

    move-result v3

    invoke-virtual {v2, v3}, Lczu;->a(Z)Lczu;

    move-result-object v2

    invoke-virtual {v1}, Ldea;->g()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lczu;->a(J)Lczu;

    move-result-object v2

    invoke-virtual {v1}, Ldea;->h()Z

    move-result v3

    invoke-virtual {v2, v3}, Lczu;->b(Z)Lczu;

    move-result-object v2

    invoke-virtual {v1}, Ldea;->i()I

    move-result v1

    invoke-virtual {v2, v1}, Lczu;->a(I)Lczu;

    move-result-object v1

    invoke-virtual {v1}, Lczu;->a()Lczt;

    move-result-object v1

    iget-object v2, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lczv;->notifyDataSetChanged()V

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 155
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0}, Lctm;->a()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lczv;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, p2, p3}, Lctm;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 165
    invoke-static {}, Ldat;->a()[I

    move-result-object v0

    array-length v0, v0

    return v0
.end method
