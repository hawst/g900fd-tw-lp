.class public final Ljup;
.super Ljuk;
.source "PG"

# interfaces
.implements Ljuw;


# static fields
.field private static a:Landroid/content/Context;


# instance fields
.field private b:Lvp;

.field private c:Lvn;

.field private d:Ljava/lang/Runnable;

.field private e:Ljuu;

.field private f:Lizu;

.field private g:Lihr;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljul;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljum;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljun;

.field private k:Ljus;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Ljus;

    invoke-direct {v0}, Ljus;-><init>()V

    invoke-direct {p0, p1, v0}, Ljup;-><init>(Landroid/content/Context;Ljus;)V

    .line 99
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljus;)V
    .locals 4

    .prologue
    .line 101
    invoke-direct {p0}, Ljuk;-><init>()V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljup;->h:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljup;->i:Ljava/util/List;

    .line 102
    iput-object p2, p0, Ljup;->k:Ljus;

    .line 103
    sput-object p1, Ljup;->a:Landroid/content/Context;

    .line 105
    invoke-virtual {p2}, Ljus;->a()Lvn;

    move-result-object v0

    iput-object v0, p0, Ljup;->c:Lvn;

    .line 106
    new-instance v0, Ljut;

    invoke-direct {v0, p0}, Ljut;-><init>(Ljup;)V

    .line 107
    iget-object v1, p0, Ljup;->k:Ljus;

    sget-object v2, Ljup;->a:Landroid/content/Context;

    iget-object v3, p0, Ljup;->c:Lvn;

    invoke-virtual {v1, v2, v3, v0}, Ljus;->a(Landroid/content/Context;Lvn;Ljut;)Lvp;

    move-result-object v0

    iput-object v0, p0, Ljup;->b:Lvp;

    .line 111
    new-instance v0, Ljuq;

    invoke-direct {v0, p0}, Ljuq;-><init>(Ljup;)V

    iput-object v0, p0, Ljup;->d:Ljava/lang/Runnable;

    .line 124
    return-void
.end method

.method private a(Lizu;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 356
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 357
    :cond_0
    const/4 v0, 0x0

    .line 373
    :goto_0
    return-object v0

    .line 360
    :cond_1
    const/4 v0, 0x0

    .line 361
    invoke-virtual {p1}, Lizu;->g()Ljac;

    move-result-object v1

    sget-object v2, Ljac;->b:Ljac;

    invoke-virtual {v1, v2}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 362
    const/16 v0, 0x10

    .line 366
    :cond_2
    const/16 v2, 0x500

    .line 367
    const/16 v1, 0x2d0

    .line 368
    iget-object v3, p0, Ljup;->e:Ljuu;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ljup;->e:Ljuu;

    invoke-virtual {v3}, Ljuu;->f()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 369
    iget-object v1, p0, Ljup;->e:Ljuu;

    invoke-virtual {v1}, Ljuu;->h()I

    move-result v2

    .line 370
    iget-object v1, p0, Ljup;->e:Ljuu;

    invoke-virtual {v1}, Ljuu;->g()I

    move-result v1

    .line 373
    :cond_3
    invoke-static {p2, v0, v2, v1}, Ljbd;->a(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Ljup;)Ljun;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ljup;->j:Ljun;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;Lizu;IZ)V
    .locals 4

    .prologue
    .line 438
    iput-object p2, p0, Ljup;->f:Lizu;

    .line 440
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Ljup;->a:Landroid/content/Context;

    sget-object v0, Ljup;->a:Landroid/content/Context;

    const-class v3, Ljuo;

    .line 441
    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 442
    const-string v0, "notification_bundle"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 443
    const-string v0, "notification_media"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 444
    const-string v0, "notification_index"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 445
    const-string v0, "notification_video_playing"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 446
    sget-object v0, Ljup;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 447
    return-void
.end method

.method static synthetic a(Ljup;Liic;)V
    .locals 1

    .prologue
    .line 38
    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Ljup;->a(Liic;)V

    iget-object v0, p0, Ljup;->g:Lihr;

    invoke-interface {v0}, Lihr;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Ljup;->m()V

    goto :goto_0
.end method

.method static synthetic b(Ljup;)V
    .locals 6

    .prologue
    .line 38
    iget-object v0, p0, Ljup;->j:Ljun;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljup;->e:Ljuu;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ljup;->e:Ljuu;

    invoke-virtual {v0}, Ljuu;->j()D

    move-result-wide v2

    iget-object v0, p0, Ljup;->e:Ljuu;

    invoke-virtual {v0}, Ljuu;->k()D

    move-result-wide v4

    const-wide/16 v0, 0x0

    cmpl-double v0, v4, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljup;->j:Ljun;

    iget-object v1, p0, Ljup;->f:Lizu;

    invoke-interface/range {v0 .. v5}, Ljun;->a(Lizu;DD)V

    goto :goto_0
.end method

.method static synthetic c(Ljup;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ljup;->d:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic d(Ljup;)Lihr;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ljup;->g:Lihr;

    return-object v0
.end method

.method static synthetic e(Ljup;)Ljuu;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ljup;->e:Ljuu;

    return-object v0
.end method

.method static synthetic f(Ljup;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljup;->m()V

    return-void
.end method

.method static synthetic g(Ljup;)Ljava/util/List;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ljup;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic k()Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Ljup;->a:Landroid/content/Context;

    return-object v0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 300
    iget-object v0, p0, Ljup;->j:Ljun;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljup;->e:Ljuu;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Ljup;->j:Ljun;

    iget-object v1, p0, Ljup;->f:Lizu;

    iget-object v2, p0, Ljup;->e:Ljuu;

    .line 302
    invoke-virtual {v2}, Ljuu;->i()Z

    move-result v2

    .line 301
    invoke-interface {v0, v1, v2}, Ljun;->a(Lizu;Z)V

    .line 305
    :cond_0
    iget-object v0, p0, Ljup;->e:Ljuu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljup;->e:Ljuu;

    invoke-virtual {v0}, Ljuu;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Ljup;->d:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Ljup;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x21

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 311
    :goto_0
    return-void

    .line 308
    :cond_1
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Ljup;->a:Landroid/content/Context;

    sget-object v0, Ljup;->a:Landroid/content/Context;

    const-class v3, Ljuo;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "notification_video_playing"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v0, Ljup;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 309
    iget-object v0, p0, Ljup;->d:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private m()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 407
    iget-object v0, p0, Ljup;->g:Lihr;

    if-eqz v0, :cond_1

    .line 409
    :try_start_0
    invoke-direct {p0}, Ljup;->l()V

    .line 410
    const/4 v0, 0x0

    iput-object v0, p0, Ljup;->f:Lizu;

    .line 412
    iget-object v0, p0, Ljup;->e:Ljuu;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Ljup;->e:Ljuu;

    const-string v1, "invalidateSession"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljuu;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 414
    const/4 v0, 0x0

    iput-object v0, p0, Ljup;->e:Ljuu;

    .line 417
    :cond_0
    iget-object v0, p0, Ljup;->g:Lihr;

    invoke-interface {v0}, Lihr;->b()V

    .line 421
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljup;->a(Z)V

    .line 422
    sget-object v1, Ljup;->a:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    sget-object v3, Ljup;->a:Landroid/content/Context;

    sget-object v0, Ljup;->a:Landroid/content/Context;

    const-class v4, Ljuo;

    .line 423
    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 422
    invoke-virtual {v1, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    iput-object v5, p0, Ljup;->g:Lihr;

    .line 430
    :cond_1
    :goto_0
    return-void

    .line 424
    :catch_0
    move-exception v0

    .line 425
    :try_start_1
    const-string v1, "CastApi"

    const-string v2, "Disconnecting from a device we are not connected to."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 427
    iput-object v5, p0, Ljup;->g:Lihr;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v5, p0, Ljup;->g:Lihr;

    throw v0
.end method


# virtual methods
.method public a(D)V
    .locals 3

    .prologue
    .line 260
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 261
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "newPosition must not be negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_0
    iget-object v0, p0, Ljup;->e:Ljuu;

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Ljup;->e:Ljuu;

    invoke-virtual {v0, p1, p2}, Ljuu;->a(D)V

    .line 266
    invoke-direct {p0}, Ljup;->l()V

    .line 268
    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;ILizu;Lizu;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 186
    if-eqz p3, :cond_0

    iget-object v1, p0, Ljup;->e:Ljuu;

    if-eqz v1, :cond_0

    .line 188
    invoke-virtual {p3}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p3}, Lizu;->e()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    .line 193
    :cond_2
    iget-object v1, p0, Ljup;->e:Ljuu;

    .line 194
    invoke-virtual {p3}, Lizu;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p3, v2}, Ljup;->a(Lizu;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 195
    invoke-direct {p0, p4, v0}, Ljup;->a(Lizu;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 193
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v4, "asset"

    const-string v5, "image"

    const-string v6, "remote"

    invoke-virtual {v1, v2, v5, v6}, Ljuu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz v0, :cond_3

    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    const-string v4, "image"

    const-string v5, "remote"

    invoke-virtual {v1, v0, v4, v5}, Ljuu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    const-string v0, "precache"

    invoke-virtual {v3, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_3
    const-string v0, "newAsset"

    invoke-virtual {v1, v0, v3}, Ljuu;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    const/4 v0, 0x0

    iput-object v0, v1, Ljuu;->a:Lnzb;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljuu;->a(Z)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :goto_1
    invoke-direct {p0, p1, p3, p2, v7}, Ljup;->a(Landroid/os/Bundle;Lizu;IZ)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public a(Landroid/support/v7/app/MediaRouteButton;)V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Ljup;->c:Lvn;

    invoke-virtual {p1, v0}, Landroid/support/v7/app/MediaRouteButton;->a(Lvn;)V

    .line 136
    invoke-static {}, Lqh;->a()Lqh;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/app/MediaRouteButton;->a(Lqh;)V

    .line 137
    return-void
.end method

.method a(Liic;)V
    .locals 3

    .prologue
    .line 128
    sget-object v0, Ljup;->a:Landroid/content/Context;

    const-class v1, Lihu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lihu;

    sget-object v1, Ljup;->a:Landroid/content/Context;

    const-string v2, "5FD0CDC9"

    invoke-interface {v0, v1, v2}, Lihu;->a(Landroid/content/Context;Ljava/lang/String;)Lihr;

    move-result-object v0

    iput-object v0, p0, Ljup;->g:Lihr;

    .line 130
    iget-object v0, p0, Ljup;->g:Lihr;

    new-instance v1, Ljur;

    invoke-direct {v1, p0}, Ljur;-><init>(Ljup;)V

    invoke-interface {v0, v1}, Lihr;->a(Lihs;)V

    .line 131
    iget-object v0, p0, Ljup;->g:Lihr;

    invoke-interface {v0, p1}, Lihr;->a(Liic;)V

    .line 132
    return-void
.end method

.method public a(Lizu;)V
    .locals 6

    .prologue
    .line 204
    if-eqz p1, :cond_0

    iget-object v0, p0, Ljup;->e:Ljuu;

    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {p1}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    iget-object v0, p0, Ljup;->e:Ljuu;

    invoke-virtual {p1}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Ljup;->a(Lizu;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    const-string v4, "image"

    const-string v5, "remote"

    invoke-virtual {v0, v1, v4, v5}, Ljuu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    const-string v1, "precache"

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "precacheAssets"

    invoke-virtual {v0, v1, v2}, Ljuu;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljul;)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Ljup;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    invoke-interface {p1}, Ljul;->ak()V

    .line 379
    return-void
.end method

.method public a(Ljum;)V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Ljup;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-virtual {p0}, Ljup;->b()Z

    move-result v0

    invoke-interface {p1, v0}, Ljum;->a(Z)V

    .line 163
    return-void
.end method

.method public a(Ljun;)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Ljup;->j:Ljun;

    .line 342
    return-void
.end method

.method a(Ljuu;)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Ljup;->e:Ljuu;

    .line 395
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Ljup;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljum;

    .line 172
    invoke-interface {v0, p1}, Ljum;->a(Z)V

    goto :goto_0

    .line 174
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Ljup;->b:Lvp;

    iget-object v1, p0, Ljup;->c:Lvn;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lvp;->a(Lvn;I)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/os/Bundle;ILizu;Lnzb;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 220
    invoke-virtual {p0}, Ljup;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    iget-object v1, p4, Lnzb;->c:[Lnzc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljup;->e:Ljuu;

    if-nez v1, :cond_1

    .line 222
    :cond_0
    const/4 v0, 0x0

    .line 240
    :goto_0
    return v0

    .line 227
    :cond_1
    iget-object v1, p0, Ljup;->e:Ljuu;

    invoke-virtual {v1, p4}, Ljuu;->b(Lnzb;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 228
    iget-object v1, p0, Ljup;->e:Ljuu;

    invoke-virtual {v1}, Ljuu;->e()Z

    move-result v1

    if-nez v1, :cond_2

    .line 229
    iget-object v1, p0, Ljup;->e:Ljuu;

    invoke-virtual {v1}, Ljuu;->a()V

    .line 237
    :goto_1
    invoke-direct {p0, p1, p3, p2, v0}, Ljup;->a(Landroid/os/Bundle;Lizu;IZ)V

    .line 238
    invoke-direct {p0}, Ljup;->l()V

    goto :goto_0

    .line 234
    :cond_2
    iget-object v1, p0, Ljup;->e:Ljuu;

    invoke-virtual {v1, p4}, Ljuu;->a(Lnzb;)V

    goto :goto_1
.end method

.method public b(Ljul;)V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Ljup;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 383
    return-void
.end method

.method public b(Ljum;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Ljup;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 167
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Ljup;->g:Lihr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljup;->g:Lihr;

    invoke-interface {v0}, Lihr;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lizu;)Z
    .locals 2

    .prologue
    .line 281
    if-eqz p1, :cond_0

    iget-object v0, p0, Ljup;->e:Ljuu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljup;->f:Lizu;

    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {p1}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ljup;->f:Lizu;

    invoke-virtual {v1}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Ljup;->e:Ljuu;

    invoke-virtual {v0}, Ljuu;->i()Z

    move-result v0

    .line 286
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 154
    invoke-virtual {p0}, Ljup;->i()V

    .line 155
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Ljup;->e:Ljuu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljup;->f:Lizu;

    if-nez v0, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v0, p0, Ljup;->e:Ljuu;

    invoke-virtual {v0}, Ljuu;->b()V

    .line 252
    invoke-direct {p0}, Ljup;->l()V

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Ljup;->b:Lvp;

    if-nez v0, :cond_0

    .line 349
    const/4 v0, 0x0

    .line 352
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ljup;->b:Lvp;

    invoke-virtual {v0}, Lvp;->c()Lvy;

    move-result-object v0

    invoke-virtual {v0}, Lvy;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Ljup;->e:Ljuu;

    invoke-virtual {v0}, Ljuu;->c()V

    .line 292
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Ljup;->e:Ljuu;

    invoke-virtual {v0}, Ljuu;->d()V

    .line 297
    return-void
.end method

.method h()V
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Ljup;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljul;

    .line 388
    invoke-interface {v0}, Ljul;->ak()V

    goto :goto_0

    .line 390
    :cond_0
    return-void
.end method

.method i()V
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Ljup;->b:Lvp;

    iget-object v1, p0, Ljup;->b:Lvp;

    invoke-virtual {v1}, Lvp;->b()Lvy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lvp;->a(Lvy;)V

    .line 435
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 458
    invoke-direct {p0}, Ljup;->l()V

    .line 459
    return-void
.end method
