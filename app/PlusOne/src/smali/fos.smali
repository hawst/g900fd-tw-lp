.class public final Lfos;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Lfpm;
.implements Lfra;
.implements Lftc;
.implements Lftd;
.implements Lfte;
.implements Lhec;
.implements Lhjj;
.implements Lhmq;
.implements Lhob;
.implements Llgs;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# static fields
.field private static final N:Lloz;


# instance fields
.field private final O:Lhje;

.field private P:Lhee;

.field private Q:Lijk;

.field private final R:Lhoc;

.field private S:Lmmq;

.field private T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

.field private U:Lfqu;

.field private V:I

.field private W:Lmms;

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aa:Z

.field private ab:Z

.field private ac:[Lmmr;

.field private ad:Z

.field private ae:Ljava/lang/String;

.field private af:J

.field private ag:Ljava/lang/String;

.field private ah:Z

.field private ai:Z

.field private aj:Z

.field private ak:Landroid/widget/TextView;

.field private al:Z

.field private final am:Ljava/lang/Runnable;

.field private final an:Ljava/lang/Runnable;

.field private final ao:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lfob;",
            ">;"
        }
    .end annotation
.end field

.field private final ap:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 170
    new-instance v0, Lloz;

    const-string v1, "debug.stories.layout"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfos;->N:Lloz;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 148
    invoke-direct {p0}, Llol;-><init>()V

    .line 271
    new-instance v0, Lhje;

    iget-object v1, p0, Lfos;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Lfos;->O:Lhje;

    .line 277
    new-instance v0, Lhoc;

    .line 278
    invoke-virtual {p0}, Lfos;->z_()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lfos;->R:Lhoc;

    .line 279
    new-instance v0, Lfpl;

    .line 280
    invoke-virtual {p0}, Lfos;->z_()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p0}, Lfpl;-><init>(Lu;Llqr;Lfpm;)V

    .line 285
    iput v2, p0, Lfos;->V:I

    .line 287
    iput-boolean v2, p0, Lfos;->X:Z

    .line 288
    iput-boolean v2, p0, Lfos;->Y:Z

    .line 289
    iput-boolean v2, p0, Lfos;->Z:Z

    .line 290
    iput-boolean v2, p0, Lfos;->aa:Z

    .line 291
    iput-boolean v2, p0, Lfos;->ab:Z

    .line 306
    new-instance v0, Lfot;

    invoke-direct {v0, p0}, Lfot;-><init>(Lfos;)V

    iput-object v0, p0, Lfos;->am:Ljava/lang/Runnable;

    .line 317
    new-instance v0, Lfou;

    invoke-direct {v0, p0}, Lfou;-><init>(Lfos;)V

    iput-object v0, p0, Lfos;->an:Ljava/lang/Runnable;

    .line 327
    new-instance v0, Lfov;

    invoke-direct {v0, p0}, Lfov;-><init>(Lfos;)V

    iput-object v0, p0, Lfos;->ao:Lbc;

    .line 409
    new-instance v0, Lfox;

    invoke-direct {v0, p0}, Lfox;-><init>(Lfos;)V

    iput-object v0, p0, Lfos;->ap:Lbc;

    .line 2113
    return-void
.end method

.method private V()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    .line 793
    invoke-direct {p0}, Lfos;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 804
    :goto_0
    return-void

    .line 798
    :cond_0
    iput-boolean v6, p0, Lfos;->ai:Z

    .line 799
    iget-object v9, p0, Lfos;->R:Lhoc;

    new-instance v0, Lfns;

    iget-object v1, p0, Lfos;->at:Llnl;

    iget-object v2, p0, Lfos;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 800
    invoke-direct {p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "auth_key"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 801
    invoke-virtual {p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v5

    const-string v7, "gpinv"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    .line 802
    invoke-direct {p0}, Lfos;->ag()Landroid/graphics/Point;

    move-result-object v8

    invoke-static {v8}, Lfss;->a(Landroid/graphics/Point;)[Landroid/graphics/Point;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lfns;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;)V

    .line 799
    invoke-virtual {v9, v0}, Lhoc;->b(Lhny;)V

    .line 803
    iget-object v0, p0, Lfos;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto :goto_0
.end method

.method private W()Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x100000

    .line 1018
    :try_start_0
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 1019
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 1020
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1038
    :cond_0
    :goto_0
    return-object v0

    .line 1023
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1024
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1025
    if-eqz v0, :cond_0

    .line 1026
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v2

    if-lt v2, v5, :cond_0

    .line 1027
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1028
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 1029
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v4

    if-lt v4, v5, :cond_0

    .line 1030
    shr-int/lit8 v3, v3, 0x1

    .line 1031
    shr-int/lit8 v2, v2, 0x1

    .line 1032
    const/4 v4, 0x1

    invoke-static {v0, v3, v2, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 1038
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method private X()Z
    .locals 1

    .prologue
    .line 1043
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Y()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    const/16 v7, 0x8

    .line 1605
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10026d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1606
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f10026e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1607
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f100270

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1608
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f10026f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1609
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f100271

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1610
    iget-object v5, p0, Lfos;->S:Lmmq;

    iget-object v5, v5, Lmmq;->g:Lmlz;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lfos;->S:Lmmq;

    iget-object v5, v5, Lmmq;->g:Lmlz;

    iget-object v5, v5, Lmlz;->b:Logr;

    if-eqz v5, :cond_0

    .line 1612
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1613
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1614
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1615
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1616
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1617
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100268

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 1618
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f100269

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1619
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f10026a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1620
    iget-object v3, p0, Lfos;->S:Lmmq;

    iget-object v3, v3, Lmmq;->b:Lodo;

    iget-object v3, v3, Lodo;->c:Ljava/lang/String;

    iget-object v4, p0, Lfos;->S:Lmmq;

    iget-object v4, v4, Lmmq;->b:Lodo;

    iget-object v4, v4, Lodo;->f:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1621
    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    iget-object v0, v0, Logr;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1622
    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    iget-object v0, v0, Logr;->n:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1623
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f100267

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1624
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a01d6

    .line 1625
    invoke-virtual {p0, v3}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "; "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1624
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1650
    :goto_0
    return-void

    .line 1626
    :cond_0
    iget-object v5, p0, Lfos;->R:Lhoc;

    const-string v6, "story_share_refresh"

    invoke-virtual {v5, v6}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1629
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1630
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1631
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1632
    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1633
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1634
    :cond_1
    invoke-direct {p0}, Lfos;->aa()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1636
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1637
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1638
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1639
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1640
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1644
    :cond_2
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1645
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1646
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1647
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1648
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private Z()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1747
    .line 1748
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lfos;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-direct {p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v2

    .line 1747
    invoke-static {v0, v1, v2}, Lfqt;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lfos;->a(Landroid/content/Intent;I)V

    .line 1749
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Lz;->overridePendingTransition(II)V

    .line 1750
    sget-object v0, Lhmv;->eY:Lhmv;

    invoke-direct {p0, v0}, Lfos;->a(Lhmv;)V

    .line 1751
    return-void
.end method

.method static synthetic a(Lfos;I)I
    .locals 0

    .prologue
    .line 148
    iput p1, p0, Lfos;->V:I

    return p1
.end method

.method static synthetic a(Lfos;Lmmq;)Lmmq;
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lfos;->S:Lmmq;

    return-object p1
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 1830
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Lfos;->a(Ljava/lang/String;III)V

    .line 1831
    return-void
.end method

.method static synthetic a(Lfos;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x258

    const-wide/16 v8, 0x15e

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfos;->Y:Z

    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10026b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f10026c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f10032a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v3

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lfos;Z)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lfos;->j(Z)V

    return-void
.end method

.method static synthetic a(Lfos;[Lmmr;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lfos;->b([Lmmr;)V

    return-void
.end method

.method private a(Lhmv;)V
    .locals 3

    .prologue
    .line 2032
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lfos;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 2033
    invoke-virtual {v1, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2034
    invoke-direct {p0}, Lfos;->aj()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2032
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2035
    return-void
.end method

.method private a(Ljava/lang/String;III)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1839
    .line 1840
    invoke-virtual {p0, p2}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v3

    if-nez p3, :cond_1

    move-object v2, v1

    .line 1841
    :goto_0
    if-nez p4, :cond_2

    move-object v0, v1

    .line 1839
    :goto_1
    invoke-static {v1, v3, v2, v0}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 1843
    if-eqz p1, :cond_0

    .line 1844
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    .line 1846
    :cond_0
    invoke-virtual {p0}, Lfos;->p()Lae;

    move-result-object v1

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v0, v1, p1}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 1847
    return-void

    .line 1841
    :cond_1
    invoke-virtual {p0, p3}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 1842
    :cond_2
    invoke-virtual {p0, p4}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1846
    :cond_3
    const-string p1, "dialog_alert"

    goto :goto_2
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 1990
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1991
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1992
    const-string v2, "tile_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993
    const-string v2, "photo_id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1994
    const-string v2, "passthrough_data"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1995
    const-string v1, "text"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1996
    const-string v1, "placeholder"

    const v2, 0x7f0a01c7

    .line 1997
    invoke-virtual {p0, v2}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 1996
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1998
    const-string v1, "max_length"

    const/16 v2, 0x800

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1999
    const-string v1, "allow_empty"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2000
    new-instance v1, Lfpx;

    invoke-direct {v1}, Lfpx;-><init>()V

    .line 2001
    invoke-virtual {v1, v0}, Lfpx;->f(Landroid/os/Bundle;)V

    .line 2002
    const/4 v0, 0x4

    invoke-virtual {v1, p0, v0}, Lfpx;->a(Lu;I)V

    .line 2003
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    const-string v2, "StoryEditTextFragment"

    invoke-virtual {v1, v0, v2}, Lfpx;->a(Lae;Ljava/lang/String;)V

    .line 2004
    return-void
.end method

.method private a([Lmmr;)V
    .locals 1

    .prologue
    .line 1502
    invoke-direct {p0}, Lfos;->ah()V

    .line 1503
    iput-object p1, p0, Lfos;->ac:[Lmmr;

    .line 1504
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-nez v0, :cond_0

    .line 1506
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfos;->ad:Z

    .line 1510
    :goto_0
    return-void

    .line 1508
    :cond_0
    invoke-direct {p0, p1}, Lfos;->b([Lmmr;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1767
    if-eqz p1, :cond_0

    iget-object v0, p0, Lfos;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/Collection;Z)[Lmmr;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lmms;",
            ">;Z)[",
            "Lmmr;"
        }
    .end annotation

    .prologue
    .line 1468
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v5, v0, [Lmmr;

    .line 1469
    const/4 v1, 0x0

    .line 1470
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 1471
    invoke-direct {p0}, Lfos;->aj()Landroid/os/Bundle;

    move-result-object v6

    .line 1473
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v1

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmms;

    .line 1475
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8, v6}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 1476
    iget-object v2, p0, Lfos;->S:Lmmq;

    const-class v3, Lmml;

    invoke-static {v2, v1, v3}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v2

    check-cast v2, Lmml;

    .line 1478
    iget-object v3, p0, Lfos;->S:Lmmq;

    const-class v9, Lmmc;

    .line 1479
    invoke-static {v3, v1, v9}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v3

    check-cast v3, Lmmc;

    .line 1480
    if-eqz v2, :cond_0

    .line 1481
    const-string v9, "story_moment_type"

    iget v2, v2, Lmml;->c:I

    invoke-virtual {v8, v9, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1483
    :cond_0
    if-eqz v3, :cond_1

    .line 1484
    const-string v2, "story_enrichment_type"

    iget v3, v3, Lmmc;->b:I

    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1487
    :cond_1
    new-instance v3, Lhmr;

    iget-object v2, p0, Lfos;->at:Llnl;

    invoke-direct {v3, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    if-eqz p2, :cond_2

    sget-object v2, Lhmv;->fb:Lhmv;

    .line 1488
    :goto_1
    invoke-virtual {v3, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 1489
    invoke-virtual {v2, v8}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v2

    .line 1487
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 1492
    new-instance v3, Lmmr;

    invoke-direct {v3}, Lmmr;-><init>()V

    .line 1493
    if-eqz p2, :cond_3

    const/4 v2, 0x1

    :goto_2
    iput v2, v3, Lmmr;->b:I

    .line 1494
    iput-object v1, v3, Lmmr;->f:Lmms;

    .line 1495
    add-int/lit8 v1, v4, 0x1

    aput-object v3, v5, v4

    move v4, v1

    .line 1496
    goto :goto_0

    .line 1487
    :cond_2
    sget-object v2, Lhmv;->fc:Lhmv;

    goto :goto_1

    .line 1493
    :cond_3
    const/4 v2, 0x2

    goto :goto_2

    .line 1498
    :cond_4
    return-object v5
.end method

.method private aa()Z
    .locals 1

    .prologue
    .line 1763
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->b:Lodo;

    iget-object v0, v0, Lodo;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lfos;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ab()Z
    .locals 1

    .prologue
    .line 1771
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->i:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ac()Z
    .locals 1

    .prologue
    .line 1775
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ad()Z
    .locals 2

    .prologue
    .line 1779
    iget-object v0, p0, Lfos;->R:Lhoc;

    const-string v1, "GetStoryTask"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private ae()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1787
    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->b:Lodo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->b:Lodo;

    iget-object v0, v0, Lodo;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private af()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1791
    invoke-virtual {p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "story_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private ag()Landroid/graphics/Point;
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 1800
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1801
    const/4 v0, 0x0

    .line 1811
    :goto_0
    return-object v0

    .line 1803
    :cond_0
    invoke-virtual {p0}, Lfos;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1806
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_1

    .line 1807
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1809
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    .line 1810
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v0

    add-float/2addr v2, v4

    float-to-int v2, v2

    .line 1811
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v0

    add-float/2addr v0, v4

    float-to-int v0, v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v1

    goto :goto_0
.end method

.method private ah()V
    .locals 3

    .prologue
    .line 1815
    const/4 v0, 0x0

    const v1, 0x7f0a01c6

    .line 1816
    invoke-virtual {p0, v1}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 1815
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 1818
    invoke-virtual {p0}, Lfos;->p()Lae;

    move-result-object v1

    const-string v2, "dialog_pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 1819
    return-void
.end method

.method private ai()V
    .locals 2

    .prologue
    .line 1822
    .line 1823
    invoke-virtual {p0}, Lfos;->p()Lae;

    move-result-object v0

    const-string v1, "dialog_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 1824
    if-eqz v0, :cond_0

    .line 1825
    invoke-virtual {v0}, Lt;->a()V

    .line 1827
    :cond_0
    return-void
.end method

.method private aj()Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 2038
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2039
    const-string v3, "story_id"

    invoke-direct {p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2040
    iget-object v3, p0, Lfos;->S:Lmmq;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lfos;->S:Lmmq;

    iget-object v3, v3, Lmmq;->g:Lmlz;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lfos;->S:Lmmq;

    iget-object v3, v3, Lmmq;->g:Lmlz;

    iget-object v3, v3, Lmlz;->b:Logr;

    if-eqz v3, :cond_0

    .line 2041
    const-string v3, "story_status"

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2044
    :cond_0
    invoke-virtual {p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "owner_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2045
    iget-object v4, p0, Lfos;->S:Lmmq;

    if-eqz v4, :cond_4

    .line 2046
    const-string v3, "story_viewer_relationship"

    .line 2047
    invoke-direct {p0}, Lfos;->aa()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2046
    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2061
    :goto_1
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-eqz v0, :cond_1

    .line 2062
    const-string v0, "story_fraction_complete"

    iget-object v1, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    .line 2063
    invoke-virtual {v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f()D

    move-result-wide v4

    .line 2062
    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 2065
    :cond_1
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->c:Lmoo;

    if-eqz v0, :cond_2

    .line 2066
    const-string v0, "story_num_moments"

    iget-object v1, p0, Lfos;->S:Lmmq;

    iget-object v1, v1, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->e:Ljava/lang/Integer;

    .line 2067
    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    .line 2066
    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2069
    :cond_2
    return-object v2

    :cond_3
    move v0, v1

    .line 2047
    goto :goto_0

    .line 2049
    :cond_4
    if-eqz v3, :cond_6

    .line 2052
    const-string v4, "story_viewer_relationship"

    .line 2053
    invoke-direct {p0, v3}, Lfos;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2052
    :goto_2
    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 2053
    goto :goto_2

    .line 2057
    :cond_6
    const-string v0, "story_viewer_relationship"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method static synthetic b(Lfos;)V
    .locals 4

    .prologue
    .line 148
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100272

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f100273

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lfos;->ab:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lfos;->Y:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v1}, Landroid/view/View;->getMinimumHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v2

    int-to-float v3, v1

    sub-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method private b([Lmmr;)V
    .locals 10

    .prologue
    .line 1514
    new-instance v0, Lkoe;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lfos;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 1515
    iget-object v9, p0, Lfos;->R:Lhoc;

    new-instance v0, Lfnr;

    iget-object v1, p0, Lfos;->at:Llnl;

    iget-object v2, p0, Lfos;->P:Lhee;

    .line 1516
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lfos;->S:Lmmq;

    iget-object v4, v4, Lmmq;->f:Lmoi;

    iget-object v4, v4, Lmoi;->a:Ljava/lang/Boolean;

    .line 1517
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lfos;->S:Lmmq;

    iget-object v5, v5, Lmmq;->f:Lmoi;

    iget-object v5, v5, Lmoi;->b:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v6, p0, Lfos;->S:Lmmq;

    iget-object v6, v6, Lmmq;->a:Lmok;

    iget-object v6, v6, Lmok;->b:Lmol;

    .line 1519
    invoke-direct {p0}, Lfos;->ag()Landroid/graphics/Point;

    move-result-object v7

    invoke-static {v7}, Lfss;->a(Landroid/graphics/Point;)[Landroid/graphics/Point;

    move-result-object v8

    move-object v7, p1

    invoke-direct/range {v0 .. v8}, Lfnr;-><init>(Landroid/content/Context;ILjava/lang/String;ZZLmol;[Lmmr;[Landroid/graphics/Point;)V

    .line 1515
    invoke-virtual {v9, v0}, Lhoc;->b(Lhny;)V

    .line 1521
    return-void
.end method

.method static synthetic b(Lfos;Z)Z
    .locals 0

    .prologue
    .line 148
    iput-boolean p1, p0, Lfos;->ad:Z

    return p1
.end method

.method static synthetic b(Lfos;[Lmmr;)[Lmmr;
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lfos;->ac:[Lmmr;

    return-object p1
.end method

.method static synthetic c(Lfos;)Lhee;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->P:Lhee;

    return-object v0
.end method

.method static synthetic c(Lfos;Z)Z
    .locals 0

    .prologue
    .line 148
    iput-boolean p1, p0, Lfos;->ab:Z

    return p1
.end method

.method static synthetic d(Lfos;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lfos;Z)Z
    .locals 0

    .prologue
    .line 148
    iput-boolean p1, p0, Lfos;->aj:Z

    return p1
.end method

.method static synthetic e(Lfos;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Lfos;->ag()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lfos;Z)Z
    .locals 0

    .prologue
    .line 148
    iput-boolean p1, p0, Lfos;->aa:Z

    return p1
.end method

.method static synthetic f(Lfos;)Lcom/google/android/apps/plus/stories/views/StoryLayout;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    return-object v0
.end method

.method static synthetic f(Lfos;Z)Z
    .locals 0

    .prologue
    .line 148
    iput-boolean p1, p0, Lfos;->Y:Z

    return p1
.end method

.method static synthetic g(Lfos;)Lfqu;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->U:Lfqu;

    return-object v0
.end method

.method static synthetic h(Lfos;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lfos;->Y()V

    return-void
.end method

.method static synthetic i(Lfos;)Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lfos;->Y:Z

    return v0
.end method

.method private j(Z)V
    .locals 14

    .prologue
    .line 962
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-nez v0, :cond_1

    .line 986
    :cond_0
    :goto_0
    return-void

    .line 965
    :cond_1
    invoke-direct {p0}, Lfos;->ag()Landroid/graphics/Point;

    move-result-object v7

    .line 966
    iget v0, v7, Landroid/graphics/Point;->x:I

    if-eqz v0, :cond_0

    iget v0, v7, Landroid/graphics/Point;->y:I

    if-eqz v0, :cond_0

    .line 969
    iget-object v0, p0, Lfos;->S:Lmmq;

    iget v1, v7, Landroid/graphics/Point;->x:I

    iget v2, v7, Landroid/graphics/Point;->y:I

    mul-int/2addr v1, v2

    int-to-float v8, v1

    const/4 v5, -0x1

    const/4 v3, 0x1

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v4, 0x0

    iget-object v9, v0, Lmmq;->h:[Lmmt;

    array-length v10, v9

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v10, :cond_2

    aget-object v0, v9, v6

    iget-object v0, v0, Lmmt;->e:Lmoa;

    if-eqz v0, :cond_6

    const/high16 v2, 0x3f800000    # 1.0f

    iget v11, v7, Landroid/graphics/Point;->x:I

    int-to-float v11, v11

    iget-object v12, v0, Lmoa;->a:Ljava/lang/Float;

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    div-float/2addr v11, v12

    iget v12, v7, Landroid/graphics/Point;->y:I

    int-to-float v12, v12

    iget-object v13, v0, Lmoa;->b:Ljava/lang/Float;

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v13

    div-float/2addr v12, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v2, v11}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget-object v11, v0, Lmoa;->a:Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    mul-float/2addr v11, v2

    iget-object v0, v0, Lmoa;->b:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v2

    mul-float/2addr v0, v11

    sub-float v0, v8, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v11, v0, v1

    if-gtz v11, :cond_6

    cmpl-float v11, v2, v3

    if-ltz v11, :cond_6

    move v1, v2

    move v2, v4

    :goto_2
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v5, v2

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_2
    if-lez v4, :cond_3

    const/4 v0, -0x1

    if-ne v5, v0, :cond_3

    const/4 v5, 0x0

    .line 971
    :cond_3
    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    aget-object v0, v0, v5

    iget-object v0, v0, Lmmt;->e:Lmoa;

    .line 972
    iget-object v1, v0, Lmoa;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v0, v0, Lmoa;->b:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float/2addr v1, v0

    .line 973
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 974
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 975
    int-to-float v3, v2

    div-float/2addr v3, v1

    int-to-float v4, v0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    int-to-float v0, v2

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 978
    :cond_4
    if-nez p1, :cond_5

    iget-object v1, p0, Lfos;->U:Lfqu;

    invoke-virtual {v1}, Lfqu;->b()Lfrb;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lfos;->U:Lfqu;

    .line 979
    invoke-virtual {v1}, Lfqu;->b()Lfrb;

    move-result-object v1

    invoke-virtual {v1}, Lfrb;->a()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 983
    :cond_5
    iget-object v1, p0, Lfos;->U:Lfqu;

    invoke-virtual {v1, v5, v0}, Lfqu;->a(II)V

    .line 984
    iget-object v0, p0, Lfos;->U:Lfqu;

    invoke-virtual {v0}, Lfqu;->b()Lfrb;

    move-result-object v0

    .line 985
    iget-object v1, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Lfrb;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v2, v5

    move v1, v3

    goto :goto_2
.end method

.method static synthetic j(Lfos;)Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lfos;->al:Z

    return v0
.end method

.method static synthetic k(Lfos;)Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lfos;->ad:Z

    return v0
.end method

.method static synthetic l(Lfos;)[Lmmr;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->ac:[Lmmr;

    return-object v0
.end method

.method static synthetic m(Lfos;)Lmmq;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->S:Lmmq;

    return-object v0
.end method

.method static synthetic n(Lfos;)Lbc;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->ap:Lbc;

    return-object v0
.end method

.method static synthetic o(Lfos;)Llnl;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->at:Llnl;

    return-object v0
.end method

.method static synthetic p(Lfos;)Llnl;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->at:Llnl;

    return-object v0
.end method

.method static synthetic q(Lfos;)Llnl;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->at:Llnl;

    return-object v0
.end method

.method static synthetic r(Lfos;)Z
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Lfos;->aa()Z

    move-result v0

    return v0
.end method

.method static synthetic s(Lfos;)Lhoc;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->R:Lhoc;

    return-object v0
.end method

.method static synthetic t(Lfos;)Z
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Lfos;->ad()Z

    move-result v0

    return v0
.end method

.method static synthetic u(Lfos;)Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lfos;->ai:Z

    return v0
.end method

.method static synthetic v(Lfos;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lfos;->V()V

    return-void
.end method

.method static synthetic w(Lfos;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lfos;->ai()V

    return-void
.end method

.method static synthetic x(Lfos;)Lhje;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->O:Lhje;

    return-object v0
.end method

.method static synthetic y(Lfos;)Llnl;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfos;->at:Llnl;

    return-object v0
.end method

.method static synthetic z(Lfos;)Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lfos;->aa:Z

    return v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 1048
    sget-object v0, Lhmw;->aC:Lhmw;

    return-object v0
.end method

.method protected U()V
    .locals 2

    .prologue
    .line 2015
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2016
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 538
    const v0, 0x7f0400e4

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 539
    const v0, 0x7f100328

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/stories/views/StoryLayout;

    iput-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    .line 540
    sget-object v0, Lfos;->N:Lloz;

    .line 541
    new-instance v0, Lfqu;

    iget-object v4, p0, Lfos;->at:Llnl;

    invoke-direct {v0, v4, p0, p0}, Lfqu;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Lfra;)V

    iput-object v0, p0, Lfos;->U:Lfqu;

    .line 548
    if-eqz p3, :cond_1

    .line 549
    const-string v0, "story_end_card"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 550
    const v0, 0x7f10026b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 551
    const v0, 0x7f10032a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 552
    iput-boolean v1, p0, Lfos;->Y:Z

    .line 553
    const-string v0, "story_show_promo"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfos;->ab:Z

    .line 554
    iget-boolean v0, p0, Lfos;->ab:Z

    if-eqz v0, :cond_0

    .line 555
    const v0, 0x7f100272

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 558
    :cond_0
    const-string v0, "has_seen_end_card"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfos;->Z:Z

    .line 561
    :cond_1
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 563
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_2

    .line 564
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 567
    :cond_2
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    iget-object v4, p0, Lfos;->U:Lfqu;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Lfqu;)V

    .line 568
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Lfte;)V

    .line 569
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Lftd;)V

    .line 570
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Lftc;)V

    .line 572
    const v0, 0x7f1004e0

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 573
    const v0, 0x7f100552

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 574
    const v0, 0x7f100226

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 575
    const v0, 0x7f10055e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 576
    const v0, 0x7f10026d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 577
    const v0, 0x7f100273

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 579
    iget-boolean v0, p0, Lfos;->al:Z

    if-nez v0, :cond_3

    .line 580
    iget-boolean v0, p0, Lfos;->Y:Z

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lfos;->a(Z)V

    .line 583
    :cond_3
    return-object v3

    :cond_4
    move v0, v2

    .line 580
    goto :goto_0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 16

    .prologue
    .line 1270
    packed-switch p1, :pswitch_data_0

    .line 1464
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1272
    :pswitch_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lfos;->al:Z

    .line 1273
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lfos;->X:Z

    if-eqz v2, :cond_1

    .line 1274
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lfos;->b(Z)V

    .line 1278
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->ak:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 1279
    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->ak:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1281
    :cond_2
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 1282
    const-string v2, "stories_edit_type"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1284
    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 1286
    const-string v2, "stories_edit_place"

    .line 1287
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lfof;

    .line 1288
    new-instance v4, Lmmr;

    invoke-direct {v4}, Lmmr;-><init>()V

    .line 1289
    const/16 v3, 0x9

    iput v3, v4, Lmmr;->b:I

    .line 1290
    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->W:Lmms;

    iput-object v3, v4, Lmmr;->f:Lmms;

    .line 1291
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lfos;->W:Lmms;

    .line 1292
    invoke-virtual {v2}, Lfof;->m()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Lfof;->n()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Lfof;->o()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v2, "StoryLocationUtils"

    const-string v3, "Invalid locations"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_1
    iput-object v2, v4, Lmmr;->d:Lmmo;

    .line 1293
    iget-object v2, v4, Lmmr;->d:Lmmo;

    if-eqz v2, :cond_7

    .line 1294
    const/4 v2, 0x1

    new-array v2, v2, [Lmmr;

    const/4 v3, 0x0

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lfos;->a([Lmmr;)V

    .line 1295
    sget-object v2, Lhmv;->fe:Lhmv;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lfos;->a(Lhmv;)V

    goto/16 :goto_0

    .line 1292
    :cond_3
    new-instance v3, Lmmo;

    invoke-direct {v3}, Lmmo;-><init>()V

    new-instance v5, Lofq;

    invoke-direct {v5}, Lofq;-><init>()V

    iput-object v5, v3, Lmmo;->b:Lofq;

    iget-object v5, v3, Lmmo;->b:Lofq;

    invoke-virtual {v2}, Lfof;->b()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lofq;->a:Ljava/lang/Integer;

    iget-object v5, v3, Lmmo;->b:Lofq;

    invoke-virtual {v2}, Lfof;->c()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lofq;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Lfof;->m()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    iput v5, v3, Lmmo;->c:I

    iget-object v5, v3, Lmmo;->b:Lofq;

    new-instance v6, Lofr;

    invoke-direct {v6}, Lofr;-><init>()V

    iput-object v6, v5, Lofq;->h:Lofr;

    iget-object v5, v3, Lmmo;->b:Lofq;

    iget-object v5, v5, Lofq;->h:Lofr;

    invoke-virtual {v2}, Lfof;->f()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lofr;->a:Ljava/lang/String;

    iget-object v5, v3, Lmmo;->b:Lofq;

    iget-object v5, v5, Lofq;->h:Lofr;

    invoke-virtual {v2}, Lfof;->g()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lofr;->b:Ljava/lang/String;

    iget-object v5, v3, Lmmo;->b:Lofq;

    invoke-virtual {v2}, Lfof;->e()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lofq;->f:Ljava/lang/String;

    iget-object v5, v3, Lmmo;->b:Lofq;

    invoke-virtual {v2}, Lfof;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lofq;->e:Ljava/lang/String;

    :cond_4
    :goto_2
    move-object v2, v3

    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Lfof;->o()Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x5

    iput v5, v3, Lmmo;->c:I

    invoke-virtual {v2}, Lfof;->h()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lmmo;->f:Ljava/lang/String;

    iget-object v5, v3, Lmmo;->b:Lofq;

    invoke-virtual {v2}, Lfof;->e()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lofq;->f:Ljava/lang/String;

    iget-object v5, v3, Lmmo;->b:Lofq;

    invoke-virtual {v2}, Lfof;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lofq;->e:Ljava/lang/String;

    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Lfof;->n()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x4

    iput v5, v3, Lmmo;->c:I

    new-instance v5, Lodo;

    invoke-direct {v5}, Lodo;-><init>()V

    iput-object v5, v3, Lmmo;->e:Lodo;

    iget-object v5, v3, Lmmo;->e:Lodo;

    invoke-virtual {v2}, Lfof;->i()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lodo;->b:Ljava/lang/String;

    iget-object v5, v3, Lmmo;->e:Lodo;

    invoke-virtual {v2}, Lfof;->j()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lodo;->c:Ljava/lang/String;

    iget-object v5, v3, Lmmo;->e:Lodo;

    invoke-virtual {v2}, Lfof;->k()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lodo;->d:Ljava/lang/Boolean;

    iget-object v5, v3, Lmmo;->e:Lodo;

    invoke-virtual {v2}, Lfof;->l()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v5, Lodo;->e:Ljava/lang/Boolean;

    goto :goto_2

    .line 1297
    :cond_7
    const-string v2, "HostedStoryFragment"

    const-string v3, "Invalid place location."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1299
    :cond_8
    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    .line 1300
    new-instance v3, Lmmr;

    invoke-direct {v3}, Lmmr;-><init>()V

    .line 1301
    const/4 v2, 0x1

    iput v2, v3, Lmmr;->b:I

    .line 1302
    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->W:Lmms;

    iput-object v2, v3, Lmmr;->f:Lmms;

    .line 1303
    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->S:Lmmq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfos;->W:Lmms;

    const-class v5, Lmml;

    .line 1304
    invoke-static {v2, v4, v5}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v2

    check-cast v2, Lmml;

    .line 1305
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lfos;->W:Lmms;

    .line 1306
    const/4 v4, 0x1

    new-array v4, v4, [Lmmr;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lfos;->a([Lmmr;)V

    .line 1307
    invoke-direct/range {p0 .. p0}, Lfos;->aj()Landroid/os/Bundle;

    move-result-object v3

    .line 1308
    if-eqz v2, :cond_9

    .line 1309
    const-string v4, "story_moment_type"

    iget v2, v2, Lmml;->c:I

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1311
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v2, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhms;

    new-instance v4, Lhmr;

    move-object/from16 v0, p0

    iget-object v5, v0, Lfos;->at:Llnl;

    invoke-direct {v4, v5}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v5, Lhmv;->fb:Lhmv;

    .line 1312
    invoke-virtual {v4, v5}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v4

    .line 1313
    invoke-virtual {v4, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v3

    .line 1311
    invoke-interface {v2, v3}, Lhms;->a(Lhmr;)V

    goto/16 :goto_0

    .line 1314
    :cond_a
    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 1315
    const-string v2, "stories_edit_place"

    .line 1316
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lfof;

    .line 1317
    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->au:Llnh;

    const-class v4, Lilw;

    .line 1318
    invoke-virtual {v3, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lilw;

    .line 1320
    invoke-virtual {v2}, Lfof;->b()I

    move-result v4

    invoke-virtual {v2}, Lfof;->c()I

    move-result v2

    .line 1319
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ling;

    int-to-double v8, v4

    const-wide v10, 0x416312d000000000L    # 1.0E7

    div-double/2addr v8, v10

    const-wide v10, 0x3f7eb851e0000000L    # 0.007499999832361937

    sub-double/2addr v8, v10

    int-to-double v10, v2

    const-wide v12, 0x416312d000000000L    # 1.0E7

    div-double/2addr v10, v12

    const-wide v12, 0x3f7eb851e0000000L    # 0.007499999832361937

    sub-double/2addr v10, v12

    const-wide v12, 0x4066800000000000L    # 180.0

    add-double/2addr v10, v12

    const-wide v12, 0x4076800000000000L    # 360.0

    rem-double/2addr v10, v12

    const-wide v12, 0x4066800000000000L    # 180.0

    sub-double/2addr v10, v12

    invoke-direct {v6, v8, v9, v10, v11}, Ling;-><init>(DD)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Ling;

    int-to-double v8, v4

    const-wide v10, 0x416312d000000000L    # 1.0E7

    div-double/2addr v8, v10

    const-wide v10, 0x3f7eb851e0000000L    # 0.007499999832361937

    add-double/2addr v8, v10

    int-to-double v10, v2

    const-wide v12, 0x416312d000000000L    # 1.0E7

    div-double/2addr v10, v12

    const-wide v12, 0x3f7eb851e0000000L    # 0.007499999832361937

    add-double/2addr v10, v12

    const-wide v12, 0x4066800000000000L    # 180.0

    add-double/2addr v10, v12

    const-wide v12, 0x4076800000000000L    # 360.0

    rem-double/2addr v10, v12

    const-wide v12, 0x4066800000000000L    # 180.0

    sub-double/2addr v10, v12

    invoke-direct {v6, v8, v9, v10, v11}, Ling;-><init>(DD)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321
    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->au:Llnh;

    const-class v4, Lini;

    invoke-virtual {v2, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lini;

    .line 1322
    invoke-interface {v2, v5}, Lini;->a(Ljava/util/List;)Linh;

    move-result-object v2

    .line 1323
    invoke-interface {v3, v2}, Lilw;->a(Linh;)Lilw;

    .line 1324
    invoke-virtual/range {p0 .. p0}, Lfos;->n()Lz;

    move-result-object v2

    invoke-interface {v3, v2}, Lilw;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 1325
    if-nez v2, :cond_b

    .line 1326
    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->at:Llnl;

    const v3, 0x7f0a01c3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 1327
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1329
    :cond_b
    const/16 v3, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lfos;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1336
    :pswitch_2
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 1337
    const-string v2, "element_selection"

    .line 1338
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lfoi;

    .line 1339
    if-eqz v2, :cond_0

    .line 1340
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lfoi;->a(Z)Ljava/util/Collection;

    move-result-object v3

    .line 1343
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lfoi;->a(Z)Ljava/util/Collection;

    move-result-object v2

    .line 1344
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1347
    :cond_c
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lfos;->a(Ljava/util/Collection;Z)[Lmmr;

    move-result-object v3

    .line 1348
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lfos;->a(Ljava/util/Collection;Z)[Lmmr;

    move-result-object v2

    .line 1350
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1351
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1352
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1353
    const/4 v2, 0x0

    new-array v2, v2, [Lmmr;

    invoke-interface {v4, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lmmr;

    .line 1355
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lfos;->a([Lmmr;)V

    goto/16 :goto_0

    .line 1361
    :pswitch_3
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_d

    .line 1362
    new-instance v2, Lmmr;

    invoke-direct {v2}, Lmmr;-><init>()V

    .line 1363
    const/4 v3, 0x5

    iput v3, v2, Lmmr;->b:I

    .line 1364
    const-string v3, "text"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lmmr;->c:Ljava/lang/String;

    .line 1365
    const/4 v3, 0x1

    new-array v3, v3, [Lmmr;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lfos;->a([Lmmr;)V

    .line 1366
    sget-object v2, Lhmv;->fd:Lhmv;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lfos;->a(Lhmv;)V

    goto/16 :goto_0

    .line 1367
    :cond_d
    if-nez p2, :cond_0

    goto/16 :goto_0

    .line 1377
    :pswitch_4
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_e

    .line 1378
    invoke-direct/range {p0 .. p0}, Lfos;->ah()V

    .line 1379
    const-string v2, "passthrough_data"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    .line 1380
    move-object/from16 v0, p0

    iget-object v15, v0, Lfos;->R:Lhoc;

    new-instance v2, Lfnu;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfos;->P:Lhee;

    .line 1382
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    const-string v5, "tile_id"

    .line 1383
    invoke-virtual {v6, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "photo_id"

    .line 1384
    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    const-string v8, "text"

    .line 1385
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lfos;->S:Lmmq;

    .line 1386
    invoke-static {v9}, Lfoa;->a(Lmmq;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lfos;->S:Lmmq;

    iget-object v10, v10, Lmmq;->a:Lmok;

    iget-object v10, v10, Lmok;->a:Ljava/lang/String;

    .line 1388
    invoke-virtual/range {p0 .. p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "auth_key"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 1391
    invoke-direct/range {p0 .. p0}, Lfos;->ag()Landroid/graphics/Point;

    move-result-object v14

    invoke-static {v14}, Lfss;->a(Landroid/graphics/Point;)[Landroid/graphics/Point;

    move-result-object v14

    invoke-direct/range {v2 .. v14}, Lfnu;-><init>(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;)V

    .line 1380
    invoke-virtual {v15, v2}, Lhoc;->b(Lhny;)V

    goto/16 :goto_0

    .line 1392
    :cond_e
    if-nez p2, :cond_0

    goto/16 :goto_0

    .line 1398
    :pswitch_5
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 1399
    new-instance v2, Lfnq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfos;->P:Lhee;

    .line 1401
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    .line 1402
    invoke-direct/range {p0 .. p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v5

    .line 1403
    invoke-virtual/range {p0 .. p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "auth_key"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    .line 1405
    move-object/from16 v0, p0

    iget-object v8, v0, Lfos;->S:Lmmq;

    iget-object v8, v8, Lmmq;->k:Ljava/lang/String;

    .line 1406
    invoke-direct/range {p0 .. p0}, Lfos;->ae()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Lfnq;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 1407
    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->R:Lhoc;

    invoke-virtual {v3}, Lhoc;->d()Lhos;

    move-result-object v3

    invoke-virtual {v3, v2}, Lhos;->a(Lhny;)V

    .line 1408
    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->R:Lhoc;

    invoke-virtual {v3, v2}, Lhoc;->b(Lhny;)V

    goto/16 :goto_0

    .line 1413
    :pswitch_6
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 1414
    new-instance v2, Lfnq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfos;->P:Lhee;

    .line 1416
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    .line 1417
    invoke-direct/range {p0 .. p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v5

    .line 1418
    invoke-virtual/range {p0 .. p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "auth_key"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    .line 1420
    move-object/from16 v0, p0

    iget-object v8, v0, Lfos;->S:Lmmq;

    iget-object v8, v8, Lmmq;->k:Ljava/lang/String;

    .line 1421
    invoke-direct/range {p0 .. p0}, Lfos;->ae()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Lfnq;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 1422
    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->R:Lhoc;

    invoke-virtual {v3}, Lhoc;->d()Lhos;

    move-result-object v3

    invoke-virtual {v3, v2}, Lhos;->a(Lhny;)V

    .line 1423
    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->R:Lhoc;

    invoke-virtual {v3, v2}, Lhoc;->b(Lhny;)V

    goto/16 :goto_0

    .line 1428
    :pswitch_7
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 1429
    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->S:Lmmq;

    iget-object v2, v2, Lmmq;->g:Lmlz;

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->S:Lmmq;

    iget-object v2, v2, Lmmq;->g:Lmlz;

    iget-object v2, v2, Lmlz;->b:Logr;

    if-nez v2, :cond_10

    .line 1431
    :cond_f
    new-instance v2, Lfns;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfos;->P:Lhee;

    .line 1432
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct/range {p0 .. p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v5

    .line 1433
    invoke-virtual/range {p0 .. p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "auth_key"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1434
    invoke-virtual/range {p0 .. p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "gpinv"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lfns;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;)V

    .line 1435
    const-string v3, "story_share_refresh"

    invoke-virtual {v2, v3}, Lfns;->b(Ljava/lang/String;)Lhny;

    .line 1436
    move-object/from16 v0, p0

    iget-object v3, v0, Lfos;->R:Lhoc;

    invoke-virtual {v3, v2}, Lhoc;->b(Lhny;)V

    .line 1437
    invoke-direct/range {p0 .. p0}, Lfos;->Y()V

    .line 1439
    :cond_10
    sget-object v2, Lhmv;->eZ:Lhmv;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lfos;->a(Lhmv;)V

    goto/16 :goto_0

    .line 1444
    :pswitch_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lfos;->Y:Z

    if-nez v2, :cond_0

    .line 1445
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lfos;->a(Z)V

    goto/16 :goto_0

    .line 1450
    :pswitch_9
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    .line 1451
    move-object/from16 v0, p0

    iget-object v2, v0, Lfos;->au:Llnh;

    const-class v3, Lilu;

    invoke-virtual {v2, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lilu;

    .line 1452
    invoke-virtual/range {p0 .. p0}, Lfos;->n()Lz;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-interface {v2, v3, v0}, Lilu;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 1454
    new-instance v3, Lmmr;

    invoke-direct {v3}, Lmmr;-><init>()V

    .line 1455
    const/16 v4, 0x9

    iput v4, v3, Lmmr;->b:I

    .line 1456
    move-object/from16 v0, p0

    iget-object v4, v0, Lfos;->W:Lmms;

    iput-object v4, v3, Lmmr;->f:Lmms;

    .line 1457
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lfos;->W:Lmms;

    .line 1458
    new-instance v4, Lmmo;

    invoke-direct {v4}, Lmmo;-><init>()V

    if-eqz v2, :cond_14

    new-instance v5, Lofq;

    invoke-direct {v5}, Lofq;-><init>()V

    iput-object v5, v4, Lmmo;->b:Lofq;

    const/4 v5, 0x5

    iput v5, v4, Lmmo;->c:I

    invoke-interface {v2}, Lilu;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_11

    invoke-interface {v2}, Lilu;->a()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lmmo;->f:Ljava/lang/String;

    :cond_11
    invoke-interface {v2}, Lilu;->d()Ling;

    move-result-object v5

    if-eqz v5, :cond_12

    iget-object v5, v4, Lmmo;->b:Lofq;

    invoke-interface {v2}, Lilu;->d()Ling;

    move-result-object v6

    iget-wide v6, v6, Ling;->a:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-static {v6}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v6

    const-wide v8, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v6, v8

    double-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lofq;->a:Ljava/lang/Integer;

    iget-object v5, v4, Lmmo;->b:Lofq;

    invoke-interface {v2}, Lilu;->d()Ling;

    move-result-object v6

    iget-wide v6, v6, Ling;->b:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-static {v6}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v6

    const-wide v8, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v6, v8

    double-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lofq;->b:Ljava/lang/Integer;

    :cond_12
    invoke-interface {v2}, Lilu;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_13

    iget-object v5, v4, Lmmo;->b:Lofq;

    invoke-interface {v2}, Lilu;->b()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lofq;->f:Ljava/lang/String;

    :cond_13
    invoke-interface {v2}, Lilu;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_14

    iget-object v5, v4, Lmmo;->b:Lofq;

    invoke-interface {v2}, Lilu;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lofq;->e:Ljava/lang/String;

    :cond_14
    iput-object v4, v3, Lmmr;->d:Lmmo;

    .line 1459
    const/4 v2, 0x1

    new-array v2, v2, [Lmmr;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lfos;->a([Lmmr;)V

    .line 1460
    sget-object v2, Lhmv;->fe:Lhmv;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lfos;->a(Lhmv;)V

    goto/16 :goto_0

    .line 1270
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1869
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1873
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 485
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 486
    iget-object v0, p0, Lfos;->R:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 487
    invoke-virtual {p0}, Lfos;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lfos;->ao:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 488
    if-nez p1, :cond_0

    .line 489
    invoke-direct {p0}, Lfos;->aj()Landroid/os/Bundle;

    move-result-object v1

    .line 490
    const-string v0, "device_info"

    new-instance v2, Lhyt;

    .line 491
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v3

    invoke-static {v3}, Lhmt;->b(Landroid/content/Context;)Lmwg;

    move-result-object v3

    invoke-direct {v2, v3}, Lhyt;-><init>(Loxu;)V

    .line 490
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 492
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lfos;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->eV:Lhmv;

    .line 493
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 494
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 492
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 517
    :goto_0
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v1, Likp;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Likp;

    invoke-interface {v0}, Likp;->a()Liko;

    move-result-object v1

    .line 518
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v2, Lijl;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lijl;

    .line 519
    invoke-interface {v0, v1}, Lijl;->a(Lijj;)Lijl;

    move-result-object v0

    .line 520
    invoke-interface {v0}, Lijl;->a()Lijk;

    move-result-object v0

    iput-object v0, p0, Lfos;->Q:Lijk;

    .line 521
    return-void

    .line 496
    :cond_0
    const-string v0, "story_load"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfos;->V:I

    .line 497
    const-string v0, "has_refreshed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfos;->ai:Z

    .line 498
    const-string v0, "passive_refresh"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfos;->aj:Z

    .line 499
    const-string v0, "showing_location_picker"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfos;->al:Z

    .line 500
    iget-boolean v0, p0, Lfos;->al:Z

    if-eqz v0, :cond_1

    .line 501
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfos;->X:Z

    .line 503
    :cond_1
    const-string v0, "story_editable_element"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 504
    const-string v0, "story_editable_element"

    .line 505
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    new-instance v2, Lmms;

    invoke-direct {v2}, Lmms;-><init>()V

    invoke-virtual {v0, v2}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lmms;

    iput-object v0, p0, Lfos;->W:Lmms;

    .line 507
    :cond_2
    const-string v0, "story_edits"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 508
    const-string v0, "story_edits"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 509
    array-length v0, v2

    new-array v0, v0, [Lmmr;

    iput-object v0, p0, Lfos;->ac:[Lmmr;

    .line 510
    :goto_1
    array-length v0, v2

    if-ge v1, v0, :cond_3

    .line 511
    iget-object v3, p0, Lfos;->ac:[Lmmr;

    aget-object v0, v2, v1

    check-cast v0, Lhyt;

    new-instance v4, Lmmr;

    invoke-direct {v4}, Lmmr;-><init>()V

    invoke-virtual {v0, v4}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lmmr;

    aput-object v0, v3, v1

    .line 510
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 514
    :cond_3
    const-string v0, "story_pending_edits"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfos;->ad:Z

    goto/16 :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1851
    const-string v0, "edit_conflict"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1852
    invoke-virtual {p0}, Lfos;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbb;->b(I)Ldo;

    move-result-object v0

    .line 1853
    check-cast v0, Lfqd;

    invoke-virtual {v0}, Lfqd;->f()V

    .line 1857
    :cond_0
    :goto_0
    return-void

    .line 1854
    :cond_1
    const-string v0, "edit_error"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855
    iget-object v0, p0, Lfos;->ac:[Lmmr;

    invoke-direct {p0, v0}, Lfos;->a([Lmmr;)V

    goto :goto_0
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 707
    const-string v0, ""

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 708
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-eqz v0, :cond_2

    .line 709
    invoke-direct {p0}, Lfos;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 710
    const v0, 0x7f100665

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 712
    :cond_0
    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->i:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 713
    const v0, 0x7f100666

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 715
    :cond_1
    invoke-direct {p0}, Lfos;->aa()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 716
    const v0, 0x7f100667

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 717
    const v0, 0x7f100669

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 718
    const v0, 0x7f10066a

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 722
    :goto_0
    const v0, 0x7f100668

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 723
    const v0, 0x7f10066c

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 724
    const v0, 0x7f10066d

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 726
    :cond_2
    return-void

    .line 720
    :cond_3
    const v0, 0x7f10066b

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected a(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2019
    invoke-direct {p0}, Lfos;->X()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2020
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2021
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2022
    const v0, 0x7f10025f

    .line 2023
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2024
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2025
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2026
    const v0, 0x7f100260

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    .line 2027
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2029
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 7

    .prologue
    const v4, 0x7f0a01be

    const/4 v6, 0x4

    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1526
    invoke-virtual {p3, v1}, Lhos;->a(Z)V

    .line 1527
    const-string v2, "EditStoryTask"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "PhotosEditCaptionTask"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1528
    :cond_0
    invoke-direct {p0}, Lfos;->ai()V

    .line 1529
    const-string v0, "EditStoryTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1531
    new-instance v0, Lkoe;

    invoke-direct {v0, v6}, Lkoe;-><init>(I)V

    iget-object v2, p0, Lfos;->at:Llnl;

    invoke-virtual {v0, v2}, Lkoe;->a(Landroid/content/Context;)V

    .line 1533
    :cond_1
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1534
    iput-object v3, p0, Lfos;->ac:[Lmmr;

    .line 1602
    :cond_2
    :goto_0
    return-void

    .line 1536
    :cond_3
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "edit_succeeded"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1538
    if-eqz v0, :cond_4

    .line 1539
    iput-object v3, p0, Lfos;->ac:[Lmmr;

    .line 1541
    iget-object v0, p0, Lfos;->U:Lfqu;

    invoke-virtual {v0, v3}, Lfqu;->a(Lfob;)V

    .line 1542
    const/4 v0, 0x2

    iput v0, p0, Lfos;->V:I

    .line 1543
    invoke-virtual {p0, v4}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfos;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1546
    :cond_4
    invoke-virtual {p2}, Lhoz;->a()I

    move-result v0

    .line 1548
    invoke-virtual {p2}, Lhoz;->a()I

    move-result v2

    invoke-virtual {p2}, Lhoz;->b()Ljava/lang/Exception;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x29

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Story edit failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1547
    packed-switch v0, :pswitch_data_0

    .line 1556
    div-int/lit8 v0, v0, 0x64

    if-ne v0, v6, :cond_5

    .line 1558
    const v0, 0x7f0a01b6

    .line 1598
    :goto_1
    const v1, 0x7f0a0596

    invoke-direct {p0, v0, v1}, Lfos;->a(II)V

    goto :goto_0

    .line 1552
    :pswitch_0
    const-string v0, "edit_conflict"

    const v2, 0x7f0a01b5

    const v3, 0x7f0a01b9

    invoke-direct {p0, v0, v2, v3, v1}, Lfos;->a(Ljava/lang/String;III)V

    goto :goto_0

    .line 1561
    :cond_5
    const-string v0, "edit_error"

    const v1, 0x7f0a01b7

    const v2, 0x7f0a01ba

    const v3, 0x7f0a01b8

    invoke-direct {p0, v0, v1, v2, v3}, Lfos;->a(Ljava/lang/String;III)V

    goto :goto_0

    .line 1569
    :cond_6
    const-string v2, "GetStoryTask"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1570
    invoke-direct {p0}, Lfos;->ai()V

    .line 1571
    iget-object v0, p0, Lfos;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 1572
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1573
    iget-object v0, p0, Lfos;->at:Llnl;

    invoke-static {v0, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1575
    :cond_7
    const-string v2, "DeleteStoryTask"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1576
    invoke-direct {p0}, Lfos;->ai()V

    .line 1579
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1580
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "delete_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1581
    if-ne v1, v0, :cond_8

    .line 1582
    const v0, 0x7f0a01f9

    goto :goto_1

    .line 1583
    :cond_8
    if-nez v1, :cond_2

    .line 1584
    const v0, 0x7f0a01f8

    const v1, 0x7f0a0596

    invoke-direct {p0, v0, v1}, Lfos;->a(II)V

    goto/16 :goto_0

    .line 1586
    :cond_9
    if-eqz p2, :cond_2

    .line 1587
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    goto/16 :goto_0

    .line 1589
    :cond_a
    const-string v2, "story_share_refresh"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1593
    if-eqz p2, :cond_b

    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "has_share"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iget-object v3, p0, Lfos;->S:Lmmq;

    iget-object v3, v3, Lmmq;->g:Lmlz;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lfos;->S:Lmmq;

    iget-object v3, v3, Lmmq;->g:Lmlz;

    iget-object v3, v3, Lmlz;->b:Logr;

    if-eqz v3, :cond_c

    :goto_2
    if-ne v2, v0, :cond_2

    .line 1595
    :cond_b
    invoke-direct {p0}, Lfos;->Y()V

    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 1593
    goto :goto_2

    .line 1597
    :cond_d
    const-string v0, "ReportStoryAbuseTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1598
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x7f0a01ff

    goto/16 :goto_1

    :cond_e
    const v0, 0x7f0a01fe

    goto/16 :goto_1

    .line 1547
    nop

    :pswitch_data_0
    .packed-switch 0x199
        :pswitch_0
    .end packed-switch
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 697
    invoke-virtual {p1, v0}, Loo;->b(Z)V

    .line 698
    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 699
    return-void
.end method

.method public a(Z)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x12c

    const/16 v6, 0x13

    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 846
    iput-boolean p1, p0, Lfos;->X:Z

    .line 847
    iget-object v0, p0, Lfos;->U:Lfqu;

    if-eqz v0, :cond_0

    .line 848
    iget-boolean v0, p0, Lfos;->X:Z

    if-eqz v0, :cond_2

    .line 849
    iget-object v0, p0, Lfos;->U:Lfqu;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lfqu;->onSystemUiVisibilityChange(I)V

    .line 855
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    .line 856
    if-nez v0, :cond_3

    .line 901
    :cond_1
    :goto_1
    return-void

    .line 851
    :cond_2
    iget-object v0, p0, Lfos;->U:Lfqu;

    invoke-virtual {v0, v2}, Lfqu;->onSystemUiVisibilityChange(I)V

    goto :goto_0

    .line 860
    :cond_3
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 861
    if-eqz v0, :cond_1

    .line 865
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    .line 866
    if-eqz v3, :cond_1

    .line 870
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    .line 871
    if-eqz p1, :cond_6

    .line 872
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v6, :cond_5

    .line 873
    const/16 v4, 0xf06

    invoke-virtual {v3, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 884
    :goto_2
    invoke-virtual {v0}, Loo;->g()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 885
    invoke-virtual {v0}, Loo;->f()V

    .line 900
    :cond_4
    :goto_3
    if-nez p1, :cond_8

    move v0, v1

    :goto_4
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v1

    const v3, 0x7f100329

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_9

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_1

    .line 882
    :cond_5
    invoke-virtual {v3, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_2

    .line 888
    :cond_6
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v6, :cond_7

    .line 889
    const/16 v4, 0x700

    invoke-virtual {v3, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 896
    :goto_5
    invoke-virtual {v0}, Loo;->g()Z

    move-result v3

    if-nez v3, :cond_4

    .line 897
    invoke-virtual {v0}, Loo;->e()V

    goto :goto_3

    .line 894
    :cond_7
    invoke-virtual {v3, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_5

    :cond_8
    move v0, v2

    .line 900
    goto :goto_4

    :cond_9
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v2, Lfoy;

    invoke-direct {v2, v1}, Lfoy;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_1
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 807
    iget-boolean v0, p0, Lfos;->al:Z

    if-eqz v0, :cond_0

    .line 809
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    const-string v1, "StoryEditLocationFragment"

    .line 810
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lfpr;

    .line 811
    if-eqz v0, :cond_0

    .line 812
    invoke-virtual {v0}, Lfpr;->a()V

    .line 813
    const/4 v0, 0x1

    .line 816
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 730
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 731
    const v4, 0x7f100666

    if-ne v0, v4, :cond_6

    .line 737
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g()I

    move-result v0

    if-nez v0, :cond_2

    .line 738
    :cond_0
    :goto_0
    iget-object v0, p0, Lfos;->at:Llnl;

    iget-object v1, p0, Lfos;->P:Lhee;

    .line 739
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-direct {p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v4

    .line 740
    invoke-direct {p0}, Lfos;->ag()Landroid/graphics/Point;

    move-result-object v5

    invoke-static {v5}, Lfss;->a(Landroid/graphics/Point;)[Landroid/graphics/Point;

    move-result-object v5

    .line 738
    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/google/android/apps/plus/stories/phone/StoryElementPickerActivity;

    invoke-direct {v6, v0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account_id"

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "story_id"

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "story_render_sizes"

    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    if-eqz v2, :cond_1

    const-string v0, "story_element_ref"

    new-instance v1, Lhyt;

    invoke-direct {v1, v2}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 741
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v6, v0}, Lfos;->a(Landroid/content/Intent;I)V

    .line 742
    sget-object v0, Lhmv;->fa:Lhmv;

    invoke-direct {p0, v0}, Lfos;->a(Lhmv;)V

    move v0, v3

    .line 789
    :goto_1
    return v0

    .line 737
    :cond_2
    iget-object v0, p0, Lfos;->U:Lfqu;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lfos;->U:Lfqu;

    iget-object v1, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lfqu;->c(I)Ljava/util/List;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmms;

    iget v1, v0, Lmms;->a:I

    const/4 v5, 0x3

    if-ne v1, v5, :cond_5

    iget-object v1, p0, Lfos;->S:Lmmq;

    const-class v5, Lmml;

    invoke-static {v1, v0, v5}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v1

    check-cast v1, Lmml;

    invoke-static {v1}, Lfss;->a(Lmml;)Z

    move-result v1

    if-eqz v1, :cond_13

    :goto_3
    move-object v2, v0

    :cond_4
    :goto_4
    if-eqz v2, :cond_3

    goto :goto_0

    :cond_5
    iget v1, v0, Lmms;->a:I

    const/4 v5, 0x4

    if-ne v1, v5, :cond_4

    iget-object v1, p0, Lfos;->S:Lmmq;

    const-class v5, Lmmc;

    invoke-static {v1, v0, v5}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v1

    check-cast v1, Lmmc;

    invoke-static {v1}, Lfss;->a(Lmmc;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v2, v0

    goto :goto_4

    .line 744
    :cond_6
    const v4, 0x7f100667

    if-ne v0, v4, :cond_7

    .line 745
    invoke-direct {p0}, Lfos;->Z()V

    move v0, v3

    .line 746
    goto :goto_1

    .line 747
    :cond_7
    const v4, 0x7f100668

    if-ne v0, v4, :cond_8

    .line 748
    iput-boolean v1, p0, Lfos;->aj:Z

    .line 749
    invoke-direct {p0}, Lfos;->V()V

    move v0, v3

    .line 750
    goto :goto_1

    .line 751
    :cond_8
    const v4, 0x7f10066b

    if-ne v0, v4, :cond_a

    .line 752
    new-instance v0, Lfpq;

    invoke-direct {v0}, Lfpq;-><init>()V

    .line 753
    invoke-virtual {p0}, Lfos;->q()Lae;

    move-result-object v2

    const-string v3, "ReportStoryAbuseDialog"

    invoke-virtual {v0, v2, v3}, Lfpq;->a(Lae;Ljava/lang/String;)V

    :cond_9
    :goto_5
    move v0, v1

    .line 789
    goto :goto_1

    .line 754
    :cond_a
    const v4, 0x7f100669

    if-ne v0, v4, :cond_b

    .line 755
    new-instance v0, Lfol;

    invoke-direct {v0}, Lfol;-><init>()V

    .line 756
    const/4 v2, 0x6

    invoke-virtual {v0, p0, v2}, Lfol;->a(Lu;I)V

    .line 757
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->f()Lae;

    move-result-object v2

    const-string v3, "DeleteRebuildStoryFragment"

    invoke-virtual {v0, v2, v3}, Lfol;->a(Lae;Ljava/lang/String;)V

    goto :goto_5

    .line 758
    :cond_b
    const v4, 0x7f10066a

    if-ne v0, v4, :cond_c

    .line 759
    new-instance v0, Lfpn;

    invoke-direct {v0}, Lfpn;-><init>()V

    .line 760
    const/4 v2, 0x7

    invoke-virtual {v0, p0, v2}, Lfpn;->a(Lu;I)V

    .line 761
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->f()Lae;

    move-result-object v2

    const-string v3, "RemoveStoryFragment"

    invoke-virtual {v0, v2, v3}, Lfpn;->a(Lae;Ljava/lang/String;)V

    goto :goto_5

    .line 762
    :cond_c
    const v4, 0x7f10066c

    if-ne v0, v4, :cond_d

    .line 763
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 765
    const-string v0, "is_story"

    const-string v2, "true"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    const-string v0, "story_id"

    invoke-direct {p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    const-string v0, "story_owner_obfuscated_id"

    iget-object v2, p0, Lfos;->S:Lmmq;

    iget-object v2, v2, Lmmq;->b:Lodo;

    iget-object v2, v2, Lodo;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    const-string v0, "story_start_timestamp"

    iget-object v2, p0, Lfos;->S:Lmmq;

    iget-object v2, v2, Lmmq;->c:Lmoo;

    iget-object v2, v2, Lmoo;->b:Lmon;

    iget-object v2, v2, Lmon;->a:Lmmh;

    iget-object v2, v2, Lmmh;->a:Ljava/lang/Long;

    .line 769
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 768
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    const-string v0, "story_end_timestamp"

    iget-object v2, p0, Lfos;->S:Lmmq;

    iget-object v2, v2, Lmmq;->c:Lmoo;

    iget-object v2, v2, Lmoo;->b:Lmon;

    iget-object v2, v2, Lmon;->b:Lmmh;

    iget-object v2, v2, Lmmh;->a:Ljava/lang/Long;

    .line 771
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 770
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v2, Likn;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Likn;

    iget-object v2, p0, Lfos;->Q:Lijk;

    invoke-interface {v0, v2}, Likn;->a(Lijk;)Likm;

    move-result-object v2

    .line 774
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v4, Likv;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Likv;

    .line 775
    invoke-direct {p0}, Lfos;->W()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-interface {v0, v4}, Likv;->a(Landroid/graphics/Bitmap;)Likv;

    move-result-object v0

    .line 776
    invoke-interface {v0, v1}, Likv;->a(Landroid/os/Bundle;)Likv;

    move-result-object v0

    .line 777
    invoke-interface {v0}, Likv;->b()Liku;

    move-result-object v0

    .line 778
    invoke-interface {v2, v0}, Likm;->b(Liku;)Lijq;

    move v0, v3

    .line 779
    goto/16 :goto_1

    .line 780
    :cond_d
    const v4, 0x102002c

    if-ne v0, v4, :cond_12

    .line 781
    invoke-virtual {p0}, Lfos;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "from_url_gateway"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->onBackPressed()V

    goto/16 :goto_5

    :cond_e
    iget-object v0, p0, Lfos;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iget-object v3, p0, Lfos;->S:Lmmq;

    if-eqz v3, :cond_f

    invoke-direct {p0}, Lfos;->aa()Z

    move-result v3

    if-eqz v3, :cond_10

    :cond_f
    iget-object v2, p0, Lfos;->at:Llnl;

    invoke-static {v2, v0, v1}, Leyq;->a(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    :goto_6
    invoke-virtual {p0, v0}, Lfos;->a(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    goto/16 :goto_5

    :cond_10
    iget-object v3, p0, Lfos;->S:Lmmq;

    iget-object v3, v3, Lmmq;->g:Lmlz;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lfos;->S:Lmmq;

    iget-object v3, v3, Lmmq;->g:Lmlz;

    iget-object v3, v3, Lmlz;->b:Logr;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lfos;->S:Lmmq;

    iget-object v3, v3, Lmmq;->g:Lmlz;

    iget-object v3, v3, Lmlz;->a:Ljava/lang/String;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lfos;->at:Llnl;

    iget-object v4, p0, Lfos;->S:Lmmq;

    iget-object v4, v4, Lmmq;->g:Lmlz;

    iget-object v4, v4, Lmlz;->a:Ljava/lang/String;

    invoke-static {v3, v0, v4, v1, v2}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Z[B)Landroid/content/Intent;

    move-result-object v0

    goto :goto_6

    :cond_11
    iget-object v2, p0, Lfos;->at:Llnl;

    invoke-static {v2, v0}, Leyq;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_6

    .line 782
    :cond_12
    const v2, 0x7f10066d

    if-ne v0, v2, :cond_9

    .line 783
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    const-string v1, "stories"

    const-string v2, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v0, v1, v2}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 784
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 785
    const/high16 v0, 0x80000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 786
    invoke-virtual {p0, v1}, Lfos;->a(Landroid/content/Intent;)V

    move v0, v3

    .line 787
    goto/16 :goto_1

    :cond_13
    move-object v0, v2

    goto/16 :goto_3

    :cond_14
    move-object v0, v2

    goto/16 :goto_2
.end method

.method public aO_()V
    .locals 9

    .prologue
    const v8, 0x7f100260

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 641
    invoke-super {p0}, Llol;->aO_()V

    .line 642
    iget v0, p0, Lfos;->V:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 643
    const v0, 0x7f0a01be

    invoke-virtual {p0, v0}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfos;->a(Ljava/lang/CharSequence;)V

    .line 648
    :cond_0
    :goto_0
    invoke-direct {p0}, Lfos;->ac()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lfos;->X:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lfos;->Y:Z

    if-nez v0, :cond_1

    .line 649
    invoke-virtual {p0, v7}, Lfos;->a(Z)V

    .line 652
    :cond_1
    iget-boolean v0, p0, Lfos;->al:Z

    if-eqz v0, :cond_2

    .line 653
    invoke-virtual {p0, v7}, Lfos;->b(Z)V

    .line 655
    :cond_2
    return-void

    .line 644
    :cond_3
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-nez v0, :cond_0

    .line 645
    invoke-direct {p0}, Lfos;->X()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfos;->ah:Z

    if-nez v0, :cond_4

    iput-boolean v7, p0, Lfos;->ah:Z

    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    :try_start_0
    invoke-virtual {p0}, Lfos;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0205a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v3, 0x2800

    new-array v3, v3, [B

    :goto_1
    const/4 v4, 0x0

    const/16 v5, 0x2800

    invoke-virtual {v1, v3, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    if-lez v4, :cond_5

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_4
    :goto_2
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f10025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_5
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    new-instance v2, Lirp;

    invoke-direct {v2, v1}, Lirp;-><init>([B)V

    new-instance v3, Liro;

    iget-object v1, p0, Lfos;->at:Llnl;

    const-class v4, Lizs;

    invoke-static {v1, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizs;

    invoke-virtual {v1}, Lizs;->b()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    invoke-direct {v3, v2, v1}, Liro;-><init>(Lirp;Landroid/graphics/Bitmap$Config;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 588
    invoke-super {p0}, Llol;->ae_()V

    .line 589
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b()V

    .line 590
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2108
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1861
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 703
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 924
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 959
    :cond_0
    :goto_0
    return-void

    .line 928
    :cond_1
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    .line 929
    if-eqz v0, :cond_0

    .line 933
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 934
    if-eqz v0, :cond_0

    .line 938
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 939
    if-eqz v0, :cond_0

    .line 943
    if-eqz p1, :cond_2

    .line 944
    const/16 v1, 0x704

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0

    .line 950
    :cond_2
    const/16 v1, 0xf06

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 957
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfos;->X:Z

    goto :goto_0
.end method

.method public b(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1925
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1926
    const v1, 0x7f100673

    if-ne v0, v1, :cond_5

    .line 1927
    new-instance v5, Lmmr;

    invoke-direct {v5}, Lmmr;-><init>()V

    .line 1928
    const/16 v0, 0xe

    iput v0, v5, Lmmr;->b:I

    .line 1929
    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v1, v0, Lmmq;->c:Lmoo;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->d:[Lnzx;

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v0, v0, Lmmq;->c:Lmoo;

    iget-object v0, v0, Lmoo;->d:[Lnzx;

    aget-object v0, v0, v3

    :goto_0
    if-eqz v0, :cond_2

    sget-object v1, Lnzu;->a:Loxr;

    invoke-virtual {v0, v1}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    :goto_1
    if-eqz v0, :cond_3

    iget-object v1, v0, Lnzu;->b:Lnym;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->e:Ljava/lang/String;

    move-object v1, v0

    .line 1930
    :goto_2
    if-eqz v1, :cond_4

    const/4 v0, 0x2

    :goto_3
    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v5, Lmmr;->e:[Ljava/lang/String;

    .line 1931
    iget-object v0, v5, Lmmr;->e:[Ljava/lang/String;

    iget-wide v6, p0, Lfos;->af:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3

    .line 1932
    if-eqz v1, :cond_0

    .line 1933
    iget-object v0, v5, Lmmr;->e:[Ljava/lang/String;

    aput-object v1, v0, v2

    .line 1935
    :cond_0
    new-array v0, v2, [Lmmr;

    aput-object v5, v0, v3

    invoke-direct {p0, v0}, Lfos;->a([Lmmr;)V

    .line 1936
    sget-object v0, Lhmv;->ff:Lhmv;

    invoke-direct {p0, v0}, Lfos;->a(Lhmv;)V

    .line 1973
    :goto_4
    return v2

    :cond_1
    move-object v0, v4

    .line 1929
    goto :goto_0

    :cond_2
    move-object v0, v4

    goto :goto_1

    :cond_3
    move-object v1, v4

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1930
    goto :goto_3

    .line 1938
    :cond_5
    const v1, 0x7f100674

    if-ne v0, v1, :cond_9

    .line 1939
    iget-object v0, p0, Lfos;->S:Lmmq;

    invoke-static {v0}, Lfss;->a(Lmmq;)I

    move-result v0

    if-le v0, v7, :cond_8

    .line 1940
    new-instance v5, Lmmr;

    invoke-direct {v5}, Lmmr;-><init>()V

    .line 1941
    iput v2, v5, Lmmr;->b:I

    .line 1942
    iget-object v0, p0, Lfos;->W:Lmms;

    iput-object v0, v5, Lmmr;->f:Lmms;

    .line 1943
    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v1, p0, Lfos;->W:Lmms;

    const-class v6, Lmml;

    invoke-static {v0, v1, v6}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 1944
    iget-object v1, p0, Lfos;->S:Lmmq;

    iget-object v6, p0, Lfos;->W:Lmms;

    const-class v7, Lmmc;

    .line 1945
    invoke-static {v1, v6, v7}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v1

    check-cast v1, Lmmc;

    .line 1946
    iput-object v4, p0, Lfos;->W:Lmms;

    .line 1947
    new-array v4, v2, [Lmmr;

    aput-object v5, v4, v3

    invoke-direct {p0, v4}, Lfos;->a([Lmmr;)V

    .line 1948
    invoke-direct {p0}, Lfos;->aj()Landroid/os/Bundle;

    move-result-object v3

    .line 1949
    if-eqz v0, :cond_6

    .line 1950
    const-string v4, "story_moment_type"

    iget v0, v0, Lmml;->c:I

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1952
    :cond_6
    if-eqz v1, :cond_7

    .line 1953
    const-string v0, "story_enrichment_type"

    iget v1, v1, Lmmc;->b:I

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1955
    :cond_7
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v4, p0, Lfos;->at:Llnl;

    invoke-direct {v1, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->fb:Lhmv;

    .line 1956
    invoke-virtual {v1, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1957
    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 1955
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_4

    .line 1960
    :cond_8
    invoke-virtual {p0}, Lfos;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11000a

    new-array v5, v2, [Ljava/lang/Object;

    .line 1962
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    .line 1960
    invoke-virtual {v0, v1, v7, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0596

    .line 1963
    invoke-virtual {p0, v1}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 1959
    invoke-static {v4, v0, v1, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 1964
    invoke-virtual {p0}, Lfos;->p()Lae;

    move-result-object v1

    const-string v3, "dialog_alert"

    invoke-virtual {v0, v1, v3}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1967
    :cond_9
    const v1, 0x7f100675

    if-ne v0, v1, :cond_a

    .line 1968
    iget-object v0, p0, Lfos;->ae:Ljava/lang/String;

    iget-wide v4, p0, Lfos;->af:J

    const-string v1, ""

    invoke-direct {p0, v0, v4, v5, v1}, Lfos;->a(Ljava/lang/String;JLjava/lang/String;)V

    goto/16 :goto_4

    .line 1970
    :cond_a
    const v1, 0x7f100676

    if-ne v0, v1, :cond_b

    .line 1971
    iget-object v0, p0, Lfos;->ae:Ljava/lang/String;

    iget-wide v4, p0, Lfos;->af:J

    iget-object v1, p0, Lfos;->ag:Ljava/lang/String;

    invoke-direct {p0, v0, v4, v5, v1}, Lfos;->a(Ljava/lang/String;JLjava/lang/String;)V

    :cond_b
    move v2, v3

    .line 1973
    goto/16 :goto_4
.end method

.method public c(I)V
    .locals 6

    .prologue
    .line 2008
    new-instance v0, Lfnx;

    iget-object v1, p0, Lfos;->at:Llnl;

    iget-object v2, p0, Lfos;->P:Lhee;

    .line 2009
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {p0}, Lfos;->af()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lfos;->S:Lmmq;

    iget-object v3, v3, Lmmq;->a:Lmok;

    iget-object v5, v3, Lmok;->b:Lmol;

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lfnx;-><init>(Landroid/content/Context;IILjava/lang/String;Lmol;)V

    .line 2010
    iget-object v1, p0, Lfos;->R:Lhoc;

    invoke-virtual {v1}, Lhoc;->d()Lhos;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhos;->a(Lhny;)V

    .line 2011
    iget-object v1, p0, Lfos;->R:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 2012
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 477
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 478
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 479
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lfos;->P:Lhee;

    .line 480
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v1, Lhec;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 481
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1865
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1197
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    if-ne p1, v0, :cond_1

    .line 1198
    invoke-virtual {p0}, Lfos;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbb;->b(I)Ldo;

    move-result-object v1

    move-object v0, v1

    .line 1199
    check-cast v0, Lfqd;

    .line 1200
    invoke-direct {p0}, Lfos;->ag()Landroid/graphics/Point;

    move-result-object v3

    invoke-static {v3}, Lfss;->a(Landroid/graphics/Point;)[Landroid/graphics/Point;

    move-result-object v3

    .line 1199
    invoke-virtual {v0, v3}, Lfqd;->a([Landroid/graphics/Point;)V

    .line 1201
    iget v0, p0, Lfos;->V:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 1202
    invoke-virtual {v1}, Ldo;->s()V

    .line 1205
    :cond_0
    invoke-direct {p0, v2}, Lfos;->j(Z)V

    .line 1210
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v1

    .line 1212
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    if-ne v0, v3, :cond_2

    .line 1213
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    if-ne v0, v3, :cond_2

    move v0, v2

    .line 1211
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1217
    :cond_1
    return-void

    .line 1213
    :cond_2
    const/high16 v0, -0x1000000

    goto :goto_0
.end method

.method public c(Z)V
    .locals 4

    .prologue
    .line 1221
    iget-boolean v0, p0, Lfos;->Z:Z

    if-nez v0, :cond_0

    .line 1222
    sget-object v0, Lhmv;->fi:Lhmv;

    invoke-direct {p0, v0}, Lfos;->a(Lhmv;)V

    .line 1223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfos;->Z:Z

    .line 1227
    :cond_0
    if-eqz p1, :cond_1

    .line 1228
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    iget-object v1, p0, Lfos;->am:Ljava/lang/Runnable;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1229
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    iget-object v1, p0, Lfos;->an:Ljava/lang/Runnable;

    const-wide/16 v2, 0xdac

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1237
    :goto_0
    return-void

    .line 1232
    :cond_1
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    iget-object v1, p0, Lfos;->am:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1234
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    iget-object v1, p0, Lfos;->an:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public d()V
    .locals 12

    .prologue
    const-wide/16 v10, 0xfa

    const-wide/16 v8, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 1241
    invoke-virtual {p0, v4}, Lfos;->a(Z)V

    .line 1242
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    iget-object v1, p0, Lfos;->am:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1243
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10026b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f10026c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f10032a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0, v4}, Lfos;->a(Z)V

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    new-instance v5, Lfoz;

    invoke-direct {v5, v2}, Lfoz;-><init>(Landroid/view/View;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    sub-float v3, v4, v3

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Lfpa;

    invoke-direct {v3, p0, v0, v1}, Lfpa;-><init>(Lfos;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1244
    invoke-virtual {p0}, Lfos;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100272

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x15e

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lfpb;

    invoke-direct {v2, v0}, Lfpb;-><init>(Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1245
    :cond_0
    return-void
.end method

.method public d(I)V
    .locals 4

    .prologue
    .line 525
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 526
    invoke-direct {p0}, Lfos;->aj()Landroid/os/Bundle;

    move-result-object v1

    .line 527
    const-string v0, "device_info"

    new-instance v2, Lhyt;

    .line 528
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v3

    invoke-static {v3}, Lhmt;->b(Landroid/content/Context;)Lmwg;

    move-result-object v3

    invoke-direct {v2, v3}, Lhyt;-><init>(Loxu;)V

    .line 527
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 529
    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lfos;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->fg:Lhmv;

    .line 530
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 531
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 529
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 533
    :cond_0
    return-void
.end method

.method public d(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1877
    invoke-virtual {p0, p1}, Lfos;->b_(Landroid/view/View;)V

    .line 1878
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1977
    iget-boolean v0, p0, Lfos;->X:Z

    if-nez v0, :cond_0

    .line 1978
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfos;->a(Z)V

    .line 1980
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 667
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 668
    const-string v0, "story_load"

    iget v1, p0, Lfos;->V:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 669
    const-string v0, "story_end_card"

    iget-boolean v1, p0, Lfos;->Y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 670
    const-string v0, "has_seen_end_card"

    iget-boolean v1, p0, Lfos;->Z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 671
    const-string v0, "story_show_promo"

    iget-boolean v1, p0, Lfos;->ab:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 672
    const-string v0, "has_refreshed"

    iget-boolean v1, p0, Lfos;->ai:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 673
    const-string v0, "passive_refresh"

    iget-boolean v1, p0, Lfos;->aj:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 674
    const-string v0, "showing_location_picker"

    iget-boolean v1, p0, Lfos;->al:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 675
    iget-object v0, p0, Lfos;->W:Lmms;

    if-eqz v0, :cond_0

    .line 676
    const-string v0, "story_editable_element"

    new-instance v1, Lhyt;

    iget-object v2, p0, Lfos;->W:Lmms;

    invoke-direct {v1, v2}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 679
    :cond_0
    iget-object v0, p0, Lfos;->ac:[Lmmr;

    if-eqz v0, :cond_2

    .line 680
    iget-object v0, p0, Lfos;->ac:[Lmmr;

    array-length v0, v0

    new-array v1, v0, [Landroid/os/Parcelable;

    .line 681
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lfos;->ac:[Lmmr;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 682
    new-instance v2, Lhyt;

    iget-object v3, p0, Lfos;->ac:[Lmmr;

    aget-object v3, v3, v0

    invoke-direct {v2, v3}, Lhyt;-><init>(Loxu;)V

    aput-object v2, v1, v0

    .line 681
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 684
    :cond_1
    const-string v0, "story_edits"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 685
    const-string v0, "story_pending_edits"

    iget-boolean v1, p0, Lfos;->ad:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 687
    :cond_2
    return-void
.end method

.method public e(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1882
    invoke-virtual {p0, p1}, Lfos;->b(Landroid/view/View;)V

    .line 1883
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 635
    invoke-super {p0}, Llol;->g()V

    .line 636
    iget-object v0, p0, Lfos;->Q:Lijk;

    invoke-interface {v0}, Lijk;->b()V

    .line 637
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 691
    invoke-super {p0}, Llol;->h()V

    .line 692
    iget-object v0, p0, Lfos;->Q:Lijk;

    invoke-interface {v0}, Lijk;->c()V

    .line 693
    return-void
.end method

.method public i(Z)V
    .locals 1

    .prologue
    .line 1249
    iget-boolean v0, p0, Lfos;->Y:Z

    if-eqz v0, :cond_0

    .line 1254
    :goto_0
    return-void

    .line 1252
    :cond_0
    invoke-direct {p0}, Lfos;->ac()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lfos;->X:Z

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 1253
    :goto_1
    invoke-virtual {p0, v0}, Lfos;->a(Z)V

    goto :goto_0

    .line 1252
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const v3, 0x7f100034

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 1053
    const v0, 0x7f100039

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    const v0, 0x7f100039

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    move v9, v8

    :cond_1
    :goto_0
    if-eqz v9, :cond_2

    .line 1054
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->d()Lftb;

    move-result-object v0

    invoke-virtual {v0}, Lftb;->a()V

    .line 1056
    :cond_2
    return-void

    .line 1053
    :pswitch_0
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v1

    const v0, 0x7f10003a

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v1, v0}, Lisy;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.WEB_SEARCH"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "query"

    const v0, 0x7f10003b

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0, v1}, Lz;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lfos;->T:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    const-string v1, "StoryEditLocationFragment"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lfos;->X:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0, v9}, Lfos;->b(Z)V

    :cond_3
    iput-boolean v9, p0, Lfos;->al:Z

    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfos;->ak:Landroid/widget/TextView;

    iget-object v0, p0, Lfos;->ak:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setSelected(Z)V

    const v0, 0x7f100035

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfof;

    const v1, 0x7f100038

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfof;

    const v2, 0x7f10003d

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmms;

    iput-object v2, p0, Lfos;->W:Lmms;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "stories_edit_place"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    const-string v0, "stories_current_place"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v0, Lfpr;

    invoke-direct {v0}, Lfpr;-><init>()V

    invoke-virtual {v0, v2}, Lfpr;->f(Landroid/os/Bundle;)V

    invoke-virtual {v0, p0, v9}, Lfpr;->a(Lu;I)V

    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->f()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const v2, 0x7f10032b

    const-string v3, "StoryEditLocationFragment"

    invoke-virtual {v1, v2, v0, v3}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    invoke-virtual {v1, v6}, Lat;->a(Ljava/lang/String;)Lat;

    invoke-virtual {v1}, Lat;->b()I

    goto/16 :goto_0

    :pswitch_4
    const v0, 0x7f100032

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lfos;->at:Llnl;

    iget-object v2, p0, Lfos;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-static {v1, v2, v0, v6}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfos;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10002c

    if-ne v0, v1, :cond_5

    invoke-direct {p0}, Lfos;->ab()Z

    move-result v0

    if-eqz v0, :cond_5

    check-cast p1, Landroid/widget/TextView;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "text"

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "placeholder"

    const v2, 0x7f0a01b4

    invoke-virtual {p0, v2}, Lfos;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "max_length"

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "allow_empty"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Lfpx;

    invoke-direct {v1}, Lfpx;-><init>()V

    invoke-virtual {v1, v0}, Lfpx;->f(Landroid/os/Bundle;)V

    invoke-virtual {v1, p0, v7}, Lfpx;->a(Lu;I)V

    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    const-string v2, "StoryEditTextFragment"

    invoke-virtual {v1, v0, v2}, Lfpx;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f100030

    if-eq v0, v1, :cond_6

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10002f

    if-ne v0, v1, :cond_8

    invoke-direct {p0}, Lfos;->ab()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    const v0, 0x7f100079

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const v1, 0x7f10006b

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v4, 0x7f100030

    if-ne v1, v4, :cond_7

    const-string v1, ""

    :goto_1
    invoke-direct {p0, v0, v2, v3, v1}, Lfos;->a(Ljava/lang/String;JLjava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10002f

    if-eq v0, v1, :cond_9

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10002c

    if-ne v0, v1, :cond_a

    :cond_9
    new-instance v0, Lfqk;

    invoke-direct {v0}, Lfqk;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "text"

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lfqk;->f(Landroid/os/Bundle;)V

    const/16 v1, 0x8

    invoke-virtual {v0, p0, v1}, Lfqk;->a(Lu;I)V

    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->f()Lae;

    move-result-object v1

    const-string v2, "TextDisplayFragment"

    invoke-virtual {v0, v1, v2}, Lfqk;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10002e

    if-ne v0, v1, :cond_d

    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnym;

    if-eqz v0, :cond_b

    iget-object v1, v0, Lnym;->e:Ljava/lang/String;

    if-eqz v1, :cond_b

    iget-object v1, v0, Lnym;->h:Lnyz;

    if-eqz v1, :cond_b

    iget-object v1, v0, Lnym;->l:Lnyb;

    if-nez v1, :cond_c

    :cond_b
    move v9, v8

    goto/16 :goto_0

    :cond_c
    new-instance v1, Ldew;

    iget-object v2, p0, Lfos;->at:Llnl;

    iget-object v3, p0, Lfos;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v1, v2, v3}, Ldew;-><init>(Landroid/content/Context;I)V

    new-array v2, v9, [Ljava/lang/String;

    iget-object v3, v0, Lnym;->h:Lnyz;

    iget-object v3, v3, Lnyz;->c:Ljava/lang/String;

    iget-object v4, v0, Lnym;->l:Lnyb;

    iget-object v4, v4, Lnyb;->d:Ljava/lang/String;

    const-string v5, "ALBUM"

    invoke-static {v6, v3, v4, v5}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v7, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v1

    iget-object v0, v0, Lnym;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ldew;->b(Ljava/lang/String;)Ldew;

    move-result-object v0

    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lfos;->a(Landroid/content/Intent;I)V

    invoke-direct {p0}, Lfos;->aj()Landroid/os/Bundle;

    move-result-object v1

    const-string v0, "story_moment_type"

    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lfos;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lfos;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->eX:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f100368

    if-ne v0, v1, :cond_e

    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnym;

    new-instance v1, Ldew;

    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Lfos;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v1, v2, v3}, Ldew;-><init>(Landroid/content/Context;I)V

    new-array v2, v9, [Ljava/lang/String;

    iget-object v3, v0, Lnym;->h:Lnyz;

    iget-object v3, v3, Lnyz;->c:Ljava/lang/String;

    iget-object v4, v0, Lnym;->l:Lnyb;

    iget-object v4, v4, Lnyb;->d:Ljava/lang/String;

    const-string v5, "ALBUM"

    invoke-static {v6, v3, v4, v5}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v7, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v1

    iget-object v0, v0, Lnym;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ldew;->b(Ljava/lang/String;)Ldew;

    move-result-object v0

    invoke-virtual {v0, v9}, Ldew;->j(Z)Ldew;

    move-result-object v0

    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lfos;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f100367

    if-ne v0, v1, :cond_10

    const v0, 0x7f100044

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iget-object v2, p0, Lfos;->U:Lfqu;

    const v1, 0x7f100079

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1, p1}, Lfqu;->a(Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lnym;

    iget-object v1, p0, Lfos;->at:Llnl;

    iget-object v2, p0, Lfos;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, v4, Lnym;->h:Lnyz;

    iget-object v3, v3, Lnyz;->c:Ljava/lang/String;

    iget-object v4, v4, Lnym;->e:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const v6, 0x7f100079

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v7, p0, Lfos;->S:Lmmq;

    invoke-static {v7}, Lfoa;->a(Lmmq;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_f

    move v8, v9

    :cond_f
    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10055e

    if-eq v0, v1, :cond_11

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f100552

    if-ne v0, v1, :cond_12

    :cond_11
    invoke-direct {p0}, Lfos;->Z()V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1004e0

    if-ne v0, v1, :cond_14

    iget-object v0, p0, Lfos;->S:Lmmq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->g:Lmlz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfos;->S:Lmmq;

    iget-object v0, v0, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lfos;->aa:Z

    if-nez v0, :cond_13

    iget-object v0, p0, Lfos;->at:Llnl;

    iget-object v1, p0, Lfos;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lfos;->S:Lmmq;

    iget-object v2, v2, Lmmq;->g:Lmlz;

    iget-object v2, v2, Lmlz;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2, v8, v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Z[B)I

    goto/16 :goto_0

    :cond_13
    iget-object v0, p0, Lfos;->at:Llnl;

    iget-object v1, p0, Lfos;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lfos;->S:Lmmq;

    iget-object v2, v2, Lmmq;->g:Lmlz;

    iget-object v2, v2, Lmlz;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->d(Landroid/content/Context;ILjava/lang/String;)I

    goto/16 :goto_0

    :cond_14
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f100226

    if-eq v0, v1, :cond_15

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10026d

    if-ne v0, v1, :cond_16

    :cond_15
    iget-object v0, p0, Lfos;->S:Lmmq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfos;->at:Llnl;

    iget-object v1, p0, Lfos;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lfos;->S:Lmmq;

    iget-object v2, v2, Lmmq;->g:Lmlz;

    iget-object v2, v2, Lmlz;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfos;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_16
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f100273

    if-ne v0, v1, :cond_0

    sget-object v0, Lhmv;->fh:Lhmv;

    invoke-direct {p0, v0}, Lfos;->a(Lhmv;)V

    iget-object v0, p0, Lfos;->at:Llnl;

    iget-object v1, p0, Lfos;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1}, Lfqt;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v1

    invoke-virtual {v1, v0}, Lz;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 6

    .prologue
    const v5, 0x7f100676

    const v4, 0x7f100675

    const v3, 0x7f100079

    const v2, 0x7f10006b

    .line 1887
    invoke-super {p0, p1, p2, p3}, Llol;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1890
    invoke-interface {p1}, Landroid/view/ContextMenu;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1921
    :cond_0
    :goto_0
    return-void

    .line 1893
    :cond_1
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 1894
    const v0, 0x7f10003d

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmms;

    iput-object v0, p0, Lfos;->W:Lmms;

    .line 1895
    const v0, 0x7f120004

    invoke-virtual {v1, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1897
    const v0, 0x7f100049

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1898
    const v0, 0x7f100673

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 1904
    :goto_1
    const v0, 0x7f100046

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1905
    const v0, 0x7f100047

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1906
    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 1907
    const v0, 0x7f100048

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfos;->ag:Ljava/lang/String;

    .line 1911
    :goto_2
    invoke-virtual {p2, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfos;->ae:Ljava/lang/String;

    .line 1912
    invoke-virtual {p2, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lfos;->af:J

    .line 1918
    :goto_3
    const v0, 0x7f100045

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1919
    const v0, 0x7f100674

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->removeItem(I)V

    goto/16 :goto_0

    .line 1900
    :cond_2
    invoke-virtual {p2, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfos;->ae:Ljava/lang/String;

    .line 1901
    invoke-virtual {p2, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v0

    iput-wide v0, p0, Lfos;->af:J

    goto :goto_1

    .line 1909
    :cond_3
    invoke-interface {p1, v5}, Landroid/view/ContextMenu;->removeItem(I)V

    goto :goto_2

    .line 1914
    :cond_4
    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 1915
    invoke-interface {p1, v5}, Landroid/view/ContextMenu;->removeItem(I)V

    goto :goto_3
.end method

.method public onSystemUiVisibilityChange(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1258
    iget-object v0, p0, Lfos;->U:Lfqu;

    invoke-virtual {v0, p1}, Lfqu;->onSystemUiVisibilityChange(I)V

    .line 1259
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 1260
    and-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_0

    .line 1261
    invoke-virtual {p0, v2}, Lfos;->a(Z)V

    .line 1266
    :cond_0
    :goto_0
    return-void

    .line 1263
    :cond_1
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    .line 1264
    invoke-virtual {p0, v2}, Lfos;->a(Z)V

    goto :goto_0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 659
    invoke-super {p0}, Llol;->z()V

    .line 660
    invoke-virtual {p0}, Lfos;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    sget-object v0, Lhmv;->eW:Lhmv;

    invoke-direct {p0, v0}, Lfos;->a(Lhmv;)V

    .line 663
    :cond_0
    return-void
.end method
