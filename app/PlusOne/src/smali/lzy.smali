.class public final Llzy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnmj;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4129
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4132
    iput-object v0, p0, Llzy;->apiHeader:Llyq;

    .line 4135
    iput-object v0, p0, Llzy;->a:Lnmj;

    .line 4129
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4152
    const/4 v0, 0x0

    .line 4153
    iget-object v1, p0, Llzy;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 4154
    const/4 v0, 0x1

    iget-object v1, p0, Llzy;->apiHeader:Llyq;

    .line 4155
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4157
    :cond_0
    iget-object v1, p0, Llzy;->a:Lnmj;

    if-eqz v1, :cond_1

    .line 4158
    const/4 v1, 0x2

    iget-object v2, p0, Llzy;->a:Lnmj;

    .line 4159
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4161
    :cond_1
    iget-object v1, p0, Llzy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4162
    iput v0, p0, Llzy;->ai:I

    .line 4163
    return v0
.end method

.method public a(Loxn;)Llzy;
    .locals 2

    .prologue
    .line 4171
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4172
    sparse-switch v0, :sswitch_data_0

    .line 4176
    iget-object v1, p0, Llzy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4177
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzy;->ah:Ljava/util/List;

    .line 4180
    :cond_1
    iget-object v1, p0, Llzy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4182
    :sswitch_0
    return-object p0

    .line 4187
    :sswitch_1
    iget-object v0, p0, Llzy;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 4188
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzy;->apiHeader:Llyq;

    .line 4190
    :cond_2
    iget-object v0, p0, Llzy;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4194
    :sswitch_2
    iget-object v0, p0, Llzy;->a:Lnmj;

    if-nez v0, :cond_3

    .line 4195
    new-instance v0, Lnmj;

    invoke-direct {v0}, Lnmj;-><init>()V

    iput-object v0, p0, Llzy;->a:Lnmj;

    .line 4197
    :cond_3
    iget-object v0, p0, Llzy;->a:Lnmj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4172
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4140
    iget-object v0, p0, Llzy;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 4141
    const/4 v0, 0x1

    iget-object v1, p0, Llzy;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4143
    :cond_0
    iget-object v0, p0, Llzy;->a:Lnmj;

    if-eqz v0, :cond_1

    .line 4144
    const/4 v0, 0x2

    iget-object v1, p0, Llzy;->a:Lnmj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4146
    :cond_1
    iget-object v0, p0, Llzy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4148
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4125
    invoke-virtual {p0, p1}, Llzy;->a(Loxn;)Llzy;

    move-result-object v0

    return-object v0
.end method
