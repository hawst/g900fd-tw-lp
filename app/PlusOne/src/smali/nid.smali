.class public final Lnid;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnja;

.field private b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4632
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4635
    const/4 v0, 0x0

    iput-object v0, p0, Lnid;->a:Lnja;

    .line 4632
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4654
    const/4 v0, 0x0

    .line 4655
    iget-object v1, p0, Lnid;->a:Lnja;

    if-eqz v1, :cond_0

    .line 4656
    const/4 v0, 0x1

    iget-object v1, p0, Lnid;->a:Lnja;

    .line 4657
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4659
    :cond_0
    iget-object v1, p0, Lnid;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 4660
    const/4 v1, 0x2

    iget-object v2, p0, Lnid;->b:Ljava/lang/Boolean;

    .line 4661
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4663
    :cond_1
    iget-object v1, p0, Lnid;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4664
    iput v0, p0, Lnid;->ai:I

    .line 4665
    return v0
.end method

.method public a(Loxn;)Lnid;
    .locals 2

    .prologue
    .line 4673
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4674
    sparse-switch v0, :sswitch_data_0

    .line 4678
    iget-object v1, p0, Lnid;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4679
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnid;->ah:Ljava/util/List;

    .line 4682
    :cond_1
    iget-object v1, p0, Lnid;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4684
    :sswitch_0
    return-object p0

    .line 4689
    :sswitch_1
    iget-object v0, p0, Lnid;->a:Lnja;

    if-nez v0, :cond_2

    .line 4690
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnid;->a:Lnja;

    .line 4692
    :cond_2
    iget-object v0, p0, Lnid;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4696
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnid;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 4674
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4642
    iget-object v0, p0, Lnid;->a:Lnja;

    if-eqz v0, :cond_0

    .line 4643
    const/4 v0, 0x1

    iget-object v1, p0, Lnid;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4645
    :cond_0
    iget-object v0, p0, Lnid;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 4646
    const/4 v0, 0x2

    iget-object v1, p0, Lnid;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 4648
    :cond_1
    iget-object v0, p0, Lnid;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4650
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4628
    invoke-virtual {p0, p1}, Lnid;->a(Loxn;)Lnid;

    move-result-object v0

    return-object v0
.end method
