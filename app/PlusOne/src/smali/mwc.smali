.class public final Lmwc;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lmwa;

.field private b:[Lmwb;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11969
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11972
    sget-object v0, Lmwa;->a:[Lmwa;

    iput-object v0, p0, Lmwc;->a:[Lmwa;

    .line 11975
    sget-object v0, Lmwb;->a:[Lmwb;

    iput-object v0, p0, Lmwc;->b:[Lmwb;

    .line 11969
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 12005
    .line 12006
    iget-object v0, p0, Lmwc;->a:[Lmwa;

    if-eqz v0, :cond_1

    .line 12007
    iget-object v3, p0, Lmwc;->a:[Lmwa;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 12008
    if-eqz v5, :cond_0

    .line 12009
    const/4 v6, 0x1

    .line 12010
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 12007
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 12014
    :cond_2
    iget-object v2, p0, Lmwc;->b:[Lmwb;

    if-eqz v2, :cond_4

    .line 12015
    iget-object v2, p0, Lmwc;->b:[Lmwb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 12016
    if-eqz v4, :cond_3

    .line 12017
    const/4 v5, 0x2

    .line 12018
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 12015
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 12022
    :cond_4
    iget-object v1, p0, Lmwc;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 12023
    const/4 v1, 0x3

    iget-object v2, p0, Lmwc;->c:Ljava/lang/Integer;

    .line 12024
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12026
    :cond_5
    iget-object v1, p0, Lmwc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12027
    iput v0, p0, Lmwc;->ai:I

    .line 12028
    return v0
.end method

.method public a(Loxn;)Lmwc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12036
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12037
    sparse-switch v0, :sswitch_data_0

    .line 12041
    iget-object v2, p0, Lmwc;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 12042
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmwc;->ah:Ljava/util/List;

    .line 12045
    :cond_1
    iget-object v2, p0, Lmwc;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12047
    :sswitch_0
    return-object p0

    .line 12052
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 12053
    iget-object v0, p0, Lmwc;->a:[Lmwa;

    if-nez v0, :cond_3

    move v0, v1

    .line 12054
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmwa;

    .line 12055
    iget-object v3, p0, Lmwc;->a:[Lmwa;

    if-eqz v3, :cond_2

    .line 12056
    iget-object v3, p0, Lmwc;->a:[Lmwa;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12058
    :cond_2
    iput-object v2, p0, Lmwc;->a:[Lmwa;

    .line 12059
    :goto_2
    iget-object v2, p0, Lmwc;->a:[Lmwa;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 12060
    iget-object v2, p0, Lmwc;->a:[Lmwa;

    new-instance v3, Lmwa;

    invoke-direct {v3}, Lmwa;-><init>()V

    aput-object v3, v2, v0

    .line 12061
    iget-object v2, p0, Lmwc;->a:[Lmwa;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 12062
    invoke-virtual {p1}, Loxn;->a()I

    .line 12059
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 12053
    :cond_3
    iget-object v0, p0, Lmwc;->a:[Lmwa;

    array-length v0, v0

    goto :goto_1

    .line 12065
    :cond_4
    iget-object v2, p0, Lmwc;->a:[Lmwa;

    new-instance v3, Lmwa;

    invoke-direct {v3}, Lmwa;-><init>()V

    aput-object v3, v2, v0

    .line 12066
    iget-object v2, p0, Lmwc;->a:[Lmwa;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12070
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 12071
    iget-object v0, p0, Lmwc;->b:[Lmwb;

    if-nez v0, :cond_6

    move v0, v1

    .line 12072
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmwb;

    .line 12073
    iget-object v3, p0, Lmwc;->b:[Lmwb;

    if-eqz v3, :cond_5

    .line 12074
    iget-object v3, p0, Lmwc;->b:[Lmwb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 12076
    :cond_5
    iput-object v2, p0, Lmwc;->b:[Lmwb;

    .line 12077
    :goto_4
    iget-object v2, p0, Lmwc;->b:[Lmwb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 12078
    iget-object v2, p0, Lmwc;->b:[Lmwb;

    new-instance v3, Lmwb;

    invoke-direct {v3}, Lmwb;-><init>()V

    aput-object v3, v2, v0

    .line 12079
    iget-object v2, p0, Lmwc;->b:[Lmwb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 12080
    invoke-virtual {p1}, Loxn;->a()I

    .line 12077
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 12071
    :cond_6
    iget-object v0, p0, Lmwc;->b:[Lmwb;

    array-length v0, v0

    goto :goto_3

    .line 12083
    :cond_7
    iget-object v2, p0, Lmwc;->b:[Lmwb;

    new-instance v3, Lmwb;

    invoke-direct {v3}, Lmwb;-><init>()V

    aput-object v3, v2, v0

    .line 12084
    iget-object v2, p0, Lmwc;->b:[Lmwb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 12088
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwc;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 12037
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 11982
    iget-object v1, p0, Lmwc;->a:[Lmwa;

    if-eqz v1, :cond_1

    .line 11983
    iget-object v2, p0, Lmwc;->a:[Lmwa;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 11984
    if-eqz v4, :cond_0

    .line 11985
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 11983
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 11989
    :cond_1
    iget-object v1, p0, Lmwc;->b:[Lmwb;

    if-eqz v1, :cond_3

    .line 11990
    iget-object v1, p0, Lmwc;->b:[Lmwb;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 11991
    if-eqz v3, :cond_2

    .line 11992
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 11990
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 11996
    :cond_3
    iget-object v0, p0, Lmwc;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 11997
    const/4 v0, 0x3

    iget-object v1, p0, Lmwc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11999
    :cond_4
    iget-object v0, p0, Lmwc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12001
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11965
    invoke-virtual {p0, p1}, Lmwc;->a(Loxn;)Lmwc;

    move-result-object v0

    return-object v0
.end method
