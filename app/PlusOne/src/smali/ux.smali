.class public final Lux;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ltp;


# instance fields
.field a:Landroid/support/v7/widget/Toolbar;

.field b:Ljava/lang/CharSequence;

.field c:Lqm;

.field d:Z

.field private e:I

.field private f:Landroid/view/View;

.field private g:Lud;

.field private h:Landroid/view/View;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Z

.field private m:Ljava/lang/CharSequence;

.field private n:Ljava/lang/CharSequence;

.field private o:Lxq;

.field private p:I

.field private final q:Lur;

.field private r:I

.field private s:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Z)V
    .locals 2

    .prologue
    .line 88
    const v0, 0x7f0a05a6

    const v1, 0x7f020010

    invoke-direct {p0, p1, p2, v0, v1}, Lux;-><init>(Landroid/support/v7/widget/Toolbar;ZII)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/support/v7/widget/Toolbar;ZII)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput v2, p0, Lux;->p:I

    .line 84
    iput v2, p0, Lux;->r:I

    .line 94
    iput-object p1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    .line 95
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->i()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lux;->b:Ljava/lang/CharSequence;

    .line 96
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->j()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lux;->m:Ljava/lang/CharSequence;

    .line 97
    iget-object v0, p0, Lux;->b:Ljava/lang/CharSequence;

    if-eqz v0, :cond_c

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lux;->l:Z

    .line 99
    if-eqz p2, :cond_d

    .line 100
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v3, 0x0

    sget-object v4, Lqk;->a:[I

    const v5, 0x7f0100af

    invoke-static {v0, v3, v4, v5, v2}, Luw;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Luw;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v2}, Luw;->b(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 104
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 105
    invoke-virtual {p0, v3}, Lux;->b(Ljava/lang/CharSequence;)V

    .line 108
    :cond_0
    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Luw;->b(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 109
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 110
    invoke-virtual {p0, v3}, Lux;->c(Ljava/lang/CharSequence;)V

    .line 113
    :cond_1
    const/16 v3, 0x9

    invoke-virtual {v0, v3}, Luw;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 114
    if-eqz v3, :cond_2

    .line 115
    invoke-virtual {p0, v3}, Lux;->a(Landroid/graphics/drawable/Drawable;)V

    .line 118
    :cond_2
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Luw;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 119
    if-eqz v3, :cond_3

    .line 120
    invoke-virtual {p0, v3}, Lux;->d(Landroid/graphics/drawable/Drawable;)V

    .line 123
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Luw;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 124
    if-eqz v3, :cond_4

    .line 125
    invoke-virtual {p0, v3}, Lux;->b(Landroid/graphics/drawable/Drawable;)V

    .line 128
    :cond_4
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v2}, Luw;->a(II)I

    move-result v3

    invoke-virtual {p0, v3}, Lux;->a(I)V

    .line 130
    const/16 v3, 0xe

    invoke-virtual {v0, v3, v2}, Luw;->f(II)I

    move-result v3

    .line 132
    if-eqz v3, :cond_5

    .line 133
    iget-object v4, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    iget-object v5, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4, v3, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lux;->a(Landroid/view/View;)V

    .line 135
    iget v3, p0, Lux;->e:I

    or-int/lit8 v3, v3, 0x10

    invoke-virtual {p0, v3}, Lux;->a(I)V

    .line 138
    :cond_5
    invoke-virtual {v0, v1, v2}, Luw;->e(II)I

    move-result v1

    .line 139
    if-lez v1, :cond_6

    .line 140
    iget-object v3, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 141
    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 142
    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/Toolbar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 145
    :cond_6
    const/16 v1, 0x15

    invoke-virtual {v0, v1, v6}, Luw;->c(II)I

    move-result v1

    .line 147
    const/16 v3, 0x16

    invoke-virtual {v0, v3, v6}, Luw;->c(II)I

    move-result v3

    .line 149
    if-gez v1, :cond_7

    if-ltz v3, :cond_8

    .line 150
    :cond_7
    iget-object v4, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v4, v1, v3}, Landroid/support/v7/widget/Toolbar;->a(II)V

    .line 154
    :cond_8
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Luw;->f(II)I

    move-result v1

    .line 155
    if-eqz v1, :cond_9

    .line 156
    iget-object v3, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/content/Context;I)V

    .line 159
    :cond_9
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Luw;->f(II)I

    move-result v1

    .line 161
    if-eqz v1, :cond_a

    .line 162
    iget-object v3, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Landroid/support/v7/widget/Toolbar;->b(Landroid/content/Context;I)V

    .line 165
    :cond_a
    const/16 v1, 0x1a

    invoke-virtual {v0, v1, v2}, Luw;->f(II)I

    move-result v1

    .line 166
    if-eqz v1, :cond_b

    .line 167
    iget-object v2, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/Toolbar;->a(I)V

    .line 170
    :cond_b
    invoke-virtual {v0}, Luw;->b()V

    .line 172
    invoke-virtual {v0}, Luw;->c()Lur;

    move-result-object v0

    iput-object v0, p0, Lux;->q:Lur;

    .line 179
    :goto_1
    invoke-virtual {p0, p3}, Lux;->g(I)V

    .line 180
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->k()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lux;->n:Ljava/lang/CharSequence;

    .line 182
    iget-object v0, p0, Lux;->q:Lur;

    invoke-virtual {v0, p4}, Lur;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lux;->c(Landroid/graphics/drawable/Drawable;)V

    .line 184
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Luy;

    invoke-direct {v1, p0}, Luy;-><init>(Lux;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View$OnClickListener;)V

    .line 194
    return-void

    :cond_c
    move v0, v2

    .line 97
    goto/16 :goto_0

    .line 174
    :cond_d
    const/16 v0, 0xb

    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->l()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_e

    const/16 v0, 0xf

    :cond_e
    iput v0, p0, Lux;->e:I

    .line 176
    new-instance v0, Lur;

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lur;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lux;->q:Lur;

    goto :goto_1
.end method

.method private e(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 283
    iput-object p1, p0, Lux;->b:Ljava/lang/CharSequence;

    .line 284
    iget v0, p0, Lux;->e:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->a(Ljava/lang/CharSequence;)V

    .line 287
    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 367
    iget v1, p0, Lux;->e:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 368
    iget v0, p0, Lux;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 369
    iget-object v0, p0, Lux;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lux;->j:Landroid/graphics/drawable/Drawable;

    .line 374
    :cond_0
    :goto_0
    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 375
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Lux;->i:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 371
    :cond_2
    iget-object v0, p0, Lux;->i:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 654
    iget v0, p0, Lux;->e:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lux;->n:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 656
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget v1, p0, Lux;->r:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->b(I)V

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 658
    :cond_1
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->n:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->c(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 664
    iget v0, p0, Lux;->e:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 665
    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Lux;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lux;->k:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->b(Landroid/graphics/drawable/Drawable;)V

    .line 667
    :cond_0
    return-void

    .line 665
    :cond_1
    iget-object v0, p0, Lux;->s:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 429
    iget v0, p0, Lux;->e:I

    .line 430
    xor-int/2addr v0, p1

    .line 431
    iput p1, p0, Lux;->e:I

    .line 432
    if-eqz v0, :cond_3

    .line 433
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    .line 434
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_4

    .line 435
    invoke-direct {p0}, Lux;->s()V

    .line 436
    invoke-direct {p0}, Lux;->r()V

    .line 442
    :cond_0
    :goto_0
    and-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_1

    .line 443
    invoke-direct {p0}, Lux;->q()V

    .line 446
    :cond_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    .line 447
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_5

    .line 448
    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Lux;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Ljava/lang/CharSequence;)V

    .line 449
    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Lux;->m:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Ljava/lang/CharSequence;)V

    .line 456
    :cond_2
    :goto_1
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    iget-object v0, p0, Lux;->h:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 457
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_6

    .line 458
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 464
    :cond_3
    :goto_2
    return-void

    .line 438
    :cond_4
    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 451
    :cond_5
    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Ljava/lang/CharSequence;)V

    .line 452
    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 460
    :cond_6
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lux;->j:Landroid/graphics/drawable/Drawable;

    .line 362
    invoke-direct {p0}, Lux;->q()V

    .line 363
    return-void
.end method

.method public a(Landroid/view/Menu;Lsb;)V
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Lux;->o:Lxq;

    if-nez v0, :cond_0

    .line 410
    new-instance v0, Lxq;

    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lxq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lux;->o:Lxq;

    .line 411
    iget-object v0, p0, Lux;->o:Lxq;

    .line 413
    :cond_0
    iget-object v0, p0, Lux;->o:Lxq;

    invoke-virtual {v0, p2}, Lxq;->a(Lsb;)V

    .line 414
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    check-cast p1, Lrl;

    iget-object v1, p0, Lux;->o:Lxq;

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/Toolbar;->a(Lrl;Lxq;)V

    .line 415
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 586
    iget-object v0, p0, Lux;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lux;->e:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 589
    :cond_0
    iput-object p1, p0, Lux;->h:Landroid/view/View;

    .line 590
    if-eqz p1, :cond_1

    iget v0, p0, Lux;->e:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    .line 591
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 593
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lux;->l:Z

    if-nez v0, :cond_0

    .line 267
    invoke-direct {p0, p1}, Lux;->e(Ljava/lang/CharSequence;)V

    .line 269
    :cond_0
    return-void
.end method

.method public a(Lqm;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lux;->c:Lqm;

    .line 261
    return-void
.end method

.method public a(Ltx;)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 468
    iget-object v0, p0, Lux;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lux;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_0

    .line 469
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 471
    :cond_0
    iput-object p1, p0, Lux;->f:Landroid/view/View;

    .line 472
    if-eqz p1, :cond_1

    iget v0, p0, Lux;->p:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 473
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->f:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    .line 474
    iget-object v0, p0, Lux;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lzm;

    .line 475
    iput v3, v0, Lzm;->width:I

    .line 476
    iput v3, v0, Lzm;->height:I

    .line 477
    const v1, 0x800053

    iput v1, v0, Lzm;->a:I

    .line 478
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ltx;->a(Z)V

    .line 480
    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->a(Z)V

    .line 495
    return-void
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 509
    iget v0, p0, Lux;->p:I

    .line 510
    if-eq p1, v0, :cond_2

    .line 511
    packed-switch v0, :pswitch_data_0

    .line 524
    :cond_0
    :goto_0
    iput p1, p0, Lux;->p:I

    .line 526
    packed-switch p1, :pswitch_data_1

    .line 543
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid navigation mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 513
    :pswitch_0
    iget-object v0, p0, Lux;->g:Lud;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lux;->g:Lud;

    invoke-virtual {v0}, Lud;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_0

    .line 514
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->g:Lud;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 518
    :pswitch_1
    iget-object v0, p0, Lux;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lux;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_0

    .line 519
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 530
    :pswitch_2
    iget-object v0, p0, Lux;->g:Lud;

    if-nez v0, :cond_1

    new-instance v0, Lud;

    invoke-virtual {p0}, Lux;->b()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0100c7

    invoke-direct {v0, v1, v2, v3}, Lud;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lux;->g:Lud;

    new-instance v0, Lzm;

    const v1, 0x800013

    invoke-direct {v0, v4, v4, v1}, Lzm;-><init>(III)V

    iget-object v1, p0, Lux;->g:Lud;

    invoke-virtual {v1, v0}, Lud;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 531
    :cond_1
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->g:Lud;

    invoke-virtual {v0, v1, v5}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    .line 546
    :cond_2
    :goto_1
    :pswitch_3
    return-void

    .line 534
    :pswitch_4
    iget-object v0, p0, Lux;->f:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 535
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lux;->f:Landroid/view/View;

    invoke-virtual {v0, v1, v5}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    .line 536
    iget-object v0, p0, Lux;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lzm;

    .line 537
    iput v4, v0, Lzm;->width:I

    .line 538
    iput v4, v0, Lzm;->height:I

    .line 539
    const v1, 0x800053

    iput v1, v0, Lzm;->a:I

    goto :goto_1

    .line 511
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 526
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 631
    iput-object p1, p0, Lux;->k:Landroid/graphics/drawable/Drawable;

    .line 632
    invoke-direct {p0}, Lux;->s()V

    .line 633
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    iput-boolean v0, p0, Lux;->l:Z

    .line 279
    invoke-direct {p0, p1}, Lux;->e(Ljava/lang/CharSequence;)V

    .line 280
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 567
    iget-object v0, p0, Lux;->g:Lud;

    if-nez v0, :cond_0

    .line 568
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t set dropdown selected position without an adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 571
    :cond_0
    iget-object v0, p0, Lux;->g:Lud;

    invoke-virtual {v0, p1}, Lud;->a(I)V

    .line 572
    return-void
.end method

.method public c(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lux;->s:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    .line 219
    iput-object p1, p0, Lux;->s:Landroid/graphics/drawable/Drawable;

    .line 220
    invoke-direct {p0}, Lux;->s()V

    .line 222
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 296
    iput-object p1, p0, Lux;->m:Ljava/lang/CharSequence;

    .line 297
    iget v0, p0, Lux;->e:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->b(Ljava/lang/CharSequence;)V

    .line 300
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return v0
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 602
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 603
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Liu;->j(Landroid/view/View;)Lkj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkj;->a(F)Lkj;

    move-result-object v0

    new-instance v1, Luz;

    invoke-direct {v1, p0}, Luz;-><init>(Lux;)V

    invoke-virtual {v0, v1}, Lkj;->a(Lky;)Lkj;

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    if-nez p1, :cond_0

    .line 619
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Liu;->j(Landroid/view/View;)Lkj;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lkj;->a(F)Lkj;

    move-result-object v0

    new-instance v1, Lva;

    invoke-direct {v1, p0}, Lva;-><init>(Lux;)V

    invoke-virtual {v0, v1}, Lkj;->a(Lky;)Lkj;

    goto :goto_0
.end method

.method public d(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lux;->i:Landroid/graphics/drawable/Drawable;

    .line 351
    invoke-direct {p0}, Lux;->q()V

    .line 352
    return-void
.end method

.method public d(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 644
    iput-object p1, p0, Lux;->n:Ljava/lang/CharSequence;

    .line 645
    invoke-direct {p0}, Lux;->r()V

    .line 646
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->g()Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->h()V

    .line 256
    return-void
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 637
    if-eqz p1, :cond_0

    iget-object v0, p0, Lux;->q:Lur;

    invoke-virtual {v0, p1}, Lur;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lux;->b(Landroid/graphics/drawable/Drawable;)V

    .line 640
    return-void

    .line 637
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(I)V
    .locals 1

    .prologue
    .line 650
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lux;->d(Ljava/lang/CharSequence;)V

    .line 651
    return-void

    .line 650
    :cond_0
    invoke-virtual {p0}, Lux;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->a()Z

    move-result v0

    return v0
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lux;->r:I

    if-ne p1, v0, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    iput p1, p0, Lux;->r:I

    .line 211
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->k()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    iget v0, p0, Lux;->r:I

    invoke-virtual {p0, v0}, Lux;->f(I)V

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->b()Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->c()Z

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->d()Z

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->e()Z

    move-result v0

    return v0
.end method

.method public k()V
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Lux;->d:Z

    .line 405
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lux;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->f()V

    .line 420
    return-void
.end method

.method public m()I
    .locals 1

    .prologue
    .line 424
    iget v0, p0, Lux;->e:I

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 504
    iget v0, p0, Lux;->p:I

    return v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lux;->g:Lud;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lux;->g:Lud;

    invoke-virtual {v0}, Lud;->g()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Landroid/view/View;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lux;->h:Landroid/view/View;

    return-object v0
.end method
