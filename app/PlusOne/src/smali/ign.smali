.class public final Lign;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ligr;

.field b:Ligr;

.field private final c:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ligq;

    invoke-direct {v0}, Ligq;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    .prologue
    const/4 v1, 0x2

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ligr;

    invoke-direct {v0, v1}, Ligr;-><init>(I)V

    iput-object v0, p0, Lign;->a:Ligr;

    .line 27
    new-instance v0, Ligr;

    invoke-direct {v0, v1}, Ligr;-><init>(I)V

    iput-object v0, p0, Lign;->b:Ligr;

    .line 73
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x4

    const/16 v3, 0x8

    const-wide/16 v4, 0xa

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Ligl;

    const-string v0, "thread-pool"

    const/16 v9, 0xa

    invoke-direct {v8, v0, v9}, Ligl;-><init>(Ljava/lang/String;I)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lign;->c:Ljava/util/concurrent/Executor;

    .line 78
    return-void
.end method


# virtual methods
.method public a(Ligo;)Ligi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ligo",
            "<TT;>;)",
            "Ligi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lign;->a(Ligo;Ligj;)Ligi;

    move-result-object v0

    return-object v0
.end method

.method public a(Ligo;Ligj;)Ligi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ligo",
            "<TT;>;",
            "Ligj",
            "<TT;>;)",
            "Ligi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Ligs;

    invoke-direct {v0, p0, p1, p2}, Ligs;-><init>(Lign;Ligo;Ligj;)V

    .line 84
    iget-object v1, p0, Lign;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 85
    return-object v0
.end method
