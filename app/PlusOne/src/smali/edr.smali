.class public final Ledr;
.super Lhye;
.source "PG"


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 227
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "item_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "header_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "event_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "event_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "past_event"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "author_gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "author_avatar_url"

    aput-object v2, v0, v1

    sput-object v0, Ledr;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 241
    sget-object v0, Lidg;->a:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 242
    iput p2, p0, Ledr;->c:I

    .line 243
    return-void
.end method

.method private a(Lhym;Landroid/database/Cursor;IIZZ[Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 289
    if-ltz p3, :cond_0

    if-eqz p6, :cond_1

    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_5

    if-eqz p6, :cond_5

    .line 290
    :cond_1
    sget-object v0, Ledr;->b:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lhym;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 291
    if-eqz p6, :cond_3

    const/4 v0, -0x1

    :goto_0
    add-int/2addr v0, p3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 293
    :goto_1
    if-eqz p6, :cond_4

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 294
    :cond_2
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 295
    const/4 v1, 0x2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 296
    const/4 v2, 0x3

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 297
    const/4 v3, 0x4

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 298
    const/4 v4, 0x0

    invoke-virtual {p1}, Lhym;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p7, v4

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p7, v4

    const/4 v4, 0x3

    aput-object v0, p7, v4

    const/4 v0, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p7, v0

    const/4 v1, 0x5

    if-eqz p5, :cond_6

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p7, v1

    const/4 v0, 0x6

    aput-object v2, p7, v0

    const/4 v0, 0x7

    aput-object v3, p7, v0

    invoke-virtual {p1, p7}, Lhym;->a([Ljava/lang/Object;)V

    .line 299
    const/4 v0, 0x0

    invoke-static {p7, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 291
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 293
    :cond_4
    invoke-interface {p2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    if-nez v0, :cond_2

    .line 302
    :cond_5
    return-void

    .line 298
    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 247
    invoke-virtual {p0}, Ledr;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ledr;->c:I

    .line 248
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    sget-object v2, Leds;->a:[Ljava/lang/String;

    .line 247
    invoke-static {v0, v1, v2}, Ldrm;->a(Landroid/content/Context;I[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 250
    if-eqz v2, :cond_2

    .line 251
    new-instance v1, Lhym;

    sget-object v0, Ledr;->b:[Ljava/lang/String;

    invoke-direct {v1, v0}, Lhym;-><init>([Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v0, Ledr;->b:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v7, v0, [Ljava/lang/Object;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    const/4 v6, 0x2

    invoke-static {v2, v0, v6}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0, v4, v5}, Ldrm;->a(Lidh;J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    :cond_1
    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Ledr;->a(Lhym;Landroid/database/Cursor;IIZZ[Ljava/lang/Object;)V

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Ledr;->a(Lhym;Landroid/database/Cursor;IIZZ[Ljava/lang/Object;)V

    .line 252
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 255
    :goto_0
    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
