.class public final Lmsz;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3355
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3366
    const/high16 v0, -0x80000000

    iput v0, p0, Lmsz;->b:I

    .line 3355
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3383
    const/4 v0, 0x0

    .line 3384
    iget-object v1, p0, Lmsz;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3385
    const/4 v0, 0x1

    iget-object v1, p0, Lmsz;->a:Ljava/lang/Integer;

    .line 3386
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3388
    :cond_0
    iget v1, p0, Lmsz;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 3389
    const/4 v1, 0x2

    iget v2, p0, Lmsz;->b:I

    .line 3390
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3392
    :cond_1
    iget-object v1, p0, Lmsz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3393
    iput v0, p0, Lmsz;->ai:I

    .line 3394
    return v0
.end method

.method public a(Loxn;)Lmsz;
    .locals 2

    .prologue
    .line 3402
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3403
    sparse-switch v0, :sswitch_data_0

    .line 3407
    iget-object v1, p0, Lmsz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3408
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmsz;->ah:Ljava/util/List;

    .line 3411
    :cond_1
    iget-object v1, p0, Lmsz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3413
    :sswitch_0
    return-object p0

    .line 3418
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsz;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 3422
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3423
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 3426
    :cond_2
    iput v0, p0, Lmsz;->b:I

    goto :goto_0

    .line 3428
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmsz;->b:I

    goto :goto_0

    .line 3403
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3371
    iget-object v0, p0, Lmsz;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3372
    const/4 v0, 0x1

    iget-object v1, p0, Lmsz;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3374
    :cond_0
    iget v0, p0, Lmsz;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 3375
    const/4 v0, 0x2

    iget v1, p0, Lmsz;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3377
    :cond_1
    iget-object v0, p0, Lmsz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3379
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3351
    invoke-virtual {p0, p1}, Lmsz;->a(Loxn;)Lmsz;

    move-result-object v0

    return-object v0
.end method
