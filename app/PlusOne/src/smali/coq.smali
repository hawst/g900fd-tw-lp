.class public final enum Lcoq;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcoq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcoq;

.field public static final enum b:Lcoq;

.field public static final enum c:Lcoq;

.field public static final enum d:Lcoq;

.field public static final enum e:Lcoq;

.field public static final enum f:Lcoq;

.field public static final enum g:Lcoq;

.field public static final enum h:Lcoq;

.field public static final enum i:Lcoq;

.field public static final enum j:Lcoq;

.field private static enum k:Lcoq;

.field private static final synthetic l:[Lcoq;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 7
    new-instance v0, Lcoq;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v3}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->a:Lcoq;

    .line 8
    new-instance v0, Lcoq;

    const-string v1, "MONTH"

    invoke-direct {v0, v1, v4}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->b:Lcoq;

    .line 9
    new-instance v0, Lcoq;

    const-string v1, "YEAR"

    invoke-direct {v0, v1, v5}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->c:Lcoq;

    .line 10
    new-instance v0, Lcoq;

    const-string v1, "LIBRARY_STATUS_BAR"

    invoke-direct {v0, v1, v6}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->d:Lcoq;

    .line 11
    new-instance v0, Lcoq;

    const-string v1, "AUTO_BACKUP_BAR"

    invoke-direct {v0, v1, v7}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->e:Lcoq;

    .line 12
    new-instance v0, Lcoq;

    const-string v1, "LOAD_MORE_BAR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->f:Lcoq;

    .line 13
    new-instance v0, Lcoq;

    const-string v1, "LOADING_MORE_SPINNER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->g:Lcoq;

    .line 14
    new-instance v0, Lcoq;

    const-string v1, "PROMO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->h:Lcoq;

    .line 15
    new-instance v0, Lcoq;

    const-string v1, "EMPTY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->k:Lcoq;

    .line 16
    new-instance v0, Lcoq;

    const-string v1, "LOAD_PREVIOUS_BAR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->i:Lcoq;

    .line 17
    new-instance v0, Lcoq;

    const-string v1, "DATE_HEADER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcoq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcoq;->j:Lcoq;

    .line 6
    const/16 v0, 0xb

    new-array v0, v0, [Lcoq;

    sget-object v1, Lcoq;->a:Lcoq;

    aput-object v1, v0, v3

    sget-object v1, Lcoq;->b:Lcoq;

    aput-object v1, v0, v4

    sget-object v1, Lcoq;->c:Lcoq;

    aput-object v1, v0, v5

    sget-object v1, Lcoq;->d:Lcoq;

    aput-object v1, v0, v6

    sget-object v1, Lcoq;->e:Lcoq;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcoq;->f:Lcoq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcoq;->g:Lcoq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcoq;->h:Lcoq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcoq;->k:Lcoq;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcoq;->i:Lcoq;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcoq;->j:Lcoq;

    aput-object v2, v0, v1

    sput-object v0, Lcoq;->l:[Lcoq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcoq;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lcoq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcoq;

    return-object v0
.end method

.method public static values()[Lcoq;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcoq;->l:[Lcoq;

    invoke-virtual {v0}, [Lcoq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcoq;

    return-object v0
.end method
