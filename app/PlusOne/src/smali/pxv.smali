.class public final Lpxv;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Ljava/lang/String;

.field private c:Lpxw;

.field private d:Ljava/lang/Boolean;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    iput v1, p0, Lpxv;->a:I

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lpxv;->c:Lpxw;

    .line 23
    iput v1, p0, Lpxv;->e:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 49
    const/4 v0, 0x0

    .line 50
    iget v1, p0, Lpxv;->a:I

    if-eq v1, v3, :cond_0

    .line 51
    const/4 v0, 0x1

    iget v1, p0, Lpxv;->a:I

    .line 52
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 54
    :cond_0
    iget-object v1, p0, Lpxv;->c:Lpxw;

    if-eqz v1, :cond_1

    .line 55
    const/4 v1, 0x2

    iget-object v2, p0, Lpxv;->c:Lpxw;

    .line 56
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_1
    iget-object v1, p0, Lpxv;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 59
    const/4 v1, 0x3

    iget-object v2, p0, Lpxv;->d:Ljava/lang/Boolean;

    .line 60
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 62
    :cond_2
    iget v1, p0, Lpxv;->e:I

    if-eq v1, v3, :cond_3

    .line 63
    const/4 v1, 0x5

    iget v2, p0, Lpxv;->e:I

    .line 64
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_3
    iget-object v1, p0, Lpxv;->b:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 67
    const/4 v1, 0x6

    iget-object v2, p0, Lpxv;->b:Ljava/lang/String;

    .line 68
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_4
    iget-object v1, p0, Lpxv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    iput v0, p0, Lpxv;->ai:I

    .line 72
    return v0
.end method

.method public a(Loxn;)Lpxv;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 80
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 81
    sparse-switch v0, :sswitch_data_0

    .line 85
    iget-object v1, p0, Lpxv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 86
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpxv;->ah:Ljava/util/List;

    .line 89
    :cond_1
    iget-object v1, p0, Lpxv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    :sswitch_0
    return-object p0

    .line 96
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 97
    if-eqz v0, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x64

    if-eq v0, v1, :cond_2

    const/16 v1, 0x66

    if-eq v0, v1, :cond_2

    const/16 v1, 0x67

    if-eq v0, v1, :cond_2

    const/16 v1, 0x68

    if-eq v0, v1, :cond_2

    const/16 v1, 0x69

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x78

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x77

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x86

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x87

    if-eq v0, v1, :cond_2

    const/16 v1, 0x79

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x80

    if-eq v0, v1, :cond_2

    const/16 v1, 0x81

    if-eq v0, v1, :cond_2

    const/16 v1, 0x82

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83

    if-eq v0, v1, :cond_2

    const/16 v1, 0x89

    if-eq v0, v1, :cond_2

    const/16 v1, 0x65

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x70

    if-eq v0, v1, :cond_2

    const/16 v1, 0x71

    if-eq v0, v1, :cond_2

    const/16 v1, 0x72

    if-eq v0, v1, :cond_2

    const/16 v1, 0x73

    if-eq v0, v1, :cond_2

    const/16 v1, 0x74

    if-eq v0, v1, :cond_2

    const/16 v1, 0x75

    if-eq v0, v1, :cond_2

    const/16 v1, 0x76

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x85

    if-eq v0, v1, :cond_2

    const/16 v1, 0x88

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x92

    if-eq v0, v1, :cond_2

    const/16 v1, 0x93

    if-eq v0, v1, :cond_2

    const/16 v1, 0x95

    if-eq v0, v1, :cond_2

    const/16 v1, 0x97

    if-eq v0, v1, :cond_2

    const/16 v1, 0x98

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x90

    if-eq v0, v1, :cond_2

    const/16 v1, 0x91

    if-eq v0, v1, :cond_2

    const/16 v1, 0x99

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_2

    const/16 v1, 0x94

    if-eq v0, v1, :cond_2

    const/16 v1, 0x96

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa4

    if-ne v0, v1, :cond_3

    .line 167
    :cond_2
    iput v0, p0, Lpxv;->a:I

    goto/16 :goto_0

    .line 169
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpxv;->a:I

    goto/16 :goto_0

    .line 174
    :sswitch_2
    iget-object v0, p0, Lpxv;->c:Lpxw;

    if-nez v0, :cond_4

    .line 175
    new-instance v0, Lpxw;

    invoke-direct {v0}, Lpxw;-><init>()V

    iput-object v0, p0, Lpxv;->c:Lpxw;

    .line 177
    :cond_4
    iget-object v0, p0, Lpxv;->c:Lpxw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 181
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpxv;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 185
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 186
    if-eq v0, v2, :cond_5

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    .line 190
    :cond_5
    iput v0, p0, Lpxv;->e:I

    goto/16 :goto_0

    .line 192
    :cond_6
    iput v2, p0, Lpxv;->e:I

    goto/16 :goto_0

    .line 197
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxv;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 81
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 28
    iget v0, p0, Lpxv;->a:I

    if-eq v0, v2, :cond_0

    .line 29
    const/4 v0, 0x1

    iget v1, p0, Lpxv;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 31
    :cond_0
    iget-object v0, p0, Lpxv;->c:Lpxw;

    if-eqz v0, :cond_1

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Lpxv;->c:Lpxw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 34
    :cond_1
    iget-object v0, p0, Lpxv;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x3

    iget-object v1, p0, Lpxv;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 37
    :cond_2
    iget v0, p0, Lpxv;->e:I

    if-eq v0, v2, :cond_3

    .line 38
    const/4 v0, 0x5

    iget v1, p0, Lpxv;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 40
    :cond_3
    iget-object v0, p0, Lpxv;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 41
    const/4 v0, 0x6

    iget-object v1, p0, Lpxv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 43
    :cond_4
    iget-object v0, p0, Lpxv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 45
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpxv;->a(Loxn;)Lpxv;

    move-result-object v0

    return-object v0
.end method
