.class public Lcxr;
.super Ldct;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldct",
        "<",
        "Ldec;",
        ">;"
    }
.end annotation


# instance fields
.field private P:Ldec;

.field private Q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 22
    invoke-direct {p0}, Ldct;-><init>()V

    .line 31
    new-instance v0, Ldep;

    iget-object v1, p0, Lcxr;->av:Llqm;

    new-instance v2, Lcxs;

    invoke-direct {v2, p0}, Lcxs;-><init>(Lcxr;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 75
    return-void
.end method

.method static synthetic a(Lcxr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcxr;->Q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcxr;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcxr;->Q:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcxr;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcxr;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic b(Lcxr;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0}, Lcxr;->U()V

    return-void
.end method

.method static synthetic c(Lcxr;)Ldec;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcxr;->P:Ldec;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Ldct;->a(Landroid/os/Bundle;)V

    .line 56
    iget-object v0, p0, Lcxr;->Q:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcxr;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tile_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcxr;->Q:Ljava/lang/String;

    .line 59
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 63
    iget-object v1, p0, Lcxr;->Q:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 64
    invoke-virtual {p0}, Lcxr;->w()Lbb;

    move-result-object v1

    invoke-virtual {p0}, Lcxr;->k()Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Lcxt;

    invoke-direct {v3, p0}, Lcxt;-><init>(Lcxr;)V

    invoke-virtual {v1, v0, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 66
    const/4 v0, 0x1

    .line 68
    :cond_0
    return v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0, p1}, Ldct;->c(Landroid/os/Bundle;)V

    .line 50
    iget-object v0, p0, Lcxr;->au:Llnh;

    const-class v1, Ldec;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldec;

    iput-object v0, p0, Lcxr;->P:Ldec;

    .line 51
    return-void
.end method
