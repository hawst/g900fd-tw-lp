.class public final Ldxz;
.super Lu;
.source "PG"


# instance fields
.field private N:I

.field private O:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lu;-><init>()V

    return-void
.end method

.method static synthetic a(Ldxz;)V
    .locals 6

    .prologue
    .line 22
    new-instance v0, Ldxv;

    iget v2, p0, Ldxz;->N:I

    const-string v3, "dialog_sync_disabled"

    iget-object v1, p0, Ldxz;->O:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    const v4, 0x7f1001b7

    if-ne v1, v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    new-instance v5, Ldyc;

    invoke-direct {v5, p0}, Ldyc;-><init>(Ldxz;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldxv;-><init>(Lu;ILjava/lang/String;ZLdxy;)V

    invoke-virtual {v0}, Ldxv;->a()V

    return-void

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 33
    invoke-virtual {p0}, Ldxz;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_0

    .line 35
    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ldxz;->N:I

    .line 39
    :cond_0
    const v0, 0x7f040053

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 41
    invoke-virtual {p0}, Ldxz;->n()Lz;

    move-result-object v2

    .line 42
    invoke-virtual {p0}, Ldxz;->n()Lz;

    move-result-object v0

    const-string v3, "auto_backup"

    const-string v4, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v0, v3, v4}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 41
    const v0, 0x7f1001b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v4, 0x7f0a0607

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 45
    const v0, 0x7f1001bf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 46
    new-instance v2, Ldya;

    invoke-direct {v2, p0}, Ldya;-><init>(Ldxz;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    const v0, 0x7f1001c0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 54
    new-instance v2, Ldyb;

    invoke-direct {v2, p0}, Ldyb;-><init>(Ldxz;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    const v0, 0x7f1001be

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Ldxz;->O:Landroid/widget/RadioGroup;

    .line 62
    return-object v1
.end method
