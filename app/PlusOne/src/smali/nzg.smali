.class public final Lnzg;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 162
    const/4 v0, 0x0

    .line 163
    iget-object v1, p0, Lnzg;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 164
    const/4 v0, 0x1

    iget-object v1, p0, Lnzg;->a:Ljava/lang/String;

    .line 165
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 167
    :cond_0
    iget-object v1, p0, Lnzg;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 168
    const/4 v1, 0x2

    iget-object v2, p0, Lnzg;->b:Ljava/lang/String;

    .line 169
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_1
    iget-object v1, p0, Lnzg;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 172
    const/4 v1, 0x3

    iget-object v2, p0, Lnzg;->c:Ljava/lang/String;

    .line 173
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_2
    iget-object v1, p0, Lnzg;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 176
    const/4 v1, 0x4

    iget-object v2, p0, Lnzg;->d:Ljava/lang/String;

    .line 177
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_3
    iget-object v1, p0, Lnzg;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 180
    const/4 v1, 0x5

    iget-object v2, p0, Lnzg;->e:Ljava/lang/String;

    .line 181
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :cond_4
    iget-object v1, p0, Lnzg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    iput v0, p0, Lnzg;->ai:I

    .line 185
    return v0
.end method

.method public a(Loxn;)Lnzg;
    .locals 2

    .prologue
    .line 193
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 194
    sparse-switch v0, :sswitch_data_0

    .line 198
    iget-object v1, p0, Lnzg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 199
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnzg;->ah:Ljava/util/List;

    .line 202
    :cond_1
    iget-object v1, p0, Lnzg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    :sswitch_0
    return-object p0

    .line 209
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzg;->a:Ljava/lang/String;

    goto :goto_0

    .line 213
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzg;->b:Ljava/lang/String;

    goto :goto_0

    .line 217
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzg;->c:Ljava/lang/String;

    goto :goto_0

    .line 221
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzg;->d:Ljava/lang/String;

    goto :goto_0

    .line 225
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzg;->e:Ljava/lang/String;

    goto :goto_0

    .line 194
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lnzg;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x1

    iget-object v1, p0, Lnzg;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lnzg;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 145
    const/4 v0, 0x2

    iget-object v1, p0, Lnzg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 147
    :cond_1
    iget-object v0, p0, Lnzg;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 148
    const/4 v0, 0x3

    iget-object v1, p0, Lnzg;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 150
    :cond_2
    iget-object v0, p0, Lnzg;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 151
    const/4 v0, 0x4

    iget-object v1, p0, Lnzg;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 153
    :cond_3
    iget-object v0, p0, Lnzg;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 154
    const/4 v0, 0x5

    iget-object v1, p0, Lnzg;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 156
    :cond_4
    iget-object v0, p0, Lnzg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 158
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lnzg;->a(Loxn;)Lnzg;

    move-result-object v0

    return-object v0
.end method
