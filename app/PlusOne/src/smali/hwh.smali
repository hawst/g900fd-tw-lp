.class public final Lhwh;
.super Landroid/widget/ImageButton;
.source "PG"

# interfaces
.implements Lhmm;


# instance fields
.field private a:Lhwp;

.field private b:I

.field private c:Lhmk;

.field private d:Lhuz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 42
    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 57
    invoke-virtual {p0}, Lhwh;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 58
    invoke-static {v0}, Lhwp;->a(Landroid/content/Context;)Lhwp;

    move-result-object v1

    iput-object v1, p0, Lhwh;->a:Lhwp;

    .line 61
    invoke-virtual {p0, v3}, Lhwh;->setMinimumHeight(I)V

    .line 62
    invoke-virtual {p0, v3}, Lhwh;->setMinimumWidth(I)V

    .line 64
    const/16 v1, 0xb

    iget-object v2, p0, Lhwh;->a:Lhwp;

    iget v2, v2, Lhwp;->a:I

    invoke-static {v0, p0, v1, v2, v3}, Lkcr;->a(Landroid/content/Context;Landroid/widget/ImageButton;III)V

    .line 67
    invoke-static {p0}, Llii;->g(Landroid/view/View;)V

    .line 69
    invoke-virtual {p0}, Lhwh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0589

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhwh;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 72
    new-instance v0, Lhmk;

    sget-object v1, Lonm;->n:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    iput-object v0, p0, Lhwh;->c:Lhmk;

    .line 43
    return-void
.end method


# virtual methods
.method public a(Lhuu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    iput v2, p0, Lhwh;->b:I

    iget-object v0, p0, Lhwh;->d:Lhuz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwh;->d:Lhuz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhuz;->a(Ljava/util/List;)V

    :cond_0
    invoke-virtual {p0, v2}, Lhwh;->setImageResource(I)V

    .line 88
    if-eqz p1, :cond_4

    .line 89
    invoke-virtual {p1}, Lhuu;->a()I

    move-result v0

    iput v0, p0, Lhwh;->b:I

    .line 91
    iget v0, p0, Lhwh;->b:I

    if-gez v0, :cond_1

    .line 92
    iput v2, p0, Lhwh;->b:I

    .line 95
    :cond_1
    const v0, 0x7f020347

    invoke-virtual {p0, v0}, Lhwh;->setImageResource(I)V

    .line 98
    invoke-virtual {p1}, Lhuu;->b()Ljava/util/ArrayList;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_3

    .line 101
    iget-object v1, p0, Lhwh;->d:Lhuz;

    if-nez v1, :cond_2

    .line 102
    new-instance v1, Lhuz;

    invoke-direct {v1, p0}, Lhuz;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lhwh;->d:Lhuz;

    .line 105
    :cond_2
    iget-object v1, p0, Lhwh;->d:Lhuz;

    invoke-virtual {v1, v0}, Lhuz;->a(Ljava/util/List;)V

    .line 110
    :cond_3
    :goto_0
    return-void

    .line 108
    :cond_4
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhwh;->setVisibility(I)V

    goto :goto_0
.end method

.method public ac_()Lhmk;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lhwh;->c:Lhmk;

    return-object v0
.end method
