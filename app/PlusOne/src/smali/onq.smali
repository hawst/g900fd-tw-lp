.class public final Lonq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lonp;

.field private b:[Lons;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1207
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1210
    sget-object v0, Lons;->a:[Lons;

    iput-object v0, p0, Lonq;->b:[Lons;

    .line 1213
    const/4 v0, 0x0

    iput-object v0, p0, Lonq;->a:Lonp;

    .line 1207
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1234
    .line 1235
    iget-object v1, p0, Lonq;->b:[Lons;

    if-eqz v1, :cond_1

    .line 1236
    iget-object v2, p0, Lonq;->b:[Lons;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1237
    if-eqz v4, :cond_0

    .line 1238
    const/4 v5, 0x1

    .line 1239
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1236
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1243
    :cond_1
    iget-object v1, p0, Lonq;->a:Lonp;

    if-eqz v1, :cond_2

    .line 1244
    const/4 v1, 0x2

    iget-object v2, p0, Lonq;->a:Lonp;

    .line 1245
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1247
    :cond_2
    iget-object v1, p0, Lonq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1248
    iput v0, p0, Lonq;->ai:I

    .line 1249
    return v0
.end method

.method public a(Loxn;)Lonq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1257
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1258
    sparse-switch v0, :sswitch_data_0

    .line 1262
    iget-object v2, p0, Lonq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1263
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lonq;->ah:Ljava/util/List;

    .line 1266
    :cond_1
    iget-object v2, p0, Lonq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1268
    :sswitch_0
    return-object p0

    .line 1273
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1274
    iget-object v0, p0, Lonq;->b:[Lons;

    if-nez v0, :cond_3

    move v0, v1

    .line 1275
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lons;

    .line 1276
    iget-object v3, p0, Lonq;->b:[Lons;

    if-eqz v3, :cond_2

    .line 1277
    iget-object v3, p0, Lonq;->b:[Lons;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1279
    :cond_2
    iput-object v2, p0, Lonq;->b:[Lons;

    .line 1280
    :goto_2
    iget-object v2, p0, Lonq;->b:[Lons;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1281
    iget-object v2, p0, Lonq;->b:[Lons;

    new-instance v3, Lons;

    invoke-direct {v3}, Lons;-><init>()V

    aput-object v3, v2, v0

    .line 1282
    iget-object v2, p0, Lonq;->b:[Lons;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1283
    invoke-virtual {p1}, Loxn;->a()I

    .line 1280
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1274
    :cond_3
    iget-object v0, p0, Lonq;->b:[Lons;

    array-length v0, v0

    goto :goto_1

    .line 1286
    :cond_4
    iget-object v2, p0, Lonq;->b:[Lons;

    new-instance v3, Lons;

    invoke-direct {v3}, Lons;-><init>()V

    aput-object v3, v2, v0

    .line 1287
    iget-object v2, p0, Lonq;->b:[Lons;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1291
    :sswitch_2
    iget-object v0, p0, Lonq;->a:Lonp;

    if-nez v0, :cond_5

    .line 1292
    new-instance v0, Lonp;

    invoke-direct {v0}, Lonp;-><init>()V

    iput-object v0, p0, Lonq;->a:Lonp;

    .line 1294
    :cond_5
    iget-object v0, p0, Lonq;->a:Lonp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1258
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1218
    iget-object v0, p0, Lonq;->b:[Lons;

    if-eqz v0, :cond_1

    .line 1219
    iget-object v1, p0, Lonq;->b:[Lons;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1220
    if-eqz v3, :cond_0

    .line 1221
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1219
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1225
    :cond_1
    iget-object v0, p0, Lonq;->a:Lonp;

    if-eqz v0, :cond_2

    .line 1226
    const/4 v0, 0x2

    iget-object v1, p0, Lonq;->a:Lonp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1228
    :cond_2
    iget-object v0, p0, Lonq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1230
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1203
    invoke-virtual {p0, p1}, Lonq;->a(Loxn;)Lonq;

    move-result-object v0

    return-object v0
.end method
