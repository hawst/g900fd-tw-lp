.class public final Lhdv;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(II)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const/16 v5, 0x80

    const/16 v0, 0x20

    const/4 v1, 0x0

    .line 70
    invoke-static {p0, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 71
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 73
    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 74
    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 76
    mul-int v0, v4, v5

    new-array v6, v0, [I

    .line 77
    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    move v0, v1

    :goto_0
    array-length v8, v6

    if-ge v0, v8, :cond_0

    const/high16 v8, -0x1000000

    const/high16 v9, 0x1000000

    invoke-virtual {v7, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    or-int/2addr v8, v9

    aput v8, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v4, v5, v0}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 79
    if-ne v4, v2, :cond_1

    if-eq v5, v3, :cond_2

    .line 80
    :cond_1
    invoke-static {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 83
    :cond_2
    return-object v0
.end method

.method public static a()Landroid/net/Uri;
    .locals 8

    .prologue
    const/16 v7, 0x64

    const/4 v6, 0x3

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 38
    new-array v4, v6, [Ljava/io/File;

    new-instance v0, Ljava/io/File;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    .line 40
    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    const-string v5, "Camera"

    invoke-direct {v0, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aput-object v0, v4, v3

    const/4 v0, 0x1

    sget-object v2, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    .line 42
    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x2

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    .line 43
    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    aput-object v2, v4, v0

    move v2, v3

    move-object v0, v1

    .line 47
    :goto_0
    if-ge v2, v6, :cond_3

    aget-object v0, v4, v2

    .line 48
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    move-object v0, v1

    .line 49
    :goto_1
    if-nez v0, :cond_3

    .line 50
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 48
    :cond_1
    new-instance v5, Lhdw;

    invoke-direct {v5}, Lhdw;-><init>()V

    invoke-virtual {v0, v5}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_2

    array-length v5, v0

    if-lez v5, :cond_2

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    aget-object v0, v0, v5

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 54
    :cond_3
    if-eqz v0, :cond_4

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_4
    invoke-static {v7, v7}, Lhdv;->a(II)Landroid/graphics/Bitmap;

    move-result-object v0

    const-string v1, ""

    const-string v2, "GeneratedSampleImage"

    invoke-static {v0, v1, v2, v3}, Lhcf;->a(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2
.end method
