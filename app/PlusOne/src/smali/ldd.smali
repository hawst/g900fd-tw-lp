.class public final Lldd;
.super Llch;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static c:I

.field private static d:I


# instance fields
.field private e:Llcj;

.field private f:Llcb;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lldd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lldd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Llch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Llcj;

    invoke-direct {v0, p1, p2, p3}, Llcj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lldd;->e:Llcj;

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lldd;->setClickable(Z)V

    .line 51
    invoke-virtual {p0}, Lldd;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lldd;->c:I

    .line 53
    return-void
.end method


# virtual methods
.method protected a(IIII)I
    .locals 7

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 86
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 88
    iget v2, p0, Lldd;->t:I

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 90
    iget-object v3, p0, Lldd;->e:Llcj;

    invoke-virtual {v3, v2, v1}, Llcj;->measure(II)V

    .line 91
    sget v1, Lldd;->c:I

    shl-int/lit8 v1, v1, 0x1

    .line 93
    iget v2, p0, Lldd;->t:I

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x3

    sput v1, Lldd;->d:I

    .line 94
    iget-object v1, p0, Lldd;->e:Llcj;

    invoke-virtual {v1}, Llcj;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p2

    .line 95
    sget v2, Lldd;->d:I

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 96
    invoke-virtual {p0}, Lldd;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v6, v0

    move v0, v1

    move v1, v6

    .line 97
    :goto_0
    if-ge v1, v3, :cond_1

    .line 98
    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p0, v4}, Lldd;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 99
    invoke-virtual {v4, v2, v2}, Landroid/view/View;->measure(II)V

    .line 101
    rem-int/lit8 v5, v1, 0x3

    if-nez v5, :cond_0

    .line 102
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    .line 97
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    :cond_1
    iget v1, p0, Lldd;->g:I

    add-int/lit8 v1, v1, -0x1

    .line 106
    sget v2, Lldd;->c:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lldd;->getHeight()I

    move-result v0

    add-int/2addr v0, p6

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 145
    invoke-super {p0}, Llch;->a()V

    .line 146
    invoke-virtual {p0}, Lldd;->m()V

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lldd;->f:Llcb;

    .line 148
    return-void
.end method

.method protected a(Landroid/database/Cursor;Llcr;I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-super {p0, p1, p2, p3}, Llch;->a(Landroid/database/Cursor;Llcr;I)V

    .line 59
    invoke-virtual {p0}, Lldd;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 60
    invoke-virtual {p0}, Lldd;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 62
    const/16 v0, 0x1b

    .line 63
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 62
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    iput-object v0, p0, Lldd;->f:Llcb;

    .line 64
    iget-object v0, p0, Lldd;->f:Llcb;

    invoke-virtual {v0}, Llcb;->a()I

    move-result v2

    .line 65
    int-to-float v0, v2

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v0, v5

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v0, v6

    iput v0, p0, Lldd;->g:I

    .line 66
    iget-object v0, p0, Lldd;->e:Llcj;

    const v5, 0x7f0a0483

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lldd;->z:Llct;

    iget-object v6, v6, Llct;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v5, v6}, Llcj;->a(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 68
    iget-object v0, p0, Lldd;->e:Llcj;

    const v5, 0x7f0d01e7

    .line 69
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    .line 68
    invoke-virtual {v0, v5}, Llcj;->a(I)V

    .line 70
    iget-object v0, p0, Lldd;->e:Llcj;

    const v5, 0x7f0d01e6

    .line 71
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    .line 70
    invoke-virtual {v0, v4}, Llcj;->b(I)V

    .line 72
    iget-object v0, p0, Lldd;->e:Llcj;

    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Llcj;->c(I)V

    .line 73
    iget-object v0, p0, Lldd;->e:Llcj;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Llcj;->a(Z)V

    .line 74
    iget-object v0, p0, Lldd;->e:Llcj;

    invoke-virtual {p0, v0}, Lldd;->addView(Landroid/view/View;)V

    move v0, v1

    .line 75
    :goto_0
    if-ge v0, v2, :cond_2

    .line 76
    new-instance v1, Llei;

    invoke-direct {v1, v3}, Llei;-><init>(Landroid/content/Context;)V

    .line 77
    iget-object v4, p0, Lldd;->f:Llcb;

    invoke-virtual {v4, v0}, Llcb;->a(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lldd;->f:Llcb;

    invoke-virtual {v5, v0}, Llcb;->c(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5, v0}, Llei;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 78
    invoke-virtual {v1, p0}, Llei;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    invoke-virtual {p0, v1}, Lldd;->addView(Landroid/view/View;)V

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_1
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    new-instance v0, Llcb;

    invoke-direct {v0}, Llcb;-><init>()V

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    new-array v2, v6, [Ljava/lang/String;

    iput-object v2, v0, Llcb;->a:[Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/String;

    iput-object v2, v0, Llcb;->b:[Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/String;

    iput-object v2, v0, Llcb;->c:[Ljava/lang/String;

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_0

    iget-object v7, v0, Llcb;->a:[Ljava/lang/String;

    invoke-static {v5}, Llcb;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    iget-object v7, v0, Llcb;->b:[Ljava/lang/String;

    invoke-static {v5}, Llcb;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    iget-object v7, v0, Llcb;->c:[Ljava/lang/String;

    invoke-static {v5}, Llcb;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 81
    :cond_2
    return-void
.end method

.method protected a(ZIIII)V
    .locals 8

    .prologue
    .line 111
    invoke-super/range {p0 .. p5}, Llch;->a(ZIIII)V

    .line 113
    iget-object v0, p0, Lldd;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 114
    iget-object v1, p0, Lldd;->e:Llcj;

    invoke-virtual {v1}, Llcj;->getMeasuredHeight()I

    move-result v1

    .line 115
    iget-object v2, p0, Lldd;->e:Llcj;

    iget-object v3, p0, Lldd;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lldd;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget v5, p0, Lldd;->t:I

    add-int/2addr v4, v5

    add-int v5, v0, v1

    invoke-virtual {v2, v3, v0, v4, v5}, Llcj;->layout(IIII)V

    .line 118
    invoke-virtual {p0}, Lldd;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 119
    if-nez v2, :cond_1

    .line 135
    :cond_0
    return-void

    .line 122
    :cond_1
    add-int/2addr v0, v1

    .line 123
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 125
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Lldd;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 126
    rem-int/lit8 v4, v1, 0x3

    .line 127
    iget-object v5, p0, Lldd;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sget v6, Lldd;->d:I

    sget v7, Lldd;->c:I

    add-int/2addr v6, v7

    mul-int/2addr v6, v4

    add-int/2addr v5, v6

    .line 128
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v5

    .line 129
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v0

    .line 128
    invoke-virtual {v3, v5, v0, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 131
    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 132
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sget v4, Lldd;->c:I

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 124
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 152
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 153
    iget-object v1, p0, Lldd;->a:Llci;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 154
    iget-object v1, p0, Lldd;->a:Llci;

    iget-object v2, p0, Lldd;->f:Llcb;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Llcb;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Llci;->r(Ljava/lang/String;)V

    .line 156
    :cond_0
    return-void
.end method
