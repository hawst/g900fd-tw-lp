.class public Lbwq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwe;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Laic;

.field private static final c:Landroid/net/Uri;


# instance fields
.field private final d:Lbwu;

.field private final e:Lbvs;

.field private final f:Lbvy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbvy",
            "<",
            "Laii;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lajt;

.field private final h:Lanh;

.field private final i:Landroid/os/PowerManager;

.field private final j:Laim;

.field private final k:Livc;

.field private final l:Livc;

.field private final m:Ljfb;

.field private final n:Lbig;

.field private final o:Lapo;

.field private final p:Lahw;

.field private final q:Lbwv;

.field private final r:Ljdw;

.field private final s:Lbwt;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 46
    const-class v0, Lbwq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbwq;->a:Ljava/lang/String;

    .line 91
    new-instance v0, Laic;

    sget-object v1, Laib;->e:Laib;

    sget-object v2, Lahx;->b:[Lahz;

    invoke-direct {v0, v1, v2}, Laic;-><init>(Laib;[Lahz;)V

    sput-object v0, Lbwq;->b:Laic;

    .line 94
    const-string v0, "moviemaker://post_sync"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbwq;->c:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lbwu;Ljfb;Lbvs;Lbvy;Lajt;Lbit;Lanh;Landroid/os/PowerManager;Lbig;Lapo;Livc;Livc;Ljdw;Lahw;Lbwv;Lbks;Ljava/util/concurrent/Executor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbwu;",
            "Ljfb;",
            "Lbvs;",
            "Lbvy",
            "<",
            "Laii;",
            ">;",
            "Lajt;",
            "Lbit;",
            "Lanh;",
            "Landroid/os/PowerManager;",
            "Lbig;",
            "Lapo;",
            "Livc;",
            "Livc;",
            "Ljdw;",
            "Lahw;",
            "Lbwv;",
            "Lbks;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    new-instance v2, Lbwt;

    invoke-direct {v2, p0}, Lbwt;-><init>(Lbwq;)V

    iput-object v2, p0, Lbwq;->s:Lbwt;

    .line 140
    const-string v2, "service"

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbwu;

    iput-object v2, p0, Lbwq;->d:Lbwu;

    .line 141
    const-string v2, "plusDataProvider"

    const/4 v3, 0x0

    invoke-static {p2, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljfb;

    iput-object v2, p0, Lbwq;->m:Ljfb;

    .line 142
    const-string v2, "backgroundServiceHelper"

    .line 143
    const/4 v3, 0x0

    invoke-static {p3, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbvs;

    iput-object v2, p0, Lbwq;->e:Lbvs;

    .line 144
    const-string v2, "analyzerConnection"

    const/4 v3, 0x0

    invoke-static {p4, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbvy;

    iput-object v2, p0, Lbwq;->f:Lbvy;

    .line 145
    const-string v2, "metricsAndMetadataStore"

    .line 146
    const/4 v3, 0x0

    invoke-static {p5, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lajt;

    iput-object v2, p0, Lbwq;->g:Lajt;

    .line 147
    const-string v2, "gservicesSettings"

    const/4 v3, 0x0

    invoke-static {p7, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lanh;

    iput-object v2, p0, Lbwq;->h:Lanh;

    .line 148
    const-string v2, "powerManager"

    const/4 v3, 0x0

    move-object/from16 v0, p8

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lbwq;->i:Landroid/os/PowerManager;

    .line 149
    const-string v2, "mediaExtractorFactory"

    .line 150
    const/4 v3, 0x0

    move-object/from16 v0, p9

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbig;

    iput-object v2, p0, Lbwq;->n:Lbig;

    .line 151
    const-string v2, "movieMakerClusterFilter"

    .line 152
    const/4 v3, 0x0

    move-object/from16 v0, p10

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lapo;

    iput-object v2, p0, Lbwq;->o:Lapo;

    .line 154
    new-instance v2, Lajd;

    iget-object v4, p0, Lbwq;->g:Lajt;

    new-instance v5, Lajx;

    iget-object v3, p0, Lbwq;->f:Lbvy;

    invoke-direct {v5, v3}, Lajx;-><init>(Lbvy;)V

    iget-object v7, p0, Lbwq;->m:Ljfb;

    move-object/from16 v3, p16

    move-object v6, p6

    invoke-direct/range {v2 .. v7}, Lajd;-><init>(Lbks;Lajl;Lajv;Lbit;Ljfb;)V

    .line 160
    new-instance v3, Laim;

    move-object/from16 v0, p17

    move-object/from16 v1, p17

    invoke-direct {v3, v0, v1, v2}, Laim;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lahx;)V

    iput-object v3, p0, Lbwq;->j:Laim;

    .line 163
    const-string v2, "batteryBudgetTracker"

    const/4 v3, 0x0

    move-object/from16 v0, p14

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lahw;

    iput-object v2, p0, Lbwq;->p:Lahw;

    .line 164
    const-string v2, "postSyncSettings"

    const/4 v3, 0x0

    move-object/from16 v0, p15

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbwv;

    iput-object v2, p0, Lbwq;->q:Lbwv;

    .line 165
    const-string v2, "aamEventsLogger"

    const/4 v3, 0x0

    move-object/from16 v0, p13

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljdw;

    iput-object v2, p0, Lbwq;->r:Ljdw;

    .line 166
    const/4 v2, 0x0

    sget-object v3, Lbwq;->a:Ljava/lang/String;

    move-object/from16 v0, p11

    invoke-static {v2, v3, v0}, Livb;->a(ZLjava/lang/String;Livc;)Livc;

    move-result-object v2

    iput-object v2, p0, Lbwq;->k:Livc;

    .line 167
    const/4 v2, 0x0

    sget-object v3, Lbwq;->a:Ljava/lang/String;

    move-object/from16 v0, p12

    invoke-static {v2, v3, v0}, Livb;->a(ZLjava/lang/String;Livc;)Livc;

    move-result-object v2

    iput-object v2, p0, Lbwq;->l:Livc;

    .line 168
    return-void
.end method

.method private a(Ljava/util/List;)I
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcen;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 357
    const/4 v0, 0x1

    .line 358
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v8, v0

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcen;

    .line 359
    iget-object v10, v0, Lcen;->a:Ljed;

    .line 362
    const/4 v7, 0x1

    .line 364
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, p0, Lbwq;->k:Livc;

    iget-object v2, v10, Ljed;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcen;->c:Ljava/util/List;

    .line 366
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x33

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Analyzing videos for cluster: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " videos)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 365
    invoke-interface {v1, v2}, Livc;->a(Ljava/lang/String;)V

    .line 369
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v1, v0, Lcen;->c:Ljava/util/List;

    .line 370
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v3, v0, Lcen;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v1, v3

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 373
    iget-object v1, v0, Lcen;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbon;

    .line 374
    new-instance v4, Ljej;

    invoke-virtual {v1}, Lbon;->a()Ljeg;

    move-result-object v1

    iget-object v1, v1, Ljeg;->b:Landroid/net/Uri;

    sget-object v5, Ljel;->b:Ljel;

    invoke-direct {v4, v1, v5}, Ljej;-><init>(Landroid/net/Uri;Ljel;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 376
    :cond_1
    iget-object v0, v0, Lcen;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmw;

    .line 377
    new-instance v3, Ljej;

    invoke-virtual {v0}, Lbmw;->a()Ljeg;

    move-result-object v0

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    sget-object v4, Ljel;->a:Ljel;

    invoke-direct {v3, v0, v4}, Ljej;-><init>(Landroid/net/Uri;Ljel;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 380
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    .line 381
    iget-object v1, p0, Lbwq;->g:Lajt;

    iget-object v2, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1, v2}, Lajt;->f(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 382
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_3

    .line 383
    iget-object v1, p0, Lbwq;->k:Livc;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Already have metrics for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Livc;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 388
    :cond_4
    iget-object v1, p0, Lbwq;->e:Lbvs;

    invoke-interface {v1}, Lbvs;->a()Z

    move-result v1

    if-nez v1, :cond_6

    .line 389
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_5

    .line 390
    iget-object v1, p0, Lbwq;->k:Livc;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Power connected for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Livc;->a(Ljava/lang/String;)V

    .line 392
    :cond_5
    const/4 v0, 0x6

    .line 529
    :goto_4
    return v0

    .line 395
    :cond_6
    iget-object v1, p0, Lbwq;->e:Lbvs;

    invoke-interface {v1}, Lbvs;->c()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 396
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_7

    .line 397
    iget-object v1, p0, Lbwq;->k:Livc;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Low battery for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Livc;->a(Ljava/lang/String;)V

    .line 399
    :cond_7
    const/4 v0, 0x5

    goto :goto_4

    .line 402
    :cond_8
    iget-object v1, p0, Lbwq;->e:Lbvs;

    invoke-interface {v1}, Lbvs;->b()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 403
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_9

    .line 404
    iget-object v1, p0, Lbwq;->k:Livc;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Screen on for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Livc;->a(Ljava/lang/String;)V

    .line 406
    :cond_9
    const/4 v0, 0x4

    goto :goto_4

    .line 409
    :cond_a
    iget-object v1, p0, Lbwq;->p:Lahw;

    .line 410
    invoke-interface {v1}, Lahw;->a()J

    move-result-wide v4

    .line 411
    const-wide/16 v2, 0x0

    cmp-long v1, v4, v2

    if-gtz v1, :cond_c

    .line 412
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_b

    .line 413
    iget-object v1, p0, Lbwq;->k:Livc;

    const-string v2, "Skipping analysis of item due to empty battery analysis budget:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Livc;->a(Ljava/lang/String;)V

    .line 416
    :cond_b
    const/4 v0, 0x3

    goto/16 :goto_4

    .line 419
    :cond_c
    iget-object v1, p0, Lbwq;->g:Lajt;

    iget-object v2, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1, v2}, Lajt;->c(Landroid/net/Uri;)I

    move-result v1

    iget-object v2, p0, Lbwq;->q:Lbwv;

    .line 420
    invoke-interface {v2}, Lbwv;->d()I

    move-result v2

    if-lt v1, v2, :cond_d

    .line 421
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_3

    .line 422
    iget-object v1, p0, Lbwq;->k:Livc;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Too many attempts for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Livc;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 429
    :cond_d
    iget-object v1, v0, Ljej;->b:Ljel;

    sget-object v2, Ljel;->b:Ljel;

    if-ne v1, v2, :cond_14

    .line 430
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_e

    .line 431
    iget-object v1, p0, Lbwq;->k:Livc;

    iget-object v2, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x33

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Analyzing video: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for up to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Livc;->a(Ljava/lang/String;)V

    .line 434
    :cond_e
    iget-object v1, p0, Lbwq;->r:Ljdw;

    iget-object v2, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1, v2}, Ljdw;->b(Landroid/net/Uri;)V

    .line 435
    iget-object v1, p0, Lbwq;->j:Laim;

    iget-object v2, v0, Ljej;->a:Landroid/net/Uri;

    sget-object v3, Lbwq;->b:Laic;

    iget-object v6, p0, Lbwq;->h:Lanh;

    .line 440
    invoke-virtual {v6}, Lanh;->ac()Z

    move-result v6

    .line 435
    invoke-static/range {v1 .. v6}, Lbww;->a(Laim;Landroid/net/Uri;Laic;JZ)Lbwz;

    move-result-object v1

    .line 441
    iget-object v2, p0, Lbwq;->r:Ljdw;

    iget-object v3, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1}, Lbwz;->b()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Ljdw;->b(Landroid/net/Uri;Z)V

    .line 456
    :goto_5
    iget-object v2, p0, Lbwq;->k:Livc;

    if-eqz v2, :cond_f

    .line 457
    iget-object v2, p0, Lbwq;->k:Livc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x11

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Analysis result: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Livc;->a(Ljava/lang/String;)V

    .line 459
    :cond_f
    iget-object v2, p0, Lbwq;->p:Lahw;

    invoke-interface {v1}, Lbwz;->f()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lahw;->a(J)V

    .line 460
    invoke-interface {v1}, Lbwz;->c()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 461
    iget-object v2, p0, Lbwq;->k:Livc;

    if-eqz v2, :cond_10

    .line 462
    iget-object v2, p0, Lbwq;->k:Livc;

    const-string v3, "Incrementing failures."

    invoke-interface {v2, v3}, Livc;->a(Ljava/lang/String;)V

    .line 464
    :cond_10
    iget-object v2, p0, Lbwq;->g:Lajt;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v2, v0}, Lajt;->d(Landroid/net/Uri;)V

    .line 466
    :cond_11
    invoke-interface {v1}, Lbwz;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 467
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_12

    .line 468
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "Skipping rest of cluster."

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 470
    :cond_12
    const/4 v0, 0x0

    .line 477
    :goto_6
    if-eqz v0, :cond_18

    .line 478
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_13

    .line 479
    iget-object v1, p0, Lbwq;->k:Livc;

    const-string v2, "Cluster video analysis completed successfully: "

    iget-object v0, v10, Ljed;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_17

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-interface {v1, v0}, Livc;->a(Ljava/lang/String;)V

    .line 482
    :cond_13
    iget-object v0, p0, Lbwq;->r:Ljdw;

    invoke-virtual {v10}, Ljed;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljdw;->a(Ljava/lang/String;)V

    .line 483
    iget-object v0, p0, Lbwq;->m:Ljfb;

    iget-object v1, p0, Lbwq;->o:Lapo;

    iget-object v2, p0, Lbwq;->n:Lbig;

    iget-object v3, p0, Lbwq;->g:Lajt;

    const/4 v5, 0x1

    iget-object v4, p0, Lbwq;->q:Lbwv;

    .line 490
    invoke-interface {v4}, Lbwv;->e()Z

    move-result v6

    iget-object v7, p0, Lbwq;->l:Livc;

    move-object v4, v10

    .line 483
    invoke-static/range {v0 .. v7}, Lcel;->a(Ljfb;Lapo;Lbig;Lajl;Ljed;ZZLivc;)Lcen;

    goto/16 :goto_0

    .line 443
    :cond_14
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_15

    .line 444
    iget-object v1, p0, Lbwq;->k:Livc;

    iget-object v2, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x33

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Analyzing photo: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for up to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Livc;->a(Ljava/lang/String;)V

    .line 447
    :cond_15
    iget-object v1, p0, Lbwq;->r:Ljdw;

    iget-object v2, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1, v2}, Ljdw;->a(Landroid/net/Uri;)V

    .line 448
    iget-object v2, p0, Lbwq;->j:Laim;

    iget-object v3, v0, Ljej;->a:Landroid/net/Uri;

    sget-object v6, Lbwq;->b:Laic;

    iget-object v1, p0, Lbwq;->h:Lanh;

    .line 453
    invoke-virtual {v1}, Lanh;->ac()Z

    move-result v12

    .line 448
    new-instance v1, Lbwx;

    invoke-direct {v1, v12}, Lbwx;-><init>(Z)V

    invoke-virtual {v1}, Lbwx;->a()Lahy;

    move-result-object v12

    invoke-interface {v2, v3, v6, v12}, Lahx;->b(Landroid/net/Uri;Laic;Lahy;)V

    invoke-virtual {v1, v4, v5}, Lbwx;->a(J)V

    invoke-virtual {v1}, Lbwx;->e()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-interface {v2}, Lahx;->a()V

    .line 454
    :cond_16
    iget-object v2, p0, Lbwq;->r:Ljdw;

    iget-object v3, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1}, Lbwz;->b()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Ljdw;->a(Landroid/net/Uri;Z)V

    goto/16 :goto_5

    .line 479
    :cond_17
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 493
    :cond_18
    const/4 v0, 0x0

    .line 494
    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_19

    .line 495
    iget-object v2, p0, Lbwq;->k:Livc;

    const-string v1, "Couldn\'t finish cluster video analysis, keeping old flag value for: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, v10, Ljed;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1a

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_8
    invoke-interface {v2, v1}, Livc;->a(Ljava/lang/String;)V

    :cond_19
    move v8, v0

    .line 499
    goto/16 :goto_0

    .line 495
    :cond_1a
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    .line 501
    :cond_1b
    iget-object v0, p0, Lbwq;->p:Lahw;

    invoke-interface {v0}, Lahw;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1d

    .line 502
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_1c

    .line 503
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "Out of battery budget."

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 505
    :cond_1c
    const/4 v0, 0x3

    goto/16 :goto_4

    .line 508
    :cond_1d
    iget-object v0, p0, Lbwq;->e:Lbvs;

    invoke-interface {v0}, Lbvs;->a()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 509
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_1e

    .line 510
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "Power disconnected."

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 512
    :cond_1e
    const/4 v0, 0x6

    goto/16 :goto_4

    .line 515
    :cond_1f
    iget-object v0, p0, Lbwq;->e:Lbvs;

    invoke-interface {v0}, Lbvs;->c()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 516
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_20

    .line 517
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "Low battery."

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 519
    :cond_20
    const/4 v0, 0x5

    goto/16 :goto_4

    .line 522
    :cond_21
    iget-object v0, p0, Lbwq;->e:Lbvs;

    invoke-interface {v0}, Lbvs;->b()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 523
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_22

    .line 524
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "Screen on."

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 526
    :cond_22
    const/4 v0, 0x4

    goto/16 :goto_4

    .line 529
    :cond_23
    if-eqz v8, :cond_24

    const/4 v0, 0x1

    goto/16 :goto_4

    :cond_24
    const/4 v0, 0x2

    goto/16 :goto_4

    :cond_25
    move v0, v7

    goto/16 :goto_6
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 74
    sget-object v0, Lbwq;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 75
    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 77
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/moviemaker/service/PostSyncService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbwq;)Lbvs;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbwq;->e:Lbvs;

    return-object v0
.end method

.method static synthetic b(Lbwq;)Livc;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbwq;->k:Livc;

    return-object v0
.end method

.method static synthetic c(Lbwq;)Laim;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbwq;->j:Laim;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lbwq;->f:Lbvy;

    invoke-virtual {v0}, Lbvy;->a()V

    .line 173
    iget-object v0, p0, Lbwq;->e:Lbvs;

    iget-object v1, p0, Lbwq;->s:Lbwt;

    invoke-interface {v0, v1}, Lbvs;->a(Ljava/lang/Runnable;)V

    .line 175
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 185
    iget-object v0, p0, Lbwq;->e:Lbvs;

    invoke-interface {v0}, Lbvs;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_2

    .line 190
    iget-object v0, p0, Lbwq;->k:Livc;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "processIntent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Livc;->a(Ljava/lang/String;)V

    .line 193
    :cond_2
    iget-object v0, p0, Lbwq;->h:Lanh;

    invoke-virtual {v0}, Lanh;->ab()Z

    move-result v0

    if-nez v0, :cond_3

    .line 194
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "post sync service not enabled: abort now"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 200
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 201
    if-nez v0, :cond_4

    .line 202
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "provided intent doesn\'t have data: abort now"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_4
    const-string v2, "gaia_id"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 209
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 210
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "provided intent doesn\'t have a Gaia ID: abort now"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_5
    iget-object v0, p0, Lbwq;->m:Ljfb;

    invoke-virtual {v0}, Ljfb;->b()Ljava/util/List;

    move-result-object v0

    .line 217
    if-eqz v0, :cond_6

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 218
    :goto_1
    if-nez v0, :cond_7

    .line 219
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "provided Gaia ID is not one of the synced accounts: abort now"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 217
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 225
    :cond_7
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_8

    .line 226
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v2, "service started"

    invoke-interface {v0, v2}, Livc;->a(Ljava/lang/String;)V

    .line 229
    :cond_8
    iget-object v0, p0, Lbwq;->i:Landroid/os/PowerManager;

    sget-object v2, Lbwq;->a:Ljava/lang/String;

    .line 230
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    .line 231
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 233
    :try_start_0
    iget-object v0, p0, Lbwq;->m:Ljfb;

    invoke-virtual {v0, v4}, Ljfb;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lbwq;->m:Ljfb;

    iget-object v1, p0, Lbwq;->o:Lapo;

    iget-object v2, p0, Lbwq;->n:Lbig;

    iget-object v3, p0, Lbwq;->g:Lajt;

    const/4 v5, 0x1

    iget-object v6, p0, Lbwq;->q:Lbwv;

    invoke-interface {v6}, Lbwv;->e()Z

    move-result v6

    iget-object v7, p0, Lbwq;->l:Livc;

    invoke-static/range {v0 .. v7}, Lcel;->a(Ljfb;Lapo;Lbig;Lajl;Ljava/lang/String;ZZLivc;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lbwq;->k:Livc;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lbwq;->k:Livc;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x34

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "There are "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " clusters with media to analyze"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Livc;->a(Ljava/lang/String;)V

    :cond_9
    iget-object v1, p0, Lbwq;->r:Ljdw;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v1, v2}, Ljdw;->a(I)V

    iget-object v1, p0, Lbwq;->h:Lanh;

    invoke-virtual {v1}, Lanh;->aa()Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "post sync analysis not enabled: abort now"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    :cond_a
    :goto_2
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 238
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "service finished"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 233
    :cond_b
    :try_start_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lbwq;->e:Lbvs;

    invoke-interface {v1}, Lbvs;->a()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-direct {p0, v0}, Lbwq;->a(Ljava/util/List;)I

    move-result v0

    sget-object v1, Lbwr;->a:[I

    add-int/lit8 v0, v0, -0x1

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "Not rescheduling"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 235
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    .line 233
    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lbwq;->q:Lbwv;

    invoke-interface {v0}, Lbwv;->b()J

    move-result-wide v0

    iget-object v2, p0, Lbwq;->k:Livc;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lbwq;->k:Livc;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x26

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Rescheduling in "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "ms"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Livc;->a(Ljava/lang/String;)V

    :cond_c
    iget-object v2, p0, Lbwq;->d:Lbwu;

    invoke-interface {v2, v4, v0, v1}, Lbwu;->a(Ljava/lang/String;J)V

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lbwq;->q:Lbwv;

    invoke-interface {v0}, Lbwv;->a()J

    move-result-wide v0

    iget-object v2, p0, Lbwq;->k:Livc;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lbwq;->k:Livc;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x26

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Rescheduling in "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "ms"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Livc;->a(Ljava/lang/String;)V

    :cond_d
    iget-object v2, p0, Lbwq;->d:Lbwu;

    invoke-interface {v2, v4, v0, v1}, Lbwu;->a(Ljava/lang/String;J)V

    goto/16 :goto_2

    :pswitch_3
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "Starting plugged-in analyzer instead."

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    :cond_e
    iget-object v0, p0, Lbwq;->d:Lbwu;

    invoke-interface {v0}, Lbwu;->b()V

    goto/16 :goto_2

    :pswitch_4
    iget-object v0, p0, Lbwq;->q:Lbwv;

    invoke-interface {v0}, Lbwv;->c()J

    move-result-wide v0

    iget-object v2, p0, Lbwq;->k:Livc;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lbwq;->k:Livc;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x26

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Rescheduling in "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "ms"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Livc;->a(Ljava/lang/String;)V

    :cond_f
    iget-object v2, p0, Lbwq;->d:Lbwu;

    invoke-interface {v2, v4, v0, v1}, Lbwu;->a(Ljava/lang/String;J)V

    goto/16 :goto_2

    :pswitch_5
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "All done, stopping service."

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_10
    iget-object v0, p0, Lbwq;->k:Livc;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lbwq;->k:Livc;

    const-string v1, "Starting plugged-in analyzer instead."

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    :cond_11
    iget-object v0, p0, Lbwq;->d:Lbwu;

    invoke-interface {v0}, Lbwu;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lbwq;->f:Lbvy;

    invoke-virtual {v0}, Lbvy;->b()V

    .line 180
    iget-object v0, p0, Lbwq;->e:Lbvs;

    invoke-interface {v0}, Lbvs;->d()V

    .line 181
    return-void
.end method
