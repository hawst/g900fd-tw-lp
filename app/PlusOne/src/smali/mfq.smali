.class public final Lmfq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnno;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12715
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12718
    iput-object v0, p0, Lmfq;->apiHeader:Llyq;

    .line 12721
    iput-object v0, p0, Lmfq;->a:Lnno;

    .line 12715
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12738
    const/4 v0, 0x0

    .line 12739
    iget-object v1, p0, Lmfq;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 12740
    const/4 v0, 0x1

    iget-object v1, p0, Lmfq;->apiHeader:Llyq;

    .line 12741
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12743
    :cond_0
    iget-object v1, p0, Lmfq;->a:Lnno;

    if-eqz v1, :cond_1

    .line 12744
    const/4 v1, 0x2

    iget-object v2, p0, Lmfq;->a:Lnno;

    .line 12745
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12747
    :cond_1
    iget-object v1, p0, Lmfq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12748
    iput v0, p0, Lmfq;->ai:I

    .line 12749
    return v0
.end method

.method public a(Loxn;)Lmfq;
    .locals 2

    .prologue
    .line 12757
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12758
    sparse-switch v0, :sswitch_data_0

    .line 12762
    iget-object v1, p0, Lmfq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12763
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfq;->ah:Ljava/util/List;

    .line 12766
    :cond_1
    iget-object v1, p0, Lmfq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12768
    :sswitch_0
    return-object p0

    .line 12773
    :sswitch_1
    iget-object v0, p0, Lmfq;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 12774
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfq;->apiHeader:Llyq;

    .line 12776
    :cond_2
    iget-object v0, p0, Lmfq;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12780
    :sswitch_2
    iget-object v0, p0, Lmfq;->a:Lnno;

    if-nez v0, :cond_3

    .line 12781
    new-instance v0, Lnno;

    invoke-direct {v0}, Lnno;-><init>()V

    iput-object v0, p0, Lmfq;->a:Lnno;

    .line 12783
    :cond_3
    iget-object v0, p0, Lmfq;->a:Lnno;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12758
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12726
    iget-object v0, p0, Lmfq;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 12727
    const/4 v0, 0x1

    iget-object v1, p0, Lmfq;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12729
    :cond_0
    iget-object v0, p0, Lmfq;->a:Lnno;

    if-eqz v0, :cond_1

    .line 12730
    const/4 v0, 0x2

    iget-object v1, p0, Lmfq;->a:Lnno;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12732
    :cond_1
    iget-object v0, p0, Lmfq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12734
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12711
    invoke-virtual {p0, p1}, Lmfq;->a(Loxn;)Lmfq;

    move-result-object v0

    return-object v0
.end method
