.class public final Lmlb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfo;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25270
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25273
    iput-object v0, p0, Lmlb;->apiHeader:Llyr;

    .line 25276
    iput-object v0, p0, Lmlb;->a:Lnfo;

    .line 25270
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 25293
    const/4 v0, 0x0

    .line 25294
    iget-object v1, p0, Lmlb;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 25295
    const/4 v0, 0x1

    iget-object v1, p0, Lmlb;->apiHeader:Llyr;

    .line 25296
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25298
    :cond_0
    iget-object v1, p0, Lmlb;->a:Lnfo;

    if-eqz v1, :cond_1

    .line 25299
    const/4 v1, 0x2

    iget-object v2, p0, Lmlb;->a:Lnfo;

    .line 25300
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25302
    :cond_1
    iget-object v1, p0, Lmlb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25303
    iput v0, p0, Lmlb;->ai:I

    .line 25304
    return v0
.end method

.method public a(Loxn;)Lmlb;
    .locals 2

    .prologue
    .line 25312
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 25313
    sparse-switch v0, :sswitch_data_0

    .line 25317
    iget-object v1, p0, Lmlb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 25318
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmlb;->ah:Ljava/util/List;

    .line 25321
    :cond_1
    iget-object v1, p0, Lmlb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25323
    :sswitch_0
    return-object p0

    .line 25328
    :sswitch_1
    iget-object v0, p0, Lmlb;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 25329
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmlb;->apiHeader:Llyr;

    .line 25331
    :cond_2
    iget-object v0, p0, Lmlb;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25335
    :sswitch_2
    iget-object v0, p0, Lmlb;->a:Lnfo;

    if-nez v0, :cond_3

    .line 25336
    new-instance v0, Lnfo;

    invoke-direct {v0}, Lnfo;-><init>()V

    iput-object v0, p0, Lmlb;->a:Lnfo;

    .line 25338
    :cond_3
    iget-object v0, p0, Lmlb;->a:Lnfo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25313
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25281
    iget-object v0, p0, Lmlb;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 25282
    const/4 v0, 0x1

    iget-object v1, p0, Lmlb;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25284
    :cond_0
    iget-object v0, p0, Lmlb;->a:Lnfo;

    if-eqz v0, :cond_1

    .line 25285
    const/4 v0, 0x2

    iget-object v1, p0, Lmlb;->a:Lnfo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25287
    :cond_1
    iget-object v0, p0, Lmlb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 25289
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 25266
    invoke-virtual {p0, p1}, Lmlb;->a(Loxn;)Lmlb;

    move-result-object v0

    return-object v0
.end method
