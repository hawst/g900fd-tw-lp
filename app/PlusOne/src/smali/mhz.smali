.class public final Lmhz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lovi;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20896
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20899
    iput-object v0, p0, Lmhz;->apiHeader:Llyr;

    .line 20902
    iput-object v0, p0, Lmhz;->a:Lovi;

    .line 20896
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 20919
    const/4 v0, 0x0

    .line 20920
    iget-object v1, p0, Lmhz;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 20921
    const/4 v0, 0x1

    iget-object v1, p0, Lmhz;->apiHeader:Llyr;

    .line 20922
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20924
    :cond_0
    iget-object v1, p0, Lmhz;->a:Lovi;

    if-eqz v1, :cond_1

    .line 20925
    const/4 v1, 0x2

    iget-object v2, p0, Lmhz;->a:Lovi;

    .line 20926
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20928
    :cond_1
    iget-object v1, p0, Lmhz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20929
    iput v0, p0, Lmhz;->ai:I

    .line 20930
    return v0
.end method

.method public a(Loxn;)Lmhz;
    .locals 2

    .prologue
    .line 20938
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 20939
    sparse-switch v0, :sswitch_data_0

    .line 20943
    iget-object v1, p0, Lmhz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 20944
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhz;->ah:Ljava/util/List;

    .line 20947
    :cond_1
    iget-object v1, p0, Lmhz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20949
    :sswitch_0
    return-object p0

    .line 20954
    :sswitch_1
    iget-object v0, p0, Lmhz;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 20955
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmhz;->apiHeader:Llyr;

    .line 20957
    :cond_2
    iget-object v0, p0, Lmhz;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20961
    :sswitch_2
    iget-object v0, p0, Lmhz;->a:Lovi;

    if-nez v0, :cond_3

    .line 20962
    new-instance v0, Lovi;

    invoke-direct {v0}, Lovi;-><init>()V

    iput-object v0, p0, Lmhz;->a:Lovi;

    .line 20964
    :cond_3
    iget-object v0, p0, Lmhz;->a:Lovi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20939
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 20907
    iget-object v0, p0, Lmhz;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 20908
    const/4 v0, 0x1

    iget-object v1, p0, Lmhz;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20910
    :cond_0
    iget-object v0, p0, Lmhz;->a:Lovi;

    if-eqz v0, :cond_1

    .line 20911
    const/4 v0, 0x2

    iget-object v1, p0, Lmhz;->a:Lovi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20913
    :cond_1
    iget-object v0, p0, Lmhz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 20915
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 20892
    invoke-virtual {p0, p1}, Lmhz;->a(Loxn;)Lmhz;

    move-result-object v0

    return-object v0
.end method
