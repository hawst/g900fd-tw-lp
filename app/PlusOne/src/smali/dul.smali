.class public final Ldul;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldum;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldul;->a:Ljava/util/List;

    .line 165
    return-void
.end method

.method private f()Ldun;
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Ldul;->a:Ljava/util/List;

    iget v1, p0, Ldul;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldum;

    iget v1, p0, Ldul;->c:I

    iget-object v2, v0, Ldum;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Ldum;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldun;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Ldun;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 91
    .line 92
    iget-object v0, p0, Ldul;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldum;

    .line 93
    iget-object v1, v0, Ldum;->b:Ljava/util/Map;

    invoke-virtual {v0, p1, p2}, Ldum;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_0

    move-object v0, v3

    .line 94
    :goto_1
    if-eqz v0, :cond_2

    :goto_2
    move-object v2, v0

    .line 97
    goto :goto_0

    .line 93
    :cond_0
    iget v5, v0, Ldum;->c:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v0, Ldum;->c:I

    iget-object v5, v0, Ldum;->a:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v5, v1, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldun;

    iget-wide v6, v0, Ldum;->d:J

    iget-wide v8, v1, Ldun;->e:J

    sub-long/2addr v6, v8

    iput-wide v6, v0, Ldum;->d:J

    move-object v0, v1

    goto :goto_1

    .line 98
    :cond_1
    return-object v2

    :cond_2
    move-object v0, v2

    goto :goto_2
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Ldul;->b:I

    .line 65
    iput v1, p0, Ldul;->c:I

    .line 67
    iput v1, p0, Ldul;->d:I

    .line 68
    iget-object v0, p0, Ldul;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldum;

    .line 69
    iget-object v0, v0, Ldum;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 70
    iget v2, p0, Ldul;->d:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ldul;->d:I

    goto :goto_0

    .line 72
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 7

    .prologue
    .line 38
    invoke-virtual {p0}, Ldul;->a()V

    .line 39
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 40
    invoke-virtual {p0}, Ldul;->c()Ldun;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    iget-wide v0, v0, Ldun;->e:J

    sub-long/2addr p1, v0

    .line 46
    goto :goto_0

    .line 49
    :cond_0
    invoke-virtual {p0}, Ldul;->b()Ldun;

    move-result-object v0

    .line 50
    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Ldul;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldum;

    .line 56
    iget-object v1, v0, Ldum;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_2
    if-ltz v1, :cond_2

    iget-object v3, v0, Ldum;->a:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_2
    iget-object v3, v0, Ldum;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, v0, Ldum;->a:Ljava/util/List;

    const/4 v5, 0x0

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v4, v5, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v3, v0, Ldum;->a:Ljava/util/List;

    goto :goto_1

    .line 58
    :cond_3
    return-void
.end method

.method public a(Ldum;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Ldul;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    return-void
.end method

.method public b()Ldun;
    .locals 3

    .prologue
    .line 75
    invoke-virtual {p0}, Ldul;->c()Ldun;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_0

    .line 77
    iget-object v1, v0, Ldun;->b:Ljava/lang/String;

    iget v2, v0, Ldun;->c:I

    invoke-virtual {p0, v1, v2}, Ldul;->a(Ljava/lang/String;I)Ldun;

    .line 79
    :cond_0
    return-object v0
.end method

.method public c()Ldun;
    .locals 2

    .prologue
    .line 84
    :cond_0
    iget v0, p0, Ldul;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldul;->b:I

    iget-object v1, p0, Ldul;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Ldul;->b:I

    iget v0, p0, Ldul;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldul;->c:I

    .line 85
    :cond_1
    invoke-direct {p0}, Ldul;->f()Ldun;

    move-result-object v0

    if-nez v0, :cond_2

    iget v0, p0, Ldul;->c:I

    iget v1, p0, Ldul;->d:I

    if-le v0, v1, :cond_0

    .line 87
    :cond_2
    invoke-direct {p0}, Ldul;->f()Ldun;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 3

    .prologue
    .line 109
    const/4 v0, 0x0

    .line 110
    iget-object v1, p0, Ldul;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldum;

    .line 111
    invoke-virtual {v0}, Ldum;->b()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 112
    goto :goto_0

    .line 113
    :cond_0
    return v1
.end method

.method e()J
    .locals 5

    .prologue
    .line 117
    const-wide/16 v0, 0x0

    .line 118
    iget-object v2, p0, Ldul;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldum;

    .line 119
    invoke-virtual {v0}, Ldum;->c()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 120
    goto :goto_0

    .line 121
    :cond_0
    return-wide v2
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 126
    invoke-virtual {p0}, Ldul;->d()I

    move-result v0

    .line 127
    invoke-virtual {p0}, Ldul;->e()J

    move-result-wide v2

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x4c

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "{CacheEvictionPlan numEntries: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", totalBytes:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
