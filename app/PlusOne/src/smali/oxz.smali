.class public final Loxz;
.super Loxq;
.source "PG"


# instance fields
.field public a:[I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0}, Loxq;-><init>()V

    .line 187
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Loxz;->a:[I

    .line 190
    const/high16 v0, -0x80000000

    iput v0, p0, Loxz;->b:I

    .line 178
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 209
    .line 210
    iget-object v1, p0, Loxz;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Loxz;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 212
    iget-object v2, p0, Loxz;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 214
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_0
    iget-object v0, p0, Loxz;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 219
    :cond_1
    iget v1, p0, Loxz;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 220
    const/4 v1, 0x2

    iget v2, p0, Loxz;->b:I

    .line 221
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_2
    iget-object v1, p0, Loxz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    iput v0, p0, Loxz;->ai:I

    .line 225
    return v0
.end method

.method public a(Loxn;)Loxz;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 233
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 234
    sparse-switch v0, :sswitch_data_0

    .line 238
    iget-object v1, p0, Loxz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 239
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loxz;->ah:Ljava/util/List;

    .line 242
    :cond_1
    iget-object v1, p0, Loxz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    :sswitch_0
    return-object p0

    .line 249
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 250
    iget-object v0, p0, Loxz;->a:[I

    array-length v0, v0

    .line 251
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 252
    iget-object v2, p0, Loxz;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 253
    iput-object v1, p0, Loxz;->a:[I

    .line 254
    :goto_1
    iget-object v1, p0, Loxz;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 255
    iget-object v1, p0, Loxz;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 256
    invoke-virtual {p1}, Loxn;->a()I

    .line 254
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 259
    :cond_2
    iget-object v1, p0, Loxz;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 263
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 264
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 267
    :cond_3
    iput v0, p0, Loxz;->b:I

    goto :goto_0

    .line 269
    :cond_4
    iput v3, p0, Loxz;->b:I

    goto :goto_0

    .line 234
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 195
    iget-object v0, p0, Loxz;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Loxz;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 196
    iget-object v1, p0, Loxz;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 197
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 200
    :cond_0
    iget v0, p0, Loxz;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 201
    const/4 v0, 0x2

    iget v1, p0, Loxz;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 203
    :cond_1
    iget-object v0, p0, Loxz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 205
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Loxz;->a(Loxn;)Loxz;

    move-result-object v0

    return-object v0
.end method
