.class public final Lhfu;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/libraries/social/avatars/ui/AvatarView;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 213
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 214
    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 216
    packed-switch p2, :pswitch_data_0

    .line 223
    const/16 v0, 0xa

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 224
    const/16 v0, 0x9

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    move v0, v1

    .line 226
    :goto_0
    invoke-virtual {v2, v1, v0, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 227
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    return-void

    .line 218
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d023c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 219
    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/libraries/social/avatars/ui/AvatarView;Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 232
    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 233
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 234
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 235
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 236
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 237
    const v0, 0x7f0d023a

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setMinimumWidth(I)V

    .line 238
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    .line 239
    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 65
    const v0, 0x7f100154

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 66
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 67
    const v0, 0x7f100155

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 68
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 69
    return-void
.end method

.method public static a(Lhfv;Lhfw;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    const v0, 0x7f100154

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 34
    const v0, 0x7f100155

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 37
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 38
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 40
    invoke-virtual {p1}, Lhfw;->d()Lhxc;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {v0}, Lhxc;->d()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 43
    invoke-interface {p0, p1}, Lhfv;->b(Lhfw;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 44
    :goto_0
    if-eqz v0, :cond_1

    .line 45
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 56
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 43
    goto :goto_0

    .line 47
    :cond_1
    invoke-interface {p0, p1}, Lhfv;->a(Lhfw;)I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 48
    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 54
    :goto_2
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 50
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d023d

    .line 51
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 52
    invoke-virtual {v4, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_2
.end method
