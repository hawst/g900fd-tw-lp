.class final Ljvz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Z

.field final b:Ljwg;


# direct methods
.method private constructor <init>(Ljwg;)V
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljvz;-><init>(Ljwg;Z)V

    .line 380
    return-void
.end method

.method private constructor <init>(Ljwg;Z)V
    .locals 0

    .prologue
    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383
    iput-object p1, p0, Ljvz;->b:Ljwg;

    .line 384
    iput-boolean p2, p0, Ljvz;->a:Z

    .line 385
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljvz;
    .locals 2

    .prologue
    .line 388
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 389
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "separator may not be empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_1
    new-instance v0, Ljvz;

    new-instance v1, Ljwa;

    invoke-direct {v1, p0}, Ljwa;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljvz;-><init>(Ljwg;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 429
    new-instance v0, Ljwc;

    invoke-direct {v0, p0, p1}, Ljwc;-><init>(Ljvz;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public a()Ljvz;
    .locals 3

    .prologue
    .line 425
    new-instance v0, Ljvz;

    iget-object v1, p0, Ljvz;->b:Ljwg;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljvz;-><init>(Ljwg;Z)V

    return-object v0
.end method
