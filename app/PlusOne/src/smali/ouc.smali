.class public final Louc;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Lous;

.field private c:Lowd;

.field private d:Loqa;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 713
    invoke-direct {p0}, Loxq;-><init>()V

    .line 723
    const/high16 v0, -0x80000000

    iput v0, p0, Louc;->a:I

    .line 726
    iput-object v1, p0, Louc;->b:Lous;

    .line 729
    iput-object v1, p0, Louc;->c:Lowd;

    .line 732
    iput-object v1, p0, Louc;->d:Loqa;

    .line 713
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 755
    const/4 v0, 0x0

    .line 756
    iget v1, p0, Louc;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 757
    const/4 v0, 0x1

    iget v1, p0, Louc;->a:I

    .line 758
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 760
    :cond_0
    iget-object v1, p0, Louc;->b:Lous;

    if-eqz v1, :cond_1

    .line 761
    const/4 v1, 0x2

    iget-object v2, p0, Louc;->b:Lous;

    .line 762
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 764
    :cond_1
    iget-object v1, p0, Louc;->c:Lowd;

    if-eqz v1, :cond_2

    .line 765
    const/4 v1, 0x3

    iget-object v2, p0, Louc;->c:Lowd;

    .line 766
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 768
    :cond_2
    iget-object v1, p0, Louc;->d:Loqa;

    if-eqz v1, :cond_3

    .line 769
    const/4 v1, 0x4

    iget-object v2, p0, Louc;->d:Loqa;

    .line 770
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 772
    :cond_3
    iget-object v1, p0, Louc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 773
    iput v0, p0, Louc;->ai:I

    .line 774
    return v0
.end method

.method public a(Loxn;)Louc;
    .locals 2

    .prologue
    .line 782
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 783
    sparse-switch v0, :sswitch_data_0

    .line 787
    iget-object v1, p0, Louc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 788
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Louc;->ah:Ljava/util/List;

    .line 791
    :cond_1
    iget-object v1, p0, Louc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 793
    :sswitch_0
    return-object p0

    .line 798
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 799
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 803
    :cond_2
    iput v0, p0, Louc;->a:I

    goto :goto_0

    .line 805
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Louc;->a:I

    goto :goto_0

    .line 810
    :sswitch_2
    iget-object v0, p0, Louc;->b:Lous;

    if-nez v0, :cond_4

    .line 811
    new-instance v0, Lous;

    invoke-direct {v0}, Lous;-><init>()V

    iput-object v0, p0, Louc;->b:Lous;

    .line 813
    :cond_4
    iget-object v0, p0, Louc;->b:Lous;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 817
    :sswitch_3
    iget-object v0, p0, Louc;->c:Lowd;

    if-nez v0, :cond_5

    .line 818
    new-instance v0, Lowd;

    invoke-direct {v0}, Lowd;-><init>()V

    iput-object v0, p0, Louc;->c:Lowd;

    .line 820
    :cond_5
    iget-object v0, p0, Louc;->c:Lowd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 824
    :sswitch_4
    iget-object v0, p0, Louc;->d:Loqa;

    if-nez v0, :cond_6

    .line 825
    new-instance v0, Loqa;

    invoke-direct {v0}, Loqa;-><init>()V

    iput-object v0, p0, Louc;->d:Loqa;

    .line 827
    :cond_6
    iget-object v0, p0, Louc;->d:Loqa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 783
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 737
    iget v0, p0, Louc;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 738
    const/4 v0, 0x1

    iget v1, p0, Louc;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 740
    :cond_0
    iget-object v0, p0, Louc;->b:Lous;

    if-eqz v0, :cond_1

    .line 741
    const/4 v0, 0x2

    iget-object v1, p0, Louc;->b:Lous;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 743
    :cond_1
    iget-object v0, p0, Louc;->c:Lowd;

    if-eqz v0, :cond_2

    .line 744
    const/4 v0, 0x3

    iget-object v1, p0, Louc;->c:Lowd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 746
    :cond_2
    iget-object v0, p0, Louc;->d:Loqa;

    if-eqz v0, :cond_3

    .line 747
    const/4 v0, 0x4

    iget-object v1, p0, Louc;->d:Loqa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 749
    :cond_3
    iget-object v0, p0, Louc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 751
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 709
    invoke-virtual {p0, p1}, Louc;->a(Loxn;)Louc;

    move-result-object v0

    return-object v0
.end method
