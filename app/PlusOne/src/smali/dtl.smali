.class final Ldtl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldtg;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ldts;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILdts;)V
    .locals 2

    .prologue
    .line 1510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1511
    iput-object p1, p0, Ldtl;->a:Landroid/content/Context;

    .line 1512
    iput p2, p0, Ldtl;->b:I

    .line 1513
    iput-object p3, p0, Ldtl;->c:Ldts;

    .line 1514
    sget-object v0, Ldtf;->a:[I

    invoke-virtual {p3}, Ldts;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1526
    const/4 v0, 0x0

    iput v0, p0, Ldtl;->d:I

    .line 1528
    :goto_0
    return-void

    .line 1516
    :pswitch_0
    const/4 v0, 0x3

    iput v0, p0, Ldtl;->d:I

    goto :goto_0

    .line 1519
    :pswitch_1
    const/4 v0, 0x2

    iput v0, p0, Ldtl;->d:I

    goto :goto_0

    .line 1523
    :pswitch_2
    const/4 v0, 0x1

    iput v0, p0, Ldtl;->d:I

    goto :goto_0

    .line 1514
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ldth;
    .locals 6

    .prologue
    .line 1532
    new-instance v0, Ldkr;

    iget-object v1, p0, Ldtl;->a:Landroid/content/Context;

    iget v2, p0, Ldtl;->b:I

    iget v4, p0, Ldtl;->d:I

    iget-object v3, p0, Ldtl;->c:Ldts;

    iget-boolean v5, v3, Ldts;->g:Z

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Ldkr;-><init>(Landroid/content/Context;ILjava/lang/String;IZ)V

    .line 1534
    invoke-virtual {v0}, Ldkr;->l()V

    .line 1535
    const-string v1, "EsTileSync"

    invoke-virtual {v0, v1}, Ldkr;->e(Ljava/lang/String;)V

    .line 1537
    new-instance v1, Ldth;

    invoke-direct {v1, v0}, Ldth;-><init>(Ldkr;)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1542
    const-string v0, "InitialAllPhotosFetcher, requestReason: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Ldtl;->d:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xb

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
