.class public final Lkvp;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Lkfo;

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private final e:I

.field private final f:Lktq;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;I)V
    .locals 2

    .prologue
    .line 76
    const-string v0, "EditSquareMembershipTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 78
    iput p2, p0, Lkvp;->a:I

    .line 79
    iput-object p3, p0, Lkvp;->c:Ljava/lang/String;

    .line 80
    iput p4, p0, Lkvp;->e:I

    .line 82
    new-instance v0, Lkfo;

    iget v1, p0, Lkvp;->a:I

    invoke-direct {v0, p1, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkvp;->b:Lkfo;

    .line 83
    const-class v0, Lktq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iput-object v0, p0, Lkvp;->f:Lktq;

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 50
    const-string v0, "EditSquareMembershipTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 52
    iput p2, p0, Lkvp;->a:I

    .line 53
    iput-object p3, p0, Lkvp;->c:Ljava/lang/String;

    .line 55
    if-eqz p4, :cond_0

    const-string v0, ":"

    invoke-virtual {p4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    const-string v0, "g:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object p4, v0

    .line 58
    :cond_0
    iput-object p4, p0, Lkvp;->d:Ljava/lang/String;

    .line 59
    iput p5, p0, Lkvp;->e:I

    .line 61
    new-instance v0, Lkfo;

    iget v1, p0, Lkvp;->a:I

    invoke-direct {v0, p1, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkvp;->b:Lkfo;

    .line 62
    const-class v0, Lktq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iput-object v0, p0, Lkvp;->f:Lktq;

    .line 63
    return-void

    .line 56
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 92
    new-instance v2, Lkvo;

    invoke-virtual {p0}, Lkvp;->f()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lkvp;->b:Lkfo;

    iget-object v4, p0, Lkvp;->c:Ljava/lang/String;

    iget v5, p0, Lkvp;->e:I

    invoke-direct {v2, v0, v3, v4, v5}, Lkvo;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;I)V

    .line 95
    iget-object v0, p0, Lkvp;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lkvp;->d:Ljava/lang/String;

    invoke-static {v0}, Lkto;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 97
    iget-object v0, p0, Lkvp;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v4, "t:"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 98
    :goto_0
    if-eqz v3, :cond_4

    .line 99
    invoke-virtual {v2, v3}, Lkvo;->b(Ljava/lang/String;)V

    .line 105
    :cond_0
    :goto_1
    invoke-virtual {v2}, Lkvo;->l()V

    .line 107
    invoke-virtual {v2}, Lkvo;->t()Z

    move-result v0

    if-nez v0, :cond_2

    .line 108
    iget-object v0, p0, Lkvp;->d:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 109
    invoke-virtual {v2}, Lkvo;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmaj;

    iget-object v0, v0, Lmaj;->a:Lntm;

    iget-object v0, v0, Lntm;->a:Lnsr;

    .line 110
    if-eqz v0, :cond_1

    .line 112
    :try_start_0
    iget-object v3, p0, Lkvp;->f:Lktq;

    iget v4, p0, Lkvp;->a:I

    invoke-interface {v3, v4, v0}, Lktq;->a(ILnsr;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :cond_1
    :goto_2
    if-nez v0, :cond_2

    .line 118
    iget-object v0, p0, Lkvp;->f:Lktq;

    iget v1, p0, Lkvp;->a:I

    iget-object v3, p0, Lkvp;->c:Ljava/lang/String;

    iget v4, p0, Lkvp;->e:I

    invoke-interface {v0, v1, v3, v4}, Lktq;->a(ILjava/lang/String;I)V

    .line 126
    :cond_2
    :goto_3
    new-instance v0, Lhoz;

    iget v1, v2, Lkff;->i:I

    iget-object v3, v2, Lkff;->k:Ljava/lang/Exception;

    .line 127
    invoke-virtual {v2}, Lkvo;->t()Z

    move-result v2

    invoke-virtual {p0, v2}, Lkvp;->a(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 128
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "square_id"

    iget-object v3, p0, Lkvp;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-object v0

    :cond_3
    move-object v0, v1

    .line 97
    goto :goto_0

    .line 100
    :cond_4
    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v2, v0}, Lkvo;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 114
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_2

    .line 121
    :cond_5
    iget-object v0, p0, Lkvp;->f:Lktq;

    iget v1, p0, Lkvp;->a:I

    iget-object v3, p0, Lkvp;->c:Ljava/lang/String;

    iget-object v4, p0, Lkvp;->d:Ljava/lang/String;

    iget v5, p0, Lkvp;->e:I

    invoke-interface {v0, v1, v3, v4, v5}, Lktq;->a(ILjava/lang/String;Ljava/lang/String;I)V

    goto :goto_3
.end method

.method public a(Z)Ljava/lang/String;
    .locals 3

    .prologue
    const v0, 0x7f0a04bd

    const v1, 0x7f0a04be

    .line 164
    if-eqz p1, :cond_0

    iget v2, p0, Lkvp;->e:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0a058e

    :goto_0
    :pswitch_1
    invoke-virtual {p0}, Lkvp;->f()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :pswitch_2
    const v0, 0x7f0a04b8

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a04ba

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a04bb

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0a04c5

    goto :goto_0

    :pswitch_6
    move v0, v1

    goto :goto_0

    :pswitch_7
    move v0, v1

    goto :goto_0

    :pswitch_8
    move v0, v1

    goto :goto_0

    :pswitch_9
    const v0, 0x7f0a04bf

    goto :goto_0

    :pswitch_a
    const v0, 0x7f0a04c0

    goto :goto_0

    :pswitch_b
    const v0, 0x7f0a04c1

    goto :goto_0

    :pswitch_c
    const v0, 0x7f0a04c2

    goto :goto_0

    :pswitch_d
    const v0, 0x7f0a04c3

    goto :goto_0

    :pswitch_e
    const v0, 0x7f0a04c4

    goto :goto_0

    :pswitch_f
    const v0, 0x7f0a04bc

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_c
        :pswitch_2
        :pswitch_2
        :pswitch_d
        :pswitch_f
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_8
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_e
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lkvp;->d:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Lkvp;->e:I

    sparse-switch v0, :sswitch_data_0

    .line 155
    const v0, 0x7f0a058d

    .line 159
    :goto_0
    invoke-virtual {p0}, Lkvp;->f()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 139
    :sswitch_0
    const v0, 0x7f0a04b4

    .line 140
    goto :goto_0

    .line 143
    :sswitch_1
    const v0, 0x7f0a04b5

    .line 144
    goto :goto_0

    .line 147
    :sswitch_2
    const v0, 0x7f0a04b6

    .line 148
    goto :goto_0

    .line 151
    :sswitch_3
    const v0, 0x7f0a04b7

    .line 152
    goto :goto_0

    .line 136
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x5 -> :sswitch_3
        0x14 -> :sswitch_2
    .end sparse-switch
.end method
