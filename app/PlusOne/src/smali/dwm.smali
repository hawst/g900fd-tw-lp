.class public final Ldwm;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lkzl;

.field private final e:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 23
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Ldwm;->e:Ldp;

    .line 28
    iput p2, p0, Ldwm;->c:I

    .line 29
    iput-object p3, p0, Ldwm;->b:Ljava/lang/String;

    .line 30
    const-class v0, Lkzl;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Ldwm;->d:Lkzl;

    .line 31
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 35
    new-instance v2, Lhym;

    sget-object v0, Llbd;->a:[Ljava/lang/String;

    invoke-direct {v2, v0}, Lhym;-><init>([Ljava/lang/String;)V

    .line 36
    new-array v0, v7, [Ljava/lang/Object;

    .line 37
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    .line 38
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    .line 39
    invoke-virtual {v2, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Ldwm;->d:Lkzl;

    iget v1, p0, Ldwm;->c:I

    sget-object v3, Llbf;->c:[Ljava/lang/String;

    iget-object v4, p0, Ldwm;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v3, v4}, Lkzl;->a(I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 43
    if-eqz v1, :cond_0

    .line 44
    iget-object v0, p0, Ldwm;->e:Ldp;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 46
    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 56
    :goto_0
    return-object v0

    .line 50
    :cond_2
    iget-object v0, p0, Ldwm;->d:Lkzl;

    iget v3, p0, Ldwm;->c:I

    sget-object v4, Llbg;->a:[Ljava/lang/String;

    iget-object v5, p0, Ldwm;->b:Ljava/lang/String;

    invoke-interface {v0, v3, v4, v5}, Lkzl;->b(I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 52
    if-eqz v3, :cond_3

    .line 53
    iget-object v0, p0, Ldwm;->e:Ldp;

    invoke-interface {v3, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 56
    :cond_3
    new-instance v0, Landroid/database/MergeCursor;

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/database/Cursor;

    aput-object v2, v4, v6

    aput-object v1, v4, v8

    aput-object v3, v4, v7

    invoke-direct {v0, v4}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    goto :goto_0
.end method
