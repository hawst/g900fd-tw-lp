.class public final Lmtq;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 3801
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3804
    iput v0, p0, Lmtq;->a:I

    .line 3811
    iput v0, p0, Lmtq;->b:I

    .line 3801
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 3834
    const/4 v0, 0x0

    .line 3835
    iget v1, p0, Lmtq;->a:I

    if-eq v1, v3, :cond_0

    .line 3836
    const/4 v0, 0x1

    iget v1, p0, Lmtq;->a:I

    .line 3837
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3839
    :cond_0
    iget-object v1, p0, Lmtq;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3840
    const/4 v1, 0x2

    iget-object v2, p0, Lmtq;->c:Ljava/lang/Integer;

    .line 3841
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3843
    :cond_1
    iget-object v1, p0, Lmtq;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 3844
    const/4 v1, 0x3

    iget-object v2, p0, Lmtq;->d:Ljava/lang/Integer;

    .line 3845
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3847
    :cond_2
    iget v1, p0, Lmtq;->b:I

    if-eq v1, v3, :cond_3

    .line 3848
    const/4 v1, 0x4

    iget v2, p0, Lmtq;->b:I

    .line 3849
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3851
    :cond_3
    iget-object v1, p0, Lmtq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3852
    iput v0, p0, Lmtq;->ai:I

    .line 3853
    return v0
.end method

.method public a(Loxn;)Lmtq;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3861
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3862
    sparse-switch v0, :sswitch_data_0

    .line 3866
    iget-object v1, p0, Lmtq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3867
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtq;->ah:Ljava/util/List;

    .line 3870
    :cond_1
    iget-object v1, p0, Lmtq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3872
    :sswitch_0
    return-object p0

    .line 3877
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3878
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x16

    if-eq v0, v1, :cond_2

    const/16 v1, 0x17

    if-eq v0, v1, :cond_2

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    const/16 v1, 0x19

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x20

    if-eq v0, v1, :cond_2

    const/16 v1, 0x21

    if-eq v0, v1, :cond_2

    const/16 v1, 0x22

    if-eq v0, v1, :cond_2

    const/16 v1, 0x23

    if-eq v0, v1, :cond_2

    const/16 v1, 0x24

    if-eq v0, v1, :cond_2

    const/16 v1, 0x25

    if-eq v0, v1, :cond_2

    const/16 v1, 0x26

    if-eq v0, v1, :cond_2

    const/16 v1, 0x27

    if-eq v0, v1, :cond_2

    const/16 v1, 0x28

    if-eq v0, v1, :cond_2

    const/16 v1, 0x29

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x30

    if-eq v0, v1, :cond_2

    const/16 v1, 0x31

    if-eq v0, v1, :cond_2

    const/16 v1, 0x32

    if-eq v0, v1, :cond_2

    const/16 v1, 0x33

    if-eq v0, v1, :cond_2

    const/16 v1, 0x34

    if-eq v0, v1, :cond_2

    const/16 v1, 0x35

    if-eq v0, v1, :cond_2

    const/16 v1, 0x36

    if-eq v0, v1, :cond_2

    const/16 v1, 0x37

    if-eq v0, v1, :cond_2

    const/16 v1, 0x38

    if-eq v0, v1, :cond_2

    const/16 v1, 0x39

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x40

    if-eq v0, v1, :cond_2

    const/16 v1, 0x41

    if-eq v0, v1, :cond_2

    const/16 v1, 0x42

    if-eq v0, v1, :cond_2

    const/16 v1, 0x43

    if-eq v0, v1, :cond_2

    const/16 v1, 0x44

    if-eq v0, v1, :cond_2

    const/16 v1, 0x45

    if-eq v0, v1, :cond_2

    const/16 v1, 0x46

    if-eq v0, v1, :cond_2

    const/16 v1, 0x47

    if-eq v0, v1, :cond_2

    const/16 v1, 0x48

    if-eq v0, v1, :cond_2

    const/16 v1, 0x49

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x50

    if-eq v0, v1, :cond_2

    const/16 v1, 0x51

    if-eq v0, v1, :cond_2

    const/16 v1, 0x52

    if-eq v0, v1, :cond_2

    const/16 v1, 0x53

    if-eq v0, v1, :cond_2

    const/16 v1, 0x54

    if-eq v0, v1, :cond_2

    const/16 v1, 0x55

    if-eq v0, v1, :cond_2

    const/16 v1, 0x56

    if-eq v0, v1, :cond_2

    const/16 v1, 0x57

    if-eq v0, v1, :cond_2

    const/16 v1, 0x58

    if-eq v0, v1, :cond_2

    const/16 v1, 0x59

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x60

    if-eq v0, v1, :cond_2

    const/16 v1, 0x61

    if-eq v0, v1, :cond_2

    const/16 v1, 0x62

    if-eq v0, v1, :cond_2

    const/16 v1, 0x63

    if-eq v0, v1, :cond_2

    const/16 v1, 0x64

    if-eq v0, v1, :cond_2

    const/16 v1, 0x65

    if-eq v0, v1, :cond_2

    const/16 v1, 0x66

    if-eq v0, v1, :cond_2

    const/16 v1, 0x67

    if-eq v0, v1, :cond_2

    const/16 v1, 0x68

    if-eq v0, v1, :cond_2

    const/16 v1, 0x69

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x70

    if-eq v0, v1, :cond_2

    const/16 v1, 0x71

    if-eq v0, v1, :cond_2

    const/16 v1, 0x72

    if-eq v0, v1, :cond_2

    const/16 v1, 0x73

    if-eq v0, v1, :cond_2

    const/16 v1, 0x74

    if-eq v0, v1, :cond_2

    const/16 v1, 0x75

    if-eq v0, v1, :cond_2

    const/16 v1, 0x76

    if-eq v0, v1, :cond_2

    const/16 v1, 0x77

    if-eq v0, v1, :cond_2

    const/16 v1, 0x78

    if-eq v0, v1, :cond_2

    const/16 v1, 0x79

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x80

    if-eq v0, v1, :cond_2

    const/16 v1, 0x81

    if-eq v0, v1, :cond_2

    const/16 v1, 0x82

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84

    if-eq v0, v1, :cond_2

    const/16 v1, 0x85

    if-eq v0, v1, :cond_2

    const/16 v1, 0x86

    if-eq v0, v1, :cond_2

    const/16 v1, 0x87

    if-eq v0, v1, :cond_2

    const/16 v1, 0x88

    if-eq v0, v1, :cond_2

    const/16 v1, 0x89

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x90

    if-eq v0, v1, :cond_2

    const/16 v1, 0x91

    if-eq v0, v1, :cond_2

    const/16 v1, 0x92

    if-eq v0, v1, :cond_2

    const/16 v1, 0x93

    if-eq v0, v1, :cond_2

    const/16 v1, 0x94

    if-eq v0, v1, :cond_2

    const/16 v1, 0x95

    if-eq v0, v1, :cond_2

    const/16 v1, 0x96

    if-eq v0, v1, :cond_2

    const/16 v1, 0x97

    if-eq v0, v1, :cond_2

    const/16 v1, 0x98

    if-eq v0, v1, :cond_2

    const/16 v1, 0x99

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xaa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xab

    if-eq v0, v1, :cond_2

    const/16 v1, 0xac

    if-eq v0, v1, :cond_2

    const/16 v1, 0xad

    if-eq v0, v1, :cond_2

    const/16 v1, 0xae

    if-eq v0, v1, :cond_2

    const/16 v1, 0xaf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xba

    if-eq v0, v1, :cond_2

    const/16 v1, 0xbb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xbc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xbd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xbe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xbf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xca

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xce

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xda

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xde

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xea

    if-eq v0, v1, :cond_2

    const/16 v1, 0xeb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xec

    if-eq v0, v1, :cond_2

    const/16 v1, 0xed

    if-eq v0, v1, :cond_2

    const/16 v1, 0xee

    if-eq v0, v1, :cond_2

    const/16 v1, 0xef

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xfa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xfb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xfc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xfd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xff

    if-eq v0, v1, :cond_2

    const/16 v1, 0x100

    if-eq v0, v1, :cond_2

    const/16 v1, 0x101

    if-eq v0, v1, :cond_2

    const/16 v1, 0x102

    if-eq v0, v1, :cond_2

    const/16 v1, 0x103

    if-eq v0, v1, :cond_2

    const/16 v1, 0x104

    if-eq v0, v1, :cond_2

    const/16 v1, 0x105

    if-eq v0, v1, :cond_2

    const/16 v1, 0x106

    if-eq v0, v1, :cond_2

    const/16 v1, 0x107

    if-eq v0, v1, :cond_2

    const/16 v1, 0x108

    if-eq v0, v1, :cond_2

    const/16 v1, 0x109

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x110

    if-eq v0, v1, :cond_2

    const/16 v1, 0x111

    if-eq v0, v1, :cond_2

    const/16 v1, 0x112

    if-eq v0, v1, :cond_2

    const/16 v1, 0x113

    if-eq v0, v1, :cond_2

    const/16 v1, 0x114

    if-eq v0, v1, :cond_2

    const/16 v1, 0x115

    if-eq v0, v1, :cond_2

    const/16 v1, 0x116

    if-eq v0, v1, :cond_2

    const/16 v1, 0x117

    if-eq v0, v1, :cond_2

    const/16 v1, 0x118

    if-eq v0, v1, :cond_2

    const/16 v1, 0x119

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x120

    if-eq v0, v1, :cond_2

    const/16 v1, 0x121

    if-eq v0, v1, :cond_2

    const/16 v1, 0x122

    if-ne v0, v1, :cond_3

    .line 4168
    :cond_2
    iput v0, p0, Lmtq;->a:I

    goto/16 :goto_0

    .line 4170
    :cond_3
    iput v2, p0, Lmtq;->a:I

    goto/16 :goto_0

    .line 4175
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtq;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 4179
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtq;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 4183
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4184
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    const/16 v1, 0xa6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x20

    if-eq v0, v1, :cond_4

    const/16 v1, 0x21

    if-eq v0, v1, :cond_4

    const/16 v1, 0x22

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/16 v1, 0x23

    if-eq v0, v1, :cond_4

    const/16 v1, 0x10

    if-eq v0, v1, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd2

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0x24

    if-eq v0, v1, :cond_4

    const/16 v1, 0x25

    if-eq v0, v1, :cond_4

    const/16 v1, 0x26

    if-eq v0, v1, :cond_4

    const/16 v1, 0x27

    if-eq v0, v1, :cond_4

    const/16 v1, 0x28

    if-eq v0, v1, :cond_4

    const/16 v1, 0x29

    if-eq v0, v1, :cond_4

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x11

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe1

    if-eq v0, v1, :cond_4

    const/16 v1, 0x2b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x30

    if-eq v0, v1, :cond_4

    const/16 v1, 0x31

    if-eq v0, v1, :cond_4

    const/16 v1, 0x32

    if-eq v0, v1, :cond_4

    const/16 v1, 0x72

    if-eq v0, v1, :cond_4

    const/16 v1, 0x93

    if-eq v0, v1, :cond_4

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x95

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_4

    const/16 v1, 0x33

    if-eq v0, v1, :cond_4

    const/16 v1, 0x34

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xaa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xab

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd8

    if-eq v0, v1, :cond_4

    const/16 v1, 0xac

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb5

    if-eq v0, v1, :cond_4

    const/16 v1, 0x35

    if-eq v0, v1, :cond_4

    const/16 v1, 0x13

    if-eq v0, v1, :cond_4

    const/16 v1, 0x36

    if-eq v0, v1, :cond_4

    const/16 v1, 0xf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x37

    if-eq v0, v1, :cond_4

    const/16 v1, 0x38

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb8

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0x39

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb9

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3d

    if-eq v0, v1, :cond_4

    const/16 v1, 0xad

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa5

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x40

    if-eq v0, v1, :cond_4

    const/16 v1, 0x41

    if-eq v0, v1, :cond_4

    const/16 v1, 0x42

    if-eq v0, v1, :cond_4

    const/16 v1, 0x43

    if-eq v0, v1, :cond_4

    const/16 v1, 0x44

    if-eq v0, v1, :cond_4

    const/16 v1, 0x45

    if-eq v0, v1, :cond_4

    const/16 v1, 0x46

    if-eq v0, v1, :cond_4

    const/16 v1, 0xae

    if-eq v0, v1, :cond_4

    const/16 v1, 0x47

    if-eq v0, v1, :cond_4

    const/16 v1, 0x48

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd4

    if-eq v0, v1, :cond_4

    const/16 v1, 0x4e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x4f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x50

    if-eq v0, v1, :cond_4

    const/16 v1, 0x51

    if-eq v0, v1, :cond_4

    const/16 v1, 0x52

    if-eq v0, v1, :cond_4

    const/16 v1, 0x53

    if-eq v0, v1, :cond_4

    const/16 v1, 0x54

    if-eq v0, v1, :cond_4

    const/16 v1, 0x55

    if-eq v0, v1, :cond_4

    const/16 v1, 0x56

    if-eq v0, v1, :cond_4

    const/16 v1, 0x57

    if-eq v0, v1, :cond_4

    const/16 v1, 0x58

    if-eq v0, v1, :cond_4

    const/16 v1, 0xba

    if-eq v0, v1, :cond_4

    const/16 v1, 0xbb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xbc

    if-eq v0, v1, :cond_4

    const/16 v1, 0x59

    if-eq v0, v1, :cond_4

    const/16 v1, 0xaf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x5b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x5c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x5e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x5f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x60

    if-eq v0, v1, :cond_4

    const/16 v1, 0x61

    if-eq v0, v1, :cond_4

    const/16 v1, 0x62

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12

    if-eq v0, v1, :cond_4

    const/16 v1, 0x15

    if-eq v0, v1, :cond_4

    const/16 v1, 0x63

    if-eq v0, v1, :cond_4

    const/16 v1, 0x64

    if-eq v0, v1, :cond_4

    const/16 v1, 0x65

    if-eq v0, v1, :cond_4

    const/16 v1, 0x66

    if-eq v0, v1, :cond_4

    const/16 v1, 0x67

    if-eq v0, v1, :cond_4

    const/16 v1, 0xbd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb0

    if-eq v0, v1, :cond_4

    const/16 v1, 0x68

    if-eq v0, v1, :cond_4

    const/16 v1, 0x69

    if-eq v0, v1, :cond_4

    const/16 v1, 0xbf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb1

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x70

    if-eq v0, v1, :cond_4

    const/16 v1, 0x71

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc0

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe6

    if-eq v0, v1, :cond_4

    const/16 v1, 0x73

    if-eq v0, v1, :cond_4

    const/16 v1, 0x74

    if-eq v0, v1, :cond_4

    const/16 v1, 0x75

    if-eq v0, v1, :cond_4

    const/16 v1, 0x76

    if-eq v0, v1, :cond_4

    const/16 v1, 0x77

    if-eq v0, v1, :cond_4

    const/16 v1, 0x78

    if-eq v0, v1, :cond_4

    const/16 v1, 0x79

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x49

    if-eq v0, v1, :cond_4

    const/16 v1, 0x4a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x4b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x4c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x4d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x91

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x14

    if-eq v0, v1, :cond_4

    const/16 v1, 0x80

    if-eq v0, v1, :cond_4

    const/16 v1, 0x81

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd0

    if-eq v0, v1, :cond_4

    const/16 v1, 0x82

    if-eq v0, v1, :cond_4

    const/16 v1, 0x83

    if-eq v0, v1, :cond_4

    const/16 v1, 0x84

    if-eq v0, v1, :cond_4

    const/16 v1, 0x85

    if-eq v0, v1, :cond_4

    const/16 v1, 0x86

    if-eq v0, v1, :cond_4

    const/16 v1, 0x87

    if-eq v0, v1, :cond_4

    const/16 v1, 0x88

    if-eq v0, v1, :cond_4

    const/16 v1, 0x89

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x90

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc1

    if-eq v0, v1, :cond_4

    const/16 v1, 0x19

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x92

    if-eq v0, v1, :cond_4

    const/16 v1, 0x94

    if-eq v0, v1, :cond_4

    const/16 v1, 0x97

    if-eq v0, v1, :cond_4

    const/16 v1, 0x98

    if-eq v0, v1, :cond_4

    const/16 v1, 0x99

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb3

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa8

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xca

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xce

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xda

    if-eq v0, v1, :cond_4

    const/16 v1, 0xdb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xdd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xde

    if-eq v0, v1, :cond_4

    const/16 v1, 0xdf

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe0

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xea

    if-eq v0, v1, :cond_4

    const/16 v1, 0xeb

    if-eq v0, v1, :cond_4

    const/16 v1, 0x16

    if-eq v0, v1, :cond_4

    const/16 v1, 0x17

    if-eq v0, v1, :cond_4

    const/16 v1, 0x18

    if-ne v0, v1, :cond_5

    .line 4418
    :cond_4
    iput v0, p0, Lmtq;->b:I

    goto/16 :goto_0

    .line 4420
    :cond_5
    iput v2, p0, Lmtq;->b:I

    goto/16 :goto_0

    .line 3862
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 3816
    iget v0, p0, Lmtq;->a:I

    if-eq v0, v2, :cond_0

    .line 3817
    const/4 v0, 0x1

    iget v1, p0, Lmtq;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3819
    :cond_0
    iget-object v0, p0, Lmtq;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3820
    const/4 v0, 0x2

    iget-object v1, p0, Lmtq;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3822
    :cond_1
    iget-object v0, p0, Lmtq;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 3823
    const/4 v0, 0x3

    iget-object v1, p0, Lmtq;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3825
    :cond_2
    iget v0, p0, Lmtq;->b:I

    if-eq v0, v2, :cond_3

    .line 3826
    const/4 v0, 0x4

    iget v1, p0, Lmtq;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3828
    :cond_3
    iget-object v0, p0, Lmtq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3830
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3797
    invoke-virtual {p0, p1}, Lmtq;->a(Loxn;)Lmtq;

    move-result-object v0

    return-object v0
.end method
