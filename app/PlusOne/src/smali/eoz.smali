.class final Leoz;
.super Landroid/app/SharedElementCallback;
.source "PG"


# instance fields
.field private synthetic a:Landroid/content/res/Resources;

.field private synthetic b:Z

.field private synthetic c:Landroid/view/Window;

.field private synthetic d:Leoy;


# direct methods
.method constructor <init>(Leoy;Landroid/content/res/Resources;ZLandroid/view/Window;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Leoz;->d:Leoy;

    iput-object p2, p0, Leoz;->a:Landroid/content/res/Resources;

    iput-boolean p3, p0, Leoz;->b:Z

    iput-object p4, p0, Leoz;->c:Landroid/view/Window;

    invoke-direct {p0}, Landroid/app/SharedElementCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapSharedElements(Ljava/util/List;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 145
    iget-object v0, p0, Leoz;->d:Leoy;

    iget-boolean v0, v0, Leoy;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Leoz;->d:Leoy;

    iget-boolean v0, v0, Leoy;->d:Z

    if-nez v0, :cond_0

    .line 146
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Leoz;->d:Leoy;

    iget-object v1, v1, Leoy;->g:Landroid/view/View;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v0, p0, Leoz;->d:Leoy;

    iget-object v0, v0, Leoy;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Leoz;->d:Leoy;

    iget-object v0, v0, Leoy;->c:Lfba;

    invoke-virtual {v0, v2}, Lfba;->b(Z)V

    .line 150
    :cond_0
    return-void
.end method

.method public onSharedElementEnd(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const v11, 0x7f0d0337

    const v10, 0x7f0d032d

    const/4 v3, 0x0

    .line 107
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 108
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 109
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 110
    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 111
    if-nez v1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v2, p0, Leoz;->a:Landroid/content/res/Resources;

    const v4, 0x7f0d0338

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 116
    iget-object v2, p0, Leoz;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 117
    iget-object v2, p0, Leoz;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 118
    iget-object v7, p0, Leoz;->d:Leoy;

    iget-object v2, v7, Leoy;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x2

    if-ne v2, v9, :cond_2

    const/4 v2, 0x1

    :goto_1
    iget-object v7, v7, Leoy;->a:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    if-eqz v2, :cond_3

    const v2, 0x40633333    # 3.55f

    :goto_2
    div-float v2, v7, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/2addr v2, v7

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int/2addr v2, v7

    .line 120
    iget-boolean v7, p0, Leoz;->b:Z

    if-eqz v7, :cond_4

    .line 121
    iget-object v7, p0, Leoz;->c:Landroid/view/Window;

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 122
    sub-int v8, v7, v6

    sub-int/2addr v8, v4

    invoke-virtual {v0, v8}, Landroid/widget/FrameLayout;->setLeft(I)V

    .line 123
    sub-int/2addr v7, v6

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setRight(I)V

    .line 128
    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setTop(I)V

    .line 129
    add-int v7, v2, v5

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setBottom(I)V

    .line 131
    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v7, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 132
    invoke-virtual {v7, v6}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    .line 133
    iput v2, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 134
    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    invoke-virtual {v1, v3}, Landroid/view/View;->setLeft(I)V

    .line 137
    invoke-virtual {v1, v4}, Landroid/view/View;->setRight(I)V

    .line 138
    invoke-virtual {v1, v3}, Landroid/view/View;->setTop(I)V

    .line 139
    invoke-virtual {v1, v5}, Landroid/view/View;->setBottom(I)V

    goto/16 :goto_0

    :cond_2
    move v2, v3

    .line 118
    goto :goto_1

    :cond_3
    const v2, 0x3fe38e39

    goto :goto_2

    .line 125
    :cond_4
    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setLeft(I)V

    .line 126
    add-int v7, v6, v4

    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setRight(I)V

    goto :goto_3
.end method

.method public onSharedElementStart(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 77
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 78
    invoke-interface {p2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 79
    invoke-interface {p2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 80
    invoke-interface {p3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 81
    if-nez v1, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 86
    iget-object v2, p0, Leoz;->d:Leoy;

    iget-object v2, v2, Leoy;->b:Landroid/content/Context;

    invoke-static {v2}, Lfvf;->a(Landroid/content/Context;)I

    move-result v2

    .line 87
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v3

    .line 88
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v4

    .line 89
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v5

    .line 90
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int v2, v6, v2

    .line 92
    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setLeft(I)V

    .line 93
    add-int/2addr v5, v3

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setRight(I)V

    .line 94
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setTop(I)V

    .line 95
    add-int/2addr v2, v4

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setBottom(I)V

    .line 97
    invoke-virtual {v1, v7}, Landroid/view/View;->setLeft(I)V

    .line 98
    invoke-virtual {v1, v3}, Landroid/view/View;->setRight(I)V

    .line 99
    invoke-virtual {v1, v7}, Landroid/view/View;->setTop(I)V

    .line 100
    invoke-virtual {v1, v4}, Landroid/view/View;->setBottom(I)V

    goto :goto_0
.end method
