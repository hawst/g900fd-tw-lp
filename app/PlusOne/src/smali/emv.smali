.class public final Lemv;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lloj;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private Q:Lhee;

.field private R:Lfcj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lloj;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 68
    const v0, 0x7f0400f6

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 70
    new-instance v0, Lfcj;

    invoke-virtual {p0}, Lemv;->n()Lz;

    move-result-object v2

    invoke-direct {v0, v2, v4}, Lfcj;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lemv;->R:Lfcj;

    .line 71
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 72
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 73
    iget-object v2, p0, Lemv;->R:Lfcj;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    const v0, 0x7f1001db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    const v0, 0x7f1001da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 78
    invoke-virtual {p0}, Lemv;->c()Landroid/app/Dialog;

    move-result-object v0

    const v2, 0x7f0a07d3

    invoke-virtual {p0, v2}, Lemv;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {p0}, Lemv;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v3, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 81
    const v0, 0x7f100260

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 83
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 106
    packed-switch p1, :pswitch_data_0

    .line 118
    :cond_0
    :goto_0
    return-object v0

    .line 108
    :pswitch_0
    iget-object v1, p0, Lemv;->Q:Lhee;

    invoke-interface {v1}, Lhee;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    invoke-virtual {p0}, Lemv;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 113
    const-string v1, "plus_one_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 114
    new-instance v0, Lfck;

    iget-object v2, p0, Lemv;->N:Llnl;

    iget-object v3, p0, Lemv;->Q:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v0, v2, v3, v1}, Lfck;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 130
    :goto_0
    return-void

    .line 125
    :pswitch_0
    invoke-virtual {p0}, Lemv;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100260

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 126
    invoke-virtual {p0}, Lemv;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "total_plus_ones"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 127
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 128
    :goto_1
    iget-object v2, p0, Lemv;->R:Lfcj;

    sub-int v0, v1, v0

    invoke-virtual {v2, v0}, Lfcj;->a(I)V

    .line 129
    iget-object v0, p0, Lemv;->R:Lfcj;

    invoke-virtual {v0, p2}, Lfcj;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    .line 127
    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_1

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 31
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lemv;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public aO_()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 88
    invoke-super {p0}, Lloj;->aO_()V

    .line 90
    invoke-virtual {p0}, Lemv;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "restrict_to_domain"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lemv;->Q:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "domain_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lemv;->x()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f100173

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lemv;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0989

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    :cond_0
    return-void
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lemv;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lemv;->Q:Lhee;

    .line 63
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 142
    invoke-virtual {p0}, Lemv;->a()V

    .line 143
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lemv;->R:Lfcj;

    invoke-virtual {v0, p3}, Lfcj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lemv;->R:Lfcj;

    invoke-virtual {v0, p3}, Lfcj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 152
    if-eqz v0, :cond_0

    .line 156
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lemv;->Q:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 158
    invoke-virtual {p0}, Lemv;->n()Lz;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v1, v0, v3}, Leyq;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 160
    invoke-virtual {p0, v0}, Lemv;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
