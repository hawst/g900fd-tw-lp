.class public final Lnbc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:[Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0}, Loxq;-><init>()V

    .line 212
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnbc;->b:[Ljava/lang/String;

    .line 205
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 232
    const/4 v0, 0x1

    iget-object v2, p0, Lnbc;->a:Ljava/lang/Boolean;

    .line 234
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 235
    iget-object v2, p0, Lnbc;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 236
    const/4 v2, 0x2

    iget-object v3, p0, Lnbc;->c:Ljava/lang/String;

    .line 237
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 239
    :cond_0
    iget-object v2, p0, Lnbc;->b:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnbc;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 241
    iget-object v3, p0, Lnbc;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 243
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 241
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    :cond_1
    add-int/2addr v0, v2

    .line 246
    iget-object v1, p0, Lnbc;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 248
    :cond_2
    iget-object v1, p0, Lnbc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    iput v0, p0, Lnbc;->ai:I

    .line 250
    return v0
.end method

.method public a(Loxn;)Lnbc;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 258
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 259
    sparse-switch v0, :sswitch_data_0

    .line 263
    iget-object v1, p0, Lnbc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 264
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbc;->ah:Ljava/util/List;

    .line 267
    :cond_1
    iget-object v1, p0, Lnbc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    :sswitch_0
    return-object p0

    .line 274
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnbc;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 278
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnbc;->c:Ljava/lang/String;

    goto :goto_0

    .line 282
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 283
    iget-object v0, p0, Lnbc;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 284
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 285
    iget-object v2, p0, Lnbc;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 286
    iput-object v1, p0, Lnbc;->b:[Ljava/lang/String;

    .line 287
    :goto_1
    iget-object v1, p0, Lnbc;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 288
    iget-object v1, p0, Lnbc;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 289
    invoke-virtual {p1}, Loxn;->a()I

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 292
    :cond_2
    iget-object v1, p0, Lnbc;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 259
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 217
    const/4 v0, 0x1

    iget-object v1, p0, Lnbc;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 218
    iget-object v0, p0, Lnbc;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 219
    const/4 v0, 0x2

    iget-object v1, p0, Lnbc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 221
    :cond_0
    iget-object v0, p0, Lnbc;->b:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 222
    iget-object v1, p0, Lnbc;->b:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 223
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 222
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_1
    iget-object v0, p0, Lnbc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 228
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0, p1}, Lnbc;->a(Loxn;)Lnbc;

    move-result-object v0

    return-object v0
.end method
