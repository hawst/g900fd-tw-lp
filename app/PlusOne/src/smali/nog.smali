.class public final Lnog;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lnoj;

.field public c:Loge;

.field public d:I

.field public e:Loif;

.field private f:Lnny;

.field private g:Lnoi;

.field private h:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 488
    invoke-direct {p0}, Loxq;-><init>()V

    .line 503
    iput v1, p0, Lnog;->a:I

    .line 506
    iput-object v0, p0, Lnog;->b:Lnoj;

    .line 509
    iput-object v0, p0, Lnog;->f:Lnny;

    .line 512
    iput-object v0, p0, Lnog;->g:Lnoi;

    .line 515
    iput-object v0, p0, Lnog;->c:Loge;

    .line 518
    iput v1, p0, Lnog;->d:I

    .line 521
    iput-object v0, p0, Lnog;->e:Loif;

    .line 488
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 558
    const/4 v0, 0x0

    .line 559
    iget v1, p0, Lnog;->a:I

    if-eq v1, v3, :cond_0

    .line 560
    const/4 v0, 0x1

    iget v1, p0, Lnog;->a:I

    .line 561
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 563
    :cond_0
    iget-object v1, p0, Lnog;->b:Lnoj;

    if-eqz v1, :cond_1

    .line 564
    const/4 v1, 0x2

    iget-object v2, p0, Lnog;->b:Lnoj;

    .line 565
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_1
    iget-object v1, p0, Lnog;->f:Lnny;

    if-eqz v1, :cond_2

    .line 568
    const/4 v1, 0x3

    iget-object v2, p0, Lnog;->f:Lnny;

    .line 569
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_2
    iget-object v1, p0, Lnog;->g:Lnoi;

    if-eqz v1, :cond_3

    .line 572
    const/4 v1, 0x4

    iget-object v2, p0, Lnog;->g:Lnoi;

    .line 573
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_3
    iget-object v1, p0, Lnog;->c:Loge;

    if-eqz v1, :cond_4

    .line 576
    const/4 v1, 0x5

    iget-object v2, p0, Lnog;->c:Loge;

    .line 577
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    :cond_4
    iget v1, p0, Lnog;->d:I

    if-eq v1, v3, :cond_5

    .line 580
    const/4 v1, 0x6

    iget v2, p0, Lnog;->d:I

    .line 581
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_5
    iget-object v1, p0, Lnog;->e:Loif;

    if-eqz v1, :cond_6

    .line 584
    const/4 v1, 0x7

    iget-object v2, p0, Lnog;->e:Loif;

    .line 585
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    :cond_6
    iget-object v1, p0, Lnog;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 588
    const/16 v1, 0x8

    iget-object v2, p0, Lnog;->h:Ljava/lang/Integer;

    .line 589
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 591
    :cond_7
    iget-object v1, p0, Lnog;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 592
    iput v0, p0, Lnog;->ai:I

    .line 593
    return v0
.end method

.method public a(Loxn;)Lnog;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 601
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 602
    sparse-switch v0, :sswitch_data_0

    .line 606
    iget-object v1, p0, Lnog;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 607
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnog;->ah:Ljava/util/List;

    .line 610
    :cond_1
    iget-object v1, p0, Lnog;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 612
    :sswitch_0
    return-object p0

    .line 617
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 618
    if-eqz v0, :cond_2

    if-eq v0, v2, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 622
    :cond_2
    iput v0, p0, Lnog;->a:I

    goto :goto_0

    .line 624
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnog;->a:I

    goto :goto_0

    .line 629
    :sswitch_2
    iget-object v0, p0, Lnog;->b:Lnoj;

    if-nez v0, :cond_4

    .line 630
    new-instance v0, Lnoj;

    invoke-direct {v0}, Lnoj;-><init>()V

    iput-object v0, p0, Lnog;->b:Lnoj;

    .line 632
    :cond_4
    iget-object v0, p0, Lnog;->b:Lnoj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 636
    :sswitch_3
    iget-object v0, p0, Lnog;->f:Lnny;

    if-nez v0, :cond_5

    .line 637
    new-instance v0, Lnny;

    invoke-direct {v0}, Lnny;-><init>()V

    iput-object v0, p0, Lnog;->f:Lnny;

    .line 639
    :cond_5
    iget-object v0, p0, Lnog;->f:Lnny;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 643
    :sswitch_4
    iget-object v0, p0, Lnog;->g:Lnoi;

    if-nez v0, :cond_6

    .line 644
    new-instance v0, Lnoi;

    invoke-direct {v0}, Lnoi;-><init>()V

    iput-object v0, p0, Lnog;->g:Lnoi;

    .line 646
    :cond_6
    iget-object v0, p0, Lnog;->g:Lnoi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 650
    :sswitch_5
    iget-object v0, p0, Lnog;->c:Loge;

    if-nez v0, :cond_7

    .line 651
    new-instance v0, Loge;

    invoke-direct {v0}, Loge;-><init>()V

    iput-object v0, p0, Lnog;->c:Loge;

    .line 653
    :cond_7
    iget-object v0, p0, Lnog;->c:Loge;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 657
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 658
    if-eq v0, v2, :cond_8

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    .line 660
    :cond_8
    iput v0, p0, Lnog;->d:I

    goto :goto_0

    .line 662
    :cond_9
    iput v2, p0, Lnog;->d:I

    goto/16 :goto_0

    .line 667
    :sswitch_7
    iget-object v0, p0, Lnog;->e:Loif;

    if-nez v0, :cond_a

    .line 668
    new-instance v0, Loif;

    invoke-direct {v0}, Loif;-><init>()V

    iput-object v0, p0, Lnog;->e:Loif;

    .line 670
    :cond_a
    iget-object v0, p0, Lnog;->e:Loif;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 674
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnog;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 602
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 528
    iget v0, p0, Lnog;->a:I

    if-eq v0, v2, :cond_0

    .line 529
    const/4 v0, 0x1

    iget v1, p0, Lnog;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 531
    :cond_0
    iget-object v0, p0, Lnog;->b:Lnoj;

    if-eqz v0, :cond_1

    .line 532
    const/4 v0, 0x2

    iget-object v1, p0, Lnog;->b:Lnoj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 534
    :cond_1
    iget-object v0, p0, Lnog;->f:Lnny;

    if-eqz v0, :cond_2

    .line 535
    const/4 v0, 0x3

    iget-object v1, p0, Lnog;->f:Lnny;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 537
    :cond_2
    iget-object v0, p0, Lnog;->g:Lnoi;

    if-eqz v0, :cond_3

    .line 538
    const/4 v0, 0x4

    iget-object v1, p0, Lnog;->g:Lnoi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 540
    :cond_3
    iget-object v0, p0, Lnog;->c:Loge;

    if-eqz v0, :cond_4

    .line 541
    const/4 v0, 0x5

    iget-object v1, p0, Lnog;->c:Loge;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 543
    :cond_4
    iget v0, p0, Lnog;->d:I

    if-eq v0, v2, :cond_5

    .line 544
    const/4 v0, 0x6

    iget v1, p0, Lnog;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 546
    :cond_5
    iget-object v0, p0, Lnog;->e:Loif;

    if-eqz v0, :cond_6

    .line 547
    const/4 v0, 0x7

    iget-object v1, p0, Lnog;->e:Loif;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 549
    :cond_6
    iget-object v0, p0, Lnog;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 550
    const/16 v0, 0x8

    iget-object v1, p0, Lnog;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 552
    :cond_7
    iget-object v0, p0, Lnog;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 554
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 484
    invoke-virtual {p0, p1}, Lnog;->a(Loxn;)Lnog;

    move-result-object v0

    return-object v0
.end method
