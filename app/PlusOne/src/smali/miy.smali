.class public final Lmiy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnnq;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12553
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12556
    iput-object v0, p0, Lmiy;->apiHeader:Llyq;

    .line 12559
    iput-object v0, p0, Lmiy;->a:Lnnq;

    .line 12553
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12576
    const/4 v0, 0x0

    .line 12577
    iget-object v1, p0, Lmiy;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 12578
    const/4 v0, 0x1

    iget-object v1, p0, Lmiy;->apiHeader:Llyq;

    .line 12579
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12581
    :cond_0
    iget-object v1, p0, Lmiy;->a:Lnnq;

    if-eqz v1, :cond_1

    .line 12582
    const/4 v1, 0x2

    iget-object v2, p0, Lmiy;->a:Lnnq;

    .line 12583
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12585
    :cond_1
    iget-object v1, p0, Lmiy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12586
    iput v0, p0, Lmiy;->ai:I

    .line 12587
    return v0
.end method

.method public a(Loxn;)Lmiy;
    .locals 2

    .prologue
    .line 12595
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12596
    sparse-switch v0, :sswitch_data_0

    .line 12600
    iget-object v1, p0, Lmiy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12601
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmiy;->ah:Ljava/util/List;

    .line 12604
    :cond_1
    iget-object v1, p0, Lmiy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12606
    :sswitch_0
    return-object p0

    .line 12611
    :sswitch_1
    iget-object v0, p0, Lmiy;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 12612
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmiy;->apiHeader:Llyq;

    .line 12614
    :cond_2
    iget-object v0, p0, Lmiy;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12618
    :sswitch_2
    iget-object v0, p0, Lmiy;->a:Lnnq;

    if-nez v0, :cond_3

    .line 12619
    new-instance v0, Lnnq;

    invoke-direct {v0}, Lnnq;-><init>()V

    iput-object v0, p0, Lmiy;->a:Lnnq;

    .line 12621
    :cond_3
    iget-object v0, p0, Lmiy;->a:Lnnq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12596
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12564
    iget-object v0, p0, Lmiy;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 12565
    const/4 v0, 0x1

    iget-object v1, p0, Lmiy;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12567
    :cond_0
    iget-object v0, p0, Lmiy;->a:Lnnq;

    if-eqz v0, :cond_1

    .line 12568
    const/4 v0, 0x2

    iget-object v1, p0, Lmiy;->a:Lnnq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12570
    :cond_1
    iget-object v0, p0, Lmiy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12572
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12549
    invoke-virtual {p0, p1}, Lmiy;->a(Loxn;)Lmiy;

    move-result-object v0

    return-object v0
.end method
