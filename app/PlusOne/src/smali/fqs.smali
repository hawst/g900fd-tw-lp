.class final Lfqs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lfqr;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lfqr;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lfqr;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lfoh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 577
    iput p1, p0, Lfqs;->a:I

    .line 578
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfqs;->b:Ljava/util/ArrayList;

    .line 579
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfqs;->c:Ljava/util/ArrayList;

    .line 580
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfqs;->d:Ljava/util/ArrayList;

    .line 581
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfqs;->e:Ljava/util/ArrayList;

    .line 582
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 606
    invoke-direct {p0}, Lfqs;->c()V

    .line 607
    iget-object v0, p0, Lfqs;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lfqs;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 608
    iget-object v0, p0, Lfqs;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 609
    iget-object v0, p0, Lfqs;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lfqs;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 610
    iget-object v0, p0, Lfqs;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 611
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 615
    iget-object v0, p0, Lfqs;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    iget-object v1, p0, Lfqs;->d:Ljava/util/ArrayList;

    new-instance v2, Lfqr;

    iget-object v0, p0, Lfqs;->e:Ljava/util/ArrayList;

    .line 617
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Lfqr;-><init>(Ljava/util/List;)V

    .line 616
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    iget-object v0, p0, Lfqs;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 620
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lfqr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 585
    invoke-direct {p0}, Lfqs;->b()V

    .line 586
    iget-object v0, p0, Lfqs;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Lfoh;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 595
    invoke-virtual {p1}, Lfoh;->a()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p1}, Lfoh;->b()Lmml;

    move-result-object v1

    iget v1, v1, Lmml;->c:I

    if-eq v1, v0, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 596
    iget-object v0, p0, Lfqs;->c:Ljava/util/ArrayList;

    new-instance v1, Lfqr;

    invoke-direct {v1, p1}, Lfqr;-><init>(Lfoh;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 603
    :goto_1
    return-void

    .line 595
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 598
    :cond_2
    iget-object v0, p0, Lfqs;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lfqs;->a:I

    if-ne v0, v1, :cond_3

    .line 599
    invoke-direct {p0}, Lfqs;->c()V

    .line 601
    :cond_3
    iget-object v0, p0, Lfqs;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 590
    invoke-direct {p0}, Lfqs;->b()V

    .line 591
    iget-object v0, p0, Lfqs;->c:Ljava/util/ArrayList;

    new-instance v1, Lfqr;

    invoke-direct {v1, p1}, Lfqr;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    return-void
.end method
