.class public final Lmbg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvf;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4777
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4780
    iput-object v0, p0, Lmbg;->apiHeader:Llyq;

    .line 4783
    iput-object v0, p0, Lmbg;->a:Lnvf;

    .line 4777
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4800
    const/4 v0, 0x0

    .line 4801
    iget-object v1, p0, Lmbg;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 4802
    const/4 v0, 0x1

    iget-object v1, p0, Lmbg;->apiHeader:Llyq;

    .line 4803
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4805
    :cond_0
    iget-object v1, p0, Lmbg;->a:Lnvf;

    if-eqz v1, :cond_1

    .line 4806
    const/4 v1, 0x2

    iget-object v2, p0, Lmbg;->a:Lnvf;

    .line 4807
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4809
    :cond_1
    iget-object v1, p0, Lmbg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4810
    iput v0, p0, Lmbg;->ai:I

    .line 4811
    return v0
.end method

.method public a(Loxn;)Lmbg;
    .locals 2

    .prologue
    .line 4819
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4820
    sparse-switch v0, :sswitch_data_0

    .line 4824
    iget-object v1, p0, Lmbg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4825
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbg;->ah:Ljava/util/List;

    .line 4828
    :cond_1
    iget-object v1, p0, Lmbg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4830
    :sswitch_0
    return-object p0

    .line 4835
    :sswitch_1
    iget-object v0, p0, Lmbg;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 4836
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmbg;->apiHeader:Llyq;

    .line 4838
    :cond_2
    iget-object v0, p0, Lmbg;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4842
    :sswitch_2
    iget-object v0, p0, Lmbg;->a:Lnvf;

    if-nez v0, :cond_3

    .line 4843
    new-instance v0, Lnvf;

    invoke-direct {v0}, Lnvf;-><init>()V

    iput-object v0, p0, Lmbg;->a:Lnvf;

    .line 4845
    :cond_3
    iget-object v0, p0, Lmbg;->a:Lnvf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4820
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4788
    iget-object v0, p0, Lmbg;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 4789
    const/4 v0, 0x1

    iget-object v1, p0, Lmbg;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4791
    :cond_0
    iget-object v0, p0, Lmbg;->a:Lnvf;

    if-eqz v0, :cond_1

    .line 4792
    const/4 v0, 0x2

    iget-object v1, p0, Lmbg;->a:Lnvf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4794
    :cond_1
    iget-object v0, p0, Lmbg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4796
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4773
    invoke-virtual {p0, p1}, Lmbg;->a(Loxn;)Lmbg;

    move-result-object v0

    return-object v0
.end method
