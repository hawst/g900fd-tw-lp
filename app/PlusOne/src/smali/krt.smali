.class public final Lkrt;
.super Llol;
.source "PG"


# instance fields
.field private final N:Lkrv;

.field private final O:Lkru;

.field private P:Lkrs;

.field private Q:Landroid/content/Context;

.field private R:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Llol;-><init>()V

    .line 32
    new-instance v0, Lkrv;

    invoke-direct {v0, p0}, Lkrv;-><init>(Lkrt;)V

    iput-object v0, p0, Lkrt;->N:Lkrv;

    .line 33
    new-instance v0, Lkru;

    invoke-direct {v0, p0}, Lkru;-><init>(Lkrt;)V

    iput-object v0, p0, Lkrt;->O:Lkru;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkrt;->R:Z

    .line 184
    return-void
.end method

.method static synthetic a(Lkrt;Lkrs;)Lkrs;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lkrt;->P:Lkrs;

    return-object p1
.end method

.method static synthetic a(Lkrt;)Lkru;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lkrt;->O:Lkru;

    return-object v0
.end method

.method private a()Lkrw;
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Lkrt;->p()Lae;

    move-result-object v0

    const-string v1, "cast_progress_dialog"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lkrw;

    return-object v0
.end method

.method static synthetic b(Lkrt;)Lkrs;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lkrt;->P:Lkrs;

    return-object v0
.end method

.method static synthetic c(Lkrt;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 29
    iget-boolean v0, p0, Lkrt;->R:Z

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lkrt;->R:Z

    new-instance v0, Lkrw;

    invoke-direct {v0}, Lkrw;-><init>()V

    invoke-virtual {p0}, Lkrt;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a057b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1, v4, v3}, Lkrw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkrw;->f(Landroid/os/Bundle;)V

    invoke-virtual {v0, v3}, Lkrw;->b(Z)V

    invoke-virtual {p0}, Lkrt;->p()Lae;

    move-result-object v1

    const-string v2, "cast_progress_dialog"

    invoke-virtual {v0, v1, v2}, Lkrw;->a(Lae;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lkrt;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkrt;->R:Z

    invoke-direct {p0}, Lkrt;->a()Lkrw;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkrw;->a()V

    :cond_0
    return-void
.end method

.method static synthetic e(Lkrt;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lkrt;->a()Lkrw;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lkrw;->a()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkrt;->R:Z

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lkrt;->Q:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 93
    const v1, 0x7f040072

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 74
    invoke-virtual {p0}, Lkrt;->n()Lz;

    move-result-object v0

    iput-object v0, p0, Lkrt;->Q:Landroid/content/Context;

    .line 75
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lkrt;->n()Lz;

    move-result-object v0

    iput-object v0, p0, Lkrt;->Q:Landroid/content/Context;

    .line 82
    if-eqz p1, :cond_0

    .line 83
    const-string v0, "cast_connecting"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lkrt;->R:Z

    .line 85
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 98
    invoke-super {p0}, Llol;->aO_()V

    .line 99
    iget-object v0, p0, Lkrt;->Q:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lkrt;->n()Lz;

    move-result-object v2

    const-class v3, Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lkrt;->N:Lkrv;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 101
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 115
    const-string v0, "cast_connecting"

    iget-boolean v1, p0, Lkrt;->R:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 117
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lkrt;->P:Lkrs;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lkrt;->P:Lkrs;

    iget-object v1, p0, Lkrt;->O:Lkru;

    invoke-interface {v0, v1}, Lkrs;->b(Lkrl;)V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lkrt;->P:Lkrs;

    .line 109
    :cond_0
    iget-object v0, p0, Lkrt;->Q:Landroid/content/Context;

    iget-object v1, p0, Lkrt;->N:Lkrv;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 110
    invoke-super {p0}, Llol;->z()V

    .line 111
    return-void
.end method
