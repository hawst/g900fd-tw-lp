.class public final Ldne;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmjs;",
        "Lmjt;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ldvq;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILdvq;)V
    .locals 6

    .prologue
    .line 34
    const-string v3, "settingsset"

    new-instance v4, Lmjs;

    invoke-direct {v4}, Lmjs;-><init>()V

    new-instance v5, Lmjt;

    invoke-direct {v5}, Lmjt;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 37
    iput-object p3, p0, Ldne;->a:Ldvq;

    .line 38
    return-void
.end method


# virtual methods
.method protected a(Lmjs;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 42
    .line 43
    iget-object v0, p0, Ldne;->a:Ldvq;

    invoke-virtual {v0}, Ldvq;->c()I

    move-result v5

    move v0, v2

    move v3, v2

    .line 44
    :goto_0
    if-ge v0, v5, :cond_0

    .line 45
    iget-object v4, p0, Ldne;->a:Ldvq;

    invoke-virtual {v4, v0}, Ldvq;->a(I)Ldvo;

    move-result-object v4

    .line 46
    invoke-virtual {v4}, Ldvo;->b()I

    move-result v4

    add-int/2addr v3, v4

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    new-array v6, v3, [Lnoy;

    move v4, v2

    move v0, v2

    .line 51
    :goto_1
    if-ge v4, v5, :cond_2

    .line 52
    iget-object v3, p0, Ldne;->a:Ldvq;

    invoke-virtual {v3, v4}, Ldvq;->a(I)Ldvo;

    move-result-object v7

    .line 53
    invoke-virtual {v7}, Ldvo;->b()I

    move-result v8

    move v3, v0

    move v0, v2

    .line 54
    :goto_2
    if-ge v0, v8, :cond_1

    .line 55
    invoke-virtual {v7, v0}, Ldvo;->a(I)Lnoy;

    move-result-object v9

    aput-object v9, v6, v3

    .line 56
    add-int/lit8 v3, v3, 0x1

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 51
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v0, v3

    goto :goto_1

    .line 60
    :cond_2
    new-instance v0, Lnox;

    invoke-direct {v0}, Lnox;-><init>()V

    .line 61
    iput-object v6, v0, Lnox;->a:[Lnoy;

    .line 62
    iget-object v2, p0, Ldne;->a:Ldvq;

    invoke-virtual {v2}, Ldvq;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lnox;->b:Ljava/lang/String;

    .line 64
    new-instance v2, Lnou;

    invoke-direct {v2}, Lnou;-><init>()V

    .line 65
    const/4 v3, 0x2

    iput v3, v2, Lnou;->a:I

    .line 67
    new-instance v3, Lnpf;

    invoke-direct {v3}, Lnpf;-><init>()V

    .line 68
    iput-object v0, v3, Lnpf;->a:Lnox;

    .line 69
    iput-object v2, v3, Lnpf;->b:Lnou;

    .line 72
    iget-object v0, p0, Ldne;->a:Ldvq;

    invoke-virtual {v0}, Ldvq;->e()Lhgw;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_4

    .line 74
    new-instance v2, Lnpk;

    invoke-direct {v2}, Lnpk;-><init>()V

    .line 75
    invoke-virtual {v0}, Lhgw;->m()Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v1

    .line 78
    :cond_3
    invoke-static {v0, v1}, Lhft;->a(Lhgw;Lhgw;)Lock;

    move-result-object v0

    iput-object v0, v2, Lnpk;->b:Lock;

    .line 80
    iput-object v2, v3, Lnpf;->c:Lnpk;

    .line 83
    :cond_4
    new-instance v0, Lnpt;

    invoke-direct {v0}, Lnpt;-><init>()V

    iput-object v0, p1, Lmjs;->a:Lnpt;

    .line 84
    iget-object v0, p1, Lmjs;->a:Lnpt;

    .line 85
    iput-object v3, v0, Lnpt;->a:Lnpf;

    .line 86
    return-void
.end method

.method protected a(Lmjt;)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p1, Lmjt;->a:Lnpn;

    .line 91
    iget-object v1, v0, Lnpn;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lnpn;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lkfm;

    const-string v1, "SettingsSetRequest failed"

    invoke-direct {v0, v1}, Lkfm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lmjs;

    invoke-virtual {p0, p1}, Ldne;->a(Lmjs;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lmjt;

    invoke-virtual {p0, p1}, Ldne;->a(Lmjt;)V

    return-void
.end method
