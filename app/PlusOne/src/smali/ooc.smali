.class public final Looc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Looc;


# instance fields
.field private b:I

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x0

    new-array v0, v0, [Looc;

    sput-object v0, Looc;->a:[Looc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 420
    invoke-direct {p0}, Loxq;-><init>()V

    .line 429
    const/high16 v0, -0x80000000

    iput v0, p0, Looc;->b:I

    .line 420
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 453
    const/4 v0, 0x0

    .line 454
    iget v1, p0, Looc;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 455
    const/4 v0, 0x1

    iget v1, p0, Looc;->b:I

    .line 456
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 458
    :cond_0
    iget-object v1, p0, Looc;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 459
    const/4 v1, 0x2

    iget-object v2, p0, Looc;->c:Ljava/lang/Float;

    .line 460
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 462
    :cond_1
    iget-object v1, p0, Looc;->d:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 463
    const/4 v1, 0x3

    iget-object v2, p0, Looc;->d:Ljava/lang/Float;

    .line 464
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 466
    :cond_2
    iget-object v1, p0, Looc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    iput v0, p0, Looc;->ai:I

    .line 468
    return v0
.end method

.method public a(Loxn;)Looc;
    .locals 2

    .prologue
    .line 476
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 477
    sparse-switch v0, :sswitch_data_0

    .line 481
    iget-object v1, p0, Looc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 482
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Looc;->ah:Ljava/util/List;

    .line 485
    :cond_1
    iget-object v1, p0, Looc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 487
    :sswitch_0
    return-object p0

    .line 492
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 493
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 496
    :cond_2
    iput v0, p0, Looc;->b:I

    goto :goto_0

    .line 498
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Looc;->b:I

    goto :goto_0

    .line 503
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Looc;->c:Ljava/lang/Float;

    goto :goto_0

    .line 507
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Looc;->d:Ljava/lang/Float;

    goto :goto_0

    .line 477
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 438
    iget v0, p0, Looc;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 439
    const/4 v0, 0x1

    iget v1, p0, Looc;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 441
    :cond_0
    iget-object v0, p0, Looc;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 442
    const/4 v0, 0x2

    iget-object v1, p0, Looc;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 444
    :cond_1
    iget-object v0, p0, Looc;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 445
    const/4 v0, 0x3

    iget-object v1, p0, Looc;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 447
    :cond_2
    iget-object v0, p0, Looc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 449
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 416
    invoke-virtual {p0, p1}, Looc;->a(Loxn;)Looc;

    move-result-object v0

    return-object v0
.end method
