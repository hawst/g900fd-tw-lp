.class public final Lnyc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5766
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5812
    const/4 v0, 0x0

    .line 5813
    iget-object v1, p0, Lnyc;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 5814
    const/4 v0, 0x1

    iget-object v1, p0, Lnyc;->a:Ljava/lang/Integer;

    .line 5815
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5817
    :cond_0
    iget-object v1, p0, Lnyc;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 5818
    const/4 v1, 0x2

    iget-object v2, p0, Lnyc;->b:Ljava/lang/Integer;

    .line 5819
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5821
    :cond_1
    iget-object v1, p0, Lnyc;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 5822
    const/4 v1, 0x3

    iget-object v2, p0, Lnyc;->c:Ljava/lang/Integer;

    .line 5823
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5825
    :cond_2
    iget-object v1, p0, Lnyc;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 5826
    const/4 v1, 0x4

    iget-object v2, p0, Lnyc;->d:Ljava/lang/Integer;

    .line 5827
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5829
    :cond_3
    iget-object v1, p0, Lnyc;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 5830
    const/4 v1, 0x5

    iget-object v2, p0, Lnyc;->e:Ljava/lang/Integer;

    .line 5831
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5833
    :cond_4
    iget-object v1, p0, Lnyc;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 5834
    const/4 v1, 0x6

    iget-object v2, p0, Lnyc;->f:Ljava/lang/Integer;

    .line 5835
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5837
    :cond_5
    iget-object v1, p0, Lnyc;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 5838
    const/4 v1, 0x7

    iget-object v2, p0, Lnyc;->g:Ljava/lang/Integer;

    .line 5839
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5841
    :cond_6
    iget-object v1, p0, Lnyc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5842
    iput v0, p0, Lnyc;->ai:I

    .line 5843
    return v0
.end method

.method public a(Loxn;)Lnyc;
    .locals 2

    .prologue
    .line 5851
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5852
    sparse-switch v0, :sswitch_data_0

    .line 5856
    iget-object v1, p0, Lnyc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5857
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyc;->ah:Ljava/util/List;

    .line 5860
    :cond_1
    iget-object v1, p0, Lnyc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5862
    :sswitch_0
    return-object p0

    .line 5867
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyc;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 5871
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyc;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 5875
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyc;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 5879
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyc;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 5883
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyc;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 5887
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyc;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 5891
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyc;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 5852
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5785
    iget-object v0, p0, Lnyc;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5786
    const/4 v0, 0x1

    iget-object v1, p0, Lnyc;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5788
    :cond_0
    iget-object v0, p0, Lnyc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 5789
    const/4 v0, 0x2

    iget-object v1, p0, Lnyc;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5791
    :cond_1
    iget-object v0, p0, Lnyc;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 5792
    const/4 v0, 0x3

    iget-object v1, p0, Lnyc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5794
    :cond_2
    iget-object v0, p0, Lnyc;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 5795
    const/4 v0, 0x4

    iget-object v1, p0, Lnyc;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5797
    :cond_3
    iget-object v0, p0, Lnyc;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 5798
    const/4 v0, 0x5

    iget-object v1, p0, Lnyc;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5800
    :cond_4
    iget-object v0, p0, Lnyc;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 5801
    const/4 v0, 0x6

    iget-object v1, p0, Lnyc;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5803
    :cond_5
    iget-object v0, p0, Lnyc;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 5804
    const/4 v0, 0x7

    iget-object v1, p0, Lnyc;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5806
    :cond_6
    iget-object v0, p0, Lnyc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5808
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5762
    invoke-virtual {p0, p1}, Lnyc;->a(Loxn;)Lnyc;

    move-result-object v0

    return-object v0
.end method
