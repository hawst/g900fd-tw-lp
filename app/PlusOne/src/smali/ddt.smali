.class public final enum Lddt;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lddt;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lddt;

.field public static final enum b:Lddt;

.field public static final enum c:Lddt;

.field public static final enum d:Lddt;

.field private static final synthetic e:[Lddt;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, Lddt;

    const-string v1, "Original"

    invoke-direct {v0, v1, v2}, Lddt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddt;->a:Lddt;

    new-instance v0, Lddt;

    const-string v1, "Unfiltered"

    invoke-direct {v0, v1, v3}, Lddt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddt;->b:Lddt;

    new-instance v0, Lddt;

    const-string v1, "UnfilteredWithTransform"

    invoke-direct {v0, v1, v4}, Lddt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddt;->c:Lddt;

    new-instance v0, Lddt;

    const-string v1, "Comparable"

    invoke-direct {v0, v1, v5}, Lddt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddt;->d:Lddt;

    .line 62
    const/4 v0, 0x4

    new-array v0, v0, [Lddt;

    sget-object v1, Lddt;->a:Lddt;

    aput-object v1, v0, v2

    sget-object v1, Lddt;->b:Lddt;

    aput-object v1, v0, v3

    sget-object v1, Lddt;->c:Lddt;

    aput-object v1, v0, v4

    sget-object v1, Lddt;->d:Lddt;

    aput-object v1, v0, v5

    sput-object v0, Lddt;->e:[Lddt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lddt;
    .locals 1

    .prologue
    .line 62
    const-class v0, Lddt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lddt;

    return-object v0
.end method

.method public static values()[Lddt;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lddt;->e:[Lddt;

    invoke-virtual {v0}, [Lddt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lddt;

    return-object v0
.end method
