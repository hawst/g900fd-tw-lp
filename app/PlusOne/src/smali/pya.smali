.class public final Lpya;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:[Lpyb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Loxq;-><init>()V

    .line 197
    const/high16 v0, -0x80000000

    iput v0, p0, Lpya;->a:I

    .line 200
    sget-object v0, Lpyb;->a:[Lpyb;

    iput-object v0, p0, Lpya;->b:[Lpyb;

    .line 108
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 221
    .line 222
    iget v0, p0, Lpya;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_2

    .line 223
    const/4 v0, 0x1

    iget v2, p0, Lpya;->a:I

    .line 224
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 226
    :goto_0
    iget-object v2, p0, Lpya;->b:[Lpyb;

    if-eqz v2, :cond_1

    .line 227
    iget-object v2, p0, Lpya;->b:[Lpyb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 228
    if-eqz v4, :cond_0

    .line 229
    const/4 v5, 0x2

    .line 230
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 227
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 234
    :cond_1
    iget-object v1, p0, Lpya;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    iput v0, p0, Lpya;->ai:I

    .line 236
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpya;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 244
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 245
    sparse-switch v0, :sswitch_data_0

    .line 249
    iget-object v2, p0, Lpya;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 250
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpya;->ah:Ljava/util/List;

    .line 253
    :cond_1
    iget-object v2, p0, Lpya;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    :sswitch_0
    return-object p0

    .line 260
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 261
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 263
    :cond_2
    iput v0, p0, Lpya;->a:I

    goto :goto_0

    .line 265
    :cond_3
    iput v1, p0, Lpya;->a:I

    goto :goto_0

    .line 270
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 271
    iget-object v0, p0, Lpya;->b:[Lpyb;

    if-nez v0, :cond_5

    move v0, v1

    .line 272
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpyb;

    .line 273
    iget-object v3, p0, Lpya;->b:[Lpyb;

    if-eqz v3, :cond_4

    .line 274
    iget-object v3, p0, Lpya;->b:[Lpyb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 276
    :cond_4
    iput-object v2, p0, Lpya;->b:[Lpyb;

    .line 277
    :goto_2
    iget-object v2, p0, Lpya;->b:[Lpyb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 278
    iget-object v2, p0, Lpya;->b:[Lpyb;

    new-instance v3, Lpyb;

    invoke-direct {v3}, Lpyb;-><init>()V

    aput-object v3, v2, v0

    .line 279
    iget-object v2, p0, Lpya;->b:[Lpyb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 280
    invoke-virtual {p1}, Loxn;->a()I

    .line 277
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 271
    :cond_5
    iget-object v0, p0, Lpya;->b:[Lpyb;

    array-length v0, v0

    goto :goto_1

    .line 283
    :cond_6
    iget-object v2, p0, Lpya;->b:[Lpyb;

    new-instance v3, Lpyb;

    invoke-direct {v3}, Lpyb;-><init>()V

    aput-object v3, v2, v0

    .line 284
    iget-object v2, p0, Lpya;->b:[Lpyb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 245
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 205
    iget v0, p0, Lpya;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 206
    const/4 v0, 0x1

    iget v1, p0, Lpya;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 208
    :cond_0
    iget-object v0, p0, Lpya;->b:[Lpyb;

    if-eqz v0, :cond_2

    .line 209
    iget-object v1, p0, Lpya;->b:[Lpyb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 210
    if-eqz v3, :cond_1

    .line 211
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 209
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    :cond_2
    iget-object v0, p0, Lpya;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 217
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lpya;->a(Loxn;)Lpya;

    move-result-object v0

    return-object v0
.end method
