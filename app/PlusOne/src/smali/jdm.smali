.class final Ljdm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:I

.field private h:Ljdm;

.field private i:Ljava/lang/String;

.field private j:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/String;)Ljdm;
    .locals 4

    .prologue
    .line 69
    const-class v1, Ljdm;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljdk;->a:Ljdm;

    .line 70
    if-nez v0, :cond_0

    .line 71
    new-instance v0, Ljdm;

    invoke-direct {v0}, Ljdm;-><init>()V

    .line 75
    :goto_0
    iput-object p0, v0, Ljdm;->i:Ljava/lang/String;

    .line 76
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Ljdm;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit v1

    return-object v0

    .line 73
    :cond_0
    :try_start_1
    iget-object v2, v0, Ljdm;->h:Ljdm;

    sput-object v2, Ljdk;->a:Ljdm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Ljdm;)V
    .locals 2

    .prologue
    .line 81
    const-class v1, Ljdm;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljdk;->a:Ljdm;

    iput-object v0, p0, Ljdm;->h:Ljdm;

    .line 82
    sput-object p0, Ljdk;->a:Ljdm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit v1

    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Ljdm;->i:Ljava/lang/String;

    .line 87
    iput v1, p0, Ljdm;->a:I

    .line 88
    iput v1, p0, Ljdm;->j:I

    .line 89
    iput-wide v2, p0, Ljdm;->b:J

    .line 90
    iput-wide v2, p0, Ljdm;->c:J

    .line 91
    iput-wide v2, p0, Ljdm;->f:J

    .line 92
    iput v1, p0, Ljdm;->g:I

    .line 93
    invoke-static {p0}, Ljdm;->a(Ljdm;)V

    .line 94
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 106
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    const-string v0, "["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ljdm;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget v0, p0, Ljdm;->a:I

    if-eqz v0, :cond_0

    .line 110
    const-string v0, " query-result:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Ljdm;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget v0, p0, Ljdm;->j:I

    if-eqz v0, :cond_1

    .line 114
    const-string v0, " update:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Ljdm;->j:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 117
    :cond_1
    iget-wide v2, p0, Ljdm;->b:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_2

    .line 118
    const-string v0, " in:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljdm;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 121
    :cond_2
    iget-wide v2, p0, Ljdm;->c:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_3

    .line 122
    const-string v0, " out:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljdm;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 125
    :cond_3
    iget-wide v2, p0, Ljdm;->f:J

    cmp-long v0, v2, v6

    if-lez v0, :cond_4

    .line 126
    const-string v0, " net-time:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljdm;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 129
    :cond_4
    iget v0, p0, Ljdm;->g:I

    const/4 v2, 0x1

    if-le v0, v2, :cond_5

    .line 130
    const-string v0, " net-op:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Ljdm;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 133
    :cond_5
    iget-wide v2, p0, Ljdm;->e:J

    iget-wide v4, p0, Ljdm;->d:J

    sub-long/2addr v2, v4

    .line 134
    cmp-long v0, v2, v6

    if-lez v0, :cond_6

    .line 135
    const-string v0, " time:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 138
    :cond_6
    if-eqz p1, :cond_7

    .line 139
    const-string v2, " report:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_7
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 139
    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljdm;)V
    .locals 4

    .prologue
    .line 97
    iget v0, p0, Ljdm;->a:I

    iget v1, p1, Ljdm;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Ljdm;->a:I

    .line 98
    iget v0, p0, Ljdm;->j:I

    iget v1, p1, Ljdm;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Ljdm;->j:I

    .line 99
    iget-wide v0, p0, Ljdm;->b:J

    iget-wide v2, p1, Ljdm;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ljdm;->b:J

    .line 100
    iget-wide v0, p0, Ljdm;->c:J

    iget-wide v2, p1, Ljdm;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ljdm;->c:J

    .line 101
    iget-wide v0, p0, Ljdm;->f:J

    iget-wide v2, p1, Ljdm;->f:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ljdm;->f:J

    .line 102
    iget v0, p0, Ljdm;->g:I

    iget v1, p1, Ljdm;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Ljdm;->g:I

    .line 103
    return-void
.end method
