.class public final Lncf;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Lnxj;

.field private c:Lncg;

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-direct {p0}, Loxq;-><init>()V

    .line 251
    const/high16 v0, -0x80000000

    iput v0, p0, Lncf;->a:I

    .line 254
    iput-object v1, p0, Lncf;->b:Lnxj;

    .line 257
    iput-object v1, p0, Lncf;->c:Lncg;

    .line 150
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 282
    const/4 v0, 0x0

    .line 283
    iget v1, p0, Lncf;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 284
    const/4 v0, 0x1

    iget v1, p0, Lncf;->a:I

    .line 285
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 287
    :cond_0
    iget-object v1, p0, Lncf;->b:Lnxj;

    if-eqz v1, :cond_1

    .line 288
    const/4 v1, 0x2

    iget-object v2, p0, Lncf;->b:Lnxj;

    .line 289
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_1
    iget-object v1, p0, Lncf;->c:Lncg;

    if-eqz v1, :cond_2

    .line 292
    const/4 v1, 0x3

    iget-object v2, p0, Lncf;->c:Lncg;

    .line 293
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_2
    iget-object v1, p0, Lncf;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 296
    const/4 v1, 0x4

    iget-object v2, p0, Lncf;->d:Ljava/lang/Boolean;

    .line 297
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 299
    :cond_3
    iget-object v1, p0, Lncf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 300
    iput v0, p0, Lncf;->ai:I

    .line 301
    return v0
.end method

.method public a(Loxn;)Lncf;
    .locals 2

    .prologue
    .line 309
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 310
    sparse-switch v0, :sswitch_data_0

    .line 314
    iget-object v1, p0, Lncf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 315
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lncf;->ah:Ljava/util/List;

    .line 318
    :cond_1
    iget-object v1, p0, Lncf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    :sswitch_0
    return-object p0

    .line 325
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 326
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 330
    :cond_2
    iput v0, p0, Lncf;->a:I

    goto :goto_0

    .line 332
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lncf;->a:I

    goto :goto_0

    .line 337
    :sswitch_2
    iget-object v0, p0, Lncf;->b:Lnxj;

    if-nez v0, :cond_4

    .line 338
    new-instance v0, Lnxj;

    invoke-direct {v0}, Lnxj;-><init>()V

    iput-object v0, p0, Lncf;->b:Lnxj;

    .line 340
    :cond_4
    iget-object v0, p0, Lncf;->b:Lnxj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 344
    :sswitch_3
    iget-object v0, p0, Lncf;->c:Lncg;

    if-nez v0, :cond_5

    .line 345
    new-instance v0, Lncg;

    invoke-direct {v0}, Lncg;-><init>()V

    iput-object v0, p0, Lncf;->c:Lncg;

    .line 347
    :cond_5
    iget-object v0, p0, Lncf;->c:Lncg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 351
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lncf;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 310
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 264
    iget v0, p0, Lncf;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 265
    const/4 v0, 0x1

    iget v1, p0, Lncf;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 267
    :cond_0
    iget-object v0, p0, Lncf;->b:Lnxj;

    if-eqz v0, :cond_1

    .line 268
    const/4 v0, 0x2

    iget-object v1, p0, Lncf;->b:Lnxj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 270
    :cond_1
    iget-object v0, p0, Lncf;->c:Lncg;

    if-eqz v0, :cond_2

    .line 271
    const/4 v0, 0x3

    iget-object v1, p0, Lncf;->c:Lncg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 273
    :cond_2
    iget-object v0, p0, Lncf;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 274
    const/4 v0, 0x4

    iget-object v1, p0, Lncf;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 276
    :cond_3
    iget-object v0, p0, Lncf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 278
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0, p1}, Lncf;->a(Loxn;)Lncf;

    move-result-object v0

    return-object v0
.end method
