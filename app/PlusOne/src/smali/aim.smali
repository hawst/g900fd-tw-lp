.class public Laim;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahx;


# instance fields
.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Lahx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Laim;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lahx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "backgroundExecutor"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Laim;->c:Ljava/util/concurrent/Executor;

    .line 39
    const-string v0, "mainExecutor"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Laim;->d:Ljava/util/concurrent/Executor;

    .line 40
    const-string v0, "analyzer"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahx;

    iput-object v0, p0, Laim;->e:Lahx;

    .line 41
    return-void
.end method

.method static synthetic a(Laim;)Lahx;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Laim;->e:Lahx;

    return-object v0
.end method

.method static synthetic b(Laim;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Laim;->d:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Laim;->e:Lahx;

    invoke-interface {v0}, Lahx;->a()V

    .line 78
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lahy;)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Laim;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lain;

    invoke-direct {v1, p0, p1, p2, p3}, Lain;-><init>(Laim;Landroid/net/Uri;Laic;Lahy;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 57
    return-void
.end method

.method public b(Landroid/net/Uri;Laic;Lahy;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Laim;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Laio;

    invoke-direct {v1, p0, p1, p2, p3}, Laio;-><init>(Laim;Landroid/net/Uri;Laic;Lahy;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 73
    return-void
.end method
