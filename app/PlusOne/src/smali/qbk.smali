.class public final Lqbk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lqbk;


# instance fields
.field public b:I

.field public c:[Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;

.field public i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1993
    const/4 v0, 0x0

    new-array v0, v0, [Lqbk;

    sput-object v0, Lqbk;->a:[Lqbk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 1994
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1997
    iput v1, p0, Lqbk;->b:I

    .line 2000
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqbk;->c:[Ljava/lang/String;

    .line 2013
    iput v1, p0, Lqbk;->i:I

    .line 1994
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    const/4 v1, 0x0

    .line 2050
    .line 2051
    iget v0, p0, Lqbk;->b:I

    if-eq v0, v6, :cond_8

    .line 2052
    const/4 v0, 0x1

    iget v2, p0, Lqbk;->b:I

    .line 2053
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2055
    :goto_0
    iget-object v2, p0, Lqbk;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lqbk;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 2057
    iget-object v3, p0, Lqbk;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 2059
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 2057
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2061
    :cond_0
    add-int/2addr v0, v2

    .line 2062
    iget-object v1, p0, Lqbk;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2064
    :cond_1
    iget-object v1, p0, Lqbk;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2065
    const/4 v1, 0x3

    iget-object v2, p0, Lqbk;->d:Ljava/lang/Integer;

    .line 2066
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2068
    :cond_2
    iget-object v1, p0, Lqbk;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2069
    const/4 v1, 0x4

    iget-object v2, p0, Lqbk;->e:Ljava/lang/Integer;

    .line 2070
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2072
    :cond_3
    iget-object v1, p0, Lqbk;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 2073
    const/4 v1, 0x5

    iget-object v2, p0, Lqbk;->f:Ljava/lang/Integer;

    .line 2074
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2076
    :cond_4
    iget-object v1, p0, Lqbk;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 2077
    const/4 v1, 0x6

    iget-object v2, p0, Lqbk;->g:Ljava/lang/Integer;

    .line 2078
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2080
    :cond_5
    iget-object v1, p0, Lqbk;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 2081
    const/4 v1, 0x7

    iget-object v2, p0, Lqbk;->h:Ljava/lang/Integer;

    .line 2082
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2084
    :cond_6
    iget v1, p0, Lqbk;->i:I

    if-eq v1, v6, :cond_7

    .line 2085
    const/16 v1, 0x8

    iget v2, p0, Lqbk;->i:I

    .line 2086
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2088
    :cond_7
    iget-object v1, p0, Lqbk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2089
    iput v0, p0, Lqbk;->ai:I

    .line 2090
    return v0

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lqbk;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2098
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2099
    sparse-switch v0, :sswitch_data_0

    .line 2103
    iget-object v1, p0, Lqbk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2104
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbk;->ah:Ljava/util/List;

    .line 2107
    :cond_1
    iget-object v1, p0, Lqbk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2109
    :sswitch_0
    return-object p0

    .line 2114
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2115
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-eq v0, v7, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 2121
    :cond_2
    iput v0, p0, Lqbk;->b:I

    goto :goto_0

    .line 2123
    :cond_3
    iput v3, p0, Lqbk;->b:I

    goto :goto_0

    .line 2128
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 2129
    iget-object v0, p0, Lqbk;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 2130
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 2131
    iget-object v2, p0, Lqbk;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2132
    iput-object v1, p0, Lqbk;->c:[Ljava/lang/String;

    .line 2133
    :goto_1
    iget-object v1, p0, Lqbk;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 2134
    iget-object v1, p0, Lqbk;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2135
    invoke-virtual {p1}, Loxn;->a()I

    .line 2133
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2138
    :cond_4
    iget-object v1, p0, Lqbk;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 2142
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbk;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2146
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbk;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 2150
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbk;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2154
    :sswitch_6
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbk;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2158
    :sswitch_7
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbk;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2162
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2163
    if-eqz v0, :cond_5

    if-eq v0, v4, :cond_5

    if-eq v0, v5, :cond_5

    if-eq v0, v6, :cond_5

    if-eq v0, v7, :cond_5

    const/4 v1, 0x5

    if-eq v0, v1, :cond_5

    const/4 v1, 0x6

    if-ne v0, v1, :cond_6

    .line 2170
    :cond_5
    iput v0, p0, Lqbk;->i:I

    goto/16 :goto_0

    .line 2172
    :cond_6
    iput v3, p0, Lqbk;->i:I

    goto/16 :goto_0

    .line 2099
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 2018
    iget v0, p0, Lqbk;->b:I

    if-eq v0, v5, :cond_0

    .line 2019
    const/4 v0, 0x1

    iget v1, p0, Lqbk;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2021
    :cond_0
    iget-object v0, p0, Lqbk;->c:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2022
    iget-object v1, p0, Lqbk;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2023
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 2022
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2026
    :cond_1
    iget-object v0, p0, Lqbk;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2027
    const/4 v0, 0x3

    iget-object v1, p0, Lqbk;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2029
    :cond_2
    iget-object v0, p0, Lqbk;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2030
    const/4 v0, 0x4

    iget-object v1, p0, Lqbk;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2032
    :cond_3
    iget-object v0, p0, Lqbk;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2033
    const/4 v0, 0x5

    iget-object v1, p0, Lqbk;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2035
    :cond_4
    iget-object v0, p0, Lqbk;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 2036
    const/4 v0, 0x6

    iget-object v1, p0, Lqbk;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 2038
    :cond_5
    iget-object v0, p0, Lqbk;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 2039
    const/4 v0, 0x7

    iget-object v1, p0, Lqbk;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 2041
    :cond_6
    iget v0, p0, Lqbk;->i:I

    if-eq v0, v5, :cond_7

    .line 2042
    const/16 v0, 0x8

    iget v1, p0, Lqbk;->i:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2044
    :cond_7
    iget-object v0, p0, Lqbk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2046
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1990
    invoke-virtual {p0, p1}, Lqbk;->a(Loxn;)Lqbk;

    move-result-object v0

    return-object v0
.end method
