.class public final Lnhr;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnhm;

.field public b:Lnhk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    sget-object v0, Lnhm;->a:[Lnhm;

    iput-object v0, p0, Lnhr;->a:[Lnhm;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lnhr;->b:Lnhk;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 37
    .line 38
    iget-object v1, p0, Lnhr;->a:[Lnhm;

    if-eqz v1, :cond_1

    .line 39
    iget-object v2, p0, Lnhr;->a:[Lnhm;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 40
    if-eqz v4, :cond_0

    .line 41
    const/4 v5, 0x1

    .line 42
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 39
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    :cond_1
    iget-object v1, p0, Lnhr;->b:Lnhk;

    if-eqz v1, :cond_2

    .line 47
    const/4 v1, 0x2

    iget-object v2, p0, Lnhr;->b:Lnhk;

    .line 48
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    :cond_2
    iget-object v1, p0, Lnhr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    iput v0, p0, Lnhr;->ai:I

    .line 52
    return v0
.end method

.method public a(Loxn;)Lnhr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 60
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 61
    sparse-switch v0, :sswitch_data_0

    .line 65
    iget-object v2, p0, Lnhr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnhr;->ah:Ljava/util/List;

    .line 69
    :cond_1
    iget-object v2, p0, Lnhr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    :sswitch_0
    return-object p0

    .line 76
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 77
    iget-object v0, p0, Lnhr;->a:[Lnhm;

    if-nez v0, :cond_3

    move v0, v1

    .line 78
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnhm;

    .line 79
    iget-object v3, p0, Lnhr;->a:[Lnhm;

    if-eqz v3, :cond_2

    .line 80
    iget-object v3, p0, Lnhr;->a:[Lnhm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    :cond_2
    iput-object v2, p0, Lnhr;->a:[Lnhm;

    .line 83
    :goto_2
    iget-object v2, p0, Lnhr;->a:[Lnhm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 84
    iget-object v2, p0, Lnhr;->a:[Lnhm;

    new-instance v3, Lnhm;

    invoke-direct {v3}, Lnhm;-><init>()V

    aput-object v3, v2, v0

    .line 85
    iget-object v2, p0, Lnhr;->a:[Lnhm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 86
    invoke-virtual {p1}, Loxn;->a()I

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 77
    :cond_3
    iget-object v0, p0, Lnhr;->a:[Lnhm;

    array-length v0, v0

    goto :goto_1

    .line 89
    :cond_4
    iget-object v2, p0, Lnhr;->a:[Lnhm;

    new-instance v3, Lnhm;

    invoke-direct {v3}, Lnhm;-><init>()V

    aput-object v3, v2, v0

    .line 90
    iget-object v2, p0, Lnhr;->a:[Lnhm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 94
    :sswitch_2
    iget-object v0, p0, Lnhr;->b:Lnhk;

    if-nez v0, :cond_5

    .line 95
    new-instance v0, Lnhk;

    invoke-direct {v0}, Lnhk;-><init>()V

    iput-object v0, p0, Lnhr;->b:Lnhk;

    .line 97
    :cond_5
    iget-object v0, p0, Lnhr;->b:Lnhk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 61
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 21
    iget-object v0, p0, Lnhr;->a:[Lnhm;

    if-eqz v0, :cond_1

    .line 22
    iget-object v1, p0, Lnhr;->a:[Lnhm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 23
    if-eqz v3, :cond_0

    .line 24
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 22
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_1
    iget-object v0, p0, Lnhr;->b:Lnhk;

    if-eqz v0, :cond_2

    .line 29
    const/4 v0, 0x2

    iget-object v1, p0, Lnhr;->b:Lnhk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_2
    iget-object v0, p0, Lnhr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnhr;->a(Loxn;)Lnhr;

    move-result-object v0

    return-object v0
.end method
