.class public final Levo;
.super Ljfh;
.source "PG"


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private d:Ljava/lang/String;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "circle_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "circle_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "contact_count"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "volume"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "notifications_enabled"

    aput-object v2, v0, v1

    sput-object v0, Levo;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljfh;-><init>()V

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Levo;->e:Z

    .line 70
    const-string v0, "circles"

    invoke-virtual {p0, v0}, Levo;->b(Ljava/lang/String;)V

    .line 71
    const-string v0, "circle_id NOT IN (\'v.all.circles\',\'v.whatshot\',\'v.nearby\')"

    iput-object v0, p0, Levo;->d:Ljava/lang/String;

    .line 72
    return-void
.end method

.method private y()Z
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Levo;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Levo;->b:I

    .line 294
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/database/ContentObserver;)V
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Levo;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 269
    return-void
.end method

.method protected a(Landroid/database/Cursor;Ljgb;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 201
    invoke-virtual {p0}, Levo;->k()I

    move-result v1

    invoke-virtual {p0, p1}, Levo;->d(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    move-object v0, p2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 204
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 205
    new-instance v1, Lkqu;

    sget-object v2, Lomy;->e:Lhmn;

    invoke-direct {v1, v2, v0}, Lkqu;-><init>(Lhmn;Ljava/lang/String;)V

    invoke-interface {p2, v1}, Ljgb;->a(Lhmk;)Lhmk;

    .line 207
    return-void
.end method

.method protected a(Ljfz;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 283
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 284
    const-string v1, "people_view_type"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 286
    invoke-virtual {p0, p2}, Levo;->d(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0202f2

    iget-object v3, p0, Levo;->a:Landroid/content/Context;

    const v4, 0x7f0a06fd

    .line 288
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 286
    invoke-virtual {p1, v1, v2, v3, v0}, Ljfz;->a(Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;)V

    .line 290
    return-void
.end method

.method protected a(Ljgb;)V
    .locals 8

    .prologue
    const v1, 0x7f0204e0

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 97
    invoke-virtual {p0}, Levo;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Levo;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a0808

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Levo;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0a0809

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p1

    move-object v5, v2

    move-object v6, v2

    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance v0, Lhmk;

    sget-object v1, Lomy;->a:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p1, v0}, Ljgb;->a(Lhmk;)Lhmk;

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Levo;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a080b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Levo;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a080a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Levo;->m()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Levo;->u()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Levo;->r()Z

    move-result v7

    move-object v0, p1

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance v0, Lhmk;

    sget-object v1, Lomy;->b:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p1, v0}, Ljgb;->a(Lhmk;)Lhmk;

    goto :goto_0
.end method

.method protected aq_()Z
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Levo;->y()Z

    move-result v0

    return v0
.end method

.method protected ar_()Z
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Levo;->u()I

    move-result v0

    .line 154
    invoke-virtual {p0}, Levo;->m()Z

    move-result v1

    if-nez v1, :cond_0

    if-lez v0, :cond_0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/database/Cursor;)Landroid/content/Intent;
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 211
    new-instance v10, Landroid/content/Intent;

    iget-object v1, p0, Levo;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v10, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 212
    const-string v1, "destination"

    invoke-virtual {v10, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 213
    const-string v11, "circle_info"

    new-instance v1, Levm;

    iget-object v2, p0, Levo;->a:Landroid/content/Context;

    const/4 v3, 0x2

    .line 214
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 215
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    .line 216
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v7, 0x5

    .line 217
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_0

    :goto_0
    const/4 v7, 0x4

    .line 218
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 219
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-direct/range {v1 .. v9}, Levm;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZIJ)V

    .line 213
    invoke-virtual {v10, v11, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 220
    return-object v10

    :cond_0
    move v6, v0

    .line 217
    goto :goto_0
.end method

.method public b(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Levo;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 274
    return-void
.end method

.method protected b(Ljgb;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 169
    iget-object v0, p0, Levo;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a080c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move v7, v1

    .line 171
    invoke-interface/range {v0 .. v7}, Ljgb;->a(ILizu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 174
    new-instance v0, Lhmk;

    sget-object v1, Lomy;->a:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-interface {p1, v0}, Ljgb;->a(Lhmk;)Lhmk;

    .line 176
    return-void
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Levo;->y()Z

    move-result v0

    return v0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Levo;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    const/4 v0, 0x2

    .line 90
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Levo;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected c(Landroid/database/Cursor;)J
    .locals 2

    .prologue
    .line 225
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 139
    const v0, 0x7f0204e0

    return v0
.end method

.method protected d(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Levo;->a:Landroid/content/Context;

    const/4 v1, 0x1

    .line 230
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    .line 231
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 229
    invoke-static {v0, v1, v2}, Levm;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected e()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 144
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Levo;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    const-string v1, "destination"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 147
    return-object v0
.end method

.method protected g()Z
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Levo;->ar_()Z

    move-result v0

    return v0
.end method

.method protected h()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x5

    return v0
.end method

.method protected i()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 180
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Levo;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 181
    const-string v1, "destination"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 182
    return-object v0
.end method

.method protected j()Z
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x1

    return v0
.end method

.method protected k()I
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x0

    return v0
.end method

.method protected l()Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 236
    iget-object v0, p0, Levo;->a:Landroid/content/Context;

    iget v1, p0, Levo;->b:I

    const/4 v2, 0x3

    sget-object v3, Levo;->c:[Ljava/lang/String;

    iget-object v4, p0, Levo;->d:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;IZ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Levo;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Levo;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n()I
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    return v0
.end method

.method protected o()V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Levo;->e:Z

    .line 254
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    iput-boolean v0, p0, Levo;->e:Z

    .line 259
    return-void
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x1

    return v0
.end method

.method protected r()Z
    .locals 1

    .prologue
    .line 278
    invoke-virtual {p0}, Levo;->u()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
