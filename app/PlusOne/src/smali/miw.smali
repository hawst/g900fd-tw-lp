.class public final Lmiw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmrm;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34099
    invoke-direct {p0}, Loxq;-><init>()V

    .line 34102
    iput-object v0, p0, Lmiw;->apiHeader:Llyq;

    .line 34105
    iput-object v0, p0, Lmiw;->a:Lmrm;

    .line 34099
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 34122
    const/4 v0, 0x0

    .line 34123
    iget-object v1, p0, Lmiw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 34124
    const/4 v0, 0x1

    iget-object v1, p0, Lmiw;->apiHeader:Llyq;

    .line 34125
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 34127
    :cond_0
    iget-object v1, p0, Lmiw;->a:Lmrm;

    if-eqz v1, :cond_1

    .line 34128
    const/4 v1, 0x2

    iget-object v2, p0, Lmiw;->a:Lmrm;

    .line 34129
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34131
    :cond_1
    iget-object v1, p0, Lmiw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34132
    iput v0, p0, Lmiw;->ai:I

    .line 34133
    return v0
.end method

.method public a(Loxn;)Lmiw;
    .locals 2

    .prologue
    .line 34141
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 34142
    sparse-switch v0, :sswitch_data_0

    .line 34146
    iget-object v1, p0, Lmiw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 34147
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmiw;->ah:Ljava/util/List;

    .line 34150
    :cond_1
    iget-object v1, p0, Lmiw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34152
    :sswitch_0
    return-object p0

    .line 34157
    :sswitch_1
    iget-object v0, p0, Lmiw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 34158
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmiw;->apiHeader:Llyq;

    .line 34160
    :cond_2
    iget-object v0, p0, Lmiw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 34164
    :sswitch_2
    iget-object v0, p0, Lmiw;->a:Lmrm;

    if-nez v0, :cond_3

    .line 34165
    new-instance v0, Lmrm;

    invoke-direct {v0}, Lmrm;-><init>()V

    iput-object v0, p0, Lmiw;->a:Lmrm;

    .line 34167
    :cond_3
    iget-object v0, p0, Lmiw;->a:Lmrm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 34142
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 34110
    iget-object v0, p0, Lmiw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 34111
    const/4 v0, 0x1

    iget-object v1, p0, Lmiw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 34113
    :cond_0
    iget-object v0, p0, Lmiw;->a:Lmrm;

    if-eqz v0, :cond_1

    .line 34114
    const/4 v0, 0x2

    iget-object v1, p0, Lmiw;->a:Lmrm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 34116
    :cond_1
    iget-object v0, p0, Lmiw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 34118
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 34095
    invoke-virtual {p0, p1}, Lmiw;->a(Loxn;)Lmiw;

    move-result-object v0

    return-object v0
.end method
