.class public final Lneq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Long;

.field public c:Llwr;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1014
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1023
    const/4 v0, 0x0

    iput-object v0, p0, Lneq;->c:Llwr;

    .line 1014
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 1056
    const/4 v0, 0x0

    .line 1057
    iget-object v1, p0, Lneq;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1058
    const/4 v0, 0x1

    iget-object v1, p0, Lneq;->a:Ljava/lang/String;

    .line 1059
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1061
    :cond_0
    iget-object v1, p0, Lneq;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 1062
    const/4 v1, 0x2

    iget-object v2, p0, Lneq;->b:Ljava/lang/Long;

    .line 1063
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1065
    :cond_1
    iget-object v1, p0, Lneq;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1066
    const/4 v1, 0x3

    iget-object v2, p0, Lneq;->d:Ljava/lang/String;

    .line 1067
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1069
    :cond_2
    iget-object v1, p0, Lneq;->c:Llwr;

    if-eqz v1, :cond_3

    .line 1070
    const/4 v1, 0x4

    iget-object v2, p0, Lneq;->c:Llwr;

    .line 1071
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1073
    :cond_3
    iget-object v1, p0, Lneq;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1074
    const/4 v1, 0x5

    iget-object v2, p0, Lneq;->e:Ljava/lang/String;

    .line 1075
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1077
    :cond_4
    iget-object v1, p0, Lneq;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1078
    const/4 v1, 0x6

    iget-object v2, p0, Lneq;->f:Ljava/lang/String;

    .line 1079
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1081
    :cond_5
    iget-object v1, p0, Lneq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1082
    iput v0, p0, Lneq;->ai:I

    .line 1083
    return v0
.end method

.method public a(Loxn;)Lneq;
    .locals 2

    .prologue
    .line 1091
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1092
    sparse-switch v0, :sswitch_data_0

    .line 1096
    iget-object v1, p0, Lneq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1097
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lneq;->ah:Ljava/util/List;

    .line 1100
    :cond_1
    iget-object v1, p0, Lneq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1102
    :sswitch_0
    return-object p0

    .line 1107
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lneq;->a:Ljava/lang/String;

    goto :goto_0

    .line 1111
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lneq;->b:Ljava/lang/Long;

    goto :goto_0

    .line 1115
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lneq;->d:Ljava/lang/String;

    goto :goto_0

    .line 1119
    :sswitch_4
    iget-object v0, p0, Lneq;->c:Llwr;

    if-nez v0, :cond_2

    .line 1120
    new-instance v0, Llwr;

    invoke-direct {v0}, Llwr;-><init>()V

    iput-object v0, p0, Lneq;->c:Llwr;

    .line 1122
    :cond_2
    iget-object v0, p0, Lneq;->c:Llwr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1126
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lneq;->e:Ljava/lang/String;

    goto :goto_0

    .line 1130
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lneq;->f:Ljava/lang/String;

    goto :goto_0

    .line 1092
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 1032
    iget-object v0, p0, Lneq;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1033
    const/4 v0, 0x1

    iget-object v1, p0, Lneq;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1035
    :cond_0
    iget-object v0, p0, Lneq;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1036
    const/4 v0, 0x2

    iget-object v1, p0, Lneq;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 1038
    :cond_1
    iget-object v0, p0, Lneq;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1039
    const/4 v0, 0x3

    iget-object v1, p0, Lneq;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1041
    :cond_2
    iget-object v0, p0, Lneq;->c:Llwr;

    if-eqz v0, :cond_3

    .line 1042
    const/4 v0, 0x4

    iget-object v1, p0, Lneq;->c:Llwr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1044
    :cond_3
    iget-object v0, p0, Lneq;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1045
    const/4 v0, 0x5

    iget-object v1, p0, Lneq;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1047
    :cond_4
    iget-object v0, p0, Lneq;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1048
    const/4 v0, 0x6

    iget-object v1, p0, Lneq;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1050
    :cond_5
    iget-object v0, p0, Lneq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1052
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1010
    invoke-virtual {p0, p1}, Lneq;->a(Loxn;)Lneq;

    move-result-object v0

    return-object v0
.end method
