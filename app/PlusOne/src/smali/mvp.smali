.class public final Lmvp;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lmvq;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11344
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11347
    sget-object v0, Lmvq;->a:[Lmvq;

    iput-object v0, p0, Lmvp;->a:[Lmvq;

    .line 11344
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 11375
    .line 11376
    iget-object v1, p0, Lmvp;->a:[Lmvq;

    if-eqz v1, :cond_1

    .line 11377
    iget-object v2, p0, Lmvp;->a:[Lmvq;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 11378
    if-eqz v4, :cond_0

    .line 11379
    const/4 v5, 0x1

    .line 11380
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 11377
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 11384
    :cond_1
    iget-object v1, p0, Lmvp;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 11385
    const/4 v1, 0x2

    iget-object v2, p0, Lmvp;->b:Ljava/lang/Integer;

    .line 11386
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11388
    :cond_2
    iget-object v1, p0, Lmvp;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 11389
    const/4 v1, 0x3

    iget-object v2, p0, Lmvp;->c:Ljava/lang/Integer;

    .line 11390
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11392
    :cond_3
    iget-object v1, p0, Lmvp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11393
    iput v0, p0, Lmvp;->ai:I

    .line 11394
    return v0
.end method

.method public a(Loxn;)Lmvp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11402
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11403
    sparse-switch v0, :sswitch_data_0

    .line 11407
    iget-object v2, p0, Lmvp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 11408
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmvp;->ah:Ljava/util/List;

    .line 11411
    :cond_1
    iget-object v2, p0, Lmvp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11413
    :sswitch_0
    return-object p0

    .line 11418
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 11419
    iget-object v0, p0, Lmvp;->a:[Lmvq;

    if-nez v0, :cond_3

    move v0, v1

    .line 11420
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmvq;

    .line 11421
    iget-object v3, p0, Lmvp;->a:[Lmvq;

    if-eqz v3, :cond_2

    .line 11422
    iget-object v3, p0, Lmvp;->a:[Lmvq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 11424
    :cond_2
    iput-object v2, p0, Lmvp;->a:[Lmvq;

    .line 11425
    :goto_2
    iget-object v2, p0, Lmvp;->a:[Lmvq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 11426
    iget-object v2, p0, Lmvp;->a:[Lmvq;

    new-instance v3, Lmvq;

    invoke-direct {v3}, Lmvq;-><init>()V

    aput-object v3, v2, v0

    .line 11427
    iget-object v2, p0, Lmvp;->a:[Lmvq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 11428
    invoke-virtual {p1}, Loxn;->a()I

    .line 11425
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 11419
    :cond_3
    iget-object v0, p0, Lmvp;->a:[Lmvq;

    array-length v0, v0

    goto :goto_1

    .line 11431
    :cond_4
    iget-object v2, p0, Lmvp;->a:[Lmvq;

    new-instance v3, Lmvq;

    invoke-direct {v3}, Lmvq;-><init>()V

    aput-object v3, v2, v0

    .line 11432
    iget-object v2, p0, Lmvp;->a:[Lmvq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11436
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvp;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 11440
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvp;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 11403
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 11356
    iget-object v0, p0, Lmvp;->a:[Lmvq;

    if-eqz v0, :cond_1

    .line 11357
    iget-object v1, p0, Lmvp;->a:[Lmvq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 11358
    if-eqz v3, :cond_0

    .line 11359
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 11357
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11363
    :cond_1
    iget-object v0, p0, Lmvp;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 11364
    const/4 v0, 0x2

    iget-object v1, p0, Lmvp;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11366
    :cond_2
    iget-object v0, p0, Lmvp;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 11367
    const/4 v0, 0x3

    iget-object v1, p0, Lmvp;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11369
    :cond_3
    iget-object v0, p0, Lmvp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11371
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11340
    invoke-virtual {p0, p1}, Lmvp;->a(Loxn;)Lmvp;

    move-result-object v0

    return-object v0
.end method
