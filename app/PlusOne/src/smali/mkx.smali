.class public final Lmkx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmox;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30778
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30781
    iput-object v0, p0, Lmkx;->apiHeader:Llyr;

    .line 30784
    iput-object v0, p0, Lmkx;->a:Lmox;

    .line 30778
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 30801
    const/4 v0, 0x0

    .line 30802
    iget-object v1, p0, Lmkx;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 30803
    const/4 v0, 0x1

    iget-object v1, p0, Lmkx;->apiHeader:Llyr;

    .line 30804
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 30806
    :cond_0
    iget-object v1, p0, Lmkx;->a:Lmox;

    if-eqz v1, :cond_1

    .line 30807
    const/4 v1, 0x2

    iget-object v2, p0, Lmkx;->a:Lmox;

    .line 30808
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30810
    :cond_1
    iget-object v1, p0, Lmkx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30811
    iput v0, p0, Lmkx;->ai:I

    .line 30812
    return v0
.end method

.method public a(Loxn;)Lmkx;
    .locals 2

    .prologue
    .line 30820
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 30821
    sparse-switch v0, :sswitch_data_0

    .line 30825
    iget-object v1, p0, Lmkx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 30826
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkx;->ah:Ljava/util/List;

    .line 30829
    :cond_1
    iget-object v1, p0, Lmkx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30831
    :sswitch_0
    return-object p0

    .line 30836
    :sswitch_1
    iget-object v0, p0, Lmkx;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 30837
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkx;->apiHeader:Llyr;

    .line 30839
    :cond_2
    iget-object v0, p0, Lmkx;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30843
    :sswitch_2
    iget-object v0, p0, Lmkx;->a:Lmox;

    if-nez v0, :cond_3

    .line 30844
    new-instance v0, Lmox;

    invoke-direct {v0}, Lmox;-><init>()V

    iput-object v0, p0, Lmkx;->a:Lmox;

    .line 30846
    :cond_3
    iget-object v0, p0, Lmkx;->a:Lmox;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30821
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30789
    iget-object v0, p0, Lmkx;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 30790
    const/4 v0, 0x1

    iget-object v1, p0, Lmkx;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30792
    :cond_0
    iget-object v0, p0, Lmkx;->a:Lmox;

    if-eqz v0, :cond_1

    .line 30793
    const/4 v0, 0x2

    iget-object v1, p0, Lmkx;->a:Lmox;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30795
    :cond_1
    iget-object v0, p0, Lmkx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 30797
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 30774
    invoke-virtual {p0, p1}, Lmkx;->a(Loxn;)Lmkx;

    move-result-object v0

    return-object v0
.end method
