.class public final Lmlt;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnzx;

.field public b:[Lnzx;

.field public c:[Lnyz;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 461
    invoke-direct {p0}, Loxq;-><init>()V

    .line 464
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lmlt;->a:[Lnzx;

    .line 467
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lmlt;->b:[Lnzx;

    .line 470
    sget-object v0, Lnyz;->a:[Lnyz;

    iput-object v0, p0, Lmlt;->c:[Lnyz;

    .line 461
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 507
    .line 508
    iget-object v0, p0, Lmlt;->a:[Lnzx;

    if-eqz v0, :cond_1

    .line 509
    iget-object v3, p0, Lmlt;->a:[Lnzx;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 510
    if-eqz v5, :cond_0

    .line 511
    const/4 v6, 0x1

    .line 512
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 509
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 516
    :cond_2
    iget-object v2, p0, Lmlt;->b:[Lnzx;

    if-eqz v2, :cond_4

    .line 517
    iget-object v3, p0, Lmlt;->b:[Lnzx;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 518
    if-eqz v5, :cond_3

    .line 519
    const/4 v6, 0x2

    .line 520
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 517
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 524
    :cond_4
    iget-object v2, p0, Lmlt;->c:[Lnyz;

    if-eqz v2, :cond_6

    .line 525
    iget-object v2, p0, Lmlt;->c:[Lnyz;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 526
    if-eqz v4, :cond_5

    .line 527
    const/4 v5, 0x3

    .line 528
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 525
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 532
    :cond_6
    iget-object v1, p0, Lmlt;->d:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 533
    const/4 v1, 0x4

    iget-object v2, p0, Lmlt;->d:Ljava/lang/String;

    .line 534
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 536
    :cond_7
    iget-object v1, p0, Lmlt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 537
    iput v0, p0, Lmlt;->ai:I

    .line 538
    return v0
.end method

.method public a(Loxn;)Lmlt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 546
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 547
    sparse-switch v0, :sswitch_data_0

    .line 551
    iget-object v2, p0, Lmlt;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 552
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmlt;->ah:Ljava/util/List;

    .line 555
    :cond_1
    iget-object v2, p0, Lmlt;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 557
    :sswitch_0
    return-object p0

    .line 562
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 563
    iget-object v0, p0, Lmlt;->a:[Lnzx;

    if-nez v0, :cond_3

    move v0, v1

    .line 564
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 565
    iget-object v3, p0, Lmlt;->a:[Lnzx;

    if-eqz v3, :cond_2

    .line 566
    iget-object v3, p0, Lmlt;->a:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 568
    :cond_2
    iput-object v2, p0, Lmlt;->a:[Lnzx;

    .line 569
    :goto_2
    iget-object v2, p0, Lmlt;->a:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 570
    iget-object v2, p0, Lmlt;->a:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 571
    iget-object v2, p0, Lmlt;->a:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 572
    invoke-virtual {p1}, Loxn;->a()I

    .line 569
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 563
    :cond_3
    iget-object v0, p0, Lmlt;->a:[Lnzx;

    array-length v0, v0

    goto :goto_1

    .line 575
    :cond_4
    iget-object v2, p0, Lmlt;->a:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 576
    iget-object v2, p0, Lmlt;->a:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 580
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 581
    iget-object v0, p0, Lmlt;->b:[Lnzx;

    if-nez v0, :cond_6

    move v0, v1

    .line 582
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 583
    iget-object v3, p0, Lmlt;->b:[Lnzx;

    if-eqz v3, :cond_5

    .line 584
    iget-object v3, p0, Lmlt;->b:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 586
    :cond_5
    iput-object v2, p0, Lmlt;->b:[Lnzx;

    .line 587
    :goto_4
    iget-object v2, p0, Lmlt;->b:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 588
    iget-object v2, p0, Lmlt;->b:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 589
    iget-object v2, p0, Lmlt;->b:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 590
    invoke-virtual {p1}, Loxn;->a()I

    .line 587
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 581
    :cond_6
    iget-object v0, p0, Lmlt;->b:[Lnzx;

    array-length v0, v0

    goto :goto_3

    .line 593
    :cond_7
    iget-object v2, p0, Lmlt;->b:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 594
    iget-object v2, p0, Lmlt;->b:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 598
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 599
    iget-object v0, p0, Lmlt;->c:[Lnyz;

    if-nez v0, :cond_9

    move v0, v1

    .line 600
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyz;

    .line 601
    iget-object v3, p0, Lmlt;->c:[Lnyz;

    if-eqz v3, :cond_8

    .line 602
    iget-object v3, p0, Lmlt;->c:[Lnyz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 604
    :cond_8
    iput-object v2, p0, Lmlt;->c:[Lnyz;

    .line 605
    :goto_6
    iget-object v2, p0, Lmlt;->c:[Lnyz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 606
    iget-object v2, p0, Lmlt;->c:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 607
    iget-object v2, p0, Lmlt;->c:[Lnyz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 608
    invoke-virtual {p1}, Loxn;->a()I

    .line 605
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 599
    :cond_9
    iget-object v0, p0, Lmlt;->c:[Lnyz;

    array-length v0, v0

    goto :goto_5

    .line 611
    :cond_a
    iget-object v2, p0, Lmlt;->c:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 612
    iget-object v2, p0, Lmlt;->c:[Lnyz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 616
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmlt;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 547
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 477
    iget-object v1, p0, Lmlt;->a:[Lnzx;

    if-eqz v1, :cond_1

    .line 478
    iget-object v2, p0, Lmlt;->a:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 479
    if-eqz v4, :cond_0

    .line 480
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 478
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 484
    :cond_1
    iget-object v1, p0, Lmlt;->b:[Lnzx;

    if-eqz v1, :cond_3

    .line 485
    iget-object v2, p0, Lmlt;->b:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 486
    if-eqz v4, :cond_2

    .line 487
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 485
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 491
    :cond_3
    iget-object v1, p0, Lmlt;->c:[Lnyz;

    if-eqz v1, :cond_5

    .line 492
    iget-object v1, p0, Lmlt;->c:[Lnyz;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 493
    if-eqz v3, :cond_4

    .line 494
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 492
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 498
    :cond_5
    iget-object v0, p0, Lmlt;->d:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 499
    const/4 v0, 0x4

    iget-object v1, p0, Lmlt;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 501
    :cond_6
    iget-object v0, p0, Lmlt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 503
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0, p1}, Lmlt;->a(Loxn;)Lmlt;

    move-result-object v0

    return-object v0
.end method
