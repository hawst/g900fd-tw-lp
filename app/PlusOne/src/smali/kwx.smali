.class public final Lkwx;
.super Llol;
.source "PG"

# interfaces
.implements Lbc;
.implements Lhio;
.implements Lhyz;
.implements Lhzq;
.implements Lksv;
.implements Lllh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhio;",
        "Lhyz;",
        "Lhzq;",
        "Lksv;",
        "Lllh;"
    }
.end annotation


# static fields
.field private static final N:[Ljava/lang/String;


# instance fields
.field private O:Lhzb;

.field private P:I

.field private Q:Ljava/lang/String;

.field private R:Z

.field private S:Lhzl;

.field private T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkwt;",
            ">;"
        }
    .end annotation
.end field

.field private U:Lksu;

.field private V:Lhie;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "square_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "square_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "photo_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "restricted_domain"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "square_streams"

    aput-object v2, v0, v1

    sput-object v0, Lkwx;->N:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Llol;-><init>()V

    return-void
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 231
    if-nez p1, :cond_1

    .line 232
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkwx;->T:Ljava/util/List;

    .line 264
    :goto_0
    iget-object v0, p0, Lkwx;->O:Lhzb;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lkwx;->O:Lhzb;

    invoke-interface {v0}, Lhzb;->e()V

    .line 267
    :cond_0
    return-void

    .line 234
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 235
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 236
    :cond_2
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 237
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 238
    iget-object v0, p0, Lkwx;->S:Lhzl;

    instance-of v0, v0, Lhzk;

    if-eqz v0, :cond_3

    .line 239
    iget-object v0, p0, Lkwx;->S:Lhzl;

    check-cast v0, Lhzk;

    const/16 v2, 0xaa

    invoke-interface {v0, v2, v1}, Lhzk;->a(ILjava/lang/String;)Z

    move-result v0

    .line 241
    if-nez v0, :cond_2

    .line 242
    :cond_3
    const/4 v0, 0x4

    .line 246
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 245
    invoke-static {v0}, Lktg;->a([B)[Lktg;

    move-result-object v0

    .line 249
    if-eqz v0, :cond_6

    array-length v2, v0

    if-ne v2, v7, :cond_6

    .line 250
    aget-object v2, v0, v8

    invoke-virtual {v2}, Lktg;->a()Ljava/lang/String;

    move-result-object v3

    .line 251
    aget-object v0, v0, v8

    invoke-virtual {v0}, Lktg;->b()Ljava/lang/String;

    move-result-object v4

    .line 253
    :goto_2
    invoke-static {}, Lkwt;->a()Lkwv;

    move-result-object v0

    const/4 v2, 0x2

    .line 254
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkwv;->a(Ljava/lang/String;)Lkwv;

    move-result-object v10

    new-instance v0, Lkxr;

    .line 256
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x3

    .line 259
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v7

    :goto_3
    invoke-direct/range {v0 .. v5}, Lkxr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 255
    invoke-virtual {v10, v0}, Lkwv;->a(Lkxr;)Lkwv;

    move-result-object v0

    .line 259
    invoke-virtual {v0}, Lkwv;->a()Lkwt;

    move-result-object v0

    .line 253
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move v5, v8

    .line 259
    goto :goto_3

    .line 261
    :cond_5
    iput-object v9, p0, Lkwx;->T:Ljava/util/List;

    goto :goto_0

    :cond_6
    move-object v4, v6

    move-object v3, v6

    goto :goto_2
.end method


# virtual methods
.method public U()V
    .locals 3

    .prologue
    .line 211
    invoke-virtual {p0}, Lkwx;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 212
    return-void
.end method

.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x1

    return v0
.end method

.method public a(I)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lkwx;->T:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216
    new-instance v0, Lktu;

    iget-object v1, p0, Lkwx;->at:Llnl;

    iget v2, p0, Lkwx;->P:I

    sget-object v3, Lkwx;->N:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lktu;-><init>(Landroid/content/Context;I[Ljava/lang/String;)V

    iget-object v1, p0, Lkwx;->Q:Ljava/lang/String;

    .line 217
    invoke-virtual {v0, v1}, Lktu;->c(Ljava/lang/String;)Lktu;

    move-result-object v0

    const/4 v1, 0x1

    .line 218
    invoke-virtual {v0, v1}, Lktu;->a(I)Lktu;

    move-result-object v0

    iget-boolean v1, p0, Lkwx;->R:Z

    .line 219
    invoke-virtual {v0, v1}, Lktu;->a(Z)Lktu;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 179
    const v0, 0x7f100174

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;

    .line 180
    iget-object v1, p0, Lkwx;->T:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkwt;

    .line 181
    invoke-virtual {v1}, Lkwt;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/squares/providers/acl/SquareAvatarView;

    .line 183
    const v0, 0x7f1001f8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 184
    invoke-virtual {v1}, Lkwt;->b()Lkxr;

    move-result-object v1

    invoke-virtual {v1}, Lkxr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    invoke-virtual {p0}, Lkwx;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d024c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 185
    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 187
    invoke-virtual {v0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 188
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 189
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 190
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 191
    invoke-virtual {p0}, Lkwx;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0141

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 192
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 224
    invoke-direct {p0, p1}, Lkwx;->b(Landroid/database/Cursor;)V

    .line 225
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lkwx;->S:Lhzl;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lkwt;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lkwx;->S:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->c(Landroid/os/Parcelable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lkwx;->S:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 94
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 84
    check-cast v0, Lkwt;

    .line 85
    invoke-virtual {v0}, Lkwt;->b()Lkxr;

    move-result-object v1

    invoke-virtual {v1}, Lkxr;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 86
    iget-object v1, p0, Lkwx;->U:Lksu;

    invoke-virtual {v0}, Lkwt;->b()Lkxr;

    move-result-object v0

    invoke-interface {v1, v0}, Lksu;->a(Lkxr;)Lt;

    move-result-object v0

    invoke-virtual {p0}, Lkwx;->q()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :cond_2
    iget-object v0, p0, Lkwx;->S:Lhzl;

    invoke-interface {v0}, Lhzl;->a()V

    .line 89
    iget-object v0, p0, Lkwx;->S:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->a(Landroid/os/Parcelable;)Z

    .line 90
    iget-object v0, p0, Lkwx;->V:Lhie;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lhie;->a(I)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 47
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lkwx;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhzb;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lkwx;->O:Lhzb;

    .line 207
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 271
    if-eqz p1, :cond_0

    .line 272
    iget-object v0, p0, Lkwx;->Q:Ljava/lang/String;

    .line 273
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkwx;->Q:Ljava/lang/String;

    .line 274
    iget-object v1, p0, Lkwx;->Q:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lkwx;->Q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkwx;->b(Landroid/database/Cursor;)V

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    invoke-virtual {p0}, Lkwx;->U()V

    goto :goto_0
.end method

.method public a(Lkxr;)V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lkwx;->S:Lhzl;

    invoke-interface {v0}, Lhzl;->a()V

    .line 99
    iget-object v0, p0, Lkwx;->S:Lhzl;

    invoke-static {}, Lkwt;->a()Lkwv;

    move-result-object v1

    .line 100
    invoke-virtual {v1, p1}, Lkwv;->a(Lkxr;)Lkwv;

    move-result-object v1

    .line 101
    invoke-virtual {v1}, Lkwv;->a()Lkwt;

    move-result-object v1

    .line 99
    invoke-interface {v0, v1}, Lhzl;->a(Landroid/os/Parcelable;)Z

    .line 102
    iget-object v0, p0, Lkwx;->V:Lhie;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lhie;->a(I)V

    .line 103
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 286
    iput-boolean p1, p0, Lkwx;->R:Z

    .line 287
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkwx;->b(Landroid/database/Cursor;)V

    .line 288
    invoke-virtual {p0}, Lkwx;->U()V

    .line 289
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return v0
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    .line 141
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 142
    const v1, 0x7f0401fe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lkwx;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lkwx;->P:I

    .line 114
    iget-object v0, p0, Lkwx;->au:Llnh;

    const-class v1, Lllg;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllg;

    .line 115
    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {v0, p0}, Lllg;->a(Lllh;)V

    .line 119
    :cond_0
    iget-object v0, p0, Lkwx;->au:Llnh;

    const-class v1, Lhin;

    .line 120
    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhin;

    .line 121
    if-eqz v0, :cond_1

    .line 122
    invoke-virtual {v0, p0}, Lhin;->a(Lhio;)V

    .line 123
    invoke-virtual {v0}, Lhin;->a()Z

    move-result v0

    iput-boolean v0, p0, Lkwx;->R:Z

    .line 128
    :goto_0
    iget-object v0, p0, Lkwx;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Lkwx;->S:Lhzl;

    .line 129
    iget-object v0, p0, Lkwx;->au:Llnh;

    const-class v1, Lksv;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 130
    iget-object v0, p0, Lkwx;->au:Llnh;

    const-class v1, Lksu;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lksu;

    iput-object v0, p0, Lkwx;->U:Lksu;

    .line 131
    iget-object v0, p0, Lkwx;->au:Llnh;

    const-class v1, Lhie;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhie;

    iput-object v0, p0, Lkwx;->V:Lhie;

    .line 132
    return-void

    .line 125
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkwx;->R:Z

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lkwx;->T:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkwx;->T:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 108
    return-void
.end method
