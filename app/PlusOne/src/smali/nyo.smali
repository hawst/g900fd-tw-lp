.class public final Lnyo;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnyn;

.field private b:Lnyn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3447
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3450
    iput-object v0, p0, Lnyo;->a:Lnyn;

    .line 3453
    iput-object v0, p0, Lnyo;->b:Lnyn;

    .line 3447
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3470
    const/4 v0, 0x0

    .line 3471
    iget-object v1, p0, Lnyo;->a:Lnyn;

    if-eqz v1, :cond_0

    .line 3472
    const/4 v0, 0x1

    iget-object v1, p0, Lnyo;->a:Lnyn;

    .line 3473
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3475
    :cond_0
    iget-object v1, p0, Lnyo;->b:Lnyn;

    if-eqz v1, :cond_1

    .line 3476
    const/4 v1, 0x2

    iget-object v2, p0, Lnyo;->b:Lnyn;

    .line 3477
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3479
    :cond_1
    iget-object v1, p0, Lnyo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3480
    iput v0, p0, Lnyo;->ai:I

    .line 3481
    return v0
.end method

.method public a(Loxn;)Lnyo;
    .locals 2

    .prologue
    .line 3489
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3490
    sparse-switch v0, :sswitch_data_0

    .line 3494
    iget-object v1, p0, Lnyo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3495
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyo;->ah:Ljava/util/List;

    .line 3498
    :cond_1
    iget-object v1, p0, Lnyo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3500
    :sswitch_0
    return-object p0

    .line 3505
    :sswitch_1
    iget-object v0, p0, Lnyo;->a:Lnyn;

    if-nez v0, :cond_2

    .line 3506
    new-instance v0, Lnyn;

    invoke-direct {v0}, Lnyn;-><init>()V

    iput-object v0, p0, Lnyo;->a:Lnyn;

    .line 3508
    :cond_2
    iget-object v0, p0, Lnyo;->a:Lnyn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3512
    :sswitch_2
    iget-object v0, p0, Lnyo;->b:Lnyn;

    if-nez v0, :cond_3

    .line 3513
    new-instance v0, Lnyn;

    invoke-direct {v0}, Lnyn;-><init>()V

    iput-object v0, p0, Lnyo;->b:Lnyn;

    .line 3515
    :cond_3
    iget-object v0, p0, Lnyo;->b:Lnyn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3490
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3458
    iget-object v0, p0, Lnyo;->a:Lnyn;

    if-eqz v0, :cond_0

    .line 3459
    const/4 v0, 0x1

    iget-object v1, p0, Lnyo;->a:Lnyn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3461
    :cond_0
    iget-object v0, p0, Lnyo;->b:Lnyn;

    if-eqz v0, :cond_1

    .line 3462
    const/4 v0, 0x2

    iget-object v1, p0, Lnyo;->b:Lnyn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3464
    :cond_1
    iget-object v0, p0, Lnyo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3466
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3443
    invoke-virtual {p0, p1}, Lnyo;->a(Loxn;)Lnyo;

    move-result-object v0

    return-object v0
.end method
