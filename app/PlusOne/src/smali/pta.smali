.class public final Lpta;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lpsz;

.field private b:[Lpsz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Loxq;-><init>()V

    .line 119
    sget-object v0, Lpsz;->a:[Lpsz;

    iput-object v0, p0, Lpta;->a:[Lpsz;

    .line 122
    sget-object v0, Lpsz;->a:[Lpsz;

    iput-object v0, p0, Lpta;->b:[Lpsz;

    .line 116
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 147
    .line 148
    iget-object v0, p0, Lpta;->a:[Lpsz;

    if-eqz v0, :cond_1

    .line 149
    iget-object v3, p0, Lpta;->a:[Lpsz;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 150
    if-eqz v5, :cond_0

    .line 151
    const/4 v6, 0x1

    .line 152
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 149
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 156
    :cond_2
    iget-object v2, p0, Lpta;->b:[Lpsz;

    if-eqz v2, :cond_4

    .line 157
    iget-object v2, p0, Lpta;->b:[Lpsz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 158
    if-eqz v4, :cond_3

    .line 159
    const/4 v5, 0x2

    .line 160
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 157
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 164
    :cond_4
    iget-object v1, p0, Lpta;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    iput v0, p0, Lpta;->ai:I

    .line 166
    return v0
.end method

.method public a(Loxn;)Lpta;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 174
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 175
    sparse-switch v0, :sswitch_data_0

    .line 179
    iget-object v2, p0, Lpta;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 180
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpta;->ah:Ljava/util/List;

    .line 183
    :cond_1
    iget-object v2, p0, Lpta;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    :sswitch_0
    return-object p0

    .line 190
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 191
    iget-object v0, p0, Lpta;->a:[Lpsz;

    if-nez v0, :cond_3

    move v0, v1

    .line 192
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpsz;

    .line 193
    iget-object v3, p0, Lpta;->a:[Lpsz;

    if-eqz v3, :cond_2

    .line 194
    iget-object v3, p0, Lpta;->a:[Lpsz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 196
    :cond_2
    iput-object v2, p0, Lpta;->a:[Lpsz;

    .line 197
    :goto_2
    iget-object v2, p0, Lpta;->a:[Lpsz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 198
    iget-object v2, p0, Lpta;->a:[Lpsz;

    new-instance v3, Lpsz;

    invoke-direct {v3}, Lpsz;-><init>()V

    aput-object v3, v2, v0

    .line 199
    iget-object v2, p0, Lpta;->a:[Lpsz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 200
    invoke-virtual {p1}, Loxn;->a()I

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 191
    :cond_3
    iget-object v0, p0, Lpta;->a:[Lpsz;

    array-length v0, v0

    goto :goto_1

    .line 203
    :cond_4
    iget-object v2, p0, Lpta;->a:[Lpsz;

    new-instance v3, Lpsz;

    invoke-direct {v3}, Lpsz;-><init>()V

    aput-object v3, v2, v0

    .line 204
    iget-object v2, p0, Lpta;->a:[Lpsz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 208
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 209
    iget-object v0, p0, Lpta;->b:[Lpsz;

    if-nez v0, :cond_6

    move v0, v1

    .line 210
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpsz;

    .line 211
    iget-object v3, p0, Lpta;->b:[Lpsz;

    if-eqz v3, :cond_5

    .line 212
    iget-object v3, p0, Lpta;->b:[Lpsz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 214
    :cond_5
    iput-object v2, p0, Lpta;->b:[Lpsz;

    .line 215
    :goto_4
    iget-object v2, p0, Lpta;->b:[Lpsz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 216
    iget-object v2, p0, Lpta;->b:[Lpsz;

    new-instance v3, Lpsz;

    invoke-direct {v3}, Lpsz;-><init>()V

    aput-object v3, v2, v0

    .line 217
    iget-object v2, p0, Lpta;->b:[Lpsz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 218
    invoke-virtual {p1}, Loxn;->a()I

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 209
    :cond_6
    iget-object v0, p0, Lpta;->b:[Lpsz;

    array-length v0, v0

    goto :goto_3

    .line 221
    :cond_7
    iget-object v2, p0, Lpta;->b:[Lpsz;

    new-instance v3, Lpsz;

    invoke-direct {v3}, Lpsz;-><init>()V

    aput-object v3, v2, v0

    .line 222
    iget-object v2, p0, Lpta;->b:[Lpsz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 175
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 127
    iget-object v1, p0, Lpta;->a:[Lpsz;

    if-eqz v1, :cond_1

    .line 128
    iget-object v2, p0, Lpta;->a:[Lpsz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 129
    if-eqz v4, :cond_0

    .line 130
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 128
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 134
    :cond_1
    iget-object v1, p0, Lpta;->b:[Lpsz;

    if-eqz v1, :cond_3

    .line 135
    iget-object v1, p0, Lpta;->b:[Lpsz;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 136
    if-eqz v3, :cond_2

    .line 137
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 135
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 141
    :cond_3
    iget-object v0, p0, Lpta;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 143
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lpta;->a(Loxn;)Lpta;

    move-result-object v0

    return-object v0
.end method
