.class public final Lpee;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpef;

.field public b:Lpeg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v0, p0, Lpee;->a:Lpef;

    .line 16
    iput-object v0, p0, Lpee;->b:Lpeg;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x0

    .line 34
    iget-object v1, p0, Lpee;->a:Lpef;

    if-eqz v1, :cond_0

    .line 35
    const/4 v0, 0x1

    iget-object v1, p0, Lpee;->a:Lpef;

    .line 36
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 38
    :cond_0
    iget-object v1, p0, Lpee;->b:Lpeg;

    if-eqz v1, :cond_1

    .line 39
    const/4 v1, 0x2

    iget-object v2, p0, Lpee;->b:Lpeg;

    .line 40
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_1
    iget-object v1, p0, Lpee;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    iput v0, p0, Lpee;->ai:I

    .line 44
    return v0
.end method

.method public a(Loxn;)Lpee;
    .locals 2

    .prologue
    .line 52
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 53
    sparse-switch v0, :sswitch_data_0

    .line 57
    iget-object v1, p0, Lpee;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpee;->ah:Ljava/util/List;

    .line 61
    :cond_1
    iget-object v1, p0, Lpee;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    :sswitch_0
    return-object p0

    .line 68
    :sswitch_1
    iget-object v0, p0, Lpee;->a:Lpef;

    if-nez v0, :cond_2

    .line 69
    new-instance v0, Lpef;

    invoke-direct {v0}, Lpef;-><init>()V

    iput-object v0, p0, Lpee;->a:Lpef;

    .line 71
    :cond_2
    iget-object v0, p0, Lpee;->a:Lpef;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 75
    :sswitch_2
    iget-object v0, p0, Lpee;->b:Lpeg;

    if-nez v0, :cond_3

    .line 76
    new-instance v0, Lpeg;

    invoke-direct {v0}, Lpeg;-><init>()V

    iput-object v0, p0, Lpee;->b:Lpeg;

    .line 78
    :cond_3
    iget-object v0, p0, Lpee;->b:Lpeg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 53
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lpee;->a:Lpef;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lpee;->a:Lpef;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24
    :cond_0
    iget-object v0, p0, Lpee;->b:Lpeg;

    if-eqz v0, :cond_1

    .line 25
    const/4 v0, 0x2

    iget-object v1, p0, Lpee;->b:Lpeg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27
    :cond_1
    iget-object v0, p0, Lpee;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpee;->a(Loxn;)Lpee;

    move-result-object v0

    return-object v0
.end method
