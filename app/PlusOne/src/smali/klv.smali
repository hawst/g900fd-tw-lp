.class public final Lklv;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmhi;",
        "Lmhj;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Lkjc;

.field private final c:Lhgw;

.field private final p:Lhgw;

.field private final q:Lkzl;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILkjc;)V
    .locals 6

    .prologue
    .line 46
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v3, "postactivity"

    new-instance v4, Lmhi;

    invoke-direct {v4}, Lmhi;-><init>()V

    new-instance v5, Lmhj;

    invoke-direct {v5}, Lmhj;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 51
    iput p2, p0, Lklv;->a:I

    .line 52
    iget-object v0, p0, Lklv;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Lklv;->q:Lkzl;

    .line 53
    iput-object p3, p0, Lklv;->b:Lkjc;

    .line 54
    invoke-virtual {p3}, Lkjc;->k()Lhgw;

    move-result-object v0

    iput-object v0, p0, Lklv;->c:Lhgw;

    .line 55
    invoke-virtual {p3}, Lkjc;->l()Lhgw;

    move-result-object v0

    iput-object v0, p0, Lklv;->p:Lhgw;

    .line 56
    return-void
.end method


# virtual methods
.method protected a(Lmhi;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 60
    new-instance v1, Lodg;

    invoke-direct {v1}, Lodg;-><init>()V

    iput-object v1, p1, Lmhi;->a:Lodg;

    .line 61
    iget-object v2, p1, Lmhi;->a:Lodg;

    .line 62
    iget-object v1, p0, Lklv;->b:Lkjc;

    invoke-virtual {v1}, Lkjc;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lodg;->d:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Lklv;->b:Lkjc;

    invoke-virtual {v1}, Lkjc;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lodg;->a:Ljava/lang/String;

    .line 64
    new-instance v1, Lpee;

    invoke-direct {v1}, Lpee;-><init>()V

    iput-object v1, v2, Lodg;->b:Lpee;

    .line 65
    iget-object v1, v2, Lodg;->b:Lpee;

    new-instance v3, Lpef;

    invoke-direct {v3}, Lpef;-><init>()V

    iput-object v3, v1, Lpee;->a:Lpef;

    .line 66
    iget-object v1, v2, Lodg;->b:Lpee;

    iget-object v1, v1, Lpee;->a:Lpef;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v1, Lpef;->a:Ljava/lang/Boolean;

    .line 67
    iget-object v1, p0, Lklv;->c:Lhgw;

    iget-object v3, p0, Lklv;->p:Lhgw;

    .line 68
    invoke-static {v1, v3}, Lhft;->a(Lhgw;Lhgw;)Lock;

    move-result-object v1

    iput-object v1, v2, Lodg;->j:Lock;

    .line 69
    iput v8, v2, Lodg;->r:I

    .line 71
    new-instance v1, Loxz;

    invoke-direct {v1}, Loxz;-><init>()V

    iput-object v1, v2, Lodg;->t:Loxz;

    .line 72
    iget-object v1, v2, Lodg;->t:Loxz;

    iget-object v3, p0, Lklv;->q:Lkzl;

    iget-object v4, p0, Lklv;->f:Landroid/content/Context;

    iget v5, p0, Lklv;->a:I

    invoke-interface {v3, v4, v5}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v3

    iput-object v3, v1, Loxz;->a:[I

    .line 74
    iget-object v1, p0, Lklv;->c:Lhgw;

    invoke-virtual {v1}, Lhgw;->i()I

    move-result v1

    if-lez v1, :cond_1

    .line 75
    iget-object v1, p0, Lklv;->c:Lhgw;

    invoke-virtual {v1}, Lhgw;->c()[Lkxr;

    move-result-object v1

    array-length v3, v1

    .line 76
    new-array v1, v3, [Lodl;

    iput-object v1, v2, Lodg;->m:[Lodl;

    move v1, v0

    .line 77
    :goto_0
    if-ge v1, v3, :cond_1

    .line 78
    iget-object v4, p0, Lklv;->c:Lhgw;

    invoke-virtual {v4, v1}, Lhgw;->c(I)Lkxr;

    move-result-object v4

    .line 79
    new-instance v5, Lodl;

    invoke-direct {v5}, Lodl;-><init>()V

    .line 80
    invoke-virtual {v4}, Lkxr;->a()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lodl;->b:Ljava/lang/String;

    .line 81
    invoke-virtual {v4}, Lkxr;->c()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lodl;->c:Ljava/lang/String;

    .line 82
    invoke-virtual {v4}, Lkxr;->c()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing Square Stream Id."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    iget-object v4, v2, Lodg;->m:[Lodl;

    aput-object v5, v4, v1

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    :cond_1
    iget-object v1, p0, Lklv;->c:Lhgw;

    invoke-virtual {v1}, Lhgw;->j()I

    move-result v1

    if-lez v1, :cond_2

    .line 90
    new-instance v1, Lodj;

    invoke-direct {v1}, Lodj;-><init>()V

    .line 91
    iput v8, v1, Lodj;->b:I

    .line 92
    new-array v3, v7, [Ljava/lang/String;

    iget-object v4, p0, Lklv;->c:Lhgw;

    invoke-virtual {v4, v0}, Lhgw;->d(I)Lhxs;

    move-result-object v4

    invoke-virtual {v4}, Lhxs;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    iput-object v3, v1, Lodj;->c:[Ljava/lang/String;

    .line 94
    new-array v3, v7, [Lodj;

    aput-object v1, v3, v0

    iput-object v3, v2, Lodg;->n:[Lodj;

    .line 97
    :cond_2
    iget-object v1, p0, Lklv;->b:Lkjc;

    .line 98
    invoke-virtual {v1}, Lkjc;->A()[B

    move-result-object v1

    invoke-static {v1}, Llap;->a([B)Lnwr;

    move-result-object v1

    .line 99
    if-eqz v1, :cond_3

    .line 100
    new-instance v3, Logw;

    invoke-direct {v3}, Logw;-><init>()V

    iput-object v3, v2, Lodg;->u:Logw;

    .line 101
    iget-object v3, v2, Lodg;->u:Logw;

    iput-object v1, v3, Logw;->a:Lnwr;

    .line 104
    :cond_3
    iget-object v1, p0, Lklv;->f:Landroid/content/Context;

    const-class v3, Lkjb;

    .line 105
    invoke-static {v1, v3}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v3

    move v1, v0

    .line 107
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 108
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjb;

    iget-object v4, p0, Lklv;->b:Lkjc;

    invoke-interface {v0, v2}, Lkjb;->a(Lodg;)V

    .line 107
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 110
    :cond_4
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lmhi;

    invoke-virtual {p0, p1}, Lklv;->a(Lmhi;)V

    return-void
.end method

.method protected bridge synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 32
    return-void
.end method
