.class public final Lidm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lidm;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/TimeZone;

.field private b:J

.field private c:I


# direct methods
.method public constructor <init>(Ljava/util/TimeZone;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lidm;->a:Ljava/util/TimeZone;

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lidm;->c:I

    .line 35
    return-void
.end method


# virtual methods
.method public a(Lidm;)I
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lidm;->a:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lidm;->a:Ljava/util/TimeZone;

    .line 60
    invoke-virtual {v1}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public a()Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lidm;->a:Ljava/util/TimeZone;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Lidm;->c:I

    .line 47
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 50
    iput-wide p1, p0, Lidm;->b:J

    .line 51
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lidm;->c:I

    return v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lidm;->b:J

    return-wide v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 27
    check-cast p1, Lidm;

    invoke-virtual {p0, p1}, Lidm;->a(Lidm;)I

    move-result v0

    return v0
.end method
