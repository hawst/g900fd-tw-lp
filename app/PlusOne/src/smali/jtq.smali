.class public final Ljtq;
.super Llol;
.source "PG"

# interfaces
.implements Lhio;
.implements Lhyz;
.implements Ljqd;
.implements Lllh;


# instance fields
.field private N:Lhzb;

.field private O:I

.field private P:Ljpr;

.field private Q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljava/lang/String;

.field private S:Ljqe;

.field private T:Ljps;

.field private U:Z

.field private V:Lllg;

.field private W:Lhzl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Llol;-><init>()V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 195
    iget-boolean v0, p0, Ljtq;->U:Z

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 196
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 197
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 198
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    .line 199
    invoke-interface {v0}, Ljpv;->g()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 200
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 203
    :cond_1
    invoke-direct {p0, v2}, Ljtq;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljtq;->Q:Ljava/util/List;

    .line 208
    :goto_1
    iget-object v0, p0, Ljtq;->N:Lhzb;

    if-eqz v0, :cond_2

    .line 209
    iget-object v0, p0, Ljtq;->N:Lhzb;

    invoke-interface {v0}, Lhzb;->e()V

    .line 211
    :cond_2
    return-void

    .line 205
    :cond_3
    invoke-direct {p0, p1}, Ljtq;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljtq;->Q:Ljava/util/List;

    goto :goto_1
.end method

.method private c(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    if-nez p1, :cond_0

    .line 215
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 230
    :goto_0
    return-object v0

    .line 218
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 219
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 220
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    .line 221
    iget-object v1, p0, Ljtq;->W:Lhzl;

    instance-of v1, v1, Lhzk;

    if-eqz v1, :cond_1

    .line 222
    iget-object v1, p0, Ljtq;->W:Lhzl;

    check-cast v1, Lhzk;

    const/16 v4, 0xaa

    .line 223
    invoke-interface {v0}, Ljpv;->a()Ljava/lang/String;

    move-result-object v5

    .line 222
    invoke-interface {v1, v4, v5}, Lhzk;->a(ILjava/lang/String;)Z

    move-result v1

    .line 224
    if-nez v1, :cond_2

    .line 225
    :cond_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v3

    .line 230
    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    return v0
.end method

.method public a(I)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Ljtq;->Q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public a(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 151
    check-cast p2, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;

    iget-object v0, p0, Ljtq;->Q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->a(Ljpv;)V

    .line 152
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 55
    if-eqz p1, :cond_0

    .line 56
    const-string v0, "state_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljtq;->R:Ljava/lang/String;

    .line 58
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public a(Lhzb;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Ljtq;->N:Lhzb;

    .line 167
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 181
    if-eqz p1, :cond_0

    .line 182
    iget-object v0, p0, Ljtq;->R:Ljava/lang/String;

    .line 183
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ljtq;->R:Ljava/lang/String;

    .line 184
    iget-object v1, p0, Ljtq;->R:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Ljtq;->R:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ljtq;->b(Ljava/util/List;)V

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    invoke-virtual {p0}, Ljtq;->e()V

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    invoke-direct {p0, p1}, Ljtq;->b(Ljava/util/List;)V

    .line 236
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 239
    iput-boolean p1, p0, Ljtq;->U:Z

    .line 240
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ljtq;->b(Ljava/util/List;)V

    .line 241
    invoke-virtual {p0}, Ljtq;->e()V

    .line 242
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 114
    invoke-virtual {p0, v2}, Ljtq;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040172

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 82
    iget-object v0, p0, Ljtq;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Ljtq;->O:I

    .line 83
    iget-object v0, p0, Ljtq;->au:Llnh;

    const-class v1, Ljpr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpr;

    iput-object v0, p0, Ljtq;->P:Ljpr;

    .line 84
    iget-object v0, p0, Ljtq;->P:Ljpr;

    invoke-interface {v0}, Ljpr;->c()Ljps;

    move-result-object v0

    iput-object v0, p0, Ljtq;->T:Ljps;

    .line 85
    iget-object v0, p0, Ljtq;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Ljtq;->W:Lhzl;

    .line 87
    iget-object v0, p0, Ljtq;->au:Llnh;

    const-class v1, Lllg;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllg;

    iput-object v0, p0, Ljtq;->V:Lllg;

    .line 88
    iget-object v0, p0, Ljtq;->V:Lllg;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Ljtq;->V:Lllg;

    invoke-virtual {v0, p0}, Lllg;->a(Lllh;)V

    .line 92
    :cond_0
    iget-object v0, p0, Ljtq;->au:Llnh;

    const-class v1, Ljqe;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqe;

    iput-object v0, p0, Ljtq;->S:Ljqe;

    .line 93
    iget-object v0, p0, Ljtq;->S:Ljqe;

    if-nez v0, :cond_1

    .line 94
    new-instance v0, Ljqe;

    invoke-direct {v0}, Ljqe;-><init>()V

    iput-object v0, p0, Ljtq;->S:Ljqe;

    .line 97
    :cond_1
    iget-object v0, p0, Ljtq;->au:Llnh;

    const-class v1, Lhin;

    .line 98
    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhin;

    .line 99
    if-eqz v0, :cond_2

    .line 100
    invoke-virtual {v0, p0}, Lhin;->a(Lhio;)V

    .line 101
    invoke-virtual {v0}, Lhin;->a()Z

    move-result v0

    iput-boolean v0, p0, Ljtq;->U:Z

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljtq;->U:Z

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Ljtq;->Q:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ljtq;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Ljtq;->P:Ljpr;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Ljtq;->S:Ljqe;

    iget-object v1, p0, Ljtq;->R:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljqe;->b(Ljava/lang/String;)Ljqe;

    .line 173
    iget-object v0, p0, Ljtq;->R:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Ljtq;->P:Ljpr;

    iget v1, p0, Ljtq;->O:I

    iget-object v2, p0, Ljtq;->S:Ljqe;

    invoke-interface {v0, p0, v1, v2}, Ljpr;->a(Ljqd;ILjqe;)V

    .line 177
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 63
    const-string v0, "state_query"

    iget-object v1, p0, Ljtq;->R:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Llol;->g()V

    .line 69
    iget-object v0, p0, Ljtq;->T:Ljps;

    invoke-interface {v0}, Ljps;->d()V

    .line 70
    invoke-virtual {p0}, Ljtq;->e()V

    .line 71
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Llol;->h()V

    .line 76
    iget-object v0, p0, Ljtq;->T:Ljps;

    invoke-interface {v0}, Ljps;->e()V

    .line 77
    return-void
.end method
