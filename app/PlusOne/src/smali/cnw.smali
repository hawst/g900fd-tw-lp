.class public final Lcnw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;
.implements Llnx;
.implements Llrb;
.implements Llrc;
.implements Llrg;


# instance fields
.field private final a:Lz;

.field private final b:Z

.field private c:Lctz;

.field private d:Lhoc;

.field private e:Lhee;

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;Z)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    check-cast p1, Lz;

    iput-object p1, p0, Lcnw;->a:Lz;

    .line 53
    iput-boolean p3, p0, Lcnw;->b:Z

    .line 54
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 55
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 66
    iget-object v0, p0, Lcnw;->c:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v3

    .line 67
    invoke-virtual {v3}, Ljcn;->b()Z

    move-result v1

    .line 68
    if-eqz v1, :cond_0

    invoke-virtual {v3}, Ljcn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 72
    :goto_0
    iget-boolean v2, p0, Lcnw;->b:Z

    if-eqz v2, :cond_3

    .line 73
    if-eqz v0, :cond_1

    .line 74
    const v0, 0x7f11005a

    .line 80
    :goto_1
    sget-object v1, Lhmv;->eI:Lhmv;

    move v2, v0

    .line 91
    :goto_2
    iget-object v0, p0, Lcnw;->a:Lz;

    const-class v4, Lhms;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lcnw;->f:Landroid/content/Context;

    invoke-direct {v4, v5}, Lhmr;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 93
    const-class v0, Ldwu;

    invoke-virtual {v3, v0}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v3

    .line 94
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 96
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_6

    .line 97
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwu;

    invoke-virtual {v0}, Ldwu;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :cond_1
    if-eqz v1, :cond_2

    .line 76
    const v0, 0x7f11005b

    goto :goto_1

    .line 78
    :cond_2
    const v0, 0x7f11005c

    goto :goto_1

    .line 82
    :cond_3
    if-eqz v0, :cond_4

    .line 83
    const v0, 0x7f110057

    .line 89
    :goto_4
    sget-object v1, Lhmv;->eK:Lhmv;

    move v2, v0

    goto :goto_2

    .line 84
    :cond_4
    if-eqz v1, :cond_5

    .line 85
    const v0, 0x7f110058

    goto :goto_4

    .line 87
    :cond_5
    const v0, 0x7f110059

    goto :goto_4

    .line 100
    :cond_6
    new-instance v0, Ldpy;

    iget-object v1, p0, Lcnw;->a:Lz;

    .line 101
    iget-object v3, p0, Lcnw;->e:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget-boolean v5, p0, Lcnw;->b:Z

    invoke-direct {v0, v1, v3, v4, v5}, Ldpy;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Z)V

    .line 103
    iget-object v1, p0, Lcnw;->a:Lz;

    invoke-virtual {v1}, Lz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 104
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    .line 106
    new-instance v2, Lhpd;

    iget-object v3, p0, Lcnw;->a:Lz;

    iget-object v4, p0, Lcnw;->a:Lz;

    .line 107
    invoke-virtual {v4}, Lz;->f()Lae;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lhpd;-><init>(Landroid/content/Context;Lae;)V

    .line 108
    invoke-virtual {v0}, Ldpy;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lhos;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v1, p0, Lcnw;->d:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 110
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 59
    iput-object p1, p0, Lcnw;->f:Landroid/content/Context;

    .line 60
    const-class v0, Lctz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcnw;->c:Lctz;

    .line 61
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lcnw;->d:Lhoc;

    .line 62
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcnw;->e:Lhee;

    .line 63
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 125
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcnw;->a:Lz;

    invoke-virtual {v0}, Lz;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    .line 131
    const-string v1, "RemovePhotosFromTrashTask"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "restore"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-boolean v2, p0, Lcnw;->b:Z

    if-ne v2, v1, :cond_0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcnw;->a:Lz;

    if-eqz v1, :cond_2

    const v0, 0x7f0a07a7

    :goto_1
    const/4 v1, 0x0

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const v0, 0x7f0a07a6

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcnw;->c:Lctz;

    invoke-virtual {v0}, Lctz;->c()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcnw;->d:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 115
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcnw;->d:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->b(Lhob;)Lhoc;

    .line 120
    return-void
.end method
