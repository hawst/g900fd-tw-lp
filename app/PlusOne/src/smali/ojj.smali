.class public final Lojj;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lohp;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 554
    invoke-direct {p0}, Loxq;-><init>()V

    .line 557
    sget-object v0, Lohp;->a:[Lohp;

    iput-object v0, p0, Lojj;->a:[Lohp;

    .line 554
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 590
    .line 591
    iget-object v1, p0, Lojj;->a:[Lohp;

    if-eqz v1, :cond_1

    .line 592
    iget-object v2, p0, Lojj;->a:[Lohp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 593
    if-eqz v4, :cond_0

    .line 594
    const/4 v5, 0x1

    .line 595
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 592
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 599
    :cond_1
    iget-object v1, p0, Lojj;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 600
    const/4 v1, 0x2

    iget-object v2, p0, Lojj;->b:Ljava/lang/Boolean;

    .line 601
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 603
    :cond_2
    iget-object v1, p0, Lojj;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 604
    const/4 v1, 0x3

    iget-object v2, p0, Lojj;->c:Ljava/lang/Boolean;

    .line 605
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 607
    :cond_3
    iget-object v1, p0, Lojj;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 608
    const/4 v1, 0x4

    iget-object v2, p0, Lojj;->d:Ljava/lang/String;

    .line 609
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 611
    :cond_4
    iget-object v1, p0, Lojj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 612
    iput v0, p0, Lojj;->ai:I

    .line 613
    return v0
.end method

.method public a(Loxn;)Lojj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 621
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 622
    sparse-switch v0, :sswitch_data_0

    .line 626
    iget-object v2, p0, Lojj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 627
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lojj;->ah:Ljava/util/List;

    .line 630
    :cond_1
    iget-object v2, p0, Lojj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 632
    :sswitch_0
    return-object p0

    .line 637
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 638
    iget-object v0, p0, Lojj;->a:[Lohp;

    if-nez v0, :cond_3

    move v0, v1

    .line 639
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohp;

    .line 640
    iget-object v3, p0, Lojj;->a:[Lohp;

    if-eqz v3, :cond_2

    .line 641
    iget-object v3, p0, Lojj;->a:[Lohp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 643
    :cond_2
    iput-object v2, p0, Lojj;->a:[Lohp;

    .line 644
    :goto_2
    iget-object v2, p0, Lojj;->a:[Lohp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 645
    iget-object v2, p0, Lojj;->a:[Lohp;

    new-instance v3, Lohp;

    invoke-direct {v3}, Lohp;-><init>()V

    aput-object v3, v2, v0

    .line 646
    iget-object v2, p0, Lojj;->a:[Lohp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 647
    invoke-virtual {p1}, Loxn;->a()I

    .line 644
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 638
    :cond_3
    iget-object v0, p0, Lojj;->a:[Lohp;

    array-length v0, v0

    goto :goto_1

    .line 650
    :cond_4
    iget-object v2, p0, Lojj;->a:[Lohp;

    new-instance v3, Lohp;

    invoke-direct {v3}, Lohp;-><init>()V

    aput-object v3, v2, v0

    .line 651
    iget-object v2, p0, Lojj;->a:[Lohp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 655
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lojj;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 659
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lojj;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 663
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojj;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 622
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 568
    iget-object v0, p0, Lojj;->a:[Lohp;

    if-eqz v0, :cond_1

    .line 569
    iget-object v1, p0, Lojj;->a:[Lohp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 570
    if-eqz v3, :cond_0

    .line 571
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 569
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 575
    :cond_1
    iget-object v0, p0, Lojj;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 576
    const/4 v0, 0x2

    iget-object v1, p0, Lojj;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 578
    :cond_2
    iget-object v0, p0, Lojj;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 579
    const/4 v0, 0x3

    iget-object v1, p0, Lojj;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 581
    :cond_3
    iget-object v0, p0, Lojj;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 582
    const/4 v0, 0x4

    iget-object v1, p0, Lojj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 584
    :cond_4
    iget-object v0, p0, Lojj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 586
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 550
    invoke-virtual {p0, p1}, Lojj;->a(Loxn;)Lojj;

    move-result-object v0

    return-object v0
.end method
