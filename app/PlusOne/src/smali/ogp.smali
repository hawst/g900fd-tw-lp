.class public final Logp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Logp;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5335
    const/4 v0, 0x0

    new-array v0, v0, [Logp;

    sput-object v0, Logp;->a:[Logp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5336
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5349
    const/high16 v0, -0x80000000

    iput v0, p0, Logp;->d:I

    .line 5336
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5374
    const/4 v0, 0x0

    .line 5375
    iget-object v1, p0, Logp;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5376
    const/4 v0, 0x1

    iget-object v1, p0, Logp;->b:Ljava/lang/String;

    .line 5377
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5379
    :cond_0
    iget-object v1, p0, Logp;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5380
    const/4 v1, 0x2

    iget-object v2, p0, Logp;->c:Ljava/lang/String;

    .line 5381
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5383
    :cond_1
    iget v1, p0, Logp;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 5384
    const/4 v1, 0x3

    iget v2, p0, Logp;->d:I

    .line 5385
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5387
    :cond_2
    iget-object v1, p0, Logp;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5388
    const/4 v1, 0x4

    iget-object v2, p0, Logp;->e:Ljava/lang/String;

    .line 5389
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5391
    :cond_3
    iget-object v1, p0, Logp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5392
    iput v0, p0, Logp;->ai:I

    .line 5393
    return v0
.end method

.method public a(Loxn;)Logp;
    .locals 2

    .prologue
    .line 5401
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5402
    sparse-switch v0, :sswitch_data_0

    .line 5406
    iget-object v1, p0, Logp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5407
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Logp;->ah:Ljava/util/List;

    .line 5410
    :cond_1
    iget-object v1, p0, Logp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5412
    :sswitch_0
    return-object p0

    .line 5417
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logp;->b:Ljava/lang/String;

    goto :goto_0

    .line 5421
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logp;->c:Ljava/lang/String;

    goto :goto_0

    .line 5425
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5426
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 5429
    :cond_2
    iput v0, p0, Logp;->d:I

    goto :goto_0

    .line 5431
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Logp;->d:I

    goto :goto_0

    .line 5436
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logp;->e:Ljava/lang/String;

    goto :goto_0

    .line 5402
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5356
    iget-object v0, p0, Logp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 5357
    const/4 v0, 0x1

    iget-object v1, p0, Logp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5359
    :cond_0
    iget-object v0, p0, Logp;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5360
    const/4 v0, 0x2

    iget-object v1, p0, Logp;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5362
    :cond_1
    iget v0, p0, Logp;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 5363
    const/4 v0, 0x3

    iget v1, p0, Logp;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5365
    :cond_2
    iget-object v0, p0, Logp;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 5366
    const/4 v0, 0x4

    iget-object v1, p0, Logp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5368
    :cond_3
    iget-object v0, p0, Logp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5370
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5332
    invoke-virtual {p0, p1}, Logp;->a(Loxn;)Logp;

    move-result-object v0

    return-object v0
.end method
