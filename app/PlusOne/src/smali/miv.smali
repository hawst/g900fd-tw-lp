.class public final Lmiv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmlv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32884
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32887
    iput-object v0, p0, Lmiv;->apiHeader:Llyr;

    .line 32890
    iput-object v0, p0, Lmiv;->a:Lmlv;

    .line 32884
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32907
    const/4 v0, 0x0

    .line 32908
    iget-object v1, p0, Lmiv;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 32909
    const/4 v0, 0x1

    iget-object v1, p0, Lmiv;->apiHeader:Llyr;

    .line 32910
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32912
    :cond_0
    iget-object v1, p0, Lmiv;->a:Lmlv;

    if-eqz v1, :cond_1

    .line 32913
    const/4 v1, 0x2

    iget-object v2, p0, Lmiv;->a:Lmlv;

    .line 32914
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32916
    :cond_1
    iget-object v1, p0, Lmiv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32917
    iput v0, p0, Lmiv;->ai:I

    .line 32918
    return v0
.end method

.method public a(Loxn;)Lmiv;
    .locals 2

    .prologue
    .line 32926
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32927
    sparse-switch v0, :sswitch_data_0

    .line 32931
    iget-object v1, p0, Lmiv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32932
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmiv;->ah:Ljava/util/List;

    .line 32935
    :cond_1
    iget-object v1, p0, Lmiv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32937
    :sswitch_0
    return-object p0

    .line 32942
    :sswitch_1
    iget-object v0, p0, Lmiv;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 32943
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmiv;->apiHeader:Llyr;

    .line 32945
    :cond_2
    iget-object v0, p0, Lmiv;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32949
    :sswitch_2
    iget-object v0, p0, Lmiv;->a:Lmlv;

    if-nez v0, :cond_3

    .line 32950
    new-instance v0, Lmlv;

    invoke-direct {v0}, Lmlv;-><init>()V

    iput-object v0, p0, Lmiv;->a:Lmlv;

    .line 32952
    :cond_3
    iget-object v0, p0, Lmiv;->a:Lmlv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32927
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32895
    iget-object v0, p0, Lmiv;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 32896
    const/4 v0, 0x1

    iget-object v1, p0, Lmiv;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32898
    :cond_0
    iget-object v0, p0, Lmiv;->a:Lmlv;

    if-eqz v0, :cond_1

    .line 32899
    const/4 v0, 0x2

    iget-object v1, p0, Lmiv;->a:Lmlv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32901
    :cond_1
    iget-object v0, p0, Lmiv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32903
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 32880
    invoke-virtual {p0, p1}, Lmiv;->a(Loxn;)Lmiv;

    move-result-object v0

    return-object v0
.end method
