.class public final Ljky;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PG"


# direct methods
.method protected constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 19
    const-string v0, "NotificationsDB"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Ljky;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 24
    const/4 v0, 0x0

    const/16 v1, 0xa

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 25
    return-void
.end method


# virtual methods
.method protected a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 64
    invoke-static {p1}, Ljku;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 65
    invoke-static {p1}, Ljku;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 66
    invoke-virtual {p0, p1}, Ljky;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 67
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 29
    const-string v0, "CREATE TABLE notifications(_id  INTEGER PRIMARY KEY, key TEXT UNIQUE NOT NULL, priority INT NOT NULL DEFAULT(0), read_state INT NOT NULL DEFAULT(0), sort_version INT NOT NULL DEFAULT(0), last_modified_version INT NOT NULL DEFAULT(0), push_enabled INT NOT NULL DEFAULT(0), creation_time INT NOT NULL DEFAULT(0), category TEXT, default_destination TEXT, payload BLOB, simple_collapsed_layout BLOB, expanded_info BLOB );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 30
    const-string v0, "CREATE TABLE user_data (_id INTEGER PRIMARY KEY, user_id TEXT, unread_count INT DEFAULT(0), unread_count_string STRING NOT NULL DEFAULT(\'0\'), has_unread BOOLEAN DEFAULT(0), sync_version INT DEFAULT(0), viewed_sync_version INT DEFAULT(0), important_fetch_paging_token BLOB, low_fetch_paging_token BLOB, important_sync_token BLOB, low_sync_token BLOB, view_id TEXT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    const-string v0, "CREATE TABLE statistics (_id INTEGER PRIMARY KEY, time INTEGER NOT NULL, log INTEGER NOT NULL, comment TEXT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Ljky;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 61
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 8

    .prologue
    const/16 v0, 0x8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 37
    const/4 v1, 0x7

    if-ge p2, v1, :cond_1

    .line 38
    invoke-virtual {p0, p1}, Ljky;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    if-ge p2, v0, :cond_3

    .line 43
    const-string v1, "ALTER TABLE %s ADD COLUMN %s %s"

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "notifications"

    aput-object v3, v2, v4

    const-string v3, "category"

    aput-object v3, v2, v5

    const-string v3, "TEXT"

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 47
    :goto_1
    const/16 v1, 0x9

    if-ge v0, v1, :cond_2

    .line 48
    const-string v0, "ALTER TABLE %s ADD COLUMN %s %s"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "user_data"

    aput-object v2, v1, v4

    const-string v2, "viewed_sync_version"

    aput-object v2, v1, v5

    const-string v2, "INT DEFAULT(0)"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 49
    const/16 v0, 0x9

    .line 52
    :cond_2
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 53
    const-string v0, "DROP TABLE IF EXISTS statistics"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE statistics (_id INTEGER PRIMARY KEY, time INTEGER NOT NULL, log INTEGER NOT NULL, comment TEXT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v0, p2

    goto :goto_1
.end method
