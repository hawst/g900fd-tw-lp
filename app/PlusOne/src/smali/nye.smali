.class public final Lnye;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnye;


# instance fields
.field private b:Ljava/lang/String;

.field private c:[Lnys;

.field private d:[Lnyz;

.field private e:[Lnzm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4123
    const/4 v0, 0x0

    new-array v0, v0, [Lnye;

    sput-object v0, Lnye;->a:[Lnye;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4124
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4129
    sget-object v0, Lnys;->a:[Lnys;

    iput-object v0, p0, Lnye;->c:[Lnys;

    .line 4132
    sget-object v0, Lnyz;->a:[Lnyz;

    iput-object v0, p0, Lnye;->d:[Lnyz;

    .line 4135
    sget-object v0, Lnzm;->a:[Lnzm;

    iput-object v0, p0, Lnye;->e:[Lnzm;

    .line 4124
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 4170
    .line 4171
    iget-object v0, p0, Lnye;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 4172
    const/4 v0, 0x1

    iget-object v2, p0, Lnye;->b:Ljava/lang/String;

    .line 4173
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4175
    :goto_0
    iget-object v2, p0, Lnye;->c:[Lnys;

    if-eqz v2, :cond_1

    .line 4176
    iget-object v3, p0, Lnye;->c:[Lnys;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 4177
    if-eqz v5, :cond_0

    .line 4178
    const/4 v6, 0x2

    .line 4179
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4176
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4183
    :cond_1
    iget-object v2, p0, Lnye;->d:[Lnyz;

    if-eqz v2, :cond_3

    .line 4184
    iget-object v3, p0, Lnye;->d:[Lnyz;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 4185
    if-eqz v5, :cond_2

    .line 4186
    const/4 v6, 0x3

    .line 4187
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4184
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 4191
    :cond_3
    iget-object v2, p0, Lnye;->e:[Lnzm;

    if-eqz v2, :cond_5

    .line 4192
    iget-object v2, p0, Lnye;->e:[Lnzm;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 4193
    if-eqz v4, :cond_4

    .line 4194
    const/4 v5, 0x4

    .line 4195
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4192
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 4199
    :cond_5
    iget-object v1, p0, Lnye;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4200
    iput v0, p0, Lnye;->ai:I

    .line 4201
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnye;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4209
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4210
    sparse-switch v0, :sswitch_data_0

    .line 4214
    iget-object v2, p0, Lnye;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4215
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnye;->ah:Ljava/util/List;

    .line 4218
    :cond_1
    iget-object v2, p0, Lnye;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4220
    :sswitch_0
    return-object p0

    .line 4225
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnye;->b:Ljava/lang/String;

    goto :goto_0

    .line 4229
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4230
    iget-object v0, p0, Lnye;->c:[Lnys;

    if-nez v0, :cond_3

    move v0, v1

    .line 4231
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnys;

    .line 4232
    iget-object v3, p0, Lnye;->c:[Lnys;

    if-eqz v3, :cond_2

    .line 4233
    iget-object v3, p0, Lnye;->c:[Lnys;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4235
    :cond_2
    iput-object v2, p0, Lnye;->c:[Lnys;

    .line 4236
    :goto_2
    iget-object v2, p0, Lnye;->c:[Lnys;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 4237
    iget-object v2, p0, Lnye;->c:[Lnys;

    new-instance v3, Lnys;

    invoke-direct {v3}, Lnys;-><init>()V

    aput-object v3, v2, v0

    .line 4238
    iget-object v2, p0, Lnye;->c:[Lnys;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4239
    invoke-virtual {p1}, Loxn;->a()I

    .line 4236
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4230
    :cond_3
    iget-object v0, p0, Lnye;->c:[Lnys;

    array-length v0, v0

    goto :goto_1

    .line 4242
    :cond_4
    iget-object v2, p0, Lnye;->c:[Lnys;

    new-instance v3, Lnys;

    invoke-direct {v3}, Lnys;-><init>()V

    aput-object v3, v2, v0

    .line 4243
    iget-object v2, p0, Lnye;->c:[Lnys;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4247
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4248
    iget-object v0, p0, Lnye;->d:[Lnyz;

    if-nez v0, :cond_6

    move v0, v1

    .line 4249
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyz;

    .line 4250
    iget-object v3, p0, Lnye;->d:[Lnyz;

    if-eqz v3, :cond_5

    .line 4251
    iget-object v3, p0, Lnye;->d:[Lnyz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4253
    :cond_5
    iput-object v2, p0, Lnye;->d:[Lnyz;

    .line 4254
    :goto_4
    iget-object v2, p0, Lnye;->d:[Lnyz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 4255
    iget-object v2, p0, Lnye;->d:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 4256
    iget-object v2, p0, Lnye;->d:[Lnyz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4257
    invoke-virtual {p1}, Loxn;->a()I

    .line 4254
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 4248
    :cond_6
    iget-object v0, p0, Lnye;->d:[Lnyz;

    array-length v0, v0

    goto :goto_3

    .line 4260
    :cond_7
    iget-object v2, p0, Lnye;->d:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 4261
    iget-object v2, p0, Lnye;->d:[Lnyz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4265
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4266
    iget-object v0, p0, Lnye;->e:[Lnzm;

    if-nez v0, :cond_9

    move v0, v1

    .line 4267
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzm;

    .line 4268
    iget-object v3, p0, Lnye;->e:[Lnzm;

    if-eqz v3, :cond_8

    .line 4269
    iget-object v3, p0, Lnye;->e:[Lnzm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4271
    :cond_8
    iput-object v2, p0, Lnye;->e:[Lnzm;

    .line 4272
    :goto_6
    iget-object v2, p0, Lnye;->e:[Lnzm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 4273
    iget-object v2, p0, Lnye;->e:[Lnzm;

    new-instance v3, Lnzm;

    invoke-direct {v3}, Lnzm;-><init>()V

    aput-object v3, v2, v0

    .line 4274
    iget-object v2, p0, Lnye;->e:[Lnzm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4275
    invoke-virtual {p1}, Loxn;->a()I

    .line 4272
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 4266
    :cond_9
    iget-object v0, p0, Lnye;->e:[Lnzm;

    array-length v0, v0

    goto :goto_5

    .line 4278
    :cond_a
    iget-object v2, p0, Lnye;->e:[Lnzm;

    new-instance v3, Lnzm;

    invoke-direct {v3}, Lnzm;-><init>()V

    aput-object v3, v2, v0

    .line 4279
    iget-object v2, p0, Lnye;->e:[Lnzm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4210
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 4140
    iget-object v1, p0, Lnye;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4141
    const/4 v1, 0x1

    iget-object v2, p0, Lnye;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 4143
    :cond_0
    iget-object v1, p0, Lnye;->c:[Lnys;

    if-eqz v1, :cond_2

    .line 4144
    iget-object v2, p0, Lnye;->c:[Lnys;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 4145
    if-eqz v4, :cond_1

    .line 4146
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 4144
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4150
    :cond_2
    iget-object v1, p0, Lnye;->d:[Lnyz;

    if-eqz v1, :cond_4

    .line 4151
    iget-object v2, p0, Lnye;->d:[Lnyz;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 4152
    if-eqz v4, :cond_3

    .line 4153
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 4151
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4157
    :cond_4
    iget-object v1, p0, Lnye;->e:[Lnzm;

    if-eqz v1, :cond_6

    .line 4158
    iget-object v1, p0, Lnye;->e:[Lnzm;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 4159
    if-eqz v3, :cond_5

    .line 4160
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 4158
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4164
    :cond_6
    iget-object v0, p0, Lnye;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4166
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4120
    invoke-virtual {p0, p1}, Lnye;->a(Loxn;)Lnye;

    move-result-object v0

    return-object v0
.end method
