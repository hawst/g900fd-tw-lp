.class public Lfdj;
.super Lhya;
.source "PG"

# interfaces
.implements Lgca;
.implements Llhc;


# static fields
.field private static a:Lhym;

.field private static b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Landroid/view/animation/Interpolator;


# instance fields
.field private A:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private B:Liax;

.field private C:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lfzf;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkyo;",
            ">;"
        }
    .end annotation
.end field

.field private final E:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final F:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnqp;",
            ">;"
        }
    .end annotation
.end field

.field private G:Z

.field private H:Z

.field private I:Llij;

.field private J:Z

.field private K:Ljava/lang/String;

.field private L:Landroid/view/View;

.field private d:Z

.field private e:Lfdp;

.field private f:Lfdn;

.field public final g:I

.field public h:Levp;

.field public i:I

.field public j:Llcr;

.field private l:Llci;

.field private final m:Lgl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgl",
            "<",
            "Ljava/lang/String;",
            "Lfeu;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/lang/Runnable;

.field private p:Z

.field private q:Llhc;

.field private r:Landroid/util/SparseIntArray;

.field private s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:I

.field private u:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lleb;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private z:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lfdj;->c:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 180
    invoke-direct {p0, p1}, Lhya;-><init>(Landroid/content/Context;)V

    .line 124
    iput-boolean v2, p0, Lfdj;->p:Z

    .line 128
    const/high16 v0, -0x80000000

    iput v0, p0, Lfdj;->i:I

    .line 131
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lfdj;->r:Landroid/util/SparseIntArray;

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    .line 134
    const/4 v0, -0x1

    iput v0, p0, Lfdj;->t:I

    .line 135
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfdj;->u:Ljava/util/HashSet;

    .line 138
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfdj;->v:Ljava/util/HashSet;

    .line 139
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfdj;->w:Ljava/util/HashSet;

    .line 140
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfdj;->x:Ljava/util/HashSet;

    .line 141
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfdj;->y:Ljava/util/HashMap;

    .line 143
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfdj;->z:Ljava/util/HashMap;

    .line 145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfdj;->A:Ljava/util/Map;

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfdj;->C:Ljava/util/ArrayList;

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfdj;->D:Ljava/util/ArrayList;

    .line 155
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfdj;->E:Ljava/util/HashSet;

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfdj;->F:Ljava/util/ArrayList;

    .line 182
    invoke-virtual {p0, v2, v2}, Lfdj;->b(ZZ)V

    .line 183
    invoke-virtual {p0, v2, v2}, Lfdj;->b(ZZ)V

    .line 184
    invoke-virtual {p0, v2, v2}, Lfdj;->b(ZZ)V

    .line 186
    iput p4, p0, Lfdj;->g:I

    .line 187
    iput-object p5, p0, Lfdj;->f:Lfdn;

    .line 188
    iput-object p6, p0, Lfdj;->e:Lfdp;

    .line 189
    iput-object p8, p0, Lfdj;->l:Llci;

    .line 190
    new-instance v0, Lgl;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lgl;-><init>(I)V

    iput-object v0, p0, Lfdj;->m:Lgl;

    .line 191
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfdj;->n:Ljava/util/HashSet;

    .line 192
    new-instance v0, Lfdk;

    invoke-direct {v0, p0}, Lfdk;-><init>(Lfdj;)V

    iput-object v0, p0, Lfdj;->o:Ljava/lang/Runnable;

    .line 207
    iput-boolean v2, p0, Lfdj;->d:Z

    .line 208
    iput-boolean v3, p0, Lfdj;->G:Z

    .line 210
    iput-boolean v3, p0, Lfdj;->J:Z

    .line 211
    iput-object p7, p0, Lfdj;->h:Levp;

    .line 213
    iput-object p3, p0, Lfdj;->j:Llcr;

    .line 215
    iput-object v4, p0, Lfdj;->K:Ljava/lang/String;

    .line 216
    iput-object v4, p0, Lfdj;->L:Landroid/view/View;

    .line 218
    const-class v0, Liax;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liax;

    iput-object v0, p0, Lfdj;->B:Liax;

    .line 220
    new-instance v0, Lfdl;

    invoke-direct {v0, p0}, Lfdl;-><init>(Lfdj;)V

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Llea;)V

    .line 240
    return-void
.end method

.method static synthetic a(Lfdj;)Lgl;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lfdj;->m:Lgl;

    return-object v0
.end method

.method private static a(Lkzj;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 536
    invoke-interface {p0}, Lkzj;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lfdj;Z)Z
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lfdj;->p:Z

    return p1
.end method

.method public static ar()Lhym;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1073
    sget-object v0, Lfdj;->a:Lhym;

    if-nez v0, :cond_0

    .line 1074
    new-instance v0, Lhym;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    .line 1075
    sput-object v0, Lfdj;->a:Lhym;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    .line 1077
    :cond_0
    sget-object v0, Lfdj;->a:Lhym;

    return-object v0
.end method

.method private b()Ljava/util/HashMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 544
    sget-object v0, Lfdj;->b:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 545
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lfdj;->b:Ljava/util/HashMap;

    .line 547
    invoke-virtual {p0}, Lfdj;->as()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    .line 548
    invoke-interface {v0}, Lkzl;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 550
    const/4 v0, 0x2

    move v2, v3

    move v1, v0

    .line 551
    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 552
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzj;

    .line 553
    invoke-interface {v0}, Lkzj;->a()Ljava/util/ArrayList;

    move-result-object v7

    move v5, v3

    move v4, v1

    .line 554
    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v5, v1, :cond_0

    .line 555
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lfdj;->a(Lkzj;I)Ljava/lang/String;

    move-result-object v1

    .line 556
    sget-object v8, Lfdj;->b:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 557
    add-int/lit8 v4, v4, 0x1

    .line 554
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 551
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v1, v4

    goto :goto_0

    .line 561
    :cond_1
    sget-object v0, Lfdj;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic b(Lfdj;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lfdj;->n:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic c(Lfdj;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lfdj;->F:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lfdj;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lfdj;->C:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lfdj;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lfdj;->D:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method protected a(II)I
    .locals 1

    .prologue
    .line 617
    packed-switch p1, :pswitch_data_0

    .line 627
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 619
    :pswitch_0
    invoke-virtual {p0, p2}, Lfdj;->g(I)I

    move-result v0

    goto :goto_0

    .line 623
    :pswitch_1
    invoke-virtual {p0, p2}, Lfdj;->c(I)I

    move-result v0

    goto :goto_0

    .line 617
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 568
    packed-switch p2, :pswitch_data_0

    .line 581
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 570
    :pswitch_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Lldx;

    invoke-static {p1}, Ljgh;->a(Landroid/content/Context;)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v1, v2}, Lldx;-><init>(I)V

    new-instance v2, Llcr;

    invoke-direct {v2, p1}, Llcr;-><init>(Landroid/content/Context;)V

    iget v3, v2, Llcr;->f:I

    neg-int v3, v3

    iget v4, v2, Llcr;->d:I

    neg-int v4, v4

    iget v5, v2, Llcr;->f:I

    neg-int v5, v5

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Lldx;->setMargins(IIII)V

    iget v2, v2, Llcr;->a:I

    iput v2, v1, Lldx;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 573
    :pswitch_1
    invoke-virtual {p0, p1, p3}, Lfdj;->a(Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 577
    :pswitch_2
    invoke-virtual {p0, p1, p3, p5}, Lfdj;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 568
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 604
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 605
    const/16 v1, 0xa

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 607
    invoke-static {p1, v0, v2, v3}, Lfvh;->a(Landroid/content/Context;Ljava/lang/String;J)Landroid/view/ViewGroup;

    move-result-object v1

    .line 608
    instance-of v0, v1, Llch;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 609
    check-cast v0, Llch;

    iget-object v2, p0, Lfdj;->l:Llci;

    invoke-virtual {v0, v2}, Llch;->a(Llci;)V

    .line 612
    :cond_0
    return-object v1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 901
    iget-object v0, p0, Lfdj;->y:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 902
    iget-object v0, p0, Lfdj;->y:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 906
    :goto_0
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 907
    iget-object v1, p0, Lfdj;->y:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 909
    return-object v0

    .line 904
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 499
    invoke-virtual {p0, v1}, Lfdj;->l(I)Landroid/database/Cursor;

    move-result-object v2

    .line 502
    if-nez p1, :cond_3

    move v3, v1

    :goto_0
    if-nez v2, :cond_4

    move v2, v1

    :goto_1
    xor-int/2addr v2, v3

    if-eqz v2, :cond_0

    move v0, v1

    .line 507
    :cond_0
    const-string v2, "StreamAdapter"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 508
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "changeStreamHeaderCursor cursorChanged="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 511
    :cond_1
    invoke-super {p0, v1, p1}, Lhya;->a(ILandroid/database/Cursor;)V

    .line 512
    if-eqz v0, :cond_2

    .line 513
    const/4 v0, -0x1

    invoke-virtual {p0, v1, v0}, Lfdj;->a(ZI)V

    .line 515
    :cond_2
    return-void

    :cond_3
    move v3, v0

    .line 502
    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1
.end method

.method public a(Landroid/database/Cursor;I)V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 742
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 743
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 745
    :cond_0
    const/16 v0, 0x24

    .line 746
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 745
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 747
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 749
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 753
    :cond_1
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_a

    .line 754
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_a

    .line 756
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v4

    :goto_0
    if-ge v2, v5, :cond_c

    .line 757
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 759
    const/4 p2, -0x1

    .line 761
    const-string v0, "StreamAdapter"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 762
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x3b

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "recalculateStreamHash: changed because of index "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 763
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v6, v4

    .line 767
    :goto_1
    if-ge v6, v5, :cond_3

    .line 768
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 769
    iget-object v1, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 771
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, ""

    :goto_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 770
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 771
    :cond_2
    const-string v2, " *"

    goto :goto_2

    .line 775
    :cond_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v5

    .line 776
    :goto_3
    if-ge v1, v2, :cond_5

    .line 777
    const-string v5, "  "

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 776
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 777
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    move v0, v3

    .line 790
    :goto_5
    if-nez v0, :cond_6

    if-ltz p2, :cond_7

    :cond_6
    move v4, v3

    :cond_7
    invoke-virtual {p0, v4, p2}, Lfdj;->a(ZI)V

    .line 792
    iget-object v1, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 793
    iput-object v7, p0, Lfdj;->s:Ljava/util/ArrayList;

    .line 795
    if-eqz v0, :cond_8

    .line 796
    iget-object v0, p0, Lfdj;->v:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 797
    iget-object v0, p0, Lfdj;->w:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 798
    iget-object v0, p0, Lfdj;->x:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 799
    iget-object v0, p0, Lfdj;->y:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 800
    iget-object v0, p0, Lfdj;->z:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 802
    :cond_8
    return-void

    .line 756
    :cond_9
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_a
    move v0, v3

    .line 784
    goto :goto_5

    :cond_b
    move v0, v3

    goto :goto_5

    :cond_c
    move v0, v4

    goto :goto_5
.end method

.method public a(Landroid/view/View;I)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1025
    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    .line 1026
    iget-object v1, p0, Lfdj;->h:Levp;

    if-eqz v1, :cond_0

    .line 1027
    iget-object v1, p0, Lfdj;->h:Levp;

    invoke-virtual {v1, p1, p2}, Levp;->a(Landroid/view/View;I)V

    .line 1030
    :cond_0
    iget-object v1, p0, Lfdj;->C:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v4

    .line 1031
    :goto_0
    if-ge v5, v6, :cond_1

    .line 1032
    iget-object v1, p0, Lfdj;->C:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfzf;

    .line 1033
    iget-object v7, p0, Lfdj;->B:Liax;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-interface {v7, v1, v2}, Liax;->a(Libe;Landroid/view/View;)V

    .line 1031
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    .line 1036
    :cond_1
    iget-object v1, p0, Lfdj;->D:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v4

    .line 1037
    :goto_1
    if-ge v5, v6, :cond_2

    .line 1038
    iget-object v1, p0, Lfdj;->D:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkyo;

    .line 1039
    iget-object v7, p0, Lfdj;->B:Liax;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-interface {v7, v1, v2}, Liax;->a(Libe;Landroid/view/View;)V

    .line 1037
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 1042
    :cond_2
    if-nez p2, :cond_4

    move v1, v3

    :goto_2
    iput-boolean v1, p0, Lfdj;->G:Z

    .line 1043
    iget-boolean v1, p0, Lfdj;->G:Z

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    .line 1044
    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v6

    move v5, v4

    :goto_3
    if-ge v5, v6, :cond_6

    .line 1045
    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1046
    instance-of v2, v1, Lldq;

    if-eqz v2, :cond_3

    .line 1047
    check-cast v1, Lldq;

    iget-boolean v2, p0, Lfdj;->G:Z

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lfdj;->H:Z

    if-eqz v2, :cond_5

    move v2, v3

    :goto_4
    invoke-virtual {v1, v2}, Lldq;->e(Z)V

    .line 1044
    :cond_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_3

    :cond_4
    move v1, v4

    .line 1042
    goto :goto_2

    :cond_5
    move v2, v4

    .line 1047
    goto :goto_4

    .line 1052
    :cond_6
    iget-object v0, p0, Lfdj;->I:Llij;

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lfdj;->G:Z

    if-eqz v0, :cond_7

    .line 1053
    iget-object v0, p0, Lfdj;->I:Llij;

    .line 1055
    :cond_7
    return-void
.end method

.method public a(Landroid/view/View;III)V
    .locals 1

    .prologue
    .line 1059
    iget-object v0, p0, Lfdj;->h:Levp;

    if-eqz v0, :cond_0

    .line 1060
    iget-object v0, p0, Lfdj;->h:Levp;

    invoke-virtual {v0, p1, p2, p3, p4}, Levp;->a(Landroid/view/View;III)V

    .line 1063
    :cond_0
    iget-object v0, p0, Lfdj;->q:Llhc;

    if-eqz v0, :cond_1

    .line 1064
    iget-object v0, p0, Lfdj;->q:Llhc;

    invoke-interface {v0, p1, p2, p3, p4}, Llhc;->a(Landroid/view/View;III)V

    .line 1066
    :cond_1
    return-void
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 280
    instance-of v0, p1, Lkdd;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 281
    check-cast v0, Lkdd;

    invoke-interface {v0}, Lkdd;->c()V

    .line 283
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 295
    :goto_0
    instance-of v0, p1, Lkdd;

    if-eqz v0, :cond_1

    .line 296
    check-cast p1, Lkdd;

    invoke-interface {p1}, Lkdd;->b()V

    .line 298
    :cond_1
    return-void

    .line 285
    :pswitch_0
    invoke-virtual {p0, p1, p3}, Lfdj;->a(Landroid/view/View;Landroid/database/Cursor;)V

    goto :goto_0

    .line 290
    :pswitch_1
    invoke-virtual {p0, p1, p3, p5}, Lfdj;->a(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V

    goto :goto_0

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 304
    return-void
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 307
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 308
    invoke-virtual {p0, v8}, Lfdj;->o(I)I

    move-result v0

    add-int v5, v1, v0

    .line 316
    instance-of v0, p1, Lldq;

    if-eqz v0, :cond_a

    .line 317
    iget-object v0, p0, Lfdj;->r:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    if-ne v0, v3, :cond_0

    check-cast p3, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {p3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d()I

    move-result v0

    iget-object v3, p0, Lfdj;->r:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    :cond_0
    move v1, v0

    instance-of v0, p1, Lgbz;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lgbz;

    iget-object v3, p0, Lfdj;->e:Lfdp;

    invoke-virtual {v0, v3}, Lgbz;->a(Lfdp;)Lgbz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lgbz;->a(Lgca;)Lgbz;

    :cond_1
    move-object v0, p1

    check-cast v0, Lldq;

    iget-object v3, p0, Lfdj;->j:Llcr;

    invoke-virtual {v0, p2, v3, v1, v2}, Lldq;->a(Landroid/database/Cursor;Llcr;II)Lldq;

    instance-of v1, p1, Lgbz;

    if-eqz v1, :cond_6

    move-object v1, p1

    check-cast v1, Lgbz;

    iget-object v2, p0, Lfdj;->w:Ljava/util/HashSet;

    invoke-virtual {v1}, Lgbz;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lgbz;->c(Z)V

    iget-object v2, p0, Lfdj;->x:Ljava/util/HashSet;

    invoke-virtual {v1}, Lgbz;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lgbz;->i()V

    :cond_2
    invoke-virtual {v1}, Lgbz;->s()Lkzp;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lkzp;->l()Llao;

    move-result-object v2

    invoke-virtual {v1}, Lgbz;->t()Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Llao;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Llao;->a()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lfdj;->v:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lfdj;->as()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lfdj;->g:I

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/service/EsService;->e(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/Integer;

    iget-object v2, p0, Lfdj;->v:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_0
    new-instance v1, Lldx;

    const/4 v2, -0x2

    invoke-direct {v1, v2}, Lldx;-><init>(I)V

    invoke-virtual {v0}, Lldq;->x()I

    move-result v2

    iput v2, v1, Lldx;->a:I

    invoke-virtual {v0, v1}, Lldq;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lfdj;->j:Llcr;

    invoke-virtual {v0, v1}, Lldq;->a(Lhuk;)V

    iget-object v1, p0, Lfdj;->K:Ljava/lang/String;

    invoke-virtual {v0}, Lldq;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iput-object p1, p0, Lfdj;->L:Landroid/view/View;

    .line 326
    :cond_4
    :goto_1
    invoke-virtual {p0, p1, v5}, Lfdj;->b(Landroid/view/View;I)V

    .line 328
    iget-object v0, p0, Lfdj;->f:Lfdn;

    if-eqz v0, :cond_5

    .line 329
    iget-object v0, p0, Lfdj;->f:Lfdn;

    invoke-interface {v0, v5}, Lfdn;->f(I)V

    .line 331
    :cond_5
    return-void

    .line 317
    :cond_6
    instance-of v1, p1, Llch;

    if-eqz v1, :cond_3

    move-object v1, p1

    check-cast v1, Llch;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "extra_promo_group_id"

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "extra_promo_type"

    invoke-virtual {v1}, Llch;->k()I

    move-result v1

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v4, p0, Lfdj;->g:I

    iget-object v1, p0, Lfdj;->k:Landroid/content/Context;

    const-class v6, Lhms;

    invoke-static {v1, v6}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhms;

    new-instance v6, Lhmr;

    iget-object v7, p0, Lfdj;->k:Landroid/content/Context;

    invoke-direct {v6, v7, v4}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v4, Lhmv;->ad:Lhmv;

    invoke-virtual {v6, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v4

    invoke-virtual {v4, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v3

    invoke-interface {v1, v3}, Lhms;->a(Lhmr;)V

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    instance-of v1, p1, Lfzf;

    if-eqz v1, :cond_8

    move-object v1, p1

    check-cast v1, Lfzf;

    iget-object v3, p0, Lfdj;->C:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v2

    :goto_2
    invoke-virtual {v1}, Lfzf;->getChildCount()I

    move-result v2

    if-ge v4, v2, :cond_9

    invoke-virtual {v1, v4}, Lfzf;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lgbl;

    if-eqz v3, :cond_7

    check-cast v2, Lgbl;

    iget-object v3, p0, Lfdj;->A:Ljava/util/Map;

    invoke-virtual {v2}, Lgbl;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lfdj;->A:Ljava/util/Map;

    invoke-virtual {v2}, Lgbl;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {v2, v3}, Lgbl;->a(Ljava/util/List;)V

    :cond_7
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    :cond_8
    instance-of v1, p1, Lkyo;

    if-eqz v1, :cond_9

    move-object v1, p1

    check-cast v1, Lkyo;

    iget-object v2, p0, Lfdj;->D:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    instance-of v1, p1, Llda;

    if-eqz v1, :cond_3

    move-object v1, p1

    check-cast v1, Llda;

    invoke-interface {v1}, Llda;->j()Llcu;

    move-result-object v2

    iget-object v1, p0, Lfdj;->y:Ljava/util/HashMap;

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Llcu;->a(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 318
    :cond_a
    instance-of v0, p1, Lhwq;

    if-eqz v0, :cond_b

    move-object v0, p1

    .line 319
    check-cast v0, Lhwq;

    iget-object v1, p0, Lfdj;->k:Landroid/content/Context;

    const-class v2, Lirq;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lirq;

    invoke-interface {v1}, Lirq;->a()Lhuw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhwq;->a(Lhuw;)V

    invoke-virtual {v0, v4}, Lhwq;->a(I)V

    invoke-virtual {v0, v4}, Lhwq;->b(I)V

    goto/16 :goto_1

    .line 321
    :cond_b
    const-string v0, "StreamAdapter"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 322
    const-string v0, "StreamAdapter"

    const-string v1, "Unrecognized view type."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public a(Levp;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lfdj;->h:Levp;

    .line 274
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1008
    invoke-virtual {p0}, Lfdj;->ap()V

    .line 1009
    iput-object p1, p0, Lfdj;->K:Ljava/lang/String;

    .line 1010
    iput-object p2, p0, Lfdj;->L:Landroid/view/View;

    .line 1011
    iget-object v0, p0, Lfdj;->L:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1012
    iget-object v0, p0, Lfdj;->L:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1014
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Lfdj;->B:Liax;

    invoke-interface {v0, p1, p2, p3}, Liax;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 921
    invoke-virtual {p0}, Lfdj;->an()V

    .line 922
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1017
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1018
    :cond_0
    iget-object v0, p0, Lfdj;->A:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1022
    :goto_0
    return-void

    .line 1020
    :cond_1
    iget-object v0, p0, Lfdj;->A:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 809
    iput-object p1, p0, Lfdj;->s:Ljava/util/ArrayList;

    .line 810
    return-void
.end method

.method public a(Llhc;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lfdj;->q:Llhc;

    .line 267
    return-void
.end method

.method public a(Llij;)V
    .locals 0

    .prologue
    .line 940
    iput-object p1, p0, Lfdj;->I:Llij;

    .line 941
    return-void
.end method

.method public a(ZI)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 854
    const-string v0, "StreamAdapter"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 855
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "triggerStreamObservers "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 858
    :cond_0
    if-eqz p1, :cond_1

    .line 859
    iput v3, p0, Lfdj;->t:I

    .line 861
    if-gez p2, :cond_2

    .line 862
    iget-object v0, p0, Lfdj;->r:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 873
    :cond_1
    iget-object v0, p0, Lfdj;->u:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lleb;

    .line 874
    iget v2, p0, Lfdj;->t:I

    invoke-virtual {v0, p1, p2, v2}, Lleb;->a(ZII)V

    goto :goto_0

    .line 864
    :cond_2
    iget-object v0, p0, Lfdj;->r:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    .line 865
    iget-object v1, p0, Lfdj;->r:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    if-lt v1, p2, :cond_1

    .line 866
    iget-object v1, p0, Lfdj;->r:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->removeAt(I)V

    .line 864
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 877
    :cond_3
    iput v3, p0, Lfdj;->t:I

    .line 878
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[B)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 665
    .line 667
    iget-boolean v1, p0, Lfdj;->d:Z

    if-eqz v1, :cond_4

    .line 671
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 673
    iget-object v1, p0, Lfdj;->m:Lgl;

    invoke-virtual {v1, p1}, Lgl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    .line 674
    iget-object v6, p0, Lfdj;->m:Lgl;

    new-instance v0, Lfeu;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lfeu;-><init>(Ljava/lang/String;Ljava/lang/String;IJ)V

    invoke-virtual {v6, p1, v0}, Lgl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    iget-object v0, p0, Lfdj;->n:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move v6, v7

    move v0, v7

    .line 681
    :goto_0
    if-eqz p4, :cond_2

    iget-object v1, p0, Lfdj;->m:Lgl;

    invoke-virtual {v1, p4}, Lgl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    .line 682
    iget-object v8, p0, Lfdj;->m:Lgl;

    new-instance v0, Lfeu;

    move-object v1, p4

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lfeu;-><init>(Ljava/lang/String;Ljava/lang/String;IJ)V

    invoke-virtual {v8, p4, v0}, Lgl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 684
    iget-object v0, p0, Lfdj;->n:Ljava/util/HashSet;

    invoke-virtual {v0, p4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move v0, v6

    move v1, v7

    .line 690
    :goto_1
    invoke-static {p5}, Llap;->a([B)Lnwr;

    move-result-object v2

    .line 691
    if-eqz v2, :cond_0

    iget-object v3, p0, Lfdj;->E:Ljava/util/HashSet;

    invoke-virtual {v3, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 692
    new-instance v1, Lnqp;

    invoke-direct {v1}, Lnqp;-><init>()V

    iput-object v2, v1, Lnqp;->b:Lnwr;

    const/4 v2, 0x6

    iput v2, v1, Lnqp;->c:I

    iget-object v2, p0, Lfdj;->F:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 693
    iget-object v1, p0, Lfdj;->E:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move v1, v7

    .line 697
    :cond_0
    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lfdj;->p:Z

    if-nez v1, :cond_1

    .line 698
    iput-boolean v7, p0, Lfdj;->p:Z

    .line 699
    iget-object v1, p0, Lfdj;->o:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-static {v1, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 703
    :cond_1
    return v0

    :cond_2
    move v1, v0

    move v0, v6

    goto :goto_1

    :cond_3
    move v6, v0

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method public ah()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lfdj;->I:Llij;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lfdj;->I:Llij;

    .line 246
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfdj;->H:Z

    .line 247
    return-void
.end method

.method public ai()V
    .locals 2

    .prologue
    .line 250
    iget-boolean v0, p0, Lfdj;->p:Z

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lfdj;->o:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 252
    iget-object v0, p0, Lfdj;->o:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 254
    :cond_0
    iget-object v0, p0, Lfdj;->I:Llij;

    if-eqz v0, :cond_1

    .line 255
    iget-object v0, p0, Lfdj;->I:Llij;

    .line 257
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfdj;->H:Z

    .line 258
    return-void
.end method

.method public aj()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 712
    iget-object v0, p0, Lfdj;->n:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 713
    if-nez v0, :cond_0

    .line 714
    const/4 v0, 0x0

    .line 716
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lfdj;->n:Ljava/util/HashSet;

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public ak()V
    .locals 1

    .prologue
    .line 721
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfdj;->a(I)I

    move-result v0

    .line 722
    if-lez v0, :cond_0

    .line 723
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lfdj;->i:I

    .line 727
    :goto_0
    return-void

    .line 725
    :cond_0
    const/high16 v0, -0x80000000

    iput v0, p0, Lfdj;->i:I

    goto :goto_0
.end method

.method public al()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 805
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method public am()I
    .locals 1

    .prologue
    .line 826
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public an()V
    .locals 4

    .prologue
    .line 928
    iget-object v0, p0, Lfdj;->B:Liax;

    iget-object v1, p0, Lfdj;->k:Landroid/content/Context;

    iget v2, p0, Lfdj;->g:I

    const/16 v3, 0x12

    invoke-interface {v0, v1, v2, v3}, Liax;->a(Landroid/content/Context;II)V

    .line 930
    return-void
.end method

.method public ao()V
    .locals 1

    .prologue
    .line 944
    iget-object v0, p0, Lfdj;->I:Llij;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfdj;->G:Z

    if-eqz v0, :cond_0

    .line 945
    iget-object v0, p0, Lfdj;->I:Llij;

    .line 947
    :cond_0
    return-void
.end method

.method public ap()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 968
    iput-object v2, p0, Lfdj;->K:Ljava/lang/String;

    .line 969
    iget-object v0, p0, Lfdj;->L:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 970
    iget-object v0, p0, Lfdj;->L:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 971
    iput-object v2, p0, Lfdj;->L:Landroid/view/View;

    .line 973
    :cond_0
    return-void
.end method

.method public aq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 980
    iget-object v0, p0, Lfdj;->K:Ljava/lang/String;

    return-object v0
.end method

.method public b(Landroid/database/Cursor;I)V
    .locals 2

    .prologue
    .line 518
    const-string v0, "StreamAdapter"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "changeStreamCursor lastClickedPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 522
    :cond_0
    const/4 v0, 0x2

    invoke-super {p0, v0, p1}, Lhya;->a(ILandroid/database/Cursor;)V

    .line 523
    invoke-virtual {p0, p1, p2}, Lfdj;->a(Landroid/database/Cursor;I)V

    .line 524
    return-void
.end method

.method protected b(Landroid/view/View;I)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 414
    iget-boolean v0, p0, Lfdj;->J:Z

    if-eqz v0, :cond_1

    .line 415
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lfdj;->i:I

    if-le p2, v0, :cond_1

    .line 417
    iput p2, p0, Lfdj;->i:I

    .line 420
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    float-to-int v2, v0

    .line 421
    instance-of v0, p1, Lldq;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 422
    check-cast v0, Lldq;

    iget-boolean v3, v0, Lhvs;->v:Z

    if-nez v3, :cond_0

    iput-boolean v1, v0, Lhvs;->v:Z

    iget-boolean v3, v0, Lhvs;->u:Z

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lhvs;->z()Z

    move-result v3

    invoke-virtual {v0, v3}, Lhvs;->a(Z)V

    .line 424
    :cond_0
    iget-object v0, p0, Lfdj;->j:Llcr;

    iget v0, v0, Llcr;->c:I

    div-int/lit8 v0, v0, 0x3

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 425
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 428
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v2, v2

    .line 429
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    .line 430
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v2, Lfdj;->c:Landroid/view/animation/Interpolator;

    .line 431
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 433
    new-instance v2, Lfdm;

    invoke-direct {v2, p1}, Lfdm;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 463
    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 465
    :cond_1
    instance-of v0, p1, Lldq;

    if-eqz v0, :cond_2

    .line 466
    check-cast p1, Lldq;

    iget-boolean v0, p0, Lfdj;->G:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lfdj;->H:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lldq;->e(Z)V

    .line 469
    :cond_2
    return-void

    .line 466
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(I)I
    .locals 6

    .prologue
    const/4 v4, 0x2

    .line 638
    invoke-virtual {p0, v4}, Lfdj;->l(I)Landroid/database/Cursor;

    move-result-object v0

    .line 639
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 640
    const/16 v1, 0xa

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 641
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 643
    invoke-virtual {p0}, Lfdj;->as()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lkzl;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    .line 645
    invoke-interface {v0, v1}, Lkzl;->a(Ljava/lang/String;)Lkzj;

    move-result-object v0

    .line 646
    if-eqz v0, :cond_3

    .line 647
    invoke-interface {v0, v1, v2, v3}, Lkzj;->a(Ljava/lang/String;J)I

    move-result v1

    .line 648
    invoke-static {v0, v1}, Lfdj;->a(Lkzj;I)Ljava/lang/String;

    move-result-object v1

    .line 649
    invoke-direct {p0}, Lfdj;->b()Ljava/util/HashMap;

    move-result-object v0

    .line 650
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 651
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0}, Lfdj;->getViewTypeCount()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 652
    :cond_0
    const-string v2, "StreamAdapter"

    const-string v3, "Unrecognized view type: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 659
    :goto_1
    return v0

    .line 652
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 657
    :cond_3
    const-string v0, "StreamAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x46

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Could not find a view type for activityId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", flags="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 738
    iput-boolean p1, p0, Lfdj;->d:Z

    .line 739
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lfdj;->w:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 882
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 960
    iput-boolean p1, p0, Lfdj;->J:Z

    .line 961
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 885
    iget-object v0, p0, Lfdj;->x:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 886
    return-void
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 1069
    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-static {}, Lfdj;->ar()Lhym;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v1, v0}, Lfdj;->a(ILandroid/database/Cursor;)V

    .line 1070
    return-void

    .line 1069
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 889
    iget-object v0, p0, Lfdj;->x:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 890
    return-void
.end method

.method public g(I)I
    .locals 1

    .prologue
    .line 634
    const/4 v0, 0x0

    return v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 989
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lfdj;->a(Ljava/lang/String;Landroid/view/View;)V

    .line 990
    return-void
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 528
    invoke-direct {p0}, Lfdj;->b()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public h(I)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 813
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 814
    const/4 v0, 0x0

    .line 822
    :goto_0
    return-object v0

    .line 816
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 817
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, p1, :cond_2

    .line 818
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Llbk;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 819
    iget-object v0, p0, Lfdj;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 817
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 822
    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return v0
.end method

.method public i(I)V
    .locals 2

    .prologue
    .line 830
    const-string v0, "StreamAdapter"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 831
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "setStreamRestorePosition "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 834
    :cond_0
    iput p1, p0, Lfdj;->t:I

    .line 835
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 731
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lfdj;->a(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 839
    invoke-super {p0, p1}, Lhya;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 840
    instance-of v0, p1, Lleb;

    if-eqz v0, :cond_0

    .line 841
    iget-object v0, p0, Lfdj;->u:Ljava/util/HashSet;

    check-cast p1, Lleb;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 843
    :cond_0
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 847
    invoke-super {p0, p1}, Lhya;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 848
    instance-of v0, p1, Lleb;

    if-eqz v0, :cond_0

    .line 849
    iget-object v0, p0, Lfdj;->u:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 851
    :cond_0
    return-void
.end method
