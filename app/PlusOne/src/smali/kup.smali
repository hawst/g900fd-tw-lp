.class public final Lkup;
.super Lhye;
.source "PG"


# static fields
.field public static final b:[Ljava/lang/String;

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field private static f:[Ljava/lang/String;


# instance fields
.field private final g:Lktq;

.field private final h:Lktw;

.field private final i:I

.field private j:Z

.field private k:[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x11

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "square_id"

    aput-object v1, v0, v4

    const-string v1, "square_name"

    aput-object v1, v0, v5

    const-string v1, "photo_url"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "post_visibility"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "member_count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "unread_count"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "inviter_gaia_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "inviter_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "inviter_photo_url"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "invitation_dismissed"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "is_member"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "list_category"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "suggestion_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "restricted_domain"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "joinability"

    aput-object v2, v0, v1

    .line 69
    sput-object v0, Lkup;->b:[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "row_type"

    aput-object v2, v1, v3

    const-string v2, "header_type"

    aput-object v2, v1, v4

    const-string v2, "square_type"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Llrv;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lkup;->f:[Ljava/lang/String;

    .line 98
    sget-object v0, Lkup;->b:[Ljava/lang/String;

    .line 99
    sput v7, Lkup;->c:I

    const/16 v0, 0x12

    .line 100
    sput v0, Lkup;->d:I

    const/16 v0, 0x13

    sput v0, Lkup;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I[I)V
    .locals 2

    .prologue
    .line 109
    sget-object v0, Lktp;->a:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 110
    invoke-virtual {p0}, Lkup;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lktq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iput-object v0, p0, Lkup;->g:Lktq;

    .line 111
    const-class v0, Lktw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktw;

    iput-object v0, p0, Lkup;->h:Lktw;

    .line 112
    iput p2, p0, Lkup;->i:I

    .line 113
    iput-object p3, p0, Lkup;->k:[I

    .line 114
    return-void
.end method

.method private a([Ljava/lang/Object;Landroid/database/Cursor;Ljava/util/HashSet;Lhym;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            "Landroid/database/Cursor;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lhym;",
            "I)V"
        }
    .end annotation

    .prologue
    const/16 v8, 0xc

    const/16 v7, 0xb

    const/4 v6, 0x3

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 166
    .line 168
    const/4 v0, -0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    .line 169
    :cond_0
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 170
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    const/16 v4, 0xd

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    packed-switch p5, :pswitch_data_0

    :cond_1
    move v2, v1

    :goto_2
    :pswitch_0
    if-eqz v2, :cond_0

    .line 171
    if-nez v0, :cond_2

    .line 173
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v0, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, v0

    const/16 v0, 0x12

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, v0

    invoke-virtual {p4, p1}, Lhym;->a([Ljava/lang/Object;)V

    move v0, v3

    .line 177
    :cond_2
    const/4 v2, 0x0

    invoke-static {p1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v2, 0x11

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p1, v2

    const/16 v2, 0x13

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p1, v2

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p1, v1

    aput-object v2, p1, v3

    const/4 v4, 0x2

    const/4 v5, 0x2

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v4

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, p1, v6

    const/4 v4, 0x4

    const/4 v5, 0x4

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p1, v4

    const/4 v4, 0x5

    const/4 v5, 0x5

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p1, v4

    const/4 v4, 0x6

    const/4 v5, 0x6

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p1, v4

    const/4 v4, 0x7

    const/4 v5, 0x7

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p1, v4

    const/16 v4, 0x8

    const/16 v5, 0x8

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v4

    const/16 v4, 0x9

    const/16 v5, 0x9

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v4

    const/16 v4, 0xa

    const/16 v5, 0xa

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v4

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p1, v7

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p1, v8

    const/16 v4, 0xd

    const/16 v5, 0xd

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p1, v4

    const/16 v4, 0xe

    const/16 v5, 0xe

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v4

    const/16 v4, 0xf

    const/16 v5, 0xf

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v4

    const/16 v4, 0x10

    const/16 v5, 0x10

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p1, v4

    invoke-virtual {p4, p1}, Lhym;->a([Ljava/lang/Object;)V

    .line 178
    invoke-virtual {p3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 170
    goto/16 :goto_1

    :pswitch_1
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_4

    move v4, v3

    :goto_3
    if-ne v5, v3, :cond_5

    if-nez v2, :cond_5

    if-nez v4, :cond_5

    move v2, v3

    goto/16 :goto_2

    :cond_4
    move v4, v1

    goto :goto_3

    :cond_5
    move v2, v1

    goto/16 :goto_2

    :pswitch_2
    if-ne v5, v6, :cond_6

    if-nez v2, :cond_6

    move v2, v3

    goto/16 :goto_2

    :cond_6
    move v2, v1

    goto/16 :goto_2

    .line 181
    :cond_7
    return-void

    .line 170
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 118
    iget-object v0, p0, Lkup;->h:Lktw;

    iget v1, p0, Lkup;->i:I

    invoke-virtual {v0, v1}, Lktw;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lkup;->j:Z

    .line 120
    iget-object v0, p0, Lkup;->g:Lktq;

    iget v1, p0, Lkup;->i:I

    sget-object v2, Lkup;->b:[Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lktq;->a(I[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 121
    const/4 v4, 0x0

    .line 123
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 124
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    new-instance v4, Lhym;

    sget-object v0, Lkup;->f:[Ljava/lang/String;

    invoke-direct {v4, v0}, Lhym;-><init>([Ljava/lang/String;)V

    sget-object v0, Lkup;->f:[Ljava/lang/String;

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v7, p0, Lkup;->k:[I

    array-length v8, v7

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_0

    aget v5, v7, v6

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lkup;->a([Ljava/lang/Object;Landroid/database/Cursor;Ljava/util/HashSet;Lhym;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 127
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 130
    return-object v4

    .line 127
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Lkup;->j:Z

    return v0
.end method
