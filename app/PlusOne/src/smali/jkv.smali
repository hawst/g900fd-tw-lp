.class public abstract Ljkv;
.super Ldf;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/database/Cursor;",
        ">",
        "Ldf",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private b:Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Ldf;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 23
    iget-object v1, p0, Ljkv;->b:Landroid/database/Cursor;

    if-eq v1, p1, :cond_1

    .line 24
    iget-object v1, p0, Ljkv;->b:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljkv;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 25
    iget-object v1, p0, Ljkv;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 27
    :cond_0
    iput-object p1, p0, Ljkv;->b:Landroid/database/Cursor;

    .line 34
    :cond_1
    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 36
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p1

    .line 43
    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    .line 44
    :goto_1
    if-eqz v2, :cond_3

    .line 48
    :goto_2
    invoke-super {p0, v0}, Ldf;->b(Ljava/lang/Object;)V

    .line 49
    return-void

    .line 38
    :catch_0
    move-exception v1

    move-object v1, v0

    goto :goto_0

    .line 43
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v1, p1

    goto :goto_0
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Ljkv;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method public b(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 67
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 70
    :cond_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Ljkv;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Ljkv;->f()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public abstract f()Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Ljkv;->y()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljkv;->b:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 54
    :cond_0
    invoke-virtual {p0}, Ljkv;->t()V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_1
    iget-object v0, p0, Ljkv;->b:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Ljkv;->a(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 62
    invoke-virtual {p0}, Ljkv;->b()Z

    .line 63
    return-void
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Ldf;->i()V

    .line 75
    invoke-virtual {p0}, Ljkv;->h()V

    .line 76
    iget-object v0, p0, Ljkv;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljkv;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Ljkv;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 79
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ljkv;->b:Landroid/database/Cursor;

    .line 80
    return-void
.end method
