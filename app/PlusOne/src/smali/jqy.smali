.class public final Ljqy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljpr;
.implements Ljps;


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljrm;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Liof;

.field private d:Lipl;

.field private e:Lipj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Ljqy;->b:Landroid/content/Context;

    .line 85
    const-class v0, Liof;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liof;

    iput-object v0, p0, Ljqy;->c:Liof;

    .line 86
    const-class v0, Liok;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 87
    const-class v0, Linz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 88
    const-class v0, Lipl;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lipl;

    iput-object v0, p0, Ljqy;->d:Lipl;

    .line 89
    iget-object v0, p0, Ljqy;->c:Liof;

    invoke-interface {v0}, Liof;->a()Lipj;

    move-result-object v0

    iput-object v0, p0, Ljqy;->e:Lipj;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ljqy;->a:Ljava/util/HashMap;

    .line 91
    new-instance v0, Ljqu;

    invoke-direct {v0, p1}, Ljqu;-><init>(Landroid/content/Context;)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Llqr;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Ljqy;-><init>(Landroid/content/Context;)V

    .line 108
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 109
    return-void
.end method

.method static synthetic a(Ljqy;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ljqy;->b:Landroid/content/Context;

    return-object v0
.end method

.method private a(Liqc;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 509
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 510
    :goto_0
    if-eqz p1, :cond_0

    invoke-interface {p1}, Liqc;->b()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 511
    invoke-interface {p1, v1}, Liqc;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liqb;

    .line 512
    const-string v4, "%s, "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-interface {v0}, Liqb;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 515
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljqy;Liqc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1}, Ljqy;->a(Liqc;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljqy;Liqi;Ljava/util/HashMap;)Ljava/util/List;
    .locals 5

    .prologue
    .line 70
    invoke-interface {p1}, Liqi;->b()I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-interface {p1, v1}, Liqi;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liqh;

    new-instance v4, Ljry;

    invoke-direct {v4, v0, p2}, Ljry;-><init>(Liqh;Ljava/util/HashMap;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method private declared-synchronized a(I)Ljrm;
    .locals 3

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    .line 96
    iget-object v0, p0, Ljqy;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrm;

    .line 98
    if-nez v0, :cond_0

    .line 99
    new-instance v0, Ljrm;

    iget-object v2, p0, Ljqy;->b:Landroid/content/Context;

    invoke-direct {v0, v2, p1}, Ljrm;-><init>(Landroid/content/Context;I)V

    .line 100
    iget-object v2, p0, Ljqy;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    :cond_0
    iget-object v0, p0, Ljqy;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Ljqy;I)Ljrm;
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1}, Ljqy;->a(I)Ljrm;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 488
    invoke-virtual {p0}, Ljqy;->d()V

    .line 489
    return-void
.end method

.method public a(Ljoy;ILjqc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljoy;",
            "I",
            "Ljqc",
            "<",
            "Ljod;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0, p2}, Ljqy;->a(I)Ljrm;

    move-result-object v0

    .line 120
    new-instance v1, Ljrq;

    invoke-direct {v1, v0, p1, p3}, Ljrq;-><init>(Ljrm;Ljoy;Ljqc;)V

    .line 122
    invoke-virtual {v0, v1}, Ljrm;->a(Ljrl;)V

    .line 123
    return-void
.end method

.method public a(Ljoz;IIILjqc;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljoz;",
            "III",
            "Ljqc",
            "<",
            "Ljpv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 206
    invoke-direct {p0, p2}, Ljqy;->a(I)Ljrm;

    move-result-object v1

    .line 207
    new-instance v0, Ljrx;

    move-object v2, p1

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ljrx;-><init>(Ljrm;Ljoz;IILjqc;)V

    .line 209
    invoke-virtual {v1, v0}, Ljrm;->a(Ljrl;)V

    .line 210
    return-void
.end method

.method public a(Ljpa;IILjqc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljpa;",
            "II",
            "Ljqc",
            "<",
            "Ljod;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0, p2}, Ljqy;->a(I)Ljrm;

    move-result-object v0

    .line 129
    new-instance v1, Ljsa;

    invoke-direct {v1, v0, p1, p3, p4}, Ljsa;-><init>(Ljrm;Ljpa;ILjqc;)V

    .line 131
    invoke-virtual {v0, v1}, Ljrm;->a(Ljrl;)V

    .line 132
    return-void
.end method

.method public a(Ljpc;ILjava/lang/String;ILjava/lang/String;ZZ)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 215
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    const-string v0, "circleId: %s."

    new-array v1, v7, [Ljava/lang/Object;

    aput-object p3, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 220
    :cond_0
    if-nez p1, :cond_1

    .line 221
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_1
    iget-object v0, p0, Ljqy;->b:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 225
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 226
    const-string v2, "effective_gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 227
    new-instance v2, Lioq;

    invoke-direct {v2}, Lioq;-><init>()V

    invoke-virtual {v2, p3}, Lioq;->a(Ljava/lang/String;)Lioq;

    move-result-object v3

    invoke-virtual {v3, p4}, Lioq;->a(I)Lioq;

    move-result-object v3

    invoke-virtual {v3, p5}, Lioq;->b(Ljava/lang/String;)Lioq;

    move-result-object v3

    invoke-virtual {v3, p6}, Lioq;->a(Z)Lioq;

    .line 229
    iget-object v3, p0, Ljqy;->c:Liof;

    iget-object v4, p0, Ljqy;->e:Lipj;

    .line 230
    invoke-interface {v3, v4, v1, v0, v2}, Liof;->a(Lipj;Ljava/lang/String;Ljava/lang/String;Lioq;)Lijq;

    move-result-object v0

    .line 232
    if-nez v0, :cond_3

    .line 233
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234
    invoke-static {}, Ljpe;->b()V

    .line 237
    :cond_2
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljpc;->a(Liqc;)V

    .line 269
    :goto_0
    return-void

    .line 241
    :cond_3
    if-eqz p7, :cond_4

    .line 243
    invoke-interface {v0}, Lijq;->a()Lijs;

    move-result-object v0

    check-cast v0, Lior;

    .line 244
    invoke-interface {v0}, Lior;->b()Liqc;

    move-result-object v2

    .line 246
    const-string v3, "isSuccess: %s. %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 247
    invoke-interface {v0}, Lior;->a()Lika;

    move-result-object v5

    invoke-interface {v5}, Lika;->b()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-direct {p0, v2}, Ljqy;->a(Liqc;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 246
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 248
    iget-object v3, p0, Ljqy;->b:Landroid/content/Context;

    const/4 v4, 0x3

    invoke-static {v3, v1, v4, v0}, Ljpe;->a(Landroid/content/Context;Ljava/lang/String;ILijs;)V

    .line 251
    invoke-interface {p1, v2}, Ljpc;->a(Liqc;)V

    goto :goto_0

    .line 253
    :cond_4
    new-instance v2, Ljrb;

    invoke-direct {v2, p0, v1, p1}, Ljrb;-><init>(Ljqy;Ljava/lang/String;Ljpc;)V

    invoke-interface {v0, v2}, Lijq;->a(Lijt;)V

    goto :goto_0
.end method

.method public a(Ljpd;IZ)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 273
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    const-string v0, "waitUntilComplete: %s"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 278
    :cond_0
    if-nez p1, :cond_1

    .line 279
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_1
    iget-object v0, p0, Ljqy;->b:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 283
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 284
    const-string v2, "effective_gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 285
    new-instance v2, Liow;

    invoke-direct {v2}, Liow;-><init>()V

    .line 286
    iget-object v3, p0, Ljqy;->c:Liof;

    iget-object v4, p0, Ljqy;->e:Lipj;

    .line 287
    invoke-interface {v3, v4, v1, v0, v2}, Liof;->a(Lipj;Ljava/lang/String;Ljava/lang/String;Liow;)Lijq;

    move-result-object v0

    .line 289
    if-nez v0, :cond_3

    .line 290
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 291
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 292
    invoke-static {}, Ljpe;->b()V

    .line 296
    :cond_2
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljpd;->a(Liqi;)V

    .line 330
    :goto_0
    return-void

    .line 300
    :cond_3
    if-eqz p3, :cond_5

    .line 302
    invoke-interface {v0}, Lijq;->a()Lijs;

    move-result-object v0

    check-cast v0, Liox;

    .line 303
    invoke-interface {v0}, Liox;->b()Liqi;

    move-result-object v2

    .line 305
    invoke-static {}, Ljpe;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v2, :cond_4

    .line 306
    const-string v3, "isSuccess: %s. People count: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 307
    invoke-interface {v0}, Liox;->a()Lika;

    move-result-object v5

    invoke-interface {v5}, Lika;->b()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-interface {v2}, Liqi;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 306
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 309
    :cond_4
    iget-object v3, p0, Ljqy;->b:Landroid/content/Context;

    const/4 v4, 0x4

    invoke-static {v3, v1, v4, v0}, Ljpe;->a(Landroid/content/Context;Ljava/lang/String;ILijs;)V

    .line 312
    invoke-interface {p1, v2}, Ljpd;->a(Liqi;)V

    goto :goto_0

    .line 314
    :cond_5
    new-instance v2, Ljrc;

    invoke-direct {v2, p0, v1, p1}, Ljrc;-><init>(Ljqy;Ljava/lang/String;Ljpd;)V

    invoke-interface {v0, v2}, Lijq;->a(Lijt;)V

    goto :goto_0
.end method

.method public a(Ljqd;ILjqe;)V
    .locals 6

    .prologue
    .line 165
    if-nez p1, :cond_0

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    iget-object v0, p0, Ljqy;->b:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 170
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 171
    const-string v2, "effective_gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    new-instance v2, Liow;

    invoke-direct {v2}, Liow;-><init>()V

    invoke-virtual {p3}, Ljqe;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Liow;->b(Ljava/lang/String;)Liow;

    move-result-object v2

    invoke-virtual {p3}, Ljqe;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Liow;->b(I)Liow;

    move-result-object v2

    invoke-virtual {p3}, Ljqe;->f()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Liow;->a(J)Liow;

    move-result-object v2

    invoke-virtual {p3}, Ljqe;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Liow;->a(Ljava/lang/String;)Liow;

    move-result-object v2

    invoke-virtual {p3}, Ljqe;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Liow;->d(I)Liow;

    move-result-object v2

    invoke-virtual {p3}, Ljqe;->e()Z

    move-result v3

    invoke-virtual {v2, v3}, Liow;->a(Z)Liow;

    move-result-object v2

    invoke-virtual {p3}, Ljqe;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Liow;->a(I)Liow;

    move-result-object v2

    invoke-virtual {p3}, Ljqe;->c()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v2, v3}, Liow;->a(Ljava/util/Collection;)Liow;

    move-result-object v2

    invoke-virtual {p3}, Ljqe;->i()I

    move-result v3

    invoke-virtual {v2, v3}, Liow;->c(I)Liow;

    move-result-object v2

    .line 173
    iget-object v3, p0, Ljqy;->c:Liof;

    iget-object v4, p0, Ljqy;->e:Lipj;

    .line 174
    invoke-interface {v3, v4, v1, v0, v2}, Liof;->a(Lipj;Ljava/lang/String;Ljava/lang/String;Liow;)Lijq;

    move-result-object v0

    .line 176
    if-nez v0, :cond_1

    .line 177
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljqd;->a(Ljava/util/List;)V

    .line 201
    :goto_0
    return-void

    .line 181
    :cond_1
    new-instance v1, Ljqz;

    invoke-direct {v1, p0, p1, p2}, Ljqz;-><init>(Ljqy;Ljqd;I)V

    invoke-interface {v0, v1}, Lijq;->a(Lijt;)V

    goto :goto_0
.end method

.method public a(Ljqf;I)V
    .locals 5

    .prologue
    .line 370
    if-nez p1, :cond_0

    .line 373
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 376
    :cond_0
    iget-object v0, p0, Ljqy;->b:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 377
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 378
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 380
    const/4 v1, 0x0

    .line 382
    :try_start_0
    iget-object v3, p0, Ljqy;->d:Lipl;

    iget-object v4, p0, Ljqy;->e:Lipj;

    invoke-interface {v3, v4, v2, v0}, Lipl;->a(Lipj;Ljava/lang/String;Ljava/lang/String;)Lijq;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 388
    :goto_0
    if-nez v0, :cond_3

    .line 389
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390
    invoke-static {}, Ljpe;->b()V

    .line 393
    :cond_1
    invoke-interface {p1}, Ljqf;->a()V

    .line 412
    :goto_1
    return-void

    .line 383
    :catch_0
    move-exception v0

    .line 384
    const-string v3, "Unexpected error when requesting sync by user action: "

    .line 385
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_2
    move-object v0, v1

    .line 384
    invoke-static {}, Ljpe;->b()V

    goto :goto_0

    .line 385
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 397
    :cond_3
    new-instance v1, Ljrd;

    invoke-direct {v1, p0, v2, p1}, Ljrd;-><init>(Ljqy;Ljava/lang/String;Ljqf;)V

    invoke-interface {v0, v1}, Lijq;->a(Lijt;)V

    goto :goto_1
.end method

.method public a(Ljqg;IJZ)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 417
    if-nez p1, :cond_0

    .line 420
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423
    :cond_0
    iget-object v0, p0, Ljqy;->b:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 424
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 425
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 429
    :try_start_0
    iget-object v0, p0, Ljqy;->d:Lipl;

    iget-object v1, p0, Ljqy;->e:Lipj;

    move-wide v4, p3

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lipl;->a(Lipj;Ljava/lang/String;Ljava/lang/String;JZ)Lijq;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 435
    :goto_0
    if-nez v0, :cond_3

    .line 436
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    invoke-static {}, Ljpe;->b()V

    .line 440
    :cond_1
    invoke-interface {p1, v7}, Ljqg;->a(Lika;)V

    .line 459
    :goto_1
    return-void

    .line 431
    :catch_0
    move-exception v0

    .line 432
    const-string v1, "Unexpected error when requesting sync: "

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_2
    move-object v0, v7

    invoke-static {}, Ljpe;->b()V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 444
    :cond_3
    new-instance v1, Ljre;

    invoke-direct {v1, p0, v2, p1}, Ljre;-><init>(Ljqy;Ljava/lang/String;Ljqg;)V

    invoke-interface {v0, v1}, Lijq;->a(Lijt;)V

    goto :goto_1
.end method

.method public aP_()V
    .locals 0

    .prologue
    .line 493
    invoke-virtual {p0}, Ljqy;->e()V

    .line 494
    return-void
.end method

.method public c()Ljps;
    .locals 0

    .prologue
    .line 113
    return-object p0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Ljqy;->e:Lipj;

    invoke-interface {v0}, Lipj;->c()V

    .line 464
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Ljqy;->e:Lipj;

    invoke-interface {v0}, Lipj;->d()V

    .line 474
    return-void
.end method
