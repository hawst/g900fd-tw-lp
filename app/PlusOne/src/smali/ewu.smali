.class public abstract Lewu;
.super Lloa;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lhmq;


# instance fields
.field private e:Lenc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lloa;-><init>()V

    .line 32
    new-instance v0, Lkbx;

    iget-object v1, p0, Lewu;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkbx;-><init>(Lz;Llqr;)V

    .line 33
    new-instance v0, Lhet;

    iget-object v1, p0, Lewu;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lewu;->x:Llnh;

    .line 34
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 35
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 124
    iget-object v0, p0, Lewu;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 125
    return-void
.end method

.method public a(Lu;)V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1}, Lloa;->a(Lu;)V

    .line 42
    instance-of v0, p1, Lenc;

    if-eqz v0, :cond_0

    .line 43
    check-cast p1, Lenc;

    iput-object p1, p0, Lewu;->e:Lenc;

    .line 45
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method public l()V
    .locals 0

    .prologue
    .line 118
    invoke-virtual {p0}, Lewu;->az_()V

    .line 119
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lewu;->e:Lenc;

    invoke-interface {v0}, Lenc;->V()V

    .line 76
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 100
    packed-switch p2, :pswitch_data_0

    .line 104
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 112
    return-void

    .line 102
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lewu;->setResult(I)V

    .line 103
    invoke-virtual {p0}, Lewu;->finish()V

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 80
    const v0, 0xdc073

    if-ne p1, v0, :cond_0

    .line 81
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 82
    const v1, 0x7f0a07f5

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 83
    const v1, 0x7f0a07fa

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    const v1, 0x7f0a07fd

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 85
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 86
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    .line 87
    :cond_0
    const v0, 0x48ba7

    if-ne p1, v0, :cond_1

    .line 88
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 89
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 90
    const v1, 0x7f0a058d

    invoke-virtual {p0, v1}, Lewu;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 91
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 62
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 63
    const v2, 0x102002c

    if-eq v1, v2, :cond_0

    const v2, 0x7f10067a

    if-ne v1, v2, :cond_1

    .line 64
    :cond_0
    iget-object v1, p0, Lewu;->e:Lenc;

    invoke-interface {v1}, Lenc;->V()V

    .line 70
    :goto_0
    return v0

    .line 66
    :cond_1
    const v2, 0x7f100679

    if-ne v1, v2, :cond_2

    .line 67
    iget-object v1, p0, Lewu;->e:Lenc;

    invoke-interface {v1}, Lenc;->b()V

    goto :goto_0

    .line 70
    :cond_2
    invoke-super {p0, p1}, Lloa;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 49
    const v0, 0x7f100679

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lewu;->e:Lenc;

    invoke-interface {v1}, Lenc;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 52
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 57
    :goto_0
    return v2

    .line 54
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 55
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method
