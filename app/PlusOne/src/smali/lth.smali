.class public final Llth;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llth;


# instance fields
.field public b:I

.field public c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Llth;

    sput-object v0, Llth;->a:[Llth;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    iput v0, p0, Llth;->b:I

    .line 22
    iput v0, p0, Llth;->c:I

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 39
    const/4 v0, 0x0

    .line 40
    iget v1, p0, Llth;->b:I

    if-eq v1, v2, :cond_0

    .line 41
    const/4 v0, 0x1

    iget v1, p0, Llth;->b:I

    .line 42
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 44
    :cond_0
    iget v1, p0, Llth;->c:I

    if-eq v1, v2, :cond_1

    .line 45
    const/4 v1, 0x2

    iget v2, p0, Llth;->c:I

    .line 46
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_1
    iget-object v1, p0, Llth;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    iput v0, p0, Llth;->ai:I

    .line 50
    return v0
.end method

.method public a(Loxn;)Llth;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 59
    sparse-switch v0, :sswitch_data_0

    .line 63
    iget-object v1, p0, Llth;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llth;->ah:Ljava/util/List;

    .line 67
    :cond_1
    iget-object v1, p0, Llth;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    :sswitch_0
    return-object p0

    .line 74
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 75
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    .line 79
    :cond_2
    iput v0, p0, Llth;->b:I

    goto :goto_0

    .line 81
    :cond_3
    iput v2, p0, Llth;->b:I

    goto :goto_0

    .line 86
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 87
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    .line 94
    :cond_4
    iput v0, p0, Llth;->c:I

    goto :goto_0

    .line 96
    :cond_5
    iput v2, p0, Llth;->c:I

    goto :goto_0

    .line 59
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 27
    iget v0, p0, Llth;->b:I

    if-eq v0, v2, :cond_0

    .line 28
    const/4 v0, 0x1

    iget v1, p0, Llth;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 30
    :cond_0
    iget v0, p0, Llth;->c:I

    if-eq v0, v2, :cond_1

    .line 31
    const/4 v0, 0x2

    iget v1, p0, Llth;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 33
    :cond_1
    iget-object v0, p0, Llth;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 35
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llth;->a(Loxn;)Llth;

    move-result-object v0

    return-object v0
.end method
