.class final Llmn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Ljava/lang/String;

.field private final c:Lllr;

.field private transient d:Z

.field private transient e:Z

.field private f:J

.field private g:J


# direct methods
.method constructor <init>(Ljava/io/File;Ljava/lang/String;Lllr;ZJJ)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const-string v0, "cacheDir cannot be null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Llmn;->a:Ljava/io/File;

    .line 126
    const-string v0, "name cannot be null"

    invoke-static {p2, v0}, Llsk;->a(Ljava/lang/CharSequence;Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Llmn;->b:Ljava/lang/String;

    .line 127
    const-string v0, "map cannot be null"

    invoke-static {p3, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllr;

    iput-object v0, p0, Llmn;->c:Lllr;

    .line 128
    iput-boolean p4, p0, Llmn;->d:Z

    .line 129
    iput-boolean v2, p0, Llmn;->e:Z

    .line 131
    cmp-long v0, p5, v4

    if-ltz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "lastAccessedTimestampMs cannot be negative"

    invoke-static {v0, v3}, Llsk;->a(ZLjava/lang/Object;)V

    .line 133
    iput-wide p5, p0, Llmn;->f:J

    .line 135
    cmp-long v0, p7, v4

    if-gez v0, :cond_0

    const-wide/16 v4, -0x1

    cmp-long v0, p7, v4

    if-nez v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "fileLengthBytes must be non-negative or must match FILE_LENGTH_UNKNOWN"

    invoke-static {v2, v0}, Llsk;->a(ZLjava/lang/Object;)V

    .line 138
    iput-wide p7, p0, Llmn;->g:J

    .line 139
    return-void

    :cond_2
    move v0, v2

    .line 131
    goto :goto_0
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;)Llmn;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 76
    new-instance v2, Ljava/io/File;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "~m"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, p0, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 81
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v0, "r"

    invoke-direct {v3, v2, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :try_start_1
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    long-to-int v0, v4

    new-array v0, v0, [B

    .line 87
    invoke-virtual {v3, v0}, Ljava/io/RandomAccessFile;->readFully([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    .line 91
    invoke-static {v0}, Llna;->a([B)Llna;

    move-result-object v0

    .line 93
    iget v2, v0, Llna;->a:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 98
    :goto_1
    return-object v1

    .line 76
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    throw v0

    .line 97
    :cond_1
    iget-object v1, v0, Llna;->d:[B

    invoke-static {v1}, Lllr;->a([B)Lllr;

    move-result-object v4

    .line 98
    new-instance v1, Llmn;

    const/4 v5, 0x0

    iget-wide v6, v0, Llna;->b:J

    iget-wide v8, v0, Llna;->c:J

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v9}, Llmn;-><init>(Ljava/io/File;Ljava/lang/String;Lllr;ZJJ)V

    goto :goto_1

    .line 83
    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public a(II)I
    .locals 6

    .prologue
    .line 160
    invoke-virtual {p0, p1}, Llmn;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    const/4 p2, 0x0

    .line 167
    :cond_0
    :goto_0
    return p2

    .line 162
    :cond_1
    iget-wide v0, p0, Llmn;->g:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 165
    mul-int v0, p1, p2

    int-to-long v0, v0

    .line 166
    iget-wide v2, p0, Llmn;->g:J

    add-int/lit8 v4, p1, 0x1

    mul-int/2addr v4, p2

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 167
    sub-long v0, v2, v0

    long-to-int p2, v0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Llmn;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(IJ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 180
    if-ltz p1, :cond_2

    move v0, v1

    :goto_0
    const-string v2, "index must not be negative"

    invoke-static {v0, v2}, Llsk;->a(ZLjava/lang/Object;)V

    .line 181
    iput-boolean v1, p0, Llmn;->e:Z

    .line 182
    const-wide/16 v2, -0x1

    cmp-long v0, p2, v2

    if-eqz v0, :cond_0

    .line 183
    iput-wide p2, p0, Llmn;->g:J

    .line 186
    :cond_0
    iget-boolean v0, p0, Llmn;->d:Z

    if-nez v0, :cond_1

    .line 187
    iput-boolean v1, p0, Llmn;->d:Z

    .line 189
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Llmn;->a:Ljava/io/File;

    iget-object v0, p0, Llmn;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "~m"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 190
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 194
    :cond_1
    iget-object v0, p0, Llmn;->c:Lllr;

    invoke-virtual {v0, p1, v1}, Lllr;->a(IZ)V

    .line 195
    return-void

    .line 180
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 189
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 276
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "timestampMs must be non-negative"

    invoke-static {v0, v1}, Llsk;->a(ZLjava/lang/Object;)V

    .line 277
    iput-wide p1, p0, Llmn;->f:J

    .line 278
    return-void

    .line 276
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Llmn;->c:Lllr;

    invoke-virtual {v0, p1}, Lllr;->a(I)Z

    move-result v0

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Llmn;->f:J

    return-wide v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Llmn;->c:Lllr;

    invoke-virtual {v0}, Lllr;->c()I

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Llmn;->e:Z

    .line 214
    return-void
.end method

.method public e()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 223
    iget-boolean v0, p0, Llmn;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Llmn;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 244
    :goto_0
    return v0

    .line 226
    :cond_1
    new-instance v3, Ljava/io/File;

    iget-object v2, p0, Llmn;->a:Ljava/io/File;

    iget-object v0, p0, Llmn;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "~m"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v3, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 228
    new-instance v0, Llna;

    invoke-direct {v0}, Llna;-><init>()V

    .line 229
    iput v1, v0, Llna;->a:I

    .line 230
    iget-wide v4, p0, Llmn;->f:J

    iput-wide v4, v0, Llna;->b:J

    .line 231
    iget-wide v4, p0, Llmn;->g:J

    iput-wide v4, v0, Llna;->c:J

    .line 232
    iget-object v2, p0, Llmn;->c:Lllr;

    invoke-virtual {v2}, Lllr;->b()[B

    move-result-object v2

    iput-object v2, v0, Llna;->d:[B

    .line 234
    invoke-static {v0}, Llna;->a(Loxu;)[B

    move-result-object v4

    .line 235
    const/4 v2, 0x0

    .line 237
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    :try_start_1
    invoke-virtual {v0, v4}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 242
    invoke-static {v0}, Llrz;->a(Ljava/io/Closeable;)V

    move v0, v1

    .line 244
    goto :goto_0

    .line 226
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 240
    :catch_0
    move-exception v0

    move-object v0, v2

    :goto_2
    invoke-static {v0}, Llrz;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v2}, Llrz;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public f()V
    .locals 5

    .prologue
    .line 251
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Llmn;->a:Ljava/io/File;

    iget-object v0, p0, Llmn;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "~m"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 252
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 253
    return-void

    .line 251
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 285
    iget-wide v0, p0, Llmn;->g:J

    return-wide v0
.end method
