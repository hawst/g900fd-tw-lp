.class public final Lofe;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5166
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5175
    const/high16 v0, -0x80000000

    iput v0, p0, Lofe;->a:I

    .line 5166
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5194
    const/4 v0, 0x0

    .line 5195
    iget v1, p0, Lofe;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 5196
    const/4 v0, 0x1

    iget v1, p0, Lofe;->a:I

    .line 5197
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5199
    :cond_0
    iget-object v1, p0, Lofe;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5200
    const/4 v1, 0x2

    iget-object v2, p0, Lofe;->b:Ljava/lang/String;

    .line 5201
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5203
    :cond_1
    iget-object v1, p0, Lofe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5204
    iput v0, p0, Lofe;->ai:I

    .line 5205
    return v0
.end method

.method public a(Loxn;)Lofe;
    .locals 2

    .prologue
    .line 5213
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5214
    sparse-switch v0, :sswitch_data_0

    .line 5218
    iget-object v1, p0, Lofe;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5219
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofe;->ah:Ljava/util/List;

    .line 5222
    :cond_1
    iget-object v1, p0, Lofe;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5224
    :sswitch_0
    return-object p0

    .line 5229
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5230
    if-eqz v0, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    .line 5233
    :cond_2
    iput v0, p0, Lofe;->a:I

    goto :goto_0

    .line 5235
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lofe;->a:I

    goto :goto_0

    .line 5240
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofe;->b:Ljava/lang/String;

    goto :goto_0

    .line 5214
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5182
    iget v0, p0, Lofe;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 5183
    const/4 v0, 0x1

    iget v1, p0, Lofe;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5185
    :cond_0
    iget-object v0, p0, Lofe;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5186
    const/4 v0, 0x2

    iget-object v1, p0, Lofe;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5188
    :cond_1
    iget-object v0, p0, Lofe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5190
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5162
    invoke-virtual {p0, p1}, Lofe;->a(Loxn;)Lofe;

    move-result-object v0

    return-object v0
.end method
