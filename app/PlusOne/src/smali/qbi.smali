.class public final Lqbi;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Lqbj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 742
    invoke-direct {p0}, Loxq;-><init>()V

    .line 809
    const/high16 v0, -0x80000000

    iput v0, p0, Lqbi;->a:I

    .line 812
    const/4 v0, 0x0

    iput-object v0, p0, Lqbi;->b:Lqbj;

    .line 742
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 829
    const/4 v0, 0x0

    .line 830
    iget v1, p0, Lqbi;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 831
    const/4 v0, 0x1

    iget v1, p0, Lqbi;->a:I

    .line 832
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 834
    :cond_0
    iget-object v1, p0, Lqbi;->b:Lqbj;

    if-eqz v1, :cond_1

    .line 835
    const/4 v1, 0x2

    iget-object v2, p0, Lqbi;->b:Lqbj;

    .line 836
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 838
    :cond_1
    iget-object v1, p0, Lqbi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 839
    iput v0, p0, Lqbi;->ai:I

    .line 840
    return v0
.end method

.method public a(Loxn;)Lqbi;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 848
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 849
    sparse-switch v0, :sswitch_data_0

    .line 853
    iget-object v1, p0, Lqbi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 854
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbi;->ah:Ljava/util/List;

    .line 857
    :cond_1
    iget-object v1, p0, Lqbi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 859
    :sswitch_0
    return-object p0

    .line 864
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 865
    if-ne v0, v2, :cond_2

    .line 866
    iput v0, p0, Lqbi;->a:I

    goto :goto_0

    .line 868
    :cond_2
    iput v2, p0, Lqbi;->a:I

    goto :goto_0

    .line 873
    :sswitch_2
    iget-object v0, p0, Lqbi;->b:Lqbj;

    if-nez v0, :cond_3

    .line 874
    new-instance v0, Lqbj;

    invoke-direct {v0}, Lqbj;-><init>()V

    iput-object v0, p0, Lqbi;->b:Lqbj;

    .line 876
    :cond_3
    iget-object v0, p0, Lqbi;->b:Lqbj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 849
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 817
    iget v0, p0, Lqbi;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 818
    const/4 v0, 0x1

    iget v1, p0, Lqbi;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 820
    :cond_0
    iget-object v0, p0, Lqbi;->b:Lqbj;

    if-eqz v0, :cond_1

    .line 821
    const/4 v0, 0x2

    iget-object v1, p0, Lqbi;->b:Lqbj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 823
    :cond_1
    iget-object v0, p0, Lqbi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 825
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 738
    invoke-virtual {p0, p1}, Lqbi;->a(Loxn;)Lqbi;

    move-result-object v0

    return-object v0
.end method
