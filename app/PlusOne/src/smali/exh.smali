.class final Lexh;
.super Lhny;
.source "PG"


# instance fields
.field private a:Lexl;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lexl;I)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0, p1, p2}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 194
    iput-object p3, p0, Lexh;->a:Lexl;

    .line 195
    iput p4, p0, Lexh;->b:I

    .line 196
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 8

    .prologue
    .line 200
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 201
    const/4 v0, 0x0

    .line 202
    iget-object v1, p0, Lexh;->a:Lexl;

    invoke-virtual {v1}, Lexl;->c()Ljava/lang/String;

    move-result-object v2

    .line 204
    const/4 v1, 0x1

    .line 206
    if-eqz v2, :cond_8

    .line 207
    :try_start_0
    iget-object v0, p0, Lexh;->a:Lexl;

    .line 208
    invoke-virtual {v0}, Lexl;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lexh;->f()Landroid/content/Context;

    move-result-object v4

    const-class v0, Lkfd;

    invoke-static {v4, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    new-instance v5, Lexg;

    iget v6, p0, Lexh;->b:I

    invoke-direct {v5, v4, v6, v2}, Lexg;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    invoke-interface {v0, v5}, Lkfd;->a(Lkff;)V

    invoke-virtual {v5}, Lexg;->t()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v5}, Lexg;->i()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, v5, Lkff;->k:Ljava/lang/Exception;

    instance-of v0, v0, Lkgf;

    if-eqz v0, :cond_1

    new-instance v0, Lexe;

    const v1, 0x7f0a0697

    invoke-direct {v0, v1}, Lexe;-><init>(I)V

    throw v0
    :try_end_0
    .catch Lexe; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 216
    const/4 v0, 0x0

    .line 217
    const-string v2, "error"

    invoke-virtual {v1}, Lexe;->a()I

    move-result v1

    invoke-virtual {v3, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 220
    :goto_0
    new-instance v1, Lhoz;

    invoke-direct {v1, v0}, Lhoz;-><init>(Z)V

    .line 221
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 222
    return-object v1

    .line 208
    :cond_1
    :try_start_1
    new-instance v0, Lexe;

    const v1, 0x7f0a0695

    invoke-direct {v0, v1}, Lexe;-><init>(I)V

    throw v0

    :cond_2
    invoke-virtual {v5}, Lexg;->i()Ljava/lang/String;

    move-result-object v0

    .line 209
    :goto_1
    const-string v2, "profile_id"

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v0

    .line 211
    :goto_2
    if-eqz v2, :cond_7

    iget-object v0, p0, Lexh;->a:Lexl;

    invoke-virtual {v0}, Lexl;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 212
    iget-object v0, p0, Lexh;->a:Lexl;

    invoke-virtual {v0}, Lexl;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lexh;->f()Landroid/content/Context;

    move-result-object v5

    const-class v0, Lkfd;

    invoke-static {v5, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    new-instance v6, Lexf;

    iget v7, p0, Lexh;->b:I

    invoke-direct {v6, v5, v7, v2, v4}, Lexf;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Lkfd;->a(Lkff;)V

    invoke-virtual {v6}, Lexf;->t()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v6}, Lexf;->i()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_3
    const-string v0, "EsIntentRedirector"

    invoke-virtual {v6, v0}, Lexf;->d(Ljava/lang/String;)V

    iget-object v0, v6, Lkff;->k:Ljava/lang/Exception;

    instance-of v0, v0, Lkgf;

    if-eqz v0, :cond_5

    new-instance v0, Lexe;

    const v1, 0x7f0a0698

    invoke-direct {v0, v1}, Lexe;-><init>(I)V

    throw v0

    :cond_4
    move-object v0, v2

    .line 208
    goto :goto_1

    .line 212
    :cond_5
    new-instance v0, Lexe;

    const v1, 0x7f0a0695

    invoke-direct {v0, v1}, Lexe;-><init>(I)V

    throw v0

    :cond_6
    invoke-virtual {v6}, Lexf;->i()Ljava/lang/String;

    move-result-object v0

    .line 213
    const-string v2, "activity_id"

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lexe; {:try_start_1 .. :try_end_1} :catch_0

    :cond_7
    move v0, v1

    .line 218
    goto :goto_0

    :cond_8
    move-object v2, v0

    goto :goto_2
.end method
