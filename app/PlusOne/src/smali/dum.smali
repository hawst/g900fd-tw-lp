.class public final Ldum;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldun;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:J

.field private final e:I

.field private final f:J

.field private final g:Z

.field private h:Z


# direct methods
.method public constructor <init>(IJZ)V
    .locals 2

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldum;->a:Ljava/util/List;

    .line 171
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldum;->b:Ljava/util/Map;

    .line 186
    iput p1, p0, Ldum;->e:I

    .line 187
    iput-wide p2, p0, Ldum;->f:J

    .line 188
    iput-boolean p4, p0, Ldum;->g:Z

    .line 190
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldum;->h:Z

    .line 193
    :cond_0
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xb

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Ldum;->h:Z

    return v0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;J)Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 201
    new-instance v1, Ldun;

    iget v2, p0, Ldum;->e:I

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Ldun;-><init>(ILjava/lang/String;ILjava/lang/String;J)V

    iget-object v2, v1, Ldun;->b:Ljava/lang/String;

    iget v3, v1, Ldun;->c:I

    invoke-virtual {p0, v2, v3}, Ldum;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldum;->b:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v3, p0, Ldum;->h:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Ldum;->g:Z

    if-nez v3, :cond_2

    iget-wide v4, p0, Ldum;->d:J

    iget-wide v6, v1, Ldun;->e:J

    add-long/2addr v4, v6

    iget-wide v6, p0, Ldum;->f:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    iput-boolean v8, p0, Ldum;->h:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldum;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v3, p0, Ldum;->a:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Ldum;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Ldum;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldum;->c:I

    iget-wide v2, p0, Ldum;->d:J

    iget-wide v0, v1, Ldun;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldum;->d:J

    iget-wide v0, p0, Ldum;->d:J

    iget-wide v2, p0, Ldum;->f:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    iput-boolean v8, p0, Ldum;->h:Z

    :cond_3
    move v0, v8

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Ldum;->c:I

    return v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 260
    iget-wide v0, p0, Ldum;->d:J

    return-wide v0
.end method
