.class public final Ldfv;
.super Llol;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;
.implements Lhob;
.implements Lhsw;
.implements Llgv;


# instance fields
.field private final N:Lhov;

.field private final O:Lhje;

.field private final P:Ldgr;

.field private Q:Ldyh;

.field private R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ldgl;",
            ">;"
        }
    .end annotation
.end field

.field private S:Lcxl;

.field private T:Lcxf;

.field private U:Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;

.field private V:Ldgj;

.field private W:Ldeo;

.field private X:Z

.field private Y:Ldef;

.field private Z:Ldfo;

.field private final aa:Ldge;

.field private final ab:Ldgg;

.field private final ac:Ldgc;

.field private final ad:Lcxo;

.field private ae:Lfcu;

.field private af:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

.field private ag:Landroid/view/View;

.field private final ah:Licq;

.field private ai:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 127
    invoke-direct {p0}, Llol;-><init>()V

    .line 138
    new-instance v0, Lhov;

    iget-object v1, p0, Ldfv;->av:Llqm;

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Ldfv;->N:Lhov;

    .line 139
    new-instance v0, Lhje;

    iget-object v1, p0, Ldfv;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Ldfv;->O:Lhje;

    .line 143
    new-instance v0, Ldgr;

    invoke-direct {v0}, Ldgr;-><init>()V

    iput-object v0, p0, Ldfv;->P:Ldgr;

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldfv;->R:Ljava/util/List;

    .line 170
    new-instance v0, Ldge;

    invoke-direct {v0, p0}, Ldge;-><init>(Ldfv;)V

    iput-object v0, p0, Ldfv;->aa:Ldge;

    .line 172
    new-instance v0, Ldgg;

    invoke-direct {v0, p0}, Ldgg;-><init>(Ldfv;)V

    iput-object v0, p0, Ldfv;->ab:Ldgg;

    .line 174
    new-instance v0, Ldgc;

    invoke-direct {v0, p0}, Ldgc;-><init>(Ldfv;)V

    iput-object v0, p0, Ldfv;->ac:Ldgc;

    .line 176
    new-instance v0, Ldgd;

    invoke-direct {v0, p0}, Ldgd;-><init>(Ldfv;)V

    iput-object v0, p0, Ldfv;->ad:Lcxo;

    .line 182
    new-instance v0, Licq;

    iget-object v1, p0, Ldfv;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    iput-object v0, p0, Ldfv;->ah:Licq;

    .line 200
    new-instance v0, Licd;

    iget-object v1, p0, Ldfv;->av:Llqm;

    invoke-direct {v0, v1}, Licd;-><init>(Llqr;)V

    .line 201
    new-instance v0, Ldgn;

    iget-object v1, p0, Ldfv;->av:Llqm;

    sget-object v2, Lcxf;->N:Ldgu;

    new-instance v3, Ldfw;

    invoke-direct {v3, p0}, Ldfw;-><init>(Ldfv;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 211
    new-instance v0, Ldep;

    iget-object v1, p0, Ldfv;->av:Llqm;

    new-instance v2, Ldfx;

    invoke-direct {v2, p0}, Ldfx;-><init>(Ldfv;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 217
    new-instance v0, Ldeg;

    iget-object v1, p0, Ldfv;->av:Llqm;

    new-instance v2, Ldfy;

    invoke-direct {v2, p0}, Ldfy;-><init>(Ldfv;)V

    invoke-direct {v0, v1, v2}, Ldeg;-><init>(Llqr;Ldeh;)V

    .line 226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 228
    const-class v1, Ldba;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    const-class v1, Ldbs;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    const-class v1, Lcyk;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    new-instance v1, Ldfo;

    iget-object v2, p0, Ldfv;->av:Llqm;

    new-instance v3, Ldfz;

    invoke-direct {v3, p0, v0}, Ldfz;-><init>(Ldfv;Ljava/util/List;)V

    new-instance v0, Ldga;

    invoke-direct {v0, p0}, Ldga;-><init>(Ldfv;)V

    invoke-direct {v1, p0, v2, v3, v0}, Ldfo;-><init>(Lu;Llqr;Ldfr;Ldft;)V

    iput-object v1, p0, Ldfv;->Z:Ldfo;

    .line 264
    new-instance v0, Lhmg;

    new-instance v1, Ldgb;

    sget-object v2, Lond;->b:Lhmn;

    invoke-direct {v1, p0, v2}, Ldgb;-><init>(Ldfv;Lhmn;)V

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmk;)V

    iget-object v1, p0, Ldfv;->au:Llnh;

    .line 279
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 921
    return-void
.end method

.method private static a(Landroid/view/View;II)Landroid/view/View;
    .locals 1

    .prologue
    .line 731
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 732
    if-eqz v0, :cond_0

    .line 737
    :goto_0
    return-object v0

    .line 736
    :cond_0
    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 737
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Ldfv;)Ldfo;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->Z:Ldfo;

    return-object v0
.end method

.method static synthetic a(Ldfv;Ljava/lang/Class;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0, p1}, Ldfv;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lu;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 722
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 723
    invoke-virtual {p0}, Ldfv;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "pager_identifier"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lat;",
            "Landroid/os/Bundle;",
            "I",
            "Ljava/lang/Class",
            "<+",
            "Lu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 700
    :try_start_0
    invoke-virtual {p4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    .line 701
    invoke-virtual {v0, p2}, Lu;->f(Landroid/os/Bundle;)V

    .line 702
    if-eqz p3, :cond_0

    .line 703
    invoke-direct {p0, p4}, Ldfv;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p3, v0, v1}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    .line 713
    :goto_0
    return-void

    .line 705
    :cond_0
    invoke-direct {p0, p4}, Ldfv;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lat;->a(Lu;Ljava/lang/String;)Lat;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 707
    :catch_0
    move-exception v0

    .line 708
    const-string v1, "PhotoFragment"

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Couldn\'t instantiate fragment: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 709
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 710
    :catch_1
    move-exception v0

    .line 711
    const-string v1, "PhotoFragment"

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Couldn\'t instantiate fragment: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 712
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lat;",
            "Landroid/os/Bundle;",
            "Ljava/lang/Class",
            "<+",
            "Lu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 688
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    .line 689
    return-void
.end method

.method static synthetic a(Ldfv;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Ldfv;->c(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Ldfv;Landroid/view/View;Llix;)V
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->ag:Landroid/view/View;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Ldfv;->ag:Landroid/view/View;

    invoke-virtual {p0}, Ldfv;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1004bd

    const v2, 0x7f1004bc

    invoke-static {v0, v1, v2}, Ldfv;->a(Landroid/view/View;II)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->removeAllViews()V

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->addView(Landroid/view/View;)V

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a(Llix;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Ldfv;Z)Z
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Ldfv;->X:Z

    return p1
.end method

.method static synthetic b(Ldfv;)Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->U:Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;

    return-object v0
.end method

.method static synthetic c(Ldfv;)Ldeo;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->W:Ldeo;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 644
    if-nez p1, :cond_0

    .line 662
    :goto_0
    return-void

    .line 647
    :cond_0
    invoke-direct {p0}, Ldfv;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 648
    iget-object v0, p0, Ldfv;->T:Lcxf;

    invoke-virtual {v0}, Lcxf;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ldfv;->S:Lcxl;

    .line 649
    invoke-virtual {v0}, Lcxl;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 650
    iget-object v0, p0, Ldfv;->ah:Licq;

    const v1, 0x7f0a064b

    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    .line 651
    iget-object v0, p0, Ldfv;->ah:Licq;

    invoke-virtual {v0}, Licq;->d()V

    .line 661
    :goto_1
    iget-object v0, p0, Ldfv;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto :goto_0

    .line 652
    :cond_1
    iget-object v0, p0, Ldfv;->aa:Ldge;

    invoke-virtual {v0}, Ldge;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v0}, Ldgj;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 653
    iget-object v0, p0, Ldfv;->ah:Licq;

    invoke-virtual {v0}, Licq;->a()V

    goto :goto_1

    .line 655
    :cond_2
    iget-object v0, p0, Ldfv;->ah:Licq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Licq;->a(Ljava/lang/CharSequence;)Licq;

    .line 656
    iget-object v0, p0, Ldfv;->ah:Licq;

    invoke-virtual {v0}, Licq;->d()V

    goto :goto_1

    .line 659
    :cond_3
    iget-object v0, p0, Ldfv;->ah:Licq;

    invoke-virtual {v0}, Licq;->e()V

    goto :goto_1
.end method

.method static synthetic d(Ldfv;)Llnl;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->at:Llnl;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 292
    iget-boolean v0, p0, Ldfv;->ai:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldfv;->X:Z

    if-eqz v0, :cond_0

    .line 293
    invoke-static {p0}, Lhmc;->a(Llol;)V

    .line 295
    :cond_0
    return-void
.end method

.method static synthetic e(Ldfv;)Llnh;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->au:Llnh;

    return-object v0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Ldfv;->T:Lcxf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfv;->T:Lcxf;

    invoke-virtual {v0}, Lcxf;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Ldfv;)Llnl;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->at:Llnl;

    return-object v0
.end method

.method static synthetic g(Ldfv;)Llnh;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->au:Llnh;

    return-object v0
.end method

.method static synthetic h(Ldfv;)Ljava/util/List;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->R:Ljava/util/List;

    return-object v0
.end method

.method static synthetic i(Ldfv;)Ldge;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->aa:Ldge;

    return-object v0
.end method

.method static synthetic j(Ldfv;)Ldgg;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->ab:Ldgg;

    return-object v0
.end method

.method public static k(Landroid/os/Bundle;)Ldfv;
    .locals 1

    .prologue
    .line 188
    new-instance v0, Ldfv;

    invoke-direct {v0}, Ldfv;-><init>()V

    .line 189
    invoke-virtual {v0, p0}, Ldfv;->f(Landroid/os/Bundle;)V

    .line 191
    return-object v0
.end method

.method static synthetic k(Ldfv;)Ldyh;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->Q:Ldyh;

    return-object v0
.end method

.method static synthetic l(Ldfv;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->af:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfv;->af:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldfv;->af:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->c()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ldfv;->af:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->b()V

    goto :goto_0
.end method

.method static synthetic m(Ldfv;)Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->af:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfv;->af:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic n(Ldfv;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Ldfv;->d()V

    return-void
.end method

.method static synthetic o(Ldfv;)Lhje;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->O:Lhje;

    return-object v0
.end method

.method static synthetic p(Ldfv;)Z
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Ldfv;->e()Z

    move-result v0

    return v0
.end method

.method static synthetic q(Ldfv;)Ldef;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldfv;->Y:Ldef;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 594
    sget-object v0, Lhmw;->ah:Lhmw;

    return-object v0
.end method

.method public V()Z
    .locals 2

    .prologue
    .line 667
    iget-object v0, p0, Ldfv;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgl;

    .line 668
    invoke-interface {v0}, Ldgl;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    const/4 v0, 0x1

    .line 673
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const v8, 0x7f1004b7

    const v7, 0x7f1004b5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 352
    const v0, 0x7f040183

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 353
    if-nez v4, :cond_0

    .line 354
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Couldn\'t inflate view."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :cond_0
    invoke-virtual {p0}, Ldfv;->q()Lae;

    move-result-object v3

    const-class v0, Lcxl;

    invoke-direct {p0, v0}, Ldfv;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcxl;

    iput-object v0, p0, Ldfv;->S:Lcxl;

    const-class v0, Lcxf;

    invoke-direct {p0, v0}, Ldfv;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lcxf;

    iput-object v0, p0, Ldfv;->T:Lcxf;

    iget-object v0, p0, Ldfv;->S:Lcxl;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Ldfv;->T:Lcxf;

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    xor-int/2addr v0, v3

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "All or no mandatory child fragments must be found."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ldfv;->S:Lcxl;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_11

    .line 358
    invoke-virtual {p0}, Ldfv;->q()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->f()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lae;->f()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragments already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    .line 357
    goto :goto_2

    .line 358
    :cond_5
    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    invoke-virtual {p0}, Ldfv;->k()Landroid/os/Bundle;

    move-result-object v3

    new-instance v5, Lcxl;

    invoke-direct {v5}, Lcxl;-><init>()V

    iput-object v5, p0, Ldfv;->S:Lcxl;

    iget-object v5, p0, Ldfv;->S:Lcxl;

    invoke-virtual {v5, v3}, Lcxl;->f(Landroid/os/Bundle;)V

    new-instance v5, Lcxf;

    invoke-direct {v5}, Lcxf;-><init>()V

    iput-object v5, p0, Ldfv;->T:Lcxf;

    iget-object v5, p0, Ldfv;->T:Lcxf;

    invoke-virtual {v5, v3}, Lcxf;->f(Landroid/os/Bundle;)V

    iget-object v3, p0, Ldfv;->S:Lcxl;

    const-class v5, Lcxl;

    invoke-direct {p0, v5}, Ldfv;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    const v3, 0x7f1004af

    iget-object v5, p0, Ldfv;->T:Lcxf;

    const-class v6, Lcxf;

    invoke-direct {p0, v6}, Ldfv;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v3, v5, v6}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    iget-object v3, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v3}, Ldgj;->a()Z

    move-result v3

    if-nez v3, :cond_10

    invoke-virtual {p0}, Ldfv;->k()Landroid/os/Bundle;

    move-result-object v3

    const v5, 0x7f1004b1

    const-class v6, Lcwz;

    invoke-direct {p0, v0, v3, v5, v6}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    iget-object v5, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v5}, Ldgj;->e()Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v5}, Ldgj;->g()Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v5}, Ldgj;->f()Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v5}, Ldgj;->h()Z

    move-result v5

    if-eqz v5, :cond_7

    :cond_6
    move v2, v1

    :cond_7
    if-eqz v2, :cond_8

    const v2, 0x7f1004b9

    const-class v5, Ldgx;

    invoke-direct {p0, v0, v3, v2, v5}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    :cond_8
    iget-object v2, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v2}, Ldgj;->e()Z

    move-result v2

    if-eqz v2, :cond_9

    const-class v2, Ldhe;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lcvq;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lcwt;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lcyt;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lcvx;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lcvi;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lcyg;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lcyo;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lcyc;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lcyx;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const v2, 0x7f1004b3

    const-class v5, Ldba;

    invoke-direct {p0, v0, v3, v2, v5}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    :cond_9
    iget-object v2, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v2}, Ldgj;->h()Z

    move-result v2

    if-eqz v2, :cond_a

    const-class v2, Lcvl;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    :cond_a
    iget-object v2, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v2}, Ldgj;->f()Z

    move-result v2

    if-eqz v2, :cond_b

    const-class v2, Lcvu;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    :cond_b
    iget-object v2, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v2}, Ldgj;->g()Z

    move-result v2

    if-eqz v2, :cond_c

    const-class v2, Lcxu;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    :cond_c
    iget-object v2, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v2}, Ldgj;->d()Z

    move-result v2

    if-eqz v2, :cond_d

    const v2, 0x7f1004b2

    const-class v5, Lcyk;

    invoke-direct {p0, v0, v3, v2, v5}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    :cond_d
    iget-object v2, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v2}, Ldgj;->c()Z

    move-result v2

    if-eqz v2, :cond_e

    const-class v2, Lcxr;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Lczf;

    invoke-direct {p0, v0, v3, v8, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    const-class v2, Ldbj;

    invoke-direct {p0, v0, v3, v8, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    const-class v2, Lczb;

    invoke-direct {p0, v0, v3, v7, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    const v2, 0x7f1004b0

    const-class v5, Ldbs;

    invoke-direct {p0, v0, v3, v2, v5}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    const-class v2, Ldaq;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Ldac;

    invoke-direct {p0, v0, v3, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;Ljava/lang/Class;)V

    const-class v2, Ldco;

    invoke-direct {p0, v0, v3, v7, v2}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    :cond_e
    iget-object v2, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v2}, Ldgj;->b()Z

    move-result v2

    if-eqz v2, :cond_f

    new-instance v2, Lczm;

    invoke-direct {v2}, Lczm;-><init>()V

    invoke-virtual {v2, v3}, Lczm;->f(Landroid/os/Bundle;)V

    const v5, 0x7f1004ba

    const-class v6, Lczm;

    invoke-direct {p0, v6}, Ldfv;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v2, v6}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    :cond_f
    iget-object v2, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v2}, Ldgj;->c()Z

    move-result v2

    if-eqz v2, :cond_10

    iget-object v2, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v2}, Ldgj;->g()Z

    move-result v2

    if-eqz v2, :cond_10

    const v2, 0x7f1004b6

    const-class v5, Lcww;

    invoke-direct {p0, v0, v3, v2, v5}, Ldfv;->a(Lat;Landroid/os/Bundle;ILjava/lang/Class;)V

    :cond_10
    invoke-virtual {v0}, Lat;->b()I

    .line 360
    :cond_11
    iget-object v0, p0, Ldfv;->S:Lcxl;

    iget-object v2, p0, Ldfv;->ad:Lcxo;

    invoke-virtual {v0, v2}, Lcxl;->a(Lcxo;)V

    .line 362
    iget-object v0, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v0}, Ldgj;->c()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 363
    const v0, 0x7f1002a9

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;

    iput-object v0, p0, Ldfv;->U:Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;

    iget-object v0, p0, Ldfv;->U:Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;

    if-eqz v0, :cond_12

    const v0, 0x7f1004bb

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/TouchInterceptParent;

    iget-object v2, p0, Ldfv;->U:Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;->a(Lcom/google/android/apps/plus/views/TouchInterceptParent;)V

    iget-object v0, p0, Ldfv;->U:Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;->d(Z)V

    iget-object v0, p0, Ldfv;->U:Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;

    iget-object v1, p0, Ldfv;->Z:Ldfo;

    invoke-virtual {v1}, Ldfo;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/pager/core/PhotoFragmentTouchHandler;->c(Z)V

    const v0, 0x7f1004bd

    const v1, 0x7f1004bc

    invoke-static {v4, v0, v1}, Ldfv;->a(Landroid/view/View;II)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    iput-object v0, p0, Ldfv;->af:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    iget-object v0, p0, Ldfv;->af:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    iget-object v1, p0, Ldfv;->ac:Ldgc;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a(Lljp;)V

    .line 367
    :cond_12
    return-object v4
.end method

.method public a()V
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldfv;->ai:Z

    .line 288
    invoke-direct {p0}, Ldfv;->d()V

    .line 289
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 299
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 300
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 301
    new-instance v1, Lhpf;

    iget-object v2, p0, Ldfv;->at:Llnl;

    .line 302
    invoke-virtual {p0}, Ldfv;->p()Lae;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lhpf;-><init>(Landroid/content/Context;Lae;)V

    .line 303
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, p0, v2, v3}, Lhos;->a(Lu;Ljava/lang/String;Z)V

    .line 304
    invoke-virtual {v0, v1}, Lhoc;->a(Lhos;)V

    .line 305
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 306
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 372
    invoke-super {p0, p1, p2}, Llol;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 374
    invoke-direct {p0, p1}, Ldfv;->c(Landroid/view/View;)V

    .line 375
    return-void
.end method

.method public a(Lhjk;)V
    .locals 2

    .prologue
    .line 563
    iget-object v0, p0, Ldfv;->W:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    const v0, 0x7f10067b

    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 566
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 418
    const-string v0, "GetRedirectUrlTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Ldfv;->ae:Lfcu;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Ldfv;->ae:Lfcu;

    invoke-virtual {v0, p2}, Lfcu;->a(Lhoz;)V

    .line 423
    :cond_0
    return-void
.end method

.method public a(Loo;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 550
    iget-object v0, p0, Ldfv;->at:Llnl;

    invoke-static {v0, v2}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 551
    iget-object v0, p0, Ldfv;->at:Llnl;

    invoke-static {v0, v2}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    invoke-static {p1, v1}, Lley;->a(Loo;Z)V

    .line 553
    invoke-virtual {p1, v1}, Loo;->c(Z)V

    .line 555
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 580
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 581
    const v1, 0x7f10067b

    if-ne v0, v1, :cond_0

    .line 582
    iget-object v0, p0, Ldfv;->P:Ldgr;

    sget-object v1, Ldgr;->a:Ldgu;

    invoke-virtual {v0, v1}, Ldgr;->d(Ldgu;)V

    .line 583
    const/4 v0, 0x1

    .line 585
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 534
    invoke-super {p0}, Llol;->aO_()V

    .line 536
    iget-object v0, p0, Ldfv;->Q:Ldyh;

    iget-object v1, p0, Ldfv;->aa:Ldge;

    invoke-interface {v0, v1}, Ldyh;->a(Ldfu;)V

    .line 537
    iget-object v0, p0, Ldfv;->Q:Ldyh;

    iget-object v1, p0, Ldfv;->ab:Ldgg;

    invoke-interface {v0, v1}, Ldyh;->a(Ldyi;)V

    .line 538
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 599
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 559
    return-void
.end method

.method public c()Ldeo;
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Ldfv;->W:Ldeo;

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 310
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 311
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v3, Lhmq;

    invoke-virtual {v0, v3, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 313
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v3, Ldyh;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyh;

    iput-object v0, p0, Ldfv;->Q:Ldyh;

    .line 315
    iget-object v4, p0, Ldfv;->at:Llnl;

    .line 316
    invoke-virtual {p0}, Ldfv;->k()Landroid/os/Bundle;

    move-result-object v5

    new-instance v3, Ldgk;

    invoke-direct {v3}, Ldgk;-><init>()V

    const-string v0, "for_animation"

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v3, Ldgk;->a:Z

    iget-boolean v0, v3, Ldgk;->a:Z

    if-eqz v0, :cond_1

    move-object v0, v3

    .line 317
    :goto_0
    invoke-virtual {v0}, Ldgk;->a()Ldgj;

    move-result-object v0

    iput-object v0, p0, Ldfv;->V:Ldgj;

    .line 319
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v1, Ldgm;

    new-instance v2, Ldgh;

    invoke-direct {v2, p0}, Ldgh;-><init>(Ldfv;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 321
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v1, Ldgi;

    new-instance v2, Ldgf;

    invoke-direct {v2, p0}, Ldgf;-><init>(Ldfv;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 324
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v1, Ldgr;

    iget-object v2, p0, Ldfv;->P:Ldgr;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 326
    new-instance v0, Lfcu;

    iget-object v1, p0, Ldfv;->at:Llnl;

    invoke-direct {v0, v1}, Lfcu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldfv;->ae:Lfcu;

    .line 327
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v1, Lkzh;

    iget-object v2, p0, Ldfv;->ae:Lfcu;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 329
    new-instance v0, Ldeo;

    iget-object v1, p0, Ldfv;->N:Lhov;

    invoke-direct {v0, v1}, Ldeo;-><init>(Lhov;)V

    iput-object v0, p0, Ldfv;->W:Ldeo;

    .line 331
    new-instance v0, Ldef;

    invoke-direct {v0}, Ldef;-><init>()V

    iput-object v0, p0, Ldfv;->Y:Ldef;

    .line 334
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v1, Ldeo;

    iget-object v2, p0, Ldfv;->W:Ldeo;

    .line 335
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldel;

    new-instance v2, Ldel;

    iget-object v3, p0, Ldfv;->N:Lhov;

    invoke-direct {v2, v3}, Ldel;-><init>(Lhov;)V

    .line 336
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldef;

    iget-object v2, p0, Ldfv;->Y:Ldef;

    .line 337
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldes;

    new-instance v2, Ldes;

    iget-object v3, p0, Ldfv;->N:Lhov;

    invoke-direct {v2, v3}, Ldes;-><init>(Lhov;)V

    .line 338
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldei;

    new-instance v2, Ldei;

    iget-object v3, p0, Ldfv;->N:Lhov;

    invoke-direct {v2, v3}, Ldei;-><init>(Lhov;)V

    .line 339
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 343
    iget-object v0, p0, Ldfv;->V:Ldgj;

    invoke-virtual {v0}, Ldgj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v1, Ldec;

    new-instance v2, Ldec;

    iget-object v3, p0, Ldfv;->N:Lhov;

    invoke-direct {v2, v3}, Ldec;-><init>(Lhov;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 345
    iget-object v0, p0, Ldfv;->au:Llnh;

    const-class v1, Ldeb;

    new-instance v2, Ldeb;

    iget-object v3, p0, Ldfv;->N:Lhov;

    invoke-direct {v2, v3}, Ldeb;-><init>(Lhov;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 347
    :cond_0
    return-void

    .line 316
    :cond_1
    const-string v0, "account_id"

    invoke-virtual {v5, v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    const-class v0, Lieh;

    invoke-static {v4, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    const-string v7, "disable_chromecast"

    invoke-virtual {v5, v7, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_2

    sget-object v7, Ldxd;->m:Lief;

    invoke-interface {v0, v7, v6}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "for_animation"

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Ldgk;->b:Z

    iput-boolean v1, v3, Ldgk;->d:Z

    const-string v0, "force_return_edit_list"

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v1, v3, Ldgk;->g:Z

    move-object v0, v3

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    const-class v0, Lctq;

    invoke-static {v4, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "selected_only"

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v3

    goto/16 :goto_0

    :cond_4
    if-eq v6, v8, :cond_5

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Ldgk;->c:Z

    iput-boolean v1, v3, Ldgk;->e:Z

    const-string v0, "prevent_share"

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, v3, Ldgk;->f:Z

    const-string v0, "prevent_edit"

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    iput-boolean v0, v3, Ldgk;->g:Z

    const-string v0, "prevent_delete"

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_8

    :goto_5
    iput-boolean v1, v3, Ldgk;->h:Z

    move-object v0, v3

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_5
.end method

.method public l(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 747
    iget-object v0, p0, Ldfv;->ae:Lfcu;

    if-eqz v0, :cond_0

    .line 748
    iget-object v0, p0, Ldfv;->ae:Lfcu;

    invoke-virtual {v0, p1}, Lfcu;->l(Landroid/os/Bundle;)V

    .line 750
    :cond_0
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 542
    invoke-super {p0}, Llol;->z()V

    .line 544
    iget-object v0, p0, Ldfv;->Q:Ldyh;

    iget-object v1, p0, Ldfv;->aa:Ldge;

    invoke-interface {v0, v1}, Ldyh;->b(Ldfu;)V

    .line 545
    iget-object v0, p0, Ldfv;->Q:Ldyh;

    iget-object v1, p0, Ldfv;->ab:Ldgg;

    invoke-interface {v0, v1}, Ldyh;->b(Ldyi;)V

    .line 546
    return-void
.end method
