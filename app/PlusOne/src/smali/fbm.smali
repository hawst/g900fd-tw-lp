.class final Lfbm;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lfbl;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:I


# direct methods
.method constructor <init>(Landroid/content/Context;ILfbl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 227
    iput-object p1, p0, Lfbm;->b:Landroid/content/Context;

    .line 228
    iput p2, p0, Lfbm;->d:I

    .line 229
    iput-object p3, p0, Lfbm;->a:Lfbl;

    .line 230
    iput-object p4, p0, Lfbm;->c:Ljava/lang/String;

    .line 231
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 235
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 236
    :cond_0
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 250
    :goto_0
    return-object v0

    .line 239
    :cond_1
    aget-object v5, p1, v6

    .line 242
    iget-object v0, p0, Lfbm;->c:Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    invoke-static {v7, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 243
    new-instance v0, Ldih;

    iget-object v1, p0, Lfbm;->b:Landroid/content/Context;

    iget v2, p0, Lfbm;->d:I

    invoke-direct {v0, v1, v2, v5}, Ldih;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 249
    :goto_1
    invoke-virtual {v0}, Lkff;->l()V

    .line 250
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v6, v7

    :cond_3
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 245
    :cond_4
    iget-object v0, p0, Lfbm;->c:Ljava/lang/String;

    invoke-static {v0}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 246
    iget-object v0, p0, Lfbm;->b:Landroid/content/Context;

    iget v1, p0, Lfbm;->d:I

    iget-object v3, p0, Lfbm;->c:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static/range {v0 .. v6}, Ldig;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lkff;

    move-result-object v0

    goto :goto_1
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lfbm;->a:Lfbl;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lfbl;->a(Lfbl;Z)V

    .line 256
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 219
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lfbm;->a([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 219
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lfbm;->a(Ljava/lang/Boolean;)V

    return-void
.end method
