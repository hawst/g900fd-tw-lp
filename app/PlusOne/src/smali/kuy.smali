.class public final Lkuy;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lhmm;


# instance fields
.field private Q:Lhee;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/String;

.field private U:I

.field private V:I

.field private W:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0}, Lloj;-><init>()V

    .line 60
    new-instance v0, Lhmf;

    iget-object v1, p0, Lkuy;->P:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 61
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lt;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lkuy;

    invoke-direct {v0}, Lkuy;-><init>()V

    .line 82
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 83
    const-string v2, "square_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v2, "qualified_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v2, "user_name"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v2, "user_member_type"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 87
    const-string v2, "viewer_member_type"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 88
    invoke-virtual {v0, v1}, Lt;->f(Landroid/os/Bundle;)V

    .line 89
    return-object v0
.end method


# virtual methods
.method public U()Landroid/util/SparseIntArray;
    .locals 10

    .prologue
    const v8, 0x7f0a0242

    const/16 v7, 0xe

    const/16 v6, 0xc

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 132
    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    .line 133
    iget v3, p0, Lkuy;->V:I

    invoke-static {v3}, Lkto;->b(I)Z

    move-result v3

    .line 134
    iget-object v4, p0, Lkuy;->Q:Lhee;

    invoke-interface {v4}, Lhee;->g()Lhej;

    move-result-object v4

    const-string v5, "gaia_id"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 135
    iget-object v5, p0, Lkuy;->S:Ljava/lang/String;

    invoke-static {v5}, Lkto;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 137
    if-eqz v3, :cond_0

    .line 138
    const v0, 0x7f0a0245

    invoke-virtual {v2, v6, v0}, Landroid/util/SparseIntArray;->append(II)V

    .line 140
    :cond_0
    const v0, 0x7f0a0244

    invoke-virtual {v2, v7, v0}, Landroid/util/SparseIntArray;->append(II)V

    .line 198
    :cond_1
    :goto_0
    return-object v2

    .line 144
    :cond_2
    iget v4, p0, Lkuy;->U:I

    packed-switch v4, :pswitch_data_0

    move v1, v0

    .line 187
    :goto_1
    iget v4, p0, Lkuy;->U:I

    invoke-static {v4}, Lkto;->b(I)Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v3, :cond_1

    .line 189
    :cond_3
    if-eqz v0, :cond_4

    .line 190
    const/4 v0, 0x6

    const v3, 0x7f0a0248

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseIntArray;->append(II)V

    .line 192
    :cond_4
    if-nez v1, :cond_1

    iget-object v0, p0, Lkuy;->S:Ljava/lang/String;

    invoke-static {v0}, Lkto;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 193
    const/4 v0, 0x7

    const v1, 0x7f0a0246

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseIntArray;->append(II)V

    goto :goto_0

    .line 147
    :pswitch_0
    const/16 v4, 0x8

    const v5, 0x7f0a0247

    invoke-virtual {v2, v4, v5}, Landroid/util/SparseIntArray;->append(II)V

    goto :goto_1

    .line 151
    :pswitch_1
    const v4, 0x7f0a0249

    invoke-virtual {v2, v1, v4}, Landroid/util/SparseIntArray;->append(II)V

    .line 152
    const/4 v1, 0x4

    const v4, 0x7f0a024a

    invoke-virtual {v2, v1, v4}, Landroid/util/SparseIntArray;->append(II)V

    move v1, v0

    .line 153
    goto :goto_1

    .line 156
    :pswitch_2
    const/16 v1, 0x16

    const v4, 0x7f0a024b

    invoke-virtual {v2, v1, v4}, Landroid/util/SparseIntArray;->append(II)V

    move v1, v0

    .line 157
    goto :goto_1

    .line 161
    :pswitch_3
    if-eqz v3, :cond_5

    .line 162
    const v4, 0x7f0a0243

    invoke-virtual {v2, v6, v4}, Landroid/util/SparseIntArray;->append(II)V

    .line 163
    invoke-virtual {v2, v7, v8}, Landroid/util/SparseIntArray;->append(II)V

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    .line 169
    :pswitch_4
    if-eqz v3, :cond_5

    .line 170
    const/16 v4, 0xa

    const v5, 0x7f0a0241

    invoke-virtual {v2, v4, v5}, Landroid/util/SparseIntArray;->append(II)V

    .line 171
    const/16 v4, 0xd

    invoke-virtual {v2, v4, v8}, Landroid/util/SparseIntArray;->append(II)V

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    .line 177
    :pswitch_5
    if-eqz v3, :cond_5

    .line 178
    const/16 v4, 0x9

    const v5, 0x7f0a0240

    invoke-virtual {v2, v4, v5}, Landroid/util/SparseIntArray;->append(II)V

    :cond_5
    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    .line 144
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 101
    invoke-super {p0, p1}, Lloj;->a(Landroid/os/Bundle;)V

    .line 102
    invoke-virtual {p0}, Lkuy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkuy;->R:Ljava/lang/String;

    .line 103
    invoke-virtual {p0}, Lkuy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "qualified_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkuy;->S:Ljava/lang/String;

    .line 104
    invoke-virtual {p0}, Lkuy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "user_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkuy;->T:Ljava/lang/String;

    .line 105
    invoke-virtual {p0}, Lkuy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "user_member_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lkuy;->U:I

    .line 106
    invoke-virtual {p0}, Lkuy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "viewer_member_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lkuy;->V:I

    .line 108
    invoke-virtual {p0}, Lkuy;->U()Landroid/util/SparseIntArray;

    move-result-object v0

    iput-object v0, p0, Lkuy;->W:Landroid/util/SparseIntArray;

    .line 109
    iget-object v0, p0, Lkuy;->W:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lkuy;->N:Llnl;

    iget-object v1, p0, Lkuy;->N:Llnl;

    const v2, 0x7f0a023e

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lkuy;->T:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 112
    invoke-virtual {p0}, Lkuy;->a()V

    .line 114
    :cond_0
    return-void
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 248
    new-instance v0, Lkqw;

    sget-object v1, Lomv;->y:Lhmn;

    iget-object v2, p0, Lkuy;->R:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 118
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lkuy;->N:Llnl;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 119
    iget-object v0, p0, Lkuy;->T:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 120
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 122
    iget-object v0, p0, Lkuy;->W:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lkuy;->W:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 124
    iget-object v3, p0, Lkuy;->N:Llnl;

    iget-object v4, p0, Lkuy;->W:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 123
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 126
    :cond_0
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 128
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 95
    iget-object v0, p0, Lkuy;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkuy;->Q:Lhee;

    .line 96
    iget-object v0, p0, Lkuy;->O:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 97
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 203
    if-ltz p2, :cond_0

    .line 204
    iget-object v0, p0, Lkuy;->W:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    .line 205
    iget-object v0, p0, Lkuy;->O:Llnh;

    const-class v2, Lkuz;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkuz;

    iget-object v2, p0, Lkuy;->R:Ljava/lang/String;

    iget-object v3, p0, Lkuy;->S:Ljava/lang/String;

    .line 206
    invoke-interface {v0, v2, v3, v1}, Lkuz;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 207
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    .line 208
    :goto_0
    if-eqz v0, :cond_0

    .line 209
    iget-object v1, p0, Lkuy;->N:Llnl;

    const/4 v2, 0x4

    new-instance v3, Lhml;

    invoke-direct {v3}, Lhml;-><init>()V

    new-instance v4, Lhmk;

    invoke-direct {v4, v0}, Lhmk;-><init>(Lhmn;)V

    .line 210
    invoke-virtual {v3, v4}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v0

    iget-object v3, p0, Lkuy;->N:Llnl;

    .line 211
    invoke-virtual {v0, v3}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v0

    .line 209
    invoke-static {v1, v2, v0}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 214
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 215
    return-void

    .line 207
    :pswitch_1
    sget-object v0, Lomv;->W:Lhmn;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lomv;->X:Lhmn;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lomv;->H:Lhmn;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lomv;->I:Lhmn;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lomv;->J:Lhmn;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lomv;->Y:Lhmn;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lomv;->g:Lhmn;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lomv;->ad:Lhmn;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lomv;->d:Lhmn;

    goto :goto_0

    :pswitch_a
    sget-object v0, Lomv;->O:Lhmn;

    goto :goto_0

    :pswitch_b
    sget-object v0, Lomv;->j:Lhmn;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
    .end packed-switch
.end method
