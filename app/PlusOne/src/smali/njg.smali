.class public final Lnjg;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lnix;

.field public c:Loae;

.field private d:Ljava/lang/Boolean;

.field private e:Lnhy;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 6520
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6534
    iput v1, p0, Lnjg;->a:I

    .line 6539
    iput-object v0, p0, Lnjg;->e:Lnhy;

    .line 6542
    iput v1, p0, Lnjg;->f:I

    .line 6545
    iput-object v0, p0, Lnjg;->b:Lnix;

    .line 6548
    iput-object v0, p0, Lnjg;->c:Loae;

    .line 6520
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 6577
    const/4 v0, 0x0

    .line 6578
    iget v1, p0, Lnjg;->a:I

    if-eq v1, v3, :cond_0

    .line 6579
    const/4 v0, 0x1

    iget v1, p0, Lnjg;->a:I

    .line 6580
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6582
    :cond_0
    iget-object v1, p0, Lnjg;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 6583
    const/4 v1, 0x2

    iget-object v2, p0, Lnjg;->d:Ljava/lang/Boolean;

    .line 6584
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6586
    :cond_1
    iget-object v1, p0, Lnjg;->e:Lnhy;

    if-eqz v1, :cond_2

    .line 6587
    const/4 v1, 0x3

    iget-object v2, p0, Lnjg;->e:Lnhy;

    .line 6588
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6590
    :cond_2
    iget v1, p0, Lnjg;->f:I

    if-eq v1, v3, :cond_3

    .line 6591
    const/4 v1, 0x4

    iget v2, p0, Lnjg;->f:I

    .line 6592
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6594
    :cond_3
    iget-object v1, p0, Lnjg;->b:Lnix;

    if-eqz v1, :cond_4

    .line 6595
    const/4 v1, 0x5

    iget-object v2, p0, Lnjg;->b:Lnix;

    .line 6596
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6598
    :cond_4
    iget-object v1, p0, Lnjg;->c:Loae;

    if-eqz v1, :cond_5

    .line 6599
    const/4 v1, 0x6

    iget-object v2, p0, Lnjg;->c:Loae;

    .line 6600
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6602
    :cond_5
    iget-object v1, p0, Lnjg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6603
    iput v0, p0, Lnjg;->ai:I

    .line 6604
    return v0
.end method

.method public a(Loxn;)Lnjg;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 6612
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6613
    sparse-switch v0, :sswitch_data_0

    .line 6617
    iget-object v1, p0, Lnjg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6618
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjg;->ah:Ljava/util/List;

    .line 6621
    :cond_1
    iget-object v1, p0, Lnjg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6623
    :sswitch_0
    return-object p0

    .line 6628
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6629
    if-eqz v0, :cond_2

    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 6637
    :cond_2
    iput v0, p0, Lnjg;->a:I

    goto :goto_0

    .line 6639
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnjg;->a:I

    goto :goto_0

    .line 6644
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnjg;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 6648
    :sswitch_3
    iget-object v0, p0, Lnjg;->e:Lnhy;

    if-nez v0, :cond_4

    .line 6649
    new-instance v0, Lnhy;

    invoke-direct {v0}, Lnhy;-><init>()V

    iput-object v0, p0, Lnjg;->e:Lnhy;

    .line 6651
    :cond_4
    iget-object v0, p0, Lnjg;->e:Lnhy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6655
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6656
    if-eq v0, v2, :cond_5

    if-ne v0, v3, :cond_6

    .line 6658
    :cond_5
    iput v0, p0, Lnjg;->f:I

    goto :goto_0

    .line 6660
    :cond_6
    iput v2, p0, Lnjg;->f:I

    goto :goto_0

    .line 6665
    :sswitch_5
    iget-object v0, p0, Lnjg;->b:Lnix;

    if-nez v0, :cond_7

    .line 6666
    new-instance v0, Lnix;

    invoke-direct {v0}, Lnix;-><init>()V

    iput-object v0, p0, Lnjg;->b:Lnix;

    .line 6668
    :cond_7
    iget-object v0, p0, Lnjg;->b:Lnix;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6672
    :sswitch_6
    iget-object v0, p0, Lnjg;->c:Loae;

    if-nez v0, :cond_8

    .line 6673
    new-instance v0, Loae;

    invoke-direct {v0}, Loae;-><init>()V

    iput-object v0, p0, Lnjg;->c:Loae;

    .line 6675
    :cond_8
    iget-object v0, p0, Lnjg;->c:Loae;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6613
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 6553
    iget v0, p0, Lnjg;->a:I

    if-eq v0, v2, :cond_0

    .line 6554
    const/4 v0, 0x1

    iget v1, p0, Lnjg;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6556
    :cond_0
    iget-object v0, p0, Lnjg;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 6557
    const/4 v0, 0x2

    iget-object v1, p0, Lnjg;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6559
    :cond_1
    iget-object v0, p0, Lnjg;->e:Lnhy;

    if-eqz v0, :cond_2

    .line 6560
    const/4 v0, 0x3

    iget-object v1, p0, Lnjg;->e:Lnhy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6562
    :cond_2
    iget v0, p0, Lnjg;->f:I

    if-eq v0, v2, :cond_3

    .line 6563
    const/4 v0, 0x4

    iget v1, p0, Lnjg;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6565
    :cond_3
    iget-object v0, p0, Lnjg;->b:Lnix;

    if-eqz v0, :cond_4

    .line 6566
    const/4 v0, 0x5

    iget-object v1, p0, Lnjg;->b:Lnix;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6568
    :cond_4
    iget-object v0, p0, Lnjg;->c:Loae;

    if-eqz v0, :cond_5

    .line 6569
    const/4 v0, 0x6

    iget-object v1, p0, Lnjg;->c:Loae;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6571
    :cond_5
    iget-object v0, p0, Lnjg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6573
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6516
    invoke-virtual {p0, p1}, Lnjg;->a(Loxn;)Lnjg;

    move-result-object v0

    return-object v0
.end method
