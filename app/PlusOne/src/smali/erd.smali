.class final Lerd;
.super Landroid/widget/MediaController;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Lera;


# direct methods
.method constructor <init>(Lera;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 571
    iput-object p1, p0, Lerd;->a:Lera;

    .line 572
    invoke-direct {p0, p2}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    .line 573
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 591
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 592
    iget-object v0, p0, Lerd;->a:Lera;

    invoke-virtual {v0}, Lera;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->onBackPressed()V

    .line 593
    const/4 v0, 0x1

    .line 595
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/MediaController;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 600
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10011a

    if-ne v0, v1, :cond_0

    .line 601
    iget-object v1, p0, Lerd;->a:Lera;

    check-cast p1, Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lera;->a(Z)V

    .line 603
    :cond_0
    return-void

    .line 601
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAnchorView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 577
    invoke-super {p0, p1}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    .line 578
    iget-object v0, p0, Lerd;->a:Lera;

    invoke-static {v0}, Lera;->a(Lera;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lerd;->a:Lera;

    invoke-static {v0}, Lera;->b(Lera;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 579
    invoke-virtual {p0}, Lerd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 580
    const v1, 0x7f0400c6

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 581
    const v1, 0x7f10011a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 582
    iget-object v1, p0, Lerd;->a:Lera;

    invoke-static {v1}, Lera;->c(Lera;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 583
    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 585
    :cond_0
    return-void

    .line 582
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
