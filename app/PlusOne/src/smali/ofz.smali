.class public final Lofz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Loez;

.field public b:Loez;

.field public c:Loez;

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7948
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7951
    iput-object v0, p0, Lofz;->a:Loez;

    .line 7954
    iput-object v0, p0, Lofz;->b:Loez;

    .line 7957
    iput-object v0, p0, Lofz;->c:Loez;

    .line 7948
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7982
    const/4 v0, 0x0

    .line 7983
    iget-object v1, p0, Lofz;->a:Loez;

    if-eqz v1, :cond_0

    .line 7984
    const/4 v0, 0x1

    iget-object v1, p0, Lofz;->a:Loez;

    .line 7985
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7987
    :cond_0
    iget-object v1, p0, Lofz;->b:Loez;

    if-eqz v1, :cond_1

    .line 7988
    const/4 v1, 0x2

    iget-object v2, p0, Lofz;->b:Loez;

    .line 7989
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7991
    :cond_1
    iget-object v1, p0, Lofz;->c:Loez;

    if-eqz v1, :cond_2

    .line 7992
    const/4 v1, 0x3

    iget-object v2, p0, Lofz;->c:Loez;

    .line 7993
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7995
    :cond_2
    iget-object v1, p0, Lofz;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 7996
    const/4 v1, 0x4

    iget-object v2, p0, Lofz;->d:Ljava/lang/Boolean;

    .line 7997
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7999
    :cond_3
    iget-object v1, p0, Lofz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8000
    iput v0, p0, Lofz;->ai:I

    .line 8001
    return v0
.end method

.method public a(Loxn;)Lofz;
    .locals 2

    .prologue
    .line 8009
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8010
    sparse-switch v0, :sswitch_data_0

    .line 8014
    iget-object v1, p0, Lofz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8015
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofz;->ah:Ljava/util/List;

    .line 8018
    :cond_1
    iget-object v1, p0, Lofz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8020
    :sswitch_0
    return-object p0

    .line 8025
    :sswitch_1
    iget-object v0, p0, Lofz;->a:Loez;

    if-nez v0, :cond_2

    .line 8026
    new-instance v0, Loez;

    invoke-direct {v0}, Loez;-><init>()V

    iput-object v0, p0, Lofz;->a:Loez;

    .line 8028
    :cond_2
    iget-object v0, p0, Lofz;->a:Loez;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8032
    :sswitch_2
    iget-object v0, p0, Lofz;->b:Loez;

    if-nez v0, :cond_3

    .line 8033
    new-instance v0, Loez;

    invoke-direct {v0}, Loez;-><init>()V

    iput-object v0, p0, Lofz;->b:Loez;

    .line 8035
    :cond_3
    iget-object v0, p0, Lofz;->b:Loez;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8039
    :sswitch_3
    iget-object v0, p0, Lofz;->c:Loez;

    if-nez v0, :cond_4

    .line 8040
    new-instance v0, Loez;

    invoke-direct {v0}, Loez;-><init>()V

    iput-object v0, p0, Lofz;->c:Loez;

    .line 8042
    :cond_4
    iget-object v0, p0, Lofz;->c:Loez;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8046
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofz;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 8010
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7964
    iget-object v0, p0, Lofz;->a:Loez;

    if-eqz v0, :cond_0

    .line 7965
    const/4 v0, 0x1

    iget-object v1, p0, Lofz;->a:Loez;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7967
    :cond_0
    iget-object v0, p0, Lofz;->b:Loez;

    if-eqz v0, :cond_1

    .line 7968
    const/4 v0, 0x2

    iget-object v1, p0, Lofz;->b:Loez;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7970
    :cond_1
    iget-object v0, p0, Lofz;->c:Loez;

    if-eqz v0, :cond_2

    .line 7971
    const/4 v0, 0x3

    iget-object v1, p0, Lofz;->c:Loez;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7973
    :cond_2
    iget-object v0, p0, Lofz;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 7974
    const/4 v0, 0x4

    iget-object v1, p0, Lofz;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7976
    :cond_3
    iget-object v0, p0, Lofz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7978
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7944
    invoke-virtual {p0, p1}, Lofz;->a(Loxn;)Lofz;

    move-result-object v0

    return-object v0
.end method
