.class public final Lnkg;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnhv;

.field private b:Lnhv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7819
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7822
    iput-object v0, p0, Lnkg;->a:Lnhv;

    .line 7825
    iput-object v0, p0, Lnkg;->b:Lnhv;

    .line 7819
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7842
    const/4 v0, 0x0

    .line 7843
    iget-object v1, p0, Lnkg;->a:Lnhv;

    if-eqz v1, :cond_0

    .line 7844
    const/4 v0, 0x1

    iget-object v1, p0, Lnkg;->a:Lnhv;

    .line 7845
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7847
    :cond_0
    iget-object v1, p0, Lnkg;->b:Lnhv;

    if-eqz v1, :cond_1

    .line 7848
    const/4 v1, 0x2

    iget-object v2, p0, Lnkg;->b:Lnhv;

    .line 7849
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7851
    :cond_1
    iget-object v1, p0, Lnkg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7852
    iput v0, p0, Lnkg;->ai:I

    .line 7853
    return v0
.end method

.method public a(Loxn;)Lnkg;
    .locals 2

    .prologue
    .line 7861
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7862
    sparse-switch v0, :sswitch_data_0

    .line 7866
    iget-object v1, p0, Lnkg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7867
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnkg;->ah:Ljava/util/List;

    .line 7870
    :cond_1
    iget-object v1, p0, Lnkg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7872
    :sswitch_0
    return-object p0

    .line 7877
    :sswitch_1
    iget-object v0, p0, Lnkg;->a:Lnhv;

    if-nez v0, :cond_2

    .line 7878
    new-instance v0, Lnhv;

    invoke-direct {v0}, Lnhv;-><init>()V

    iput-object v0, p0, Lnkg;->a:Lnhv;

    .line 7880
    :cond_2
    iget-object v0, p0, Lnkg;->a:Lnhv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7884
    :sswitch_2
    iget-object v0, p0, Lnkg;->b:Lnhv;

    if-nez v0, :cond_3

    .line 7885
    new-instance v0, Lnhv;

    invoke-direct {v0}, Lnhv;-><init>()V

    iput-object v0, p0, Lnkg;->b:Lnhv;

    .line 7887
    :cond_3
    iget-object v0, p0, Lnkg;->b:Lnhv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7862
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7830
    iget-object v0, p0, Lnkg;->a:Lnhv;

    if-eqz v0, :cond_0

    .line 7831
    const/4 v0, 0x1

    iget-object v1, p0, Lnkg;->a:Lnhv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7833
    :cond_0
    iget-object v0, p0, Lnkg;->b:Lnhv;

    if-eqz v0, :cond_1

    .line 7834
    const/4 v0, 0x2

    iget-object v1, p0, Lnkg;->b:Lnhv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7836
    :cond_1
    iget-object v0, p0, Lnkg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7838
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7815
    invoke-virtual {p0, p1}, Lnkg;->a(Loxn;)Lnkg;

    move-result-object v0

    return-object v0
.end method
