.class public final Lnva;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnvb;

.field public b:Logy;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2707
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2710
    sget-object v0, Lnvb;->a:[Lnvb;

    iput-object v0, p0, Lnva;->a:[Lnvb;

    .line 2715
    const/4 v0, 0x0

    iput-object v0, p0, Lnva;->b:Logy;

    .line 2707
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2739
    .line 2740
    iget-object v1, p0, Lnva;->a:[Lnvb;

    if-eqz v1, :cond_1

    .line 2741
    iget-object v2, p0, Lnva;->a:[Lnvb;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2742
    if-eqz v4, :cond_0

    .line 2743
    const/4 v5, 0x1

    .line 2744
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2741
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2748
    :cond_1
    iget-object v1, p0, Lnva;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2749
    const/4 v1, 0x2

    iget-object v2, p0, Lnva;->c:Ljava/lang/String;

    .line 2750
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2752
    :cond_2
    iget-object v1, p0, Lnva;->b:Logy;

    if-eqz v1, :cond_3

    .line 2753
    const/4 v1, 0x3

    iget-object v2, p0, Lnva;->b:Logy;

    .line 2754
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2756
    :cond_3
    iget-object v1, p0, Lnva;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2757
    iput v0, p0, Lnva;->ai:I

    .line 2758
    return v0
.end method

.method public a(Loxn;)Lnva;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2766
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2767
    sparse-switch v0, :sswitch_data_0

    .line 2771
    iget-object v2, p0, Lnva;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2772
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnva;->ah:Ljava/util/List;

    .line 2775
    :cond_1
    iget-object v2, p0, Lnva;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2777
    :sswitch_0
    return-object p0

    .line 2782
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2783
    iget-object v0, p0, Lnva;->a:[Lnvb;

    if-nez v0, :cond_3

    move v0, v1

    .line 2784
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnvb;

    .line 2785
    iget-object v3, p0, Lnva;->a:[Lnvb;

    if-eqz v3, :cond_2

    .line 2786
    iget-object v3, p0, Lnva;->a:[Lnvb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2788
    :cond_2
    iput-object v2, p0, Lnva;->a:[Lnvb;

    .line 2789
    :goto_2
    iget-object v2, p0, Lnva;->a:[Lnvb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2790
    iget-object v2, p0, Lnva;->a:[Lnvb;

    new-instance v3, Lnvb;

    invoke-direct {v3}, Lnvb;-><init>()V

    aput-object v3, v2, v0

    .line 2791
    iget-object v2, p0, Lnva;->a:[Lnvb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2792
    invoke-virtual {p1}, Loxn;->a()I

    .line 2789
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2783
    :cond_3
    iget-object v0, p0, Lnva;->a:[Lnvb;

    array-length v0, v0

    goto :goto_1

    .line 2795
    :cond_4
    iget-object v2, p0, Lnva;->a:[Lnvb;

    new-instance v3, Lnvb;

    invoke-direct {v3}, Lnvb;-><init>()V

    aput-object v3, v2, v0

    .line 2796
    iget-object v2, p0, Lnva;->a:[Lnvb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2800
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnva;->c:Ljava/lang/String;

    goto :goto_0

    .line 2804
    :sswitch_3
    iget-object v0, p0, Lnva;->b:Logy;

    if-nez v0, :cond_5

    .line 2805
    new-instance v0, Logy;

    invoke-direct {v0}, Logy;-><init>()V

    iput-object v0, p0, Lnva;->b:Logy;

    .line 2807
    :cond_5
    iget-object v0, p0, Lnva;->b:Logy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2767
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2720
    iget-object v0, p0, Lnva;->a:[Lnvb;

    if-eqz v0, :cond_1

    .line 2721
    iget-object v1, p0, Lnva;->a:[Lnvb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2722
    if-eqz v3, :cond_0

    .line 2723
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2721
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2727
    :cond_1
    iget-object v0, p0, Lnva;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2728
    const/4 v0, 0x2

    iget-object v1, p0, Lnva;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2730
    :cond_2
    iget-object v0, p0, Lnva;->b:Logy;

    if-eqz v0, :cond_3

    .line 2731
    const/4 v0, 0x3

    iget-object v1, p0, Lnva;->b:Logy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2733
    :cond_3
    iget-object v0, p0, Lnva;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2735
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2703
    invoke-virtual {p0, p1}, Lnva;->a(Loxn;)Lnva;

    move-result-object v0

    return-object v0
.end method
