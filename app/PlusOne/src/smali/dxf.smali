.class public final Ldxf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Ldsx;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Ldxf;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Ldsx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Ldxf;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->d(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Lhee;

    move-result-object v0

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-static {v0}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    new-instance v1, Leox;

    iget-object v2, p0, Ldxf;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Ldxf;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    .line 118
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->d(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Lhee;

    move-result-object v3

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v0, v4}, Leox;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    return-object v1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ldsx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 108
    check-cast p2, Ldsx;

    invoke-virtual {p0, p2}, Ldxf;->a(Ldsx;)V

    return-void
.end method

.method public a(Ldsx;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldsx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 126
    if-eqz p1, :cond_0

    .line 131
    iget-object v0, p1, Ldsx;->h:Lnjt;

    if-eqz v0, :cond_2

    iget-object v0, p1, Ldsx;->h:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    if-eqz v0, :cond_2

    iget-object v0, p1, Ldsx;->h:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->f:Ljava/lang/String;

    .line 132
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 133
    iget-object v0, p0, Ldxf;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->e(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    move-result-object v1

    iget-object v2, p1, Ldsx;->b:Ljava/lang/String;

    const-string v3, "http:"

    iget-object v0, p1, Ldsx;->h:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    :goto_1
    return-void

    .line 133
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 136
    :cond_2
    iget-object v0, p0, Ldxf;->a:Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;->e(Lcom/google/android/apps/plus/fragments/AddProfilePhotoSpringboardFragment;)Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    move-result-object v0

    iget-object v1, p1, Ldsx;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
