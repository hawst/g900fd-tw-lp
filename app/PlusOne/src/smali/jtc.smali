.class public final Ljtc;
.super Llol;
.source "PG"

# interfaces
.implements Lhio;
.implements Lhyz;
.implements Ljtk;
.implements Lllh;


# instance fields
.field N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljtf;",
            ">;"
        }
    .end annotation
.end field

.field private O:Lhzb;

.field private P:I

.field private Q:Ljava/lang/String;

.field private R:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljava/lang/String;

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Lhzl;

.field private X:Ljsz;

.field private Y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Llol;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljtc;->T:Z

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljtc;->U:Z

    .line 59
    return-void
.end method

.method private V()V
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    iput-object v0, p0, Ljtc;->S:Ljava/lang/String;

    .line 314
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljtc;->U:Z

    .line 315
    return-void
.end method

.method static synthetic a(Ljtc;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Ljtc;->R:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic a(Ljtc;)Llnl;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ljtc;->at:Llnl;

    return-object v0
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286
    iput-object p1, p0, Ljtc;->Y:Ljava/util/List;

    .line 287
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljtc;->T:Z

    .line 289
    iget-object v0, p0, Ljtc;->O:Lhzb;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Ljtc;->O:Lhzb;

    invoke-interface {v0}, Lhzb;->e()V

    .line 292
    :cond_0
    return-void
.end method

.method static synthetic a(Ljtc;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    if-eqz p1, :cond_0

    iget-object v0, p0, Ljtc;->S:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljtc;->S:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ljtc;->S:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Ljtc;->U:Z

    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Ljtc;->S:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic b(Ljtc;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Ljtc;->P:I

    return v0
.end method

.method static synthetic c(Ljtc;)V
    .locals 6

    .prologue
    .line 32
    iget-object v0, p0, Ljtc;->N:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljtc;->R:Ljava/util/Map;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljtc;->Y:Ljava/util/List;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v0

    :goto_0
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    iget-object v0, p0, Ljtc;->N:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    iget-object v0, p0, Ljtc;->N:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljtf;

    iget-object v1, p0, Ljtc;->R:Ljava/util/Map;

    invoke-virtual {v0}, Ljtf;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljpv;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljpv;->f()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljtf;->a(Ljava/util/ArrayList;)V

    :cond_0
    iget-object v1, p0, Ljtc;->W:Lhzl;

    instance-of v1, v1, Lhzk;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljtc;->W:Lhzl;

    check-cast v1, Lhzk;

    const/16 v4, 0xaa

    invoke-virtual {v0}, Ljtf;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Lhzk;->a(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ljtc;->Y:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v2, v0

    goto :goto_0

    :cond_4
    invoke-direct {p0, v2}, Ljtc;->a(Ljava/util/List;)V

    :cond_5
    return-void
.end method

.method static synthetic d(Ljtc;)Llnl;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ljtc;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Ljtc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ljtc;->Q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Ljtc;)Ljsz;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ljtc;->X:Ljsz;

    return-object v0
.end method

.method static synthetic g(Ljtc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ljtc;->S:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Ljtc;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Ljtc;->V:Z

    return v0
.end method


# virtual methods
.method public U()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 298
    iget-object v0, p0, Ljtc;->Y:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ljtc;->T:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ljtc;->U:Z

    if-eqz v0, :cond_0

    .line 299
    iput-boolean v1, p0, Ljtc;->T:Z

    .line 300
    invoke-virtual {p0}, Ljtc;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbb;->b(I)Ldo;

    move-result-object v0

    invoke-virtual {v0}, Ldo;->t()V

    .line 302
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x1

    return v0
.end method

.method public a(I)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Ljtc;->Y:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public a(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 173
    check-cast p2, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;

    iget-object v0, p0, Ljtc;->Y:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->a(Ljpv;)V

    .line 176
    invoke-virtual {p0}, Ljtc;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    if-lt p1, v0, :cond_0

    .line 177
    invoke-virtual {p0}, Ljtc;->U()V

    .line 179
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 64
    if-eqz p1, :cond_0

    .line 65
    const-string v0, "state_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljtc;->Q:Ljava/lang/String;

    .line 67
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method public a(Lhzb;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Ljtc;->O:Lhzb;

    .line 199
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 228
    if-eqz p1, :cond_0

    .line 229
    iget-object v0, p0, Ljtc;->Q:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ljtc;->Q:Ljava/lang/String;

    .line 231
    iget-object v1, p0, Ljtc;->Q:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Ljtc;->Q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Ljtc;->X:Ljsz;

    iget v1, v1, Ljsz;->a:I

    if-lt v0, v1, :cond_1

    .line 233
    iput-object v2, p0, Ljtc;->Y:Ljava/util/List;

    .line 234
    invoke-direct {p0}, Ljtc;->V()V

    .line 235
    invoke-virtual {p0}, Ljtc;->e()V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    invoke-direct {p0, v2}, Ljtc;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 306
    iput-boolean p1, p0, Ljtc;->V:Z

    .line 307
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ljtc;->a(Ljava/util/List;)V

    .line 308
    invoke-direct {p0}, Ljtc;->V()V

    .line 309
    invoke-virtual {p0}, Ljtc;->e()V

    .line 310
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    return v0
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    invoke-virtual {p0, v2}, Ljtc;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040172

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 104
    iget-object v0, p0, Ljtc;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Ljtc;->P:I

    .line 105
    iget-object v0, p0, Ljtc;->au:Llnh;

    const-class v1, Ljsz;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsz;

    iput-object v0, p0, Ljtc;->X:Ljsz;

    .line 106
    iget-object v0, p0, Ljtc;->X:Ljsz;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Ljsz;

    invoke-direct {v0}, Ljsz;-><init>()V

    iput-object v0, p0, Ljtc;->X:Ljsz;

    .line 110
    :cond_0
    iget-object v0, p0, Ljtc;->au:Llnh;

    const-class v1, Lllg;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllg;

    .line 111
    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {v0, p0}, Lllg;->a(Lllh;)V

    .line 115
    :cond_1
    if-nez p1, :cond_2

    .line 116
    iget-object v0, p0, Ljtc;->au:Llnh;

    const-class v1, Ljtj;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljtj;

    .line 117
    if-eqz v0, :cond_2

    .line 118
    invoke-virtual {v0, p0}, Ljtj;->a(Ljtk;)V

    .line 122
    :cond_2
    iget-object v0, p0, Ljtc;->au:Llnh;

    const-class v1, Lhin;

    .line 123
    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhin;

    .line 124
    if-eqz v0, :cond_3

    .line 125
    invoke-virtual {v0, p0}, Lhin;->a(Lhio;)V

    .line 126
    invoke-virtual {v0}, Lhin;->a()Z

    move-result v0

    iput-boolean v0, p0, Ljtc;->V:Z

    .line 131
    :goto_0
    iget-object v0, p0, Ljtc;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Ljtc;->W:Lhzl;

    .line 132
    return-void

    .line 128
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljtc;->V:Z

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Ljtc;->Y:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljtc;->Y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 203
    invoke-virtual {p0}, Ljtc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v3, Ljte;

    invoke-direct {v3, p0}, Ljte;-><init>(Ljtc;)V

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 224
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 72
    const-string v0, "state_query"

    iget-object v1, p0, Ljtc;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 77
    invoke-super {p0}, Llol;->g()V

    .line 78
    invoke-virtual {p0}, Ljtc;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    new-instance v3, Ljtd;

    invoke-direct {v3, p0}, Ljtd;-><init>(Ljtc;)V

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 96
    iget-object v0, p0, Ljtc;->Q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Ljtc;->e()V

    .line 99
    :cond_0
    return-void
.end method
