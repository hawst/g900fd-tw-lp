.class public final Lnzr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lnzr;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lnxr;

.field public c:[Lnyz;

.field public d:Lnxh;

.field public e:Lnxo;

.field public f:Lnxb;

.field private g:Lnyz;

.field private h:Lorf;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x28699cc

    new-instance v1, Lnzs;

    invoke-direct {v1}, Lnzs;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lnzr;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    iput-object v1, p0, Lnzr;->g:Lnyz;

    .line 20
    iput-object v1, p0, Lnzr;->b:Lnxr;

    .line 23
    sget-object v0, Lnyz;->a:[Lnyz;

    iput-object v0, p0, Lnzr;->c:[Lnyz;

    .line 26
    iput-object v1, p0, Lnzr;->d:Lnxh;

    .line 29
    iput-object v1, p0, Lnzr;->e:Lnxo;

    .line 32
    iput-object v1, p0, Lnzr;->f:Lnxb;

    .line 35
    iput-object v1, p0, Lnzr;->h:Lorf;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 71
    .line 72
    iget-object v0, p0, Lnzr;->b:Lnxr;

    if-eqz v0, :cond_7

    .line 73
    const/4 v0, 0x1

    iget-object v2, p0, Lnzr;->b:Lnxr;

    .line 74
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 76
    :goto_0
    iget-object v2, p0, Lnzr;->c:[Lnyz;

    if-eqz v2, :cond_1

    .line 77
    iget-object v2, p0, Lnzr;->c:[Lnyz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 78
    if-eqz v4, :cond_0

    .line 79
    const/4 v5, 0x2

    .line 80
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 77
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 84
    :cond_1
    iget-object v1, p0, Lnzr;->d:Lnxh;

    if-eqz v1, :cond_2

    .line 85
    const/4 v1, 0x3

    iget-object v2, p0, Lnzr;->d:Lnxh;

    .line 86
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_2
    iget-object v1, p0, Lnzr;->e:Lnxo;

    if-eqz v1, :cond_3

    .line 89
    const/4 v1, 0x4

    iget-object v2, p0, Lnzr;->e:Lnxo;

    .line 90
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_3
    iget-object v1, p0, Lnzr;->f:Lnxb;

    if-eqz v1, :cond_4

    .line 93
    const/4 v1, 0x5

    iget-object v2, p0, Lnzr;->f:Lnxb;

    .line 94
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_4
    iget-object v1, p0, Lnzr;->h:Lorf;

    if-eqz v1, :cond_5

    .line 97
    const/4 v1, 0x6

    iget-object v2, p0, Lnzr;->h:Lorf;

    .line 98
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_5
    iget-object v1, p0, Lnzr;->g:Lnyz;

    if-eqz v1, :cond_6

    .line 101
    const/4 v1, 0x7

    iget-object v2, p0, Lnzr;->g:Lnyz;

    .line 102
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_6
    iget-object v1, p0, Lnzr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    iput v0, p0, Lnzr;->ai:I

    .line 106
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnzr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 114
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 115
    sparse-switch v0, :sswitch_data_0

    .line 119
    iget-object v2, p0, Lnzr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 120
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnzr;->ah:Ljava/util/List;

    .line 123
    :cond_1
    iget-object v2, p0, Lnzr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    :sswitch_0
    return-object p0

    .line 130
    :sswitch_1
    iget-object v0, p0, Lnzr;->b:Lnxr;

    if-nez v0, :cond_2

    .line 131
    new-instance v0, Lnxr;

    invoke-direct {v0}, Lnxr;-><init>()V

    iput-object v0, p0, Lnzr;->b:Lnxr;

    .line 133
    :cond_2
    iget-object v0, p0, Lnzr;->b:Lnxr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 137
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 138
    iget-object v0, p0, Lnzr;->c:[Lnyz;

    if-nez v0, :cond_4

    move v0, v1

    .line 139
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyz;

    .line 140
    iget-object v3, p0, Lnzr;->c:[Lnyz;

    if-eqz v3, :cond_3

    .line 141
    iget-object v3, p0, Lnzr;->c:[Lnyz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    :cond_3
    iput-object v2, p0, Lnzr;->c:[Lnyz;

    .line 144
    :goto_2
    iget-object v2, p0, Lnzr;->c:[Lnyz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 145
    iget-object v2, p0, Lnzr;->c:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 146
    iget-object v2, p0, Lnzr;->c:[Lnyz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 147
    invoke-virtual {p1}, Loxn;->a()I

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 138
    :cond_4
    iget-object v0, p0, Lnzr;->c:[Lnyz;

    array-length v0, v0

    goto :goto_1

    .line 150
    :cond_5
    iget-object v2, p0, Lnzr;->c:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 151
    iget-object v2, p0, Lnzr;->c:[Lnyz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 155
    :sswitch_3
    iget-object v0, p0, Lnzr;->d:Lnxh;

    if-nez v0, :cond_6

    .line 156
    new-instance v0, Lnxh;

    invoke-direct {v0}, Lnxh;-><init>()V

    iput-object v0, p0, Lnzr;->d:Lnxh;

    .line 158
    :cond_6
    iget-object v0, p0, Lnzr;->d:Lnxh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 162
    :sswitch_4
    iget-object v0, p0, Lnzr;->e:Lnxo;

    if-nez v0, :cond_7

    .line 163
    new-instance v0, Lnxo;

    invoke-direct {v0}, Lnxo;-><init>()V

    iput-object v0, p0, Lnzr;->e:Lnxo;

    .line 165
    :cond_7
    iget-object v0, p0, Lnzr;->e:Lnxo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 169
    :sswitch_5
    iget-object v0, p0, Lnzr;->f:Lnxb;

    if-nez v0, :cond_8

    .line 170
    new-instance v0, Lnxb;

    invoke-direct {v0}, Lnxb;-><init>()V

    iput-object v0, p0, Lnzr;->f:Lnxb;

    .line 172
    :cond_8
    iget-object v0, p0, Lnzr;->f:Lnxb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 176
    :sswitch_6
    iget-object v0, p0, Lnzr;->h:Lorf;

    if-nez v0, :cond_9

    .line 177
    new-instance v0, Lorf;

    invoke-direct {v0}, Lorf;-><init>()V

    iput-object v0, p0, Lnzr;->h:Lorf;

    .line 179
    :cond_9
    iget-object v0, p0, Lnzr;->h:Lorf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 183
    :sswitch_7
    iget-object v0, p0, Lnzr;->g:Lnyz;

    if-nez v0, :cond_a

    .line 184
    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    iput-object v0, p0, Lnzr;->g:Lnyz;

    .line 186
    :cond_a
    iget-object v0, p0, Lnzr;->g:Lnyz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 115
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 40
    iget-object v0, p0, Lnzr;->b:Lnxr;

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x1

    iget-object v1, p0, Lnzr;->b:Lnxr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 43
    :cond_0
    iget-object v0, p0, Lnzr;->c:[Lnyz;

    if-eqz v0, :cond_2

    .line 44
    iget-object v1, p0, Lnzr;->c:[Lnyz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 45
    if-eqz v3, :cond_1

    .line 46
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 44
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_2
    iget-object v0, p0, Lnzr;->d:Lnxh;

    if-eqz v0, :cond_3

    .line 51
    const/4 v0, 0x3

    iget-object v1, p0, Lnzr;->d:Lnxh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 53
    :cond_3
    iget-object v0, p0, Lnzr;->e:Lnxo;

    if-eqz v0, :cond_4

    .line 54
    const/4 v0, 0x4

    iget-object v1, p0, Lnzr;->e:Lnxo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 56
    :cond_4
    iget-object v0, p0, Lnzr;->f:Lnxb;

    if-eqz v0, :cond_5

    .line 57
    const/4 v0, 0x5

    iget-object v1, p0, Lnzr;->f:Lnxb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 59
    :cond_5
    iget-object v0, p0, Lnzr;->h:Lorf;

    if-eqz v0, :cond_6

    .line 60
    const/4 v0, 0x6

    iget-object v1, p0, Lnzr;->h:Lorf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 62
    :cond_6
    iget-object v0, p0, Lnzr;->g:Lnyz;

    if-eqz v0, :cond_7

    .line 63
    const/4 v0, 0x7

    iget-object v1, p0, Lnzr;->g:Lnyz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 65
    :cond_7
    iget-object v0, p0, Lnzr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 67
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnzr;->a(Loxn;)Lnzr;

    move-result-object v0

    return-object v0
.end method
