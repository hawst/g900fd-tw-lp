.class public final Lnmu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Logi;

.field public b:Ljava/lang/String;

.field private c:Lnms;

.field private d:Ljava/lang/Integer;

.field private e:Logy;

.field private f:Ljava/lang/String;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1336
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1339
    iput-object v0, p0, Lnmu;->a:Logi;

    .line 1342
    iput-object v0, p0, Lnmu;->c:Lnms;

    .line 1349
    iput-object v0, p0, Lnmu;->e:Logy;

    .line 1354
    const/high16 v0, -0x80000000

    iput v0, p0, Lnmu;->g:I

    .line 1336
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1386
    const/4 v0, 0x0

    .line 1387
    iget-object v1, p0, Lnmu;->a:Logi;

    if-eqz v1, :cond_0

    .line 1388
    const/4 v0, 0x1

    iget-object v1, p0, Lnmu;->a:Logi;

    .line 1389
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1391
    :cond_0
    iget-object v1, p0, Lnmu;->c:Lnms;

    if-eqz v1, :cond_1

    .line 1392
    const/4 v1, 0x2

    iget-object v2, p0, Lnmu;->c:Lnms;

    .line 1393
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1395
    :cond_1
    iget-object v1, p0, Lnmu;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1396
    const/4 v1, 0x3

    iget-object v2, p0, Lnmu;->b:Ljava/lang/String;

    .line 1397
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1399
    :cond_2
    iget-object v1, p0, Lnmu;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1400
    const/4 v1, 0x4

    iget-object v2, p0, Lnmu;->d:Ljava/lang/Integer;

    .line 1401
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1403
    :cond_3
    iget-object v1, p0, Lnmu;->e:Logy;

    if-eqz v1, :cond_4

    .line 1404
    const/4 v1, 0x5

    iget-object v2, p0, Lnmu;->e:Logy;

    .line 1405
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1407
    :cond_4
    iget-object v1, p0, Lnmu;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1408
    const/4 v1, 0x6

    iget-object v2, p0, Lnmu;->f:Ljava/lang/String;

    .line 1409
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1411
    :cond_5
    iget v1, p0, Lnmu;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_6

    .line 1412
    const/4 v1, 0x7

    iget v2, p0, Lnmu;->g:I

    .line 1413
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1415
    :cond_6
    iget-object v1, p0, Lnmu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1416
    iput v0, p0, Lnmu;->ai:I

    .line 1417
    return v0
.end method

.method public a(Loxn;)Lnmu;
    .locals 2

    .prologue
    .line 1425
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1426
    sparse-switch v0, :sswitch_data_0

    .line 1430
    iget-object v1, p0, Lnmu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1431
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnmu;->ah:Ljava/util/List;

    .line 1434
    :cond_1
    iget-object v1, p0, Lnmu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1436
    :sswitch_0
    return-object p0

    .line 1441
    :sswitch_1
    iget-object v0, p0, Lnmu;->a:Logi;

    if-nez v0, :cond_2

    .line 1442
    new-instance v0, Logi;

    invoke-direct {v0}, Logi;-><init>()V

    iput-object v0, p0, Lnmu;->a:Logi;

    .line 1444
    :cond_2
    iget-object v0, p0, Lnmu;->a:Logi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1448
    :sswitch_2
    iget-object v0, p0, Lnmu;->c:Lnms;

    if-nez v0, :cond_3

    .line 1449
    new-instance v0, Lnms;

    invoke-direct {v0}, Lnms;-><init>()V

    iput-object v0, p0, Lnmu;->c:Lnms;

    .line 1451
    :cond_3
    iget-object v0, p0, Lnmu;->c:Lnms;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1455
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmu;->b:Ljava/lang/String;

    goto :goto_0

    .line 1459
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnmu;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 1463
    :sswitch_5
    iget-object v0, p0, Lnmu;->e:Logy;

    if-nez v0, :cond_4

    .line 1464
    new-instance v0, Logy;

    invoke-direct {v0}, Logy;-><init>()V

    iput-object v0, p0, Lnmu;->e:Logy;

    .line 1466
    :cond_4
    iget-object v0, p0, Lnmu;->e:Logy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1470
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmu;->f:Ljava/lang/String;

    goto :goto_0

    .line 1474
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1475
    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 1477
    :cond_5
    iput v0, p0, Lnmu;->g:I

    goto :goto_0

    .line 1479
    :cond_6
    const/4 v0, 0x0

    iput v0, p0, Lnmu;->g:I

    goto :goto_0

    .line 1426
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1359
    iget-object v0, p0, Lnmu;->a:Logi;

    if-eqz v0, :cond_0

    .line 1360
    const/4 v0, 0x1

    iget-object v1, p0, Lnmu;->a:Logi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1362
    :cond_0
    iget-object v0, p0, Lnmu;->c:Lnms;

    if-eqz v0, :cond_1

    .line 1363
    const/4 v0, 0x2

    iget-object v1, p0, Lnmu;->c:Lnms;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1365
    :cond_1
    iget-object v0, p0, Lnmu;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1366
    const/4 v0, 0x3

    iget-object v1, p0, Lnmu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1368
    :cond_2
    iget-object v0, p0, Lnmu;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1369
    const/4 v0, 0x4

    iget-object v1, p0, Lnmu;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1371
    :cond_3
    iget-object v0, p0, Lnmu;->e:Logy;

    if-eqz v0, :cond_4

    .line 1372
    const/4 v0, 0x5

    iget-object v1, p0, Lnmu;->e:Logy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1374
    :cond_4
    iget-object v0, p0, Lnmu;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1375
    const/4 v0, 0x6

    iget-object v1, p0, Lnmu;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1377
    :cond_5
    iget v0, p0, Lnmu;->g:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_6

    .line 1378
    const/4 v0, 0x7

    iget v1, p0, Lnmu;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1380
    :cond_6
    iget-object v0, p0, Lnmu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1382
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1332
    invoke-virtual {p0, p1}, Lnmu;->a(Loxn;)Lnmu;

    move-result-object v0

    return-object v0
.end method
