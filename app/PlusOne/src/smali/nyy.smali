.class public final Lnyy;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6115
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6156
    const/4 v0, 0x0

    .line 6157
    iget-object v1, p0, Lnyy;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 6158
    const/4 v0, 0x1

    iget-object v1, p0, Lnyy;->a:Ljava/lang/Integer;

    .line 6159
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6161
    :cond_0
    iget-object v1, p0, Lnyy;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 6162
    const/4 v1, 0x2

    iget-object v2, p0, Lnyy;->b:Ljava/lang/Integer;

    .line 6163
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6165
    :cond_1
    iget-object v1, p0, Lnyy;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 6166
    const/4 v1, 0x3

    iget-object v2, p0, Lnyy;->c:Ljava/lang/Integer;

    .line 6167
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6169
    :cond_2
    iget-object v1, p0, Lnyy;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 6170
    const/4 v1, 0x4

    iget-object v2, p0, Lnyy;->e:Ljava/lang/Integer;

    .line 6171
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6173
    :cond_3
    iget-object v1, p0, Lnyy;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 6174
    const/4 v1, 0x5

    iget-object v2, p0, Lnyy;->f:Ljava/lang/Integer;

    .line 6175
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6177
    :cond_4
    iget-object v1, p0, Lnyy;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 6178
    const/4 v1, 0x6

    iget-object v2, p0, Lnyy;->d:Ljava/lang/Integer;

    .line 6179
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6181
    :cond_5
    iget-object v1, p0, Lnyy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6182
    iput v0, p0, Lnyy;->ai:I

    .line 6183
    return v0
.end method

.method public a(Loxn;)Lnyy;
    .locals 2

    .prologue
    .line 6191
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6192
    sparse-switch v0, :sswitch_data_0

    .line 6196
    iget-object v1, p0, Lnyy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6197
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyy;->ah:Ljava/util/List;

    .line 6200
    :cond_1
    iget-object v1, p0, Lnyy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6202
    :sswitch_0
    return-object p0

    .line 6207
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyy;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 6211
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyy;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 6215
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyy;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 6219
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyy;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 6223
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyy;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 6227
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyy;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 6192
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6132
    iget-object v0, p0, Lnyy;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 6133
    const/4 v0, 0x1

    iget-object v1, p0, Lnyy;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6135
    :cond_0
    iget-object v0, p0, Lnyy;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 6136
    const/4 v0, 0x2

    iget-object v1, p0, Lnyy;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6138
    :cond_1
    iget-object v0, p0, Lnyy;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 6139
    const/4 v0, 0x3

    iget-object v1, p0, Lnyy;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6141
    :cond_2
    iget-object v0, p0, Lnyy;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 6142
    const/4 v0, 0x4

    iget-object v1, p0, Lnyy;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6144
    :cond_3
    iget-object v0, p0, Lnyy;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 6145
    const/4 v0, 0x5

    iget-object v1, p0, Lnyy;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6147
    :cond_4
    iget-object v0, p0, Lnyy;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 6148
    const/4 v0, 0x6

    iget-object v1, p0, Lnyy;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6150
    :cond_5
    iget-object v0, p0, Lnyy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6152
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6111
    invoke-virtual {p0, p1}, Lnyy;->a(Loxn;)Lnyy;

    move-result-object v0

    return-object v0
.end method
