.class public final Ljkz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljkz;


# instance fields
.field private b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljky;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:Lhei;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Ljkz;->b:Landroid/util/SparseArray;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Ljkz;->b:Landroid/util/SparseArray;

    .line 38
    iput-object p1, p0, Ljkz;->c:Landroid/content/Context;

    .line 39
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ljkz;->d:Lhei;

    .line 40
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Ljkz;
    .locals 2

    .prologue
    .line 30
    const-class v1, Ljkz;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljkz;->a:Ljkz;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Ljkz;

    invoke-direct {v0, p0}, Ljkz;-><init>(Landroid/content/Context;)V

    sput-object v0, Ljkz;->a:Ljkz;

    .line 33
    :cond_0
    sget-object v0, Ljkz;->a:Ljkz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(I)Ljky;
    .locals 2

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ljkz;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljky;

    .line 73
    if-nez v0, :cond_0

    .line 74
    new-instance v0, Ljky;

    iget-object v1, p0, Ljkz;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Ljky;-><init>(Landroid/content/Context;I)V

    .line 75
    iget-object v1, p0, Ljkz;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :cond_0
    monitor-exit p0

    return-object v0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)Ljky;
    .locals 2

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ljkz;->d:Lhei;

    invoke-interface {v0, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 59
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 62
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0, v0}, Ljkz;->a(I)Ljky;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
