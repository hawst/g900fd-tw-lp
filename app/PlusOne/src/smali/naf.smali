.class public final Lnaf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Locl;

.field private b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 844
    invoke-direct {p0}, Loxq;-><init>()V

    .line 847
    const/4 v0, 0x0

    iput-object v0, p0, Lnaf;->a:Locl;

    .line 844
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 866
    const/4 v0, 0x0

    .line 867
    iget-object v1, p0, Lnaf;->a:Locl;

    if-eqz v1, :cond_0

    .line 868
    const/4 v0, 0x1

    iget-object v1, p0, Lnaf;->a:Locl;

    .line 869
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 871
    :cond_0
    iget-object v1, p0, Lnaf;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 872
    const/4 v1, 0x2

    iget-object v2, p0, Lnaf;->b:Ljava/lang/Boolean;

    .line 873
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 875
    :cond_1
    iget-object v1, p0, Lnaf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 876
    iput v0, p0, Lnaf;->ai:I

    .line 877
    return v0
.end method

.method public a(Loxn;)Lnaf;
    .locals 2

    .prologue
    .line 885
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 886
    sparse-switch v0, :sswitch_data_0

    .line 890
    iget-object v1, p0, Lnaf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 891
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnaf;->ah:Ljava/util/List;

    .line 894
    :cond_1
    iget-object v1, p0, Lnaf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 896
    :sswitch_0
    return-object p0

    .line 901
    :sswitch_1
    iget-object v0, p0, Lnaf;->a:Locl;

    if-nez v0, :cond_2

    .line 902
    new-instance v0, Locl;

    invoke-direct {v0}, Locl;-><init>()V

    iput-object v0, p0, Lnaf;->a:Locl;

    .line 904
    :cond_2
    iget-object v0, p0, Lnaf;->a:Locl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 908
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnaf;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 886
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 854
    iget-object v0, p0, Lnaf;->a:Locl;

    if-eqz v0, :cond_0

    .line 855
    const/4 v0, 0x1

    iget-object v1, p0, Lnaf;->a:Locl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 857
    :cond_0
    iget-object v0, p0, Lnaf;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 858
    const/4 v0, 0x2

    iget-object v1, p0, Lnaf;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 860
    :cond_1
    iget-object v0, p0, Lnaf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 862
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 840
    invoke-virtual {p0, p1}, Lnaf;->a(Loxn;)Lnaf;

    move-result-object v0

    return-object v0
.end method
