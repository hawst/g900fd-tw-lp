.class public final Lfku;
.super Lkgn;
.source "PG"

# interfaces
.implements Lkig;


# instance fields
.field private final Q:Lkif;

.field private R:Lhee;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Lkgn;-><init>()V

    .line 28
    new-instance v0, Lkif;

    iget-object v1, p0, Lfku;->P:Llqm;

    invoke-direct {v0, p0, v1}, Lkif;-><init>(Lkgn;Llqr;)V

    iput-object v0, p0, Lfku;->Q:Lkif;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 41
    iget-object v0, p0, Lfku;->N:Llnl;

    const-class v1, Lhpu;

    .line 42
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 43
    invoke-virtual {v0}, Lhpu;->a()Ljava/util/List;

    move-result-object v0

    .line 46
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lfku;->Q:Lkif;

    new-instance v1, Lfky;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lfky;-><init>(I)V

    invoke-virtual {v0, v1}, Lkif;->a(Lu;)V

    .line 51
    :cond_0
    new-instance v0, Lizf;

    invoke-direct {v0}, Lizf;-><init>()V

    .line 52
    iget-object v1, p0, Lfku;->N:Llnl;

    iget-object v2, p0, Lfku;->R:Lhee;

    .line 53
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 52
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/settings/PhotosSettingsActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Lizf;->b(Landroid/content/Intent;)V

    .line 54
    iget-object v1, p0, Lfku;->Q:Lkif;

    invoke-virtual {v1, v0}, Lkif;->a(Lu;)V

    .line 55
    invoke-static {}, Lfvc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    new-instance v0, Lkir;

    invoke-direct {v0}, Lkir;-><init>()V

    .line 58
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lfku;->N:Llnl;

    const-class v3, Lcom/google/android/apps/plus/settings/PhotosAboutSettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    const-string v2, "privacy_uri"

    const-string v3, "http://m.google.com/app/plus/serviceurl?type=privacy"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const-string v2, "terms_uri"

    const-string v3, "http://m.google.com/app/plus/serviceurl?type=tos"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    const v2, 0x7f0a088b

    invoke-virtual {p0, v2}, Lfku;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lkir;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 65
    iget-object v1, p0, Lfku;->Q:Lkif;

    invoke-virtual {v1, v0}, Lkif;->a(Lu;)V

    .line 67
    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lfvc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 68
    iget-object v0, p0, Lfku;->Q:Lkif;

    new-instance v1, Lfky;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Lfky;-><init>(I)V

    invoke-virtual {v0, v1}, Lkif;->a(Lu;)V

    .line 71
    :cond_1
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 34
    invoke-super {p0, p1}, Lkgn;->c(Landroid/os/Bundle;)V

    .line 35
    iget-object v0, p0, Lfku;->O:Llnh;

    const-class v1, Lkij;

    iget-object v2, p0, Lfku;->Q:Lkif;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 36
    iget-object v0, p0, Lfku;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lfku;->R:Lhee;

    .line 37
    return-void
.end method
