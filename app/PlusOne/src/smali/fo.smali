.class public final Lfo;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lfw;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Lfo;

.field private static final e:Lfo;


# instance fields
.field private final f:Z

.field private final g:I

.field private final h:Lfw;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 83
    sget-object v0, Lfx;->c:Lfw;

    sput-object v0, Lfo;->a:Lfw;

    .line 113
    const/16 v0, 0x200e

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfo;->b:Ljava/lang/String;

    .line 118
    const/16 v0, 0x200f

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfo;->c:Ljava/lang/String;

    .line 215
    new-instance v0, Lfo;

    const/4 v1, 0x0

    sget-object v2, Lfo;->a:Lfw;

    invoke-direct {v0, v1, v3, v2}, Lfo;-><init>(ZILfw;)V

    sput-object v0, Lfo;->d:Lfo;

    .line 220
    new-instance v0, Lfo;

    const/4 v1, 0x1

    sget-object v2, Lfo;->a:Lfw;

    invoke-direct {v0, v1, v3, v2}, Lfo;-><init>(ZILfw;)V

    sput-object v0, Lfo;->e:Lfo;

    return-void
.end method

.method constructor <init>(ZILfw;)V
    .locals 0

    .prologue
    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    iput-boolean p1, p0, Lfo;->f:Z

    .line 262
    iput p2, p0, Lfo;->g:I

    .line 263
    iput-object p3, p0, Lfo;->h:Lfw;

    .line 264
    return-void
.end method

.method public static a()Lfo;
    .locals 1

    .prologue
    .line 234
    new-instance v0, Lfp;

    invoke-direct {v0}, Lfp;-><init>()V

    invoke-virtual {v0}, Lfp;->a()Lfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/util/Locale;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 78
    invoke-static {p0}, Lge;->a(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 462
    new-instance v0, Lfq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfq;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lfq;->b()I

    move-result v0

    return v0
.end method

.method private static d(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 479
    new-instance v0, Lfq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfq;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lfq;->a()I

    move-result v0

    return v0
.end method

.method static synthetic d()Lfw;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lfo;->a:Lfw;

    return-object v0
.end method

.method static synthetic e()Lfo;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lfo;->e:Lfo;

    return-object v0
.end method

.method static synthetic f()Lfo;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lfo;->d:Lfo;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lfw;Z)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 374
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-interface {p2, p1, v4, v0}, Lfw;->a(Ljava/lang/CharSequence;II)Z

    move-result v1

    .line 375
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 376
    invoke-virtual {p0}, Lfo;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 377
    if-eqz v1, :cond_4

    sget-object v0, Lfx;->b:Lfw;

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-interface {v0, p1, v4, v3}, Lfw;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    iget-boolean v3, p0, Lfo;->f:Z

    if-nez v3, :cond_5

    if-nez v0, :cond_0

    invoke-static {p1}, Lfo;->d(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v6, :cond_5

    :cond_0
    sget-object v0, Lfo;->b:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    :cond_1
    iget-boolean v0, p0, Lfo;->f:Z

    if-eq v1, v0, :cond_9

    .line 381
    if-eqz v1, :cond_8

    const/16 v0, 0x202b

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 382
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    const/16 v0, 0x202c

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 387
    :goto_3
    if-eqz p3, :cond_3

    .line 388
    if-eqz v1, :cond_a

    sget-object v0, Lfx;->b:Lfw;

    :goto_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {v0, p1, v4, v1}, Lfw;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    iget-boolean v1, p0, Lfo;->f:Z

    if-nez v1, :cond_b

    if-nez v0, :cond_2

    invoke-static {p1}, Lfo;->c(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v6, :cond_b

    :cond_2
    sget-object v0, Lfo;->b:Ljava/lang/String;

    :goto_5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 377
    :cond_4
    sget-object v0, Lfx;->a:Lfw;

    goto :goto_0

    :cond_5
    iget-boolean v3, p0, Lfo;->f:Z

    if-eqz v3, :cond_7

    if-eqz v0, :cond_6

    invoke-static {p1}, Lfo;->d(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v5, :cond_7

    :cond_6
    sget-object v0, Lfo;->c:Ljava/lang/String;

    goto :goto_1

    :cond_7
    const-string v0, ""

    goto :goto_1

    .line 381
    :cond_8
    const/16 v0, 0x202a

    goto :goto_2

    .line 385
    :cond_9
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 388
    :cond_a
    sget-object v0, Lfx;->a:Lfw;

    goto :goto_4

    :cond_b
    iget-boolean v1, p0, Lfo;->f:Z

    if-eqz v1, :cond_d

    if-eqz v0, :cond_c

    invoke-static {p1}, Lfo;->c(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v5, :cond_d

    :cond_c
    sget-object v0, Lfo;->c:Ljava/lang/String;

    goto :goto_5

    :cond_d
    const-string v0, ""

    goto :goto_5
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 343
    iget-object v0, p0, Lfo;->h:Lfw;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, p1, v1, v2}, Lfw;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Lfo;->h:Lfw;

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lfo;->a(Ljava/lang/String;Lfw;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lfo;->f:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lfo;->g:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
