.class public final Lmmt;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmmt;


# instance fields
.field public b:Lmof;

.field public c:Lmob;

.field public d:[Lmnk;

.field public e:Lmoa;

.field public f:Lmnx;

.field public g:[Lmmu;

.field public h:[Lmmz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2923
    const/4 v0, 0x0

    new-array v0, v0, [Lmmt;

    sput-object v0, Lmmt;->a:[Lmmt;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2924
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7340
    iput-object v1, p0, Lmmt;->b:Lmof;

    .line 7343
    iput-object v1, p0, Lmmt;->c:Lmob;

    .line 7346
    sget-object v0, Lmnk;->a:[Lmnk;

    iput-object v0, p0, Lmmt;->d:[Lmnk;

    .line 7349
    iput-object v1, p0, Lmmt;->e:Lmoa;

    .line 7352
    iput-object v1, p0, Lmmt;->f:Lmnx;

    .line 7355
    sget-object v0, Lmmu;->a:[Lmmu;

    iput-object v0, p0, Lmmt;->g:[Lmmu;

    .line 7358
    sget-object v0, Lmmz;->a:[Lmmz;

    iput-object v0, p0, Lmmt;->h:[Lmmz;

    .line 2924
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 7402
    .line 7403
    iget-object v0, p0, Lmmt;->b:Lmof;

    if-eqz v0, :cond_9

    .line 7404
    const/4 v0, 0x1

    iget-object v2, p0, Lmmt;->b:Lmof;

    .line 7405
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7407
    :goto_0
    iget-object v2, p0, Lmmt;->c:Lmob;

    if-eqz v2, :cond_0

    .line 7408
    const/4 v2, 0x2

    iget-object v3, p0, Lmmt;->c:Lmob;

    .line 7409
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7411
    :cond_0
    iget-object v2, p0, Lmmt;->d:[Lmnk;

    if-eqz v2, :cond_2

    .line 7412
    iget-object v3, p0, Lmmt;->d:[Lmnk;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 7413
    if-eqz v5, :cond_1

    .line 7414
    const/4 v6, 0x6

    .line 7415
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7412
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 7419
    :cond_2
    iget-object v2, p0, Lmmt;->e:Lmoa;

    if-eqz v2, :cond_3

    .line 7420
    const/16 v2, 0x8

    iget-object v3, p0, Lmmt;->e:Lmoa;

    .line 7421
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7423
    :cond_3
    iget-object v2, p0, Lmmt;->f:Lmnx;

    if-eqz v2, :cond_4

    .line 7424
    const/16 v2, 0x9

    iget-object v3, p0, Lmmt;->f:Lmnx;

    .line 7425
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7427
    :cond_4
    iget-object v2, p0, Lmmt;->g:[Lmmu;

    if-eqz v2, :cond_6

    .line 7428
    iget-object v3, p0, Lmmt;->g:[Lmmu;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 7429
    if-eqz v5, :cond_5

    .line 7430
    const/16 v6, 0xa

    .line 7431
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7428
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 7435
    :cond_6
    iget-object v2, p0, Lmmt;->h:[Lmmz;

    if-eqz v2, :cond_8

    .line 7436
    iget-object v2, p0, Lmmt;->h:[Lmmz;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 7437
    if-eqz v4, :cond_7

    .line 7438
    const/16 v5, 0xb

    .line 7439
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 7436
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 7443
    :cond_8
    iget-object v1, p0, Lmmt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7444
    iput v0, p0, Lmmt;->ai:I

    .line 7445
    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmmt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7453
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7454
    sparse-switch v0, :sswitch_data_0

    .line 7458
    iget-object v2, p0, Lmmt;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 7459
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmmt;->ah:Ljava/util/List;

    .line 7462
    :cond_1
    iget-object v2, p0, Lmmt;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7464
    :sswitch_0
    return-object p0

    .line 7469
    :sswitch_1
    iget-object v0, p0, Lmmt;->b:Lmof;

    if-nez v0, :cond_2

    .line 7470
    new-instance v0, Lmof;

    invoke-direct {v0}, Lmof;-><init>()V

    iput-object v0, p0, Lmmt;->b:Lmof;

    .line 7472
    :cond_2
    iget-object v0, p0, Lmmt;->b:Lmof;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7476
    :sswitch_2
    iget-object v0, p0, Lmmt;->c:Lmob;

    if-nez v0, :cond_3

    .line 7477
    new-instance v0, Lmob;

    invoke-direct {v0}, Lmob;-><init>()V

    iput-object v0, p0, Lmmt;->c:Lmob;

    .line 7479
    :cond_3
    iget-object v0, p0, Lmmt;->c:Lmob;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7483
    :sswitch_3
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7484
    iget-object v0, p0, Lmmt;->d:[Lmnk;

    if-nez v0, :cond_5

    move v0, v1

    .line 7485
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmnk;

    .line 7486
    iget-object v3, p0, Lmmt;->d:[Lmnk;

    if-eqz v3, :cond_4

    .line 7487
    iget-object v3, p0, Lmmt;->d:[Lmnk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7489
    :cond_4
    iput-object v2, p0, Lmmt;->d:[Lmnk;

    .line 7490
    :goto_2
    iget-object v2, p0, Lmmt;->d:[Lmnk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 7491
    iget-object v2, p0, Lmmt;->d:[Lmnk;

    new-instance v3, Lmnk;

    invoke-direct {v3}, Lmnk;-><init>()V

    aput-object v3, v2, v0

    .line 7492
    iget-object v2, p0, Lmmt;->d:[Lmnk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7493
    invoke-virtual {p1}, Loxn;->a()I

    .line 7490
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 7484
    :cond_5
    iget-object v0, p0, Lmmt;->d:[Lmnk;

    array-length v0, v0

    goto :goto_1

    .line 7496
    :cond_6
    iget-object v2, p0, Lmmt;->d:[Lmnk;

    new-instance v3, Lmnk;

    invoke-direct {v3}, Lmnk;-><init>()V

    aput-object v3, v2, v0

    .line 7497
    iget-object v2, p0, Lmmt;->d:[Lmnk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7501
    :sswitch_4
    iget-object v0, p0, Lmmt;->e:Lmoa;

    if-nez v0, :cond_7

    .line 7502
    new-instance v0, Lmoa;

    invoke-direct {v0}, Lmoa;-><init>()V

    iput-object v0, p0, Lmmt;->e:Lmoa;

    .line 7504
    :cond_7
    iget-object v0, p0, Lmmt;->e:Lmoa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7508
    :sswitch_5
    iget-object v0, p0, Lmmt;->f:Lmnx;

    if-nez v0, :cond_8

    .line 7509
    new-instance v0, Lmnx;

    invoke-direct {v0}, Lmnx;-><init>()V

    iput-object v0, p0, Lmmt;->f:Lmnx;

    .line 7511
    :cond_8
    iget-object v0, p0, Lmmt;->f:Lmnx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7515
    :sswitch_6
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7516
    iget-object v0, p0, Lmmt;->g:[Lmmu;

    if-nez v0, :cond_a

    move v0, v1

    .line 7517
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmmu;

    .line 7518
    iget-object v3, p0, Lmmt;->g:[Lmmu;

    if-eqz v3, :cond_9

    .line 7519
    iget-object v3, p0, Lmmt;->g:[Lmmu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7521
    :cond_9
    iput-object v2, p0, Lmmt;->g:[Lmmu;

    .line 7522
    :goto_4
    iget-object v2, p0, Lmmt;->g:[Lmmu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 7523
    iget-object v2, p0, Lmmt;->g:[Lmmu;

    new-instance v3, Lmmu;

    invoke-direct {v3}, Lmmu;-><init>()V

    aput-object v3, v2, v0

    .line 7524
    iget-object v2, p0, Lmmt;->g:[Lmmu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7525
    invoke-virtual {p1}, Loxn;->a()I

    .line 7522
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 7516
    :cond_a
    iget-object v0, p0, Lmmt;->g:[Lmmu;

    array-length v0, v0

    goto :goto_3

    .line 7528
    :cond_b
    iget-object v2, p0, Lmmt;->g:[Lmmu;

    new-instance v3, Lmmu;

    invoke-direct {v3}, Lmmu;-><init>()V

    aput-object v3, v2, v0

    .line 7529
    iget-object v2, p0, Lmmt;->g:[Lmmu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7533
    :sswitch_7
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7534
    iget-object v0, p0, Lmmt;->h:[Lmmz;

    if-nez v0, :cond_d

    move v0, v1

    .line 7535
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lmmz;

    .line 7536
    iget-object v3, p0, Lmmt;->h:[Lmmz;

    if-eqz v3, :cond_c

    .line 7537
    iget-object v3, p0, Lmmt;->h:[Lmmz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7539
    :cond_c
    iput-object v2, p0, Lmmt;->h:[Lmmz;

    .line 7540
    :goto_6
    iget-object v2, p0, Lmmt;->h:[Lmmz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    .line 7541
    iget-object v2, p0, Lmmt;->h:[Lmmz;

    new-instance v3, Lmmz;

    invoke-direct {v3}, Lmmz;-><init>()V

    aput-object v3, v2, v0

    .line 7542
    iget-object v2, p0, Lmmt;->h:[Lmmz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7543
    invoke-virtual {p1}, Loxn;->a()I

    .line 7540
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 7534
    :cond_d
    iget-object v0, p0, Lmmt;->h:[Lmmz;

    array-length v0, v0

    goto :goto_5

    .line 7546
    :cond_e
    iget-object v2, p0, Lmmt;->h:[Lmmz;

    new-instance v3, Lmmz;

    invoke-direct {v3}, Lmmz;-><init>()V

    aput-object v3, v2, v0

    .line 7547
    iget-object v2, p0, Lmmt;->h:[Lmmz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7454
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x32 -> :sswitch_3
        0x42 -> :sswitch_4
        0x4a -> :sswitch_5
        0x52 -> :sswitch_6
        0x5a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 7363
    iget-object v1, p0, Lmmt;->b:Lmof;

    if-eqz v1, :cond_0

    .line 7364
    const/4 v1, 0x1

    iget-object v2, p0, Lmmt;->b:Lmof;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7366
    :cond_0
    iget-object v1, p0, Lmmt;->c:Lmob;

    if-eqz v1, :cond_1

    .line 7367
    const/4 v1, 0x2

    iget-object v2, p0, Lmmt;->c:Lmob;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7369
    :cond_1
    iget-object v1, p0, Lmmt;->d:[Lmnk;

    if-eqz v1, :cond_3

    .line 7370
    iget-object v2, p0, Lmmt;->d:[Lmnk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 7371
    if-eqz v4, :cond_2

    .line 7372
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7370
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7376
    :cond_3
    iget-object v1, p0, Lmmt;->e:Lmoa;

    if-eqz v1, :cond_4

    .line 7377
    const/16 v1, 0x8

    iget-object v2, p0, Lmmt;->e:Lmoa;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7379
    :cond_4
    iget-object v1, p0, Lmmt;->f:Lmnx;

    if-eqz v1, :cond_5

    .line 7380
    const/16 v1, 0x9

    iget-object v2, p0, Lmmt;->f:Lmnx;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7382
    :cond_5
    iget-object v1, p0, Lmmt;->g:[Lmmu;

    if-eqz v1, :cond_7

    .line 7383
    iget-object v2, p0, Lmmt;->g:[Lmmu;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 7384
    if-eqz v4, :cond_6

    .line 7385
    const/16 v5, 0xa

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7383
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7389
    :cond_7
    iget-object v1, p0, Lmmt;->h:[Lmmz;

    if-eqz v1, :cond_9

    .line 7390
    iget-object v1, p0, Lmmt;->h:[Lmmz;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 7391
    if-eqz v3, :cond_8

    .line 7392
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 7390
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 7396
    :cond_9
    iget-object v0, p0, Lmmt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7398
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2920
    invoke-virtual {p0, p1}, Lmmt;->a(Loxn;)Lmmt;

    move-result-object v0

    return-object v0
.end method
