.class public final Ljjc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjm;
.implements Ljjp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "com.google.android.libraries.social.notifications.DISMISS_ALL_NOTIFICATIONS"

    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 20
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->a(Landroid/content/Intent;)Lkfo;

    move-result-object v1

    .line 21
    if-nez v1, :cond_0

    .line 28
    :goto_0
    return-void

    .line 24
    :cond_0
    const-string v0, "notification_event_type"

    .line 25
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljir;

    .line 26
    invoke-virtual {v1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v2, "GunsSyncer"

    const-string v3, "Cannot find database helper for account."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-static {p2, v1}, Ljju;->b(Landroid/content/Context;Lkfo;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v4, Ljjw;

    invoke-direct {v4, p2, v1, v2, v3}, Ljjw;-><init>(Landroid/content/Context;Lkfo;J)V

    const-class v1, Ljjo;

    invoke-static {p2, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljjo;

    invoke-virtual {v1, v4, p0, p2, v8}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-virtual {v4}, Ljjw;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "SetReadStatesHelper"

    const-string v2, "Failed to set read states."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    :cond_1
    invoke-static {p2, v8, v0}, Ljkh;->a(Landroid/content/Context;[Ljiu;Ljir;)V

    goto :goto_0

    .line 26
    :cond_2
    invoke-virtual {v4}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    const-string v6, "read_state"

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "notifications"

    const-string v7, "read_state IN (1,4)"

    invoke-virtual {v4, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v4, Ljkn;

    invoke-direct {v4}, Ljkn;-><init>()V

    invoke-virtual {v4, p2, v2, v3}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Ljjc;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
