.class public final Lnar;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:Lnzi;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnar;->a:[Ljava/lang/String;

    .line 25
    const/high16 v0, -0x80000000

    iput v0, p0, Lnar;->d:I

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lnar;->g:Lnzi;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 68
    .line 69
    iget-object v1, p0, Lnar;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnar;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 71
    iget-object v2, p0, Lnar;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 73
    invoke-static {v4}, Loxo;->b(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    iget-object v0, p0, Lnar;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 78
    :cond_1
    iget-object v1, p0, Lnar;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 79
    const/4 v1, 0x2

    iget-object v2, p0, Lnar;->b:Ljava/lang/String;

    .line 80
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_2
    iget-object v1, p0, Lnar;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 83
    const/4 v1, 0x3

    iget-object v2, p0, Lnar;->c:Ljava/lang/String;

    .line 84
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_3
    iget-object v1, p0, Lnar;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 87
    const/4 v1, 0x4

    iget-object v2, p0, Lnar;->e:Ljava/lang/String;

    .line 88
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_4
    iget-object v1, p0, Lnar;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 91
    const/4 v1, 0x5

    iget-object v2, p0, Lnar;->f:Ljava/lang/Boolean;

    .line 92
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 94
    :cond_5
    iget v1, p0, Lnar;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_6

    .line 95
    const/4 v1, 0x6

    iget v2, p0, Lnar;->d:I

    .line 96
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_6
    iget-object v1, p0, Lnar;->g:Lnzi;

    if-eqz v1, :cond_7

    .line 99
    const/4 v1, 0x7

    iget-object v2, p0, Lnar;->g:Lnzi;

    .line 100
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_7
    iget-object v1, p0, Lnar;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    iput v0, p0, Lnar;->ai:I

    .line 104
    return v0
.end method

.method public a(Loxn;)Lnar;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 112
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 113
    sparse-switch v0, :sswitch_data_0

    .line 117
    iget-object v1, p0, Lnar;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 118
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnar;->ah:Ljava/util/List;

    .line 121
    :cond_1
    iget-object v1, p0, Lnar;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    :sswitch_0
    return-object p0

    .line 128
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 129
    iget-object v0, p0, Lnar;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 130
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 131
    iget-object v2, p0, Lnar;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    iput-object v1, p0, Lnar;->a:[Ljava/lang/String;

    .line 133
    :goto_1
    iget-object v1, p0, Lnar;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 134
    iget-object v1, p0, Lnar;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 135
    invoke-virtual {p1}, Loxn;->a()I

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 138
    :cond_2
    iget-object v1, p0, Lnar;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 142
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnar;->b:Ljava/lang/String;

    goto :goto_0

    .line 146
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnar;->c:Ljava/lang/String;

    goto :goto_0

    .line 150
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnar;->e:Ljava/lang/String;

    goto :goto_0

    .line 154
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnar;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 158
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 159
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 163
    :cond_3
    iput v0, p0, Lnar;->d:I

    goto :goto_0

    .line 165
    :cond_4
    iput v3, p0, Lnar;->d:I

    goto :goto_0

    .line 170
    :sswitch_7
    iget-object v0, p0, Lnar;->g:Lnzi;

    if-nez v0, :cond_5

    .line 171
    new-instance v0, Lnzi;

    invoke-direct {v0}, Lnzi;-><init>()V

    iput-object v0, p0, Lnar;->g:Lnzi;

    .line 173
    :cond_5
    iget-object v0, p0, Lnar;->g:Lnzi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 113
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 39
    iget-object v0, p0, Lnar;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 40
    iget-object v1, p0, Lnar;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 41
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    iget-object v0, p0, Lnar;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 45
    const/4 v0, 0x2

    iget-object v1, p0, Lnar;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 47
    :cond_1
    iget-object v0, p0, Lnar;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 48
    const/4 v0, 0x3

    iget-object v1, p0, Lnar;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 50
    :cond_2
    iget-object v0, p0, Lnar;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 51
    const/4 v0, 0x4

    iget-object v1, p0, Lnar;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 53
    :cond_3
    iget-object v0, p0, Lnar;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 54
    const/4 v0, 0x5

    iget-object v1, p0, Lnar;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 56
    :cond_4
    iget v0, p0, Lnar;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_5

    .line 57
    const/4 v0, 0x6

    iget v1, p0, Lnar;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 59
    :cond_5
    iget-object v0, p0, Lnar;->g:Lnzi;

    if-eqz v0, :cond_6

    .line 60
    const/4 v0, 0x7

    iget-object v1, p0, Lnar;->g:Lnzi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 62
    :cond_6
    iget-object v0, p0, Lnar;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 64
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnar;->a(Loxn;)Lnar;

    move-result-object v0

    return-object v0
.end method
