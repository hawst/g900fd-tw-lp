.class public final Lmhv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmzv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22354
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22357
    iput-object v0, p0, Lmhv;->apiHeader:Llyr;

    .line 22360
    iput-object v0, p0, Lmhv;->a:Lmzv;

    .line 22354
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 22377
    const/4 v0, 0x0

    .line 22378
    iget-object v1, p0, Lmhv;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 22379
    const/4 v0, 0x1

    iget-object v1, p0, Lmhv;->apiHeader:Llyr;

    .line 22380
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22382
    :cond_0
    iget-object v1, p0, Lmhv;->a:Lmzv;

    if-eqz v1, :cond_1

    .line 22383
    const/4 v1, 0x2

    iget-object v2, p0, Lmhv;->a:Lmzv;

    .line 22384
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22386
    :cond_1
    iget-object v1, p0, Lmhv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22387
    iput v0, p0, Lmhv;->ai:I

    .line 22388
    return v0
.end method

.method public a(Loxn;)Lmhv;
    .locals 2

    .prologue
    .line 22396
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 22397
    sparse-switch v0, :sswitch_data_0

    .line 22401
    iget-object v1, p0, Lmhv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 22402
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhv;->ah:Ljava/util/List;

    .line 22405
    :cond_1
    iget-object v1, p0, Lmhv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22407
    :sswitch_0
    return-object p0

    .line 22412
    :sswitch_1
    iget-object v0, p0, Lmhv;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 22413
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmhv;->apiHeader:Llyr;

    .line 22415
    :cond_2
    iget-object v0, p0, Lmhv;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22419
    :sswitch_2
    iget-object v0, p0, Lmhv;->a:Lmzv;

    if-nez v0, :cond_3

    .line 22420
    new-instance v0, Lmzv;

    invoke-direct {v0}, Lmzv;-><init>()V

    iput-object v0, p0, Lmhv;->a:Lmzv;

    .line 22422
    :cond_3
    iget-object v0, p0, Lmhv;->a:Lmzv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22397
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22365
    iget-object v0, p0, Lmhv;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 22366
    const/4 v0, 0x1

    iget-object v1, p0, Lmhv;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22368
    :cond_0
    iget-object v0, p0, Lmhv;->a:Lmzv;

    if-eqz v0, :cond_1

    .line 22369
    const/4 v0, 0x2

    iget-object v1, p0, Lmhv;->a:Lmzv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22371
    :cond_1
    iget-object v0, p0, Lmhv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 22373
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22350
    invoke-virtual {p0, p1}, Lmhv;->a(Loxn;)Lmhv;

    move-result-object v0

    return-object v0
.end method
