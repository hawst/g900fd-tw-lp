.class public final Lnkj;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 236
    invoke-direct {p0}, Loxq;-><init>()V

    .line 272
    iput v0, p0, Lnkj;->a:I

    .line 275
    iput v0, p0, Lnkj;->b:I

    .line 278
    iput v0, p0, Lnkj;->c:I

    .line 236
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 303
    const/4 v0, 0x0

    .line 304
    iget v1, p0, Lnkj;->a:I

    if-eq v1, v3, :cond_0

    .line 305
    const/4 v0, 0x1

    iget v1, p0, Lnkj;->a:I

    .line 306
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 308
    :cond_0
    iget v1, p0, Lnkj;->b:I

    if-eq v1, v3, :cond_1

    .line 309
    const/4 v1, 0x2

    iget v2, p0, Lnkj;->b:I

    .line 310
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 312
    :cond_1
    iget v1, p0, Lnkj;->c:I

    if-eq v1, v3, :cond_2

    .line 313
    const/4 v1, 0x3

    iget v2, p0, Lnkj;->c:I

    .line 314
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 316
    :cond_2
    iget-object v1, p0, Lnkj;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 317
    const/4 v1, 0x4

    iget-object v2, p0, Lnkj;->d:Ljava/lang/String;

    .line 318
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 320
    :cond_3
    iget-object v1, p0, Lnkj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    iput v0, p0, Lnkj;->ai:I

    .line 322
    return v0
.end method

.method public a(Loxn;)Lnkj;
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 330
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 331
    sparse-switch v0, :sswitch_data_0

    .line 335
    iget-object v1, p0, Lnkj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 336
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnkj;->ah:Ljava/util/List;

    .line 339
    :cond_1
    iget-object v1, p0, Lnkj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    :sswitch_0
    return-object p0

    .line 346
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 347
    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    if-ne v0, v4, :cond_3

    .line 350
    :cond_2
    iput v0, p0, Lnkj;->a:I

    goto :goto_0

    .line 352
    :cond_3
    iput v2, p0, Lnkj;->a:I

    goto :goto_0

    .line 357
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 358
    if-eq v0, v2, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-ne v0, v1, :cond_5

    .line 370
    :cond_4
    iput v0, p0, Lnkj;->b:I

    goto :goto_0

    .line 372
    :cond_5
    iput v2, p0, Lnkj;->b:I

    goto :goto_0

    .line 377
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 378
    if-eqz v0, :cond_6

    if-eq v0, v2, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    const/4 v1, 0x7

    if-eq v0, v1, :cond_6

    if-eq v0, v5, :cond_6

    if-eq v0, v6, :cond_6

    const/4 v1, 0x6

    if-eq v0, v1, :cond_6

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 387
    :cond_6
    iput v0, p0, Lnkj;->c:I

    goto :goto_0

    .line 389
    :cond_7
    const/4 v0, 0x0

    iput v0, p0, Lnkj;->c:I

    goto :goto_0

    .line 394
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnkj;->d:Ljava/lang/String;

    goto :goto_0

    .line 331
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 285
    iget v0, p0, Lnkj;->a:I

    if-eq v0, v2, :cond_0

    .line 286
    const/4 v0, 0x1

    iget v1, p0, Lnkj;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 288
    :cond_0
    iget v0, p0, Lnkj;->b:I

    if-eq v0, v2, :cond_1

    .line 289
    const/4 v0, 0x2

    iget v1, p0, Lnkj;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 291
    :cond_1
    iget v0, p0, Lnkj;->c:I

    if-eq v0, v2, :cond_2

    .line 292
    const/4 v0, 0x3

    iget v1, p0, Lnkj;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 294
    :cond_2
    iget-object v0, p0, Lnkj;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 295
    const/4 v0, 0x4

    iget-object v1, p0, Lnkj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 297
    :cond_3
    iget-object v0, p0, Lnkj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 299
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 232
    invoke-virtual {p0, p1}, Lnkj;->a(Loxn;)Lnkj;

    move-result-object v0

    return-object v0
.end method
