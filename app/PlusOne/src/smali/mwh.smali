.class public final Lmwh;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8070
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8075
    const/high16 v0, -0x80000000

    iput v0, p0, Lmwh;->b:I

    .line 8070
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8092
    const/4 v0, 0x0

    .line 8093
    iget-object v1, p0, Lmwh;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8094
    const/4 v0, 0x1

    iget-object v1, p0, Lmwh;->a:Ljava/lang/String;

    .line 8095
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8097
    :cond_0
    iget v1, p0, Lmwh;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 8098
    const/4 v1, 0x2

    iget v2, p0, Lmwh;->b:I

    .line 8099
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8101
    :cond_1
    iget-object v1, p0, Lmwh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8102
    iput v0, p0, Lmwh;->ai:I

    .line 8103
    return v0
.end method

.method public a(Loxn;)Lmwh;
    .locals 2

    .prologue
    .line 8111
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8112
    sparse-switch v0, :sswitch_data_0

    .line 8116
    iget-object v1, p0, Lmwh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8117
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwh;->ah:Ljava/util/List;

    .line 8120
    :cond_1
    iget-object v1, p0, Lmwh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8122
    :sswitch_0
    return-object p0

    .line 8127
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwh;->a:Ljava/lang/String;

    goto :goto_0

    .line 8131
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 8132
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 8137
    :cond_2
    iput v0, p0, Lmwh;->b:I

    goto :goto_0

    .line 8139
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmwh;->b:I

    goto :goto_0

    .line 8112
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8080
    iget-object v0, p0, Lmwh;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 8081
    const/4 v0, 0x1

    iget-object v1, p0, Lmwh;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8083
    :cond_0
    iget v0, p0, Lmwh;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 8084
    const/4 v0, 0x2

    iget v1, p0, Lmwh;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8086
    :cond_1
    iget-object v0, p0, Lmwh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8088
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8066
    invoke-virtual {p0, p1}, Lmwh;->a(Loxn;)Lmwh;

    move-result-object v0

    return-object v0
.end method
