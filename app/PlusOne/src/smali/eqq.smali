.class final Leqq;
.super Lhyd;
.source "PG"


# instance fields
.field private final e:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 578
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhyd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 579
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Leqq;->e:Landroid/view/LayoutInflater;

    .line 580
    return-void
.end method

.method static synthetic a(Leqq;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Leqq;->c:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 584
    iget-object v0, p0, Leqq;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f04022d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 32

    .prologue
    .line 589
    const v4, 0x7f100620

    .line 590
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;

    .line 591
    const v5, 0x7f100621

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 592
    const v6, 0x7f100622

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 593
    const v7, 0x7f10031d

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 594
    const v8, 0x7f100343

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 595
    const v9, 0x7f100623

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 597
    const/4 v9, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 598
    const/4 v9, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 599
    const/4 v9, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 600
    const/4 v9, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 601
    const/4 v9, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 602
    const/4 v9, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 603
    const/4 v9, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 604
    const/4 v9, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 605
    const/16 v9, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 606
    const/16 v9, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 608
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 610
    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-static {v9, v11}, Lhsf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v28

    const-wide/16 v30, 0x0

    cmp-long v9, v28, v30

    if-lez v9, :cond_4

    const/4 v9, 0x1

    move v11, v9

    .line 612
    :goto_0
    if-eqz v10, :cond_5

    move v9, v10

    .line 614
    :goto_1
    const v21, 0x7f100091

    move-object/from16 v0, p1

    move/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 615
    const v21, 0x7f100094

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, p1

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 616
    const v16, 0x7f100095

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v9}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 617
    const/4 v9, -0x1

    if-ne v13, v9, :cond_6

    .line 618
    const v9, 0x7f100096

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v13}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 624
    :goto_2
    if-eqz v11, :cond_9

    .line 625
    const v9, 0x7f0a0a07

    .line 626
    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    sget-object v14, Ljac;->a:Ljac;

    move-object/from16 v0, p2

    invoke-static {v0, v13, v14}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v13

    invoke-virtual {v4, v13}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->a(Lizu;)V

    .line 627
    const v13, 0x7f100091

    move-object/from16 v0, v18

    invoke-virtual {v4, v13, v0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setTag(ILjava/lang/Object;)V

    .line 628
    new-instance v13, Leqr;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Leqr;-><init>(Leqq;)V

    invoke-virtual {v4, v13}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 638
    const/16 v13, 0x12c

    move/from16 v0, v19

    if-ne v0, v13, :cond_7

    .line 639
    const/high16 v13, -0x10000

    invoke-virtual {v4, v13}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setBackgroundColor(I)V

    move v4, v9

    .line 651
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Leqq;->c:Landroid/content/Context;

    const/4 v9, 0x2

    new-array v14, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    .line 652
    sparse-switch v19, :sswitch_data_0

    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v16, 0x7f0a0a12

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    :goto_4
    aput-object v9, v14, v15

    const/4 v15, 0x1

    sparse-switch v10, :sswitch_data_1

    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v10, 0x7f0a0a18

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    :goto_5
    aput-object v9, v14, v15

    .line 651
    invoke-virtual {v13, v4, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 653
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 654
    packed-switch v20, :pswitch_data_0

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a30

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_6
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 656
    const/16 v4, 0x64

    move/from16 v0, v19

    if-ne v0, v4, :cond_b

    const/4 v4, 0x1

    move v5, v4

    .line 657
    :goto_7
    const/16 v4, 0xc8

    move/from16 v0, v19

    if-ne v0, v4, :cond_c

    const/4 v4, 0x1

    .line 659
    :goto_8
    if-nez v5, :cond_0

    if-eqz v4, :cond_d

    :cond_0
    const/4 v4, 0x1

    .line 660
    :goto_9
    if-eqz v11, :cond_e

    if-eqz v4, :cond_e

    const-wide/16 v4, 0x0

    cmp-long v4, v26, v4

    if-eqz v4, :cond_e

    const/4 v4, 0x1

    move v5, v4

    .line 661
    :goto_a
    if-eqz v5, :cond_2

    .line 662
    move-object/from16 v0, p0

    iget-object v6, v0, Leqq;->c:Landroid/content/Context;

    const v9, 0x7f0a0a08

    const/4 v4, 0x2

    new-array v10, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 663
    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v10, v4

    const/4 v13, 0x1

    const-wide/16 v14, 0x0

    cmp-long v4, v26, v14

    if-eqz v4, :cond_1

    const-wide/16 v14, 0x0

    cmp-long v4, v24, v14

    if-nez v4, :cond_f

    :cond_1
    const/4 v4, 0x0

    :goto_b
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v10, v13

    .line 662
    invoke-virtual {v6, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 665
    :cond_2
    if-eqz v5, :cond_10

    const/4 v4, 0x0

    :goto_c
    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 667
    if-eqz v11, :cond_11

    const-wide/16 v4, 0x0

    cmp-long v4, v22, v4

    if-lez v4, :cond_11

    const/4 v4, 0x1

    .line 668
    :goto_d
    if-eqz v4, :cond_3

    .line 669
    const-string v5, "MMM dd, yyyy h:mmaa"

    .line 670
    move-wide/from16 v0, v22

    invoke-static {v5, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 672
    :cond_3
    if-eqz v4, :cond_12

    const/4 v4, 0x0

    :goto_e
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 674
    if-eqz v11, :cond_13

    const/16 v4, 0x8

    :goto_f
    invoke-virtual {v12, v4}, Landroid/view/View;->setVisibility(I)V

    .line 675
    return-void

    .line 610
    :cond_4
    const/4 v9, 0x0

    move v11, v9

    goto/16 :goto_0

    .line 612
    :cond_5
    const/16 v9, 0xa

    goto/16 :goto_1

    .line 620
    :cond_6
    const v9, 0x7f100096

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v13}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_2

    .line 640
    :cond_7
    const/16 v13, 0x190

    move/from16 v0, v19

    if-ne v0, v13, :cond_8

    .line 641
    const v13, -0xff0100

    invoke-virtual {v4, v13}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setBackgroundColor(I)V

    move v4, v9

    goto/16 :goto_3

    .line 643
    :cond_8
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move v4, v9

    goto/16 :goto_3

    .line 646
    :cond_9
    const v9, 0x7f0a0a0b

    .line 647
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->a(Lizu;)V

    .line 648
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 649
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move v4, v9

    goto/16 :goto_3

    .line 652
    :sswitch_0
    const/4 v9, 0x1

    move/from16 v0, v20

    if-ne v0, v9, :cond_a

    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v16, 0x7f0a0a0c

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    :cond_a
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v16, 0x7f0a0a0d

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v16, 0x7f0a0a0e

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v16, 0x7f0a0a11

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v16, 0x7f0a0a10

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v16, 0x7f0a0a0f

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v10, 0x7f0a0a13

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_5

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v10, 0x7f0a0a14

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_5

    :sswitch_7
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v10, 0x7f0a0a15

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_5

    :sswitch_8
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v10, 0x7f0a0a16

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_5

    :sswitch_9
    move-object/from16 v0, p0

    iget-object v9, v0, Leqq;->c:Landroid/content/Context;

    const v10, 0x7f0a0a17

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_5

    .line 654
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a19

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a1a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a1b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_4
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a1c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a1d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a1e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a1f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_8
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a20

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_9
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a21

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_a
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a22

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_b
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a23

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_c
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a24

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_d
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a25

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_e
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a26

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_f
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a27

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_10
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a28

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_11
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a29

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_12
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a2a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_13
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a2b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_14
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a2c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_15
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a2d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_16
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a2e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_17
    move-object/from16 v0, p0

    iget-object v4, v0, Leqq;->c:Landroid/content/Context;

    const v5, 0x7f0a0a2f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    .line 656
    :cond_b
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_7

    .line 657
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_8

    .line 659
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_9

    .line 660
    :cond_e
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_a

    .line 663
    :cond_f
    move-wide/from16 v0, v24

    long-to-float v4, v0

    move-wide/from16 v0, v26

    long-to-float v14, v0

    div-float/2addr v4, v14

    float-to-double v14, v4

    const-wide/high16 v16, 0x4059000000000000L    # 100.0

    mul-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->round(D)J

    move-result-wide v14

    long-to-int v4, v14

    const/16 v14, 0x64

    invoke-static {v4, v14}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto/16 :goto_b

    .line 665
    :cond_10
    const/16 v4, 0x8

    goto/16 :goto_c

    .line 667
    :cond_11
    const/4 v4, 0x0

    goto/16 :goto_d

    .line 672
    :cond_12
    const/16 v4, 0x8

    goto/16 :goto_e

    .line 674
    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_f

    .line 652
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_5
        0xa -> :sswitch_8
        0x14 -> :sswitch_6
        0x1e -> :sswitch_7
        0x28 -> :sswitch_9
    .end sparse-switch

    .line 654
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method
