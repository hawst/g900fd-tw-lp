.class public final Lnqj;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnqe;

.field public b:Lnpx;

.field public c:I

.field public d:I

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25
    sget-object v0, Lnqe;->a:[Lnqe;

    iput-object v0, p0, Lnqj;->a:[Lnqe;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lnqj;->b:Lnpx;

    .line 31
    iput v1, p0, Lnqj;->c:I

    .line 44
    iput v1, p0, Lnqj;->d:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v6, -0x80000000

    .line 101
    .line 102
    iget-object v1, p0, Lnqj;->a:[Lnqe;

    if-eqz v1, :cond_1

    .line 103
    iget-object v2, p0, Lnqj;->a:[Lnqe;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 104
    if-eqz v4, :cond_0

    .line 105
    const/4 v5, 0x1

    .line 106
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 103
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    :cond_1
    iget-object v1, p0, Lnqj;->b:Lnpx;

    if-eqz v1, :cond_2

    .line 111
    const/4 v1, 0x2

    iget-object v2, p0, Lnqj;->b:Lnpx;

    .line 112
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    :cond_2
    iget v1, p0, Lnqj;->c:I

    if-eq v1, v6, :cond_3

    .line 115
    const/4 v1, 0x3

    iget v2, p0, Lnqj;->c:I

    .line 116
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_3
    iget-object v1, p0, Lnqj;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 119
    const/4 v1, 0x4

    iget-object v2, p0, Lnqj;->g:Ljava/lang/String;

    .line 120
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    :cond_4
    iget-object v1, p0, Lnqj;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 123
    const/4 v1, 0x5

    iget-object v2, p0, Lnqj;->h:Ljava/lang/Boolean;

    .line 124
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 126
    :cond_5
    iget-object v1, p0, Lnqj;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 127
    const/4 v1, 0x6

    iget-object v2, p0, Lnqj;->i:Ljava/lang/String;

    .line 128
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    :cond_6
    iget-object v1, p0, Lnqj;->j:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 131
    const/4 v1, 0x7

    iget-object v2, p0, Lnqj;->j:Ljava/lang/String;

    .line 132
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    :cond_7
    iget-object v1, p0, Lnqj;->k:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 135
    const/16 v1, 0x8

    iget-object v2, p0, Lnqj;->k:Ljava/lang/String;

    .line 136
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_8
    iget v1, p0, Lnqj;->d:I

    if-eq v1, v6, :cond_9

    .line 139
    const/16 v1, 0x9

    iget v2, p0, Lnqj;->d:I

    .line 140
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    :cond_9
    iget-object v1, p0, Lnqj;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 143
    const/16 v1, 0xa

    iget-object v2, p0, Lnqj;->l:Ljava/lang/String;

    .line 144
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    :cond_a
    iget-object v1, p0, Lnqj;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 147
    const/16 v1, 0xb

    iget-object v2, p0, Lnqj;->e:Ljava/lang/Boolean;

    .line 148
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 150
    :cond_b
    iget-object v1, p0, Lnqj;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 151
    const/16 v1, 0xc

    iget-object v2, p0, Lnqj;->f:Ljava/lang/Boolean;

    .line 152
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 154
    :cond_c
    iget-object v1, p0, Lnqj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    iput v0, p0, Lnqj;->ai:I

    .line 156
    return v0
.end method

.method public a(Loxn;)Lnqj;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 164
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 165
    sparse-switch v0, :sswitch_data_0

    .line 169
    iget-object v2, p0, Lnqj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 170
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnqj;->ah:Ljava/util/List;

    .line 173
    :cond_1
    iget-object v2, p0, Lnqj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    :sswitch_0
    return-object p0

    .line 180
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 181
    iget-object v0, p0, Lnqj;->a:[Lnqe;

    if-nez v0, :cond_3

    move v0, v1

    .line 182
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnqe;

    .line 183
    iget-object v3, p0, Lnqj;->a:[Lnqe;

    if-eqz v3, :cond_2

    .line 184
    iget-object v3, p0, Lnqj;->a:[Lnqe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    :cond_2
    iput-object v2, p0, Lnqj;->a:[Lnqe;

    .line 187
    :goto_2
    iget-object v2, p0, Lnqj;->a:[Lnqe;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 188
    iget-object v2, p0, Lnqj;->a:[Lnqe;

    new-instance v3, Lnqe;

    invoke-direct {v3}, Lnqe;-><init>()V

    aput-object v3, v2, v0

    .line 189
    iget-object v2, p0, Lnqj;->a:[Lnqe;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 190
    invoke-virtual {p1}, Loxn;->a()I

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 181
    :cond_3
    iget-object v0, p0, Lnqj;->a:[Lnqe;

    array-length v0, v0

    goto :goto_1

    .line 193
    :cond_4
    iget-object v2, p0, Lnqj;->a:[Lnqe;

    new-instance v3, Lnqe;

    invoke-direct {v3}, Lnqe;-><init>()V

    aput-object v3, v2, v0

    .line 194
    iget-object v2, p0, Lnqj;->a:[Lnqe;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 198
    :sswitch_2
    iget-object v0, p0, Lnqj;->b:Lnpx;

    if-nez v0, :cond_5

    .line 199
    new-instance v0, Lnpx;

    invoke-direct {v0}, Lnpx;-><init>()V

    iput-object v0, p0, Lnqj;->b:Lnpx;

    .line 201
    :cond_5
    iget-object v0, p0, Lnqj;->b:Lnpx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 205
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 206
    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_6

    if-ne v0, v5, :cond_7

    .line 209
    :cond_6
    iput v0, p0, Lnqj;->c:I

    goto/16 :goto_0

    .line 211
    :cond_7
    iput v1, p0, Lnqj;->c:I

    goto/16 :goto_0

    .line 216
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqj;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 220
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqj;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 224
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqj;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 228
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqj;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 232
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqj;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 236
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 237
    if-eq v0, v4, :cond_8

    if-eq v0, v5, :cond_8

    const/4 v2, 0x3

    if-ne v0, v2, :cond_9

    .line 240
    :cond_8
    iput v0, p0, Lnqj;->d:I

    goto/16 :goto_0

    .line 242
    :cond_9
    iput v4, p0, Lnqj;->d:I

    goto/16 :goto_0

    .line 247
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqj;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 251
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqj;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 255
    :sswitch_c
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqj;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 165
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 55
    iget-object v0, p0, Lnqj;->a:[Lnqe;

    if-eqz v0, :cond_1

    .line 56
    iget-object v1, p0, Lnqj;->a:[Lnqe;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 57
    if-eqz v3, :cond_0

    .line 58
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 56
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_1
    iget-object v0, p0, Lnqj;->b:Lnpx;

    if-eqz v0, :cond_2

    .line 63
    const/4 v0, 0x2

    iget-object v1, p0, Lnqj;->b:Lnpx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 65
    :cond_2
    iget v0, p0, Lnqj;->c:I

    if-eq v0, v5, :cond_3

    .line 66
    const/4 v0, 0x3

    iget v1, p0, Lnqj;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 68
    :cond_3
    iget-object v0, p0, Lnqj;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 69
    const/4 v0, 0x4

    iget-object v1, p0, Lnqj;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 71
    :cond_4
    iget-object v0, p0, Lnqj;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 72
    const/4 v0, 0x5

    iget-object v1, p0, Lnqj;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 74
    :cond_5
    iget-object v0, p0, Lnqj;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 75
    const/4 v0, 0x6

    iget-object v1, p0, Lnqj;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 77
    :cond_6
    iget-object v0, p0, Lnqj;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 78
    const/4 v0, 0x7

    iget-object v1, p0, Lnqj;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 80
    :cond_7
    iget-object v0, p0, Lnqj;->k:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 81
    const/16 v0, 0x8

    iget-object v1, p0, Lnqj;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 83
    :cond_8
    iget v0, p0, Lnqj;->d:I

    if-eq v0, v5, :cond_9

    .line 84
    const/16 v0, 0x9

    iget v1, p0, Lnqj;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 86
    :cond_9
    iget-object v0, p0, Lnqj;->l:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 87
    const/16 v0, 0xa

    iget-object v1, p0, Lnqj;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 89
    :cond_a
    iget-object v0, p0, Lnqj;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 90
    const/16 v0, 0xb

    iget-object v1, p0, Lnqj;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 92
    :cond_b
    iget-object v0, p0, Lnqj;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 93
    const/16 v0, 0xc

    iget-object v1, p0, Lnqj;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 95
    :cond_c
    iget-object v0, p0, Lnqj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 97
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnqj;->a(Loxn;)Lnqj;

    move-result-object v0

    return-object v0
.end method
