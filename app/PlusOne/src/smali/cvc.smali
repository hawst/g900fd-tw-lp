.class final Lcvc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcuz;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private synthetic c:Lcuy;


# direct methods
.method public constructor <init>(Lcuy;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 352
    iput-object p1, p0, Lcvc;->c:Lcuy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353
    iput-object p2, p0, Lcvc;->a:Ljava/util/Set;

    .line 354
    const/4 v0, 0x0

    iput v0, p0, Lcvc;->b:I

    .line 355
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 377
    iget v0, p0, Lcvc;->b:I

    return v0
.end method

.method public a(II)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 359
    iget-object v0, p0, Lcvc;->c:Lcuy;

    invoke-static {v0}, Lcuy;->g(Lcuy;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "all_photos"

    .line 360
    invoke-static {}, Lcuy;->c()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "local_content_uri LIKE ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcvc;->c:Lcuy;

    .line 361
    invoke-static {v7}, Lcuy;->a(Lcuy;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x18

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v6, v5

    move-object v7, v5

    .line 359
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 367
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcvc;->c:Lcuy;

    invoke-static {v0}, Lcuy;->c(Lcuy;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 368
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 369
    iget-object v1, p0, Lcvc;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 370
    iget-object v1, p0, Lcvc;->c:Lcuy;

    invoke-static {v1}, Lcuy;->g(Lcuy;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {v1, v0}, Ljvd;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 371
    iget v0, p0, Lcvc;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcvc;->b:I

    goto :goto_0

    .line 374
    :cond_1
    return-void
.end method
