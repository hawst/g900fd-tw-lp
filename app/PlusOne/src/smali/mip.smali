.class public final Lmip;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnfs;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24946
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24949
    iput-object v0, p0, Lmip;->apiHeader:Llyr;

    .line 24952
    iput-object v0, p0, Lmip;->a:Lnfs;

    .line 24946
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 24969
    const/4 v0, 0x0

    .line 24970
    iget-object v1, p0, Lmip;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 24971
    const/4 v0, 0x1

    iget-object v1, p0, Lmip;->apiHeader:Llyr;

    .line 24972
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24974
    :cond_0
    iget-object v1, p0, Lmip;->a:Lnfs;

    if-eqz v1, :cond_1

    .line 24975
    const/4 v1, 0x2

    iget-object v2, p0, Lmip;->a:Lnfs;

    .line 24976
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24978
    :cond_1
    iget-object v1, p0, Lmip;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24979
    iput v0, p0, Lmip;->ai:I

    .line 24980
    return v0
.end method

.method public a(Loxn;)Lmip;
    .locals 2

    .prologue
    .line 24988
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 24989
    sparse-switch v0, :sswitch_data_0

    .line 24993
    iget-object v1, p0, Lmip;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 24994
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmip;->ah:Ljava/util/List;

    .line 24997
    :cond_1
    iget-object v1, p0, Lmip;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24999
    :sswitch_0
    return-object p0

    .line 25004
    :sswitch_1
    iget-object v0, p0, Lmip;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 25005
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmip;->apiHeader:Llyr;

    .line 25007
    :cond_2
    iget-object v0, p0, Lmip;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25011
    :sswitch_2
    iget-object v0, p0, Lmip;->a:Lnfs;

    if-nez v0, :cond_3

    .line 25012
    new-instance v0, Lnfs;

    invoke-direct {v0}, Lnfs;-><init>()V

    iput-object v0, p0, Lmip;->a:Lnfs;

    .line 25014
    :cond_3
    iget-object v0, p0, Lmip;->a:Lnfs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24989
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24957
    iget-object v0, p0, Lmip;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 24958
    const/4 v0, 0x1

    iget-object v1, p0, Lmip;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24960
    :cond_0
    iget-object v0, p0, Lmip;->a:Lnfs;

    if-eqz v0, :cond_1

    .line 24961
    const/4 v0, 0x2

    iget-object v1, p0, Lmip;->a:Lnfs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24963
    :cond_1
    iget-object v0, p0, Lmip;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 24965
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 24942
    invoke-virtual {p0, p1}, Lmip;->a(Loxn;)Lmip;

    move-result-object v0

    return-object v0
.end method
