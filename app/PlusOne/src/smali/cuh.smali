.class public final Lcuh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcne;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lctz;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lctz;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcuh;->a:Landroid/app/Activity;

    .line 49
    iput-object p2, p0, Lcuh;->b:Lctz;

    .line 50
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcuh;->b:Lctz;

    invoke-virtual {v0}, Lctz;->c()V

    .line 83
    return-void
.end method

.method public a(Lxn;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public a(Lxn;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    const v0, 0x7f1003a7

    const v1, 0x7f0a0780

    invoke-interface {p2, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lxn;Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 73
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 74
    const v1, 0x7f1003a7

    if-ne v0, v1, :cond_0

    .line 75
    iget-object v0, p0, Lcuh;->a:Landroid/app/Activity;

    const-class v1, Lcoa;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoa;

    invoke-virtual {v0}, Lcoa;->a()V

    .line 77
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lxn;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcuh;->b:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v0

    .line 65
    :goto_0
    if-lez v0, :cond_1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_1
    invoke-virtual {p1, v0}, Lxn;->b(Ljava/lang/CharSequence;)V

    .line 68
    const/4 v0, 0x1

    return v0

    .line 64
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 65
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
