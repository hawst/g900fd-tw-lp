.class public final Lmbd;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnwn;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5020
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5023
    iput-object v0, p0, Lmbd;->apiHeader:Llyr;

    .line 5026
    iput-object v0, p0, Lmbd;->a:Lnwn;

    .line 5020
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5043
    const/4 v0, 0x0

    .line 5044
    iget-object v1, p0, Lmbd;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 5045
    const/4 v0, 0x1

    iget-object v1, p0, Lmbd;->apiHeader:Llyr;

    .line 5046
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5048
    :cond_0
    iget-object v1, p0, Lmbd;->a:Lnwn;

    if-eqz v1, :cond_1

    .line 5049
    const/4 v1, 0x2

    iget-object v2, p0, Lmbd;->a:Lnwn;

    .line 5050
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5052
    :cond_1
    iget-object v1, p0, Lmbd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5053
    iput v0, p0, Lmbd;->ai:I

    .line 5054
    return v0
.end method

.method public a(Loxn;)Lmbd;
    .locals 2

    .prologue
    .line 5062
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5063
    sparse-switch v0, :sswitch_data_0

    .line 5067
    iget-object v1, p0, Lmbd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5068
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbd;->ah:Ljava/util/List;

    .line 5071
    :cond_1
    iget-object v1, p0, Lmbd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5073
    :sswitch_0
    return-object p0

    .line 5078
    :sswitch_1
    iget-object v0, p0, Lmbd;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 5079
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbd;->apiHeader:Llyr;

    .line 5081
    :cond_2
    iget-object v0, p0, Lmbd;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5085
    :sswitch_2
    iget-object v0, p0, Lmbd;->a:Lnwn;

    if-nez v0, :cond_3

    .line 5086
    new-instance v0, Lnwn;

    invoke-direct {v0}, Lnwn;-><init>()V

    iput-object v0, p0, Lmbd;->a:Lnwn;

    .line 5088
    :cond_3
    iget-object v0, p0, Lmbd;->a:Lnwn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5063
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5031
    iget-object v0, p0, Lmbd;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 5032
    const/4 v0, 0x1

    iget-object v1, p0, Lmbd;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5034
    :cond_0
    iget-object v0, p0, Lmbd;->a:Lnwn;

    if-eqz v0, :cond_1

    .line 5035
    const/4 v0, 0x2

    iget-object v1, p0, Lmbd;->a:Lnwn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5037
    :cond_1
    iget-object v0, p0, Lmbd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5039
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5016
    invoke-virtual {p0, p1}, Lmbd;->a(Loxn;)Lmbd;

    move-result-object v0

    return-object v0
.end method
