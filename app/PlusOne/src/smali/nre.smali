.class public final Lnre;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 967
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 993
    const/4 v0, 0x0

    .line 994
    iget-object v1, p0, Lnre;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 995
    const/4 v0, 0x1

    iget-object v1, p0, Lnre;->a:Ljava/lang/String;

    .line 996
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 998
    :cond_0
    iget-object v1, p0, Lnre;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 999
    const/4 v1, 0x2

    iget-object v2, p0, Lnre;->b:Ljava/lang/Integer;

    .line 1000
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1002
    :cond_1
    iget-object v1, p0, Lnre;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1003
    const/4 v1, 0x3

    iget-object v2, p0, Lnre;->c:Ljava/lang/Integer;

    .line 1004
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1006
    :cond_2
    iget-object v1, p0, Lnre;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1007
    iput v0, p0, Lnre;->ai:I

    .line 1008
    return v0
.end method

.method public a(Loxn;)Lnre;
    .locals 2

    .prologue
    .line 1016
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1017
    sparse-switch v0, :sswitch_data_0

    .line 1021
    iget-object v1, p0, Lnre;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1022
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnre;->ah:Ljava/util/List;

    .line 1025
    :cond_1
    iget-object v1, p0, Lnre;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1027
    :sswitch_0
    return-object p0

    .line 1032
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnre;->a:Ljava/lang/String;

    goto :goto_0

    .line 1036
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnre;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1040
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnre;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1017
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 978
    iget-object v0, p0, Lnre;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 979
    const/4 v0, 0x1

    iget-object v1, p0, Lnre;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 981
    :cond_0
    iget-object v0, p0, Lnre;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 982
    const/4 v0, 0x2

    iget-object v1, p0, Lnre;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 984
    :cond_1
    iget-object v0, p0, Lnre;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 985
    const/4 v0, 0x3

    iget-object v1, p0, Lnre;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 987
    :cond_2
    iget-object v0, p0, Lnre;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 989
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 963
    invoke-virtual {p0, p1}, Lnre;->a(Loxn;)Lnre;

    move-result-object v0

    return-object v0
.end method
