.class public final Lomj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32
    const/high16 v0, -0x80000000

    iput v0, p0, Lomj;->g:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 65
    const/4 v0, 0x1

    iget-object v1, p0, Lomj;->a:Ljava/lang/Integer;

    .line 67
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 68
    const/4 v1, 0x2

    iget-object v2, p0, Lomj;->b:Ljava/lang/Long;

    .line 69
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    iget-object v1, p0, Lomj;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 71
    const/4 v1, 0x3

    iget-object v2, p0, Lomj;->c:Ljava/lang/String;

    .line 72
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_0
    iget-object v1, p0, Lomj;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 75
    const/4 v1, 0x4

    iget-object v2, p0, Lomj;->d:Ljava/lang/Boolean;

    .line 76
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 78
    :cond_1
    iget-object v1, p0, Lomj;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 79
    const/4 v1, 0x5

    iget-object v2, p0, Lomj;->f:Ljava/lang/String;

    .line 80
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_2
    iget v1, p0, Lomj;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 83
    const/4 v1, 0x6

    iget v2, p0, Lomj;->g:I

    .line 84
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_3
    iget-object v1, p0, Lomj;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 87
    const/4 v1, 0x7

    iget-object v2, p0, Lomj;->h:Ljava/lang/String;

    .line 88
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_4
    iget-object v1, p0, Lomj;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 91
    const/16 v1, 0x8

    iget-object v2, p0, Lomj;->e:Ljava/lang/Integer;

    .line 92
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_5
    iget-object v1, p0, Lomj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    iput v0, p0, Lomj;->ai:I

    .line 96
    return v0
.end method

.method public a(Loxn;)Lomj;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 104
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 105
    sparse-switch v0, :sswitch_data_0

    .line 109
    iget-object v1, p0, Lomj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lomj;->ah:Ljava/util/List;

    .line 113
    :cond_1
    iget-object v1, p0, Lomj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    :sswitch_0
    return-object p0

    .line 120
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lomj;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 124
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lomj;->b:Ljava/lang/Long;

    goto :goto_0

    .line 128
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lomj;->c:Ljava/lang/String;

    goto :goto_0

    .line 132
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lomj;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 136
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lomj;->f:Ljava/lang/String;

    goto :goto_0

    .line 140
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 141
    if-eq v0, v2, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-ne v0, v1, :cond_3

    .line 145
    :cond_2
    iput v0, p0, Lomj;->g:I

    goto :goto_0

    .line 147
    :cond_3
    iput v2, p0, Lomj;->g:I

    goto :goto_0

    .line 152
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lomj;->h:Ljava/lang/String;

    goto :goto_0

    .line 156
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lomj;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 105
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 39
    const/4 v0, 0x1

    iget-object v1, p0, Lomj;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 40
    const/4 v0, 0x2

    iget-object v1, p0, Lomj;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 41
    iget-object v0, p0, Lomj;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x3

    iget-object v1, p0, Lomj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lomj;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 45
    const/4 v0, 0x4

    iget-object v1, p0, Lomj;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 47
    :cond_1
    iget-object v0, p0, Lomj;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 48
    const/4 v0, 0x5

    iget-object v1, p0, Lomj;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 50
    :cond_2
    iget v0, p0, Lomj;->g:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 51
    const/4 v0, 0x6

    iget v1, p0, Lomj;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 53
    :cond_3
    iget-object v0, p0, Lomj;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 54
    const/4 v0, 0x7

    iget-object v1, p0, Lomj;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 56
    :cond_4
    iget-object v0, p0, Lomj;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 57
    const/16 v0, 0x8

    iget-object v1, p0, Lomj;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 59
    :cond_5
    iget-object v0, p0, Lomj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 61
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lomj;->a(Loxn;)Lomj;

    move-result-object v0

    return-object v0
.end method
