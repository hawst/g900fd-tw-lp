.class public Lcoa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Landroid/content/Context;

.field private c:Lctz;

.field private d:Lhee;

.field private final e:Lhkd;

.field private f:Lhke;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcob;

    invoke-direct {v0, p0}, Lcob;-><init>(Lcoa;)V

    iput-object v0, p0, Lcoa;->e:Lhkd;

    .line 65
    iput-object p1, p0, Lcoa;->a:Landroid/app/Activity;

    .line 67
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 68
    return-void
.end method

.method static synthetic a(Lcoa;)Lctz;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcoa;->c:Lctz;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcoa;->a:Landroid/app/Activity;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lcoa;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->dP:Lhmv;

    .line 82
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 80
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 84
    iget-object v0, p0, Lcoa;->c:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    .line 85
    const-class v1, Ljuf;

    invoke-virtual {v0, v1}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcoa;->d:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 87
    iget-object v2, p0, Lcoa;->a:Landroid/app/Activity;

    invoke-static {v2, v1, v0}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcoa;->f:Lhke;

    const v2, 0x7f1000c4

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 89
    iget-object v0, p0, Lcoa;->a:Landroid/app/Activity;

    const v1, 0x7f05001e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 90
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 72
    iput-object p1, p0, Lcoa;->b:Landroid/content/Context;

    .line 73
    const-class v0, Lctz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcoa;->c:Lctz;

    .line 74
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcoa;->d:Lhee;

    .line 75
    const-class v0, Lhke;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhke;

    iput-object v0, p0, Lcoa;->f:Lhke;

    .line 76
    iget-object v0, p0, Lcoa;->f:Lhke;

    const v1, 0x7f1000c4

    iget-object v2, p0, Lcoa;->e:Lhkd;

    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    .line 77
    return-void
.end method
