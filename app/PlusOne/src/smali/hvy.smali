.class public final Lhvy;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lhmm;
.implements Lljh;


# instance fields
.field private final a:Lhve;

.field private b:[Ljava/lang/String;

.field private c:[Lhwa;

.field private d:Z

.field private e:Landroid/graphics/Rect;

.field private f:I

.field private g:I

.field private h:J

.field private i:F

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:Lhmk;

.field private o:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 167
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lhvy;->c:[Lhwa;

    .line 92
    const/4 v0, -0x1

    iput v0, p0, Lhvy;->g:I

    .line 94
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lhvy;->i:F

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvy;->l:Z

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvy;->m:Z

    .line 107
    new-instance v0, Lhvz;

    invoke-direct {v0, p0}, Lhvz;-><init>(Lhvy;)V

    iput-object v0, p0, Lhvy;->o:Ljava/lang/Runnable;

    .line 182
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhvy;->e:Landroid/graphics/Rect;

    .line 183
    invoke-virtual {p0}, Lhvy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhve;->a(Landroid/content/Context;)Lhve;

    move-result-object v0

    iput-object v0, p0, Lhvy;->a:Lhve;

    .line 185
    new-instance v0, Lhmk;

    sget-object v1, Lonm;->g:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    iput-object v0, p0, Lhvy;->n:Lhmk;

    .line 168
    return-void
.end method

.method static synthetic a(Lhvy;F)F
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lhvy;->i:F

    return p1
.end method

.method static synthetic a(Lhvy;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lhvy;->g:I

    return p1
.end method

.method static synthetic a(Lhvy;J)J
    .locals 1

    .prologue
    .line 37
    iput-wide p1, p0, Lhvy;->h:J

    return-wide p1
.end method

.method static synthetic a(Lhvy;)[Lhwa;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lhvy;->c:[Lhwa;

    return-object v0
.end method

.method static synthetic b(Lhvy;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lhvy;->g:I

    return v0
.end method

.method static synthetic b(Lhvy;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lhvy;->k:I

    return p1
.end method

.method private b()V
    .locals 4

    .prologue
    .line 529
    iget-boolean v0, p0, Lhvy;->m:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhvy;->l:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lhvy;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 530
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    const/4 v0, 0x0

    iput v0, p0, Lhvy;->g:I

    .line 532
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvy;->o:Ljava/lang/Runnable;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 534
    :cond_0
    return-void
.end method

.method static synthetic c(Lhvy;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lhvy;->j:I

    return p1
.end method

.method static synthetic c(Lhvy;)J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lhvy;->h:J

    return-wide v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 537
    iget v0, p0, Lhvy;->g:I

    if-eq v0, v2, :cond_0

    .line 538
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvy;->o:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 539
    iput v2, p0, Lhvy;->g:I

    .line 541
    :cond_0
    return-void
.end method

.method static synthetic d(Lhvy;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lhvy;->j:I

    return v0
.end method

.method static synthetic e(Lhvy;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lhvy;->k:I

    return v0
.end method

.method static synthetic f(Lhvy;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lhvy;->f:I

    return v0
.end method

.method static synthetic g(Lhvy;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lhvy;->l:Z

    return v0
.end method

.method static synthetic h(Lhvy;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lhvy;->o:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method a(II)I
    .locals 1

    .prologue
    .line 340
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 350
    invoke-virtual {p0}, Lhvy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 351
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    return v0

    .line 342
    :sswitch_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    goto :goto_0

    .line 345
    :sswitch_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 340
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 499
    invoke-direct {p0}, Lhvy;->c()V

    .line 501
    iput-object v0, p0, Lhvy;->b:[Ljava/lang/String;

    .line 502
    iput-object v0, p0, Lhvy;->c:[Lhwa;

    .line 503
    return-void
.end method

.method public a(Lhut;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 202
    invoke-direct {p0}, Lhvy;->c()V

    iput-object v1, p0, Lhvy;->b:[Ljava/lang/String;

    iput-object v1, p0, Lhvy;->c:[Lhwa;

    iget-object v1, p0, Lhvy;->e:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    iput v0, p0, Lhvy;->f:I

    const/4 v1, -0x1

    iput v1, p0, Lhvy;->g:I

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lhvy;->h:J

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lhvy;->i:F

    iput v0, p0, Lhvy;->j:I

    iput v0, p0, Lhvy;->k:I

    invoke-virtual {p0, v0}, Lhvy;->a(Z)V

    iput-boolean v0, p0, Lhvy;->d:Z

    .line 204
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lhut;->a()I

    move-result v1

    if-gtz v1, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    iput-boolean v4, p0, Lhvy;->d:Z

    .line 209
    invoke-virtual {p1}, Lhut;->a()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lhvy;->b:[Ljava/lang/String;

    .line 211
    :goto_1
    invoke-virtual {p1}, Lhut;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 212
    iget-object v1, p0, Lhvy;->b:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Lhut;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 215
    :cond_2
    invoke-virtual {p0, v4}, Lhvy;->a(Z)V

    .line 217
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 517
    iget-boolean v0, p0, Lhvy;->m:Z

    if-eq v0, p1, :cond_0

    .line 518
    iput-boolean p1, p0, Lhvy;->m:Z

    .line 520
    iget-boolean v0, p0, Lhvy;->m:Z

    if-eqz v0, :cond_1

    .line 521
    invoke-direct {p0}, Lhvy;->b()V

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 523
    :cond_1
    invoke-direct {p0}, Lhvy;->c()V

    goto :goto_0
.end method

.method public ac_()Lhmk;
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lhvy;->n:Lhmk;

    return-object v0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 507
    invoke-virtual {p0}, Lhvy;->invalidate()V

    .line 508
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 509
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 6
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 226
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    .line 227
    iget-object v0, p0, Lhvy;->b:[Ljava/lang/String;

    array-length v3, v0

    move v0, v1

    .line 228
    :goto_0
    if-ge v0, v3, :cond_0

    .line 229
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/CharSequence;

    iget-object v5, p0, Lhvy;->b:[Ljava/lang/String;

    aget-object v5, v5, v0

    aput-object v5, v4, v1

    invoke-static {v2, v4}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 228
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 231
    :cond_0
    invoke-static {v2}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 239
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 240
    invoke-direct {p0}, Lhvy;->b()V

    .line 241
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 248
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 249
    invoke-direct {p0}, Lhvy;->c()V

    .line 250
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/high16 v8, 0x437f0000    # 255.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 396
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 398
    iget-boolean v0, p0, Lhvy;->d:Z

    if-nez v0, :cond_1

    .line 469
    :cond_0
    return-void

    .line 403
    :cond_1
    invoke-virtual {p0}, Lhvy;->isFocused()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lhvy;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 404
    :cond_2
    iget-object v0, p0, Lhvy;->a:Lhve;

    iget-object v0, v0, Lhve;->p:Landroid/graphics/drawable/NinePatchDrawable;

    .line 409
    :goto_0
    iget-object v2, p0, Lhvy;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 410
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 413
    invoke-virtual {p0}, Lhvy;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v2, 0xa

    invoke-static {v0, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    .line 415
    iget v0, p0, Lhvy;->g:I

    if-ne v0, v10, :cond_6

    .line 416
    iget v0, p0, Lhvy;->k:I

    iget v3, p0, Lhvy;->j:I

    if-ge v0, v3, :cond_5

    .line 417
    iget v0, p0, Lhvy;->i:F

    iget-object v3, p0, Lhvy;->a:Lhve;

    iget v3, v3, Lhve;->h:I

    neg-int v3, v3

    int-to-float v3, v3

    .line 418
    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    add-float/2addr v3, v4

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 428
    :goto_1
    iget-object v3, p0, Lhvy;->c:[Lhwa;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lhvy;->c:[Lhwa;

    array-length v3, v3

    if-lez v3, :cond_0

    .line 429
    invoke-virtual {v2}, Landroid/text/TextPaint;->getAlpha()I

    move-result v3

    .line 431
    iget v4, p0, Lhvy;->i:F

    mul-float/2addr v4, v8

    float-to-int v4, v4

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 432
    iget-object v4, p0, Lhvy;->c:[Lhwa;

    iget v5, p0, Lhvy;->j:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lhwa;->a:Ljava/lang/String;

    iget-object v5, p0, Lhvy;->a:Lhve;

    iget v5, v5, Lhve;->g:I

    int-to-float v5, v5

    iget-object v6, p0, Lhvy;->a:Lhve;

    iget v6, v6, Lhve;->h:I

    int-to-float v6, v6

    .line 433
    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v7

    sub-float/2addr v6, v7

    .line 432
    invoke-virtual {p1, v4, v5, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 436
    iget v4, p0, Lhvy;->g:I

    if-ne v4, v10, :cond_3

    .line 437
    iget v4, p0, Lhvy;->i:F

    mul-float/2addr v4, v8

    float-to-int v4, v4

    rsub-int v4, v4, 0xff

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 438
    iget-object v4, p0, Lhvy;->c:[Lhwa;

    iget v5, p0, Lhvy;->k:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lhwa;->a:Ljava/lang/String;

    iget-object v5, p0, Lhvy;->a:Lhve;

    iget v5, v5, Lhve;->g:I

    int-to-float v5, v5

    iget-object v6, p0, Lhvy;->a:Lhve;

    iget v6, v6, Lhve;->h:I

    int-to-float v6, v6

    .line 440
    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v7

    sub-float/2addr v6, v7

    int-to-float v0, v0

    add-float/2addr v0, v6

    .line 438
    invoke-virtual {p1, v4, v5, v0, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 443
    :cond_3
    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 445
    iget-object v0, p0, Lhvy;->c:[Lhwa;

    aget-object v0, v0, v1

    iget-object v0, v0, Lhwa;->c:Landroid/graphics/Rect;

    .line 446
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 447
    iget v0, v0, Landroid/graphics/Rect;->right:I

    .line 449
    :goto_2
    iget-object v3, p0, Lhvy;->c:[Lhwa;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 450
    iget-object v3, p0, Lhvy;->a:Lhve;

    iget-object v3, v3, Lhve;->i:Landroid/graphics/Rect;

    .line 451
    iput v2, v3, Landroid/graphics/Rect;->left:I

    .line 452
    iput v0, v3, Landroid/graphics/Rect;->right:I

    .line 454
    iget-object v4, p0, Lhvy;->c:[Lhwa;

    aget-object v4, v4, v1

    iget-object v4, v4, Lhwa;->c:Landroid/graphics/Rect;

    .line 455
    iget-object v5, p0, Lhvy;->c:[Lhwa;

    aget-object v5, v5, v1

    iget-object v5, v5, Lhwa;->d:Landroid/graphics/Rect;

    .line 457
    iget v6, v4, Landroid/graphics/Rect;->top:I

    .line 458
    iget v7, v5, Landroid/graphics/Rect;->top:I

    .line 459
    iget v8, p0, Lhvy;->i:F

    int-to-float v6, v6

    mul-float/2addr v6, v8

    iget v8, p0, Lhvy;->i:F

    sub-float v8, v9, v8

    int-to-float v7, v7

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v3, Landroid/graphics/Rect;->top:I

    .line 462
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    .line 463
    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    .line 464
    iget v6, p0, Lhvy;->i:F

    int-to-float v4, v4

    mul-float/2addr v4, v6

    iget v6, p0, Lhvy;->i:F

    sub-float v6, v9, v6

    int-to-float v5, v5

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 466
    iget-object v4, p0, Lhvy;->c:[Lhwa;

    aget-object v4, v4, v1

    iget-object v4, v4, Lhwa;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 449
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 406
    :cond_4
    iget-object v0, p0, Lhvy;->a:Lhve;

    iget-object v0, v0, Lhve;->o:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_0

    .line 420
    :cond_5
    iget v0, p0, Lhvy;->i:F

    iget-object v3, p0, Lhvy;->a:Lhve;

    iget v3, v3, Lhve;->h:I

    int-to-float v3, v3

    .line 421
    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v4

    add-float/2addr v3, v4

    mul-float/2addr v0, v3

    float-to-int v0, v0

    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 424
    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 261
    iget-boolean v0, p0, Lhvy;->d:Z

    if-nez v0, :cond_0

    .line 262
    invoke-virtual {p0, v2, v2}, Lhvy;->setMeasuredDimension(II)V

    .line 331
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lhvy;->b:[Ljava/lang/String;

    array-length v4, v0

    .line 268
    invoke-virtual {p0}, Lhvy;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    .line 270
    const/high16 v0, -0x80000000

    move v1, v0

    move v0, v2

    .line 271
    :goto_1
    if-ge v0, v4, :cond_1

    .line 272
    iget-object v3, p0, Lhvy;->b:[Ljava/lang/String;

    aget-object v3, v3, v0

    .line 273
    invoke-static {v5, v3}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v3

    .line 272
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 271
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 275
    :cond_1
    iget-object v0, p0, Lhvy;->a:Lhve;

    iget v0, v0, Lhve;->g:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 277
    invoke-virtual {p0, p1, v0}, Lhvy;->a(II)I

    move-result v6

    .line 281
    invoke-static {v5}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v0

    iget-object v1, p0, Lhvy;->a:Lhve;

    iget v1, v1, Lhve;->h:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lhvy;->f:I

    .line 285
    new-array v7, v4, [Ljava/lang/String;

    .line 286
    new-array v8, v4, [I

    move v3, v2

    move v1, v2

    .line 288
    :goto_2
    if-ge v3, v4, :cond_4

    .line 289
    iget-object v0, p0, Lhvy;->b:[Ljava/lang/String;

    aget-object v9, v0, v3

    .line 292
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 293
    iget-object v0, p0, Lhvy;->a:Lhve;

    iget v0, v0, Lhve;->g:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v6, v0

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    const/4 v11, 0x0

    invoke-static {v9, v5, v0, v10, v11}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 299
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    int-to-float v9, v9

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v9, v10

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v9, v10

    add-int/lit8 v9, v9, 0x2

    .line 300
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "\u2026"

    invoke-virtual {v0, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 301
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    if-lt v10, v9, :cond_8

    .line 302
    :cond_3
    aput-object v0, v7, v1

    .line 303
    aput v3, v8, v1

    .line 304
    add-int/lit8 v0, v1, 0x1

    .line 288
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_2

    .line 309
    :cond_4
    new-array v0, v1, [Lhwa;

    iput-object v0, p0, Lhvy;->c:[Lhwa;

    move v3, v2

    move v0, v2

    move v5, v2

    .line 311
    :goto_4
    if-ge v3, v1, :cond_6

    .line 312
    new-instance v8, Lhwa;

    invoke-direct {v8}, Lhwa;-><init>()V

    .line 314
    aget-object v4, v7, v3

    iput-object v4, v8, Lhwa;->a:Ljava/lang/String;

    .line 315
    iget-object v4, p0, Lhvy;->a:Lhve;

    iget-object v9, v4, Lhve;->q:[Landroid/graphics/Paint;

    add-int/lit8 v4, v0, 0x1

    aget-object v0, v9, v0

    iput-object v0, v8, Lhwa;->b:Landroid/graphics/Paint;

    .line 317
    new-instance v9, Landroid/graphics/Rect;

    iget-object v0, p0, Lhvy;->a:Lhve;

    iget v0, v0, Lhve;->r:I

    sub-int v10, v6, v0

    if-nez v3, :cond_5

    iget v0, p0, Lhvy;->f:I

    :goto_5
    add-int/2addr v0, v5

    invoke-direct {v9, v10, v5, v6, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v9, v8, Lhwa;->c:Landroid/graphics/Rect;

    .line 319
    new-instance v0, Landroid/graphics/Rect;

    iget-object v5, v8, Lhwa;->c:Landroid/graphics/Rect;

    invoke-direct {v0, v5}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, v8, Lhwa;->d:Landroid/graphics/Rect;

    .line 320
    iget-object v0, v8, Lhwa;->c:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 323
    iget-object v0, p0, Lhvy;->c:[Lhwa;

    aput-object v8, v0, v3

    .line 311
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v4

    goto :goto_4

    .line 317
    :cond_5
    iget v0, p0, Lhvy;->f:I

    div-int/lit8 v0, v0, 0x4

    goto :goto_5

    .line 326
    :cond_6
    iget-object v0, p0, Lhvy;->e:Landroid/graphics/Rect;

    iget v3, p0, Lhvy;->f:I

    invoke-virtual {v0, v2, v2, v6, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 327
    iget-object v0, p0, Lhvy;->c:[Lhwa;

    add-int/lit8 v3, v1, -0x1

    aget-object v0, v0, v3

    iget-object v0, v0, Lhwa;->c:Landroid/graphics/Rect;

    .line 328
    if-lez v1, :cond_7

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 330
    :goto_6
    invoke-virtual {p0, v6, v0}, Lhvy;->setMeasuredDimension(II)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 328
    goto :goto_6

    :cond_8
    move v0, v1

    goto :goto_3
.end method
