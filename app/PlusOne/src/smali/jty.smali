.class public final Ljty;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Llza;",
        "Llzb;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 35
    new-instance v2, Lkfo;

    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    .line 36
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "copyphotosbyshare"

    new-instance v4, Llza;

    invoke-direct {v4}, Llza;-><init>()V

    new-instance v5, Llzb;

    invoke-direct {v5}, Llzb;-><init>()V

    move-object v0, p0

    move-object v1, p1

    .line 35
    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 41
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v7

    :goto_0
    const-string v1, "Either albumId or albumTitle must be non-empty."

    invoke-static {v0, v1}, Llsk;->a(ZLjava/lang/Object;)V

    .line 43
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v7

    :goto_1
    const-string v1, "Cannot specify both albumId and albumTitle."

    invoke-static {v0, v1}, Llsk;->a(ZLjava/lang/Object;)V

    .line 45
    const-string v0, "photoIds cannot be null."

    invoke-static {p5, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-interface {p5}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_2
    const-string v0, "photoIds cannot be empty."

    invoke-static {v7, v0}, Llsk;->a(ZLjava/lang/Object;)V

    .line 48
    iput-object p3, p0, Ljty;->c:Ljava/lang/String;

    .line 49
    iput-object p4, p0, Ljty;->a:Ljava/lang/String;

    .line 50
    iput-object p5, p0, Ljty;->b:Ljava/util/Collection;

    .line 51
    return-void

    :cond_2
    move v0, v6

    .line 41
    goto :goto_0

    :cond_3
    move v0, v6

    .line 43
    goto :goto_1

    :cond_4
    move v7, v6

    .line 46
    goto :goto_2
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ljty;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected a(Llza;)V
    .locals 3

    .prologue
    .line 55
    new-instance v0, Lnar;

    invoke-direct {v0}, Lnar;-><init>()V

    iput-object v0, p1, Llza;->a:Lnar;

    .line 56
    iget-object v1, p1, Llza;->a:Lnar;

    .line 58
    iget-object v0, p0, Ljty;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Ljty;->a:Ljava/lang/String;

    iput-object v0, v1, Lnar;->c:Ljava/lang/String;

    .line 64
    :goto_0
    iget-object v0, p0, Ljty;->b:Ljava/util/Collection;

    iget-object v2, p0, Ljty;->b:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lnar;->a:[Ljava/lang/String;

    .line 65
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Ljty;->c:Ljava/lang/String;

    iput-object v0, v1, Lnar;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method protected a(Llzb;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Ljty;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p1, Llzb;->a:Lnas;

    iget-object v0, v0, Lnas;->a:Ljava/lang/String;

    iput-object v0, p0, Ljty;->c:Ljava/lang/String;

    .line 73
    :cond_0
    iget-object v0, p1, Llzb;->a:Lnas;

    iget-object v0, v0, Lnas;->b:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljty;->p:Ljava/util/List;

    .line 74
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Llza;

    invoke-virtual {p0, p1}, Ljty;->a(Llza;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Llzb;

    invoke-virtual {p0, p1}, Ljty;->a(Llzb;)V

    return-void
.end method

.method public i()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ljty;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ljty;->c:Ljava/lang/String;

    return-object v0
.end method
