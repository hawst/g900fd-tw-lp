.class public final Lfal;
.super Lhya;
.source "PG"


# static fields
.field private static a:Z

.field private static b:Landroid/graphics/drawable/Drawable;

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:Landroid/graphics/drawable/Drawable;

.field private static e:Landroid/graphics/drawable/Drawable;

.field private static f:Landroid/graphics/drawable/Drawable;

.field private static g:Landroid/graphics/drawable/Drawable;

.field private static h:Landroid/graphics/drawable/Drawable;

.field private static i:Landroid/graphics/drawable/Drawable;


# instance fields
.field private j:Lgu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgu",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lgu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgu",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lgu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgu",
            "<",
            "Lhhh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0, p1}, Lhya;-><init>(Landroid/content/Context;)V

    .line 78
    const/4 v0, 0x2

    :goto_0
    if-ltz v0, :cond_0

    .line 79
    invoke-virtual {p0, v1, v1}, Lfal;->b(ZZ)V

    .line 78
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 82
    :cond_0
    sget-boolean v0, Lfal;->a:Z

    if-nez v0, :cond_2

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 84
    const v0, 0x7f020194

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfal;->b:Landroid/graphics/drawable/Drawable;

    .line 85
    const v0, 0x7f020435

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfal;->c:Landroid/graphics/drawable/Drawable;

    .line 86
    const v0, 0x7f020437

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfal;->e:Landroid/graphics/drawable/Drawable;

    .line 88
    const v0, 0x7f020438

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfal;->f:Landroid/graphics/drawable/Drawable;

    .line 89
    const v0, 0x7f020436

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfal;->g:Landroid/graphics/drawable/Drawable;

    .line 90
    const v0, 0x7f020434

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfal;->h:Landroid/graphics/drawable/Drawable;

    .line 92
    const-class v0, Lgcs;

    .line 93
    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcs;

    .line 94
    if-eqz v0, :cond_1

    .line 95
    invoke-interface {v0}, Lgcs;->e()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfal;->d:Landroid/graphics/drawable/Drawable;

    .line 100
    :cond_1
    invoke-virtual {p0}, Lfal;->as()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0376

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 102
    invoke-virtual {p0}, Lfal;->as()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0377

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 104
    new-instance v3, Ldhi;

    invoke-direct {v3}, Ldhi;-><init>()V

    .line 105
    const v4, 0x7f0a0b3e

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    invoke-virtual {p0}, Lfal;->as()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0x1e

    invoke-static {v4, v5}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    .line 105
    invoke-virtual {v3, v1, v4}, Ldhi;->a(Ljava/lang/String;Landroid/text/TextPaint;)V

    .line 107
    invoke-virtual {v3, v0, v2}, Ldhi;->a(II)V

    .line 109
    invoke-virtual {p0}, Lfal;->as()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    .line 108
    invoke-virtual {v3, v0}, Ldhi;->a(Landroid/graphics/drawable/NinePatchDrawable;)V

    .line 110
    sput-object v3, Lfal;->i:Landroid/graphics/drawable/Drawable;

    .line 112
    const/4 v0, 0x1

    sput-boolean v0, Lfal;->a:Z

    .line 115
    :cond_2
    new-instance v0, Lgu;

    invoke-direct {v0, v6}, Lgu;-><init>(I)V

    iput-object v0, p0, Lfal;->j:Lgu;

    .line 116
    new-instance v0, Lgu;

    invoke-direct {v0, v6}, Lgu;-><init>(I)V

    iput-object v0, p0, Lfal;->l:Lgu;

    .line 117
    new-instance v0, Lgu;

    invoke-direct {v0}, Lgu;-><init>()V

    iput-object v0, p0, Lfal;->m:Lgu;

    .line 118
    iget-object v0, p0, Lfal;->k:Landroid/content/Context;

    const-class v1, Lhhh;

    invoke-static {v0, v1}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhh;

    .line 119
    iget-object v2, p0, Lfal;->m:Lgu;

    invoke-virtual {v0}, Lhhh;->a()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Lgu;->b(ILjava/lang/Object;)V

    goto :goto_1

    .line 121
    :cond_3
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 175
    iget-object v0, p0, Lfal;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 181
    const v1, 0x7f04010e

    invoke-virtual {v0, v1, p5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 184
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 187
    const/16 v1, 0x11

    iput v1, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 188
    const v1, 0x7f100374

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 189
    const/4 v2, 0x2

    :goto_0
    if-ltz v2, :cond_0

    .line 190
    new-instance v4, Lfyq;

    iget-object v5, p0, Lfal;->k:Landroid/content/Context;

    invoke-direct {v4, v5}, Lfyq;-><init>(Landroid/content/Context;)V

    .line 191
    invoke-virtual {v4, v3}, Lfyq;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 189
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 195
    :cond_0
    return-object v0
.end method

.method public a(ILandroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 151
    iget-object v0, p0, Lfal;->l:Lgu;

    invoke-virtual {v0, p1, p2}, Lgu;->b(ILjava/lang/Object;)V

    .line 153
    if-eqz p2, :cond_2

    .line 154
    new-instance v3, Lhym;

    sget-object v0, Lfam;->a:[Ljava/lang/String;

    invoke-direct {v3, v0}, Lhym;-><init>([Ljava/lang/String;)V

    .line 155
    sget-object v0, Lfam;->a:[Ljava/lang/String;

    const/4 v0, 0x3

    new-array v6, v0, [Ljava/lang/Object;

    .line 157
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 158
    :goto_0
    if-ge v5, v7, :cond_1

    .line 159
    const/4 v0, 0x0

    invoke-static {v6, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 160
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    .line 161
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    .line 162
    const/4 v8, 0x2

    add-int/lit8 v0, v5, 0x3

    if-le v0, v7, :cond_0

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v8

    .line 163
    invoke-virtual {v3, v6}, Lhym;->a([Ljava/lang/Object;)V

    .line 164
    add-int/lit8 v0, v4, 0x1

    .line 158
    add-int/lit8 v4, v5, 0x3

    move v5, v4

    move v4, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 162
    goto :goto_1

    :cond_1
    move-object p2, v3

    .line 169
    :cond_2
    invoke-super {p0, p1, p2}, Lhya;->a(ILandroid/database/Cursor;)V

    .line 170
    return-void
.end method

.method public a(ILandroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lfal;->j:Lgu;

    invoke-virtual {v0, p1, p2}, Lgu;->b(ILjava/lang/Object;)V

    .line 125
    return-void
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 202
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 203
    iget-object v0, p0, Lfal;->l:Lgu;

    invoke-virtual {v0, p2}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 204
    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 206
    const v1, 0x7f100374

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 207
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 208
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    sub-int v2, v3, v2

    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v7

    move v6, v4

    .line 209
    :goto_0
    if-ge v6, v7, :cond_0

    .line 210
    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lfyq;

    .line 211
    invoke-virtual {v2, v4}, Lfyq;->setVisibility(I)V

    .line 212
    invoke-virtual {v2, v5}, Lfyq;->setClickable(Z)V

    .line 213
    packed-switch p2, :pswitch_data_0

    .line 231
    :goto_1
    :pswitch_0
    iget-object v3, p0, Lfal;->j:Lgu;

    invoke-virtual {v3, p2}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lfyq;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    invoke-virtual {v2, v5}, Lfyq;->setClickable(Z)V

    .line 233
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 209
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_0

    .line 215
    :pswitch_1
    invoke-virtual {p0, v2, v0}, Lfal;->c(Lfyq;Landroid/database/Cursor;)V

    goto :goto_1

    .line 219
    :pswitch_2
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_1

    goto :goto_1

    .line 221
    :pswitch_3
    invoke-virtual {p0, v2, v0}, Lfal;->a(Lfyq;Landroid/database/Cursor;)V

    goto :goto_1

    .line 225
    :pswitch_4
    invoke-virtual {p0, v2, v0}, Lfal;->b(Lfyq;Landroid/database/Cursor;)V

    goto :goto_1

    .line 237
    :cond_0
    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_1

    .line 238
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gt v0, v8, :cond_2

    :cond_1
    move v0, v5

    .line 239
    :goto_2
    const v1, 0x7f10016d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 240
    invoke-virtual {p0, v5}, Lfal;->a(I)I

    move-result v2

    if-lez v2, :cond_3

    :goto_3
    if-ne p2, v5, :cond_4

    if-eqz v0, :cond_4

    .line 239
    :goto_4
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 241
    return-void

    :cond_2
    move v0, v4

    .line 238
    goto :goto_2

    :cond_3
    move v5, v4

    .line 240
    goto :goto_3

    :cond_4
    const/16 v4, 0x8

    goto :goto_4

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 219
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lfyq;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    const v1, 0x7f1000b6

    const v3, 0x7f1000b7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 244
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 245
    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 248
    if-eqz v0, :cond_0

    .line 249
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lhhg;->a(Ljava/nio/ByteBuffer;)Lhgw;

    move-result-object v0

    .line 250
    invoke-virtual {p1, v1, v0}, Lfyq;->setTag(ILjava/lang/Object;)V

    .line 251
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 252
    packed-switch v0, :pswitch_data_0

    sget-object v0, Lfal;->c:Landroid/graphics/drawable/Drawable;

    .line 277
    :goto_0
    invoke-virtual {p1, v2, v0, v4}, Lfyq;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    .line 278
    return-void

    .line 252
    :pswitch_0
    sget-object v0, Lfal;->f:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lfal;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lfal;->g:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 254
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lfyq;->setTag(ILjava/lang/Object;)V

    .line 255
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 256
    if-ne v0, v6, :cond_1

    .line 258
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 257
    invoke-virtual {p1, v3, v0}, Lfyq;->setTag(ILjava/lang/Object;)V

    .line 259
    sget-object v0, Lfal;->h:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 260
    :cond_1
    iget-object v1, p0, Lfal;->m:Lgu;

    invoke-virtual {v1, v0}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 261
    iget-object v1, p0, Lfal;->m:Lgu;

    invoke-virtual {v1, v0}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhh;

    .line 263
    invoke-virtual {v0}, Lhhh;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 262
    invoke-virtual {p1, v3, v1}, Lfyq;->setTag(ILjava/lang/Object;)V

    .line 264
    iget-object v1, p0, Lfal;->k:Landroid/content/Context;

    invoke-virtual {v0}, Lhhh;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 265
    iget-object v3, p0, Lfal;->k:Landroid/content/Context;

    invoke-virtual {v0}, Lhhh;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    new-array v3, v6, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v3, v4

    sget-object v1, Lfal;->i:Landroid/graphics/drawable/Drawable;

    aput-object v1, v3, v5

    invoke-direct {v0, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 270
    goto :goto_0

    .line 272
    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 271
    invoke-virtual {p1, v3, v0}, Lfyq;->setTag(ILjava/lang/Object;)V

    .line 273
    sget-object v0, Lfal;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 252
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public b(Lfyq;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 281
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lhhg;->a(Ljava/nio/ByteBuffer;)Lhgw;

    move-result-object v1

    .line 283
    const v0, 0x7f1000b6

    invoke-virtual {p1, v0, v1}, Lfyq;->setTag(ILjava/lang/Object;)V

    .line 286
    invoke-virtual {v1}, Lhgw;->h()I

    move-result v0

    if-lez v0, :cond_0

    .line 287
    sget-object v0, Lfal;->c:Landroid/graphics/drawable/Drawable;

    .line 295
    :goto_0
    invoke-virtual {p1, v1, v0}, Lfyq;->a(Lhgw;Landroid/graphics/drawable/Drawable;)V

    .line 296
    return-void

    .line 288
    :cond_0
    invoke-virtual {v1}, Lhgw;->i()I

    move-result v0

    if-lez v0, :cond_1

    .line 289
    sget-object v0, Lfal;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 290
    :cond_1
    invoke-virtual {v1}, Lhgw;->j()I

    move-result v0

    if-lez v0, :cond_2

    .line 291
    sget-object v0, Lfal;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 293
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lfyq;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 299
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 301
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 303
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 304
    array-length v3, v0

    invoke-virtual {v2, v0, v4, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 305
    invoke-virtual {v2, v4}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 307
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 308
    const v3, 0x7f10006c

    invoke-virtual {p1, v3, v0}, Lfyq;->setTag(ILjava/lang/Object;)V

    .line 310
    sget-object v0, Landroid/content/pm/ResolveInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 311
    const v2, 0x7f10006d

    invoke-virtual {p1, v2, v0}, Lfyq;->setTag(ILjava/lang/Object;)V

    .line 313
    iget-object v2, p0, Lfal;->k:Landroid/content/Context;

    .line 314
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 313
    invoke-virtual {v0, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v1, v0, v5}, Lfyq;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    .line 315
    return-void
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return v0
.end method
