.class public final Leox;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Ldsx;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private e:Ldsx;

.field private final f:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Ldsx;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Leox;->f:Ldp;

    .line 41
    iput p2, p0, Leox;->b:I

    .line 42
    iput-object p3, p0, Leox;->c:Ljava/lang/String;

    .line 43
    iput-boolean p4, p0, Leox;->d:Z

    .line 44
    return-void
.end method


# virtual methods
.method protected D_()Z
    .locals 4

    .prologue
    .line 55
    invoke-virtual {p0}, Leox;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->e:Landroid/net/Uri;

    iget-object v2, p0, Leox;->c:Ljava/lang/String;

    .line 56
    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Leox;->f:Ldp;

    .line 55
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ldsx;)V
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Leox;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iput-object p1, p0, Leox;->e:Ldsx;

    .line 84
    invoke-virtual {p0}, Leox;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    invoke-super {p0, p1}, Lhxz;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Ldsx;

    invoke-virtual {p0, p1}, Leox;->a(Ldsx;)V

    return-void
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Leox;->D_()Z

    .line 71
    iget-object v0, p0, Leox;->e:Ldsx;

    if-nez v0, :cond_0

    .line 72
    invoke-virtual {p0}, Leox;->t()V

    .line 74
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lhxz;->i()V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Leox;->e:Ldsx;

    .line 93
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Leox;->l()Ldsx;

    move-result-object v0

    return-object v0
.end method

.method protected k()Z
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Leox;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Leox;->f:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 64
    const/4 v0, 0x1

    return v0
.end method

.method public l()Ldsx;
    .locals 4

    .prologue
    .line 48
    .line 49
    invoke-virtual {p0}, Leox;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Leox;->b:I

    iget-object v2, p0, Leox;->c:Ljava/lang/String;

    iget-boolean v3, p0, Leox;->d:Z

    .line 48
    invoke-static {v0, v1, v2, v3}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Z)Ldsx;

    move-result-object v0

    return-object v0
.end method
