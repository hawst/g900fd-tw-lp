.class public final Lnjb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field private e:Lnja;

.field private f:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1823
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1826
    const/4 v0, 0x0

    iput-object v0, p0, Lnjb;->e:Lnja;

    .line 1823
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 1865
    const/4 v0, 0x0

    .line 1866
    iget-object v1, p0, Lnjb;->e:Lnja;

    if-eqz v1, :cond_0

    .line 1867
    const/4 v0, 0x1

    iget-object v1, p0, Lnjb;->e:Lnja;

    .line 1868
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1870
    :cond_0
    iget-object v1, p0, Lnjb;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1871
    const/4 v1, 0x2

    iget-object v2, p0, Lnjb;->a:Ljava/lang/String;

    .line 1872
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1874
    :cond_1
    iget-object v1, p0, Lnjb;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1875
    const/4 v1, 0x3

    iget-object v2, p0, Lnjb;->b:Ljava/lang/String;

    .line 1876
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1878
    :cond_2
    iget-object v1, p0, Lnjb;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1879
    const/4 v1, 0x4

    iget-object v2, p0, Lnjb;->c:Ljava/lang/String;

    .line 1880
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1882
    :cond_3
    iget-object v1, p0, Lnjb;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1883
    const/4 v1, 0x5

    iget-object v2, p0, Lnjb;->d:Ljava/lang/Boolean;

    .line 1884
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1886
    :cond_4
    iget-object v1, p0, Lnjb;->f:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 1887
    const/4 v1, 0x6

    iget-object v2, p0, Lnjb;->f:Ljava/lang/Long;

    .line 1888
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1890
    :cond_5
    iget-object v1, p0, Lnjb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1891
    iput v0, p0, Lnjb;->ai:I

    .line 1892
    return v0
.end method

.method public a(Loxn;)Lnjb;
    .locals 2

    .prologue
    .line 1900
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1901
    sparse-switch v0, :sswitch_data_0

    .line 1905
    iget-object v1, p0, Lnjb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1906
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjb;->ah:Ljava/util/List;

    .line 1909
    :cond_1
    iget-object v1, p0, Lnjb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1911
    :sswitch_0
    return-object p0

    .line 1916
    :sswitch_1
    iget-object v0, p0, Lnjb;->e:Lnja;

    if-nez v0, :cond_2

    .line 1917
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnjb;->e:Lnja;

    .line 1919
    :cond_2
    iget-object v0, p0, Lnjb;->e:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1923
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnjb;->a:Ljava/lang/String;

    goto :goto_0

    .line 1927
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnjb;->b:Ljava/lang/String;

    goto :goto_0

    .line 1931
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnjb;->c:Ljava/lang/String;

    goto :goto_0

    .line 1935
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnjb;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 1939
    :sswitch_6
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnjb;->f:Ljava/lang/Long;

    goto :goto_0

    .line 1901
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 1841
    iget-object v0, p0, Lnjb;->e:Lnja;

    if-eqz v0, :cond_0

    .line 1842
    const/4 v0, 0x1

    iget-object v1, p0, Lnjb;->e:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1844
    :cond_0
    iget-object v0, p0, Lnjb;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1845
    const/4 v0, 0x2

    iget-object v1, p0, Lnjb;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1847
    :cond_1
    iget-object v0, p0, Lnjb;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1848
    const/4 v0, 0x3

    iget-object v1, p0, Lnjb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1850
    :cond_2
    iget-object v0, p0, Lnjb;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1851
    const/4 v0, 0x4

    iget-object v1, p0, Lnjb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1853
    :cond_3
    iget-object v0, p0, Lnjb;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1854
    const/4 v0, 0x5

    iget-object v1, p0, Lnjb;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1856
    :cond_4
    iget-object v0, p0, Lnjb;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1857
    const/4 v0, 0x6

    iget-object v1, p0, Lnjb;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 1859
    :cond_5
    iget-object v0, p0, Lnjb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1861
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1819
    invoke-virtual {p0, p1}, Lnjb;->a(Loxn;)Lnjb;

    move-result-object v0

    return-object v0
.end method
