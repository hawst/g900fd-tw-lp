.class public final Loto;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lorn;

.field private b:Ljava/lang/Boolean;

.field private c:Lorh;

.field private d:Lori;

.field private e:Lorj;

.field private f:Lorl;

.field private g:Lotn;

.field private h:Lotj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 240
    invoke-direct {p0}, Loxq;-><init>()V

    .line 245
    iput-object v0, p0, Loto;->a:Lorn;

    .line 248
    iput-object v0, p0, Loto;->c:Lorh;

    .line 251
    iput-object v0, p0, Loto;->d:Lori;

    .line 254
    iput-object v0, p0, Loto;->e:Lorj;

    .line 257
    iput-object v0, p0, Loto;->f:Lorl;

    .line 260
    iput-object v0, p0, Loto;->g:Lotn;

    .line 263
    iput-object v0, p0, Loto;->h:Lotj;

    .line 240
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 298
    const/4 v0, 0x0

    .line 299
    iget-object v1, p0, Loto;->a:Lorn;

    if-eqz v1, :cond_0

    .line 300
    const/4 v0, 0x1

    iget-object v1, p0, Loto;->a:Lorn;

    .line 301
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 303
    :cond_0
    iget-object v1, p0, Loto;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 304
    const/4 v1, 0x2

    iget-object v2, p0, Loto;->b:Ljava/lang/Boolean;

    .line 305
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 307
    :cond_1
    iget-object v1, p0, Loto;->c:Lorh;

    if-eqz v1, :cond_2

    .line 308
    const/4 v1, 0x3

    iget-object v2, p0, Loto;->c:Lorh;

    .line 309
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 311
    :cond_2
    iget-object v1, p0, Loto;->d:Lori;

    if-eqz v1, :cond_3

    .line 312
    const/4 v1, 0x4

    iget-object v2, p0, Loto;->d:Lori;

    .line 313
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 315
    :cond_3
    iget-object v1, p0, Loto;->e:Lorj;

    if-eqz v1, :cond_4

    .line 316
    const/4 v1, 0x5

    iget-object v2, p0, Loto;->e:Lorj;

    .line 317
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 319
    :cond_4
    iget-object v1, p0, Loto;->f:Lorl;

    if-eqz v1, :cond_5

    .line 320
    const/4 v1, 0x6

    iget-object v2, p0, Loto;->f:Lorl;

    .line 321
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_5
    iget-object v1, p0, Loto;->g:Lotn;

    if-eqz v1, :cond_6

    .line 324
    const/4 v1, 0x7

    iget-object v2, p0, Loto;->g:Lotn;

    .line 325
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 327
    :cond_6
    iget-object v1, p0, Loto;->h:Lotj;

    if-eqz v1, :cond_7

    .line 328
    const/16 v1, 0xf

    iget-object v2, p0, Loto;->h:Lotj;

    .line 329
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    :cond_7
    iget-object v1, p0, Loto;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 332
    iput v0, p0, Loto;->ai:I

    .line 333
    return v0
.end method

.method public a(Loxn;)Loto;
    .locals 2

    .prologue
    .line 341
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 342
    sparse-switch v0, :sswitch_data_0

    .line 346
    iget-object v1, p0, Loto;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 347
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loto;->ah:Ljava/util/List;

    .line 350
    :cond_1
    iget-object v1, p0, Loto;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 352
    :sswitch_0
    return-object p0

    .line 357
    :sswitch_1
    iget-object v0, p0, Loto;->a:Lorn;

    if-nez v0, :cond_2

    .line 358
    new-instance v0, Lorn;

    invoke-direct {v0}, Lorn;-><init>()V

    iput-object v0, p0, Loto;->a:Lorn;

    .line 360
    :cond_2
    iget-object v0, p0, Loto;->a:Lorn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 364
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loto;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 368
    :sswitch_3
    iget-object v0, p0, Loto;->c:Lorh;

    if-nez v0, :cond_3

    .line 369
    new-instance v0, Lorh;

    invoke-direct {v0}, Lorh;-><init>()V

    iput-object v0, p0, Loto;->c:Lorh;

    .line 371
    :cond_3
    iget-object v0, p0, Loto;->c:Lorh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 375
    :sswitch_4
    iget-object v0, p0, Loto;->d:Lori;

    if-nez v0, :cond_4

    .line 376
    new-instance v0, Lori;

    invoke-direct {v0}, Lori;-><init>()V

    iput-object v0, p0, Loto;->d:Lori;

    .line 378
    :cond_4
    iget-object v0, p0, Loto;->d:Lori;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 382
    :sswitch_5
    iget-object v0, p0, Loto;->e:Lorj;

    if-nez v0, :cond_5

    .line 383
    new-instance v0, Lorj;

    invoke-direct {v0}, Lorj;-><init>()V

    iput-object v0, p0, Loto;->e:Lorj;

    .line 385
    :cond_5
    iget-object v0, p0, Loto;->e:Lorj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 389
    :sswitch_6
    iget-object v0, p0, Loto;->f:Lorl;

    if-nez v0, :cond_6

    .line 390
    new-instance v0, Lorl;

    invoke-direct {v0}, Lorl;-><init>()V

    iput-object v0, p0, Loto;->f:Lorl;

    .line 392
    :cond_6
    iget-object v0, p0, Loto;->f:Lorl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 396
    :sswitch_7
    iget-object v0, p0, Loto;->g:Lotn;

    if-nez v0, :cond_7

    .line 397
    new-instance v0, Lotn;

    invoke-direct {v0}, Lotn;-><init>()V

    iput-object v0, p0, Loto;->g:Lotn;

    .line 399
    :cond_7
    iget-object v0, p0, Loto;->g:Lotn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 403
    :sswitch_8
    iget-object v0, p0, Loto;->h:Lotj;

    if-nez v0, :cond_8

    .line 404
    new-instance v0, Lotj;

    invoke-direct {v0}, Lotj;-><init>()V

    iput-object v0, p0, Loto;->h:Lotj;

    .line 406
    :cond_8
    iget-object v0, p0, Loto;->h:Lotj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 342
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x7a -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Loto;->a:Lorn;

    if-eqz v0, :cond_0

    .line 269
    const/4 v0, 0x1

    iget-object v1, p0, Loto;->a:Lorn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 271
    :cond_0
    iget-object v0, p0, Loto;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 272
    const/4 v0, 0x2

    iget-object v1, p0, Loto;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 274
    :cond_1
    iget-object v0, p0, Loto;->c:Lorh;

    if-eqz v0, :cond_2

    .line 275
    const/4 v0, 0x3

    iget-object v1, p0, Loto;->c:Lorh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 277
    :cond_2
    iget-object v0, p0, Loto;->d:Lori;

    if-eqz v0, :cond_3

    .line 278
    const/4 v0, 0x4

    iget-object v1, p0, Loto;->d:Lori;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 280
    :cond_3
    iget-object v0, p0, Loto;->e:Lorj;

    if-eqz v0, :cond_4

    .line 281
    const/4 v0, 0x5

    iget-object v1, p0, Loto;->e:Lorj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 283
    :cond_4
    iget-object v0, p0, Loto;->f:Lorl;

    if-eqz v0, :cond_5

    .line 284
    const/4 v0, 0x6

    iget-object v1, p0, Loto;->f:Lorl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 286
    :cond_5
    iget-object v0, p0, Loto;->g:Lotn;

    if-eqz v0, :cond_6

    .line 287
    const/4 v0, 0x7

    iget-object v1, p0, Loto;->g:Lotn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 289
    :cond_6
    iget-object v0, p0, Loto;->h:Lotj;

    if-eqz v0, :cond_7

    .line 290
    const/16 v0, 0xf

    iget-object v1, p0, Loto;->h:Lotj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 292
    :cond_7
    iget-object v0, p0, Loto;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 294
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 236
    invoke-virtual {p0, p1}, Loto;->a(Loxn;)Loto;

    move-result-object v0

    return-object v0
.end method
