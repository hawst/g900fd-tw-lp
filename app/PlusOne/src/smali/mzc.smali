.class public final Lmzc;
.super Loxq;
.source "PG"


# instance fields
.field private a:[B

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 342
    const/4 v0, 0x0

    .line 343
    iget-object v1, p0, Lmzc;->a:[B

    if-eqz v1, :cond_0

    .line 344
    const/4 v0, 0x1

    iget-object v1, p0, Lmzc;->a:[B

    .line 345
    invoke-static {v0, v1}, Loxo;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 347
    :cond_0
    iget-object v1, p0, Lmzc;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 348
    const/4 v1, 0x2

    iget-object v2, p0, Lmzc;->b:Ljava/lang/String;

    .line 349
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :cond_1
    iget-object v1, p0, Lmzc;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 352
    const/4 v1, 0x3

    iget-object v2, p0, Lmzc;->c:Ljava/lang/String;

    .line 353
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    :cond_2
    iget-object v1, p0, Lmzc;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 356
    const/4 v1, 0x4

    iget-object v2, p0, Lmzc;->d:Ljava/lang/String;

    .line 357
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_3
    iget-object v1, p0, Lmzc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 360
    iput v0, p0, Lmzc;->ai:I

    .line 361
    return v0
.end method

.method public a(Loxn;)Lmzc;
    .locals 2

    .prologue
    .line 369
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 370
    sparse-switch v0, :sswitch_data_0

    .line 374
    iget-object v1, p0, Lmzc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 375
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmzc;->ah:Ljava/util/List;

    .line 378
    :cond_1
    iget-object v1, p0, Lmzc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    :sswitch_0
    return-object p0

    .line 385
    :sswitch_1
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lmzc;->a:[B

    goto :goto_0

    .line 389
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmzc;->b:Ljava/lang/String;

    goto :goto_0

    .line 393
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmzc;->c:Ljava/lang/String;

    goto :goto_0

    .line 397
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmzc;->d:Ljava/lang/String;

    goto :goto_0

    .line 370
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lmzc;->a:[B

    if-eqz v0, :cond_0

    .line 325
    const/4 v0, 0x1

    iget-object v1, p0, Lmzc;->a:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 327
    :cond_0
    iget-object v0, p0, Lmzc;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 328
    const/4 v0, 0x2

    iget-object v1, p0, Lmzc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 330
    :cond_1
    iget-object v0, p0, Lmzc;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 331
    const/4 v0, 0x3

    iget-object v1, p0, Lmzc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 333
    :cond_2
    iget-object v0, p0, Lmzc;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 334
    const/4 v0, 0x4

    iget-object v1, p0, Lmzc;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 336
    :cond_3
    iget-object v0, p0, Lmzc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 338
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 307
    invoke-virtual {p0, p1}, Lmzc;->a(Loxn;)Lmzc;

    move-result-object v0

    return-object v0
.end method
