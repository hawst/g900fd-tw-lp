.class public final Llwz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llwz;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4724
    const/4 v0, 0x0

    new-array v0, v0, [Llwz;

    sput-object v0, Llwz;->a:[Llwz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4725
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4728
    const/4 v0, 0x0

    iput-object v0, p0, Llwz;->b:Llxp;

    .line 4725
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4767
    const/4 v0, 0x0

    .line 4768
    iget-object v1, p0, Llwz;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4769
    const/4 v0, 0x1

    iget-object v1, p0, Llwz;->c:Ljava/lang/String;

    .line 4770
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4772
    :cond_0
    iget-object v1, p0, Llwz;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4773
    const/4 v1, 0x2

    iget-object v2, p0, Llwz;->d:Ljava/lang/String;

    .line 4774
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4776
    :cond_1
    iget-object v1, p0, Llwz;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 4777
    const/4 v1, 0x3

    iget-object v2, p0, Llwz;->e:Ljava/lang/Integer;

    .line 4778
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4780
    :cond_2
    iget-object v1, p0, Llwz;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 4781
    const/4 v1, 0x4

    iget-object v2, p0, Llwz;->f:Ljava/lang/Integer;

    .line 4782
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4784
    :cond_3
    iget-object v1, p0, Llwz;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 4785
    const/4 v1, 0x5

    iget-object v2, p0, Llwz;->g:Ljava/lang/Boolean;

    .line 4786
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4788
    :cond_4
    iget-object v1, p0, Llwz;->b:Llxp;

    if-eqz v1, :cond_5

    .line 4789
    const/4 v1, 0x6

    iget-object v2, p0, Llwz;->b:Llxp;

    .line 4790
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4792
    :cond_5
    iget-object v1, p0, Llwz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4793
    iput v0, p0, Llwz;->ai:I

    .line 4794
    return v0
.end method

.method public a(Loxn;)Llwz;
    .locals 2

    .prologue
    .line 4802
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4803
    sparse-switch v0, :sswitch_data_0

    .line 4807
    iget-object v1, p0, Llwz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4808
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llwz;->ah:Ljava/util/List;

    .line 4811
    :cond_1
    iget-object v1, p0, Llwz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4813
    :sswitch_0
    return-object p0

    .line 4818
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llwz;->c:Ljava/lang/String;

    goto :goto_0

    .line 4822
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llwz;->d:Ljava/lang/String;

    goto :goto_0

    .line 4826
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Llwz;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 4830
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Llwz;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 4834
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llwz;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 4838
    :sswitch_6
    iget-object v0, p0, Llwz;->b:Llxp;

    if-nez v0, :cond_2

    .line 4839
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llwz;->b:Llxp;

    .line 4841
    :cond_2
    iget-object v0, p0, Llwz;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4803
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4743
    iget-object v0, p0, Llwz;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4744
    const/4 v0, 0x1

    iget-object v1, p0, Llwz;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4746
    :cond_0
    iget-object v0, p0, Llwz;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4747
    const/4 v0, 0x2

    iget-object v1, p0, Llwz;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4749
    :cond_1
    iget-object v0, p0, Llwz;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 4750
    const/4 v0, 0x3

    iget-object v1, p0, Llwz;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4752
    :cond_2
    iget-object v0, p0, Llwz;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 4753
    const/4 v0, 0x4

    iget-object v1, p0, Llwz;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4755
    :cond_3
    iget-object v0, p0, Llwz;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 4756
    const/4 v0, 0x5

    iget-object v1, p0, Llwz;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 4758
    :cond_4
    iget-object v0, p0, Llwz;->b:Llxp;

    if-eqz v0, :cond_5

    .line 4759
    const/4 v0, 0x6

    iget-object v1, p0, Llwz;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4761
    :cond_5
    iget-object v0, p0, Llwz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4763
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4721
    invoke-virtual {p0, p1}, Llwz;->a(Loxn;)Llwz;

    move-result-object v0

    return-object v0
.end method
