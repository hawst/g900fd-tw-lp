.class public final Lens;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Ldsx;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lenl;


# direct methods
.method protected constructor <init>(Lenl;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lens;->a:Lenl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Ldsx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lens;->a:Lenl;

    iget-object v0, v0, Lenl;->am:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-static {v0}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    new-instance v1, Leox;

    iget-object v2, p0, Lens;->a:Lenl;

    invoke-static {v2}, Lenl;->a(Lenl;)Llnl;

    move-result-object v2

    iget-object v3, p0, Lens;->a:Lenl;

    iget-object v3, v3, Lenl;->am:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v0, v4}, Leox;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    return-object v1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ldsx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 167
    check-cast p2, Ldsx;

    invoke-virtual {p0, p2}, Lens;->a(Ldsx;)V

    return-void
.end method

.method public a(Ldsx;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldsx;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 180
    iget-object v0, p0, Lens;->a:Lenl;

    iget-boolean v0, v0, Lenl;->V:Z

    if-nez v0, :cond_0

    .line 181
    iget-object v0, p0, Lens;->a:Lenl;

    invoke-virtual {v0}, Lenl;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lens;->a:Lenl;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 184
    :cond_0
    iget-object v0, p0, Lens;->a:Lenl;

    iput-boolean v4, v0, Lenl;->ab:Z

    .line 185
    iget-object v0, p0, Lens;->a:Lenl;

    invoke-virtual {v0}, Lenl;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbb;->a(I)V

    .line 187
    iget-object v0, p1, Ldsx;->h:Lnjt;

    if-eqz v0, :cond_1

    .line 188
    iget-object v0, p0, Lens;->a:Lenl;

    iget-object v1, p1, Ldsx;->h:Lnjt;

    iget-object v1, v1, Lnjt;->h:Lnjs;

    iput-object v1, v0, Lenl;->W:Lnjs;

    .line 189
    iget-object v0, p0, Lens;->a:Lenl;

    invoke-virtual {v0, p1}, Lenl;->a(Ldsx;)V

    .line 190
    iget-object v0, p0, Lens;->a:Lenl;

    invoke-virtual {v0}, Lenl;->e()V

    .line 191
    iget-object v0, p0, Lens;->a:Lenl;

    invoke-virtual {v0}, Lenl;->U()V

    .line 194
    :cond_1
    iget-object v0, p0, Lens;->a:Lenl;

    iget-object v1, p0, Lens;->a:Lenl;

    invoke-virtual {v1}, Lenl;->x()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lenl;->i(Landroid/view/View;)V

    .line 195
    return-void
.end method
