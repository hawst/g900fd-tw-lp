.class final enum Lddq;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lddq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lddq;

.field public static final enum b:Lddq;

.field public static final enum c:Lddq;

.field private static final synthetic d:[Lddq;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    new-instance v0, Lddq;

    const-string v1, "Local"

    invoke-direct {v0, v1, v2}, Lddq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddq;->a:Lddq;

    new-instance v0, Lddq;

    const-string v1, "Remote"

    invoke-direct {v0, v1, v3}, Lddq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddq;->b:Lddq;

    new-instance v0, Lddq;

    const-string v1, "Combined"

    invoke-direct {v0, v1, v4}, Lddq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lddq;->c:Lddq;

    .line 94
    const/4 v0, 0x3

    new-array v0, v0, [Lddq;

    sget-object v1, Lddq;->a:Lddq;

    aput-object v1, v0, v2

    sget-object v1, Lddq;->b:Lddq;

    aput-object v1, v0, v3

    sget-object v1, Lddq;->c:Lddq;

    aput-object v1, v0, v4

    sput-object v0, Lddq;->d:[Lddq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Landroid/os/Bundle;Lizu;)Lddq;
    .locals 2

    .prologue
    .line 98
    const-string v0, "all_photos_row_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 99
    invoke-virtual {p1}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lizu;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    :cond_0
    sget-object v0, Lddq;->c:Lddq;

    .line 106
    :goto_0
    return-object v0

    .line 102
    :cond_1
    invoke-virtual {p1}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    sget-object v0, Lddq;->a:Lddq;

    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {p1}, Lizu;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 106
    sget-object v0, Lddq;->b:Lddq;

    goto :goto_0

    .line 109
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No suitable ProxyType for this builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lddq;
    .locals 1

    .prologue
    .line 94
    const-class v0, Lddq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lddq;

    return-object v0
.end method

.method public static values()[Lddq;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lddq;->d:[Lddq;

    invoke-virtual {v0}, [Lddq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lddq;

    return-object v0
.end method
