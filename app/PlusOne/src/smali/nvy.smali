.class public final Lnvy;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lofv;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 277
    invoke-direct {p0}, Loxq;-><init>()V

    .line 282
    sget-object v0, Lofv;->a:[Lofv;

    iput-object v0, p0, Lnvy;->a:[Lofv;

    .line 277
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    .line 316
    const/4 v0, 0x1

    iget-object v1, p0, Lnvy;->d:Ljava/lang/String;

    .line 318
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 319
    iget-object v1, p0, Lnvy;->a:[Lofv;

    if-eqz v1, :cond_1

    .line 320
    iget-object v2, p0, Lnvy;->a:[Lofv;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 321
    if-eqz v4, :cond_0

    .line 322
    const/4 v5, 0x2

    .line 323
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 320
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 327
    :cond_1
    iget-object v1, p0, Lnvy;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 328
    const/4 v1, 0x3

    iget-object v2, p0, Lnvy;->e:Ljava/lang/String;

    .line 329
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    :cond_2
    iget-object v1, p0, Lnvy;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 332
    const/4 v1, 0x4

    iget-object v2, p0, Lnvy;->b:Ljava/lang/Integer;

    .line 333
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 335
    :cond_3
    iget-object v1, p0, Lnvy;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 336
    const/4 v1, 0x5

    iget-object v2, p0, Lnvy;->c:Ljava/lang/Integer;

    .line 337
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 339
    :cond_4
    iget-object v1, p0, Lnvy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 340
    iput v0, p0, Lnvy;->ai:I

    .line 341
    return v0
.end method

.method public a(Loxn;)Lnvy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 349
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 350
    sparse-switch v0, :sswitch_data_0

    .line 354
    iget-object v2, p0, Lnvy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 355
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnvy;->ah:Ljava/util/List;

    .line 358
    :cond_1
    iget-object v2, p0, Lnvy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    :sswitch_0
    return-object p0

    .line 365
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvy;->d:Ljava/lang/String;

    goto :goto_0

    .line 369
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 370
    iget-object v0, p0, Lnvy;->a:[Lofv;

    if-nez v0, :cond_3

    move v0, v1

    .line 371
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lofv;

    .line 372
    iget-object v3, p0, Lnvy;->a:[Lofv;

    if-eqz v3, :cond_2

    .line 373
    iget-object v3, p0, Lnvy;->a:[Lofv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 375
    :cond_2
    iput-object v2, p0, Lnvy;->a:[Lofv;

    .line 376
    :goto_2
    iget-object v2, p0, Lnvy;->a:[Lofv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 377
    iget-object v2, p0, Lnvy;->a:[Lofv;

    new-instance v3, Lofv;

    invoke-direct {v3}, Lofv;-><init>()V

    aput-object v3, v2, v0

    .line 378
    iget-object v2, p0, Lnvy;->a:[Lofv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 379
    invoke-virtual {p1}, Loxn;->a()I

    .line 376
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 370
    :cond_3
    iget-object v0, p0, Lnvy;->a:[Lofv;

    array-length v0, v0

    goto :goto_1

    .line 382
    :cond_4
    iget-object v2, p0, Lnvy;->a:[Lofv;

    new-instance v3, Lofv;

    invoke-direct {v3}, Lofv;-><init>()V

    aput-object v3, v2, v0

    .line 383
    iget-object v2, p0, Lnvy;->a:[Lofv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 387
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvy;->e:Ljava/lang/String;

    goto :goto_0

    .line 391
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnvy;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 395
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnvy;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 350
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 293
    const/4 v0, 0x1

    iget-object v1, p0, Lnvy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 294
    iget-object v0, p0, Lnvy;->a:[Lofv;

    if-eqz v0, :cond_1

    .line 295
    iget-object v1, p0, Lnvy;->a:[Lofv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 296
    if-eqz v3, :cond_0

    .line 297
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 295
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_1
    iget-object v0, p0, Lnvy;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 302
    const/4 v0, 0x3

    iget-object v1, p0, Lnvy;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 304
    :cond_2
    iget-object v0, p0, Lnvy;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 305
    const/4 v0, 0x4

    iget-object v1, p0, Lnvy;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 307
    :cond_3
    iget-object v0, p0, Lnvy;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 308
    const/4 v0, 0x5

    iget-object v1, p0, Lnvy;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 310
    :cond_4
    iget-object v0, p0, Lnvy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 312
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0, p1}, Lnvy;->a(Loxn;)Lnvy;

    move-result-object v0

    return-object v0
.end method
