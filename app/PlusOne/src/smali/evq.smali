.class public final Levq;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhmm;


# static fields
.field private static final T:Z


# instance fields
.field private N:Landroid/view/View;

.field private O:Landroid/widget/CheckBox;

.field private P:Landroid/widget/CheckBox;

.field private Q:Landroid/widget/TextView;

.field private R:Lhee;

.field private S:Ljmd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Levq;->T:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Llol;-><init>()V

    .line 55
    new-instance v0, Lhmf;

    iget-object v1, p0, Levq;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 56
    return-void
.end method

.method static synthetic a(Levq;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Levq;->O:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic b(Levq;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Levq;->P:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 71
    iget-object v1, p0, Levq;->at:Llnl;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 72
    invoke-virtual {p0}, Levq;->n()Lz;

    move-result-object v3

    .line 75
    const v2, 0x7f040074

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 76
    if-nez v2, :cond_0

    .line 152
    :goto_0
    return-object v0

    .line 80
    :cond_0
    const v0, 0x7f10022b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Levq;->N:Landroid/view/View;

    .line 81
    iget-object v1, p0, Levq;->N:Landroid/view/View;

    sget-boolean v0, Levq;->T:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 83
    const v0, 0x7f10022c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Levq;->O:Landroid/widget/CheckBox;

    .line 84
    iget-object v0, p0, Levq;->O:Landroid/widget/CheckBox;

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 85
    iget-object v0, p0, Levq;->O:Landroid/widget/CheckBox;

    new-instance v1, Lhmh;

    new-instance v4, Lhmk;

    sget-object v5, Lona;->g:Lhmn;

    invoke-direct {v4, v5}, Lhmk;-><init>(Lhmn;)V

    new-instance v5, Lhmk;

    sget-object v6, Lona;->h:Lhmn;

    invoke-direct {v5, v6}, Lhmk;-><init>(Lhmn;)V

    new-instance v6, Levr;

    invoke-direct {v6}, Levr;-><init>()V

    invoke-direct {v1, v4, v5, v6}, Lhmh;-><init>(Lhmk;Lhmk;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 95
    const v0, 0x7f100230

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Levq;->P:Landroid/widget/CheckBox;

    .line 96
    iget-object v0, p0, Levq;->P:Landroid/widget/CheckBox;

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 97
    iget-object v0, p0, Levq;->P:Landroid/widget/CheckBox;

    new-instance v1, Lhmh;

    new-instance v4, Lhmk;

    sget-object v5, Lona;->l:Lhmn;

    invoke-direct {v4, v5}, Lhmk;-><init>(Lhmn;)V

    new-instance v5, Lhmk;

    sget-object v6, Lona;->m:Lhmn;

    invoke-direct {v5, v6}, Lhmk;-><init>(Lhmn;)V

    new-instance v6, Levs;

    invoke-direct {v6}, Levs;-><init>()V

    invoke-direct {v1, v4, v5, v6}, Lhmh;-><init>(Lhmk;Lhmk;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 107
    const v0, 0x7f100235

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Levq;->Q:Landroid/widget/TextView;

    .line 108
    iget-object v0, p0, Levq;->Q:Landroid/widget/TextView;

    new-instance v1, Lhmi;

    invoke-direct {v1, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v0, p0, Levq;->Q:Landroid/widget/TextView;

    new-instance v1, Lhmk;

    sget-object v4, Lona;->i:Lhmn;

    invoke-direct {v1, v4}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 112
    const v0, 0x7f10022d

    .line 113
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 114
    new-instance v1, Levt;

    invoke-direct {v1, p0}, Levt;-><init>(Levq;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    const v0, 0x7f100233

    .line 122
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 123
    invoke-static {v3}, Lfug;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0a08a9

    :goto_2
    invoke-virtual {p0, v1}, Levq;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    const v0, 0x7f100231

    .line 128
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 129
    new-instance v1, Levu;

    invoke-direct {v1, p0}, Levu;-><init>(Levq;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    const v0, 0x7f100234

    .line 137
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 138
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setClickable(Z)V

    .line 139
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 140
    new-instance v1, Lhmi;

    new-instance v4, Levv;

    invoke-direct {v4, p0, v3}, Levv;-><init>(Levq;Lz;)V

    invoke-direct {v1, v4}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    new-instance v1, Lhmk;

    sget-object v3, Lona;->n:Lhmn;

    invoke-direct {v1, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    move-object v0, v2

    .line 152
    goto/16 :goto_0

    .line 81
    :cond_1
    const/16 v0, 0x8

    goto/16 :goto_1

    .line 123
    :cond_2
    const v1, 0x7f0a08aa

    goto :goto_2
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 187
    new-instance v0, Lhmk;

    sget-object v1, Lona;->j:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 63
    iget-object v0, p0, Levq;->au:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 64
    iget-object v0, p0, Levq;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Levq;->R:Lhee;

    .line 65
    iget-object v0, p0, Levq;->au:Llnh;

    const-class v1, Ljmd;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmd;

    iput-object v0, p0, Levq;->S:Ljmd;

    .line 66
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Levq;->Q:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    .line 158
    iget-object v0, p0, Levq;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 159
    invoke-virtual {p0}, Levq;->n()Lz;

    move-result-object v1

    .line 161
    if-eqz v1, :cond_1

    .line 162
    sget-boolean v2, Levq;->T:Z

    if-eqz v2, :cond_0

    .line 163
    iget-object v2, p0, Levq;->O:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 164
    invoke-static {v1, v0, v2}, Ldhv;->d(Landroid/content/Context;IZ)V

    .line 167
    :cond_0
    iget-object v2, p0, Levq;->P:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 168
    invoke-static {v1, v0, v2}, Ldhv;->e(Landroid/content/Context;IZ)V

    .line 170
    sget-object v3, Lhmw;->a:Lhmw;

    invoke-static {v1, v0, v2, v3}, Lhmu;->a(Landroid/content/Context;IZLhmw;)V

    .line 173
    if-eqz v2, :cond_2

    .line 175
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/EsService;->h(Landroid/content/Context;I)I

    .line 180
    :goto_0
    iget-object v0, p0, Levq;->S:Ljmd;

    invoke-interface {v0}, Ljmd;->i()V

    .line 183
    :cond_1
    return-void

    .line 178
    :cond_2
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/EsService;->g(Landroid/content/Context;I)I

    goto :goto_0
.end method
