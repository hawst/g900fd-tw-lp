.class public final Lmxq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Double;

.field public c:Ljava/lang/String;

.field private d:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 269
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 300
    const/4 v0, 0x0

    .line 301
    iget-object v1, p0, Lmxq;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 302
    const/4 v0, 0x1

    iget-object v1, p0, Lmxq;->a:Ljava/lang/Long;

    .line 303
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 305
    :cond_0
    iget-object v1, p0, Lmxq;->b:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 306
    const/4 v1, 0x2

    iget-object v2, p0, Lmxq;->b:Ljava/lang/Double;

    .line 307
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 309
    :cond_1
    iget-object v1, p0, Lmxq;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 310
    const/4 v1, 0x3

    iget-object v2, p0, Lmxq;->c:Ljava/lang/String;

    .line 311
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 313
    :cond_2
    iget-object v1, p0, Lmxq;->d:[B

    if-eqz v1, :cond_3

    .line 314
    const/4 v1, 0x4

    iget-object v2, p0, Lmxq;->d:[B

    .line 315
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 317
    :cond_3
    iget-object v1, p0, Lmxq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 318
    iput v0, p0, Lmxq;->ai:I

    .line 319
    return v0
.end method

.method public a(Loxn;)Lmxq;
    .locals 2

    .prologue
    .line 327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 328
    sparse-switch v0, :sswitch_data_0

    .line 332
    iget-object v1, p0, Lmxq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 333
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmxq;->ah:Ljava/util/List;

    .line 336
    :cond_1
    iget-object v1, p0, Lmxq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    :sswitch_0
    return-object p0

    .line 343
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmxq;->a:Ljava/lang/Long;

    goto :goto_0

    .line 347
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lmxq;->b:Ljava/lang/Double;

    goto :goto_0

    .line 351
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmxq;->c:Ljava/lang/String;

    goto :goto_0

    .line 355
    :sswitch_4
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lmxq;->d:[B

    goto :goto_0

    .line 328
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lmxq;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 283
    const/4 v0, 0x1

    iget-object v1, p0, Lmxq;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 285
    :cond_0
    iget-object v0, p0, Lmxq;->b:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 286
    const/4 v0, 0x2

    iget-object v1, p0, Lmxq;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 288
    :cond_1
    iget-object v0, p0, Lmxq;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 289
    const/4 v0, 0x3

    iget-object v1, p0, Lmxq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 291
    :cond_2
    iget-object v0, p0, Lmxq;->d:[B

    if-eqz v0, :cond_3

    .line 292
    const/4 v0, 0x4

    iget-object v1, p0, Lmxq;->d:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 294
    :cond_3
    iget-object v0, p0, Lmxq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 296
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 265
    invoke-virtual {p0, p1}, Lmxq;->a(Loxn;)Lmxq;

    move-result-object v0

    return-object v0
.end method
