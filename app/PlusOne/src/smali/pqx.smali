.class public final Lpqx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Lpow;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field private k:Ljava/lang/Long;

.field private l:[Ljava/lang/String;

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 496
    invoke-direct {p0}, Loxq;-><init>()V

    .line 509
    const/4 v0, 0x0

    iput-object v0, p0, Lpqx;->c:Lpow;

    .line 522
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpqx;->l:[Ljava/lang/String;

    .line 529
    const/high16 v0, -0x80000000

    iput v0, p0, Lpqx;->m:I

    .line 496
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 598
    .line 599
    iget-object v0, p0, Lpqx;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    .line 600
    const/4 v0, 0x1

    iget-object v2, p0, Lpqx;->a:Ljava/lang/Integer;

    .line 601
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 603
    :goto_0
    iget-object v2, p0, Lpqx;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 604
    const/4 v2, 0x2

    iget-object v3, p0, Lpqx;->b:Ljava/lang/Integer;

    .line 605
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 607
    :cond_0
    iget-object v2, p0, Lpqx;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 608
    const/4 v2, 0x3

    iget-object v3, p0, Lpqx;->d:Ljava/lang/String;

    .line 609
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 611
    :cond_1
    iget-object v2, p0, Lpqx;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 612
    const/4 v2, 0x4

    iget-object v3, p0, Lpqx;->e:Ljava/lang/Integer;

    .line 613
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    invoke-static {v3}, Loxo;->o(I)I

    move-result v3

    invoke-static {v3}, Loxo;->m(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 615
    :cond_2
    iget-object v2, p0, Lpqx;->k:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 616
    const/4 v2, 0x5

    iget-object v3, p0, Lpqx;->k:Ljava/lang/Long;

    .line 617
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    invoke-static {v4, v5}, Loxo;->h(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Loxo;->f(J)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 619
    :cond_3
    iget-object v2, p0, Lpqx;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 620
    const/4 v2, 0x6

    iget-object v3, p0, Lpqx;->h:Ljava/lang/Boolean;

    .line 621
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 623
    :cond_4
    iget-object v2, p0, Lpqx;->f:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 624
    const/4 v2, 0x7

    iget-object v3, p0, Lpqx;->f:Ljava/lang/String;

    .line 625
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 627
    :cond_5
    iget-object v2, p0, Lpqx;->i:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 628
    const/16 v2, 0x8

    iget-object v3, p0, Lpqx;->i:Ljava/lang/String;

    .line 629
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 631
    :cond_6
    iget v2, p0, Lpqx;->m:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_7

    .line 632
    const/16 v2, 0x9

    iget v3, p0, Lpqx;->m:I

    .line 633
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 635
    :cond_7
    iget-object v2, p0, Lpqx;->j:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 636
    const/16 v2, 0xa

    iget-object v3, p0, Lpqx;->j:Ljava/lang/String;

    .line 637
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 639
    :cond_8
    iget-object v2, p0, Lpqx;->c:Lpow;

    if-eqz v2, :cond_9

    .line 640
    const/16 v2, 0xb

    iget-object v3, p0, Lpqx;->c:Lpow;

    .line 641
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 643
    :cond_9
    iget-object v2, p0, Lpqx;->n:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 644
    const/16 v2, 0xc

    iget-object v3, p0, Lpqx;->n:Ljava/lang/String;

    .line 645
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 647
    :cond_a
    iget-object v2, p0, Lpqx;->g:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 648
    const/16 v2, 0xd

    iget-object v3, p0, Lpqx;->g:Ljava/lang/String;

    .line 649
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 651
    :cond_b
    iget-object v2, p0, Lpqx;->o:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 652
    const/16 v2, 0xe

    iget-object v3, p0, Lpqx;->o:Ljava/lang/String;

    .line 653
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 655
    :cond_c
    iget-object v2, p0, Lpqx;->l:[Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lpqx;->l:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_e

    .line 657
    iget-object v3, p0, Lpqx;->l:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_d

    aget-object v5, v3, v1

    .line 659
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 657
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 661
    :cond_d
    add-int/2addr v0, v2

    .line 662
    iget-object v1, p0, Lpqx;->l:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 664
    :cond_e
    iget-object v1, p0, Lpqx;->p:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 665
    const/16 v1, 0x11

    iget-object v2, p0, Lpqx;->p:Ljava/lang/Boolean;

    .line 666
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 668
    :cond_f
    iget-object v1, p0, Lpqx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 669
    iput v0, p0, Lpqx;->ai:I

    .line 670
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpqx;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 678
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 679
    sparse-switch v0, :sswitch_data_0

    .line 683
    iget-object v1, p0, Lpqx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 684
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpqx;->ah:Ljava/util/List;

    .line 687
    :cond_1
    iget-object v1, p0, Lpqx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 689
    :sswitch_0
    return-object p0

    .line 694
    :sswitch_1
    invoke-virtual {p1}, Loxn;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpqx;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 698
    :sswitch_2
    invoke-virtual {p1}, Loxn;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpqx;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 702
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqx;->d:Ljava/lang/String;

    goto :goto_0

    .line 706
    :sswitch_4
    invoke-virtual {p1}, Loxn;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpqx;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 710
    :sswitch_5
    invoke-virtual {p1}, Loxn;->p()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpqx;->k:Ljava/lang/Long;

    goto :goto_0

    .line 714
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpqx;->h:Ljava/lang/Boolean;

    goto :goto_0

    .line 718
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqx;->f:Ljava/lang/String;

    goto :goto_0

    .line 722
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqx;->i:Ljava/lang/String;

    goto :goto_0

    .line 726
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 727
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 730
    :cond_2
    iput v0, p0, Lpqx;->m:I

    goto :goto_0

    .line 732
    :cond_3
    iput v3, p0, Lpqx;->m:I

    goto :goto_0

    .line 737
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqx;->j:Ljava/lang/String;

    goto :goto_0

    .line 741
    :sswitch_b
    iget-object v0, p0, Lpqx;->c:Lpow;

    if-nez v0, :cond_4

    .line 742
    new-instance v0, Lpow;

    invoke-direct {v0}, Lpow;-><init>()V

    iput-object v0, p0, Lpqx;->c:Lpow;

    .line 744
    :cond_4
    iget-object v0, p0, Lpqx;->c:Lpow;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 748
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqx;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 752
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqx;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 756
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqx;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 760
    :sswitch_f
    const/16 v0, 0x82

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 761
    iget-object v0, p0, Lpqx;->l:[Ljava/lang/String;

    array-length v0, v0

    .line 762
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 763
    iget-object v2, p0, Lpqx;->l:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 764
    iput-object v1, p0, Lpqx;->l:[Ljava/lang/String;

    .line 765
    :goto_1
    iget-object v1, p0, Lpqx;->l:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    .line 766
    iget-object v1, p0, Lpqx;->l:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 767
    invoke-virtual {p1}, Loxn;->a()I

    .line 765
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 770
    :cond_5
    iget-object v1, p0, Lpqx;->l:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 774
    :sswitch_10
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpqx;->p:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 679
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x82 -> :sswitch_f
        0x88 -> :sswitch_10
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 542
    iget-object v0, p0, Lpqx;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 543
    const/4 v0, 0x1

    iget-object v1, p0, Lpqx;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->d(II)V

    .line 545
    :cond_0
    iget-object v0, p0, Lpqx;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 546
    const/4 v0, 0x2

    iget-object v1, p0, Lpqx;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->d(II)V

    .line 548
    :cond_1
    iget-object v0, p0, Lpqx;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 549
    const/4 v0, 0x3

    iget-object v1, p0, Lpqx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 551
    :cond_2
    iget-object v0, p0, Lpqx;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 552
    const/4 v0, 0x4

    iget-object v1, p0, Lpqx;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->e(II)V

    .line 554
    :cond_3
    iget-object v0, p0, Lpqx;->k:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 555
    const/4 v0, 0x5

    iget-object v1, p0, Lpqx;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->d(IJ)V

    .line 557
    :cond_4
    iget-object v0, p0, Lpqx;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 558
    const/4 v0, 0x6

    iget-object v1, p0, Lpqx;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 560
    :cond_5
    iget-object v0, p0, Lpqx;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 561
    const/4 v0, 0x7

    iget-object v1, p0, Lpqx;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 563
    :cond_6
    iget-object v0, p0, Lpqx;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 564
    const/16 v0, 0x8

    iget-object v1, p0, Lpqx;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 566
    :cond_7
    iget v0, p0, Lpqx;->m:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_8

    .line 567
    const/16 v0, 0x9

    iget v1, p0, Lpqx;->m:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 569
    :cond_8
    iget-object v0, p0, Lpqx;->j:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 570
    const/16 v0, 0xa

    iget-object v1, p0, Lpqx;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 572
    :cond_9
    iget-object v0, p0, Lpqx;->c:Lpow;

    if-eqz v0, :cond_a

    .line 573
    const/16 v0, 0xb

    iget-object v1, p0, Lpqx;->c:Lpow;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 575
    :cond_a
    iget-object v0, p0, Lpqx;->n:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 576
    const/16 v0, 0xc

    iget-object v1, p0, Lpqx;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 578
    :cond_b
    iget-object v0, p0, Lpqx;->g:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 579
    const/16 v0, 0xd

    iget-object v1, p0, Lpqx;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 581
    :cond_c
    iget-object v0, p0, Lpqx;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 582
    const/16 v0, 0xe

    iget-object v1, p0, Lpqx;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 584
    :cond_d
    iget-object v0, p0, Lpqx;->l:[Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 585
    iget-object v1, p0, Lpqx;->l:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 586
    const/16 v4, 0x10

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 585
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 589
    :cond_e
    iget-object v0, p0, Lpqx;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 590
    const/16 v0, 0x11

    iget-object v1, p0, Lpqx;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 592
    :cond_f
    iget-object v0, p0, Lpqx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 594
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 492
    invoke-virtual {p0, p1}, Lpqx;->a(Loxn;)Lpqx;

    move-result-object v0

    return-object v0
.end method
