.class public final Llzg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lneb;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28267
    invoke-direct {p0}, Loxq;-><init>()V

    .line 28270
    iput-object v0, p0, Llzg;->apiHeader:Llyq;

    .line 28273
    iput-object v0, p0, Llzg;->a:Lneb;

    .line 28267
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 28290
    const/4 v0, 0x0

    .line 28291
    iget-object v1, p0, Llzg;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 28292
    const/4 v0, 0x1

    iget-object v1, p0, Llzg;->apiHeader:Llyq;

    .line 28293
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 28295
    :cond_0
    iget-object v1, p0, Llzg;->a:Lneb;

    if-eqz v1, :cond_1

    .line 28296
    const/4 v1, 0x2

    iget-object v2, p0, Llzg;->a:Lneb;

    .line 28297
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28299
    :cond_1
    iget-object v1, p0, Llzg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28300
    iput v0, p0, Llzg;->ai:I

    .line 28301
    return v0
.end method

.method public a(Loxn;)Llzg;
    .locals 2

    .prologue
    .line 28309
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 28310
    sparse-switch v0, :sswitch_data_0

    .line 28314
    iget-object v1, p0, Llzg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 28315
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzg;->ah:Ljava/util/List;

    .line 28318
    :cond_1
    iget-object v1, p0, Llzg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28320
    :sswitch_0
    return-object p0

    .line 28325
    :sswitch_1
    iget-object v0, p0, Llzg;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 28326
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzg;->apiHeader:Llyq;

    .line 28328
    :cond_2
    iget-object v0, p0, Llzg;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28332
    :sswitch_2
    iget-object v0, p0, Llzg;->a:Lneb;

    if-nez v0, :cond_3

    .line 28333
    new-instance v0, Lneb;

    invoke-direct {v0}, Lneb;-><init>()V

    iput-object v0, p0, Llzg;->a:Lneb;

    .line 28335
    :cond_3
    iget-object v0, p0, Llzg;->a:Lneb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28310
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28278
    iget-object v0, p0, Llzg;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 28279
    const/4 v0, 0x1

    iget-object v1, p0, Llzg;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28281
    :cond_0
    iget-object v0, p0, Llzg;->a:Lneb;

    if-eqz v0, :cond_1

    .line 28282
    const/4 v0, 0x2

    iget-object v1, p0, Llzg;->a:Lneb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28284
    :cond_1
    iget-object v0, p0, Llzg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 28286
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 28263
    invoke-virtual {p0, p1}, Llzg;->a(Loxn;)Llzg;

    move-result-object v0

    return-object v0
.end method
