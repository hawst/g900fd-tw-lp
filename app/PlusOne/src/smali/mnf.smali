.class public final Lmnf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmnf;


# instance fields
.field public b:Lmnu;

.field public c:Lmnu;

.field public d:Lmnu;

.field public e:Lmnu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3145
    const/4 v0, 0x0

    new-array v0, v0, [Lmnf;

    sput-object v0, Lmnf;->a:[Lmnf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3146
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3149
    iput-object v0, p0, Lmnf;->b:Lmnu;

    .line 3152
    iput-object v0, p0, Lmnf;->c:Lmnu;

    .line 3155
    iput-object v0, p0, Lmnf;->d:Lmnu;

    .line 3158
    iput-object v0, p0, Lmnf;->e:Lmnu;

    .line 3146
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3181
    const/4 v0, 0x0

    .line 3182
    iget-object v1, p0, Lmnf;->b:Lmnu;

    if-eqz v1, :cond_0

    .line 3183
    const/4 v0, 0x1

    iget-object v1, p0, Lmnf;->b:Lmnu;

    .line 3184
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3186
    :cond_0
    iget-object v1, p0, Lmnf;->c:Lmnu;

    if-eqz v1, :cond_1

    .line 3187
    const/4 v1, 0x2

    iget-object v2, p0, Lmnf;->c:Lmnu;

    .line 3188
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3190
    :cond_1
    iget-object v1, p0, Lmnf;->d:Lmnu;

    if-eqz v1, :cond_2

    .line 3191
    const/4 v1, 0x3

    iget-object v2, p0, Lmnf;->d:Lmnu;

    .line 3192
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3194
    :cond_2
    iget-object v1, p0, Lmnf;->e:Lmnu;

    if-eqz v1, :cond_3

    .line 3195
    const/4 v1, 0x4

    iget-object v2, p0, Lmnf;->e:Lmnu;

    .line 3196
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3198
    :cond_3
    iget-object v1, p0, Lmnf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3199
    iput v0, p0, Lmnf;->ai:I

    .line 3200
    return v0
.end method

.method public a(Loxn;)Lmnf;
    .locals 2

    .prologue
    .line 3208
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3209
    sparse-switch v0, :sswitch_data_0

    .line 3213
    iget-object v1, p0, Lmnf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3214
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnf;->ah:Ljava/util/List;

    .line 3217
    :cond_1
    iget-object v1, p0, Lmnf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3219
    :sswitch_0
    return-object p0

    .line 3224
    :sswitch_1
    iget-object v0, p0, Lmnf;->b:Lmnu;

    if-nez v0, :cond_2

    .line 3225
    new-instance v0, Lmnu;

    invoke-direct {v0}, Lmnu;-><init>()V

    iput-object v0, p0, Lmnf;->b:Lmnu;

    .line 3227
    :cond_2
    iget-object v0, p0, Lmnf;->b:Lmnu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3231
    :sswitch_2
    iget-object v0, p0, Lmnf;->c:Lmnu;

    if-nez v0, :cond_3

    .line 3232
    new-instance v0, Lmnu;

    invoke-direct {v0}, Lmnu;-><init>()V

    iput-object v0, p0, Lmnf;->c:Lmnu;

    .line 3234
    :cond_3
    iget-object v0, p0, Lmnf;->c:Lmnu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3238
    :sswitch_3
    iget-object v0, p0, Lmnf;->d:Lmnu;

    if-nez v0, :cond_4

    .line 3239
    new-instance v0, Lmnu;

    invoke-direct {v0}, Lmnu;-><init>()V

    iput-object v0, p0, Lmnf;->d:Lmnu;

    .line 3241
    :cond_4
    iget-object v0, p0, Lmnf;->d:Lmnu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3245
    :sswitch_4
    iget-object v0, p0, Lmnf;->e:Lmnu;

    if-nez v0, :cond_5

    .line 3246
    new-instance v0, Lmnu;

    invoke-direct {v0}, Lmnu;-><init>()V

    iput-object v0, p0, Lmnf;->e:Lmnu;

    .line 3248
    :cond_5
    iget-object v0, p0, Lmnf;->e:Lmnu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3209
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3163
    iget-object v0, p0, Lmnf;->b:Lmnu;

    if-eqz v0, :cond_0

    .line 3164
    const/4 v0, 0x1

    iget-object v1, p0, Lmnf;->b:Lmnu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3166
    :cond_0
    iget-object v0, p0, Lmnf;->c:Lmnu;

    if-eqz v0, :cond_1

    .line 3167
    const/4 v0, 0x2

    iget-object v1, p0, Lmnf;->c:Lmnu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3169
    :cond_1
    iget-object v0, p0, Lmnf;->d:Lmnu;

    if-eqz v0, :cond_2

    .line 3170
    const/4 v0, 0x3

    iget-object v1, p0, Lmnf;->d:Lmnu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3172
    :cond_2
    iget-object v0, p0, Lmnf;->e:Lmnu;

    if-eqz v0, :cond_3

    .line 3173
    const/4 v0, 0x4

    iget-object v1, p0, Lmnf;->e:Lmnu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3175
    :cond_3
    iget-object v0, p0, Lmnf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3177
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3142
    invoke-virtual {p0, p1}, Lmnf;->a(Loxn;)Lmnf;

    move-result-object v0

    return-object v0
.end method
