.class public final Lnpj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Locf;

.field private c:Lock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1004
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1009
    iput-object v0, p0, Lnpj;->b:Locf;

    .line 1012
    iput-object v0, p0, Lnpj;->c:Lock;

    .line 1004
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1032
    const/4 v0, 0x0

    .line 1033
    iget-object v1, p0, Lnpj;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1034
    const/4 v0, 0x1

    iget-object v1, p0, Lnpj;->a:Ljava/lang/String;

    .line 1035
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1037
    :cond_0
    iget-object v1, p0, Lnpj;->b:Locf;

    if-eqz v1, :cond_1

    .line 1038
    const/4 v1, 0x2

    iget-object v2, p0, Lnpj;->b:Locf;

    .line 1039
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1041
    :cond_1
    iget-object v1, p0, Lnpj;->c:Lock;

    if-eqz v1, :cond_2

    .line 1042
    const/4 v1, 0x3

    iget-object v2, p0, Lnpj;->c:Lock;

    .line 1043
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1045
    :cond_2
    iget-object v1, p0, Lnpj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1046
    iput v0, p0, Lnpj;->ai:I

    .line 1047
    return v0
.end method

.method public a(Loxn;)Lnpj;
    .locals 2

    .prologue
    .line 1055
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1056
    sparse-switch v0, :sswitch_data_0

    .line 1060
    iget-object v1, p0, Lnpj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1061
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnpj;->ah:Ljava/util/List;

    .line 1064
    :cond_1
    iget-object v1, p0, Lnpj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1066
    :sswitch_0
    return-object p0

    .line 1071
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnpj;->a:Ljava/lang/String;

    goto :goto_0

    .line 1075
    :sswitch_2
    iget-object v0, p0, Lnpj;->b:Locf;

    if-nez v0, :cond_2

    .line 1076
    new-instance v0, Locf;

    invoke-direct {v0}, Locf;-><init>()V

    iput-object v0, p0, Lnpj;->b:Locf;

    .line 1078
    :cond_2
    iget-object v0, p0, Lnpj;->b:Locf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1082
    :sswitch_3
    iget-object v0, p0, Lnpj;->c:Lock;

    if-nez v0, :cond_3

    .line 1083
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Lnpj;->c:Lock;

    .line 1085
    :cond_3
    iget-object v0, p0, Lnpj;->c:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1056
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1017
    iget-object v0, p0, Lnpj;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1018
    const/4 v0, 0x1

    iget-object v1, p0, Lnpj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1020
    :cond_0
    iget-object v0, p0, Lnpj;->b:Locf;

    if-eqz v0, :cond_1

    .line 1021
    const/4 v0, 0x2

    iget-object v1, p0, Lnpj;->b:Locf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1023
    :cond_1
    iget-object v0, p0, Lnpj;->c:Lock;

    if-eqz v0, :cond_2

    .line 1024
    const/4 v0, 0x3

    iget-object v1, p0, Lnpj;->c:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1026
    :cond_2
    iget-object v0, p0, Lnpj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1028
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1000
    invoke-virtual {p0, p1}, Lnpj;->a(Loxn;)Lnpj;

    move-result-object v0

    return-object v0
.end method
