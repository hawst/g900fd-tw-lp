.class public abstract Ldds;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public A:J

.field public B:J

.field public C:Ljava/lang/String;

.field public D:J

.field public d:Landroid/os/Bundle;

.field public e:I

.field public f:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lddt;",
            "Lizu;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lnzi;

.field public h:Lnzi;

.field public i:Lnzb;

.field public j:Lnzb;

.field public k:Lnym;

.field public l:Ljcn;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/Integer;

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lddt;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Ldds;->f:Ljava/util/EnumMap;

    .line 106
    return-void
.end method


# virtual methods
.method public abstract a()Lddl;
.end method

.method public a(Landroid/content/Context;Landroid/os/Bundle;)Ldds;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 239
    iput-object p2, p0, Ldds;->d:Landroid/os/Bundle;

    .line 241
    const-string v0, "photo_ref"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {p0, v0}, Ldds;->a(Lizu;)Ldds;

    .line 242
    const-string v0, "account_id"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 244
    iput v0, p0, Ldds;->e:I

    .line 245
    const-string v0, "media_attr"

    invoke-virtual {p2, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Ldds;->z:J

    .line 246
    const-string v0, "user_actions"

    invoke-virtual {p2, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Ldds;->A:J

    .line 247
    const-string v0, "selectable"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldds;->u:Z

    .line 249
    return-object p0
.end method

.method public a(Landroid/content/Context;Lfdg;)Ldds;
    .locals 16

    .prologue
    .line 154
    const-class v2, Ljgn;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljgn;

    invoke-interface {v2}, Ljgn;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 229
    :goto_0
    return-object p0

    .line 158
    :cond_0
    move-object/from16 v0, p2

    iget-object v11, v0, Lfdg;->a:Lnym;

    .line 159
    iget-object v2, v11, Lnym;->h:Lnyz;

    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Ldds;->f:Ljava/util/EnumMap;

    sget-object v3, Lddt;->a:Lddt;

    .line 160
    invoke-virtual {v2, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizu;

    invoke-virtual {v2}, Lizu;->b()Ljava/lang/String;

    move-result-object v5

    .line 161
    :goto_1
    iget-object v2, v11, Lnym;->e:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 164
    iget-object v2, v11, Lnym;->b:Lnyl;

    if-eqz v2, :cond_b

    .line 165
    iget-object v2, v11, Lnym;->b:Lnyl;

    iget-object v8, v2, Lnyl;->b:Ljava/lang/String;

    .line 170
    :goto_2
    invoke-static {v11}, Ljvd;->b(Lnym;)Ljac;

    move-result-object v10

    .line 171
    invoke-static {v11}, Ljvd;->c(Lnym;)Ljava/lang/String;

    move-result-object v12

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Ldds;->f:Ljava/util/EnumMap;

    sget-object v3, Lddt;->a:Lddt;

    .line 173
    invoke-virtual {v2, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizu;

    invoke-virtual {v2}, Lizu;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Ldds;->f:Ljava/util/EnumMap;

    sget-object v3, Lddt;->a:Lddt;

    .line 174
    invoke-virtual {v2, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizu;

    invoke-virtual {v2}, Lizu;->e()Landroid/net/Uri;

    move-result-object v9

    move-object/from16 v3, p1

    .line 172
    invoke-static/range {v3 .. v10}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v3

    .line 177
    iget-object v2, v11, Lnym;->m:Lnzb;

    if-eqz v2, :cond_1

    .line 178
    iget-object v2, v11, Lnym;->m:Lnzb;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->a(Lnzb;)Ldds;

    .line 181
    :cond_1
    iget-object v2, v11, Lnym;->b:Lnyl;

    if-eqz v2, :cond_2

    .line 182
    iget-object v2, v11, Lnym;->b:Lnyl;

    iget-object v2, v2, Lnyl;->e:Ljava/lang/Integer;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->a(Ljava/lang/Integer;)Ldds;

    .line 185
    :cond_2
    move-object/from16 v0, p2

    iget-wide v8, v0, Lfdg;->b:J

    const-wide/16 v14, 0x0

    cmp-long v2, v8, v14

    if-eqz v2, :cond_3

    .line 186
    move-object/from16 v0, p2

    iget-wide v8, v0, Lfdg;->b:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v9}, Ldds;->b(J)Ldds;

    .line 189
    :cond_3
    iget-object v2, v11, Lnym;->s:Lnyl;

    if-eqz v2, :cond_4

    .line 190
    iget-object v2, v11, Lnym;->s:Lnyl;

    iget-object v2, v2, Lnyl;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v10}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    sget-object v4, Lddt;->b:Lddt;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Ldds;->a(Lizu;Lddt;)Ldds;

    .line 193
    :cond_4
    iget-object v2, v11, Lnym;->t:Lnyl;

    if-eqz v2, :cond_5

    .line 194
    iget-object v2, v11, Lnym;->t:Lnyl;

    iget-object v2, v2, Lnyl;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v10}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    sget-object v4, Lddt;->c:Lddt;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Ldds;->a(Lizu;Lddt;)Ldds;

    .line 197
    :cond_5
    iget-object v2, v11, Lnym;->N:[Lnxd;

    if-eqz v2, :cond_6

    iget-object v2, v11, Lnym;->N:[Lnxd;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 198
    iget-object v2, v11, Lnym;->N:[Lnxd;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget-object v2, v2, Lnxd;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->d(Ljava/lang/String;)Ldds;

    .line 201
    :cond_6
    iget-object v2, v11, Lnym;->y:[Lnxr;

    if-eqz v2, :cond_7

    iget-object v2, v11, Lnym;->y:[Lnxr;

    array-length v2, v2

    if-eqz v2, :cond_7

    .line 202
    iget-object v2, v11, Lnym;->y:[Lnxr;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    .line 203
    iget-object v4, v2, Lnxr;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ldds;->f(Ljava/lang/String;)Ldds;

    .line 204
    iget-object v2, v2, Lnxr;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->c(Ljava/lang/String;)Ldds;

    .line 207
    :cond_7
    sget-object v2, Ljac;->a:Ljac;

    invoke-virtual {v2, v10}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    iget-object v4, v11, Lnym;->I:[I

    if-eqz v4, :cond_8

    iget-object v2, v11, Lnym;->s:Lnyl;

    if-nez v2, :cond_c

    :cond_8
    const/4 v2, 0x0

    .line 208
    :goto_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 209
    move-object/from16 v0, p1

    invoke-static {v0, v2, v10}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    sget-object v4, Lddt;->d:Lddt;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Ldds;->a(Lizu;Lddt;)Ldds;

    .line 214
    :goto_4
    move-object/from16 v0, p2

    iget-boolean v2, v0, Lfdg;->e:Z

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->a(Z)V

    .line 215
    move-object/from16 v0, p2

    iget-wide v8, v0, Lfdg;->c:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v9}, Ldds;->c(J)Ldds;

    .line 216
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ldds;->a(Ljava/lang/String;)Ldds;

    .line 217
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Ldds;->d(J)Ldds;

    .line 218
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Ldds;->g(Ljava/lang/String;)Ldds;

    .line 219
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Ldds;->a(Lizu;)Ldds;

    .line 220
    iget-object v2, v11, Lnym;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->e(Ljava/lang/String;)Ldds;

    .line 221
    move-object/from16 v0, p0

    iget-object v2, v0, Ldds;->g:Lnzi;

    if-nez v2, :cond_9

    .line 222
    iget-object v2, v11, Lnym;->G:Lnzi;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->b(Lnzi;)Ldds;

    .line 224
    :cond_9
    iget-object v2, v11, Lnym;->G:Lnzi;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->a(Lnzi;)Ldds;

    .line 225
    iget-object v2, v11, Lnym;->m:Lnzb;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->b(Lnzb;)Ldds;

    .line 226
    iget-object v2, v11, Lnym;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldds;->e(Ljava/lang/String;)Ldds;

    .line 227
    move-object/from16 v0, p2

    iget-wide v2, v0, Lfdg;->d:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Ldds;->e(J)Ldds;

    goto/16 :goto_0

    .line 160
    :cond_a
    iget-object v2, v11, Lnym;->h:Lnyz;

    iget-object v5, v2, Lnyz;->c:Ljava/lang/String;

    goto/16 :goto_1

    .line 167
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Ldds;->f:Ljava/util/EnumMap;

    sget-object v3, Lddt;->a:Lddt;

    invoke-virtual {v2, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizu;

    invoke-virtual {v2}, Lizu;->d()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_2

    .line 207
    :cond_c
    new-instance v8, Landroid/util/SparseBooleanArray;

    const/4 v2, 0x4

    invoke-direct {v8, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    const/4 v2, 0x0

    :goto_5
    array-length v9, v4

    if-ge v2, v9, :cond_d

    aget v9, v4, v2

    const/4 v13, 0x1

    invoke-virtual {v8, v9, v13}, Landroid/util/SparseBooleanArray;->put(IZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_d
    const/4 v2, 0x2

    invoke-virtual {v8, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, v11, Lnym;->s:Lnyl;

    iget-object v2, v2, Lnyl;->b:Ljava/lang/String;

    goto/16 :goto_3

    :cond_e
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_f

    iget v2, v11, Lnym;->J:I

    const/high16 v4, -0x80000000

    if-ne v2, v4, :cond_10

    iget-object v2, v11, Lnym;->s:Lnyl;

    iget-object v2, v2, Lnyl;->b:Ljava/lang/String;

    goto/16 :goto_3

    :cond_f
    const/4 v2, 0x3

    invoke-virtual {v8, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_10

    iget-object v2, v11, Lnym;->s:Lnyl;

    iget-object v2, v2, Lnyl;->b:Ljava/lang/String;

    goto/16 :goto_3

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 211
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Ldds;->f:Ljava/util/EnumMap;

    sget-object v4, Lddt;->a:Lddt;

    invoke-virtual {v2, v4}, Ljava/util/EnumMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4
.end method

.method public a(Lddl;)Ldds;
    .locals 2

    .prologue
    .line 113
    instance-of v0, p1, Lddr;

    if-eqz v0, :cond_1

    .line 114
    check-cast p1, Lddr;

    .line 115
    iget-object v0, p1, Lddr;->a:Landroid/os/Bundle;

    iput-object v0, p0, Ldds;->d:Landroid/os/Bundle;

    .line 116
    iget v0, p1, Lddr;->b:I

    iput v0, p0, Ldds;->e:I

    .line 117
    iget-object v0, p1, Lddr;->c:Ljava/util/EnumMap;

    iput-object v0, p0, Ldds;->f:Ljava/util/EnumMap;

    .line 118
    iget-object v0, p1, Lddr;->d:Lnzi;

    iput-object v0, p0, Ldds;->g:Lnzi;

    .line 119
    iget-object v0, p1, Lddr;->e:Lnzi;

    iput-object v0, p0, Ldds;->h:Lnzi;

    .line 120
    iget-object v0, p1, Lddr;->f:Lnzb;

    iput-object v0, p0, Ldds;->i:Lnzb;

    .line 121
    iget-object v0, p1, Lddr;->g:Lnzb;

    iput-object v0, p0, Ldds;->j:Lnzb;

    .line 122
    iget-object v0, p1, Lddr;->h:Lnym;

    iput-object v0, p0, Ldds;->k:Lnym;

    .line 123
    iget-object v0, p1, Lddr;->i:Ljcn;

    iput-object v0, p0, Ldds;->l:Ljcn;

    .line 124
    iget-object v0, p1, Lddr;->j:Ljava/lang/String;

    iput-object v0, p0, Ldds;->m:Ljava/lang/String;

    .line 125
    iget-object v0, p1, Lddr;->k:Ljava/lang/String;

    iput-object v0, p0, Ldds;->n:Ljava/lang/String;

    .line 126
    iget-object v0, p1, Lddr;->l:Ljava/lang/String;

    iput-object v0, p0, Ldds;->o:Ljava/lang/String;

    .line 127
    iget-object v0, p1, Lddr;->m:Ljava/lang/String;

    iput-object v0, p0, Ldds;->p:Ljava/lang/String;

    .line 128
    iget-object v0, p1, Lddr;->n:Ljava/lang/String;

    iput-object v0, p0, Ldds;->q:Ljava/lang/String;

    .line 129
    iget-object v0, p1, Lddr;->o:Ljava/lang/String;

    iput-object v0, p0, Ldds;->r:Ljava/lang/String;

    .line 130
    iget-object v0, p1, Lddr;->p:Ljava/lang/Integer;

    iput-object v0, p0, Ldds;->s:Ljava/lang/Integer;

    .line 131
    iget-boolean v0, p1, Lddr;->q:Z

    iput-boolean v0, p0, Ldds;->t:Z

    .line 132
    iget-boolean v0, p1, Lddr;->r:Z

    iput-boolean v0, p0, Ldds;->u:Z

    .line 133
    iget-boolean v0, p1, Lddr;->s:Z

    iput-boolean v0, p0, Ldds;->v:Z

    .line 134
    iget-boolean v0, p1, Lddr;->u:Z

    iput-boolean v0, p0, Ldds;->x:Z

    .line 135
    iget-boolean v0, p1, Lddr;->t:Z

    iput-boolean v0, p0, Ldds;->w:Z

    .line 136
    iget-boolean v0, p1, Lddr;->v:Z

    iput-boolean v0, p0, Ldds;->y:Z

    .line 137
    iget-wide v0, p1, Lddr;->w:J

    iput-wide v0, p0, Ldds;->z:J

    .line 138
    iget-wide v0, p1, Lddr;->x:J

    iput-wide v0, p0, Ldds;->A:J

    .line 139
    iget-wide v0, p1, Lddr;->y:J

    iput-wide v0, p0, Ldds;->B:J

    .line 140
    iget-object v0, p1, Lddr;->z:Ljava/lang/String;

    iput-object v0, p0, Ldds;->C:Ljava/lang/String;

    .line 141
    iget-wide v0, p1, Lddr;->A:J

    iput-wide v0, p0, Ldds;->D:J

    .line 147
    :cond_0
    return-object p0

    .line 142
    :cond_1
    if-eqz p1, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t mergeFromProxy with this type of proxy."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lizu;)Ldds;
    .locals 2

    .prologue
    .line 265
    if-eqz p1, :cond_0

    .line 266
    iget-object v0, p0, Ldds;->f:Ljava/util/EnumMap;

    sget-object v1, Lddt;->a:Lddt;

    invoke-virtual {v0, v1, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    :cond_0
    return-object p0
.end method

.method public a(Lizu;Lddt;)Ldds;
    .locals 1

    .prologue
    .line 258
    if-eqz p1, :cond_0

    .line 259
    iget-object v0, p0, Ldds;->f:Ljava/util/EnumMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/Integer;)Ldds;
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Ldds;->s:Ljava/lang/Integer;

    .line 328
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ldds;
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Ldds;->m:Ljava/lang/String;

    .line 298
    return-object p0
.end method

.method public a(Ljcn;)Ldds;
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Ldds;->l:Ljcn;

    .line 293
    return-object p0
.end method

.method public a(Lnym;)Ldds;
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Ldds;->k:Lnym;

    .line 288
    return-object p0
.end method

.method public a(Lnzb;)Ldds;
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Ldds;->i:Lnzb;

    .line 278
    return-object p0
.end method

.method public a(Lnzi;)Ldds;
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Ldds;->h:Lnzi;

    .line 235
    return-object p0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 332
    iput-boolean p1, p0, Ldds;->t:Z

    .line 333
    return-void
.end method

.method public b(J)Ldds;
    .locals 1

    .prologue
    .line 361
    iput-wide p1, p0, Ldds;->z:J

    .line 362
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ldds;
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Ldds;->n:Ljava/lang/String;

    .line 303
    return-object p0
.end method

.method public b(Lnzb;)Ldds;
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Ldds;->j:Lnzb;

    .line 283
    return-object p0
.end method

.method public b(Lnzi;)Ldds;
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Ldds;->g:Lnzi;

    .line 273
    return-object p0
.end method

.method public b(Z)Ldds;
    .locals 0

    .prologue
    .line 341
    iput-boolean p1, p0, Ldds;->v:Z

    .line 342
    return-object p0
.end method

.method public c(J)Ldds;
    .locals 1

    .prologue
    .line 366
    iput-wide p1, p0, Ldds;->A:J

    .line 367
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ldds;
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Ldds;->o:Ljava/lang/String;

    .line 308
    return-object p0
.end method

.method public c(Z)Ldds;
    .locals 0

    .prologue
    .line 346
    iput-boolean p1, p0, Ldds;->x:Z

    .line 347
    return-object p0
.end method

.method public d(J)Ldds;
    .locals 1

    .prologue
    .line 371
    iput-wide p1, p0, Ldds;->B:J

    .line 372
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ldds;
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Ldds;->p:Ljava/lang/String;

    .line 313
    return-object p0
.end method

.method public d(Z)Ldds;
    .locals 0

    .prologue
    .line 351
    iput-boolean p1, p0, Ldds;->w:Z

    .line 352
    return-object p0
.end method

.method public e(J)Ldds;
    .locals 1

    .prologue
    .line 381
    iput-wide p1, p0, Ldds;->D:J

    .line 382
    return-object p0
.end method

.method public e(Ljava/lang/String;)Ldds;
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Ldds;->q:Ljava/lang/String;

    .line 318
    return-object p0
.end method

.method public e(Z)Ldds;
    .locals 0

    .prologue
    .line 356
    iput-boolean p1, p0, Ldds;->y:Z

    .line 357
    return-object p0
.end method

.method public f(Ljava/lang/String;)Ldds;
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Ldds;->r:Ljava/lang/String;

    .line 323
    return-object p0
.end method

.method public g(Ljava/lang/String;)Ldds;
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Ldds;->C:Ljava/lang/String;

    .line 377
    return-object p0
.end method
