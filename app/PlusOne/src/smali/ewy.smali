.class public final Lewy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field private synthetic b:I

.field private synthetic c:Lcom/google/android/apps/plus/phone/EsApplication;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/EsApplication;Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lewy;->c:Lcom/google/android/apps/plus/phone/EsApplication;

    iput-object p2, p0, Lewy;->a:Landroid/content/Context;

    iput p3, p0, Lewy;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 190
    const/16 v0, 0x13

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 196
    iget-object v0, p0, Lewy;->c:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsApplication;->h_()Llnh;

    move-result-object v0

    const-class v1, Lfur;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfur;

    .line 197
    invoke-virtual {v0}, Lfur;->a()V

    .line 198
    iget-object v1, p0, Lewy;->c:Lcom/google/android/apps/plus/phone/EsApplication;

    .line 199
    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsApplication;->h_()Llnh;

    move-result-object v1

    const-class v2, Lhsc;

    invoke-virtual {v1, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhsc;

    .line 200
    invoke-virtual {v1}, Lhsc;->a()V

    .line 202
    invoke-virtual {v0}, Lfur;->c()Z

    move-result v1

    .line 203
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    .line 204
    invoke-static {v0}, Lfuy;->a(Landroid/content/Context;)Z

    move-result v0

    .line 205
    iget-object v2, p0, Lewy;->a:Landroid/content/Context;

    .line 206
    invoke-static {v2}, Lfuy;->b(Landroid/content/Context;)Z

    move-result v2

    .line 208
    iget-object v3, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v3, v0}, Lfuy;->a(Landroid/content/Context;Z)V

    .line 210
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lfuy;->a(Landroid/content/Context;ZZ)V

    .line 213
    if-eqz v1, :cond_0

    if-nez v2, :cond_3

    .line 214
    :cond_0
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->b(Landroid/content/Context;)V

    .line 222
    :cond_1
    :goto_0
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    const-class v3, Lhfp;

    .line 223
    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfp;

    .line 224
    invoke-interface {v0}, Lhfp;->a()V

    .line 226
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v0}, Lhqd;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 228
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v0}, Lhqd;->i(Landroid/content/Context;)V

    .line 230
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v0}, Lhqd;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    .line 232
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v3, 0x0

    .line 231
    invoke-static {v0, v4, v5, v3}, Lhsb;->a(Landroid/content/Context;JZ)V

    .line 236
    :cond_2
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    const-class v3, Lhpu;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 238
    invoke-virtual {v0}, Lhpu;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 239
    iget-object v4, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v4, v0}, Ldrm;->a(Landroid/content/Context;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 272
    :catch_0
    move-exception v0

    .line 273
    const-string v1, "EsApplication"

    const-string v2, "Failed app initialization"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 275
    :goto_2
    return-void

    .line 215
    :cond_3
    :try_start_1
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;

    invoke-static {v0, v3}, Lfug;->b(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->c(Landroid/content/Context;)V

    .line 218
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;

    invoke-static {v0, v3}, Lfug;->a(Landroid/content/Context;Ljava/lang/Class;)V

    .line 219
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/photos/content/GooglePhotoDownsyncProvider;

    invoke-static {v0, v3}, Lfug;->a(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 242
    :cond_4
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    iget v3, p0, Lewy;->b:I

    invoke-static {v0, v3}, Llbn;->a(Landroid/content/Context;I)V

    .line 244
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v0}, Lffe;->b(Landroid/content/Context;)V

    .line 247
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    invoke-static {v0}, Ldwq;->a(Landroid/content/Context;)V

    .line 249
    if-eqz v1, :cond_5

    if-nez v2, :cond_6

    .line 251
    :cond_5
    iget-object v0, p0, Lewy;->c:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 252
    const-class v0, Lfew;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    invoke-virtual {v0}, Lfew;->c()V

    .line 254
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lewz;

    invoke-direct {v2, p0, v1}, Lewz;-><init>(Lewy;Landroid/content/Context;)V

    const-string v1, "allphotos_localmedia_fingerprints_on_create"

    invoke-direct {v0, v2, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 268
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 271
    :cond_6
    iget-object v0, p0, Lewy;->a:Landroid/content/Context;

    const-class v1, Lhqo;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqo;

    invoke-interface {v0}, Lhqo;->a()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
