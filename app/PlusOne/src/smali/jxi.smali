.class final Ljxi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:Ljava/io/RandomAccessFile;

.field public b:J

.field public c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/io/File;

.field private f:J

.field private g:I

.field private synthetic h:Ljxg;


# direct methods
.method public constructor <init>(Ljxg;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 101
    iput-object p1, p0, Ljxi;->h:Ljxg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljxi;->b:J

    .line 96
    const/4 v0, 0x1

    iput v0, p0, Ljxi;->c:I

    .line 102
    iput-object p2, p0, Ljxi;->d:Ljava/lang/String;

    .line 103
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 125
    iput p1, p0, Ljxi;->c:I

    .line 126
    iget-object v0, p0, Ljxi;->h:Ljxg;

    iget-object v0, v0, Ljxg;->a:Ljava/util/HashMap;

    iget-object v1, p0, Ljxi;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v0, p0, Ljxi;->h:Ljxg;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 128
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 106
    iget-object v1, p0, Ljxi;->h:Ljxg;

    monitor-enter v1

    .line 107
    :try_start_0
    iget v0, p0, Ljxi;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxi;->g:I

    .line 108
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 6

    .prologue
    .line 112
    iget-object v1, p0, Ljxi;->h:Ljxg;

    monitor-enter v1

    .line 113
    :try_start_0
    iget v0, p0, Ljxi;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ljxi;->g:I

    .line 114
    iget v0, p0, Ljxi;->g:I

    if-nez v0, :cond_0

    .line 115
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0xbb8

    add-long/2addr v2, v4

    iput-wide v2, p0, Ljxi;->f:J

    .line 116
    iget v0, p0, Ljxi;->c:I

    and-int/lit8 v0, v0, 0x1c

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Ljxi;->a:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lifu;->a(Ljava/io/Closeable;)V

    .line 118
    iget v0, p0, Ljxi;->c:I

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Ljxi;->e:Ljava/io/File;

    invoke-static {v0}, Ljxg;->a(Ljava/io/File;)V

    .line 121
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 14

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v12, 0x4

    .line 133
    iget-object v2, p0, Ljxi;->h:Ljxg;

    monitor-enter v2

    .line 134
    :try_start_0
    iget v3, p0, Ljxi;->c:I

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lifu;->a(Z)V

    .line 135
    iget v0, p0, Ljxi;->g:I

    if-nez v0, :cond_1

    .line 136
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Ljxi;->a(I)V

    .line 137
    monitor-exit v2

    .line 214
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 134
    goto :goto_0

    .line 139
    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Ljxi;->c:I

    .line 140
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    const-string v1, "PicasaStore.download "

    iget-object v0, p0, Ljxi;->d:Ljava/lang/String;

    .line 143
    invoke-static {v0}, Lifu;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    :goto_2
    invoke-static {v0}, Ljwn;->a(Ljava/lang/String;)I

    move-result v2

    .line 144
    const/4 v0, 0x0

    .line 146
    :try_start_1
    iget-object v1, p0, Ljxi;->h:Ljxg;

    iget-object v1, v1, Ljxg;->c:Ljxh;

    invoke-interface {v1}, Ljxh;->c()Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Ljxi;->e:Ljava/io/File;

    .line 147
    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v3, p0, Ljxi;->e:Ljava/io/File;

    const-string v4, "rw"

    invoke-direct {v1, v3, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Ljxi;->a:Ljava/io/RandomAccessFile;

    .line 148
    iget-object v1, p0, Ljxi;->d:Ljava/lang/String;

    invoke-static {v1}, Ljwj;->a(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_c

    move-result-object v1

    .line 149
    const/16 v0, 0x800

    :try_start_2
    new-array v3, v0, [B

    .line 152
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_6

    move-result-wide v4

    .line 154
    :try_start_3
    invoke-virtual {v1, v3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 155
    :goto_3
    if-lez v0, :cond_9

    .line 156
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 157
    :try_start_4
    iget-object v6, p0, Ljxi;->a:Ljava/io/RandomAccessFile;

    iget-wide v8, p0, Ljxi;->b:J

    invoke-virtual {v6, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 158
    iget-object v6, p0, Ljxi;->a:Ljava/io/RandomAccessFile;

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v7, v0}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 159
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 160
    :try_start_5
    iget-object v6, p0, Ljxi;->h:Ljxg;

    monitor-enter v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 166
    :try_start_6
    iget v7, p0, Ljxi;->g:I

    if-nez v7, :cond_8

    iget-object v7, p0, Ljxi;->h:Ljxg;

    iget-object v7, v7, Ljxg;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v7}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 167
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-wide v10, p0, Ljxi;->f:J

    cmp-long v7, v8, v10

    if-lez v7, :cond_8

    .line 168
    :cond_2
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Ljxi;->a(I)V

    .line 169
    monitor-exit v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 179
    :try_start_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 178
    invoke-static {v4, v5}, Ljwn;->c(J)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 202
    iget v0, p0, Ljxi;->c:I

    if-eq v0, v12, :cond_3

    invoke-static {v1}, Ljwj;->a(Ljava/io/InputStream;)V

    .line 203
    :cond_3
    invoke-static {v1}, Lifu;->a(Ljava/io/Closeable;)V

    .line 204
    iget-object v1, p0, Ljxi;->h:Ljxg;

    monitor-enter v1

    .line 205
    :try_start_8
    iget v0, p0, Ljxi;->g:I

    if-nez v0, :cond_4

    .line 206
    iget-object v0, p0, Ljxi;->a:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lifu;->a(Ljava/io/Closeable;)V

    .line 207
    iget v0, p0, Ljxi;->c:I

    if-eq v0, v12, :cond_4

    iget-object v0, p0, Ljxi;->e:Ljava/io/File;

    invoke-static {v0}, Ljxg;->a(Ljava/io/File;)V

    .line 209
    :cond_4
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 212
    const-string v0, "picasa.download.photo_video"

    invoke-static {v2, v0}, Ljwn;->a(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 140
    :catchall_0
    move-exception v0

    :try_start_9
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    throw v0

    .line 143
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 159
    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 178
    :catchall_2
    move-exception v0

    .line 179
    :try_start_c
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 178
    invoke-static {v4, v5}, Ljwn;->c(J)V

    throw v0
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    .line 194
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_4
    :try_start_d
    iget-object v3, p0, Ljxi;->h:Ljxg;

    monitor-enter v3
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_9

    .line 196
    const/16 v1, 0x8

    :try_start_e
    invoke-direct {p0, v1}, Ljxi;->a(I)V

    .line 197
    monitor-exit v3
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_8

    .line 202
    iget v1, p0, Ljxi;->c:I

    if-eq v1, v12, :cond_6

    invoke-static {v0}, Ljwj;->a(Ljava/io/InputStream;)V

    .line 203
    :cond_6
    invoke-static {v0}, Lifu;->a(Ljava/io/Closeable;)V

    .line 204
    iget-object v1, p0, Ljxi;->h:Ljxg;

    monitor-enter v1

    .line 205
    :try_start_f
    iget v0, p0, Ljxi;->g:I

    if-nez v0, :cond_7

    .line 206
    iget-object v0, p0, Ljxi;->a:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lifu;->a(Ljava/io/Closeable;)V

    .line 207
    iget v0, p0, Ljxi;->c:I

    if-eq v0, v12, :cond_7

    iget-object v0, p0, Ljxi;->e:Ljava/io/File;

    invoke-static {v0}, Ljxg;->a(Ljava/io/File;)V

    .line 209
    :cond_7
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_a

    .line 212
    const-string v0, "picasa.download.photo_video"

    invoke-static {v2, v0}, Ljwn;->a(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 209
    :catchall_3
    move-exception v0

    :try_start_10
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    throw v0

    .line 172
    :cond_8
    :try_start_11
    iget-wide v8, p0, Ljxi;->b:J

    int-to-long v10, v0

    add-long/2addr v8, v10

    iput-wide v8, p0, Ljxi;->b:J

    .line 173
    iget-object v0, p0, Ljxi;->h:Ljxg;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 174
    monitor-exit v6
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    .line 175
    :try_start_12
    invoke-virtual {v1, v3}, Ljava/io/InputStream;->read([B)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    move-result v0

    goto/16 :goto_3

    .line 174
    :catchall_4
    move-exception v0

    :try_start_13
    monitor-exit v6
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    :try_start_14
    throw v0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    .line 179
    :cond_9
    :try_start_15
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 178
    invoke-static {v4, v5}, Ljwn;->c(J)V
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_0
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    .line 183
    :try_start_16
    iget-object v0, p0, Ljxi;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    .line 184
    iget-object v0, p0, Ljxi;->h:Ljxg;

    iget-object v0, v0, Ljxg;->c:Ljxh;

    iget-object v3, p0, Ljxi;->d:Ljava/lang/String;

    iget-object v4, p0, Ljxi;->e:Ljava/io/File;

    invoke-interface {v0, v3, v4}, Ljxh;->a(Ljava/lang/String;Ljava/io/File;)V
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_2
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    .line 187
    :goto_5
    :try_start_17
    iget-object v3, p0, Ljxi;->h:Ljxg;

    monitor-enter v3
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_0
    .catchall {:try_start_17 .. :try_end_17} :catchall_6

    .line 191
    const/4 v0, 0x4

    :try_start_18
    invoke-direct {p0, v0}, Ljxi;->a(I)V

    .line 192
    monitor-exit v3
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_5

    .line 202
    iget v0, p0, Ljxi;->c:I

    if-eq v0, v12, :cond_a

    invoke-static {v1}, Ljwj;->a(Ljava/io/InputStream;)V

    .line 203
    :cond_a
    invoke-static {v1}, Lifu;->a(Ljava/io/Closeable;)V

    .line 204
    iget-object v1, p0, Ljxi;->h:Ljxg;

    monitor-enter v1

    .line 205
    :try_start_19
    iget v0, p0, Ljxi;->g:I

    if-nez v0, :cond_b

    .line 206
    iget-object v0, p0, Ljxi;->a:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lifu;->a(Ljava/io/Closeable;)V

    .line 207
    iget v0, p0, Ljxi;->c:I

    if-eq v0, v12, :cond_b

    iget-object v0, p0, Ljxi;->e:Ljava/io/File;

    invoke-static {v0}, Ljxg;->a(Ljava/io/File;)V

    .line 209
    :cond_b
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_7

    .line 212
    const-string v0, "picasa.download.photo_video"

    invoke-static {v2, v0}, Ljwn;->a(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 192
    :catchall_5
    move-exception v0

    :try_start_1a
    monitor-exit v3
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_5

    :try_start_1b
    throw v0
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_0
    .catchall {:try_start_1b .. :try_end_1b} :catchall_6

    .line 202
    :catchall_6
    move-exception v0

    :goto_6
    iget v3, p0, Ljxi;->c:I

    if-eq v3, v12, :cond_c

    invoke-static {v1}, Ljwj;->a(Ljava/io/InputStream;)V

    .line 203
    :cond_c
    invoke-static {v1}, Lifu;->a(Ljava/io/Closeable;)V

    .line 204
    iget-object v1, p0, Ljxi;->h:Ljxg;

    monitor-enter v1

    .line 205
    :try_start_1c
    iget v3, p0, Ljxi;->g:I

    if-nez v3, :cond_d

    .line 206
    iget-object v3, p0, Ljxi;->a:Ljava/io/RandomAccessFile;

    invoke-static {v3}, Lifu;->a(Ljava/io/Closeable;)V

    .line 207
    iget v3, p0, Ljxi;->c:I

    if-eq v3, v12, :cond_d

    iget-object v3, p0, Ljxi;->e:Ljava/io/File;

    invoke-static {v3}, Ljxg;->a(Ljava/io/File;)V

    .line 209
    :cond_d
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_b

    .line 212
    const-string v1, "picasa.download.photo_video"

    invoke-static {v2, v1}, Ljwn;->a(ILjava/lang/String;)V

    throw v0

    .line 209
    :catchall_7
    move-exception v0

    :try_start_1d
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_7

    throw v0

    .line 197
    :catchall_8
    move-exception v1

    :try_start_1e
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_8

    :try_start_1f
    throw v1
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_9

    .line 202
    :catchall_9
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_6

    .line 209
    :catchall_a
    move-exception v0

    :try_start_20
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_a

    throw v0

    :catchall_b
    move-exception v0

    :try_start_21
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_b

    throw v0

    .line 202
    :catchall_c
    move-exception v1

    move-object v13, v1

    move-object v1, v0

    move-object v0, v13

    goto :goto_6

    .line 194
    :catch_1
    move-exception v1

    goto/16 :goto_4

    :catch_2
    move-exception v0

    goto :goto_5
.end method
