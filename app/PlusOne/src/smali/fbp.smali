.class public final Lfbp;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 48
    iput-object p3, p0, Lfbp;->a:Ljava/lang/String;

    .line 49
    iput p4, p0, Lfbp;->b:I

    .line 50
    iput-object p5, p0, Lfbp;->c:Landroid/content/Intent;

    .line 51
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 5

    .prologue
    .line 55
    const-wide/16 v0, 0x0

    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 57
    iget-object v3, p0, Lfbp;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    invoke-virtual {p0}, Lfbp;->f()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lfbp;->b:I

    invoke-static {v3, v4, v2}, Ljvj;->a(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 61
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 62
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljad;

    invoke-virtual {v0}, Ljad;->b()J

    move-result-wide v0

    .line 65
    :cond_0
    iget-object v2, p0, Lfbp;->c:Landroid/content/Intent;

    const-string v3, "picasa_photo_id"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 67
    new-instance v0, Lhoz;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lhoz;-><init>(Z)V

    .line 68
    invoke-virtual {v0}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "result_intent"

    iget-object v3, p0, Lfbp;->c:Landroid/content/Intent;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    return-object v0
.end method
