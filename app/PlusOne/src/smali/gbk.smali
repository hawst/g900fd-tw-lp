.class public final Lgbk;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Lozp;

.field private c:Lgbj;

.field private d:Lfyd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    sget-object v0, Lgbk;->a:Llct;

    if-nez v0, :cond_0

    .line 56
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lgbk;->a:Llct;

    .line 58
    :cond_0
    new-instance v0, Lgbj;

    invoke-direct {v0, p1}, Lgbj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbk;->c:Lgbj;

    .line 59
    new-instance v0, Lfyd;

    invoke-direct {v0, p1, p2, p3}, Lfyd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbk;->d:Lfyd;

    .line 60
    iget-object v0, p0, Lgbk;->d:Lfyd;

    invoke-virtual {v0, p0}, Lfyd;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void
.end method

.method private d()Loyy;
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lgbk;->b:Lozp;

    iget-object v0, v0, Lozp;->n:Loya;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbk;->b:Lozp;

    iget-object v0, v0, Lozp;->n:Loya;

    sget-object v1, Loyy;->a:Loxr;

    .line 147
    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lgbk;->c:Lgbj;

    invoke-virtual {v0}, Lgbj;->a()V

    .line 92
    iget-object v0, p0, Lgbk;->d:Lfyd;

    invoke-virtual {v0}, Lfyd;->a()V

    .line 93
    invoke-virtual {p0}, Lgbk;->removeAllViews()V

    .line 94
    return-void
.end method

.method public a(Lozp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    .line 76
    invoke-virtual {p0}, Lgbk;->removeAllViews()V

    .line 77
    iput-object p1, p0, Lgbk;->b:Lozp;

    .line 79
    iget-object v0, p0, Lgbk;->b:Lozp;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lgbk;->d:Lfyd;

    invoke-virtual {p0, v0}, Lgbk;->addView(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lgbk;->d:Lfyd;

    iget-object v1, p0, Lgbk;->b:Lozp;

    invoke-virtual {v0, v1, p2, p3, v6}, Lfyd;->a(Lozp;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 83
    invoke-virtual {p0, v6}, Lgbk;->setBackgroundColor(I)V

    .line 84
    iget-object v7, p0, Lgbk;->c:Lgbj;

    iget-object v8, p0, Lgbk;->b:Lozp;

    iget-object v0, p0, Lgbk;->b:Lozp;

    iget-object v0, v0, Lozp;->n:Loya;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lgbk;->d()Loyy;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lgbk;->d()Loyy;

    move-result-object v0

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    move-wide v2, v0

    :goto_0
    iget-object v0, p0, Lgbk;->b:Lozp;

    iget-object v0, v0, Lozp;->o:Loya;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgbk;->b:Lozp;

    iget-object v0, v0, Lozp;->o:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    :goto_1
    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    if-eqz v0, :cond_0

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-gtz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v7, v8, p2, v0}, Lgbj;->a(Lozp;Ljava/lang/String;Z)V

    .line 85
    iget-object v0, p0, Lgbk;->c:Lgbj;

    invoke-virtual {p0, v0}, Lgbk;->addView(Landroid/view/View;)V

    .line 87
    :cond_1
    return-void

    :cond_2
    move-wide v2, v4

    .line 84
    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move v0, v6

    goto :goto_2
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lgbk;->b:Lozp;

    if-nez v0, :cond_0

    .line 66
    const-string v0, ""

    .line 71
    :goto_0
    return-object v0

    .line 68
    :cond_0
    iget-object v0, p0, Lgbk;->c:Lgbj;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lgbk;->c:Lgbj;

    invoke-virtual {v0}, Lgbj;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {p0}, Lgbk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a096a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected c()I
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Lgbk;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 142
    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    return v0
.end method

.method public synthetic getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lgbk;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 132
    invoke-virtual {p0}, Lgbk;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 133
    invoke-virtual {p0}, Lgbk;->c()I

    move-result v2

    .line 135
    iget-object v0, p0, Lgbk;->b:Lozp;

    iget-object v3, v0, Lozp;->g:Ljava/lang/String;

    iget-object v0, p0, Lgbk;->b:Lozp;

    iget-object v4, v0, Lozp;->d:Ljava/lang/String;

    const-class v0, Lieh;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v5, Ldxd;->w:Lief;

    invoke-interface {v0, v5, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v1, v2, v3, v4, v6}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 137
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 138
    return-void

    .line 135
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v5, "com.google.android.libraries.social.showtimerefresh.VIEW_HOA_EVENT_DETAILS"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "event_id"

    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account_id"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "owner_id"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "invitation_token"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "auth_key"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "notif_type"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "com.google.android.libraries.social.notifications.notif_id"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "rsvp"

    const/high16 v3, -0x80000000

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 102
    iget-object v0, p0, Lgbk;->b:Lozp;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lgbk;->d:Lfyd;

    sget-object v1, Lgbk;->a:Llct;

    iget v1, v1, Llct;->m:I

    iget-object v2, p0, Lgbk;->d:Lfyd;

    .line 104
    invoke-virtual {v2}, Lfyd;->getMeasuredWidth()I

    move-result v2

    sget-object v3, Lgbk;->a:Llct;

    iget v3, v3, Llct;->m:I

    iget-object v4, p0, Lgbk;->d:Lfyd;

    .line 106
    invoke-virtual {v4}, Lfyd;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 103
    invoke-virtual {v0, v5, v1, v2, v3}, Lfyd;->layout(IIII)V

    .line 107
    sget-object v0, Lgbk;->a:Llct;

    iget v0, v0, Llct;->m:I

    iget-object v1, p0, Lgbk;->d:Lfyd;

    .line 108
    invoke-virtual {v1}, Lfyd;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    iget-object v1, p0, Lgbk;->c:Lgbj;

    iget-object v2, p0, Lgbk;->c:Lgbj;

    invoke-virtual {v2}, Lgbj;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lgbk;->c:Lgbj;

    .line 110
    invoke-virtual {v3}, Lgbj;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 109
    invoke-virtual {v1, v5, v0, v2, v3}, Lgbj;->layout(IIII)V

    .line 112
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    const/4 v0, 0x0

    .line 116
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 118
    iget-object v2, p0, Lgbk;->b:Lozp;

    if-eqz v2, :cond_0

    .line 119
    iget-object v2, p0, Lgbk;->d:Lfyd;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 120
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 119
    invoke-virtual {v2, v3, v4}, Lfyd;->measure(II)V

    .line 121
    iget-object v2, p0, Lgbk;->c:Lgbj;

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 122
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 121
    invoke-virtual {v2, v3, v0}, Lgbj;->measure(II)V

    .line 123
    sget-object v0, Lgbk;->a:Llct;

    iget v0, v0, Llct;->m:I

    iget-object v2, p0, Lgbk;->c:Lgbj;

    invoke-virtual {v2}, Lgbj;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lgbk;->d:Lfyd;

    .line 124
    invoke-virtual {v2}, Lfyd;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_0
    invoke-virtual {p0, v1, v0}, Lgbk;->setMeasuredDimension(II)V

    .line 128
    return-void
.end method
