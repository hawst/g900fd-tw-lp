.class public final Lmfn;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnvx;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5830
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5833
    iput-object v0, p0, Lmfn;->apiHeader:Llyr;

    .line 5836
    iput-object v0, p0, Lmfn;->a:Lnvx;

    .line 5830
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5853
    const/4 v0, 0x0

    .line 5854
    iget-object v1, p0, Lmfn;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 5855
    const/4 v0, 0x1

    iget-object v1, p0, Lmfn;->apiHeader:Llyr;

    .line 5856
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5858
    :cond_0
    iget-object v1, p0, Lmfn;->a:Lnvx;

    if-eqz v1, :cond_1

    .line 5859
    const/4 v1, 0x2

    iget-object v2, p0, Lmfn;->a:Lnvx;

    .line 5860
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5862
    :cond_1
    iget-object v1, p0, Lmfn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5863
    iput v0, p0, Lmfn;->ai:I

    .line 5864
    return v0
.end method

.method public a(Loxn;)Lmfn;
    .locals 2

    .prologue
    .line 5872
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5873
    sparse-switch v0, :sswitch_data_0

    .line 5877
    iget-object v1, p0, Lmfn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5878
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfn;->ah:Ljava/util/List;

    .line 5881
    :cond_1
    iget-object v1, p0, Lmfn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5883
    :sswitch_0
    return-object p0

    .line 5888
    :sswitch_1
    iget-object v0, p0, Lmfn;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 5889
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfn;->apiHeader:Llyr;

    .line 5891
    :cond_2
    iget-object v0, p0, Lmfn;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5895
    :sswitch_2
    iget-object v0, p0, Lmfn;->a:Lnvx;

    if-nez v0, :cond_3

    .line 5896
    new-instance v0, Lnvx;

    invoke-direct {v0}, Lnvx;-><init>()V

    iput-object v0, p0, Lmfn;->a:Lnvx;

    .line 5898
    :cond_3
    iget-object v0, p0, Lmfn;->a:Lnvx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5873
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5841
    iget-object v0, p0, Lmfn;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 5842
    const/4 v0, 0x1

    iget-object v1, p0, Lmfn;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5844
    :cond_0
    iget-object v0, p0, Lmfn;->a:Lnvx;

    if-eqz v0, :cond_1

    .line 5845
    const/4 v0, 0x2

    iget-object v1, p0, Lmfn;->a:Lnvx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5847
    :cond_1
    iget-object v0, p0, Lmfn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5849
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5826
    invoke-virtual {p0, p1}, Lmfn;->a(Loxn;)Lmfn;

    move-result-object v0

    return-object v0
.end method
