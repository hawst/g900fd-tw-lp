.class public final Leng;
.super Lenl;
.source "PG"


# instance fields
.field private aA:Landroid/view/ViewGroup;

.field private aB:Landroid/widget/EditText;

.field private aC:Landroid/widget/TextView;

.field private aD:Landroid/widget/TextView;

.field private aE:Landroid/widget/Spinner;

.field private an:Lnip;

.field private ao:Lnip;

.field private ap:Ljnw;

.field private aq:Landroid/view/ViewGroup;

.field private ar:Landroid/widget/TextView;

.field private as:Landroid/view/ViewGroup;

.field private aw:Landroid/widget/RadioGroup;

.field private ax:[Ljava/lang/String;

.field private ay:[Ljava/lang/String;

.field private az:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lenl;-><init>()V

    .line 418
    return-void
.end method

.method static synthetic a(Leng;)Landroid/widget/RadioGroup;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    return-object v0
.end method

.method private a(Landroid/widget/RadioGroup;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 287
    new-instance v0, Landroid/widget/RadioButton;

    iget-object v1, p0, Leng;->at:Llnl;

    invoke-direct {v0, v1}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    .line 288
    invoke-virtual {v0, p2}, Landroid/widget/RadioButton;->setId(I)V

    .line 289
    invoke-virtual {v0, p3}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 290
    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 291
    return-void
.end method

.method static synthetic a(Leng;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Leng;->f(I)V

    return-void
.end method

.method private al()Lnip;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lnip;

    invoke-direct {v0}, Lnip;-><init>()V

    .line 75
    iget-object v1, p0, Leng;->aw:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    .line 76
    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_1

    .line 77
    iget-object v1, p0, Leng;->aE:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    iput v1, v0, Lnip;->b:I

    .line 78
    iget-object v1, p0, Leng;->aB:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnip;->c:Ljava/lang/String;

    .line 82
    :goto_0
    iget v1, v0, Lnip;->b:I

    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_0

    .line 83
    const/4 v1, 0x0

    iput v1, v0, Lnip;->b:I

    .line 85
    :cond_0
    iget-object v1, p0, Leng;->ao:Lnip;

    invoke-virtual {p0}, Leng;->aa()Lnja;

    move-result-object v2

    iput-object v2, v1, Lnip;->a:Lnja;

    .line 86
    iget-object v1, p0, Leng;->ao:Lnip;

    iget-object v1, v1, Lnip;->a:Lnja;

    iput-object v1, v0, Lnip;->a:Lnja;

    .line 87
    return-object v0

    .line 80
    :cond_1
    iget-object v1, p0, Leng;->aw:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    iput v1, v0, Lnip;->b:I

    goto :goto_0
.end method

.method static synthetic b(Leng;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Leng;->e(I)V

    return-void
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Leng;->ay:[Ljava/lang/String;

    aget-object v0, v0, p1

    .line 295
    iget-object v1, p0, Leng;->aD:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v1, p0, Leng;->aD:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 297
    return-void

    .line 296
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(I)V
    .locals 4

    .prologue
    const/16 v1, 0x3eb

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 300
    if-eq p1, v1, :cond_1

    iget-object v0, p0, Leng;->aA:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 301
    iget-object v0, p0, Leng;->aB:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v0, p0, Leng;->aE:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 303
    iget-object v0, p0, Leng;->aA:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 304
    iget-object v0, p0, Leng;->aB:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    if-ne p1, v1, :cond_0

    .line 306
    iget-object v0, p0, Leng;->aA:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Leng;->aB:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public U()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 240
    invoke-super {p0}, Lenl;->U()V

    .line 243
    iget-object v0, p0, Leng;->ap:Ljnw;

    if-nez v0, :cond_0

    .line 270
    :goto_0
    return-void

    .line 247
    :cond_0
    const-string v4, ""

    .line 249
    iget-object v0, p0, Leng;->ao:Lnip;

    iget v0, v0, Lnip;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Leng;->ao:Lnip;

    iget v0, v0, Lnip;->b:I

    .line 251
    :goto_1
    iget-object v2, p0, Leng;->ao:Lnip;

    iget-object v2, v2, Lnip;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 253
    const/16 v2, 0x3eb

    .line 254
    iget-object v1, p0, Leng;->ao:Lnip;

    iget-object v4, v1, Lnip;->c:Ljava/lang/String;

    move v3, v0

    .line 257
    :goto_2
    new-instance v0, Leni;

    iget-object v1, p0, Leng;->aw:Landroid/widget/RadioGroup;

    iget-object v5, p0, Leng;->aE:Landroid/widget/Spinner;

    iget-object v6, p0, Leng;->aB:Landroid/widget/EditText;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Leni;-><init>(Leng;IILjava/lang/String;Landroid/widget/Spinner;Landroid/widget/EditText;)V

    .line 259
    new-instance v1, Lenj;

    iget-object v4, p0, Leng;->aw:Landroid/widget/RadioGroup;

    invoke-direct {v1, p0, v2, v0, v4}, Lenj;-><init>(Leng;ILeni;Landroid/widget/RadioGroup;)V

    .line 261
    iget-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 263
    new-instance v2, Lenk;

    move-object v0, v1

    check-cast v0, Lenj;

    invoke-direct {v2, p0, v3, v0}, Lenk;-><init>(Leng;ILenj;)V

    .line 265
    iget-object v0, p0, Leng;->aE:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 267
    new-instance v0, Lenh;

    iget-object v2, p0, Leng;->aB:Landroid/widget/EditText;

    check-cast v1, Lenj;

    invoke-direct {v0, p0, v2, v1}, Lenh;-><init>(Leng;Landroid/view/View;Lenj;)V

    .line 269
    iget-object v1, p0, Leng;->aB:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 249
    goto :goto_1

    :cond_2
    move v2, v0

    move v3, v1

    goto :goto_2
.end method

.method protected V()V
    .locals 3

    .prologue
    .line 274
    invoke-super {p0}, Lenl;->V()V

    .line 279
    new-instance v0, Lnjt;

    invoke-direct {v0}, Lnjt;-><init>()V

    .line 280
    new-instance v1, Lnkd;

    invoke-direct {v1}, Lnkd;-><init>()V

    iput-object v1, v0, Lnjt;->e:Lnkd;

    .line 281
    iget-object v1, v0, Lnjt;->e:Lnkd;

    invoke-direct {p0}, Leng;->al()Lnip;

    move-result-object v2

    iput-object v2, v1, Lnkd;->d:Lnip;

    .line 283
    invoke-virtual {p0, v0}, Leng;->a(Lnjt;)V

    .line 284
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Leng;->an:Lnip;

    iget-object v0, v0, Lnip;->a:Lnja;

    .line 93
    iget-object v1, p0, Leng;->ao:Lnip;

    iget-object v1, v1, Lnip;->a:Lnja;

    .line 95
    invoke-virtual {p0, v0}, Leng;->a(Lnja;)Lhgw;

    move-result-object v0

    invoke-static {v0}, Lhgw;->a(Lhgw;)Lhgw;

    move-result-object v0

    iput-object v0, p0, Leng;->Y:Lhgw;

    .line 96
    iget-object v0, p0, Leng;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h()Lhgw;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhgw;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    :cond_0
    invoke-virtual {p0, v1}, Leng;->a(Lnja;)Lhgw;

    move-result-object v0

    invoke-static {v0}, Lhgw;->a(Lhgw;)Lhgw;

    move-result-object v0

    .line 100
    :cond_1
    invoke-static {v0}, Lhgw;->a(Lhgw;)Lhgw;

    move-result-object v0

    .line 101
    iget-object v1, p0, Leng;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Lhgw;)V

    .line 103
    iget-object v0, p0, Leng;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->setEnabled(Z)V

    .line 104
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0}, Leng;->k()Landroid/os/Bundle;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Leng;->ap:Ljnw;

    iget-object v0, p0, Leng;->au:Llnh;

    const-class v1, Ljnv;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljnv;

    if-eqz v0, :cond_0

    iget-object v1, p0, Leng;->at:Llnl;

    invoke-interface {v0, v1}, Ljnv;->a(Landroid/content/Context;)Ljnw;

    move-result-object v0

    iput-object v0, p0, Leng;->ap:Ljnw;

    .line 64
    :cond_0
    invoke-super {p0, p1}, Lenl;->a(Landroid/os/Bundle;)V

    .line 65
    return-void
.end method

.method protected a()[B
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Leng;->al()Lnip;

    move-result-object v0

    .line 70
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    return-object v0
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Leng;->Z:[B

    if-eqz v0, :cond_0

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Leng;->ao:Lnip;

    .line 111
    :try_start_0
    new-instance v0, Lnip;

    invoke-direct {v0}, Lnip;-><init>()V

    iget-object v1, p0, Leng;->Z:[B

    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnip;

    iput-object v0, p0, Leng;->ao:Lnip;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    iget-object v0, p0, Leng;->ao:Lnip;

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lnip;

    invoke-direct {v0}, Lnip;-><init>()V

    iput-object v0, p0, Leng;->ao:Lnip;

    .line 119
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected d()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 123
    invoke-super {p0}, Lenl;->d()V

    .line 127
    iget-object v0, p0, Leng;->aa:[B

    if-eqz v0, :cond_2

    .line 128
    iput-object v1, p0, Leng;->an:Lnip;

    .line 130
    :try_start_0
    new-instance v0, Lnip;

    invoke-direct {v0}, Lnip;-><init>()V

    iget-object v2, p0, Leng;->aa:[B

    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnip;

    iput-object v0, p0, Leng;->an:Lnip;

    .line 131
    iget-object v0, p0, Leng;->an:Lnip;

    iget-object v0, v0, Lnip;->a:Lnja;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    iget-object v1, p0, Leng;->an:Lnip;

    if-nez v1, :cond_0

    .line 136
    new-instance v1, Lnip;

    invoke-direct {v1}, Lnip;-><init>()V

    iput-object v1, p0, Leng;->an:Lnip;

    .line 140
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    iget-object v1, v0, Lnja;->a:Lock;

    if-eqz v1, :cond_1

    .line 142
    iget-object v0, v0, Lnja;->a:Lock;

    iget-object v0, v0, Lock;->b:Locn;

    iput-object v0, p0, Leng;->X:Locn;

    .line 144
    :cond_1
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method protected e()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 148
    invoke-super {p0}, Lenl;->e()V

    .line 151
    iget-object v0, p0, Leng;->ap:Ljnw;

    if-nez v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 154
    :cond_0
    invoke-virtual {p0}, Leng;->o()Landroid/content/res/Resources;

    move-result-object v2

    .line 156
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Leng;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0401b4

    iget-object v4, p0, Leng;->ai:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leng;->aq:Landroid/view/ViewGroup;

    .line 158
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 160
    sget v3, Leng;->R:I

    sget v4, Leng;->R:I

    sget v5, Leng;->R:I

    sget v6, Leng;->R:I

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 162
    iget-object v3, p0, Leng;->ai:Landroid/widget/LinearLayout;

    iget-object v4, p0, Leng;->aq:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 164
    iget-object v0, p0, Leng;->aq:Landroid/view/ViewGroup;

    const v3, 0x7f100515

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leng;->az:Landroid/view/ViewGroup;

    .line 165
    iget-object v0, p0, Leng;->az:Landroid/view/ViewGroup;

    const v3, 0x7f100516

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leng;->ar:Landroid/widget/TextView;

    .line 166
    iget-object v0, p0, Leng;->az:Landroid/view/ViewGroup;

    const v3, 0x7f100517

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leng;->as:Landroid/view/ViewGroup;

    .line 168
    iget-object v0, p0, Leng;->az:Landroid/view/ViewGroup;

    const v3, 0x7f0200be

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 170
    iget-object v0, p0, Leng;->as:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 172
    iget-object v0, p0, Leng;->ar:Landroid/widget/TextView;

    const v3, 0x7f0a031d

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 173
    iget-object v0, p0, Leng;->ar:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    new-instance v0, Landroid/widget/RadioGroup;

    iget-object v3, p0, Leng;->at:Llnl;

    invoke-direct {v0, v3}, Landroid/widget/RadioGroup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    .line 176
    iget-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v7}, Landroid/widget/RadioGroup;->setOrientation(I)V

    .line 177
    iget-object v0, p0, Leng;->as:Landroid/view/ViewGroup;

    iget-object v3, p0, Leng;->aw:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 178
    iget-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    const v3, 0x7f0a031f

    .line 179
    invoke-virtual {p0, v3}, Leng;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 178
    invoke-direct {p0, v0, v7, v3}, Leng;->a(Landroid/widget/RadioGroup;ILjava/lang/String;)V

    .line 180
    iget-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    const v3, 0x7f0a0320

    .line 181
    invoke-virtual {p0, v3}, Leng;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 180
    invoke-direct {p0, v0, v8, v3}, Leng;->a(Landroid/widget/RadioGroup;ILjava/lang/String;)V

    .line 182
    iget-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    iget-object v3, p0, Leng;->ap:Ljnw;

    .line 183
    invoke-interface {v3}, Ljnw;->h()Ljava/lang/String;

    move-result-object v3

    .line 182
    invoke-direct {p0, v0, v9, v3}, Leng;->a(Landroid/widget/RadioGroup;ILjava/lang/String;)V

    .line 184
    iget-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    const/16 v3, 0x3eb

    iget-object v4, p0, Leng;->ap:Ljnw;

    .line 185
    invoke-interface {v4}, Ljnw;->a()Ljava/lang/String;

    move-result-object v4

    .line 184
    invoke-direct {p0, v0, v3, v4}, Leng;->a(Landroid/widget/RadioGroup;ILjava/lang/String;)V

    .line 186
    new-instance v0, Landroid/widget/EditText;

    iget-object v3, p0, Leng;->at:Llnl;

    invoke-direct {v0, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leng;->aB:Landroid/widget/EditText;

    .line 187
    iget-object v0, p0, Leng;->aB:Landroid/widget/EditText;

    iget-object v3, p0, Leng;->ap:Ljnw;

    invoke-interface {v3}, Ljnw;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 189
    new-array v0, v7, [Landroid/text/InputFilter;

    .line 190
    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/16 v4, 0x32

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v0, v1

    .line 191
    iget-object v3, p0, Leng;->aB:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 192
    iget-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    iget-object v3, p0, Leng;->aB:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 196
    new-array v0, v10, [Ljava/lang/String;

    iget-object v3, p0, Leng;->ap:Ljnw;

    .line 197
    invoke-interface {v3}, Ljnw;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const v3, 0x7f0a031f

    .line 198
    invoke-virtual {p0, v3}, Leng;->e_(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v7

    const v3, 0x7f0a0320

    .line 199
    invoke-virtual {p0, v3}, Leng;->e_(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v8

    const v3, 0x7f0a0321

    .line 200
    invoke-virtual {p0, v3}, Leng;->e_(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v9

    iput-object v0, p0, Leng;->ax:[Ljava/lang/String;

    .line 201
    new-array v0, v10, [Ljava/lang/String;

    iget-object v3, p0, Leng;->ap:Ljnw;

    .line 202
    invoke-interface {v3}, Ljnw;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v3, p0, Leng;->ap:Ljnw;

    .line 203
    invoke-interface {v3}, Ljnw;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v7

    iget-object v3, p0, Leng;->ap:Ljnw;

    .line 204
    invoke-interface {v3}, Ljnw;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v8

    iget-object v3, p0, Leng;->ap:Ljnw;

    .line 205
    invoke-interface {v3}, Ljnw;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v9

    iput-object v0, p0, Leng;->ay:[Ljava/lang/String;

    .line 207
    iget-object v0, p0, Leng;->aq:Landroid/view/ViewGroup;

    const v3, 0x7f100518

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leng;->aA:Landroid/view/ViewGroup;

    .line 208
    iget-object v0, p0, Leng;->aA:Landroid/view/ViewGroup;

    const v3, 0x7f100519

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leng;->aC:Landroid/widget/TextView;

    .line 209
    iget-object v0, p0, Leng;->aA:Landroid/view/ViewGroup;

    const v3, 0x7f10051b

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Leng;->aE:Landroid/widget/Spinner;

    .line 210
    iget-object v0, p0, Leng;->aA:Landroid/view/ViewGroup;

    const v3, 0x7f10051c

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leng;->aD:Landroid/widget/TextView;

    .line 211
    const v0, 0x7f0d0136

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 212
    iget-object v0, p0, Leng;->aA:Landroid/view/ViewGroup;

    const v3, 0x7f0200c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 213
    iget-object v0, p0, Leng;->aC:Landroid/widget/TextView;

    iget-object v2, p0, Leng;->ap:Ljnw;

    invoke-interface {v2}, Ljnw;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Leng;->at:Llnl;

    const v3, 0x7f0401eb

    iget-object v4, p0, Leng;->ax:[Ljava/lang/String;

    invoke-direct {v0, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 216
    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 217
    iget-object v2, p0, Leng;->aE:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 219
    const-string v2, ""

    .line 221
    iget-object v0, p0, Leng;->ao:Lnip;

    iget v0, v0, Lnip;->b:I

    const/high16 v3, -0x80000000

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Leng;->ao:Lnip;

    iget v0, v0, Lnip;->b:I

    .line 223
    :goto_1
    iget-object v3, p0, Leng;->ao:Lnip;

    iget-object v3, v3, Lnip;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 225
    const/16 v1, 0x3eb

    .line 226
    iget-object v2, p0, Leng;->ao:Lnip;

    iget-object v2, v2, Lnip;->c:Ljava/lang/String;

    move-object v3, v2

    move v2, v0

    .line 228
    :goto_2
    iget-object v0, p0, Leng;->aw:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 229
    if-eqz v0, :cond_1

    .line 230
    invoke-virtual {v0, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 232
    :cond_1
    iget-object v0, p0, Leng;->aE:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 233
    iget-object v0, p0, Leng;->aB:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 234
    invoke-direct {p0, v1}, Leng;->f(I)V

    .line 235
    invoke-direct {p0, v2}, Leng;->e(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 221
    goto :goto_1

    :cond_3
    move-object v3, v2

    move v2, v1

    move v1, v0

    goto :goto_2
.end method
