.class public final Lnif;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnka;

.field public b:[Lnjy;

.field public c:[Lnjx;

.field public d:[Lnjz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7287
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7290
    sget-object v0, Lnka;->a:[Lnka;

    iput-object v0, p0, Lnif;->a:[Lnka;

    .line 7293
    sget-object v0, Lnjy;->a:[Lnjy;

    iput-object v0, p0, Lnif;->b:[Lnjy;

    .line 7296
    sget-object v0, Lnjx;->a:[Lnjx;

    iput-object v0, p0, Lnif;->c:[Lnjx;

    .line 7299
    sget-object v0, Lnjz;->a:[Lnjz;

    iput-object v0, p0, Lnif;->d:[Lnjz;

    .line 7287
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 7338
    .line 7339
    iget-object v0, p0, Lnif;->a:[Lnka;

    if-eqz v0, :cond_1

    .line 7340
    iget-object v3, p0, Lnif;->a:[Lnka;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 7341
    if-eqz v5, :cond_0

    .line 7342
    const/4 v6, 0x1

    .line 7343
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7340
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 7347
    :cond_2
    iget-object v2, p0, Lnif;->b:[Lnjy;

    if-eqz v2, :cond_4

    .line 7348
    iget-object v3, p0, Lnif;->b:[Lnjy;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 7349
    if-eqz v5, :cond_3

    .line 7350
    const/4 v6, 0x2

    .line 7351
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7348
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 7355
    :cond_4
    iget-object v2, p0, Lnif;->c:[Lnjx;

    if-eqz v2, :cond_6

    .line 7356
    iget-object v3, p0, Lnif;->c:[Lnjx;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 7357
    if-eqz v5, :cond_5

    .line 7358
    const/4 v6, 0x3

    .line 7359
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7356
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 7363
    :cond_6
    iget-object v2, p0, Lnif;->d:[Lnjz;

    if-eqz v2, :cond_8

    .line 7364
    iget-object v2, p0, Lnif;->d:[Lnjz;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 7365
    if-eqz v4, :cond_7

    .line 7366
    const/4 v5, 0x4

    .line 7367
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 7364
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 7371
    :cond_8
    iget-object v1, p0, Lnif;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7372
    iput v0, p0, Lnif;->ai:I

    .line 7373
    return v0
.end method

.method public a(Loxn;)Lnif;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7381
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7382
    sparse-switch v0, :sswitch_data_0

    .line 7386
    iget-object v2, p0, Lnif;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 7387
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnif;->ah:Ljava/util/List;

    .line 7390
    :cond_1
    iget-object v2, p0, Lnif;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7392
    :sswitch_0
    return-object p0

    .line 7397
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7398
    iget-object v0, p0, Lnif;->a:[Lnka;

    if-nez v0, :cond_3

    move v0, v1

    .line 7399
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnka;

    .line 7400
    iget-object v3, p0, Lnif;->a:[Lnka;

    if-eqz v3, :cond_2

    .line 7401
    iget-object v3, p0, Lnif;->a:[Lnka;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7403
    :cond_2
    iput-object v2, p0, Lnif;->a:[Lnka;

    .line 7404
    :goto_2
    iget-object v2, p0, Lnif;->a:[Lnka;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 7405
    iget-object v2, p0, Lnif;->a:[Lnka;

    new-instance v3, Lnka;

    invoke-direct {v3}, Lnka;-><init>()V

    aput-object v3, v2, v0

    .line 7406
    iget-object v2, p0, Lnif;->a:[Lnka;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7407
    invoke-virtual {p1}, Loxn;->a()I

    .line 7404
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 7398
    :cond_3
    iget-object v0, p0, Lnif;->a:[Lnka;

    array-length v0, v0

    goto :goto_1

    .line 7410
    :cond_4
    iget-object v2, p0, Lnif;->a:[Lnka;

    new-instance v3, Lnka;

    invoke-direct {v3}, Lnka;-><init>()V

    aput-object v3, v2, v0

    .line 7411
    iget-object v2, p0, Lnif;->a:[Lnka;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7415
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7416
    iget-object v0, p0, Lnif;->b:[Lnjy;

    if-nez v0, :cond_6

    move v0, v1

    .line 7417
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnjy;

    .line 7418
    iget-object v3, p0, Lnif;->b:[Lnjy;

    if-eqz v3, :cond_5

    .line 7419
    iget-object v3, p0, Lnif;->b:[Lnjy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7421
    :cond_5
    iput-object v2, p0, Lnif;->b:[Lnjy;

    .line 7422
    :goto_4
    iget-object v2, p0, Lnif;->b:[Lnjy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 7423
    iget-object v2, p0, Lnif;->b:[Lnjy;

    new-instance v3, Lnjy;

    invoke-direct {v3}, Lnjy;-><init>()V

    aput-object v3, v2, v0

    .line 7424
    iget-object v2, p0, Lnif;->b:[Lnjy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7425
    invoke-virtual {p1}, Loxn;->a()I

    .line 7422
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 7416
    :cond_6
    iget-object v0, p0, Lnif;->b:[Lnjy;

    array-length v0, v0

    goto :goto_3

    .line 7428
    :cond_7
    iget-object v2, p0, Lnif;->b:[Lnjy;

    new-instance v3, Lnjy;

    invoke-direct {v3}, Lnjy;-><init>()V

    aput-object v3, v2, v0

    .line 7429
    iget-object v2, p0, Lnif;->b:[Lnjy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7433
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7434
    iget-object v0, p0, Lnif;->c:[Lnjx;

    if-nez v0, :cond_9

    move v0, v1

    .line 7435
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lnjx;

    .line 7436
    iget-object v3, p0, Lnif;->c:[Lnjx;

    if-eqz v3, :cond_8

    .line 7437
    iget-object v3, p0, Lnif;->c:[Lnjx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7439
    :cond_8
    iput-object v2, p0, Lnif;->c:[Lnjx;

    .line 7440
    :goto_6
    iget-object v2, p0, Lnif;->c:[Lnjx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 7441
    iget-object v2, p0, Lnif;->c:[Lnjx;

    new-instance v3, Lnjx;

    invoke-direct {v3}, Lnjx;-><init>()V

    aput-object v3, v2, v0

    .line 7442
    iget-object v2, p0, Lnif;->c:[Lnjx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7443
    invoke-virtual {p1}, Loxn;->a()I

    .line 7440
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 7434
    :cond_9
    iget-object v0, p0, Lnif;->c:[Lnjx;

    array-length v0, v0

    goto :goto_5

    .line 7446
    :cond_a
    iget-object v2, p0, Lnif;->c:[Lnjx;

    new-instance v3, Lnjx;

    invoke-direct {v3}, Lnjx;-><init>()V

    aput-object v3, v2, v0

    .line 7447
    iget-object v2, p0, Lnif;->c:[Lnjx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7451
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7452
    iget-object v0, p0, Lnif;->d:[Lnjz;

    if-nez v0, :cond_c

    move v0, v1

    .line 7453
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lnjz;

    .line 7454
    iget-object v3, p0, Lnif;->d:[Lnjz;

    if-eqz v3, :cond_b

    .line 7455
    iget-object v3, p0, Lnif;->d:[Lnjz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7457
    :cond_b
    iput-object v2, p0, Lnif;->d:[Lnjz;

    .line 7458
    :goto_8
    iget-object v2, p0, Lnif;->d:[Lnjz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 7459
    iget-object v2, p0, Lnif;->d:[Lnjz;

    new-instance v3, Lnjz;

    invoke-direct {v3}, Lnjz;-><init>()V

    aput-object v3, v2, v0

    .line 7460
    iget-object v2, p0, Lnif;->d:[Lnjz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7461
    invoke-virtual {p1}, Loxn;->a()I

    .line 7458
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 7452
    :cond_c
    iget-object v0, p0, Lnif;->d:[Lnjz;

    array-length v0, v0

    goto :goto_7

    .line 7464
    :cond_d
    iget-object v2, p0, Lnif;->d:[Lnjz;

    new-instance v3, Lnjz;

    invoke-direct {v3}, Lnjz;-><init>()V

    aput-object v3, v2, v0

    .line 7465
    iget-object v2, p0, Lnif;->d:[Lnjz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7382
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 7304
    iget-object v1, p0, Lnif;->a:[Lnka;

    if-eqz v1, :cond_1

    .line 7305
    iget-object v2, p0, Lnif;->a:[Lnka;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 7306
    if-eqz v4, :cond_0

    .line 7307
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7305
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7311
    :cond_1
    iget-object v1, p0, Lnif;->b:[Lnjy;

    if-eqz v1, :cond_3

    .line 7312
    iget-object v2, p0, Lnif;->b:[Lnjy;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 7313
    if-eqz v4, :cond_2

    .line 7314
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7312
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7318
    :cond_3
    iget-object v1, p0, Lnif;->c:[Lnjx;

    if-eqz v1, :cond_5

    .line 7319
    iget-object v2, p0, Lnif;->c:[Lnjx;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 7320
    if-eqz v4, :cond_4

    .line 7321
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7319
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 7325
    :cond_5
    iget-object v1, p0, Lnif;->d:[Lnjz;

    if-eqz v1, :cond_7

    .line 7326
    iget-object v1, p0, Lnif;->d:[Lnjz;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 7327
    if-eqz v3, :cond_6

    .line 7328
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 7326
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 7332
    :cond_7
    iget-object v0, p0, Lnif;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7334
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7283
    invoke-virtual {p0, p1}, Lnif;->a(Loxn;)Lnif;

    move-result-object v0

    return-object v0
.end method
