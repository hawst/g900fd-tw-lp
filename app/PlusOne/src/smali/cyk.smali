.class public Lcyk;
.super Llol;
.source "PG"

# interfaces
.implements Lhjj;


# instance fields
.field private N:Loo;

.field private O:Ldeo;

.field private P:Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

.field private Q:Lfyp;

.field private R:Lctz;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Llol;-><init>()V

    .line 46
    new-instance v0, Lhje;

    iget-object v1, p0, Lcyk;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    .line 47
    new-instance v0, Ldep;

    iget-object v1, p0, Lcyk;->av:Llqm;

    new-instance v2, Lcyl;

    invoke-direct {v2, p0}, Lcyl;-><init>(Lcyk;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 53
    new-instance v0, Lcub;

    iget-object v1, p0, Lcyk;->av:Llqm;

    new-instance v2, Lcym;

    invoke-direct {v2, p0}, Lcym;-><init>(Lcyk;)V

    invoke-direct {v0, v1, v2}, Lcub;-><init>(Llqr;Lcuc;)V

    .line 141
    return-void
.end method

.method static synthetic a(Lcyk;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 35
    iget-object v0, p0, Lcyk;->O:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcyk;->N:Loo;

    if-nez v3, :cond_2

    :cond_0
    :goto_0
    iget-object v0, p0, Lcyk;->O:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v4

    iget-object v0, p0, Lcyk;->P:Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcyk;->O:Ldeo;

    invoke-virtual {v0}, Ldeo;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Lddl;->Q()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Lddl;->L()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-interface {v0}, Lddl;->L()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcyk;->R:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v0

    iget-object v3, p0, Lcyk;->N:Loo;

    invoke-virtual {p0}, Lcyk;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f11003d

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v0, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Loo;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcyk;->N:Loo;

    invoke-virtual {v0, v1}, Loo;->d(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcyk;->N:Loo;

    invoke-virtual {v0, v2}, Loo;->d(Z)V

    goto :goto_0

    :cond_4
    invoke-interface {v4}, Lddl;->c()Ljuf;

    move-result-object v0

    if-eqz v0, :cond_5

    move-object v3, v0

    move v0, v1

    :goto_2
    iget-object v5, p0, Lcyk;->P:Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

    invoke-virtual {v5, v3, v0}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->a(Ljcl;Z)V

    iget-object v0, p0, Lcyk;->P:Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->a(Z)V

    iget-object v0, p0, Lcyk;->P:Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

    invoke-interface {v4}, Lddl;->K()Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->c(Z)V

    iget-object v0, p0, Lcyk;->P:Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

    invoke-interface {v3}, Ljuf;->h()Lnzi;

    move-result-object v3

    if-eqz v3, :cond_6

    :goto_3
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->d(Z)V

    goto :goto_1

    :cond_5
    invoke-interface {v4}, Lddl;->d()Ljuf;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_3
.end method

.method static synthetic b(Lcyk;)Lfyp;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcyk;->Q:Lfyp;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 72
    const v0, 0x7f04018a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

    iput-object v0, p0, Lcyk;->P:Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

    .line 74
    iget-object v0, p0, Lcyk;->P:Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

    new-instance v1, Lcyn;

    invoke-direct {v1, p0}, Lcyn;-><init>(Lcyk;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;->a(Lfzp;)V

    .line 76
    iget-object v0, p0, Lcyk;->P:Lcom/google/android/apps/plus/views/PhotoOneUpSelectionButton;

    return-object v0
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcyk;->N:Loo;

    .line 124
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcyk;->N:Loo;

    .line 129
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 64
    iget-object v0, p0, Lcyk;->au:Llnh;

    const-class v1, Lfyp;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfyp;

    iput-object v0, p0, Lcyk;->Q:Lfyp;

    .line 65
    iget-object v0, p0, Lcyk;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcyk;->O:Ldeo;

    .line 66
    iget-object v0, p0, Lcyk;->au:Llnh;

    const-class v1, Lctz;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcyk;->R:Lctz;

    .line 67
    return-void
.end method
