.class public final Lpqm;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lpqn;

.field private b:[B

.field private c:Ljava/lang/String;

.field private d:Lpqn;

.field private e:Lppf;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 196
    sget-object v0, Lpqn;->a:[Lpqn;

    iput-object v0, p0, Lpqm;->a:[Lpqn;

    .line 199
    iput-object v1, p0, Lpqm;->d:Lpqn;

    .line 202
    iput-object v1, p0, Lpqm;->e:Lppf;

    .line 205
    const/high16 v0, -0x80000000

    iput v0, p0, Lpqm;->f:I

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 238
    .line 239
    iget-object v0, p0, Lpqm;->b:[B

    if-eqz v0, :cond_6

    .line 240
    const/4 v0, 0x1

    iget-object v2, p0, Lpqm;->b:[B

    .line 241
    invoke-static {v0, v2}, Loxo;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 243
    :goto_0
    iget-object v2, p0, Lpqm;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 244
    const/4 v2, 0x2

    iget-object v3, p0, Lpqm;->c:Ljava/lang/String;

    .line 245
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 247
    :cond_0
    iget-object v2, p0, Lpqm;->a:[Lpqn;

    if-eqz v2, :cond_2

    .line 248
    iget-object v2, p0, Lpqm;->a:[Lpqn;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 249
    if-eqz v4, :cond_1

    .line 250
    const/4 v5, 0x3

    .line 251
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 248
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 255
    :cond_2
    iget-object v1, p0, Lpqm;->d:Lpqn;

    if-eqz v1, :cond_3

    .line 256
    const/4 v1, 0x4

    iget-object v2, p0, Lpqm;->d:Lpqn;

    .line 257
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_3
    iget-object v1, p0, Lpqm;->e:Lppf;

    if-eqz v1, :cond_4

    .line 260
    const/4 v1, 0x5

    iget-object v2, p0, Lpqm;->e:Lppf;

    .line 261
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    :cond_4
    iget v1, p0, Lpqm;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_5

    .line 264
    const/4 v1, 0x6

    iget v2, p0, Lpqm;->f:I

    .line 265
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 267
    :cond_5
    iget-object v1, p0, Lpqm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    iput v0, p0, Lpqm;->ai:I

    .line 269
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpqm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 277
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 278
    sparse-switch v0, :sswitch_data_0

    .line 282
    iget-object v2, p0, Lpqm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 283
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpqm;->ah:Ljava/util/List;

    .line 286
    :cond_1
    iget-object v2, p0, Lpqm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    :sswitch_0
    return-object p0

    .line 293
    :sswitch_1
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpqm;->b:[B

    goto :goto_0

    .line 297
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqm;->c:Ljava/lang/String;

    goto :goto_0

    .line 301
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 302
    iget-object v0, p0, Lpqm;->a:[Lpqn;

    if-nez v0, :cond_3

    move v0, v1

    .line 303
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpqn;

    .line 304
    iget-object v3, p0, Lpqm;->a:[Lpqn;

    if-eqz v3, :cond_2

    .line 305
    iget-object v3, p0, Lpqm;->a:[Lpqn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 307
    :cond_2
    iput-object v2, p0, Lpqm;->a:[Lpqn;

    .line 308
    :goto_2
    iget-object v2, p0, Lpqm;->a:[Lpqn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 309
    iget-object v2, p0, Lpqm;->a:[Lpqn;

    new-instance v3, Lpqn;

    invoke-direct {v3}, Lpqn;-><init>()V

    aput-object v3, v2, v0

    .line 310
    iget-object v2, p0, Lpqm;->a:[Lpqn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 311
    invoke-virtual {p1}, Loxn;->a()I

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 302
    :cond_3
    iget-object v0, p0, Lpqm;->a:[Lpqn;

    array-length v0, v0

    goto :goto_1

    .line 314
    :cond_4
    iget-object v2, p0, Lpqm;->a:[Lpqn;

    new-instance v3, Lpqn;

    invoke-direct {v3}, Lpqn;-><init>()V

    aput-object v3, v2, v0

    .line 315
    iget-object v2, p0, Lpqm;->a:[Lpqn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 319
    :sswitch_4
    iget-object v0, p0, Lpqm;->d:Lpqn;

    if-nez v0, :cond_5

    .line 320
    new-instance v0, Lpqn;

    invoke-direct {v0}, Lpqn;-><init>()V

    iput-object v0, p0, Lpqm;->d:Lpqn;

    .line 322
    :cond_5
    iget-object v0, p0, Lpqm;->d:Lpqn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 326
    :sswitch_5
    iget-object v0, p0, Lpqm;->e:Lppf;

    if-nez v0, :cond_6

    .line 327
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpqm;->e:Lppf;

    .line 329
    :cond_6
    iget-object v0, p0, Lpqm;->e:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 333
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 334
    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-ne v0, v2, :cond_8

    .line 337
    :cond_7
    iput v0, p0, Lpqm;->f:I

    goto/16 :goto_0

    .line 339
    :cond_8
    iput v1, p0, Lpqm;->f:I

    goto/16 :goto_0

    .line 278
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 210
    iget-object v0, p0, Lpqm;->b:[B

    if-eqz v0, :cond_0

    .line 211
    const/4 v0, 0x1

    iget-object v1, p0, Lpqm;->b:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 213
    :cond_0
    iget-object v0, p0, Lpqm;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 214
    const/4 v0, 0x2

    iget-object v1, p0, Lpqm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 216
    :cond_1
    iget-object v0, p0, Lpqm;->a:[Lpqn;

    if-eqz v0, :cond_3

    .line 217
    iget-object v1, p0, Lpqm;->a:[Lpqn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 218
    if-eqz v3, :cond_2

    .line 219
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 217
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_3
    iget-object v0, p0, Lpqm;->d:Lpqn;

    if-eqz v0, :cond_4

    .line 224
    const/4 v0, 0x4

    iget-object v1, p0, Lpqm;->d:Lpqn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 226
    :cond_4
    iget-object v0, p0, Lpqm;->e:Lppf;

    if-eqz v0, :cond_5

    .line 227
    const/4 v0, 0x5

    iget-object v1, p0, Lpqm;->e:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 229
    :cond_5
    iget v0, p0, Lpqm;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_6

    .line 230
    const/4 v0, 0x6

    iget v1, p0, Lpqm;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 232
    :cond_6
    iget-object v0, p0, Lpqm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 234
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpqm;->a(Loxn;)Lpqm;

    move-result-object v0

    return-object v0
.end method
