.class public final Llzh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnff;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28348
    invoke-direct {p0}, Loxq;-><init>()V

    .line 28351
    iput-object v0, p0, Llzh;->apiHeader:Llyr;

    .line 28354
    iput-object v0, p0, Llzh;->a:Lnff;

    .line 28348
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 28371
    const/4 v0, 0x0

    .line 28372
    iget-object v1, p0, Llzh;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 28373
    const/4 v0, 0x1

    iget-object v1, p0, Llzh;->apiHeader:Llyr;

    .line 28374
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 28376
    :cond_0
    iget-object v1, p0, Llzh;->a:Lnff;

    if-eqz v1, :cond_1

    .line 28377
    const/4 v1, 0x2

    iget-object v2, p0, Llzh;->a:Lnff;

    .line 28378
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28380
    :cond_1
    iget-object v1, p0, Llzh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28381
    iput v0, p0, Llzh;->ai:I

    .line 28382
    return v0
.end method

.method public a(Loxn;)Llzh;
    .locals 2

    .prologue
    .line 28390
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 28391
    sparse-switch v0, :sswitch_data_0

    .line 28395
    iget-object v1, p0, Llzh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 28396
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzh;->ah:Ljava/util/List;

    .line 28399
    :cond_1
    iget-object v1, p0, Llzh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28401
    :sswitch_0
    return-object p0

    .line 28406
    :sswitch_1
    iget-object v0, p0, Llzh;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 28407
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzh;->apiHeader:Llyr;

    .line 28409
    :cond_2
    iget-object v0, p0, Llzh;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28413
    :sswitch_2
    iget-object v0, p0, Llzh;->a:Lnff;

    if-nez v0, :cond_3

    .line 28414
    new-instance v0, Lnff;

    invoke-direct {v0}, Lnff;-><init>()V

    iput-object v0, p0, Llzh;->a:Lnff;

    .line 28416
    :cond_3
    iget-object v0, p0, Llzh;->a:Lnff;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28391
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28359
    iget-object v0, p0, Llzh;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 28360
    const/4 v0, 0x1

    iget-object v1, p0, Llzh;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28362
    :cond_0
    iget-object v0, p0, Llzh;->a:Lnff;

    if-eqz v0, :cond_1

    .line 28363
    const/4 v0, 0x2

    iget-object v1, p0, Llzh;->a:Lnff;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28365
    :cond_1
    iget-object v0, p0, Llzh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 28367
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 28344
    invoke-virtual {p0, p1}, Llzh;->a(Loxn;)Llzh;

    move-result-object v0

    return-object v0
.end method
