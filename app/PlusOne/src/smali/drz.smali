.class public Ldrz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liwq;
.implements Lixl;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhrt;

.field private final c:Lhei;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Ldrz;->a:Landroid/content/Context;

    .line 81
    const-class v0, Lhrt;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrt;

    iput-object v0, p0, Ldrz;->b:Lhrt;

    .line 82
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ldrz;->c:Lhei;

    .line 83
    return-void
.end method

.method static synthetic a(Ldrz;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 7

    .prologue
    .line 343
    invoke-direct {p0}, Ldrz;->b()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 344
    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v3

    .line 345
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 346
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 347
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 348
    iget-object v1, p0, Ldrz;->a:Landroid/content/Context;

    const-class v5, Lieh;

    invoke-static {v1, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lieh;

    .line 350
    sget-object v5, Ldxd;->i:Lief;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v1, v5, v6}, Lieh;->b(Lief;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 351
    iget-object v1, p0, Ldrz;->c:Lhei;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v1, v5}, Lhei;->a(I)Lhej;

    move-result-object v1

    invoke-interface {v1}, Lhej;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 352
    iget-object v1, p0, Ldrz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lffl;->a(Landroid/content/Context;I)V

    .line 346
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 360
    :cond_1
    return-void
.end method

.method private b()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 395
    iget-object v1, p0, Ldrz;->c:Lhei;

    invoke-interface {v1}, Lhei;->a()Ljava/util/List;

    move-result-object v3

    .line 397
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    .line 399
    :goto_0
    if-ge v2, v4, :cond_0

    .line 400
    iget-object v5, p0, Ldrz;->c:Lhei;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v5, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    add-int/lit8 v0, v1, 0x1

    .line 399
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 404
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private b(I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 239
    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 240
    const-string v1, "database_status"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    :goto_0
    return-void

    .line 244
    :cond_0
    iget-object v1, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    .line 245
    invoke-virtual {v1}, Ldrg;->c()V

    .line 247
    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 250
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 251
    const-string v3, "notification_poll_interval"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 252
    const-string v3, "last_stats_sync_time"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 253
    const-string v3, "last_contacted_time"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 254
    const-string v3, "wipeout_stats"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 255
    const-string v3, "circle_settings_sync_time"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 256
    const-string v3, "people_last_update_token"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 257
    const-string v3, "user_id"

    const-string v4, "gaia_id"

    .line 258
    invoke-interface {v0, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 257
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string v0, "account_status"

    invoke-virtual {v1, v0, v2, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 261
    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ldsm;->d(Landroid/content/Context;I)V

    .line 263
    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "database_status"

    const/4 v2, 0x1

    .line 264
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;I)Lhek;

    move-result-object v0

    .line 265
    invoke-interface {v0}, Lhek;->c()I

    goto :goto_0
.end method

.method private c(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 269
    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 270
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 273
    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Ljava/lang/String;)V

    .line 275
    const-string v2, "sync_enabled"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "sync_disabled"

    .line 276
    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->a(Ljava/lang/String;Z)V

    .line 278
    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "sync_enabled"

    invoke-interface {v0, v1, v3}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    .line 280
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 104
    const-string v3, "logged_in"

    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 105
    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v3, "logout_complete"

    invoke-interface {v0, v3, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v3

    invoke-direct {p0, p1}, Ldrz;->b(I)V

    invoke-direct {p0, p1}, Ldrz;->c(I)V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {}, Ldsm;->a()V

    const-string v0, "is_managed_account"

    invoke-interface {v3, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    iget-object v4, p0, Ldrz;->c:Lhei;

    invoke-interface {v4, p1}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v5, "account_name"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->b(Landroid/content/Context;Ljava/lang/String;)V

    const-string v6, "auto_upload_sync_enabled"

    invoke-interface {v4, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "auto_upload_sync_disabled"

    invoke-interface {v4, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0, v5}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v4, "auto_upload_sync_enabled"

    invoke-interface {v0, v4, v1}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    :cond_0
    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    iget-object v4, p0, Ldrz;->c:Lhei;

    invoke-interface {v4, p1}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v5, "account_name"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->c(Ljava/lang/String;)V

    const-string v6, "photos_sync_enabled"

    invoke-interface {v4, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "photos_sync_disabled"

    invoke-interface {v4, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v0, v5}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v4, "photos_sync_enabled"

    invoke-interface {v0, v4, v1}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    :cond_1
    invoke-direct {p0}, Ldrz;->a()V

    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v4, "local_media_refresh_requested"

    invoke-interface {v0, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Ldrz;->b()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_0
    new-instance v2, Ljava/lang/Thread;

    new-instance v4, Ldsb;

    invoke-direct {v4, p0, v0}, Ldsb;-><init>(Ldrz;Z)V

    invoke-direct {v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v2, "local_media_refresh_requested"

    invoke-interface {v0, v2, v1}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    :cond_2
    const-string v0, "tmp_notifications_prefetched"

    invoke-interface {v3, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    const-class v2, Lieh;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v2, Ldxd;->i:Lief;

    invoke-interface {v0, v2, p1}, Lieh;->b(Lief;I)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Ldxd;->l:Lief;

    invoke-interface {v0, v2, p1}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    new-instance v2, Ldou;

    iget-object v3, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {p1}, Lfhu;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, p1}, Ldou;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-static {v0, v2}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v2, "tmp_notifications_prefetched"

    invoke-interface {v0, v2, v1}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    .line 120
    :cond_3
    :goto_1
    return-void

    :cond_4
    move v0, v2

    .line 105
    goto :goto_0

    .line 109
    :cond_5
    const-string v3, "logged_out"

    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 110
    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "logout_complete"

    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "is_managed_account"

    invoke-interface {v0, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->b(Ljava/lang/String;)V

    iget-object v3, p0, Ldrz;->c:Lhei;

    invoke-interface {v3, p1}, Lhei;->b(I)Lhek;

    move-result-object v3

    const-string v4, "sync_enabled"

    invoke-interface {v3, v4, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v3

    const-string v4, "sync_disabled"

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    invoke-interface {v3, v4, v0}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    iget-object v3, p0, Ldrz;->c:Lhei;

    invoke-interface {v3, p1}, Lhei;->a(I)Lhej;

    move-result-object v3

    const-string v4, "account_name"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;I)V

    iget-object v4, p0, Ldrz;->c:Lhei;

    invoke-interface {v4, p1}, Lhei;->b(I)Lhek;

    move-result-object v4

    const-string v5, "auto_upload_sync_enabled"

    invoke-interface {v4, v5, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v4

    const-string v5, "auto_upload_sync_disabled"

    invoke-static {v0, v3}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    invoke-interface {v4, v5, v0}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->b(Ljava/lang/String;)V

    iget-object v3, p0, Ldrz;->c:Lhei;

    invoke-interface {v3, p1}, Lhei;->b(I)Lhek;

    move-result-object v3

    const-string v4, "photos_sync_enabled"

    invoke-interface {v3, v4, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v3

    const-string v4, "photos_sync_disabled"

    invoke-static {v0}, Lcom/google/android/apps/photos/service/GooglePhotoDownsyncService;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_4
    invoke-interface {v3, v4, v0}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    :cond_6
    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lhqd;->c(Landroid/content/Context;I)V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    const-class v3, Lhpu;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    invoke-virtual {v0, p1}, Lhpu;->e(I)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0, p1, v1}, Lesd;->a(Landroid/content/Context;IZ)V

    iget-object v0, p0, Ldrz;->b:Lhrt;

    iget-object v3, p0, Ldrz;->a:Landroid/content/Context;

    invoke-virtual {v0, v3, p1}, Lhrt;->a(Landroid/content/Context;I)V

    :cond_7
    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "iu.received_low_quota"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "iu.received_no_quota"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    const-string v3, "alarm"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v3, p0, Ldrz;->a:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Leyq;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ldsf;->e(Landroid/content/Context;I)V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->a()V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0}, Ldwq;->b(Landroid/content/Context;)V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lffe;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0}, Ldwn;->a(Landroid/content/Context;)Ldwn;

    move-result-object v0

    invoke-virtual {v0}, Ldwn;->a()V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0}, Ldwe;->a(Landroid/content/Context;)Ldwe;

    move-result-object v0

    invoke-virtual {v0}, Ldwe;->a()V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lfho;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->b(Landroid/content/Context;I)V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0}, Lgci;->a(Landroid/content/Context;)V

    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-static {v0}, Lgcq;->a(Landroid/content/Context;)V

    sget-object v0, Lkcu;->a:Lkcu;

    invoke-virtual {v0}, Lkcu;->d()V

    new-instance v0, Ldsa;

    invoke-direct {v0, p0}, Ldsa;-><init>(Ldrz;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    iget-object v0, p0, Ldrz;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v3, "logout_complete"

    invoke-interface {v0, v3, v1}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    const-string v1, "tmp_notifications_prefetched"

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    goto/16 :goto_1

    :cond_8
    move v0, v2

    goto/16 :goto_2

    :cond_9
    move v0, v2

    goto/16 :goto_3

    :cond_a
    move v0, v2

    goto/16 :goto_4

    .line 114
    :cond_b
    const-string v1, "gplus_no_mobile_tos"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 115
    invoke-direct {p0, p1}, Ldrz;->b(I)V

    invoke-direct {p0, p1}, Ldrz;->c(I)V

    invoke-direct {p0}, Ldrz;->a()V

    goto/16 :goto_1

    .line 119
    :cond_c
    invoke-direct {p0, p1}, Ldrz;->c(I)V

    goto/16 :goto_1
.end method

.method public a(Lhem;Lmcb;)V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Ldrz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 98
    return-void
.end method

.method public a(Lhej;Lmca;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 87
    iget-object v0, p2, Lmca;->a:Lnoa;

    new-instance v1, Lpee;

    invoke-direct {v1}, Lpee;-><init>()V

    iput-object v1, v0, Lnoa;->b:Lpee;

    .line 88
    iget-object v0, p2, Lmca;->a:Lnoa;

    iget-object v0, v0, Lnoa;->b:Lpee;

    new-instance v1, Lpef;

    invoke-direct {v1}, Lpef;-><init>()V

    iput-object v1, v0, Lpee;->a:Lpef;

    .line 89
    iget-object v0, p2, Lmca;->a:Lnoa;

    iget-object v0, v0, Lnoa;->b:Lpee;

    iget-object v0, v0, Lpee;->a:Lpef;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lpef;->a:Ljava/lang/Boolean;

    .line 90
    return v2
.end method
