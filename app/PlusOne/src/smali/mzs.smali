.class public final Lmzs;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 336
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 372
    const/4 v0, 0x0

    .line 373
    iget-object v1, p0, Lmzs;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 374
    const/4 v0, 0x1

    iget-object v1, p0, Lmzs;->a:Ljava/lang/Boolean;

    .line 375
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 377
    :cond_0
    iget-object v1, p0, Lmzs;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 378
    const/4 v1, 0x2

    iget-object v2, p0, Lmzs;->b:Ljava/lang/Boolean;

    .line 379
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 381
    :cond_1
    iget-object v1, p0, Lmzs;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 382
    const/4 v1, 0x3

    iget-object v2, p0, Lmzs;->c:Ljava/lang/Boolean;

    .line 383
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 385
    :cond_2
    iget-object v1, p0, Lmzs;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 386
    const/4 v1, 0x4

    iget-object v2, p0, Lmzs;->d:Ljava/lang/Boolean;

    .line 387
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 389
    :cond_3
    iget-object v1, p0, Lmzs;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 390
    const/4 v1, 0x5

    iget-object v2, p0, Lmzs;->e:Ljava/lang/Boolean;

    .line 391
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 393
    :cond_4
    iget-object v1, p0, Lmzs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 394
    iput v0, p0, Lmzs;->ai:I

    .line 395
    return v0
.end method

.method public a(Loxn;)Lmzs;
    .locals 2

    .prologue
    .line 403
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 404
    sparse-switch v0, :sswitch_data_0

    .line 408
    iget-object v1, p0, Lmzs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 409
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmzs;->ah:Ljava/util/List;

    .line 412
    :cond_1
    iget-object v1, p0, Lmzs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 414
    :sswitch_0
    return-object p0

    .line 419
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzs;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 423
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzs;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 427
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzs;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 431
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzs;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 435
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzs;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 404
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lmzs;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 352
    const/4 v0, 0x1

    iget-object v1, p0, Lmzs;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 354
    :cond_0
    iget-object v0, p0, Lmzs;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 355
    const/4 v0, 0x2

    iget-object v1, p0, Lmzs;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 357
    :cond_1
    iget-object v0, p0, Lmzs;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 358
    const/4 v0, 0x3

    iget-object v1, p0, Lmzs;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 360
    :cond_2
    iget-object v0, p0, Lmzs;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 361
    const/4 v0, 0x4

    iget-object v1, p0, Lmzs;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 363
    :cond_3
    iget-object v0, p0, Lmzs;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 364
    const/4 v0, 0x5

    iget-object v1, p0, Lmzs;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 366
    :cond_4
    iget-object v0, p0, Lmzs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 368
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 332
    invoke-virtual {p0, p1}, Lmzs;->a(Loxn;)Lmzs;

    move-result-object v0

    return-object v0
.end method
