.class public Lhsc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhsc;->b:Z

    .line 38
    iput-object p1, p0, Lhsc;->a:Landroid/content/ContentResolver;

    .line 39
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 42
    invoke-static {}, Llsx;->c()V

    .line 45
    iget-object v0, p0, Lhsc;->a:Landroid/content/ContentResolver;

    const-string v1, "dummy"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhsc;->b:Z

    .line 47
    return-void
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Llsx;->c()V

    .line 56
    iget-object v0, p0, Lhsc;->a:Landroid/content/ContentResolver;

    const-string v1, "plusone:autobackup_logged_out_notification_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 66
    iget-boolean v1, p0, Lhsc;->b:Z

    if-nez v1, :cond_0

    invoke-static {}, Llsx;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    if-eqz v1, :cond_1

    .line 70
    :goto_1
    return v0

    .line 66
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 70
    :cond_1
    iget-object v1, p0, Lhsc;->a:Landroid/content/ContentResolver;

    const-string v2, "plusone:disable_instantshare_in_gplus_app"

    invoke-static {v1, v2, v0}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_1
.end method
