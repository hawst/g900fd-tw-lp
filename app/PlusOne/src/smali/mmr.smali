.class public final Lmmr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmmr;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:Lmmo;

.field public e:[Ljava/lang/String;

.field public f:Lmms;

.field private g:Lodo;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7960
    const/4 v0, 0x0

    new-array v0, v0, [Lmmr;

    sput-object v0, Lmmr;->a:[Lmmr;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7961
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7981
    const/high16 v0, -0x80000000

    iput v0, p0, Lmmr;->b:I

    .line 7986
    iput-object v1, p0, Lmmr;->g:Lodo;

    .line 7989
    iput-object v1, p0, Lmmr;->d:Lmmo;

    .line 7992
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lmmr;->e:[Ljava/lang/String;

    .line 7995
    iput-object v1, p0, Lmmr;->f:Lmms;

    .line 7961
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8031
    .line 8032
    iget v0, p0, Lmmr;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_7

    .line 8033
    const/4 v0, 0x1

    iget v2, p0, Lmmr;->b:I

    .line 8034
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8036
    :goto_0
    iget-object v2, p0, Lmmr;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 8037
    const/4 v2, 0x3

    iget-object v3, p0, Lmmr;->c:Ljava/lang/String;

    .line 8038
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 8040
    :cond_0
    iget-object v2, p0, Lmmr;->g:Lodo;

    if-eqz v2, :cond_1

    .line 8041
    const/4 v2, 0x4

    iget-object v3, p0, Lmmr;->g:Lodo;

    .line 8042
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 8044
    :cond_1
    iget-object v2, p0, Lmmr;->d:Lmmo;

    if-eqz v2, :cond_2

    .line 8045
    const/16 v2, 0x8

    iget-object v3, p0, Lmmr;->d:Lmmo;

    .line 8046
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 8048
    :cond_2
    iget-object v2, p0, Lmmr;->e:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lmmr;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 8050
    iget-object v3, p0, Lmmr;->e:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 8052
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 8050
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8054
    :cond_3
    add-int/2addr v0, v2

    .line 8055
    iget-object v1, p0, Lmmr;->e:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8057
    :cond_4
    iget-object v1, p0, Lmmr;->f:Lmms;

    if-eqz v1, :cond_5

    .line 8058
    const/16 v1, 0xb

    iget-object v2, p0, Lmmr;->f:Lmms;

    .line 8059
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8061
    :cond_5
    iget-object v1, p0, Lmmr;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 8062
    const/16 v1, 0xc

    iget-object v2, p0, Lmmr;->h:Ljava/lang/String;

    .line 8063
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8065
    :cond_6
    iget-object v1, p0, Lmmr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8066
    iput v0, p0, Lmmr;->ai:I

    .line 8067
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmmr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 8075
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8076
    sparse-switch v0, :sswitch_data_0

    .line 8080
    iget-object v1, p0, Lmmr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8081
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmmr;->ah:Ljava/util/List;

    .line 8084
    :cond_1
    iget-object v1, p0, Lmmr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8086
    :sswitch_0
    return-object p0

    .line 8091
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 8092
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-ne v0, v1, :cond_3

    .line 8106
    :cond_2
    iput v0, p0, Lmmr;->b:I

    goto :goto_0

    .line 8108
    :cond_3
    iput v3, p0, Lmmr;->b:I

    goto :goto_0

    .line 8113
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmmr;->c:Ljava/lang/String;

    goto :goto_0

    .line 8117
    :sswitch_3
    iget-object v0, p0, Lmmr;->g:Lodo;

    if-nez v0, :cond_4

    .line 8118
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lmmr;->g:Lodo;

    .line 8120
    :cond_4
    iget-object v0, p0, Lmmr;->g:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8124
    :sswitch_4
    iget-object v0, p0, Lmmr;->d:Lmmo;

    if-nez v0, :cond_5

    .line 8125
    new-instance v0, Lmmo;

    invoke-direct {v0}, Lmmo;-><init>()V

    iput-object v0, p0, Lmmr;->d:Lmmo;

    .line 8127
    :cond_5
    iget-object v0, p0, Lmmr;->d:Lmmo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8131
    :sswitch_5
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 8132
    iget-object v0, p0, Lmmr;->e:[Ljava/lang/String;

    array-length v0, v0

    .line 8133
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 8134
    iget-object v2, p0, Lmmr;->e:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 8135
    iput-object v1, p0, Lmmr;->e:[Ljava/lang/String;

    .line 8136
    :goto_1
    iget-object v1, p0, Lmmr;->e:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    .line 8137
    iget-object v1, p0, Lmmr;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 8138
    invoke-virtual {p1}, Loxn;->a()I

    .line 8136
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 8141
    :cond_6
    iget-object v1, p0, Lmmr;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 8145
    :sswitch_6
    iget-object v0, p0, Lmmr;->f:Lmms;

    if-nez v0, :cond_7

    .line 8146
    new-instance v0, Lmms;

    invoke-direct {v0}, Lmms;-><init>()V

    iput-object v0, p0, Lmmr;->f:Lmms;

    .line 8148
    :cond_7
    iget-object v0, p0, Lmmr;->f:Lmms;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 8152
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmmr;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 8076
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x42 -> :sswitch_4
        0x4a -> :sswitch_5
        0x5a -> :sswitch_6
        0x62 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 8002
    iget v0, p0, Lmmr;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 8003
    const/4 v0, 0x1

    iget v1, p0, Lmmr;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8005
    :cond_0
    iget-object v0, p0, Lmmr;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 8006
    const/4 v0, 0x3

    iget-object v1, p0, Lmmr;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8008
    :cond_1
    iget-object v0, p0, Lmmr;->g:Lodo;

    if-eqz v0, :cond_2

    .line 8009
    const/4 v0, 0x4

    iget-object v1, p0, Lmmr;->g:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8011
    :cond_2
    iget-object v0, p0, Lmmr;->d:Lmmo;

    if-eqz v0, :cond_3

    .line 8012
    const/16 v0, 0x8

    iget-object v1, p0, Lmmr;->d:Lmmo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8014
    :cond_3
    iget-object v0, p0, Lmmr;->e:[Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 8015
    iget-object v1, p0, Lmmr;->e:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 8016
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 8015
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8019
    :cond_4
    iget-object v0, p0, Lmmr;->f:Lmms;

    if-eqz v0, :cond_5

    .line 8020
    const/16 v0, 0xb

    iget-object v1, p0, Lmmr;->f:Lmms;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8022
    :cond_5
    iget-object v0, p0, Lmmr;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 8023
    const/16 v0, 0xc

    iget-object v1, p0, Lmmr;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8025
    :cond_6
    iget-object v0, p0, Lmmr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8027
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7957
    invoke-virtual {p0, p1}, Lmmr;->a(Loxn;)Lmmr;

    move-result-object v0

    return-object v0
.end method
