.class public final Lnbw;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lopf;

.field private b:Lnbl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 920
    invoke-direct {p0}, Loxq;-><init>()V

    .line 923
    iput-object v0, p0, Lnbw;->a:Lopf;

    .line 926
    iput-object v0, p0, Lnbw;->b:Lnbl;

    .line 920
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 943
    const/4 v0, 0x0

    .line 944
    iget-object v1, p0, Lnbw;->a:Lopf;

    if-eqz v1, :cond_0

    .line 945
    const/4 v0, 0x1

    iget-object v1, p0, Lnbw;->a:Lopf;

    .line 946
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 948
    :cond_0
    iget-object v1, p0, Lnbw;->b:Lnbl;

    if-eqz v1, :cond_1

    .line 949
    const/4 v1, 0x2

    iget-object v2, p0, Lnbw;->b:Lnbl;

    .line 950
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 952
    :cond_1
    iget-object v1, p0, Lnbw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 953
    iput v0, p0, Lnbw;->ai:I

    .line 954
    return v0
.end method

.method public a(Loxn;)Lnbw;
    .locals 2

    .prologue
    .line 962
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 963
    sparse-switch v0, :sswitch_data_0

    .line 967
    iget-object v1, p0, Lnbw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 968
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbw;->ah:Ljava/util/List;

    .line 971
    :cond_1
    iget-object v1, p0, Lnbw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 973
    :sswitch_0
    return-object p0

    .line 978
    :sswitch_1
    iget-object v0, p0, Lnbw;->a:Lopf;

    if-nez v0, :cond_2

    .line 979
    new-instance v0, Lopf;

    invoke-direct {v0}, Lopf;-><init>()V

    iput-object v0, p0, Lnbw;->a:Lopf;

    .line 981
    :cond_2
    iget-object v0, p0, Lnbw;->a:Lopf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 985
    :sswitch_2
    iget-object v0, p0, Lnbw;->b:Lnbl;

    if-nez v0, :cond_3

    .line 986
    new-instance v0, Lnbl;

    invoke-direct {v0}, Lnbl;-><init>()V

    iput-object v0, p0, Lnbw;->b:Lnbl;

    .line 988
    :cond_3
    iget-object v0, p0, Lnbw;->b:Lnbl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 963
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 931
    iget-object v0, p0, Lnbw;->a:Lopf;

    if-eqz v0, :cond_0

    .line 932
    const/4 v0, 0x1

    iget-object v1, p0, Lnbw;->a:Lopf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 934
    :cond_0
    iget-object v0, p0, Lnbw;->b:Lnbl;

    if-eqz v0, :cond_1

    .line 935
    const/4 v0, 0x2

    iget-object v1, p0, Lnbw;->b:Lnbl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 937
    :cond_1
    iget-object v0, p0, Lnbw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 939
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 916
    invoke-virtual {p0, p1}, Lnbw;->a(Loxn;)Lnbw;

    move-result-object v0

    return-object v0
.end method
