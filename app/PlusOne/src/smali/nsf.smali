.class public final Lnsf;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 534
    invoke-direct {p0}, Loxq;-><init>()V

    .line 542
    iput v0, p0, Lnsf;->a:I

    .line 545
    iput v0, p0, Lnsf;->b:I

    .line 534
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 562
    const/4 v0, 0x0

    .line 563
    iget v1, p0, Lnsf;->a:I

    if-eq v1, v2, :cond_0

    .line 564
    const/4 v0, 0x1

    iget v1, p0, Lnsf;->a:I

    .line 565
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 567
    :cond_0
    iget v1, p0, Lnsf;->b:I

    if-eq v1, v2, :cond_1

    .line 568
    const/4 v1, 0x2

    iget v2, p0, Lnsf;->b:I

    .line 569
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_1
    iget-object v1, p0, Lnsf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 572
    iput v0, p0, Lnsf;->ai:I

    .line 573
    return v0
.end method

.method public a(Loxn;)Lnsf;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 581
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 582
    sparse-switch v0, :sswitch_data_0

    .line 586
    iget-object v1, p0, Lnsf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 587
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnsf;->ah:Ljava/util/List;

    .line 590
    :cond_1
    iget-object v1, p0, Lnsf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 592
    :sswitch_0
    return-object p0

    .line 597
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 598
    if-eqz v0, :cond_2

    if-ne v0, v3, :cond_3

    .line 600
    :cond_2
    iput v0, p0, Lnsf;->a:I

    goto :goto_0

    .line 602
    :cond_3
    iput v2, p0, Lnsf;->a:I

    goto :goto_0

    .line 607
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 608
    if-eqz v0, :cond_4

    if-ne v0, v3, :cond_5

    .line 610
    :cond_4
    iput v0, p0, Lnsf;->b:I

    goto :goto_0

    .line 612
    :cond_5
    iput v2, p0, Lnsf;->b:I

    goto :goto_0

    .line 582
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 550
    iget v0, p0, Lnsf;->a:I

    if-eq v0, v2, :cond_0

    .line 551
    const/4 v0, 0x1

    iget v1, p0, Lnsf;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 553
    :cond_0
    iget v0, p0, Lnsf;->b:I

    if-eq v0, v2, :cond_1

    .line 554
    const/4 v0, 0x2

    iget v1, p0, Lnsf;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 556
    :cond_1
    iget-object v0, p0, Lnsf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 558
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 530
    invoke-virtual {p0, p1}, Lnsf;->a(Loxn;)Lnsf;

    move-result-object v0

    return-object v0
.end method
