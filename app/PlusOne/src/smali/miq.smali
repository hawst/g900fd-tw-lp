.class public final Lmiq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlj;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 727
    invoke-direct {p0}, Loxq;-><init>()V

    .line 730
    iput-object v0, p0, Lmiq;->apiHeader:Llyq;

    .line 733
    iput-object v0, p0, Lmiq;->a:Lnlj;

    .line 727
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 750
    const/4 v0, 0x0

    .line 751
    iget-object v1, p0, Lmiq;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 752
    const/4 v0, 0x1

    iget-object v1, p0, Lmiq;->apiHeader:Llyq;

    .line 753
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 755
    :cond_0
    iget-object v1, p0, Lmiq;->a:Lnlj;

    if-eqz v1, :cond_1

    .line 756
    const/4 v1, 0x2

    iget-object v2, p0, Lmiq;->a:Lnlj;

    .line 757
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 759
    :cond_1
    iget-object v1, p0, Lmiq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 760
    iput v0, p0, Lmiq;->ai:I

    .line 761
    return v0
.end method

.method public a(Loxn;)Lmiq;
    .locals 2

    .prologue
    .line 769
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 770
    sparse-switch v0, :sswitch_data_0

    .line 774
    iget-object v1, p0, Lmiq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 775
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmiq;->ah:Ljava/util/List;

    .line 778
    :cond_1
    iget-object v1, p0, Lmiq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 780
    :sswitch_0
    return-object p0

    .line 785
    :sswitch_1
    iget-object v0, p0, Lmiq;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 786
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmiq;->apiHeader:Llyq;

    .line 788
    :cond_2
    iget-object v0, p0, Lmiq;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 792
    :sswitch_2
    iget-object v0, p0, Lmiq;->a:Lnlj;

    if-nez v0, :cond_3

    .line 793
    new-instance v0, Lnlj;

    invoke-direct {v0}, Lnlj;-><init>()V

    iput-object v0, p0, Lmiq;->a:Lnlj;

    .line 795
    :cond_3
    iget-object v0, p0, Lmiq;->a:Lnlj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 770
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 738
    iget-object v0, p0, Lmiq;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 739
    const/4 v0, 0x1

    iget-object v1, p0, Lmiq;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 741
    :cond_0
    iget-object v0, p0, Lmiq;->a:Lnlj;

    if-eqz v0, :cond_1

    .line 742
    const/4 v0, 0x2

    iget-object v1, p0, Lmiq;->a:Lnlj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 744
    :cond_1
    iget-object v0, p0, Lmiq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 746
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 723
    invoke-virtual {p0, p1}, Lmiq;->a(Loxn;)Lmiq;

    move-result-object v0

    return-object v0
.end method
