.class public final Lpoo;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Float;

.field private b:Ljava/lang/Float;

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/Float;

.field private e:Ljava/lang/Float;

.field private f:Ljava/lang/Float;

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 36
    iput v0, p0, Lpoo;->g:I

    .line 39
    iput v0, p0, Lpoo;->h:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 74
    const/4 v0, 0x0

    .line 75
    iget-object v1, p0, Lpoo;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 76
    const/4 v0, 0x1

    iget-object v1, p0, Lpoo;->a:Ljava/lang/Float;

    .line 77
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 79
    :cond_0
    iget-object v1, p0, Lpoo;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 80
    const/4 v1, 0x2

    iget-object v2, p0, Lpoo;->b:Ljava/lang/Float;

    .line 81
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 83
    :cond_1
    iget-object v1, p0, Lpoo;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 84
    const/4 v1, 0x3

    iget-object v2, p0, Lpoo;->c:Ljava/lang/Float;

    .line 85
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 87
    :cond_2
    iget-object v1, p0, Lpoo;->d:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 88
    const/4 v1, 0x4

    iget-object v2, p0, Lpoo;->d:Ljava/lang/Float;

    .line 89
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 91
    :cond_3
    iget-object v1, p0, Lpoo;->e:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 92
    const/4 v1, 0x5

    iget-object v2, p0, Lpoo;->e:Ljava/lang/Float;

    .line 93
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 95
    :cond_4
    iget-object v1, p0, Lpoo;->f:Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 96
    const/4 v1, 0x6

    iget-object v2, p0, Lpoo;->f:Ljava/lang/Float;

    .line 97
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 99
    :cond_5
    iget v1, p0, Lpoo;->g:I

    if-eq v1, v3, :cond_6

    .line 100
    const/4 v1, 0x7

    iget v2, p0, Lpoo;->g:I

    .line 101
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_6
    iget v1, p0, Lpoo;->h:I

    if-eq v1, v3, :cond_7

    .line 104
    const/16 v1, 0x8

    iget v2, p0, Lpoo;->h:I

    .line 105
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_7
    iget-object v1, p0, Lpoo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    iput v0, p0, Lpoo;->ai:I

    .line 109
    return v0
.end method

.method public a(Loxn;)Lpoo;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 117
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 118
    sparse-switch v0, :sswitch_data_0

    .line 122
    iget-object v1, p0, Lpoo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpoo;->ah:Ljava/util/List;

    .line 126
    :cond_1
    iget-object v1, p0, Lpoo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    :sswitch_0
    return-object p0

    .line 133
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpoo;->a:Ljava/lang/Float;

    goto :goto_0

    .line 137
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpoo;->b:Ljava/lang/Float;

    goto :goto_0

    .line 141
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpoo;->c:Ljava/lang/Float;

    goto :goto_0

    .line 145
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpoo;->d:Ljava/lang/Float;

    goto :goto_0

    .line 149
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpoo;->e:Ljava/lang/Float;

    goto :goto_0

    .line 153
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpoo;->f:Ljava/lang/Float;

    goto :goto_0

    .line 157
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 158
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 161
    :cond_2
    iput v0, p0, Lpoo;->g:I

    goto :goto_0

    .line 163
    :cond_3
    iput v2, p0, Lpoo;->g:I

    goto :goto_0

    .line 168
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 169
    if-eqz v0, :cond_4

    if-ne v0, v3, :cond_5

    .line 171
    :cond_4
    iput v0, p0, Lpoo;->h:I

    goto :goto_0

    .line 173
    :cond_5
    iput v2, p0, Lpoo;->h:I

    goto :goto_0

    .line 118
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 44
    iget-object v0, p0, Lpoo;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x1

    iget-object v1, p0, Lpoo;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 47
    :cond_0
    iget-object v0, p0, Lpoo;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 48
    const/4 v0, 0x2

    iget-object v1, p0, Lpoo;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 50
    :cond_1
    iget-object v0, p0, Lpoo;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 51
    const/4 v0, 0x3

    iget-object v1, p0, Lpoo;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 53
    :cond_2
    iget-object v0, p0, Lpoo;->d:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 54
    const/4 v0, 0x4

    iget-object v1, p0, Lpoo;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 56
    :cond_3
    iget-object v0, p0, Lpoo;->e:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 57
    const/4 v0, 0x5

    iget-object v1, p0, Lpoo;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 59
    :cond_4
    iget-object v0, p0, Lpoo;->f:Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 60
    const/4 v0, 0x6

    iget-object v1, p0, Lpoo;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 62
    :cond_5
    iget v0, p0, Lpoo;->g:I

    if-eq v0, v2, :cond_6

    .line 63
    const/4 v0, 0x7

    iget v1, p0, Lpoo;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 65
    :cond_6
    iget v0, p0, Lpoo;->h:I

    if-eq v0, v2, :cond_7

    .line 66
    const/16 v0, 0x8

    iget v1, p0, Lpoo;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 68
    :cond_7
    iget-object v0, p0, Lpoo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 70
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpoo;->a(Loxn;)Lpoo;

    move-result-object v0

    return-object v0
.end method
