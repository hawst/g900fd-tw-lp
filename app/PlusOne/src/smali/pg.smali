.class public final Lpg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lmy;


# instance fields
.field a:Z

.field private final b:Lpi;

.field private final c:Lms;

.field private d:Lpl;

.field private final e:I

.field private final f:I


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Lms;Landroid/graphics/drawable/Drawable;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/graphics/drawable/Drawable;",
            ":",
            "Lpl;",
            ">(",
            "Landroid/app/Activity;",
            "Landroid/support/v7/widget/Toolbar;",
            "Lms;",
            "TT;II)V"
        }
    .end annotation

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpg;->a:Z

    .line 194
    if-eqz p2, :cond_0

    .line 195
    new-instance v0, Lpq;

    invoke-direct {v0, p2}, Lpq;-><init>(Landroid/support/v7/widget/Toolbar;)V

    iput-object v0, p0, Lpg;->b:Lpi;

    .line 196
    new-instance v0, Lph;

    invoke-direct {v0, p0}, Lph;-><init>(Lpg;)V

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View$OnClickListener;)V

    .line 218
    :goto_0
    iput-object p3, p0, Lpg;->c:Lms;

    .line 219
    iput p5, p0, Lpg;->e:I

    .line 220
    iput p6, p0, Lpg;->f:I

    .line 221
    if-nez p4, :cond_5

    .line 222
    new-instance v0, Lpk;

    iget-object v1, p0, Lpg;->b:Lpi;

    invoke-interface {v1}, Lpi;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lpk;-><init>(Landroid/app/Activity;Landroid/content/Context;)V

    iput-object v0, p0, Lpg;->d:Lpl;

    .line 228
    :goto_1
    invoke-virtual {p0}, Lpg;->d()Landroid/graphics/drawable/Drawable;

    .line 229
    return-void

    .line 206
    :cond_0
    instance-of v0, p1, Lpj;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 207
    check-cast v0, Lpj;

    invoke-interface {v0}, Lpj;->a()Lpi;

    move-result-object v0

    iput-object v0, p0, Lpg;->b:Lpi;

    goto :goto_0

    .line 208
    :cond_1
    instance-of v0, p1, Lpp;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 209
    check-cast v0, Lpp;

    invoke-interface {v0}, Lpp;->j()Lpi;

    move-result-object v0

    iput-object v0, p0, Lpg;->b:Lpi;

    goto :goto_0

    .line 210
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_3

    .line 211
    new-instance v0, Lpo;

    invoke-direct {v0, p1}, Lpo;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lpg;->b:Lpi;

    goto :goto_0

    .line 212
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_4

    .line 213
    new-instance v0, Lpn;

    invoke-direct {v0, p1}, Lpn;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lpg;->b:Lpi;

    goto :goto_0

    .line 215
    :cond_4
    new-instance v0, Lpm;

    invoke-direct {v0, p1}, Lpm;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lpg;->b:Lpi;

    goto :goto_0

    .line 225
    :cond_5
    check-cast p4, Lpl;

    iput-object p4, p0, Lpg;->d:Lpl;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/app/Activity;Lms;II)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 150
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lpg;-><init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Lms;Landroid/graphics/drawable/Drawable;II)V

    .line 152
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const v2, 0x800003

    .line 241
    iget-object v0, p0, Lpg;->c:Lms;

    invoke-virtual {v0, v2}, Lms;->g(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    iget-object v0, p0, Lpg;->d:Lpl;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Lpl;->a(F)V

    .line 246
    :goto_0
    iget-boolean v0, p0, Lpg;->a:Z

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lpg;->d:Lpl;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lpg;->c:Lms;

    invoke-virtual {v1, v2}, Lms;->g(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lpg;->f:I

    :goto_1
    invoke-virtual {p0, v0, v1}, Lpg;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 251
    :cond_0
    return-void

    .line 244
    :cond_1
    iget-object v0, p0, Lpg;->d:Lpl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lpl;->a(F)V

    goto :goto_0

    .line 247
    :cond_2
    iget v1, p0, Lpg;->e:I

    goto :goto_1
.end method

.method a(I)V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lpg;->b:Lpi;

    invoke-interface {v0, p1}, Lpi;->a(I)V

    .line 457
    return-void
.end method

.method a(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lpg;->b:Lpi;

    invoke-interface {v0, p1, p2}, Lpi;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 453
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lpg;->d:Lpl;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Lpl;->a(F)V

    .line 393
    iget-boolean v0, p0, Lpg;->a:Z

    if-eqz v0, :cond_0

    .line 394
    iget v0, p0, Lpg;->f:I

    invoke-virtual {p0, v0}, Lpg;->a(I)V

    .line 396
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 3

    .prologue
    .line 380
    iget-object v0, p0, Lpg;->d:Lpl;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-static {v2, p2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-interface {v0, v1}, Lpl;->a(F)V

    .line 381
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 279
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lpg;->a:Z

    if-eqz v0, :cond_0

    .line 280
    invoke-virtual {p0}, Lpg;->c()V

    .line 281
    const/4 v0, 0x1

    .line 283
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 263
    invoke-virtual {p0}, Lpg;->d()Landroid/graphics/drawable/Drawable;

    .line 266
    invoke-virtual {p0}, Lpg;->a()V

    .line 267
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lpg;->d:Lpl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lpl;->a(F)V

    .line 408
    iget-boolean v0, p0, Lpg;->a:Z

    if-eqz v0, :cond_0

    .line 409
    iget v0, p0, Lpg;->e:I

    invoke-virtual {p0, v0}, Lpg;->a(I)V

    .line 411
    :cond_0
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    const v1, 0x800003

    .line 287
    iget-object v0, p0, Lpg;->c:Lms;

    invoke-virtual {v0, v1}, Lms;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lpg;->c:Lms;

    invoke-virtual {v0, v1}, Lms;->f(I)V

    .line 292
    :goto_0
    return-void

    .line 290
    :cond_0
    iget-object v0, p0, Lpg;->c:Lms;

    invoke-virtual {v0, v1}, Lms;->e(I)V

    goto :goto_0
.end method

.method d()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lpg;->b:Lpi;

    invoke-interface {v0}, Lpi;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
