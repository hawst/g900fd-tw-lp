.class public Lian;
.super Llol;
.source "PG"

# interfaces
.implements Lkhf;


# instance fields
.field public N:Lkhr;

.field private O:Lkhe;

.field private P:Landroid/content/Intent;

.field private Q:Landroid/content/Intent;

.field private R:Landroid/content/Intent;

.field private S:Landroid/content/Intent;

.field private T:Landroid/content/Intent;

.field private U:Landroid/content/Intent;

.field private V:Z

.field private W:Z

.field private X:Lcom/google/android/libraries/social/settings/LabelPreference;

.field private Y:Liaq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Llol;-><init>()V

    .line 62
    new-instance v0, Lkhe;

    iget-object v1, p0, Lian;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    iput-object v0, p0, Lian;->O:Lkhe;

    .line 87
    return-void
.end method

.method static synthetic a(Lian;)Liaq;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lian;->Y:Liaq;

    return-object v0
.end method

.method static synthetic b(Lian;)Llnl;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lian;->at:Llnl;

    return-object v0
.end method

.method private d(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 1

    .prologue
    .line 513
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Z)V

    .line 514
    iget-object v0, p0, Lian;->O:Lkhe;

    invoke-virtual {v0, p1}, Lkhe;->a(Lkhl;)Lkhl;

    .line 515
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 499
    new-instance v0, Lkhr;

    iget-object v1, p0, Lian;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lian;->N:Lkhr;

    .line 501
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a0421

    .line 502
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 501
    invoke-virtual {v0, v1}, Lkhr;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    .line 503
    invoke-direct {p0, v0}, Lian;->d(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 504
    invoke-virtual {p0, v0}, Lian;->a(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 506
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a0422

    .line 507
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 506
    invoke-virtual {v0, v1}, Lkhr;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    .line 508
    invoke-direct {p0, v0}, Lian;->d(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 509
    invoke-virtual {p0, v0}, Lian;->b(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 510
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 92
    if-eqz p1, :cond_6

    .line 93
    const-string v0, "state_account_status_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    const-string v0, "state_account_status_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lian;->P:Landroid/content/Intent;

    .line 97
    :cond_0
    const-string v0, "state_browse_experiments_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 98
    const-string v0, "state_browse_experiments_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lian;->Q:Landroid/content/Intent;

    .line 101
    :cond_1
    const-string v0, "state_network_requests_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 102
    const-string v0, "state_network_requests_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lian;->R:Landroid/content/Intent;

    .line 105
    :cond_2
    const-string v0, "state_network_stats_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 106
    const-string v0, "state_network_stats_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lian;->S:Landroid/content/Intent;

    .line 109
    :cond_3
    const-string v0, "state_upload_stats_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 110
    const-string v0, "state_upload_stats_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lian;->T:Landroid/content/Intent;

    .line 113
    :cond_4
    const-string v0, "state_tracing_pref_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 114
    const-string v0, "state_tracing_pref_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lian;->U:Landroid/content/Intent;

    .line 117
    :cond_5
    const-string v0, "state_show_apiary_pref"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lian;->V:Z

    .line 118
    const-string v0, "state_show_override_experiments_pref"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lian;->W:Z

    .line 121
    :cond_6
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 1

    .prologue
    .line 191
    const-string v0, "tracing_preferences"

    invoke-virtual {p0, v0, p1}, Lian;->a(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 192
    const-string v0, "debug.plus.frontend.config"

    invoke-virtual {p0, v0, p1}, Lian;->e(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 193
    const-string v0, "account_status_key"

    invoke-virtual {p0, v0, p1}, Lian;->h(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 194
    const-string v0, "experiments_key"

    invoke-virtual {p0, v0, p1}, Lian;->g(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 195
    const-string v0, "experiment_override_key"

    invoke-virtual {p0, v0, p1}, Lian;->f(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 196
    const-string v0, "debug.plus.force_sync"

    invoke-virtual {p0, v0, p1}, Lian;->i(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 197
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 3

    .prologue
    .line 212
    iget-object v0, p0, Lian;->U:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a056a

    .line 214
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 213
    invoke-virtual {v0, v1, v2}, Lkhr;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/LabelPreference;

    move-result-object v0

    iput-object v0, p0, Lian;->X:Lcom/google/android/libraries/social/settings/LabelPreference;

    .line 215
    iget-object v0, p0, Lian;->X:Lcom/google/android/libraries/social/settings/LabelPreference;

    iget-object v1, p0, Lian;->U:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/LabelPreference;->a(Landroid/content/Intent;)V

    .line 216
    iget-object v0, p0, Lian;->X:Lcom/google/android/libraries/social/settings/LabelPreference;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/String;)V

    .line 217
    if-eqz p2, :cond_1

    .line 218
    iget-object v0, p0, Lian;->X:Lcom/google/android/libraries/social/settings/LabelPreference;

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v0, p0, Lian;->O:Lkhe;

    iget-object v1, p0, Lian;->X:Lcom/google/android/libraries/social/settings/LabelPreference;

    invoke-virtual {v0, v1}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 179
    iput-boolean p1, p0, Lian;->V:Z

    .line 180
    return-void
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 138
    invoke-super {p0}, Llol;->aO_()V

    .line 139
    iget-object v0, p0, Lian;->X:Lcom/google/android/libraries/social/settings/LabelPreference;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lian;->at:Llnl;

    invoke-static {v0}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 141
    const-string v1, "debug.plus.frontend.tracing"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 143
    if-eqz v0, :cond_1

    const-string v0, "ON"

    .line 144
    :goto_0
    iget-object v1, p0, Lian;->X:Lcom/google/android/libraries/social/settings/LabelPreference;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/LabelPreference;->a(Ljava/lang/CharSequence;)V

    .line 146
    :cond_0
    return-void

    .line 143
    :cond_1
    const-string v0, "OFF"

    goto :goto_0
.end method

.method public b(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lian;->P:Landroid/content/Intent;

    .line 156
    return-void
.end method

.method public b(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 1

    .prologue
    .line 203
    const-string v0, "net_transactions_key"

    invoke-virtual {p0, v0, p1}, Lian;->j(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 204
    const-string v0, "net_stats_key"

    invoke-virtual {p0, v0, p1}, Lian;->k(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 205
    const-string v0, "upload_stats_key"

    invoke-virtual {p0, v0, p1}, Lian;->l(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 206
    return-void
.end method

.method public b(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a056a

    .line 240
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a056e

    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 239
    invoke-virtual {v0, v1, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    .line 241
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 242
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 243
    if-nez p2, :cond_0

    .line 244
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 248
    :goto_0
    return-void

    .line 246
    :cond_0
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 183
    iput-boolean p1, p0, Lian;->W:Z

    .line 184
    return-void
.end method

.method public c(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lian;->Q:Landroid/content/Intent;

    .line 160
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 151
    iget-object v0, p0, Lian;->au:Llnh;

    const-class v1, Liaq;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liaq;

    iput-object v0, p0, Lian;->Y:Liaq;

    .line 152
    return-void
.end method

.method public c(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 1

    .prologue
    .line 230
    const-string v0, "debug.plus.frontend.tracing"

    invoke-virtual {p0, v0, p1}, Lian;->b(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 231
    const-string v0, "tracing_token_key"

    invoke-virtual {p0, v0, p1}, Lian;->c(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 232
    const-string v0, "tracing_pattern_key"

    invoke-virtual {p0, v0, p1}, Lian;->d(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 233
    return-void
.end method

.method public c(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 3

    .prologue
    .line 254
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a056f

    .line 255
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0570

    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 254
    invoke-virtual {v0, v1, v2}, Lkhr;->f(Ljava/lang/String;Ljava/lang/String;)Lkgw;

    move-result-object v0

    .line 256
    invoke-virtual {v0, p1}, Lkgw;->d(Ljava/lang/String;)V

    .line 257
    if-nez p2, :cond_0

    .line 258
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    goto :goto_0
.end method

.method public d(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lian;->R:Landroid/content/Intent;

    .line 164
    return-void
.end method

.method public d(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a0571

    .line 269
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0572

    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 268
    invoke-virtual {v0, v1, v2}, Lkhr;->f(Ljava/lang/String;Ljava/lang/String;)Lkgw;

    move-result-object v0

    .line 270
    invoke-virtual {v0, p1}, Lkgw;->d(Ljava/lang/String;)V

    .line 271
    const-string v1, ""

    invoke-virtual {v0, v1}, Lkgw;->a(Ljava/lang/Object;)V

    .line 272
    if-nez p2, :cond_0

    .line 273
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 277
    :goto_0
    return-void

    .line 275
    :cond_0
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    goto :goto_0
.end method

.method public e(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lian;->S:Landroid/content/Intent;

    .line 168
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 126
    const-string v0, "state_account_status_intent"

    iget-object v1, p0, Lian;->P:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 127
    const-string v0, "state_browse_experiments_intent"

    iget-object v1, p0, Lian;->Q:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 128
    const-string v0, "state_network_requests_intent"

    iget-object v1, p0, Lian;->R:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 129
    const-string v0, "state_network_stats_intent"

    iget-object v1, p0, Lian;->S:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 130
    const-string v0, "state_upload_stats_intent"

    iget-object v1, p0, Lian;->T:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 131
    const-string v0, "state_tracing_pref_intent"

    iget-object v1, p0, Lian;->U:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 132
    const-string v0, "state_show_apiary_pref"

    iget-boolean v1, p0, Lian;->V:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 133
    const-string v0, "state_show_override_experiments_pref"

    iget-boolean v1, p0, Lian;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 134
    return-void
.end method

.method public e(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 283
    iget-boolean v0, p0, Lian;->V:Z

    if-eqz v0, :cond_3

    .line 284
    iget-object v0, p0, Lian;->N:Lkhr;

    const v2, 0x7f0a0573

    .line 285
    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0574

    .line 286
    invoke-virtual {p0, v3}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 284
    invoke-virtual {v0, v2, v3}, Lkhr;->g(Ljava/lang/String;Ljava/lang/String;)Lkha;

    move-result-object v4

    .line 287
    invoke-virtual {v4, p1}, Lkha;->d(Ljava/lang/String;)V

    .line 288
    const-string v0, ""

    invoke-virtual {v4, v0}, Lkha;->a(Ljava/lang/Object;)V

    .line 289
    invoke-static {}, Liar;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    iget-object v0, p0, Lian;->at:Llnl;

    invoke-static {v0}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lian;->at:Llnl;

    const-class v3, Lkfn;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfn;

    instance-of v3, v0, Liak;

    if-eqz v3, :cond_0

    check-cast v0, Liak;

    invoke-virtual {v0}, Liak;->b()Lkfn;

    move-result-object v0

    :cond_0
    const-string v3, "plusi"

    invoke-interface {v0, v3}, Lkfn;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "ADB Default - "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v5, v1

    const-string v0, ""

    aput-object v0, v6, v1

    const/4 v0, 0x1

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v0

    move v3, v1

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    aput-object v1, v5, v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    aput-object v0, v6, v2

    aget-object v0, v6, v2

    invoke-static {v7, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v1, v2

    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v4, v5}, Lkha;->a([Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v6}, Lkha;->b([Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v3}, Lkha;->d(I)V

    aget-object v0, v5, v3

    invoke-virtual {v4, v0}, Lkha;->d(Ljava/lang/CharSequence;)V

    new-instance v0, Liap;

    invoke-direct {v0, p0, p1, v4}, Liap;-><init>(Lian;Ljava/lang/String;Lkha;)V

    invoke-virtual {v4, v0}, Lkha;->a(Lkhp;)V

    .line 290
    if-eqz p2, :cond_4

    .line 291
    invoke-virtual {p2, v4}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 296
    :cond_3
    :goto_3
    return-void

    .line 293
    :cond_4
    iget-object v0, p0, Lian;->O:Lkhe;

    invoke-virtual {v0, v4}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_2
.end method

.method public f(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lian;->T:Landroid/content/Intent;

    .line 172
    return-void
.end method

.method public f(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 3

    .prologue
    .line 302
    iget-boolean v0, p0, Lian;->W:Z

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a0575

    .line 304
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0576

    .line 305
    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 303
    invoke-virtual {v0, v1, v2}, Lkhr;->f(Ljava/lang/String;Ljava/lang/String;)Lkgw;

    move-result-object v0

    .line 306
    invoke-virtual {v0, p1}, Lkgw;->d(Ljava/lang/String;)V

    .line 307
    const v1, 0x7f0401a8

    invoke-virtual {v0, v1}, Lkgw;->c(I)V

    .line 308
    if-eqz p2, :cond_1

    .line 309
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_0
.end method

.method public g(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lian;->U:Landroid/content/Intent;

    .line 176
    return-void
.end method

.method public g(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 4

    .prologue
    .line 321
    iget-object v0, p0, Lian;->Q:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a0429

    .line 323
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a042a

    .line 324
    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lian;->Q:Landroid/content/Intent;

    .line 322
    invoke-virtual {v0, v1, v2, v3}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v0

    .line 326
    invoke-virtual {v0, p1}, Lkhl;->d(Ljava/lang/String;)V

    .line 327
    if-eqz p2, :cond_1

    .line 328
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_0
.end method

.method public h(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 4

    .prologue
    .line 340
    iget-object v0, p0, Lian;->P:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a042b

    .line 342
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a042c

    .line 343
    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lian;->P:Landroid/content/Intent;

    .line 341
    invoke-virtual {v0, v1, v2, v3}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v0

    .line 345
    invoke-virtual {v0, p1}, Lkhl;->d(Ljava/lang/String;)V

    .line 346
    if-eqz p2, :cond_1

    .line 347
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_0
.end method

.method public i(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 3

    .prologue
    .line 359
    iget-object v0, p0, Lian;->Y:Liaq;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a042d

    .line 361
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a042e

    .line 362
    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 360
    invoke-virtual {v0, v1, v2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    .line 363
    invoke-virtual {v0, p1}, Lkhl;->d(Ljava/lang/String;)V

    .line 364
    new-instance v1, Liao;

    invoke-direct {v1, p0}, Liao;-><init>(Lian;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 371
    if-eqz p2, :cond_1

    .line 372
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_0
.end method

.method public j(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 4

    .prologue
    .line 384
    iget-object v0, p0, Lian;->R:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a0427

    .line 386
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0428

    .line 387
    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lian;->R:Landroid/content/Intent;

    .line 385
    invoke-virtual {v0, v1, v2, v3}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v0

    .line 389
    invoke-virtual {v0, p1}, Lkhl;->d(Ljava/lang/String;)V

    .line 390
    if-eqz p2, :cond_1

    .line 391
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_0
.end method

.method public k(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 4

    .prologue
    .line 403
    iget-object v0, p0, Lian;->S:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a0423

    .line 405
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0424

    .line 406
    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lian;->S:Landroid/content/Intent;

    .line 404
    invoke-virtual {v0, v1, v2, v3}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v0

    .line 408
    invoke-virtual {v0, p1}, Lkhl;->d(Ljava/lang/String;)V

    .line 409
    if-eqz p2, :cond_1

    .line 410
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_0
.end method

.method public l(Ljava/lang/String;Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 4

    .prologue
    .line 422
    iget-object v0, p0, Lian;->T:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lian;->N:Lkhr;

    const v1, 0x7f0a0425

    .line 424
    invoke-virtual {p0, v1}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0426

    .line 425
    invoke-virtual {p0, v2}, Lian;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lian;->T:Landroid/content/Intent;

    .line 423
    invoke-virtual {v0, v1, v2, v3}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v0

    .line 427
    invoke-virtual {v0, p1}, Lkhl;->d(Ljava/lang/String;)V

    .line 428
    if-eqz p2, :cond_1

    .line 429
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 431
    :cond_1
    iget-object v1, p0, Lian;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    goto :goto_0
.end method
