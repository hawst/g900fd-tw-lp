.class public final Lecb;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnLongClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private ah:Levf;

.field private ai:Ljava/lang/Integer;

.field private aj:Z

.field private ak:Z

.field private final al:Lfhh;

.field private final am:Licq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Legi;-><init>()V

    .line 78
    new-instance v0, Lecc;

    invoke-direct {v0, p0}, Lecc;-><init>(Lecb;)V

    iput-object v0, p0, Lecb;->al:Lfhh;

    .line 86
    new-instance v0, Licq;

    iget-object v1, p0, Lecb;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    new-instance v1, Lecd;

    invoke-direct {v1, p0}, Lecd;-><init>(Lecb;)V

    .line 87
    invoke-virtual {v0, v1}, Licq;->a(Lico;)Licq;

    move-result-object v0

    iput-object v0, p0, Lecb;->am:Licq;

    .line 435
    return-void
.end method

.method static synthetic a(Lecb;)Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    return-object v0
.end method

.method private a(ILfib;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 406
    iget-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    instance-of v0, p2, Lfhx;

    if-nez v0, :cond_1

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    check-cast p2, Lfhx;

    .line 413
    invoke-virtual {p2}, Lfhx;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "#autoawesome"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    const/4 v0, 0x0

    iput-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    .line 418
    invoke-virtual {p2}, Lfhx;->e()Z

    move-result v0

    iput-boolean v0, p0, Lecb;->Z:Z

    .line 419
    iget-boolean v0, p0, Lecb;->Z:Z

    if-eqz v0, :cond_2

    .line 420
    invoke-virtual {p0}, Lecb;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 421
    const v1, 0x7f0a07ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 422
    invoke-virtual {p0}, Lecb;->n()Lz;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 425
    :cond_2
    iput-boolean v3, p0, Lecb;->aj:Z

    .line 426
    invoke-virtual {p0}, Lecb;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lecb;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 381
    if-nez p1, :cond_0

    .line 403
    :goto_0
    return-void

    .line 385
    :cond_0
    invoke-virtual {p0}, Lecb;->U()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lecb;->aj:Z

    if-nez v0, :cond_5

    .line 386
    :cond_1
    iget-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 387
    iget-object v1, p0, Lecb;->am:Licq;

    iget-boolean v0, p0, Lecb;->aj:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0a064e

    :goto_1
    invoke-virtual {v1, v0}, Licq;->b(I)Licq;

    .line 389
    iget-object v0, p0, Lecb;->am:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 401
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lecb;->Z_()V

    .line 402
    invoke-virtual {p0}, Lecb;->ag()V

    goto :goto_0

    .line 387
    :cond_3
    const v0, 0x7f0a057b

    goto :goto_1

    .line 391
    :cond_4
    iget-object v0, p0, Lecb;->am:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 394
    :cond_5
    iget-object v0, p0, Lecb;->am:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 396
    if-eqz p2, :cond_2

    .line 397
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h()V

    goto :goto_2
.end method

.method static synthetic a(Lecb;ILfib;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lecb;->a(ILfib;)V

    return-void
.end method

.method private ac()V
    .locals 9

    .prologue
    const/4 v3, 0x1

    .line 458
    iget-object v0, p0, Lecb;->at:Llnl;

    iget-object v1, p0, Lecb;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "#autoawesome"

    const/4 v4, 0x0

    const/4 v5, 0x5

    new-array v6, v3, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "#autoawesome"

    aput-object v8, v6, v7

    .line 460
    invoke-static {v5, v6}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 458
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    .line 461
    return-void
.end method

.method private d(I)Z
    .locals 1

    .prologue
    .line 430
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljfb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecb;->at:Llnl;

    .line 431
    invoke-static {v0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 247
    sget-object v0, Lhmw;->an:Lhmw;

    return-object v0
.end method

.method public J_()Z
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 444
    invoke-super {p0}, Legi;->L_()V

    .line 446
    iget-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 455
    :goto_0
    return-void

    .line 450
    :cond_0
    iput-boolean v1, p0, Lecb;->Z:Z

    .line 451
    invoke-direct {p0}, Lecb;->ac()V

    .line 452
    invoke-virtual {p0}, Lecb;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lecb;->a(Landroid/view/View;Z)V

    .line 453
    iget-object v0, p0, Lecb;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lecb;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ek:Lhmv;

    .line 454
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 453
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lecb;->ah:Levf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecb;->ah:Levf;

    invoke-virtual {v0}, Levf;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l()Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 118
    iget-object v0, p0, Lecb;->at:Llnl;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400de

    invoke-super {p0, v0, p2, p3, v1}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 120
    iget-object v0, p0, Lecb;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0291

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 123
    new-instance v0, Levf;

    iget-object v3, p0, Lecb;->at:Llnl;

    iget-object v4, p0, Lecb;->P:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v0, v3, v4}, Levf;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lecb;->ah:Levf;

    .line 124
    iget-object v0, p0, Lecb;->ah:Levf;

    invoke-virtual {v0, p0}, Levf;->a(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lecb;->ah:Levf;

    invoke-virtual {v0, p0}, Levf;->a(Landroid/view/View$OnLongClickListener;)V

    .line 126
    iget-object v0, p0, Lecb;->ah:Levf;

    const/4 v3, 0x5

    new-array v4, v6, [Ljava/lang/String;

    const-string v5, "#autoawesome"

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v6}, Levf;->a(Ljava/lang/String;I)V

    .line 128
    iget-object v0, p0, Lecb;->ah:Levf;

    const-string v3, "#autoawesome"

    invoke-virtual {v0, v3}, Levf;->a(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lecb;->ah:Levf;

    invoke-virtual {v0, v6}, Levf;->a(Z)V

    .line 131
    iget-object v0, p0, Lecb;->ah:Levf;

    invoke-virtual {v0, v6}, Levf;->b(Z)V

    .line 133
    const v0, 0x7f100304

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 134
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 135
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v2, Ljvl;

    iget-object v3, p0, Lecb;->at:Llnl;

    invoke-direct {v2, v3}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v2, v2, Ljvl;->a:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 136
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(Z)V

    .line 137
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v2, Lece;

    invoke-direct {v2}, Lece;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 138
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v2, p0, Lecb;->ah:Levf;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 139
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const v2, 0x7f020415

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 140
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkc;)V

    .line 142
    iget-object v0, p0, Lecb;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-direct {p0, v0}, Lecb;->d(I)Z

    move-result v0

    iput-boolean v0, p0, Lecb;->ak:Z

    .line 143
    iget-object v0, p0, Lecb;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lecb;->ak:Z

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p0}, Lecb;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v7, v8, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 147
    :cond_0
    invoke-virtual {p0}, Lecb;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v6, v8, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 149
    invoke-direct {p0, v1, v7}, Lecb;->a(Landroid/view/View;Z)V

    .line 154
    iget-object v0, p0, Lecb;->at:Llnl;

    iget-object v2, p0, Lecb;->P:Lhee;

    .line 155
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 154
    invoke-static {v0, v2, v7}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->c(Landroid/content/Context;IZ)V

    .line 158
    if-nez p3, :cond_1

    .line 160
    invoke-direct {p0}, Lecb;->ac()V

    .line 163
    :cond_1
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 319
    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    .line 331
    :goto_0
    return-object v0

    .line 321
    :pswitch_0
    new-instance v0, Levg;

    iget-object v1, p0, Lecb;->at:Llnl;

    iget-object v2, p0, Lecb;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget v3, p0, Lecb;->aa:I

    invoke-direct {v0, v1, v2, v3}, Levg;-><init>(Landroid/content/Context;II)V

    goto :goto_0

    .line 325
    :pswitch_1
    new-instance v0, Lfbu;

    iget-object v1, p0, Lecb;->at:Llnl;

    iget-object v2, p0, Lecb;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x5

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "#autoawesome"

    aput-object v7, v5, v6

    .line 326
    invoke-static {v3, v5}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget v5, p0, Lecb;->aa:I

    invoke-direct/range {v0 .. v5}, Lfbu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;I)V

    goto :goto_0

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 104
    if-eqz p1, :cond_1

    .line 105
    const-string v0, "photo_search_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const-string v0, "photo_search_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    .line 109
    :cond_0
    const-string v0, "results_present"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    const-string v0, "results_present"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lecb;->aj:Z

    .line 113
    :cond_1
    return-void
.end method

.method public a(Ldo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 357
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    if-nez v0, :cond_0

    .line 358
    iget-object v0, p0, Lecb;->ah:Levf;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Levf;->a(Landroid/database/Cursor;)V

    .line 360
    :cond_0
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 338
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 339
    packed-switch v0, :pswitch_data_0

    .line 351
    :goto_0
    iget-boolean v2, p0, Lecb;->aj:Z

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    or-int/2addr v0, v2

    iput-boolean v0, p0, Lecb;->aj:Z

    .line 352
    invoke-virtual {p0}, Lecb;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lecb;->a(Landroid/view/View;Z)V

    .line 353
    return-void

    .line 341
    :pswitch_0
    iget-object v0, p0, Lecb;->ah:Levf;

    invoke-virtual {v0, p2}, Levf;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 346
    :pswitch_1
    iget-object v0, p0, Lecb;->ah:Levf;

    invoke-virtual {v0, p2}, Levf;->b(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 351
    goto :goto_1

    .line 339
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 59
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lecb;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 220
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public aO_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    invoke-super {p0}, Legi;->aO_()V

    .line 175
    iget-object v0, p0, Lecb;->al:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 178
    iget-boolean v0, p0, Lecb;->ak:Z

    iget-object v1, p0, Lecb;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-direct {p0, v1}, Lecb;->d(I)Z

    move-result v1

    iput-boolean v1, p0, Lecb;->ak:Z

    iget-boolean v1, p0, Lecb;->ak:Z

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lecb;->ak:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lecb;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    :goto_0
    invoke-direct {p0}, Lecb;->ac()V

    .line 180
    :cond_0
    iget-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    invoke-virtual {p0}, Lecb;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lecb;->am:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 191
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lecb;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100319

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 192
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 193
    return-void

    .line 178
    :cond_2
    invoke-virtual {p0}, Lecb;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbb;->a(I)V

    goto :goto_0

    .line 186
    :cond_3
    iget-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lecb;->ai:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lecb;->a(ILfib;)V

    goto :goto_1
.end method

.method protected b(Lhjk;)V
    .locals 2

    .prologue
    .line 225
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 226
    const v0, 0x7f0a0a79

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 227
    invoke-virtual {p0}, Lecb;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 228
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lecb;->a(Lhjk;I)V

    .line 230
    const v0, 0x7f10067b

    .line 231
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 232
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 233
    return-void
.end method

.method public c(Landroid/view/View;)Z
    .locals 8

    .prologue
    const v1, 0x7f100093

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 252
    if-eqz p1, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v3

    .line 304
    :goto_0
    return v0

    .line 256
    :cond_1
    instance-of v0, p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 257
    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTileView;

    .line 258
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v0

    .line 259
    invoke-virtual {p0, v0}, Lecb;->a(Lizu;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 260
    goto :goto_0

    .line 264
    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v0

    .line 266
    iget-object v4, p0, Lecb;->P:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    .line 268
    instance-of v5, p1, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;

    if-eqz v5, :cond_3

    .line 269
    const v0, 0x7f10007f

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 270
    iget-object v1, p0, Lecb;->at:Llnl;

    const-class v4, Lcnt;

    invoke-static {v1, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcnt;

    .line 271
    invoke-virtual {v1, v0, v3}, Lcnt;->a(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 300
    :goto_1
    invoke-static {p1}, Llsn;->b(Landroid/view/View;)V

    .line 301
    iget-object v0, p0, Lecb;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lecb;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->dZ:Lhmv;

    .line 302
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 301
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 303
    invoke-virtual {p0, v1}, Lecb;->b(Landroid/content/Intent;)V

    move v0, v2

    .line 304
    goto :goto_0

    .line 272
    :cond_3
    const-wide/32 v6, 0x40000

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 273
    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTileView;

    .line 274
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v0

    .line 275
    new-instance v1, Ljcn;

    invoke-direct {v1}, Ljcn;-><init>()V

    .line 276
    new-instance v5, Ljuc;

    invoke-direct {v5, v0}, Ljuc;-><init>(Lizu;)V

    invoke-virtual {v1, v5}, Ljcn;->a(Ljcl;)V

    .line 277
    new-instance v5, Ldew;

    invoke-virtual {p0}, Lecb;->n()Lz;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 278
    invoke-virtual {v5, v0}, Ldew;->a(Lizu;)Ldew;

    move-result-object v0

    .line 279
    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lecb;->Y:Lctq;

    .line 280
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    .line 281
    invoke-virtual {v0, v2}, Ldew;->d(Z)Ldew;

    move-result-object v0

    .line 282
    invoke-virtual {v0, v3}, Ldew;->i(Z)Ldew;

    move-result-object v0

    const/4 v1, 0x3

    .line 283
    invoke-virtual {v0, v1}, Ldew;->b(I)Ldew;

    move-result-object v0

    .line 284
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 285
    goto :goto_1

    .line 286
    :cond_4
    const v0, 0x7f100079

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 287
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v0, v3

    .line 288
    goto/16 :goto_0

    .line 290
    :cond_5
    new-instance v1, Ldew;

    invoke-virtual {p0}, Lecb;->n()Lz;

    move-result-object v5

    invoke-direct {v1, v5, v4}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 291
    invoke-virtual {v1, v0}, Ldew;->a(Ljava/lang/String;)Ldew;

    move-result-object v0

    const/4 v1, 0x5

    new-array v4, v2, [Ljava/lang/String;

    const-string v5, "#autoawesome"

    aput-object v5, v4, v3

    .line 292
    invoke-static {v1, v4}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lecb;->X:Lctz;

    .line 293
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lecb;->Y:Lctq;

    .line 294
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    .line 295
    invoke-virtual {v0, v3}, Ldew;->i(Z)Ldew;

    move-result-object v0

    iget-object v1, p0, Lecb;->Y:Lctq;

    .line 296
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->b(I)Ldew;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 209
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 211
    const-string v0, "results_present"

    iget-boolean v1, p0, Lecb;->aj:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 213
    iget-object v0, p0, Lecb;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 214
    const-string v0, "photo_search_request"

    iget-object v1, p0, Lecb;->ai:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 216
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Legi;->g()V

    .line 198
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 199
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 203
    invoke-super {p0}, Legi;->h()V

    .line 204
    iget-object v0, p0, Lecb;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 205
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 309
    instance-of v1, p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    if-eqz v1, :cond_0

    .line 310
    check-cast p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    invoke-virtual {p0, p1}, Lecb;->b(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    .line 311
    invoke-virtual {p0, v0}, Lecb;->e(I)V

    .line 312
    const/4 v0, 0x1

    .line 314
    :cond_0
    return v0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 168
    invoke-super {p0}, Legi;->z()V

    .line 169
    iget-object v0, p0, Lecb;->al:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 170
    return-void
.end method
