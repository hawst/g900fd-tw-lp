.class public final Lmdk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnci;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21625
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21628
    iput-object v0, p0, Lmdk;->apiHeader:Llyq;

    .line 21631
    iput-object v0, p0, Lmdk;->a:Lnci;

    .line 21625
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 21648
    const/4 v0, 0x0

    .line 21649
    iget-object v1, p0, Lmdk;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 21650
    const/4 v0, 0x1

    iget-object v1, p0, Lmdk;->apiHeader:Llyq;

    .line 21651
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21653
    :cond_0
    iget-object v1, p0, Lmdk;->a:Lnci;

    if-eqz v1, :cond_1

    .line 21654
    const/4 v1, 0x2

    iget-object v2, p0, Lmdk;->a:Lnci;

    .line 21655
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21657
    :cond_1
    iget-object v1, p0, Lmdk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21658
    iput v0, p0, Lmdk;->ai:I

    .line 21659
    return v0
.end method

.method public a(Loxn;)Lmdk;
    .locals 2

    .prologue
    .line 21667
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 21668
    sparse-switch v0, :sswitch_data_0

    .line 21672
    iget-object v1, p0, Lmdk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 21673
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdk;->ah:Ljava/util/List;

    .line 21676
    :cond_1
    iget-object v1, p0, Lmdk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 21678
    :sswitch_0
    return-object p0

    .line 21683
    :sswitch_1
    iget-object v0, p0, Lmdk;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 21684
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmdk;->apiHeader:Llyq;

    .line 21686
    :cond_2
    iget-object v0, p0, Lmdk;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21690
    :sswitch_2
    iget-object v0, p0, Lmdk;->a:Lnci;

    if-nez v0, :cond_3

    .line 21691
    new-instance v0, Lnci;

    invoke-direct {v0}, Lnci;-><init>()V

    iput-object v0, p0, Lmdk;->a:Lnci;

    .line 21693
    :cond_3
    iget-object v0, p0, Lmdk;->a:Lnci;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21668
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 21636
    iget-object v0, p0, Lmdk;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 21637
    const/4 v0, 0x1

    iget-object v1, p0, Lmdk;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21639
    :cond_0
    iget-object v0, p0, Lmdk;->a:Lnci;

    if-eqz v0, :cond_1

    .line 21640
    const/4 v0, 0x2

    iget-object v1, p0, Lmdk;->a:Lnci;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21642
    :cond_1
    iget-object v0, p0, Lmdk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 21644
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 21621
    invoke-virtual {p0, p1}, Lmdk;->a(Loxn;)Lmdk;

    move-result-object v0

    return-object v0
.end method
