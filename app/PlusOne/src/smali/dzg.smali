.class public final Ldzg;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:[Ljava/lang/String;

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 31
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Ldzg;->b:Ldp;

    .line 32
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->c:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Ldzg;->a(Landroid/net/Uri;)V

    .line 33
    iput p2, p0, Ldzg;->c:I

    .line 34
    iput-object p4, p0, Ldzg;->d:[Ljava/lang/String;

    .line 35
    iput p3, p0, Ldzg;->e:I

    .line 36
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 40
    iget v0, p0, Ldzg;->c:I

    .line 42
    invoke-virtual {p0}, Ldzg;->n()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Ldzg;->e:I

    iget-object v3, p0, Ldzg;->d:[Ljava/lang/String;

    .line 41
    invoke-static {v1, v0, v2, v3}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 44
    if-eqz v0, :cond_0

    .line 45
    iget-object v1, p0, Ldzg;->b:Ldp;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 47
    :cond_0
    return-object v0
.end method
