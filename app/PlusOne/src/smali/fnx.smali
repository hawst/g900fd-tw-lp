.class public final Lfnx;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Lmol;

.field private final f:Lhei;

.field private final h:Lkfd;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;Lmol;)V
    .locals 1

    .prologue
    .line 31
    const-string v0, "ReportStoryAbuseTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 32
    iput-object p1, p0, Lfnx;->a:Landroid/content/Context;

    .line 33
    iput p2, p0, Lfnx;->b:I

    .line 34
    iput p3, p0, Lfnx;->c:I

    .line 35
    iput-object p4, p0, Lfnx;->d:Ljava/lang/String;

    .line 36
    iput-object p5, p0, Lfnx;->e:Lmol;

    .line 37
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfnx;->f:Lhei;

    .line 38
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lfnx;->h:Lkfd;

    .line 39
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 43
    iget-object v0, p0, Lfnx;->f:Lhei;

    iget v1, p0, Lfnx;->b:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v3

    .line 44
    new-instance v0, Lfnp;

    iget-object v1, p0, Lfnx;->a:Landroid/content/Context;

    new-instance v2, Lkfo;

    const-string v4, "account_name"

    .line 45
    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v6}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget v3, p0, Lfnx;->c:I

    iget-object v4, p0, Lfnx;->d:Ljava/lang/String;

    iget-object v5, p0, Lfnx;->e:Lmol;

    invoke-direct/range {v0 .. v5}, Lfnp;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Lmol;)V

    .line 47
    iget-object v1, p0, Lfnx;->h:Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 48
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v1, v2, v0, v6}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v1
.end method
