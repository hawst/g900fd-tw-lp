.class public final Lncb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lnzx;

.field public c:[Lnzx;

.field public d:[Lnyz;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0}, Loxq;-><init>()V

    .line 332
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lncb;->b:[Lnzx;

    .line 335
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lncb;->c:[Lnzx;

    .line 338
    sget-object v0, Lnyz;->a:[Lnyz;

    iput-object v0, p0, Lncb;->d:[Lnyz;

    .line 325
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 381
    .line 382
    iget-object v0, p0, Lncb;->a:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 383
    const/4 v0, 0x1

    iget-object v2, p0, Lncb;->a:Ljava/lang/String;

    .line 384
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 386
    :goto_0
    iget-object v2, p0, Lncb;->b:[Lnzx;

    if-eqz v2, :cond_1

    .line 387
    iget-object v3, p0, Lncb;->b:[Lnzx;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 388
    if-eqz v5, :cond_0

    .line 389
    const/4 v6, 0x2

    .line 390
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 387
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 394
    :cond_1
    iget-object v2, p0, Lncb;->c:[Lnzx;

    if-eqz v2, :cond_3

    .line 395
    iget-object v3, p0, Lncb;->c:[Lnzx;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 396
    if-eqz v5, :cond_2

    .line 397
    const/4 v6, 0x3

    .line 398
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 395
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 402
    :cond_3
    iget-object v2, p0, Lncb;->d:[Lnyz;

    if-eqz v2, :cond_5

    .line 403
    iget-object v2, p0, Lncb;->d:[Lnyz;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 404
    if-eqz v4, :cond_4

    .line 405
    const/4 v5, 0x4

    .line 406
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 403
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 410
    :cond_5
    iget-object v1, p0, Lncb;->e:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 411
    const/4 v1, 0x5

    iget-object v2, p0, Lncb;->e:Ljava/lang/String;

    .line 412
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 414
    :cond_6
    iget-object v1, p0, Lncb;->f:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 415
    const/4 v1, 0x6

    iget-object v2, p0, Lncb;->f:Ljava/lang/String;

    .line 416
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 418
    :cond_7
    iget-object v1, p0, Lncb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 419
    iput v0, p0, Lncb;->ai:I

    .line 420
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lncb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 428
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 429
    sparse-switch v0, :sswitch_data_0

    .line 433
    iget-object v2, p0, Lncb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 434
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lncb;->ah:Ljava/util/List;

    .line 437
    :cond_1
    iget-object v2, p0, Lncb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    :sswitch_0
    return-object p0

    .line 444
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncb;->a:Ljava/lang/String;

    goto :goto_0

    .line 448
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 449
    iget-object v0, p0, Lncb;->b:[Lnzx;

    if-nez v0, :cond_3

    move v0, v1

    .line 450
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 451
    iget-object v3, p0, Lncb;->b:[Lnzx;

    if-eqz v3, :cond_2

    .line 452
    iget-object v3, p0, Lncb;->b:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 454
    :cond_2
    iput-object v2, p0, Lncb;->b:[Lnzx;

    .line 455
    :goto_2
    iget-object v2, p0, Lncb;->b:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 456
    iget-object v2, p0, Lncb;->b:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 457
    iget-object v2, p0, Lncb;->b:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 458
    invoke-virtual {p1}, Loxn;->a()I

    .line 455
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 449
    :cond_3
    iget-object v0, p0, Lncb;->b:[Lnzx;

    array-length v0, v0

    goto :goto_1

    .line 461
    :cond_4
    iget-object v2, p0, Lncb;->b:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 462
    iget-object v2, p0, Lncb;->b:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 466
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 467
    iget-object v0, p0, Lncb;->c:[Lnzx;

    if-nez v0, :cond_6

    move v0, v1

    .line 468
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 469
    iget-object v3, p0, Lncb;->c:[Lnzx;

    if-eqz v3, :cond_5

    .line 470
    iget-object v3, p0, Lncb;->c:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 472
    :cond_5
    iput-object v2, p0, Lncb;->c:[Lnzx;

    .line 473
    :goto_4
    iget-object v2, p0, Lncb;->c:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 474
    iget-object v2, p0, Lncb;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 475
    iget-object v2, p0, Lncb;->c:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 476
    invoke-virtual {p1}, Loxn;->a()I

    .line 473
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 467
    :cond_6
    iget-object v0, p0, Lncb;->c:[Lnzx;

    array-length v0, v0

    goto :goto_3

    .line 479
    :cond_7
    iget-object v2, p0, Lncb;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 480
    iget-object v2, p0, Lncb;->c:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 484
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 485
    iget-object v0, p0, Lncb;->d:[Lnyz;

    if-nez v0, :cond_9

    move v0, v1

    .line 486
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyz;

    .line 487
    iget-object v3, p0, Lncb;->d:[Lnyz;

    if-eqz v3, :cond_8

    .line 488
    iget-object v3, p0, Lncb;->d:[Lnyz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 490
    :cond_8
    iput-object v2, p0, Lncb;->d:[Lnyz;

    .line 491
    :goto_6
    iget-object v2, p0, Lncb;->d:[Lnyz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 492
    iget-object v2, p0, Lncb;->d:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 493
    iget-object v2, p0, Lncb;->d:[Lnyz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 494
    invoke-virtual {p1}, Loxn;->a()I

    .line 491
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 485
    :cond_9
    iget-object v0, p0, Lncb;->d:[Lnyz;

    array-length v0, v0

    goto :goto_5

    .line 497
    :cond_a
    iget-object v2, p0, Lncb;->d:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 498
    iget-object v2, p0, Lncb;->d:[Lnyz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 502
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncb;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 506
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncb;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 429
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 345
    iget-object v1, p0, Lncb;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 346
    const/4 v1, 0x1

    iget-object v2, p0, Lncb;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 348
    :cond_0
    iget-object v1, p0, Lncb;->b:[Lnzx;

    if-eqz v1, :cond_2

    .line 349
    iget-object v2, p0, Lncb;->b:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 350
    if-eqz v4, :cond_1

    .line 351
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 349
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 355
    :cond_2
    iget-object v1, p0, Lncb;->c:[Lnzx;

    if-eqz v1, :cond_4

    .line 356
    iget-object v2, p0, Lncb;->c:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 357
    if-eqz v4, :cond_3

    .line 358
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 356
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 362
    :cond_4
    iget-object v1, p0, Lncb;->d:[Lnyz;

    if-eqz v1, :cond_6

    .line 363
    iget-object v1, p0, Lncb;->d:[Lnyz;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 364
    if-eqz v3, :cond_5

    .line 365
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 363
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 369
    :cond_6
    iget-object v0, p0, Lncb;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 370
    const/4 v0, 0x5

    iget-object v1, p0, Lncb;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 372
    :cond_7
    iget-object v0, p0, Lncb;->f:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 373
    const/4 v0, 0x6

    iget-object v1, p0, Lncb;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 375
    :cond_8
    iget-object v0, p0, Lncb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 377
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Lncb;->a(Loxn;)Lncb;

    move-result-object v0

    return-object v0
.end method
