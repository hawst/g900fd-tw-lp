.class public final Lesj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llpn;
.implements Llpu;
.implements Llpz;
.implements Llqb;
.implements Llqz;
.implements Llrb;
.implements Llrc;
.implements Llrd;
.implements Llrg;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/app/Activity;

.field private final c:Lhmq;

.field private d:Lhms;

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "ActivityInstrumentationMixin"

    invoke-static {v0}, Llse;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lesj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Llqr;Lhmq;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lesj;->b:Landroid/app/Activity;

    .line 84
    iput-object p3, p0, Lesj;->c:Lhmq;

    .line 85
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 86
    return-void
.end method

.method private c(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 167
    :try_start_0
    iget-object v0, p0, Lesj;->c:Lhmq;

    invoke-interface {v0}, Lhmq;->F_()Lhmw;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_0

    .line 169
    const-string v1, "com.google.plus.analytics.intent.extra.START_VIEW"

    invoke-virtual {v0}, Lhmw;->ordinal()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 170
    const-string v0, "com.google.plus.analytics.intent.extra.START_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 172
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 173
    iget-object v1, p0, Lesj;->c:Lhmq;

    invoke-interface {v1, v0}, Lhmq;->b(Landroid/os/Bundle;)V

    .line 174
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 175
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_0
    :goto_0
    return-object p1

    .line 185
    :catch_0
    move-exception v0

    sget-object v0, Lesj;->a:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    sget-object v0, Lesj;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private static d(Landroid/content/Intent;)Lhmw;
    .locals 2

    .prologue
    .line 268
    const-string v0, "com.google.plus.analytics.intent.extra.START_VIEW"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 269
    invoke-static {v0}, Lhmw;->a(I)Lhmw;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 195
    iget-boolean v0, p0, Lesj;->g:Z

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lesj;->d:Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lesj;->b:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->b:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lesj;->f:Z

    .line 203
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 90
    const-class v0, Lhms;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Lesj;->d:Lhms;

    .line 91
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 137
    invoke-static {p1}, Lesj;->d(Landroid/content/Intent;)Lhmw;

    move-result-object v0

    .line 138
    const-string v1, "com.google.plus.analytics.intent.extra.EXTRA_START_VIEW_EXTRAS"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 139
    iget-object v2, p0, Lesj;->c:Lhmq;

    invoke-interface {v2}, Lhmq;->F_()Lhmw;

    move-result-object v2

    .line 140
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 141
    iget-object v4, p0, Lesj;->c:Lhmq;

    invoke-interface {v4, v3}, Lhmq;->b(Landroid/os/Bundle;)V

    .line 143
    iget-object v4, p0, Lesj;->d:Lhms;

    new-instance v5, Lhmr;

    iget-object v6, p0, Lesj;->b:Landroid/app/Activity;

    invoke-direct {v5, v6}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 144
    invoke-virtual {v5, v0}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v0

    .line 145
    invoke-virtual {v0, v2}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v0

    .line 146
    invoke-virtual {v0, v1}, Lhmr;->b(Landroid/os/Bundle;)Lhmr;

    move-result-object v0

    .line 147
    invoke-virtual {v0, v3}, Lhmr;->c(Landroid/os/Bundle;)Lhmr;

    move-result-object v0

    .line 143
    invoke-interface {v4, v0}, Lhms;->a(Lhmr;)V

    .line 149
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 95
    if-eqz p1, :cond_0

    .line 96
    const-string v0, "analytics:recorded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lesj;->e:Z

    .line 97
    const-string v0, "analytics:exited"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lesj;->f:Z

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lesj;->f:Z

    goto :goto_0
.end method

.method public b()V
    .locals 8

    .prologue
    .line 105
    iget-boolean v0, p0, Lesj;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lesj;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lesj;->d:Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lesj;->b:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->a:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lesj;->f:Z

    .line 110
    :cond_0
    iget-boolean v0, p0, Lesj;->e:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lesj;->e:Z

    iget-object v0, p0, Lesj;->c:Lhmq;

    invoke-interface {v0}, Lhmq;->F_()Lhmw;

    move-result-object v0

    iget-object v1, p0, Lesj;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lesj;->d(Landroid/content/Intent;)Lhmw;

    move-result-object v2

    const-string v3, "com.google.plus.analytics.intent.extra.EXTRA_START_VIEW_EXTRAS"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "com.google.plus.analytics.intent.extra.START_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v1, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iget-object v1, p0, Lesj;->d:Lhms;

    new-instance v6, Lhmr;

    iget-object v7, p0, Lesj;->b:Landroid/app/Activity;

    invoke-direct {v6, v7}, Lhmr;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v0}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhmr;->a(Ljava/lang/Long;)Lhmr;

    move-result-object v0

    invoke-virtual {v0, v3}, Lhmr;->b(Landroid/os/Bundle;)Lhmr;

    move-result-object v0

    invoke-interface {v1, v0}, Lhms;->a(Lhmr;)V

    .line 111
    :cond_1
    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lesj;->c(Landroid/content/Intent;)Landroid/content/Intent;

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lesj;->g:Z

    .line 155
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 127
    const-string v0, "analytics:recorded"

    iget-boolean v1, p0, Lesj;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 128
    const-string v0, "analytics:exited"

    iget-boolean v1, p0, Lesj;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 129
    return-void
.end method

.method public c()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 116
    iput-boolean v2, p0, Lesj;->g:Z

    .line 118
    iget-object v0, p0, Lesj;->b:Landroid/app/Activity;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iget-object v1, p0, Lesj;->b:Landroid/app/Activity;

    const-string v4, "power"

    invoke-virtual {v1, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lesj;->d:Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lesj;->b:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->b:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 121
    iput-boolean v3, p0, Lesj;->f:Z

    .line 123
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 118
    goto :goto_0
.end method

.method public d()V
    .locals 7

    .prologue
    .line 207
    iget-object v0, p0, Lesj;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-boolean v0, p0, Lesj;->g:Z

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lesj;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lesj;->d:Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lesj;->b:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->b:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    iget-object v0, p0, Lesj;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lesj;->c:Lhmq;

    invoke-interface {v1}, Lhmq;->F_()Lhmw;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iget-object v3, p0, Lesj;->c:Lhmq;

    invoke-interface {v3, v2}, Lhmq;->b(Landroid/os/Bundle;)V

    invoke-static {v0}, Lesj;->d(Landroid/content/Intent;)Lhmw;

    move-result-object v3

    const-string v4, "com.google.plus.analytics.intent.extra.EXTRA_START_VIEW_EXTRAS"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v3, :cond_0

    iget-object v4, p0, Lesj;->d:Lhms;

    new-instance v5, Lhmr;

    iget-object v6, p0, Lesj;->b:Landroid/app/Activity;

    invoke-direct {v5, v6}, Lhmr;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v1}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    invoke-virtual {v1, v3}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v1

    invoke-virtual {v1, v2}, Lhmr;->b(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhmr;->c(Landroid/os/Bundle;)Lhmr;

    move-result-object v0

    invoke-interface {v4, v0}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method
