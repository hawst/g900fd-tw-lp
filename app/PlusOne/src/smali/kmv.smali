.class public Lkmv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheo;
.implements Liwq;
.implements Lixl;


# instance fields
.field private final a:Lhei;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lkmv;->b:Landroid/content/Context;

    .line 48
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lkmv;->a:Lhei;

    .line 49
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 156
    iget-object v0, p0, Lkmv;->a:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 158
    const-string v1, "logged_in"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    const-string v1, "sharebox_settings_base64"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    if-nez v0, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-object v1, p0, Lkmv;->a:Lhei;

    invoke-interface {v1, p1}, Lhei;->b(I)Lhek;

    move-result-object v1

    const-string v2, "sharebox_settings_base64"

    invoke-interface {v1, v2}, Lhek;->e(Ljava/lang/String;)Lhek;

    .line 166
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 167
    new-instance v1, Loge;

    invoke-direct {v1}, Loge;-><init>()V

    .line 168
    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    .line 170
    invoke-virtual {p0, p1, v1}, Lkmv;->a(ILoge;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    const-string v1, "SharekitAccountSettings"

    const-string v2, "Invalid acl base64"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 173
    :catch_1
    move-exception v0

    .line 174
    const-string v1, "SharekitAccountSettings"

    const-string v2, "Invalid acl protobuf"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(ILoge;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 101
    iget-object v0, p2, Loge;->a:Locf;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lkmv;->b:Landroid/content/Context;

    iget-object v1, p2, Loge;->a:Locf;

    invoke-static {v0, p1, v1, v2, v2}, Lhft;->a(Landroid/content/Context;ILocf;ZZ)Lhgw;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    .line 110
    :try_start_0
    iget-object v1, p0, Lkmv;->b:Landroid/content/Context;

    .line 111
    invoke-static {v0}, Lhhg;->a(Lhgw;)[B

    move-result-object v2

    .line 110
    const-class v0, Lhzr;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    invoke-virtual {v0, v1, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    const/4 v3, 0x2

    invoke-direct {v1, v3}, Landroid/content/ContentValues;-><init>(I)V

    const-string v3, "audience_data"

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v2, "account_status"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 116
    const-string v1, "SharekitAccountSettings"

    const-string v2, "Error saving default audience"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(IZ)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lkmv;->a:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "restrict_posts_by_default"

    .line 61
    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 62
    invoke-interface {v0}, Lhek;->c()I

    .line 63
    return-void
.end method

.method public a(Landroid/content/Context;Lhem;)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public a(Lhem;Lmcb;)V
    .locals 2

    .prologue
    .line 145
    if-eqz p2, :cond_0

    iget-object v0, p2, Lmcb;->a:Lnog;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lmcb;->a:Lnog;

    iget-object v0, v0, Lnog;->c:Loge;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p2, Lmcb;->a:Lnog;

    iget-object v0, v0, Lnog;->c:Loge;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 149
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 150
    const-string v1, "sharebox_settings_base64"

    invoke-interface {p1, v1, v0}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    .line 152
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lheq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    return-void
.end method

.method public a(Lhej;Lmca;)Z
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    return v0
.end method

.method public b(I)Z
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lkmv;->a:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "restrict_posts_by_default"

    .line 53
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c(I)Z
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lkmv;->a:Lhei;

    .line 67
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "restricted_explanation"

    .line 68
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public d(I)V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lkmv;->a:Lhei;

    .line 73
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "restricted_explanation"

    const/4 v2, 0x1

    .line 74
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 75
    invoke-interface {v0}, Lhek;->c()I

    .line 76
    return-void
.end method

.method public e(I)Z
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lkmv;->a:Lhei;

    .line 81
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "public_explanation"

    .line 82
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public f(I)V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lkmv;->a:Lhei;

    .line 87
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "public_explanation"

    const/4 v2, 0x1

    .line 88
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 89
    invoke-interface {v0}, Lhek;->c()I

    .line 90
    return-void
.end method
