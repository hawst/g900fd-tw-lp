.class public final Llzb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnas;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26728
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26731
    iput-object v0, p0, Llzb;->apiHeader:Llyr;

    .line 26734
    iput-object v0, p0, Llzb;->a:Lnas;

    .line 26728
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26751
    const/4 v0, 0x0

    .line 26752
    iget-object v1, p0, Llzb;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 26753
    const/4 v0, 0x1

    iget-object v1, p0, Llzb;->apiHeader:Llyr;

    .line 26754
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26756
    :cond_0
    iget-object v1, p0, Llzb;->a:Lnas;

    if-eqz v1, :cond_1

    .line 26757
    const/4 v1, 0x2

    iget-object v2, p0, Llzb;->a:Lnas;

    .line 26758
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26760
    :cond_1
    iget-object v1, p0, Llzb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26761
    iput v0, p0, Llzb;->ai:I

    .line 26762
    return v0
.end method

.method public a(Loxn;)Llzb;
    .locals 2

    .prologue
    .line 26770
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26771
    sparse-switch v0, :sswitch_data_0

    .line 26775
    iget-object v1, p0, Llzb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26776
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzb;->ah:Ljava/util/List;

    .line 26779
    :cond_1
    iget-object v1, p0, Llzb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26781
    :sswitch_0
    return-object p0

    .line 26786
    :sswitch_1
    iget-object v0, p0, Llzb;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 26787
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzb;->apiHeader:Llyr;

    .line 26789
    :cond_2
    iget-object v0, p0, Llzb;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26793
    :sswitch_2
    iget-object v0, p0, Llzb;->a:Lnas;

    if-nez v0, :cond_3

    .line 26794
    new-instance v0, Lnas;

    invoke-direct {v0}, Lnas;-><init>()V

    iput-object v0, p0, Llzb;->a:Lnas;

    .line 26796
    :cond_3
    iget-object v0, p0, Llzb;->a:Lnas;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26771
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26739
    iget-object v0, p0, Llzb;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 26740
    const/4 v0, 0x1

    iget-object v1, p0, Llzb;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26742
    :cond_0
    iget-object v0, p0, Llzb;->a:Lnas;

    if-eqz v0, :cond_1

    .line 26743
    const/4 v0, 0x2

    iget-object v1, p0, Llzb;->a:Lnas;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26745
    :cond_1
    iget-object v0, p0, Llzb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26747
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26724
    invoke-virtual {p0, p1}, Llzb;->a(Loxn;)Llzb;

    move-result-object v0

    return-object v0
.end method
