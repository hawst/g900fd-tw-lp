.class public final Lmmn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmmo;

.field public b:[Lmmo;

.field private c:Lmmf;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 696
    invoke-direct {p0}, Loxq;-><init>()V

    .line 705
    iput-object v0, p0, Lmmn;->c:Lmmf;

    .line 716
    iput-object v0, p0, Lmmn;->a:Lmmo;

    .line 719
    sget-object v0, Lmmo;->a:[Lmmo;

    iput-object v0, p0, Lmmn;->b:[Lmmo;

    .line 722
    const/high16 v0, -0x80000000

    iput v0, p0, Lmmn;->h:I

    .line 696
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 761
    .line 762
    iget-object v0, p0, Lmmn;->c:Lmmf;

    if-eqz v0, :cond_8

    .line 763
    const/4 v0, 0x1

    iget-object v2, p0, Lmmn;->c:Lmmf;

    .line 764
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 766
    :goto_0
    iget-object v2, p0, Lmmn;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 767
    const/4 v2, 0x2

    iget-object v3, p0, Lmmn;->d:Ljava/lang/String;

    .line 768
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 770
    :cond_0
    iget-object v2, p0, Lmmn;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 771
    const/4 v2, 0x3

    iget-object v3, p0, Lmmn;->e:Ljava/lang/String;

    .line 772
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 774
    :cond_1
    iget-object v2, p0, Lmmn;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 775
    const/4 v2, 0x4

    iget-object v3, p0, Lmmn;->f:Ljava/lang/Boolean;

    .line 776
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 778
    :cond_2
    iget-object v2, p0, Lmmn;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 779
    const/4 v2, 0x7

    iget-object v3, p0, Lmmn;->g:Ljava/lang/Boolean;

    .line 780
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 782
    :cond_3
    iget-object v2, p0, Lmmn;->a:Lmmo;

    if-eqz v2, :cond_4

    .line 783
    const/16 v2, 0x8

    iget-object v3, p0, Lmmn;->a:Lmmo;

    .line 784
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 786
    :cond_4
    iget-object v2, p0, Lmmn;->b:[Lmmo;

    if-eqz v2, :cond_6

    .line 787
    iget-object v2, p0, Lmmn;->b:[Lmmo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 788
    if-eqz v4, :cond_5

    .line 789
    const/16 v5, 0x9

    .line 790
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 787
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 794
    :cond_6
    iget v1, p0, Lmmn;->h:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_7

    .line 795
    const/16 v1, 0xa

    iget v2, p0, Lmmn;->h:I

    .line 796
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 798
    :cond_7
    iget-object v1, p0, Lmmn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 799
    iput v0, p0, Lmmn;->ai:I

    .line 800
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmmn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 808
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 809
    sparse-switch v0, :sswitch_data_0

    .line 813
    iget-object v2, p0, Lmmn;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 814
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmmn;->ah:Ljava/util/List;

    .line 817
    :cond_1
    iget-object v2, p0, Lmmn;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 819
    :sswitch_0
    return-object p0

    .line 824
    :sswitch_1
    iget-object v0, p0, Lmmn;->c:Lmmf;

    if-nez v0, :cond_2

    .line 825
    new-instance v0, Lmmf;

    invoke-direct {v0}, Lmmf;-><init>()V

    iput-object v0, p0, Lmmn;->c:Lmmf;

    .line 827
    :cond_2
    iget-object v0, p0, Lmmn;->c:Lmmf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 831
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmmn;->d:Ljava/lang/String;

    goto :goto_0

    .line 835
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmmn;->e:Ljava/lang/String;

    goto :goto_0

    .line 839
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmmn;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 843
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmmn;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 847
    :sswitch_6
    iget-object v0, p0, Lmmn;->a:Lmmo;

    if-nez v0, :cond_3

    .line 848
    new-instance v0, Lmmo;

    invoke-direct {v0}, Lmmo;-><init>()V

    iput-object v0, p0, Lmmn;->a:Lmmo;

    .line 850
    :cond_3
    iget-object v0, p0, Lmmn;->a:Lmmo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 854
    :sswitch_7
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 855
    iget-object v0, p0, Lmmn;->b:[Lmmo;

    if-nez v0, :cond_5

    move v0, v1

    .line 856
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmmo;

    .line 857
    iget-object v3, p0, Lmmn;->b:[Lmmo;

    if-eqz v3, :cond_4

    .line 858
    iget-object v3, p0, Lmmn;->b:[Lmmo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 860
    :cond_4
    iput-object v2, p0, Lmmn;->b:[Lmmo;

    .line 861
    :goto_2
    iget-object v2, p0, Lmmn;->b:[Lmmo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 862
    iget-object v2, p0, Lmmn;->b:[Lmmo;

    new-instance v3, Lmmo;

    invoke-direct {v3}, Lmmo;-><init>()V

    aput-object v3, v2, v0

    .line 863
    iget-object v2, p0, Lmmn;->b:[Lmmo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 864
    invoke-virtual {p1}, Loxn;->a()I

    .line 861
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 855
    :cond_5
    iget-object v0, p0, Lmmn;->b:[Lmmo;

    array-length v0, v0

    goto :goto_1

    .line 867
    :cond_6
    iget-object v2, p0, Lmmn;->b:[Lmmo;

    new-instance v3, Lmmo;

    invoke-direct {v3}, Lmmo;-><init>()V

    aput-object v3, v2, v0

    .line 868
    iget-object v2, p0, Lmmn;->b:[Lmmo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 872
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 873
    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-ne v0, v2, :cond_8

    .line 876
    :cond_7
    iput v0, p0, Lmmn;->h:I

    goto/16 :goto_0

    .line 878
    :cond_8
    iput v1, p0, Lmmn;->h:I

    goto/16 :goto_0

    .line 809
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x38 -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 727
    iget-object v0, p0, Lmmn;->c:Lmmf;

    if-eqz v0, :cond_0

    .line 728
    const/4 v0, 0x1

    iget-object v1, p0, Lmmn;->c:Lmmf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 730
    :cond_0
    iget-object v0, p0, Lmmn;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 731
    const/4 v0, 0x2

    iget-object v1, p0, Lmmn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 733
    :cond_1
    iget-object v0, p0, Lmmn;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 734
    const/4 v0, 0x3

    iget-object v1, p0, Lmmn;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 736
    :cond_2
    iget-object v0, p0, Lmmn;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 737
    const/4 v0, 0x4

    iget-object v1, p0, Lmmn;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 739
    :cond_3
    iget-object v0, p0, Lmmn;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 740
    const/4 v0, 0x7

    iget-object v1, p0, Lmmn;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 742
    :cond_4
    iget-object v0, p0, Lmmn;->a:Lmmo;

    if-eqz v0, :cond_5

    .line 743
    const/16 v0, 0x8

    iget-object v1, p0, Lmmn;->a:Lmmo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 745
    :cond_5
    iget-object v0, p0, Lmmn;->b:[Lmmo;

    if-eqz v0, :cond_7

    .line 746
    iget-object v1, p0, Lmmn;->b:[Lmmo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 747
    if-eqz v3, :cond_6

    .line 748
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 746
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 752
    :cond_7
    iget v0, p0, Lmmn;->h:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_8

    .line 753
    const/16 v0, 0xa

    iget v1, p0, Lmmn;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 755
    :cond_8
    iget-object v0, p0, Lmmn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 757
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 692
    invoke-virtual {p0, p1}, Lmmn;->a(Loxn;)Lmmn;

    move-result-object v0

    return-object v0
.end method
