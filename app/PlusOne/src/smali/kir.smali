.class public final Lkir;
.super Llol;
.source "PG"

# interfaces
.implements Lkhf;


# instance fields
.field private N:Lkhr;

.field private O:Ljava/lang/String;

.field private P:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Llol;-><init>()V

    .line 21
    new-instance v0, Lkhe;

    iget-object v1, p0, Lkir;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    .line 33
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 58
    new-instance v0, Lkhr;

    iget-object v1, p0, Lkir;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkir;->N:Lkhr;

    .line 60
    iget-object v0, p0, Lkir;->N:Lkhr;

    const v1, 0x7f0a0563

    .line 61
    invoke-virtual {v0, v1}, Lkhr;->a(I)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lkir;->N:Lkhr;

    iget-object v2, p0, Lkir;->O:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lkir;->P:Landroid/content/Intent;

    .line 64
    invoke-virtual {v1, v2, v3, v4}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 65
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 43
    if-eqz p1, :cond_0

    .line 44
    const-string v0, "go_to_about_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkir;->O:Ljava/lang/String;

    .line 45
    const-string v0, "go_to_about_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lkir;->P:Landroid/content/Intent;

    .line 47
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lkir;->O:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lkir;->P:Landroid/content/Intent;

    .line 38
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 52
    const-string v0, "go_to_about_text"

    iget-object v1, p0, Lkir;->O:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v0, "go_to_about_intent"

    iget-object v1, p0, Lkir;->P:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 54
    return-void
.end method
