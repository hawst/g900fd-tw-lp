.class public final Lfcw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lilo;


# instance fields
.field final a:Landroid/content/Context;

.field final b:I

.field final c:Lilo;

.field final d:Landroid/os/Handler;

.field final e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILilo;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lfcw;->a:Landroid/content/Context;

    .line 34
    iput p2, p0, Lfcw;->b:I

    .line 35
    iput-object p3, p0, Lfcw;->c:Lilo;

    .line 36
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lfcw;->d:Landroid/os/Handler;

    .line 37
    sget-object v0, Lfvc;->b:Lfvc;

    invoke-virtual {v0}, Lfvc;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TRUE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfcw;->e:Z

    .line 38
    return-void
.end method

.method public static b(Landroid/location/Location;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    :goto_0
    const-class v1, Llae;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 113
    const-string v1, "location_description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 111
    :cond_0
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lfcx;

    invoke-direct {v0, p0, p1}, Lfcx;-><init>(Lfcw;Landroid/location/Location;)V

    invoke-virtual {v0}, Lfcx;->start()V

    .line 43
    return-void
.end method
