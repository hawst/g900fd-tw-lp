.class public final Lmct;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlp;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 646
    invoke-direct {p0}, Loxq;-><init>()V

    .line 649
    iput-object v0, p0, Lmct;->apiHeader:Llyr;

    .line 652
    iput-object v0, p0, Lmct;->a:Lnlp;

    .line 646
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 669
    const/4 v0, 0x0

    .line 670
    iget-object v1, p0, Lmct;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 671
    const/4 v0, 0x1

    iget-object v1, p0, Lmct;->apiHeader:Llyr;

    .line 672
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 674
    :cond_0
    iget-object v1, p0, Lmct;->a:Lnlp;

    if-eqz v1, :cond_1

    .line 675
    const/4 v1, 0x2

    iget-object v2, p0, Lmct;->a:Lnlp;

    .line 676
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 678
    :cond_1
    iget-object v1, p0, Lmct;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 679
    iput v0, p0, Lmct;->ai:I

    .line 680
    return v0
.end method

.method public a(Loxn;)Lmct;
    .locals 2

    .prologue
    .line 688
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 689
    sparse-switch v0, :sswitch_data_0

    .line 693
    iget-object v1, p0, Lmct;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 694
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmct;->ah:Ljava/util/List;

    .line 697
    :cond_1
    iget-object v1, p0, Lmct;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 699
    :sswitch_0
    return-object p0

    .line 704
    :sswitch_1
    iget-object v0, p0, Lmct;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 705
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmct;->apiHeader:Llyr;

    .line 707
    :cond_2
    iget-object v0, p0, Lmct;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 711
    :sswitch_2
    iget-object v0, p0, Lmct;->a:Lnlp;

    if-nez v0, :cond_3

    .line 712
    new-instance v0, Lnlp;

    invoke-direct {v0}, Lnlp;-><init>()V

    iput-object v0, p0, Lmct;->a:Lnlp;

    .line 714
    :cond_3
    iget-object v0, p0, Lmct;->a:Lnlp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 689
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lmct;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 658
    const/4 v0, 0x1

    iget-object v1, p0, Lmct;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 660
    :cond_0
    iget-object v0, p0, Lmct;->a:Lnlp;

    if-eqz v0, :cond_1

    .line 661
    const/4 v0, 0x2

    iget-object v1, p0, Lmct;->a:Lnlp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 663
    :cond_1
    iget-object v0, p0, Lmct;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 665
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 642
    invoke-virtual {p0, p1}, Lmct;->a(Loxn;)Lmct;

    move-result-object v0

    return-object v0
.end method
