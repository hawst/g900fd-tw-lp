.class public final Lfno;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmhi;",
        "Lmhj;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lkzl;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final p:Loya;

.field private final q:Lhgw;

.field private final r:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILkfo;Ljava/lang/String;Loya;Lhgw;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 39
    const-string v3, "postactivity"

    new-instance v4, Lmhi;

    invoke-direct {v4}, Lmhi;-><init>()V

    new-instance v5, Lmhj;

    invoke-direct {v5}, Lmhj;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 41
    iget-object v0, p0, Lfno;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Lfno;->a:Lkzl;

    .line 42
    iput-object p4, p0, Lfno;->b:Ljava/lang/String;

    .line 43
    iput-object p7, p0, Lfno;->c:Ljava/lang/String;

    .line 44
    iput-object p5, p0, Lfno;->p:Loya;

    .line 45
    iput-object p6, p0, Lfno;->q:Lhgw;

    .line 46
    iput p2, p0, Lfno;->r:I

    .line 47
    return-void
.end method


# virtual methods
.method protected a(Lmhi;)V
    .locals 6

    .prologue
    .line 51
    new-instance v0, Lodg;

    invoke-direct {v0}, Lodg;-><init>()V

    iput-object v0, p1, Lmhi;->a:Lodg;

    .line 52
    iget-object v1, p1, Lmhi;->a:Lodg;

    .line 53
    new-instance v0, Lodh;

    invoke-direct {v0}, Lodh;-><init>()V

    iput-object v0, v1, Lodg;->p:Lodh;

    .line 54
    iget-object v0, v1, Lodg;->p:Lodh;

    const-string v2, "Mobile"

    iput-object v2, v0, Lodh;->a:Ljava/lang/String;

    .line 56
    new-instance v0, Lpee;

    invoke-direct {v0}, Lpee;-><init>()V

    iput-object v0, v1, Lodg;->b:Lpee;

    .line 57
    iget-object v0, v1, Lodg;->b:Lpee;

    new-instance v2, Lpef;

    invoke-direct {v2}, Lpef;-><init>()V

    iput-object v2, v0, Lpee;->a:Lpef;

    .line 58
    iget-object v0, v1, Lodg;->b:Lpee;

    iget-object v0, v0, Lpee;->a:Lpef;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lpef;->a:Ljava/lang/Boolean;

    .line 60
    iget-object v0, p0, Lfno;->c:Ljava/lang/String;

    iput-object v0, v1, Lodg;->c:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lfno;->q:Lhgw;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lhft;->a(Lhgw;Lhgw;)Lock;

    move-result-object v0

    iput-object v0, v1, Lodg;->j:Lock;

    .line 63
    iget-object v0, p0, Lfno;->q:Lhgw;

    invoke-virtual {v0}, Lhgw;->i()I

    move-result v0

    if-lez v0, :cond_1

    .line 64
    iget-object v0, p0, Lfno;->q:Lhgw;

    invoke-virtual {v0}, Lhgw;->c()[Lkxr;

    move-result-object v2

    .line 65
    array-length v0, v2

    new-array v3, v0, [Lodl;

    .line 66
    const/4 v0, 0x0

    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 67
    new-instance v4, Lodl;

    invoke-direct {v4}, Lodl;-><init>()V

    .line 68
    aget-object v5, v2, v0

    invoke-virtual {v5}, Lkxr;->a()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lodl;->b:Ljava/lang/String;

    .line 69
    aget-object v5, v2, v0

    invoke-virtual {v5}, Lkxr;->c()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lodl;->c:Ljava/lang/String;

    .line 70
    aput-object v4, v3, v0

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_0
    iput-object v3, v1, Lodg;->m:[Lodl;

    .line 75
    :cond_1
    iget-object v0, p0, Lfno;->b:Ljava/lang/String;

    iput-object v0, v1, Lodg;->a:Ljava/lang/String;

    .line 76
    iget-object v0, p0, Lfno;->p:Loya;

    iput-object v0, v1, Lodg;->q:Loya;

    .line 78
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, v1, Lodg;->t:Loxz;

    .line 79
    iget-object v0, v1, Lodg;->t:Loxz;

    iget-object v1, p0, Lfno;->a:Lkzl;

    iget-object v2, p0, Lfno;->f:Landroid/content/Context;

    iget v3, p0, Lfno;->r:I

    invoke-interface {v1, v2, v3}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v1

    iput-object v1, v0, Loxz;->a:[I

    .line 80
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lmhi;

    invoke-virtual {p0, p1}, Lfno;->a(Lmhi;)V

    return-void
.end method
