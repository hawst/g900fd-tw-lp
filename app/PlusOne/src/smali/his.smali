.class public final Lhis;
.super Landroid/widget/ImageView;
.source "PG"

# interfaces
.implements Lhzn;


# static fields
.field private static a:Landroid/graphics/Bitmap;

.field private static b:Z


# instance fields
.field private final c:Lhzl;

.field private d:Landroid/view/animation/Animation;

.field private e:Landroid/view/animation/Animation;

.field private f:Landroid/os/Parcelable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    sput-boolean v0, Lhis;->b:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 70
    invoke-virtual {p0}, Lhis;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhzl;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Lhis;->c:Lhzl;

    .line 71
    iget-object v0, p0, Lhis;->c:Lhzl;

    instance-of v0, v0, Lhzp;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lhis;->c:Lhzl;

    check-cast v0, Lhzp;

    invoke-interface {v0, p0}, Lhzp;->a(Lhzn;)V

    .line 75
    :cond_0
    const-string v0, "item_check_view_tag"

    invoke-virtual {p0, v0}, Lhis;->setTag(Ljava/lang/Object;)V

    .line 76
    const v0, 0x7f0204d0

    invoke-virtual {p0, v0}, Lhis;->setImageResource(I)V

    .line 77
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhis;->setVisibility(I)V

    .line 78
    invoke-virtual {p0}, Lhis;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f050008

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lhis;->d:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lhis;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f050007

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lhis;->e:Landroid/view/animation/Animation;

    .line 59
    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    sput-boolean v0, Lhis;->b:Z

    .line 48
    return-void
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    sput-boolean v0, Lhis;->b:Z

    .line 55
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Parcelable;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 110
    invoke-virtual {p0}, Lhis;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 111
    :cond_0
    iget-object v0, p0, Lhis;->f:Landroid/os/Parcelable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhis;->f:Landroid/os/Parcelable;

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    sget-object v0, Lhit;->a:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 120
    invoke-virtual {p0}, Lhis;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 121
    sget-boolean v0, Lhis;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhis;->e:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhis;->e:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lhis;->startAnimation(Landroid/view/animation/Animation;)V

    .line 122
    :cond_1
    invoke-virtual {p0, v2}, Lhis;->setVisibility(I)V

    .line 127
    :cond_2
    :goto_0
    return-void

    .line 114
    :pswitch_0
    invoke-virtual {p0}, Lhis;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhis;->setVisibility(I)V

    .line 116
    sget-boolean v0, Lhis;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhis;->d:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhis;->d:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lhis;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(ILjava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 140
    iput-object p1, p0, Lhis;->f:Landroid/os/Parcelable;

    .line 141
    iget-object v0, p0, Lhis;->c:Lhzl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhis;->c:Lhzl;

    iget-object v1, p0, Lhis;->f:Landroid/os/Parcelable;

    invoke-interface {v0, v1}, Lhzl;->c(Landroid/os/Parcelable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhis;->setVisibility(I)V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhis;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 83
    sget-object v0, Lhis;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 84
    invoke-virtual {p0}, Lhis;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lhis;->getHeight()I

    move-result v0

    if-gtz v0, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    invoke-virtual {p0}, Lhis;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lhis;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhis;->a:Landroid/graphics/Bitmap;

    .line 88
    new-instance v1, Landroid/graphics/Canvas;

    sget-object v0, Lhis;->a:Landroid/graphics/Bitmap;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 91
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 92
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 93
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    invoke-virtual {p0}, Lhis;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 95
    int-to-float v3, v2

    int-to-float v4, v2

    int-to-float v2, v2

    invoke-virtual {v1, v3, v4, v2, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 98
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 99
    invoke-virtual {p0}, Lhis;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0130

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 100
    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v0, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 101
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 102
    invoke-virtual {p0}, Lhis;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 105
    :cond_2
    sget-object v0, Lhis;->a:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v5, v5, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method
