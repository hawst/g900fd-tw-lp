.class public final Lnfn;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnzx;

.field public b:Ljava/lang/String;

.field private c:[Loud;

.field private d:Ljava/lang/Boolean;

.field private e:[Loqn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1874
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1877
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lnfn;->a:[Lnzx;

    .line 1880
    sget-object v0, Loud;->a:[Loud;

    iput-object v0, p0, Lnfn;->c:[Loud;

    .line 1887
    sget-object v0, Loqn;->a:[Loqn;

    iput-object v0, p0, Lnfn;->e:[Loqn;

    .line 1874
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1925
    .line 1926
    iget-object v0, p0, Lnfn;->a:[Lnzx;

    if-eqz v0, :cond_1

    .line 1927
    iget-object v3, p0, Lnfn;->a:[Lnzx;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1928
    if-eqz v5, :cond_0

    .line 1929
    const/4 v6, 0x1

    .line 1930
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1927
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1934
    :cond_2
    iget-object v2, p0, Lnfn;->b:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 1935
    const/4 v2, 0x2

    iget-object v3, p0, Lnfn;->b:Ljava/lang/String;

    .line 1936
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1938
    :cond_3
    iget-object v2, p0, Lnfn;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 1939
    const/4 v2, 0x3

    iget-object v3, p0, Lnfn;->d:Ljava/lang/Boolean;

    .line 1940
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1942
    :cond_4
    iget-object v2, p0, Lnfn;->c:[Loud;

    if-eqz v2, :cond_6

    .line 1943
    iget-object v3, p0, Lnfn;->c:[Loud;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 1944
    if-eqz v5, :cond_5

    .line 1945
    const/4 v6, 0x4

    .line 1946
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1943
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1950
    :cond_6
    iget-object v2, p0, Lnfn;->e:[Loqn;

    if-eqz v2, :cond_8

    .line 1951
    iget-object v2, p0, Lnfn;->e:[Loqn;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 1952
    if-eqz v4, :cond_7

    .line 1953
    const/4 v5, 0x5

    .line 1954
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1951
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1958
    :cond_8
    iget-object v1, p0, Lnfn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1959
    iput v0, p0, Lnfn;->ai:I

    .line 1960
    return v0
.end method

.method public a(Loxn;)Lnfn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1968
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1969
    sparse-switch v0, :sswitch_data_0

    .line 1973
    iget-object v2, p0, Lnfn;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1974
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnfn;->ah:Ljava/util/List;

    .line 1977
    :cond_1
    iget-object v2, p0, Lnfn;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1979
    :sswitch_0
    return-object p0

    .line 1984
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1985
    iget-object v0, p0, Lnfn;->a:[Lnzx;

    if-nez v0, :cond_3

    move v0, v1

    .line 1986
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 1987
    iget-object v3, p0, Lnfn;->a:[Lnzx;

    if-eqz v3, :cond_2

    .line 1988
    iget-object v3, p0, Lnfn;->a:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1990
    :cond_2
    iput-object v2, p0, Lnfn;->a:[Lnzx;

    .line 1991
    :goto_2
    iget-object v2, p0, Lnfn;->a:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1992
    iget-object v2, p0, Lnfn;->a:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 1993
    iget-object v2, p0, Lnfn;->a:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1994
    invoke-virtual {p1}, Loxn;->a()I

    .line 1991
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1985
    :cond_3
    iget-object v0, p0, Lnfn;->a:[Lnzx;

    array-length v0, v0

    goto :goto_1

    .line 1997
    :cond_4
    iget-object v2, p0, Lnfn;->a:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 1998
    iget-object v2, p0, Lnfn;->a:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2002
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfn;->b:Ljava/lang/String;

    goto :goto_0

    .line 2006
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnfn;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 2010
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2011
    iget-object v0, p0, Lnfn;->c:[Loud;

    if-nez v0, :cond_6

    move v0, v1

    .line 2012
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loud;

    .line 2013
    iget-object v3, p0, Lnfn;->c:[Loud;

    if-eqz v3, :cond_5

    .line 2014
    iget-object v3, p0, Lnfn;->c:[Loud;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2016
    :cond_5
    iput-object v2, p0, Lnfn;->c:[Loud;

    .line 2017
    :goto_4
    iget-object v2, p0, Lnfn;->c:[Loud;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 2018
    iget-object v2, p0, Lnfn;->c:[Loud;

    new-instance v3, Loud;

    invoke-direct {v3}, Loud;-><init>()V

    aput-object v3, v2, v0

    .line 2019
    iget-object v2, p0, Lnfn;->c:[Loud;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2020
    invoke-virtual {p1}, Loxn;->a()I

    .line 2017
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2011
    :cond_6
    iget-object v0, p0, Lnfn;->c:[Loud;

    array-length v0, v0

    goto :goto_3

    .line 2023
    :cond_7
    iget-object v2, p0, Lnfn;->c:[Loud;

    new-instance v3, Loud;

    invoke-direct {v3}, Loud;-><init>()V

    aput-object v3, v2, v0

    .line 2024
    iget-object v2, p0, Lnfn;->c:[Loud;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2028
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2029
    iget-object v0, p0, Lnfn;->e:[Loqn;

    if-nez v0, :cond_9

    move v0, v1

    .line 2030
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loqn;

    .line 2031
    iget-object v3, p0, Lnfn;->e:[Loqn;

    if-eqz v3, :cond_8

    .line 2032
    iget-object v3, p0, Lnfn;->e:[Loqn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2034
    :cond_8
    iput-object v2, p0, Lnfn;->e:[Loqn;

    .line 2035
    :goto_6
    iget-object v2, p0, Lnfn;->e:[Loqn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 2036
    iget-object v2, p0, Lnfn;->e:[Loqn;

    new-instance v3, Loqn;

    invoke-direct {v3}, Loqn;-><init>()V

    aput-object v3, v2, v0

    .line 2037
    iget-object v2, p0, Lnfn;->e:[Loqn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2038
    invoke-virtual {p1}, Loxn;->a()I

    .line 2035
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 2029
    :cond_9
    iget-object v0, p0, Lnfn;->e:[Loqn;

    array-length v0, v0

    goto :goto_5

    .line 2041
    :cond_a
    iget-object v2, p0, Lnfn;->e:[Loqn;

    new-instance v3, Loqn;

    invoke-direct {v3}, Loqn;-><init>()V

    aput-object v3, v2, v0

    .line 2042
    iget-object v2, p0, Lnfn;->e:[Loqn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1969
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1892
    iget-object v1, p0, Lnfn;->a:[Lnzx;

    if-eqz v1, :cond_1

    .line 1893
    iget-object v2, p0, Lnfn;->a:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1894
    if-eqz v4, :cond_0

    .line 1895
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1893
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1899
    :cond_1
    iget-object v1, p0, Lnfn;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1900
    const/4 v1, 0x2

    iget-object v2, p0, Lnfn;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1902
    :cond_2
    iget-object v1, p0, Lnfn;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1903
    const/4 v1, 0x3

    iget-object v2, p0, Lnfn;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1905
    :cond_3
    iget-object v1, p0, Lnfn;->c:[Loud;

    if-eqz v1, :cond_5

    .line 1906
    iget-object v2, p0, Lnfn;->c:[Loud;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 1907
    if-eqz v4, :cond_4

    .line 1908
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1906
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1912
    :cond_5
    iget-object v1, p0, Lnfn;->e:[Loqn;

    if-eqz v1, :cond_7

    .line 1913
    iget-object v1, p0, Lnfn;->e:[Loqn;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 1914
    if-eqz v3, :cond_6

    .line 1915
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1913
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1919
    :cond_7
    iget-object v0, p0, Lnfn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1921
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1870
    invoke-virtual {p0, p1}, Lnfn;->a(Loxn;)Lnfn;

    move-result-object v0

    return-object v0
.end method
