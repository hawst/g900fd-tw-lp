.class public final Lmuq;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lmuo;

.field private b:[Lmuo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14486
    invoke-direct {p0}, Loxq;-><init>()V

    .line 14489
    sget-object v0, Lmuo;->a:[Lmuo;

    iput-object v0, p0, Lmuq;->a:[Lmuo;

    .line 14492
    sget-object v0, Lmuo;->a:[Lmuo;

    iput-object v0, p0, Lmuq;->b:[Lmuo;

    .line 14486
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 14517
    .line 14518
    iget-object v0, p0, Lmuq;->a:[Lmuo;

    if-eqz v0, :cond_1

    .line 14519
    iget-object v3, p0, Lmuq;->a:[Lmuo;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 14520
    if-eqz v5, :cond_0

    .line 14521
    const/4 v6, 0x1

    .line 14522
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 14519
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 14526
    :cond_2
    iget-object v2, p0, Lmuq;->b:[Lmuo;

    if-eqz v2, :cond_4

    .line 14527
    iget-object v2, p0, Lmuq;->b:[Lmuo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 14528
    if-eqz v4, :cond_3

    .line 14529
    const/4 v5, 0x2

    .line 14530
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 14527
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 14534
    :cond_4
    iget-object v1, p0, Lmuq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14535
    iput v0, p0, Lmuq;->ai:I

    .line 14536
    return v0
.end method

.method public a(Loxn;)Lmuq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 14544
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 14545
    sparse-switch v0, :sswitch_data_0

    .line 14549
    iget-object v2, p0, Lmuq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 14550
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmuq;->ah:Ljava/util/List;

    .line 14553
    :cond_1
    iget-object v2, p0, Lmuq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14555
    :sswitch_0
    return-object p0

    .line 14560
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 14561
    iget-object v0, p0, Lmuq;->a:[Lmuo;

    if-nez v0, :cond_3

    move v0, v1

    .line 14562
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmuo;

    .line 14563
    iget-object v3, p0, Lmuq;->a:[Lmuo;

    if-eqz v3, :cond_2

    .line 14564
    iget-object v3, p0, Lmuq;->a:[Lmuo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 14566
    :cond_2
    iput-object v2, p0, Lmuq;->a:[Lmuo;

    .line 14567
    :goto_2
    iget-object v2, p0, Lmuq;->a:[Lmuo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 14568
    iget-object v2, p0, Lmuq;->a:[Lmuo;

    new-instance v3, Lmuo;

    invoke-direct {v3}, Lmuo;-><init>()V

    aput-object v3, v2, v0

    .line 14569
    iget-object v2, p0, Lmuq;->a:[Lmuo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 14570
    invoke-virtual {p1}, Loxn;->a()I

    .line 14567
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 14561
    :cond_3
    iget-object v0, p0, Lmuq;->a:[Lmuo;

    array-length v0, v0

    goto :goto_1

    .line 14573
    :cond_4
    iget-object v2, p0, Lmuq;->a:[Lmuo;

    new-instance v3, Lmuo;

    invoke-direct {v3}, Lmuo;-><init>()V

    aput-object v3, v2, v0

    .line 14574
    iget-object v2, p0, Lmuq;->a:[Lmuo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 14578
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 14579
    iget-object v0, p0, Lmuq;->b:[Lmuo;

    if-nez v0, :cond_6

    move v0, v1

    .line 14580
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmuo;

    .line 14581
    iget-object v3, p0, Lmuq;->b:[Lmuo;

    if-eqz v3, :cond_5

    .line 14582
    iget-object v3, p0, Lmuq;->b:[Lmuo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 14584
    :cond_5
    iput-object v2, p0, Lmuq;->b:[Lmuo;

    .line 14585
    :goto_4
    iget-object v2, p0, Lmuq;->b:[Lmuo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 14586
    iget-object v2, p0, Lmuq;->b:[Lmuo;

    new-instance v3, Lmuo;

    invoke-direct {v3}, Lmuo;-><init>()V

    aput-object v3, v2, v0

    .line 14587
    iget-object v2, p0, Lmuq;->b:[Lmuo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 14588
    invoke-virtual {p1}, Loxn;->a()I

    .line 14585
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 14579
    :cond_6
    iget-object v0, p0, Lmuq;->b:[Lmuo;

    array-length v0, v0

    goto :goto_3

    .line 14591
    :cond_7
    iget-object v2, p0, Lmuq;->b:[Lmuo;

    new-instance v3, Lmuo;

    invoke-direct {v3}, Lmuo;-><init>()V

    aput-object v3, v2, v0

    .line 14592
    iget-object v2, p0, Lmuq;->b:[Lmuo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 14545
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 14497
    iget-object v1, p0, Lmuq;->a:[Lmuo;

    if-eqz v1, :cond_1

    .line 14498
    iget-object v2, p0, Lmuq;->a:[Lmuo;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 14499
    if-eqz v4, :cond_0

    .line 14500
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 14498
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 14504
    :cond_1
    iget-object v1, p0, Lmuq;->b:[Lmuo;

    if-eqz v1, :cond_3

    .line 14505
    iget-object v1, p0, Lmuq;->b:[Lmuo;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 14506
    if-eqz v3, :cond_2

    .line 14507
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 14505
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 14511
    :cond_3
    iget-object v0, p0, Lmuq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 14513
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 14482
    invoke-virtual {p0, p1}, Lmuq;->a(Loxn;)Lmuq;

    move-result-object v0

    return-object v0
.end method
