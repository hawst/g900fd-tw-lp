.class final Lvt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lwx;
.implements Lxk;


# instance fields
.field final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lvp;",
            ">;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lvx;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lwz;

.field final d:Lxa;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lvy;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lvw;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lvv;

.field private final i:Lvu;

.field private final j:Z

.field private k:Lwu;

.field private l:Lvy;

.field private m:Lvy;

.field private n:Lvj;

.field private o:Lve;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1503
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lvt;->a:Ljava/util/ArrayList;

    .line 1505
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    .line 1506
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lvt;->b:Ljava/util/ArrayList;

    .line 1508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lvt;->g:Ljava/util/ArrayList;

    .line 1510
    new-instance v0, Lwz;

    invoke-direct {v0}, Lwz;-><init>()V

    iput-object v0, p0, Lvt;->c:Lwz;

    .line 1512
    new-instance v0, Lvv;

    invoke-direct {v0, p0}, Lvv;-><init>(Lvt;)V

    iput-object v0, p0, Lvt;->h:Lvv;

    .line 1513
    new-instance v0, Lvu;

    invoke-direct {v0, p0}, Lvu;-><init>(Lvt;)V

    iput-object v0, p0, Lvt;->i:Lvu;

    .line 1526
    iput-object p1, p0, Lvt;->e:Landroid/content/Context;

    .line 1527
    invoke-static {p1}, Lel;->a(Landroid/content/Context;)Lel;

    .line 1528
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lvt;->j:Z

    .line 1535
    invoke-static {p1, p0}, Lxa;->a(Landroid/content/Context;Lxk;)Lxa;

    move-result-object v0

    iput-object v0, p0, Lvt;->d:Lxa;

    .line 1536
    iget-object v0, p0, Lvt;->d:Lxa;

    invoke-virtual {p0, v0}, Lvt;->a(Lvf;)V

    .line 1537
    return-void

    .line 1528
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1936
    iget-object v0, p0, Lvt;->l:Lvy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lvt;->l:Lvy;

    invoke-direct {p0, v0}, Lvt;->b(Lvy;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1937
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Clearing the default route because it is no longer selectable: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lvt;->l:Lvy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1939
    iput-object v4, p0, Lvt;->l:Lvy;

    .line 1941
    :cond_0
    iget-object v0, p0, Lvt;->l:Lvy;

    if-nez v0, :cond_2

    iget-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1942
    iget-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvy;

    .line 1943
    invoke-virtual {v0}, Lvy;->p()Lvf;

    move-result-object v1

    iget-object v3, p0, Lvt;->d:Lxa;

    if-ne v1, v3, :cond_5

    iget-object v1, v0, Lvy;->a:Ljava/lang/String;

    const-string v3, "DEFAULT_ROUTE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lvt;->b(Lvy;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1944
    iput-object v0, p0, Lvt;->l:Lvy;

    .line 1945
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Found default route: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lvt;->l:Lvy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1952
    :cond_2
    iget-object v0, p0, Lvt;->m:Lvy;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lvt;->m:Lvy;

    invoke-direct {p0, v0}, Lvt;->b(Lvy;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1953
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unselecting the current route because it is no longer selectable: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lvt;->m:Lvy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1955
    invoke-direct {p0, v4}, Lvt;->c(Lvy;)V

    .line 1957
    :cond_3
    iget-object v0, p0, Lvt;->m:Lvy;

    if-nez v0, :cond_6

    .line 1961
    invoke-direct {p0}, Lvt;->f()Lvy;

    move-result-object v0

    invoke-direct {p0, v0}, Lvt;->c(Lvy;)V

    .line 1966
    :cond_4
    :goto_1
    return-void

    .line 1943
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 1962
    :cond_6
    if-eqz p1, :cond_4

    .line 1964
    invoke-direct {p0}, Lvt;->g()V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1925
    iget-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1926
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1927
    iget-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvy;

    iget-object v0, v0, Lvy;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1931
    :goto_1
    return v0

    .line 1926
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1931
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private b(Lvy;)Z
    .locals 1

    .prologue
    .line 1992
    iget-object v0, p1, Lvy;->d:Lvc;

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lvy;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lvy;)V
    .locals 3

    .prologue
    .line 2002
    iget-object v0, p0, Lvt;->m:Lvy;

    if-eq v0, p1, :cond_5

    .line 2003
    iget-object v0, p0, Lvt;->m:Lvy;

    if-eqz v0, :cond_1

    .line 2004
    invoke-static {}, Lvp;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route unselected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lvt;->m:Lvy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2007
    :cond_0
    iget-object v0, p0, Lvt;->i:Lvu;

    const/16 v1, 0x107

    iget-object v2, p0, Lvt;->m:Lvy;

    invoke-virtual {v0, v1, v2}, Lvu;->a(ILjava/lang/Object;)V

    .line 2008
    iget-object v0, p0, Lvt;->n:Lvj;

    if-eqz v0, :cond_1

    .line 2009
    iget-object v0, p0, Lvt;->n:Lvj;

    invoke-virtual {v0}, Lvj;->c()V

    .line 2010
    iget-object v0, p0, Lvt;->n:Lvj;

    invoke-virtual {v0}, Lvj;->a()V

    .line 2011
    const/4 v0, 0x0

    iput-object v0, p0, Lvt;->n:Lvj;

    .line 2015
    :cond_1
    iput-object p1, p0, Lvt;->m:Lvy;

    .line 2017
    iget-object v0, p0, Lvt;->m:Lvy;

    if-eqz v0, :cond_4

    .line 2018
    invoke-virtual {p1}, Lvy;->p()Lvf;

    move-result-object v0

    iget-object v1, p1, Lvy;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lvf;->a(Ljava/lang/String;)Lvj;

    move-result-object v0

    iput-object v0, p0, Lvt;->n:Lvj;

    .line 2020
    iget-object v0, p0, Lvt;->n:Lvj;

    if-eqz v0, :cond_2

    .line 2021
    iget-object v0, p0, Lvt;->n:Lvj;

    invoke-virtual {v0}, Lvj;->b()V

    .line 2023
    :cond_2
    invoke-static {}, Lvp;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2024
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route selected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lvt;->m:Lvy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2026
    :cond_3
    iget-object v0, p0, Lvt;->i:Lvu;

    const/16 v1, 0x106

    iget-object v2, p0, Lvt;->m:Lvy;

    invoke-virtual {v0, v1, v2}, Lvu;->a(ILjava/lang/Object;)V

    .line 2029
    :cond_4
    invoke-direct {p0}, Lvt;->g()V

    .line 2031
    :cond_5
    return-void
.end method

.method private f()Lvy;
    .locals 4

    .prologue
    .line 1973
    iget-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvy;

    .line 1974
    iget-object v1, p0, Lvt;->l:Lvy;

    if-eq v0, v1, :cond_0

    invoke-virtual {v0}, Lvy;->p()Lvf;

    move-result-object v1

    iget-object v3, p0, Lvt;->d:Lxa;

    if-ne v1, v3, :cond_1

    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Lvy;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Lvy;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lvt;->b(Lvy;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1980
    :goto_1
    return-object v0

    .line 1974
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1980
    :cond_2
    iget-object v0, p0, Lvt;->l:Lvy;

    goto :goto_1
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2086
    iget-object v0, p0, Lvt;->m:Lvy;

    if-eqz v0, :cond_0

    .line 2087
    iget-object v0, p0, Lvt;->c:Lwz;

    iget-object v0, p0, Lvt;->m:Lvy;

    invoke-virtual {v0}, Lvy;->k()I

    .line 2088
    iget-object v0, p0, Lvt;->c:Lwz;

    iget-object v0, p0, Lvt;->m:Lvy;

    invoke-virtual {v0}, Lvy;->l()I

    .line 2089
    iget-object v0, p0, Lvt;->c:Lwz;

    iget-object v0, p0, Lvt;->m:Lvy;

    invoke-virtual {v0}, Lvy;->j()I

    .line 2090
    iget-object v0, p0, Lvt;->c:Lwz;

    iget-object v0, p0, Lvt;->m:Lvy;

    invoke-virtual {v0}, Lvy;->i()I

    .line 2091
    iget-object v0, p0, Lvt;->c:Lwz;

    iget-object v0, p0, Lvt;->m:Lvy;

    invoke-virtual {v0}, Lvy;->h()I

    .line 2093
    iget-object v0, p0, Lvt;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2094
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2095
    iget-object v0, p0, Lvt;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvw;

    .line 2096
    invoke-virtual {v0}, Lvw;->a()V

    .line 2094
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2098
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lvp;
    .locals 3

    .prologue
    .line 1549
    iget-object v0, p0, Lvt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    .line 1550
    iget-object v0, p0, Lvt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvp;

    .line 1551
    if-nez v0, :cond_0

    .line 1552
    iget-object v0, p0, Lvt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 1553
    :cond_0
    iget-object v2, v0, Lvp;->b:Landroid/content/Context;

    if-ne v2, p1, :cond_2

    .line 1559
    :goto_1
    return-object v0

    .line 1557
    :cond_1
    new-instance v0, Lvp;

    invoke-direct {v0, p1}, Lvp;-><init>(Landroid/content/Context;)V

    .line 1558
    iget-object v1, p0, Lvt;->a:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lvy;
    .locals 2

    .prologue
    .line 2035
    iget-object v0, p0, Lvt;->d:Lxa;

    invoke-virtual {p0, v0}, Lvt;->c(Lvf;)I

    move-result v0

    .line 2036
    if-ltz v0, :cond_0

    .line 2037
    iget-object v1, p0, Lvt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvx;

    .line 2038
    invoke-virtual {v0, p1}, Lvx;->a(Ljava/lang/String;)I

    move-result v1

    .line 2039
    if-ltz v1, :cond_0

    .line 2040
    iget-object v0, v0, Lvx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvy;

    .line 2043
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 1542
    new-instance v0, Lwu;

    iget-object v1, p0, Lvt;->e:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lwu;-><init>(Landroid/content/Context;Lwx;)V

    iput-object v0, p0, Lvt;->k:Lwu;

    .line 1544
    iget-object v0, p0, Lvt;->k:Lwu;

    invoke-virtual {v0}, Lwu;->a()V

    .line 1545
    return-void
.end method

.method public a(Lvf;)V
    .locals 3

    .prologue
    .line 1742
    invoke-virtual {p0, p1}, Lvt;->c(Lvf;)I

    move-result v0

    .line 1743
    if-gez v0, :cond_1

    .line 1745
    new-instance v0, Lvx;

    invoke-direct {v0, p1}, Lvx;-><init>(Lvf;)V

    .line 1746
    iget-object v1, p0, Lvt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1747
    invoke-static {}, Lvp;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1748
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Provider added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1750
    :cond_0
    iget-object v1, p0, Lvt;->i:Lvu;

    const/16 v2, 0x201

    invoke-virtual {v1, v2, v0}, Lvu;->a(ILjava/lang/Object;)V

    .line 1752
    iget-object v1, p1, Lvf;->g:Lvk;

    invoke-virtual {p0, v0, v1}, Lvt;->a(Lvx;Lvk;)V

    .line 1754
    iget-object v0, p0, Lvt;->h:Lvv;

    invoke-virtual {p1, v0}, Lvf;->a(Lvg;)V

    .line 1756
    iget-object v0, p0, Lvt;->o:Lve;

    invoke-virtual {p1, v0}, Lvf;->a(Lve;)V

    .line 1758
    :cond_1
    return-void
.end method

.method a(Lvx;Lvk;)V
    .locals 15

    .prologue
    .line 1802
    invoke-virtual/range {p1 .. p2}, Lvx;->a(Lvk;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1805
    const/4 v6, 0x0

    .line 1806
    const/4 v5, 0x0

    .line 1807
    if-eqz p2, :cond_c

    .line 1808
    invoke-virtual/range {p2 .. p2}, Lvk;->b()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1809
    invoke-virtual/range {p2 .. p2}, Lvk;->a()Ljava/util/List;

    move-result-object v8

    .line 1811
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    .line 1812
    const/4 v1, 0x0

    move v7, v1

    :goto_0
    if-ge v7, v9, :cond_c

    .line 1813
    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvc;

    .line 1814
    invoke-virtual {v1}, Lvc;->a()Ljava/lang/String;

    move-result-object v10

    .line 1815
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lvx;->a(Ljava/lang/String;)I

    move-result v4

    .line 1816
    if-gez v4, :cond_3

    .line 1818
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lvx;->c()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lvt;->b(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    move-object v2, v3

    .line 1819
    :goto_1
    new-instance v3, Lvy;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v10, v2}, Lvy;-><init>(Lvx;Ljava/lang/String;Ljava/lang/String;)V

    .line 1820
    move-object/from16 v0, p1

    iget-object v4, v0, Lvx;->b:Ljava/util/ArrayList;

    add-int/lit8 v2, v6, 0x1

    invoke-virtual {v4, v6, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1821
    iget-object v4, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1823
    invoke-virtual {v3, v1}, Lvy;->a(Lvc;)I

    .line 1825
    invoke-static {}, Lvp;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1826
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Route added: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1828
    :cond_0
    iget-object v1, p0, Lvt;->i:Lvu;

    const/16 v4, 0x101

    invoke-virtual {v1, v4, v3}, Lvu;->a(ILjava/lang/Object;)V

    move v1, v5

    .line 1812
    :goto_2
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v5, v1

    move v6, v2

    goto :goto_0

    .line 1818
    :cond_1
    const/4 v2, 0x2

    :goto_3
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "%s_%d"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v3, v12, v13

    const/4 v13, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v4, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lvt;->b(Ljava/lang/String;)I

    move-result v11

    if-gez v11, :cond_2

    move-object v2, v4

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1829
    :cond_3
    if-ge v4, v6, :cond_4

    .line 1830
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring route descriptor with duplicate id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v1, v5

    move v2, v6

    goto :goto_2

    .line 1834
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lvx;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lvy;

    .line 1835
    move-object/from16 v0, p1

    iget-object v10, v0, Lvx;->b:Ljava/util/ArrayList;

    add-int/lit8 v3, v6, 0x1

    invoke-static {v10, v4, v6}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1838
    invoke-virtual {v2, v1}, Lvy;->a(Lvc;)I

    move-result v1

    .line 1840
    if-eqz v1, :cond_12

    .line 1841
    and-int/lit8 v4, v1, 0x1

    if-eqz v4, :cond_6

    .line 1842
    invoke-static {}, Lvp;->e()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1843
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Route changed: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1845
    :cond_5
    iget-object v4, p0, Lvt;->i:Lvu;

    const/16 v6, 0x103

    invoke-virtual {v4, v6, v2}, Lvu;->a(ILjava/lang/Object;)V

    .line 1848
    :cond_6
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_8

    .line 1849
    invoke-static {}, Lvp;->e()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1850
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Route volume changed: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1852
    :cond_7
    iget-object v4, p0, Lvt;->i:Lvu;

    const/16 v6, 0x104

    invoke-virtual {v4, v6, v2}, Lvu;->a(ILjava/lang/Object;)V

    .line 1855
    :cond_8
    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_a

    .line 1856
    invoke-static {}, Lvp;->e()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1857
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Route presentation display changed: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1860
    :cond_9
    iget-object v1, p0, Lvt;->i:Lvu;

    const/16 v4, 0x105

    invoke-virtual {v1, v4, v2}, Lvu;->a(ILjava/lang/Object;)V

    .line 1863
    :cond_a
    iget-object v1, p0, Lvt;->m:Lvy;

    if-ne v2, v1, :cond_12

    .line 1864
    const/4 v1, 0x1

    move v2, v3

    goto/16 :goto_2

    .line 1870
    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring invalid provider descriptor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1875
    :cond_c
    move-object/from16 v0, p1

    iget-object v1, v0, Lvx;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_4
    if-lt v2, v6, :cond_d

    .line 1877
    move-object/from16 v0, p1

    iget-object v1, v0, Lvx;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvy;

    .line 1878
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lvy;->a(Lvc;)I

    .line 1880
    iget-object v3, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1875
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_4

    .line 1884
    :cond_d
    invoke-direct {p0, v5}, Lvt;->a(Z)V

    .line 1891
    move-object/from16 v0, p1

    iget-object v1, v0, Lvx;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_5
    if-lt v2, v6, :cond_f

    .line 1892
    move-object/from16 v0, p1

    iget-object v1, v0, Lvx;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvy;

    .line 1893
    invoke-static {}, Lvp;->e()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1894
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Route removed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1896
    :cond_e
    iget-object v3, p0, Lvt;->i:Lvu;

    const/16 v4, 0x102

    invoke-virtual {v3, v4, v1}, Lvu;->a(ILjava/lang/Object;)V

    .line 1891
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_5

    .line 1900
    :cond_f
    invoke-static {}, Lvp;->e()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1901
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Provider changed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1903
    :cond_10
    iget-object v1, p0, Lvt;->i:Lvu;

    const/16 v2, 0x203

    move-object/from16 v0, p1

    invoke-virtual {v1, v2, v0}, Lvu;->a(ILjava/lang/Object;)V

    .line 1905
    :cond_11
    return-void

    :cond_12
    move v1, v5

    move v2, v3

    goto/16 :goto_2
.end method

.method public a(Lvy;)V
    .locals 2

    .prologue
    .line 1637
    iget-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1638
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ignoring attempt to select removed route: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1647
    :goto_0
    return-void

    .line 1641
    :cond_0
    iget-boolean v0, p1, Lvy;->c:Z

    if-nez v0, :cond_1

    .line 1642
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ignoring attempt to select disabled route: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1646
    :cond_1
    invoke-direct {p0, p1}, Lvt;->c(Lvy;)V

    goto :goto_0
.end method

.method public a(Lvy;I)V
    .locals 1

    .prologue
    .line 1595
    iget-object v0, p0, Lvt;->m:Lvy;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lvt;->n:Lvj;

    if-eqz v0, :cond_0

    .line 1596
    iget-object v0, p0, Lvt;->n:Lvj;

    invoke-virtual {v0, p2}, Lvj;->a(I)V

    .line 1598
    :cond_0
    return-void
.end method

.method public a(Lvn;I)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1650
    invoke-virtual {p1}, Lvn;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1673
    :goto_0
    return v0

    .line 1655
    :cond_0
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lvt;->j:Z

    if-eqz v0, :cond_1

    move v0, v2

    .line 1656
    goto :goto_0

    .line 1660
    :cond_1
    iget-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v1

    .line 1661
    :goto_1
    if-ge v3, v4, :cond_4

    .line 1662
    iget-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvy;

    .line 1663
    and-int/lit8 v5, p2, 0x1

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lvy;->g()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1665
    :cond_2
    invoke-virtual {v0, p1}, Lvy;->a(Lvn;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 1668
    goto :goto_0

    .line 1661
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 1673
    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lvy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1607
    iget-object v0, p0, Lvt;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method public b(Lvf;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1762
    invoke-virtual {p0, p1}, Lvt;->c(Lvf;)I

    move-result v1

    .line 1763
    if-ltz v1, :cond_1

    .line 1765
    invoke-virtual {p1, v2}, Lvf;->a(Lvg;)V

    .line 1767
    invoke-virtual {p1, v2}, Lvf;->a(Lve;)V

    .line 1769
    iget-object v0, p0, Lvt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvx;

    .line 1770
    invoke-virtual {p0, v0, v2}, Lvt;->a(Lvx;Lvk;)V

    .line 1772
    invoke-static {}, Lvp;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1773
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider removed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1775
    :cond_0
    iget-object v2, p0, Lvt;->i:Lvu;

    const/16 v3, 0x202

    invoke-virtual {v2, v3, v0}, Lvu;->a(ILjava/lang/Object;)V

    .line 1776
    iget-object v0, p0, Lvt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1778
    :cond_1
    return-void
.end method

.method public b(Lvy;I)V
    .locals 1

    .prologue
    .line 1601
    iget-object v0, p0, Lvt;->m:Lvy;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lvt;->n:Lvj;

    if-eqz v0, :cond_0

    .line 1602
    iget-object v0, p0, Lvt;->n:Lvj;

    invoke-virtual {v0, p2}, Lvj;->b(I)V

    .line 1604
    :cond_0
    return-void
.end method

.method c(Lvf;)I
    .locals 3

    .prologue
    .line 1791
    iget-object v0, p0, Lvt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1792
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1793
    iget-object v0, p0, Lvt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvx;

    iget-object v0, v0, Lvx;->a:Lvf;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 1797
    :goto_1
    return v0

    .line 1792
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1797
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public c()Lvy;
    .locals 2

    .prologue
    .line 1615
    iget-object v0, p0, Lvt;->l:Lvy;

    if-nez v0, :cond_0

    .line 1619
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no default route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1622
    :cond_0
    iget-object v0, p0, Lvt;->l:Lvy;

    return-object v0
.end method

.method public d()Lvy;
    .locals 2

    .prologue
    .line 1626
    iget-object v0, p0, Lvt;->m:Lvy;

    if-nez v0, :cond_0

    .line 1630
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1633
    :cond_0
    iget-object v0, p0, Lvt;->m:Lvy;

    return-object v0
.end method

.method public e()V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1678
    .line 1680
    new-instance v8, Lvo;

    invoke-direct {v8}, Lvo;-><init>()V

    .line 1681
    iget-object v0, p0, Lvt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v5

    move v4, v5

    :goto_0
    add-int/lit8 v7, v0, -0x1

    if-ltz v7, :cond_5

    .line 1682
    iget-object v0, p0, Lvt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvp;

    .line 1683
    if-nez v0, :cond_0

    .line 1684
    iget-object v0, p0, Lvt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v7

    goto :goto_0

    .line 1686
    :cond_0
    iget-object v1, v0, Lvp;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v5

    .line 1687
    :goto_1
    if-ge v6, v9, :cond_4

    .line 1688
    iget-object v1, v0, Lvp;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvr;

    .line 1689
    iget-object v10, v1, Lvr;->c:Lvn;

    invoke-virtual {v8, v10}, Lvo;->a(Lvn;)Lvo;

    .line 1690
    iget v10, v1, Lvr;->d:I

    and-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_1

    move v2, v3

    move v4, v3

    .line 1694
    :cond_1
    iget v10, v1, Lvr;->d:I

    and-int/lit8 v10, v10, 0x4

    if-eqz v10, :cond_2

    .line 1695
    iget-boolean v10, p0, Lvt;->j:Z

    if-nez v10, :cond_2

    move v4, v3

    .line 1699
    :cond_2
    iget v1, v1, Lvr;->d:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    move v4, v3

    .line 1687
    :cond_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_4
    move v0, v7

    .line 1704
    goto :goto_0

    .line 1705
    :cond_5
    if-eqz v4, :cond_7

    invoke-virtual {v8}, Lvo;->a()Lvn;

    move-result-object v0

    .line 1708
    :goto_2
    iget-object v1, p0, Lvt;->o:Lve;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lvt;->o:Lve;

    invoke-virtual {v1}, Lve;->a()Lvn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lvn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lvt;->o:Lve;

    invoke-virtual {v1}, Lve;->b()Z

    move-result v1

    if-ne v1, v2, :cond_8

    .line 1738
    :cond_6
    return-void

    .line 1705
    :cond_7
    sget-object v0, Lvn;->a:Lvn;

    goto :goto_2

    .line 1713
    :cond_8
    invoke-virtual {v0}, Lvn;->b()Z

    move-result v1

    if-eqz v1, :cond_b

    if-nez v2, :cond_b

    .line 1715
    iget-object v0, p0, Lvt;->o:Lve;

    if-eqz v0, :cond_6

    .line 1718
    const/4 v0, 0x0

    iput-object v0, p0, Lvt;->o:Lve;

    .line 1723
    :goto_3
    invoke-static {}, Lvp;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1724
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Updated discovery request: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lvt;->o:Lve;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1726
    :cond_9
    if-eqz v4, :cond_a

    if-nez v2, :cond_a

    iget-boolean v0, p0, Lvt;->j:Z

    .line 1727
    :cond_a
    iget-object v0, p0, Lvt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v5

    .line 1735
    :goto_4
    if-ge v1, v2, :cond_6

    .line 1736
    iget-object v0, p0, Lvt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvx;

    iget-object v0, v0, Lvx;->a:Lvf;

    iget-object v3, p0, Lvt;->o:Lve;

    invoke-virtual {v0, v3}, Lvf;->a(Lve;)V

    .line 1735
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1721
    :cond_b
    new-instance v1, Lve;

    invoke-direct {v1, v0, v2}, Lve;-><init>(Lvn;Z)V

    iput-object v1, p0, Lvt;->o:Lve;

    goto :goto_3
.end method
