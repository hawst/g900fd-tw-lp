.class public Lknx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlv;


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lknx;->a:I

    .line 25
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lhlx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    invoke-interface {p2, p1}, Lhlx;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lhlw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    invoke-interface {v0, p1, p0}, Lhlw;->a(Landroid/content/Context;Lhlv;)V

    .line 32
    return-void
.end method

.method public a(Lhmp;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 50
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CounterEvent: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lknx;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
