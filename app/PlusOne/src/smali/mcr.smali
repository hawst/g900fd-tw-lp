.class public final Lmcr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmrg;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31102
    invoke-direct {p0}, Loxq;-><init>()V

    .line 31105
    iput-object v0, p0, Lmcr;->apiHeader:Llyr;

    .line 31108
    iput-object v0, p0, Lmcr;->a:Lmrg;

    .line 31102
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 31125
    const/4 v0, 0x0

    .line 31126
    iget-object v1, p0, Lmcr;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 31127
    const/4 v0, 0x1

    iget-object v1, p0, Lmcr;->apiHeader:Llyr;

    .line 31128
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 31130
    :cond_0
    iget-object v1, p0, Lmcr;->a:Lmrg;

    if-eqz v1, :cond_1

    .line 31131
    const/4 v1, 0x2

    iget-object v2, p0, Lmcr;->a:Lmrg;

    .line 31132
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31134
    :cond_1
    iget-object v1, p0, Lmcr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31135
    iput v0, p0, Lmcr;->ai:I

    .line 31136
    return v0
.end method

.method public a(Loxn;)Lmcr;
    .locals 2

    .prologue
    .line 31144
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 31145
    sparse-switch v0, :sswitch_data_0

    .line 31149
    iget-object v1, p0, Lmcr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 31150
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcr;->ah:Ljava/util/List;

    .line 31153
    :cond_1
    iget-object v1, p0, Lmcr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 31155
    :sswitch_0
    return-object p0

    .line 31160
    :sswitch_1
    iget-object v0, p0, Lmcr;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 31161
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmcr;->apiHeader:Llyr;

    .line 31163
    :cond_2
    iget-object v0, p0, Lmcr;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 31167
    :sswitch_2
    iget-object v0, p0, Lmcr;->a:Lmrg;

    if-nez v0, :cond_3

    .line 31168
    new-instance v0, Lmrg;

    invoke-direct {v0}, Lmrg;-><init>()V

    iput-object v0, p0, Lmcr;->a:Lmrg;

    .line 31170
    :cond_3
    iget-object v0, p0, Lmcr;->a:Lmrg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 31145
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 31113
    iget-object v0, p0, Lmcr;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 31114
    const/4 v0, 0x1

    iget-object v1, p0, Lmcr;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31116
    :cond_0
    iget-object v0, p0, Lmcr;->a:Lmrg;

    if-eqz v0, :cond_1

    .line 31117
    const/4 v0, 0x2

    iget-object v1, p0, Lmcr;->a:Lmrg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31119
    :cond_1
    iget-object v0, p0, Lmcr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 31121
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 31098
    invoke-virtual {p0, p1}, Lmcr;->a(Loxn;)Lmcr;

    move-result-object v0

    return-object v0
.end method
