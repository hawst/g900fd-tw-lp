.class public final Ljrx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljpi;
.implements Ljrl;


# instance fields
.field private final a:Ljrm;

.field private final b:Ljoz;

.field private c:Z

.field private final d:I

.field private final e:I

.field private final f:Ljqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljqc",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljrm;Ljoz;IILjqc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljrm;",
            "Ljoz;",
            "II",
            "Ljqc",
            "<",
            "Ljpv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Ljrx;->a:Ljrm;

    .line 29
    iput-object p2, p0, Ljrx;->b:Ljoz;

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljrx;->c:Z

    .line 31
    iput p3, p0, Ljrx;->d:I

    .line 32
    iput p4, p0, Ljrx;->e:I

    .line 33
    iput-object p5, p0, Ljrx;->f:Ljqc;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljrx;->g:Ljava/util/List;

    .line 35
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Ljrx;->g:Ljava/util/List;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ljrx;->a:Ljrm;

    invoke-virtual {v0, p0}, Ljrm;->b(Ljrl;)V

    .line 45
    return-void
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 86
    iget-object v0, p0, Ljrx;->a:Ljrm;

    invoke-virtual {v0}, Ljrm;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Ljrx;->a:Ljrm;

    invoke-virtual {v0}, Ljrm;->c()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    iget-object v6, p0, Ljrx;->f:Ljqc;

    invoke-interface {v6, v0}, Ljqc;->a(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v0, p0, Ljrx;->d:I

    packed-switch v0, :pswitch_data_0

    :goto_1
    iget v0, p0, Ljrx;->e:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_2

    iget v0, p0, Ljrx;->e:I

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    :cond_2
    iput-object v3, p0, Ljrx;->g:Ljava/util/List;

    .line 88
    :cond_3
    :goto_2
    iget-boolean v0, p0, Ljrx;->c:Z

    if-eqz v0, :cond_5

    .line 89
    iput-boolean v2, p0, Ljrx;->c:Z

    .line 90
    iget-object v0, p0, Ljrx;->b:Ljoz;

    invoke-interface {v0, p0}, Ljoz;->a(Ljpi;)V

    .line 94
    :goto_3
    return-void

    .line 86
    :pswitch_0
    sget-object v0, Ljru;->a:Ljava/util/Comparator;

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    :pswitch_1
    sget-object v0, Ljru;->b:Ljava/util/Comparator;

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    :cond_4
    iget v0, p0, Ljrx;->e:I

    invoke-interface {v3, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljrx;->g:Ljava/util/List;

    goto :goto_2

    .line 92
    :cond_5
    iget-object v0, p0, Ljrx;->b:Ljoz;

    goto :goto_3

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
