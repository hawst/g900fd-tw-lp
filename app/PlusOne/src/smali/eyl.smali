.class public final Leyl;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Leas;
.implements Lhjj;
.implements Lhmq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/widget/AbsListView$RecyclerListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Leas;",
        "Lhjj;",
        "Lhmq;"
    }
.end annotation


# instance fields
.field private final N:Lhje;

.field private O:Lhee;

.field private P:Ljava/lang/Integer;

.field private Q:Ljava/lang/Integer;

.field private R:Ljava/lang/Integer;

.field private S:Ljava/lang/Integer;

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Lidh;

.field private X:Z

.field private Y:Lear;

.field private final Z:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Llol;-><init>()V

    .line 98
    new-instance v0, Lhje;

    iget-object v1, p0, Leyl;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Leyl;->N:Lhje;

    .line 121
    new-instance v0, Lhnw;

    new-instance v1, Leyp;

    invoke-direct {v1, p0}, Leyp;-><init>(Leyl;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 156
    new-instance v0, Leym;

    invoke-direct {v0, p0}, Leym;-><init>(Leyl;)V

    iput-object v0, p0, Leyl;->Z:Lfhh;

    return-void
.end method

.method static synthetic a(Leyl;)Lhee;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Leyl;->O:Lhee;

    return-object v0
.end method

.method static synthetic a(Leyl;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Leyl;->U:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Leyl;->N:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 491
    iget-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 492
    const/4 v0, 0x0

    iput-boolean v0, p0, Leyl;->X:Z

    .line 494
    :cond_0
    return-void
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    .line 516
    iget-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 532
    :cond_0
    :goto_0
    return-void

    .line 520
    :cond_1
    invoke-virtual {p0}, Leyl;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 522
    if-eqz v0, :cond_2

    .line 523
    invoke-virtual {v0}, Lt;->a()V

    .line 526
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    .line 528
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    invoke-virtual {p0}, Leyl;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 530
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic a(Leyl;ILfib;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Leyl;->c(ILfib;)V

    return-void
.end method

.method static synthetic a(Leyl;Lhgw;)V
    .locals 6

    .prologue
    .line 71
    invoke-virtual {p0}, Leyl;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyl;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyl;->T:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0a0947

    invoke-direct {p0, v0}, Leyl;->d(I)V

    invoke-virtual {p0}, Leyl;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leyl;->O:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leyl;->U:Ljava/lang/String;

    iget-object v3, p0, Leyl;->V:Ljava/lang/String;

    iget-object v4, p0, Leyl;->T:Ljava/lang/String;

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhgw;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyl;->R:Ljava/lang/Integer;

    goto :goto_0
.end method

.method private a(Lfib;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 563
    iget-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 567
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 568
    invoke-virtual {p0}, Leyl;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 571
    :cond_1
    iput-object v2, p0, Leyl;->P:Ljava/lang/Integer;

    .line 573
    invoke-direct {p0}, Leyl;->a()V

    .line 574
    return-void
.end method

.method static synthetic b(Leyl;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Leyl;->U:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Leyl;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Leyl;->T:Ljava/lang/String;

    return-object p1
.end method

.method private b(ILfib;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 540
    iget-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 555
    :cond_0
    :goto_0
    return-void

    .line 544
    :cond_1
    iget-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    if-nez v0, :cond_2

    .line 545
    const/4 v0, 0x0

    iput-boolean v0, p0, Leyl;->X:Z

    .line 548
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 549
    invoke-virtual {p0}, Leyl;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 552
    :cond_3
    iput-object v2, p0, Leyl;->Q:Ljava/lang/Integer;

    .line 554
    invoke-direct {p0}, Leyl;->a()V

    goto :goto_0
.end method

.method static synthetic b(Leyl;ILfib;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p2}, Leyl;->a(Lfib;)V

    return-void
.end method

.method static synthetic c(Leyl;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Leyl;->V:Ljava/lang/String;

    return-object p1
.end method

.method private c(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 467
    if-eq p1, v4, :cond_0

    if-nez p1, :cond_1

    .line 468
    :cond_0
    iget-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 469
    iget-object v0, p0, Leyl;->at:Llnl;

    iget-object v1, p0, Leyl;->O:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leyl;->U:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->j(Landroid/content/Context;ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    .line 473
    :cond_1
    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    if-nez p1, :cond_3

    .line 474
    :cond_2
    iget-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    if-nez v0, :cond_3

    .line 475
    iget-object v0, p0, Leyl;->at:Llnl;

    iget-object v1, p0, Leyl;->O:Lhee;

    .line 476
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leyl;->U:Ljava/lang/String;

    iget-object v3, p0, Leyl;->V:Ljava/lang/String;

    .line 475
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->e(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    .line 481
    :cond_3
    invoke-direct {p0}, Leyl;->a()V

    .line 482
    return-void
.end method

.method private c(ILfib;)V
    .locals 3

    .prologue
    .line 582
    iget-object v0, p0, Leyl;->R:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyl;->R:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 596
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    invoke-virtual {p0}, Leyl;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lt;->a()V

    .line 588
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Leyl;->R:Ljava/lang/Integer;

    .line 590
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 591
    invoke-virtual {p0}, Leyl;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 592
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 594
    :cond_3
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Leyl;->c(I)V

    goto :goto_0
.end method

.method static synthetic c(Leyl;ILfib;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Leyl;->b(ILfib;)V

    return-void
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 599
    const/4 v0, 0x0

    .line 600
    invoke-virtual {p0, p1}, Leyl;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 599
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 602
    invoke-virtual {p0}, Leyl;->p()Lae;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 603
    return-void
.end method

.method static synthetic d(Leyl;ILfib;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Leyl;->a(ILfib;)V

    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 307
    sget-object v0, Lhmw;->D:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 334
    iget-object v0, p0, Leyl;->O:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "domain_name"

    .line 335
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 337
    const v0, 0x7f040163

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 338
    const v0, 0x102000a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 339
    iget-object v3, p0, Leyl;->Y:Lear;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 340
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 341
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 342
    iget-object v0, p0, Leyl;->Y:Lear;

    invoke-virtual {v0, v1}, Lear;->c(Ljava/lang/String;)V

    .line 344
    invoke-virtual {p0}, Leyl;->w()Lbb;

    move-result-object v0

    .line 345
    invoke-virtual {v0, v4, v5, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 346
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v5, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 348
    return-object v2
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353
    const/4 v0, 0x0

    .line 354
    invoke-virtual {p0}, Leyl;->n()Lz;

    move-result-object v1

    .line 356
    packed-switch p1, :pswitch_data_0

    .line 372
    :goto_0
    return-object v0

    .line 358
    :pswitch_0
    new-instance v0, Leyn;

    sget-object v2, Lidg;->a:Landroid/net/Uri;

    invoke-direct {v0, p0, v1, v2, v1}, Leyn;-><init>(Leyl;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;)V

    goto :goto_0

    .line 367
    :pswitch_1
    new-instance v0, Leat;

    invoke-virtual {p0}, Leyl;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leyl;->O:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Leyl;->U:Ljava/lang/String;

    iget-object v4, p0, Leyl;->T:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Leat;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 356
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 428
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 429
    packed-switch p1, :pswitch_data_0

    .line 451
    :goto_0
    return-void

    .line 431
    :pswitch_0
    iget-object v0, p0, Leyl;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leyl;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aG:Lhmv;

    .line 432
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 431
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0

    .line 439
    :cond_0
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 441
    :pswitch_1
    iget-object v0, p0, Leyl;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leyl;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aF:Lhmv;

    .line 442
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 441
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 443
    const-string v0, "extra_acl"

    .line 444
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 445
    new-instance v1, Leyo;

    invoke-direct {v1, p0, v0}, Leyo;-><init>(Leyl;Lhgw;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 439
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 252
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 253
    new-instance v0, Lear;

    invoke-direct {v0, p1}, Lear;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leyl;->Y:Lear;

    .line 254
    iget-object v0, p0, Leyl;->Y:Lear;

    invoke-virtual {v0, p0}, Lear;->a(Leas;)V

    .line 255
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 207
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Leyl;->X:Z

    .line 212
    if-eqz p1, :cond_6

    .line 213
    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leyl;->U:Ljava/lang/String;

    .line 217
    :cond_0
    const-string v0, "ownerid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    const-string v0, "ownerid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leyl;->T:Ljava/lang/String;

    .line 221
    :cond_1
    const-string v0, "invitemoreid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    const-string v0, "invitemoreid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyl;->R:Ljava/lang/Integer;

    .line 225
    :cond_2
    const-string v0, "inviteesreq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 226
    const-string v0, "inviteesreq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    .line 229
    :cond_3
    const-string v0, "eventreq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 230
    const-string v0, "eventreq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    .line 233
    :cond_4
    const-string v0, "eventaddremovereq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 234
    const-string v0, "eventaddremovereq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    .line 237
    :cond_5
    const-string v0, "inviteesrefreshneeded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 238
    const-string v0, "inviteesrefreshneeded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leyl;->X:Z

    .line 242
    :cond_6
    iget-object v0, p0, Leyl;->Y:Lear;

    iget-object v1, p0, Leyl;->O:Lhee;

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lear;->b(Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Leyl;->Y:Lear;

    iget-object v1, p0, Leyl;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lear;->a(Ljava/lang/String;)V

    .line 245
    iget-boolean v0, p0, Leyl;->X:Z

    if-eqz v0, :cond_7

    .line 246
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Leyl;->c(I)V

    .line 248
    :cond_7
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 398
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 377
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 379
    :pswitch_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-static {p2, v0, v1}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;

    move-result-object v0

    iput-object v0, p0, Leyl;->W:Lidh;

    .line 383
    iget-object v0, p0, Leyl;->W:Lidh;

    invoke-virtual {v0}, Lidh;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leyl;->V:Ljava/lang/String;

    .line 386
    iget-object v0, p0, Leyl;->N:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto :goto_0

    .line 390
    :pswitch_1
    iget-object v0, p0, Leyl;->Y:Lear;

    invoke-virtual {v0, p2}, Lear;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    .line 377
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 71
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Leyl;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 312
    invoke-virtual {p0}, Leyl;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leyl;->W:Lidh;

    iget-object v2, p0, Leyl;->O:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-static {v0, v1, v2}, Ldrm;->b(Landroid/content/Context;Lidh;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyl;->O:Lhee;

    .line 313
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    const v0, 0x7f10068a

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 317
    :cond_0
    const v0, 0x7f10067b

    .line 318
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 319
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 321
    iget-object v1, p0, Leyl;->P:Ljava/lang/Integer;

    if-nez v1, :cond_1

    iget-object v1, p0, Leyl;->Q:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 322
    :cond_1
    invoke-virtual {v0}, Lhjv;->b()V

    .line 325
    :cond_2
    iget-object v0, p0, Leyl;->W:Lidh;

    if-eqz v0, :cond_3

    .line 326
    iget-object v0, p0, Leyl;->W:Lidh;

    invoke-virtual {v0}, Lidh;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 328
    :cond_3
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 615
    const v0, 0x7f0a0954

    invoke-direct {p0, v0}, Leyl;->d(I)V

    .line 616
    iget-object v0, p0, Leyl;->at:Llnl;

    iget-object v1, p0, Leyl;->O:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leyl;->U:Ljava/lang/String;

    iget-object v3, p0, Leyl;->V:Ljava/lang/String;

    const/4 v4, 0x1

    move-object v5, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    .line 618
    return-void
.end method

.method public a(Loo;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Leyl;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 134
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 402
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 403
    const v1, 0x7f10067b

    if-ne v0, v1, :cond_0

    .line 404
    invoke-direct {p0, v5}, Leyl;->c(I)V

    .line 423
    :goto_0
    return v7

    .line 406
    :cond_0
    const v1, 0x7f10068a

    if-ne v0, v1, :cond_2

    .line 407
    iget-object v0, p0, Leyl;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leyl;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->aD:Lhmv;

    .line 408
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 407
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 409
    iget-object v0, p0, Leyl;->O:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 410
    invoke-virtual {p0}, Leyl;->n()Lz;

    move-result-object v0

    const v2, 0x7f0a0941

    .line 411
    invoke-virtual {p0, v2}, Leyl;->e_(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Leyl;->W:Lidh;

    .line 413
    invoke-virtual {v4}, Lidh;->i()Lpbj;

    move-result-object v4

    iget-object v4, v4, Lpbj;->b:Lpbd;

    iget-object v4, v4, Lpbd;->b:Lpbc;

    iget-object v4, v4, Lpbc;->a:Ljava/lang/Boolean;

    .line 412
    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v4, 0xc

    :goto_1
    move v6, v5

    move v8, v5

    .line 410
    invoke-static/range {v0 .. v8}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Lhgw;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Leyl;->a(Landroid/content/Intent;I)V

    goto :goto_0

    .line 412
    :cond_1
    const/16 v4, 0xb

    goto :goto_1

    :cond_2
    move v7, v5

    .line 423
    goto :goto_0
.end method

.method public aO_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 265
    invoke-super {p0}, Llol;->aO_()V

    .line 266
    iget-object v0, p0, Leyl;->Z:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 268
    iget-object v0, p0, Leyl;->R:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Leyl;->R:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Leyl;->R:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 271
    iget-object v1, p0, Leyl;->R:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leyl;->c(ILfib;)V

    .line 272
    iput-object v2, p0, Leyl;->R:Ljava/lang/Integer;

    .line 276
    :cond_0
    iget-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 278
    iget-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 279
    iget-object v1, p0, Leyl;->Q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leyl;->b(ILfib;)V

    .line 280
    iput-object v2, p0, Leyl;->Q:Ljava/lang/Integer;

    .line 284
    :cond_1
    iget-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 285
    iget-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 286
    iget-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 287
    iget-object v1, p0, Leyl;->P:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    invoke-direct {p0, v0}, Leyl;->a(Lfib;)V

    .line 288
    iput-object v2, p0, Leyl;->P:Ljava/lang/Integer;

    .line 292
    :cond_2
    iget-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 293
    iget-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 294
    iget-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 295
    iget-object v1, p0, Leyl;->S:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Leyl;->a(ILfib;)V

    .line 296
    iput-object v2, p0, Leyl;->S:Ljava/lang/Integer;

    .line 300
    :cond_3
    iget-boolean v0, p0, Leyl;->X:Z

    if-eqz v0, :cond_4

    .line 301
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Leyl;->c(I)V

    .line 303
    :cond_4
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 622
    const v0, 0x7f0a0955

    invoke-direct {p0, v0}, Leyl;->d(I)V

    .line 623
    iget-object v0, p0, Leyl;->at:Llnl;

    iget-object v1, p0, Leyl;->O:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leyl;->U:Ljava/lang/String;

    iget-object v3, p0, Leyl;->V:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v5, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    .line 625
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 138
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 127
    iget-object v0, p0, Leyl;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 128
    iget-object v0, p0, Leyl;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Leyl;->O:Lhee;

    .line 129
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 181
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 183
    const-string v0, "id"

    iget-object v1, p0, Leyl;->U:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v0, "ownerid"

    iget-object v1, p0, Leyl;->T:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Leyl;->R:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 187
    const-string v0, "invitemoreid"

    iget-object v1, p0, Leyl;->R:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 190
    :cond_0
    iget-object v0, p0, Leyl;->Q:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 191
    const-string v0, "inviteesreq"

    iget-object v1, p0, Leyl;->Q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    :cond_1
    iget-object v0, p0, Leyl;->P:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 195
    const-string v0, "eventreq"

    iget-object v1, p0, Leyl;->P:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 198
    :cond_2
    iget-object v0, p0, Leyl;->S:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 199
    const-string v0, "eventaddremovereq"

    iget-object v1, p0, Leyl;->S:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 202
    :cond_3
    const-string v0, "inviteesrefreshneeded"

    iget-boolean v1, p0, Leyl;->X:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 203
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 636
    instance-of v0, p2, Ljpg;

    if-eqz v0, :cond_1

    .line 637
    check-cast p2, Ljpg;

    invoke-virtual {p2}, Ljpg;->e()Ljava/lang/String;

    move-result-object v0

    .line 638
    if-eqz v0, :cond_1

    .line 639
    iget-object v1, p0, Leyl;->O:Lhee;

    .line 640
    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    .line 641
    iget-object v2, p0, Leyl;->O:Lhee;

    invoke-interface {v2}, Lhee;->g()Lhej;

    move-result-object v2

    const-string v3, "is_google_plus"

    invoke-interface {v2, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v2

    .line 642
    if-eqz v1, :cond_0

    if-eqz v2, :cond_1

    .line 644
    :cond_0
    invoke-virtual {p0}, Leyl;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Leyl;->O:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x0

    .line 643
    invoke-static {v1, v2, v0, v3}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Leyl;->a(Landroid/content/Intent;)V

    .line 648
    :cond_1
    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 629
    instance-of v0, p1, Lljh;

    if-eqz v0, :cond_0

    .line 630
    check-cast p1, Lljh;

    invoke-interface {p1}, Lljh;->a()V

    .line 632
    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Leyl;->Z:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 260
    invoke-super {p0}, Llol;->z()V

    .line 261
    return-void
.end method
