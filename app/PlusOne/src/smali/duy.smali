.class Lduy;
.super Ldvb;
.source "PG"


# instance fields
.field private synthetic a:Ldur;


# direct methods
.method public constructor <init>(Ldur;I)V
    .locals 0

    .prologue
    .line 1863
    iput-object p1, p0, Lduy;->a:Ldur;

    .line 1864
    invoke-direct {p0, p1, p2}, Ldvb;-><init>(Ldur;I)V

    .line 1865
    return-void
.end method

.method public constructor <init>(Ldur;II)V
    .locals 0

    .prologue
    .line 1867
    iput-object p1, p0, Lduy;->a:Ldur;

    .line 1868
    invoke-direct {p0, p1, p2, p3}, Ldvb;-><init>(Ldur;II)V

    .line 1869
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1873
    const-string v0, "all_tiles LEFT OUTER JOIN media_cache ON (all_tiles.image_url=media_cache.image_url)"

    return-object v0
.end method

.method protected b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1878
    invoke-static {}, Ldur;->c()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1883
    const-string v0, "view_id = ? AND type = ? AND media_attr & 512 == 0"

    return-object v0
.end method

.method protected d()[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1888
    iget-object v0, p0, Lduy;->a:Ldur;

    invoke-static {v0}, Ldur;->b(Ldur;)Lhei;

    move-result-object v0

    iget-object v1, p0, Lduy;->a:Ldur;

    invoke-static {v1}, Ldur;->a(Ldur;)Ldve;

    move-result-object v1

    iget v1, v1, Ldve;->a:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1889
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    aput-object v0, v2, v3

    .line 1890
    invoke-static {v3, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    const/4 v0, 0x4

    .line 1891
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    return-object v1
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1897
    const-string v0, "all_tiles.image_url"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1902
    const-string v0, "view_order ASC"

    return-object v0
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 1907
    const/4 v0, 0x1

    return v0
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1912
    const-string v0, "Highlights"

    return-object v0
.end method
