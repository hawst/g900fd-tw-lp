.class public final Lcto;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljma;
.implements Llnx;
.implements Llrb;
.implements Llrc;
.implements Llrg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljma",
        "<",
        "Lctq;",
        ">;",
        "Llnx;",
        "Llrb;",
        "Llrc;",
        "Llrg;"
    }
.end annotation


# instance fields
.field private final a:Lu;

.field private final b:Lctp;

.field private c:Lctq;

.field private d:Lhms;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lu;Llqr;Lctp;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcto;->a:Lu;

    .line 51
    iput-object p3, p0, Lcto;->b:Lctp;

    .line 53
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 54
    return-void
.end method

.method private a(Lhmv;)V
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 97
    iget-object v0, p0, Lcto;->a:Lu;

    .line 98
    invoke-virtual {v0}, Lu;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 100
    if-ne v0, v2, :cond_0

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v1, p0, Lcto;->d:Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lcto;->e:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 105
    invoke-virtual {v2, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v0

    .line 104
    invoke-interface {v1, v0}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcto;->b:Lctp;

    invoke-interface {v0}, Lctp;->I_()V

    .line 76
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcto;->c:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    .line 83
    iget-object v1, p0, Lcto;->c:Lctq;

    invoke-virtual {v1, p1}, Lctq;->a(I)V

    .line 84
    iget-object v1, p0, Lcto;->c:Lctq;

    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    .line 86
    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    .line 87
    sget-object v0, Lhmv;->ee:Lhmv;

    invoke-direct {p0, v0}, Lcto;->a(Lhmv;)V

    .line 92
    :cond_0
    :goto_0
    iget-object v0, p0, Lcto;->b:Lctp;

    invoke-interface {v0}, Lctp;->ab_()V

    .line 93
    return-void

    .line 88
    :cond_1
    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    .line 89
    sget-object v0, Lhmv;->ef:Lhmv;

    invoke-direct {p0, v0}, Lcto;->a(Lhmv;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 58
    iput-object p1, p0, Lcto;->e:Landroid/content/Context;

    .line 59
    const-class v0, Lctq;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    iput-object v0, p0, Lcto;->c:Lctq;

    .line 60
    const-class v0, Lhms;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Lcto;->d:Lhms;

    .line 61
    return-void
.end method

.method public synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    invoke-virtual {p0}, Lcto;->a()V

    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcto;->c:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Ljlx;->a(Ljma;Z)V

    .line 66
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcto;->c:Lctq;

    invoke-virtual {v0}, Lctq;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0, p0}, Ljlx;->a(Ljma;)V

    .line 71
    return-void
.end method
