.class public final Lmnc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3425
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3456
    const/4 v0, 0x0

    .line 3457
    iget-object v1, p0, Lmnc;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 3458
    const/4 v0, 0x1

    iget-object v1, p0, Lmnc;->a:Ljava/lang/Float;

    .line 3459
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 3461
    :cond_0
    iget-object v1, p0, Lmnc;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 3462
    const/4 v1, 0x2

    iget-object v2, p0, Lmnc;->b:Ljava/lang/Float;

    .line 3463
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 3465
    :cond_1
    iget-object v1, p0, Lmnc;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 3466
    const/4 v1, 0x3

    iget-object v2, p0, Lmnc;->c:Ljava/lang/Float;

    .line 3467
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 3469
    :cond_2
    iget-object v1, p0, Lmnc;->d:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 3470
    const/4 v1, 0x4

    iget-object v2, p0, Lmnc;->d:Ljava/lang/Float;

    .line 3471
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 3473
    :cond_3
    iget-object v1, p0, Lmnc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3474
    iput v0, p0, Lmnc;->ai:I

    .line 3475
    return v0
.end method

.method public a(Loxn;)Lmnc;
    .locals 2

    .prologue
    .line 3483
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3484
    sparse-switch v0, :sswitch_data_0

    .line 3488
    iget-object v1, p0, Lmnc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3489
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnc;->ah:Ljava/util/List;

    .line 3492
    :cond_1
    iget-object v1, p0, Lmnc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3494
    :sswitch_0
    return-object p0

    .line 3499
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnc;->a:Ljava/lang/Float;

    goto :goto_0

    .line 3503
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnc;->b:Ljava/lang/Float;

    goto :goto_0

    .line 3507
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnc;->c:Ljava/lang/Float;

    goto :goto_0

    .line 3511
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnc;->d:Ljava/lang/Float;

    goto :goto_0

    .line 3484
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3438
    iget-object v0, p0, Lmnc;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 3439
    const/4 v0, 0x1

    iget-object v1, p0, Lmnc;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 3441
    :cond_0
    iget-object v0, p0, Lmnc;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 3442
    const/4 v0, 0x2

    iget-object v1, p0, Lmnc;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 3444
    :cond_1
    iget-object v0, p0, Lmnc;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 3445
    const/4 v0, 0x3

    iget-object v1, p0, Lmnc;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 3447
    :cond_2
    iget-object v0, p0, Lmnc;->d:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 3448
    const/4 v0, 0x4

    iget-object v1, p0, Lmnc;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 3450
    :cond_3
    iget-object v0, p0, Lmnc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3452
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3421
    invoke-virtual {p0, p1}, Lmnc;->a(Loxn;)Lmnc;

    move-result-object v0

    return-object v0
.end method
