.class final Lfvj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:F

.field private b:F

.field private synthetic c:Lfvi;


# direct methods
.method constructor <init>(Lfvi;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lfvj;->c:Lfvi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 101
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-virtual {v0, p1}, Lfvi;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    move v3, v7

    .line 234
    :cond_0
    :goto_0
    return v3

    .line 105
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v3, v7

    .line 231
    goto :goto_0

    .line 107
    :pswitch_0
    const-string v0, "SwipeToDismissHelper"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; MotionEvent.ACTION_DOWN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    :cond_2
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0, p1}, Lfvi;->a(Lfvi;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0}, Lfvi;->a(Lfvi;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 113
    :cond_3
    const-string v0, "SwipeToDismissHelper"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x30

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; MotionEvent.ACTION_DOWN; couldn\'t obtain lock!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 119
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lfvj;->a:F

    .line 120
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0, v7}, Lfvi;->a(Lfvi;Z)Z

    :cond_5
    :goto_1
    move v3, v7

    .line 234
    goto :goto_0

    .line 125
    :pswitch_1
    const-string v0, "SwipeToDismissHelper"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 126
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; MotionEvent.ACTION_CANCEL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_6
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0, p1}, Lfvi;->b(Lfvi;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0}, Lfvi;->a(Lfvi;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0, p1, v4}, Lfvi;->a(Lfvi;Landroid/view/View;F)V

    .line 133
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0, p1}, Lfvi;->c(Lfvi;Landroid/view/View;)V

    goto :goto_1

    .line 138
    :pswitch_2
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0, p1}, Lfvi;->b(Lfvi;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0}, Lfvi;->a(Lfvi;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 139
    :cond_7
    const-string v0, "SwipeToDismissHelper"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; ACTION_MOVE(): denied"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 144
    :cond_8
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lfvj;->c:Lfvi;

    invoke-static {v1, p1}, Lfvi;->d(Lfvi;Landroid/view/View;)F

    move-result v1

    add-float/2addr v0, v1

    .line 145
    iget v1, p0, Lfvj;->a:F

    sub-float/2addr v0, v1

    .line 146
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 148
    iget-object v2, p0, Lfvj;->c:Lfvi;

    invoke-static {v2}, Lfvi;->b(Lfvi;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lfvj;->c:Lfvi;

    invoke-virtual {v2, v0}, Lfvi;->a(F)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 149
    invoke-static {}, Lfvi;->e()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_a

    .line 150
    iget-object v1, p0, Lfvj;->c:Lfvi;

    invoke-virtual {v1, p1, v7}, Lfvi;->a(Landroid/view/View;Z)V

    .line 151
    iget-object v1, p0, Lfvj;->c:Lfvi;

    iget-object v1, v1, Lfvi;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 152
    invoke-virtual {p1, v7}, Landroid/view/View;->setPressed(Z)V

    .line 154
    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v1

    iput v1, p0, Lfvj;->b:F

    .line 155
    iget-object v1, p0, Lfvj;->c:Lfvi;

    invoke-static {v1, v3}, Lfvi;->a(Lfvi;Z)Z

    .line 157
    iget-object v1, p0, Lfvj;->c:Lfvi;

    invoke-static {v1}, Lfvi;->c(Lfvi;)Lfvq;

    move-result-object v1

    invoke-interface {v1, v3}, Lfvq;->a(Z)V

    .line 164
    :cond_9
    :goto_2
    iget-object v1, p0, Lfvj;->c:Lfvi;

    invoke-static {v1}, Lfvi;->b(Lfvi;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 165
    iget-object v1, p0, Lfvj;->c:Lfvi;

    invoke-static {v1, p1, v0}, Lfvi;->a(Lfvi;Landroid/view/View;F)V

    goto/16 :goto_1

    .line 160
    :cond_a
    iget-object v1, p0, Lfvj;->c:Lfvi;

    iget-object v2, p0, Lfvj;->c:Lfvi;

    iget-object v2, v2, Lfvi;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Lfvi;->a(Landroid/view/ViewGroup;)V

    goto :goto_2

    .line 171
    :pswitch_3
    const-string v0, "SwipeToDismissHelper"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 172
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x17

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; MotionEvent.ACTION_UP"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    :cond_b
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0, p1}, Lfvi;->b(Lfvi;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0}, Lfvi;->a(Lfvi;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0}, Lfvi;->b(Lfvi;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 181
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v2, p0, Lfvj;->c:Lfvi;

    invoke-static {v2, p1}, Lfvi;->d(Lfvi;Landroid/view/View;)F

    move-result v2

    add-float/2addr v0, v2

    .line 182
    iget v2, p0, Lfvj;->a:F

    sub-float v5, v0, v2

    .line 183
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 184
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 185
    cmpl-float v6, v0, v1

    if-lez v6, :cond_c

    move v0, v1

    .line 192
    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x4

    int-to-float v6, v6

    cmpl-float v2, v2, v6

    if-lez v2, :cond_11

    .line 193
    iget v2, p0, Lfvj;->b:F

    .line 195
    iget v6, p0, Lfvj;->b:F

    cmpl-float v2, v2, v6

    if-nez v2, :cond_d

    const/high16 v2, 0x3e800000    # 0.25f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_d

    move v2, v3

    .line 203
    :goto_3
    if-eqz v2, :cond_f

    .line 204
    const/16 v2, 0x15

    invoke-static {p1, v2}, Lhly;->a(Landroid/view/View;I)V

    .line 207
    cmpg-float v2, v5, v4

    if-gez v2, :cond_e

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    :goto_4
    move v6, v3

    move v3, v2

    .line 217
    :goto_5
    sub-float v0, v1, v0

    const/high16 v1, 0x43480000    # 200.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-long v4, v0

    .line 218
    iget-object v1, p0, Lfvj;->c:Lfvi;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lfvi;->a(Landroid/view/View;FJZ)V

    .line 220
    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 222
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0}, Lfvi;->c(Lfvi;)Lfvq;

    move-result-object v0

    invoke-interface {v0, v7}, Lfvq;->a(Z)V

    goto/16 :goto_1

    :cond_d
    move v2, v7

    .line 195
    goto :goto_3

    .line 207
    :cond_e
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    goto :goto_4

    .line 211
    :cond_f
    sub-float v0, v1, v0

    move v6, v7

    move v3, v4

    .line 213
    goto :goto_5

    .line 225
    :cond_10
    iget-object v0, p0, Lfvj;->c:Lfvi;

    invoke-static {v0, p1}, Lfvi;->c(Lfvi;Landroid/view/View;)V

    goto/16 :goto_1

    :cond_11
    move v2, v7

    goto :goto_3

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
