.class public final Lltt;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Lltu;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21
    const/high16 v0, -0x80000000

    iput v0, p0, Lltt;->b:I

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lltt;->c:Lltu;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 50
    iget-object v1, p0, Lltt;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 51
    const/4 v0, 0x1

    iget-object v1, p0, Lltt;->a:Ljava/lang/String;

    .line 52
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 54
    :cond_0
    iget v1, p0, Lltt;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 55
    const/4 v1, 0x2

    iget v2, p0, Lltt;->b:I

    .line 56
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_1
    iget-object v1, p0, Lltt;->c:Lltu;

    if-eqz v1, :cond_2

    .line 59
    const/4 v1, 0x3

    iget-object v2, p0, Lltt;->c:Lltu;

    .line 60
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_2
    iget-object v1, p0, Lltt;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 63
    const/4 v1, 0x4

    iget-object v2, p0, Lltt;->d:Ljava/lang/String;

    .line 64
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_3
    iget-object v1, p0, Lltt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    iput v0, p0, Lltt;->ai:I

    .line 68
    return v0
.end method

.method public a(Loxn;)Lltt;
    .locals 2

    .prologue
    .line 76
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 77
    sparse-switch v0, :sswitch_data_0

    .line 81
    iget-object v1, p0, Lltt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lltt;->ah:Ljava/util/List;

    .line 85
    :cond_1
    iget-object v1, p0, Lltt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    :sswitch_0
    return-object p0

    .line 92
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltt;->a:Ljava/lang/String;

    goto :goto_0

    .line 96
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 97
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 101
    :cond_2
    iput v0, p0, Lltt;->b:I

    goto :goto_0

    .line 103
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lltt;->b:I

    goto :goto_0

    .line 108
    :sswitch_3
    iget-object v0, p0, Lltt;->c:Lltu;

    if-nez v0, :cond_4

    .line 109
    new-instance v0, Lltu;

    invoke-direct {v0}, Lltu;-><init>()V

    iput-object v0, p0, Lltt;->c:Lltu;

    .line 111
    :cond_4
    iget-object v0, p0, Lltt;->c:Lltu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 115
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltt;->d:Ljava/lang/String;

    goto :goto_0

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lltt;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x1

    iget-object v1, p0, Lltt;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 34
    :cond_0
    iget v0, p0, Lltt;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 35
    const/4 v0, 0x2

    iget v1, p0, Lltt;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 37
    :cond_1
    iget-object v0, p0, Lltt;->c:Lltu;

    if-eqz v0, :cond_2

    .line 38
    const/4 v0, 0x3

    iget-object v1, p0, Lltt;->c:Lltu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 40
    :cond_2
    iget-object v0, p0, Lltt;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 41
    const/4 v0, 0x4

    iget-object v1, p0, Lltt;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 43
    :cond_3
    iget-object v0, p0, Lltt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 45
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lltt;->a(Loxn;)Lltt;

    move-result-object v0

    return-object v0
.end method
