.class public final Leyj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Lhjj;
.implements Lhsw;
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private a:I

.field private b:Z

.field private c:Landroid/content/Context;

.field private synthetic d:Lcom/google/android/apps/plus/phone/HomeActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/HomeActivity;Llqr;)V
    .locals 1

    .prologue
    .line 1128
    iput-object p1, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 1130
    iput-object p1, p0, Leyj;->c:Landroid/content/Context;

    .line 1131
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(Lcom/google/android/apps/plus/phone/HomeActivity;)Livx;

    move-result-object v0

    invoke-virtual {v0, p0}, Livx;->b(Lheg;)Livx;

    .line 1132
    return-void
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 1188
    if-eqz p1, :cond_0

    .line 1189
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->i(Lcom/google/android/apps/plus/phone/HomeActivity;)Lhjf;

    move-result-object v0

    invoke-interface {v0, p0}, Lhjf;->c(Lhjj;)V

    .line 1190
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->j(Lcom/google/android/apps/plus/phone/HomeActivity;)Lhsu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lhsu;->a(Lhsw;)Lhsu;

    .line 1195
    :goto_0
    return-void

    .line 1192
    :cond_0
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->i(Lcom/google/android/apps/plus/phone/HomeActivity;)Lhjf;

    move-result-object v0

    invoke-interface {v0, p0}, Lhjf;->d(Lhjj;)V

    .line 1193
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->j(Lcom/google/android/apps/plus/phone/HomeActivity;)Lhsu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lhsu;->b(Lhsw;)Lhsu;

    goto :goto_0
.end method


# virtual methods
.method public V()Z
    .locals 1

    .prologue
    .line 1264
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Leyj;->a(Z)V

    .line 1265
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1137
    if-eqz p1, :cond_0

    .line 1138
    const-string v0, "notification_bar_visible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leyj;->b:Z

    .line 1139
    iget-boolean v0, p0, Leyj;->b:Z

    invoke-direct {p0, v0}, Leyj;->b(Z)V

    .line 1141
    :cond_0
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 1211
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 1212
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 1213
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 1214
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->k(Lcom/google/android/apps/plus/phone/HomeActivity;)Llnh;

    move-result-object v0

    const-class v1, Lieh;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 1215
    sget-object v1, Ldxd;->i:Lief;

    iget-object v2, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    .line 1216
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(Lcom/google/android/apps/plus/phone/HomeActivity;)Livx;

    move-result-object v2

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    .line 1215
    invoke-interface {v0, v1, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1217
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->g(Lcom/google/android/apps/plus/phone/HomeActivity;)Ljit;

    move-result-object v0

    invoke-interface {v0}, Ljit;->U()I

    move-result v0

    if-lez v0, :cond_0

    .line 1218
    const v0, 0x7f100691

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1222
    :cond_0
    const v0, 0x7f100692

    .line 1224
    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lie;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/actionbar/NotificationButtonView;

    .line 1226
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/actionbar/NotificationButtonView;->a(I)V

    .line 1228
    const v0, 0x7f10067b

    .line 1229
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 1230
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 1232
    iget-object v1, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->g(Lcom/google/android/apps/plus/phone/HomeActivity;)Ljit;

    move-result-object v1

    invoke-interface {v1}, Ljit;->ab()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1233
    invoke-virtual {v0}, Lhjv;->b()V

    .line 1235
    :cond_1
    return-void
.end method

.method public a(Loo;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1199
    invoke-virtual {p1}, Loo;->b()I

    move-result v0

    iput v0, p0, Leyj;->a:I

    .line 1200
    invoke-virtual {p1, v1}, Loo;->d(Z)V

    .line 1201
    invoke-virtual {p1, v1}, Loo;->e(Z)V

    .line 1202
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 1162
    iget-boolean v0, p0, Leyj;->b:Z

    if-ne p1, v0, :cond_0

    .line 1185
    :goto_0
    return-void

    .line 1166
    :cond_0
    iput-boolean p1, p0, Leyj;->b:Z

    .line 1168
    if-eqz p1, :cond_1

    .line 1170
    iget-object v0, p0, Leyj;->c:Landroid/content/Context;

    iget-object v1, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(Lcom/google/android/apps/plus/phone/HomeActivity;)Livx;

    move-result-object v1

    invoke-virtual {v1}, Livx;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;I)Ljava/lang/Integer;

    .line 1171
    iget-object v0, p0, Leyj;->c:Landroid/content/Context;

    iget-object v1, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(Lcom/google/android/apps/plus/phone/HomeActivity;)Livx;

    move-result-object v1

    invoke-virtual {v1}, Livx;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;I)Ljava/lang/Integer;

    .line 1172
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->f(Lcom/google/android/apps/plus/phone/HomeActivity;)Lcom/google/android/apps/plus/views/EsDrawerLayout;

    move-result-object v0

    iget-object v1, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->e(Lcom/google/android/apps/plus/phone/HomeActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->h(Landroid/view/View;)V

    .line 1179
    :goto_1
    invoke-direct {p0, p1}, Leyj;->b(Z)V

    .line 1180
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->g(Lcom/google/android/apps/plus/phone/HomeActivity;)Ljit;

    move-result-object v0

    invoke-interface {v0, p1}, Ljit;->b(Z)V

    .line 1181
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->h(Lcom/google/android/apps/plus/phone/HomeActivity;)Llnh;

    move-result-object v0

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v1, p0, Leyj;->c:Landroid/content/Context;

    iget-object v3, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    .line 1182
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(Lcom/google/android/apps/plus/phone/HomeActivity;)Livx;

    move-result-object v3

    invoke-virtual {v3}, Livx;->d()I

    move-result v3

    invoke-direct {v2, v1, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    if-eqz p1, :cond_2

    sget-object v1, Lhmv;->bu:Lhmv;

    .line 1183
    :goto_2
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1181
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0

    .line 1174
    :cond_1
    iget-object v0, p0, Leyj;->c:Landroid/content/Context;

    iget-object v1, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(Lcom/google/android/apps/plus/phone/HomeActivity;)Livx;

    move-result-object v1

    invoke-virtual {v1}, Livx;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;I)Ljava/lang/Integer;

    .line 1175
    iget-object v0, p0, Leyj;->c:Landroid/content/Context;

    iget-object v1, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(Lcom/google/android/apps/plus/phone/HomeActivity;)Livx;

    move-result-object v1

    invoke-virtual {v1}, Livx;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;I)Ljava/lang/Integer;

    .line 1176
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->f(Lcom/google/android/apps/plus/phone/HomeActivity;)Lcom/google/android/apps/plus/views/EsDrawerLayout;

    move-result-object v0

    iget-object v1, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->e(Lcom/google/android/apps/plus/phone/HomeActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsDrawerLayout;->i(Landroid/view/View;)V

    goto :goto_1

    .line 1182
    :cond_2
    sget-object v1, Lhmv;->bv:Lhmv;

    goto :goto_2
.end method

.method public a(ZIIII)V
    .locals 3

    .prologue
    .line 1152
    const/4 v0, 0x3

    if-ne p3, v0, :cond_0

    .line 1153
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1154
    const-string v1, "com.google.android.libraries.social.notifications.show_notifications"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1155
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Leyj;->a(Z)V

    .line 1156
    const-string v1, "com.google.android.libraries.social.notifications.show_notifications"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1159
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1239
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 1240
    iget-object v3, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/HomeActivity;->b(Lcom/google/android/apps/plus/phone/HomeActivity;)Livx;

    move-result-object v3

    invoke-virtual {v3}, Livx;->d()I

    move-result v3

    .line 1242
    const v4, 0x7f10067b

    if-ne v2, v4, :cond_1

    .line 1243
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->g(Lcom/google/android/apps/plus/phone/HomeActivity;)Ljit;

    move-result-object v0

    invoke-interface {v0}, Ljit;->e()V

    move v0, v1

    .line 1259
    :cond_0
    :goto_0
    return v0

    .line 1245
    :cond_1
    const v4, 0x7f100692

    if-ne v2, v4, :cond_2

    .line 1246
    invoke-virtual {p0, v0}, Leyj;->a(Z)V

    move v0, v1

    .line 1247
    goto :goto_0

    .line 1248
    :cond_2
    const v4, 0x7f100691

    if-ne v2, v4, :cond_0

    .line 1249
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->g(Lcom/google/android/apps/plus/phone/HomeActivity;)Ljit;

    move-result-object v0

    invoke-interface {v0}, Ljit;->V()V

    .line 1251
    iget-object v0, p0, Leyj;->d:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->l(Lcom/google/android/apps/plus/phone/HomeActivity;)Llnh;

    move-result-object v0

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v4, p0, Leyj;->c:Landroid/content/Context;

    invoke-direct {v2, v4, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->bJ:Lhmv;

    .line 1253
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    sget-object v3, Lhmw;->w:Lhmw;

    .line 1254
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v2

    .line 1251
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    move v0, v1

    .line 1256
    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1145
    const-string v0, "notification_bar_visible"

    iget-boolean v1, p0, Leyj;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1146
    return-void
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 1206
    iget v0, p0, Leyj;->a:I

    invoke-virtual {p1, v0}, Loo;->d(I)V

    .line 1207
    return-void
.end method
