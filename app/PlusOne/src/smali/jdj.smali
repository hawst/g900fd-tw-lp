.class final Ljdj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lorg/chromium/net/HttpUrlRequestListener;


# instance fields
.field private synthetic a:Ljdi;


# direct methods
.method constructor <init>(Ljdi;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Ljdj;->a:Ljdi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 4

    .prologue
    .line 38
    :try_start_0
    iget-object v0, p0, Ljdj;->a:Ljdi;

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->b()I

    move-result v1

    iput v1, v0, Ljdi;->b:I

    .line 39
    iget-object v0, p0, Ljdj;->a:Ljdi;

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->a()J

    move-result-wide v2

    iput-wide v2, v0, Ljdi;->c:J

    .line 40
    iget-object v0, p0, Ljdj;->a:Ljdi;

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->e()[B

    move-result-object v1

    iput-object v1, v0, Ljdi;->d:[B

    .line 41
    iget-object v0, p0, Ljdj;->a:Ljdi;

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->c()Ljava/io/IOException;

    move-result-object v1

    iput-object v1, v0, Ljdi;->e:Ljava/io/IOException;

    .line 42
    iget-object v0, p0, Ljdj;->a:Ljdi;

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->i()Z

    move-result v1

    iput-boolean v1, v0, Ljdi;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    iget-object v0, p0, Ljdj;->a:Ljdi;

    iget-object v0, v0, Ljdi;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 45
    return-void

    .line 44
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ljdj;->a:Ljdi;

    iget-object v1, v1, Ljdi;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    throw v0
.end method

.method public onResponseStarted(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Ljdj;->a:Ljdi;

    invoke-virtual {v0, p1}, Ljdi;->a(Lorg/chromium/net/HttpUrlRequest;)V

    .line 33
    return-void
.end method
