.class public final Lmpd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmpd;


# instance fields
.field public b:Lmpb;

.field private c:Lmor;

.field private d:Ljava/lang/Integer;

.field private e:[Lmpc;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    new-array v0, v0, [Lmpd;

    sput-object v0, Lmpd;->a:[Lmpd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 106
    invoke-direct {p0}, Loxq;-><init>()V

    .line 109
    iput-object v0, p0, Lmpd;->b:Lmpb;

    .line 112
    iput-object v0, p0, Lmpd;->c:Lmor;

    .line 117
    sget-object v0, Lmpc;->a:[Lmpc;

    iput-object v0, p0, Lmpd;->e:[Lmpc;

    .line 106
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 174
    .line 175
    iget-object v0, p0, Lmpd;->b:Lmpb;

    if-eqz v0, :cond_a

    .line 176
    const/4 v0, 0x1

    iget-object v2, p0, Lmpd;->b:Lmpb;

    .line 177
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 179
    :goto_0
    iget-object v2, p0, Lmpd;->c:Lmor;

    if-eqz v2, :cond_0

    .line 180
    const/4 v2, 0x2

    iget-object v3, p0, Lmpd;->c:Lmor;

    .line 181
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 183
    :cond_0
    iget-object v2, p0, Lmpd;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 184
    const/4 v2, 0x3

    iget-object v3, p0, Lmpd;->d:Ljava/lang/Integer;

    .line 185
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 187
    :cond_1
    iget-object v2, p0, Lmpd;->e:[Lmpc;

    if-eqz v2, :cond_3

    .line 188
    iget-object v2, p0, Lmpd;->e:[Lmpc;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 189
    if-eqz v4, :cond_2

    .line 190
    const/4 v5, 0x4

    .line 191
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 188
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 195
    :cond_3
    iget-object v1, p0, Lmpd;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 196
    const/4 v1, 0x5

    iget-object v2, p0, Lmpd;->f:Ljava/lang/Boolean;

    .line 197
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 199
    :cond_4
    iget-object v1, p0, Lmpd;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 200
    const/4 v1, 0x6

    iget-object v2, p0, Lmpd;->g:Ljava/lang/String;

    .line 201
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_5
    iget-object v1, p0, Lmpd;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 204
    const/4 v1, 0x7

    iget-object v2, p0, Lmpd;->h:Ljava/lang/String;

    .line 205
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_6
    iget-object v1, p0, Lmpd;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 208
    const/16 v1, 0x8

    iget-object v2, p0, Lmpd;->i:Ljava/lang/Boolean;

    .line 209
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 211
    :cond_7
    iget-object v1, p0, Lmpd;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 212
    const/16 v1, 0x9

    iget-object v2, p0, Lmpd;->j:Ljava/lang/Boolean;

    .line 213
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 215
    :cond_8
    iget-object v1, p0, Lmpd;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 216
    const/16 v1, 0xa

    iget-object v2, p0, Lmpd;->k:Ljava/lang/Boolean;

    .line 217
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 219
    :cond_9
    iget-object v1, p0, Lmpd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    iput v0, p0, Lmpd;->ai:I

    .line 221
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmpd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 229
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 230
    sparse-switch v0, :sswitch_data_0

    .line 234
    iget-object v2, p0, Lmpd;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 235
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmpd;->ah:Ljava/util/List;

    .line 238
    :cond_1
    iget-object v2, p0, Lmpd;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    :sswitch_0
    return-object p0

    .line 245
    :sswitch_1
    iget-object v0, p0, Lmpd;->b:Lmpb;

    if-nez v0, :cond_2

    .line 246
    new-instance v0, Lmpb;

    invoke-direct {v0}, Lmpb;-><init>()V

    iput-object v0, p0, Lmpd;->b:Lmpb;

    .line 248
    :cond_2
    iget-object v0, p0, Lmpd;->b:Lmpb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 252
    :sswitch_2
    iget-object v0, p0, Lmpd;->c:Lmor;

    if-nez v0, :cond_3

    .line 253
    new-instance v0, Lmor;

    invoke-direct {v0}, Lmor;-><init>()V

    iput-object v0, p0, Lmpd;->c:Lmor;

    .line 255
    :cond_3
    iget-object v0, p0, Lmpd;->c:Lmor;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 259
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmpd;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 263
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 264
    iget-object v0, p0, Lmpd;->e:[Lmpc;

    if-nez v0, :cond_5

    move v0, v1

    .line 265
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmpc;

    .line 266
    iget-object v3, p0, Lmpd;->e:[Lmpc;

    if-eqz v3, :cond_4

    .line 267
    iget-object v3, p0, Lmpd;->e:[Lmpc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 269
    :cond_4
    iput-object v2, p0, Lmpd;->e:[Lmpc;

    .line 270
    :goto_2
    iget-object v2, p0, Lmpd;->e:[Lmpc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 271
    iget-object v2, p0, Lmpd;->e:[Lmpc;

    new-instance v3, Lmpc;

    invoke-direct {v3}, Lmpc;-><init>()V

    aput-object v3, v2, v0

    .line 272
    iget-object v2, p0, Lmpd;->e:[Lmpc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 273
    invoke-virtual {p1}, Loxn;->a()I

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 264
    :cond_5
    iget-object v0, p0, Lmpd;->e:[Lmpc;

    array-length v0, v0

    goto :goto_1

    .line 276
    :cond_6
    iget-object v2, p0, Lmpd;->e:[Lmpc;

    new-instance v3, Lmpc;

    invoke-direct {v3}, Lmpc;-><init>()V

    aput-object v3, v2, v0

    .line 277
    iget-object v2, p0, Lmpd;->e:[Lmpc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 281
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpd;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 285
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpd;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 289
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpd;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 293
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpd;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 297
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpd;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 301
    :sswitch_a
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpd;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 230
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 134
    iget-object v0, p0, Lmpd;->b:Lmpb;

    if-eqz v0, :cond_0

    .line 135
    const/4 v0, 0x1

    iget-object v1, p0, Lmpd;->b:Lmpb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 137
    :cond_0
    iget-object v0, p0, Lmpd;->c:Lmor;

    if-eqz v0, :cond_1

    .line 138
    const/4 v0, 0x2

    iget-object v1, p0, Lmpd;->c:Lmor;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 140
    :cond_1
    iget-object v0, p0, Lmpd;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 141
    const/4 v0, 0x3

    iget-object v1, p0, Lmpd;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 143
    :cond_2
    iget-object v0, p0, Lmpd;->e:[Lmpc;

    if-eqz v0, :cond_4

    .line 144
    iget-object v1, p0, Lmpd;->e:[Lmpc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 145
    if-eqz v3, :cond_3

    .line 146
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 144
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_4
    iget-object v0, p0, Lmpd;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 151
    const/4 v0, 0x5

    iget-object v1, p0, Lmpd;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 153
    :cond_5
    iget-object v0, p0, Lmpd;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 154
    const/4 v0, 0x6

    iget-object v1, p0, Lmpd;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 156
    :cond_6
    iget-object v0, p0, Lmpd;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 157
    const/4 v0, 0x7

    iget-object v1, p0, Lmpd;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 159
    :cond_7
    iget-object v0, p0, Lmpd;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 160
    const/16 v0, 0x8

    iget-object v1, p0, Lmpd;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 162
    :cond_8
    iget-object v0, p0, Lmpd;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 163
    const/16 v0, 0x9

    iget-object v1, p0, Lmpd;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 165
    :cond_9
    iget-object v0, p0, Lmpd;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 166
    const/16 v0, 0xa

    iget-object v1, p0, Lmpd;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 168
    :cond_a
    iget-object v0, p0, Lmpd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 170
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0, p1}, Lmpd;->a(Loxn;)Lmpd;

    move-result-object v0

    return-object v0
.end method
