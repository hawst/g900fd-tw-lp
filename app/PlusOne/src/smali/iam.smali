.class public final Liam;
.super Lkgn;
.source "PG"

# interfaces
.implements Lkig;


# instance fields
.field private final Q:Lkif;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Lkgn;-><init>()V

    .line 17
    new-instance v0, Lkif;

    iget-object v1, p0, Liam;->P:Llqm;

    invoke-direct {v0, p0, v1}, Lkif;-><init>(Lkgn;Llqr;)V

    iput-object v0, p0, Liam;->Q:Lkif;

    .line 47
    return-void
.end method


# virtual methods
.method protected U()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Liam;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_network_requests_intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method protected V()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Liam;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_network_stats_intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method protected W()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Liam;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_upload_stats_intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method protected X()Z
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Liam;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_show_apiary_pref"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected Y()Z
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Liam;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_show_override_pref"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected Z()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Liam;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_show_tracing_pref"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Liam;->c()Lian;

    move-result-object v0

    .line 58
    invoke-virtual {p0}, Liam;->d()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lian;->b(Landroid/content/Intent;)V

    .line 59
    invoke-virtual {p0}, Liam;->e()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lian;->c(Landroid/content/Intent;)V

    .line 60
    invoke-virtual {p0}, Liam;->U()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lian;->d(Landroid/content/Intent;)V

    .line 61
    invoke-virtual {p0}, Liam;->V()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lian;->e(Landroid/content/Intent;)V

    .line 62
    invoke-virtual {p0}, Liam;->W()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lian;->f(Landroid/content/Intent;)V

    .line 63
    invoke-virtual {p0}, Liam;->X()Z

    move-result v1

    invoke-virtual {v0, v1}, Lian;->a(Z)V

    .line 64
    invoke-virtual {p0}, Liam;->Y()Z

    move-result v1

    invoke-virtual {v0, v1}, Lian;->b(Z)V

    .line 65
    invoke-virtual {p0}, Liam;->Z()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lian;->g(Landroid/content/Intent;)V

    .line 66
    iget-object v1, p0, Liam;->Q:Lkif;

    invoke-virtual {v1, v0}, Lkif;->a(Lu;)V

    .line 67
    return-void
.end method

.method protected c()Lian;
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lian;

    invoke-direct {v0}, Lian;-><init>()V

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 51
    invoke-super {p0, p1}, Lkgn;->c(Landroid/os/Bundle;)V

    .line 52
    iget-object v0, p0, Liam;->O:Llnh;

    const-class v1, Lkij;

    iget-object v2, p0, Liam;->Q:Lkif;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 53
    return-void
.end method

.method protected d()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Liam;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_account_status_intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method protected e()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Liam;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_browse_experiments_intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method
