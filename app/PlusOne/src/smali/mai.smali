.class public final Lmai;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnsz;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15307
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15310
    iput-object v0, p0, Lmai;->apiHeader:Llyq;

    .line 15313
    iput-object v0, p0, Lmai;->a:Lnsz;

    .line 15307
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 15330
    const/4 v0, 0x0

    .line 15331
    iget-object v1, p0, Lmai;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 15332
    const/4 v0, 0x1

    iget-object v1, p0, Lmai;->apiHeader:Llyq;

    .line 15333
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15335
    :cond_0
    iget-object v1, p0, Lmai;->a:Lnsz;

    if-eqz v1, :cond_1

    .line 15336
    const/4 v1, 0x2

    iget-object v2, p0, Lmai;->a:Lnsz;

    .line 15337
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15339
    :cond_1
    iget-object v1, p0, Lmai;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15340
    iput v0, p0, Lmai;->ai:I

    .line 15341
    return v0
.end method

.method public a(Loxn;)Lmai;
    .locals 2

    .prologue
    .line 15349
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 15350
    sparse-switch v0, :sswitch_data_0

    .line 15354
    iget-object v1, p0, Lmai;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 15355
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmai;->ah:Ljava/util/List;

    .line 15358
    :cond_1
    iget-object v1, p0, Lmai;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15360
    :sswitch_0
    return-object p0

    .line 15365
    :sswitch_1
    iget-object v0, p0, Lmai;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 15366
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmai;->apiHeader:Llyq;

    .line 15368
    :cond_2
    iget-object v0, p0, Lmai;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15372
    :sswitch_2
    iget-object v0, p0, Lmai;->a:Lnsz;

    if-nez v0, :cond_3

    .line 15373
    new-instance v0, Lnsz;

    invoke-direct {v0}, Lnsz;-><init>()V

    iput-object v0, p0, Lmai;->a:Lnsz;

    .line 15375
    :cond_3
    iget-object v0, p0, Lmai;->a:Lnsz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15350
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 15318
    iget-object v0, p0, Lmai;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 15319
    const/4 v0, 0x1

    iget-object v1, p0, Lmai;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15321
    :cond_0
    iget-object v0, p0, Lmai;->a:Lnsz;

    if-eqz v0, :cond_1

    .line 15322
    const/4 v0, 0x2

    iget-object v1, p0, Lmai;->a:Lnsz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15324
    :cond_1
    iget-object v0, p0, Lmai;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 15326
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 15303
    invoke-virtual {p0, p1}, Lmai;->a(Loxn;)Lmai;

    move-result-object v0

    return-object v0
.end method
