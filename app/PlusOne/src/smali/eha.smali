.class public final Leha;
.super Lehh;
.source "PG"


# instance fields
.field private N:Llai;

.field private O:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lehh;-><init>()V

    return-void
.end method


# virtual methods
.method protected U()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method protected V()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public X()V
    .locals 3

    .prologue
    .line 205
    invoke-virtual {p0}, Leha;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 206
    return-void
.end method

.method protected a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;
    .locals 9

    .prologue
    .line 98
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v8, p8

    invoke-super/range {v0 .. v8}, Lehh;->a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-super {p0, p1}, Lehh;->a(Landroid/os/Bundle;)V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Leha;->ai:Z

    .line 61
    invoke-virtual {p0}, Leha;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "relateds"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Llai;

    iput-object v0, p0, Leha;->N:Llai;

    .line 62
    if-eqz p1, :cond_0

    .line 63
    const-string v0, "current_tab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Leha;->O:I

    .line 64
    invoke-virtual {p0}, Leha;->af_()V

    .line 65
    invoke-virtual {p0}, Leha;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 71
    :goto_0
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Leha;->O:I

    .line 68
    invoke-virtual {p0}, Leha;->af_()V

    .line 69
    invoke-virtual {p0}, Leha;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 110
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 156
    :goto_0
    return-void

    .line 112
    :pswitch_0
    invoke-super {p0, p1, p2}, Lehh;->a(Ldo;Landroid/database/Cursor;)V

    goto :goto_0

    .line 117
    :pswitch_1
    iget-object v0, p0, Leha;->W:Lfdj;

    invoke-virtual {v0, v2}, Lfdj;->c(Z)V

    .line 118
    iget-object v0, p0, Leha;->W:Lfdj;

    const/4 v1, -0x1

    invoke-virtual {v0, p2, v1}, Lfdj;->b(Landroid/database/Cursor;I)V

    .line 119
    invoke-virtual {p0}, Leha;->aA()V

    .line 121
    iput-boolean v2, p0, Leha;->aq:Z

    .line 122
    iput-boolean v2, p0, Leha;->ar:Z

    .line 124
    invoke-virtual {p0}, Leha;->x()Landroid/view/View;

    .line 125
    iget-boolean v0, p0, Leha;->ak:Z

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Leha;->U:Licq;

    const v1, 0x7f0a058f

    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    .line 127
    iget-object v0, p0, Leha;->U:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 153
    :goto_1
    invoke-virtual {p0}, Leha;->ay()V

    goto :goto_0

    .line 129
    :cond_0
    iget-object v0, p0, Leha;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 130
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 131
    iget-object v0, p0, Leha;->U:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 132
    iget-object v0, p0, Leha;->Y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Leha;->aq:Z

    .line 150
    :cond_1
    :goto_2
    iput-boolean v2, p0, Leha;->ai:Z

    goto :goto_1

    .line 133
    :cond_2
    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "fetch_older"

    .line 134
    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135
    :cond_3
    invoke-virtual {p0}, Leha;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Leha;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 138
    :cond_4
    iget-object v0, p0, Leha;->N:Llai;

    if-eqz v0, :cond_6

    iget-object v0, p0, Leha;->N:Llai;

    .line 139
    invoke-virtual {v0}, Llai;->a()I

    move-result v0

    iget v1, p0, Leha;->O:I

    if-le v0, v1, :cond_6

    iget-object v0, p0, Leha;->N:Llai;

    iget v1, p0, Leha;->O:I

    .line 140
    invoke-virtual {v0, v1}, Llai;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 141
    iget-boolean v0, p0, Leha;->ai:Z

    if-eqz v0, :cond_5

    .line 142
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Leha;->a(Z)V

    goto :goto_2

    .line 143
    :cond_5
    invoke-virtual {p0}, Leha;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Leha;->U:Licq;

    const v1, 0x7f0a06f6

    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    .line 145
    iget-object v0, p0, Leha;->U:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 148
    :cond_6
    iget-object v0, p0, Leha;->U:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 110
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Leha;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 47
    const-string v0, "fetch_newer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fetch_older"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    :cond_0
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    iput-boolean v0, p0, Leha;->ak:Z

    .line 49
    invoke-virtual {p0}, Leha;->aD()V

    .line 50
    invoke-virtual {p0}, Leha;->X()V

    .line 51
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lhos;->a(Z)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lehh;->a(Ljava/lang/String;Lhoz;Lhos;)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 7

    .prologue
    .line 180
    iget-object v0, p0, Leha;->N:Llai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leha;->N:Llai;

    .line 181
    invoke-virtual {v0}, Llai;->a()I

    move-result v0

    iget v1, p0, Leha;->O:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Leha;->N:Llai;

    iget v1, p0, Leha;->O:I

    .line 182
    invoke-virtual {v0, v1}, Llai;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v0, p0, Leha;->N:Llai;

    iget v1, p0, Leha;->O:I

    invoke-virtual {v0, v1}, Llai;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 187
    invoke-virtual {p0}, Leha;->n()Lz;

    move-result-object v1

    .line 189
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Leha;->az()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    iget-object v0, p0, Leha;->U:Licq;

    sget-object v2, Lict;->a:Lict;

    invoke-virtual {v0, v2}, Licq;->a(Lict;)V

    .line 193
    :cond_2
    new-instance v0, Ldqc;

    iget-object v2, p0, Leha;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v6, p1

    invoke-direct/range {v0 .. v6}, Ldqc;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)V

    .line 195
    if-eqz p1, :cond_3

    const-string v1, "fetch_newer"

    :goto_1
    invoke-virtual {v0, v1}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 196
    iget-object v1, p0, Leha;->au:Llnh;

    const-class v2, Lhoc;

    invoke-virtual {v1, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 198
    invoke-virtual {p0}, Leha;->ay()V

    goto :goto_0

    .line 195
    :cond_3
    const-string v1, "fetch_older"

    goto :goto_1
.end method

.method protected ad_()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method protected af_()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Leha;->N:Llai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leha;->N:Llai;

    .line 211
    invoke-virtual {v0}, Llai;->a()I

    move-result v0

    iget v1, p0, Leha;->O:I

    if-gt v0, v1, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    iget-object v0, p0, Leha;->N:Llai;

    iget v1, p0, Leha;->O:I

    invoke-virtual {v0, v1}, Llai;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 217
    const-string v0, "com.google.android.apps.plus.INVALID_SEARCH_QUERY"

    iput-object v0, p0, Leha;->ag:Ljava/lang/String;

    goto :goto_0

    .line 219
    :cond_2
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lfvd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leha;->ag:Ljava/lang/String;

    goto :goto_0
.end method

.method protected ag_()V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method protected ai_()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x1

    return v0
.end method

.method public c(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 164
    iget v0, p0, Leha;->O:I

    if-eq v0, p1, :cond_0

    .line 165
    iput p1, p0, Leha;->O:I

    .line 166
    iput-boolean v3, p0, Leha;->ai:Z

    invoke-virtual {p0}, Leha;->af_()V

    invoke-virtual {p0}, Leha;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    invoke-virtual {p0, v3}, Leha;->a(Z)V

    .line 168
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0, p1}, Lehh;->e(Landroid/os/Bundle;)V

    .line 76
    const-string v0, "current_tab"

    iget v1, p0, Leha;->O:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    return-void
.end method
