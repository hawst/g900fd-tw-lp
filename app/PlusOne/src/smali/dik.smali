.class public final Ldik;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Llyu;",
        "Llyv;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Ldil;


# instance fields
.field private final b:Ldvt;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Ldil;

    invoke-direct {v0}, Ldil;-><init>()V

    sput-object v0, Ldik;->a:Ldil;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;ILdvt;)V
    .locals 6

    .prologue
    .line 46
    const-string v3, "blockuser"

    new-instance v4, Llyu;

    invoke-direct {v4}, Llyu;-><init>()V

    new-instance v5, Llyv;

    invoke-direct {v5}, Llyv;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 48
    iput-object p3, p0, Ldik;->b:Ldvt;

    .line 49
    return-void
.end method

.method public static a()Ldil;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Ldik;->a:Ldil;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Ldik;->p:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Ldik;->r:Ljava/lang/String;

    .line 61
    iput-boolean p3, p0, Ldik;->q:Z

    .line 62
    return-void
.end method

.method protected a(Llyu;)V
    .locals 4

    .prologue
    .line 66
    new-instance v0, Loih;

    invoke-direct {v0}, Loih;-><init>()V

    .line 67
    iget-object v1, p0, Ldik;->r:Ljava/lang/String;

    iput-object v1, v0, Loih;->c:Ljava/lang/String;

    .line 68
    iget-object v1, p0, Ldik;->p:Ljava/lang/String;

    invoke-static {v1}, Ldsm;->c(Ljava/lang/String;)Lohp;

    move-result-object v1

    iput-object v1, v0, Loih;->b:Lohp;

    .line 69
    new-instance v1, Loii;

    invoke-direct {v1}, Loii;-><init>()V

    .line 70
    const/4 v2, 0x1

    new-array v2, v2, [Loih;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iput-object v2, v1, Loii;->a:[Loih;

    .line 71
    new-instance v0, Loje;

    invoke-direct {v0}, Loje;-><init>()V

    iput-object v0, p1, Llyu;->a:Loje;

    .line 72
    iget-object v0, p1, Llyu;->a:Loje;

    .line 73
    iput-object v1, v0, Loje;->a:Loii;

    .line 74
    iget-object v0, v0, Loje;->a:Loii;

    iget-boolean v1, p0, Ldik;->q:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Loii;->b:Ljava/lang/Boolean;

    .line 75
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Llyu;

    invoke-virtual {p0, p1}, Ldik;->a(Llyu;)V

    return-void
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Ldik;->b:Ldvt;

    iget-object v1, p0, Ldik;->p:Ljava/lang/String;

    iget-object v2, p0, Ldik;->r:Ljava/lang/String;

    iget-boolean v3, p0, Ldik;->q:Z

    invoke-virtual {v0, v1, v2, v3}, Ldvt;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 80
    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0}, Ldik;->b()V

    return-void
.end method
