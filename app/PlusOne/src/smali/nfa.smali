.class public final Lnfa;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/Long;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lnfa;->a:[Ljava/lang/Long;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 34
    .line 35
    iget-object v1, p0, Lnfa;->a:[Ljava/lang/Long;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnfa;->a:[Ljava/lang/Long;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 37
    iget-object v2, p0, Lnfa;->a:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 39
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Loxo;->f(J)I

    move-result v4

    add-int/2addr v1, v4

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_0
    iget-object v0, p0, Lnfa;->a:[Ljava/lang/Long;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 44
    :cond_1
    iget-object v1, p0, Lnfa;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 45
    const/4 v1, 0x2

    iget-object v2, p0, Lnfa;->b:Ljava/lang/String;

    .line 46
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_2
    iget-object v1, p0, Lnfa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    iput v0, p0, Lnfa;->ai:I

    .line 50
    return v0
.end method

.method public a(Loxn;)Lnfa;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 58
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 59
    sparse-switch v0, :sswitch_data_0

    .line 63
    iget-object v1, p0, Lnfa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnfa;->ah:Ljava/util/List;

    .line 67
    :cond_1
    iget-object v1, p0, Lnfa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    :sswitch_0
    return-object p0

    .line 74
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 75
    iget-object v0, p0, Lnfa;->a:[Ljava/lang/Long;

    array-length v0, v0

    .line 76
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    .line 77
    iget-object v2, p0, Lnfa;->a:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    iput-object v1, p0, Lnfa;->a:[Ljava/lang/Long;

    .line 79
    :goto_1
    iget-object v1, p0, Lnfa;->a:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 80
    iget-object v1, p0, Lnfa;->a:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 81
    invoke-virtual {p1}, Loxn;->a()I

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 84
    :cond_2
    iget-object v1, p0, Lnfa;->a:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 88
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfa;->b:Ljava/lang/String;

    goto :goto_0

    .line 59
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    .line 20
    iget-object v0, p0, Lnfa;->a:[Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 21
    iget-object v1, p0, Lnfa;->a:[Ljava/lang/Long;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 22
    const/4 v4, 0x1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v4, v6, v7}, Loxo;->b(IJ)V

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25
    :cond_0
    iget-object v0, p0, Lnfa;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 26
    const/4 v0, 0x2

    iget-object v1, p0, Lnfa;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 28
    :cond_1
    iget-object v0, p0, Lnfa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 30
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnfa;->a(Loxn;)Lnfa;

    move-result-object v0

    return-object v0
.end method
