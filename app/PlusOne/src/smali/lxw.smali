.class public final Llxw;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxw;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3792
    const/4 v0, 0x0

    new-array v0, v0, [Llxw;

    sput-object v0, Llxw;->a:[Llxw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3793
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3796
    const/4 v0, 0x0

    iput-object v0, p0, Llxw;->b:Llxp;

    .line 3793
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3820
    const/4 v0, 0x0

    .line 3821
    iget-object v1, p0, Llxw;->b:Llxp;

    if-eqz v1, :cond_0

    .line 3822
    const/4 v0, 0x1

    iget-object v1, p0, Llxw;->b:Llxp;

    .line 3823
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3825
    :cond_0
    iget-object v1, p0, Llxw;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3826
    const/4 v1, 0x2

    iget-object v2, p0, Llxw;->c:Ljava/lang/String;

    .line 3827
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3829
    :cond_1
    iget-object v1, p0, Llxw;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3830
    const/4 v1, 0x3

    iget-object v2, p0, Llxw;->d:Ljava/lang/String;

    .line 3831
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3833
    :cond_2
    iget-object v1, p0, Llxw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3834
    iput v0, p0, Llxw;->ai:I

    .line 3835
    return v0
.end method

.method public a(Loxn;)Llxw;
    .locals 2

    .prologue
    .line 3843
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3844
    sparse-switch v0, :sswitch_data_0

    .line 3848
    iget-object v1, p0, Llxw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3849
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxw;->ah:Ljava/util/List;

    .line 3852
    :cond_1
    iget-object v1, p0, Llxw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3854
    :sswitch_0
    return-object p0

    .line 3859
    :sswitch_1
    iget-object v0, p0, Llxw;->b:Llxp;

    if-nez v0, :cond_2

    .line 3860
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxw;->b:Llxp;

    .line 3862
    :cond_2
    iget-object v0, p0, Llxw;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3866
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxw;->c:Ljava/lang/String;

    goto :goto_0

    .line 3870
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxw;->d:Ljava/lang/String;

    goto :goto_0

    .line 3844
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3805
    iget-object v0, p0, Llxw;->b:Llxp;

    if-eqz v0, :cond_0

    .line 3806
    const/4 v0, 0x1

    iget-object v1, p0, Llxw;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3808
    :cond_0
    iget-object v0, p0, Llxw;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3809
    const/4 v0, 0x2

    iget-object v1, p0, Llxw;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3811
    :cond_1
    iget-object v0, p0, Llxw;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3812
    const/4 v0, 0x3

    iget-object v1, p0, Llxw;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3814
    :cond_2
    iget-object v0, p0, Llxw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3816
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3789
    invoke-virtual {p0, p1}, Llxw;->a(Loxn;)Llxw;

    move-result-object v0

    return-object v0
.end method
