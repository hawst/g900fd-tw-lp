.class public final Livz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Liwi;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Lhee;

.field private d:Liwc;


# direct methods
.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Livz;-><init>(Llqr;B)V

    .line 32
    return-void
.end method

.method constructor <init>(Llqr;B)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Livz;->a:Ljava/util/List;

    .line 35
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 36
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Livz;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Liwi;",
            ">;)",
            "Livz;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Livz;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call this method before onAttachBinder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    iget-object v0, p0, Livz;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-object p0
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 57
    iput-object p1, p0, Livz;->b:Landroid/content/Context;

    .line 58
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Livz;->c:Lhee;

    .line 59
    const-class v0, Liwc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwc;

    iput-object v0, p0, Livz;->d:Liwc;

    .line 60
    iget-object v0, p0, Livz;->c:Lhee;

    invoke-interface {v0, p0}, Lhee;->a(Lheg;)Lhee;

    .line 61
    return-void
.end method

.method public a(ZIIII)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 66
    iget-object v0, p0, Livz;->c:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    invoke-virtual {v1, v0}, Liwg;->a(I)Liwg;

    move-result-object v3

    move v1, v2

    :goto_0
    iget-object v0, p0, Livz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Livz;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v3, v0}, Liwg;->a(Ljava/lang/Class;)Liwg;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Livz;->c:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iget-object v1, p0, Livz;->d:Liwc;

    invoke-interface {v1, v3, v0}, Liwc;->a(Liwg;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 67
    new-instance v1, Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x3b

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Login requirements is not satisfied for account "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 69
    const-string v0, " Requirements: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    :goto_1
    iget-object v0, p0, Livz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 71
    iget-object v0, p0, Livz;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 70
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 73
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    const-string v1, "LoginAssert"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75
    const-string v1, "LoginAssert"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 79
    :cond_3
    return-void
.end method
