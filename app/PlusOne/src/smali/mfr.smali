.class public final Lmfr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnnt;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12796
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12799
    iput-object v0, p0, Lmfr;->apiHeader:Llyr;

    .line 12802
    iput-object v0, p0, Lmfr;->a:Lnnt;

    .line 12796
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12819
    const/4 v0, 0x0

    .line 12820
    iget-object v1, p0, Lmfr;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 12821
    const/4 v0, 0x1

    iget-object v1, p0, Lmfr;->apiHeader:Llyr;

    .line 12822
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12824
    :cond_0
    iget-object v1, p0, Lmfr;->a:Lnnt;

    if-eqz v1, :cond_1

    .line 12825
    const/4 v1, 0x2

    iget-object v2, p0, Lmfr;->a:Lnnt;

    .line 12826
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12828
    :cond_1
    iget-object v1, p0, Lmfr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12829
    iput v0, p0, Lmfr;->ai:I

    .line 12830
    return v0
.end method

.method public a(Loxn;)Lmfr;
    .locals 2

    .prologue
    .line 12838
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12839
    sparse-switch v0, :sswitch_data_0

    .line 12843
    iget-object v1, p0, Lmfr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12844
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfr;->ah:Ljava/util/List;

    .line 12847
    :cond_1
    iget-object v1, p0, Lmfr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12849
    :sswitch_0
    return-object p0

    .line 12854
    :sswitch_1
    iget-object v0, p0, Lmfr;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 12855
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfr;->apiHeader:Llyr;

    .line 12857
    :cond_2
    iget-object v0, p0, Lmfr;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12861
    :sswitch_2
    iget-object v0, p0, Lmfr;->a:Lnnt;

    if-nez v0, :cond_3

    .line 12862
    new-instance v0, Lnnt;

    invoke-direct {v0}, Lnnt;-><init>()V

    iput-object v0, p0, Lmfr;->a:Lnnt;

    .line 12864
    :cond_3
    iget-object v0, p0, Lmfr;->a:Lnnt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12839
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12807
    iget-object v0, p0, Lmfr;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 12808
    const/4 v0, 0x1

    iget-object v1, p0, Lmfr;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12810
    :cond_0
    iget-object v0, p0, Lmfr;->a:Lnnt;

    if-eqz v0, :cond_1

    .line 12811
    const/4 v0, 0x2

    iget-object v1, p0, Lmfr;->a:Lnnt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12813
    :cond_1
    iget-object v0, p0, Lmfr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12815
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12792
    invoke-virtual {p0, p1}, Lmfr;->a(Loxn;)Lmfr;

    move-result-object v0

    return-object v0
.end method
