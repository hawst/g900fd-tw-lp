.class public final Lmma;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmma;


# instance fields
.field public b:[Lmmp;

.field public c:Lmmb;

.field private d:Lmon;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1991
    const/4 v0, 0x0

    new-array v0, v0, [Lmma;

    sput-object v0, Lmma;->a:[Lmma;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1992
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1995
    iput-object v1, p0, Lmma;->d:Lmon;

    .line 1998
    sget-object v0, Lmmp;->a:[Lmmp;

    iput-object v0, p0, Lmma;->b:[Lmmp;

    .line 2001
    iput-object v1, p0, Lmma;->c:Lmmb;

    .line 1992
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2025
    .line 2026
    iget-object v0, p0, Lmma;->d:Lmon;

    if-eqz v0, :cond_3

    .line 2027
    const/4 v0, 0x1

    iget-object v2, p0, Lmma;->d:Lmon;

    .line 2028
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2030
    :goto_0
    iget-object v2, p0, Lmma;->b:[Lmmp;

    if-eqz v2, :cond_1

    .line 2031
    iget-object v2, p0, Lmma;->b:[Lmmp;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2032
    if-eqz v4, :cond_0

    .line 2033
    const/4 v5, 0x2

    .line 2034
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2031
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2038
    :cond_1
    iget-object v1, p0, Lmma;->c:Lmmb;

    if-eqz v1, :cond_2

    .line 2039
    const/4 v1, 0x3

    iget-object v2, p0, Lmma;->c:Lmmb;

    .line 2040
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2042
    :cond_2
    iget-object v1, p0, Lmma;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2043
    iput v0, p0, Lmma;->ai:I

    .line 2044
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmma;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2052
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2053
    sparse-switch v0, :sswitch_data_0

    .line 2057
    iget-object v2, p0, Lmma;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2058
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmma;->ah:Ljava/util/List;

    .line 2061
    :cond_1
    iget-object v2, p0, Lmma;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2063
    :sswitch_0
    return-object p0

    .line 2068
    :sswitch_1
    iget-object v0, p0, Lmma;->d:Lmon;

    if-nez v0, :cond_2

    .line 2069
    new-instance v0, Lmon;

    invoke-direct {v0}, Lmon;-><init>()V

    iput-object v0, p0, Lmma;->d:Lmon;

    .line 2071
    :cond_2
    iget-object v0, p0, Lmma;->d:Lmon;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2075
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2076
    iget-object v0, p0, Lmma;->b:[Lmmp;

    if-nez v0, :cond_4

    move v0, v1

    .line 2077
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmmp;

    .line 2078
    iget-object v3, p0, Lmma;->b:[Lmmp;

    if-eqz v3, :cond_3

    .line 2079
    iget-object v3, p0, Lmma;->b:[Lmmp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2081
    :cond_3
    iput-object v2, p0, Lmma;->b:[Lmmp;

    .line 2082
    :goto_2
    iget-object v2, p0, Lmma;->b:[Lmmp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2083
    iget-object v2, p0, Lmma;->b:[Lmmp;

    new-instance v3, Lmmp;

    invoke-direct {v3}, Lmmp;-><init>()V

    aput-object v3, v2, v0

    .line 2084
    iget-object v2, p0, Lmma;->b:[Lmmp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2085
    invoke-virtual {p1}, Loxn;->a()I

    .line 2082
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2076
    :cond_4
    iget-object v0, p0, Lmma;->b:[Lmmp;

    array-length v0, v0

    goto :goto_1

    .line 2088
    :cond_5
    iget-object v2, p0, Lmma;->b:[Lmmp;

    new-instance v3, Lmmp;

    invoke-direct {v3}, Lmmp;-><init>()V

    aput-object v3, v2, v0

    .line 2089
    iget-object v2, p0, Lmma;->b:[Lmmp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2093
    :sswitch_3
    iget-object v0, p0, Lmma;->c:Lmmb;

    if-nez v0, :cond_6

    .line 2094
    new-instance v0, Lmmb;

    invoke-direct {v0}, Lmmb;-><init>()V

    iput-object v0, p0, Lmma;->c:Lmmb;

    .line 2096
    :cond_6
    iget-object v0, p0, Lmma;->c:Lmmb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2053
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2006
    iget-object v0, p0, Lmma;->d:Lmon;

    if-eqz v0, :cond_0

    .line 2007
    const/4 v0, 0x1

    iget-object v1, p0, Lmma;->d:Lmon;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2009
    :cond_0
    iget-object v0, p0, Lmma;->b:[Lmmp;

    if-eqz v0, :cond_2

    .line 2010
    iget-object v1, p0, Lmma;->b:[Lmmp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2011
    if-eqz v3, :cond_1

    .line 2012
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2010
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2016
    :cond_2
    iget-object v0, p0, Lmma;->c:Lmmb;

    if-eqz v0, :cond_3

    .line 2017
    const/4 v0, 0x3

    iget-object v1, p0, Lmma;->c:Lmmb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2019
    :cond_3
    iget-object v0, p0, Lmma;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2021
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1988
    invoke-virtual {p0, p1}, Lmma;->a(Loxn;)Lmma;

    move-result-object v0

    return-object v0
.end method
