.class public final Lfao;
.super Ljfy;
.source "PG"

# interfaces
.implements Ljfx;


# instance fields
.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljfy;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljfy;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    .line 33
    const-string v0, "navigation_footer"

    invoke-virtual {p0, v0}, Lfao;->b(Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method private c(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 126
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 127
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 128
    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 132
    :goto_1
    return v0

    .line 126
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private d(J)I
    .locals 5

    .prologue
    .line 175
    const/4 v0, -0x1

    .line 176
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 177
    const-string v0, "photos"

    invoke-direct {p0, v0}, Lfao;->c(Ljava/lang/String;)I

    move-result v0

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 178
    :cond_1
    const-wide/16 v2, 0x2

    cmp-long v1, p1, v2

    if-nez v1, :cond_2

    .line 179
    const-string v0, "events"

    invoke-direct {p0, v0}, Lfao;->c(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 180
    :cond_2
    const-wide/16 v2, 0x1

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 181
    const-string v0, "locations"

    invoke-direct {p0, v0}, Lfao;->c(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 94
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljfy;->a(I)Landroid/content/Intent;

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ljfy;
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 195
    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 199
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILjgb;)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 83
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Ljfy;->a(ILjgb;)V

    .line 85
    :cond_0
    return-void
.end method

.method public a(JLjgb;)V
    .locals 3

    .prologue
    .line 168
    invoke-direct {p0, p1, p2}, Lfao;->d(J)I

    move-result v0

    .line 169
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 170
    invoke-virtual {p0, v0, p3}, Lfao;->a(ILjgb;)V

    .line 172
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Ljfy;->a(Landroid/content/Context;I)V

    .line 40
    new-instance v0, Lfbz;

    invoke-direct {v0}, Lfbz;-><init>()V

    .line 42
    invoke-virtual {v0, p1, p2}, Lfbz;->a(Landroid/content/Context;I)V

    .line 44
    new-instance v1, Lfaa;

    invoke-direct {v1}, Lfaa;-><init>()V

    .line 46
    invoke-virtual {v1, p1, p2}, Lfaa;->a(Landroid/content/Context;I)V

    .line 48
    new-instance v2, Lext;

    invoke-direct {v2}, Lext;-><init>()V

    .line 50
    invoke-virtual {v2, p1, p2}, Lext;->a(Landroid/content/Context;I)V

    .line 52
    iget-object v3, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 53
    iget-object v3, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    iget v0, p0, Lfao;->b:I

    invoke-static {p1, v0}, Leuc;->a(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lfao;->d:Z

    .line 58
    return-void
.end method

.method public a(Ljfz;J)V
    .locals 4

    .prologue
    .line 137
    invoke-direct {p0, p2, p3}, Lfao;->d(J)I

    move-result v0

    .line 138
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 139
    iget-object v1, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, v2, v3}, Ljfy;->a(Ljfz;J)V

    .line 141
    :cond_0
    return-void
.end method

.method public a(J)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 145
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 152
    :cond_0
    :goto_0
    return v0

    .line 147
    :cond_1
    const-wide/16 v2, 0x2

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    .line 149
    const-wide/16 v0, 0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 150
    iget-boolean v0, p0, Lfao;->d:Z

    goto :goto_0

    .line 152
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public as_()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method protected at_()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public au_()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public b(J)I
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)Z
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 189
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljfy;->b(I)Z

    move-result v0

    return v0
.end method

.method public c(J)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Lfao;->d(J)I

    move-result v0

    .line 163
    invoke-virtual {p0, v0}, Lfao;->a(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    invoke-virtual {v0, p1}, Ljfy;->c(I)Z

    move-result v0

    return v0
.end method

.method public d(I)I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public e(I)J
    .locals 2

    .prologue
    .line 105
    if-ltz p1, :cond_2

    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 106
    iget-object v0, p0, Lfao;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 108
    instance-of v1, v0, Lfbz;

    if-eqz v1, :cond_0

    .line 109
    const-wide/16 v0, 0x0

    .line 121
    :goto_0
    return-wide v0

    .line 112
    :cond_0
    instance-of v1, v0, Lext;

    if-eqz v1, :cond_1

    .line 113
    const-wide/16 v0, 0x2

    goto :goto_0

    .line 116
    :cond_1
    instance-of v0, v0, Lfaa;

    if-eqz v0, :cond_2

    .line 117
    const-wide/16 v0, 0x1

    goto :goto_0

    .line 121
    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0
.end method
