.class public final Lmhn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvz;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6802
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6805
    iput-object v0, p0, Lmhn;->apiHeader:Llyr;

    .line 6808
    iput-object v0, p0, Lmhn;->a:Lnvz;

    .line 6802
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6825
    const/4 v0, 0x0

    .line 6826
    iget-object v1, p0, Lmhn;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 6827
    const/4 v0, 0x1

    iget-object v1, p0, Lmhn;->apiHeader:Llyr;

    .line 6828
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6830
    :cond_0
    iget-object v1, p0, Lmhn;->a:Lnvz;

    if-eqz v1, :cond_1

    .line 6831
    const/4 v1, 0x2

    iget-object v2, p0, Lmhn;->a:Lnvz;

    .line 6832
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6834
    :cond_1
    iget-object v1, p0, Lmhn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6835
    iput v0, p0, Lmhn;->ai:I

    .line 6836
    return v0
.end method

.method public a(Loxn;)Lmhn;
    .locals 2

    .prologue
    .line 6844
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6845
    sparse-switch v0, :sswitch_data_0

    .line 6849
    iget-object v1, p0, Lmhn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6850
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhn;->ah:Ljava/util/List;

    .line 6853
    :cond_1
    iget-object v1, p0, Lmhn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6855
    :sswitch_0
    return-object p0

    .line 6860
    :sswitch_1
    iget-object v0, p0, Lmhn;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 6861
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmhn;->apiHeader:Llyr;

    .line 6863
    :cond_2
    iget-object v0, p0, Lmhn;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6867
    :sswitch_2
    iget-object v0, p0, Lmhn;->a:Lnvz;

    if-nez v0, :cond_3

    .line 6868
    new-instance v0, Lnvz;

    invoke-direct {v0}, Lnvz;-><init>()V

    iput-object v0, p0, Lmhn;->a:Lnvz;

    .line 6870
    :cond_3
    iget-object v0, p0, Lmhn;->a:Lnvz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6845
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6813
    iget-object v0, p0, Lmhn;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 6814
    const/4 v0, 0x1

    iget-object v1, p0, Lmhn;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6816
    :cond_0
    iget-object v0, p0, Lmhn;->a:Lnvz;

    if-eqz v0, :cond_1

    .line 6817
    const/4 v0, 0x2

    iget-object v1, p0, Lmhn;->a:Lnvz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6819
    :cond_1
    iget-object v0, p0, Lmhn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6821
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6798
    invoke-virtual {p0, p1}, Lmhn;->a(Loxn;)Lmhn;

    move-result-object v0

    return-object v0
.end method
