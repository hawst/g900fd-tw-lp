.class public final Lfva;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Lae;IZLjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    invoke-static {p0, p3, p2}, Lfva;->a(Landroid/content/Context;ZI)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54
    const v0, 0x7f0a0acf

    .line 55
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 56
    const-string v0, "plusone_posts"

    const-string v4, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {p0, v0, v4}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v5, "is_default_restricted"

    invoke-interface {v0, v5}, Lhej;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const v0, 0x7f0a0ad2

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v4, v5, v2

    invoke-virtual {p0, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const v2, 0x7f0a0596

    .line 57
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    .line 54
    invoke-static {v3, v0, v2, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 58
    invoke-virtual {v0, p1, p4}, Llgr;->a(Lae;Ljava/lang/String;)V

    move v0, v1

    .line 61
    :goto_1
    return v0

    .line 56
    :cond_0
    const-string v5, "is_child"

    invoke-interface {v0, v5}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0ad1

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v4, v5, v2

    invoke-virtual {p0, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f0a0ad0

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v4, v5, v2

    invoke-virtual {p0, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 61
    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ZI)Z
    .locals 2

    .prologue
    .line 66
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 67
    sget-object v1, Ldxd;->e:Lief;

    invoke-interface {v0, v1, p2}, Lieh;->b(Lief;I)Z

    move-result v0

    .line 69
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 70
    invoke-static {p0, p2}, Ldhv;->b(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
