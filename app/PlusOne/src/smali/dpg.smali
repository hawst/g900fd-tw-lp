.class public final Ldpg;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lknz;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljuf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 37
    const-string v0, "LogShareTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 38
    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 39
    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    iput v1, p0, Ldpg;->c:I

    .line 40
    invoke-interface {v0}, Lhee;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-static {p2, v0}, Lknz;->a(Ljava/lang/String;Ljava/lang/String;)Lknz;

    move-result-object v0

    iput-object v0, p0, Ldpg;->a:Lknz;

    .line 46
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldpg;->b:Ljava/util/List;

    .line 47
    return-void

    .line 44
    :cond_0
    invoke-static {p2}, Lknz;->a(Ljava/lang/String;)Lknz;

    move-result-object v0

    iput-object v0, p0, Ldpg;->a:Lknz;

    goto :goto_0
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 75
    invoke-virtual {p0}, Ldpg;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v1

    .line 78
    iget-object v0, p0, Ldpg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    .line 79
    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v3

    .line 80
    invoke-interface {v0}, Ljuf;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 81
    iget-object v3, p0, Ldpg;->a:Lknz;

    invoke-interface {v0}, Ljuf;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lknz;->b(Ljava/lang/String;)Lknz;

    goto :goto_0

    .line 82
    :cond_1
    invoke-virtual {v3}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    invoke-virtual {v3}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhrx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 86
    iget-object v3, p0, Ldpg;->a:Lknz;

    invoke-virtual {v3, v0}, Lknz;->c(Ljava/lang/String;)Lknz;

    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {v3}, Lizu;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {v3}, Lizu;->a()Ljava/lang/String;

    move-result-object v0

    iget v4, p0, Ldpg;->c:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    invoke-virtual {p0}, Ldpg;->f()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Ldpg;->c:I

    invoke-static {v4, v5, v0}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 90
    :goto_1
    if-eqz v0, :cond_4

    .line 91
    iget-object v3, p0, Ldpg;->a:Lknz;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lknz;->a(J)Lknz;

    goto :goto_0

    .line 89
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 93
    :cond_4
    const-string v0, "MediaItem has a tileId, but we could not find the photoId.  MediaItem TileId: %s"

    new-array v4, v7, [Ljava/lang/Object;

    .line 95
    invoke-virtual {v3}, Lizu;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v6

    .line 93
    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    .line 103
    :cond_5
    iget-object v0, p0, Ldpg;->a:Lknz;

    invoke-virtual {p0}, Ldpg;->f()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lknz;->a(Landroid/content/Context;)V

    .line 106
    new-instance v0, Lhoz;

    invoke-direct {v0, v7}, Lhoz;-><init>(Z)V

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Ldpg;->a:Lknz;

    invoke-virtual {v0, p1, p2}, Lknz;->b(J)Lknz;

    .line 62
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Ldpg;->a:Lknz;

    invoke-virtual {v0, p1}, Lknz;->d(Ljava/lang/String;)Lknz;

    .line 69
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljuf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Ldpg;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 55
    return-void
.end method
