.class public final Lnbz;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Lota;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Loxq;-><init>()V

    .line 134
    const/high16 v0, -0x80000000

    iput v0, p0, Lnbz;->a:I

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lnbz;->b:Lota;

    .line 125
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 154
    const/4 v0, 0x0

    .line 155
    iget v1, p0, Lnbz;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 156
    const/4 v0, 0x1

    iget v1, p0, Lnbz;->a:I

    .line 157
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 159
    :cond_0
    iget-object v1, p0, Lnbz;->b:Lota;

    if-eqz v1, :cond_1

    .line 160
    const/4 v1, 0x2

    iget-object v2, p0, Lnbz;->b:Lota;

    .line 161
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_1
    iget-object v1, p0, Lnbz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    iput v0, p0, Lnbz;->ai:I

    .line 165
    return v0
.end method

.method public a(Loxn;)Lnbz;
    .locals 2

    .prologue
    .line 173
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 174
    sparse-switch v0, :sswitch_data_0

    .line 178
    iget-object v1, p0, Lnbz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbz;->ah:Ljava/util/List;

    .line 182
    :cond_1
    iget-object v1, p0, Lnbz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    :sswitch_0
    return-object p0

    .line 189
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 190
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 193
    :cond_2
    iput v0, p0, Lnbz;->a:I

    goto :goto_0

    .line 195
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnbz;->a:I

    goto :goto_0

    .line 200
    :sswitch_2
    iget-object v0, p0, Lnbz;->b:Lota;

    if-nez v0, :cond_4

    .line 201
    new-instance v0, Lota;

    invoke-direct {v0}, Lota;-><init>()V

    iput-object v0, p0, Lnbz;->b:Lota;

    .line 203
    :cond_4
    iget-object v0, p0, Lnbz;->b:Lota;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 174
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 142
    iget v0, p0, Lnbz;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 143
    const/4 v0, 0x1

    iget v1, p0, Lnbz;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 145
    :cond_0
    iget-object v0, p0, Lnbz;->b:Lota;

    if-eqz v0, :cond_1

    .line 146
    const/4 v0, 0x2

    iget-object v1, p0, Lnbz;->b:Lota;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 148
    :cond_1
    iget-object v0, p0, Lnbz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 150
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lnbz;->a(Loxn;)Lnbz;

    move-result-object v0

    return-object v0
.end method
