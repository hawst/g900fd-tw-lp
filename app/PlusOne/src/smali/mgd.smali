.class public final Lmgd;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfg;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27538
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27541
    iput-object v0, p0, Lmgd;->apiHeader:Llyr;

    .line 27544
    iput-object v0, p0, Lmgd;->a:Lnfg;

    .line 27538
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27561
    const/4 v0, 0x0

    .line 27562
    iget-object v1, p0, Lmgd;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 27563
    const/4 v0, 0x1

    iget-object v1, p0, Lmgd;->apiHeader:Llyr;

    .line 27564
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27566
    :cond_0
    iget-object v1, p0, Lmgd;->a:Lnfg;

    if-eqz v1, :cond_1

    .line 27567
    const/4 v1, 0x2

    iget-object v2, p0, Lmgd;->a:Lnfg;

    .line 27568
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27570
    :cond_1
    iget-object v1, p0, Lmgd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27571
    iput v0, p0, Lmgd;->ai:I

    .line 27572
    return v0
.end method

.method public a(Loxn;)Lmgd;
    .locals 2

    .prologue
    .line 27580
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27581
    sparse-switch v0, :sswitch_data_0

    .line 27585
    iget-object v1, p0, Lmgd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27586
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgd;->ah:Ljava/util/List;

    .line 27589
    :cond_1
    iget-object v1, p0, Lmgd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27591
    :sswitch_0
    return-object p0

    .line 27596
    :sswitch_1
    iget-object v0, p0, Lmgd;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 27597
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgd;->apiHeader:Llyr;

    .line 27599
    :cond_2
    iget-object v0, p0, Lmgd;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27603
    :sswitch_2
    iget-object v0, p0, Lmgd;->a:Lnfg;

    if-nez v0, :cond_3

    .line 27604
    new-instance v0, Lnfg;

    invoke-direct {v0}, Lnfg;-><init>()V

    iput-object v0, p0, Lmgd;->a:Lnfg;

    .line 27606
    :cond_3
    iget-object v0, p0, Lmgd;->a:Lnfg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27581
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27549
    iget-object v0, p0, Lmgd;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 27550
    const/4 v0, 0x1

    iget-object v1, p0, Lmgd;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27552
    :cond_0
    iget-object v0, p0, Lmgd;->a:Lnfg;

    if-eqz v0, :cond_1

    .line 27553
    const/4 v0, 0x2

    iget-object v1, p0, Lmgd;->a:Lnfg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27555
    :cond_1
    iget-object v0, p0, Lmgd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27557
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 27534
    invoke-virtual {p0, p1}, Lmgd;->a(Loxn;)Lmgd;

    move-result-object v0

    return-object v0
.end method
