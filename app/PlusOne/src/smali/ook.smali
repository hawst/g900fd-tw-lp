.class public final Look;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Look;


# instance fields
.field public b:[Ljava/lang/Integer;

.field public c:Loom;

.field public d:Loom;

.field public e:Lool;

.field public f:[Ljava/lang/Double;

.field public g:Looh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 488
    const/4 v0, 0x0

    new-array v0, v0, [Look;

    sput-object v0, Look;->a:[Look;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 489
    invoke-direct {p0}, Loxq;-><init>()V

    .line 690
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Look;->b:[Ljava/lang/Integer;

    .line 693
    iput-object v1, p0, Look;->c:Loom;

    .line 696
    iput-object v1, p0, Look;->d:Loom;

    .line 699
    iput-object v1, p0, Look;->e:Lool;

    .line 702
    sget-object v0, Loxx;->j:[Ljava/lang/Double;

    iput-object v0, p0, Look;->f:[Ljava/lang/Double;

    .line 705
    iput-object v1, p0, Look;->g:Looh;

    .line 489
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 738
    .line 739
    iget-object v1, p0, Look;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Look;->b:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 741
    iget-object v2, p0, Look;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 743
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 741
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 746
    :cond_0
    iget-object v0, p0, Look;->b:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 748
    :cond_1
    iget-object v1, p0, Look;->c:Loom;

    if-eqz v1, :cond_2

    .line 749
    const/4 v1, 0x2

    iget-object v2, p0, Look;->c:Loom;

    .line 750
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 752
    :cond_2
    iget-object v1, p0, Look;->d:Loom;

    if-eqz v1, :cond_3

    .line 753
    const/4 v1, 0x3

    iget-object v2, p0, Look;->d:Loom;

    .line 754
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 756
    :cond_3
    iget-object v1, p0, Look;->e:Lool;

    if-eqz v1, :cond_4

    .line 757
    const/4 v1, 0x4

    iget-object v2, p0, Look;->e:Lool;

    .line 758
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 760
    :cond_4
    iget-object v1, p0, Look;->f:[Ljava/lang/Double;

    if-eqz v1, :cond_5

    iget-object v1, p0, Look;->f:[Ljava/lang/Double;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 761
    iget-object v1, p0, Look;->f:[Ljava/lang/Double;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 762
    add-int/2addr v0, v1

    .line 763
    iget-object v1, p0, Look;->f:[Ljava/lang/Double;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 765
    :cond_5
    iget-object v1, p0, Look;->g:Looh;

    if-eqz v1, :cond_6

    .line 766
    const/4 v1, 0x6

    iget-object v2, p0, Look;->g:Looh;

    .line 767
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 769
    :cond_6
    iget-object v1, p0, Look;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 770
    iput v0, p0, Look;->ai:I

    .line 771
    return v0
.end method

.method public a(Loxn;)Look;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 779
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 780
    sparse-switch v0, :sswitch_data_0

    .line 784
    iget-object v1, p0, Look;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 785
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Look;->ah:Ljava/util/List;

    .line 788
    :cond_1
    iget-object v1, p0, Look;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 790
    :sswitch_0
    return-object p0

    .line 795
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 796
    iget-object v0, p0, Look;->b:[Ljava/lang/Integer;

    array-length v0, v0

    .line 797
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 798
    iget-object v2, p0, Look;->b:[Ljava/lang/Integer;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 799
    iput-object v1, p0, Look;->b:[Ljava/lang/Integer;

    .line 800
    :goto_1
    iget-object v1, p0, Look;->b:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 801
    iget-object v1, p0, Look;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 802
    invoke-virtual {p1}, Loxn;->a()I

    .line 800
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 805
    :cond_2
    iget-object v1, p0, Look;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 809
    :sswitch_2
    iget-object v0, p0, Look;->c:Loom;

    if-nez v0, :cond_3

    .line 810
    new-instance v0, Loom;

    invoke-direct {v0}, Loom;-><init>()V

    iput-object v0, p0, Look;->c:Loom;

    .line 812
    :cond_3
    iget-object v0, p0, Look;->c:Loom;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 816
    :sswitch_3
    iget-object v0, p0, Look;->d:Loom;

    if-nez v0, :cond_4

    .line 817
    new-instance v0, Loom;

    invoke-direct {v0}, Loom;-><init>()V

    iput-object v0, p0, Look;->d:Loom;

    .line 819
    :cond_4
    iget-object v0, p0, Look;->d:Loom;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 823
    :sswitch_4
    iget-object v0, p0, Look;->e:Lool;

    if-nez v0, :cond_5

    .line 824
    new-instance v0, Lool;

    invoke-direct {v0}, Lool;-><init>()V

    iput-object v0, p0, Look;->e:Lool;

    .line 826
    :cond_5
    iget-object v0, p0, Look;->e:Lool;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 830
    :sswitch_5
    const/16 v0, 0x29

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 831
    iget-object v0, p0, Look;->f:[Ljava/lang/Double;

    array-length v0, v0

    .line 832
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Double;

    .line 833
    iget-object v2, p0, Look;->f:[Ljava/lang/Double;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 834
    iput-object v1, p0, Look;->f:[Ljava/lang/Double;

    .line 835
    :goto_2
    iget-object v1, p0, Look;->f:[Ljava/lang/Double;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    .line 836
    iget-object v1, p0, Look;->f:[Ljava/lang/Double;

    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v0

    .line 837
    invoke-virtual {p1}, Loxn;->a()I

    .line 835
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 840
    :cond_6
    iget-object v1, p0, Look;->f:[Ljava/lang/Double;

    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 844
    :sswitch_6
    iget-object v0, p0, Look;->g:Looh;

    if-nez v0, :cond_7

    .line 845
    new-instance v0, Looh;

    invoke-direct {v0}, Looh;-><init>()V

    iput-object v0, p0, Look;->g:Looh;

    .line 847
    :cond_7
    iget-object v0, p0, Look;->g:Looh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 780
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 710
    iget-object v1, p0, Look;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 711
    iget-object v2, p0, Look;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 712
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 711
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 715
    :cond_0
    iget-object v1, p0, Look;->c:Loom;

    if-eqz v1, :cond_1

    .line 716
    const/4 v1, 0x2

    iget-object v2, p0, Look;->c:Loom;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 718
    :cond_1
    iget-object v1, p0, Look;->d:Loom;

    if-eqz v1, :cond_2

    .line 719
    const/4 v1, 0x3

    iget-object v2, p0, Look;->d:Loom;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 721
    :cond_2
    iget-object v1, p0, Look;->e:Lool;

    if-eqz v1, :cond_3

    .line 722
    const/4 v1, 0x4

    iget-object v2, p0, Look;->e:Lool;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 724
    :cond_3
    iget-object v1, p0, Look;->f:[Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 725
    iget-object v1, p0, Look;->f:[Ljava/lang/Double;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 726
    const/4 v4, 0x5

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual {p1, v4, v6, v7}, Loxo;->a(ID)V

    .line 725
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 729
    :cond_4
    iget-object v0, p0, Look;->g:Looh;

    if-eqz v0, :cond_5

    .line 730
    const/4 v0, 0x6

    iget-object v1, p0, Look;->g:Looh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 732
    :cond_5
    iget-object v0, p0, Look;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 734
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 485
    invoke-virtual {p0, p1}, Look;->a(Loxn;)Look;

    move-result-object v0

    return-object v0
.end method
