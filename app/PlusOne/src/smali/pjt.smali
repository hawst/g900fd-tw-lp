.class public final Lpjt;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 870
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 892
    const/4 v0, 0x1

    iget-object v1, p0, Lpjt;->a:Ljava/lang/String;

    .line 894
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 895
    const/4 v1, 0x2

    iget-object v2, p0, Lpjt;->b:Ljava/lang/String;

    .line 896
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 897
    iget-object v1, p0, Lpjt;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 898
    const/4 v1, 0x7

    iget-object v2, p0, Lpjt;->c:Ljava/lang/Long;

    .line 899
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 901
    :cond_0
    iget-object v1, p0, Lpjt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 902
    iput v0, p0, Lpjt;->ai:I

    .line 903
    return v0
.end method

.method public a(Loxn;)Lpjt;
    .locals 2

    .prologue
    .line 911
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 912
    sparse-switch v0, :sswitch_data_0

    .line 916
    iget-object v1, p0, Lpjt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 917
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjt;->ah:Ljava/util/List;

    .line 920
    :cond_1
    iget-object v1, p0, Lpjt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 922
    :sswitch_0
    return-object p0

    .line 927
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjt;->a:Ljava/lang/String;

    goto :goto_0

    .line 931
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjt;->b:Ljava/lang/String;

    goto :goto_0

    .line 935
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpjt;->c:Ljava/lang/Long;

    goto :goto_0

    .line 912
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x38 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 881
    const/4 v0, 0x1

    iget-object v1, p0, Lpjt;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 882
    const/4 v0, 0x2

    iget-object v1, p0, Lpjt;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 883
    iget-object v0, p0, Lpjt;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 884
    const/4 v0, 0x7

    iget-object v1, p0, Lpjt;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 886
    :cond_0
    iget-object v0, p0, Lpjt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 888
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 866
    invoke-virtual {p0, p1}, Lpjt;->a(Loxn;)Lpjt;

    move-result-object v0

    return-object v0
.end method
