.class public final Lqbh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lqbe;

.field private b:Lqar;

.field private c:Lqbi;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 325
    invoke-direct {p0}, Loxq;-><init>()V

    .line 328
    iput-object v0, p0, Lqbh;->a:Lqbe;

    .line 331
    iput-object v0, p0, Lqbh;->b:Lqar;

    .line 334
    iput-object v0, p0, Lqbh;->c:Lqbi;

    .line 325
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 354
    const/4 v0, 0x0

    .line 355
    iget-object v1, p0, Lqbh;->a:Lqbe;

    if-eqz v1, :cond_0

    .line 356
    const/4 v0, 0x1

    iget-object v1, p0, Lqbh;->a:Lqbe;

    .line 357
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 359
    :cond_0
    iget-object v1, p0, Lqbh;->b:Lqar;

    if-eqz v1, :cond_1

    .line 360
    const/4 v1, 0x2

    iget-object v2, p0, Lqbh;->b:Lqar;

    .line 361
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_1
    iget-object v1, p0, Lqbh;->c:Lqbi;

    if-eqz v1, :cond_2

    .line 364
    const/4 v1, 0x3

    iget-object v2, p0, Lqbh;->c:Lqbi;

    .line 365
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_2
    iget-object v1, p0, Lqbh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 368
    iput v0, p0, Lqbh;->ai:I

    .line 369
    return v0
.end method

.method public a(Loxn;)Lqbh;
    .locals 2

    .prologue
    .line 377
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 378
    sparse-switch v0, :sswitch_data_0

    .line 382
    iget-object v1, p0, Lqbh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 383
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbh;->ah:Ljava/util/List;

    .line 386
    :cond_1
    iget-object v1, p0, Lqbh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    :sswitch_0
    return-object p0

    .line 393
    :sswitch_1
    iget-object v0, p0, Lqbh;->a:Lqbe;

    if-nez v0, :cond_2

    .line 394
    new-instance v0, Lqbe;

    invoke-direct {v0}, Lqbe;-><init>()V

    iput-object v0, p0, Lqbh;->a:Lqbe;

    .line 396
    :cond_2
    iget-object v0, p0, Lqbh;->a:Lqbe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 400
    :sswitch_2
    iget-object v0, p0, Lqbh;->b:Lqar;

    if-nez v0, :cond_3

    .line 401
    new-instance v0, Lqar;

    invoke-direct {v0}, Lqar;-><init>()V

    iput-object v0, p0, Lqbh;->b:Lqar;

    .line 403
    :cond_3
    iget-object v0, p0, Lqbh;->b:Lqar;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 407
    :sswitch_3
    iget-object v0, p0, Lqbh;->c:Lqbi;

    if-nez v0, :cond_4

    .line 408
    new-instance v0, Lqbi;

    invoke-direct {v0}, Lqbi;-><init>()V

    iput-object v0, p0, Lqbh;->c:Lqbi;

    .line 410
    :cond_4
    iget-object v0, p0, Lqbh;->c:Lqbi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 378
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lqbh;->a:Lqbe;

    if-eqz v0, :cond_0

    .line 340
    const/4 v0, 0x1

    iget-object v1, p0, Lqbh;->a:Lqbe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 342
    :cond_0
    iget-object v0, p0, Lqbh;->b:Lqar;

    if-eqz v0, :cond_1

    .line 343
    const/4 v0, 0x2

    iget-object v1, p0, Lqbh;->b:Lqar;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 345
    :cond_1
    iget-object v0, p0, Lqbh;->c:Lqbi;

    if-eqz v0, :cond_2

    .line 346
    const/4 v0, 0x3

    iget-object v1, p0, Lqbh;->c:Lqbi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 348
    :cond_2
    iget-object v0, p0, Lqbh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 350
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Lqbh;->a(Loxn;)Lqbh;

    move-result-object v0

    return-object v0
.end method
