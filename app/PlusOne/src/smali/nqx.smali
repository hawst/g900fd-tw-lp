.class public final Lnqx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnqx;


# instance fields
.field public b:I

.field public c:Ljava/lang/Integer;

.field public d:Lohl;

.field public e:Ljava/lang/Boolean;

.field public f:Lnqw;

.field public g:Lnqy;

.field private h:Ljava/lang/Boolean;

.field private i:Lnqz;

.field private j:Lnqv;

.field private k:Lnrb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    new-array v0, v0, [Lnqx;

    sput-object v0, Lnqx;->a:[Lnqx;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Loxq;-><init>()V

    .line 96
    const/high16 v0, -0x80000000

    iput v0, p0, Lnqx;->b:I

    .line 101
    iput-object v1, p0, Lnqx;->d:Lohl;

    .line 108
    iput-object v1, p0, Lnqx;->i:Lnqz;

    .line 111
    iput-object v1, p0, Lnqx;->f:Lnqw;

    .line 114
    iput-object v1, p0, Lnqx;->j:Lnqv;

    .line 117
    iput-object v1, p0, Lnqx;->g:Lnqy;

    .line 120
    iput-object v1, p0, Lnqx;->k:Lnrb;

    .line 93
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 162
    iget v1, p0, Lnqx;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 163
    const/4 v0, 0x1

    iget v1, p0, Lnqx;->b:I

    .line 164
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 166
    :cond_0
    iget-object v1, p0, Lnqx;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 167
    const/4 v1, 0x2

    iget-object v2, p0, Lnqx;->c:Ljava/lang/Integer;

    .line 168
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_1
    iget-object v1, p0, Lnqx;->d:Lohl;

    if-eqz v1, :cond_2

    .line 171
    const/4 v1, 0x3

    iget-object v2, p0, Lnqx;->d:Lohl;

    .line 172
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_2
    iget-object v1, p0, Lnqx;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 175
    const/4 v1, 0x4

    iget-object v2, p0, Lnqx;->e:Ljava/lang/Boolean;

    .line 176
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 178
    :cond_3
    iget-object v1, p0, Lnqx;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 179
    const/4 v1, 0x5

    iget-object v2, p0, Lnqx;->h:Ljava/lang/Boolean;

    .line 180
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 182
    :cond_4
    iget-object v1, p0, Lnqx;->i:Lnqz;

    if-eqz v1, :cond_5

    .line 183
    const/16 v1, 0x8

    iget-object v2, p0, Lnqx;->i:Lnqz;

    .line 184
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_5
    iget-object v1, p0, Lnqx;->f:Lnqw;

    if-eqz v1, :cond_6

    .line 187
    const/16 v1, 0x9

    iget-object v2, p0, Lnqx;->f:Lnqw;

    .line 188
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    :cond_6
    iget-object v1, p0, Lnqx;->j:Lnqv;

    if-eqz v1, :cond_7

    .line 191
    const/16 v1, 0xa

    iget-object v2, p0, Lnqx;->j:Lnqv;

    .line 192
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_7
    iget-object v1, p0, Lnqx;->g:Lnqy;

    if-eqz v1, :cond_8

    .line 195
    const/16 v1, 0xc

    iget-object v2, p0, Lnqx;->g:Lnqy;

    .line 196
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    :cond_8
    iget-object v1, p0, Lnqx;->k:Lnrb;

    if-eqz v1, :cond_9

    .line 199
    const/16 v1, 0xd

    iget-object v2, p0, Lnqx;->k:Lnrb;

    .line 200
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 202
    :cond_9
    iget-object v1, p0, Lnqx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    iput v0, p0, Lnqx;->ai:I

    .line 204
    return v0
.end method

.method public a(Loxn;)Lnqx;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 212
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 213
    sparse-switch v0, :sswitch_data_0

    .line 217
    iget-object v1, p0, Lnqx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 218
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnqx;->ah:Ljava/util/List;

    .line 221
    :cond_1
    iget-object v1, p0, Lnqx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    :sswitch_0
    return-object p0

    .line 228
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 229
    if-eq v0, v2, :cond_2

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-ne v0, v1, :cond_3

    .line 246
    :cond_2
    iput v0, p0, Lnqx;->b:I

    goto :goto_0

    .line 248
    :cond_3
    iput v2, p0, Lnqx;->b:I

    goto :goto_0

    .line 253
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnqx;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 257
    :sswitch_3
    iget-object v0, p0, Lnqx;->d:Lohl;

    if-nez v0, :cond_4

    .line 258
    new-instance v0, Lohl;

    invoke-direct {v0}, Lohl;-><init>()V

    iput-object v0, p0, Lnqx;->d:Lohl;

    .line 260
    :cond_4
    iget-object v0, p0, Lnqx;->d:Lohl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 264
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqx;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 268
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqx;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 272
    :sswitch_6
    iget-object v0, p0, Lnqx;->i:Lnqz;

    if-nez v0, :cond_5

    .line 273
    new-instance v0, Lnqz;

    invoke-direct {v0}, Lnqz;-><init>()V

    iput-object v0, p0, Lnqx;->i:Lnqz;

    .line 275
    :cond_5
    iget-object v0, p0, Lnqx;->i:Lnqz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 279
    :sswitch_7
    iget-object v0, p0, Lnqx;->f:Lnqw;

    if-nez v0, :cond_6

    .line 280
    new-instance v0, Lnqw;

    invoke-direct {v0}, Lnqw;-><init>()V

    iput-object v0, p0, Lnqx;->f:Lnqw;

    .line 282
    :cond_6
    iget-object v0, p0, Lnqx;->f:Lnqw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 286
    :sswitch_8
    iget-object v0, p0, Lnqx;->j:Lnqv;

    if-nez v0, :cond_7

    .line 287
    new-instance v0, Lnqv;

    invoke-direct {v0}, Lnqv;-><init>()V

    iput-object v0, p0, Lnqx;->j:Lnqv;

    .line 289
    :cond_7
    iget-object v0, p0, Lnqx;->j:Lnqv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 293
    :sswitch_9
    iget-object v0, p0, Lnqx;->g:Lnqy;

    if-nez v0, :cond_8

    .line 294
    new-instance v0, Lnqy;

    invoke-direct {v0}, Lnqy;-><init>()V

    iput-object v0, p0, Lnqx;->g:Lnqy;

    .line 296
    :cond_8
    iget-object v0, p0, Lnqx;->g:Lnqy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 300
    :sswitch_a
    iget-object v0, p0, Lnqx;->k:Lnrb;

    if-nez v0, :cond_9

    .line 301
    new-instance v0, Lnrb;

    invoke-direct {v0}, Lnrb;-><init>()V

    iput-object v0, p0, Lnqx;->k:Lnrb;

    .line 303
    :cond_9
    iget-object v0, p0, Lnqx;->k:Lnrb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 213
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x62 -> :sswitch_9
        0x6a -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 125
    iget v0, p0, Lnqx;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 126
    const/4 v0, 0x1

    iget v1, p0, Lnqx;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 128
    :cond_0
    iget-object v0, p0, Lnqx;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 129
    const/4 v0, 0x2

    iget-object v1, p0, Lnqx;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 131
    :cond_1
    iget-object v0, p0, Lnqx;->d:Lohl;

    if-eqz v0, :cond_2

    .line 132
    const/4 v0, 0x3

    iget-object v1, p0, Lnqx;->d:Lohl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 134
    :cond_2
    iget-object v0, p0, Lnqx;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 135
    const/4 v0, 0x4

    iget-object v1, p0, Lnqx;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 137
    :cond_3
    iget-object v0, p0, Lnqx;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 138
    const/4 v0, 0x5

    iget-object v1, p0, Lnqx;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 140
    :cond_4
    iget-object v0, p0, Lnqx;->i:Lnqz;

    if-eqz v0, :cond_5

    .line 141
    const/16 v0, 0x8

    iget-object v1, p0, Lnqx;->i:Lnqz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 143
    :cond_5
    iget-object v0, p0, Lnqx;->f:Lnqw;

    if-eqz v0, :cond_6

    .line 144
    const/16 v0, 0x9

    iget-object v1, p0, Lnqx;->f:Lnqw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 146
    :cond_6
    iget-object v0, p0, Lnqx;->j:Lnqv;

    if-eqz v0, :cond_7

    .line 147
    const/16 v0, 0xa

    iget-object v1, p0, Lnqx;->j:Lnqv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 149
    :cond_7
    iget-object v0, p0, Lnqx;->g:Lnqy;

    if-eqz v0, :cond_8

    .line 150
    const/16 v0, 0xc

    iget-object v1, p0, Lnqx;->g:Lnqy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 152
    :cond_8
    iget-object v0, p0, Lnqx;->k:Lnrb;

    if-eqz v0, :cond_9

    .line 153
    const/16 v0, 0xd

    iget-object v1, p0, Lnqx;->k:Lnrb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 155
    :cond_9
    iget-object v0, p0, Lnqx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 157
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lnqx;->a(Loxn;)Lnqx;

    move-result-object v0

    return-object v0
.end method
