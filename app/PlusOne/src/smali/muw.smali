.class public final Lmuw;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8699
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8740
    const/4 v0, 0x0

    .line 8741
    iget-object v1, p0, Lmuw;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 8742
    const/4 v0, 0x1

    iget-object v1, p0, Lmuw;->a:Ljava/lang/Boolean;

    .line 8743
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 8745
    :cond_0
    iget-object v1, p0, Lmuw;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 8746
    const/4 v1, 0x2

    iget-object v2, p0, Lmuw;->b:Ljava/lang/Boolean;

    .line 8747
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8749
    :cond_1
    iget-object v1, p0, Lmuw;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 8750
    const/4 v1, 0x3

    iget-object v2, p0, Lmuw;->c:Ljava/lang/Integer;

    .line 8751
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8753
    :cond_2
    iget-object v1, p0, Lmuw;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 8754
    const/4 v1, 0x4

    iget-object v2, p0, Lmuw;->d:Ljava/lang/Integer;

    .line 8755
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8757
    :cond_3
    iget-object v1, p0, Lmuw;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 8758
    const/4 v1, 0x5

    iget-object v2, p0, Lmuw;->e:Ljava/lang/Integer;

    .line 8759
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8761
    :cond_4
    iget-object v1, p0, Lmuw;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 8762
    const/4 v1, 0x6

    iget-object v2, p0, Lmuw;->f:Ljava/lang/Integer;

    .line 8763
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8765
    :cond_5
    iget-object v1, p0, Lmuw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8766
    iput v0, p0, Lmuw;->ai:I

    .line 8767
    return v0
.end method

.method public a(Loxn;)Lmuw;
    .locals 2

    .prologue
    .line 8775
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8776
    sparse-switch v0, :sswitch_data_0

    .line 8780
    iget-object v1, p0, Lmuw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8781
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmuw;->ah:Ljava/util/List;

    .line 8784
    :cond_1
    iget-object v1, p0, Lmuw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8786
    :sswitch_0
    return-object p0

    .line 8791
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmuw;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 8795
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmuw;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 8799
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmuw;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 8803
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmuw;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 8807
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmuw;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 8811
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmuw;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 8776
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8716
    iget-object v0, p0, Lmuw;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 8717
    const/4 v0, 0x1

    iget-object v1, p0, Lmuw;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8719
    :cond_0
    iget-object v0, p0, Lmuw;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 8720
    const/4 v0, 0x2

    iget-object v1, p0, Lmuw;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 8722
    :cond_1
    iget-object v0, p0, Lmuw;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 8723
    const/4 v0, 0x3

    iget-object v1, p0, Lmuw;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8725
    :cond_2
    iget-object v0, p0, Lmuw;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 8726
    const/4 v0, 0x4

    iget-object v1, p0, Lmuw;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8728
    :cond_3
    iget-object v0, p0, Lmuw;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 8729
    const/4 v0, 0x5

    iget-object v1, p0, Lmuw;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8731
    :cond_4
    iget-object v0, p0, Lmuw;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 8732
    const/4 v0, 0x6

    iget-object v1, p0, Lmuw;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8734
    :cond_5
    iget-object v0, p0, Lmuw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8736
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8695
    invoke-virtual {p0, p1}, Lmuw;->a(Loxn;)Lmuw;

    move-result-object v0

    return-object v0
.end method
