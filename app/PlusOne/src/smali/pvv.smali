.class public final Lpvv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpvv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Lpxa;

.field public e:Ljava/lang/Float;

.field public f:Ljava/lang/Float;

.field public g:Ljava/lang/Float;

.field public h:Ljava/lang/Float;

.field public i:Ljava/lang/Float;

.field public j:Ljava/lang/Float;

.field public k:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x28ef1ba

    new-instance v1, Lpvw;

    invoke-direct {v1}, Lpvw;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpvv;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lpvv;->d:Lpxa;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 77
    iget-object v1, p0, Lpvv;->b:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 78
    const/4 v0, 0x1

    iget-object v1, p0, Lpvv;->b:Ljava/lang/Float;

    .line 79
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 81
    :cond_0
    iget-object v1, p0, Lpvv;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 82
    const/4 v1, 0x2

    iget-object v2, p0, Lpvv;->c:Ljava/lang/Float;

    .line 83
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 85
    :cond_1
    iget-object v1, p0, Lpvv;->d:Lpxa;

    if-eqz v1, :cond_2

    .line 86
    const/4 v1, 0x3

    iget-object v2, p0, Lpvv;->d:Lpxa;

    .line 87
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_2
    iget-object v1, p0, Lpvv;->e:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 90
    const/4 v1, 0x4

    iget-object v2, p0, Lpvv;->e:Ljava/lang/Float;

    .line 91
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 93
    :cond_3
    iget-object v1, p0, Lpvv;->f:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 94
    const/4 v1, 0x5

    iget-object v2, p0, Lpvv;->f:Ljava/lang/Float;

    .line 95
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 97
    :cond_4
    iget-object v1, p0, Lpvv;->g:Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 98
    const/4 v1, 0x6

    iget-object v2, p0, Lpvv;->g:Ljava/lang/Float;

    .line 99
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 101
    :cond_5
    iget-object v1, p0, Lpvv;->h:Ljava/lang/Float;

    if-eqz v1, :cond_6

    .line 102
    const/4 v1, 0x7

    iget-object v2, p0, Lpvv;->h:Ljava/lang/Float;

    .line 103
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 105
    :cond_6
    iget-object v1, p0, Lpvv;->i:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 106
    const/16 v1, 0x8

    iget-object v2, p0, Lpvv;->i:Ljava/lang/Float;

    .line 107
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 109
    :cond_7
    iget-object v1, p0, Lpvv;->j:Ljava/lang/Float;

    if-eqz v1, :cond_8

    .line 110
    const/16 v1, 0x9

    iget-object v2, p0, Lpvv;->j:Ljava/lang/Float;

    .line 111
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 113
    :cond_8
    iget-object v1, p0, Lpvv;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 114
    const/16 v1, 0xa

    iget-object v2, p0, Lpvv;->k:Ljava/lang/Integer;

    .line 115
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    :cond_9
    iget-object v1, p0, Lpvv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    iput v0, p0, Lpvv;->ai:I

    .line 119
    return v0
.end method

.method public a(Loxn;)Lpvv;
    .locals 2

    .prologue
    .line 127
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 128
    sparse-switch v0, :sswitch_data_0

    .line 132
    iget-object v1, p0, Lpvv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 133
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpvv;->ah:Ljava/util/List;

    .line 136
    :cond_1
    iget-object v1, p0, Lpvv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    :sswitch_0
    return-object p0

    .line 143
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpvv;->b:Ljava/lang/Float;

    goto :goto_0

    .line 147
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpvv;->c:Ljava/lang/Float;

    goto :goto_0

    .line 151
    :sswitch_3
    iget-object v0, p0, Lpvv;->d:Lpxa;

    if-nez v0, :cond_2

    .line 152
    new-instance v0, Lpxa;

    invoke-direct {v0}, Lpxa;-><init>()V

    iput-object v0, p0, Lpvv;->d:Lpxa;

    .line 154
    :cond_2
    iget-object v0, p0, Lpvv;->d:Lpxa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 158
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpvv;->e:Ljava/lang/Float;

    goto :goto_0

    .line 162
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpvv;->f:Ljava/lang/Float;

    goto :goto_0

    .line 166
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpvv;->g:Ljava/lang/Float;

    goto :goto_0

    .line 170
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpvv;->h:Ljava/lang/Float;

    goto :goto_0

    .line 174
    :sswitch_8
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpvv;->i:Ljava/lang/Float;

    goto :goto_0

    .line 178
    :sswitch_9
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpvv;->j:Ljava/lang/Float;

    goto/16 :goto_0

    .line 182
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpvv;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 128
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
        0x4d -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lpvv;->b:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x1

    iget-object v1, p0, Lpvv;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 43
    :cond_0
    iget-object v0, p0, Lpvv;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 44
    const/4 v0, 0x2

    iget-object v1, p0, Lpvv;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 46
    :cond_1
    iget-object v0, p0, Lpvv;->d:Lpxa;

    if-eqz v0, :cond_2

    .line 47
    const/4 v0, 0x3

    iget-object v1, p0, Lpvv;->d:Lpxa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 49
    :cond_2
    iget-object v0, p0, Lpvv;->e:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 50
    const/4 v0, 0x4

    iget-object v1, p0, Lpvv;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 52
    :cond_3
    iget-object v0, p0, Lpvv;->f:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 53
    const/4 v0, 0x5

    iget-object v1, p0, Lpvv;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 55
    :cond_4
    iget-object v0, p0, Lpvv;->g:Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 56
    const/4 v0, 0x6

    iget-object v1, p0, Lpvv;->g:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 58
    :cond_5
    iget-object v0, p0, Lpvv;->h:Ljava/lang/Float;

    if-eqz v0, :cond_6

    .line 59
    const/4 v0, 0x7

    iget-object v1, p0, Lpvv;->h:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 61
    :cond_6
    iget-object v0, p0, Lpvv;->i:Ljava/lang/Float;

    if-eqz v0, :cond_7

    .line 62
    const/16 v0, 0x8

    iget-object v1, p0, Lpvv;->i:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 64
    :cond_7
    iget-object v0, p0, Lpvv;->j:Ljava/lang/Float;

    if-eqz v0, :cond_8

    .line 65
    const/16 v0, 0x9

    iget-object v1, p0, Lpvv;->j:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 67
    :cond_8
    iget-object v0, p0, Lpvv;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 68
    const/16 v0, 0xa

    iget-object v1, p0, Lpvv;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 70
    :cond_9
    iget-object v0, p0, Lpvv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 72
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpvv;->a(Loxn;)Lpvv;

    move-result-object v0

    return-object v0
.end method
