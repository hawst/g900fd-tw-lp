.class public final Lpsx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpsz;

.field private b:Lpsz;

.field private c:Lpsz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21
    iput-object v0, p0, Lpsx;->b:Lpsz;

    .line 24
    iput-object v0, p0, Lpsx;->a:Lpsz;

    .line 27
    iput-object v0, p0, Lpsx;->c:Lpsz;

    .line 18
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x0

    .line 48
    iget-object v1, p0, Lpsx;->b:Lpsz;

    if-eqz v1, :cond_0

    .line 49
    const/4 v0, 0x1

    iget-object v1, p0, Lpsx;->b:Lpsz;

    .line 50
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 52
    :cond_0
    iget-object v1, p0, Lpsx;->a:Lpsz;

    if-eqz v1, :cond_1

    .line 53
    const/4 v1, 0x2

    iget-object v2, p0, Lpsx;->a:Lpsz;

    .line 54
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_1
    iget-object v1, p0, Lpsx;->c:Lpsz;

    if-eqz v1, :cond_2

    .line 57
    const/4 v1, 0x3

    iget-object v2, p0, Lpsx;->c:Lpsz;

    .line 58
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_2
    iget-object v1, p0, Lpsx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    iput v0, p0, Lpsx;->ai:I

    .line 62
    return v0
.end method

.method public a(Loxn;)Lpsx;
    .locals 2

    .prologue
    .line 70
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 71
    sparse-switch v0, :sswitch_data_0

    .line 75
    iget-object v1, p0, Lpsx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 76
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpsx;->ah:Ljava/util/List;

    .line 79
    :cond_1
    iget-object v1, p0, Lpsx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    :sswitch_0
    return-object p0

    .line 86
    :sswitch_1
    iget-object v0, p0, Lpsx;->b:Lpsz;

    if-nez v0, :cond_2

    .line 87
    new-instance v0, Lpsz;

    invoke-direct {v0}, Lpsz;-><init>()V

    iput-object v0, p0, Lpsx;->b:Lpsz;

    .line 89
    :cond_2
    iget-object v0, p0, Lpsx;->b:Lpsz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 93
    :sswitch_2
    iget-object v0, p0, Lpsx;->a:Lpsz;

    if-nez v0, :cond_3

    .line 94
    new-instance v0, Lpsz;

    invoke-direct {v0}, Lpsz;-><init>()V

    iput-object v0, p0, Lpsx;->a:Lpsz;

    .line 96
    :cond_3
    iget-object v0, p0, Lpsx;->a:Lpsz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 100
    :sswitch_3
    iget-object v0, p0, Lpsx;->c:Lpsz;

    if-nez v0, :cond_4

    .line 101
    new-instance v0, Lpsz;

    invoke-direct {v0}, Lpsz;-><init>()V

    iput-object v0, p0, Lpsx;->c:Lpsz;

    .line 103
    :cond_4
    iget-object v0, p0, Lpsx;->c:Lpsz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lpsx;->b:Lpsz;

    if-eqz v0, :cond_0

    .line 33
    const/4 v0, 0x1

    iget-object v1, p0, Lpsx;->b:Lpsz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 35
    :cond_0
    iget-object v0, p0, Lpsx;->a:Lpsz;

    if-eqz v0, :cond_1

    .line 36
    const/4 v0, 0x2

    iget-object v1, p0, Lpsx;->a:Lpsz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 38
    :cond_1
    iget-object v0, p0, Lpsx;->c:Lpsz;

    if-eqz v0, :cond_2

    .line 39
    const/4 v0, 0x3

    iget-object v1, p0, Lpsx;->c:Lpsz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 41
    :cond_2
    iget-object v0, p0, Lpsx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 43
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lpsx;->a(Loxn;)Lpsx;

    move-result-object v0

    return-object v0
.end method
