.class public final Lduf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;"
        }
    .end annotation
.end field

.field private static c:J


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v11, 0x2

    const/4 v5, 0x1

    .line 34
    const-wide/16 v0, 0x0

    sput-wide v0, Lduf;->c:J

    .line 50
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 51
    sput-object v8, Lduf;->a:Ljava/util/ArrayList;

    new-instance v0, Ldud;

    const v1, 0x7f0a08bd

    const v2, 0x7f0a08be

    const v3, 0x7f0202bd

    const/16 v4, 0x8

    const/4 v7, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v7}, Ldud;-><init>(IIIIIIZ)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    sget-object v0, Lduf;->a:Ljava/util/ArrayList;

    new-instance v1, Ldud;

    const v2, 0x7f0a08b5

    const v3, 0x7f0a08b6

    const v4, 0x7f02015e

    const/16 v7, 0x32

    move v6, v11

    move v8, v5

    invoke-direct/range {v1 .. v8}, Ldud;-><init>(IIIIIIZ)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v0, Lduf;->a:Ljava/util/ArrayList;

    new-instance v6, Ldud;

    const v7, 0x7f0a08b8

    const v8, 0x7f0a08b9

    const v9, 0x7f0202a8

    const/4 v10, 0x4

    const/16 v12, 0x9

    move v13, v5

    invoke-direct/range {v6 .. v13}, Ldud;-><init>(IIIIIIZ)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;I)Landroid/content/Intent;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;I)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 174
    invoke-static {p3, p4}, Lduf;->a(Ljava/util/ArrayList;I)Ldui;

    move-result-object v1

    .line 176
    const/16 v0, 0x14

    .line 178
    invoke-interface {v1}, Ldui;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    const/16 v0, 0x1d

    .line 183
    :cond_0
    new-instance v2, Ljuj;

    const-class v3, Lcom/google/android/apps/plus/phone/HostSingleAlbumTileActivity;

    invoke-direct {v2, p0, v3, p1}, Ljuj;-><init>(Landroid/content/Context;Ljava/lang/Class;I)V

    .line 184
    invoke-virtual {v2, p2}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v2

    const/4 v3, 0x2

    .line 185
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljuj;->a(Ljava/lang/Integer;)Ljuj;

    move-result-object v2

    .line 186
    invoke-virtual {v2, v4}, Ljuj;->a(Z)Ljuj;

    move-result-object v2

    .line 187
    invoke-virtual {v2, v4}, Ljuj;->b(Z)Ljuj;

    move-result-object v2

    .line 188
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljuj;->c(Ljava/lang/Integer;)Ljuj;

    move-result-object v2

    .line 190
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0b4e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 189
    invoke-virtual {v2, v3}, Ljuj;->e(Ljava/lang/String;)Ljuj;

    move-result-object v2

    .line 191
    invoke-interface {v1}, Ldui;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljuj;->c(I)Ljuj;

    move-result-object v2

    .line 192
    invoke-interface {v1}, Ldui;->e()I

    move-result v1

    invoke-virtual {v2, v1}, Ljuj;->d(I)Ljuj;

    move-result-object v1

    .line 193
    invoke-virtual {v1, v0}, Ljuj;->e(I)Ljuj;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    .line 195
    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/util/ArrayList;I)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;I)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 149
    invoke-static {p2, p3}, Lduf;->a(Ljava/util/ArrayList;I)Ldui;

    move-result-object v1

    .line 151
    const/16 v0, 0x14

    .line 153
    invoke-interface {v1}, Ldui;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    const/16 v0, 0x1d

    .line 158
    :cond_0
    new-instance v2, Leyt;

    const-class v3, Lcom/google/android/apps/plus/phone/SelectFromHomePhotosActivity;

    invoke-direct {v2, p0, v3, p1}, Leyt;-><init>(Landroid/content/Context;Ljava/lang/Class;I)V

    const/4 v3, 0x2

    .line 159
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Leyt;->a(Ljava/lang/Integer;)Leyt;

    move-result-object v2

    const/4 v3, 0x6

    .line 160
    invoke-virtual {v2, v3}, Leyt;->a(I)Leyt;

    move-result-object v2

    .line 161
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Leyt;->b(Ljava/lang/Integer;)Leyt;

    move-result-object v0

    const/4 v2, 0x1

    .line 162
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Leyt;->a(Ljava/lang/Boolean;)Leyt;

    move-result-object v0

    const/4 v2, 0x0

    .line 163
    invoke-virtual {v0, v2}, Leyt;->c(Z)Leyt;

    move-result-object v0

    .line 165
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0b4e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 164
    invoke-virtual {v0, v2}, Leyt;->a(Ljava/lang/String;)Leyt;

    move-result-object v0

    .line 166
    invoke-interface {v1}, Ldui;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Leyt;->e(I)Leyt;

    move-result-object v0

    .line 167
    invoke-interface {v1}, Ldui;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Leyt;->f(I)Leyt;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Leyt;->a()Landroid/content/Intent;

    move-result-object v0

    .line 169
    return-object v0
.end method

.method public static a(Landroid/content/Context;ZI)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 138
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/photos/phone/ManualAwesomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    const-string v1, "show_movie"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 140
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 141
    return-object v0
.end method

.method public static a(Ljava/util/ArrayList;I)Ldui;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;I)",
            "Ldui;"
        }
    .end annotation

    .prologue
    .line 203
    if-nez p0, :cond_0

    .line 204
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Manual awesome types not initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldui;

    .line 207
    invoke-interface {v0}, Ldui;->c()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 208
    return-object v0

    .line 211
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot recognize render type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(I)Lhmv;
    .locals 1

    .prologue
    .line 262
    packed-switch p0, :pswitch_data_0

    .line 268
    :pswitch_0
    sget-object v0, Lhmv;->eP:Lhmv;

    :goto_0
    return-object v0

    .line 264
    :pswitch_1
    sget-object v0, Lhmv;->eN:Lhmv;

    goto :goto_0

    .line 266
    :pswitch_2
    sget-object v0, Lhmv;->eO:Lhmv;

    goto :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;I)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 219
    invoke-static {p1, p2}, Lduf;->a(Ljava/util/ArrayList;I)Ldui;

    move-result-object v0

    .line 220
    invoke-interface {v0}, Ldui;->d()I

    move-result v1

    .line 221
    invoke-interface {v0}, Ldui;->e()I

    move-result v2

    .line 222
    invoke-interface {v0}, Ldui;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    if-ne v1, v2, :cond_0

    .line 224
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110078

    new-array v3, v4, [Ljava/lang/Object;

    .line 225
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 224
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 235
    :goto_0
    return-object v0

    .line 227
    :cond_0
    const v0, 0x7f0a0b38

    new-array v3, v3, [Ljava/lang/Object;

    .line 228
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    .line 227
    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 231
    :cond_1
    if-ne v1, v2, :cond_2

    .line 232
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110079

    new-array v3, v4, [Ljava/lang/Object;

    .line 233
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 232
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 235
    :cond_2
    const v0, 0x7f0a0b39

    new-array v3, v3, [Ljava/lang/Object;

    .line 236
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    .line 235
    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Z)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lduf;->c:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x112a880

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 71
    sput-object v0, Lduf;->b:Ljava/util/ArrayList;

    .line 73
    :cond_0
    sget-object v1, Lduf;->b:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 86
    :goto_0
    return-object v0

    .line 76
    :cond_1
    if-eqz p0, :cond_2

    .line 77
    sget-object v0, Lduf;->b:Ljava/util/ArrayList;

    goto :goto_0

    .line 79
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 81
    sget-object v0, Lduf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldui;

    .line 82
    invoke-interface {v0}, Ldui;->c()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_3

    .line 83
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 86
    goto :goto_0
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 110
    const-wide/16 v0, 0x0

    sput-wide v0, Lduf;->c:J

    .line 111
    return-void
.end method

.method public static a(Ljava/util/ArrayList;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-static {p0, p1}, Lduf;->b(Ljava/util/ArrayList;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    sput-object p0, Lduf;->b:Ljava/util/ArrayList;

    .line 103
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lduf;->c:J

    .line 104
    return-void

    .line 98
    :cond_1
    sget-object v0, Lduf;->a:Ljava/util/ArrayList;

    .line 99
    sput-object v0, Lduf;->b:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Lduf;->b(Ljava/util/ArrayList;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid default manual awesome types"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Landroid/content/Context;Ljava/util/ArrayList;I)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 246
    invoke-static {p1, p2}, Lduf;->a(Ljava/util/ArrayList;I)Ldui;

    move-result-object v0

    invoke-interface {v0, p0}, Ldui;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/ArrayList;Landroid/content/Context;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;",
            "Landroid/content/Context;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 115
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 130
    :goto_0
    return v0

    .line 118
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldui;

    .line 120
    invoke-interface {v0}, Ldui;->c()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ldui;->c()I

    move-result v3

    if-ltz v3, :cond_4

    .line 121
    invoke-interface {v0}, Ldui;->d()I

    move-result v3

    if-lez v3, :cond_4

    invoke-interface {v0}, Ldui;->d()I

    move-result v3

    invoke-interface {v0}, Ldui;->e()I

    move-result v4

    if-gt v3, v4, :cond_4

    .line 122
    invoke-interface {v0, p1}, Ldui;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 123
    invoke-interface {v0, p1}, Ldui;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 124
    invoke-interface {v0}, Ldui;->a()I

    move-result v3

    if-ltz v3, :cond_3

    invoke-interface {v0}, Ldui;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 125
    :cond_3
    invoke-interface {v0}, Ldui;->a()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    .line 126
    invoke-interface {v0}, Ldui;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_4
    move v0, v1

    .line 127
    goto :goto_0

    .line 130
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljava/util/ArrayList;I)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ldui;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 254
    const v0, 0x7f0a0b37

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 255
    invoke-static {p1, p2}, Lduf;->a(Ljava/util/ArrayList;I)Ldui;

    move-result-object v3

    invoke-interface {v3, p0}, Ldui;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 254
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
