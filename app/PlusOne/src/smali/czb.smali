.class public Lczb;
.super Llol;
.source "PG"

# interfaces
.implements Lhob;


# static fields
.field private static P:Z

.field private static Q:Z


# instance fields
.field private N:I

.field private O:Ldeo;

.field private R:Landroid/view/View;

.field private final S:Lhoc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    sput-boolean v0, Lczb;->P:Z

    .line 44
    sput-boolean v0, Lczb;->Q:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0}, Llol;-><init>()V

    .line 48
    new-instance v0, Lhoc;

    .line 49
    invoke-virtual {p0}, Lczb;->z_()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lczb;->S:Lhoc;

    .line 52
    new-instance v0, Ldep;

    iget-object v1, p0, Lczb;->av:Llqm;

    new-instance v2, Lczc;

    invoke-direct {v2, p0}, Lczc;-><init>(Lczb;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 58
    return-void
.end method

.method static synthetic a(Lczb;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lczb;->d()V

    return-void
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 32
    sget-boolean v0, Lczb;->Q:Z

    return v0
.end method

.method static synthetic a(Lczb;Lovf;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 32
    if-eqz p1, :cond_2

    const-string v2, "PhotoAutoEnhancePromo"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lovf;->a:[Louz;

    array-length v2, v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Number of promos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v3, p1, Lovf;->a:[Louz;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    iget v5, v5, Louz;->b:I

    if-ne v5, v0, :cond_1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 32
    sput-boolean p0, Lczb;->P:Z

    return p0
.end method

.method static synthetic b(Lczb;)Llnl;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lczb;->at:Llnl;

    return-object v0
.end method

.method static synthetic b(Z)Z
    .locals 0

    .prologue
    .line 32
    sput-boolean p0, Lczb;->Q:Z

    return p0
.end method

.method static synthetic c(Lczb;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lczb;->N:I

    return v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 32
    sget-boolean v0, Lczb;->P:Z

    return v0
.end method

.method static synthetic d(Lczb;)Llnl;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lczb;->at:Llnl;

    return-object v0
.end method

.method private d()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 123
    iget-object v0, p0, Lczb;->O:Ldeo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczb;->O:Ldeo;

    .line 124
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczb;->O:Ldeo;

    .line 125
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v0

    if-nez v0, :cond_2

    .line 126
    :cond_0
    iget-object v0, p0, Lczb;->R:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lczb;->R:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 196
    :cond_1
    :goto_0
    return-void

    .line 131
    :cond_2
    iget-object v0, p0, Lczb;->O:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v3

    .line 133
    iget-object v0, p0, Lczb;->O:Ldeo;

    invoke-virtual {v0}, Ldeo;->c()Z

    move-result v4

    .line 134
    invoke-interface {v3}, Lddl;->e()Lnzi;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 135
    invoke-interface {v3}, Lddl;->e()Lnzi;

    move-result-object v0

    invoke-interface {v3}, Lddl;->f()Lnzi;

    move-result-object v5

    if-ne v0, v5, :cond_4

    move v0, v1

    .line 137
    :goto_1
    invoke-interface {v3}, Lddl;->g()Lnym;

    move-result-object v5

    .line 138
    invoke-interface {v3}, Lddl;->a()Lizu;

    move-result-object v6

    invoke-virtual {v6}, Lizu;->g()Ljac;

    move-result-object v6

    .line 139
    invoke-interface {v3}, Lddl;->e()Lnzi;

    move-result-object v7

    invoke-static {v7}, Ljub;->b(Lnzi;)Z

    move-result v7

    .line 137
    invoke-static {v5, v6, v4, v0, v7}, Ljvd;->a(Lnym;Ljac;ZZZ)I

    move-result v0

    .line 141
    const/4 v4, 0x2

    if-ne v0, v4, :cond_5

    .line 144
    :goto_2
    invoke-interface {v3}, Lddl;->g()Lnym;

    move-result-object v0

    iget-object v0, v0, Lnym;->P:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 145
    invoke-interface {v3}, Lddl;->g()Lnym;

    move-result-object v0

    iget-object v0, v0, Lnym;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 147
    :goto_3
    sget-boolean v3, Lczb;->P:Z

    .line 159
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    sget-boolean v0, Lczb;->P:Z

    if-eqz v0, :cond_3

    .line 165
    iget-object v0, p0, Lczb;->O:Ldeo;

    invoke-virtual {v0}, Ldeo;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 166
    iget-object v0, p0, Lczb;->R:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 173
    iget-object v0, p0, Lczb;->S:Lhoc;

    new-instance v1, Lcze;

    iget-object v2, p0, Lczb;->at:Llnl;

    const-string v3, "SendAutoEnhancePromo"

    invoke-direct {v1, p0, v2, v3}, Lcze;-><init>(Lczb;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 194
    :cond_3
    iget-object v0, p0, Lczb;->R:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 135
    goto :goto_1

    :cond_5
    move v1, v2

    .line 141
    goto :goto_2

    .line 189
    :cond_6
    iget-object v0, p0, Lczb;->R:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_3
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 92
    const v0, 0x7f040188

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lczb;->R:Landroid/view/View;

    .line 95
    iget-object v0, p0, Lczb;->S:Lhoc;

    new-instance v1, Lczd;

    iget-object v2, p0, Lczb;->at:Llnl;

    const-string v3, "CheckAutoEnhancePromo"

    invoke-direct {v1, p0, v2, v3}, Lczd;-><init>(Lczb;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 113
    iget-object v0, p0, Lczb;->R:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lczb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lczb;->N:I

    .line 70
    iget-object v0, p0, Lczb;->S:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 71
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 201
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lhos;->a(Z)V

    .line 202
    const-string v0, "CheckAutoEnhancePromo"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    invoke-direct {p0}, Lczb;->d()V

    .line 204
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    const-string v0, "PhotoAutoEnhancePromo"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    const-string v0, "SendAutoEnhancePromo"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const-string v0, "PhotoAutoEnhancePromo"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    goto :goto_0
.end method

.method public aO_()V
    .locals 0

    .prologue
    .line 118
    invoke-super {p0}, Llol;->aO_()V

    .line 119
    invoke-direct {p0}, Lczb;->d()V

    .line 120
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 63
    iget-object v0, p0, Lczb;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lczb;->O:Ldeo;

    .line 64
    return-void
.end method
