.class public final Lnyw;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lnyx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6484
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6489
    sget-object v0, Lnyx;->a:[Lnyx;

    iput-object v0, p0, Lnyw;->b:[Lnyx;

    .line 6484
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 6510
    .line 6511
    iget-object v0, p0, Lnyw;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6512
    const/4 v0, 0x1

    iget-object v2, p0, Lnyw;->a:Ljava/lang/String;

    .line 6513
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6515
    :goto_0
    iget-object v2, p0, Lnyw;->b:[Lnyx;

    if-eqz v2, :cond_1

    .line 6516
    iget-object v2, p0, Lnyw;->b:[Lnyx;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6517
    if-eqz v4, :cond_0

    .line 6518
    const/4 v5, 0x2

    .line 6519
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6516
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6523
    :cond_1
    iget-object v1, p0, Lnyw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6524
    iput v0, p0, Lnyw;->ai:I

    .line 6525
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnyw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6533
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6534
    sparse-switch v0, :sswitch_data_0

    .line 6538
    iget-object v2, p0, Lnyw;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 6539
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnyw;->ah:Ljava/util/List;

    .line 6542
    :cond_1
    iget-object v2, p0, Lnyw;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6544
    :sswitch_0
    return-object p0

    .line 6549
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyw;->a:Ljava/lang/String;

    goto :goto_0

    .line 6553
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6554
    iget-object v0, p0, Lnyw;->b:[Lnyx;

    if-nez v0, :cond_3

    move v0, v1

    .line 6555
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyx;

    .line 6556
    iget-object v3, p0, Lnyw;->b:[Lnyx;

    if-eqz v3, :cond_2

    .line 6557
    iget-object v3, p0, Lnyw;->b:[Lnyx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6559
    :cond_2
    iput-object v2, p0, Lnyw;->b:[Lnyx;

    .line 6560
    :goto_2
    iget-object v2, p0, Lnyw;->b:[Lnyx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 6561
    iget-object v2, p0, Lnyw;->b:[Lnyx;

    new-instance v3, Lnyx;

    invoke-direct {v3}, Lnyx;-><init>()V

    aput-object v3, v2, v0

    .line 6562
    iget-object v2, p0, Lnyw;->b:[Lnyx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6563
    invoke-virtual {p1}, Loxn;->a()I

    .line 6560
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6554
    :cond_3
    iget-object v0, p0, Lnyw;->b:[Lnyx;

    array-length v0, v0

    goto :goto_1

    .line 6566
    :cond_4
    iget-object v2, p0, Lnyw;->b:[Lnyx;

    new-instance v3, Lnyx;

    invoke-direct {v3}, Lnyx;-><init>()V

    aput-object v3, v2, v0

    .line 6567
    iget-object v2, p0, Lnyw;->b:[Lnyx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6534
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 6494
    iget-object v0, p0, Lnyw;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 6495
    const/4 v0, 0x1

    iget-object v1, p0, Lnyw;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6497
    :cond_0
    iget-object v0, p0, Lnyw;->b:[Lnyx;

    if-eqz v0, :cond_2

    .line 6498
    iget-object v1, p0, Lnyw;->b:[Lnyx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 6499
    if-eqz v3, :cond_1

    .line 6500
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 6498
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6504
    :cond_2
    iget-object v0, p0, Lnyw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6506
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6480
    invoke-virtual {p0, p1}, Lnyw;->a(Loxn;)Lnyw;

    move-result-object v0

    return-object v0
.end method
