.class public final Llvw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Llvv;

.field private b:Llvs;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12
    iput-object v0, p0, Llvw;->a:Llvv;

    .line 15
    iput-object v0, p0, Llvw;->b:Llvs;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x0

    .line 33
    iget-object v1, p0, Llvw;->a:Llvv;

    if-eqz v1, :cond_0

    .line 34
    const/4 v0, 0x1

    iget-object v1, p0, Llvw;->a:Llvv;

    .line 35
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 37
    :cond_0
    iget-object v1, p0, Llvw;->b:Llvs;

    if-eqz v1, :cond_1

    .line 38
    const/4 v1, 0x2

    iget-object v2, p0, Llvw;->b:Llvs;

    .line 39
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    :cond_1
    iget-object v1, p0, Llvw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    iput v0, p0, Llvw;->ai:I

    .line 43
    return v0
.end method

.method public a(Loxn;)Llvw;
    .locals 2

    .prologue
    .line 51
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 52
    sparse-switch v0, :sswitch_data_0

    .line 56
    iget-object v1, p0, Llvw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llvw;->ah:Ljava/util/List;

    .line 60
    :cond_1
    iget-object v1, p0, Llvw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    :sswitch_0
    return-object p0

    .line 67
    :sswitch_1
    iget-object v0, p0, Llvw;->a:Llvv;

    if-nez v0, :cond_2

    .line 68
    new-instance v0, Llvv;

    invoke-direct {v0}, Llvv;-><init>()V

    iput-object v0, p0, Llvw;->a:Llvv;

    .line 70
    :cond_2
    iget-object v0, p0, Llvw;->a:Llvv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 74
    :sswitch_2
    iget-object v0, p0, Llvw;->b:Llvs;

    if-nez v0, :cond_3

    .line 75
    new-instance v0, Llvs;

    invoke-direct {v0}, Llvs;-><init>()V

    iput-object v0, p0, Llvw;->b:Llvs;

    .line 77
    :cond_3
    iget-object v0, p0, Llvw;->b:Llvs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 52
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Llvw;->a:Llvv;

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x1

    iget-object v1, p0, Llvw;->a:Llvv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23
    :cond_0
    iget-object v0, p0, Llvw;->b:Llvs;

    if-eqz v0, :cond_1

    .line 24
    const/4 v0, 0x2

    iget-object v1, p0, Llvw;->b:Llvs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26
    :cond_1
    iget-object v0, p0, Llvw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 28
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llvw;->a(Loxn;)Llvw;

    move-result-object v0

    return-object v0
.end method
