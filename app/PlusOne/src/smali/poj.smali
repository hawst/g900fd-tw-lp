.class public final Lpoj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpoh;

.field private b:Lpoh;

.field private c:Lppg;

.field private d:Lppg;

.field private e:Lpog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 312
    invoke-direct {p0}, Loxq;-><init>()V

    .line 315
    iput-object v0, p0, Lpoj;->a:Lpoh;

    .line 318
    iput-object v0, p0, Lpoj;->b:Lpoh;

    .line 321
    iput-object v0, p0, Lpoj;->c:Lppg;

    .line 324
    iput-object v0, p0, Lpoj;->d:Lppg;

    .line 327
    iput-object v0, p0, Lpoj;->e:Lpog;

    .line 312
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 353
    const/4 v0, 0x0

    .line 354
    iget-object v1, p0, Lpoj;->a:Lpoh;

    if-eqz v1, :cond_0

    .line 355
    const/4 v0, 0x1

    iget-object v1, p0, Lpoj;->a:Lpoh;

    .line 356
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 358
    :cond_0
    iget-object v1, p0, Lpoj;->b:Lpoh;

    if-eqz v1, :cond_1

    .line 359
    const/4 v1, 0x2

    iget-object v2, p0, Lpoj;->b:Lpoh;

    .line 360
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    :cond_1
    iget-object v1, p0, Lpoj;->d:Lppg;

    if-eqz v1, :cond_2

    .line 363
    const/4 v1, 0x3

    iget-object v2, p0, Lpoj;->d:Lppg;

    .line 364
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    :cond_2
    iget-object v1, p0, Lpoj;->e:Lpog;

    if-eqz v1, :cond_3

    .line 367
    const/4 v1, 0x4

    iget-object v2, p0, Lpoj;->e:Lpog;

    .line 368
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 370
    :cond_3
    iget-object v1, p0, Lpoj;->c:Lppg;

    if-eqz v1, :cond_4

    .line 371
    const/4 v1, 0x5

    iget-object v2, p0, Lpoj;->c:Lppg;

    .line 372
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 374
    :cond_4
    iget-object v1, p0, Lpoj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    iput v0, p0, Lpoj;->ai:I

    .line 376
    return v0
.end method

.method public a(Loxn;)Lpoj;
    .locals 2

    .prologue
    .line 384
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 385
    sparse-switch v0, :sswitch_data_0

    .line 389
    iget-object v1, p0, Lpoj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 390
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpoj;->ah:Ljava/util/List;

    .line 393
    :cond_1
    iget-object v1, p0, Lpoj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    :sswitch_0
    return-object p0

    .line 400
    :sswitch_1
    iget-object v0, p0, Lpoj;->a:Lpoh;

    if-nez v0, :cond_2

    .line 401
    new-instance v0, Lpoh;

    invoke-direct {v0}, Lpoh;-><init>()V

    iput-object v0, p0, Lpoj;->a:Lpoh;

    .line 403
    :cond_2
    iget-object v0, p0, Lpoj;->a:Lpoh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 407
    :sswitch_2
    iget-object v0, p0, Lpoj;->b:Lpoh;

    if-nez v0, :cond_3

    .line 408
    new-instance v0, Lpoh;

    invoke-direct {v0}, Lpoh;-><init>()V

    iput-object v0, p0, Lpoj;->b:Lpoh;

    .line 410
    :cond_3
    iget-object v0, p0, Lpoj;->b:Lpoh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 414
    :sswitch_3
    iget-object v0, p0, Lpoj;->d:Lppg;

    if-nez v0, :cond_4

    .line 415
    new-instance v0, Lppg;

    invoke-direct {v0}, Lppg;-><init>()V

    iput-object v0, p0, Lpoj;->d:Lppg;

    .line 417
    :cond_4
    iget-object v0, p0, Lpoj;->d:Lppg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 421
    :sswitch_4
    iget-object v0, p0, Lpoj;->e:Lpog;

    if-nez v0, :cond_5

    .line 422
    new-instance v0, Lpog;

    invoke-direct {v0}, Lpog;-><init>()V

    iput-object v0, p0, Lpoj;->e:Lpog;

    .line 424
    :cond_5
    iget-object v0, p0, Lpoj;->e:Lpog;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 428
    :sswitch_5
    iget-object v0, p0, Lpoj;->c:Lppg;

    if-nez v0, :cond_6

    .line 429
    new-instance v0, Lppg;

    invoke-direct {v0}, Lppg;-><init>()V

    iput-object v0, p0, Lpoj;->c:Lppg;

    .line 431
    :cond_6
    iget-object v0, p0, Lpoj;->c:Lppg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 385
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lpoj;->a:Lpoh;

    if-eqz v0, :cond_0

    .line 333
    const/4 v0, 0x1

    iget-object v1, p0, Lpoj;->a:Lpoh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 335
    :cond_0
    iget-object v0, p0, Lpoj;->b:Lpoh;

    if-eqz v0, :cond_1

    .line 336
    const/4 v0, 0x2

    iget-object v1, p0, Lpoj;->b:Lpoh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 338
    :cond_1
    iget-object v0, p0, Lpoj;->d:Lppg;

    if-eqz v0, :cond_2

    .line 339
    const/4 v0, 0x3

    iget-object v1, p0, Lpoj;->d:Lppg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 341
    :cond_2
    iget-object v0, p0, Lpoj;->e:Lpog;

    if-eqz v0, :cond_3

    .line 342
    const/4 v0, 0x4

    iget-object v1, p0, Lpoj;->e:Lpog;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 344
    :cond_3
    iget-object v0, p0, Lpoj;->c:Lppg;

    if-eqz v0, :cond_4

    .line 345
    const/4 v0, 0x5

    iget-object v1, p0, Lpoj;->c:Lppg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 347
    :cond_4
    iget-object v0, p0, Lpoj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 349
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 308
    invoke-virtual {p0, p1}, Lpoj;->a(Loxn;)Lpoj;

    move-result-object v0

    return-object v0
.end method
