.class final Lfwc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field private synthetic a:Lfwb;


# direct methods
.method constructor <init>(Lfwb;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lfwc;->a:Lfwb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lfwc;->a:Lfwb;

    invoke-static {v0, p1}, Lfwb;->a(Lfwb;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 50
    iget-object v0, p0, Lfwc;->a:Lfwb;

    invoke-static {v0}, Lfwb;->b(Lfwb;)Landroid/view/SurfaceHolder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lfwc;->a:Lfwb;

    invoke-static {v0}, Lfwb;->b(Lfwb;)Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lfwc;->a:Lfwb;

    invoke-static {v0}, Lfwb;->c(Lfwb;)V

    .line 57
    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lfwc;->a:Lfwb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lfwb;->a(Lfwb;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 43
    iget-object v0, p0, Lfwc;->a:Lfwb;

    invoke-static {v0}, Lfwb;->a(Lfwb;)V

    .line 44
    return-void
.end method
