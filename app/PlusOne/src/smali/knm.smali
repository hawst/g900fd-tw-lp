.class public final Lknm;
.super Ldf;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldf",
        "<",
        "Lkmz;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lknk;

.field private final c:I

.field private final d:Lkey;

.field private final e:Ljava/lang/String;

.field private final f:Lkng;

.field private g:Lkmz;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILkey;Ljava/lang/String;Lkng;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Ldf;-><init>(Landroid/content/Context;)V

    .line 23
    iput p2, p0, Lknm;->c:I

    .line 24
    iput-object p3, p0, Lknm;->d:Lkey;

    .line 25
    iput-object p4, p0, Lknm;->e:Ljava/lang/String;

    .line 26
    iput-object p5, p0, Lknm;->f:Lkng;

    .line 27
    const-class v0, Lknk;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lknk;

    iput-object v0, p0, Lknm;->b:Lknk;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lkmz;)V
    .locals 1

    .prologue
    .line 41
    iput-object p1, p0, Lknm;->g:Lkmz;

    .line 42
    invoke-virtual {p0}, Lknm;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-super {p0, p1}, Ldf;->b(Ljava/lang/Object;)V

    .line 45
    :cond_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lkmz;

    invoke-virtual {p0, p1}, Lknm;->a(Lkmz;)V

    return-void
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lknm;->f()Lkmz;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkmz;
    .locals 6

    .prologue
    .line 49
    iget-object v0, p0, Lknm;->b:Lknk;

    iget v1, p0, Lknm;->c:I

    iget-object v2, p0, Lknm;->d:Lkey;

    iget-object v3, p0, Lknm;->e:Ljava/lang/String;

    iget-object v4, p0, Lknm;->f:Lkng;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lknk;->a(ILkey;Ljava/lang/String;Lkng;Z)Lkmz;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lknm;->g:Lkmz;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lknm;->g:Lkmz;

    invoke-virtual {p0, v0}, Lknm;->a(Lkmz;)V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Lknm;->t()V

    goto :goto_0
.end method
