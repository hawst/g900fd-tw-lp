.class public final Lhgt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lhgt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lhgv;

.field private b:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lhfw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    new-instance v0, Lhgu;

    invoke-direct {v0}, Lhgu;-><init>()V

    sput-object v0, Lhgt;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    .line 34
    return-void
.end method

.method public constructor <init>(Lhgw;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    .line 37
    if-nez p1, :cond_1

    .line 67
    :cond_0
    return-void

    .line 41
    :cond_1
    invoke-virtual {p1}, Lhgw;->h()I

    move-result v2

    .line 42
    if-lez v2, :cond_2

    move v1, v0

    .line 43
    :goto_0
    if-ge v1, v2, :cond_2

    .line 44
    new-instance v3, Lhfw;

    invoke-direct {v3}, Lhfw;-><init>()V

    invoke-virtual {p1, v1}, Lhgw;->b(I)Lhxc;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lhfw;->a(Lhxc;I)Lhfw;

    move-result-object v3

    .line 46
    iget-object v4, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lhfw;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50
    :cond_2
    invoke-virtual {p1}, Lhgw;->g()I

    move-result v2

    .line 51
    if-lez v2, :cond_3

    move v1, v0

    .line 52
    :goto_1
    if-ge v1, v2, :cond_3

    .line 53
    new-instance v3, Lhfw;

    invoke-direct {v3}, Lhfw;-><init>()V

    invoke-virtual {p1, v1}, Lhgw;->a(I)Ljqs;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v3, v4, v5}, Lhfw;->a(Ljqs;I)Lhfw;

    move-result-object v3

    .line 55
    iget-object v4, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lhfw;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 59
    :cond_3
    invoke-virtual {p1}, Lhgw;->i()I

    move-result v1

    .line 60
    if-lez v1, :cond_0

    .line 61
    :goto_2
    if-ge v0, v1, :cond_0

    .line 62
    new-instance v2, Lhfw;

    invoke-direct {v2}, Lhfw;-><init>()V

    .line 63
    invoke-virtual {p1, v0}, Lhgw;->c(I)Lkxr;

    move-result-object v3

    invoke-virtual {v2, v3}, Lhfw;->a(Lkxr;)Lhfw;

    move-result-object v2

    .line 64
    iget-object v3, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Lhfw;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method static synthetic a(Lhgt;)Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    return-object v0
.end method


# virtual methods
.method public a(Lhfv;)Lhfv;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Lhhw;

    .line 170
    invoke-virtual {p0}, Lhgt;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1}, Lhfv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lhhw;-><init>(Ljava/util/List;Lhfv;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(I)Lhfw;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lhgt;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfw;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 117
    iget-object v0, p0, Lhgt;->a:Lhgv;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lhgt;->a:Lhgv;

    invoke-interface {v0}, Lhgv;->a()V

    .line 120
    :cond_0
    return-void
.end method

.method public a(Lhfw;)V
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lhgt;->b(Lhfw;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lhfw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v0, p0, Lhgt;->a:Lhgv;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lhgt;->a:Lhgv;

    invoke-interface {v0}, Lhgv;->a()V

    .line 88
    :cond_0
    return-void
.end method

.method public a(Lhgv;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lhgt;->a:Lhgv;

    .line 75
    return-void
.end method

.method public b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lhfw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public b(Lhfw;)Z
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lhfw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    return v0
.end method

.method public c(Lhfw;)V
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lhgt;->b(Lhfw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lhfw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lhgt;->a:Lhgv;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lhgt;->a:Lhgv;

    invoke-interface {v0}, Lhgv;->a()V

    .line 110
    :cond_0
    return-void
.end method

.method public d()Lhgw;
    .locals 8

    .prologue
    .line 148
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 149
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 150
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 151
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v5

    .line 152
    array-length v6, v5

    .line 153
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_3

    .line 154
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    aget-object v7, v5, v1

    invoke-virtual {v0, v7}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfw;

    .line 155
    invoke-virtual {v0}, Lhfw;->e()Ljqs;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 156
    invoke-virtual {v0}, Lhfw;->e()Ljqs;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 157
    :cond_0
    invoke-virtual {v0}, Lhfw;->d()Lhxc;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 158
    invoke-virtual {v0}, Lhfw;->d()Lhxc;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 159
    :cond_1
    invoke-virtual {v0}, Lhfw;->f()Lkxr;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 160
    invoke-virtual {v0}, Lhfw;->f()Lkxr;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 162
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid AclPickerEntry, cannot create audience."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_3
    new-instance v0, Lhgw;

    invoke-direct {v0, v2, v3, v4, v6}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 200
    iget-object v0, p0, Lhgt;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 201
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 202
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 204
    :cond_0
    return-void
.end method
