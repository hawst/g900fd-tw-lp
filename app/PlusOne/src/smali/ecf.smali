.class public final Lecf;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Lbc;
.implements Lcqm;
.implements Lcqw;
.implements Lcrk;
.implements Lcsn;
.implements Lene;
.implements Lept;
.implements Leqz;
.implements Lfyl;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Landroid/view/View$OnLongClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcqm;",
        "Lcqw;",
        "Lcrk;",
        "Lcsn;",
        "Lene",
        "<",
        "Lizu;",
        "Lcom/google/android/libraries/social/media/MediaResource;",
        ">;",
        "Lept;",
        "Leqz;",
        "Lfyl;",
        "Llgs;"
    }
.end annotation


# instance fields
.field private N:Z

.field private aA:[Lepm;

.field private aB:Landroid/view/View;

.field private aC:Ljava/lang/Float;

.field private aD:Lhqa;

.field private final aE:Licq;

.field private final aF:Lfhh;

.field private final ah:Ljava/lang/Runnable;

.field private ai:Lequ;

.field private aj:Lcom/google/android/apps/plus/views/FastScrollListView;

.field private ak:Lcom/google/android/apps/plus/views/FastScrollContainer;

.field private al:Lcqj;

.field private am:Z

.field private an:Z

.field private ao:Z

.field private ap:Ljava/lang/Integer;

.field private aq:Z

.field private ar:Z

.field private as:Lizs;

.field private aw:Landroid/view/animation/Animation;

.field private ax:Landroid/net/Uri;

.field private ay:Lend;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lend",
            "<",
            "Lizu;",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field private az:Ldwg;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 103
    invoke-direct {p0}, Legi;-><init>()V

    .line 143
    new-instance v0, Lecg;

    invoke-direct {v0, p0}, Lecg;-><init>(Lecf;)V

    iput-object v0, p0, Lecf;->ah:Ljava/lang/Runnable;

    .line 153
    new-instance v0, Lequ;

    iget-object v1, p0, Lecf;->av:Llqm;

    invoke-direct {v0, p0, v1, p0, v2}, Lequ;-><init>(Lu;Llqr;Leqz;I)V

    .line 155
    invoke-virtual {v0, v2}, Lequ;->a(Z)Lequ;

    move-result-object v0

    iput-object v0, p0, Lecf;->ai:Lequ;

    .line 177
    new-instance v0, Licq;

    iget-object v1, p0, Lecf;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064c

    .line 178
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lecf;->aE:Licq;

    .line 180
    new-instance v0, Lech;

    invoke-direct {v0, p0}, Lech;-><init>(Lecf;)V

    iput-object v0, p0, Lecf;->aF:Lfhh;

    .line 189
    new-instance v0, Lcsc;

    iget-object v1, p0, Lecf;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcsc;-><init>(Lu;Llqr;I)V

    .line 1111
    return-void
.end method

.method static synthetic a(Lecf;)Lcqj;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lecf;->al:Lcqj;

    return-object v0
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 865
    iget-object v2, p0, Lecf;->ap:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lecf;->ap:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_1

    .line 885
    :cond_0
    :goto_0
    return-void

    .line 868
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lecf;->ap:Ljava/lang/Integer;

    .line 869
    iput-boolean v0, p0, Lecf;->ao:Z

    .line 871
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lecf;->Z:Z

    .line 872
    iget-boolean v0, p0, Lecf;->Z:Z

    if-eqz v0, :cond_2

    .line 873
    invoke-virtual {p0}, Lecf;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 874
    const v2, 0x7f0a07ad

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 875
    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 878
    :cond_2
    invoke-virtual {p0}, Lecf;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lecf;->d(Landroid/view/View;)V

    .line 880
    iget-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    .line 884
    iget-object v0, p0, Lecf;->al:Lcqj;

    invoke-virtual {v0}, Lcqj;->c()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 871
    goto :goto_1
.end method

.method static synthetic a(Lecf;ILfib;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lecf;->a(ILfib;)V

    return-void
.end method

.method static synthetic a(Lecf;Z)Z
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lecf;->N:Z

    return p1
.end method

.method private au()Ljava/lang/String;
    .locals 3

    .prologue
    .line 306
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "owner_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 307
    if-nez v0, :cond_0

    iget-object v1, p0, Lecf;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 308
    iget-object v0, p0, Lecf;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    :cond_0
    return-object v0
.end method

.method private av()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 430
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "mode"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static synthetic b(Lecf;)Llnl;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lecf;->at:Llnl;

    return-object v0
.end method

.method private b(Landroid/database/Cursor;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 730
    if-nez p1, :cond_1

    .line 755
    :cond_0
    :goto_0
    return v0

    .line 734
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 735
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 741
    sget-object v2, Lepn;->d:Lepn;

    invoke-static {v2}, Ldtc;->a(Lepn;)Ljava/lang/String;

    move-result-object v5

    move v2, v0

    .line 745
    :goto_1
    add-int/lit8 v3, v2, 0x1

    const/4 v6, 0x2

    if-gt v2, v6, :cond_3

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_3

    .line 746
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 747
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 748
    invoke-interface {p1, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    goto :goto_0

    .line 750
    :cond_2
    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v3

    .line 751
    goto :goto_1

    .line 753
    :cond_3
    invoke-interface {p1, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    throw v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 489
    if-eqz p2, :cond_0

    .line 490
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0x40000

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 491
    :goto_0
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo_picker_crop_mode"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 495
    if-eqz v0, :cond_2

    .line 496
    iget-object v0, p0, Lecf;->at:Llnl;

    iget-object v2, p0, Lecf;->P:Lhee;

    .line 497
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    if-eqz p1, :cond_1

    .line 496
    :goto_1
    invoke-static {v0, v2, p1}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecf;->Y:Lctq;

    .line 500
    invoke-virtual {v2}, Lctq;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->a(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget v2, p0, Lecf;->aa:I

    .line 501
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->b(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecf;->X:Lctz;

    .line 502
    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->a(Ljcn;)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecf;->ab:Z

    .line 503
    invoke-virtual {v0, v2}, Ljuj;->a(Z)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecf;->ac:Z

    .line 504
    invoke-virtual {v0, v2}, Ljuj;->b(Z)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecf;->ae:Z

    .line 505
    invoke-virtual {v0, v2}, Ljuj;->c(Z)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecf;->af:Ljava/lang/String;

    .line 506
    invoke-virtual {v0, v2}, Ljuj;->c(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 507
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->c(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    .line 509
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "show_autobackup_status"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 508
    invoke-virtual {v0, v1}, Ljuj;->f(Z)Ljuj;

    move-result-object v0

    .line 510
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 538
    :goto_2
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lecf;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->eb:Lhmv;

    .line 539
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 538
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 540
    invoke-virtual {p0, v1}, Lecf;->b(Landroid/content/Intent;)V

    .line 541
    return-void

    :cond_0
    move v0, v1

    .line 490
    goto/16 :goto_0

    .line 498
    :cond_1
    invoke-static {}, Ljvj;->a()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 513
    :cond_2
    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v0

    iget-object v2, p0, Lecf;->P:Lhee;

    .line 514
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 513
    invoke-static {v0, v2}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v0

    .line 515
    invoke-virtual {v0, p1}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecf;->Y:Lctq;

    .line 516
    invoke-virtual {v2}, Lctq;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->a(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget v2, p0, Lecf;->aa:I

    .line 517
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->b(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecf;->X:Lctz;

    .line 518
    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljuj;->a(Ljcn;)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecf;->ab:Z

    .line 519
    invoke-virtual {v0, v2}, Ljuj;->a(Z)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecf;->ac:Z

    .line 520
    invoke-virtual {v0, v2}, Ljuj;->b(Z)Ljuj;

    move-result-object v0

    iget-boolean v2, p0, Lecf;->ae:Z

    .line 521
    invoke-virtual {v0, v2}, Ljuj;->c(Z)Ljuj;

    move-result-object v0

    iget-object v2, p0, Lecf;->af:Ljava/lang/String;

    .line 522
    invoke-virtual {v0, v2}, Ljuj;->c(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 523
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->c(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget v1, p0, Lecf;->aa:I

    .line 524
    invoke-virtual {v0, v1}, Ljuj;->e(I)Ljuj;

    move-result-object v0

    .line 525
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "button_title_res_id"

    const/4 v3, 0x0

    .line 526
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 525
    invoke-virtual {v0, v1}, Ljuj;->e(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 528
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "max_selection_count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 529
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "max_selection_count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljuj;->d(I)Ljuj;

    .line 532
    :cond_3
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "min_selection_count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 533
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "min_selection_count"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljuj;->c(I)Ljuj;

    .line 536
    :cond_4
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_2
.end method

.method private d(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 837
    if-nez p1, :cond_0

    .line 862
    :goto_0
    return-void

    .line 841
    :cond_0
    invoke-virtual {p0}, Lecf;->U()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 842
    iget-boolean v0, p0, Lecf;->an:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lecf;->am:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lecf;->ap:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 843
    :cond_1
    iget-object v0, p0, Lecf;->aE:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 851
    :goto_1
    invoke-virtual {p0}, Lecf;->U()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lecf;->am:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lecf;->aC:Ljava/lang/Float;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lecf;->aC:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v0, v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_5

    .line 852
    iget-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v1, p0, Lecf;->al:Lcqj;

    iget-object v2, p0, Lecf;->aC:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Lcqj;->a(F)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    .line 853
    iput-object v4, p0, Lecf;->aC:Ljava/lang/Float;

    .line 859
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lecf;->Z_()V

    .line 861
    invoke-virtual {p0}, Lecf;->ag()V

    goto :goto_0

    .line 845
    :cond_3
    iget-object v0, p0, Lecf;->aE:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 848
    :cond_4
    iget-object v0, p0, Lecf;->aE:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 854
    :cond_5
    iget-object v0, p0, Lecf;->ax:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 855
    iget-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v1, p0, Lecf;->al:Lcqj;

    iget-object v2, p0, Lecf;->ax:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcqj;->a(Landroid/net/Uri;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    .line 856
    iput-object v4, p0, Lecf;->ax:Landroid/net/Uri;

    goto :goto_2
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 441
    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lhmw;->aj:Lhmw;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhmw;->ad:Lhmw;

    goto :goto_0
.end method

.method public J_()Z
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lecf;->ap:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 446
    invoke-super {p0}, Legi;->L_()V

    .line 448
    iget-object v1, p0, Lecf;->ap:Ljava/lang/Integer;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lecf;->ae()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 464
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    iput-boolean v0, p0, Lecf;->Z:Z

    .line 454
    iget-object v1, p0, Lecf;->al:Lcqj;

    invoke-virtual {v1}, Lcqj;->b()V

    .line 456
    invoke-virtual {p0}, Lecf;->at()Z

    move-result v1

    .line 457
    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Lecf;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget-object v4, p0, Lecf;->P:Lhee;

    .line 458
    invoke-interface {v4}, Lhee;->g()Lhej;

    move-result-object v4

    const-string v5, "gaia_id"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    if-nez v1, :cond_2

    const/4 v0, 0x1

    .line 457
    :cond_2
    invoke-static {v2, v3, v4, v5, v0}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lecf;->ap:Ljava/lang/Integer;

    .line 461
    invoke-virtual {p0}, Lecf;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lecf;->d(Landroid/view/View;)V

    .line 462
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lecf;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ek:Lhmv;

    .line 463
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 462
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lecf;->al:Lcqj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecf;->al:Lcqj;

    invoke-virtual {v0}, Lcqj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 468
    iget-boolean v0, p0, Lecf;->ac:Z

    if-eqz v0, :cond_0

    .line 469
    invoke-super {p0}, Legi;->a()Z

    move-result v0

    .line 471
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Legi;->V()Z

    move-result v0

    goto :goto_0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 894
    iget-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-static {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a(Landroid/widget/AbsListView;)Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 889
    invoke-super {p0}, Legi;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecf;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lecf;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    .line 220
    iget-object v0, p0, Lecf;->at:Llnl;

    .line 221
    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400d6

    .line 220
    invoke-super {p0, v0, p2, p3, v1}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v7

    .line 223
    iget-object v0, p0, Lecf;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0291

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 225
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v8

    .line 227
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "local_folders_only"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 229
    const v0, 0x7f100305

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/FastScrollContainer;

    iput-object v0, p0, Lecf;->ak:Lcom/google/android/apps/plus/views/FastScrollContainer;

    .line 230
    iget-object v0, p0, Lecf;->ak:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a(Lfyl;)V

    .line 232
    if-eqz v5, :cond_5

    const/4 v0, 0x2

    .line 233
    :goto_0
    invoke-virtual {p0}, Lecf;->an()Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    .line 234
    :goto_1
    invoke-virtual {p0}, Lecf;->an()Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x1

    .line 236
    :goto_2
    new-instance v4, Lcqn;

    invoke-direct {v4, v0, v1, v3, v2}, Lcqn;-><init>(IZIZ)V

    .line 238
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 242
    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0}, Lecf;->av()Z

    move-result v0

    if-nez v0, :cond_8

    if-nez v5, :cond_8

    const/4 v0, 0x1

    .line 243
    :goto_3
    if-eqz v0, :cond_9

    const/4 v0, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 244
    invoke-direct {p0}, Lecf;->au()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    invoke-static {v0, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 245
    :goto_4
    new-instance v0, Lcqj;

    iget-object v1, p0, Lecf;->at:Llnl;

    .line 246
    invoke-virtual {p0}, Lecf;->F_()Lhmw;

    move-result-object v6

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcqj;-><init>(Landroid/content/Context;ILjava/lang/String;Lcqn;Lcqm;Lhmw;)V

    .line 247
    invoke-virtual {v0, p0}, Lcqj;->a(Landroid/view/View$OnLongClickListener;)V

    .line 248
    iget-object v1, p0, Lecf;->ai:Lequ;

    invoke-virtual {v0, v1}, Lcqj;->a(Leqt;)V

    .line 249
    iget-object v1, p0, Lecf;->ak:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0, v1}, Lcqj;->a(Lcom/google/android/apps/plus/views/FastScrollContainer;)V

    .line 250
    iput-object v0, p0, Lecf;->al:Lcqj;

    .line 252
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    const-string v0, "local_folders_only"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    if-eqz v0, :cond_a

    const/4 v0, 0x0

    new-array v0, v0, [Lepm;

    iput-object v0, p0, Lecf;->aA:[Lepm;

    .line 254
    :goto_5
    new-instance v0, Ldwg;

    iget-object v1, p0, Lecf;->at:Llnl;

    .line 255
    invoke-virtual {p0}, Lecf;->w()Lbb;

    move-result-object v3

    const/4 v4, 0x2

    iget-object v2, p0, Lecf;->P:Lhee;

    .line 256
    invoke-interface {v2}, Lhee;->d()I

    move-result v5

    iget-object v2, p0, Lecf;->Y:Lctq;

    .line 257
    invoke-virtual {v2}, Lctq;->c()I

    move-result v6

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Ldwg;-><init>(Landroid/content/Context;Lept;Lbb;III)V

    iput-object v0, p0, Lecf;->az:Ldwg;

    .line 259
    const v0, 0x7f100306

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/FastScrollListView;

    iput-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 260
    iget-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    new-instance v1, Lecl;

    invoke-direct {v1}, Lecl;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 261
    iget-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v1, p0, Lecf;->al:Lcqj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 263
    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x2

    .line 265
    :goto_6
    new-instance v1, Lend;

    iget-object v2, p0, Lecf;->al:Lcqj;

    invoke-direct {v1, v0, v2, p0}, Lend;-><init>(ILenf;Lene;)V

    iput-object v1, p0, Lecf;->ay:Lend;

    .line 267
    invoke-virtual {p0}, Lecf;->as()Lepx;

    move-result-object v0

    .line 269
    iget-object v1, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v2, p0, Lecf;->ak:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0, v1, p0, v2}, Lepx;->a(Landroid/widget/AbsListView;Landroid/widget/AbsListView$OnScrollListener;Lcom/google/android/apps/plus/views/FastScrollContainer;)V

    .line 273
    if-nez p3, :cond_1

    .line 274
    invoke-virtual {p0}, Lecf;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 277
    :cond_1
    invoke-virtual {p0}, Lecf;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 280
    const-string v0, "scroll_to_uri"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lecf;->ax:Landroid/net/Uri;

    .line 282
    if-eqz p3, :cond_2

    .line 283
    const-string v0, "first_refresh_finished"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lecf;->ao:Z

    .line 284
    const-string v0, "accessibility_enabled"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lecf;->ar:Z

    .line 286
    const-string v0, "scroll_pos"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    const-string v0, "scroll_pos"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lecf;->aC:Ljava/lang/Float;

    .line 288
    const/4 v0, 0x0

    iput-object v0, p0, Lecf;->ax:Landroid/net/Uri;

    .line 292
    :cond_2
    iget-object v0, p0, Lecf;->ai:Lequ;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lecf;->ai:Lequ;

    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "show_autobackup_status"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lecf;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_7
    invoke-virtual {v1, v0}, Lequ;->b(Z)V

    .line 294
    :cond_3
    invoke-direct {p0, v7}, Lecf;->d(Landroid/view/View;)V

    .line 296
    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 297
    iget-object v0, p0, Lecf;->ak:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a()V

    .line 299
    :cond_4
    return-object v7

    .line 232
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 233
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 234
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 242
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 244
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 252
    :cond_a
    iget-object v0, p0, Lecf;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lecf;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1, p0, p0}, Ldwg;->a(Landroid/content/Context;ILept;Legi;)[Lepm;

    move-result-object v0

    iput-object v0, p0, Lecf;->aA:[Lepm;

    goto/16 :goto_5

    :cond_b
    const/4 v0, 0x1

    new-array v1, v0, [Lepm;

    const/4 v2, 0x0

    new-instance v3, Leqd;

    iget-object v4, p0, Lecf;->at:Llnl;

    iget-object v0, p0, Lecf;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v5

    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_8
    invoke-direct {v3, v4, v5, p0, v0}, Leqd;-><init>(Landroid/content/Context;ILept;Z)V

    aput-object v3, v1, v2

    iput-object v1, p0, Lecf;->aA:[Lepm;

    goto/16 :goto_5

    :cond_c
    const/4 v0, 0x0

    goto :goto_8

    .line 263
    :cond_d
    const/4 v0, 0x5

    goto/16 :goto_6

    .line 292
    :cond_e
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 687
    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    new-instance v5, Lezu;

    iget-object v0, p0, Lecf;->at:Llnl;

    iget v1, p0, Lecf;->aa:I

    invoke-direct {v5, v0, v3, v1}, Lezu;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    .line 707
    :goto_0
    return-object v5

    .line 689
    :cond_0
    invoke-direct {p0}, Lecf;->av()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 690
    new-instance v5, Lfda;

    iget-object v0, p0, Lecf;->at:Llnl;

    invoke-direct {v5, v0}, Lfda;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 691
    :cond_1
    iget-object v0, p0, Lecf;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lecf;->ae()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v4

    :goto_1
    if-eqz v0, :cond_4

    .line 692
    invoke-virtual {p0}, Lecf;->al()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 693
    iget-object v0, p0, Lecf;->at:Llnl;

    iget-object v1, p0, Lecf;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    new-array v5, v4, [Ljava/lang/String;

    .line 694
    invoke-direct {p0}, Lecf;->au()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v2, v5}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget v5, p0, Lecf;->aa:I

    .line 693
    invoke-static/range {v0 .. v5}, Levl;->a(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;ZI)Levl;

    move-result-object v5

    goto :goto_0

    :cond_2
    move v0, v2

    .line 691
    goto :goto_1

    .line 697
    :cond_3
    new-instance v5, Levl;

    iget-object v6, p0, Lecf;->at:Llnl;

    iget-object v0, p0, Lecf;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v7

    new-array v0, v4, [Ljava/lang/String;

    .line 698
    invoke-direct {p0}, Lecf;->au()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v2, v0}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget v11, p0, Lecf;->aa:I

    move-object v9, v3

    move v10, v4

    invoke-direct/range {v5 .. v11}, Levl;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZI)V

    goto :goto_0

    .line 702
    :cond_4
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "local_folders_only"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 703
    new-instance v1, Ljvl;

    iget-object v3, p0, Lecf;->at:Llnl;

    invoke-direct {v1, v3}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v1, v1, Ljvl;->c:I

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    :goto_2
    mul-int/2addr v1, v0

    .line 706
    iget v0, p0, Lecf;->aa:I

    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_6

    const/4 v0, 0x4

    .line 707
    :goto_3
    new-instance v5, Lezt;

    iget-object v2, p0, Lecf;->at:Llnl;

    invoke-direct {v5, v2, v1, v0, v4}, Lezt;-><init>(Landroid/content/Context;IIZ)V

    goto/16 :goto_0

    :cond_5
    move v0, v4

    .line 703
    goto :goto_2

    .line 706
    :cond_6
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    const/4 v0, 0x5

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 942
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 943
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 944
    iget-object v3, p0, Lecf;->as:Lizs;

    const/4 v4, 0x2

    const v5, 0x10040

    invoke-virtual {v3, v0, v4, v5}, Lizs;->c(Lizu;II)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    .line 946
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 948
    :cond_0
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 6

    .prologue
    const v4, 0x7f0a05e6

    const/4 v3, 0x0

    .line 793
    const-string v0, "collection_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 794
    const-string v0, "action"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 795
    const-string v1, "selected_media"

    .line 796
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 797
    iget-object v1, p0, Lecf;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 798
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 833
    invoke-super {p0, p1, p2, p3}, Legi;->a(ILandroid/os/Bundle;Ljava/lang/String;)V

    .line 834
    :goto_0
    return-void

    .line 800
    :pswitch_0
    invoke-static {v5}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 801
    invoke-static {v5}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 802
    invoke-static {v5}, Ljvj;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 804
    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v0

    .line 803
    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 806
    invoke-virtual {p0, v0}, Lecf;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 811
    :pswitch_1
    iget-object v0, p0, Lecf;->at:Llnl;

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 812
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 817
    :pswitch_2
    iget-object v0, p0, Lecf;->at:Llnl;

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 818
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 822
    :pswitch_3
    iget-object v0, p0, Lecf;->at:Llnl;

    invoke-static {v0, v1, v2}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 823
    invoke-virtual {p0, v0}, Lecf;->a(Landroid/content/Intent;)V

    .line 824
    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v0

    const v1, 0x7f05001e

    invoke-virtual {v0, v1, v3}, Lz;->overridePendingTransition(II)V

    goto :goto_0

    .line 828
    :pswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lecf;->b(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    goto :goto_0

    .line 798
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 760
    invoke-direct {p0, p1}, Lecf;->b(Landroid/database/Cursor;)Z

    move-result v1

    iput-boolean v1, p0, Lecf;->aq:Z

    .line 761
    iput-boolean v0, p0, Lecf;->an:Z

    .line 762
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_4

    .line 763
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lecf;->ao:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lecf;->P:Lhee;

    invoke-interface {v1}, Lhee;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lecf;->ae()Z

    move-result v1

    if-nez v1, :cond_1

    .line 764
    invoke-virtual {p0}, Lecf;->L_()V

    .line 766
    :cond_1
    iget-object v1, p0, Lecf;->al:Lcqj;

    invoke-virtual {v1, p1}, Lcqj;->a(Landroid/database/Cursor;)V

    .line 768
    iget-boolean v1, p0, Lecf;->am:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lecf;->aB:Landroid/view/View;

    if-eqz v1, :cond_3

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lecf;->av()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 770
    :cond_2
    iget-object v0, p0, Lecf;->al:Lcqj;

    iget-object v1, p0, Lecf;->aB:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcqj;->a(Landroid/view/View;)V

    .line 771
    const/4 v0, 0x0

    iput-object v0, p0, Lecf;->aB:Landroid/view/View;

    .line 773
    :cond_3
    invoke-virtual {p0}, Lecf;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lecf;->d(Landroid/view/View;)V

    .line 774
    return-void

    .line 762
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 194
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 195
    if-eqz p1, :cond_0

    .line 196
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lecf;->ap:Ljava/lang/Integer;

    .line 200
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Lctm;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 547
    new-instance v7, Lyo;

    iget-object v0, p0, Lecf;->at:Llnl;

    invoke-direct {v7, v0, p1}, Lyo;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 548
    const v0, 0x7f120003

    invoke-virtual {v7, v0}, Lyo;->a(I)V

    .line 550
    invoke-virtual {v7}, Lyo;->a()Landroid/view/Menu;

    move-result-object v1

    .line 551
    invoke-interface {v1}, Landroid/view/Menu;->size()I

    .line 552
    iget-object v0, p0, Lecf;->al:Lcqj;

    invoke-virtual {v0, p2}, Lcqj;->a(Lctm;)Ljava/util/ArrayList;

    move-result-object v6

    .line 555
    invoke-interface {v1}, Landroid/view/Menu;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 556
    invoke-interface {v1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 558
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f100671

    if-ne v3, v4, :cond_0

    .line 559
    iget-object v3, p0, Lecf;->at:Llnl;

    invoke-virtual {v3}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0b3d

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 560
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v8

    .line 559
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 555
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 564
    :cond_1
    new-instance v0, Lecj;

    iget-object v1, p0, Lecf;->at:Llnl;

    .line 565
    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Lecf;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    iget v5, p0, Lecf;->aa:I

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lecj;-><init>(Landroid/content/Context;Landroid/app/Activity;ILjava/lang/String;ILjava/util/ArrayList;)V

    .line 564
    invoke-virtual {v7, v0}, Lyo;->a(Lyp;)V

    .line 566
    invoke-virtual {v7}, Lyo;->c()V

    .line 567
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 600
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lecf;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eH:Lhmv;

    .line 601
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 600
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 602
    invoke-static {p3}, Ljvj;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lecf;->at:Llnl;

    const v1, 0x7f0a0656

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 605
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 638
    :goto_0
    return-void

    .line 608
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 609
    const v1, 0x7f1001ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 611
    iget-object v1, p0, Lecf;->aD:Lhqa;

    invoke-virtual {v1, p2}, Lhqa;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 612
    iget-object v1, p0, Lecf;->aD:Lhqa;

    invoke-virtual {v1, p2}, Lhqa;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 613
    check-cast p1, Landroid/widget/ImageView;

    const v1, 0x7f02004a

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 614
    const v1, 0x7f0a0928

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 616
    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v1

    const v2, 0x7f050009

    .line 615
    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lecf;->aw:Landroid/view/animation/Animation;

    .line 630
    :cond_1
    :goto_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 631
    iget-object v1, p0, Lecf;->aw:Landroid/view/animation/Animation;

    new-instance v2, Leci;

    invoke-direct {v2, v0}, Leci;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 637
    iget-object v1, p0, Lecf;->aw:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 619
    :cond_2
    iget-object v1, p0, Lecf;->aD:Lhqa;

    invoke-virtual {v1, p2}, Lhqa;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 620
    const v1, 0x7f0a0927

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 622
    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v1

    const v2, 0x7f05000a

    .line 621
    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lecf;->aw:Landroid/view/animation/Animation;

    .line 624
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    .line 626
    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 627
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    goto :goto_1
.end method

.method public a(Lctm;)V
    .locals 5

    .prologue
    .line 572
    iget-object v0, p0, Lecf;->al:Lcqj;

    invoke-virtual {v0, p1}, Lcqj;->a(Lctm;)Ljava/util/ArrayList;

    move-result-object v2

    .line 574
    const/4 v0, 0x0

    .line 575
    iget-object v1, p0, Lecf;->X:Lctz;

    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v3

    .line 576
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    .line 577
    invoke-virtual {v3, v0}, Ljcn;->c(Ljcl;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 578
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 580
    goto :goto_0

    .line 582
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v1, v0, :cond_1

    .line 583
    iget-object v0, p0, Lecf;->Q:Lctu;

    invoke-virtual {v0, v2}, Lctu;->a(Ljava/util/ArrayList;)Z

    .line 589
    :goto_2
    return-void

    .line 585
    :cond_1
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Lecf;->at:Llnl;

    invoke-direct {v1, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->eh:Lhmv;

    .line 586
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 585
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 587
    iget-object v0, p0, Lecf;->Q:Lctu;

    invoke-virtual {v0, v2}, Lctu;->b(Ljava/util/ArrayList;)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 789
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 103
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lecf;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lepm;)V
    .locals 2

    .prologue
    .line 960
    const/4 v0, 0x1

    iput-boolean v0, p0, Lecf;->am:Z

    .line 961
    if-eqz p1, :cond_2

    invoke-interface {p1}, Lepm;->j()Landroid/view/View;

    move-result-object v0

    .line 962
    :goto_0
    iget-boolean v1, p0, Lecf;->an:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lecf;->al:Lcqj;

    invoke-virtual {v1}, Lcqj;->getCount()I

    move-result v1

    if-gtz v1, :cond_0

    invoke-direct {p0}, Lecf;->av()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 964
    :cond_0
    iget-object v1, p0, Lecf;->al:Lcqj;

    invoke-virtual {v1, v0}, Lcqj;->a(Landroid/view/View;)V

    .line 969
    :goto_1
    invoke-virtual {p0}, Lecf;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lecf;->d(Landroid/view/View;)V

    .line 970
    if-eqz p1, :cond_1

    .line 971
    invoke-interface {p1}, Lepm;->i()V

    .line 973
    :cond_1
    return-void

    .line 961
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 967
    :cond_3
    iput-object v0, p0, Lecf;->aB:Landroid/view/View;

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1102
    new-instance v0, Leck;

    iget-object v2, p0, Lecf;->at:Llnl;

    iget-object v1, p0, Lecf;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v3

    iget-object v5, p0, Lecf;->al:Lcqj;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Leck;-><init>(Lecf;Landroid/content/Context;ILjava/lang/String;Lcqj;)V

    .line 1104
    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Leck;->b(Ljava/lang/String;)Lhny;

    .line 1105
    iget-object v1, p0, Lecf;->au:Llnh;

    const-class v2, Lhoc;

    invoke-virtual {v1, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 1106
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 642
    invoke-direct {p0, p1, p2}, Lecf;->c(Ljava/lang/String;Ljava/lang/Long;)V

    .line 643
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lizu;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 648
    invoke-virtual {p0, p4}, Lecf;->a(Lizu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    :goto_0
    return-void

    .line 651
    :cond_0
    if-eqz p2, :cond_1

    .line 652
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x40000

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    move v0, v1

    .line 654
    :goto_1
    new-instance v3, Ldew;

    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v4

    iget-object v5, p0, Lecf;->P:Lhee;

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    invoke-direct {v3, v4, v5}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 655
    invoke-virtual {v3, p3}, Ldew;->a(Ljava/lang/String;)Ldew;

    move-result-object v3

    .line 656
    invoke-virtual {v3, p4}, Ldew;->a(Lizu;)Ldew;

    move-result-object v3

    if-eqz v0, :cond_2

    .line 657
    :goto_2
    invoke-virtual {v3, p1}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lecf;->Y:Lctq;

    .line 659
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->b(I)Ldew;

    move-result-object v0

    iget-object v1, p0, Lecf;->X:Lctz;

    .line 660
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lecf;->Y:Lctq;

    .line 661
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    .line 662
    invoke-virtual {v0, v2}, Ldew;->i(Z)Ldew;

    move-result-object v0

    iget v1, p0, Lecf;->aa:I

    .line 663
    invoke-virtual {v0, v1}, Ldew;->c(I)Ldew;

    move-result-object v0

    .line 664
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v1

    .line 665
    iget-object v0, p0, Lecf;->at:Llnl;

    const-class v2, Lizs;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    const/4 v2, 0x5

    const/16 v3, 0x1040

    invoke-virtual {v0, p4, v2, v3}, Lizs;->c(Lizu;II)Lcom/google/android/libraries/social/media/MediaResource;

    .line 670
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lecf;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->dZ:Lhmv;

    .line 671
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 670
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 672
    invoke-virtual {p0, v1}, Lecf;->b(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 652
    goto :goto_1

    .line 656
    :cond_2
    new-array v0, v1, [Ljava/lang/String;

    .line 658
    invoke-direct {p0}, Lecf;->au()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v2, v0}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2
.end method

.method public a(Ljava/util/List;Lduo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lduo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 726
    iget-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    .line 727
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 421
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 343
    invoke-super {p0}, Legi;->aO_()V

    .line 344
    iget-object v0, p0, Lecf;->V:Lcsm;

    invoke-interface {v0, p0}, Lcsm;->a(Lcsn;)V

    .line 346
    iget-object v0, p0, Lecf;->aF:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 348
    iget-object v0, p0, Lecf;->ap:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lecf;->ap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 350
    invoke-virtual {p0}, Lecf;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lecf;->aE:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 359
    :cond_0
    :goto_0
    iget-object v0, p0, Lecf;->at:Llnl;

    .line 360
    invoke-static {v0}, Llhn;->a(Landroid/content/Context;)Z

    move-result v0

    .line 362
    iget-object v1, p0, Lecf;->al:Lcqj;

    invoke-virtual {v1, v0}, Lcqj;->a(Z)V

    .line 364
    iget-boolean v1, p0, Lecf;->ar:Z

    if-eq v0, v1, :cond_1

    .line 365
    iput-boolean v0, p0, Lecf;->ar:Z

    .line 366
    iget-object v0, p0, Lecf;->al:Lcqj;

    invoke-virtual {v0}, Lcqj;->notifyDataSetChanged()V

    .line 369
    :cond_1
    iget-object v0, p0, Lecf;->ai:Lequ;

    invoke-virtual {v0}, Lequ;->a()V

    .line 371
    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 372
    iget-object v0, p0, Lecf;->ak:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a()V

    .line 374
    :cond_2
    iget-object v0, p0, Lecf;->az:Ldwg;

    invoke-virtual {v0}, Ldwg;->a()V

    .line 375
    return-void

    .line 354
    :cond_3
    iget-object v0, p0, Lecf;->ap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 355
    iget-object v1, p0, Lecf;->ap:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lecf;->a(ILfib;)V

    goto :goto_0
.end method

.method public aa()Z
    .locals 2

    .prologue
    .line 927
    iget-boolean v0, p0, Lecf;->aq:Z

    if-nez v0, :cond_0

    .line 928
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "show_autobackup_status"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ab()V
    .locals 4

    .prologue
    .line 933
    iget-object v0, p0, Lecf;->al:Lcqj;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lecf;->N:Z

    if-nez v0, :cond_0

    .line 934
    const/4 v0, 0x1

    iput-boolean v0, p0, Lecf;->N:Z

    .line 935
    iget-object v0, p0, Lecf;->ah:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 938
    :cond_0
    return-void
.end method

.method public ac()V
    .locals 2

    .prologue
    .line 977
    iget-object v0, p0, Lecf;->al:Lcqj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcqj;->a(Landroid/view/View;)V

    .line 978
    return-void
.end method

.method public ad()[Lepm;
    .locals 1

    .prologue
    .line 982
    iget-object v0, p0, Lecf;->aA:[Lepm;

    return-object v0
.end method

.method protected ae()Z
    .locals 1

    .prologue
    .line 908
    invoke-super {p0}, Legi;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Lhjk;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 402
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 404
    invoke-virtual {p0}, Lecf;->ae()Z

    move-result v0

    .line 405
    iget-object v1, p0, Lecf;->P:Lhee;

    invoke-interface {v1}, Lhee;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 406
    invoke-virtual {p0, p1, v2}, Lecf;->a(Lhjk;I)V

    .line 408
    const v0, 0x7f10067b

    .line 409
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 410
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 413
    :cond_0
    invoke-virtual {p0}, Lecf;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 414
    if-eqz v0, :cond_1

    const-string v1, "local_folders_only"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    const v0, 0x7f0a0a84

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 417
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 593
    iget-object v0, p0, Lecf;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 595
    invoke-virtual {p0}, Lecf;->n()Lz;

    move-result-object v1

    const/4 v2, 0x0

    .line 594
    invoke-static {v1, v0, p1, v2}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lecf;->a(Landroid/content/Intent;)V

    .line 596
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 485
    invoke-direct {p0, p1, p2}, Lecf;->c(Ljava/lang/String;Ljava/lang/Long;)V

    .line 486
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 953
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/MediaResource;

    .line 954
    iget-object v2, p0, Lecf;->as:Lizs;

    invoke-virtual {v2, v0}, Lizs;->a(Lcom/google/android/libraries/social/media/MediaResource;)V

    goto :goto_0

    .line 956
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 204
    invoke-super {p0, p1}, Legi;->c(Landroid/os/Bundle;)V

    .line 205
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v1, Lcrk;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 206
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v1, Lcqw;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 207
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v1, Lizs;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iput-object v0, p0, Lecf;->as:Lizs;

    .line 208
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v1, Lcsm;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsm;

    iput-object v0, p0, Lecf;->V:Lcsm;

    .line 209
    iget-object v0, p0, Lecf;->au:Llnh;

    const-class v1, Lhqa;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqa;

    iput-object v0, p0, Lecf;->aD:Lhqa;

    .line 210
    return-void
.end method

.method protected c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 385
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 386
    iget-object v0, p0, Lecf;->ap:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 387
    const-string v0, "refresh_request"

    iget-object v1, p0, Lecf;->ap:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 390
    :cond_0
    iget-object v0, p0, Lecf;->al:Lcqj;

    if-eqz v0, :cond_1

    .line 391
    iget-object v0, p0, Lecf;->al:Lcqj;

    invoke-virtual {v0}, Lcqj;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 392
    const-string v0, "scroll_pos"

    iget-object v1, p0, Lecf;->al:Lcqj;

    iget-object v2, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 393
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/FastScrollListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcqj;->b(I)F

    move-result v1

    .line 392
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 397
    :cond_1
    const-string v0, "first_refresh_finished"

    iget-boolean v1, p0, Lecf;->ao:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 398
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 337
    invoke-super {p0}, Legi;->g()V

    .line 338
    iget-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 339
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 379
    invoke-super {p0}, Legi;->h()V

    .line 380
    iget-object v0, p0, Lecf;->aj:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 381
    return-void
.end method

.method public h_(I)V
    .locals 0

    .prologue
    .line 914
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 677
    instance-of v0, p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    if-eqz v0, :cond_0

    .line 678
    check-cast p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    invoke-virtual {p0, p1}, Lecf;->b(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    .line 680
    const/4 v0, 0x1

    .line 682
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lecf;->ay:Lend;

    invoke-virtual {v0, p1, p2, p3, p4}, Lend;->onScroll(Landroid/widget/AbsListView;III)V

    .line 477
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 323
    invoke-super {p0}, Legi;->z()V

    .line 324
    iget-object v0, p0, Lecf;->V:Lcsm;

    invoke-interface {v0, p0}, Lcsm;->b(Lcsn;)V

    .line 326
    iget-object v0, p0, Lecf;->ay:Lend;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lecf;->ay:Lend;

    invoke-virtual {v0}, Lend;->a()V

    .line 329
    :cond_0
    invoke-virtual {p0}, Lecf;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330
    iget-object v0, p0, Lecf;->ak:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->b()V

    .line 332
    :cond_1
    iget-object v0, p0, Lecf;->aF:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 333
    return-void
.end method
