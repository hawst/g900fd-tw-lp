.class public final Lfmx;
.super Llol;
.source "PG"

# interfaces
.implements Lhob;
.implements Lkgr;


# instance fields
.field private final N:Lhoc;

.field private O:I

.field private P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private Q:Lnpd;

.field private R:Lkgp;

.field private S:Lkhr;

.field private T:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lnpd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 80
    invoke-direct {p0}, Llol;-><init>()V

    .line 44
    new-instance v0, Lhoc;

    iget-object v1, p0, Lfmx;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    .line 45
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lfmx;->N:Lhoc;

    .line 49
    new-instance v0, Lkgp;

    iget-object v1, p0, Lfmx;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkgp;-><init>(Lkgr;Llqr;)V

    iput-object v0, p0, Lfmx;->R:Lkgp;

    .line 53
    new-instance v0, Lfmy;

    iget-object v1, p0, Lfmx;->R:Lkgp;

    iget-object v2, p0, Lfmx;->av:Llqm;

    invoke-direct {v0, p0, p0, v1, v2}, Lfmy;-><init>(Lfmx;Lkgr;Lkgp;Llqr;)V

    iput-object v0, p0, Lfmx;->T:Lbc;

    .line 80
    return-void
.end method

.method static synthetic a(Lfmx;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lfmx;->O:I

    return v0
.end method

.method static synthetic a(Lfmx;Lnpd;)Lnpd;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lfmx;->Q:Lnpd;

    return-object p1
.end method

.method static synthetic b(Lfmx;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lfmx;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic c(Lfmx;)Lnpd;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lfmx;->Q:Lnpd;

    return-object v0
.end method

.method static synthetic d(Lfmx;)Llnl;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lfmx;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Lfmx;)Lhoc;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lfmx;->N:Lhoc;

    return-object v0
.end method


# virtual methods
.method public U()V
    .locals 4

    .prologue
    .line 96
    iget-object v0, p0, Lfmx;->S:Lkhr;

    const v1, 0x7f0a0b4b

    .line 97
    invoke-virtual {p0, v1}, Lfmx;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0b4c

    .line 98
    invoke-virtual {p0, v2}, Lfmx;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 96
    invoke-virtual {v0, v1, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    .line 99
    const-string v1, "show_view_counts_preference"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 100
    new-instance v1, Lfmz;

    invoke-direct {v1, p0}, Lfmz;-><init>(Lfmx;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 110
    iput-object v0, p0, Lfmx;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 111
    iget-object v0, p0, Lfmx;->R:Lkgp;

    iget-object v1, p0, Lfmx;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v1}, Lkgp;->a(Lkhl;)Lkhl;

    .line 113
    new-instance v0, Lkgz;

    iget-object v1, p0, Lfmx;->at:Llnl;

    invoke-direct {v0, v1}, Lkgz;-><init>(Landroid/content/Context;)V

    .line 114
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkgz;->c(Z)V

    .line 115
    iget-object v1, p0, Lfmx;->at:Llnl;

    const-string v2, "profile_views"

    const-string v3, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v1, v2, v3}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 116
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Lkgz;->a(Landroid/content/Intent;)V

    .line 117
    iget-object v1, p0, Lfmx;->R:Lkgp;

    invoke-virtual {v1, v0}, Lkgp;->a(Lkhl;)Lkhl;

    .line 118
    return-void
.end method

.method public a()V
    .locals 4

    .prologue
    .line 122
    invoke-virtual {p0}, Lfmx;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lfmx;->T:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 123
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 84
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 85
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lfmx;->O:I

    .line 87
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 133
    const-string v0, "SetProfileSettingsTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v1, p0, Lfmx;->Q:Lnpd;

    iget-object v0, p0, Lfmx;->Q:Lnpd;

    iget-object v0, v0, Lnpd;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnpd;->a:Ljava/lang/Boolean;

    .line 137
    iget-object v0, p0, Lfmx;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lfmx;->P:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v1, p0, Lfmx;->Q:Lnpd;

    iget-object v1, v1, Lnpd;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 142
    :cond_0
    return-void

    .line 136
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 127
    invoke-virtual {p0}, Lfmx;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lfmx;->T:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 128
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 91
    new-instance v0, Lkhr;

    iget-object v1, p0, Lfmx;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfmx;->S:Lkhr;

    .line 92
    invoke-virtual {p0}, Lfmx;->U()V

    .line 93
    return-void
.end method
