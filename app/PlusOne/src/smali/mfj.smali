.class public final Lmfj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnls;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1132
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1135
    iput-object v0, p0, Lmfj;->apiHeader:Llyr;

    .line 1138
    iput-object v0, p0, Lmfj;->a:Lnls;

    .line 1132
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1155
    const/4 v0, 0x0

    .line 1156
    iget-object v1, p0, Lmfj;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 1157
    const/4 v0, 0x1

    iget-object v1, p0, Lmfj;->apiHeader:Llyr;

    .line 1158
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1160
    :cond_0
    iget-object v1, p0, Lmfj;->a:Lnls;

    if-eqz v1, :cond_1

    .line 1161
    const/4 v1, 0x2

    iget-object v2, p0, Lmfj;->a:Lnls;

    .line 1162
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1164
    :cond_1
    iget-object v1, p0, Lmfj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1165
    iput v0, p0, Lmfj;->ai:I

    .line 1166
    return v0
.end method

.method public a(Loxn;)Lmfj;
    .locals 2

    .prologue
    .line 1174
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1175
    sparse-switch v0, :sswitch_data_0

    .line 1179
    iget-object v1, p0, Lmfj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1180
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfj;->ah:Ljava/util/List;

    .line 1183
    :cond_1
    iget-object v1, p0, Lmfj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1185
    :sswitch_0
    return-object p0

    .line 1190
    :sswitch_1
    iget-object v0, p0, Lmfj;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 1191
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfj;->apiHeader:Llyr;

    .line 1193
    :cond_2
    iget-object v0, p0, Lmfj;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1197
    :sswitch_2
    iget-object v0, p0, Lmfj;->a:Lnls;

    if-nez v0, :cond_3

    .line 1198
    new-instance v0, Lnls;

    invoke-direct {v0}, Lnls;-><init>()V

    iput-object v0, p0, Lmfj;->a:Lnls;

    .line 1200
    :cond_3
    iget-object v0, p0, Lmfj;->a:Lnls;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1175
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1143
    iget-object v0, p0, Lmfj;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 1144
    const/4 v0, 0x1

    iget-object v1, p0, Lmfj;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1146
    :cond_0
    iget-object v0, p0, Lmfj;->a:Lnls;

    if-eqz v0, :cond_1

    .line 1147
    const/4 v0, 0x2

    iget-object v1, p0, Lmfj;->a:Lnls;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1149
    :cond_1
    iget-object v0, p0, Lmfj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1151
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1128
    invoke-virtual {p0, p1}, Lmfj;->a(Loxn;)Lmfj;

    move-result-object v0

    return-object v0
.end method
