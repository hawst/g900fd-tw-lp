.class public final Ljzn;
.super Laq;
.source "PG"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljzo;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lae;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lae;",
            "Ljava/util/ArrayList",
            "<",
            "Ljzo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    invoke-direct {p0, p2}, Laq;-><init>(Lae;)V

    .line 172
    iput-object p1, p0, Ljzn;->b:Landroid/content/Context;

    .line 173
    iput-object p3, p0, Ljzn;->a:Ljava/util/ArrayList;

    .line 174
    return-void
.end method


# virtual methods
.method public a(I)Lu;
    .locals 4

    .prologue
    .line 178
    new-instance v1, Ljzh;

    invoke-direct {v1}, Ljzh;-><init>()V

    .line 179
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 180
    const-string v3, "poll_option_voters"

    iget-object v0, p0, Ljzn;->a:Ljava/util/ArrayList;

    .line 181
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzo;

    invoke-virtual {v0}, Ljzo;->d()Ljava/util/ArrayList;

    move-result-object v0

    .line 180
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 182
    invoke-virtual {v1, v2}, Lu;->f(Landroid/os/Bundle;)V

    .line 183
    return-object v1
.end method

.method public b()I
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Ljzn;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljzn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(I)Ljava/lang/CharSequence;
    .locals 10

    .prologue
    .line 193
    iget-object v0, p0, Ljzn;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzo;

    .line 195
    invoke-virtual {v0}, Ljzo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 196
    iget-object v2, p0, Ljzn;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 197
    const v3, 0x7f0a0171

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 198
    invoke-static {}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->k()Ljava/text/NumberFormat;

    move-result-object v6

    invoke-virtual {v0}, Ljzo;->b()F

    move-result v7

    float-to-double v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 199
    invoke-static {}, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;->l()Ljava/text/NumberFormat;

    move-result-object v6

    invoke-virtual {v0}, Ljzo;->c()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x2

    aput-object v1, v4, v0

    .line 197
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 201
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method
