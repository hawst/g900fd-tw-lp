.class public final Llcc;
.super Lllq;
.source "PG"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkzm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lllq;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Llcc;->a:Ljava/util/ArrayList;

    .line 33
    return-void
.end method

.method public constructor <init>(Lnmo;)V
    .locals 8

    .prologue
    .line 35
    invoke-direct {p0}, Lllq;-><init>()V

    .line 36
    const-string v0, "DbUpcomingBdays"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {p1}, Lnmo;->toString()Ljava/lang/String;

    .line 40
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lnmo;->b:[Lnmn;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Llcc;->a:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 43
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 44
    iget v1, v0, Landroid/text/format/Time;->year:I

    .line 46
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p1, Lnmo;->b:[Lnmn;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 47
    iget-object v2, p1, Lnmo;->b:[Lnmn;

    aget-object v2, v2, v0

    .line 48
    iget-object v3, p0, Llcc;->a:Ljava/util/ArrayList;

    new-instance v4, Lkzm;

    iget-object v5, v2, Lnmn;->b:Ljava/lang/String;

    iget-object v6, v2, Lnmn;->c:Ljava/lang/String;

    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iget-object v2, v2, Lnmn;->d:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v7, v2}, Lkzm;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 48
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_1
    return-void
.end method

.method public static a([B)Llcc;
    .locals 6

    .prologue
    .line 78
    if-nez p0, :cond_1

    .line 79
    const/4 v0, 0x0

    .line 91
    :cond_0
    return-object v0

    .line 82
    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 83
    new-instance v0, Llcc;

    invoke-direct {v0}, Llcc;-><init>()V

    .line 85
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    .line 86
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, v0, Llcc;->a:Ljava/util/ArrayList;

    .line 87
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 88
    iget-object v4, v0, Llcc;->a:Ljava/util/ArrayList;

    invoke-static {v2}, Lkzm;->a(Ljava/nio/ByteBuffer;)Lkzm;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a(Llcc;)[B
    .locals 5

    .prologue
    .line 59
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x100

    invoke-direct {v2, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 60
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 62
    :try_start_0
    invoke-virtual {p0}, Llcc;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 65
    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 66
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 67
    iget-object v0, p0, Llcc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzm;

    invoke-static {v0, v3}, Lkzm;->a(Lkzm;Ljava/io/DataOutputStream;)V

    .line 66
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 69
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 71
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    .line 74
    return-object v0

    .line 71
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Llcc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Lkzm;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Llcc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzm;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    iget-object v0, p0, Llcc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    .line 102
    iget-object v0, p0, Llcc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzm;

    .line 103
    invoke-virtual {v0}, Lkzm;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 104
    invoke-virtual {v0, v1}, Lkzm;->a(Z)V

    move v0, v1

    .line 108
    :goto_1
    return v0

    .line 101
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 108
    goto :goto_1
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lkzm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Llcc;->a:Ljava/util/ArrayList;

    return-object v0
.end method
