.class public Lkxp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheo;


# instance fields
.field private final a:Lhei;


# direct methods
.method public constructor <init>(Llnh;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-class v0, Lhei;

    invoke-virtual {p1, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lkxp;->a:Lhei;

    .line 35
    return-void
.end method


# virtual methods
.method public a(I)Lnto;
    .locals 3

    .prologue
    .line 74
    const/4 v1, 0x0

    .line 76
    :try_start_0
    iget-object v0, p0, Lkxp;->a:Lhei;

    const-string v2, "squares_on_profile_squares"

    invoke-interface {v0, p1, v2}, Lhei;->b(ILjava/lang/String;)[B

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    .line 78
    new-instance v2, Lnto;

    invoke-direct {v2}, Lnto;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnto;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public a(ILnso;)V
    .locals 3

    .prologue
    .line 63
    if-eqz p2, :cond_0

    .line 65
    :try_start_0
    iget-object v0, p0, Lkxp;->a:Lhei;

    const-string v1, "squares_on_profile_visibility"

    .line 66
    invoke-static {p2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    .line 65
    invoke-interface {v0, p1, v1, v2}, Lhei;->a(ILjava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    const-string v1, "SquaresSettingsStore"

    const-string v2, "Error writing community settings"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(ILnto;)V
    .locals 3

    .prologue
    .line 46
    if-eqz p2, :cond_0

    .line 48
    :try_start_0
    iget-object v0, p0, Lkxp;->a:Lhei;

    const-string v1, "squares_on_profile_squares"

    .line 49
    invoke-static {p2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    .line 48
    invoke-interface {v0, p1, v1, v2}, Lhei;->a(ILjava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    const-string v1, "SquaresSettingsStore"

    const-string v2, "Error writing user communities"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 55
    :cond_0
    :try_start_1
    iget-object v0, p0, Lkxp;->a:Lhei;

    const-string v1, "squares_on_profile_squares"

    invoke-interface {v0, p1, v1}, Lhei;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 56
    :catch_1
    move-exception v0

    .line 57
    const-string v1, "SquaresSettingsStore"

    const-string v2, "Failed to remove communities cache"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lhem;)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lheq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    return-void
.end method

.method public b(I)Lnso;
    .locals 3

    .prologue
    .line 87
    const/4 v1, 0x0

    .line 89
    :try_start_0
    iget-object v0, p0, Lkxp;->a:Lhei;

    const-string v2, "squares_on_profile_visibility"

    .line 90
    invoke-interface {v0, p1, v2}, Lhei;->b(ILjava/lang/String;)[B

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    .line 92
    new-instance v2, Lnso;

    invoke-direct {v2}, Lnso;-><init>()V

    .line 93
    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnso;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
