.class public Ljpl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljpl;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljpn;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljpo;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljpq;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljpq;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lhym;

.field private o:Z

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 71
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "lookup_key"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "matched_email"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "email"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "phone"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "phone_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "snippet"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "in_same_visibility_group"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "verified"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "auto_complete_index"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "auto_complete_suggestion"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "is_local_result"

    aput-object v2, v0, v1

    sput-object v0, Ljpl;->a:[Ljava/lang/String;

    .line 709
    new-instance v0, Ljpm;

    invoke-direct {v0}, Ljpm;-><init>()V

    sput-object v0, Ljpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ljpl;->e:Ljava/util/HashMap;

    .line 193
    iput-boolean v1, p0, Ljpl;->f:Z

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljpl;->g:Ljava/util/ArrayList;

    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljpl;->h:Ljava/util/ArrayList;

    .line 196
    iput-boolean v1, p0, Ljpl;->i:Z

    .line 197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljpl;->j:Ljava/util/ArrayList;

    .line 198
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ljpl;->k:Ljava/util/HashSet;

    .line 199
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljpl;->l:Ljava/util/ArrayList;

    .line 201
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ljpl;->m:Ljava/util/HashSet;

    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpl;->p:Z

    .line 206
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Ljpl;->e:Ljava/util/HashMap;

    .line 193
    iput-boolean v1, p0, Ljpl;->f:Z

    .line 194
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ljpl;->g:Ljava/util/ArrayList;

    .line 195
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ljpl;->h:Ljava/util/ArrayList;

    .line 196
    iput-boolean v1, p0, Ljpl;->i:Z

    .line 197
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ljpl;->j:Ljava/util/ArrayList;

    .line 198
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Ljpl;->k:Ljava/util/HashSet;

    .line 199
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ljpl;->l:Ljava/util/ArrayList;

    .line 201
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Ljpl;->m:Ljava/util/HashSet;

    .line 204
    iput-boolean v0, p0, Ljpl;->p:Z

    .line 639
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ljpl;->d:Ljava/lang/String;

    .line 640
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ljpl;->b:Ljava/lang/String;

    .line 641
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ljpl;->c:Ljava/lang/String;

    .line 642
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Ljpl;->p:Z

    .line 643
    iget-object v0, p0, Ljpl;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Ljpl;->m:Ljava/util/HashSet;

    invoke-static {p1, v0, v1}, Ljpl;->a(Landroid/os/Parcel;Ljava/util/List;Ljava/util/Set;)V

    .line 645
    iget-object v0, p0, Ljpl;->j:Ljava/util/ArrayList;

    iget-object v1, p0, Ljpl;->k:Ljava/util/HashSet;

    invoke-static {p1, v0, v1}, Ljpl;->a(Landroid/os/Parcel;Ljava/util/List;Ljava/util/Set;)V

    .line 646
    return-void

    :cond_0
    move v0, v1

    .line 642
    goto :goto_0
.end method

.method private static a(I)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 351
    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-eq p0, v1, :cond_1

    .line 353
    const-string v1, "PeopleSearchResults"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 354
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid profileType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    move p0, v0

    .line 358
    :cond_1
    return p0
.end method

.method private static a(Landroid/os/Parcel;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/List",
            "<",
            "Ljpq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 659
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 660
    invoke-virtual {p0, v5}, Landroid/os/Parcel;->writeInt(I)V

    move v4, v3

    .line 661
    :goto_0
    if-ge v4, v5, :cond_2

    .line 662
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpq;

    .line 663
    iget-object v1, v0, Ljpq;->f:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 664
    iget-object v1, v0, Ljpq;->g:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 665
    iget-object v1, v0, Ljpq;->h:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666
    iget v1, v0, Ljpq;->i:I

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 667
    iget-object v1, v0, Ljpq;->j:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 668
    iget-object v1, v0, Ljpq;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 669
    iget-boolean v1, v0, Ljpq;->b:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_1
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 670
    iget-boolean v1, v0, Ljpq;->k:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_2
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 671
    iget v1, v0, Ljpq;->c:I

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 672
    iget-object v0, v0, Ljpq;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 661
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_0
    move v1, v3

    .line 669
    goto :goto_1

    :cond_1
    move v1, v3

    .line 670
    goto :goto_2

    .line 674
    :cond_2
    return-void
.end method

.method private static a(Landroid/os/Parcel;Ljava/util/List;Ljava/util/Set;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/List",
            "<",
            "Ljpq;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 678
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v12

    .line 679
    const/4 v0, 0x0

    move v11, v0

    :goto_0
    if-ge v11, v12, :cond_5

    .line 680
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 681
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 682
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 683
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 684
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 685
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 686
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v7, 0x1

    .line 687
    :goto_1
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v8, 0x1

    .line 688
    :goto_2
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 689
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 690
    new-instance v0, Ljpq;

    invoke-direct/range {v0 .. v10}, Ljpq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZILjava/lang/String;)V

    .line 693
    iget-object v1, v0, Ljpq;->f:Ljava/lang/String;

    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 694
    const-string v1, "PeopleSearchResults"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 695
    const-string v1, "duplicate IDs "

    iget-object v0, v0, Ljpq;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 679
    :cond_0
    :goto_3
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_0

    .line 686
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    .line 687
    :cond_2
    const/4 v8, 0x0

    goto :goto_2

    .line 695
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 698
    :cond_4
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 699
    iget-object v0, v0, Ljpq;->f:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 702
    :cond_5
    return-void
.end method

.method private a(Ljpq;Ljava/util/Set;Ljava/util/Set;)[Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljpq;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 468
    iget-object v5, p1, Ljpq;->g:Ljava/lang/String;

    .line 469
    invoke-interface {p2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    :goto_0
    return-object v1

    .line 472
    :cond_0
    iget-object v0, p0, Ljpl;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljpl;->e:Ljava/util/HashMap;

    .line 473
    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 474
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v0, v1

    .line 477
    :cond_1
    const/16 v4, 0x11

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v6, p1, Ljpq;->f:Ljava/lang/String;

    aput-object v6, v4, v3

    aput-object v1, v4, v2

    const/4 v6, 0x2

    iget-object v7, p1, Ljpq;->g:Ljava/lang/String;

    aput-object v7, v4, v6

    const/4 v6, 0x3

    iget-object v7, p1, Ljpq;->h:Ljava/lang/String;

    aput-object v7, v4, v6

    const/4 v6, 0x4

    iget v7, p1, Ljpq;->i:I

    .line 482
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x5

    iget-object v7, p1, Ljpq;->j:Ljava/lang/String;

    aput-object v7, v4, v6

    const/4 v6, 0x6

    aput-object v0, v4, v6

    const/4 v0, 0x7

    aput-object v1, v4, v0

    const/16 v0, 0x8

    aput-object v1, v4, v0

    const/16 v0, 0x9

    aput-object v1, v4, v0

    const/16 v0, 0xa

    aput-object v1, v4, v0

    const/16 v0, 0xb

    iget-object v1, p1, Ljpq;->a:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v1, 0xc

    iget-boolean v0, p1, Ljpq;->b:Z

    if-eqz v0, :cond_3

    move v0, v2

    .line 490
    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    const/16 v0, 0xd

    iget-boolean v1, p1, Ljpq;->k:Z

    if-eqz v1, :cond_4

    .line 491
    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0xe

    iget v1, p1, Ljpq;->c:I

    .line 492
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    const/16 v0, 0xf

    iget-object v1, p1, Ljpq;->d:Ljava/lang/String;

    aput-object v1, v4, v0

    const/16 v0, 0x10

    .line 494
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    .line 496
    iget-object v0, p0, Ljpl;->n:Lhym;

    invoke-virtual {v0, v4}, Lhym;->a([Ljava/lang/Object;)V

    .line 497
    invoke-interface {p2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 498
    iget-object v0, p1, Ljpq;->h:Ljava/lang/String;

    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v1, v4

    .line 499
    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 473
    goto/16 :goto_1

    :cond_3
    move v0, v3

    .line 482
    goto :goto_2

    :cond_4
    move v2, v3

    .line 490
    goto :goto_3
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 631
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 632
    const-string v1, "@gmail.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 633
    const-string v1, "."

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 635
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Ljpl;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Ljpl;->c:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Ljpl;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 14

    .prologue
    .line 299
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ljpl;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    if-eqz p6, :cond_5

    .line 307
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    const/4 v1, 0x0

    .line 309
    :goto_1
    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 310
    const/16 v2, 0x7c

    move-object/from16 v0, p6

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 311
    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    .line 312
    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->length()I

    move-result v2

    .line 314
    :cond_2
    move-object/from16 v0, p6

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 316
    add-int/lit8 v4, v2, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 317
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    const/16 v1, 0x7c

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 320
    :cond_3
    add-int/lit8 v1, v2, 0x1

    .line 321
    goto :goto_1

    .line 322
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 323
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 326
    :goto_2
    iget-object v13, p0, Ljpl;->h:Ljava/util/ArrayList;

    new-instance v1, Ljpo;

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    invoke-direct/range {v1 .. v12}, Ljpo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    move-object/from16 v7, p6

    goto :goto_2
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 341
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    :goto_0
    return-void

    .line 344
    :cond_0
    iget-object v7, p0, Ljpl;->g:Ljava/util/ArrayList;

    new-instance v0, Ljpn;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Ljpn;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 254
    if-nez p2, :cond_0

    iget-object v0, p0, Ljpl;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    :goto_0
    return-void

    .line 258
    :cond_0
    iput-object p1, p0, Ljpl;->b:Ljava/lang/String;

    .line 259
    iget-object v0, p0, Ljpl;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 260
    iget-object v0, p0, Ljpl;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 261
    iget-object v0, p0, Ljpl;->k:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 262
    iget-object v0, p0, Ljpl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 263
    iget-object v0, p0, Ljpl;->m:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 264
    iput-boolean v1, p0, Ljpl;->i:Z

    .line 265
    iput-boolean v1, p0, Ljpl;->o:Z

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Ljpl;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 273
    iput-boolean p1, p0, Ljpl;->p:Z

    .line 274
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpl;->o:Z

    .line 275
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZ)Z
    .locals 10

    .prologue
    .line 364
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljpl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljpl;->k:Ljava/util/HashSet;

    .line 365
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    :cond_0
    const/4 v0, 0x0

    .line 374
    :goto_0
    return v0

    .line 369
    :cond_1
    iget-object v0, p0, Ljpl;->k:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 370
    iget-object v9, p0, Ljpl;->j:Ljava/util/ArrayList;

    new-instance v0, Ljpq;

    .line 371
    invoke-static {p4}, Ljpl;->a(I)I

    move-result v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Ljpq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZ)V

    .line 370
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpl;->o:Z

    .line 374
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZILjava/lang/String;)Z
    .locals 12

    .prologue
    .line 383
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljpl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljpl;->m:Ljava/util/HashSet;

    .line 384
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385
    :cond_0
    const/4 v0, 0x0

    .line 394
    :goto_0
    return v0

    .line 388
    :cond_1
    iget-object v0, p0, Ljpl;->m:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 389
    iget-object v11, p0, Ljpl;->l:Ljava/util/ArrayList;

    new-instance v0, Ljpq;

    .line 391
    invoke-static/range {p4 .. p4}, Ljpl;->a(I)I

    move-result v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Ljpq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZILjava/lang/String;)V

    .line 389
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpl;->o:Z

    .line 394
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Ljpl;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 245
    iput-object p1, p0, Ljpl;->d:Ljava/lang/String;

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpl;->o:Z

    .line 247
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ljpl;->a(Ljava/lang/String;Z)V

    .line 251
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Ljpl;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 231
    invoke-virtual {p0}, Ljpl;->m()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 706
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Ljpl;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Ljpl;->j:Ljava/util/ArrayList;

    .line 240
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Ljpl;->l:Ljava/util/ArrayList;

    .line 241
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x3e8

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Ljpl;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpl;->o:Z

    .line 280
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpl;->f:Z

    .line 288
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    iget-object v0, p0, Ljpl;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 292
    iput-boolean v1, p0, Ljpl;->i:Z

    .line 293
    iput-boolean v1, p0, Ljpl;->o:Z

    .line 294
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpl;->i:Z

    .line 332
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Ljpl;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 336
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpl;->o:Z

    .line 337
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 347
    return-void
.end method

.method public l()I
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Ljpl;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Ljpl;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public m()Landroid/database/Cursor;
    .locals 15

    .prologue
    const/16 v14, 0x9

    const/4 v13, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 503
    iget-boolean v0, p0, Ljpl;->o:Z

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Ljpl;->n:Lhym;

    .line 622
    :goto_0
    return-object v0

    .line 507
    :cond_0
    new-instance v0, Lhym;

    sget-object v1, Ljpl;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Ljpl;->n:Lhym;

    .line 508
    iput-boolean v3, p0, Ljpl;->o:Z

    .line 510
    iget-boolean v0, p0, Ljpl;->i:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ljpl;->f:Z

    if-nez v0, :cond_2

    .line 511
    :cond_1
    iget-object v0, p0, Ljpl;->n:Lhym;

    goto :goto_0

    .line 517
    :cond_2
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 518
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 519
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 521
    iget-boolean v0, p0, Ljpl;->p:Z

    if-eqz v0, :cond_d

    .line 524
    iget-object v0, p0, Ljpl;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpo;

    .line 525
    iget-object v9, v0, Ljpo;->g:Ljava/lang/String;

    invoke-interface {v6, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/16 v1, 0x11

    new-array v10, v1, [Ljava/lang/Object;

    iget-object v1, v0, Ljpo;->f:Ljava/lang/String;

    aput-object v1, v10, v4

    aput-object v5, v10, v3

    const/4 v1, 0x2

    aput-object v9, v10, v1

    iget-object v1, v0, Ljpo;->h:Ljava/lang/String;

    aput-object v1, v10, v13

    const/4 v1, 0x4

    iget v11, v0, Ljpo;->i:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v1

    const/4 v1, 0x5

    iget-object v11, v0, Ljpo;->j:Ljava/lang/String;

    aput-object v11, v10, v1

    const/4 v1, 0x6

    iget-object v11, v0, Ljpo;->a:Ljava/lang/String;

    aput-object v11, v10, v1

    const/4 v1, 0x7

    iget-object v11, v0, Ljpo;->b:Ljava/lang/String;

    aput-object v11, v10, v1

    const/16 v1, 0x8

    aput-object v5, v10, v1

    iget-object v1, v0, Ljpo;->c:Ljava/lang/String;

    aput-object v1, v10, v14

    const/16 v1, 0xa

    iget-object v11, v0, Ljpo;->d:Ljava/lang/String;

    aput-object v11, v10, v1

    const/16 v1, 0xb

    aput-object v5, v10, v1

    const/16 v11, 0xc

    iget-boolean v1, v0, Ljpo;->e:Z

    if-eqz v1, :cond_5

    move v1, v3

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v10, v11

    const/16 v11, 0xd

    iget-boolean v1, v0, Ljpo;->k:Z

    if-eqz v1, :cond_6

    move v1, v3

    :goto_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v10, v11

    const/16 v1, 0xe

    const/4 v11, -0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v1

    const/16 v1, 0xf

    const-string v11, ""

    aput-object v11, v10, v1

    const/16 v1, 0x10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v1

    iget-object v1, p0, Ljpl;->n:Lhym;

    invoke-virtual {v1, v10}, Lhym;->a([Ljava/lang/Object;)V

    invoke-interface {v6, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Ljpo;->h:Ljava/lang/String;

    invoke-interface {v7, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 526
    :cond_4
    iget-object v1, v0, Ljpo;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 527
    iget-object v0, v0, Ljpo;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Ljpl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_5
    move v1, v4

    .line 525
    goto :goto_2

    :cond_6
    move v1, v4

    goto :goto_3

    .line 538
    :cond_7
    iget-object v0, p0, Ljpl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpq;

    .line 540
    iget-object v1, p0, Ljpl;->e:Ljava/util/HashMap;

    iget-object v9, v0, Ljpq;->g:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Ljpl;->e:Ljava/util/HashMap;

    iget-object v9, v0, Ljpq;->g:Ljava/lang/String;

    .line 541
    invoke-virtual {v1, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 542
    :goto_5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 543
    invoke-direct {p0, v0, v6, v7}, Ljpl;->a(Ljpq;Ljava/util/Set;Ljava/util/Set;)[Ljava/lang/Object;

    goto :goto_4

    :cond_9
    move-object v1, v5

    .line 541
    goto :goto_5

    .line 546
    :cond_a
    iget-object v0, p0, Ljpl;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpq;

    .line 548
    iget-object v1, p0, Ljpl;->e:Ljava/util/HashMap;

    iget-object v9, v0, Ljpq;->g:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Ljpl;->e:Ljava/util/HashMap;

    iget-object v9, v0, Ljpq;->g:Ljava/lang/String;

    .line 549
    invoke-virtual {v1, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 550
    :goto_7
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 551
    invoke-direct {p0, v0, v6, v7}, Ljpl;->a(Ljpq;Ljava/util/Set;Ljava/util/Set;)[Ljava/lang/Object;

    goto :goto_6

    :cond_c
    move-object v1, v5

    .line 549
    goto :goto_7

    .line 556
    :cond_d
    iget-object v0, p0, Ljpl;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 564
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 565
    iget-object v0, p0, Ljpl;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_e
    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpn;

    .line 568
    iget-object v1, v0, Ljpn;->h:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 569
    iget-object v11, v0, Ljpn;->b:Ljava/lang/String;

    .line 575
    invoke-direct {p0, v11}, Ljpl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 576
    invoke-virtual {v9, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    .line 580
    if-eqz v1, :cond_11

    .line 581
    aget-object v2, v1, v13

    check-cast v2, Ljava/lang/String;

    .line 584
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_f

    .line 585
    invoke-virtual {v2, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 590
    :cond_f
    iget-object v2, v0, Ljpn;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 591
    iget-object v2, v0, Ljpn;->f:Ljava/lang/String;

    aput-object v2, v1, v4

    .line 592
    iget-object v2, v0, Ljpn;->a:Ljava/lang/String;

    aput-object v2, v1, v3

    .line 593
    iget-object v2, v0, Ljpn;->h:Ljava/lang/String;

    aput-object v2, v1, v13

    .line 596
    aget-object v2, v1, v14

    if-nez v2, :cond_10

    .line 597
    iget-object v2, v0, Ljpn;->c:Ljava/lang/String;

    aput-object v2, v1, v14

    .line 599
    :cond_10
    const/16 v2, 0xa

    aget-object v2, v1, v2

    if-nez v2, :cond_e

    .line 600
    const/16 v2, 0xa

    iget-object v0, v0, Ljpn;->d:Ljava/lang/String;

    aput-object v0, v1, v2

    goto :goto_8

    .line 608
    :cond_11
    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v0, Ljpn;->f:Ljava/lang/String;

    aput-object v2, v1, v4

    iget-object v2, v0, Ljpn;->a:Ljava/lang/String;

    aput-object v2, v1, v3

    const/4 v2, 0x2

    aput-object v5, v1, v2

    iget-object v2, v0, Ljpn;->h:Ljava/lang/String;

    aput-object v2, v1, v13

    const/4 v2, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v1, v2

    const/4 v2, 0x5

    aput-object v5, v1, v2

    const/4 v2, 0x6

    aput-object v5, v1, v2

    const/4 v2, 0x7

    aput-object v5, v1, v2

    const/16 v2, 0x8

    iget-object v12, v0, Ljpn;->b:Ljava/lang/String;

    aput-object v12, v1, v2

    iget-object v2, v0, Ljpn;->c:Ljava/lang/String;

    aput-object v2, v1, v14

    const/16 v2, 0xa

    iget-object v0, v0, Ljpn;->d:Ljava/lang/String;

    aput-object v0, v1, v2

    const/16 v0, 0xb

    aput-object v5, v1, v0

    const/16 v0, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xd

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xe

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xf

    const-string v2, ""

    aput-object v2, v1, v0

    const/16 v0, 0x10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    iget-object v0, p0, Ljpl;->n:Lhym;

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    invoke-virtual {v9, v11, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_8

    .line 615
    :cond_12
    iget-object v0, p0, Ljpl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpq;

    .line 616
    invoke-direct {p0, v0, v6, v7}, Ljpl;->a(Ljpq;Ljava/util/Set;Ljava/util/Set;)[Ljava/lang/Object;

    goto :goto_9

    .line 618
    :cond_13
    iget-object v0, p0, Ljpl;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpq;

    .line 619
    invoke-direct {p0, v0, v6, v7}, Ljpl;->a(Ljpq;Ljava/util/Set;Ljava/util/Set;)[Ljava/lang/Object;

    goto :goto_a

    .line 622
    :cond_14
    iget-object v0, p0, Ljpl;->n:Lhym;

    goto/16 :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Ljpl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 651
    iget-object v0, p0, Ljpl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 652
    iget-object v0, p0, Ljpl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 653
    iget-boolean v0, p0, Ljpl;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 654
    iget-object v0, p0, Ljpl;->l:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Ljpl;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 655
    iget-object v0, p0, Ljpl;->j:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Ljpl;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 656
    return-void

    .line 653
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
