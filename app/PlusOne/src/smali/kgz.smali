.class public final Lkgz;
.super Lkhl;
.source "PG"


# instance fields
.field private b:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1}, Lkhl;-><init>(Landroid/content/Context;)V

    .line 34
    const v0, 0x7f040119

    invoke-virtual {p0, v0}, Lkgz;->e(I)V

    invoke-virtual {p0, v1}, Lkgz;->d(Z)V

    invoke-virtual {p0, v1}, Lkgz;->c(Z)V

    .line 35
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lkgz;->b:Landroid/content/Intent;

    .line 46
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 50
    invoke-super {p0, p1}, Lkhl;->a(Landroid/view/View;)V

    .line 52
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a056b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 54
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 55
    new-instance v2, Lljt;

    invoke-virtual {p0}, Lkgz;->B()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lkgz;->b:Landroid/content/Intent;

    invoke-direct {v2, v3, v4}, Lljt;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    const/4 v3, 0x0

    .line 56
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v4, 0x21

    .line 55
    invoke-interface {v1, v2, v3, v0, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 58
    const v0, 0x7f100345

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 59
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 61
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 62
    return-void
.end method
