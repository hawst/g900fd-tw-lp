.class public final Lmgz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lngc;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9556
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9559
    iput-object v0, p0, Lmgz;->apiHeader:Llyr;

    .line 9562
    iput-object v0, p0, Lmgz;->a:Lngc;

    .line 9556
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9579
    const/4 v0, 0x0

    .line 9580
    iget-object v1, p0, Lmgz;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 9581
    const/4 v0, 0x1

    iget-object v1, p0, Lmgz;->apiHeader:Llyr;

    .line 9582
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9584
    :cond_0
    iget-object v1, p0, Lmgz;->a:Lngc;

    if-eqz v1, :cond_1

    .line 9585
    const/4 v1, 0x2

    iget-object v2, p0, Lmgz;->a:Lngc;

    .line 9586
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9588
    :cond_1
    iget-object v1, p0, Lmgz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9589
    iput v0, p0, Lmgz;->ai:I

    .line 9590
    return v0
.end method

.method public a(Loxn;)Lmgz;
    .locals 2

    .prologue
    .line 9598
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9599
    sparse-switch v0, :sswitch_data_0

    .line 9603
    iget-object v1, p0, Lmgz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9604
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgz;->ah:Ljava/util/List;

    .line 9607
    :cond_1
    iget-object v1, p0, Lmgz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9609
    :sswitch_0
    return-object p0

    .line 9614
    :sswitch_1
    iget-object v0, p0, Lmgz;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 9615
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgz;->apiHeader:Llyr;

    .line 9617
    :cond_2
    iget-object v0, p0, Lmgz;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9621
    :sswitch_2
    iget-object v0, p0, Lmgz;->a:Lngc;

    if-nez v0, :cond_3

    .line 9622
    new-instance v0, Lngc;

    invoke-direct {v0}, Lngc;-><init>()V

    iput-object v0, p0, Lmgz;->a:Lngc;

    .line 9624
    :cond_3
    iget-object v0, p0, Lmgz;->a:Lngc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9599
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9567
    iget-object v0, p0, Lmgz;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 9568
    const/4 v0, 0x1

    iget-object v1, p0, Lmgz;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9570
    :cond_0
    iget-object v0, p0, Lmgz;->a:Lngc;

    if-eqz v0, :cond_1

    .line 9571
    const/4 v0, 0x2

    iget-object v1, p0, Lmgz;->a:Lngc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9573
    :cond_1
    iget-object v0, p0, Lmgz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9575
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9552
    invoke-virtual {p0, p1}, Lmgz;->a(Loxn;)Lmgz;

    move-result-object v0

    return-object v0
.end method
