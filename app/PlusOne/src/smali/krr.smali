.class public final Lkrr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liht;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/socialcast/impl/CastService;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    .locals 0

    .prologue
    .line 780
    iput-object p1, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 784
    const-string v1, "CastService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 785
    const-string v1, "onMessageReceived: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 789
    :cond_0
    :goto_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 790
    const-string v2, "msg"

    invoke-static {v1, v2}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 792
    if-nez v2, :cond_4

    .line 794
    const-string v0, "CastService"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 795
    const-string v0, "Got unknown message from Chromecast. message: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 854
    :cond_1
    :goto_1
    return-void

    .line 785
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 795
    :cond_3
    :try_start_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 851
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 852
    const-string v2, "CastService"

    const-string v3, "Error parsing JSON from Chromecast. message: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 801
    :cond_4
    :try_start_2
    const-string v3, "state"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 802
    iget-object v2, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v3, "slideshow_is_playing"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->b(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z

    .line 803
    iget-object v2, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v3, "card_has_next"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->c(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z

    .line 804
    iget-object v2, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v3, "card_has_prev"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->d(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z

    .line 805
    iget-object v2, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v3, "card_is_expandable"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z

    .line 806
    iget-object v2, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v3, "card_is_expanded"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z

    .line 807
    const-string v2, "card_image_url"

    invoke-static {v1, v2}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 808
    iget-object v3, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v4, "card_activity_id"

    invoke-static {v1, v4}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->b(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;

    .line 809
    const-string v3, "previous_card_image_url"

    invoke-static {v1, v3}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 810
    const-string v4, "next_card_image_url"

    invoke-static {v1, v4}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 812
    iget-object v5, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v6, "author_avatar"

    invoke-static {v1, v6}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->c(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;

    .line 813
    iget-object v5, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v6, "author_name"

    invoke-static {v1, v6}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->d(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;

    .line 814
    iget-object v5, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v6, "post_text"

    invoke-static {v1, v6}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;

    .line 816
    if-eqz v2, :cond_5

    .line 817
    iget-object v1, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v1, v2}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)V

    .line 821
    :cond_5
    if-eqz v3, :cond_b

    .line 822
    iget-object v1, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v1, v3, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 827
    :goto_3
    if-eqz v4, :cond_6

    .line 828
    iget-object v0, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v0, v4, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    .line 832
    :cond_6
    if-eqz v1, :cond_7

    .line 833
    iget-object v2, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v2}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->m(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lizs;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lizs;->c(Lizu;II)Lcom/google/android/libraries/social/media/MediaResource;

    .line 837
    :cond_7
    if-eqz v0, :cond_8

    .line 838
    iget-object v1, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->m(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lizs;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lizs;->c(Lizu;II)Lcom/google/android/libraries/social/media/MediaResource;

    .line 842
    :cond_8
    iget-object v0, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->n(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    .line 844
    iget-object v0, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z

    goto/16 :goto_1

    .line 845
    :cond_9
    const-string v0, "auth"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 846
    const-string v0, "id"

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 847
    if-eqz v0, :cond_1

    iget-object v1, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->o(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 848
    iget-object v0, p0, Lkrr;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->p(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 852
    :cond_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    move-object v1, v0

    goto :goto_3
.end method
