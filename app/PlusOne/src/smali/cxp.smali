.class final Lcxp;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcxp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcxp;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method

.method public static a(Landroid/os/Bundle;)I
    .locals 2

    .prologue
    .line 102
    const-string v0, "all_photos_row_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const/4 v0, 0x3

    .line 109
    :goto_0
    return v0

    .line 105
    :cond_0
    const-string v0, "photo_ref"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    .line 106
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    const/4 v0, 0x2

    goto :goto_0

    .line 108
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lizu;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 109
    const/4 v0, 0x1

    goto :goto_0

    .line 112
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown PhotoOrigin"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a()[I
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcxp;->a:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    return-object v0
.end method
