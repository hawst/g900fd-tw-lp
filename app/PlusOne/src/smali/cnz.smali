.class public final Lcnz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcnq;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Lctz;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcnz;->a:Landroid/app/Activity;

    .line 33
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 34
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 44
    iget-object v1, p0, Lcnz;->b:Lctz;

    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    const-class v2, Ljuf;

    .line 45
    invoke-virtual {v1, v2}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    .line 46
    const-string v2, "shareables"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 48
    iget-object v1, p0, Lcnz;->a:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 49
    iget-object v0, p0, Lcnz;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 50
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lctz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcnz;->b:Lctz;

    .line 39
    return-void
.end method
