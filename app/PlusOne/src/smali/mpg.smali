.class public final Lmpg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmpg;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lmpg;

    sput-object v0, Lmpg;->a:[Lmpg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24
    const/high16 v0, -0x80000000

    iput v0, p0, Lmpg;->d:I

    .line 17
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x0

    .line 45
    iget-object v1, p0, Lmpg;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 46
    const/4 v0, 0x1

    iget-object v1, p0, Lmpg;->b:Ljava/lang/String;

    .line 47
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 49
    :cond_0
    iget-object v1, p0, Lmpg;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 50
    const/4 v1, 0x2

    iget-object v2, p0, Lmpg;->c:Ljava/lang/String;

    .line 51
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    :cond_1
    iget v1, p0, Lmpg;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 54
    const/4 v1, 0x3

    iget v2, p0, Lmpg;->d:I

    .line 55
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_2
    iget-object v1, p0, Lmpg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    iput v0, p0, Lmpg;->ai:I

    .line 59
    return v0
.end method

.method public a(Loxn;)Lmpg;
    .locals 2

    .prologue
    .line 67
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 68
    sparse-switch v0, :sswitch_data_0

    .line 72
    iget-object v1, p0, Lmpg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmpg;->ah:Ljava/util/List;

    .line 76
    :cond_1
    iget-object v1, p0, Lmpg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    :sswitch_0
    return-object p0

    .line 83
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpg;->b:Ljava/lang/String;

    goto :goto_0

    .line 87
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpg;->c:Ljava/lang/String;

    goto :goto_0

    .line 91
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 92
    if-eqz v0, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x28

    if-eq v0, v1, :cond_2

    const/16 v1, 0x32

    if-ne v0, v1, :cond_3

    .line 98
    :cond_2
    iput v0, p0, Lmpg;->d:I

    goto :goto_0

    .line 100
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmpg;->d:I

    goto :goto_0

    .line 68
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lmpg;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x1

    iget-object v1, p0, Lmpg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 32
    :cond_0
    iget-object v0, p0, Lmpg;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 33
    const/4 v0, 0x2

    iget-object v1, p0, Lmpg;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 35
    :cond_1
    iget v0, p0, Lmpg;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 36
    const/4 v0, 0x3

    iget v1, p0, Lmpg;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 38
    :cond_2
    iget-object v0, p0, Lmpg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 40
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lmpg;->a(Loxn;)Lmpg;

    move-result-object v0

    return-object v0
.end method
