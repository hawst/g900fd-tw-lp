.class public Lpyq;
.super Landroid/graphics/drawable/Drawable;
.source "PG"

# interfaces
.implements Landroid/graphics/drawable/Animatable;
.implements Landroid/os/Handler$Callback;
.implements Ljava/lang/Runnable;


# static fields
.field private static U:Lpyr;

.field private static V:Landroid/os/Handler;

.field private static final b:[B

.field private static c:Landroid/graphics/Paint;

.field private static d:Landroid/graphics/Paint;


# instance fields
.field private A:I

.field private B:I

.field private C:[B

.field private D:I

.field private E:Z

.field private F:I

.field private G:[S

.field private H:[B

.field private I:[B

.field private J:[B

.field private K:Z

.field private L:[I

.field private M:I

.field private N:J

.field private O:Z

.field private P:I

.field private Q:I

.field private R:Z

.field private S:Z

.field private final T:Landroid/os/Handler;

.field private W:Z

.field private X:Z

.field private Y:I

.field private Z:I

.field public a:Landroid/graphics/Bitmap;

.field private final aa:Landroid/graphics/Bitmap$Config;

.field private ab:Z

.field private e:Lpys;

.field private final f:[B

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:[I

.field private m:Z

.field private n:F

.field private volatile o:Z

.field private volatile p:Z

.field private volatile q:Z

.field private r:I

.field private s:Z

.field private t:I

.field private u:[I

.field private v:[I

.field private w:Z

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "NETSCAPE2.0"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lpyq;->b:[B

    return-void
.end method

.method public constructor <init>(Lpys;Landroid/graphics/Bitmap$Config;)V
    .locals 5

    .prologue
    const/16 v1, 0x1000

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 126
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 72
    iput-boolean v3, p0, Lpyq;->q:Z

    .line 90
    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lpyq;->C:[B

    .line 91
    iput v4, p0, Lpyq;->D:I

    .line 96
    new-array v0, v1, [S

    iput-object v0, p0, Lpyq;->G:[S

    .line 97
    new-array v0, v1, [B

    iput-object v0, p0, Lpyq;->H:[B

    .line 98
    const/16 v0, 0x1001

    new-array v0, v0, [B

    iput-object v0, p0, Lpyq;->I:[B

    .line 112
    iput-boolean v3, p0, Lpyq;->S:Z

    .line 113
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lpyq;->T:Landroid/os/Handler;

    .line 120
    iput v2, p0, Lpyq;->Y:I

    .line 121
    iput v2, p0, Lpyq;->Z:I

    .line 124
    iput-boolean v3, p0, Lpyq;->ab:Z

    .line 127
    iput-object p2, p0, Lpyq;->aa:Landroid/graphics/Bitmap$Config;

    .line 130
    sget-object v0, Lpyq;->U:Lpyr;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lpyr;

    invoke-direct {v0}, Lpyr;-><init>()V

    .line 132
    sput-object v0, Lpyq;->U:Lpyr;

    invoke-virtual {v0}, Lpyr;->start()V

    .line 133
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lpyq;->U:Lpyr;

    invoke-virtual {v1}, Lpyr;->getLooper()Landroid/os/Looper;

    move-result-object v1

    sget-object v2, Lpyq;->U:Lpyr;

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    sput-object v0, Lpyq;->V:Landroid/os/Handler;

    .line 136
    :cond_0
    sget-object v0, Lpyq;->c:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 137
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lpyq;->c:Landroid/graphics/Paint;

    .line 138
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 139
    sput-object v0, Lpyq;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 142
    :cond_1
    iput-object p1, p0, Lpyq;->e:Lpys;

    .line 143
    invoke-virtual {p1}, Lpys;->a()[B

    move-result-object v0

    iput-object v0, p0, Lpyq;->f:[B

    .line 144
    iget-object v0, p0, Lpyq;->e:Lpys;

    iget v0, v0, Lpys;->a:I

    iput v0, p0, Lpyq;->g:I

    .line 145
    invoke-virtual {p1}, Lpys;->b()I

    move-result v0

    iput v0, p0, Lpyq;->h:I

    iput v0, p0, Lpyq;->B:I

    iput v0, p0, Lpyq;->z:I

    .line 146
    invoke-virtual {p1}, Lpys;->c()I

    move-result v0

    iput v0, p0, Lpyq;->i:I

    iput v0, p0, Lpyq;->A:I

    .line 147
    iget-object v0, p0, Lpyq;->e:Lpys;

    iget v0, v0, Lpys;->d:I

    iput v0, p0, Lpyq;->r:I

    .line 148
    iget-object v0, p0, Lpyq;->e:Lpys;

    iget-boolean v0, v0, Lpys;->b:Z

    iput-boolean v0, p0, Lpyq;->o:Z

    .line 150
    iget-boolean v0, p0, Lpyq;->o:Z

    if-nez v0, :cond_2

    .line 152
    :try_start_0
    iget v0, p0, Lpyq;->h:I

    iget v1, p0, Lpyq;->i:I

    iget-object v2, p0, Lpyq;->aa:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lpyq;->a:Landroid/graphics/Bitmap;

    .line 153
    iget-object v0, p0, Lpyq;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_3

    .line 154
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Cannot allocate bitmap"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :catch_0
    move-exception v0

    iput-boolean v3, p0, Lpyq;->o:Z

    .line 170
    :cond_2
    :goto_0
    return-void

    .line 157
    :cond_3
    :try_start_1
    iget v0, p0, Lpyq;->h:I

    iget v1, p0, Lpyq;->i:I

    mul-int/2addr v0, v1

    .line 158
    new-array v1, v0, [I

    iput-object v1, p0, Lpyq;->l:[I

    .line 159
    new-array v0, v0, [B

    iput-object v0, p0, Lpyq;->J:[B

    .line 161
    iget v0, p0, Lpyq;->i:I

    iput v0, p0, Lpyq;->j:I

    .line 162
    iget v0, p0, Lpyq;->i:I

    iput v0, p0, Lpyq;->k:I

    .line 165
    sget-object v0, Lpyq;->V:Landroid/os/Handler;

    sget-object v1, Lpyq;->V:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method static synthetic a(Lpyq;)V
    .locals 22

    .prologue
    .line 23
    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->g:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0x3b

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->X:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lpyq;->ab:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->ab:Z

    :cond_2
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->D:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->E:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->X:Z

    const/16 v2, 0x64

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->Q:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lpyq;->u:[I

    :cond_3
    :goto_2
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->g:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lpyq;->g:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    sparse-switch v2, :sswitch_data_0

    goto :goto_2

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->g:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lpyq;->g:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    sparse-switch v2, :sswitch_data_1

    invoke-direct/range {p0 .. p0}, Lpyq;->g()V

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lpyq;->D:I

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->K:Z

    goto :goto_1

    :pswitch_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lpyq;->K:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->L:[I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lpyq;->l:[I

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lpyq;->L:[I

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    :pswitch_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->K:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lpyq;->E:Z

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, Lpyq;->r:I

    :cond_5
    const/4 v3, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v4, v0, Lpyq;->A:I

    if-ge v3, v4, :cond_2

    move-object/from16 v0, p0

    iget v4, v0, Lpyq;->y:I

    add-int/2addr v4, v3

    move-object/from16 v0, p0

    iget v5, v0, Lpyq;->h:I

    mul-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lpyq;->x:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lpyq;->z:I

    add-int/2addr v5, v4

    :goto_4
    if-ge v4, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v6, v0, Lpyq;->l:[I

    aput v2, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :sswitch_2
    move-object/from16 v0, p0

    iget v2, v0, Lpyq;->g:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->g:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->g:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lpyq;->g:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    and-int/lit8 v3, v2, 0x1c

    shr-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lpyq;->D:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->E:Z

    invoke-direct/range {p0 .. p0}, Lpyq;->f()I

    move-result v2

    mul-int/lit8 v2, v2, 0xa

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->Q:I

    move-object/from16 v0, p0

    iget v2, v0, Lpyq;->Q:I

    const/16 v3, 0xa

    if-gt v2, v3, :cond_7

    const/16 v2, 0x64

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->Q:I

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->g:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lpyq;->g:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->F:I

    move-object/from16 v0, p0

    iget v2, v0, Lpyq;->g:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->g:I

    goto/16 :goto_2

    :cond_8
    const/4 v2, 0x0

    goto :goto_5

    :sswitch_3
    invoke-direct/range {p0 .. p0}, Lpyq;->e()I

    const/4 v3, 0x1

    const/4 v2, 0x0

    :goto_6
    sget-object v4, Lpyq;->b:[B

    array-length v4, v4

    if-ge v2, v4, :cond_2d

    move-object/from16 v0, p0

    iget-object v4, v0, Lpyq;->C:[B

    aget-byte v4, v4, v2

    sget-object v5, Lpyq;->b:[B

    aget-byte v5, v5, v2

    if-eq v4, v5, :cond_a

    const/4 v2, 0x0

    :goto_7
    if-eqz v2, :cond_b

    :cond_9
    invoke-direct/range {p0 .. p0}, Lpyq;->e()I

    move-result v2

    if-lez v2, :cond_3

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lpyq;->o:Z

    if-eqz v2, :cond_9

    goto/16 :goto_2

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_b
    invoke-direct/range {p0 .. p0}, Lpyq;->g()V

    goto/16 :goto_2

    :sswitch_4
    invoke-direct/range {p0 .. p0}, Lpyq;->g()V

    goto/16 :goto_2

    :sswitch_5
    invoke-direct/range {p0 .. p0}, Lpyq;->g()V

    goto/16 :goto_2

    :sswitch_6
    invoke-direct/range {p0 .. p0}, Lpyq;->f()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->x:I

    invoke-direct/range {p0 .. p0}, Lpyq;->f()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->y:I

    invoke-direct/range {p0 .. p0}, Lpyq;->f()I

    move-result v2

    invoke-direct/range {p0 .. p0}, Lpyq;->f()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lpyq;->h:I

    move-object/from16 v0, p0

    iget v5, v0, Lpyq;->x:I

    sub-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lpyq;->z:I

    move-object/from16 v0, p0

    iget v4, v0, Lpyq;->i:I

    move-object/from16 v0, p0

    iget v5, v0, Lpyq;->y:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lpyq;->A:I

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->B:I

    mul-int/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->J:[B

    array-length v3, v3

    if-le v2, v3, :cond_c

    new-array v2, v2, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Lpyq;->J:[B

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->g:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lpyq;->g:I

    aget-byte v2, v2, v3

    and-int/lit16 v3, v2, 0xff

    and-int/lit8 v2, v3, 0x40

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_8
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->w:Z

    and-int/lit16 v2, v3, 0x80

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->s:Z

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    and-int/lit8 v2, v3, 0x7

    add-int/lit8 v2, v2, 0x1

    int-to-double v2, v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->t:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lpyq;->s:Z

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->u:[I

    if-nez v2, :cond_d

    const/16 v2, 0x100

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lpyq;->u:[I

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->u:[I

    move-object/from16 v0, p0

    iget v4, v0, Lpyq;->t:I

    const/4 v2, 0x0

    :goto_a
    if-ge v2, v4, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v6, v0, Lpyq;->g:I

    add-int/lit8 v7, v6, 0x1

    move-object/from16 v0, p0

    iput v7, v0, Lpyq;->g:I

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    move-object/from16 v0, p0

    iget-object v6, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v7, v0, Lpyq;->g:I

    add-int/lit8 v8, v7, 0x1

    move-object/from16 v0, p0

    iput v8, v0, Lpyq;->g:I

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    move-object/from16 v0, p0

    iget-object v7, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v8, v0, Lpyq;->g:I

    add-int/lit8 v9, v8, 0x1

    move-object/from16 v0, p0

    iput v9, v0, Lpyq;->g:I

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    const/high16 v8, -0x1000000

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v5, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    or-int/2addr v5, v7

    aput v5, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_f
    const/4 v2, 0x0

    goto :goto_9

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->u:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lpyq;->v:[I

    :cond_11
    :goto_b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lpyq;->E:Z

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->v:[I

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->F:I

    aget v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->v:[I

    move-object/from16 v0, p0

    iget v4, v0, Lpyq;->F:I

    const/4 v5, 0x0

    aput v5, v3, v4

    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->v:[I

    if-nez v3, :cond_13

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lpyq;->o:Z

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lpyq;->o:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->z:I

    move-object/from16 v0, p0

    iget v4, v0, Lpyq;->A:I

    mul-int v14, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v4, v0, Lpyq;->g:I

    add-int/lit8 v5, v4, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lpyq;->g:I

    aget-byte v3, v3, v4

    and-int/lit16 v15, v3, 0xff

    const/4 v3, 0x1

    shl-int v16, v3, v15

    add-int/lit8 v17, v16, 0x1

    add-int/lit8 v11, v16, 0x2

    const/4 v10, -0x1

    add-int/lit8 v9, v15, 0x1

    const/4 v3, 0x1

    shl-int/2addr v3, v9

    add-int/lit8 v8, v3, -0x1

    const/4 v3, 0x0

    :goto_c
    move/from16 v0, v16

    if-ge v3, v0, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lpyq;->G:[S

    const/4 v5, 0x0

    aput-short v5, v4, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lpyq;->H:[B

    int-to-byte v5, v3

    aput-byte v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->e:Lpys;

    iget-object v2, v2, Lpys;->c:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lpyq;->v:[I

    move-object/from16 v0, p0

    iget-object v2, v0, Lpyq;->e:Lpys;

    iget v2, v2, Lpys;->e:I

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->F:I

    if-ne v2, v3, :cond_11

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->r:I

    goto/16 :goto_b

    :cond_15
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    :cond_16
    if-ge v3, v14, :cond_25

    move-object/from16 v0, p0

    iget-object v12, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v13, v0, Lpyq;->g:I

    add-int/lit8 v18, v13, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lpyq;->g:I

    aget-byte v12, v12, v13

    and-int/lit16 v12, v12, 0xff

    if-eqz v12, :cond_25

    move-object/from16 v0, p0

    iget v13, v0, Lpyq;->g:I

    add-int v18, v13, v12

    :cond_17
    move-object/from16 v0, p0

    iget v12, v0, Lpyq;->g:I

    move/from16 v0, v18

    if-ge v12, v0, :cond_16

    move-object/from16 v0, p0

    iget-object v12, v0, Lpyq;->f:[B

    move-object/from16 v0, p0

    iget v13, v0, Lpyq;->g:I

    add-int/lit8 v19, v13, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lpyq;->g:I

    aget-byte v12, v12, v13

    and-int/lit16 v12, v12, 0xff

    shl-int/2addr v12, v6

    add-int/2addr v7, v12

    add-int/lit8 v6, v6, 0x8

    :goto_d
    if-lt v6, v9, :cond_17

    and-int v12, v7, v8

    shr-int v13, v7, v9

    sub-int v7, v6, v9

    move/from16 v0, v16

    if-ne v12, v0, :cond_18

    add-int/lit8 v9, v15, 0x1

    const/4 v6, 0x1

    shl-int/2addr v6, v9

    add-int/lit8 v8, v6, -0x1

    add-int/lit8 v11, v16, 0x2

    const/4 v10, -0x1

    move v6, v7

    move v7, v13

    goto :goto_d

    :cond_18
    move/from16 v0, v17

    if-ne v12, v0, :cond_1e

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lpyq;->g:I

    :cond_19
    :goto_e
    invoke-direct/range {p0 .. p0}, Lpyq;->g()V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lpyq;->o:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, Lpyq;->D:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1b

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lpyq;->K:Z

    if-nez v3, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->L:[I

    if-nez v3, :cond_1a

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lpyq;->L:[I

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->l:[I

    array-length v3, v3

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lpyq;->L:[I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1a
    :goto_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->L:[I

    if-eqz v3, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->l:[I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lpyq;->L:[I

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lpyq;->l:[I

    array-length v7, v7

    invoke-static {v3, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lpyq;->K:Z

    :cond_1b
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x0

    :goto_10
    move-object/from16 v0, p0

    iget v7, v0, Lpyq;->A:I

    if-ge v3, v7, :cond_27

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lpyq;->w:Z

    if-eqz v7, :cond_29

    move-object/from16 v0, p0

    iget v7, v0, Lpyq;->A:I

    if-lt v4, v7, :cond_1c

    add-int/lit8 v6, v6, 0x1

    packed-switch v6, :pswitch_data_1

    :cond_1c
    :goto_11
    add-int v7, v4, v5

    move/from16 v21, v4

    move v4, v7

    move/from16 v7, v21

    :goto_12
    move-object/from16 v0, p0

    iget v8, v0, Lpyq;->y:I

    add-int/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lpyq;->i:I

    if-ge v7, v8, :cond_26

    move-object/from16 v0, p0

    iget v8, v0, Lpyq;->h:I

    mul-int/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lpyq;->x:I

    add-int/2addr v8, v7

    move-object/from16 v0, p0

    iget v7, v0, Lpyq;->z:I

    add-int v10, v8, v7

    move-object/from16 v0, p0

    iget v7, v0, Lpyq;->B:I

    mul-int/2addr v7, v3

    move v9, v8

    :goto_13
    if-ge v9, v10, :cond_26

    move-object/from16 v0, p0

    iget-object v11, v0, Lpyq;->J:[B

    add-int/lit8 v8, v7, 0x1

    aget-byte v7, v11, v7

    and-int/lit16 v7, v7, 0xff

    move-object/from16 v0, p0

    iget-object v11, v0, Lpyq;->v:[I

    aget v7, v11, v7

    if-eqz v7, :cond_1d

    move-object/from16 v0, p0

    iget-object v11, v0, Lpyq;->l:[I

    aput v7, v11, v9

    :cond_1d
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    move v7, v8

    goto :goto_13

    :cond_1e
    const/4 v6, -0x1

    if-ne v10, v6, :cond_1f

    move-object/from16 v0, p0

    iget-object v6, v0, Lpyq;->J:[B

    add-int/lit8 v5, v3, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lpyq;->H:[B

    aget-byte v10, v10, v12

    aput-byte v10, v6, v3

    move v3, v5

    move v6, v7

    move v10, v12

    move v7, v13

    move v5, v12

    goto/16 :goto_d

    :cond_1f
    if-lt v12, v11, :cond_2c

    move-object/from16 v0, p0

    iget-object v0, v0, Lpyq;->I:[B

    move-object/from16 v19, v0

    add-int/lit8 v6, v4, 0x1

    int-to-byte v5, v5

    aput-byte v5, v19, v4

    const/16 v4, 0x1001

    if-ne v6, v4, :cond_2b

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lpyq;->o:Z

    goto/16 :goto_e

    :cond_20
    move v5, v6

    :goto_14
    move/from16 v0, v16

    if-lt v4, v0, :cond_23

    const/16 v6, 0x1001

    if-ge v4, v6, :cond_21

    move-object/from16 v0, p0

    iget-object v6, v0, Lpyq;->G:[S

    aget-short v6, v6, v4

    if-ne v4, v6, :cond_22

    :cond_21
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lpyq;->o:Z

    goto/16 :goto_e

    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lpyq;->I:[B

    move-object/from16 v19, v0

    add-int/lit8 v6, v5, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lpyq;->H:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, v4

    aput-byte v20, v19, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lpyq;->G:[S

    aget-short v4, v5, v4

    const/16 v5, 0x1001

    if-ne v6, v5, :cond_20

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lpyq;->o:Z

    goto/16 :goto_e

    :cond_23
    move-object/from16 v0, p0

    iget-object v6, v0, Lpyq;->H:[B

    aget-byte v6, v6, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lpyq;->I:[B

    move-object/from16 v19, v0

    add-int/lit8 v4, v5, 0x1

    int-to-byte v0, v6

    move/from16 v20, v0

    aput-byte v20, v19, v5

    const/16 v5, 0x1000

    if-ge v11, v5, :cond_24

    move-object/from16 v0, p0

    iget-object v5, v0, Lpyq;->G:[S

    int-to-short v10, v10

    aput-short v10, v5, v11

    move-object/from16 v0, p0

    iget-object v5, v0, Lpyq;->H:[B

    int-to-byte v10, v6

    aput-byte v10, v5, v11

    add-int/lit8 v11, v11, 0x1

    and-int v5, v11, v8

    if-nez v5, :cond_24

    const/16 v5, 0x1000

    if-ge v11, v5, :cond_24

    add-int/lit8 v9, v9, 0x1

    add-int/2addr v8, v11

    :cond_24
    move v5, v4

    :goto_15
    move-object/from16 v0, p0

    iget-object v10, v0, Lpyq;->J:[B

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lpyq;->I:[B

    move-object/from16 v19, v0

    add-int/lit8 v5, v5, -0x1

    aget-byte v19, v19, v5

    aput-byte v19, v10, v3

    if-gtz v5, :cond_2a

    move v3, v4

    move v10, v12

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v13

    goto/16 :goto_d

    :cond_25
    :goto_16
    if-ge v3, v14, :cond_19

    move-object/from16 v0, p0

    iget-object v5, v0, Lpyq;->J:[B

    add-int/lit8 v4, v3, 0x1

    const/4 v6, 0x0

    aput-byte v6, v5, v3

    move v3, v4

    goto :goto_16

    :catch_0
    move-exception v3

    const-string v4, "GifDrawable"

    const-string v5, "GifDrawable.backupFrame threw an OOME"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_f

    :pswitch_3
    const/4 v4, 0x4

    goto/16 :goto_11

    :pswitch_4
    const/4 v4, 0x2

    const/4 v5, 0x4

    goto/16 :goto_11

    :pswitch_5
    const/4 v4, 0x1

    const/4 v5, 0x2

    goto/16 :goto_11

    :cond_26
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_27
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lpyq;->E:Z

    if-eqz v3, :cond_28

    move-object/from16 v0, p0

    iget-object v3, v0, Lpyq;->v:[I

    move-object/from16 v0, p0

    iget v4, v0, Lpyq;->F:I

    aput v2, v3, v4

    :cond_28
    move-object/from16 v0, p0

    iget v2, v0, Lpyq;->M:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lpyq;->M:I

    goto/16 :goto_0

    :sswitch_7
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lpyq;->X:Z

    goto/16 :goto_0

    :cond_29
    move v7, v3

    goto/16 :goto_12

    :cond_2a
    move v3, v4

    goto :goto_15

    :cond_2b
    move v4, v10

    move v5, v6

    goto/16 :goto_14

    :cond_2c
    move v5, v4

    move v4, v12

    goto/16 :goto_14

    :cond_2d
    move v2, v3

    goto/16 :goto_7

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x21 -> :sswitch_1
        0x2c -> :sswitch_6
        0x3b -> :sswitch_7
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_5
        0xf9 -> :sswitch_2
        0xfe -> :sswitch_4
        0xff -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic a(Lpyq;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lpyq;->X:Z

    return p1
.end method

.method static synthetic b(Lpyq;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lpyq;->X:Z

    return v0
.end method

.method static synthetic b(Lpyq;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lpyq;->o:Z

    return p1
.end method

.method static synthetic c(Lpyq;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lpyq;->M:I

    return v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic c(Lpyq;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lpyq;->p:Z

    return p1
.end method

.method static synthetic d(Lpyq;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lpyq;->Y:I

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 366
    iget-object v0, p0, Lpyq;->e:Lpys;

    iget v0, v0, Lpys;->a:I

    iput v0, p0, Lpyq;->g:I

    .line 367
    iput-boolean v1, p0, Lpyq;->K:Z

    .line 368
    iput v1, p0, Lpyq;->M:I

    .line 369
    iput v1, p0, Lpyq;->D:I

    .line 370
    return-void
.end method

.method private e()I
    .locals 5

    .prologue
    .line 829
    iget-object v0, p0, Lpyq;->f:[B

    iget v1, p0, Lpyq;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lpyq;->g:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 830
    if-lez v0, :cond_0

    .line 831
    iget-object v1, p0, Lpyq;->f:[B

    iget v2, p0, Lpyq;->g:I

    iget-object v3, p0, Lpyq;->C:[B

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 832
    iget v1, p0, Lpyq;->g:I

    add-int/2addr v1, v0

    iput v1, p0, Lpyq;->g:I

    .line 834
    :cond_0
    return v0
.end method

.method static synthetic e(Lpyq;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lpyq;->Z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lpyq;->Z:I

    return v0
.end method

.method private f()I
    .locals 4

    .prologue
    .line 842
    iget-object v0, p0, Lpyq;->f:[B

    iget v1, p0, Lpyq;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lpyq;->g:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 843
    iget-object v1, p0, Lpyq;->f:[B

    iget v2, p0, Lpyq;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lpyq;->g:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 844
    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method static synthetic f(Lpyq;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lpyq;->d()V

    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 854
    :cond_0
    iget-object v0, p0, Lpyq;->f:[B

    iget v1, p0, Lpyq;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lpyq;->g:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 855
    iget v1, p0, Lpyq;->g:I

    add-int/2addr v1, v0

    iput v1, p0, Lpyq;->g:I

    .line 856
    if-gtz v0, :cond_0

    .line 857
    return-void
.end method

.method static synthetic g(Lpyq;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lpyq;->o:Z

    return v0
.end method

.method static synthetic h(Lpyq;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lpyq;->p:Z

    return v0
.end method

.method static synthetic i(Lpyq;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lpyq;->Q:I

    return v0
.end method

.method static synthetic j(Lpyq;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lpyq;->T:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 176
    iput p1, p0, Lpyq;->Y:I

    .line 177
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 190
    iput-boolean p1, p0, Lpyq;->q:Z

    .line 191
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lpyq;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lpyq;->W:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 376
    iget-boolean v0, p0, Lpyq;->p:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lpyq;->Y:I

    if-lez v0, :cond_0

    .line 377
    invoke-direct {p0}, Lpyq;->d()V

    .line 378
    iput-boolean v1, p0, Lpyq;->p:Z

    .line 379
    iput v1, p0, Lpyq;->Z:I

    .line 380
    invoke-virtual {p0}, Lpyq;->run()V

    .line 382
    :cond_0
    return-void
.end method

.method public b(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 882
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lpyq;->S:Z

    if-ne v0, p1, :cond_0

    .line 222
    :goto_0
    return-void

    .line 216
    :cond_0
    iput-boolean p1, p0, Lpyq;->S:Z

    .line 217
    iget-boolean v0, p0, Lpyq;->S:Z

    if-eqz v0, :cond_1

    .line 218
    invoke-virtual {p0}, Lpyq;->start()V

    goto :goto_0

    .line 220
    :cond_1
    invoke-virtual {p0}, Lpyq;->stop()V

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 256
    iget-boolean v0, p0, Lpyq;->o:Z

    if-nez v0, :cond_0

    iget v0, p0, Lpyq;->j:I

    if-eqz v0, :cond_0

    iget v0, p0, Lpyq;->k:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lpyq;->W:Z

    if-nez v0, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-boolean v0, p0, Lpyq;->m:Z

    if-eqz v0, :cond_2

    .line 261
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 262
    iget v0, p0, Lpyq;->n:F

    iget v1, p0, Lpyq;->n:F

    invoke-virtual {p1, v0, v1, v2, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 263
    iget-object v0, p0, Lpyq;->a:Landroid/graphics/Bitmap;

    sget-object v1, Lpyq;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 264
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 269
    :goto_1
    iget-boolean v0, p0, Lpyq;->O:Z

    if-eqz v0, :cond_3

    .line 270
    iget-boolean v0, p0, Lpyq;->R:Z

    if-nez v0, :cond_0

    .line 273
    iget-wide v0, p0, Lpyq;->N:J

    iget v2, p0, Lpyq;->P:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 275
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x5

    add-long/2addr v2, v4

    .line 273
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lpyq;->N:J

    .line 276
    iget-wide v0, p0, Lpyq;->N:J

    invoke-virtual {p0, p0, v0, v1}, Lpyq;->scheduleSelf(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 266
    :cond_2
    iget-object v0, p0, Lpyq;->a:Landroid/graphics/Bitmap;

    sget-object v1, Lpyq;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 278
    :cond_3
    iget-boolean v0, p0, Lpyq;->p:Z

    if-nez v0, :cond_4

    .line 279
    invoke-virtual {p0}, Lpyq;->start()V

    goto :goto_0

    .line 281
    :cond_4
    invoke-virtual {p0, p0}, Lpyq;->unscheduleSelf(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lpyq;->i:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lpyq;->h:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 861
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    .line 862
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lpyq;->P:I

    .line 863
    iget-object v0, p0, Lpyq;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 864
    iget-object v0, p0, Lpyq;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lpyq;->l:[I

    iget v3, p0, Lpyq;->h:I

    iget v6, p0, Lpyq;->h:I

    iget v7, p0, Lpyq;->i:I

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 866
    iget-object v0, p0, Lpyq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lpyq;->b(Landroid/graphics/Bitmap;)V

    .line 867
    iput-boolean v8, p0, Lpyq;->W:Z

    .line 868
    iput-boolean v2, p0, Lpyq;->R:Z

    .line 869
    invoke-virtual {p0}, Lpyq;->invalidateSelf()V

    :cond_0
    move v2, v8

    .line 874
    :cond_1
    return v2
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 310
    iget-boolean v0, p0, Lpyq;->O:Z

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 226
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 227
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lpyq;->j:I

    .line 228
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, Lpyq;->k:I

    .line 229
    iget v0, p0, Lpyq;->j:I

    iget v1, p0, Lpyq;->h:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Lpyq;->k:I

    iget v1, p0, Lpyq;->i:I

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lpyq;->m:Z

    .line 230
    iget-boolean v0, p0, Lpyq;->m:Z

    if-eqz v0, :cond_0

    .line 231
    iget v0, p0, Lpyq;->j:I

    int-to-float v0, v0

    iget v1, p0, Lpyq;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lpyq;->k:I

    int-to-float v1, v1

    iget v2, p0, Lpyq;->i:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lpyq;->n:F

    .line 235
    :cond_0
    iget-boolean v0, p0, Lpyq;->o:Z

    if-nez v0, :cond_1

    .line 237
    sget-object v0, Lpyq;->V:Landroid/os/Handler;

    sget-object v1, Lpyq;->V:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 239
    :cond_1
    return-void

    .line 229
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 351
    iget-boolean v0, p0, Lpyq;->p:Z

    if-nez v0, :cond_0

    .line 357
    sget-object v0, Lpyq;->V:Landroid/os/Handler;

    sget-object v1, Lpyq;->V:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 359
    :cond_0
    return-void
.end method

.method public scheduleSelf(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 334
    iget-boolean v0, p0, Lpyq;->S:Z

    if-eqz v0, :cond_0

    .line 335
    invoke-super {p0, p1, p2, p3}, Landroid/graphics/drawable/Drawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 336
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpyq;->R:Z

    .line 338
    :cond_0
    return-void
.end method

.method public setAlpha(I)V
    .locals 0

    .prologue
    .line 302
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 306
    return-void
.end method

.method public setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 243
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    .line 244
    if-eqz p1, :cond_2

    .line 245
    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    .line 246
    :cond_0
    invoke-virtual {p0}, Lpyq;->start()V

    .line 251
    :cond_1
    :goto_0
    return v0

    .line 249
    :cond_2
    invoke-virtual {p0}, Lpyq;->stop()V

    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 315
    invoke-virtual {p0}, Lpyq;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    iput-boolean v1, p0, Lpyq;->O:Z

    .line 317
    iget-boolean v0, p0, Lpyq;->q:Z

    if-nez v0, :cond_0

    .line 318
    iput-boolean v1, p0, Lpyq;->p:Z

    .line 320
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lpyq;->N:J

    .line 321
    invoke-virtual {p0}, Lpyq;->run()V

    .line 323
    :cond_1
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 327
    invoke-virtual {p0}, Lpyq;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p0, p0}, Lpyq;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 330
    :cond_0
    return-void
.end method

.method public unscheduleSelf(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 342
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 343
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpyq;->O:Z

    .line 344
    return-void
.end method
