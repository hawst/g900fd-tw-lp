.class public final Lmwj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field private l:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2536
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 2607
    const/4 v0, 0x0

    .line 2608
    iget-object v1, p0, Lmwj;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2609
    const/4 v0, 0x1

    iget-object v1, p0, Lmwj;->a:Ljava/lang/Integer;

    .line 2610
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2612
    :cond_0
    iget-object v1, p0, Lmwj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2613
    const/4 v1, 0x2

    iget-object v2, p0, Lmwj;->b:Ljava/lang/Integer;

    .line 2614
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2616
    :cond_1
    iget-object v1, p0, Lmwj;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2617
    const/4 v1, 0x3

    iget-object v2, p0, Lmwj;->c:Ljava/lang/Long;

    .line 2618
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2620
    :cond_2
    iget-object v1, p0, Lmwj;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2621
    const/4 v1, 0x4

    iget-object v2, p0, Lmwj;->d:Ljava/lang/Integer;

    .line 2622
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2624
    :cond_3
    iget-object v1, p0, Lmwj;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 2625
    const/4 v1, 0x5

    iget-object v2, p0, Lmwj;->e:Ljava/lang/Integer;

    .line 2626
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2628
    :cond_4
    iget-object v1, p0, Lmwj;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 2629
    const/4 v1, 0x6

    iget-object v2, p0, Lmwj;->f:Ljava/lang/Integer;

    .line 2630
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2632
    :cond_5
    iget-object v1, p0, Lmwj;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 2633
    const/4 v1, 0x7

    iget-object v2, p0, Lmwj;->g:Ljava/lang/Integer;

    .line 2634
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2636
    :cond_6
    iget-object v1, p0, Lmwj;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 2637
    const/16 v1, 0x8

    iget-object v2, p0, Lmwj;->h:Ljava/lang/Integer;

    .line 2638
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2640
    :cond_7
    iget-object v1, p0, Lmwj;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 2641
    const/16 v1, 0x9

    iget-object v2, p0, Lmwj;->i:Ljava/lang/Integer;

    .line 2642
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2644
    :cond_8
    iget-object v1, p0, Lmwj;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 2645
    const/16 v1, 0xa

    iget-object v2, p0, Lmwj;->j:Ljava/lang/Integer;

    .line 2646
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2648
    :cond_9
    iget-object v1, p0, Lmwj;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 2649
    const/16 v1, 0xb

    iget-object v2, p0, Lmwj;->k:Ljava/lang/Integer;

    .line 2650
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2652
    :cond_a
    iget-object v1, p0, Lmwj;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 2653
    const/16 v1, 0xc

    iget-object v2, p0, Lmwj;->l:Ljava/lang/Integer;

    .line 2654
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2656
    :cond_b
    iget-object v1, p0, Lmwj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2657
    iput v0, p0, Lmwj;->ai:I

    .line 2658
    return v0
.end method

.method public a(Loxn;)Lmwj;
    .locals 2

    .prologue
    .line 2666
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2667
    sparse-switch v0, :sswitch_data_0

    .line 2671
    iget-object v1, p0, Lmwj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2672
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwj;->ah:Ljava/util/List;

    .line 2675
    :cond_1
    iget-object v1, p0, Lmwj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2677
    :sswitch_0
    return-object p0

    .line 2682
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 2686
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2690
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwj;->c:Ljava/lang/Long;

    goto :goto_0

    .line 2694
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2698
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 2702
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 2706
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 2710
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 2714
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->i:Ljava/lang/Integer;

    goto :goto_0

    .line 2718
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2722
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2726
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwj;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2667
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 2565
    iget-object v0, p0, Lmwj;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2566
    const/4 v0, 0x1

    iget-object v1, p0, Lmwj;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2568
    :cond_0
    iget-object v0, p0, Lmwj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2569
    const/4 v0, 0x2

    iget-object v1, p0, Lmwj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2571
    :cond_1
    iget-object v0, p0, Lmwj;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2572
    const/4 v0, 0x3

    iget-object v1, p0, Lmwj;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2574
    :cond_2
    iget-object v0, p0, Lmwj;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2575
    const/4 v0, 0x4

    iget-object v1, p0, Lmwj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2577
    :cond_3
    iget-object v0, p0, Lmwj;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2578
    const/4 v0, 0x5

    iget-object v1, p0, Lmwj;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2580
    :cond_4
    iget-object v0, p0, Lmwj;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 2581
    const/4 v0, 0x6

    iget-object v1, p0, Lmwj;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2583
    :cond_5
    iget-object v0, p0, Lmwj;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 2584
    const/4 v0, 0x7

    iget-object v1, p0, Lmwj;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2586
    :cond_6
    iget-object v0, p0, Lmwj;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 2587
    const/16 v0, 0x8

    iget-object v1, p0, Lmwj;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2589
    :cond_7
    iget-object v0, p0, Lmwj;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 2590
    const/16 v0, 0x9

    iget-object v1, p0, Lmwj;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2592
    :cond_8
    iget-object v0, p0, Lmwj;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 2593
    const/16 v0, 0xa

    iget-object v1, p0, Lmwj;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2595
    :cond_9
    iget-object v0, p0, Lmwj;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 2596
    const/16 v0, 0xb

    iget-object v1, p0, Lmwj;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2598
    :cond_a
    iget-object v0, p0, Lmwj;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 2599
    const/16 v0, 0xc

    iget-object v1, p0, Lmwj;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2601
    :cond_b
    iget-object v0, p0, Lmwj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2603
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2532
    invoke-virtual {p0, p1}, Lmwj;->a(Loxn;)Lmwj;

    move-result-object v0

    return-object v0
.end method
