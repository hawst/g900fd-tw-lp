.class public final Lojc;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lohm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5368
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5371
    sget-object v0, Lohm;->a:[Lohm;

    iput-object v0, p0, Lojc;->a:[Lohm;

    .line 5368
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 5389
    .line 5390
    iget-object v1, p0, Lojc;->a:[Lohm;

    if-eqz v1, :cond_1

    .line 5391
    iget-object v2, p0, Lojc;->a:[Lohm;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 5392
    if-eqz v4, :cond_0

    .line 5393
    const/4 v5, 0x1

    .line 5394
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5391
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5398
    :cond_1
    iget-object v1, p0, Lojc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5399
    iput v0, p0, Lojc;->ai:I

    .line 5400
    return v0
.end method

.method public a(Loxn;)Lojc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5408
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5409
    sparse-switch v0, :sswitch_data_0

    .line 5413
    iget-object v2, p0, Lojc;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 5414
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lojc;->ah:Ljava/util/List;

    .line 5417
    :cond_1
    iget-object v2, p0, Lojc;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5419
    :sswitch_0
    return-object p0

    .line 5424
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5425
    iget-object v0, p0, Lojc;->a:[Lohm;

    if-nez v0, :cond_3

    move v0, v1

    .line 5426
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohm;

    .line 5427
    iget-object v3, p0, Lojc;->a:[Lohm;

    if-eqz v3, :cond_2

    .line 5428
    iget-object v3, p0, Lojc;->a:[Lohm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5430
    :cond_2
    iput-object v2, p0, Lojc;->a:[Lohm;

    .line 5431
    :goto_2
    iget-object v2, p0, Lojc;->a:[Lohm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 5432
    iget-object v2, p0, Lojc;->a:[Lohm;

    new-instance v3, Lohm;

    invoke-direct {v3}, Lohm;-><init>()V

    aput-object v3, v2, v0

    .line 5433
    iget-object v2, p0, Lojc;->a:[Lohm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 5434
    invoke-virtual {p1}, Loxn;->a()I

    .line 5431
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5425
    :cond_3
    iget-object v0, p0, Lojc;->a:[Lohm;

    array-length v0, v0

    goto :goto_1

    .line 5437
    :cond_4
    iget-object v2, p0, Lojc;->a:[Lohm;

    new-instance v3, Lohm;

    invoke-direct {v3}, Lohm;-><init>()V

    aput-object v3, v2, v0

    .line 5438
    iget-object v2, p0, Lojc;->a:[Lohm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5409
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 5376
    iget-object v0, p0, Lojc;->a:[Lohm;

    if-eqz v0, :cond_1

    .line 5377
    iget-object v1, p0, Lojc;->a:[Lohm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 5378
    if-eqz v3, :cond_0

    .line 5379
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 5377
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5383
    :cond_1
    iget-object v0, p0, Lojc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5385
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5364
    invoke-virtual {p0, p1}, Lojc;->a(Loxn;)Lojc;

    move-result-object v0

    return-object v0
.end method
