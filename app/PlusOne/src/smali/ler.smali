.class public final Ller;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/Integer;

.field c:Ljava/lang/Integer;

.field d:Ljava/lang/Long;

.field e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ller;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Ller;->e:Z

    .line 52
    return-object p0
.end method

.method public a(I)Ller;
    .locals 1

    .prologue
    .line 29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ller;->b:Ljava/lang/Integer;

    .line 30
    return-object p0
.end method

.method public a(J)Ller;
    .locals 3

    .prologue
    .line 44
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Llsk;->a(Z)V

    .line 45
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ller;->d:Ljava/lang/Long;

    .line 46
    return-object p0

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ller;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Ller;->a:Ljava/lang/String;

    .line 21
    return-object p0
.end method

.method public b(I)Ller;
    .locals 1

    .prologue
    .line 38
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ller;->c:Ljava/lang/Integer;

    .line 39
    return-object p0
.end method

.method public b()Llev;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    iget-object v0, p0, Ller;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Llsk;->b(Z)V

    .line 57
    iget-object v0, p0, Ller;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Llsk;->b(Z)V

    .line 58
    iget-object v0, p0, Ller;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    :goto_2
    invoke-static {v1}, Llsk;->b(Z)V

    .line 59
    new-instance v0, Lleq;

    invoke-direct {v0, p0}, Lleq;-><init>(Ller;)V

    return-object v0

    :cond_0
    move v0, v2

    .line 56
    goto :goto_0

    :cond_1
    move v0, v2

    .line 57
    goto :goto_1

    :cond_2
    move v1, v2

    .line 58
    goto :goto_2
.end method
