.class public final Lmom;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmmf;

.field private b:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 892
    invoke-direct {p0}, Loxq;-><init>()V

    .line 895
    const/4 v0, 0x0

    iput-object v0, p0, Lmom;->a:Lmmf;

    .line 892
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 914
    const/4 v0, 0x0

    .line 915
    iget-object v1, p0, Lmom;->a:Lmmf;

    if-eqz v1, :cond_0

    .line 916
    const/4 v0, 0x1

    iget-object v1, p0, Lmom;->a:Lmmf;

    .line 917
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 919
    :cond_0
    iget-object v1, p0, Lmom;->b:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 920
    const/4 v1, 0x2

    iget-object v2, p0, Lmom;->b:Ljava/lang/Double;

    .line 921
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 923
    :cond_1
    iget-object v1, p0, Lmom;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 924
    iput v0, p0, Lmom;->ai:I

    .line 925
    return v0
.end method

.method public a(Loxn;)Lmom;
    .locals 2

    .prologue
    .line 933
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 934
    sparse-switch v0, :sswitch_data_0

    .line 938
    iget-object v1, p0, Lmom;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 939
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmom;->ah:Ljava/util/List;

    .line 942
    :cond_1
    iget-object v1, p0, Lmom;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 944
    :sswitch_0
    return-object p0

    .line 949
    :sswitch_1
    iget-object v0, p0, Lmom;->a:Lmmf;

    if-nez v0, :cond_2

    .line 950
    new-instance v0, Lmmf;

    invoke-direct {v0}, Lmmf;-><init>()V

    iput-object v0, p0, Lmom;->a:Lmmf;

    .line 952
    :cond_2
    iget-object v0, p0, Lmom;->a:Lmmf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 956
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lmom;->b:Ljava/lang/Double;

    goto :goto_0

    .line 934
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 902
    iget-object v0, p0, Lmom;->a:Lmmf;

    if-eqz v0, :cond_0

    .line 903
    const/4 v0, 0x1

    iget-object v1, p0, Lmom;->a:Lmmf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 905
    :cond_0
    iget-object v0, p0, Lmom;->b:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 906
    const/4 v0, 0x2

    iget-object v1, p0, Lmom;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 908
    :cond_1
    iget-object v0, p0, Lmom;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 910
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 888
    invoke-virtual {p0, p1}, Lmom;->a(Loxn;)Lmom;

    move-result-object v0

    return-object v0
.end method
