.class public final Lflp;
.super Llol;
.source "PG"

# interfaces
.implements Lkgr;


# static fields
.field private static final O:Lnyr;


# instance fields
.field N:Lflu;

.field private P:Lnyq;

.field private Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private R:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private V:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

.field private W:Lflt;

.field private final X:Lhoc;

.field private Y:Lkgp;

.field private Z:Lkhr;

.field private aa:Lhee;

.field private ab:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lnyq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 64
    new-instance v0, Lnyr;

    invoke-direct {v0}, Lnyr;-><init>()V

    .line 68
    sput-object v0, Lflp;->O:Lnyr;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyr;->e:Ljava/lang/Boolean;

    .line 69
    sget-object v0, Lflp;->O:Lnyr;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyr;->f:Ljava/lang/Boolean;

    .line 70
    sget-object v0, Lflp;->O:Lnyr;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyr;->d:Ljava/lang/Boolean;

    .line 71
    sget-object v0, Lflp;->O:Lnyr;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyr;->b:Ljava/lang/Boolean;

    .line 72
    sget-object v0, Lflp;->O:Lnyr;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyr;->g:Ljava/lang/Boolean;

    .line 73
    sget-object v0, Lflp;->O:Lnyr;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyr;->a:Ljava/lang/Boolean;

    .line 74
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 119
    invoke-direct {p0}, Llol;-><init>()V

    .line 83
    new-instance v0, Lflt;

    invoke-direct {v0, p0}, Lflt;-><init>(Lflp;)V

    iput-object v0, p0, Lflp;->W:Lflt;

    .line 85
    new-instance v0, Lhoc;

    iget-object v1, p0, Lflp;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iget-object v1, p0, Lflp;->au:Llnh;

    .line 86
    invoke-virtual {v0, v1}, Lhoc;->a(Llnh;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lflp;->X:Lhoc;

    .line 87
    new-instance v0, Lkgp;

    iget-object v1, p0, Lflp;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkgp;-><init>(Lkgr;Llqr;)V

    iput-object v0, p0, Lflp;->Y:Lkgp;

    .line 91
    new-instance v0, Lflu;

    iget-object v1, p0, Lflp;->av:Llqm;

    invoke-direct {v0, v1}, Lflu;-><init>(Llqr;)V

    iput-object v0, p0, Lflp;->N:Lflu;

    .line 93
    new-instance v0, Lflq;

    iget-object v1, p0, Lflp;->Y:Lkgp;

    iget-object v2, p0, Lflp;->av:Llqm;

    invoke-direct {v0, p0, p0, v1, v2}, Lflq;-><init>(Lflp;Lkgr;Lkgp;Llqr;)V

    iput-object v0, p0, Lflp;->ab:Lbc;

    .line 119
    return-void
.end method

.method static synthetic U()Lnyr;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lflp;->O:Lnyr;

    return-object v0
.end method

.method static synthetic a(Lflp;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->at:Llnl;

    return-object v0
.end method

.method static synthetic a(Lflp;Lnyq;)Lnyq;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lflp;->P:Lnyq;

    return-object p1
.end method

.method static synthetic b(Lflp;)Lhee;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->aa:Lhee;

    return-object v0
.end method

.method static synthetic c(Lflp;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-object v0, p0, Lflp;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lflp;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v0, p0, Lflp;->P:Lnyq;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflp;->P:Lnyq;

    iget-object v0, v0, Lnyq;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflp;->P:Lnyq;

    iget-object v0, v0, Lnyq;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    :cond_0
    iget-object v0, p0, Lflp;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lflp;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v0, p0, Lflp;->P:Lnyq;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflp;->P:Lnyq;

    iget-object v0, v0, Lnyq;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflp;->P:Lnyq;

    iget-object v0, v0, Lnyq;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    :cond_1
    iget-object v0, p0, Lflp;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflp;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v2, p0, Lflp;->aa:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lflp;->at:Llnl;

    invoke-static {v3, v2}, Ldhv;->w(Landroid/content/Context;I)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    :cond_2
    iget-object v0, p0, Lflp;->R:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_3

    iget-object v2, p0, Lflp;->R:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v0, p0, Lflp;->P:Lnyq;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflp;->P:Lnyq;

    iget-object v0, v0, Lnyq;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflp;->P:Lnyq;

    iget-object v0, v0, Lnyq;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    :cond_3
    iget-object v0, p0, Lflp;->V:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflp;->P:Lnyq;

    iget-object v0, v0, Lnyq;->b:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v2, p0, Lflp;->V:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v0, p0, Lflp;->P:Lnyq;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflp;->P:Lnyq;

    iget-object v0, v0, Lnyq;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflp;->P:Lnyq;

    iget-object v0, v0, Lnyq;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_3
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    :cond_4
    :goto_4
    iget-object v0, p0, Lflp;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lflp;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v2, p0, Lflp;->P:Lnyq;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lflp;->P:Lnyq;

    iget-object v2, v2, Lnyq;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v1, p0, Lflp;->P:Lnyq;

    iget-object v1, v1, Lnyq;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :cond_5
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    :goto_5
    return-void

    :cond_6
    move v0, v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_2

    :cond_9
    move v0, v1

    goto :goto_3

    :cond_a
    iget-object v0, p0, Lflp;->Y:Lkgp;

    iget-object v2, p0, Lflp;->V:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v2}, Lkgp;->b(Lkhl;)V

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lflp;->P:Lnyq;

    const/4 v1, 0x0

    iput-object v1, v0, Lnyq;->f:Ljava/lang/Boolean;

    goto :goto_5
.end method

.method static synthetic d(Lflp;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Lflp;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->at:Llnl;

    return-object v0
.end method

.method static synthetic f(Lflp;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->at:Llnl;

    return-object v0
.end method

.method static synthetic g(Lflp;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->at:Llnl;

    return-object v0
.end method

.method static synthetic h(Lflp;)Lnyq;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->P:Lnyq;

    return-object v0
.end method

.method static synthetic i(Lflp;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->at:Llnl;

    return-object v0
.end method

.method static synthetic j(Lflp;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->at:Llnl;

    return-object v0
.end method

.method static synthetic k(Lflp;)Llnl;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->at:Llnl;

    return-object v0
.end method

.method static synthetic l(Lflp;)Lhoc;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflp;->X:Lhoc;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 129
    invoke-virtual {p0}, Lflp;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lflp;->ab:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 130
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 124
    iget-object v0, p0, Lflp;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lflp;->aa:Lhee;

    .line 125
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 134
    invoke-virtual {p0}, Lflp;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lflp;->ab:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 135
    return-void
.end method

.method public e()V
    .locals 7

    .prologue
    const v6, 0x7f0e0012

    const/4 v5, 0x0

    .line 139
    new-instance v0, Lkhr;

    iget-object v1, p0, Lflp;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflp;->Z:Lkhr;

    .line 141
    iget-object v0, p0, Lflp;->aa:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflp;->aa:Lhee;

    .line 142
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_dasher_account"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lflp;->Z:Lkhr;

    const v1, 0x7f0a06b8

    .line 144
    invoke-virtual {p0, v1}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a06bd

    .line 145
    invoke-virtual {p0, v2}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 143
    invoke-virtual {v0, v1, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Lflp;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 146
    iget-object v0, p0, Lflp;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v1, "google_drive"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lflp;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {p0}, Lflp;->o()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 149
    iget-object v0, p0, Lflp;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v1, p0, Lflp;->W:Lflt;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhq;)V

    .line 150
    iget-object v0, p0, Lflp;->Y:Lkgp;

    iget-object v1, p0, Lflp;->Q:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v1}, Lkgp;->a(Lkhl;)Lkhl;

    .line 153
    :cond_0
    iget-object v0, p0, Lflp;->Z:Lkhr;

    const v1, 0x7f0a06be

    .line 154
    invoke-virtual {p0, v1}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a06bf

    .line 155
    invoke-virtual {p0, v2}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 153
    invoke-virtual {v0, v1, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Lflp;->R:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 156
    iget-object v0, p0, Lflp;->R:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v1, "photo_location"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lflp;->R:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {p0}, Lflp;->o()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 159
    iget-object v0, p0, Lflp;->R:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v1, p0, Lflp;->W:Lflt;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhq;)V

    .line 160
    iget-object v0, p0, Lflp;->Y:Lkgp;

    iget-object v1, p0, Lflp;->R:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v1}, Lkgp;->a(Lkhl;)Lkhl;

    .line 162
    new-instance v1, Lkgz;

    iget-object v0, p0, Lflp;->at:Llnl;

    invoke-direct {v1, v0}, Lkgz;-><init>(Landroid/content/Context;)V

    .line 163
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lkgz;->c(Z)V

    .line 164
    iget-object v2, p0, Lflp;->at:Llnl;

    const-string v3, "6008918"

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "getPlusAnswerUrl(): answerId must be non-empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v0, "https://support.google.com/plus/answer/%answerid%?hl=%locale%"

    const-string v4, "%answerid%"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "%answerid%"

    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-static {v0}, Litk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v2, v0}, Litk;->a(Landroid/content/Context;Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 165
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lkgz;->a(Landroid/content/Intent;)V

    .line 166
    iget-object v0, p0, Lflp;->Y:Lkgp;

    invoke-virtual {v0, v1}, Lkgp;->a(Lkhl;)Lkhl;

    .line 168
    iget-object v0, p0, Lflp;->Z:Lkhr;

    const v1, 0x7f0a06b9

    .line 169
    invoke-virtual {p0, v1}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 168
    invoke-virtual {v0, v1, v5}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    .line 170
    const-string v1, "about_photo_location"

    invoke-virtual {v0, v1}, Lkhl;->d(Ljava/lang/String;)V

    .line 171
    new-instance v1, Lfls;

    invoke-direct {v1, p0}, Lfls;-><init>(Lflp;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 172
    iget-object v1, p0, Lflp;->Y:Lkgp;

    invoke-virtual {v1, v0}, Lkgp;->a(Lkhl;)Lkhl;

    .line 174
    iget-object v0, p0, Lflp;->Z:Lkhr;

    const v1, 0x7f0a06b4

    .line 175
    invoke-virtual {p0, v1}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a06b5

    .line 176
    invoke-virtual {p0, v2}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 174
    invoke-virtual {v0, v1, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    iput-object v0, p0, Lflp;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 177
    iget-object v0, p0, Lflp;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v1, "auto_enhance"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lflp;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {p0}, Lflp;->o()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 180
    iget-object v0, p0, Lflp;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v1, p0, Lflp;->W:Lflt;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhq;)V

    .line 181
    iget-object v0, p0, Lflp;->Y:Lkgp;

    iget-object v1, p0, Lflp;->S:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v1}, Lkgp;->a(Lkhl;)Lkhl;

    .line 183
    iget-object v0, p0, Lflp;->Z:Lkhr;

    const v1, 0x7f0a06b6

    .line 184
    invoke-virtual {p0, v1}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-virtual {v0, v1}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lflp;->Z:Lkhr;

    const v2, 0x7f0a06ba

    .line 187
    invoke-virtual {p0, v2}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 186
    invoke-virtual {v1, v5, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lflp;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 188
    iget-object v1, p0, Lflp;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v2, "auto_awesome"

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 189
    iget-object v1, p0, Lflp;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {p0}, Lflp;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 191
    iget-object v1, p0, Lflp;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v2, p0, Lflp;->W:Lflt;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhq;)V

    .line 192
    iget-object v1, p0, Lflp;->T:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 194
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v1

    invoke-virtual {v1}, Ljfb;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 195
    iget-object v1, p0, Lflp;->Z:Lkhr;

    const v2, 0x7f0a06bb

    .line 196
    invoke-virtual {p0, v2}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 195
    invoke-virtual {v1, v5, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lflp;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 197
    iget-object v1, p0, Lflp;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v2, "auto_awesome_movies"

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 198
    iget-object v1, p0, Lflp;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {p0}, Lflp;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 200
    iget-object v1, p0, Lflp;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v2, p0, Lflp;->W:Lflt;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhq;)V

    .line 201
    iget-object v1, p0, Lflp;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 202
    iget-object v0, p0, Lflp;->U:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v1, "auto_awesome"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->f(Ljava/lang/String;)V

    .line 205
    :cond_3
    iget-object v0, p0, Lflp;->Z:Lkhr;

    const v1, 0x7f0a06b7

    .line 206
    invoke-virtual {p0, v1}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 205
    invoke-virtual {v0, v1}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lflp;->Z:Lkhr;

    const v2, 0x7f0a06bc

    .line 209
    invoke-virtual {p0, v2}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 208
    invoke-virtual {v1, v5, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v1

    iput-object v1, p0, Lflp;->V:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    .line 210
    iget-object v1, p0, Lflp;->V:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    const-string v2, "find_my_face"

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 211
    iget-object v1, p0, Lflp;->V:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {p0}, Lflp;->o()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Ljava/lang/Object;)V

    .line 213
    iget-object v1, p0, Lflp;->V:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v2, p0, Lflp;->W:Lflt;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhq;)V

    .line 214
    iget-object v1, p0, Lflp;->V:Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 216
    iget-object v1, p0, Lflp;->Z:Lkhr;

    const v2, 0x7f0a06b0

    .line 217
    invoke-virtual {p0, v2}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 216
    invoke-virtual {v1, v2, v5}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v1

    .line 218
    const-string v2, "about_find_my_face"

    invoke-virtual {v1, v2}, Lkhl;->d(Ljava/lang/String;)V

    .line 219
    new-instance v2, Lfls;

    invoke-direct {v2, p0}, Lfls;-><init>(Lflp;)V

    invoke-virtual {v1, v2}, Lkhl;->a(Lkhq;)V

    .line 220
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 222
    iget-object v0, p0, Lflp;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 223
    iget-object v0, p0, Lflp;->Z:Lkhr;

    const v1, 0x7f0a0632

    invoke-virtual {p0, v1}, Lflp;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    .line 225
    new-instance v1, Lflr;

    invoke-direct {v1, p0}, Lflr;-><init>(Lflp;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 232
    iget-object v1, p0, Lflp;->Y:Lkgp;

    invoke-virtual {v1, v0}, Lkgp;->a(Lkhl;)Lkhl;

    .line 234
    :cond_4
    return-void
.end method
