.class public final Lise;
.super Lip;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:I

.field private c:Lisb;

.field private d:Liso;

.field private e:I

.field private f:Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lisb;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lip;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lise;->b:I

    .line 43
    const/4 v0, 0x2

    iput v0, p0, Lise;->e:I

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lise;->f:Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;

    .line 49
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lise;->a:Landroid/view/LayoutInflater;

    .line 50
    iput-object p2, p0, Lise;->c:Lisb;

    .line 51
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lise;->d:Liso;

    if-nez v0, :cond_0

    .line 71
    const/4 v0, -0x1

    .line 73
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lise;->d:Liso;

    iget v1, p0, Lise;->e:I

    invoke-virtual {v0, p1, v1}, Liso;->d(II)I

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 98
    iget-object v0, p0, Lise;->f:Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lise;->f:Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;

    .line 100
    const/4 v1, 0x0

    iput-object v1, p0, Lise;->f:Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;

    .line 104
    :goto_0
    iget-object v1, p0, Lise;->d:Liso;

    iget v2, p0, Lise;->e:I

    invoke-virtual {v1, p2, v2}, Liso;->b(II)Lisj;

    move-result-object v1

    .line 105
    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;->b()Lcom/google/android/libraries/social/ingest/ui/MtpImageView;

    move-result-object v2

    iget-object v3, p0, Lise;->d:Liso;

    invoke-virtual {v3}, Liso;->b()Landroid/mtp/MtpDevice;

    move-result-object v3

    iget v4, p0, Lise;->b:I

    invoke-virtual {v2, v3, v1, v4}, Lcom/google/android/libraries/social/ingest/ui/MtpImageView;->a(Landroid/mtp/MtpDevice;Lisj;I)V

    .line 106
    iget-object v1, p0, Lise;->c:Lisb;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;->a(ILisb;)V

    .line 107
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 108
    return-object v0

    .line 102
    :cond_0
    iget-object v0, p0, Lise;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f0400eb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lise;->f:Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;

    .line 79
    invoke-super {p0, p1}, Lip;->a(Landroid/view/ViewGroup;)V

    .line 80
    return-void
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 89
    check-cast p3, Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;

    .line 90
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 91
    iget-object v0, p0, Lise;->c:Lisb;

    invoke-virtual {v0, p3}, Lisb;->b(Lisc;)V

    .line 92
    iput-object p3, p0, Lise;->f:Lcom/google/android/libraries/social/ingest/ui/MtpFullscreenView;

    .line 93
    return-void
.end method

.method public a(Liso;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lise;->d:Liso;

    .line 55
    invoke-virtual {p0}, Lise;->d()V

    .line 56
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 84
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lise;->d:Liso;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lise;->d:Liso;

    invoke-virtual {v0}, Liso;->g()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lise;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lise;->b:I

    .line 66
    invoke-super {p0}, Lip;->d()V

    .line 67
    return-void
.end method
