.class final Lddc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Ldda;


# direct methods
.method constructor <init>(Ldda;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lddc;->a:Ldda;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    const-string v0, "account_id"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 80
    const-string v1, "view_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    const-string v2, "tile_id"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    new-instance v3, Lejo;

    iget-object v4, p0, Lddc;->a:Ldda;

    invoke-virtual {v4}, Ldda;->n()Lz;

    move-result-object v4

    invoke-direct {v3, v4, v0, v1, v2}, Lejo;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    :cond_0
    iget-object v0, p0, Lddc;->a:Ldda;

    invoke-virtual {v0}, Ldda;->U()V

    .line 99
    :goto_0
    return-void

    .line 92
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    new-instance v1, Lddw;

    invoke-direct {v1}, Lddw;-><init>()V

    .line 94
    iget-object v2, p0, Lddc;->a:Ldda;

    invoke-virtual {v2}, Ldda;->V()Lddl;

    move-result-object v2

    invoke-virtual {v1, v2}, Lddw;->a(Lddl;)Ldds;

    .line 95
    invoke-virtual {v1, v0}, Lddw;->h(Ljava/lang/String;)Lddw;

    .line 96
    iget-object v0, p0, Lddc;->a:Ldda;

    invoke-virtual {v1}, Lddw;->a()Lddl;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldda;->a(Lddl;)V

    .line 98
    iget-object v0, p0, Lddc;->a:Ldda;

    iget-object v1, p0, Lddc;->a:Ldda;

    invoke-virtual {v1}, Ldda;->V()Lddl;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldda;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 75
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lddc;->a(Landroid/database/Cursor;)V

    return-void
.end method
