.class public final Lmlf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmsa;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31264
    invoke-direct {p0}, Loxq;-><init>()V

    .line 31267
    iput-object v0, p0, Lmlf;->apiHeader:Llyr;

    .line 31270
    iput-object v0, p0, Lmlf;->a:Lmsa;

    .line 31264
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 31287
    const/4 v0, 0x0

    .line 31288
    iget-object v1, p0, Lmlf;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 31289
    const/4 v0, 0x1

    iget-object v1, p0, Lmlf;->apiHeader:Llyr;

    .line 31290
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 31292
    :cond_0
    iget-object v1, p0, Lmlf;->a:Lmsa;

    if-eqz v1, :cond_1

    .line 31293
    const/4 v1, 0x2

    iget-object v2, p0, Lmlf;->a:Lmsa;

    .line 31294
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31296
    :cond_1
    iget-object v1, p0, Lmlf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31297
    iput v0, p0, Lmlf;->ai:I

    .line 31298
    return v0
.end method

.method public a(Loxn;)Lmlf;
    .locals 2

    .prologue
    .line 31306
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 31307
    sparse-switch v0, :sswitch_data_0

    .line 31311
    iget-object v1, p0, Lmlf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 31312
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmlf;->ah:Ljava/util/List;

    .line 31315
    :cond_1
    iget-object v1, p0, Lmlf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 31317
    :sswitch_0
    return-object p0

    .line 31322
    :sswitch_1
    iget-object v0, p0, Lmlf;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 31323
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmlf;->apiHeader:Llyr;

    .line 31325
    :cond_2
    iget-object v0, p0, Lmlf;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 31329
    :sswitch_2
    iget-object v0, p0, Lmlf;->a:Lmsa;

    if-nez v0, :cond_3

    .line 31330
    new-instance v0, Lmsa;

    invoke-direct {v0}, Lmsa;-><init>()V

    iput-object v0, p0, Lmlf;->a:Lmsa;

    .line 31332
    :cond_3
    iget-object v0, p0, Lmlf;->a:Lmsa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 31307
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 31275
    iget-object v0, p0, Lmlf;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 31276
    const/4 v0, 0x1

    iget-object v1, p0, Lmlf;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31278
    :cond_0
    iget-object v0, p0, Lmlf;->a:Lmsa;

    if-eqz v0, :cond_1

    .line 31279
    const/4 v0, 0x2

    iget-object v1, p0, Lmlf;->a:Lmsa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31281
    :cond_1
    iget-object v0, p0, Lmlf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 31283
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 31260
    invoke-virtual {p0, p1}, Lmlf;->a(Loxn;)Lmlf;

    move-result-object v0

    return-object v0
.end method
