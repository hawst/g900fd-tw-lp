.class public final Lnyt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5475
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 5500
    const/4 v0, 0x1

    iget-object v1, p0, Lnyt;->a:Ljava/lang/Long;

    .line 5502
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5503
    const/4 v1, 0x2

    iget-object v2, p0, Lnyt;->b:Ljava/lang/Long;

    .line 5504
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5505
    const/4 v1, 0x3

    iget-object v2, p0, Lnyt;->c:Ljava/lang/Boolean;

    .line 5506
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5507
    iget-object v1, p0, Lnyt;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 5508
    const/4 v1, 0x4

    iget-object v2, p0, Lnyt;->d:Ljava/lang/Boolean;

    .line 5509
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5511
    :cond_0
    iget-object v1, p0, Lnyt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5512
    iput v0, p0, Lnyt;->ai:I

    .line 5513
    return v0
.end method

.method public a(Loxn;)Lnyt;
    .locals 2

    .prologue
    .line 5521
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5522
    sparse-switch v0, :sswitch_data_0

    .line 5526
    iget-object v1, p0, Lnyt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5527
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyt;->ah:Ljava/util/List;

    .line 5530
    :cond_1
    iget-object v1, p0, Lnyt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5532
    :sswitch_0
    return-object p0

    .line 5537
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnyt;->a:Ljava/lang/Long;

    goto :goto_0

    .line 5541
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnyt;->b:Ljava/lang/Long;

    goto :goto_0

    .line 5545
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyt;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 5549
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyt;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 5522
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 5488
    const/4 v0, 0x1

    iget-object v1, p0, Lnyt;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 5489
    const/4 v0, 0x2

    iget-object v1, p0, Lnyt;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 5490
    const/4 v0, 0x3

    iget-object v1, p0, Lnyt;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5491
    iget-object v0, p0, Lnyt;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 5492
    const/4 v0, 0x4

    iget-object v1, p0, Lnyt;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5494
    :cond_0
    iget-object v0, p0, Lnyt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5496
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5471
    invoke-virtual {p0, p1}, Lnyt;->a(Loxn;)Lnyt;

    move-result-object v0

    return-object v0
.end method
