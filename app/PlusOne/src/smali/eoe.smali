.class final Leoe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final a:Z

.field private synthetic b:Lenx;


# direct methods
.method public constructor <init>(Lenx;Z)V
    .locals 0

    .prologue
    .line 1078
    iput-object p1, p0, Leoe;->b:Lenx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1079
    iput-boolean p2, p0, Leoe;->a:Z

    .line 1080
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1084
    if-eqz p2, :cond_1

    .line 1085
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1086
    iget-object v1, p0, Leoe;->b:Lenx;

    invoke-virtual {v1}, Lenx;->ah()I

    move-result v4

    move v2, v3

    .line 1087
    :goto_0
    if-ge v2, v4, :cond_1

    .line 1088
    iget-object v1, p0, Leoe;->b:Lenx;

    iget-object v1, v1, Lenx;->ai:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1089
    if-eq v5, v0, :cond_0

    .line 1090
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leod;

    .line 1093
    iget v1, v1, Leod;->e:I

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 1094
    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1087
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1098
    :cond_1
    iget-boolean v0, p0, Leoe;->a:Z

    if-ne v0, p2, :cond_2

    .line 1099
    iget-object v0, p0, Leoe;->b:Lenx;

    invoke-virtual {v0, p1}, Lenx;->c(Landroid/view/View;)V

    .line 1103
    :goto_1
    return-void

    .line 1101
    :cond_2
    iget-object v0, p0, Leoe;->b:Lenx;

    invoke-virtual {v0, p1}, Lenx;->d(Landroid/view/View;)V

    goto :goto_1
.end method
