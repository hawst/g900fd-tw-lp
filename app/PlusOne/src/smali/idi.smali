.class public final Lidi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/text/DateFormat;

.field private static e:Ljava/text/DateFormat;

.field private static f:Ljava/text/DateFormat;

.field private static g:J

.field private static h:J

.field private static i:J

.field private static j:Ljava/lang/String;

.field private static k:Ljava/lang/String;

.field private static l:Ljava/lang/String;

.field private static m:Ljava/lang/String;

.field private static n:Ljava/lang/String;


# direct methods
.method public static a(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 223
    invoke-static {p0}, Lidi;->b(Landroid/content/Context;)V

    .line 225
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 227
    sget-object v1, Lidi;->e:Ljava/text/DateFormat;

    monitor-enter v1

    .line 228
    :try_start_0
    sget-object v2, Lidi;->e:Ljava/text/DateFormat;

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;ZLjava/util/TimeZone;Z)Ljava/lang/String;
    .locals 9

    .prologue
    .line 152
    const/4 v0, 0x0

    .line 153
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 155
    invoke-static {p0}, Lidi;->b(Landroid/content/Context;)V

    .line 156
    invoke-static {p0}, Lidi;->a(Landroid/content/Context;)V

    .line 158
    sget-wide v4, Lidi;->g:J

    cmp-long v1, p1, v4

    if-lez v1, :cond_1

    sget-wide v4, Lidi;->g:J

    const-wide/32 v6, 0x5265c00

    add-long/2addr v4, v6

    cmp-long v1, p1, v4

    if-gez v1, :cond_1

    .line 159
    sget-object v0, Lidi;->a:Ljava/lang/String;

    :goto_0
    move-object v1, v0

    .line 177
    :goto_1
    if-nez p5, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    invoke-static {p3}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object p5

    .line 180
    invoke-virtual {p5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Lidk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    const/4 p5, 0x0

    .line 185
    :cond_0
    if-eqz v1, :cond_7

    .line 186
    if-eqz p4, :cond_6

    sget-object v0, Lidi;->k:Ljava/lang/String;

    :goto_2
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    sget-object v4, Lidi;->f:Ljava/text/DateFormat;

    .line 188
    invoke-static {v4, v2, p5}, Lidi;->a(Ljava/text/DateFormat;Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    .line 186
    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 197
    :goto_3
    return-object v0

    .line 160
    :cond_1
    sget-wide v4, Lidi;->h:J

    cmp-long v1, p1, v4

    if-lez v1, :cond_2

    sget-wide v4, Lidi;->h:J

    const-wide/32 v6, 0x5265c00

    add-long/2addr v4, v6

    cmp-long v1, p1, v4

    if-gez v1, :cond_2

    .line 161
    sget-object v0, Lidi;->b:Ljava/lang/String;

    goto :goto_0

    .line 162
    :cond_2
    sget-wide v4, Lidi;->i:J

    cmp-long v1, p1, v4

    if-lez v1, :cond_3

    sget-wide v4, Lidi;->i:J

    const-wide/32 v6, 0x5265c00

    add-long/2addr v4, v6

    cmp-long v1, p1, v4

    if-gez v1, :cond_3

    .line 163
    sget-object v0, Lidi;->c:Ljava/lang/String;

    goto :goto_0

    .line 164
    :cond_3
    if-eqz p6, :cond_9

    .line 165
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 166
    if-eqz p3, :cond_4

    .line 167
    invoke-static {p3}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 169
    :cond_4
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 170
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-ge v1, v3, :cond_5

    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v1, 0x7

    const/4 v3, 0x2

    .line 171
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 186
    :cond_6
    sget-object v0, Lidi;->j:Ljava/lang/String;

    goto :goto_2

    .line 190
    :cond_7
    sget-object v0, Lidi;->n:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lidi;->d:Ljava/text/DateFormat;

    .line 191
    invoke-static {v4, v2, p5}, Lidi;->a(Ljava/text/DateFormat;Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    sget-object v4, Lidi;->f:Ljava/text/DateFormat;

    .line 192
    invoke-static {v4, v2, p5}, Lidi;->a(Ljava/text/DateFormat;Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    .line 190
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 193
    if-eqz p4, :cond_8

    sget-object v0, Lidi;->l:Ljava/lang/String;

    :goto_4
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_8
    sget-object v0, Lidi;->m:Ljava/lang/String;

    goto :goto_4

    :cond_9
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 234
    invoke-static {p0}, Lidi;->b(Landroid/content/Context;)V

    .line 236
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 238
    sget-object v1, Lidi;->e:Ljava/text/DateFormat;

    monitor-enter v1

    .line 239
    :try_start_0
    sget-object v2, Lidi;->e:Ljava/text/DateFormat;

    invoke-virtual {v2}, Ljava/text/DateFormat;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    .line 240
    sget-object v3, Lidi;->e:Ljava/text/DateFormat;

    invoke-virtual {v3, p3}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 241
    sget-object v3, Lidi;->e:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 242
    sget-object v3, Lidi;->e:Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 243
    monitor-exit v1

    return-object v0

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Loyy;ZLjava/util/TimeZone;Z)Ljava/lang/String;
    .locals 8

    .prologue
    .line 134
    iget-object v0, p1, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p1, Loyy;->c:Ljava/lang/String;

    move-object v1, p0

    move v5, p2

    move-object v6, p3

    move v7, p4

    invoke-static/range {v1 .. v7}, Lidi;->a(Landroid/content/Context;JLjava/lang/String;ZLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/text/DateFormat;Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 203
    invoke-virtual {p0}, Ljava/text/DateFormat;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    .line 205
    if-eqz p2, :cond_0

    .line 206
    invoke-virtual {p0, p2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 209
    :cond_0
    invoke-virtual {p0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 210
    invoke-virtual {p0, v0}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 211
    return-object v1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 10

    .prologue
    const-wide/32 v8, 0x5265c00

    const/4 v6, 0x0

    .line 339
    sget-object v0, Lidi;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 340
    const v0, 0x7f0a0447

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidi;->a:Ljava/lang/String;

    .line 341
    const v0, 0x7f0a0448

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidi;->b:Ljava/lang/String;

    .line 342
    const v0, 0x7f0a0449

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidi;->c:Ljava/lang/String;

    .line 343
    const v0, 0x7f0a044b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidi;->j:Ljava/lang/String;

    .line 344
    const v0, 0x7f0a044c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidi;->k:Ljava/lang/String;

    .line 345
    const v0, 0x7f0a044a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidi;->n:Ljava/lang/String;

    .line 346
    const v0, 0x7f0a044d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidi;->l:Ljava/lang/String;

    .line 347
    const v0, 0x7f0a0450

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lidi;->m:Ljava/lang/String;

    .line 351
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 352
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sget-wide v4, Lidi;->h:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 353
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 354
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 355
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 356
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 358
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 359
    sput-wide v0, Lidi;->g:J

    add-long/2addr v0, v8

    sput-wide v0, Lidi;->h:J

    .line 360
    sget-wide v0, Lidi;->g:J

    sub-long/2addr v0, v8

    sput-wide v0, Lidi;->i:J

    .line 362
    :cond_1
    return-void
.end method

.method public static a(JLjava/util/TimeZone;JLjava/util/TimeZone;Z)Z
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v0, 0x0

    .line 380
    sub-long v2, p3, p0

    .line 382
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_4

    const-wide/32 v4, 0x240c8400

    cmp-long v1, v2, v4

    if-gez v1, :cond_4

    .line 383
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 384
    if-eqz p2, :cond_0

    .line 385
    invoke-virtual {v1, p2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 387
    :cond_0
    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 389
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 390
    if-eqz p5, :cond_1

    .line 391
    invoke-virtual {v4, p5}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 393
    :cond_1
    invoke-virtual {v4, p3, p4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 395
    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 396
    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 398
    if-lt v1, v4, :cond_3

    if-nez p6, :cond_2

    if-ne v1, v4, :cond_3

    :cond_2
    const-wide/32 v4, 0x5265c00

    cmp-long v1, v2, v4

    if-gez v1, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 403
    :cond_4
    return v0
.end method

.method public static b(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 286
    invoke-static {p0}, Lidi;->b(Landroid/content/Context;)V

    .line 288
    sget-object v1, Lidi;->f:Ljava/text/DateFormat;

    monitor-enter v1

    .line 289
    :try_start_0
    sget-object v0, Lidi;->f:Ljava/text/DateFormat;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 294
    invoke-static {p0}, Lidi;->b(Landroid/content/Context;)V

    .line 296
    sget-object v1, Lidi;->f:Ljava/text/DateFormat;

    monitor-enter v1

    .line 297
    :try_start_0
    sget-object v0, Lidi;->f:Ljava/text/DateFormat;

    invoke-virtual {v0}, Ljava/text/DateFormat;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    .line 298
    sget-object v2, Lidi;->f:Ljava/text/DateFormat;

    invoke-virtual {v2, p3}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 299
    sget-object v2, Lidi;->f:Ljava/text/DateFormat;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 300
    sget-object v3, Lidi;->f:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 301
    monitor-exit v1

    return-object v2

    .line 302
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 327
    sget-object v0, Lidi;->d:Ljava/text/DateFormat;

    if-nez v0, :cond_0

    .line 328
    invoke-static {p0}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lidi;->d:Ljava/text/DateFormat;

    .line 329
    invoke-static {p0}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lidi;->e:Ljava/text/DateFormat;

    .line 330
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lidi;->f:Ljava/text/DateFormat;

    .line 331
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 332
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MMM"

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 333
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "dd"

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 335
    :cond_0
    return-void
.end method
