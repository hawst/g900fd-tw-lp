.class public final Lqap;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:[Ljava/lang/Integer;

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 241
    invoke-direct {p0}, Loxq;-><init>()V

    .line 248
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lqap;->c:[Ljava/lang/Integer;

    .line 251
    const/high16 v0, -0x80000000

    iput v0, p0, Lqap;->d:I

    .line 241
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 276
    .line 277
    iget-object v0, p0, Lqap;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 278
    const/4 v0, 0x1

    iget-object v2, p0, Lqap;->a:Ljava/lang/Integer;

    .line 279
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 281
    :goto_0
    iget-object v2, p0, Lqap;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 282
    const/4 v2, 0x2

    iget-object v3, p0, Lqap;->b:Ljava/lang/Integer;

    .line 283
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 285
    :cond_0
    iget-object v2, p0, Lqap;->c:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lqap;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 287
    iget-object v3, p0, Lqap;->c:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 289
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 287
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 291
    :cond_1
    add-int/2addr v0, v2

    .line 292
    iget-object v1, p0, Lqap;->c:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 294
    :cond_2
    iget v1, p0, Lqap;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 295
    const/4 v1, 0x4

    iget v2, p0, Lqap;->d:I

    .line 296
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_3
    iget-object v1, p0, Lqap;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 299
    iput v0, p0, Lqap;->ai:I

    .line 300
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lqap;
    .locals 5

    .prologue
    const/16 v4, 0x18

    const/4 v3, 0x0

    .line 308
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 309
    sparse-switch v0, :sswitch_data_0

    .line 313
    iget-object v1, p0, Lqap;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 314
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqap;->ah:Ljava/util/List;

    .line 317
    :cond_1
    iget-object v1, p0, Lqap;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 319
    :sswitch_0
    return-object p0

    .line 324
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqap;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 328
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqap;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 332
    :sswitch_3
    invoke-static {p1, v4}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 333
    iget-object v0, p0, Lqap;->c:[Ljava/lang/Integer;

    array-length v0, v0

    .line 334
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 335
    iget-object v2, p0, Lqap;->c:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 336
    iput-object v1, p0, Lqap;->c:[Ljava/lang/Integer;

    .line 337
    :goto_1
    iget-object v1, p0, Lqap;->c:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 338
    iget-object v1, p0, Lqap;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 339
    invoke-virtual {p1}, Loxn;->a()I

    .line 337
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 342
    :cond_2
    iget-object v1, p0, Lqap;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 346
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 347
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x10

    if-eq v0, v1, :cond_3

    const/16 v1, 0x11

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13

    if-eq v0, v1, :cond_3

    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    const/16 v1, 0x15

    if-eq v0, v1, :cond_3

    const/16 v1, 0x16

    if-eq v0, v1, :cond_3

    const/16 v1, 0x17

    if-eq v0, v1, :cond_3

    if-eq v0, v4, :cond_3

    const/16 v1, 0x19

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_3

    const/16 v1, 0x20

    if-eq v0, v1, :cond_3

    const/16 v1, 0x21

    if-ne v0, v1, :cond_4

    .line 381
    :cond_3
    iput v0, p0, Lqap;->d:I

    goto/16 :goto_0

    .line 383
    :cond_4
    iput v3, p0, Lqap;->d:I

    goto/16 :goto_0

    .line 309
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 256
    iget-object v0, p0, Lqap;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 257
    const/4 v0, 0x1

    iget-object v1, p0, Lqap;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 259
    :cond_0
    iget-object v0, p0, Lqap;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 260
    const/4 v0, 0x2

    iget-object v1, p0, Lqap;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 262
    :cond_1
    iget-object v0, p0, Lqap;->c:[Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 263
    iget-object v1, p0, Lqap;->c:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 264
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 263
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 267
    :cond_2
    iget v0, p0, Lqap;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 268
    const/4 v0, 0x4

    iget v1, p0, Lqap;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 270
    :cond_3
    iget-object v0, p0, Lqap;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 272
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0, p1}, Lqap;->a(Loxn;)Lqap;

    move-result-object v0

    return-object v0
.end method
