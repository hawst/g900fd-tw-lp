.class public final Lpjh;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lpjg;

.field private c:Lpjg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Loxq;-><init>()V

    .line 92
    iput-object v0, p0, Lpjh;->b:Lpjg;

    .line 95
    iput-object v0, p0, Lpjh;->c:Lpjg;

    .line 87
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    iget-object v1, p0, Lpjh;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 117
    const/4 v0, 0x1

    iget-object v1, p0, Lpjh;->a:Ljava/lang/String;

    .line 118
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 120
    :cond_0
    iget-object v1, p0, Lpjh;->b:Lpjg;

    if-eqz v1, :cond_1

    .line 121
    const/4 v1, 0x2

    iget-object v2, p0, Lpjh;->b:Lpjg;

    .line 122
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_1
    iget-object v1, p0, Lpjh;->c:Lpjg;

    if-eqz v1, :cond_2

    .line 125
    const/4 v1, 0x3

    iget-object v2, p0, Lpjh;->c:Lpjg;

    .line 126
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_2
    iget-object v1, p0, Lpjh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    iput v0, p0, Lpjh;->ai:I

    .line 130
    return v0
.end method

.method public a(Loxn;)Lpjh;
    .locals 2

    .prologue
    .line 138
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 139
    sparse-switch v0, :sswitch_data_0

    .line 143
    iget-object v1, p0, Lpjh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 144
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjh;->ah:Ljava/util/List;

    .line 147
    :cond_1
    iget-object v1, p0, Lpjh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    :sswitch_0
    return-object p0

    .line 154
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjh;->a:Ljava/lang/String;

    goto :goto_0

    .line 158
    :sswitch_2
    iget-object v0, p0, Lpjh;->b:Lpjg;

    if-nez v0, :cond_2

    .line 159
    new-instance v0, Lpjg;

    invoke-direct {v0}, Lpjg;-><init>()V

    iput-object v0, p0, Lpjh;->b:Lpjg;

    .line 161
    :cond_2
    iget-object v0, p0, Lpjh;->b:Lpjg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 165
    :sswitch_3
    iget-object v0, p0, Lpjh;->c:Lpjg;

    if-nez v0, :cond_3

    .line 166
    new-instance v0, Lpjg;

    invoke-direct {v0}, Lpjg;-><init>()V

    iput-object v0, p0, Lpjh;->c:Lpjg;

    .line 168
    :cond_3
    iget-object v0, p0, Lpjh;->c:Lpjg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 139
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lpjh;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x1

    iget-object v1, p0, Lpjh;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lpjh;->b:Lpjg;

    if-eqz v0, :cond_1

    .line 104
    const/4 v0, 0x2

    iget-object v1, p0, Lpjh;->b:Lpjg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 106
    :cond_1
    iget-object v0, p0, Lpjh;->c:Lpjg;

    if-eqz v0, :cond_2

    .line 107
    const/4 v0, 0x3

    iget-object v1, p0, Lpjh;->c:Lpjg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 109
    :cond_2
    iget-object v0, p0, Lpjh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 111
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0, p1}, Lpjh;->a(Loxn;)Lpjh;

    move-result-object v0

    return-object v0
.end method
