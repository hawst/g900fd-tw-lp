.class public final Lnnj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnnj;


# instance fields
.field public b:Ljava/lang/String;

.field public c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1639
    const/4 v0, 0x0

    new-array v0, v0, [Lnnj;

    sput-object v0, Lnnj;->a:[Lnnj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1640
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1651
    const/high16 v0, -0x80000000

    iput v0, p0, Lnnj;->c:I

    .line 1640
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1668
    const/4 v0, 0x0

    .line 1669
    iget-object v1, p0, Lnnj;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1670
    const/4 v0, 0x1

    iget-object v1, p0, Lnnj;->b:Ljava/lang/String;

    .line 1671
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1673
    :cond_0
    iget v1, p0, Lnnj;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 1674
    const/4 v1, 0x2

    iget v2, p0, Lnnj;->c:I

    .line 1675
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1677
    :cond_1
    iget-object v1, p0, Lnnj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1678
    iput v0, p0, Lnnj;->ai:I

    .line 1679
    return v0
.end method

.method public a(Loxn;)Lnnj;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1687
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1688
    sparse-switch v0, :sswitch_data_0

    .line 1692
    iget-object v1, p0, Lnnj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1693
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnnj;->ah:Ljava/util/List;

    .line 1696
    :cond_1
    iget-object v1, p0, Lnnj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1698
    :sswitch_0
    return-object p0

    .line 1703
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnnj;->b:Ljava/lang/String;

    goto :goto_0

    .line 1707
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1708
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 1711
    :cond_2
    iput v0, p0, Lnnj;->c:I

    goto :goto_0

    .line 1713
    :cond_3
    iput v2, p0, Lnnj;->c:I

    goto :goto_0

    .line 1688
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1656
    iget-object v0, p0, Lnnj;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1657
    const/4 v0, 0x1

    iget-object v1, p0, Lnnj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1659
    :cond_0
    iget v0, p0, Lnnj;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 1660
    const/4 v0, 0x2

    iget v1, p0, Lnnj;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1662
    :cond_1
    iget-object v0, p0, Lnnj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1664
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1636
    invoke-virtual {p0, p1}, Lnnj;->a(Loxn;)Lnnj;

    move-result-object v0

    return-object v0
.end method
