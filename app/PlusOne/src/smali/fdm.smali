.class final Lfdm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field private synthetic a:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lfdm;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 439
    iget-object v0, p0, Lfdm;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 440
    iget-object v0, p0, Lfdm;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotationX(F)V

    .line 441
    iget-object v0, p0, Lfdm;->a:Landroid/view/View;

    instance-of v0, v0, Lldq;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lfdm;->a:Landroid/view/View;

    check-cast v0, Lldq;

    .line 443
    iget-boolean v1, v0, Lhvs;->v:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lhvs;->v:Z

    iget-boolean v1, v0, Lhvs;->u:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lhvs;->z()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhvs;->a(Z)V

    .line 445
    :cond_0
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 455
    invoke-direct {p0}, Lfdm;->a()V

    .line 456
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 449
    invoke-direct {p0}, Lfdm;->a()V

    .line 450
    iget-object v0, p0, Lfdm;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 451
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 460
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 436
    return-void
.end method
