.class public final Lmym;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmym;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;

.field private f:Lmxx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 417
    const/4 v0, 0x0

    new-array v0, v0, [Lmym;

    sput-object v0, Lmym;->a:[Lmym;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 418
    invoke-direct {p0}, Loxq;-><init>()V

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lmym;->f:Lmxx;

    .line 418
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 455
    const/4 v0, 0x0

    .line 456
    iget-object v1, p0, Lmym;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 457
    const/4 v0, 0x1

    iget-object v1, p0, Lmym;->b:Ljava/lang/String;

    .line 458
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 460
    :cond_0
    iget-object v1, p0, Lmym;->d:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 461
    const/4 v1, 0x2

    iget-object v2, p0, Lmym;->d:Ljava/lang/Long;

    .line 462
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 464
    :cond_1
    iget-object v1, p0, Lmym;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 465
    const/4 v1, 0x3

    iget-object v2, p0, Lmym;->c:Ljava/lang/Long;

    .line 466
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 468
    :cond_2
    iget-object v1, p0, Lmym;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 469
    const/4 v1, 0x4

    iget-object v2, p0, Lmym;->e:Ljava/lang/String;

    .line 470
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 472
    :cond_3
    iget-object v1, p0, Lmym;->f:Lmxx;

    if-eqz v1, :cond_4

    .line 473
    const/4 v1, 0x5

    iget-object v2, p0, Lmym;->f:Lmxx;

    .line 474
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 476
    :cond_4
    iget-object v1, p0, Lmym;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 477
    iput v0, p0, Lmym;->ai:I

    .line 478
    return v0
.end method

.method public a(Loxn;)Lmym;
    .locals 2

    .prologue
    .line 486
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 487
    sparse-switch v0, :sswitch_data_0

    .line 491
    iget-object v1, p0, Lmym;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 492
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmym;->ah:Ljava/util/List;

    .line 495
    :cond_1
    iget-object v1, p0, Lmym;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    :sswitch_0
    return-object p0

    .line 502
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmym;->b:Ljava/lang/String;

    goto :goto_0

    .line 506
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmym;->d:Ljava/lang/Long;

    goto :goto_0

    .line 510
    :sswitch_3
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmym;->c:Ljava/lang/Long;

    goto :goto_0

    .line 514
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmym;->e:Ljava/lang/String;

    goto :goto_0

    .line 518
    :sswitch_5
    iget-object v0, p0, Lmym;->f:Lmxx;

    if-nez v0, :cond_2

    .line 519
    new-instance v0, Lmxx;

    invoke-direct {v0}, Lmxx;-><init>()V

    iput-object v0, p0, Lmym;->f:Lmxx;

    .line 521
    :cond_2
    iget-object v0, p0, Lmym;->f:Lmxx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 487
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 434
    iget-object v0, p0, Lmym;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 435
    const/4 v0, 0x1

    iget-object v1, p0, Lmym;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 437
    :cond_0
    iget-object v0, p0, Lmym;->d:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 438
    const/4 v0, 0x2

    iget-object v1, p0, Lmym;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 440
    :cond_1
    iget-object v0, p0, Lmym;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 441
    const/4 v0, 0x3

    iget-object v1, p0, Lmym;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 443
    :cond_2
    iget-object v0, p0, Lmym;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 444
    const/4 v0, 0x4

    iget-object v1, p0, Lmym;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 446
    :cond_3
    iget-object v0, p0, Lmym;->f:Lmxx;

    if-eqz v0, :cond_4

    .line 447
    const/4 v0, 0x5

    iget-object v1, p0, Lmym;->f:Lmxx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 449
    :cond_4
    iget-object v0, p0, Lmym;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 451
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 414
    invoke-virtual {p0, p1}, Lmym;->a(Loxn;)Lmym;

    move-result-object v0

    return-object v0
.end method
