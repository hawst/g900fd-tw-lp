.class final Ltk;
.super Landroid/database/DataSetObserver;
.source "PG"


# instance fields
.field private a:Landroid/os/Parcelable;

.field private synthetic b:Ltj;


# direct methods
.method constructor <init>(Ltj;)V
    .locals 1

    .prologue
    .line 798
    iput-object p1, p0, Ltk;->b:Ltj;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 800
    const/4 v0, 0x0

    iput-object v0, p0, Ltk;->a:Landroid/os/Parcelable;

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 804
    iget-object v0, p0, Ltk;->b:Ltj;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ltj;->l:Z

    .line 805
    iget-object v0, p0, Ltk;->b:Ltj;

    iget-object v1, p0, Ltk;->b:Ltj;

    iget v1, v1, Ltj;->q:I

    iput v1, v0, Ltj;->r:I

    .line 806
    iget-object v0, p0, Ltk;->b:Ltj;

    iget-object v1, p0, Ltk;->b:Ltj;

    invoke-virtual {v1}, Ltj;->f()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    iput v1, v0, Ltj;->q:I

    .line 810
    iget-object v0, p0, Ltk;->b:Ltj;

    invoke-virtual {v0}, Ltj;->f()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltk;->a:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltk;->b:Ltj;

    iget v0, v0, Ltj;->r:I

    if-nez v0, :cond_0

    iget-object v0, p0, Ltk;->b:Ltj;

    iget v0, v0, Ltj;->q:I

    if-lez v0, :cond_0

    .line 812
    iget-object v0, p0, Ltk;->b:Ltj;

    iget-object v1, p0, Ltk;->a:Landroid/os/Parcelable;

    invoke-static {v0, v1}, Ltj;->a(Ltj;Landroid/os/Parcelable;)V

    .line 813
    const/4 v0, 0x0

    iput-object v0, p0, Ltk;->a:Landroid/os/Parcelable;

    .line 817
    :goto_0
    iget-object v0, p0, Ltk;->b:Ltj;

    invoke-virtual {v0}, Ltj;->j()V

    .line 818
    iget-object v0, p0, Ltk;->b:Ltj;

    invoke-virtual {v0}, Ltj;->requestLayout()V

    .line 819
    return-void

    .line 815
    :cond_0
    iget-object v0, p0, Ltk;->b:Ltj;

    invoke-virtual {v0}, Ltj;->o()V

    goto :goto_0
.end method

.method public onInvalidated()V
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 823
    iget-object v0, p0, Ltk;->b:Ltj;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ltj;->l:Z

    .line 825
    iget-object v0, p0, Ltk;->b:Ltj;

    invoke-virtual {v0}, Ltj;->f()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828
    iget-object v0, p0, Ltk;->b:Ltj;

    invoke-static {v0}, Ltj;->a(Ltj;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Ltk;->a:Landroid/os/Parcelable;

    .line 832
    :cond_0
    iget-object v0, p0, Ltk;->b:Ltj;

    iget-object v1, p0, Ltk;->b:Ltj;

    iget v1, v1, Ltj;->q:I

    iput v1, v0, Ltj;->r:I

    .line 833
    iget-object v0, p0, Ltk;->b:Ltj;

    iput v3, v0, Ltj;->q:I

    .line 834
    iget-object v0, p0, Ltk;->b:Ltj;

    iput v2, v0, Ltj;->o:I

    .line 835
    iget-object v0, p0, Ltk;->b:Ltj;

    iput-wide v4, v0, Ltj;->p:J

    .line 836
    iget-object v0, p0, Ltk;->b:Ltj;

    iput v2, v0, Ltj;->m:I

    .line 837
    iget-object v0, p0, Ltk;->b:Ltj;

    iput-wide v4, v0, Ltj;->n:J

    .line 838
    iget-object v0, p0, Ltk;->b:Ltj;

    iput-boolean v3, v0, Ltj;->i:Z

    .line 840
    iget-object v0, p0, Ltk;->b:Ltj;

    invoke-virtual {v0}, Ltj;->j()V

    .line 841
    iget-object v0, p0, Ltk;->b:Ltj;

    invoke-virtual {v0}, Ltj;->requestLayout()V

    .line 842
    return-void
.end method
