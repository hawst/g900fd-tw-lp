.class public final Lolk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lolk;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:[Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    new-array v0, v0, [Lolk;

    sput-object v0, Lolk;->a:[Lolk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 169
    invoke-direct {p0}, Loxq;-><init>()V

    .line 185
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lolk;->d:[Ljava/lang/Integer;

    .line 190
    const/high16 v0, -0x80000000

    iput v0, p0, Lolk;->f:I

    .line 169
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 216
    .line 217
    iget-object v0, p0, Lolk;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 218
    const/4 v0, 0x1

    iget-object v2, p0, Lolk;->b:Ljava/lang/Integer;

    .line 219
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 221
    :goto_0
    const/4 v2, 0x2

    iget-object v3, p0, Lolk;->c:Ljava/lang/Integer;

    .line 222
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 223
    iget-object v2, p0, Lolk;->d:[Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lolk;->d:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 225
    iget-object v3, p0, Lolk;->d:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 227
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->m(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 225
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 229
    :cond_0
    add-int/2addr v0, v2

    .line 230
    iget-object v1, p0, Lolk;->d:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 232
    :cond_1
    iget-object v1, p0, Lolk;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 233
    const/4 v1, 0x4

    iget-object v2, p0, Lolk;->e:Ljava/lang/Integer;

    .line 234
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_2
    iget v1, p0, Lolk;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 237
    const/4 v1, 0x5

    iget v2, p0, Lolk;->f:I

    .line 238
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    :cond_3
    iget-object v1, p0, Lolk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    iput v0, p0, Lolk;->ai:I

    .line 242
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lolk;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 251
    sparse-switch v0, :sswitch_data_0

    .line 255
    iget-object v1, p0, Lolk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 256
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lolk;->ah:Ljava/util/List;

    .line 259
    :cond_1
    iget-object v1, p0, Lolk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    :sswitch_0
    return-object p0

    .line 266
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lolk;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 270
    :sswitch_2
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lolk;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 274
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 275
    iget-object v0, p0, Lolk;->d:[Ljava/lang/Integer;

    array-length v0, v0

    .line 276
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 277
    iget-object v2, p0, Lolk;->d:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 278
    iput-object v1, p0, Lolk;->d:[Ljava/lang/Integer;

    .line 279
    :goto_1
    iget-object v1, p0, Lolk;->d:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 280
    iget-object v1, p0, Lolk;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 281
    invoke-virtual {p1}, Loxn;->a()I

    .line 279
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 284
    :cond_2
    iget-object v1, p0, Lolk;->d:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 288
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lolk;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 292
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 293
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 299
    :cond_3
    iput v0, p0, Lolk;->f:I

    goto/16 :goto_0

    .line 301
    :cond_4
    iput v3, p0, Lolk;->f:I

    goto/16 :goto_0

    .line 251
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 195
    iget-object v0, p0, Lolk;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x1

    iget-object v1, p0, Lolk;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 198
    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Lolk;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 199
    iget-object v0, p0, Lolk;->d:[Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 200
    iget-object v1, p0, Lolk;->d:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 201
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->c(II)V

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    :cond_1
    iget-object v0, p0, Lolk;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 205
    const/4 v0, 0x4

    iget-object v1, p0, Lolk;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 207
    :cond_2
    iget v0, p0, Lolk;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 208
    const/4 v0, 0x5

    iget v1, p0, Lolk;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 210
    :cond_3
    iget-object v0, p0, Lolk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 212
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Lolk;->a(Loxn;)Lolk;

    move-result-object v0

    return-object v0
.end method
