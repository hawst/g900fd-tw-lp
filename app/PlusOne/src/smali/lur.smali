.class public final Llur;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lluk;

.field public b:Llun;

.field public c:Llwa;

.field public d:Llva;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12
    iput-object v0, p0, Llur;->a:Lluk;

    .line 15
    iput-object v0, p0, Llur;->b:Llun;

    .line 18
    iput-object v0, p0, Llur;->c:Llwa;

    .line 23
    iput-object v0, p0, Llur;->d:Llva;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 50
    iget-object v1, p0, Llur;->a:Lluk;

    if-eqz v1, :cond_0

    .line 51
    const/4 v0, 0x1

    iget-object v1, p0, Llur;->a:Lluk;

    .line 52
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 54
    :cond_0
    iget-object v1, p0, Llur;->b:Llun;

    if-eqz v1, :cond_1

    .line 55
    const/4 v1, 0x2

    iget-object v2, p0, Llur;->b:Llun;

    .line 56
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_1
    iget-object v1, p0, Llur;->c:Llwa;

    if-eqz v1, :cond_2

    .line 59
    const/4 v1, 0x4

    iget-object v2, p0, Llur;->c:Llwa;

    .line 60
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_2
    iget-object v1, p0, Llur;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 63
    const/4 v1, 0x5

    iget-object v2, p0, Llur;->e:Ljava/lang/String;

    .line 64
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_3
    iget-object v1, p0, Llur;->d:Llva;

    if-eqz v1, :cond_4

    .line 67
    const/4 v1, 0x6

    iget-object v2, p0, Llur;->d:Llva;

    .line 68
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_4
    iget-object v1, p0, Llur;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    iput v0, p0, Llur;->ai:I

    .line 72
    return v0
.end method

.method public a(Loxn;)Llur;
    .locals 2

    .prologue
    .line 80
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 81
    sparse-switch v0, :sswitch_data_0

    .line 85
    iget-object v1, p0, Llur;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 86
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llur;->ah:Ljava/util/List;

    .line 89
    :cond_1
    iget-object v1, p0, Llur;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    :sswitch_0
    return-object p0

    .line 96
    :sswitch_1
    iget-object v0, p0, Llur;->a:Lluk;

    if-nez v0, :cond_2

    .line 97
    new-instance v0, Lluk;

    invoke-direct {v0}, Lluk;-><init>()V

    iput-object v0, p0, Llur;->a:Lluk;

    .line 99
    :cond_2
    iget-object v0, p0, Llur;->a:Lluk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 103
    :sswitch_2
    iget-object v0, p0, Llur;->b:Llun;

    if-nez v0, :cond_3

    .line 104
    new-instance v0, Llun;

    invoke-direct {v0}, Llun;-><init>()V

    iput-object v0, p0, Llur;->b:Llun;

    .line 106
    :cond_3
    iget-object v0, p0, Llur;->b:Llun;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 110
    :sswitch_3
    iget-object v0, p0, Llur;->c:Llwa;

    if-nez v0, :cond_4

    .line 111
    new-instance v0, Llwa;

    invoke-direct {v0}, Llwa;-><init>()V

    iput-object v0, p0, Llur;->c:Llwa;

    .line 113
    :cond_4
    iget-object v0, p0, Llur;->c:Llwa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 117
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llur;->e:Ljava/lang/String;

    goto :goto_0

    .line 121
    :sswitch_5
    iget-object v0, p0, Llur;->d:Llva;

    if-nez v0, :cond_5

    .line 122
    new-instance v0, Llva;

    invoke-direct {v0}, Llva;-><init>()V

    iput-object v0, p0, Llur;->d:Llva;

    .line 124
    :cond_5
    iget-object v0, p0, Llur;->d:Llva;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Llur;->a:Lluk;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v1, p0, Llur;->a:Lluk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_0
    iget-object v0, p0, Llur;->b:Llun;

    if-eqz v0, :cond_1

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Llur;->b:Llun;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 34
    :cond_1
    iget-object v0, p0, Llur;->c:Llwa;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x4

    iget-object v1, p0, Llur;->c:Llwa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_2
    iget-object v0, p0, Llur;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 38
    const/4 v0, 0x5

    iget-object v1, p0, Llur;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 40
    :cond_3
    iget-object v0, p0, Llur;->d:Llva;

    if-eqz v0, :cond_4

    .line 41
    const/4 v0, 0x6

    iget-object v1, p0, Llur;->d:Llva;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 43
    :cond_4
    iget-object v0, p0, Llur;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 45
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llur;->a(Loxn;)Llur;

    move-result-object v0

    return-object v0
.end method
