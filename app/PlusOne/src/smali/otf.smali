.class public final Lotf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lotf;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lotg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    new-array v0, v0, [Lotf;

    sput-object v0, Lotf;->a:[Lotf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 361
    invoke-direct {p0}, Loxq;-><init>()V

    .line 452
    const/4 v0, 0x0

    iput-object v0, p0, Lotf;->c:Lotg;

    .line 361
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 469
    const/4 v0, 0x0

    .line 470
    iget-object v1, p0, Lotf;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 471
    const/4 v0, 0x1

    iget-object v1, p0, Lotf;->b:Ljava/lang/String;

    .line 472
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 474
    :cond_0
    iget-object v1, p0, Lotf;->c:Lotg;

    if-eqz v1, :cond_1

    .line 475
    const/4 v1, 0x2

    iget-object v2, p0, Lotf;->c:Lotg;

    .line 476
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 478
    :cond_1
    iget-object v1, p0, Lotf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 479
    iput v0, p0, Lotf;->ai:I

    .line 480
    return v0
.end method

.method public a(Loxn;)Lotf;
    .locals 2

    .prologue
    .line 488
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 489
    sparse-switch v0, :sswitch_data_0

    .line 493
    iget-object v1, p0, Lotf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 494
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lotf;->ah:Ljava/util/List;

    .line 497
    :cond_1
    iget-object v1, p0, Lotf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 499
    :sswitch_0
    return-object p0

    .line 504
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lotf;->b:Ljava/lang/String;

    goto :goto_0

    .line 508
    :sswitch_2
    iget-object v0, p0, Lotf;->c:Lotg;

    if-nez v0, :cond_2

    .line 509
    new-instance v0, Lotg;

    invoke-direct {v0}, Lotg;-><init>()V

    iput-object v0, p0, Lotf;->c:Lotg;

    .line 511
    :cond_2
    iget-object v0, p0, Lotf;->c:Lotg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 489
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lotf;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 458
    const/4 v0, 0x1

    iget-object v1, p0, Lotf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 460
    :cond_0
    iget-object v0, p0, Lotf;->c:Lotg;

    if-eqz v0, :cond_1

    .line 461
    const/4 v0, 0x2

    iget-object v1, p0, Lotf;->c:Lotg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 463
    :cond_1
    iget-object v0, p0, Lotf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 465
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 357
    invoke-virtual {p0, p1}, Lotf;->a(Loxn;)Lotf;

    move-result-object v0

    return-object v0
.end method
