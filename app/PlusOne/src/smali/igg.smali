.class public abstract Ligg;
.super Lifv;
.source "PG"


# static fields
.field private static e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ligh;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Ligh;


# instance fields
.field private g:Z

.field private h:Z

.field private i:Landroid/graphics/Bitmap;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ligg;->e:Ljava/util/HashMap;

    .line 51
    new-instance v0, Ligh;

    invoke-direct {v0}, Ligh;-><init>()V

    sput-object v0, Ligg;->f:Ligh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ligg;-><init>(Z)V

    .line 69
    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2, v2}, Lifv;-><init>(Lifx;II)V

    .line 55
    iput-boolean v1, p0, Ligg;->g:Z

    .line 58
    iput-boolean v1, p0, Ligg;->h:Z

    .line 60
    if-eqz p1, :cond_0

    .line 74
    invoke-virtual {p0, v1}, Ligg;->a(Z)V

    .line 75
    iput v1, p0, Ligg;->j:I

    .line 77
    :cond_0
    return-void
.end method

.method private static a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 124
    sget-object v1, Ligg;->f:Ligh;

    .line 125
    iput-boolean p0, v1, Ligh;->a:Z

    .line 126
    iput-object p1, v1, Ligh;->b:Landroid/graphics/Bitmap$Config;

    .line 127
    iput p2, v1, Ligh;->c:I

    .line 128
    sget-object v0, Ligg;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 129
    if-nez v0, :cond_0

    .line 130
    if-eqz p0, :cond_1

    .line 131
    invoke-static {v2, p2, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 133
    :goto_0
    sget-object v2, Ligg;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ligh;->a()Ligh;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_0
    return-object v0

    .line 132
    :cond_1
    invoke-static {p2, v2, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private o()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 139
    iget-object v0, p0, Ligg;->i:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 140
    invoke-virtual {p0}, Ligg;->aK_()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Ligg;->i:Landroid/graphics/Bitmap;

    .line 141
    iget-object v0, p0, Ligg;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, Ligg;->j:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 142
    iget-object v1, p0, Ligg;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p0, Ligg;->j:I

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    .line 143
    iget v2, p0, Ligg;->c:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 144
    invoke-virtual {p0, v0, v1}, Ligg;->a(II)V

    .line 147
    :cond_0
    iget-object v0, p0, Ligg;->i:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Ligg;->i:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 154
    :cond_0
    iget-object v0, p0, Ligg;->i:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Ligg;->a(Landroid/graphics/Bitmap;)V

    .line 155
    const/4 v0, 0x0

    iput-object v0, p0, Ligg;->i:Landroid/graphics/Bitmap;

    .line 156
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/graphics/Bitmap;)V
.end method

.method public abstract aK_()Landroid/graphics/Bitmap;
.end method

.method protected b(Lifx;)Z
    .locals 1

    .prologue
    .line 285
    invoke-virtual {p0, p1}, Ligg;->c(Lifx;)V

    .line 286
    invoke-virtual {p0}, Ligg;->n()Z

    move-result v0

    return v0
.end method

.method public c()I
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Ligg;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 161
    invoke-direct {p0}, Ligg;->o()Landroid/graphics/Bitmap;

    .line 163
    :cond_0
    iget v0, p0, Ligg;->c:I

    return v0
.end method

.method public c(Lifx;)V
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 199
    invoke-virtual {p0}, Ligg;->i()Z

    move-result v1

    if-nez v1, :cond_7

    .line 200
    invoke-direct {p0}, Ligg;->o()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_6

    :try_start_0
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {p0}, Ligg;->e()I

    move-result v10

    invoke-virtual {p0}, Ligg;->f()I

    move-result v11

    if-gt v8, v10, :cond_0

    if-gt v9, v11, :cond_0

    move v0, v7

    :cond_0
    invoke-static {v0}, Ljunit/framework/Assert;->assertTrue(Z)V

    invoke-interface {p1}, Lifx;->a()Ligd;

    move-result-object v0

    invoke-interface {v0}, Ligd;->a()I

    move-result v0

    iput v0, p0, Ligg;->a:I

    invoke-interface {p1, p0}, Lifx;->b(Lifv;)V

    if-ne v8, v10, :cond_3

    if-ne v9, v11, :cond_3

    invoke-interface {p1, p0, v4}, Lifx;->a(Lifv;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    invoke-direct {p0}, Ligg;->p()V

    invoke-virtual {p0, p1}, Ligg;->a(Lifx;)V

    iput v7, p0, Ligg;->b:I

    iput-boolean v7, p0, Ligg;->g:Z

    .line 212
    :cond_2
    :goto_1
    return-void

    .line 200
    :cond_3
    :try_start_1
    invoke-static {v4}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    invoke-static {v4}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v12

    invoke-interface {p1, p0, v5, v6}, Lifx;->a(Lifv;II)V

    iget v2, p0, Ligg;->j:I

    iget v3, p0, Ligg;->j:I

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lifx;->a(Lifv;IILandroid/graphics/Bitmap;II)V

    iget v0, p0, Ligg;->j:I

    if-lez v0, :cond_4

    const/4 v0, 0x1

    invoke-static {v0, v12, v11}, Ligg;->a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lifx;->a(Lifv;IILandroid/graphics/Bitmap;II)V

    const/4 v0, 0x0

    invoke-static {v0, v12, v10}, Ligg;->a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lifx;->a(Lifv;IILandroid/graphics/Bitmap;II)V

    :cond_4
    iget v0, p0, Ligg;->j:I

    add-int/2addr v0, v8

    if-ge v0, v10, :cond_5

    const/4 v0, 0x1

    invoke-static {v0, v12, v11}, Ligg;->a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v4

    iget v0, p0, Ligg;->j:I

    add-int v2, v0, v8

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lifx;->a(Lifv;IILandroid/graphics/Bitmap;II)V

    :cond_5
    iget v0, p0, Ligg;->j:I

    add-int/2addr v0, v9

    if-ge v0, v11, :cond_1

    const/4 v0, 0x0

    invoke-static {v0, v12, v10}, Ligg;->a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v2, 0x0

    iget v0, p0, Ligg;->j:I

    add-int v3, v0, v9

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lifx;->a(Lifv;IILandroid/graphics/Bitmap;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Ligg;->p()V

    throw v0

    :cond_6
    const/4 v0, -0x1

    iput v0, p0, Ligg;->b:I

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Texture load fail, no bitmap"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_7
    iget-boolean v0, p0, Ligg;->g:Z

    if-nez v0, :cond_2

    .line 205
    invoke-direct {p0}, Ligg;->o()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 206
    invoke-static {v4}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    .line 207
    invoke-static {v4}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    .line 208
    iget v2, p0, Ligg;->j:I

    iget v3, p0, Ligg;->j:I

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lifx;->a(Lifv;IILandroid/graphics/Bitmap;II)V

    .line 209
    invoke-direct {p0}, Ligg;->p()V

    .line 210
    iput-boolean v7, p0, Ligg;->g:Z

    goto/16 :goto_1
.end method

.method public d()I
    .locals 2

    .prologue
    .line 168
    iget v0, p0, Ligg;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 169
    invoke-direct {p0}, Ligg;->o()Landroid/graphics/Bitmap;

    .line 171
    :cond_0
    iget v0, p0, Ligg;->d:I

    return v0
.end method

.method protected h()I
    .locals 1

    .prologue
    .line 291
    const/16 v0, 0xde1

    return v0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 305
    invoke-super {p0}, Lifv;->j()V

    .line 306
    iget-object v0, p0, Ligg;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 307
    invoke-direct {p0}, Ligg;->p()V

    .line 309
    :cond_0
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 300
    iget-boolean v0, p0, Ligg;->h:Z

    return v0
.end method

.method public m()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 179
    iget-object v0, p0, Ligg;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 180
    invoke-direct {p0}, Ligg;->p()V

    .line 182
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ligg;->g:Z

    .line 183
    iput v1, p0, Ligg;->c:I

    .line 184
    iput v1, p0, Ligg;->d:I

    .line 185
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Ligg;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ligg;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
