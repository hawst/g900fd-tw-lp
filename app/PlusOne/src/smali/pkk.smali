.class public final Lpkk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpkk;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:[Lpkl;

.field private e:Lppf;

.field private f:Lppf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    new-array v0, v0, [Lpkk;

    sput-object v0, Lpkk;->a:[Lpkk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 497
    invoke-direct {p0}, Loxq;-><init>()V

    .line 585
    sget-object v0, Lpkl;->a:[Lpkl;

    iput-object v0, p0, Lpkk;->d:[Lpkl;

    .line 588
    iput-object v1, p0, Lpkk;->e:Lppf;

    .line 591
    iput-object v1, p0, Lpkk;->f:Lppf;

    .line 497
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 621
    .line 622
    iget-object v0, p0, Lpkk;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 623
    const/4 v0, 0x1

    iget-object v2, p0, Lpkk;->b:Ljava/lang/String;

    .line 624
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 626
    :goto_0
    iget-object v2, p0, Lpkk;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 627
    const/4 v2, 0x2

    iget-object v3, p0, Lpkk;->c:Ljava/lang/String;

    .line 628
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 630
    :cond_0
    iget-object v2, p0, Lpkk;->d:[Lpkl;

    if-eqz v2, :cond_2

    .line 631
    iget-object v2, p0, Lpkk;->d:[Lpkl;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 632
    if-eqz v4, :cond_1

    .line 633
    const/4 v5, 0x3

    .line 634
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 631
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 638
    :cond_2
    iget-object v1, p0, Lpkk;->e:Lppf;

    if-eqz v1, :cond_3

    .line 639
    const/4 v1, 0x4

    iget-object v2, p0, Lpkk;->e:Lppf;

    .line 640
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 642
    :cond_3
    iget-object v1, p0, Lpkk;->f:Lppf;

    if-eqz v1, :cond_4

    .line 643
    const/4 v1, 0x5

    iget-object v2, p0, Lpkk;->f:Lppf;

    .line 644
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 646
    :cond_4
    iget-object v1, p0, Lpkk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 647
    iput v0, p0, Lpkk;->ai:I

    .line 648
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpkk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 656
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 657
    sparse-switch v0, :sswitch_data_0

    .line 661
    iget-object v2, p0, Lpkk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 662
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpkk;->ah:Ljava/util/List;

    .line 665
    :cond_1
    iget-object v2, p0, Lpkk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 667
    :sswitch_0
    return-object p0

    .line 672
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpkk;->b:Ljava/lang/String;

    goto :goto_0

    .line 676
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpkk;->c:Ljava/lang/String;

    goto :goto_0

    .line 680
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 681
    iget-object v0, p0, Lpkk;->d:[Lpkl;

    if-nez v0, :cond_3

    move v0, v1

    .line 682
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpkl;

    .line 683
    iget-object v3, p0, Lpkk;->d:[Lpkl;

    if-eqz v3, :cond_2

    .line 684
    iget-object v3, p0, Lpkk;->d:[Lpkl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 686
    :cond_2
    iput-object v2, p0, Lpkk;->d:[Lpkl;

    .line 687
    :goto_2
    iget-object v2, p0, Lpkk;->d:[Lpkl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 688
    iget-object v2, p0, Lpkk;->d:[Lpkl;

    new-instance v3, Lpkl;

    invoke-direct {v3}, Lpkl;-><init>()V

    aput-object v3, v2, v0

    .line 689
    iget-object v2, p0, Lpkk;->d:[Lpkl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 690
    invoke-virtual {p1}, Loxn;->a()I

    .line 687
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 681
    :cond_3
    iget-object v0, p0, Lpkk;->d:[Lpkl;

    array-length v0, v0

    goto :goto_1

    .line 693
    :cond_4
    iget-object v2, p0, Lpkk;->d:[Lpkl;

    new-instance v3, Lpkl;

    invoke-direct {v3}, Lpkl;-><init>()V

    aput-object v3, v2, v0

    .line 694
    iget-object v2, p0, Lpkk;->d:[Lpkl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 698
    :sswitch_4
    iget-object v0, p0, Lpkk;->e:Lppf;

    if-nez v0, :cond_5

    .line 699
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpkk;->e:Lppf;

    .line 701
    :cond_5
    iget-object v0, p0, Lpkk;->e:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 705
    :sswitch_5
    iget-object v0, p0, Lpkk;->f:Lppf;

    if-nez v0, :cond_6

    .line 706
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpkk;->f:Lppf;

    .line 708
    :cond_6
    iget-object v0, p0, Lpkk;->f:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 657
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 596
    iget-object v0, p0, Lpkk;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 597
    const/4 v0, 0x1

    iget-object v1, p0, Lpkk;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 599
    :cond_0
    iget-object v0, p0, Lpkk;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 600
    const/4 v0, 0x2

    iget-object v1, p0, Lpkk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 602
    :cond_1
    iget-object v0, p0, Lpkk;->d:[Lpkl;

    if-eqz v0, :cond_3

    .line 603
    iget-object v1, p0, Lpkk;->d:[Lpkl;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 604
    if-eqz v3, :cond_2

    .line 605
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 603
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 609
    :cond_3
    iget-object v0, p0, Lpkk;->e:Lppf;

    if-eqz v0, :cond_4

    .line 610
    const/4 v0, 0x4

    iget-object v1, p0, Lpkk;->e:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 612
    :cond_4
    iget-object v0, p0, Lpkk;->f:Lppf;

    if-eqz v0, :cond_5

    .line 613
    const/4 v0, 0x5

    iget-object v1, p0, Lpkk;->f:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 615
    :cond_5
    iget-object v0, p0, Lpkk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 617
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 493
    invoke-virtual {p0, p1}, Lpkk;->a(Loxn;)Lpkk;

    move-result-object v0

    return-object v0
.end method
