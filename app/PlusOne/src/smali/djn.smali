.class public final Ldjn;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmaq;",
        "Lmar;",
        ">;"
    }
.end annotation


# static fields
.field private static final w:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private final s:Z

.field private final t:Z

.field private u:Z

.field private final v:Lhrt;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "polling_token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "resume_token"

    aput-object v2, v0, v1

    sput-object v0, Ldjn;->w:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    .line 79
    const-string v5, "eventread"

    new-instance v6, Lmaq;

    invoke-direct {v6}, Lmaq;-><init>()V

    new-instance v7, Lmar;

    invoke-direct {v7}, Lmar;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v1 .. v7}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 65
    const/4 v1, 0x0

    iput-boolean v1, p0, Ldjn;->u:Z

    .line 81
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Event ID must not be empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 84
    :cond_0
    iput-object p4, p0, Ldjn;->a:Ljava/lang/String;

    .line 85
    iput-object p5, p0, Ldjn;->p:Ljava/lang/String;

    .line 86
    iput-object p6, p0, Ldjn;->q:Ljava/lang/String;

    .line 87
    iput-object p7, p0, Ldjn;->b:Ljava/lang/String;

    .line 88
    move-object/from16 v0, p8

    iput-object v0, p0, Ldjn;->r:Ljava/lang/String;

    .line 89
    move/from16 v0, p9

    iput-boolean v0, p0, Ldjn;->s:Z

    .line 90
    const/4 v1, 0x0

    iput-boolean v1, p0, Ldjn;->t:Z

    .line 91
    const-class v1, Lhrt;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhrt;

    iput-object v1, p0, Ldjn;->v:Lhrt;

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 96
    const-string v4, "eventread"

    new-instance v5, Lmaq;

    invoke-direct {v5}, Lmaq;-><init>()V

    new-instance v6, Lmar;

    invoke-direct {v6}, Lmar;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldjn;->u:Z

    .line 98
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Event ID must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    iput-object p4, p0, Ldjn;->a:Ljava/lang/String;

    .line 102
    iput-object v7, p0, Ldjn;->p:Ljava/lang/String;

    .line 103
    iput-object v7, p0, Ldjn;->q:Ljava/lang/String;

    .line 104
    iput-object p5, p0, Ldjn;->b:Ljava/lang/String;

    .line 105
    iput-object v7, p0, Ldjn;->r:Ljava/lang/String;

    .line 106
    iput-boolean p6, p0, Ldjn;->s:Z

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldjn;->t:Z

    .line 108
    const-class v0, Lhrt;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrt;

    iput-object v0, p0, Ldjn;->v:Lhrt;

    .line 109
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/io/IOException;)V
    .locals 3

    .prologue
    .line 392
    iget-boolean v0, p0, Ldjn;->u:Z

    if-eqz v0, :cond_0

    .line 393
    const/16 v0, 0x193

    const-string v1, "INSUFFICIENT_PERMISSION"

    const/4 v2, 0x0

    invoke-super {p0, v0, v1, v2}, Leuh;->a(ILjava/lang/String;Ljava/io/IOException;)V

    .line 397
    :goto_0
    return-void

    .line 395
    :cond_0
    invoke-super {p0, p1, p2, p3}, Leuh;->a(ILjava/lang/String;Ljava/io/IOException;)V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 402
    const/16 v0, 0x194

    if-ne p1, v0, :cond_1

    .line 403
    iget-object v0, p0, Ldjn;->f:Landroid/content/Context;

    iget v1, p0, Ldjn;->c:I

    iget-object v2, p0, Ldjn;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Ldrm;->e(Landroid/content/Context;ILjava/lang/String;)V

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    const/16 v0, 0x190

    if-lt p1, v0, :cond_0

    .line 406
    iget-object v0, p0, Ldjn;->a:Ljava/lang/String;

    iget-object v1, p0, Ldjn;->f:Landroid/content/Context;

    invoke-static {v1}, Lhqd;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    const-string v0, "HttpOperation"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 408
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x34

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "[EVENT_READ] received error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; disable IS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    :cond_2
    iget-object v0, p0, Ldjn;->v:Lhrt;

    iget-object v1, p0, Ldjn;->f:Landroid/content/Context;

    iget v2, p0, Ldjn;->c:I

    invoke-virtual {v0, v1, v2}, Lhrt;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected a(Lmaq;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 113
    .line 114
    iget-object v0, p0, Ldjn;->f:Landroid/content/Context;

    iget v3, p0, Ldjn;->c:I

    iget-object v4, p0, Ldjn;->a:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Ldrm;->c(Landroid/content/Context;ILjava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_5

    .line 115
    iget-object v0, p0, Ldjn;->f:Landroid/content/Context;

    iget v3, p0, Ldjn;->c:I

    iget-object v4, p0, Ldjn;->a:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Ldrm;->d(Landroid/content/Context;ILjava/lang/String;)V

    move v0, v1

    .line 119
    :goto_0
    iget-boolean v3, p0, Ldjn;->t:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_2

    .line 120
    :cond_0
    iget-object v0, p0, Ldjn;->f:Landroid/content/Context;

    iget v3, p0, Ldjn;->c:I

    iget-object v4, p0, Ldjn;->a:Ljava/lang/String;

    sget-object v5, Ldjn;->w:[Ljava/lang/String;

    invoke-static {v0, v3, v4, v5}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 122
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjn;->p:Ljava/lang/String;

    .line 124
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjn;->q:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 131
    :cond_2
    new-instance v0, Lmqi;

    invoke-direct {v0}, Lmqi;-><init>()V

    .line 132
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lmqi;->b:Ljava/lang/Boolean;

    .line 133
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lmqi;->a:Ljava/lang/Boolean;

    .line 135
    new-instance v3, Lmqf;

    invoke-direct {v3}, Lmqf;-><init>()V

    .line 136
    const/16 v4, 0x3e8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lmqf;->a:Ljava/lang/Integer;

    .line 137
    new-instance v4, Lmqe;

    invoke-direct {v4}, Lmqe;-><init>()V

    .line 138
    const/16 v5, 0x1f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lmqe;->a:Ljava/lang/Integer;

    .line 140
    new-instance v5, Lmqh;

    invoke-direct {v5}, Lmqh;-><init>()V

    .line 142
    const/16 v6, 0x32

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lmqh;->b:Ljava/lang/Integer;

    .line 143
    new-array v6, v1, [Lmqh;

    aput-object v5, v6, v2

    .line 146
    new-instance v5, Lmpm;

    invoke-direct {v5}, Lmpm;-><init>()V

    .line 147
    iget-object v7, p0, Ldjn;->a:Ljava/lang/String;

    iput-object v7, v5, Lmpm;->a:Ljava/lang/String;

    .line 148
    iget-object v7, p0, Ldjn;->b:Ljava/lang/String;

    iput-object v7, v5, Lmpm;->b:Ljava/lang/String;

    .line 150
    new-instance v7, Lmqd;

    invoke-direct {v7}, Lmqd;-><init>()V

    .line 151
    iput-object v6, v7, Lmqd;->b:[Lmqh;

    .line 152
    iput-object v3, v7, Lmqd;->c:Lmqf;

    .line 153
    iput-object v4, v7, Lmqd;->d:Lmqe;

    .line 154
    iput-object v0, v7, Lmqd;->e:Lmqi;

    .line 155
    iput v1, v7, Lmqd;->h:I

    .line 156
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v7, Lmqd;->f:Ljava/lang/Boolean;

    .line 157
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v7, Lmqd;->g:Ljava/lang/Boolean;

    .line 158
    new-array v0, v1, [Lmqd;

    aput-object v7, v0, v2

    .line 160
    new-instance v1, Lmqj;

    invoke-direct {v1}, Lmqj;-><init>()V

    iput-object v1, p1, Lmaq;->a:Lmqj;

    .line 161
    iget-object v1, p1, Lmaq;->a:Lmqj;

    .line 162
    iput-object v0, v1, Lmqj;->d:[Lmqd;

    .line 163
    iput-object v5, v1, Lmqj;->a:Lmpm;

    .line 164
    iget-object v0, p0, Ldjn;->r:Ljava/lang/String;

    iput-object v0, v1, Lmqj;->e:Ljava/lang/String;

    .line 165
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, v1, Lmqj;->f:Loxz;

    .line 166
    iget-object v0, v1, Lmqj;->f:Loxz;

    invoke-static {}, Ldrm;->a()[I

    move-result-object v2

    iput-object v2, v0, Loxz;->a:[I

    .line 169
    iget-boolean v0, p0, Ldjn;->s:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Ldjn;->q:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 170
    :cond_3
    iget-object v0, p0, Ldjn;->p:Ljava/lang/String;

    iput-object v0, v1, Lmqj;->c:Ljava/lang/String;

    .line 174
    :goto_1
    return-void

    .line 127
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 172
    :cond_4
    iget-object v0, p0, Ldjn;->q:Ljava/lang/String;

    iput-object v0, v1, Lmqj;->b:Ljava/lang/String;

    goto :goto_1

    :cond_5
    move v0, v2

    goto/16 :goto_0
.end method

.method protected a(Lmar;)V
    .locals 30

    .prologue
    .line 197
    move-object/from16 v0, p1

    iget-object v0, v0, Lmar;->a:Lmqk;

    move-object/from16 v17, v0

    .line 203
    move-object/from16 v0, v17

    iget v4, v0, Lmqk;->i:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 204
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Ldjn;->u:Z

    .line 388
    :goto_0
    return-void

    .line 208
    :cond_0
    move-object/from16 v0, v17

    iget-object v9, v0, Lmqk;->b:Logr;

    .line 209
    move-object/from16 v0, v17

    iget-object v4, v0, Lmqk;->a:Loya;

    .line 210
    invoke-static {v4}, Ldrm;->a(Loya;)Lidh;

    move-result-object v8

    .line 211
    move-object/from16 v0, v17

    iget-object v7, v0, Lmqk;->c:Ljava/lang/String;

    .line 212
    move-object/from16 v0, p0

    iget-object v4, v0, Ldjn;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v5, v0, Ldjn;->c:I

    invoke-static {v4, v5, v8}, Ldrm;->a(Landroid/content/Context;ILidh;)J

    move-result-wide v14

    .line 213
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 214
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 218
    move-object/from16 v0, v17

    iget-object v4, v0, Lmqk;->d:[Lmqn;

    if-nez v4, :cond_2

    move-object/from16 v0, v17

    iget-object v4, v0, Lmqk;->f:[Lofa;

    if-nez v4, :cond_2

    move-object/from16 v0, v17

    iget-object v4, v0, Lmqk;->e:[Loax;

    if-nez v4, :cond_2

    .line 219
    move-object/from16 v0, p0

    iget-boolean v4, v0, Ldjn;->s:Z

    if-eqz v4, :cond_1

    .line 221
    move-object/from16 v0, p0

    iget-object v4, v0, Ldjn;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v5, v0, Ldjn;->c:I

    invoke-static {v4, v5, v7, v8, v9}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Lidh;Logr;)V

    goto :goto_0

    .line 224
    :cond_1
    const/4 v4, 0x0

    move-object/from16 v0, v17

    iput-object v4, v0, Lmqk;->h:Ljava/lang/String;

    .line 229
    :cond_2
    move-object/from16 v0, v17

    iget-object v4, v0, Lmqk;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-boolean v4, v0, Ldjn;->s:Z

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v11, v0, Ldjn;->q:Ljava/lang/String;

    .line 233
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Ldjn;->q:Ljava/lang/String;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Ldjn;->s:Z

    if-eqz v4, :cond_c

    :cond_3
    move-object/from16 v0, v17

    iget-object v10, v0, Lmqk;->g:Ljava/lang/String;

    .line 236
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Ldjn;->p:Ljava/lang/String;

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Ldjn;->q:Ljava/lang/String;

    if-eqz v4, :cond_d

    :cond_4
    const/4 v13, 0x1

    .line 237
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Ldjn;->s:Z

    if-nez v4, :cond_e

    const/4 v4, 0x1

    .line 241
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v5, v0, Ldjn;->s:Z

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Ldjn;->p:Ljava/lang/String;

    if-eqz v5, :cond_5

    move-object/from16 v0, v17

    iget-object v5, v0, Lmqk;->h:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 242
    move-object/from16 v0, p0

    iget-object v4, v0, Ldjn;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v5, v0, Ldjn;->c:I

    invoke-virtual {v8}, Lidh;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Ldrm;->e(Landroid/content/Context;ILjava/lang/String;)V

    .line 243
    const/4 v4, 0x1

    .line 246
    :cond_5
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 247
    move-object/from16 v0, v17

    iget-object v5, v0, Lmqk;->d:[Lmqn;

    if-eqz v5, :cond_10

    .line 248
    move-object/from16 v0, v17

    iget-object v0, v0, Lmqk;->d:[Lmqn;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    const/4 v5, 0x0

    move v6, v5

    :goto_5
    move/from16 v0, v20

    if-ge v6, v0, :cond_10

    aget-object v5, v19, v6

    .line 249
    iget-object v0, v5, Lmqn;->b:[Lnym;

    move-object/from16 v21, v0

    if-eqz v21, :cond_f

    .line 250
    iget-object v0, v5, Lmqn;->b:[Lnym;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    const/4 v5, 0x0

    :goto_6
    move/from16 v0, v22

    if-ge v5, v0, :cond_f

    aget-object v23, v21, v5

    .line 251
    new-instance v24, Ldrq;

    invoke-direct/range {v24 .. v24}, Ldrq;-><init>()V

    .line 252
    const/16 v25, 0x64

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Ldrq;->a:I

    .line 255
    move-object/from16 v0, v23

    iget-object v0, v0, Lnym;->p:Ljava/lang/Double;

    move-object/from16 v25, v0

    if-eqz v25, :cond_6

    .line 256
    move-object/from16 v0, v23

    iget-object v0, v0, Lnym;->p:Ljava/lang/Double;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    const-wide v28, 0x408f400000000000L    # 1000.0

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-long v0, v0

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    move-object/from16 v2, v24

    iput-wide v0, v2, Ldrq;->b:J

    .line 259
    :cond_6
    move-object/from16 v0, v23

    iget-object v0, v0, Lnym;->h:Lnyz;

    move-object/from16 v25, v0

    if-eqz v25, :cond_7

    .line 260
    move-object/from16 v0, v23

    iget-object v0, v0, Lnym;->h:Lnyz;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lnyz;->c:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    iput-object v0, v1, Ldrq;->c:Ljava/lang/String;

    .line 261
    move-object/from16 v0, v23

    iget-object v0, v0, Lnym;->h:Lnyz;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lnyz;->d:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    iput-object v0, v1, Ldrq;->d:Ljava/lang/String;

    .line 263
    new-instance v25, Lpaf;

    invoke-direct/range {v25 .. v25}, Lpaf;-><init>()V

    .line 264
    move-object/from16 v0, v24

    iget-object v0, v0, Ldrq;->d:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lpaf;->b:Ljava/lang/String;

    .line 265
    move-object/from16 v0, v24

    iget-object v0, v0, Ldrq;->c:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lpaf;->c:Ljava/lang/String;

    .line 266
    move-object/from16 v0, v23

    iget-object v0, v0, Lnym;->h:Lnyz;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lnyz;->e:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lpaf;->d:Ljava/lang/String;

    .line 267
    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_7
    move-object/from16 v0, v24

    iget-object v0, v0, Ldrq;->c:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    move-object/from16 v0, v23

    iget-object v0, v0, Lnym;->b:Lnyl;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    move-object/from16 v0, v23

    iget-object v0, v0, Lnym;->b:Lnyl;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lnyl;->b:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    .line 274
    if-eqz v4, :cond_8

    .line 275
    move-object/from16 v0, v24

    iget-wide v14, v0, Ldrq;->b:J

    .line 278
    :cond_8
    new-instance v25, Lnzx;

    invoke-direct/range {v25 .. v25}, Lnzx;-><init>()V

    new-instance v26, Lnzu;

    invoke-direct/range {v26 .. v26}, Lnzu;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    iput-object v0, v1, Lnzu;->b:Lnym;

    sget-object v27, Lnzu;->a:Loxr;

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    move-object/from16 v0, v23

    iget-object v0, v0, Lnym;->b:Lnyl;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    iput-object v0, v1, Lnzx;->f:Lnyl;

    const/16 v23, 0x4

    move/from16 v0, v23

    move-object/from16 v1, v25

    iput v0, v1, Lnzx;->k:I

    .line 279
    invoke-static/range {v25 .. v25}, Ljvj;->a(Lnzx;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    iput-object v0, v1, Lnzx;->b:Ljava/lang/String;

    .line 280
    move-object/from16 v0, v25

    iget-object v0, v0, Lnzx;->b:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    iput-object v0, v1, Ldrq;->f:Ljava/lang/String;

    .line 281
    invoke-static/range {v25 .. v25}, Lnzx;->a(Loxu;)[B

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    iput-object v0, v1, Ldrq;->e:[B

    .line 282
    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_6

    .line 229
    :cond_a
    const/4 v11, 0x0

    goto/16 :goto_1

    :cond_b
    move-object/from16 v0, v17

    iget-object v11, v0, Lmqk;->h:Ljava/lang/String;

    goto/16 :goto_1

    .line 233
    :cond_c
    move-object/from16 v0, p0

    iget-object v10, v0, Ldjn;->p:Ljava/lang/String;

    goto/16 :goto_2

    .line 236
    :cond_d
    const/4 v13, 0x0

    goto/16 :goto_3

    .line 237
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 248
    :cond_f
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto/16 :goto_5

    .line 291
    :cond_10
    const/4 v4, 0x3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v19, 0x0

    .line 292
    invoke-virtual {v8}, Lidh;->e()Ljava/lang/String;

    move-result-object v20

    invoke-virtual {v8}, Lidh;->c()Ljava/lang/String;

    move-result-object v21

    const-string v22, "PLUS_EVENT"

    .line 291
    invoke-static/range {v19 .. v22}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v5, v6

    invoke-static {v4, v5}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 295
    move-object/from16 v0, p0

    iget-object v6, v0, Ldjn;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v0, v0, Ldjn;->c:I

    move/from16 v19, v0

    .line 296
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lnzx;

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lnzx;

    const/16 v18, 0x0

    .line 295
    move/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v6, v0, v5, v4, v1}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;Ljava/lang/String;)V

    .line 299
    move-object/from16 v0, v17

    iget-object v4, v0, Lmqk;->f:[Lofa;

    if-eqz v4, :cond_15

    .line 300
    move-object/from16 v0, v17

    iget-object v5, v0, Lmqk;->f:[Lofa;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_7
    if-ge v4, v6, :cond_15

    aget-object v18, v5, v4

    .line 301
    new-instance v19, Ldrq;

    invoke-direct/range {v19 .. v19}, Ldrq;-><init>()V

    .line 302
    const/16 v20, 0x5

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Ldrq;->a:I

    .line 303
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->e:Ljava/lang/Long;

    move-object/from16 v20, v0

    if-eqz v20, :cond_11

    .line 304
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->e:Ljava/lang/Long;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, v19

    iput-wide v0, v2, Ldrq;->b:J

    .line 306
    :cond_11
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->g:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Ldrq;->c:Ljava/lang/String;

    .line 307
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->b:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Ldrq;->d:Ljava/lang/String;

    .line 309
    new-instance v20, Lpaf;

    invoke-direct/range {v20 .. v20}, Lpaf;-><init>()V

    .line 310
    move-object/from16 v0, v19

    iget-object v0, v0, Ldrq;->d:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lpaf;->b:Ljava/lang/String;

    .line 311
    move-object/from16 v0, v19

    iget-object v0, v0, Ldrq;->c:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lpaf;->c:Ljava/lang/String;

    .line 312
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->m:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lpaf;->d:Ljava/lang/String;

    .line 313
    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    new-instance v20, Ldru;

    invoke-direct/range {v20 .. v20}, Ldru;-><init>()V

    .line 316
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->f:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Ldru;->commentId:Ljava/lang/String;

    .line 317
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->c:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Ldru;->text:Ljava/lang/String;

    .line 318
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->j:Ljava/lang/Boolean;

    move-object/from16 v21, v0

    if-eqz v21, :cond_12

    .line 319
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->j:Ljava/lang/Boolean;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput-boolean v0, v1, Ldru;->ownedByViewer:Z

    .line 321
    :cond_12
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->l:Loae;

    move-object/from16 v21, v0

    if-eqz v21, :cond_13

    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->l:Loae;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Loae;->e:Ljava/lang/Integer;

    move-object/from16 v21, v0

    if-eqz v21, :cond_13

    .line 322
    move-object/from16 v0, v18

    iget-object v0, v0, Lofa;->l:Loae;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Loae;->e:Ljava/lang/Integer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, v20

    iput-wide v0, v2, Ldru;->totalPlusOnes:J

    .line 326
    :cond_13
    move-object/from16 v0, v19

    iget-object v0, v0, Ldrq;->c:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_14

    move-object/from16 v0, v20

    iget-object v0, v0, Ldru;->text:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_14

    .line 327
    sget-object v18, Ldrm;->a:Lesn;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lesn;->a(Ljava/lang/Object;)[B

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    iput-object v0, v1, Ldrq;->e:[B

    .line 328
    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 300
    :cond_14
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_7

    .line 333
    :cond_15
    move-object/from16 v0, v17

    iget-object v4, v0, Lmqk;->e:[Loax;

    if-eqz v4, :cond_1b

    .line 334
    move-object/from16 v0, v17

    iget-object v6, v0, Lmqk;->e:[Loax;

    array-length v0, v6

    move/from16 v17, v0

    const/4 v4, 0x0

    move v5, v4

    :goto_8
    move/from16 v0, v17

    if-ge v5, v0, :cond_1b

    aget-object v4, v6, v5

    .line 335
    new-instance v18, Ldrq;

    invoke-direct/range {v18 .. v18}, Ldrq;-><init>()V

    .line 339
    iget v0, v4, Loax;->b:I

    move/from16 v19, v0

    const/high16 v20, -0x80000000

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_16

    .line 340
    iget v0, v4, Loax;->b:I

    move/from16 v19, v0

    packed-switch v19, :pswitch_data_0

    .line 356
    :cond_16
    :goto_9
    iget-object v0, v4, Loax;->c:Ljava/lang/Long;

    move-object/from16 v19, v0

    if-eqz v19, :cond_17

    .line 357
    iget-object v0, v4, Loax;->c:Ljava/lang/Long;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    iput-wide v0, v2, Ldrq;->b:J

    .line 360
    :cond_17
    new-instance v19, Ldrt;

    invoke-direct/range {v19 .. v19}, Ldrt;-><init>()V

    .line 361
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Ldrt;->people:Ljava/util/List;

    .line 363
    iget-object v0, v4, Loax;->d:[Lpax;

    move-object/from16 v20, v0

    if-eqz v20, :cond_19

    .line 364
    iget-object v0, v4, Loax;->d:[Lpax;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    const/4 v4, 0x0

    :goto_a
    move/from16 v0, v21

    if-ge v4, v0, :cond_19

    aget-object v22, v20, v4

    .line 365
    move-object/from16 v0, v22

    iget-object v0, v0, Lpax;->b:Lpaf;

    move-object/from16 v23, v0

    .line 366
    if-eqz v23, :cond_18

    move-object/from16 v0, v23

    iget-object v0, v0, Lpaf;->c:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_18

    .line 367
    new-instance v24, Ldrv;

    invoke-direct/range {v24 .. v24}, Ldrv;-><init>()V

    .line 368
    move-object/from16 v0, v23

    iget-object v0, v0, Lpaf;->c:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    iput-object v0, v1, Ldrv;->gaiaId:Ljava/lang/String;

    .line 369
    move-object/from16 v0, v23

    iget-object v0, v0, Lpaf;->b:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    iput-object v0, v1, Ldrv;->name:Ljava/lang/String;

    .line 370
    move-object/from16 v0, v19

    iget-object v0, v0, Ldrt;->people:Ljava/util/List;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 371
    move-object/from16 v0, v22

    iget-object v0, v0, Lpax;->d:Ljava/lang/Integer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v24

    iput v0, v1, Ldrv;->numAdditionalGuests:I

    .line 372
    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    :cond_18
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 342
    :pswitch_0
    const/16 v19, 0x2

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Ldrq;->a:I

    goto/16 :goto_9

    .line 345
    :pswitch_1
    const/16 v19, 0x3

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Ldrq;->a:I

    goto/16 :goto_9

    .line 348
    :pswitch_2
    const/16 v19, 0x4

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Ldrq;->a:I

    goto/16 :goto_9

    .line 351
    :pswitch_3
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Ldrq;->a:I

    goto/16 :goto_9

    .line 378
    :cond_19
    move-object/from16 v0, v19

    iget-object v4, v0, Ldrt;->people:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1a

    .line 379
    sget-object v4, Ldrm;->b:Lesn;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lesn;->a(Ljava/lang/Object;)[B

    move-result-object v4

    move-object/from16 v0, v18

    iput-object v4, v0, Ldrq;->e:[B

    .line 380
    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    :cond_1a
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_8

    .line 385
    :cond_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Ldjn;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v6, v0, Ldjn;->c:I

    invoke-static/range {v5 .. v16}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Lidh;Logr;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZJLjava/util/List;)V

    goto/16 :goto_0

    .line 340
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Lmaq;

    invoke-virtual {p0, p1}, Ldjn;->a(Lmaq;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Lmar;

    invoke-virtual {p0, p1}, Ldjn;->a(Lmar;)V

    return-void
.end method
