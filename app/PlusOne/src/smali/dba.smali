.class public Ldba;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field public static final N:Lloz;


# instance fields
.field private O:I

.field private P:Ldgr;

.field private Q:Ldeo;

.field private R:Ldei;

.field private final S:Ldbg;

.field private T:Ldbh;

.field private U:Landroid/view/View;

.field private V:Ldbe;

.field private final W:Lhoc;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lloz;

    const-string v1, "debug.plus.photos_hashtag_ui"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Ldba;->N:Lloz;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 41
    invoke-direct {p0}, Llol;-><init>()V

    .line 49
    new-instance v0, Ldbg;

    invoke-direct {v0, p0}, Ldbg;-><init>(Ldba;)V

    iput-object v0, p0, Ldba;->S:Ldbg;

    .line 53
    new-instance v0, Lhoc;

    .line 54
    invoke-virtual {p0}, Ldba;->z_()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Ldba;->W:Lhoc;

    .line 57
    new-instance v0, Ldgn;

    iget-object v1, p0, Ldba;->av:Llqm;

    sget-object v2, Ldgx;->S:Ldgo;

    new-instance v3, Ldbb;

    invoke-direct {v3, p0}, Ldbb;-><init>(Ldba;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 63
    new-instance v0, Ldep;

    iget-object v1, p0, Ldba;->av:Llqm;

    new-instance v2, Ldbc;

    invoke-direct {v2, p0}, Ldbc;-><init>(Ldba;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 181
    return-void
.end method

.method static synthetic a(Ldba;)Ldbh;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldba;->T:Ldbh;

    return-object v0
.end method

.method static synthetic a(Ldba;Z)V
    .locals 2

    .prologue
    .line 41
    iget-object v1, p0, Ldba;->U:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method static synthetic b(Ldba;)V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Ldba;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldba;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->g()Lnym;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldba;->V:Ldbe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldba;->P:Ldgr;

    sget-object v1, Ldgx;->S:Ldgo;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    iget-object v0, p0, Ldba;->V:Ldbe;

    iget-object v1, p0, Ldba;->Q:Ldeo;

    invoke-virtual {v1}, Ldeo;->a()Lddl;

    move-result-object v1

    invoke-interface {v1}, Lddl;->g()Lnym;

    move-result-object v1

    iget-object v1, v1, Lnym;->O:[Lnyi;

    invoke-virtual {v0, v1}, Ldbe;->a([Lnyi;)V

    iget-object v0, p0, Ldba;->R:Ldei;

    iget-object v1, p0, Ldba;->V:Ldbe;

    invoke-virtual {v1}, Ldbe;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ldei;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldba;->P:Ldgr;

    sget-object v1, Ldgx;->S:Ldgo;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    goto :goto_0
.end method

.method static synthetic c(Ldba;)Ldeo;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldba;->Q:Ldeo;

    return-object v0
.end method

.method static synthetic d(Ldba;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Ldba;->O:I

    return v0
.end method

.method static synthetic e(Ldba;)Lhoc;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldba;->W:Lhoc;

    return-object v0
.end method

.method static synthetic f(Ldba;)Llnl;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldba;->at:Llnl;

    return-object v0
.end method

.method static synthetic g(Ldba;)Llnl;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldba;->at:Llnl;

    return-object v0
.end method

.method static synthetic h(Ldba;)Ldgr;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldba;->P:Ldgr;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 90
    const v0, 0x7f040186

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldba;->U:Landroid/view/View;

    .line 91
    iget-object v0, p0, Ldba;->U:Landroid/view/View;

    const v1, 0x7f1004bf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 92
    new-instance v1, Ldbe;

    invoke-virtual {p0}, Ldba;->n()Lz;

    move-result-object v2

    iget v3, p0, Ldba;->O:I

    new-instance v3, Ldbd;

    invoke-direct {v3, p0}, Ldbd;-><init>(Ldba;)V

    new-instance v4, Ldbf;

    invoke-direct {v4, p0}, Ldbf;-><init>(Ldba;)V

    invoke-direct {v1, v2, v3, v4}, Ldbe;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Ldba;->V:Ldbe;

    .line 94
    iget-object v1, p0, Ldba;->V:Ldbe;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 96
    iget-object v0, p0, Ldba;->U:Landroid/view/View;

    const v1, 0x7f1004c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    .line 98
    invoke-virtual {v0, p0}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 100
    iget-object v0, p0, Ldba;->U:Landroid/view/View;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Ldba;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ldba;->O:I

    .line 75
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Llol;->aO_()V

    .line 106
    iget-object v0, p0, Ldba;->T:Ldbh;

    iget-object v1, p0, Ldba;->S:Ldbg;

    invoke-interface {v0, v1}, Ldbh;->a(Ldbi;)V

    .line 107
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Ldba;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Ldba;->P:Ldgr;

    .line 82
    iget-object v0, p0, Ldba;->au:Llnh;

    const-class v1, Ldbh;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbh;

    iput-object v0, p0, Ldba;->T:Ldbh;

    .line 83
    iget-object v0, p0, Ldba;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Ldba;->Q:Ldeo;

    .line 84
    iget-object v0, p0, Ldba;->au:Llnh;

    const-class v1, Ldei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldei;

    iput-object v0, p0, Ldba;->R:Ldei;

    .line 85
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 129
    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    .line 130
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "^#+"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 132
    const-string v2, ""

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v2, p0, Ldba;->Q:Ldeo;

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->j()Ljava/lang/String;

    move-result-object v4

    .line 134
    iget-object v2, p0, Ldba;->Q:Ldeo;

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->o()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 135
    iget-object v2, p0, Ldba;->Q:Ldeo;

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->g()Lnym;

    move-result-object v2

    iget-object v3, v2, Lnym;->f:Ljava/lang/String;

    .line 136
    new-array v8, v9, [Ljava/lang/String;

    const-string v2, "ut:#"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v8, v1

    .line 137
    invoke-virtual {p0}, Ldba;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "view_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 138
    iget-object v0, p0, Ldba;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->k()Ljava/lang/String;

    move-result-object v7

    .line 139
    iget-object v10, p0, Ldba;->W:Lhoc;

    new-instance v0, Ldol;

    invoke-virtual {p0}, Ldba;->n()Lz;

    move-result-object v1

    iget v2, p0, Ldba;->O:I

    invoke-direct/range {v0 .. v8}, Ldol;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Lhoc;->c(Lhny;)V

    move v0, v9

    .line 144
    :goto_1
    return v0

    .line 136
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 144
    goto :goto_1
.end method

.method public z()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Llol;->z()V

    .line 112
    iget-object v0, p0, Ldba;->T:Ldbh;

    iget-object v1, p0, Ldba;->S:Ldbg;

    invoke-interface {v0, v1}, Ldbh;->b(Ldbi;)V

    .line 113
    return-void
.end method
