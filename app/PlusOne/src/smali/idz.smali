.class final Lidz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lidp;


# direct methods
.method constructor <init>(Lidp;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lidz;->a:Lidp;

    .line 35
    return-void
.end method


# virtual methods
.method protected a(Ljava/io/InputStream;)Lidq;
    .locals 5

    .prologue
    .line 46
    iget-object v0, p0, Lidz;->a:Lidp;

    invoke-static {p1, v0}, Lidv;->a(Ljava/io/InputStream;Lidp;)Lidv;

    move-result-object v1

    .line 47
    new-instance v2, Lidq;

    invoke-virtual {v1}, Lidv;->o()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-direct {v2, v0}, Lidq;-><init>(Ljava/nio/ByteOrder;)V

    .line 48
    invoke-virtual {v1}, Lidv;->a()I

    move-result v0

    .line 51
    :goto_0
    const/4 v3, 0x5

    if-eq v0, v3, :cond_3

    .line 52
    packed-switch v0, :pswitch_data_0

    .line 84
    :cond_0
    :goto_1
    invoke-virtual {v1}, Lidv;->a()I

    move-result v0

    goto :goto_0

    .line 54
    :pswitch_0
    new-instance v0, Lieb;

    invoke-virtual {v1}, Lidv;->d()I

    move-result v3

    invoke-direct {v0, v3}, Lieb;-><init>(I)V

    invoke-virtual {v2, v0}, Lidq;->a(Lieb;)V

    goto :goto_1

    .line 57
    :pswitch_1
    invoke-virtual {v1}, Lidv;->c()Liea;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Liea;->f()Z

    move-result v3

    if-nez v3, :cond_1

    .line 59
    invoke-virtual {v1, v0}, Lidv;->a(Liea;)V

    goto :goto_1

    .line 61
    :cond_1
    invoke-virtual {v0}, Liea;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lidq;->b(I)Lieb;

    move-result-object v3

    invoke-virtual {v3, v0}, Lieb;->a(Liea;)Liea;

    goto :goto_1

    .line 65
    :pswitch_2
    invoke-virtual {v1}, Lidv;->c()Liea;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Liea;->c()S

    move-result v3

    const/4 v4, 0x7

    if-ne v3, v4, :cond_2

    .line 67
    invoke-virtual {v1, v0}, Lidv;->b(Liea;)V

    .line 69
    :cond_2
    invoke-virtual {v0}, Liea;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lidq;->b(I)Lieb;

    move-result-object v3

    invoke-virtual {v3, v0}, Lieb;->a(Liea;)Liea;

    goto :goto_1

    .line 72
    :pswitch_3
    invoke-virtual {v1}, Lidv;->g()I

    move-result v0

    new-array v0, v0, [B

    .line 73
    array-length v3, v0

    invoke-virtual {v1, v0}, Lidv;->a([B)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 74
    invoke-virtual {v2, v0}, Lidq;->a([B)V

    goto :goto_1

    .line 80
    :pswitch_4
    invoke-virtual {v1}, Lidv;->f()I

    move-result v0

    new-array v0, v0, [B

    .line 81
    array-length v3, v0

    invoke-virtual {v1, v0}, Lidv;->a([B)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 82
    invoke-virtual {v1}, Lidv;->e()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Lidq;->a(I[B)V

    goto :goto_1

    .line 90
    :cond_3
    return-object v2

    .line 52
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
