.class public final Lkye;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhmm;
.implements Llcz;
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Llci;

.field private c:Landroid/text/StaticLayout;

.field private d:Landroid/text/StaticLayout;

.field private e:Landroid/text/StaticLayout;

.field private f:I

.field private g:I

.field private h:I

.field private i:Landroid/graphics/Bitmap;

.field private j:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private k:I

.field private l:Llbz;

.field private m:Ljava/lang/String;

.field private n:I

.field private o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

.field private p:Lkvq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lkye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lkye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    sget-object v0, Lkye;->a:Llct;

    if-nez v0, :cond_0

    .line 91
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lkye;->a:Llct;

    .line 94
    :cond_0
    const-class v0, Lkvq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkvq;

    iput-object v0, p0, Lkye;->p:Lkvq;

    .line 95
    new-instance v0, Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/squares/membership/JoinButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    .line 96
    iget-object v0, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    sget-object v1, Lkye;->a:Llct;

    iget v1, v1, Llct;->k:I

    sget-object v2, Lkye;->a:Llct;

    iget v2, v2, Llct;->k:I

    invoke-virtual {v0, v1, v4, v2, v4}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setPadding(IIII)V

    .line 97
    iget-object v0, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    const/16 v1, 0x19

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 98
    iget-object v0, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a(Z)V

    .line 100
    new-instance v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 101
    iget-object v0, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 102
    iget-object v0, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 103
    iget-object v0, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(F)V

    .line 104
    iget-object v0, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->l(Z)V

    .line 106
    invoke-virtual {p0, v4}, Lkye;->setWillNotDraw(Z)V

    .line 107
    invoke-virtual {p0, v3}, Lkye;->setFocusable(Z)V

    .line 108
    invoke-virtual {p0, v3}, Lkye;->setClickable(Z)V

    .line 109
    invoke-virtual {p0, p0}, Lkye;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 5

    .prologue
    const/16 v2, 0xb

    .line 221
    sget-object v0, Lkye;->a:Llct;

    if-nez v0, :cond_0

    .line 222
    invoke-static {p0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lkye;->a:Llct;

    .line 225
    :cond_0
    const/16 v0, 0x15

    invoke-static {p0, v0}, Llib;->a(Landroid/content/Context;I)I

    move-result v0

    .line 227
    invoke-static {p0, v2}, Llib;->a(Landroid/content/Context;I)I

    move-result v1

    .line 229
    invoke-static {p0, v2}, Llib;->a(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->as:I

    mul-int/2addr v2, v3

    .line 232
    const/16 v3, 0x1a

    invoke-static {p0, v3}, Llib;->a(Landroid/content/Context;I)I

    move-result v3

    .line 234
    sget-object v4, Lkye;->a:Llct;

    iget v4, v4, Llct;->l:I

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    sget-object v1, Lkye;->a:Llct;

    iget v1, v1, Llct;->l:I

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    sget-object v1, Lkye;->a:Llct;

    iget v1, v1, Llct;->l:I

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    sget-object v1, Lkye;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 318
    invoke-virtual {p0}, Lkye;->removeAllViews()V

    .line 320
    invoke-virtual {p0}, Lkye;->clearAnimation()V

    .line 321
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    .line 323
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lkye;->setAlpha(F)V

    .line 326
    :cond_0
    iput-object v1, p0, Lkye;->b:Llci;

    .line 327
    iput-object v1, p0, Lkye;->l:Llbz;

    .line 328
    iput v2, p0, Lkye;->n:I

    .line 330
    iput-object v1, p0, Lkye;->c:Landroid/text/StaticLayout;

    .line 331
    iput-object v1, p0, Lkye;->d:Landroid/text/StaticLayout;

    .line 332
    iput-object v1, p0, Lkye;->e:Landroid/text/StaticLayout;

    .line 333
    iput v2, p0, Lkye;->g:I

    .line 334
    iput v2, p0, Lkye;->h:I

    .line 336
    iget-object v0, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 337
    iput v2, p0, Lkye;->k:I

    .line 338
    return-void
.end method

.method public a(Ljava/lang/String;Llci;Llbz;I)V
    .locals 1

    .prologue
    .line 114
    iput-object p1, p0, Lkye;->m:Ljava/lang/String;

    .line 115
    iput-object p3, p0, Lkye;->l:Llbz;

    .line 116
    iput p4, p0, Lkye;->n:I

    .line 117
    iput-object p2, p0, Lkye;->b:Llci;

    .line 118
    invoke-virtual {p0}, Lkye;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lkye;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lkye;->f:I

    .line 120
    iget-object v0, p0, Lkye;->l:Llbz;

    invoke-virtual {v0}, Llbz;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    sget-object v0, Lkye;->a:Llct;

    iget-object v0, v0, Llct;->ar:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lkye;->i:Landroid/graphics/Bitmap;

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    sget-object v0, Lkye;->a:Llct;

    iget-object v0, v0, Llct;->aq:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lkye;->i:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 383
    new-instance v0, Lkqw;

    sget-object v1, Lomv;->t:Lhmn;

    iget-object v2, p0, Lkye;->l:Llbz;

    invoke-virtual {v2}, Llbz;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lkye;->l:Llbz;

    invoke-virtual {v0}, Llbz;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lkye;->l:Llbz;

    invoke-virtual {v0}, Llbz;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 362
    iget v0, p0, Lkye;->n:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 342
    invoke-virtual {p0}, Lkye;->invalidate()V

    .line 343
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 344
    return-void
.end method

.method public e()V
    .locals 5

    .prologue
    .line 367
    iget-object v0, p0, Lkye;->b:Llci;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    .line 368
    invoke-virtual {v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->c()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    .line 369
    invoke-virtual {v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->c()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 371
    :cond_0
    iget-object v0, p0, Lkye;->b:Llci;

    iget-object v1, p0, Lkye;->l:Llbz;

    invoke-virtual {v1}, Llbz;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lkye;->l:Llbz;

    .line 372
    invoke-virtual {v2}, Llbz;->g()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lkye;->n:I

    iget-object v4, p0, Lkye;->m:Ljava/lang/String;

    .line 371
    invoke-interface {v0, v1, v2, v3, v4}, Llci;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 374
    :cond_1
    return-void
.end method

.method public f()Llja;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lkye;->l:Llbz;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 5
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 211
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 212
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lkye;->l:Llbz;

    invoke-virtual {v2}, Llbz;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 213
    iget-object v1, p0, Lkye;->d:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    .line 214
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lkye;->d:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 216
    :cond_0
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lkye;->l:Llbz;

    invoke-virtual {v2}, Llbz;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 217
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 348
    iget-object v0, p0, Lkye;->b:Llci;

    if-eqz v0, :cond_0

    .line 349
    iget-object v1, p0, Lkye;->b:Llci;

    iget-object v0, p0, Lkye;->l:Llbz;

    invoke-virtual {v0}, Llbz;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lkye;->l:Llbz;

    invoke-virtual {v0}, Llbz;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lkye;->l:Llbz;

    .line 350
    invoke-virtual {v3}, Llbz;->g()Ljava/lang/String;

    move-result-object v3

    .line 349
    invoke-interface {v1, v2, v0, v3}, Llci;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 352
    :cond_0
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lhly;->a(Landroid/view/View;I)V

    .line 353
    return-void

    .line 349
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 244
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 246
    iget v0, p0, Lkye;->k:I

    sget-object v1, Lkye;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 247
    iget v0, p0, Lkye;->g:I

    .line 248
    iget-object v2, p0, Lkye;->c:Landroid/text/StaticLayout;

    if-eqz v2, :cond_0

    .line 249
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 250
    iget-object v2, p0, Lkye;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 251
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 252
    iget-object v2, p0, Lkye;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->l:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 255
    :cond_0
    iget-object v2, p0, Lkye;->d:Landroid/text/StaticLayout;

    if-eqz v2, :cond_1

    .line 256
    iget-object v2, p0, Lkye;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iget-object v3, p0, Lkye;->d:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 259
    iget-object v3, p0, Lkye;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int v3, v2, v3

    div-int/lit8 v3, v3, 0x2

    .line 260
    iget-object v4, p0, Lkye;->i:Landroid/graphics/Bitmap;

    int-to-float v5, v1

    add-int/2addr v3, v0

    int-to-float v3, v3

    const/4 v6, 0x0

    invoke-virtual {p1, v4, v5, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 261
    iget-object v3, p0, Lkye;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sget-object v4, Lkye;->a:Llct;

    iget v4, v4, Llct;->k:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 263
    iget-object v3, p0, Lkye;->d:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 265
    int-to-float v3, v1

    add-int v4, v0, v2

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 266
    iget-object v3, p0, Lkye;->d:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 267
    neg-int v3, v1

    int-to-float v3, v3

    add-int/2addr v2, v0

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 269
    iget-object v2, p0, Lkye;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->k:I

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 270
    iget-object v2, p0, Lkye;->d:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->l:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 273
    :cond_1
    iget-object v2, p0, Lkye;->e:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    .line 274
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 275
    iget-object v2, p0, Lkye;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 276
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 277
    iget-object v0, p0, Lkye;->e:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    sget-object v0, Lkye;->a:Llct;

    iget v0, v0, Llct;->l:I

    .line 280
    :cond_2
    invoke-virtual {p0}, Lkye;->getWidth()I

    move-result v6

    .line 281
    invoke-virtual {p0}, Lkye;->getHeight()I

    move-result v7

    .line 283
    const/4 v1, 0x0

    int-to-float v2, v7

    int-to-float v3, v6

    int-to-float v4, v7

    sget-object v0, Lkye;->a:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 285
    invoke-virtual {p0}, Lkye;->isPressed()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lkye;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 286
    :cond_3
    sget-object v0, Lkye;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8, v8, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 287
    sget-object v0, Lkye;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 289
    :cond_4
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 293
    iget-object v0, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 294
    iget-object v0, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    sget-object v1, Lkye;->a:Llct;

    iget v1, v1, Llct;->m:I

    iget v2, p0, Lkye;->h:I

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->m:I

    iget v4, p0, Lkye;->k:I

    add-int/2addr v3, v4

    iget v4, p0, Lkye;->h:I

    iget v5, p0, Lkye;->k:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 298
    :cond_0
    iget-object v0, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_4

    .line 299
    sget-object v0, Lkye;->a:Llct;

    iget v0, v0, Llct;->m:I

    shl-int/lit8 v0, v0, 0x1

    iget v1, p0, Lkye;->k:I

    add-int/2addr v1, v0

    .line 300
    iget v0, p0, Lkye;->g:I

    .line 301
    iget-object v2, p0, Lkye;->c:Landroid/text/StaticLayout;

    if-eqz v2, :cond_1

    .line 302
    iget-object v2, p0, Lkye;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->l:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 304
    :cond_1
    iget-object v2, p0, Lkye;->d:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    .line 305
    iget-object v2, p0, Lkye;->d:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->l:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 307
    :cond_2
    iget-object v2, p0, Lkye;->e:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    .line 308
    iget-object v2, p0, Lkye;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->l:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 310
    :cond_3
    iget-object v2, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    iget-object v3, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    .line 311
    invoke-virtual {v4}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 310
    invoke-virtual {v2, v1, v0, v3, v4}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->layout(IIII)V

    .line 313
    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 137
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 138
    sget-object v0, Lkye;->a:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v5, v0

    .line 139
    div-int/lit8 v2, v0, 0x4

    iget v4, p0, Lkye;->f:I

    sget-object v6, Lkye;->a:Llct;

    iget v6, v6, Llct;->m:I

    mul-int/lit8 v6, v6, 0x2

    sub-int/2addr v4, v6

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lkye;->k:I

    .line 142
    iget-object v2, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0, v2}, Lkye;->removeView(Landroid/view/View;)V

    .line 143
    iget-object v2, p0, Lkye;->l:Llbz;

    invoke-virtual {v2}, Llbz;->d()Ljava/lang/String;

    move-result-object v2

    .line 144
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 145
    iget-object v4, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget v6, p0, Lkye;->k:I

    iget v7, p0, Lkye;->k:I

    invoke-virtual {v4, v6, v7}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(II)V

    .line 146
    iget-object v4, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0}, Lkye;->getContext()Landroid/content/Context;

    move-result-object v6

    sget-object v7, Ljac;->a:Ljac;

    invoke-static {v6, v2, v7}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 147
    iget-object v2, p0, Lkye;->j:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0, v2}, Lkye;->addView(Landroid/view/View;)V

    .line 150
    :cond_0
    invoke-virtual {p0}, Lkye;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 151
    invoke-virtual {p0}, Lkye;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 154
    iget v2, p0, Lkye;->k:I

    sub-int/2addr v0, v2

    sget-object v2, Lkye;->a:Llct;

    iget v2, v2, Llct;->m:I

    sub-int v7, v0, v2

    .line 155
    iget-object v0, p0, Lkye;->l:Llbz;

    invoke-virtual {v0}, Llbz;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 156
    const/16 v0, 0x15

    .line 157
    invoke-static {v4, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iget-object v2, p0, Lkye;->l:Llbz;

    .line 158
    invoke-virtual {v2}, Llbz;->b()Ljava/lang/String;

    move-result-object v2

    .line 156
    invoke-static {v0, v2, v7, v3}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lkye;->c:Landroid/text/StaticLayout;

    .line 159
    iget-object v0, p0, Lkye;->c:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v2, Lkye;->a:Llct;

    iget v2, v2, Llct;->l:I

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 162
    :goto_0
    iget-object v2, p0, Lkye;->l:Llbz;

    invoke-virtual {v2}, Llbz;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    .line 165
    :goto_1
    const/16 v8, 0xb

    invoke-static {v4, v8}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v8

    .line 167
    if-eqz v2, :cond_4

    .line 168
    const v4, 0x7f0a0485

    new-array v9, v3, [Ljava/lang/Object;

    iget-object v10, p0, Lkye;->l:Llbz;

    .line 169
    invoke-virtual {v10}, Llbz;->f()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v1

    .line 168
    invoke-virtual {v6, v4, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 176
    :goto_2
    iget-object v6, p0, Lkye;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int v6, v7, v6

    sget-object v9, Lkye;->a:Llct;

    iget v9, v9, Llct;->k:I

    sub-int/2addr v6, v9

    .line 178
    invoke-static {v8, v4, v6, v3}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v3

    iput-object v3, p0, Lkye;->d:Landroid/text/StaticLayout;

    .line 180
    iget-object v3, p0, Lkye;->d:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget-object v4, Lkye;->a:Llct;

    iget v4, v4, Llct;->l:I

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 182
    if-nez v2, :cond_1

    iget-object v2, p0, Lkye;->l:Llbz;

    invoke-virtual {v2}, Llbz;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 183
    iget-object v2, p0, Lkye;->l:Llbz;

    .line 184
    invoke-virtual {v2}, Llbz;->c()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->as:I

    .line 183
    invoke-static {v8, v2, v7, v3}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lkye;->e:Landroid/text/StaticLayout;

    .line 186
    iget-object v2, p0, Lkye;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lkye;->a:Llct;

    iget v3, v3, Llct;->l:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 189
    :cond_1
    iget-object v2, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_2

    .line 190
    iget-object v2, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {p0, v2}, Lkye;->addView(Landroid/view/View;)V

    .line 192
    :cond_2
    iget-object v2, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    iget-object v3, p0, Lkye;->l:Llbz;

    invoke-virtual {v3}, Llbz;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lkye;->l:Llbz;

    .line 193
    invoke-virtual {v4}, Llbz;->j()I

    move-result v4

    iget-object v6, p0, Lkye;->l:Llbz;

    invoke-virtual {v6}, Llbz;->i()I

    move-result v6

    .line 192
    invoke-static {v4, v6}, Lkvu;->a(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a(Ljava/lang/String;I)V

    .line 194
    iget-object v2, p0, Lkye;->p:Lkvq;

    iget-object v3, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v2, v3}, Lkvq;->a(Lkvv;)V

    .line 195
    iget-object v2, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    const/high16 v3, -0x80000000

    invoke-static {v7, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 196
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 195
    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->measure(II)V

    .line 197
    iget-object v1, p0, Lkye;->o:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    iget v1, p0, Lkye;->f:I

    invoke-virtual {p0, v5, v1}, Lkye;->setMeasuredDimension(II)V

    .line 201
    iget v1, p0, Lkye;->f:I

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lkye;->g:I

    .line 202
    iget v1, p0, Lkye;->k:I

    if-lt v0, v1, :cond_5

    iget v0, p0, Lkye;->g:I

    :goto_3
    iput v0, p0, Lkye;->h:I

    .line 205
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 206
    return-void

    :cond_3
    move v2, v1

    .line 162
    goto/16 :goto_1

    .line 171
    :cond_4
    iget-object v4, p0, Lkye;->l:Llbz;

    invoke-virtual {v4}, Llbz;->e()I

    move-result v4

    .line 172
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v9

    int-to-long v10, v4

    invoke-virtual {v9, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    .line 173
    const v10, 0x7f110027

    new-array v11, v3, [Ljava/lang/Object;

    aput-object v9, v11, v1

    invoke-virtual {v6, v10, v4, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 202
    :cond_5
    iget v0, p0, Lkye;->f:I

    iget v1, p0, Lkye;->k:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    goto :goto_3

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method
