.class public final Lfvv;
.super Lgbz;
.source "PG"

# interfaces
.implements Llcs;
.implements Llil;


# instance fields
.field private A:Lkzr;

.field private B:Lkzv;

.field private C:Licj;

.field private D:I

.field private E:Z

.field private F:Landroid/widget/TextView;

.field private G:Z

.field private H:Llim;

.field private y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfvv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfvv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lfvv;->D:I

    .line 72
    new-instance v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    .line 73
    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lfvv;->H:Llim;

    if-eqz v0, :cond_0

    .line 413
    invoke-static {p1}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    .line 414
    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    .line 415
    iget-object v0, p0, Lfvv;->H:Llim;

    invoke-interface {v0}, Llim;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0b3f

    .line 419
    :goto_0
    iget-object v1, p0, Lfvv;->F:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    :cond_0
    return-void

    .line 415
    :cond_1
    const v0, 0x7f0a0b40

    goto :goto_0
.end method


# virtual methods
.method protected a(III)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 240
    iput p2, p0, Lfvv;->D:I

    .line 242
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b()I

    move-result v0

    .line 243
    iget-object v1, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-static {p3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 244
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 243
    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->measure(II)V

    .line 246
    add-int/2addr v0, p2

    .line 247
    iget-object v1, p0, Lfvv;->F:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lfvv;->F:Landroid/widget/TextView;

    invoke-static {p3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 249
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 248
    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->measure(II)V

    .line 250
    iget-object v1, p0, Lfvv;->F:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    :cond_0
    return v0
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b()I

    move-result v0

    add-int/2addr v0, p2

    .line 258
    iget-object v1, p0, Lfvv;->F:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 265
    iget-object v1, p0, Lfvv;->F:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 267
    :cond_0
    return v0
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-super {p0}, Lgbz;->a()V

    .line 97
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 98
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a()V

    .line 99
    iput-object v1, p0, Lfvv;->A:Lkzr;

    .line 100
    iput-object v1, p0, Lfvv;->B:Lkzv;

    .line 101
    iput-object v1, p0, Lfvv;->C:Licj;

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lfvv;->D:I

    .line 103
    iput-boolean v2, p0, Lfvv;->E:Z

    .line 104
    iput-object v1, p0, Lfvv;->F:Landroid/widget/TextView;

    .line 106
    iput-boolean v2, p0, Lfvv;->G:Z

    .line 107
    iput-object v1, p0, Lfvv;->H:Llim;

    .line 108
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 117
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 118
    const/16 v2, 0x17

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 119
    const-wide/16 v4, 0x40

    and-long/2addr v4, v0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 120
    invoke-static {v2}, Lkzr;->a([B)Lkzr;

    move-result-object v0

    iput-object v0, p0, Lfvv;->A:Lkzr;

    .line 126
    :goto_0
    return-void

    .line 121
    :cond_0
    const-wide/32 v4, 0x20000

    and-long/2addr v0, v4

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 122
    invoke-static {v2}, Licj;->a([B)Licj;

    move-result-object v0

    iput-object v0, p0, Lfvv;->C:Licj;

    goto :goto_0

    .line 124
    :cond_1
    invoke-static {v2}, Lkzv;->a([B)Lkzv;

    move-result-object v0

    iput-object v0, p0, Lfvv;->B:Lkzv;

    goto :goto_0
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 8

    .prologue
    const v7, 0x7f110028

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 219
    iget-object v0, p0, Lfvv;->A:Lkzr;

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Lfvv;->A:Lkzr;

    invoke-virtual {v0}, Lkzr;->a()I

    move-result v0

    .line 221
    new-array v1, v5, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lfvv;->A:Lkzr;

    invoke-virtual {v2}, Lkzr;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {p1, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 222
    new-array v1, v5, [Ljava/lang/CharSequence;

    .line 223
    invoke-virtual {p0}, Lfvv;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v7, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    .line 222
    invoke-static {p1, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    iget-object v0, p0, Lfvv;->C:Licj;

    if-eqz v0, :cond_2

    .line 225
    new-array v0, v5, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lfvv;->C:Licj;

    invoke-virtual {v1}, Licj;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 226
    :cond_2
    iget-object v0, p0, Lfvv;->B:Lkzv;

    if-eqz v0, :cond_0

    .line 227
    new-array v0, v5, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lfvv;->B:Lkzv;

    invoke-virtual {v1}, Lkzv;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lfvv;->B:Lkzv;

    invoke-virtual {v0}, Lkzv;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    new-array v0, v5, [Ljava/lang/CharSequence;

    .line 230
    invoke-virtual {p0}, Lfvv;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110029

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 229
    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 232
    :cond_3
    new-array v0, v5, [Ljava/lang/CharSequence;

    .line 233
    invoke-virtual {p0}, Lfvv;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1, v7, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 232
    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 1

    .prologue
    .line 272
    invoke-super {p0, p1}, Lgbz;->a(Z)V

    .line 273
    invoke-virtual {p0}, Lfvv;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {p0, p1}, Lfvv;->m_(Z)V

    .line 276
    :cond_0
    return-void
.end method

.method protected aD_()Z
    .locals 2

    .prologue
    .line 112
    iget v0, p0, Lfvv;->w:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aE_()V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lfvv;->F:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 391
    invoke-virtual {p0}, Lfvv;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lfvv;->b(Landroid/content/Context;)V

    .line 393
    :cond_0
    return-void
.end method

.method public aF_()V
    .locals 0

    .prologue
    .line 397
    return-void
.end method

.method protected a_(Landroid/database/Cursor;Llcr;I)V
    .locals 12

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 131
    iget-object v0, p0, Lfvv;->A:Lkzr;

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lfvv;->A:Lkzr;

    invoke-virtual {v0}, Lkzr;->a()I

    move-result v2

    .line 133
    const/high16 v1, -0x80000000

    move v3, v10

    .line 134
    :goto_0
    if-ge v3, v2, :cond_0

    .line 135
    iget-object v0, p0, Lfvv;->A:Lkzr;

    invoke-virtual {v0, v3}, Lkzr;->a(I)Lkzv;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lkzv;->j()S

    move-result v0

    .line 137
    if-le v0, v1, :cond_b

    .line 134
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 151
    :goto_2
    iget v2, p2, Llcr;->c:I

    iget v3, p2, Llcr;->b:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 152
    if-le v1, v3, :cond_3

    int-to-float v2, v3

    mul-float/2addr v2, v4

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 156
    :goto_3
    iget-object v2, p0, Lfvv;->A:Lkzr;

    if-eqz v2, :cond_6

    move v4, v10

    move v2, v10

    .line 158
    :goto_4
    if-ge v4, v0, :cond_4

    .line 159
    iget-object v5, p0, Lfvv;->A:Lkzr;

    invoke-virtual {v5, v4}, Lkzr;->a(I)Lkzv;

    move-result-object v5

    .line 160
    invoke-virtual {v5}, Lkzv;->i()S

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v1

    float-to-int v5, v5

    .line 161
    add-int/2addr v5, v2

    .line 158
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v5

    goto :goto_4

    .line 141
    :cond_1
    iget-object v0, p0, Lfvv;->C:Licj;

    if-eqz v0, :cond_2

    .line 142
    iget v0, p2, Llcr;->e:I

    int-to-float v0, v0

    const v1, 0x3fb9999a    # 1.45f

    div-float/2addr v0, v1

    float-to-int v1, v0

    move v0, v9

    .line 144
    goto :goto_2

    .line 147
    :cond_2
    iget-object v0, p0, Lfvv;->B:Lkzv;

    invoke-virtual {v0}, Lkzv;->j()S

    move-result v1

    move v0, v9

    goto :goto_2

    :cond_3
    move v1, v4

    .line 152
    goto :goto_3

    :cond_4
    move v1, v2

    .line 169
    :goto_5
    if-le v0, v9, :cond_a

    .line 170
    int-to-float v0, v1

    const v1, 0x3f666666    # 0.9f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 173
    :goto_6
    invoke-virtual {p2, v0}, Llcr;->b(I)I

    move-result v0

    iput v0, p0, Lfvv;->p:I

    .line 174
    iget v0, p0, Lfvv;->p:I

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lfvv;->o:I

    .line 176
    iget v0, p0, Lfvv;->o:I

    invoke-virtual {p0, p2, v0}, Lfvv;->a(Lhuk;I)I

    move-result v2

    .line 178
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {p0, v0}, Lfvv;->removeView(Landroid/view/View;)V

    .line 179
    invoke-virtual {p0}, Lfvv;->getContext()Landroid/content/Context;

    move-result-object v11

    .line 180
    const-class v0, Lieh;

    invoke-static {v11, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 181
    sget-object v1, Ldxd;->a:Lief;

    .line 182
    invoke-virtual {p0}, Lfvv;->q()I

    move-result v4

    .line 181
    invoke-interface {v0, v1, v4}, Lieh;->b(Lief;I)Z

    move-result v6

    .line 184
    iget-object v0, p0, Lfvv;->A:Lkzr;

    if-eqz v0, :cond_8

    .line 185
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v1, p0, Lfvv;->A:Lkzr;

    iget v4, p0, Lfvv;->o:I

    iget-boolean v5, p0, Lfvv;->E:Z

    .line 186
    invoke-virtual {p0}, Lfvv;->t()Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, p0, Lfvv;->k:Z

    .line 185
    invoke-virtual/range {v0 .. v8}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(Lkzr;IIIZZLjava/lang/String;Z)V

    .line 195
    :goto_7
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {p0, v0}, Lfvv;->addView(Landroid/view/View;)V

    .line 197
    iget v0, p0, Lfvv;->w:I

    if-nez v0, :cond_5

    .line 198
    const-class v0, Llim;

    .line 199
    invoke-static {v11, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llim;

    iput-object v0, p0, Lfvv;->H:Llim;

    .line 200
    iget-object v0, p0, Lfvv;->H:Llim;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfvv;->H:Llim;

    .line 201
    invoke-interface {v0}, Llim;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 202
    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v11}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v2, 0x24

    invoke-static {v11, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    const v2, 0x7f0b0338

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    const v2, 0x7f0d038e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    const v0, 0x7f020338

    invoke-virtual {v1, v10, v10, v0, v10}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iput-object v1, p0, Lfvv;->F:Landroid/widget/TextView;

    .line 203
    invoke-direct {p0, v11}, Lfvv;->b(Landroid/content/Context;)V

    .line 205
    iget-object v0, p0, Lfvv;->F:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v0, p0, Lfvv;->F:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfvv;->addView(Landroid/view/View;)V

    .line 208
    iget-boolean v0, p0, Lfvv;->G:Z

    if-nez v0, :cond_5

    .line 209
    iput-boolean v9, p0, Lfvv;->G:Z

    .line 210
    const-class v0, Lhms;

    invoke-static {v11, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    invoke-direct {v1, v11}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->as:Lhmv;

    .line 211
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 210
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 215
    :cond_5
    return-void

    .line 163
    :cond_6
    iget-object v2, p0, Lfvv;->C:Licj;

    if-eqz v2, :cond_7

    .line 164
    iget v1, p2, Llcr;->e:I

    goto/16 :goto_5

    .line 166
    :cond_7
    iget-object v2, p0, Lfvv;->B:Lkzv;

    invoke-virtual {v2}, Lkzv;->i()S

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    goto/16 :goto_5

    .line 187
    :cond_8
    iget-object v0, p0, Lfvv;->C:Licj;

    if-eqz v0, :cond_9

    .line 189
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v1, p0, Lfvv;->C:Licj;

    iget-boolean v4, p0, Lfvv;->E:Z

    .line 190
    invoke-virtual {p0}, Lfvv;->t()Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, Lfvv;->k:Z

    .line 189
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(Licj;IIZLjava/lang/String;Z)V

    goto/16 :goto_7

    .line 192
    :cond_9
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v1, p0, Lfvv;->B:Lkzv;

    iget v4, p0, Lfvv;->o:I

    iget-boolean v5, p0, Lfvv;->E:Z

    .line 193
    invoke-virtual {p0}, Lfvv;->t()Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, p0, Lfvv;->k:Z

    .line 192
    invoke-virtual/range {v0 .. v8}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(Lkzv;IIIZZLjava/lang/String;Z)V

    goto/16 :goto_7

    :cond_a
    move v0, v1

    goto/16 :goto_6

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method protected e()Z
    .locals 2

    .prologue
    .line 282
    sget-object v0, Lfvc;->f:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfvv;->z:Llct;

    iget v0, v0, Llct;->bb:I

    const/16 v1, 0x40

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected m_(Z)V
    .locals 2

    .prologue
    .line 287
    iget-boolean v0, p0, Lfvv;->E:Z

    if-eq p1, v0, :cond_0

    .line 288
    iput-boolean p1, p0, Lfvv;->E:Z

    .line 289
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-boolean v1, p0, Lfvv;->E:Z

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(Z)V

    .line 293
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 377
    iget-object v0, p0, Lfvv;->F:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfvv;->F:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 378
    invoke-virtual {p0}, Lfvv;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 379
    const-class v0, Lhee;

    .line 380
    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 379
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/settings/StreamSettingsActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 381
    const-class v0, Lhms;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, v1}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v1, Lhmv;->at:Lhmv;

    .line 382
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 381
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 386
    :goto_0
    return-void

    .line 384
    :cond_0
    invoke-super {p0, p1}, Lgbz;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 77
    invoke-super/range {p0 .. p5}, Lgbz;->onLayout(ZIIII)V

    .line 78
    iget v0, p0, Lfvv;->m:I

    iget-object v1, p0, Lfvv;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lfvv;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    .line 79
    iget v1, p0, Lfvv;->D:I

    iget-object v2, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b()I

    move-result v2

    add-int/2addr v1, v2

    .line 80
    iget v2, p0, Lfvv;->D:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget v2, p0, Lfvv;->D:I

    if-eq v2, v1, :cond_0

    .line 81
    iget-object v2, p0, Lfvv;->y:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v3, p0, Lfvv;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lfvv;->D:I

    iget-object v5, p0, Lfvv;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v0

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->layout(IIII)V

    .line 84
    :cond_0
    iget-object v2, p0, Lfvv;->F:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 85
    iget-object v2, p0, Lfvv;->F:Landroid/widget/TextView;

    iget-object v3, p0, Lfvv;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lfvv;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v4

    iget-object v4, p0, Lfvv;->F:Landroid/widget/TextView;

    .line 86
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 85
    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 88
    :cond_1
    return-void
.end method
