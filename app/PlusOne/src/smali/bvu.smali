.class public final Lbvu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbvs;


# static fields
.field private static final a:Landroid/content/IntentFilter;

.field private static final b:Landroid/content/IntentFilter;


# instance fields
.field private final c:Landroid/content/BroadcastReceiver;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/os/PowerManager;

.field private final f:Lanh;

.field private final g:Ljfb;

.field private h:Ljava/lang/Runnable;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbvu;->a:Landroid/content/IntentFilter;

    .line 26
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 29
    sput-object v0, Lbvu;->b:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 30
    sget-object v0, Lbvu;->b:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 31
    sget-object v0, Lbvu;->b:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/PowerManager;Lanh;Ljfb;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lbvv;

    invoke-direct {v0, p0}, Lbvv;-><init>(Lbvu;)V

    iput-object v0, p0, Lbvu;->c:Landroid/content/BroadcastReceiver;

    .line 73
    const-string v0, "context"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbvu;->d:Landroid/content/Context;

    .line 74
    const-string v0, "powerManager"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lbvu;->e:Landroid/os/PowerManager;

    .line 75
    const-string v0, "gservicesSettings"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanh;

    iput-object v0, p0, Lbvu;->f:Lanh;

    .line 76
    const-string v0, "movieMakerProvider"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Lbvu;->g:Ljfb;

    .line 77
    return-void
.end method

.method static synthetic a(Lbvu;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lbvu;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f()Landroid/content/IntentFilter;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lbvu;->b:Landroid/content/IntentFilter;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 116
    const-string v0, "runnable"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lbvu;->h:Ljava/lang/Runnable;

    .line 117
    iget-boolean v0, p0, Lbvu;->i:Z

    if-nez v0, :cond_0

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbvu;->i:Z

    .line 119
    iget-object v0, p0, Lbvu;->d:Landroid/content/Context;

    iget-object v1, p0, Lbvu;->c:Landroid/content/BroadcastReceiver;

    sget-object v2, Lbvu;->b:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 121
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 81
    iget-object v1, p0, Lbvu;->d:Landroid/content/Context;

    const/4 v2, 0x0

    sget-object v3, Lbvu;->a:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 82
    const-string v2, "plugged"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 84
    :cond_0
    iget-object v1, p0, Lbvu;->d:Landroid/content/Context;

    .line 85
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "moviemaker:background_service_forced_power_disconnected_value"

    .line 84
    invoke-static {v1, v2, v0}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lbvu;->e:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 94
    iget-object v1, p0, Lbvu;->d:Landroid/content/Context;

    .line 95
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "moviemaker:background_service_forced_screen_on_value"

    .line 94
    invoke-static {v1, v2, v0}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 102
    iget-object v1, p0, Lbvu;->d:Landroid/content/Context;

    const/4 v2, 0x0

    sget-object v3, Lbvu;->a:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 103
    const-string v2, "level"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 104
    const-string v3, "scale"

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 105
    mul-int/lit8 v2, v2, 0x64

    div-int v1, v2, v1

    .line 106
    const/16 v2, 0xf

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    .line 108
    :cond_0
    iget-object v1, p0, Lbvu;->d:Landroid/content/Context;

    .line 109
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "moviemaker:background_service_forced_battery_low_value"

    .line 108
    invoke-static {v1, v2, v0}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 125
    iget-boolean v0, p0, Lbvu;->i:Z

    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbvu;->i:Z

    .line 127
    iget-object v0, p0, Lbvu;->d:Landroid/content/Context;

    iget-object v1, p0, Lbvu;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lbvu;->h:Ljava/lang/Runnable;

    .line 130
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lbvu;->g:Ljfb;

    invoke-virtual {v0}, Ljfb;->a()Z

    move-result v0

    .line 135
    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvu;->f:Lanh;

    invoke-virtual {v0}, Lanh;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbvu;->g:Ljfb;

    .line 136
    invoke-virtual {v0}, Ljfb;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
