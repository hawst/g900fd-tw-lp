.class public final Lnjl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnja;

.field public b:I

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3755
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3771
    const/4 v0, 0x0

    iput-object v0, p0, Lnjl;->a:Lnja;

    .line 3774
    const/high16 v0, -0x80000000

    iput v0, p0, Lnjl;->b:I

    .line 3755
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3796
    const/4 v0, 0x0

    .line 3797
    iget-object v1, p0, Lnjl;->a:Lnja;

    if-eqz v1, :cond_0

    .line 3798
    const/4 v0, 0x1

    iget-object v1, p0, Lnjl;->a:Lnja;

    .line 3799
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3801
    :cond_0
    iget v1, p0, Lnjl;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 3802
    const/4 v1, 0x2

    iget v2, p0, Lnjl;->b:I

    .line 3803
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3805
    :cond_1
    iget-object v1, p0, Lnjl;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3806
    const/4 v1, 0x3

    iget-object v2, p0, Lnjl;->c:Ljava/lang/Boolean;

    .line 3807
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3809
    :cond_2
    iget-object v1, p0, Lnjl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3810
    iput v0, p0, Lnjl;->ai:I

    .line 3811
    return v0
.end method

.method public a(Loxn;)Lnjl;
    .locals 2

    .prologue
    .line 3819
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3820
    sparse-switch v0, :sswitch_data_0

    .line 3824
    iget-object v1, p0, Lnjl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3825
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjl;->ah:Ljava/util/List;

    .line 3828
    :cond_1
    iget-object v1, p0, Lnjl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3830
    :sswitch_0
    return-object p0

    .line 3835
    :sswitch_1
    iget-object v0, p0, Lnjl;->a:Lnja;

    if-nez v0, :cond_2

    .line 3836
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnjl;->a:Lnja;

    .line 3838
    :cond_2
    iget-object v0, p0, Lnjl;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3842
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3843
    if-eqz v0, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    .line 3853
    :cond_3
    iput v0, p0, Lnjl;->b:I

    goto :goto_0

    .line 3855
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lnjl;->b:I

    goto :goto_0

    .line 3860
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnjl;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 3820
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3781
    iget-object v0, p0, Lnjl;->a:Lnja;

    if-eqz v0, :cond_0

    .line 3782
    const/4 v0, 0x1

    iget-object v1, p0, Lnjl;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3784
    :cond_0
    iget v0, p0, Lnjl;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 3785
    const/4 v0, 0x2

    iget v1, p0, Lnjl;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3787
    :cond_1
    iget-object v0, p0, Lnjl;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3788
    const/4 v0, 0x3

    iget-object v1, p0, Lnjl;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3790
    :cond_2
    iget-object v0, p0, Lnjl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3792
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3751
    invoke-virtual {p0, p1}, Lnjl;->a(Loxn;)Lnjl;

    move-result-object v0

    return-object v0
.end method
