.class public final Ljsm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lhzm;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljsm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljod;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Ljsn;

    invoke-direct {v0}, Ljsn;-><init>()V

    sput-object v0, Ljsm;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-class v0, Ljsm;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljod;

    iput-object v0, p0, Ljsm;->a:Ljod;

    .line 27
    return-void
.end method

.method constructor <init>(Ljso;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iget-object v0, p1, Ljso;->a:Ljod;

    iput-object v0, p0, Ljsm;->a:Ljod;

    .line 23
    return-void
.end method

.method public static a()Ljso;
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ljso;

    invoke-direct {v0}, Ljso;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    packed-switch p1, :pswitch_data_0

    .line 39
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 37
    :pswitch_0
    invoke-virtual {p0}, Ljsm;->b()Ljod;

    move-result-object v0

    invoke-interface {v0}, Ljod;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 35
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public b()Ljod;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljsm;->a:Ljod;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ljsm;->a:Ljod;

    invoke-interface {v0}, Ljod;->describeContents()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 50
    instance-of v0, p1, Ljsm;

    if-eqz v0, :cond_0

    .line 51
    check-cast p1, Ljsm;

    .line 52
    iget-object v0, p0, Ljsm;->a:Ljod;

    iget-object v1, p1, Ljsm;->a:Ljod;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ljsm;->a:Ljod;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Ljsm;->a:Ljod;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Ljsm;->a:Ljod;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 70
    return-void
.end method
