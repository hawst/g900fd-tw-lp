.class public final Lpfx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpfx;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Double;

.field private e:[Ljava/lang/String;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/Long;

.field private h:Ljava/lang/Integer;

.field private i:[Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    new-array v0, v0, [Lpfx;

    sput-object v0, Lpfx;->a:[Lpfx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Loxq;-><init>()V

    .line 84
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpfx;->e:[Ljava/lang/String;

    .line 93
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lpfx;->i:[Ljava/lang/Long;

    .line 75
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 130
    const/4 v0, 0x1

    iget-object v2, p0, Lpfx;->b:Ljava/lang/String;

    .line 132
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 133
    iget-object v2, p0, Lpfx;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 134
    const/4 v2, 0x2

    iget-object v3, p0, Lpfx;->c:Ljava/lang/Boolean;

    .line 135
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 137
    :cond_0
    iget-object v2, p0, Lpfx;->d:Ljava/lang/Double;

    if-eqz v2, :cond_1

    .line 138
    const/4 v2, 0x3

    iget-object v3, p0, Lpfx;->d:Ljava/lang/Double;

    .line 139
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 141
    :cond_1
    iget-object v2, p0, Lpfx;->e:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lpfx;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 143
    iget-object v4, p0, Lpfx;->e:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 145
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 143
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 147
    :cond_2
    add-int/2addr v0, v3

    .line 148
    iget-object v2, p0, Lpfx;->e:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 150
    :cond_3
    iget-object v2, p0, Lpfx;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 151
    const/4 v2, 0x5

    iget-object v3, p0, Lpfx;->h:Ljava/lang/Integer;

    .line 152
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 154
    :cond_4
    iget-object v2, p0, Lpfx;->f:Ljava/lang/Long;

    if-eqz v2, :cond_5

    .line 155
    const/4 v2, 0x6

    iget-object v3, p0, Lpfx;->f:Ljava/lang/Long;

    .line 156
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 158
    :cond_5
    iget-object v2, p0, Lpfx;->g:Ljava/lang/Long;

    if-eqz v2, :cond_6

    .line 159
    const/4 v2, 0x7

    iget-object v3, p0, Lpfx;->g:Ljava/lang/Long;

    .line 160
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 162
    :cond_6
    iget-object v2, p0, Lpfx;->i:[Ljava/lang/Long;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lpfx;->i:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 164
    iget-object v3, p0, Lpfx;->i:[Ljava/lang/Long;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_7

    aget-object v5, v3, v1

    .line 166
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 164
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 168
    :cond_7
    add-int/2addr v0, v2

    .line 169
    iget-object v1, p0, Lpfx;->i:[Ljava/lang/Long;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 171
    :cond_8
    iget-object v1, p0, Lpfx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    iput v0, p0, Lpfx;->ai:I

    .line 173
    return v0
.end method

.method public a(Loxn;)Lpfx;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 181
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 182
    sparse-switch v0, :sswitch_data_0

    .line 186
    iget-object v1, p0, Lpfx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 187
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpfx;->ah:Ljava/util/List;

    .line 190
    :cond_1
    iget-object v1, p0, Lpfx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    :sswitch_0
    return-object p0

    .line 197
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfx;->b:Ljava/lang/String;

    goto :goto_0

    .line 201
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpfx;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 205
    :sswitch_3
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lpfx;->d:Ljava/lang/Double;

    goto :goto_0

    .line 209
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 210
    iget-object v0, p0, Lpfx;->e:[Ljava/lang/String;

    array-length v0, v0

    .line 211
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 212
    iget-object v2, p0, Lpfx;->e:[Ljava/lang/String;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 213
    iput-object v1, p0, Lpfx;->e:[Ljava/lang/String;

    .line 214
    :goto_1
    iget-object v1, p0, Lpfx;->e:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 215
    iget-object v1, p0, Lpfx;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 216
    invoke-virtual {p1}, Loxn;->a()I

    .line 214
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 219
    :cond_2
    iget-object v1, p0, Lpfx;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 223
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpfx;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 227
    :sswitch_6
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpfx;->f:Ljava/lang/Long;

    goto :goto_0

    .line 231
    :sswitch_7
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpfx;->g:Ljava/lang/Long;

    goto/16 :goto_0

    .line 235
    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 236
    iget-object v0, p0, Lpfx;->i:[Ljava/lang/Long;

    array-length v0, v0

    .line 237
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    .line 238
    iget-object v2, p0, Lpfx;->i:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    iput-object v1, p0, Lpfx;->i:[Ljava/lang/Long;

    .line 240
    :goto_2
    iget-object v1, p0, Lpfx;->i:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 241
    iget-object v1, p0, Lpfx;->i:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 242
    invoke-virtual {p1}, Loxn;->a()I

    .line 240
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 245
    :cond_3
    iget-object v1, p0, Lpfx;->i:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 182
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 98
    const/4 v1, 0x1

    iget-object v2, p0, Lpfx;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 99
    iget-object v1, p0, Lpfx;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 100
    const/4 v1, 0x2

    iget-object v2, p0, Lpfx;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 102
    :cond_0
    iget-object v1, p0, Lpfx;->d:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 103
    const/4 v1, 0x3

    iget-object v2, p0, Lpfx;->d:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(ID)V

    .line 105
    :cond_1
    iget-object v1, p0, Lpfx;->e:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 106
    iget-object v2, p0, Lpfx;->e:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 107
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    :cond_2
    iget-object v1, p0, Lpfx;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 111
    const/4 v1, 0x5

    iget-object v2, p0, Lpfx;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 113
    :cond_3
    iget-object v1, p0, Lpfx;->f:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 114
    const/4 v1, 0x6

    iget-object v2, p0, Lpfx;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 116
    :cond_4
    iget-object v1, p0, Lpfx;->g:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 117
    const/4 v1, 0x7

    iget-object v2, p0, Lpfx;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 119
    :cond_5
    iget-object v1, p0, Lpfx;->i:[Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 120
    iget-object v1, p0, Lpfx;->i:[Ljava/lang/Long;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 121
    const/16 v4, 0x8

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v4, v6, v7}, Loxo;->b(IJ)V

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 124
    :cond_6
    iget-object v0, p0, Lpfx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 126
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lpfx;->a(Loxn;)Lpfx;

    move-result-object v0

    return-object v0
.end method
