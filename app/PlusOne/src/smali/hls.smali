.class public final Lhls;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lhls;


# instance fields
.field private final b:Z

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 140
    new-instance v0, Lhls;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhls;-><init>(ZLjava/lang/String;)V

    sput-object v0, Lhls;->a:Lhls;

    return-void
.end method

.method private constructor <init>(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-boolean p1, p0, Lhls;->b:Z

    .line 145
    iput-object p2, p0, Lhls;->c:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public static a(Ljava/lang/String;)Lhls;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 135
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "photoId must be non-empty."

    invoke-static {v0, v2}, Llsk;->a(ZLjava/lang/Object;)V

    .line 136
    new-instance v0, Lhls;

    invoke-direct {v0, v1, p0}, Lhls;-><init>(ZLjava/lang/String;)V

    return-object v0

    .line 135
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lhls;->b:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lhls;->c:Ljava/lang/String;

    return-object v0
.end method
