.class public final Lnfd;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnzx;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2055
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2058
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lnfd;->a:[Lnzx;

    .line 2055
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2086
    .line 2087
    iget-object v1, p0, Lnfd;->a:[Lnzx;

    if-eqz v1, :cond_1

    .line 2088
    iget-object v2, p0, Lnfd;->a:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2089
    if-eqz v4, :cond_0

    .line 2090
    const/4 v5, 0x1

    .line 2091
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2088
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2095
    :cond_1
    iget-object v1, p0, Lnfd;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2096
    const/4 v1, 0x2

    iget-object v2, p0, Lnfd;->b:Ljava/lang/String;

    .line 2097
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2099
    :cond_2
    iget-object v1, p0, Lnfd;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2100
    const/4 v1, 0x3

    iget-object v2, p0, Lnfd;->c:Ljava/lang/Boolean;

    .line 2101
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2103
    :cond_3
    iget-object v1, p0, Lnfd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2104
    iput v0, p0, Lnfd;->ai:I

    .line 2105
    return v0
.end method

.method public a(Loxn;)Lnfd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2113
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2114
    sparse-switch v0, :sswitch_data_0

    .line 2118
    iget-object v2, p0, Lnfd;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2119
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnfd;->ah:Ljava/util/List;

    .line 2122
    :cond_1
    iget-object v2, p0, Lnfd;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2124
    :sswitch_0
    return-object p0

    .line 2129
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2130
    iget-object v0, p0, Lnfd;->a:[Lnzx;

    if-nez v0, :cond_3

    move v0, v1

    .line 2131
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 2132
    iget-object v3, p0, Lnfd;->a:[Lnzx;

    if-eqz v3, :cond_2

    .line 2133
    iget-object v3, p0, Lnfd;->a:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2135
    :cond_2
    iput-object v2, p0, Lnfd;->a:[Lnzx;

    .line 2136
    :goto_2
    iget-object v2, p0, Lnfd;->a:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2137
    iget-object v2, p0, Lnfd;->a:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 2138
    iget-object v2, p0, Lnfd;->a:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2139
    invoke-virtual {p1}, Loxn;->a()I

    .line 2136
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2130
    :cond_3
    iget-object v0, p0, Lnfd;->a:[Lnzx;

    array-length v0, v0

    goto :goto_1

    .line 2142
    :cond_4
    iget-object v2, p0, Lnfd;->a:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 2143
    iget-object v2, p0, Lnfd;->a:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2147
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfd;->b:Ljava/lang/String;

    goto :goto_0

    .line 2151
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnfd;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 2114
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2067
    iget-object v0, p0, Lnfd;->a:[Lnzx;

    if-eqz v0, :cond_1

    .line 2068
    iget-object v1, p0, Lnfd;->a:[Lnzx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2069
    if-eqz v3, :cond_0

    .line 2070
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2068
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2074
    :cond_1
    iget-object v0, p0, Lnfd;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2075
    const/4 v0, 0x2

    iget-object v1, p0, Lnfd;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2077
    :cond_2
    iget-object v0, p0, Lnfd;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2078
    const/4 v0, 0x3

    iget-object v1, p0, Lnfd;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2080
    :cond_3
    iget-object v0, p0, Lnfd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2082
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2051
    invoke-virtual {p0, p1}, Lnfd;->a(Loxn;)Lnfd;

    move-result-object v0

    return-object v0
.end method
