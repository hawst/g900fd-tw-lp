.class public final Ldie;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lz;

.field private final b:I


# direct methods
.method public constructor <init>(Lz;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Ldie;->a:Lz;

    .line 24
    iput p2, p0, Ldie;->b:I

    .line 25
    return-void
.end method


# virtual methods
.method public a(Lu;)V
    .locals 7

    .prologue
    .line 34
    iget v3, p0, Ldie;->b:I

    const-string v4, "default"

    iget-object v0, p0, Ldie;->a:Lz;

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Lu;->k()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Lu;->f(Landroid/os/Bundle;)V

    iget-object v0, p0, Ldie;->a:Lz;

    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v1

    invoke-virtual {v1, v3, p1, v4}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lat;->a(I)Lat;

    invoke-virtual {v1}, Lat;->c()I

    invoke-virtual {v0}, Lae;->b()Z

    .line 35
    return-void

    .line 34
    :cond_1
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v5

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v2, v5}, Landroid/os/Bundle;-><init>(I)V

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    move-object v0, v2

    goto :goto_0
.end method
