.class public abstract Lhyj;
.super Lhyg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "Y:",
        "Ljava/lang/Object;",
        ">",
        "Lhyg;"
    }
.end annotation


# instance fields
.field public b:Landroid/content/Context;

.field c:Z

.field d:I

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TY;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "TY;>;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/Object;

.field private final h:Lhyl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lhyj",
            "<TT;TY;>.hyl;"
        }
    .end annotation
.end field

.field private i:Lhyo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lhyo",
            "<TT;TY;>;"
        }
    .end annotation
.end field

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lae;Lhyo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lae;",
            "Lhyo",
            "<TT;TY;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p2}, Lhyg;-><init>(Lae;)V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhyj;->e:Ljava/util/Map;

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhyj;->f:Ljava/util/HashMap;

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lhyj;->d:I

    .line 37
    new-instance v0, Lhyl;

    invoke-direct {v0, p0}, Lhyl;-><init>(Lhyj;)V

    iput-object v0, p0, Lhyj;->h:Lhyl;

    .line 50
    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-object p3, p0, Lhyj;->i:Lhyo;

    iput-boolean v0, p0, Lhyj;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhyj;->h:Lhyl;

    invoke-interface {p3, v0}, Lhyo;->a(Landroid/database/DataSetObserver;)V

    :cond_0
    iput-object p1, p0, Lhyj;->b:Landroid/content/Context;

    invoke-super {p0}, Lhyg;->d()V

    .line 51
    return-void

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(I)Z
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lhyj;->i:Lhyo;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lhyj;->i:Lhyo;

    invoke-interface {v0, p1}, Lhyo;->b(I)Z

    move-result v0

    .line 274
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    const/4 v1, -0x2

    .line 161
    iget-object v0, p0, Lhyj;->g:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lhyj;->f()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 171
    :goto_0
    return v0

    .line 165
    :cond_0
    iget-object v0, p0, Lhyj;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 166
    if-nez v0, :cond_1

    move v0, v1

    .line 167
    goto :goto_0

    .line 170
    :cond_1
    iget-object v2, p0, Lhyj;->e:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 171
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Lhyo;I)Lhyo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhyo",
            "<TT;TY;>;I)",
            "Lhyo",
            "<TT;TY;>;"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 190
    const-string v1, "EsListPagerAdapter"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    iget-object v1, p0, Lhyj;->i:Lhyo;

    if-nez v1, :cond_1

    move v1, v0

    :goto_0
    if-nez p1, :cond_2

    .line 192
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "swapCursor old="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; new="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 191
    :cond_0
    iget-object v0, p0, Lhyj;->i:Lhyo;

    if-ne p1, v0, :cond_3

    .line 196
    const/4 v0, 0x0

    .line 213
    :goto_2
    return-object v0

    .line 191
    :cond_1
    iget-object v1, p0, Lhyj;->i:Lhyo;

    invoke-interface {v1}, Lhyo;->b()I

    move-result v1

    goto :goto_0

    .line 192
    :cond_2
    invoke-interface {p1}, Lhyo;->b()I

    move-result v0

    goto :goto_1

    .line 198
    :cond_3
    iget-object v0, p0, Lhyj;->i:Lhyo;

    .line 199
    if-eqz v0, :cond_4

    .line 200
    iget-object v1, p0, Lhyj;->h:Lhyl;

    invoke-interface {v0, v1}, Lhyo;->b(Landroid/database/DataSetObserver;)V

    .line 203
    :cond_4
    iput-object p1, p0, Lhyj;->i:Lhyo;

    .line 204
    if-eqz p1, :cond_5

    invoke-interface {p1}, Lhyo;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 205
    iget-object v1, p0, Lhyj;->h:Lhyl;

    invoke-interface {p1, v1}, Lhyo;->a(Landroid/database/DataSetObserver;)V

    .line 206
    const/4 v1, 0x1

    iput-boolean v1, p0, Lhyj;->c:Z

    .line 211
    :goto_3
    iput p2, p0, Lhyj;->j:I

    .line 212
    invoke-virtual {p0}, Lhyj;->d()V

    goto :goto_2

    .line 208
    :cond_5
    const/4 v1, 0x0

    iput-boolean v1, p0, Lhyj;->c:Z

    goto :goto_3
.end method

.method public a(Landroid/view/View;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 111
    iget-boolean v0, p0, Lhyj;->c:Z

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this should only be called when the cursor is valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_0
    invoke-direct {p0, p2}, Lhyj;->e(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    iget-object v0, p0, Lhyj;->i:Lhyo;

    invoke-interface {v0, p2}, Lhyo;->c(I)Ljava/lang/Object;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lhyj;->e:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-super {p0, p1, p2}, Lhyg;->a(Landroid/view/View;I)Ljava/lang/Object;

    move-result-object v1

    .line 126
    if-eqz v1, :cond_1

    .line 127
    iget-object v2, p0, Lhyj;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    :cond_1
    return-object v1

    .line 119
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to instantiate item we can\'t retrieve from cursor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Lhyg;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .line 89
    invoke-virtual {p0}, Lhyj;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    iput-object v0, p0, Lhyj;->g:Ljava/lang/Object;

    .line 92
    :cond_0
    return-object v0
.end method

.method protected a(II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 263
    invoke-direct {p0, p2}, Lhyj;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lhyj;->i:Lhyo;

    invoke-interface {v0, p2}, Lhyo;->c(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "android:espager:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 266
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lhyg;->a(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lu;
    .locals 4

    .prologue
    .line 70
    invoke-virtual {p0}, Lhyj;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget p1, p0, Lhyj;->d:I

    .line 74
    :cond_0
    const/4 v0, 0x0

    .line 75
    iget-boolean v1, p0, Lhyj;->c:Z

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lhyj;->e(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    iget-object v0, p0, Lhyj;->b:Landroid/content/Context;

    iget-object v1, p0, Lhyj;->i:Lhyo;

    invoke-virtual {p0, v0, v1, p1}, Lhyj;->a(Landroid/content/Context;Lhyo;I)Lu;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Lu;->k()Landroid/os/Bundle;

    move-result-object v2

    .line 78
    const-string v0, "for_animation"

    .line 79
    invoke-virtual {p0}, Lhyj;->f()Z

    move-result v3

    .line 78
    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 81
    const-string v3, "never_show_slide_show"

    invoke-virtual {p0}, Lhyj;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    .line 83
    :cond_1
    return-object v0

    .line 81
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(Landroid/content/Context;Lhyo;I)Lu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lhyo",
            "<TT;TY;>;I)",
            "Lu;"
        }
    .end annotation
.end method

.method public a(Landroid/view/View;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lhyj;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    invoke-super {p0, p1, p2, p3}, Lhyg;->a(Landroid/view/View;ILjava/lang/Object;)V

    .line 152
    return-void
.end method

.method public a(Lhyi;)V
    .locals 1

    .prologue
    .line 134
    if-nez p1, :cond_0

    .line 135
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lhyg;->a(Lhyi;)V

    .line 145
    :goto_0
    return-void

    .line 137
    :cond_0
    new-instance v0, Lhyk;

    invoke-direct {v0, p0, p1}, Lhyk;-><init>(Lhyj;Lhyi;)V

    invoke-super {p0, v0}, Lhyg;->a(Lhyi;)V

    goto :goto_0
.end method

.method protected a(Lu;)Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lhyj;->g:Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 97
    iget-boolean v0, p0, Lhyj;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhyj;->i:Lhyo;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lhyj;->i:Lhyo;

    invoke-interface {v0}, Lhyo;->b()I

    move-result v0

    .line 99
    if-lez v0, :cond_0

    invoke-virtual {p0}, Lhyj;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    const/4 v0, 0x1

    .line 105
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 222
    iput p1, p0, Lhyj;->d:I

    .line 223
    invoke-virtual {p0}, Lhyj;->d()V

    .line 224
    return-void
.end method

.method public d()V
    .locals 5

    .prologue
    .line 255
    iget v0, p0, Lhyj;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 256
    iget v1, p0, Lhyj;->j:I

    iget-object v0, p0, Lhyj;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-boolean v0, p0, Lhyj;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhyj;->i:Lhyo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhyj;->i:Lhyo;

    invoke-interface {v0}, Lhyo;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 258
    :cond_0
    invoke-super {p0}, Lhyg;->d()V

    .line 259
    return-void

    .line 256
    :cond_1
    add-int/lit8 v0, v1, -0x19

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v1, v1, 0x19

    iget-object v2, p0, Lhyj;->i:Lhyo;

    invoke-interface {v2}, Lhyo;->b()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lhyj;->e:Ljava/util/Map;

    iget-object v3, p0, Lhyj;->i:Lhyo;

    invoke-interface {v3, v0}, Lhyo;->c(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 231
    iput p1, p0, Lhyj;->j:I

    .line 232
    return-void
.end method

.method f()Z
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lhyj;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lhyo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lhyo",
            "<TT;TY;>;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lhyj;->i:Lhyo;

    return-object v0
.end method
