.class public final Lnrp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnrp;


# instance fields
.field public b:Lohv;

.field public c:Ljava/lang/Integer;

.field public d:[Lohv;

.field public e:Ljava/lang/String;

.field public f:[Lpeo;

.field public g:I

.field public h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Double;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:[Ljava/lang/String;

.field private m:[Lnrp;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    new-array v0, v0, [Lnrp;

    sput-object v0, Lnrp;->a:[Lnrp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lnrp;->b:Lohv;

    .line 38
    sget-object v0, Lohv;->a:[Lohv;

    iput-object v0, p0, Lnrp;->d:[Lohv;

    .line 49
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnrp;->l:[Ljava/lang/String;

    .line 52
    sget-object v0, Lpeo;->a:[Lpeo;

    iput-object v0, p0, Lnrp;->f:[Lpeo;

    .line 55
    const/high16 v0, -0x80000000

    iput v0, p0, Lnrp;->g:I

    .line 58
    sget-object v0, Lnrp;->a:[Lnrp;

    iput-object v0, p0, Lnrp;->m:[Lnrp;

    .line 30
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 126
    .line 127
    iget-object v0, p0, Lnrp;->b:Lohv;

    if-eqz v0, :cond_10

    .line 128
    const/4 v0, 0x1

    iget-object v2, p0, Lnrp;->b:Lohv;

    .line 129
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 131
    :goto_0
    iget-object v2, p0, Lnrp;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 132
    const/4 v2, 0x2

    iget-object v3, p0, Lnrp;->c:Ljava/lang/Integer;

    .line 133
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 135
    :cond_0
    iget-object v2, p0, Lnrp;->d:[Lohv;

    if-eqz v2, :cond_2

    .line 136
    iget-object v3, p0, Lnrp;->d:[Lohv;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 137
    if-eqz v5, :cond_1

    .line 138
    const/4 v6, 0x3

    .line 139
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 136
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 143
    :cond_2
    iget-object v2, p0, Lnrp;->i:Ljava/lang/Double;

    if-eqz v2, :cond_3

    .line 144
    const/4 v2, 0x4

    iget-object v3, p0, Lnrp;->i:Ljava/lang/Double;

    .line 145
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 147
    :cond_3
    iget-object v2, p0, Lnrp;->e:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 148
    const/4 v2, 0x5

    iget-object v3, p0, Lnrp;->e:Ljava/lang/String;

    .line 149
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 151
    :cond_4
    iget-object v2, p0, Lnrp;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 152
    const/4 v2, 0x6

    iget-object v3, p0, Lnrp;->j:Ljava/lang/Integer;

    .line 153
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 155
    :cond_5
    iget-object v2, p0, Lnrp;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 156
    const/4 v2, 0x7

    iget-object v3, p0, Lnrp;->k:Ljava/lang/Integer;

    .line 157
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 159
    :cond_6
    iget v2, p0, Lnrp;->g:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_7

    .line 160
    const/16 v2, 0xa

    iget v3, p0, Lnrp;->g:I

    .line 161
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 163
    :cond_7
    iget-object v2, p0, Lnrp;->m:[Lnrp;

    if-eqz v2, :cond_9

    .line 164
    iget-object v3, p0, Lnrp;->m:[Lnrp;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    .line 165
    if-eqz v5, :cond_8

    .line 166
    const/16 v6, 0xb

    .line 167
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 164
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 171
    :cond_9
    iget-object v2, p0, Lnrp;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    .line 172
    const/16 v2, 0xc

    iget-object v3, p0, Lnrp;->h:Ljava/lang/Boolean;

    .line 173
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 175
    :cond_a
    iget-object v2, p0, Lnrp;->l:[Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lnrp;->l:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 177
    iget-object v4, p0, Lnrp;->l:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_3
    if-ge v2, v5, :cond_b

    aget-object v6, v4, v2

    .line 179
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 177
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 181
    :cond_b
    add-int/2addr v0, v3

    .line 182
    iget-object v2, p0, Lnrp;->l:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 184
    :cond_c
    iget-object v2, p0, Lnrp;->f:[Lpeo;

    if-eqz v2, :cond_e

    .line 185
    iget-object v2, p0, Lnrp;->f:[Lpeo;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_e

    aget-object v4, v2, v1

    .line 186
    if-eqz v4, :cond_d

    .line 187
    const/16 v5, 0xf

    .line 188
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 185
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 192
    :cond_e
    iget-object v1, p0, Lnrp;->n:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 193
    const/16 v1, 0x10

    iget-object v2, p0, Lnrp;->n:Ljava/lang/String;

    .line 194
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_f
    iget-object v1, p0, Lnrp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    iput v0, p0, Lnrp;->ai:I

    .line 198
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnrp;
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 206
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 207
    sparse-switch v0, :sswitch_data_0

    .line 211
    iget-object v2, p0, Lnrp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 212
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnrp;->ah:Ljava/util/List;

    .line 215
    :cond_1
    iget-object v2, p0, Lnrp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    :sswitch_0
    return-object p0

    .line 222
    :sswitch_1
    iget-object v0, p0, Lnrp;->b:Lohv;

    if-nez v0, :cond_2

    .line 223
    new-instance v0, Lohv;

    invoke-direct {v0}, Lohv;-><init>()V

    iput-object v0, p0, Lnrp;->b:Lohv;

    .line 225
    :cond_2
    iget-object v0, p0, Lnrp;->b:Lohv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 229
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnrp;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 233
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 234
    iget-object v0, p0, Lnrp;->d:[Lohv;

    if-nez v0, :cond_4

    move v0, v1

    .line 235
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohv;

    .line 236
    iget-object v3, p0, Lnrp;->d:[Lohv;

    if-eqz v3, :cond_3

    .line 237
    iget-object v3, p0, Lnrp;->d:[Lohv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    :cond_3
    iput-object v2, p0, Lnrp;->d:[Lohv;

    .line 240
    :goto_2
    iget-object v2, p0, Lnrp;->d:[Lohv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 241
    iget-object v2, p0, Lnrp;->d:[Lohv;

    new-instance v3, Lohv;

    invoke-direct {v3}, Lohv;-><init>()V

    aput-object v3, v2, v0

    .line 242
    iget-object v2, p0, Lnrp;->d:[Lohv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 243
    invoke-virtual {p1}, Loxn;->a()I

    .line 240
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 234
    :cond_4
    iget-object v0, p0, Lnrp;->d:[Lohv;

    array-length v0, v0

    goto :goto_1

    .line 246
    :cond_5
    iget-object v2, p0, Lnrp;->d:[Lohv;

    new-instance v3, Lohv;

    invoke-direct {v3}, Lohv;-><init>()V

    aput-object v3, v2, v0

    .line 247
    iget-object v2, p0, Lnrp;->d:[Lohv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 251
    :sswitch_4
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnrp;->i:Ljava/lang/Double;

    goto/16 :goto_0

    .line 255
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnrp;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 259
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnrp;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 263
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnrp;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 267
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 268
    if-eq v0, v4, :cond_6

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-eq v0, v2, :cond_6

    const/4 v2, 0x2

    if-eq v0, v2, :cond_6

    const/4 v2, 0x3

    if-eq v0, v2, :cond_6

    const/4 v2, 0x4

    if-eq v0, v2, :cond_6

    const/4 v2, 0x5

    if-eq v0, v2, :cond_6

    const/4 v2, 0x6

    if-eq v0, v2, :cond_6

    const/4 v2, 0x7

    if-eq v0, v2, :cond_6

    const/16 v2, 0x9

    if-eq v0, v2, :cond_6

    const/16 v2, 0x8

    if-eq v0, v2, :cond_6

    const/16 v2, 0xa

    if-eq v0, v2, :cond_6

    const/16 v2, 0xb

    if-eq v0, v2, :cond_6

    const/16 v2, 0xc

    if-eq v0, v2, :cond_6

    const/16 v2, 0xd

    if-eq v0, v2, :cond_6

    const/16 v2, 0xe

    if-eq v0, v2, :cond_6

    const/16 v2, 0xf

    if-ne v0, v2, :cond_7

    .line 285
    :cond_6
    iput v0, p0, Lnrp;->g:I

    goto/16 :goto_0

    .line 287
    :cond_7
    iput v4, p0, Lnrp;->g:I

    goto/16 :goto_0

    .line 292
    :sswitch_9
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 293
    iget-object v0, p0, Lnrp;->m:[Lnrp;

    if-nez v0, :cond_9

    move v0, v1

    .line 294
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnrp;

    .line 295
    iget-object v3, p0, Lnrp;->m:[Lnrp;

    if-eqz v3, :cond_8

    .line 296
    iget-object v3, p0, Lnrp;->m:[Lnrp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 298
    :cond_8
    iput-object v2, p0, Lnrp;->m:[Lnrp;

    .line 299
    :goto_4
    iget-object v2, p0, Lnrp;->m:[Lnrp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 300
    iget-object v2, p0, Lnrp;->m:[Lnrp;

    new-instance v3, Lnrp;

    invoke-direct {v3}, Lnrp;-><init>()V

    aput-object v3, v2, v0

    .line 301
    iget-object v2, p0, Lnrp;->m:[Lnrp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 302
    invoke-virtual {p1}, Loxn;->a()I

    .line 299
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 293
    :cond_9
    iget-object v0, p0, Lnrp;->m:[Lnrp;

    array-length v0, v0

    goto :goto_3

    .line 305
    :cond_a
    iget-object v2, p0, Lnrp;->m:[Lnrp;

    new-instance v3, Lnrp;

    invoke-direct {v3}, Lnrp;-><init>()V

    aput-object v3, v2, v0

    .line 306
    iget-object v2, p0, Lnrp;->m:[Lnrp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 310
    :sswitch_a
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnrp;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 314
    :sswitch_b
    const/16 v0, 0x72

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 315
    iget-object v0, p0, Lnrp;->l:[Ljava/lang/String;

    array-length v0, v0

    .line 316
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 317
    iget-object v3, p0, Lnrp;->l:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 318
    iput-object v2, p0, Lnrp;->l:[Ljava/lang/String;

    .line 319
    :goto_5
    iget-object v2, p0, Lnrp;->l:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 320
    iget-object v2, p0, Lnrp;->l:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 321
    invoke-virtual {p1}, Loxn;->a()I

    .line 319
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 324
    :cond_b
    iget-object v2, p0, Lnrp;->l:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 328
    :sswitch_c
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 329
    iget-object v0, p0, Lnrp;->f:[Lpeo;

    if-nez v0, :cond_d

    move v0, v1

    .line 330
    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lpeo;

    .line 331
    iget-object v3, p0, Lnrp;->f:[Lpeo;

    if-eqz v3, :cond_c

    .line 332
    iget-object v3, p0, Lnrp;->f:[Lpeo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 334
    :cond_c
    iput-object v2, p0, Lnrp;->f:[Lpeo;

    .line 335
    :goto_7
    iget-object v2, p0, Lnrp;->f:[Lpeo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    .line 336
    iget-object v2, p0, Lnrp;->f:[Lpeo;

    new-instance v3, Lpeo;

    invoke-direct {v3}, Lpeo;-><init>()V

    aput-object v3, v2, v0

    .line 337
    iget-object v2, p0, Lnrp;->f:[Lpeo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 338
    invoke-virtual {p1}, Loxn;->a()I

    .line 335
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 329
    :cond_d
    iget-object v0, p0, Lnrp;->f:[Lpeo;

    array-length v0, v0

    goto :goto_6

    .line 341
    :cond_e
    iget-object v2, p0, Lnrp;->f:[Lpeo;

    new-instance v3, Lpeo;

    invoke-direct {v3}, Lpeo;-><init>()V

    aput-object v3, v2, v0

    .line 342
    iget-object v2, p0, Lnrp;->f:[Lpeo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 346
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnrp;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 207
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x21 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x50 -> :sswitch_8
        0x5a -> :sswitch_9
        0x60 -> :sswitch_a
        0x72 -> :sswitch_b
        0x7a -> :sswitch_c
        0x82 -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lnrp;->b:Lohv;

    if-eqz v1, :cond_0

    .line 68
    const/4 v1, 0x1

    iget-object v2, p0, Lnrp;->b:Lohv;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 70
    :cond_0
    iget-object v1, p0, Lnrp;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 71
    const/4 v1, 0x2

    iget-object v2, p0, Lnrp;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 73
    :cond_1
    iget-object v1, p0, Lnrp;->d:[Lohv;

    if-eqz v1, :cond_3

    .line 74
    iget-object v2, p0, Lnrp;->d:[Lohv;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 75
    if-eqz v4, :cond_2

    .line 76
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 74
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    :cond_3
    iget-object v1, p0, Lnrp;->i:Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 81
    const/4 v1, 0x4

    iget-object v2, p0, Lnrp;->i:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(ID)V

    .line 83
    :cond_4
    iget-object v1, p0, Lnrp;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 84
    const/4 v1, 0x5

    iget-object v2, p0, Lnrp;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 86
    :cond_5
    iget-object v1, p0, Lnrp;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 87
    const/4 v1, 0x6

    iget-object v2, p0, Lnrp;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 89
    :cond_6
    iget-object v1, p0, Lnrp;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 90
    const/4 v1, 0x7

    iget-object v2, p0, Lnrp;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 92
    :cond_7
    iget v1, p0, Lnrp;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_8

    .line 93
    const/16 v1, 0xa

    iget v2, p0, Lnrp;->g:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 95
    :cond_8
    iget-object v1, p0, Lnrp;->m:[Lnrp;

    if-eqz v1, :cond_a

    .line 96
    iget-object v2, p0, Lnrp;->m:[Lnrp;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 97
    if-eqz v4, :cond_9

    .line 98
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 96
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 102
    :cond_a
    iget-object v1, p0, Lnrp;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 103
    const/16 v1, 0xc

    iget-object v2, p0, Lnrp;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 105
    :cond_b
    iget-object v1, p0, Lnrp;->l:[Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 106
    iget-object v2, p0, Lnrp;->l:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 107
    const/16 v5, 0xe

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 110
    :cond_c
    iget-object v1, p0, Lnrp;->f:[Lpeo;

    if-eqz v1, :cond_e

    .line 111
    iget-object v1, p0, Lnrp;->f:[Lpeo;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 112
    if-eqz v3, :cond_d

    .line 113
    const/16 v4, 0xf

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 111
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 117
    :cond_e
    iget-object v0, p0, Lnrp;->n:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 118
    const/16 v0, 0x10

    iget-object v1, p0, Lnrp;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 120
    :cond_f
    iget-object v0, p0, Lnrp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 122
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lnrp;->a(Loxn;)Lnrp;

    move-result-object v0

    return-object v0
.end method
