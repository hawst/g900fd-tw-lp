.class public final Leko;
.super Leku;
.source "PG"


# instance fields
.field private N:Lekq;

.field private O:Landroid/content/Intent;

.field private final ad:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Leku;-><init>()V

    .line 61
    new-instance v0, Lekp;

    invoke-direct {v0, p0}, Lekp;-><init>(Leko;)V

    iput-object v0, p0, Leko;->ad:Lbc;

    .line 93
    return-void
.end method

.method static synthetic a(Leko;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Leko;->O:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic a(Leko;)Lekq;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Leko;->N:Lekq;

    return-object v0
.end method


# virtual methods
.method protected X()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    const v0, 0x7f0a07eb

    invoke-virtual {p0, v0}, Leko;->e_(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 183
    const/16 v0, 0x45

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 152
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 153
    iget-object v1, p0, Leko;->N:Lekq;

    invoke-virtual {p0, v1}, Leko;->a(Landroid/widget/ListAdapter;)V

    .line 154
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 145
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 146
    new-instance v0, Lekq;

    iget-object v1, p0, Leko;->at:Llnl;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lekq;-><init>(Leko;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Leko;->N:Lekq;

    .line 147
    return-void
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 159
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 160
    const v0, 0x7f0a07ea

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 161
    const v0, 0x7f10068e

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 162
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 188
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 189
    const v1, 0x7f10068e

    if-ne v0, v1, :cond_0

    .line 190
    iget-object v0, p0, Leko;->O:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Leko;->O:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Leko;->a(Landroid/content/Intent;)V

    .line 192
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Leku;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected synthetic al_()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Leko;->X()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Leko;->R:Z

    .line 167
    invoke-virtual {p0}, Leko;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Leko;->ad:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 168
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Leko;->R:Z

    .line 173
    invoke-virtual {p0}, Leko;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Leko;->ad:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 174
    return-void
.end method
