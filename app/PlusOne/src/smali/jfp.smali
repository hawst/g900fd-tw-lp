.class public final Ljfp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljfn;
.implements Ljga;
.implements Ljgf;


# instance fields
.field a:Ljge;

.field b:Ljfy;

.field c:Ljfn;

.field private d:Landroid/database/ContentObserver;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljfy;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljfm;

.field private g:Ljava/lang/String;

.field private h:J

.field private i:Ljava/lang/String;

.field private j:J

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljfy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljfn;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljfr;

    invoke-direct {v0, p0}, Ljfr;-><init>(Ljfp;)V

    iput-object v0, p0, Ljfp;->d:Landroid/database/ContentObserver;

    .line 65
    new-instance v0, Ljfm;

    invoke-direct {v0, p0}, Ljfm;-><init>(Ljfn;)V

    iput-object v0, p0, Ljfp;->f:Ljfm;

    .line 66
    iput-object p1, p0, Ljfp;->c:Ljfn;

    .line 67
    return-void
.end method

.method private a(Landroid/database/ContentObserver;)V
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 72
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 73
    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    invoke-virtual {v0, p1}, Ljfy;->c(Landroid/database/ContentObserver;)V

    .line 72
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 76
    :cond_0
    return-void
.end method

.method static synthetic a(Ljfp;)V
    .locals 4

    .prologue
    .line 25
    iget-object v0, p0, Ljfp;->f:Ljfm;

    invoke-virtual {v0}, Ljfm;->a()V

    iget-object v0, p0, Ljfp;->i:Ljava/lang/String;

    iget-wide v2, p0, Ljfp;->j:J

    invoke-direct {p0, v0, v2, v3}, Ljfp;->b(Ljava/lang/String;J)Z

    return-void
.end method

.method private b(Ljava/lang/String;J)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 300
    if-eqz p1, :cond_3

    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0, p2, p3}, Ljfy;->a(J)Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_4

    .line 301
    iput-wide p2, p0, Ljfp;->h:J

    .line 302
    iput-object p1, p0, Ljfp;->g:Ljava/lang/String;

    .line 307
    iget-object v0, p0, Ljfp;->f:Ljfm;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Ljfp;->f:Ljfm;

    iget-object v1, p0, Ljfp;->g:Ljava/lang/String;

    iget-wide v2, p0, Ljfp;->h:J

    invoke-virtual {v0, v1, v2, v3}, Ljfm;->a(Ljava/lang/String;J)Z

    .line 310
    :cond_0
    const/4 v0, 0x1

    :goto_2
    return v0

    .line 300
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ljfp;->b:Ljfy;

    instance-of v0, v0, Ljfx;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljfp;->b:Ljfy;

    check-cast v0, Ljfx;

    invoke-interface {v0, p1}, Ljfx;->a(Ljava/lang/String;)Ljfy;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, p2, p3}, Ljfy;->a(J)Z

    move-result v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 304
    goto :goto_2
.end method

.method private k()V
    .locals 6

    .prologue
    .line 193
    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 195
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 196
    const/4 v1, 0x0

    .line 197
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 198
    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 199
    invoke-virtual {v0}, Ljfy;->au_()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 197
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 207
    :cond_0
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_1

    .line 211
    :cond_1
    iget-object v0, p0, Ljfp;->f:Ljfm;

    invoke-virtual {v0, v4}, Ljfm;->a(Ljava/util/List;)V

    .line 212
    if-eqz v1, :cond_2

    .line 213
    iput-object v1, p0, Ljfp;->b:Ljfy;

    .line 216
    :cond_2
    iget-object v0, p0, Ljfp;->g:Ljava/lang/String;

    iget-wide v2, p0, Ljfp;->h:J

    invoke-direct {p0, v0, v2, v3}, Ljfp;->b(Ljava/lang/String;J)Z

    .line 217
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljfy;
    .locals 2

    .prologue
    .line 244
    const/4 v0, 0x0

    .line 245
    iget-object v1, p0, Ljfp;->e:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 246
    iget-object v0, p0, Ljfp;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 248
    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Ljfp;->b:Ljfy;

    instance-of v1, v1, Ljfx;

    if-eqz v1, :cond_1

    .line 250
    iget-object v0, p0, Ljfp;->b:Ljfy;

    check-cast v0, Ljfx;

    invoke-interface {v0, p1}, Ljfx;->a(Ljava/lang/String;)Ljfy;

    move-result-object v0

    .line 252
    :cond_1
    return-object v0
.end method

.method a()V
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, Ljfp;->d:Landroid/database/ContentObserver;

    invoke-direct {p0, v0}, Ljfp;->a(Landroid/database/ContentObserver;)V

    .line 133
    iget-object v0, p0, Ljfp;->i:Ljava/lang/String;

    iget-wide v2, p0, Ljfp;->j:J

    invoke-direct {p0, v0, v2, v3}, Ljfp;->b(Ljava/lang/String;J)Z

    .line 134
    return-void
.end method

.method a(ILandroid/content/Context;)V
    .locals 5

    .prologue
    .line 89
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 124
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-static {p2}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    .line 97
    const-class v1, Ljfy;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Ljfp;->k:Ljava/util/List;

    .line 98
    const-class v1, Ljgd;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgd;

    .line 99
    if-eqz v0, :cond_1

    .line 100
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 101
    iget-object v1, p0, Ljfp;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljgd;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljfp;->k:Ljava/util/List;

    .line 104
    :cond_1
    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 106
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ljfp;->e:Ljava/util/Map;

    .line 108
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_4

    .line 109
    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 110
    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 111
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Navigation Item with empty Handle."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_2
    iget-object v3, p0, Ljfp;->e:Ljava/util/Map;

    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-virtual {v0, p2, p1}, Ljfy;->a(Landroid/content/Context;I)V

    .line 116
    instance-of v3, v0, Ljge;

    if-eqz v3, :cond_3

    .line 117
    check-cast v0, Ljge;

    iput-object v0, p0, Ljfp;->a:Ljge;

    .line 118
    iget-object v0, p0, Ljfp;->a:Ljge;

    invoke-interface {v0, p0}, Ljge;->a(Ljga;)V

    .line 108
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 121
    :cond_4
    iget-object v0, p0, Ljfp;->a:Ljge;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljfp;->a:Ljge;

    invoke-interface {v0}, Ljge;->aL_()V

    .line 122
    :cond_5
    invoke-direct {p0}, Ljfp;->k()V

    .line 123
    iget-object v0, p0, Ljfp;->i:Ljava/lang/String;

    iget-wide v2, p0, Ljfp;->j:J

    invoke-direct {p0, v0, v2, v3}, Ljfp;->b(Ljava/lang/String;J)Z

    goto :goto_0
.end method

.method a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 175
    if-eqz p1, :cond_0

    .line 176
    const-string v0, "selection_handle"

    .line 177
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfp;->g:Ljava/lang/String;

    .line 178
    const-string v0, "selection_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Ljfp;->h:J

    .line 180
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;J)Z
    .locals 4

    .prologue
    .line 269
    iput-object p1, p0, Ljfp;->i:Ljava/lang/String;

    .line 270
    iput-wide p2, p0, Ljfp;->j:J

    .line 271
    iget-object v0, p0, Ljfp;->i:Ljava/lang/String;

    iget-wide v2, p0, Ljfp;->j:J

    invoke-direct {p0, v0, v2, v3}, Ljfp;->b(Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method b()V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ljfp;->a(Landroid/database/ContentObserver;)V

    .line 138
    iget-object v0, p0, Ljfp;->a:Ljge;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljfp;->a:Ljge;

    invoke-interface {v0}, Ljge;->g()V

    .line 139
    :cond_0
    return-void
.end method

.method b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 182
    if-eqz p1, :cond_0

    .line 183
    const-string v0, "selection_handle"

    iget-object v1, p0, Ljfp;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v0, "selection_id"

    iget-wide v2, p0, Ljfp;->h:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 186
    :cond_0
    return-void
.end method

.method public b(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Ljfp;->c:Ljfn;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Ljfp;->c:Ljfn;

    invoke-interface {v0, p1}, Ljfn;->b(Landroid/content/Intent;)Z

    move-result v0

    .line 318
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Ljfy;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Ljfp;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Ljfp;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljfp;->a(Ljava/lang/String;)Ljfy;

    move-result-object v0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()J
    .locals 2

    .prologue
    .line 149
    iget-wide v0, p0, Ljfp;->h:J

    return-wide v0
.end method

.method e()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Ljfp;->a:Ljge;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Ljfp;->a:Ljge;

    invoke-interface {v0}, Ljge;->h()V

    .line 156
    :cond_0
    return-void
.end method

.method f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljfy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Ljfp;->k:Ljava/util/List;

    return-object v0
.end method

.method g()Ljfy;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Ljfp;->b:Ljfy;

    return-object v0
.end method

.method h()Ljfq;
    .locals 1

    .prologue
    .line 167
    new-instance v0, Ljfq;

    invoke-direct {v0, p0}, Ljfq;-><init>(Ljfp;)V

    return-object v0
.end method

.method i()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Ljfp;->f:Ljfm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljfm;->a(Ljava/util/List;)V

    .line 172
    return-void
.end method

.method j()Ljfm;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Ljfp;->f:Ljfm;

    return-object v0
.end method
