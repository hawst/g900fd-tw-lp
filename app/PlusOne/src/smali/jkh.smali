.class public final Ljkh;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static a(Landroid/content/Context;Lkfo;[Ljava/lang/String;ILjava/lang/String;Ljjp;)V
    .locals 6

    .prologue
    .line 81
    array-length v0, p2

    if-lez v0, :cond_1

    .line 82
    new-instance v1, Lpfe;

    invoke-direct {v1}, Lpfe;-><init>()V

    .line 83
    iput-object p4, v1, Lpfe;->a:Ljava/lang/String;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v3, p2}, Ljla;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljkx;

    move-result-object v2

    invoke-static {p0, p1}, Ljju;->b(Landroid/content/Context;Lkfo;)Ljava/lang/Long;

    :goto_0
    invoke-virtual {v2}, Ljkx;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lpfi;

    invoke-direct {v3}, Lpfi;-><init>()V

    invoke-virtual {v2}, Ljkx;->a()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lpfi;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljkx;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v3, Lpfi;->c:Ljava/lang/Long;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lpfi;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lpfi;

    iput-object v0, v1, Lpfe;->b:[Lpfi;

    .line 86
    iput p3, v1, Lpfe;->c:I

    .line 88
    new-instance v2, Ljki;

    invoke-direct {v2, p0, p1, v1}, Ljki;-><init>(Landroid/content/Context;Lkfo;Lpfe;)V

    .line 89
    const-class v0, Ljjo;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjo;

    const/4 v1, 0x0

    .line 90
    invoke-virtual {v0, v2, p5, p0, v1}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {v2}, Ljki;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    const-string v0, "SetReadStatesHelper"

    const-string v1, "Failed to set read states."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;Lkfo;[Ljava/lang/String;ILjjp;)V
    .locals 7

    .prologue
    .line 45
    invoke-virtual {p1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-virtual {p1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v2

    .line 45
    invoke-static {p0}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "GunsSyncer"

    const-string v1, "Cannot find database helper for account."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :goto_0
    const-class v0, Ljiy;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljiy;

    .line 49
    invoke-interface {v0}, Ljiy;->a()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    .line 48
    invoke-static/range {v0 .. v5}, Ljkh;->a(Landroid/content/Context;Lkfo;[Ljava/lang/String;ILjava/lang/String;Ljjp;)V

    .line 50
    return-void

    .line 45
    :cond_0
    invoke-virtual {v0}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    :try_start_0
    new-instance v4, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(I)V

    const-string v0, "read_state"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "key"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    array-length v0, p2

    invoke-static {v0}, Ljla;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string v5, "notifications"

    invoke-virtual {v3, v5, v4, v0, p2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v0, Ljkn;

    invoke-direct {v0}, Ljkn;-><init>()V

    invoke-virtual {v0, p0, v1, v2}, Ljkn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v0
.end method

.method public static a(Landroid/content/Context;[Ljiu;Ljir;)V
    .locals 3

    .prologue
    .line 66
    const-class v0, Ljis;

    .line 67
    invoke-static {p0, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    .line 68
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 69
    new-instance v0, Ljiq;

    invoke-direct {v0}, Ljiq;-><init>()V

    .line 70
    if-eqz p1, :cond_0

    .line 71
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 73
    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 74
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_1
    return-void
.end method
