.class public final Lmie;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnti;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16603
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16606
    iput-object v0, p0, Lmie;->apiHeader:Llyq;

    .line 16609
    iput-object v0, p0, Lmie;->a:Lnti;

    .line 16603
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 16626
    const/4 v0, 0x0

    .line 16627
    iget-object v1, p0, Lmie;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 16628
    const/4 v0, 0x1

    iget-object v1, p0, Lmie;->apiHeader:Llyq;

    .line 16629
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16631
    :cond_0
    iget-object v1, p0, Lmie;->a:Lnti;

    if-eqz v1, :cond_1

    .line 16632
    const/4 v1, 0x2

    iget-object v2, p0, Lmie;->a:Lnti;

    .line 16633
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16635
    :cond_1
    iget-object v1, p0, Lmie;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16636
    iput v0, p0, Lmie;->ai:I

    .line 16637
    return v0
.end method

.method public a(Loxn;)Lmie;
    .locals 2

    .prologue
    .line 16645
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 16646
    sparse-switch v0, :sswitch_data_0

    .line 16650
    iget-object v1, p0, Lmie;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 16651
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmie;->ah:Ljava/util/List;

    .line 16654
    :cond_1
    iget-object v1, p0, Lmie;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16656
    :sswitch_0
    return-object p0

    .line 16661
    :sswitch_1
    iget-object v0, p0, Lmie;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 16662
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmie;->apiHeader:Llyq;

    .line 16664
    :cond_2
    iget-object v0, p0, Lmie;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16668
    :sswitch_2
    iget-object v0, p0, Lmie;->a:Lnti;

    if-nez v0, :cond_3

    .line 16669
    new-instance v0, Lnti;

    invoke-direct {v0}, Lnti;-><init>()V

    iput-object v0, p0, Lmie;->a:Lnti;

    .line 16671
    :cond_3
    iget-object v0, p0, Lmie;->a:Lnti;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16646
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 16614
    iget-object v0, p0, Lmie;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 16615
    const/4 v0, 0x1

    iget-object v1, p0, Lmie;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16617
    :cond_0
    iget-object v0, p0, Lmie;->a:Lnti;

    if-eqz v0, :cond_1

    .line 16618
    const/4 v0, 0x2

    iget-object v1, p0, Lmie;->a:Lnti;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16620
    :cond_1
    iget-object v0, p0, Lmie;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 16622
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 16599
    invoke-virtual {p0, p1}, Lmie;->a(Loxn;)Lmie;

    move-result-object v0

    return-object v0
.end method
