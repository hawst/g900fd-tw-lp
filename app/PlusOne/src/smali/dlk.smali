.class public final Ldlk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:[B

.field private static final d:[B

.field private static final e:[B

.field private static final f:[B


# instance fields
.field private final g:[B

.field private final h:[B

.field private final i:Ljava/io/InputStream;

.field private final j:[B

.field private final k:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "--"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Ldlk;->a:[B

    .line 23
    const-string v0, "onetwothreefourfivesixseven"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Ldlk;->b:[B

    .line 26
    const-string v0, "Content-Type: "

    .line 27
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Ldlk;->c:[B

    .line 29
    const-string v0, "application/x-protobuf"

    .line 30
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Ldlk;->d:[B

    .line 32
    const-string v0, "Content-Transfer-Encoding: binary"

    .line 33
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Ldlk;->e:[B

    .line 36
    const-string v0, "\r\n"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Ldlk;->f:[B

    return-void
.end method

.method public constructor <init>([BLjava/io/InputStream;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Ldlk;->g:[B

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Ldlk;->h:[B

    .line 61
    iput-object p2, p0, Ldlk;->i:Ljava/io/InputStream;

    .line 62
    invoke-static {p3}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Ldlk;->j:[B

    .line 63
    invoke-direct {p0}, Ldlk;->c()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, p4

    .line 65
    invoke-direct {p0}, Ldlk;->d()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldlk;->k:J

    .line 66
    return-void
.end method

.method public constructor <init>([B[BLjava/lang/String;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Ldlk;->g:[B

    .line 49
    iput-object p2, p0, Ldlk;->h:[B

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Ldlk;->i:Ljava/io/InputStream;

    .line 51
    invoke-static {p3}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Ldlk;->j:[B

    .line 52
    invoke-direct {p0}, Ldlk;->c()I

    move-result v0

    array-length v1, p2

    add-int/2addr v0, v1

    .line 54
    invoke-direct {p0}, Ldlk;->d()I

    move-result v1

    add-int/2addr v0, v1

    int-to-long v0, v0

    iput-wide v0, p0, Ldlk;->k:J

    .line 55
    return-void
.end method

.method private static a(Z)I
    .locals 2

    .prologue
    .line 155
    sget-object v0, Ldlk;->a:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x0

    .line 157
    sget-object v1, Ldlk;->b:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 158
    if-eqz p0, :cond_0

    .line 159
    sget-object v1, Ldlk;->a:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 161
    :cond_0
    sget-object v1, Ldlk;->f:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 162
    return v0
.end method

.method static synthetic a(Ldlk;)Ljava/io/InputStream;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 16
    invoke-direct {p0}, Ldlk;->c()I

    move-result v0

    new-array v0, v0, [B

    new-instance v1, Ldlm;

    invoke-direct {v1, v0}, Ldlm;-><init>([B)V

    invoke-static {v1, v4}, Ldlk;->a(Ldlm;Z)V

    iget-object v2, p0, Ldlk;->g:[B

    sget-object v3, Ldlk;->c:[B

    invoke-virtual {v1, v3}, Ldlm;->a([B)V

    sget-object v3, Ldlk;->d:[B

    invoke-virtual {v1, v3}, Ldlm;->a([B)V

    sget-object v3, Ldlk;->f:[B

    invoke-virtual {v1, v3}, Ldlm;->a([B)V

    sget-object v3, Ldlk;->f:[B

    invoke-virtual {v1, v3}, Ldlm;->a([B)V

    invoke-virtual {v1, v2}, Ldlm;->a([B)V

    sget-object v2, Ldlk;->f:[B

    invoke-virtual {v1, v2}, Ldlm;->a([B)V

    invoke-static {v1, v4}, Ldlk;->a(Ldlm;Z)V

    iget-object v2, p0, Ldlk;->j:[B

    sget-object v3, Ldlk;->c:[B

    invoke-virtual {v1, v3}, Ldlm;->a([B)V

    invoke-virtual {v1, v2}, Ldlm;->a([B)V

    sget-object v2, Ldlk;->f:[B

    invoke-virtual {v1, v2}, Ldlm;->a([B)V

    sget-object v2, Ldlk;->e:[B

    invoke-virtual {v1, v2}, Ldlm;->a([B)V

    sget-object v2, Ldlk;->f:[B

    invoke-virtual {v1, v2}, Ldlm;->a([B)V

    sget-object v2, Ldlk;->f:[B

    invoke-virtual {v1, v2}, Ldlm;->a([B)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v1
.end method

.method private static a(Ldlm;Z)V
    .locals 1

    .prologue
    .line 146
    sget-object v0, Ldlk;->a:[B

    invoke-virtual {p0, v0}, Ldlm;->a([B)V

    .line 147
    sget-object v0, Ldlk;->b:[B

    invoke-virtual {p0, v0}, Ldlm;->a([B)V

    .line 148
    if-eqz p1, :cond_0

    .line 149
    sget-object v0, Ldlk;->a:[B

    invoke-virtual {p0, v0}, Ldlm;->a([B)V

    .line 151
    :cond_0
    sget-object v0, Ldlk;->f:[B

    invoke-virtual {p0, v0}, Ldlm;->a([B)V

    .line 152
    return-void
.end method

.method static synthetic b(Ldlk;)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Ldlk;->h:[B

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Ldlk;->h:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldlk;->i:Ljava/io/InputStream;

    goto :goto_0
.end method

.method private c()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 113
    invoke-static {v4}, Ldlk;->a(Z)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 115
    iget-object v1, p0, Ldlk;->g:[B

    sget-object v2, Ldlk;->c:[B

    array-length v2, v2

    add-int/lit8 v2, v2, 0x0

    sget-object v3, Ldlk;->d:[B

    array-length v3, v3

    add-int/2addr v2, v3

    sget-object v3, Ldlk;->f:[B

    array-length v3, v3

    add-int/2addr v2, v3

    sget-object v3, Ldlk;->f:[B

    array-length v3, v3

    add-int/2addr v2, v3

    array-length v1, v1

    add-int/2addr v1, v2

    sget-object v2, Ldlk;->f:[B

    array-length v2, v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 116
    invoke-static {v4}, Ldlk;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    iget-object v1, p0, Ldlk;->j:[B

    sget-object v2, Ldlk;->c:[B

    array-length v2, v2

    add-int/lit8 v2, v2, 0x0

    array-length v1, v1

    add-int/2addr v1, v2

    sget-object v2, Ldlk;->f:[B

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, Ldlk;->e:[B

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, Ldlk;->f:[B

    array-length v2, v2

    add-int/2addr v1, v2

    sget-object v2, Ldlk;->f:[B

    array-length v2, v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 118
    return v0
.end method

.method static synthetic c(Ldlk;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 16
    invoke-direct {p0}, Ldlk;->d()I

    move-result v0

    new-array v0, v0, [B

    new-instance v1, Ldlm;

    invoke-direct {v1, v0}, Ldlm;-><init>([B)V

    sget-object v2, Ldlk;->f:[B

    invoke-virtual {v1, v2}, Ldlm;->a([B)V

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ldlk;->a(Ldlm;Z)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v1
.end method

.method private d()I
    .locals 2

    .prologue
    .line 139
    sget-object v0, Ldlk;->f:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x0

    .line 141
    const/4 v1, 0x1

    invoke-static {v1}, Ldlk;->a(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    return v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Ldlk;->k:J

    return-wide v0
.end method

.method public b()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Ljava/io/SequenceInputStream;

    new-instance v1, Ldll;

    invoke-direct {v1, p0}, Ldll;-><init>(Ldlk;)V

    invoke-direct {v0, v1}, Ljava/io/SequenceInputStream;-><init>(Ljava/util/Enumeration;)V

    return-object v0
.end method
