.class public final Ledt;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Lhjj;
.implements Lhjn;
.implements Lhmq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhjj;",
        "Lhjn;",
        "Lhmq;"
    }
.end annotation


# static fields
.field private static final T:[Ljava/lang/String;


# instance fields
.field private N:Landroid/widget/ListView;

.field private O:Lhyd;

.field private P:Lhee;

.field private Q:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ledz;",
            ">;"
        }
    .end annotation
.end field

.field private R:I

.field private S:Z

.field private U:I

.field private V:Z

.field private W:Ledy;

.field private final X:Licq;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 131
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "theme_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "placeholder_path"

    aput-object v2, v0, v1

    sput-object v0, Ledt;->T:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 182
    invoke-direct {p0}, Llol;-><init>()V

    .line 56
    new-instance v0, Lhje;

    iget-object v1, p0, Ledt;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    .line 174
    new-instance v0, Licq;

    iget-object v1, p0, Ledt;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a0940

    .line 175
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    const v1, 0x7f0a057b

    .line 176
    invoke-virtual {v0, v1}, Licq;->a(I)Licq;

    move-result-object v0

    iput-object v0, p0, Ledt;->X:Licq;

    .line 183
    const/4 v0, 0x0

    iput v0, p0, Ledt;->U:I

    .line 184
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 178
    invoke-direct {p0}, Llol;-><init>()V

    .line 56
    new-instance v0, Lhje;

    iget-object v1, p0, Ledt;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    .line 174
    new-instance v0, Licq;

    iget-object v1, p0, Ledt;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a0940

    .line 175
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    const v1, 0x7f0a057b

    .line 176
    invoke-virtual {v0, v1}, Licq;->a(I)Licq;

    move-result-object v0

    iput-object v0, p0, Ledt;->X:Licq;

    .line 179
    iput p1, p0, Ledt;->U:I

    .line 180
    return-void
.end method

.method static synthetic a(Ledt;)Lhee;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ledt;->P:Lhee;

    return-object v0
.end method

.method static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Ledt;->T:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Ledt;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Ledt;->U:I

    return v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 395
    sget-object v0, Lhmw;->E:Lhmw;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 219
    const v0, 0x7f04009e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 222
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Ledt;->N:Landroid/widget/ListView;

    .line 224
    new-instance v0, Ledw;

    invoke-virtual {p0}, Ledt;->n()Lz;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Ledw;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Ledt;->O:Lhyd;

    .line 225
    iget-object v0, p0, Ledt;->N:Landroid/widget/ListView;

    iget-object v2, p0, Ledt;->O:Lhyd;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 226
    iget-object v0, p0, Ledt;->N:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 227
    iget-object v0, p0, Ledt;->N:Landroid/widget/ListView;

    new-instance v2, Ledu;

    invoke-direct {v2}, Ledu;-><init>()V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 235
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 263
    invoke-virtual {p0}, Ledt;->n()Lz;

    move-result-object v1

    .line 264
    packed-switch p1, :pswitch_data_0

    .line 276
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 266
    :pswitch_0
    new-instance v0, Ledv;

    invoke-direct {v0, p0, v1, v1}, Ledv;-><init>(Ledt;Landroid/content/Context;Landroid/content/Context;)V

    goto :goto_0

    .line 264
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 281
    iput-boolean v1, p0, Ledt;->V:Z

    .line 282
    iget-object v2, p0, Ledt;->O:Lhyd;

    invoke-virtual {v2, p1}, Lhyd;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 284
    iget-boolean v2, p0, Ledt;->S:Z

    if-eqz v2, :cond_1

    .line 285
    iget-object v2, p0, Ledt;->N:Landroid/widget/ListView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Ledt;->N:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v0}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 286
    :cond_0
    iput-boolean v0, p0, Ledt;->S:Z

    .line 289
    :cond_1
    invoke-virtual {p0}, Ledt;->x()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Ledt;->V:Z

    if-eqz v2, :cond_3

    iget-object v0, p0, Ledt;->X:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 290
    :cond_2
    :goto_0
    return-void

    .line 289
    :cond_3
    iget-object v2, p0, Ledt;->O:Lhyd;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ledt;->O:Lhyd;

    invoke-virtual {v2}, Lhyd;->a()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Ledt;->O:Lhyd;

    invoke-virtual {v2}, Lhyd;->getCount()I

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    if-eqz v0, :cond_2

    iget-object v0, p0, Ledt;->X:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 192
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 194
    new-instance v0, Landroid/widget/ArrayAdapter;

    .line 195
    invoke-virtual {p0}, Ledt;->n()Lz;

    move-result-object v1

    const v2, 0x7f040037

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Ledt;->Q:Landroid/widget/ArrayAdapter;

    .line 196
    iget-object v0, p0, Ledt;->Q:Landroid/widget/ArrayAdapter;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 199
    iget-object v0, p0, Ledt;->Q:Landroid/widget/ArrayAdapter;

    new-instance v1, Ledz;

    iget-object v2, p0, Ledt;->at:Llnl;

    invoke-direct {v1, v2, v4}, Ledz;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 201
    iget-object v0, p0, Ledt;->Q:Landroid/widget/ArrayAdapter;

    new-instance v1, Ledz;

    iget-object v2, p0, Ledt;->at:Llnl;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ledz;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 204
    if-eqz p1, :cond_0

    .line 205
    const-string v0, "filter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ledt;->U:I

    .line 208
    :cond_0
    invoke-virtual {p0}, Ledt;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 209
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 48
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Ledt;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ledy;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Ledt;->W:Ledy;

    .line 188
    return-void
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public a(Loo;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 343
    invoke-static {p1, v3}, Lley;->a(Loo;Z)V

    .line 345
    iget v2, p0, Ledt;->U:I

    iget-object v0, p0, Ledt;->Q:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Ledt;->Q:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledz;

    invoke-virtual {v0}, Ledz;->a()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_1
    iput v0, p0, Ledt;->R:I

    .line 347
    invoke-virtual {p0}, Ledt;->n()Lz;

    move-result-object v0

    const v1, 0x7f040031

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 348
    const v0, 0x7f100176

    .line 349
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    .line 350
    iget-object v2, p0, Ledt;->Q:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 351
    iget v2, p0, Ledt;->R:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setSelection(I)V

    .line 352
    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->a(Lhjn;)V

    .line 354
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 355
    invoke-virtual {p1, v3}, Loo;->d(Z)V

    .line 356
    invoke-virtual {p1, v1}, Loo;->a(Landroid/view/View;)V

    .line 357
    return-void

    .line 345
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Llol;->aO_()V

    .line 79
    iget-object v0, p0, Ledt;->O:Lhyd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledt;->O:Lhyd;

    invoke-virtual {v0}, Lhyd;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Ledt;->O:Lhyd;

    .line 82
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 362
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 363
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 364
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 99
    iget-object v0, p0, Ledt;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 100
    iget-object v0, p0, Ledt;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Ledt;->P:Lhee;

    .line 101
    return-void
.end method

.method public c(I)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 368
    iget v0, p0, Ledt;->R:I

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 378
    :goto_0
    return v0

    .line 372
    :cond_0
    iget-object v0, p0, Ledt;->Q:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledz;

    .line 373
    iput p1, p0, Ledt;->R:I

    .line 374
    invoke-virtual {v0}, Ledz;->a()I

    move-result v0

    iput v0, p0, Ledt;->U:I

    .line 376
    invoke-virtual {p0}, Ledt;->w()Lbb;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 377
    iput-boolean v2, p0, Ledt;->S:Z

    move v0, v2

    .line 378
    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 213
    const-string v0, "filter"

    iget v1, p0, Ledt;->U:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Ledt;->O:Lhyd;

    invoke-virtual {v0, p3}, Lhyd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 241
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 242
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 243
    iget-object v2, p0, Ledt;->W:Ledy;

    if-eqz v2, :cond_0

    .line 244
    iget-object v2, p0, Ledt;->W:Ledy;

    invoke-interface {v2, v1, v0}, Ledy;->a(ILjava/lang/String;)V

    .line 247
    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Llol;->z()V

    .line 70
    iget-object v0, p0, Ledt;->O:Lhyd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledt;->O:Lhyd;

    invoke-virtual {v0}, Lhyd;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Ledt;->O:Lhyd;

    invoke-virtual {v0}, Lhyd;->e()V

    .line 73
    :cond_0
    return-void
.end method
