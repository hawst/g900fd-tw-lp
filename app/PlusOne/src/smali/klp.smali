.class final Lklp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lklo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lklo;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    .line 72
    :try_start_0
    new-instance v1, Loya;

    invoke-direct {v1}, Loya;-><init>()V

    .line 73
    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Loya;

    .line 74
    new-instance v1, Lklo;

    invoke-direct {v1, v2, v0}, Lklo;-><init>(Ljava/lang/String;Loya;)V
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 82
    :goto_0
    return-object v0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    const-string v1, "Failed to parse EmbedClient.EmbedClientItem from Parcel"

    .line 77
    const-string v2, "Preview"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 78
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 82
    :cond_0
    new-instance v0, Lklo;

    invoke-direct {v0, v1, v1}, Lklo;-><init>(Ljava/lang/String;Loya;)V

    goto :goto_0
.end method

.method public a(I)[Lklo;
    .locals 1

    .prologue
    .line 88
    new-array v0, p1, [Lklo;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lklp;->a(Landroid/os/Parcel;)Lklo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lklp;->a(I)[Lklo;

    move-result-object v0

    return-object v0
.end method
