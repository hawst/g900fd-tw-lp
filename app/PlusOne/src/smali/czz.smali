.class public Lczz;
.super Llol;
.source "PG"

# interfaces
.implements Ldys;
.implements Llgs;


# instance fields
.field private final N:Ldaa;

.field private O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ldgi;

.field private Q:Ldeo;

.field private R:I

.field private S:Ldab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Llol;-><init>()V

    .line 97
    new-instance v0, Ldaa;

    invoke-direct {v0, p0}, Ldaa;-><init>(Lczz;)V

    iput-object v0, p0, Lczz;->N:Ldaa;

    .line 369
    return-void
.end method

.method static synthetic a(Lczz;)Ldgi;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lczz;->P:Ldgi;

    return-object v0
.end method

.method static synthetic b(Lczz;)Ldeo;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lczz;->Q:Ldeo;

    return-object v0
.end method

.method static synthetic c(Lczz;)Ldab;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lczz;->S:Ldab;

    return-object v0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v7, 0x5

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, -0x1

    const/4 v8, 0x0

    .line 245
    const-string v0, "comment_action"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 246
    if-nez v0, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 259
    const-string v1, "comment_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 260
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 262
    invoke-virtual {p0}, Lczz;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "account_id"

    invoke-virtual {v1, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 265
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 267
    :pswitch_1
    const-string v0, "plusone_by_me"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 268
    const-string v0, "photo_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 269
    invoke-virtual {p0}, Lczz;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 270
    const-string v6, "view_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 272
    invoke-virtual {p0}, Lczz;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-nez v4, :cond_2

    :goto_1
    iget-object v4, p0, Lczz;->Q:Ldeo;

    .line 276
    invoke-virtual {v4}, Ldeo;->a()Lddl;

    move-result-object v4

    invoke-interface {v4}, Lddl;->k()Ljava/lang/String;

    move-result-object v6

    move-object v4, v2

    .line 271
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)I

    goto :goto_0

    :cond_2
    move v5, v8

    .line 272
    goto :goto_1

    .line 280
    :pswitch_2
    const-string v0, "comment_content"

    .line 281
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 280
    invoke-static {v0}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    .line 282
    iget-object v9, p0, Lczz;->N:Ldaa;

    const-string v0, "extra_comment_id"

    invoke-static {v0, v3}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, v9, Ldaa;->a:Lczz;

    iget-object v1, v1, Lczz;->P:Ldgi;

    sget-object v5, Lhmv;->U:Lhmv;

    invoke-interface {v1, v5, v0}, Ldgi;->a(Lhmv;Landroid/os/Bundle;)V

    iget-object v0, v9, Ldaa;->a:Lczz;

    invoke-virtual {v0}, Lczz;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v0, v9, Ldaa;->a:Lczz;

    iget-object v0, v0, Lczz;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v7

    iget-object v0, v9, Ldaa;->a:Lczz;

    invoke-virtual {v0}, Lczz;->n()Lz;

    move-result-object v0

    invoke-interface {v7}, Lddl;->o()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v7}, Lddl;->j()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v7}, Lddl;->k()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v0 .. v8}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/text/Spanned;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, v9, Ldaa;->a:Lczz;

    invoke-virtual {v1, v0}, Lczz;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 285
    :pswitch_3
    iget-object v0, p0, Lczz;->N:Ldaa;

    invoke-static {v0, v3, v8, v6}, Ldaa;->a(Ldaa;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 289
    :pswitch_4
    iget-object v0, p0, Lczz;->N:Ldaa;

    invoke-static {v0, v3, v5, v6}, Ldaa;->a(Ldaa;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 293
    :pswitch_5
    iget-object v0, p0, Lczz;->N:Ldaa;

    invoke-static {v0, v3, v8, v6}, Ldaa;->a(Ldaa;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 297
    :pswitch_6
    iget-object v0, p0, Lczz;->N:Ldaa;

    const-string v1, "extra_comment_id"

    invoke-static {v1, v3}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, v0, Ldaa;->a:Lczz;

    iget-object v2, v2, Lczz;->P:Ldgi;

    sget-object v4, Lhmv;->W:Lhmv;

    invoke-interface {v2, v4, v1}, Ldgi;->a(Lhmv;Landroid/os/Bundle;)V

    iget-object v1, v0, Ldaa;->a:Lczz;

    const v2, 0x7f0a0762

    invoke-virtual {v1, v2}, Lczz;->e_(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Ldaa;->a:Lczz;

    const v4, 0x7f0a076c

    invoke-virtual {v2, v4}, Lczz;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, v0, Ldaa;->a:Lczz;

    const v5, 0x7f0a0596

    invoke-virtual {v4, v5}, Lczz;->e_(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Ldaa;->a:Lczz;

    const v6, 0x7f0a0597

    invoke-virtual {v5, v6}, Lczz;->e_(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v4, v5}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v1

    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "comment_id"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v0, Ldaa;->a:Lczz;

    invoke-virtual {v1, v2, v8}, Llgr;->a(Lu;I)V

    iget-object v0, v0, Ldaa;->a:Lczz;

    invoke-virtual {v0}, Lczz;->p()Lae;

    move-result-object v0

    const-string v2, "commentsdialog_delete_comment"

    invoke-virtual {v1, v0, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 300
    :pswitch_7
    iget-object v0, p0, Lczz;->N:Ldaa;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v4, v0, Ldaa;->a:Lczz;

    invoke-virtual {v4}, Lczz;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0981

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0x120

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v0, Ldaa;->a:Lczz;

    invoke-virtual {v4}, Lczz;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0982

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0x121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v0, Ldaa;->a:Lczz;

    invoke-virtual {v4}, Lczz;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0983

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0x122

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v0, Ldaa;->a:Lczz;

    invoke-virtual {v4}, Lczz;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0984

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0x123

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v0, Ldaa;->a:Lczz;

    invoke-virtual {v4}, Lczz;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0985

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0x124

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    iget-object v1, v0, Ldaa;->a:Lczz;

    const v5, 0x7f0a0974

    invoke-virtual {v1, v5}, Lczz;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Llgr;->a(Ljava/lang/String;[Ljava/lang/String;)Llgr;

    move-result-object v1

    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "comment_id"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "comment_action"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v2, v0, Ldaa;->a:Lczz;

    invoke-virtual {v1, v2, v8}, Llgr;->a(Lu;I)V

    iget-object v0, v0, Ldaa;->a:Lczz;

    invoke-virtual {v0}, Lczz;->p()Lae;

    move-result-object v0

    const-string v2, "commentsdialog_delete_comment"

    invoke-virtual {v1, v0, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 303
    :pswitch_8
    iget-object v0, p0, Lczz;->N:Ldaa;

    invoke-static {v0, v3, v8, v5}, Ldaa;->a(Ldaa;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 307
    :pswitch_9
    iget-object v0, p0, Lczz;->N:Ldaa;

    const/4 v1, 0x2

    invoke-static {v0, v3, v8, v1}, Ldaa;->a(Ldaa;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 311
    :pswitch_a
    iget-object v0, p0, Lczz;->N:Ldaa;

    const/16 v1, 0x10

    invoke-static {v0, v3, v8, v1}, Ldaa;->a(Ldaa;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 315
    :pswitch_b
    iget-object v0, p0, Lczz;->N:Ldaa;

    const/4 v1, 0x4

    invoke-static {v0, v3, v8, v1}, Ldaa;->a(Ldaa;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 319
    :pswitch_c
    iget-object v0, p0, Lczz;->N:Ldaa;

    const/16 v1, 0x11

    invoke-static {v0, v3, v8, v1}, Ldaa;->a(Ldaa;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 323
    :pswitch_d
    iget-object v0, p0, Lczz;->N:Ldaa;

    const-string v1, "comment_author_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "comment_author_name"

    .line 324
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 323
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "comment_author_id"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "comment_author_name"

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ldyr;

    invoke-direct {v1, v2, v3}, Ldyr;-><init>(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, v0, Ldaa;->a:Lczz;

    invoke-virtual {v1, v2, v8}, Ldyr;->a(Lu;I)V

    iget-object v0, v0, Ldaa;->a:Lczz;

    invoke-virtual {v0}, Lczz;->p()Lae;

    move-result-object v0

    const-string v2, "commentsdialog_block_person"

    invoke-virtual {v1, v0, v2}, Ldyr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 265
    :pswitch_data_0
    .packed-switch 0x117
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 361
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 117
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 119
    invoke-virtual {p0}, Lczz;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lczz;->R:I

    .line 122
    if-eqz p1, :cond_0

    const-string v0, "blocked_gaia_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const-string v0, "blocked_gaia_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lczz;->O:Ljava/util/ArrayList;

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lczz;->O:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 218
    iget-object v0, p0, Lczz;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v8

    .line 219
    const/4 v0, 0x0

    .line 220
    invoke-virtual {p0}, Lczz;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account_id"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 222
    const-string v2, "commentsdialog_delete_comment"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 223
    invoke-virtual {p0}, Lczz;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "view_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 224
    invoke-virtual {p0}, Lczz;->n()Lz;

    move-result-object v0

    .line 226
    invoke-interface {v8}, Lddl;->o()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "comment_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 227
    invoke-interface {v8}, Lddl;->k()Ljava/lang/String;

    move-result-object v4

    .line 224
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 237
    :cond_0
    :goto_0
    iget-object v1, p0, Lczz;->S:Ldab;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 238
    iget-object v1, p0, Lczz;->S:Ldab;

    invoke-interface {v1, v0}, Ldab;->a(Ljava/lang/Integer;)V

    .line 241
    :cond_1
    return-void

    .line 228
    :cond_2
    const-string v2, "commentsdialog_delete_shape"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 229
    invoke-virtual {p0}, Lczz;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "view_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 231
    invoke-virtual {p0}, Lczz;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 233
    invoke-interface {v8}, Lddl;->o()J

    move-result-wide v2

    invoke-interface {v8}, Lddl;->j()Ljava/lang/String;

    move-result-object v4

    const-string v5, "shape_id"

    .line 234
    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 235
    invoke-interface {v8}, Lddl;->k()Ljava/lang/String;

    move-result-object v8

    const-string v9, "permanent_delete"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v9, 0x1

    .line 230
    :goto_1
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IJLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 235
    :cond_3
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 5

    .prologue
    .line 337
    check-cast p1, Landroid/os/Bundle;

    .line 338
    invoke-virtual {p0}, Lczz;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 340
    const-string v0, "comment_author_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 341
    const-string v2, "comment_author_name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 342
    const-string v3, "g:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 344
    :goto_0
    invoke-virtual {p0}, Lczz;->n()Lz;

    move-result-object v3

    const/4 v4, 0x1

    .line 343
    invoke-static {v3, v1, v0, v2, v4}, Lcom/google/android/apps/plus/service/EsService;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    .line 345
    iget-object v1, p0, Lczz;->S:Ldab;

    if-eqz v1, :cond_0

    .line 346
    iget-object v1, p0, Lczz;->S:Ldab;

    invoke-interface {v1, v0}, Ldab;->a(Ljava/lang/Integer;)V

    .line 348
    :cond_0
    iget-object v0, p0, Lczz;->P:Ldgi;

    sget-object v1, Lhmv;->bM:Lhmv;

    invoke-interface {v0, v1}, Ldgi;->a(Lhmv;)V

    .line 349
    return-void

    .line 342
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;)V
    .locals 13

    .prologue
    const/4 v9, 0x5

    const/4 v12, 0x0

    .line 142
    invoke-virtual {p0}, Lczz;->o()Landroid/content/res/Resources;

    move-result-object v1

    .line 143
    iget-object v0, p0, Lczz;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v2

    .line 144
    iget-object v0, p0, Lczz;->au:Llnh;

    const-class v3, Lhei;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v3, p0, Lczz;->R:I

    .line 145
    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-virtual {p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    .line 147
    invoke-interface {v2}, Lddl;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    .line 148
    invoke-virtual {p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->d()Ljava/lang/String;

    move-result-object v5

    .line 149
    invoke-virtual {p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->e()Ljava/lang/String;

    move-result-object v6

    .line 150
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 151
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 153
    invoke-virtual {p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->i()Z

    move-result v9

    .line 154
    invoke-virtual {p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->k()Z

    move-result v10

    .line 157
    if-nez v10, :cond_0

    .line 158
    if-eqz v9, :cond_4

    const v0, 0x7f0a0978

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    const/16 v0, 0x117

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_0
    if-eqz v3, :cond_5

    .line 164
    const v0, 0x7f0a097d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    const/16 v0, 0x118

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_1
    :goto_1
    if-nez v10, :cond_3

    if-nez v4, :cond_2

    if-eqz v3, :cond_3

    .line 190
    :cond_2
    const v0, 0x7f0a097b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    const/16 v0, 0x11c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 195
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 197
    iget-object v0, p0, Lczz;->Q:Ldeo;

    invoke-virtual {v0, v12}, Ldeo;->c(Z)V

    .line 199
    if-eqz v10, :cond_8

    const v0, 0x7f0a0973

    :goto_2
    invoke-virtual {p0, v0}, Lczz;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Llgr;->a(Ljava/lang/String;[Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 203
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "comment_action"

    invoke-virtual {v1, v3, v8}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 204
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "comment_id"

    invoke-virtual {p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "comment_content"

    .line 206
    invoke-virtual {p1}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->g()Landroid/text/Spanned;

    move-result-object v4

    invoke-static {v4}, Llht;->a(Landroid/text/Spanned;)[B

    move-result-object v4

    .line 205
    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 207
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "plusone_by_me"

    invoke-virtual {v1, v3, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 208
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "photo_id"

    invoke-interface {v2}, Lddl;->o()J

    move-result-wide v8

    invoke-virtual {v1, v3, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 209
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_author_name"

    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_author_id"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-virtual {v0, p0, v12}, Llgr;->a(Lu;I)V

    .line 213
    invoke-virtual {p0}, Lczz;->p()Lae;

    move-result-object v1

    const-string v2, "commentsdialog_delete_comment"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 214
    return-void

    .line 158
    :cond_4
    const v0, 0x7f0a0975

    goto/16 :goto_0

    .line 167
    :cond_5
    if-eqz v10, :cond_6

    .line 168
    const v0, 0x7f0a0980

    .line 169
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 168
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    const/16 v0, 0x11f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    const v0, 0x7f0a097a

    .line 172
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 171
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    const/16 v0, 0x119

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    if-eqz v4, :cond_1

    iget-object v0, p0, Lczz;->O:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 175
    const v0, 0x7f0a0986

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    aput-object v6, v11, v12

    invoke-virtual {v1, v0, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    const/16 v0, 0x125

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 181
    :cond_6
    const v0, 0x7f0a0979

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    if-eqz v4, :cond_7

    .line 183
    const/16 v0, 0x11b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 185
    :cond_7
    const/16 v0, 0x11a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 199
    :cond_8
    const v0, 0x7f0a0972

    goto/16 :goto_2
.end method

.method public a(Ldab;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lczz;->S:Ldab;

    .line 138
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lczz;->O:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 353
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 111
    iget-object v0, p0, Lczz;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Lczz;->P:Ldgi;

    .line 112
    iget-object v0, p0, Lczz;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lczz;->Q:Ldeo;

    .line 113
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 357
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 131
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 132
    const-string v0, "blocked_gaia_ids"

    iget-object v1, p0, Lczz;->O:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 133
    return-void
.end method
