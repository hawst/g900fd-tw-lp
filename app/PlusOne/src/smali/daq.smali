.class public Ldaq;
.super Ldct;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldct",
        "<",
        "Ldeb;",
        ">;"
    }
.end annotation


# instance fields
.field private P:Ldeb;

.field private Q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 22
    invoke-direct {p0}, Ldct;-><init>()V

    .line 34
    new-instance v0, Ldep;

    iget-object v1, p0, Ldaq;->av:Llqm;

    new-instance v2, Ldar;

    invoke-direct {v2, p0}, Ldar;-><init>(Ldaq;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 78
    return-void
.end method

.method static synthetic a(Ldaq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Ldaq;->Q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ldaq;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Ldaq;->Q:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Ldaq;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Ldaq;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic b(Ldaq;)Ldeb;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Ldaq;->P:Ldeb;

    return-object v0
.end method

.method static synthetic b(Ldaq;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Ldaq;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic c(Ldaq;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0}, Ldaq;->U()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1}, Ldct;->a(Landroid/os/Bundle;)V

    .line 59
    iget-object v0, p0, Ldaq;->Q:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 60
    invoke-virtual {p0}, Ldaq;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "tile_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldaq;->Q:Ljava/lang/String;

    .line 62
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 66
    iget-object v1, p0, Ldaq;->Q:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 67
    invoke-virtual {p0}, Ldaq;->w()Lbb;

    move-result-object v1

    invoke-virtual {p0}, Ldaq;->k()Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Ldas;

    invoke-direct {v3, p0}, Ldas;-><init>(Ldaq;)V

    invoke-virtual {v1, v0, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 69
    const/4 v0, 0x1

    .line 71
    :cond_0
    return v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Ldct;->c(Landroid/os/Bundle;)V

    .line 53
    iget-object v0, p0, Ldaq;->au:Llnh;

    const-class v1, Ldeb;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeb;

    iput-object v0, p0, Ldaq;->P:Ldeb;

    .line 54
    return-void
.end method
