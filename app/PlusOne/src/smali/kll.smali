.class public final Lkll;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/View;

.field private final c:I

.field private final d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private final e:Landroid/widget/TextView;

.field private final f:[Lcom/google/android/libraries/social/media/ui/MediaView;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lkll;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lkll;->b:Landroid/view/View;

    .line 37
    iput-boolean p3, p0, Lkll;->g:Z

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d011f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lkll;->c:I

    .line 39
    const v0, 0x7f10058c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lkll;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 40
    const v0, 0x7f10058d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lkll;->e:Landroid/widget/TextView;

    .line 42
    new-array v2, v4, [Lcom/google/android/libraries/social/media/ui/MediaView;

    const v0, 0x7f10058e

    .line 43
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    aput-object v0, v2, v1

    const/4 v3, 0x1

    const v0, 0x7f10058f

    .line 44
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    aput-object v0, v2, v3

    const/4 v3, 0x2

    const v0, 0x7f100590

    .line 45
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    aput-object v0, v2, v3

    iput-object v2, p0, Lkll;->f:[Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 47
    iget-object v2, p0, Lkll;->f:[Lcom/google/android/libraries/social/media/ui/MediaView;

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v3, v2, v0

    .line 48
    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(Z)V

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    iget-object v0, p0, Lkll;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    .line 53
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 55
    if-eqz p3, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p2, v0}, Landroid/view/View;->setY(F)V

    .line 56
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 60
    :goto_2
    return-void

    .line 55
    :cond_1
    iget v0, p0, Lkll;->c:I

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_1

    .line 58
    :cond_2
    if-eqz p3, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    const/16 v0, 0x8

    goto :goto_3
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 63
    iget-boolean v0, p0, Lkll;->g:Z

    if-nez v0, :cond_0

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkll;->g:Z

    .line 69
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lkll;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget-object v0, p0, Lkll;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lhej;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhej;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lizr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lkll;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const-string v1, "gaia_id"

    .line 98
    invoke-interface {p1, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "profile_photo_url"

    invoke-interface {p1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lkll;->a:Landroid/content/Context;

    const v1, 0x7f0a0261

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 103
    :cond_0
    iget-object v0, p0, Lkll;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    invoke-virtual {p0, p3}, Lkll;->a(Ljava/util/List;)V

    .line 106
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lizr;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 110
    .line 111
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 112
    iget-object v0, p0, Lkll;->f:[Lcom/google/android/libraries/social/media/ui/MediaView;

    add-int/lit8 v0, v5, -0x3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v2, v0

    move v1, v3

    :goto_0
    if-ge v2, v5, :cond_1

    .line 113
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizr;

    .line 114
    iget-object v6, p0, Lkll;->f:[Lcom/google/android/libraries/social/media/ui/MediaView;

    add-int/lit8 v4, v1, 0x1

    aget-object v1, v6, v1

    .line 115
    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 116
    invoke-interface {v0}, Lizr;->f()Lizu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 112
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v1, v4

    goto :goto_0

    .line 119
    :goto_1
    iget-object v1, p0, Lkll;->f:[Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 120
    iget-object v2, p0, Lkll;->f:[Lcom/google/android/libraries/social/media/ui/MediaView;

    add-int/lit8 v1, v0, 0x1

    aget-object v0, v2, v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    move v0, v1

    goto :goto_1

    .line 122
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 78
    iget-boolean v0, p0, Lkll;->g:Z

    if-eqz v0, :cond_0

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkll;->g:Z

    .line 84
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lkll;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lkll;->c:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    iget-object v0, p0, Lkll;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lkll;->g:Z

    return v0
.end method
