.class public final Lkif;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkij;
.implements Llqj;
.implements Llql;
.implements Llrd;
.implements Llrg;


# instance fields
.field private final a:Lkgn;

.field private b:Lat;

.field private c:Lkig;

.field private d:Lcom/google/android/libraries/social/settings/PreferenceScreen;


# direct methods
.method public constructor <init>(Lkgn;Llqr;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lkif;->a:Lkgn;

    .line 75
    iget-object v0, p0, Lkif;->a:Lkgn;

    instance-of v0, v0, Lkig;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lkif;->a:Lkgn;

    check-cast v0, Lkig;

    iput-object v0, p0, Lkif;->c:Lkig;

    .line 82
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 83
    return-void

    .line 78
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "PreferenceFragment must implement ProviderManagerCallbacks"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()Lcom/google/android/libraries/social/settings/PreferenceScreen;
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lkif;->d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PreferenceScreen cannot be accessed before OnCreateView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    iget-object v0, p0, Lkif;->d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 110
    if-nez p2, :cond_0

    .line 111
    iget-object v0, p0, Lkif;->a:Lkgn;

    invoke-virtual {v0}, Lkgn;->q()Lae;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    iput-object v0, p0, Lkif;->b:Lat;

    .line 114
    iget-object v0, p0, Lkif;->c:Lkig;

    invoke-interface {v0}, Lkig;->a()V

    .line 116
    iget-object v0, p0, Lkif;->b:Lat;

    invoke-virtual {v0}, Lat;->b()I

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lkif;->b:Lat;

    .line 119
    :cond_0
    return-void
.end method

.method public a(Lu;)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lkif;->b:Lat;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    .line 131
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lkif;->a:Lkgn;

    invoke-virtual {v0}, Lkgn;->aa()Lkib;

    move-result-object v0

    iget-object v1, p0, Lkif;->a:Lkgn;

    iget-object v1, v1, Lkgn;->N:Llnl;

    .line 100
    invoke-virtual {v0, v1}, Lkib;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/settings/PreferenceScreen;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lkif;->a:Lkgn;

    invoke-virtual {v1, v0}, Lkgn;->a(Lcom/google/android/libraries/social/settings/PreferenceScreen;)V

    .line 102
    iput-object v0, p0, Lkif;->d:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    .line 103
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method
