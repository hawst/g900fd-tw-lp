.class public final Lobl;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:I

.field private f:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Loxq;-><init>()V

    .line 110
    const/high16 v0, -0x80000000

    iput v0, p0, Lobl;->e:I

    .line 113
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lobl;->f:[Ljava/lang/String;

    .line 90
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 144
    .line 145
    iget-object v0, p0, Lobl;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 146
    const/4 v0, 0x1

    iget-object v2, p0, Lobl;->a:Ljava/lang/String;

    .line 147
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 149
    :goto_0
    iget-object v2, p0, Lobl;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 150
    const/4 v2, 0x2

    iget-object v3, p0, Lobl;->b:Ljava/lang/String;

    .line 151
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 153
    :cond_0
    iget-object v2, p0, Lobl;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 154
    const/4 v2, 0x3

    iget-object v3, p0, Lobl;->c:Ljava/lang/String;

    .line 155
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 157
    :cond_1
    iget-object v2, p0, Lobl;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 158
    const/4 v2, 0x4

    iget-object v3, p0, Lobl;->d:Ljava/lang/Integer;

    .line 159
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 161
    :cond_2
    iget v2, p0, Lobl;->e:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    .line 162
    const/4 v2, 0x5

    iget v3, p0, Lobl;->e:I

    .line 163
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 165
    :cond_3
    iget-object v2, p0, Lobl;->f:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lobl;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 167
    iget-object v3, p0, Lobl;->f:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 169
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 171
    :cond_4
    add-int/2addr v0, v2

    .line 172
    iget-object v1, p0, Lobl;->f:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 174
    :cond_5
    iget-object v1, p0, Lobl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    iput v0, p0, Lobl;->ai:I

    .line 176
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lobl;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 184
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 185
    sparse-switch v0, :sswitch_data_0

    .line 189
    iget-object v1, p0, Lobl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 190
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lobl;->ah:Ljava/util/List;

    .line 193
    :cond_1
    iget-object v1, p0, Lobl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    :sswitch_0
    return-object p0

    .line 200
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lobl;->a:Ljava/lang/String;

    goto :goto_0

    .line 204
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lobl;->b:Ljava/lang/String;

    goto :goto_0

    .line 208
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lobl;->c:Ljava/lang/String;

    goto :goto_0

    .line 212
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lobl;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 216
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 217
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 223
    :cond_2
    iput v0, p0, Lobl;->e:I

    goto :goto_0

    .line 225
    :cond_3
    iput v3, p0, Lobl;->e:I

    goto :goto_0

    .line 230
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 231
    iget-object v0, p0, Lobl;->f:[Ljava/lang/String;

    array-length v0, v0

    .line 232
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 233
    iget-object v2, p0, Lobl;->f:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 234
    iput-object v1, p0, Lobl;->f:[Ljava/lang/String;

    .line 235
    :goto_1
    iget-object v1, p0, Lobl;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 236
    iget-object v1, p0, Lobl;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 237
    invoke-virtual {p1}, Loxn;->a()I

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 240
    :cond_4
    iget-object v1, p0, Lobl;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 185
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 118
    iget-object v0, p0, Lobl;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x1

    iget-object v1, p0, Lobl;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 121
    :cond_0
    iget-object v0, p0, Lobl;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 122
    const/4 v0, 0x2

    iget-object v1, p0, Lobl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 124
    :cond_1
    iget-object v0, p0, Lobl;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 125
    const/4 v0, 0x3

    iget-object v1, p0, Lobl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 127
    :cond_2
    iget-object v0, p0, Lobl;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 128
    const/4 v0, 0x4

    iget-object v1, p0, Lobl;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 130
    :cond_3
    iget v0, p0, Lobl;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 131
    const/4 v0, 0x5

    iget v1, p0, Lobl;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 133
    :cond_4
    iget-object v0, p0, Lobl;->f:[Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 134
    iget-object v1, p0, Lobl;->f:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 135
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_5
    iget-object v0, p0, Lobl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 140
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lobl;->a(Loxn;)Lobl;

    move-result-object v0

    return-object v0
.end method
