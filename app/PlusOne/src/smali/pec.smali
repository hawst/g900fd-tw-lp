.class public final Lpec;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput v0, p0, Lpec;->a:I

    .line 16
    iput v0, p0, Lpec;->b:I

    .line 19
    iput v0, p0, Lpec;->c:I

    .line 22
    iput v0, p0, Lpec;->d:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 45
    const/4 v0, 0x0

    .line 46
    iget v1, p0, Lpec;->a:I

    if-eq v1, v3, :cond_0

    .line 47
    const/4 v0, 0x1

    iget v1, p0, Lpec;->a:I

    .line 48
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 50
    :cond_0
    iget v1, p0, Lpec;->b:I

    if-eq v1, v3, :cond_1

    .line 51
    const/4 v1, 0x2

    iget v2, p0, Lpec;->b:I

    .line 52
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_1
    iget v1, p0, Lpec;->c:I

    if-eq v1, v3, :cond_2

    .line 55
    const/4 v1, 0x3

    iget v2, p0, Lpec;->c:I

    .line 56
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_2
    iget v1, p0, Lpec;->d:I

    if-eq v1, v3, :cond_3

    .line 59
    const/4 v1, 0x4

    iget v2, p0, Lpec;->d:I

    .line 60
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_3
    iget-object v1, p0, Lpec;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    iput v0, p0, Lpec;->ai:I

    .line 64
    return v0
.end method

.method public a(Loxn;)Lpec;
    .locals 7

    .prologue
    const/16 v6, 0x6e

    const/16 v5, 0x64

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 73
    sparse-switch v0, :sswitch_data_0

    .line 77
    iget-object v1, p0, Lpec;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpec;->ah:Ljava/util/List;

    .line 81
    :cond_1
    iget-object v1, p0, Lpec;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    :sswitch_0
    return-object p0

    .line 88
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 89
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    if-ne v0, v5, :cond_3

    .line 100
    :cond_2
    iput v0, p0, Lpec;->a:I

    goto :goto_0

    .line 102
    :cond_3
    iput v2, p0, Lpec;->a:I

    goto :goto_0

    .line 107
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 108
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0x50

    if-eq v0, v1, :cond_4

    const/16 v1, 0x51

    if-eq v0, v1, :cond_4

    if-eq v0, v5, :cond_4

    const/16 v1, 0x65

    if-eq v0, v1, :cond_4

    const/16 v1, 0x66

    if-eq v0, v1, :cond_4

    const/16 v1, 0x67

    if-eq v0, v1, :cond_4

    const/16 v1, 0x68

    if-eq v0, v1, :cond_4

    const/16 v1, 0x77

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_4

    const/16 v1, 0x69

    if-eq v0, v1, :cond_4

    const/16 v1, 0x81

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_4

    if-eq v0, v6, :cond_4

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x70

    if-eq v0, v1, :cond_4

    const/16 v1, 0x71

    if-eq v0, v1, :cond_4

    const/16 v1, 0x72

    if-eq v0, v1, :cond_4

    const/16 v1, 0x73

    if-eq v0, v1, :cond_4

    const/16 v1, 0x74

    if-eq v0, v1, :cond_4

    const/16 v1, 0x76

    if-eq v0, v1, :cond_4

    const/16 v1, 0x78

    if-eq v0, v1, :cond_4

    const/16 v1, 0x79

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x80

    if-eq v0, v1, :cond_4

    const/16 v1, 0x82

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x83

    if-eq v0, v1, :cond_4

    const/16 v1, 0x84

    if-eq v0, v1, :cond_4

    const/16 v1, 0x85

    if-eq v0, v1, :cond_4

    const/16 v1, 0x86

    if-eq v0, v1, :cond_4

    const/16 v1, 0x87

    if-eq v0, v1, :cond_4

    const/16 v1, 0x88

    if-eq v0, v1, :cond_4

    const/16 v1, 0x89

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_4

    const/16 v1, 0x90

    if-eq v0, v1, :cond_4

    const/16 v1, 0x91

    if-eq v0, v1, :cond_4

    const/16 v1, 0x92

    if-eq v0, v1, :cond_4

    const/16 v1, 0x93

    if-eq v0, v1, :cond_4

    const/16 v1, 0x94

    if-eq v0, v1, :cond_4

    const/16 v1, 0x95

    if-eq v0, v1, :cond_4

    const/16 v1, 0x96

    if-eq v0, v1, :cond_4

    const/16 v1, 0x97

    if-eq v0, v1, :cond_4

    const/16 v1, 0x98

    if-eq v0, v1, :cond_4

    const/16 v1, 0x99

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xaa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xab

    if-eq v0, v1, :cond_4

    const/16 v1, 0xac

    if-eq v0, v1, :cond_4

    const/16 v1, 0xad

    if-eq v0, v1, :cond_4

    const/16 v1, 0xae

    if-eq v0, v1, :cond_4

    const/16 v1, 0xaf

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb0

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb7

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xca

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xce

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd0

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd1

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd2

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd3

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd4

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd5

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd6

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12d

    if-eq v0, v1, :cond_4

    const/16 v1, 0x75

    if-eq v0, v1, :cond_4

    const/16 v1, 0x190

    if-eq v0, v1, :cond_4

    const/16 v1, 0x191

    if-eq v0, v1, :cond_4

    const/16 v1, 0x192

    if-eq v0, v1, :cond_4

    const/16 v1, 0x193

    if-eq v0, v1, :cond_4

    const/16 v1, 0x194

    if-eq v0, v1, :cond_4

    const/16 v1, 0x195

    if-eq v0, v1, :cond_4

    const/16 v1, 0x196

    if-eq v0, v1, :cond_4

    const/16 v1, 0x199

    if-eq v0, v1, :cond_4

    const/16 v1, 0x197

    if-eq v0, v1, :cond_4

    const/16 v1, 0x198

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3e9

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ea

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ec

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ed

    if-eq v0, v1, :cond_4

    const/16 v1, 0x3ee

    if-ne v0, v1, :cond_5

    .line 244
    :cond_4
    iput v0, p0, Lpec;->b:I

    goto/16 :goto_0

    .line 246
    :cond_5
    iput v2, p0, Lpec;->b:I

    goto/16 :goto_0

    .line 251
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 252
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    const/16 v1, 0xa

    if-ne v0, v1, :cond_7

    .line 256
    :cond_6
    iput v0, p0, Lpec;->c:I

    goto/16 :goto_0

    .line 258
    :cond_7
    iput v2, p0, Lpec;->c:I

    goto/16 :goto_0

    .line 263
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 264
    if-eq v0, v6, :cond_8

    if-eq v0, v5, :cond_8

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_8

    const/16 v1, 0x32

    if-eq v0, v1, :cond_8

    const/16 v1, 0x28

    if-eq v0, v1, :cond_8

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_8

    const/16 v1, 0x14

    if-ne v0, v1, :cond_9

    .line 271
    :cond_8
    iput v0, p0, Lpec;->d:I

    goto/16 :goto_0

    .line 273
    :cond_9
    iput v6, p0, Lpec;->d:I

    goto/16 :goto_0

    .line 73
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 27
    iget v0, p0, Lpec;->a:I

    if-eq v0, v2, :cond_0

    .line 28
    const/4 v0, 0x1

    iget v1, p0, Lpec;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 30
    :cond_0
    iget v0, p0, Lpec;->b:I

    if-eq v0, v2, :cond_1

    .line 31
    const/4 v0, 0x2

    iget v1, p0, Lpec;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 33
    :cond_1
    iget v0, p0, Lpec;->c:I

    if-eq v0, v2, :cond_2

    .line 34
    const/4 v0, 0x3

    iget v1, p0, Lpec;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 36
    :cond_2
    iget v0, p0, Lpec;->d:I

    if-eq v0, v2, :cond_3

    .line 37
    const/4 v0, 0x4

    iget v1, p0, Lpec;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 39
    :cond_3
    iget-object v0, p0, Lpec;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 41
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpec;->a(Loxn;)Lpec;

    move-result-object v0

    return-object v0
.end method
