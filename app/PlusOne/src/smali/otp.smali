.class public final Lotp;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Louq;

.field private c:Lovz;

.field private d:Lopw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 539
    invoke-direct {p0}, Loxq;-><init>()V

    .line 544
    iput-object v0, p0, Lotp;->b:Louq;

    .line 547
    iput-object v0, p0, Lotp;->c:Lovz;

    .line 550
    iput-object v0, p0, Lotp;->d:Lopw;

    .line 539
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 573
    const/4 v0, 0x0

    .line 574
    iget-object v1, p0, Lotp;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 575
    const/4 v0, 0x1

    iget-object v1, p0, Lotp;->a:Ljava/lang/Boolean;

    .line 576
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 578
    :cond_0
    iget-object v1, p0, Lotp;->b:Louq;

    if-eqz v1, :cond_1

    .line 579
    const/4 v1, 0x2

    iget-object v2, p0, Lotp;->b:Louq;

    .line 580
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 582
    :cond_1
    iget-object v1, p0, Lotp;->c:Lovz;

    if-eqz v1, :cond_2

    .line 583
    const/4 v1, 0x3

    iget-object v2, p0, Lotp;->c:Lovz;

    .line 584
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 586
    :cond_2
    iget-object v1, p0, Lotp;->d:Lopw;

    if-eqz v1, :cond_3

    .line 587
    const/4 v1, 0x4

    iget-object v2, p0, Lotp;->d:Lopw;

    .line 588
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 590
    :cond_3
    iget-object v1, p0, Lotp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 591
    iput v0, p0, Lotp;->ai:I

    .line 592
    return v0
.end method

.method public a(Loxn;)Lotp;
    .locals 2

    .prologue
    .line 600
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 601
    sparse-switch v0, :sswitch_data_0

    .line 605
    iget-object v1, p0, Lotp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 606
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lotp;->ah:Ljava/util/List;

    .line 609
    :cond_1
    iget-object v1, p0, Lotp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 611
    :sswitch_0
    return-object p0

    .line 616
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lotp;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 620
    :sswitch_2
    iget-object v0, p0, Lotp;->b:Louq;

    if-nez v0, :cond_2

    .line 621
    new-instance v0, Louq;

    invoke-direct {v0}, Louq;-><init>()V

    iput-object v0, p0, Lotp;->b:Louq;

    .line 623
    :cond_2
    iget-object v0, p0, Lotp;->b:Louq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 627
    :sswitch_3
    iget-object v0, p0, Lotp;->c:Lovz;

    if-nez v0, :cond_3

    .line 628
    new-instance v0, Lovz;

    invoke-direct {v0}, Lovz;-><init>()V

    iput-object v0, p0, Lotp;->c:Lovz;

    .line 630
    :cond_3
    iget-object v0, p0, Lotp;->c:Lovz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 634
    :sswitch_4
    iget-object v0, p0, Lotp;->d:Lopw;

    if-nez v0, :cond_4

    .line 635
    new-instance v0, Lopw;

    invoke-direct {v0}, Lopw;-><init>()V

    iput-object v0, p0, Lotp;->d:Lopw;

    .line 637
    :cond_4
    iget-object v0, p0, Lotp;->d:Lopw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 601
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 555
    iget-object v0, p0, Lotp;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 556
    const/4 v0, 0x1

    iget-object v1, p0, Lotp;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 558
    :cond_0
    iget-object v0, p0, Lotp;->b:Louq;

    if-eqz v0, :cond_1

    .line 559
    const/4 v0, 0x2

    iget-object v1, p0, Lotp;->b:Louq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 561
    :cond_1
    iget-object v0, p0, Lotp;->c:Lovz;

    if-eqz v0, :cond_2

    .line 562
    const/4 v0, 0x3

    iget-object v1, p0, Lotp;->c:Lovz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 564
    :cond_2
    iget-object v0, p0, Lotp;->d:Lopw;

    if-eqz v0, :cond_3

    .line 565
    const/4 v0, 0x4

    iget-object v1, p0, Lotp;->d:Lopw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 567
    :cond_3
    iget-object v0, p0, Lotp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 569
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 535
    invoke-virtual {p0, p1}, Lotp;->a(Loxn;)Lotp;

    move-result-object v0

    return-object v0
.end method
