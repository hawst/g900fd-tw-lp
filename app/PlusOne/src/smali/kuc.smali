.class public final Lkuc;
.super Lloj;
.source "PG"


# instance fields
.field private Q:Landroid/content/Context;

.field private R:I

.field private S:Landroid/widget/Button;

.field private T:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0}, Lloj;-><init>()V

    .line 70
    new-instance v0, Lhmg;

    sget-object v1, Lomv;->o:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lkuc;->O:Llnh;

    .line 71
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 73
    new-instance v0, Lhmf;

    iget-object v1, p0, Lkuc;->P:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 74
    return-void
.end method

.method public static U()Lt;
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lkuc;

    invoke-direct {v0}, Lkuc;-><init>()V

    return-object v0
.end method

.method static synthetic a(Lkuc;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lkuc;->Q:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lkuc;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lkuc;->R:I

    return v0
.end method

.method static synthetic c(Lkuc;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lkuc;->T:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic d(Lkuc;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lkuc;->S:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    .line 84
    iget-object v0, p0, Lkuc;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Lkuc;->R:I

    .line 86
    if-nez p1, :cond_0

    .line 87
    new-instance v1, Lkwi;

    iget-object v0, p0, Lkuc;->Q:Landroid/content/Context;

    iget v2, p0, Lkuc;->R:I

    const/4 v3, 0x2

    invoke-direct {v1, v0, v2, v3}, Lkwi;-><init>(Landroid/content/Context;II)V

    .line 89
    iget-object v0, p0, Lkuc;->Q:Landroid/content/Context;

    const-class v2, Lhoc;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 92
    :cond_0
    iget-object v0, p0, Lkuc;->Q:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 93
    const v1, 0x7f0400a3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 96
    const v0, 0x7f100139

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 97
    iget-object v2, p0, Lkuc;->N:Llnl;

    const-string v3, "plus_profile_tab"

    const-string v4, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v2, v3, v4}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 98
    new-instance v3, Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lkuc;->N:Llnl;

    const v5, 0x7f0a04d2

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    .line 99
    invoke-virtual {v4, v5, v6}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 101
    invoke-static {v3}, Llju;->a(Landroid/text/Spannable;)V

    .line 102
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    invoke-static {}, Lljw;->a()Lljw;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 105
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lkuc;->Q:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 106
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a04d4

    new-instance v2, Lkue;

    invoke-direct {v2, p0}, Lkue;-><init>(Lkuc;)V

    .line 107
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a04d3

    new-instance v2, Lkud;

    invoke-direct {v2, p0}, Lkud;-><init>(Lkuc;)V

    .line 136
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 157
    invoke-super {p0}, Lloj;->g()V

    .line 159
    invoke-virtual {p0}, Lkuc;->c()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 160
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lkuc;->S:Landroid/widget/Button;

    .line 161
    iget-object v1, p0, Lkuc;->S:Landroid/widget/Button;

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->L:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v1, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 163
    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lkuc;->T:Landroid/widget/Button;

    .line 164
    iget-object v0, p0, Lkuc;->T:Landroid/widget/Button;

    new-instance v1, Lhmk;

    sget-object v2, Lomv;->K:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 166
    return-void
.end method

.method public k(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 79
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lkuc;->N:Llnl;

    const v2, 0x7f0900a9

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkuc;->Q:Landroid/content/Context;

    .line 80
    return-void
.end method
