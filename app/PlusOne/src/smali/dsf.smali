.class public final Ldsf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:[B

.field private static b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/Object;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 164
    const-string v1, "HIGH"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 167
    sput-object v0, Ldsf;->b:Ljava/util/List;

    const-string v1, "HIGH"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    sget-object v0, Ldsf;->b:Ljava/util/List;

    const-string v1, "LOW"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 171
    sput-object v0, Ldsf;->c:Ljava/util/List;

    const-string v1, "READ"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    sget-object v0, Ldsf;->c:Ljava/util/List;

    const-string v1, "UNREAD"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    new-array v0, v3, [B

    sput-object v0, Ldsf;->a:[B

    .line 384
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 388
    sput-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "ASPEN_INVITE"

    const/16 v2, 0x4a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "BIRTHDAY_WISH"

    const/16 v2, 0x3f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_CONTACT_JOINED"

    const/16 v2, 0x45

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_DIGESTED_ADD"

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_EXPLICIT_INVITE"

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_INVITE_REQUEST"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_INVITEE_JOINED_ES"

    const/16 v2, 0x26

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_MEMBER_JOINED_ES"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_PERSONAL_ADD"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_RECIPROCATING_ADD"

    const/16 v2, 0x27

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_RECOMMEND_PEOPLE"

    const/16 v2, 0x42

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "CIRCLE_STATUS_CHANGE"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "DIGEST_SWEEP"

    const/16 v2, 0x46

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE_ADD_ADMIN"

    const/16 v2, 0x22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE_REMOVE_ADMIN"

    const/16 v2, 0x23

    .line 403
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 402
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE_TRANSFER_OWNERSHIP"

    const/16 v2, 0x24

    .line 405
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 404
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_BEFORE_REMINDER"

    const/16 v2, 0x3b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_CHANGE"

    const/16 v2, 0x35

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_CHECKIN"

    const/16 v2, 0x3a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_INVITE"

    const/16 v2, 0x2f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_INVITEE_CHANGE"

    const/16 v2, 0x39

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_PHOTOS_ADDED"

    const/16 v2, 0x3e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_PHOTOS_COLLECTION"

    const/16 v2, 0x38

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_PHOTOS_REMINDER"

    const/16 v2, 0x37

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_RSVP_CONFIRMATION"

    const/16 v2, 0x43

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_STARTING"

    const/16 v2, 0x36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "EVENTS_SEND_MESSAGE"

    const/16 v2, 0x4c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "GAMES_APPLICATION_MESSAGE"

    const/16 v2, 0xc

    .line 418
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 417
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "GAMES_INVITE_REQUEST"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "GAMES_ONEUP_NOTIFICATION"

    const/16 v2, 0x49

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "GAMES_PERSONAL_MESSAGE"

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "HANGOUT_INVITE"

    const/16 v2, 0x21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "MOBILE_NEW_CONVERSATION"

    const/16 v2, 0x1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "PHOTOS_CAMERASYNC_UPLOADED"

    const/16 v2, 0x12

    .line 425
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 424
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "PHOTOS_FACE_SUGGESTED"

    const/16 v2, 0x29

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "PHOTOS_PROFILE_PHOTO_SUGGESTED"

    const/16 v2, 0x44

    .line 428
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 427
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "PHOTOS_PROFILE_PHOTO_SUGGESTION_ACCEPTED"

    const/16 v2, 0x47

    .line 430
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 429
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "PHOTOS_TAG_ADDED_ON_PHOTO"

    const/16 v2, 0xd

    .line 432
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 431
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "PHOTOS_TAGGED_IN_PHOTO"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "QUESTIONS_ANSWERER_FOLLOWUP"

    const/16 v2, 0x1e

    .line 435
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 434
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "QUESTIONS_ASKER_FOLLOWUP"

    const/16 v2, 0x1f

    .line 437
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 436
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "QUESTIONS_DASHER_WELCOME"

    const/16 v2, 0x1b

    .line 439
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 438
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "QUESTIONS_REFERRAL"

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "QUESTIONS_REPLY"

    const/16 v2, 0x16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "QUESTIONS_UNANSWERED_QUESTION"

    const/16 v2, 0x1c

    .line 443
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 442
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SQUARE_ABUSE"

    const/16 v2, 0x4f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SQUARE_INVITE"

    const/16 v2, 0x30

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SQUARE_MEMBERSHIP_APPROVED"

    const/16 v2, 0x33

    .line 447
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 446
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SQUARE_MEMBERSHIP_REQUEST"

    const/16 v2, 0x34

    .line 449
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 448
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SQUARE_NAME_CHANGE"

    const/16 v2, 0x48

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SQUARE_NEW_MODERATOR"

    const/16 v2, 0x41

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SQUARE_SUBSCRIPTION"

    const/16 v2, 0x31

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_AT_REPLY"

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_FOLLOWUP"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_FOR_PHOTO_TAGGED"

    const/16 v2, 0x19

    .line 456
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 455
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_FOR_PHOTO_TAGGER"

    const/16 v2, 0x1a

    .line 458
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 457
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_NEW"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_ON_MENTION"

    const/16 v2, 0xe

    .line 461
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 460
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_LIKE"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_PLUSONE_COMMENT"

    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_PLUSONE_POST"

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_POST_AT_REPLY"

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_POST_FROM_UNCIRCLED"

    const/16 v2, 0x3d

    .line 467
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 466
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_POST_SHARED"

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_POST"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_POST_SUBSCRIBED"

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STREAM_RESHARE"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SYSTEM_CELEBRITY_SUGGESTIONS"

    const/16 v2, 0x2d

    .line 473
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 472
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SYSTEM_CONNECTED_SITES"

    const/16 v2, 0x2e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SYSTEM_DO_NOT_USE"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SYSTEM_FRIEND_SUGGESTIONS"

    const/16 v2, 0x2c

    .line 477
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 476
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SYSTEM_INVITE"

    const/16 v2, 0x25

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SYSTEM_TOOLTIP"

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SYSTEM_WELCOME"

    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "TARGET_SHARED"

    const/16 v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "UNKNOWN_NOTIFICATION_TYPE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "PHOTOS_NEW_PHOTO_ADDED"

    const/16 v2, 0x61

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "SYSTEM_SOCEND_ANNOUNCEMENT"

    const/16 v2, 0x65

    .line 485
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 484
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "USER_LOCATION_SHARE"

    const/16 v2, 0x69

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "USER_LOCATION_SILENT_SHARE"

    const/16 v2, 0x6c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "STORIES_NEW_STORY"

    const/16 v2, 0x63

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "ENGAGE_POSTS_FROM_CLOSE_TIES"

    const/16 v2, 0x6a

    .line 490
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 489
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    sget-object v0, Ldsf;->d:Ljava/util/Map;

    const-string v1, "PHOTOS_PHOTO_EDIT_COMPLETE"

    const/16 v2, 0x6f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldsf;->e:Ljava/lang/Object;

    .line 631
    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "guns"

    aput-object v2, v1, v3

    const-string v2, "read_state=0"

    aput-object v2, v1, v5

    .line 632
    invoke-static {v0, v1}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldsf;->f:Ljava/lang/String;

    .line 635
    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "guns"

    aput-object v2, v1, v3

    const-string v2, "read_state=1"

    aput-object v2, v1, v5

    .line 636
    invoke-static {v0, v1}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldsf;->g:Ljava/lang/String;

    .line 639
    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "guns"

    aput-object v2, v1, v3

    const-string v2, "read_state=0 AND pending_read=0"

    aput-object v2, v1, v5

    .line 640
    invoke-static {v0, v1}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldsf;->h:Ljava/lang/String;

    .line 643
    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "guns"

    aput-object v2, v1, v3

    const-string v2, "pending_read!=0"

    aput-object v2, v1, v5

    .line 644
    invoke-static {v0, v1}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldsf;->i:Ljava/lang/String;

    .line 647
    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s AND %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "guns"

    aput-object v2, v1, v3

    const-string v2, "priority!=3"

    aput-object v2, v1, v5

    const-string v2, "pending_read!=0"

    aput-object v2, v1, v4

    .line 648
    invoke-static {v0, v1}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldsf;->j:Ljava/lang/String;

    .line 647
    return-void
.end method

.method public static a(Landroid/content/Context;I[BLdsl;II)I
    .locals 9

    .prologue
    .line 866
    new-instance v8, Lkfp;

    invoke-direct {v8}, Lkfp;-><init>()V

    .line 867
    sget-object v0, Ldsl;->c:Ldsl;

    if-ne p3, v0, :cond_0

    .line 868
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lkfp;->a(Z)V

    .line 870
    :cond_0
    const-string v0, "fetch more notifications"

    invoke-virtual {v8, v0}, Lkfp;->c(Ljava/lang/String;)V

    .line 872
    :try_start_0
    new-instance v0, Ldjq;

    new-instance v2, Lkfo;

    invoke-direct {v2, p0, p1, v8}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    move-object v1, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Ldjq;-><init>(Landroid/content/Context;Lkfo;I[BLdsl;II)V

    .line 875
    invoke-virtual {v0}, Ldjq;->l()V

    .line 876
    invoke-virtual {v0}, Ldjq;->b()[Llui;

    move-result-object v2

    .line 877
    const-string v1, "EsNotificationData"

    invoke-virtual {v0, v1}, Ldjq;->e(Ljava/lang/String;)V

    .line 879
    invoke-virtual {v0}, Ldjq;->d()[B

    move-result-object v4

    .line 880
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    move-object v0, p0

    move v1, p1

    move v5, p4

    move v6, p5

    .line 881
    invoke-static/range {v0 .. v6}, Ldsf;->a(Landroid/content/Context;I[Llui;Ljava/util/Set;[BII)V

    .line 884
    invoke-static {p0, p1, v3, v8}, Ldsf;->a(Landroid/content/Context;ILjava/util/Set;Lkfp;)V

    .line 887
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->b:Landroid/net/Uri;

    .line 888
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    .line 887
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 890
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 892
    :goto_0
    invoke-virtual {v8}, Lkfp;->f()V

    return v0

    .line 890
    :cond_1
    :try_start_1
    array-length v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 892
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Lkfp;->f()V

    throw v0
.end method

.method public static a(Landroid/database/Cursor;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2638
    const/16 v1, 0x17

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 2640
    if-eqz v1, :cond_0

    .line 2641
    invoke-static {v1}, Lkzr;->a([B)Lkzr;

    move-result-object v1

    .line 2642
    if-nez v1, :cond_1

    .line 2646
    :cond_0
    :goto_0
    return v0

    .line 2642
    :cond_1
    invoke-virtual {v1}, Lkzr;->a()I

    move-result v0

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;II)I
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 721
    const-wide/16 v8, -0x1

    .line 722
    const-string v1, "guns"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "display_index"

    aput-object v0, v2, v3

    .line 725
    invoke-static {p1, p2}, Ldsf;->a(II)Ljava/lang/String;

    move-result-object v3

    const-string v7, "display_index DESC LIMIT 1"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    .line 723
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 731
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 732
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 735
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 737
    long-to-int v0, v0

    return v0

    .line 735
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move-wide v0, v8

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Z)J
    .locals 5

    .prologue
    .line 654
    if-eqz p1, :cond_0

    sget-object v0, Ldsf;->g:Ljava/lang/String;

    :goto_0
    const-string v1, "pending_delete"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x7

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0

    :cond_0
    sget-object v0, Ldsf;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;IZLjava/util/List;Z)Landroid/database/Cursor;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1957
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1958
    if-eqz p4, :cond_1

    invoke-static {p0, p1, p2}, Ldsf;->b(Landroid/content/Context;IZ)Landroid/database/Cursor;

    move-result-object v0

    .line 1997
    :cond_0
    :goto_0
    return-object v0

    .line 1958
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1961
    :cond_2
    invoke-static {p0, p1}, Ldsf;->f(Landroid/content/Context;I)J

    move-result-wide v0

    .line 1962
    const-string v2, "read_state=0 AND pending_read=0 AND updated_version>%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1963
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1964
    const-string v3, "key"

    if-eqz p4, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x7

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz p4, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xb

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "\' AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " != \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0, p3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1967
    if-eqz p2, :cond_5

    const/4 v3, 0x0

    .line 1974
    :goto_3
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1975
    const-string v1, "guns"

    sget-object v2, Ldsh;->a:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "creation_time DESC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1979
    if-eqz v0, :cond_0

    .line 1980
    const-string v1, "EsNotificationData"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1981
    :goto_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1982
    const-string v1, "getPhotosNotificationsToDisplayWithRestriction:query: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1984
    :goto_5
    const-string v1, "getPhotosNotificationsToDisplayWithRestriction: unread notification key: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x9

    .line 1986
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x3

    .line 1988
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    .line 1990
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x6

    .line 1992
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x3b

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", heading: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", description: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", timestamp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1964
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x6

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x9

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "\' OR "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " = \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1967
    :cond_5
    const-string v1, "read_state=0 AND seen=0 AND push_enabled!=0 AND "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "(type=97 OR type=111 OR type=18 OR type=99)"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xa

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 1982
    :cond_6
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1994
    :cond_7
    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto/16 :goto_0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 678
    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    .line 679
    const-string v0, "priority=3"

    .line 683
    :goto_0
    return-object v0

    .line 680
    :cond_0
    const/4 v0, 0x2

    if-ne p0, v0, :cond_1

    .line 681
    const-string v0, "priority!=3"

    goto :goto_0

    .line 683
    :cond_1
    const-string v0, "priority=3"

    goto :goto_0
.end method

.method private static a(II)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x2

    .line 696
    const-string v0, ""

    .line 697
    if-ne p0, v3, :cond_2

    .line 698
    const-string v0, "read_state=1"

    .line 704
    :cond_0
    :goto_0
    const-string v1, ""

    .line 705
    const/4 v2, 0x4

    if-ne p1, v2, :cond_3

    .line 706
    const-string v1, "priority=3"

    .line 712
    :cond_1
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 699
    :cond_2
    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    .line 700
    const-string v0, "read_state=0"

    goto :goto_0

    .line 707
    :cond_3
    if-ne p1, v3, :cond_1

    .line 708
    const-string v1, "priority!=3"

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2628
    if-eqz p0, :cond_0

    .line 2629
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2630
    const v2, 0x7f0a05de

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2633
    :cond_0
    return-object v0
.end method

.method private static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2597
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0, p0, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Llun;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Llun;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1585
    if-eqz p0, :cond_1

    iget-object v0, p0, Llun;->a:Llut;

    if-eqz v0, :cond_1

    iget-object v0, p0, Llun;->a:Llut;

    iget-object v0, v0, Llut;->b:[Llup;

    if-eqz v0, :cond_1

    .line 1587
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1588
    iget-object v1, p0, Llun;->a:Llut;

    iget-object v2, v1, Llut;->b:[Llup;

    .line 1589
    array-length v3, v2

    .line 1590
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 1591
    aget-object v4, v2, v1

    .line 1592
    iget-object v5, v4, Llup;->b:Lluo;

    if-eqz v5, :cond_0

    iget-object v5, v4, Llup;->b:Lluo;

    iget-object v5, v5, Lluo;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1593
    iget-object v4, v4, Llup;->b:Lluo;

    iget-object v4, v4, Lluo;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1590
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1598
    :cond_1
    const/4 v0, 0x0

    :cond_2
    return-object v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1809
    .line 1814
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1816
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 1817
    const-string v2, "read_state"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1818
    const-string v2, "pending_read"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1819
    const-string v2, "guns"

    invoke-virtual {v0, v2, v1, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1822
    invoke-static {p0, p1, v4, v5}, Ldsf;->a(Landroid/content/Context;II[B)V

    .line 1826
    invoke-static {p0, p1, v4}, Ldsf;->a(Landroid/content/Context;II)V

    .line 1830
    const v0, 0x7f10009e

    invoke-static {p0, p1, v0}, Lfhu;->a(Landroid/content/Context;II)V

    .line 1832
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->b:Landroid/net/Uri;

    .line 1833
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    .line 1832
    invoke-virtual {v0, v1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1834
    return-void
.end method

.method public static a(Landroid/content/Context;II)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1844
    .line 1848
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1849
    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move v3, p2

    invoke-static/range {v0 .. v5}, Ldsf;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;[BIIZ)V

    .line 1851
    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p2

    invoke-static/range {v0 .. v5}, Ldsf;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;[BIIZ)V

    .line 1853
    return-void
.end method

.method public static a(Landroid/content/Context;III)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2230
    .line 2231
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2232
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2233
    invoke-static {p2, p3}, Ldsf;->c(II)Ljava/lang/String;

    move-result-object v2

    .line 2234
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2233
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2235
    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2236
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2237
    return-void
.end method

.method public static a(Landroid/content/Context;II[B)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2482
    .line 2483
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2485
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2487
    const-string v0, ""

    .line 2488
    const/4 v3, 0x2

    if-ne p2, v3, :cond_1

    .line 2489
    const-string v0, "read_low_notifications_summary"

    .line 2493
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    if-eqz p3, :cond_2

    array-length v3, p3

    if-lez v3, :cond_2

    .line 2494
    invoke-virtual {v2, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2498
    :goto_1
    const-string v0, "account_status"

    invoke-virtual {v1, v0, v2, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2499
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2500
    return-void

    .line 2490
    :cond_1
    const/4 v3, 0x1

    if-ne p2, v3, :cond_0

    .line 2491
    const-string v0, "unread_low_notifications_summary"

    goto :goto_0

    .line 2496
    :cond_2
    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;IJ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2307
    .line 2308
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2310
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2311
    const-string v2, "last_viewed_notification_version"

    .line 2312
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2311
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2313
    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2315
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2316
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2336
    .line 2337
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2339
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2340
    if-eqz p2, :cond_0

    .line 2341
    const-string v0, "unviewed_notifications_count"

    invoke-virtual {v2, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2343
    :cond_0
    if-eqz p3, :cond_1

    .line 2344
    const-string v3, "has_unread_notifications"

    .line 2345
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2344
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2347
    :cond_1
    if-eqz p4, :cond_2

    .line 2348
    const-string v0, "notification_poll_interval"

    .line 2349
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2348
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2351
    :cond_2
    const-string v0, "account_status"

    invoke-virtual {v1, v0, v2, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2353
    if-eqz p5, :cond_3

    .line 2354
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2356
    :cond_3
    return-void

    .line 2345
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/util/List;ZZ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 1768
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1806
    :cond_0
    :goto_0
    return-void

    .line 1772
    :cond_1
    const-string v0, "EsNotificationData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1773
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1774
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    .line 1775
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    .line 1776
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1775
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1778
    :cond_2
    const-string v0, "markNotificationAsRead: "

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1782
    :cond_3
    :goto_2
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1784
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    .line 1785
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_6

    .line 1786
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v5

    .line 1787
    if-eqz p3, :cond_5

    .line 1788
    const-string v0, "UPDATE guns SET read_state = 1, pending_read = 0 WHERE key =?"

    invoke-virtual {v2, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1785
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1778
    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 1790
    :cond_5
    const-string v0, "UPDATE guns SET pending_read = 1 WHERE pending_read = 0 AND read_state = 0 AND key =?"

    invoke-virtual {v2, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 1794
    :cond_6
    const/4 v0, 0x0

    invoke-static {v2, v0}, Ldsf;->a(Landroid/database/sqlite/SQLiteDatabase;Z)J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_7

    .line 1796
    const v0, 0x7f10009e

    invoke-static {p0, p1, v0}, Lfhu;->a(Landroid/content/Context;II)V

    .line 1799
    :cond_7
    const-string v0, "SELECT last_viewed_notification_version  FROM account_status"

    const/4 v1, 0x0

    invoke-static {v2, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-string v3, "read_state=0 AND pending_read=0 AND updated_version>%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SELECT COUNT(*) FROM guns WHERE "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    const/4 v1, 0x0

    invoke-static {v2, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    sget-object v0, Ldsf;->h:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v2, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-lez v0, :cond_9

    const/4 v0, 0x1

    :goto_6
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "unviewed_notifications_count"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "has_unread_notifications"

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "account_status"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1802
    if-eqz p4, :cond_0

    .line 1803
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->b:Landroid/net/Uri;

    .line 1804
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    .line 1803
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    .line 1799
    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_9
    const/4 v0, 0x0

    goto :goto_6

    :cond_a
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static a(Landroid/content/Context;ILjava/util/Set;Lkfp;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lkfp;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 1343
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1344
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    .line 1345
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1346
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1347
    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1348
    const-string v0, "EsNotificationData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1349
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Prefetching "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " activities"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1351
    :cond_1
    new-instance v0, Ldjs;

    new-instance v2, Lkfo;

    invoke-direct {v2, p0, p1, p3}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    .line 1353
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p2, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v1, p0

    move v3, p1

    move v6, v8

    invoke-direct/range {v0 .. v6}, Ldjs;-><init>(Landroid/content/Context;Lkfo;I[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1355
    invoke-virtual {v0}, Lkff;->l()V

    .line 1356
    const-string v1, "EsNotificationData"

    invoke-virtual {v0, v1}, Lkff;->d(Ljava/lang/String;)V

    .line 1359
    :cond_2
    return-void

    .line 1346
    :cond_3
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v4, v1, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "activity_id IN ("

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v8

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "?,"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v2, 0x1

    aput-object v1, v4, v2

    move v2, v3

    goto :goto_1

    :cond_4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v1, ")"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "activities"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "activity_id"

    aput-object v3, v2, v8

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move v0, v8

    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "EsNotificationData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x25

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " activities already cached"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILkfp;Lles;Ldsl;II[B)V
    .locals 21

    .prologue
    .line 781
    sget-object v19, Ldsf;->e:Ljava/lang/Object;

    monitor-enter v19

    .line 782
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 783
    monitor-exit v19

    .line 829
    :goto_0
    return-void

    .line 786
    :cond_0
    invoke-static {}, Lfvc;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 787
    const-class v4, Ljip;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljip;

    if-nez p4, :cond_9

    const-string v5, "NULL"

    :goto_1
    move/from16 v0, p1

    invoke-interface {v4, v0, v5}, Ljip;->a(ILjava/lang/String;)V

    .line 791
    :cond_1
    sget-object v4, Ldsl;->c:Ldsl;

    move-object/from16 v0, p4

    if-ne v0, v4, :cond_2

    .line 793
    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lkfp;->a(Z)V

    .line 796
    :cond_2
    const-string v4, "Notifications"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lkfp;->c(Ljava/lang/String;)V

    .line 802
    sget-object v4, Ldsl;->c:Ldsl;

    move-object/from16 v0, p4

    if-ne v0, v4, :cond_3

    .line 803
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v4}, Ldsf;->a(Landroid/content/Context;IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807
    :cond_3
    :try_start_1
    new-instance v6, Lkfo;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v6, v0, v1, v2}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lkgi;->a(Landroid/content/Context;Lkfo;)Lkfu;

    move-result-object v14

    const/4 v13, 0x0

    invoke-static/range {p5 .. p6}, Ldsf;->b(II)Z

    move-result v20

    if-eqz v20, :cond_a

    sget-object v4, Ldsl;->c:Ldsl;

    move-object/from16 v0, p4

    if-eq v0, v4, :cond_a

    const/4 v4, 0x1

    move/from16 v18, v4

    :goto_2
    if-eqz v18, :cond_b

    invoke-static/range {p0 .. p1}, Ldsf;->h(Landroid/content/Context;I)J

    move-result-wide v4

    move-wide/from16 v16, v4

    :goto_3
    if-eqz p7, :cond_c

    move-object/from16 v0, p7

    array-length v4, v0

    if-lez v4, :cond_c

    new-instance v4, Ldjq;

    move-object/from16 v5, p0

    move/from16 v7, p1

    move-object/from16 v8, p7

    move-object/from16 v9, p4

    move/from16 v10, p5

    move/from16 v11, p6

    invoke-direct/range {v4 .. v11}, Ldjq;-><init>(Landroid/content/Context;Lkfo;I[BLdsl;II)V

    move-object v15, v4

    :goto_4
    if-eqz v20, :cond_4

    new-instance v4, Ldjp;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v4, v0, v1, v2}, Ldjp;-><init>(Landroid/content/Context;ILkfp;)V

    invoke-virtual {v14, v4}, Lkfu;->a(Lkff;)V

    :cond_4
    if-eqz v18, :cond_d

    new-instance v4, Ldjq;

    const/4 v11, 0x2

    const/4 v12, 0x4

    move-object/from16 v5, p0

    move/from16 v7, p1

    move-wide/from16 v8, v16

    move-object/from16 v10, p4

    invoke-direct/range {v4 .. v12}, Ldjq;-><init>(Landroid/content/Context;Lkfo;IJLdsl;II)V

    invoke-virtual {v14, v4}, Lkfu;->a(Lkff;)V

    move-object v5, v4

    :goto_5
    invoke-virtual {v14, v15}, Lkfu;->a(Lkff;)V

    invoke-virtual {v14}, Lkfu;->l()V

    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lles;->a(Lkff;)V

    :cond_5
    invoke-virtual {v14}, Lkfu;->t()Z

    move-result v4

    if-eqz v4, :cond_f

    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6

    iget v4, v14, Lkff;->i:I

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x34

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "fetchNotifications error with error code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_6
    invoke-static {v14}, Ldsf;->a(Lkfu;)Z

    move-result v4

    if-nez v4, :cond_7

    sget-object v4, Ldsl;->b:Ldsl;

    move-object/from16 v0, p4

    if-ne v0, v4, :cond_e

    :cond_7
    new-instance v4, Ldsi;

    iget v5, v14, Lkff;->i:I

    iget-object v6, v14, Lkff;->j:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x15

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " ["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ldsi;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ldsi; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 827
    :catch_0
    move-exception v4

    :try_start_2
    invoke-virtual/range {p2 .. p2}, Lkfp;->f()V

    .line 829
    :cond_8
    :goto_6
    monitor-exit v19

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v19
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 788
    :cond_9
    :try_start_3
    invoke-virtual/range {p4 .. p4}, Ldsl;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v5

    goto/16 :goto_1

    .line 807
    :cond_a
    const/4 v4, 0x0

    move/from16 v18, v4

    goto/16 :goto_2

    :cond_b
    const-wide/16 v4, 0x0

    move-wide/from16 v16, v4

    goto/16 :goto_3

    :cond_c
    :try_start_4
    new-instance v4, Ldjq;

    move-object/from16 v5, p0

    move/from16 v7, p1

    move-wide/from16 v8, v16

    move-object/from16 v10, p4

    move/from16 v11, p5

    move/from16 v12, p6

    invoke-direct/range {v4 .. v12}, Ldjq;-><init>(Landroid/content/Context;Lkfo;IJLdsl;II)V

    move-object v15, v4

    goto/16 :goto_4

    :cond_d
    const/4 v4, 0x0

    move-object v5, v4

    goto/16 :goto_5

    :cond_e
    new-instance v4, Ljava/io/IOException;

    iget v5, v14, Lkff;->i:I

    iget-object v6, v14, Lkff;->j:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x15

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " ["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_f
    invoke-virtual {v15}, Ldjq;->b()[Llui;

    move-result-object v16

    invoke-virtual {v15}, Ldjq;->f()J

    move-result-wide v6

    invoke-static/range {p0 .. p1}, Ldsf;->f(Landroid/content/Context;I)J

    move-result-wide v8

    cmp-long v4, v6, v8

    if-lez v4, :cond_10

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v6, v7}, Ldsf;->a(Landroid/content/Context;IJ)V

    :cond_10
    invoke-virtual {v15}, Ldjq;->d()[B

    move-result-object v17

    if-eqz v18, :cond_1b

    if-eqz v17, :cond_1b

    move-object/from16 v0, v17

    array-length v4, v0

    if-lez v4, :cond_1b

    const/4 v4, 0x1

    move v14, v4

    :goto_7
    if-eqz v18, :cond_11

    if-eqz v14, :cond_1c

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p5

    move/from16 v3, p6

    invoke-static {v0, v1, v2, v3, v4}, Ldsf;->b(Landroid/content/Context;IIIZ)V

    const-string v4, "EsNotificationData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_11

    :cond_11
    :goto_8
    if-nez v18, :cond_12

    if-eqz v20, :cond_1e

    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v4

    invoke-virtual {v4}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "pending_delete"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "guns"

    invoke-static/range {p5 .. p6}, Ldsf;->a(II)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_12
    :goto_9
    if-eqz v16, :cond_16

    invoke-static/range {p0 .. p1}, Ldsf;->h(Landroid/content/Context;I)J

    move-result-wide v4

    invoke-virtual {v15}, Ldjq;->e()J

    move-result-wide v6

    cmp-long v4, v6, v4

    if-lez v4, :cond_13

    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v4

    invoke-virtual {v4}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "last_notification_sync_version"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "account_status"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_13
    if-eqz v18, :cond_14

    move-object/from16 v0, v16

    array-length v4, v0

    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v5

    invoke-virtual {v5}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "UPDATE %s SET %s = %s + %d WHERE %s"

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "guns"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "display_index"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "display_index"

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v4, 0x4

    invoke-static/range {p5 .. p6}, Ldsf;->a(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v6, v7}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_14
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    if-eqz v18, :cond_15

    if-eqz v14, :cond_1f

    :cond_15
    const/4 v9, 0x1

    :goto_a
    move-object/from16 v4, p0

    move/from16 v5, p1

    move-object/from16 v6, v16

    move-object/from16 v7, p4

    move-object/from16 v10, v17

    move/from16 v11, p5

    move/from16 v12, p6

    invoke-static/range {v4 .. v12}, Ldsf;->a(Landroid/content/Context;I[Llui;Ldsl;Ljava/util/Set;Z[BII)V

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v8, v2}, Ldsf;->a(Landroid/content/Context;ILjava/util/Set;Lkfp;)V

    :cond_16
    if-nez v18, :cond_17

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p5

    move/from16 v3, p6

    invoke-static {v0, v1, v2, v3, v4}, Ldsf;->b(Landroid/content/Context;IIIZ)V

    :cond_17
    const/4 v4, 0x4

    move/from16 v0, p6

    if-ne v0, v4, :cond_18

    const-class v4, Lieh;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lieh;

    sget-object v5, Ldxd;->g:Lief;

    move/from16 v0, p1

    invoke-interface {v4, v5, v0}, Lieh;->b(Lief;I)Z

    move-result v4

    if-eqz v4, :cond_20

    invoke-virtual {v15}, Ldjq;->c()[B

    move-result-object v4

    :goto_b
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p5

    invoke-static {v0, v1, v2, v4}, Ldsf;->a(Landroid/content/Context;II[B)V

    :cond_18
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->b:Landroid/net/Uri;

    move/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    if-nez v16, :cond_21

    const/4 v4, 0x0

    :goto_c
    add-int v5, v13, v4

    .line 811
    invoke-static/range {p5 .. p6}, Ldsf;->b(II)Z

    move-result v6

    .line 812
    if-eqz v6, :cond_22

    if-gtz v5, :cond_19

    .line 813
    invoke-static {}, Lffl;->a()Z

    move-result v4

    if-eqz v4, :cond_22

    :cond_19
    const/4 v4, 0x1

    .line 814
    :goto_d
    if-eqz v4, :cond_1a

    .line 816
    invoke-static/range {p0 .. p1}, Lffl;->a(Landroid/content/Context;I)V

    .line 819
    :cond_1a
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lkfp;->m(I)V

    .line 821
    if-eqz v6, :cond_8

    .line 822
    sget-object v4, Lfit;->a:Lfit;

    .line 823
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 822
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v4, v6, v7}, Ldhv;->a(Landroid/content/Context;ILfit;J)V

    goto/16 :goto_6

    .line 807
    :cond_1b
    const/4 v4, 0x0

    move v14, v4

    goto/16 :goto_7

    :cond_1c
    if-nez v5, :cond_1d

    const/4 v6, 0x0

    :goto_e
    if-eqz v6, :cond_11

    array-length v4, v6

    if-lez v4, :cond_11

    array-length v4, v6

    add-int/lit8 v13, v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x2

    const/4 v12, 0x4

    move-object/from16 v4, p0

    move/from16 v5, p1

    move-object/from16 v7, p4

    invoke-static/range {v4 .. v12}, Ldsf;->a(Landroid/content/Context;I[Llui;Ldsl;Ljava/util/Set;Z[BII)V

    goto/16 :goto_8

    :cond_1d
    invoke-virtual {v5}, Ldjq;->b()[Llui;

    move-result-object v6

    goto :goto_e

    :cond_1e
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p5

    move/from16 v3, p6

    invoke-static {v0, v1, v2, v3, v4}, Ldsf;->b(Landroid/content/Context;IIIZ)V

    goto/16 :goto_9

    :cond_1f
    const/4 v9, 0x0

    goto/16 :goto_a

    :cond_20
    const/4 v4, 0x0

    goto/16 :goto_b

    :cond_21
    move-object/from16 v0, v16

    array-length v4, v0
    :try_end_4
    .catch Ldsi; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_c

    .line 813
    :cond_22
    const/4 v4, 0x0

    goto :goto_d
.end method

.method public static a(Landroid/content/Context;IZ)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 837
    .line 838
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 840
    sget-object v0, Ldsf;->j:Ljava/lang/String;

    invoke-static {v2, v0, v6}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-lez v0, :cond_2

    move v0, v1

    .line 842
    :goto_0
    sget-object v3, Ldsf;->i:Ljava/lang/String;

    invoke-static {v2, v3, v6}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v3, v4, v8

    if-lez v3, :cond_1

    .line 843
    const-string v3, "UPDATE guns SET read_state = 1, pending_read = 0 WHERE pending_read = 1"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 845
    if-eqz v0, :cond_0

    .line 848
    invoke-static {p0, p1, v1, v6}, Ldsf;->a(Landroid/content/Context;II[B)V

    .line 852
    :cond_0
    if-eqz p2, :cond_1

    .line 853
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->b:Landroid/net/Uri;

    .line 854
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v1

    .line 853
    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 857
    :cond_1
    return-void

    .line 840
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;I[Llui;Ldsl;Ljava/util/Set;Z[BII)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I[",
            "Llui;",
            "Ldsl;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z[BII)V"
        }
    .end annotation

    .prologue
    .line 1081
    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    array-length v2, v0

    if-nez v2, :cond_1

    .line 1232
    :cond_0
    :goto_0
    return-void

    .line 1087
    :cond_1
    invoke-static/range {p0 .. p1}, Ldhv;->a(Landroid/content/Context;I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1088
    invoke-static/range {p0 .. p1}, Ldsm;->b(Landroid/content/Context;I)Z

    .line 1092
    :cond_2
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1094
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1097
    :try_start_0
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 1099
    const/4 v3, 0x0

    invoke-static {v2, v3}, Ldsf;->a(Landroid/database/sqlite/SQLiteDatabase;Z)J

    move-result-wide v14

    .line 1100
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1101
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1102
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1105
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v18, v0

    .line 1106
    const/4 v3, 0x0

    move v11, v3

    :goto_1
    move/from16 v0, v18

    if-ge v11, v0, :cond_d

    .line 1107
    aget-object v10, p2, v11

    .line 1108
    invoke-static {v10, v12, v11}, Ldsf;->a(Llui;Landroid/content/ContentValues;I)V

    .line 1110
    const-string v3, "key"

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1111
    const-string v3, "guns"

    const-string v4, "key"

    const/4 v5, 0x4

    invoke-virtual {v2, v3, v4, v12, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v4

    .line 1113
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_b

    .line 1115
    const-string v3, "pending_read"

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1119
    const-string v3, "updated_version"

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 1121
    const/4 v3, 0x2

    move/from16 v0, p7

    if-ne v3, v0, :cond_9

    const/4 v3, 0x4

    move/from16 v0, p8

    if-ne v3, v0, :cond_9

    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_3

    .line 1126
    const-string v3, "updated_version"

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1130
    :cond_3
    const-string v3, "guns"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "updated_version"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "push_enabled"

    aput-object v6, v4, v5

    const-string v5, "key=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v19, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1139
    if-eqz v3, :cond_4

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1140
    const-string v4, "updated_version"

    .line 1141
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 1140
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1142
    const-string v6, "push_enabled"

    .line 1143
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 1142
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1145
    cmp-long v6, v4, v20

    if-gez v6, :cond_a

    .line 1147
    const-string v4, "guns"

    const-string v5, "key=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v19, v6, v7

    invoke-virtual {v2, v4, v12, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1151
    if-lez v4, :cond_4

    sget-object v4, Ldsl;->a:Ldsl;

    move-object/from16 v0, p3

    if-eq v0, v4, :cond_4

    const-string v4, "push_enabled"

    .line 1153
    invoke-virtual {v12, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, v3, :cond_4

    .line 1154
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1171
    :cond_4
    :goto_3
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1172
    const-string v3, "type"

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1174
    const-string v3, "actors"

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v3

    .line 1176
    if-eqz v3, :cond_5

    .line 1177
    invoke-static {v3}, Ldqu;->a([B)Ljava/util/List;

    move-result-object v3

    .line 1178
    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1179
    move-object/from16 v0, v19

    invoke-static {v2, v0, v3}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V

    .line 1183
    :cond_5
    const-string v3, "PHOTOS"

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v3

    .line 1184
    if-eqz v3, :cond_7

    .line 1185
    invoke-static {v3}, Lkzr;->a([B)Lkzr;

    move-result-object v4

    .line 1187
    const/4 v3, 0x0

    .line 1188
    iget-object v5, v10, Llui;->e:Llur;

    if-eqz v5, :cond_6

    iget-object v5, v10, Llui;->e:Llur;

    iget-object v5, v5, Llur;->a:Lluk;

    if-eqz v5, :cond_6

    iget-object v5, v10, Llui;->e:Llur;

    iget-object v5, v5, Llur;->a:Lluk;

    iget-object v5, v5, Lluk;->d:Llul;

    if-eqz v5, :cond_6

    .line 1190
    iget-object v3, v10, Llui;->e:Llur;

    iget-object v3, v3, Llur;->a:Lluk;

    iget-object v3, v3, Lluk;->d:Llul;

    iget-object v3, v3, Llul;->a:Ljava/lang/String;

    .line 1193
    :cond_6
    const/4 v5, 0x0

    .line 1194
    invoke-static {v5, v4}, Llco;->a(Ljava/lang/String;Lkzr;)Lhsz;

    move-result-object v5

    .line 1193
    invoke-static {v2, v5, v3}, Ldtc;->a(Landroid/database/sqlite/SQLiteDatabase;Lhsz;Ljava/lang/String;)[Lnzx;

    move-result-object v6

    .line 1195
    const/4 v3, 0x6

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, v10, Llui;->b:Ljava/lang/String;

    aput-object v8, v5, v7

    .line 1196
    invoke-static {v3, v5}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1199
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v5}, Ljvj;->d(Landroid/content/Context;ILjava/lang/String;)Ljava/util/Set;

    move-result-object v3

    .line 1202
    if-eqz v3, :cond_c

    if-eqz v4, :cond_c

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v7

    invoke-virtual {v4}, Lkzr;->a()I

    move-result v4

    if-ne v7, v4, :cond_c

    invoke-interface {v3, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_c

    const/4 v3, 0x0

    :goto_4
    if-eqz v3, :cond_7

    .line 1203
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v3, p0

    move/from16 v4, p1

    invoke-static/range {v3 .. v10}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 1208
    :cond_7
    const-string v3, "activity_id"

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1209
    if-eqz p4, :cond_8

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1210
    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1106
    :cond_8
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    goto/16 :goto_1

    .line 1121
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 1156
    :cond_a
    cmp-long v3, v4, v20

    if-nez v3, :cond_4

    .line 1158
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1159
    const-string v4, "pending_delete"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1160
    const-string v4, "guns"

    const-string v5, "key=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v19, v6, v7

    invoke-virtual {v2, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_3

    .line 1231
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 1165
    :cond_b
    :try_start_1
    sget-object v3, Ldsl;->a:Ldsl;

    move-object/from16 v0, p3

    if-eq v0, v3, :cond_4

    .line 1167
    const-string v3, "push_enabled"

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 1168
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 1202
    :cond_c
    const/4 v3, 0x1

    goto :goto_4

    .line 1214
    :cond_d
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_e

    .line 1215
    invoke-static/range {v17 .. v17}, Ldsf;->a(Ljava/util/Collection;)V

    .line 1217
    :cond_e
    if-eqz p5, :cond_f

    .line 1218
    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object v4, v2

    move-object/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    invoke-static/range {v3 .. v8}, Ldsf;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;[BIIZ)V

    .line 1222
    :cond_f
    const-wide/16 v4, 0xc8

    invoke-static {v2, v4, v5}, Ldsf;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 1224
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1227
    const/4 v3, 0x0

    invoke-static {v2, v3}, Ldsf;->a(Landroid/database/sqlite/SQLiteDatabase;Z)J

    move-result-wide v6

    .line 1228
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    sget-object v3, Ldsg;->a:[I

    invoke-virtual/range {p3 .. p3}, Ldsl;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    sget-object v4, Lhmv;->bK:Lhmv;

    sget-object v3, Lhmw;->y:Lhmw;

    move-object v5, v4

    move-object v4, v3

    :goto_5
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_10

    const-string v3, "extra_notification_types"

    move-object/from16 v0, v16

    invoke-virtual {v8, v3, v0}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_10
    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_11

    const-string v3, "extra_notification_id_list"

    invoke-virtual {v8, v3, v13}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_11
    const-string v3, "extra_num_unread_notifi"

    long-to-int v6, v6

    invoke-virtual {v8, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "extra_prev_num_unread_noti"

    long-to-int v6, v14

    invoke-virtual {v8, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-class v3, Lhms;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhms;

    new-instance v6, Lhmr;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v6, v0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6, v5}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v5

    invoke-virtual {v5, v4}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v4

    invoke-virtual {v4, v8}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v4

    invoke-interface {v3, v4}, Lhms;->a(Lhmr;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1231
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 1228
    :pswitch_0
    :try_start_2
    sget-object v4, Lhmv;->bA:Lhmv;

    sget-object v3, Lhmw;->y:Lhmw;

    move-object v5, v4

    move-object v4, v3

    goto :goto_5

    :pswitch_1
    sget-object v4, Lhmv;->bL:Lhmv;

    sget-object v3, Lhmw;->w:Lhmw;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v5, v4

    move-object v4, v3

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;I[Llui;Ljava/util/Set;[BII)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I[",
            "Llui;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;[BII)V"
        }
    .end annotation

    .prologue
    .line 1303
    .line 1304
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1306
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1308
    if-eqz p2, :cond_1

    :try_start_0
    array-length v0, p2

    if-lez v0, :cond_1

    .line 1310
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1311
    array-length v3, p2

    .line 1313
    invoke-static {v1, p5, p6}, Ldsf;->a(Landroid/database/sqlite/SQLiteDatabase;II)I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    .line 1314
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    .line 1315
    aget-object v5, p2, v0

    .line 1316
    add-int v6, v4, v0

    invoke-static {v5, v2, v6}, Ldsf;->a(Llui;Landroid/content/ContentValues;I)V

    .line 1319
    const-string v5, "guns"

    const-string v6, "key"

    const/4 v7, 0x5

    invoke-virtual {v1, v5, v6, v2, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1322
    const-string v5, "activity_id"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1323
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1324
    invoke-interface {p3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1314
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1329
    :cond_1
    const/4 v5, 0x1

    move-object v0, p0

    move-object v2, p4

    move v3, p5

    move v4, p6

    invoke-static/range {v0 .. v5}, Ldsf;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;[BIIZ)V

    .line 1332
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1334
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1335
    return-void

    .line 1334
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;[BIIZ)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2412
    sget-object v1, Ldsf;->a:[B

    if-ne p2, v1, :cond_1

    .line 2434
    :cond_0
    :goto_0
    return-void

    .line 2416
    :cond_1
    const/4 v1, 0x2

    if-ne p3, v1, :cond_3

    sget-object v1, Ldsf;->g:Ljava/lang/String;

    invoke-static {p4}, Ldsf;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pending_delete"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " AND "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    :goto_1
    const-wide/16 v4, 0x64

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    move-object p2, v0

    .line 2421
    :cond_2
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2422
    invoke-static {p3, p4}, Ldsf;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2424
    if-eqz p2, :cond_4

    array-length v3, p2

    if-lez v3, :cond_4

    .line 2425
    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2429
    :goto_2
    const-string v2, "account_status"

    invoke-virtual {p1, v2, v1, v0, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2431
    if-eqz p5, :cond_0

    .line 2432
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 2416
    :cond_3
    sget-object v1, Ldsf;->f:Ljava/lang/String;

    invoke-static {p4}, Ldsf;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pending_delete"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " AND "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_1

    .line 2427
    :cond_4
    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2615
    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2616
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2617
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2618
    const v1, 0x7f0a05de

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2620
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2622
    :cond_0
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 2146
    const-wide/16 v0, 0xc8

    invoke-static {p0, v0, v1}, Ldsf;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    .line 2147
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 2068
    const-string v0, "guns"

    invoke-static {p0, v0, v3, v3}, Ldrg;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    .line 2070
    const-string v0, "EsNotificationData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x54

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "deleteOldNotifications, keep count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", have: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2074
    :cond_0
    sub-long v0, v4, p1

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-gtz v0, :cond_2

    .line 2109
    :cond_1
    :goto_0
    return-void

    .line 2079
    :cond_2
    const-string v1, "guns"

    sget-object v2, Ldsj;->a:[Ljava/lang/String;

    const-string v7, "creation_time ASC"

    sub-long/2addr v4, p1

    .line 2082
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    .line 2079
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2083
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2088
    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v0, 0x100

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 2090
    :try_start_0
    const-string v0, "key IN("

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2091
    const/4 v0, 0x1

    .line 2092
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2093
    if-eqz v0, :cond_3

    move v0, v9

    .line 2098
    :goto_2
    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2099
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2100
    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2105
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2096
    :cond_3
    const/16 v4, 0x2c

    :try_start_1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 2103
    :cond_4
    const/16 v0, 0x29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2108
    const-string v0, "guns"

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static a(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Llui;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1237
    const-string v0, "EsNotificationData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1238
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Notifications to Log:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1239
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llui;

    .line 1240
    invoke-virtual {v0}, Llui;->toString()Ljava/lang/String;

    goto :goto_0

    .line 1243
    :cond_0
    return-void
.end method

.method private static a(Llui;Landroid/content/ContentValues;I)V
    .locals 20

    .prologue
    .line 1366
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1367
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1368
    const/4 v7, 0x0

    .line 1370
    invoke-virtual/range {p1 .. p1}, Landroid/content/ContentValues;->clear()V

    .line 1371
    const-string v2, "key"

    move-object/from16 v0, p0

    iget-object v3, v0, Llui;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1373
    const-wide/16 v8, 0x0

    .line 1374
    const-wide/16 v2, 0x0

    .line 1376
    const-string v4, "display_index"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1378
    move-object/from16 v0, p0

    iget-object v4, v0, Llui;->i:Ljava/lang/Long;

    if-eqz v4, :cond_0

    .line 1379
    move-object/from16 v0, p0

    iget-object v2, v0, Llui;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1380
    const-string v4, "updated_version"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    move-wide v4, v2

    .line 1382
    move-object/from16 v0, p0

    iget-object v2, v0, Llui;->l:Llua;

    if-eqz v2, :cond_1

    .line 1383
    const-string v2, "analytics_data"

    move-object/from16 v0, p0

    iget-object v3, v0, Llui;->l:Llua;

    .line 1384
    invoke-static {v3}, Loxu;->a(Loxu;)[B

    move-result-object v3

    .line 1383
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1387
    :cond_1
    const-string v3, "read_state"

    move-object/from16 v0, p0

    iget v2, v0, Llui;->d:I

    const/4 v6, 0x1

    if-eq v2, v6, :cond_2

    const/4 v6, 0x4

    if-ne v2, v6, :cond_5

    :cond_2
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1388
    const-string v3, "push_enabled"

    move-object/from16 v0, p0

    iget-object v2, v0, Llui;->j:Ljava/lang/Boolean;

    .line 1389
    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1388
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1390
    const-string v2, "pending_read"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1391
    const-string v2, "seen"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1392
    const-string v2, "pending_delete"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1393
    const-string v2, "priority"

    move-object/from16 v0, p0

    iget v3, v0, Llui;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1396
    move-object/from16 v0, p0

    iget-object v2, v0, Llui;->f:[Lluj;

    if-eqz v2, :cond_3

    .line 1397
    move-object/from16 v0, p0

    iget-object v2, v0, Llui;->f:[Lluj;

    .line 1399
    array-length v3, v2

    if-eqz v3, :cond_3

    .line 1400
    const/4 v3, 0x0

    aget-object v3, v2, v3

    .line 1401
    iget-object v6, v3, Lluj;->b:Ljava/lang/String;

    .line 1402
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1403
    const-string v10, "type"

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    sget-object v2, Ldsf;->d:Ljava/util/Map;

    invoke-interface {v2, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v2, Ldsf;->d:Ljava/util/Map;

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1404
    const-string v10, "category"

    const-string v2, "STREAM"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1409
    const-string v2, "BIRTHDAY_WISH"

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1410
    iget-object v2, v3, Lluj;->c:Ljava/lang/String;

    .line 1411
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1412
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_13

    array-length v3, v2

    const/4 v6, 0x3

    if-lt v3, v6, :cond_13

    const/4 v3, 0x2

    aget-object v2, v2, v3

    .line 1413
    :goto_4
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1414
    const-string v3, "activity_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1422
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Llui;->g:[Llum;

    if-eqz v2, :cond_16

    .line 1423
    move-object/from16 v0, p0

    iget-object v10, v0, Llui;->g:[Llum;

    .line 1424
    const/4 v3, 0x0

    .line 1425
    const/4 v2, 0x0

    :goto_5
    array-length v6, v10

    if-ge v2, v6, :cond_15

    .line 1426
    aget-object v6, v10, v2

    .line 1427
    iget v11, v6, Llum;->b:I

    .line 1428
    iget-object v6, v6, Llum;->c:Ljava/lang/String;

    .line 1429
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 1430
    packed-switch v11, :pswitch_data_0

    .line 1455
    :cond_4
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1387
    :cond_5
    const/4 v6, 0x2

    if-ne v2, v6, :cond_6

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 1389
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1403
    :cond_8
    const/4 v2, 0x0

    goto :goto_2

    .line 1404
    :cond_9
    const-string v2, "EVENTS"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xa

    goto :goto_3

    :cond_a
    const-string v2, "SQUARE"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xb

    goto :goto_3

    :cond_b
    const-string v2, "PHOTOS"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v2, 0x3

    goto/16 :goto_3

    :cond_c
    const-string v2, "GAMES"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x4

    goto/16 :goto_3

    :cond_d
    const-string v2, "CIRCLE"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x2

    goto/16 :goto_3

    :cond_e
    const-string v2, "SYSTEM"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    const/4 v2, 0x5

    goto/16 :goto_3

    :cond_f
    const-string v2, "HANGOUT"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x8

    goto/16 :goto_3

    :cond_10
    const-string v2, "STORIES"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0xd

    goto/16 :goto_3

    :cond_11
    const-string v2, "ENGAGE_POSTS"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0xe

    goto/16 :goto_3

    :cond_12
    const v2, 0xffff

    goto/16 :goto_3

    .line 1412
    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 1434
    :pswitch_0
    const-string v11, "activity_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    add-int/lit8 v3, v3, 0x1

    .line 1436
    goto/16 :goto_6

    .line 1438
    :pswitch_1
    const-string v11, "event_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1441
    :pswitch_2
    const-string v11, "album_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1442
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 1445
    :pswitch_3
    const-string v11, "community_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1448
    :pswitch_4
    const-string v11, "#"

    invoke-virtual {v6, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    if-gtz v11, :cond_14

    const/4 v6, 0x0

    .line 1449
    :goto_7
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 1450
    const-string v11, "activity_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1448
    :cond_14
    const/4 v12, 0x0

    invoke-virtual {v6, v12, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    goto :goto_7

    .line 1454
    :pswitch_5
    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 1460
    :cond_15
    const-string v2, "community_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    const/4 v2, 0x1

    if-le v3, v2, :cond_16

    .line 1461
    const-string v2, "activity_id"

    const-string v3, ""

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1465
    :cond_16
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1467
    move-object/from16 v0, p0

    iget-object v2, v0, Llui;->e:Llur;

    if-eqz v2, :cond_28

    .line 1468
    move-object/from16 v0, p0

    iget-object v10, v0, Llui;->e:Llur;

    .line 1471
    iget-object v2, v10, Llur;->a:Lluk;

    if-eqz v2, :cond_19

    .line 1472
    iget-object v6, v10, Llur;->a:Lluk;

    .line 1475
    iget-object v2, v6, Lluk;->c:Ljava/lang/Long;

    if-eqz v2, :cond_27

    .line 1476
    iget-object v2, v6, Lluk;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1480
    :goto_8
    iget-object v7, v6, Lluk;->d:Llul;

    if-eqz v7, :cond_17

    .line 1481
    iget-object v7, v6, Lluk;->d:Llul;

    .line 1483
    const-string v8, "collapsed_destination"

    iget-object v7, v7, Llul;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1487
    :cond_17
    iget-object v7, v6, Lluk;->b:Llus;

    if-eqz v7, :cond_1a

    .line 1488
    iget-object v6, v6, Lluk;->b:Llus;

    .line 1489
    const-string v7, "collapsed_heading"

    iget-object v8, v6, Llus;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1490
    const-string v7, "collapsed_description"

    iget-object v8, v6, Llus;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1491
    const-string v7, "collapsed_annotation"

    iget-object v8, v6, Llus;->e:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1494
    iget-object v7, v6, Llus;->a:Lluo;

    if-eqz v7, :cond_18

    .line 1495
    const-string v7, "collapsed_icon"

    iget-object v8, v6, Llus;->a:Lluo;

    iget-object v8, v8, Lluo;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1499
    :cond_18
    iget-object v7, v6, Llus;->b:[Lluq;

    if-eqz v7, :cond_1a

    .line 1500
    iget-object v7, v6, Llus;->b:[Lluq;

    .line 1501
    array-length v8, v7

    .line 1502
    const/4 v6, 0x0

    :goto_9
    if-ge v6, v8, :cond_1a

    .line 1503
    aget-object v9, v7, v6

    .line 1504
    new-instance v11, Ldqv;

    iget-object v12, v9, Lluq;->c:Ljava/lang/String;

    iget-object v0, v9, Lluq;->d:Ljava/lang/String;

    move-object/from16 v16, v0

    iget-object v9, v9, Lluq;->b:Ljava/lang/String;

    .line 1506
    invoke-static {v9}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v16

    invoke-direct {v11, v12, v0, v9}, Ldqv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    invoke-virtual {v15, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1502
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    :cond_19
    move-wide v2, v8

    .line 1514
    :cond_1a
    iget-object v6, v10, Llur;->b:Llun;

    invoke-static {v6}, Ldsf;->a(Llun;)Ljava/util/ArrayList;

    move-result-object v7

    .line 1515
    iget-object v6, v10, Llur;->b:Llun;

    invoke-static {v6}, Ldsf;->b(Llun;)Ljava/util/List;

    move-result-object v6

    .line 1516
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1b

    .line 1518
    :try_start_0
    const-string v8, "creators"

    invoke-static {v6}, Ldqu;->a(Ljava/util/List;)[B

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1523
    :cond_1b
    :goto_a
    iget-object v6, v10, Llur;->b:Llun;

    invoke-static {v6}, Ldsf;->c(Llun;)[B

    move-result-object v6

    .line 1524
    const-string v8, "expanded_info"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1527
    iget-object v6, v10, Llur;->c:Llwa;

    if-nez v6, :cond_1d

    const/4 v6, 0x0

    .line 1528
    :goto_b
    const-string v8, "payload"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1531
    iget-object v6, v10, Llur;->d:Llva;

    if-nez v6, :cond_1e

    const/4 v6, 0x0

    .line 1532
    :goto_c
    const-string v8, "app_payload"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-wide v8, v2

    move-object v3, v7

    .line 1535
    :goto_d
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1536
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 1537
    if-nez v3, :cond_1f

    const/4 v2, 0x0

    move v12, v2

    .line 1538
    :goto_e
    if-lez v12, :cond_23

    if-ne v12, v6, :cond_23

    .line 1539
    const-string v6, ""

    .line 1540
    new-instance v17, Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 1541
    const/4 v2, 0x0

    move v11, v2

    move-object v2, v6

    :goto_f
    if-ge v11, v12, :cond_22

    .line 1542
    move/from16 v0, v16

    if-ge v11, v0, :cond_26

    .line 1543
    invoke-virtual {v14, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v6, v2

    .line 1545
    :goto_10
    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1546
    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-lez v7, :cond_20

    const/4 v10, 0x0

    invoke-virtual {v2, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 1547
    :goto_11
    const-string v10, ":"

    invoke-virtual {v2, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v18, v10, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_21

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    .line 1548
    :goto_12
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1c

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1c

    .line 1549
    new-instance v18, Lkzv;

    .line 1550
    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v0, v7, v10, v6, v2}, Lkzv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1541
    :cond_1c
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    move-object v2, v6

    goto :goto_f

    .line 1519
    :catch_0
    move-exception v6

    .line 1520
    const-string v8, "EsNotificationData"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x2a

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "Cannot serialize creators (DataActor) list"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    .line 1527
    :cond_1d
    invoke-static {v6}, Loxu;->a(Loxu;)[B

    move-result-object v6

    goto/16 :goto_b

    .line 1531
    :cond_1e
    invoke-static {v6}, Loxu;->a(Loxu;)[B

    move-result-object v6

    goto/16 :goto_c

    .line 1537
    :cond_1f
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v12, v2

    goto/16 :goto_e

    .line 1546
    :cond_20
    const/4 v7, 0x0

    goto :goto_11

    .line 1547
    :cond_21
    const/4 v2, 0x0

    move-object v10, v2

    goto :goto_12

    .line 1554
    :cond_22
    :try_start_1
    const-string v2, "PHOTOS"

    new-instance v3, Lkzr;

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Lkzr;-><init>(Ljava/util/ArrayList;)V

    invoke-static {v3}, Lkzr;->a(Lkzr;)[B

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1561
    :cond_23
    :goto_13
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_24

    .line 1563
    :try_start_2
    const-string v2, "actors"

    invoke-static {v15}, Ldqu;->a(Ljava/util/List;)[B

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1570
    :cond_24
    :goto_14
    const-string v2, "creation_time"

    const-wide/16 v6, 0x0

    cmp-long v3, v8, v6

    if-lez v3, :cond_25

    :goto_15
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1571
    return-void

    .line 1556
    :catch_1
    move-exception v2

    .line 1557
    const-string v3, "EsNotificationData"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x20

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Cannot serialize PlusPhoto list "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_13

    .line 1564
    :catch_2
    move-exception v2

    .line 1565
    const-string v3, "EsNotificationData"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x20

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Cannot serialize DataActor list "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_14

    :cond_25
    move-wide v8, v4

    .line 1570
    goto :goto_15

    :cond_26
    move-object v6, v2

    goto/16 :goto_10

    :cond_27
    move-wide v2, v8

    goto/16 :goto_8

    :cond_28
    move-object v3, v7

    goto/16 :goto_d

    .line 1430
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;IIIZ)Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 2281
    if-eqz p4, :cond_0

    move v0, v4

    .line 2299
    :goto_0
    return v0

    .line 2286
    :cond_0
    invoke-static {p2, p3}, Ldsf;->b(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2287
    sget-object v0, Lfit;->a:Lfit;

    invoke-static {p0, p1, v0}, Ldhv;->a(Landroid/content/Context;ILfit;)J

    move-result-wide v0

    move-wide v2, v0

    .line 2294
    :goto_1
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 2295
    sget-object v1, Ldxd;->h:Lief;

    invoke-interface {v0, v1, p1}, Lieh;->c(Lief;I)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2298
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    .line 2299
    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    move v0, v4

    goto :goto_0

    .line 2290
    :cond_1
    invoke-static {p0, p1, p2, p3}, Ldsf;->c(Landroid/content/Context;III)J

    move-result-wide v0

    move-wide v2, v0

    goto :goto_1

    .line 2299
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lkfu;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2601
    invoke-virtual {p0}, Lkfu;->j()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    .line 2602
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2603
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkff;

    .line 2604
    iget v4, v0, Lkff;->i:I

    const/16 v5, 0x190

    if-lt v4, v5, :cond_1

    iget v0, v0, Lkff;->i:I

    const/16 v4, 0x1f4

    if-ge v0, v4, :cond_1

    .line 2605
    const/4 v2, 0x1

    .line 2608
    :cond_0
    return v2

    .line 2602
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;IZ)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 2028
    invoke-static {p0, p1}, Ldsf;->f(Landroid/content/Context;I)J

    move-result-wide v0

    .line 2029
    const-string v2, "read_state=0 AND pending_read=0 AND updated_version>%d"

    new-array v3, v9, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2030
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2031
    if-eqz p2, :cond_0

    move-object v3, v4

    .line 2037
    :goto_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2038
    const-string v1, "guns"

    sget-object v2, Ldsh;->a:[Ljava/lang/String;

    const-string v7, "creation_time DESC"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2042
    if-eqz v0, :cond_2

    .line 2043
    const-string v1, "EsNotificationData"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2044
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2045
    const/16 v1, 0x9

    .line 2046
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    .line 2048
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2050
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    .line 2052
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x75

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "getPhotosNotificationsToDisplay: unread notification key: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ", heading: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", description: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", timestamp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2031
    :cond_0
    const-string v1, "read_state=0 AND seen=0 AND push_enabled!=0 AND "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "(type=97 OR type=111 OR type=18 OR type=99)"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 2054
    :cond_1
    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2058
    :cond_2
    return-object v0
.end method

.method private static b(Llun;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Llun;",
            ")",
            "Ljava/util/List",
            "<",
            "Ldqv;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1607
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1608
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 1609
    if-eqz p0, :cond_2

    iget-object v0, p0, Llun;->b:[Lluk;

    if-eqz v0, :cond_2

    .line 1610
    iget-object v5, p0, Llun;->b:[Lluk;

    .line 1611
    array-length v6, v5

    move v2, v1

    .line 1612
    :goto_0
    if-ge v2, v6, :cond_2

    .line 1613
    aget-object v0, v5, v2

    if-eqz v0, :cond_1

    aget-object v0, v5, v2

    iget-object v0, v0, Lluk;->b:Llus;

    if-eqz v0, :cond_1

    aget-object v0, v5, v2

    iget-object v0, v0, Lluk;->b:Llus;

    iget-object v0, v0, Llus;->b:[Lluq;

    if-eqz v0, :cond_1

    .line 1615
    aget-object v0, v5, v2

    iget-object v0, v0, Lluk;->b:Llus;

    iget-object v7, v0, Llus;->b:[Lluq;

    .line 1616
    array-length v8, v7

    move v0, v1

    .line 1617
    :goto_1
    if-ge v0, v8, :cond_1

    .line 1618
    aget-object v9, v7, v0

    .line 1619
    if-eqz v9, :cond_0

    .line 1621
    iget-object v10, v9, Lluq;->c:Ljava/lang/String;

    invoke-virtual {v4, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 1622
    iget-object v10, v9, Lluq;->b:Ljava/lang/String;

    .line 1623
    invoke-static {v10}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1624
    new-instance v11, Ldqv;

    iget-object v12, v9, Lluq;->c:Ljava/lang/String;

    iget-object v13, v9, Lluq;->d:Ljava/lang/String;

    invoke-direct {v11, v12, v13, v10}, Ldqv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1626
    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1627
    iget-object v9, v9, Lluq;->c:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1617
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1612
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1634
    :cond_2
    return-object v3
.end method

.method public static b(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1862
    .line 1866
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1868
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 1869
    const-string v2, "seen"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1871
    const-string v2, "guns"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1872
    return-void
.end method

.method private static b(Landroid/content/Context;IIIZ)V
    .locals 4

    .prologue
    .line 2129
    .line 2130
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2131
    if-eqz p4, :cond_0

    const-string v0, "pending_delete=1 AND "

    .line 2133
    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2134
    invoke-static {p2, p3}, Ldsf;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2135
    :goto_1
    const-string v2, "guns"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2136
    return-void

    .line 2131
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 2134
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static b(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 897
    if-ne v0, p0, :cond_0

    const/4 v1, 0x4

    if-ne v1, p1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;III)[B
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2458
    .line 2459
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2460
    invoke-static {p2, p3}, Ldsf;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2461
    const-string v1, "account_status"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2464
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2465
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2467
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static c(Landroid/content/Context;III)J
    .locals 6

    .prologue
    .line 2264
    .line 2265
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2268
    :try_start_0
    invoke-static {p2, p3}, Ldsf;->c(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "account_status"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xe

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "SELECT "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "  FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 2267
    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2271
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;I)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 1912
    invoke-static {p0, p1}, Ldsf;->f(Landroid/content/Context;I)J

    move-result-wide v0

    .line 1913
    const-string v2, "read_state=0 AND pending_read=0 AND updated_version>%d"

    new-array v3, v9, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 1914
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1915
    invoke-static {}, Lffl;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v3, v4

    .line 1921
    :goto_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1922
    const-string v1, "guns"

    sget-object v2, Ldsh;->a:[Ljava/lang/String;

    const-string v7, "creation_time DESC"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1926
    if-eqz v0, :cond_2

    .line 1927
    const-string v1, "EsNotificationData"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1928
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1929
    const/16 v1, 0x9

    .line 1930
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    .line 1932
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1934
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    .line 1936
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x6f

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "getNotificationsToDisplay: unread notification key: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ", heading: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", description: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", timestamp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1915
    :cond_0
    const-string v1, "read_state=0 AND seen=0 AND push_enabled!=0 AND "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "(type!=97 AND type!=111 AND type!=18 AND type!=99)"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 1938
    :cond_1
    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1942
    :cond_2
    return-object v0
.end method

.method private static c(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2241
    invoke-static {p0, p1}, Ldsf;->b(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2242
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This method should not be used for high-priority unread notifications."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2245
    :cond_0
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    .line 2246
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 2247
    const-string v0, "last_read_notifications_sync_time"

    .line 2252
    :goto_0
    return-object v0

    .line 2249
    :cond_1
    const-string v0, "last_lowpri_read_notifications_sync_time"

    goto :goto_0

    .line 2252
    :cond_2
    const-string v0, "last_lowpri_unread_notifications_sync_time"

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;IZ)[B
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2510
    .line 2511
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2512
    if-eqz p2, :cond_0

    const-string v1, "read_low_notifications_summary"

    move-object v4, v1

    .line 2514
    :goto_0
    const-string v1, "account_status"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2517
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2518
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2520
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    .line 2512
    :cond_0
    const-string v1, "unread_low_notifications_summary"

    move-object v4, v1

    goto :goto_0

    .line 2520
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static c(Llun;)[B
    .locals 9

    .prologue
    .line 1681
    if-nez p0, :cond_0

    .line 1682
    const/4 v0, 0x0

    .line 1718
    :goto_0
    return-object v0

    .line 1686
    :cond_0
    new-instance v1, Llun;

    invoke-direct {v1}, Llun;-><init>()V

    .line 1687
    iget-object v0, p0, Llun;->a:Llut;

    if-eqz v0, :cond_2

    iget-object v0, p0, Llun;->a:Llut;

    iget-object v0, v0, Llut;->b:[Llup;

    if-nez v0, :cond_1

    iget-object v0, p0, Llun;->a:Llut;

    iget-object v0, v0, Llut;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1689
    :cond_1
    new-instance v0, Llut;

    invoke-direct {v0}, Llut;-><init>()V

    iput-object v0, v1, Llun;->a:Llut;

    .line 1690
    iget-object v0, v1, Llun;->a:Llut;

    iget-object v2, p0, Llun;->a:Llut;

    iget-object v2, v2, Llut;->b:[Llup;

    iput-object v2, v0, Llut;->b:[Llup;

    .line 1691
    iget-object v0, v1, Llun;->a:Llut;

    iget-object v2, p0, Llun;->a:Llut;

    iget-object v2, v2, Llut;->a:Ljava/lang/String;

    iput-object v2, v0, Llut;->a:Ljava/lang/String;

    .line 1694
    :cond_2
    iget-object v0, p0, Llun;->b:[Lluk;

    if-eqz v0, :cond_5

    iget-object v0, p0, Llun;->b:[Lluk;

    array-length v0, v0

    if-eqz v0, :cond_5

    .line 1695
    iget-object v2, p0, Llun;->b:[Lluk;

    .line 1696
    array-length v0, v2

    const/4 v3, 0x5

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1697
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1698
    const/4 v0, 0x0

    :goto_1
    array-length v5, v2

    if-ge v0, v5, :cond_4

    .line 1699
    aget-object v5, v2, v0

    .line 1700
    iget-object v6, v5, Lluk;->b:Llus;

    if-eqz v6, :cond_3

    .line 1701
    new-instance v6, Lluk;

    invoke-direct {v6}, Lluk;-><init>()V

    .line 1705
    new-instance v7, Llus;

    invoke-direct {v7}, Llus;-><init>()V

    .line 1706
    iget-object v8, v5, Lluk;->b:Llus;

    iget-object v8, v8, Llus;->c:Ljava/lang/String;

    iput-object v8, v7, Llus;->c:Ljava/lang/String;

    .line 1707
    iget-object v5, v5, Lluk;->b:Llus;

    iget-object v5, v5, Llus;->d:Ljava/lang/String;

    iput-object v5, v7, Llus;->d:Ljava/lang/String;

    .line 1708
    iput-object v7, v6, Lluk;->b:Llus;

    .line 1710
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1711
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-eq v5, v3, :cond_4

    .line 1712
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1715
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lluk;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lluk;

    iput-object v0, v1, Llun;->b:[Lluk;

    .line 1718
    :cond_5
    invoke-static {v1}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static d(II)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x2

    .line 2437
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 2438
    if-ne p0, v1, :cond_0

    .line 2439
    const-string v0, "next_read_notifications_fetch_param"

    .line 2450
    :goto_0
    return-object v0

    .line 2440
    :cond_0
    if-ne p0, v2, :cond_3

    .line 2441
    const-string v0, "next_unread_notifications_fetch_param"

    goto :goto_0

    .line 2443
    :cond_1
    if-ne p1, v1, :cond_3

    .line 2444
    if-ne p0, v1, :cond_2

    .line 2445
    const-string v0, "next_read_low_notifications_fetch_param"

    goto :goto_0

    .line 2446
    :cond_2
    if-ne p0, v2, :cond_3

    .line 2447
    const-string v0, "next_unread_low_notifications_fetch_param"

    goto :goto_0

    .line 2450
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;I)V
    .locals 5

    .prologue
    .line 2153
    .line 2154
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2155
    const-string v1, "DELETE FROM %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "guns"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ldsf;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2156
    return-void
.end method

.method public static e(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 2165
    invoke-static {p0, p1}, Lfhu;->a(Landroid/content/Context;I)V

    .line 2166
    invoke-static {p0, p1}, Lhsb;->c(Landroid/content/Context;I)V

    .line 2167
    return-void
.end method

.method public static f(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 2381
    .line 2382
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2384
    :try_start_0
    const-string v1, "SELECT last_viewed_notification_version  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2388
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 2396
    .line 2397
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2399
    :try_start_0
    const-string v1, "SELECT notification_poll_interval  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2403
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 2553
    .line 2554
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2556
    :try_start_0
    const-string v1, "SELECT last_notification_sync_version  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2560
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2570
    .line 2571
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2573
    const-string v1, "guns"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "key"

    aput-object v3, v2, v5

    const-string v3, "updated_version"

    aput-object v3, v2, v6

    const-string v3, "read_state!=1 OR pending_read=1"

    const-string v7, "updated_version DESC"

    const-string v8, "50"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2578
    if-nez v1, :cond_0

    .line 2593
    :goto_0
    return-object v4

    .line 2582
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2585
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2586
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2587
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2588
    new-instance v5, Landroid/util/Pair;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v5, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2591
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method
