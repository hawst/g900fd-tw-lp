.class public final Lmig;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojr;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12067
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12070
    iput-object v0, p0, Lmig;->apiHeader:Llyq;

    .line 12073
    iput-object v0, p0, Lmig;->a:Lojr;

    .line 12067
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12090
    const/4 v0, 0x0

    .line 12091
    iget-object v1, p0, Lmig;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 12092
    const/4 v0, 0x1

    iget-object v1, p0, Lmig;->apiHeader:Llyq;

    .line 12093
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12095
    :cond_0
    iget-object v1, p0, Lmig;->a:Lojr;

    if-eqz v1, :cond_1

    .line 12096
    const/4 v1, 0x2

    iget-object v2, p0, Lmig;->a:Lojr;

    .line 12097
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12099
    :cond_1
    iget-object v1, p0, Lmig;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12100
    iput v0, p0, Lmig;->ai:I

    .line 12101
    return v0
.end method

.method public a(Loxn;)Lmig;
    .locals 2

    .prologue
    .line 12109
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12110
    sparse-switch v0, :sswitch_data_0

    .line 12114
    iget-object v1, p0, Lmig;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12115
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmig;->ah:Ljava/util/List;

    .line 12118
    :cond_1
    iget-object v1, p0, Lmig;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12120
    :sswitch_0
    return-object p0

    .line 12125
    :sswitch_1
    iget-object v0, p0, Lmig;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 12126
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmig;->apiHeader:Llyq;

    .line 12128
    :cond_2
    iget-object v0, p0, Lmig;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12132
    :sswitch_2
    iget-object v0, p0, Lmig;->a:Lojr;

    if-nez v0, :cond_3

    .line 12133
    new-instance v0, Lojr;

    invoke-direct {v0}, Lojr;-><init>()V

    iput-object v0, p0, Lmig;->a:Lojr;

    .line 12135
    :cond_3
    iget-object v0, p0, Lmig;->a:Lojr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12110
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12078
    iget-object v0, p0, Lmig;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 12079
    const/4 v0, 0x1

    iget-object v1, p0, Lmig;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12081
    :cond_0
    iget-object v0, p0, Lmig;->a:Lojr;

    if-eqz v0, :cond_1

    .line 12082
    const/4 v0, 0x2

    iget-object v1, p0, Lmig;->a:Lojr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12084
    :cond_1
    iget-object v0, p0, Lmig;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12086
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12063
    invoke-virtual {p0, p1}, Lmig;->a(Loxn;)Lmig;

    move-result-object v0

    return-object v0
.end method
