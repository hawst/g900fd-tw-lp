.class public final Lmit;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lntv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16360
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16363
    iput-object v0, p0, Lmit;->apiHeader:Llyr;

    .line 16366
    iput-object v0, p0, Lmit;->a:Lntv;

    .line 16360
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 16383
    const/4 v0, 0x0

    .line 16384
    iget-object v1, p0, Lmit;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 16385
    const/4 v0, 0x1

    iget-object v1, p0, Lmit;->apiHeader:Llyr;

    .line 16386
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16388
    :cond_0
    iget-object v1, p0, Lmit;->a:Lntv;

    if-eqz v1, :cond_1

    .line 16389
    const/4 v1, 0x2

    iget-object v2, p0, Lmit;->a:Lntv;

    .line 16390
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16392
    :cond_1
    iget-object v1, p0, Lmit;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16393
    iput v0, p0, Lmit;->ai:I

    .line 16394
    return v0
.end method

.method public a(Loxn;)Lmit;
    .locals 2

    .prologue
    .line 16402
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 16403
    sparse-switch v0, :sswitch_data_0

    .line 16407
    iget-object v1, p0, Lmit;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 16408
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmit;->ah:Ljava/util/List;

    .line 16411
    :cond_1
    iget-object v1, p0, Lmit;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16413
    :sswitch_0
    return-object p0

    .line 16418
    :sswitch_1
    iget-object v0, p0, Lmit;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 16419
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmit;->apiHeader:Llyr;

    .line 16421
    :cond_2
    iget-object v0, p0, Lmit;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16425
    :sswitch_2
    iget-object v0, p0, Lmit;->a:Lntv;

    if-nez v0, :cond_3

    .line 16426
    new-instance v0, Lntv;

    invoke-direct {v0}, Lntv;-><init>()V

    iput-object v0, p0, Lmit;->a:Lntv;

    .line 16428
    :cond_3
    iget-object v0, p0, Lmit;->a:Lntv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 16403
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 16371
    iget-object v0, p0, Lmit;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 16372
    const/4 v0, 0x1

    iget-object v1, p0, Lmit;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16374
    :cond_0
    iget-object v0, p0, Lmit;->a:Lntv;

    if-eqz v0, :cond_1

    .line 16375
    const/4 v0, 0x2

    iget-object v1, p0, Lmit;->a:Lntv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 16377
    :cond_1
    iget-object v0, p0, Lmit;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 16379
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 16356
    invoke-virtual {p0, p1}, Lmit;->a(Loxn;)Lmit;

    move-result-object v0

    return-object v0
.end method
