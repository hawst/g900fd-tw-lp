.class public abstract Lmi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final q:I


# instance fields
.field private final a:Lmj;

.field private final b:Landroid/view/animation/Interpolator;

.field private final c:Landroid/view/View;

.field private d:Ljava/lang/Runnable;

.field private e:[F

.field private f:[F

.field private g:I

.field private h:I

.field private i:[F

.field private j:[F

.field private k:[F

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 194
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lmi;->q:I

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f7fffff    # Float.MAX_VALUE

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f000000    # 0.5f

    const v3, 0x3e4ccccd    # 0.2f

    const/4 v1, 0x2

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    new-instance v0, Lmj;

    invoke-direct {v0}, Lmj;-><init>()V

    iput-object v0, p0, Lmi;->a:Lmj;

    .line 140
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lmi;->b:Landroid/view/animation/Interpolator;

    .line 149
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lmi;->e:[F

    .line 152
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lmi;->f:[F

    .line 161
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, Lmi;->i:[F

    .line 164
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    iput-object v0, p0, Lmi;->j:[F

    .line 167
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    iput-object v0, p0, Lmi;->k:[F

    .line 210
    iput-object p1, p0, Lmi;->c:Landroid/view/View;

    .line 212
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 213
    const v1, 0x44c4e000    # 1575.0f

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v4

    float-to-int v1, v1

    .line 214
    const v2, 0x439d8000    # 315.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v4

    float-to-int v0, v0

    .line 215
    int-to-float v2, v1

    int-to-float v1, v1

    invoke-virtual {p0, v2, v1}, Lmi;->a(FF)Lmi;

    .line 216
    int-to-float v1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v1, v0}, Lmi;->b(FF)Lmi;

    .line 218
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmi;->a(I)Lmi;

    .line 219
    invoke-virtual {p0, v6, v6}, Lmi;->e(FF)Lmi;

    .line 220
    invoke-virtual {p0, v3, v3}, Lmi;->d(FF)Lmi;

    .line 221
    invoke-virtual {p0, v5, v5}, Lmi;->c(FF)Lmi;

    .line 222
    sget v0, Lmi;->q:I

    invoke-virtual {p0, v0}, Lmi;->b(I)Lmi;

    .line 223
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Lmi;->c(I)Lmi;

    .line 224
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Lmi;->d(I)Lmi;

    .line 225
    return-void

    .line 149
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 152
    :array_1
    .array-data 4
        0x7f7fffff    # Float.MAX_VALUE
        0x7f7fffff    # Float.MAX_VALUE
    .end array-data

    .line 161
    :array_2
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 164
    :array_3
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 167
    :array_4
    .array-data 4
        0x7f7fffff    # Float.MAX_VALUE
        0x7f7fffff    # Float.MAX_VALUE
    .end array-data
.end method

.method static synthetic a(FFF)F
    .locals 1

    .prologue
    .line 84
    invoke-static {p0, p1, p2}, Lmi;->b(FFF)F

    move-result v0

    return v0
.end method

.method private a(IFFF)F
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 539
    iget-object v0, p0, Lmi;->e:[F

    aget v0, v0, p1

    .line 540
    iget-object v2, p0, Lmi;->f:[F

    aget v2, v2, p1

    .line 541
    mul-float/2addr v0, p3

    invoke-static {v0, v1, v2}, Lmi;->b(FFF)F

    move-result v0

    invoke-direct {p0, p2, v0}, Lmi;->f(FF)F

    move-result v2

    sub-float v3, p3, p2

    invoke-direct {p0, v3, v0}, Lmi;->f(FF)F

    move-result v0

    sub-float/2addr v0, v2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    iget-object v2, p0, Lmi;->b:Landroid/view/animation/Interpolator;

    neg-float v0, v0

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    neg-float v0, v0

    :goto_0
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v0, v2, v3}, Lmi;->b(FFF)F

    move-result v0

    .line 542
    :goto_1
    cmpl-float v2, v0, v1

    if-nez v2, :cond_2

    move v0, v1

    .line 558
    :goto_2
    return v0

    .line 541
    :cond_0
    cmpl-float v2, v0, v1

    if-lez v2, :cond_1

    iget-object v2, p0, Lmi;->b:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 547
    :cond_2
    iget-object v2, p0, Lmi;->i:[F

    aget v2, v2, p1

    .line 548
    iget-object v3, p0, Lmi;->j:[F

    aget v3, v3, p1

    .line 549
    iget-object v4, p0, Lmi;->k:[F

    aget v4, v4, p1

    .line 550
    mul-float/2addr v2, p4

    .line 555
    cmpl-float v1, v0, v1

    if-lez v1, :cond_3

    .line 556
    mul-float/2addr v0, v2

    invoke-static {v0, v3, v4}, Lmi;->b(FFF)F

    move-result v0

    goto :goto_2

    .line 558
    :cond_3
    neg-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0, v3, v4}, Lmi;->b(FFF)F

    move-result v0

    neg-float v0, v0

    goto :goto_2
.end method

.method static synthetic a(III)I
    .locals 0

    .prologue
    .line 84
    if-le p0, p2, :cond_0

    :goto_0
    return p2

    :cond_0
    if-ge p0, p1, :cond_1

    move p2, p1

    goto :goto_0

    :cond_1
    move p2, p0

    goto :goto_0
.end method

.method static synthetic a(Lmi;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lmi;->o:Z

    return v0
.end method

.method static synthetic a(Lmi;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lmi;->m:Z

    return p1
.end method

.method private static b(FFF)F
    .locals 1

    .prologue
    .line 663
    cmpl-float v0, p0, p2

    if-lez v0, :cond_0

    .line 668
    :goto_0
    return p2

    .line 665
    :cond_0
    cmpg-float v0, p0, p1

    if-gez v0, :cond_1

    move p2, p1

    .line 666
    goto :goto_0

    :cond_1
    move p2, p0

    .line 668
    goto :goto_0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 492
    iget-object v0, p0, Lmi;->a:Lmj;

    .line 493
    invoke-virtual {v0}, Lmj;->f()I

    move-result v1

    .line 494
    invoke-virtual {v0}, Lmj;->e()I

    move-result v0

    .line 496
    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lmi;->f(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lmi;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lmi;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lmi;->m:Z

    return v0
.end method

.method static synthetic b(Lmi;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lmi;->o:Z

    return p1
.end method

.method static synthetic c(Lmi;)Lmj;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lmi;->a:Lmj;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 528
    iget-boolean v0, p0, Lmi;->m:Z

    if-eqz v0, :cond_0

    .line 531
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmi;->o:Z

    .line 535
    :goto_0
    return-void

    .line 533
    :cond_0
    iget-object v0, p0, Lmi;->a:Lmj;

    invoke-virtual {v0}, Lmj;->b()V

    goto :goto_0
.end method

.method static synthetic c(Lmi;Z)Z
    .locals 0

    .prologue
    .line 84
    iput-boolean p1, p0, Lmi;->n:Z

    return p1
.end method

.method static synthetic d(Lmi;)Z
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Lmi;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lmi;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lmi;->n:Z

    return v0
.end method

.method private f(FF)F
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 624
    cmpl-float v2, p2, v0

    if-nez v2, :cond_1

    .line 649
    :cond_0
    :goto_0
    return v0

    .line 628
    :cond_1
    iget v2, p0, Lmi;->g:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 631
    :pswitch_0
    cmpg-float v2, p1, p2

    if-gez v2, :cond_0

    .line 632
    cmpl-float v2, p1, v0

    if-ltz v2, :cond_2

    .line 634
    div-float v0, p1, p2

    sub-float v0, v1, v0

    goto :goto_0

    .line 635
    :cond_2
    iget-boolean v2, p0, Lmi;->o:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lmi;->g:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 637
    goto :goto_0

    .line 642
    :pswitch_1
    cmpg-float v1, p1, v0

    if-gez v1, :cond_0

    .line 644
    neg-float v0, p2

    div-float v0, p1, v0

    goto :goto_0

    .line 628
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic f(Lmi;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 84
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Lmi;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    return-void
.end method

.method static synthetic g(Lmi;)Landroid/view/View;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lmi;->c:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a(FF)Lmi;
    .locals 4

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    .line 294
    iget-object v0, p0, Lmi;->k:[F

    const/4 v1, 0x0

    div-float v2, p1, v3

    aput v2, v0, v1

    .line 295
    iget-object v0, p0, Lmi;->k:[F

    const/4 v1, 0x1

    div-float v2, p2, v3

    aput v2, v0, v1

    .line 296
    return-object p0
.end method

.method public a(I)Lmi;
    .locals 0

    .prologue
    .line 354
    iput p1, p0, Lmi;->g:I

    .line 355
    return-object p0
.end method

.method public a(Z)Lmi;
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lmi;->p:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 236
    invoke-direct {p0}, Lmi;->c()V

    .line 239
    :cond_0
    iput-boolean p1, p0, Lmi;->p:Z

    .line 240
    return-object p0
.end method

.method public abstract a()Z
.end method

.method public b(FF)Lmi;
    .locals 4

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    .line 312
    iget-object v0, p0, Lmi;->j:[F

    const/4 v1, 0x0

    div-float v2, p1, v3

    aput v2, v0, v1

    .line 313
    iget-object v0, p0, Lmi;->j:[F

    const/4 v1, 0x1

    div-float v2, p2, v3

    aput v2, v0, v1

    .line 314
    return-object p0
.end method

.method public b(I)Lmi;
    .locals 0

    .prologue
    .line 412
    iput p1, p0, Lmi;->h:I

    .line 413
    return-object p0
.end method

.method public c(FF)Lmi;
    .locals 4

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    .line 333
    iget-object v0, p0, Lmi;->i:[F

    const/4 v1, 0x0

    div-float v2, p1, v3

    aput v2, v0, v1

    .line 334
    iget-object v0, p0, Lmi;->i:[F

    const/4 v1, 0x1

    div-float v2, p2, v3

    aput v2, v0, v1

    .line 335
    return-object p0
.end method

.method public c(I)Lmi;
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lmi;->a:Lmj;

    invoke-virtual {v0, p1}, Lmj;->a(I)V

    .line 428
    return-object p0
.end method

.method public d(FF)Lmi;
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lmi;->e:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 374
    iget-object v0, p0, Lmi;->e:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 375
    return-object p0
.end method

.method public d(I)Lmi;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lmi;->a:Lmj;

    invoke-virtual {v0, p1}, Lmj;->b(I)V

    .line 443
    return-object p0
.end method

.method public e(FF)Lmi;
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lmi;->f:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 396
    iget-object v0, p0, Lmi;->f:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 397
    return-object p0
.end method

.method public abstract e(I)V
.end method

.method public abstract f(I)Z
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 456
    iget-boolean v0, p0, Lmi;->p:Z

    if-nez v0, :cond_1

    .line 485
    :cond_0
    :goto_0
    return v5

    .line 460
    :cond_1
    invoke-static {p2}, Lik;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 461
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 463
    :pswitch_0
    iput-boolean v4, p0, Lmi;->n:Z

    .line 464
    iput-boolean v5, p0, Lmi;->l:Z

    .line 467
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lmi;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {p0, v5, v0, v1, v2}, Lmi;->a(IFFF)F

    move-result v0

    .line 469
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lmi;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {p0, v4, v1, v2, v3}, Lmi;->a(IFFF)F

    move-result v1

    .line 471
    iget-object v2, p0, Lmi;->a:Lmj;

    invoke-virtual {v2, v0, v1}, Lmj;->a(FF)V

    .line 475
    iget-boolean v0, p0, Lmi;->o:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmi;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lmi;->d:Ljava/lang/Runnable;

    if-nez v0, :cond_2

    new-instance v0, Lmk;

    invoke-direct {v0, p0}, Lmk;-><init>(Lmi;)V

    iput-object v0, p0, Lmi;->d:Ljava/lang/Runnable;

    :cond_2
    iput-boolean v4, p0, Lmi;->o:Z

    iput-boolean v4, p0, Lmi;->m:Z

    iget-boolean v0, p0, Lmi;->l:Z

    if-nez v0, :cond_3

    iget v0, p0, Lmi;->h:I

    if-lez v0, :cond_3

    iget-object v0, p0, Lmi;->c:Landroid/view/View;

    iget-object v1, p0, Lmi;->d:Ljava/lang/Runnable;

    iget v2, p0, Lmi;->h:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Liu;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    :goto_1
    iput-boolean v4, p0, Lmi;->l:Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmi;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 481
    :pswitch_2
    invoke-direct {p0}, Lmi;->c()V

    goto :goto_0

    .line 461
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
