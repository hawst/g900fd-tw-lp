.class public final Lnra;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnqx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    sget-object v0, Lnqx;->a:[Lnqx;

    iput-object v0, p0, Lnra;->a:[Lnqx;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 31
    .line 32
    iget-object v1, p0, Lnra;->a:[Lnqx;

    if-eqz v1, :cond_1

    .line 33
    iget-object v2, p0, Lnra;->a:[Lnqx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 34
    if-eqz v4, :cond_0

    .line 35
    const/4 v5, 0x1

    .line 36
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 33
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    :cond_1
    iget-object v1, p0, Lnra;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    iput v0, p0, Lnra;->ai:I

    .line 42
    return v0
.end method

.method public a(Loxn;)Lnra;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 50
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 51
    sparse-switch v0, :sswitch_data_0

    .line 55
    iget-object v2, p0, Lnra;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnra;->ah:Ljava/util/List;

    .line 59
    :cond_1
    iget-object v2, p0, Lnra;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    :sswitch_0
    return-object p0

    .line 66
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 67
    iget-object v0, p0, Lnra;->a:[Lnqx;

    if-nez v0, :cond_3

    move v0, v1

    .line 68
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnqx;

    .line 69
    iget-object v3, p0, Lnra;->a:[Lnqx;

    if-eqz v3, :cond_2

    .line 70
    iget-object v3, p0, Lnra;->a:[Lnqx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    :cond_2
    iput-object v2, p0, Lnra;->a:[Lnqx;

    .line 73
    :goto_2
    iget-object v2, p0, Lnra;->a:[Lnqx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 74
    iget-object v2, p0, Lnra;->a:[Lnqx;

    new-instance v3, Lnqx;

    invoke-direct {v3}, Lnqx;-><init>()V

    aput-object v3, v2, v0

    .line 75
    iget-object v2, p0, Lnra;->a:[Lnqx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 76
    invoke-virtual {p1}, Loxn;->a()I

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 67
    :cond_3
    iget-object v0, p0, Lnra;->a:[Lnqx;

    array-length v0, v0

    goto :goto_1

    .line 79
    :cond_4
    iget-object v2, p0, Lnra;->a:[Lnqx;

    new-instance v3, Lnqx;

    invoke-direct {v3}, Lnqx;-><init>()V

    aput-object v3, v2, v0

    .line 80
    iget-object v2, p0, Lnra;->a:[Lnqx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 51
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 18
    iget-object v0, p0, Lnra;->a:[Lnqx;

    if-eqz v0, :cond_1

    .line 19
    iget-object v1, p0, Lnra;->a:[Lnqx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 20
    if-eqz v3, :cond_0

    .line 21
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 19
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 25
    :cond_1
    iget-object v0, p0, Lnra;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnra;->a(Loxn;)Lnra;

    move-result-object v0

    return-object v0
.end method
