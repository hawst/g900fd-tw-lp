.class public final Loqz;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lora;

.field private c:Losf;

.field private d:Losf;

.field private e:Ljava/lang/String;

.field private f:Lorc;

.field private g:Ljava/lang/Integer;

.field private h:I

.field private i:Ljava/lang/Integer;

.field private j:Lorb;

.field private k:[Losg;

.field private l:Loqx;

.field private m:[Losh;

.field private n:Losj;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Losq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 280
    invoke-direct {p0}, Loxq;-><init>()V

    .line 291
    iput-object v1, p0, Loqz;->b:Lora;

    .line 294
    iput-object v1, p0, Loqz;->c:Losf;

    .line 297
    iput-object v1, p0, Loqz;->d:Losf;

    .line 302
    iput-object v1, p0, Loqz;->f:Lorc;

    .line 307
    const/high16 v0, -0x80000000

    iput v0, p0, Loqz;->h:I

    .line 312
    iput-object v1, p0, Loqz;->j:Lorb;

    .line 315
    sget-object v0, Losg;->a:[Losg;

    iput-object v0, p0, Loqz;->k:[Losg;

    .line 318
    iput-object v1, p0, Loqz;->l:Loqx;

    .line 321
    sget-object v0, Losh;->a:[Losh;

    iput-object v0, p0, Loqz;->m:[Losh;

    .line 324
    iput-object v1, p0, Loqz;->n:Losj;

    .line 331
    iput-object v1, p0, Loqz;->q:Losq;

    .line 280
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 401
    .line 402
    iget-object v0, p0, Loqz;->a:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 403
    const/4 v0, 0x1

    iget-object v2, p0, Loqz;->a:Ljava/lang/String;

    .line 404
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 406
    :goto_0
    iget-object v2, p0, Loqz;->b:Lora;

    if-eqz v2, :cond_0

    .line 407
    const/4 v2, 0x2

    iget-object v3, p0, Loqz;->b:Lora;

    .line 408
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 410
    :cond_0
    iget-object v2, p0, Loqz;->c:Losf;

    if-eqz v2, :cond_1

    .line 411
    const/4 v2, 0x3

    iget-object v3, p0, Loqz;->c:Losf;

    .line 412
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 414
    :cond_1
    iget-object v2, p0, Loqz;->d:Losf;

    if-eqz v2, :cond_2

    .line 415
    const/4 v2, 0x4

    iget-object v3, p0, Loqz;->d:Losf;

    .line 416
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 418
    :cond_2
    iget-object v2, p0, Loqz;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 419
    const/4 v2, 0x5

    iget-object v3, p0, Loqz;->e:Ljava/lang/String;

    .line 420
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 422
    :cond_3
    iget-object v2, p0, Loqz;->f:Lorc;

    if-eqz v2, :cond_4

    .line 423
    const/4 v2, 0x6

    iget-object v3, p0, Loqz;->f:Lorc;

    .line 424
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 426
    :cond_4
    iget-object v2, p0, Loqz;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 427
    const/4 v2, 0x7

    iget-object v3, p0, Loqz;->g:Ljava/lang/Integer;

    .line 428
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 430
    :cond_5
    iget v2, p0, Loqz;->h:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_6

    .line 431
    const/16 v2, 0x8

    iget v3, p0, Loqz;->h:I

    .line 432
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 434
    :cond_6
    iget-object v2, p0, Loqz;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 435
    const/16 v2, 0x9

    iget-object v3, p0, Loqz;->i:Ljava/lang/Integer;

    .line 436
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 438
    :cond_7
    iget-object v2, p0, Loqz;->j:Lorb;

    if-eqz v2, :cond_8

    .line 439
    const/16 v2, 0xa

    iget-object v3, p0, Loqz;->j:Lorb;

    .line 440
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 442
    :cond_8
    iget-object v2, p0, Loqz;->k:[Losg;

    if-eqz v2, :cond_a

    .line 443
    iget-object v3, p0, Loqz;->k:[Losg;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_a

    aget-object v5, v3, v2

    .line 444
    if-eqz v5, :cond_9

    .line 445
    const/16 v6, 0xb

    .line 446
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 443
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 450
    :cond_a
    iget-object v2, p0, Loqz;->l:Loqx;

    if-eqz v2, :cond_b

    .line 451
    const/16 v2, 0xc

    iget-object v3, p0, Loqz;->l:Loqx;

    .line 452
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 454
    :cond_b
    iget-object v2, p0, Loqz;->m:[Losh;

    if-eqz v2, :cond_d

    .line 455
    iget-object v2, p0, Loqz;->m:[Losh;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    .line 456
    if-eqz v4, :cond_c

    .line 457
    const/16 v5, 0xd

    .line 458
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 455
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 462
    :cond_d
    iget-object v1, p0, Loqz;->n:Losj;

    if-eqz v1, :cond_e

    .line 463
    const/16 v1, 0xe

    iget-object v2, p0, Loqz;->n:Losj;

    .line 464
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 466
    :cond_e
    iget-object v1, p0, Loqz;->o:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 467
    const/16 v1, 0xf

    iget-object v2, p0, Loqz;->o:Ljava/lang/String;

    .line 468
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 470
    :cond_f
    iget-object v1, p0, Loqz;->p:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 471
    const/16 v1, 0x10

    iget-object v2, p0, Loqz;->p:Ljava/lang/String;

    .line 472
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 474
    :cond_10
    iget-object v1, p0, Loqz;->q:Losq;

    if-eqz v1, :cond_11

    .line 475
    const/16 v1, 0x11

    iget-object v2, p0, Loqz;->q:Losq;

    .line 476
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 478
    :cond_11
    iget-object v1, p0, Loqz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 479
    iput v0, p0, Loqz;->ai:I

    .line 480
    return v0

    :cond_12
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loqz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 488
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 489
    sparse-switch v0, :sswitch_data_0

    .line 493
    iget-object v2, p0, Loqz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 494
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loqz;->ah:Ljava/util/List;

    .line 497
    :cond_1
    iget-object v2, p0, Loqz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 499
    :sswitch_0
    return-object p0

    .line 504
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loqz;->a:Ljava/lang/String;

    goto :goto_0

    .line 508
    :sswitch_2
    iget-object v0, p0, Loqz;->b:Lora;

    if-nez v0, :cond_2

    .line 509
    new-instance v0, Lora;

    invoke-direct {v0}, Lora;-><init>()V

    iput-object v0, p0, Loqz;->b:Lora;

    .line 511
    :cond_2
    iget-object v0, p0, Loqz;->b:Lora;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 515
    :sswitch_3
    iget-object v0, p0, Loqz;->c:Losf;

    if-nez v0, :cond_3

    .line 516
    new-instance v0, Losf;

    invoke-direct {v0}, Losf;-><init>()V

    iput-object v0, p0, Loqz;->c:Losf;

    .line 518
    :cond_3
    iget-object v0, p0, Loqz;->c:Losf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 522
    :sswitch_4
    iget-object v0, p0, Loqz;->d:Losf;

    if-nez v0, :cond_4

    .line 523
    new-instance v0, Losf;

    invoke-direct {v0}, Losf;-><init>()V

    iput-object v0, p0, Loqz;->d:Losf;

    .line 525
    :cond_4
    iget-object v0, p0, Loqz;->d:Losf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 529
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loqz;->e:Ljava/lang/String;

    goto :goto_0

    .line 533
    :sswitch_6
    iget-object v0, p0, Loqz;->f:Lorc;

    if-nez v0, :cond_5

    .line 534
    new-instance v0, Lorc;

    invoke-direct {v0}, Lorc;-><init>()V

    iput-object v0, p0, Loqz;->f:Lorc;

    .line 536
    :cond_5
    iget-object v0, p0, Loqz;->f:Lorc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 540
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loqz;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 544
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 545
    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-eq v0, v2, :cond_6

    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    .line 548
    :cond_6
    iput v0, p0, Loqz;->h:I

    goto/16 :goto_0

    .line 550
    :cond_7
    iput v1, p0, Loqz;->h:I

    goto/16 :goto_0

    .line 555
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loqz;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 559
    :sswitch_a
    iget-object v0, p0, Loqz;->j:Lorb;

    if-nez v0, :cond_8

    .line 560
    new-instance v0, Lorb;

    invoke-direct {v0}, Lorb;-><init>()V

    iput-object v0, p0, Loqz;->j:Lorb;

    .line 562
    :cond_8
    iget-object v0, p0, Loqz;->j:Lorb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 566
    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 567
    iget-object v0, p0, Loqz;->k:[Losg;

    if-nez v0, :cond_a

    move v0, v1

    .line 568
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Losg;

    .line 569
    iget-object v3, p0, Loqz;->k:[Losg;

    if-eqz v3, :cond_9

    .line 570
    iget-object v3, p0, Loqz;->k:[Losg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 572
    :cond_9
    iput-object v2, p0, Loqz;->k:[Losg;

    .line 573
    :goto_2
    iget-object v2, p0, Loqz;->k:[Losg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 574
    iget-object v2, p0, Loqz;->k:[Losg;

    new-instance v3, Losg;

    invoke-direct {v3}, Losg;-><init>()V

    aput-object v3, v2, v0

    .line 575
    iget-object v2, p0, Loqz;->k:[Losg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 576
    invoke-virtual {p1}, Loxn;->a()I

    .line 573
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 567
    :cond_a
    iget-object v0, p0, Loqz;->k:[Losg;

    array-length v0, v0

    goto :goto_1

    .line 579
    :cond_b
    iget-object v2, p0, Loqz;->k:[Losg;

    new-instance v3, Losg;

    invoke-direct {v3}, Losg;-><init>()V

    aput-object v3, v2, v0

    .line 580
    iget-object v2, p0, Loqz;->k:[Losg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 584
    :sswitch_c
    iget-object v0, p0, Loqz;->l:Loqx;

    if-nez v0, :cond_c

    .line 585
    new-instance v0, Loqx;

    invoke-direct {v0}, Loqx;-><init>()V

    iput-object v0, p0, Loqz;->l:Loqx;

    .line 587
    :cond_c
    iget-object v0, p0, Loqz;->l:Loqx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 591
    :sswitch_d
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 592
    iget-object v0, p0, Loqz;->m:[Losh;

    if-nez v0, :cond_e

    move v0, v1

    .line 593
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Losh;

    .line 594
    iget-object v3, p0, Loqz;->m:[Losh;

    if-eqz v3, :cond_d

    .line 595
    iget-object v3, p0, Loqz;->m:[Losh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 597
    :cond_d
    iput-object v2, p0, Loqz;->m:[Losh;

    .line 598
    :goto_4
    iget-object v2, p0, Loqz;->m:[Losh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    .line 599
    iget-object v2, p0, Loqz;->m:[Losh;

    new-instance v3, Losh;

    invoke-direct {v3}, Losh;-><init>()V

    aput-object v3, v2, v0

    .line 600
    iget-object v2, p0, Loqz;->m:[Losh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 601
    invoke-virtual {p1}, Loxn;->a()I

    .line 598
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 592
    :cond_e
    iget-object v0, p0, Loqz;->m:[Losh;

    array-length v0, v0

    goto :goto_3

    .line 604
    :cond_f
    iget-object v2, p0, Loqz;->m:[Losh;

    new-instance v3, Losh;

    invoke-direct {v3}, Losh;-><init>()V

    aput-object v3, v2, v0

    .line 605
    iget-object v2, p0, Loqz;->m:[Losh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 609
    :sswitch_e
    iget-object v0, p0, Loqz;->n:Losj;

    if-nez v0, :cond_10

    .line 610
    new-instance v0, Losj;

    invoke-direct {v0}, Losj;-><init>()V

    iput-object v0, p0, Loqz;->n:Losj;

    .line 612
    :cond_10
    iget-object v0, p0, Loqz;->n:Losj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 616
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loqz;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 620
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loqz;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 624
    :sswitch_11
    iget-object v0, p0, Loqz;->q:Losq;

    if-nez v0, :cond_11

    .line 625
    new-instance v0, Losq;

    invoke-direct {v0}, Losq;-><init>()V

    iput-object v0, p0, Loqz;->q:Losq;

    .line 627
    :cond_11
    iget-object v0, p0, Loqz;->q:Losq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 489
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 336
    iget-object v1, p0, Loqz;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 337
    const/4 v1, 0x1

    iget-object v2, p0, Loqz;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 339
    :cond_0
    iget-object v1, p0, Loqz;->b:Lora;

    if-eqz v1, :cond_1

    .line 340
    const/4 v1, 0x2

    iget-object v2, p0, Loqz;->b:Lora;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 342
    :cond_1
    iget-object v1, p0, Loqz;->c:Losf;

    if-eqz v1, :cond_2

    .line 343
    const/4 v1, 0x3

    iget-object v2, p0, Loqz;->c:Losf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 345
    :cond_2
    iget-object v1, p0, Loqz;->d:Losf;

    if-eqz v1, :cond_3

    .line 346
    const/4 v1, 0x4

    iget-object v2, p0, Loqz;->d:Losf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 348
    :cond_3
    iget-object v1, p0, Loqz;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 349
    const/4 v1, 0x5

    iget-object v2, p0, Loqz;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 351
    :cond_4
    iget-object v1, p0, Loqz;->f:Lorc;

    if-eqz v1, :cond_5

    .line 352
    const/4 v1, 0x6

    iget-object v2, p0, Loqz;->f:Lorc;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 354
    :cond_5
    iget-object v1, p0, Loqz;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 355
    const/4 v1, 0x7

    iget-object v2, p0, Loqz;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 357
    :cond_6
    iget v1, p0, Loqz;->h:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_7

    .line 358
    const/16 v1, 0x8

    iget v2, p0, Loqz;->h:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 360
    :cond_7
    iget-object v1, p0, Loqz;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 361
    const/16 v1, 0x9

    iget-object v2, p0, Loqz;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 363
    :cond_8
    iget-object v1, p0, Loqz;->j:Lorb;

    if-eqz v1, :cond_9

    .line 364
    const/16 v1, 0xa

    iget-object v2, p0, Loqz;->j:Lorb;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 366
    :cond_9
    iget-object v1, p0, Loqz;->k:[Losg;

    if-eqz v1, :cond_b

    .line 367
    iget-object v2, p0, Loqz;->k:[Losg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 368
    if-eqz v4, :cond_a

    .line 369
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 367
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 373
    :cond_b
    iget-object v1, p0, Loqz;->l:Loqx;

    if-eqz v1, :cond_c

    .line 374
    const/16 v1, 0xc

    iget-object v2, p0, Loqz;->l:Loqx;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 376
    :cond_c
    iget-object v1, p0, Loqz;->m:[Losh;

    if-eqz v1, :cond_e

    .line 377
    iget-object v1, p0, Loqz;->m:[Losh;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 378
    if-eqz v3, :cond_d

    .line 379
    const/16 v4, 0xd

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 377
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 383
    :cond_e
    iget-object v0, p0, Loqz;->n:Losj;

    if-eqz v0, :cond_f

    .line 384
    const/16 v0, 0xe

    iget-object v1, p0, Loqz;->n:Losj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 386
    :cond_f
    iget-object v0, p0, Loqz;->o:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 387
    const/16 v0, 0xf

    iget-object v1, p0, Loqz;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 389
    :cond_10
    iget-object v0, p0, Loqz;->p:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 390
    const/16 v0, 0x10

    iget-object v1, p0, Loqz;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 392
    :cond_11
    iget-object v0, p0, Loqz;->q:Losq;

    if-eqz v0, :cond_12

    .line 393
    const/16 v0, 0x11

    iget-object v1, p0, Loqz;->q:Losq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 395
    :cond_12
    iget-object v0, p0, Loqz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 397
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 276
    invoke-virtual {p0, p1}, Loqz;->a(Loxn;)Loqz;

    move-result-object v0

    return-object v0
.end method
