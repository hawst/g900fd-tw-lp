.class public final Lpop;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpop;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lpoo;

.field private c:Lpoo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 186
    const v0, 0x2e57430

    new-instance v1, Lpoq;

    invoke-direct {v1}, Lpoq;-><init>()V

    .line 191
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpop;->a:Loxr;

    .line 190
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    iget-object v1, p0, Lpop;->b:Lpoo;

    if-eqz v1, :cond_0

    .line 216
    const/4 v0, 0x1

    iget-object v1, p0, Lpop;->b:Lpoo;

    .line 217
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 219
    :cond_0
    iget-object v1, p0, Lpop;->c:Lpoo;

    if-eqz v1, :cond_1

    .line 220
    const/4 v1, 0x2

    iget-object v2, p0, Lpop;->c:Lpoo;

    .line 221
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_1
    iget-object v1, p0, Lpop;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    iput v0, p0, Lpop;->ai:I

    .line 225
    return v0
.end method

.method public a(Loxn;)Lpop;
    .locals 2

    .prologue
    .line 233
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 234
    sparse-switch v0, :sswitch_data_0

    .line 238
    iget-object v1, p0, Lpop;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 239
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpop;->ah:Ljava/util/List;

    .line 242
    :cond_1
    iget-object v1, p0, Lpop;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    :sswitch_0
    return-object p0

    .line 249
    :sswitch_1
    iget-object v0, p0, Lpop;->b:Lpoo;

    if-nez v0, :cond_2

    .line 250
    new-instance v0, Lpoo;

    invoke-direct {v0}, Lpoo;-><init>()V

    iput-object v0, p0, Lpop;->b:Lpoo;

    .line 252
    :cond_2
    iget-object v0, p0, Lpop;->b:Lpoo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 256
    :sswitch_2
    iget-object v0, p0, Lpop;->c:Lpoo;

    if-nez v0, :cond_3

    .line 257
    new-instance v0, Lpoo;

    invoke-direct {v0}, Lpoo;-><init>()V

    iput-object v0, p0, Lpop;->c:Lpoo;

    .line 259
    :cond_3
    iget-object v0, p0, Lpop;->c:Lpoo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 234
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lpop;->b:Lpoo;

    if-eqz v0, :cond_0

    .line 203
    const/4 v0, 0x1

    iget-object v1, p0, Lpop;->b:Lpoo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 205
    :cond_0
    iget-object v0, p0, Lpop;->c:Lpoo;

    if-eqz v0, :cond_1

    .line 206
    const/4 v0, 0x2

    iget-object v1, p0, Lpop;->c:Lpoo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 208
    :cond_1
    iget-object v0, p0, Lpop;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 210
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0, p1}, Lpop;->a(Loxn;)Lpop;

    move-result-object v0

    return-object v0
.end method
