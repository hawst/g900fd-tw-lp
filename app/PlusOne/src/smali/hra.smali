.class final Lhra;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhpl;


# instance fields
.field private final a:Ljgn;

.field private volatile b:Z

.field private c:Lhpj;

.field private final d:Lhrj;

.field private final e:Lhrd;

.field private final f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

.field private final g:Landroid/content/Context;

.field private final h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

.field private final i:I

.field private final j:I

.field private final k:Ljava/lang/String;

.field private final l:Lhrm;

.field private final m:Lhei;

.field private final n:Lhpg;

.field private final o:Lhqz;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-boolean v1, p0, Lhra;->b:Z

    .line 107
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Llsk;->a(Z)V

    .line 108
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v0

    iput v0, p0, Lhra;->i:I

    .line 109
    iput-object p1, p0, Lhra;->g:Landroid/content/Context;

    .line 110
    invoke-static {p1}, Lhrm;->a(Landroid/content/Context;)Lhrm;

    move-result-object v0

    iput-object v0, p0, Lhra;->l:Lhrm;

    .line 111
    iput-object p2, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 112
    const-class v0, Lhrj;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrj;

    iput-object v0, p0, Lhra;->d:Lhrj;

    .line 113
    invoke-static {p1}, Lhre;->a(Landroid/content/Context;)Lhre;

    move-result-object v0

    iput-object v0, p0, Lhra;->e:Lhrd;

    .line 114
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    move-result-object v0

    iput-object v0, p0, Lhra;->f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 115
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    const-class v3, Ljgn;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    iput-object v0, p0, Lhra;->a:Ljgn;

    .line 116
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    .line 117
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e()Z

    move-result v3

    .line 118
    shl-int/lit8 v0, v0, 0x1

    if-eqz v3, :cond_1

    :goto_1
    or-int/2addr v0, v2

    iput v0, p0, Lhra;->j:I

    .line 119
    invoke-virtual {p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhra;->k:Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhra;->m:Lhei;

    .line 121
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    const-class v1, Lhpg;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpg;

    iput-object v0, p0, Lhra;->n:Lhpg;

    .line 122
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    const-class v1, Lhqz;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqz;

    iput-object v0, p0, Lhra;->o:Lhqz;

    .line 123
    return-void

    :cond_0
    move v0, v2

    .line 107
    goto :goto_0

    :cond_1
    move v2, v1

    .line 118
    goto :goto_1
.end method

.method private a(JJ)J
    .locals 5

    .prologue
    .line 1024
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->t()J

    move-result-wide v0

    .line 1026
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1027
    add-long v0, p1, p3

    .line 1028
    iget-object v2, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 1030
    :cond_0
    const-string v2, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1031
    iget-object v2, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "+++ RETRY until "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1033
    :cond_1
    return-wide v0
.end method

.method private a(IJLjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1039
    iget-object v1, p0, Lhra;->l:Lhrm;

    monitor-enter v1

    .line 1040
    :try_start_0
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->w()Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 1041
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 1042
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v0

    .line 1041
    invoke-direct {p0, v0, p2, p3, p4}, Lhra;->b(IJLjava/lang/Throwable;)V

    .line 1043
    iget-object v0, p0, Lhra;->l:Lhrm;

    invoke-virtual {v0}, Lhrm;->b()V

    .line 1044
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(ILjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1048
    const-wide/32 v0, 0x240c8400

    invoke-direct {p0, p1, v0, v1, p2}, Lhra;->a(IJLjava/lang/Throwable;)V

    .line 1049
    return-void
.end method

.method private a(Landroid/content/SyncStats;I)V
    .locals 4

    .prologue
    .line 639
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0xb

    .line 640
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    .line 641
    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 642
    iget-wide v0, p1, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p1, Landroid/content/SyncStats;->numSkippedEntries:J

    .line 643
    return-void
.end method

.method private a(Landroid/content/SyncStats;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 472
    iget-wide v0, p1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 473
    const/4 v0, 0x3

    invoke-direct {p0, v0, p2}, Lhra;->a(ILjava/lang/Throwable;)V

    .line 474
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhra;->b(Z)V

    .line 475
    return-void
.end method

.method private a(Lhpj;)V
    .locals 2

    .prologue
    .line 461
    iget-object v1, p0, Lhra;->l:Lhrm;

    monitor-enter v1

    .line 462
    :try_start_0
    iput-object p1, p0, Lhra;->c:Lhpj;

    .line 463
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 618
    const/4 v1, 0x0

    .line 620
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 621
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-gez v2, :cond_1

    .line 622
    if-eqz v1, :cond_0

    .line 629
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 635
    :cond_0
    :goto_0
    return v0

    .line 627
    :cond_1
    if-eqz v1, :cond_2

    .line 629
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 635
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 625
    :catch_0
    move-exception v2

    if-eqz v1, :cond_0

    .line 629
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 627
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 629
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 632
    :cond_3
    :goto_2
    throw v0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method private a(Landroid/content/SyncStats;)Z
    .locals 10

    .prologue
    .line 484
    iget-object v1, p0, Lhra;->l:Lhrm;

    monitor-enter v1

    .line 485
    :try_start_0
    iget-boolean v0, p0, Lhra;->b:Z

    if-nez v0, :cond_0

    .line 486
    const/4 v0, 0x0

    monitor-exit v1

    .line 607
    :goto_0
    return v0

    .line 488
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 491
    sget-object v1, Lhpi;->a:Lief;

    iget v2, p0, Lhra;->i:I

    invoke-interface {v0, v1, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    .line 493
    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 494
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 495
    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v3

    .line 498
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 499
    iget-object v6, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v6}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->t()J

    move-result-wide v6

    .line 500
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-eqz v8, :cond_2

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 501
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502
    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "+++ SKIP task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; exceed retry time; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    :cond_1
    const/16 v0, 0x28

    invoke-direct {p0, p1, v0}, Lhra;->a(Landroid/content/SyncStats;I)V

    .line 506
    const/4 v0, 0x0

    goto :goto_0

    .line 488
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 510
    :cond_2
    const/16 v4, 0x14

    if-ne v3, v4, :cond_4

    if-nez v0, :cond_4

    .line 511
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 512
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "+++ SKIP record; instant share disabled; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    :cond_3
    const/16 v0, 0x24

    invoke-direct {p0, p1, v0}, Lhra;->a(Landroid/content/SyncStats;I)V

    .line 515
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 519
    :cond_4
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    .line 520
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1}, Lhqn;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 521
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 522
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "+++ SKIP record; has google exif; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    :cond_5
    const/16 v0, 0x25

    invoke-direct {p0, p1, v0}, Lhra;->a(Landroid/content/SyncStats;I)V

    .line 525
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 529
    :cond_6
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    invoke-static {v0, v1}, Lhra;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 530
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 531
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "+++ SKIP record; media removed; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 533
    :cond_7
    const/16 v0, 0x29

    invoke-direct {p0, p1, v0}, Lhra;->a(Landroid/content/SyncStats;I)V

    .line 534
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 537
    :cond_8
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->o()Ljava/lang/String;

    move-result-object v6

    .line 538
    if-nez v6, :cond_a

    .line 539
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    invoke-static {v0}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhrx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 540
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 543
    if-nez v6, :cond_a

    .line 544
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 545
    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2c

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "+++ QUEUE task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; fingerprint not available; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x240c8400

    invoke-direct {p0, v0, v1, v2, v3}, Lhra;->a(JJ)J

    .line 549
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhra;->a(Landroid/content/SyncStats;Ljava/lang/Throwable;)V

    .line 550
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 554
    :cond_a
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v1

    .line 555
    iget-object v0, p0, Lhra;->m:Lhei;

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 556
    const-string v2, "is_plus_page"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v2

    .line 557
    const-string v4, "gaia_id"

    invoke-interface {v0, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 559
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 560
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/16 v5, 0xa

    if-ne v0, v5, :cond_c

    const/4 v0, 0x1

    .line 561
    :goto_1
    if-nez v2, :cond_d

    if-nez v0, :cond_d

    iget-object v0, p0, Lhra;->o:Lhqz;

    iget-object v2, p0, Lhra;->d:Lhrj;

    .line 563
    invoke-virtual {v2}, Lhrj;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v0, v2, v4, v6}, Lhqz;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 564
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 565
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "+++ SKIP record; duplicate upload; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    :cond_b
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0xb

    .line 568
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0x22

    .line 569
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 570
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 560
    :cond_c
    const/4 v0, 0x0

    goto :goto_1

    .line 574
    :cond_d
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 575
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 576
    new-instance v2, Ljvu;

    iget-object v5, p0, Lhra;->g:Landroid/content/Context;

    invoke-direct {v2, v5, v1, v4, v0}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    .line 578
    invoke-virtual {v2}, Ljvu;->l()V

    .line 579
    invoke-virtual {v2, v6}, Ljvu;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 580
    invoke-virtual {v2, v6}, Ljvu;->c(Ljava/lang/String;)J

    move-result-wide v2

    .line 581
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 582
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x23

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "+++ SKIP record; duplicate upload; "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    :cond_e
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    invoke-static {v0}, Lhqz;->a(Landroid/content/Context;)Lhqz;

    move-result-object v1

    const/4 v5, 0x0

    .line 585
    invoke-virtual/range {v1 .. v6}, Lhqz;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0xb

    .line 587
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0x22

    .line 588
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 589
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 593
    :cond_f
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    .line 594
    :goto_2
    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->f()Ljava/lang/String;

    move-result-object v1

    .line 595
    if-nez v1, :cond_10

    if-nez v0, :cond_10

    .line 596
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-string v2, "instant"

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 600
    :cond_10
    const/16 v0, 0x14

    if-ne v3, v0, :cond_11

    if-nez v1, :cond_11

    .line 601
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-string v1, "instant"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 605
    :cond_11
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 606
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 607
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 593
    :cond_12
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(Z)Z
    .locals 7

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 857
    iget-object v2, p0, Lhra;->l:Lhrm;

    monitor-enter v2

    .line 858
    :try_start_0
    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 896
    :pswitch_0
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v0

    if-eq v0, v4, :cond_5

    .line 898
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 899
    const-string v0, "iu.SyncTask"

    const-string v1, "--- STOP wrong state after upload; task: "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "--- STOP wrong state;  task: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 904
    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 911
    :goto_0
    const/4 v1, 0x5

    invoke-direct {p0, v1, v0}, Lhra;->a(ILjava/lang/Throwable;)V

    .line 914
    :goto_1
    const/4 v0, 0x0

    monitor-exit v2

    :goto_2
    return v0

    .line 861
    :pswitch_1
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 862
    iget-object v1, p0, Lhra;->k:Ljava/lang/String;

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x19

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "--- QUEUE stalled "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " task: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 865
    :cond_1
    const/4 v1, 0x3

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lhra;->a(ILjava/lang/Throwable;)V

    .line 866
    invoke-direct {p0, p1}, Lhra;->b(Z)V

    .line 867
    monitor-exit v2

    goto :goto_2

    .line 915
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 870
    :pswitch_2
    :try_start_1
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 871
    iget-object v1, p0, Lhra;->k:Ljava/lang/String;

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1e

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "--- QUEUE unauthorized "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " task: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 874
    :cond_2
    const/4 v1, 0x3

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lhra;->a(ILjava/lang/Throwable;)V

    .line 875
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lhra;->a(I)V

    .line 876
    monitor-exit v2

    goto/16 :goto_2

    .line 879
    :pswitch_3
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 880
    iget-object v1, p0, Lhra;->k:Ljava/lang/String;

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x20

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "--- QUEUE quota exceeded "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " task: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 883
    :cond_3
    const/4 v1, 0x3

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lhra;->a(ILjava/lang/Throwable;)V

    .line 884
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lhra;->a(I)V

    .line 885
    monitor-exit v2

    goto/16 :goto_2

    .line 888
    :pswitch_4
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 889
    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1a

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "--- STOP cancelled "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " task: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 892
    :cond_4
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lhra;->a(ILjava/lang/Throwable;)V

    goto/16 :goto_1

    .line 906
    :cond_5
    const-string v0, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 907
    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x17

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "--- STOP failed "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " task: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    .line 858
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 442
    iget-object v1, p0, Lhra;->l:Lhrm;

    monitor-enter v1

    .line 443
    :try_start_0
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 444
    const-string v0, "iu.SyncTask"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "stopCurrentTask: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhra;->a(ILjava/lang/Throwable;)V

    .line 448
    iget-object v0, p0, Lhra;->l:Lhrm;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 450
    iget-object v0, p0, Lhra;->c:Lhpj;

    if-eqz v0, :cond_1

    .line 451
    iget-object v0, p0, Lhra;->c:Lhpj;

    invoke-virtual {v0}, Lhpj;->a()V

    .line 454
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(IJLjava/lang/Throwable;)V
    .locals 12

    .prologue
    const/16 v3, 0x28

    const/16 v4, 0x190

    const/4 v2, 0x0

    const/16 v0, 0x64

    const/16 v1, 0x12c

    .line 1059
    packed-switch p1, :pswitch_data_0

    .line 1167
    :goto_0
    :pswitch_0
    return-void

    .line 1062
    :pswitch_1
    const/4 v2, 0x1

    move v10, v2

    move v2, v0

    move v0, v10

    .line 1148
    :goto_1
    iget-object v3, p0, Lhra;->g:Landroid/content/Context;

    .line 1149
    iget-object v5, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v5

    .line 1152
    if-ne v2, v1, :cond_5

    .line 1153
    iget-object v1, p0, Lhra;->n:Lhpg;

    invoke-interface {v1, v3, v5}, Lhpg;->a(Landroid/content/Context;I)V

    .line 1158
    :cond_0
    :goto_2
    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    .line 1159
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    .line 1160
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->w()Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 1163
    iget-object v0, p0, Lhra;->d:Lhrj;

    invoke-virtual {v0}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1164
    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    iget-object v2, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v0, v2}, Lifm;->a(Landroid/database/sqlite/SQLiteDatabase;Lifj;)J

    .line 1166
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-direct {p0, v0}, Lhra;->b(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V

    goto :goto_0

    .line 1070
    :pswitch_2
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->f(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move v0, v2

    move v2, v4

    .line 1071
    goto :goto_1

    .line 1075
    :pswitch_3
    const/16 v0, 0x22

    move v2, v4

    .line 1076
    goto :goto_1

    .line 1079
    :pswitch_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1080
    invoke-direct {p0, v6, v7, p2, p3}, Lhra;->a(JJ)J

    move-result-wide v8

    cmp-long v0, v8, v6

    if-gez v0, :cond_1

    move v0, v3

    move v2, v1

    .line 1082
    goto :goto_1

    .line 1084
    :cond_1
    const/16 v0, 0xc8

    move v10, v2

    move v2, v0

    move v0, v10

    .line 1087
    goto :goto_1

    .line 1091
    :pswitch_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1092
    invoke-direct {p0, v6, v7, p2, p3}, Lhra;->a(JJ)J

    move-result-wide v8

    cmp-long v2, v8, v6

    if-gez v2, :cond_2

    move v0, v3

    move v2, v1

    .line 1094
    goto :goto_1

    .line 1097
    :cond_2
    const/4 v2, 0x2

    move v10, v2

    move v2, v0

    move v0, v10

    .line 1099
    goto :goto_1

    .line 1103
    :pswitch_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1104
    invoke-direct {p0, v2, v3, p2, p3}, Lhra;->a(JJ)J

    move-result-wide v6

    cmp-long v2, v6, v2

    if-gez v2, :cond_3

    move v0, v1

    .line 1109
    :cond_3
    const/16 v2, 0x1f

    move v10, v2

    move v2, v0

    move v0, v10

    .line 1110
    goto :goto_1

    .line 1114
    :pswitch_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1115
    invoke-direct {p0, v2, v3, p2, p3}, Lhra;->a(JJ)J

    move-result-wide v6

    cmp-long v2, v6, v2

    if-gez v2, :cond_4

    move v0, v1

    .line 1120
    :cond_4
    const/16 v2, 0x1e

    move v10, v2

    move v2, v0

    move v0, v10

    .line 1121
    goto/16 :goto_1

    :pswitch_8
    move v0, v2

    move v2, v1

    .line 1128
    goto/16 :goto_1

    .line 1133
    :pswitch_9
    const/16 v0, 0x27

    move v2, v1

    .line 1134
    goto/16 :goto_1

    .line 1139
    :pswitch_a
    const/16 v0, 0x26

    move v2, v1

    .line 1140
    goto/16 :goto_1

    .line 1154
    :cond_5
    if-ne v2, v4, :cond_0

    .line 1155
    iget-object v1, p0, Lhra;->n:Lhpg;

    invoke-interface {v1, v3, v5}, Lhpg;->b(Landroid/content/Context;I)V

    goto/16 :goto_2

    .line 1059
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_8
        :pswitch_5
        :pswitch_0
        :pswitch_9
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_3
    .end packed-switch
.end method

.method private b(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V
    .locals 3

    .prologue
    .line 147
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    :goto_0
    return-void

    .line 153
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.libraries.social.autobackup.upload_progress"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 154
    const-string v1, "upload_account_id"

    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 155
    iget-object v1, p0, Lhra;->g:Landroid/content/Context;

    invoke-static {v1}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldr;->a(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 947
    if-eqz p1, :cond_1

    iget-object v0, p0, Lhra;->a:Ljgn;

    .line 948
    invoke-interface {v0}, Ljgn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    .line 952
    :goto_0
    invoke-virtual {p0, v0}, Lhra;->a(I)V

    .line 953
    return-void

    .line 948
    :cond_0
    const/16 v0, 0xe

    goto :goto_0

    :cond_1
    const/16 v0, 0xd

    goto :goto_0
.end method

.method private c(I)V
    .locals 5

    .prologue
    .line 956
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 957
    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    .line 958
    invoke-static {p1}, Lhqv;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "REJECT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " due to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 957
    :cond_0
    invoke-virtual {p0, p1}, Lhra;->a(I)V

    .line 961
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/16 v1, 0x28

    if-ne v0, v1, :cond_1

    .line 962
    iget-object v0, p0, Lhra;->l:Lhrm;

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Lhrm;->b(I)V

    .line 964
    :cond_1
    return-void
.end method

.method private static c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z
    .locals 4

    .prologue
    .line 712
    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->q()J

    move-result-wide v0

    .line 713
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->p()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 919
    iget-object v1, p0, Lhra;->l:Lhrm;

    monitor-enter v1

    .line 920
    :try_start_0
    iget-boolean v0, p0, Lhra;->b:Z

    if-nez v0, :cond_0

    .line 921
    monitor-exit v1

    .line 933
    :goto_0
    return-void

    .line 925
    :cond_0
    iget-object v0, p0, Lhra;->l:Lhrm;

    invoke-virtual {v0}, Lhrm;->b()V

    .line 926
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    .line 927
    const/16 v2, 0x28

    if-ne v0, v2, :cond_1

    .line 928
    iget-object v0, p0, Lhra;->l:Lhrm;

    iget-object v2, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v2

    invoke-virtual {v0, v2}, Lhrm;->b(I)V

    .line 930
    :cond_1
    const-string v0, "iu.SyncTask"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 931
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "   task done: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 933
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 987
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 988
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->p()J

    move-result-wide v4

    iget-object v0, p0, Lhra;->e:Lhrd;

    invoke-interface {v0}, Lhrd;->k()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    .line 989
    :goto_0
    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 990
    invoke-virtual {v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v3

    const/16 v4, 0x14

    if-ne v3, v4, :cond_3

    move v3, v2

    .line 996
    :goto_1
    if-nez v0, :cond_0

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 997
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/16 v4, 0xa

    if-eq v0, v4, :cond_1

    if-nez v3, :cond_0

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 998
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhra;->e:Lhrd;

    invoke-interface {v0}, Lhrd;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 988
    goto :goto_0

    .line 990
    :cond_3
    iget-object v3, p0, Lhra;->e:Lhrd;

    .line 991
    invoke-interface {v3}, Lhrd;->b()Z

    move-result v3

    goto :goto_1
.end method

.method private j()Z
    .locals 2

    .prologue
    .line 1003
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 1004
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhra;->e:Lhrd;

    .line 1005
    invoke-interface {v0}, Lhrd;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 1010
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 1011
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhra;->e:Lhrd;

    .line 1012
    invoke-interface {v0}, Lhrd;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 1016
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lhra;->i:I

    return v0
.end method

.method protected a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 390
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    .line 392
    sparse-switch v0, :sswitch_data_0

    .line 406
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unknown upload reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 394
    :sswitch_0
    const-string v0, "instant_upload_state"

    .line 408
    :goto_0
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 409
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 410
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lhra;->g:Landroid/content/Context;

    invoke-static {v2}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2, v1, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 412
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v0

    const/16 v1, 0x28

    if-ne v0, v1, :cond_0

    .line 413
    iget-object v0, p0, Lhra;->l:Lhrm;

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Lhrm;->b(I)V

    .line 415
    :cond_0
    iget-object v0, p0, Lhra;->l:Lhrm;

    invoke-virtual {v0}, Lhrm;->b()V

    .line 416
    return-void

    .line 397
    :sswitch_1
    const-string v0, "instant_share_state"

    goto :goto_0

    .line 400
    :sswitch_2
    const-string v0, "upload_all_state"

    goto :goto_0

    .line 403
    :sswitch_3
    const-string v0, "manual_upload_state"

    goto :goto_0

    .line 392
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_3
        0x14 -> :sswitch_1
        0x1e -> :sswitch_0
        0x28 -> :sswitch_2
    .end sparse-switch
.end method

.method protected final a(Landroid/content/SyncResult;)V
    .locals 12

    .prologue
    .line 308
    iget-object v1, p0, Lhra;->l:Lhrm;

    monitor-enter v1

    .line 309
    :try_start_0
    iget-boolean v0, p0, Lhra;->b:Z

    if-nez v0, :cond_0

    .line 310
    monitor-exit v1

    .line 349
    :goto_0
    return-void

    .line 312
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljdk;->a(Ljava/lang/String;)I

    move-result v7

    .line 316
    :try_start_1
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    iget v1, p0, Lhra;->i:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "--- START syncing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; account: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 320
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhra;->a(I)V

    .line 322
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 323
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "+++ START; upload started; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    :cond_2
    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    invoke-direct {p0, v2}, Lhra;->a(Landroid/content/SyncStats;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    const-string v1, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhra;->k:Ljava/lang/String;

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x12

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "+++ START "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", task: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v1, p0, Lhra;->e:Lhrd;

    invoke-interface {v1}, Lhrd;->a()V

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v1

    const/16 v3, 0xa

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Lhpu;->e(I)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_4
    const/4 v0, 0x1

    move v1, v0

    :goto_1
    if-eqz v1, :cond_d

    iget-object v0, p0, Lhra;->e:Lhrd;

    invoke-interface {v0}, Lhrd;->j()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lhra;->e:Lhrd;

    invoke-interface {v0}, Lhrd;->l()Ljdn;

    move-result-object v0

    invoke-virtual {v0}, Ljdn;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_2
    if-nez v1, :cond_5

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->n()Z

    move-result v1

    if-eqz v1, :cond_e

    :cond_5
    iget-object v1, p0, Lhra;->e:Lhrd;

    invoke-interface {v1}, Lhrd;->j()Z

    move-result v1

    if-eqz v1, :cond_e

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_3
    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->z()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    :cond_6
    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->x()Z

    move-result v1

    if-nez v1, :cond_f

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_7
    :goto_4
    :try_start_3
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v0

    if-nez v0, :cond_24

    iget-wide v0, v2, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_23

    const/4 v0, 0x1

    :goto_5
    invoke-direct {p0, v0}, Lhra;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x240c8400

    invoke-direct {p0, v0, v1, v2, v3}, Lhra;->a(JJ)J

    .line 327
    :cond_8
    :goto_6
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 328
    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    iget v1, p0, Lhra;->i:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "--- DONE syncing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; account: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 339
    :cond_9
    iget-object v0, p0, Lhra;->d:Lhrj;

    invoke-virtual {v0}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 340
    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    iget-object v2, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v0, v2}, Lifm;->a(Landroid/database/sqlite/SQLiteDatabase;Lifj;)J

    .line 342
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_a

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 343
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_b

    .line 344
    :cond_a
    invoke-direct {p0}, Lhra;->h()V

    .line 348
    :cond_b
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    const-string v1, "iu.upload"

    invoke-static {v0, v7, v1}, Ljdk;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 312
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 325
    :cond_c
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_1

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_f
    const/4 v1, 0x1

    const/4 v3, 0x0

    :try_start_5
    invoke-direct {p0, v1, v3}, Lhra;->a(ILjava/lang/Throwable;)V

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    invoke-virtual {p0}, Lhra;->d()Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    new-instance v3, Lhpj;

    iget-object v6, p0, Lhra;->g:Landroid/content/Context;

    iget v8, p0, Lhra;->i:I

    invoke-direct {v3, v6, v8, p0}, Lhpj;-><init>(Landroid/content/Context;ILhpl;)V

    iget-object v6, p0, Lhra;->d:Lhrj;

    iget v8, p0, Lhra;->i:I

    invoke-static {v6, v8}, Lhqn;->c(Lhrj;I)I
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result v6

    add-int/lit8 v6, v6, -0x1

    :try_start_7
    invoke-direct {p0, v3}, Lhra;->a(Lhpj;)V

    invoke-virtual {v3, v1, v0, v6}, Lhpj;->a(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;ZI)V
    :try_end_7
    .catch Ljcy; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljcv; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljcz; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljdc; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljde; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljcx; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljdf; {:try_start_7 .. :try_end_7} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    const/4 v0, 0x0

    :try_start_8
    invoke-direct {p0, v0}, Lhra;->a(Lhpj;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto/16 :goto_4

    .line 330
    :catch_0
    move-exception v0

    .line 331
    :try_start_9
    const-string v1, "iu.SyncTask"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 332
    iget-object v1, p0, Lhra;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x18

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "+++ SKIP task "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "; "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    :cond_10
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 336
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numSkippedEntries:J

    const/16 v1, 0xb

    invoke-direct {p0, v1, v0}, Lhra;->a(ILjava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 339
    iget-object v0, p0, Lhra;->d:Lhrj;

    invoke-virtual {v0}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 340
    sget-object v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    iget-object v2, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1, v0, v2}, Lifm;->a(Landroid/database/sqlite/SQLiteDatabase;Lifj;)J

    .line 342
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_11

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 343
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_12

    .line 344
    :cond_11
    invoke-direct {p0}, Lhra;->h()V

    .line 348
    :cond_12
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    const-string v1, "iu.upload"

    invoke-static {v0, v7, v1}, Ljdk;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 325
    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v0
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 339
    :catchall_2
    move-exception v0

    iget-object v1, p0, Lhra;->d:Lhrj;

    invoke-virtual {v1}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 340
    sget-object v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v2, v1, v3}, Lifm;->a(Landroid/database/sqlite/SQLiteDatabase;Lifj;)J

    .line 342
    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_13

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 343
    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v1

    const/16 v2, 0xb

    if-ne v1, v2, :cond_14

    .line 344
    :cond_13
    invoke-direct {p0}, Lhra;->h()V

    .line 348
    :cond_14
    iget-object v1, p0, Lhra;->g:Landroid/content/Context;

    const-string v2, "iu.upload"

    invoke-static {v1, v7, v2}, Ljdk;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 349
    throw v0

    .line 325
    :catch_1
    move-exception v0

    :try_start_c
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x5

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_15

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1b

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "PAUSE task; media changed: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_15
    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(Ljava/lang/String;)V

    const/4 v1, 0x6

    invoke-direct {p0, v1, v0}, Lhra;->a(ILjava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    const/4 v0, 0x0

    :try_start_d
    invoke-direct {p0, v0}, Lhra;->a(Lhpj;)V

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto/16 :goto_4

    :catch_2
    move-exception v0

    :try_start_e
    invoke-virtual {v0}, Ljcv;->a()Z

    move-result v1

    if-eqz v1, :cond_17

    const-string v1, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-virtual {v0}, Ljcv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x26

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v6, v8

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "+++ SKIP record; invalid MIME type: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_16
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->w()Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    :goto_7
    const/4 v0, 0x0

    :try_start_f
    invoke-direct {p0, v0}, Lhra;->a(Lhpj;)V

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    goto/16 :goto_4

    :cond_17
    :try_start_10
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_18

    iget-object v1, p0, Lhra;->k:Ljava/lang/String;

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x28

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "+++ QUEUE task "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "; placeholder MIME type; "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/32 v10, 0xea60

    invoke-direct {p0, v8, v9, v10, v11}, Lhra;->a(JJ)J

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    invoke-direct {p0, v1, v0}, Lhra;->a(Landroid/content/SyncStats;Ljava/lang/Throwable;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    goto :goto_7

    :catchall_3
    move-exception v0

    const/4 v1, 0x0

    :try_start_11
    invoke-direct {p0, v1}, Lhra;->a(Lhpj;)V

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v1

    if-eqz v1, :cond_19

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    :cond_19
    throw v0
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    :catch_3
    move-exception v0

    :try_start_12
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x5

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1f

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "PAUSE task; media unavailable: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1a
    const-wide/16 v8, 0x5

    iput-wide v8, p1, Landroid/content/SyncResult;->delayUntil:J

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const/4 v1, 0x6

    const-wide/32 v8, 0x493e0

    invoke-direct {p0, v1, v8, v9, v0}, Lhra;->a(IJLjava/lang/Throwable;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    const/4 v0, 0x0

    :try_start_13
    invoke-direct {p0, v0}, Lhra;->a(Lhpj;)V

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_0
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    goto/16 :goto_4

    :catch_4
    move-exception v0

    :try_start_14
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x5

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1d

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "PAUSE task; transient error: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1b
    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Ljdc;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljdc;->b()Z

    move-result v1

    if-nez v1, :cond_1c

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v1, Landroid/content/SyncStats;->numIoExceptions:J

    :goto_8
    const/4 v1, 0x6

    invoke-direct {p0, v1, v0}, Lhra;->a(ILjava/lang/Throwable;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    const/4 v0, 0x0

    :try_start_15
    invoke-direct {p0, v0}, Lhra;->a(Lhpj;)V

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_0
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    goto/16 :goto_4

    :cond_1c
    const-wide/16 v8, 0x5460

    :try_start_16
    iput-wide v8, p1, Landroid/content/SyncResult;->delayUntil:J

    goto :goto_8

    :catch_5
    move-exception v0

    const-string v1, "iu.SyncTask"

    const/4 v3, 0x5

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1d

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1a

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "PAUSE task; unauthorized: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1d
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const/16 v1, 0x9

    invoke-direct {p0, v1, v0}, Lhra;->a(ILjava/lang/Throwable;)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    const/4 v0, 0x0

    :try_start_17
    invoke-direct {p0, v0}, Lhra;->a(Lhpj;)V

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_0
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    goto/16 :goto_4

    :catch_6
    move-exception v0

    :try_start_18
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->e()Z

    move-result v1

    if-eqz v1, :cond_1f

    const-string v1, "iu.SyncTask"

    const/4 v3, 0x5

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1e

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "FAIL task: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1e
    const/4 v1, 0x5

    invoke-direct {p0, v1, v0}, Lhra;->a(ILjava/lang/Throwable;)V

    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v0, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v0, Landroid/content/SyncStats;->numSkippedEntries:J
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    :goto_9
    const/4 v0, 0x0

    :try_start_19
    invoke-direct {p0, v0}, Lhra;->a(Lhpj;)V

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_19} :catch_0
    .catchall {:try_start_19 .. :try_end_19} :catchall_2

    goto/16 :goto_4

    :cond_1f
    :try_start_1a
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x5

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_20

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1d

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "PAUSE task; media unmounted: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_20
    const/4 v1, 0x6

    invoke-direct {p0, v1, v0}, Lhra;->a(ILjava/lang/Throwable;)V

    goto :goto_9

    :catch_7
    move-exception v0

    const-string v1, "iu.SyncTask"

    const/4 v3, 0x6

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_21

    const-string v1, "iu.SyncTask"

    const-string v3, "FAIL task: permanent failure: "

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_21
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v1, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v1, Landroid/content/SyncStats;->numSkippedEntries:J

    const/4 v1, 0x5

    invoke-direct {p0, v1, v0}, Lhra;->a(ILjava/lang/Throwable;)V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_3

    const/4 v0, 0x0

    :try_start_1b
    invoke-direct {p0, v0}, Lhra;->a(Lhpj;)V

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_0
    .catchall {:try_start_1b .. :try_end_1b} :catchall_2

    goto/16 :goto_4

    :catch_8
    move-exception v0

    :try_start_1c
    const-string v1, "iu.SyncTask"

    const/4 v3, 0x6

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_22

    const-string v1, "iu.SyncTask"

    const-string v3, "FAIL task: permanent failure: "

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_22
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v1, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v1, Landroid/content/SyncStats;->numSkippedEntries:J

    const/4 v1, 0x5

    invoke-direct {p0, v1, v0}, Lhra;->a(ILjava/lang/Throwable;)V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_3

    const/4 v0, 0x0

    :try_start_1d
    invoke-direct {p0, v0}, Lhra;->a(Lhpj;)V

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Lhra;->c(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    goto/16 :goto_4

    :cond_23
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_24
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lhra;->a(ILjava/lang/Throwable;)V

    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numInserts:J

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->m()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_25

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->r()J

    move-result-wide v2

    iget-object v1, p0, Lhra;->m:Lhei;

    invoke-interface {v1, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->s()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->o()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    invoke-static {v0}, Lhqz;->a(Landroid/content/Context;)Lhqz;

    move-result-object v1

    invoke-virtual/range {v1 .. v6}, Lhqz;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_25
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_26

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    :cond_26
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->u()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_27

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->f(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    :cond_27
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "+++ DONE; upload finished; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1d
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_1d} :catch_0
    .catchall {:try_start_1d .. :try_end_1d} :catchall_2

    goto/16 :goto_6
.end method

.method public a(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V
    .locals 5

    .prologue
    .line 127
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    if-ne p1, v0, :cond_3

    .line 128
    iget-object v1, p0, Lhra;->l:Lhrm;

    monitor-enter v1

    .line 129
    :try_start_0
    iget-boolean v0, p0, Lhra;->b:Z

    if-eqz v0, :cond_2

    .line 130
    const-string v0, "iu.SyncTask"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "  progress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    :cond_0
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->x()Z

    move-result v0

    if-nez v0, :cond_1

    .line 134
    monitor-exit v1

    .line 144
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 137
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->k()I

    move-result v0

    const-wide/32 v2, 0x240c8400

    const/4 v4, 0x0

    .line 136
    invoke-direct {p0, v0, v2, v3, v4}, Lhra;->b(IJLjava/lang/Throwable;)V

    .line 138
    iget-object v0, p0, Lhra;->l:Lhrm;

    invoke-virtual {v0}, Lhrm;->b()V

    .line 140
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :cond_3
    invoke-direct {p0, p1}, Lhra;->b(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;)V

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected a(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 423
    iget-object v3, p0, Lhra;->l:Lhrm;

    monitor-enter v3

    .line 425
    :try_start_0
    iget-object v2, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    iget-wide v4, v2, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->id:J

    cmp-long v2, p1, v4

    if-nez v2, :cond_1

    move v2, v0

    .line 426
    :goto_0
    iget-boolean v4, p0, Lhra;->b:Z

    if-eqz v4, :cond_2

    if-nez v2, :cond_2

    :goto_1
    iput-boolean v0, p0, Lhra;->b:Z

    .line 427
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    if-eqz v2, :cond_0

    .line 429
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lhra;->b(I)V

    .line 431
    :cond_0
    return v2

    :cond_1
    move v2, v1

    .line 425
    goto :goto_0

    :cond_2
    move v0, v1

    .line 426
    goto :goto_1

    .line 427
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lhra;->j:I

    shr-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lhra;->j:I

    return v0
.end method

.method d()Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    return-object v0
.end method

.method protected e()Z
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v5, 0x3

    .line 210
    new-instance v0, Landroid/accounts/Account;

    .line 211
    iget-object v2, p0, Lhra;->m:Lhei;

    iget v3, p0, Lhra;->i:I

    invoke-interface {v2, v3}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google"

    invoke-direct {v0, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lhra;->g:Landroid/content/Context;

    .line 212
    invoke-static {v2}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 210
    invoke-static {v0, v2}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v2

    .line 213
    iget-object v0, p0, Lhra;->g:Landroid/content/Context;

    const-class v3, Ljgn;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    .line 216
    const-string v3, "iu.SyncTask"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 217
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "-- isAccepted state -- isBackgroundSync: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 219
    invoke-direct {p0}, Lhra;->l()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " getMasterSync: "

    .line 220
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 221
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " doAutoSync: "

    .line 222
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 223
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isConnected: "

    .line 224
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 225
    invoke-interface {v0}, Ljgn;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " backgroundDataAllowed: "

    .line 226
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lhra;->f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 227
    invoke-virtual {v4}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->d()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isPlugged: "

    .line 228
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lhra;->f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 229
    invoke-virtual {v4}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isSyncOnBattery: "

    .line 230
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 231
    invoke-direct {p0}, Lhra;->k()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isNetworkMetered: "

    .line 232
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lhra;->f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 233
    invoke-virtual {v4}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->b()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isMobileNetwork: "

    .line 234
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 235
    invoke-interface {v0}, Ljgn;->i()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isSyncOnWifiOnly "

    .line 236
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 237
    invoke-direct {p0}, Lhra;->i()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isRoaming "

    .line 238
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lhra;->f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 239
    invoke-virtual {v4}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->c()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isSyncOnRoaming "

    .line 240
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 241
    invoke-direct {p0}, Lhra;->j()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 242
    :cond_0
    invoke-direct {p0}, Lhra;->l()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 248
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v3

    if-nez v3, :cond_2

    .line 249
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "reject "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " because master auto sync is off"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :cond_1
    invoke-direct {p0, v7}, Lhra;->c(I)V

    move v0, v1

    .line 302
    :goto_0
    return v0

    .line 256
    :cond_2
    if-nez v2, :cond_4

    .line 257
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 258
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "reject "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " because auto sync is off"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_3
    invoke-direct {p0, v7}, Lhra;->c(I)V

    move v0, v1

    .line 261
    goto :goto_0

    .line 265
    :cond_4
    invoke-interface {v0}, Ljgn;->a()Z

    move-result v2

    if-nez v2, :cond_6

    .line 266
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 267
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "reject "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " on no network"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :cond_5
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lhra;->c(I)V

    move v0, v1

    .line 270
    goto :goto_0

    .line 272
    :cond_6
    iget-object v2, p0, Lhra;->f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->d()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-direct {p0}, Lhra;->l()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 273
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 274
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "reject "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for disabled background data"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_7
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lhra;->c(I)V

    move v0, v1

    .line 277
    goto/16 :goto_0

    .line 279
    :cond_8
    iget-object v2, p0, Lhra;->f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a()Z

    move-result v2

    if-nez v2, :cond_a

    invoke-direct {p0}, Lhra;->k()Z

    move-result v2

    if-nez v2, :cond_a

    .line 280
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 281
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "reject "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " on battery"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :cond_9
    invoke-direct {p0, v6}, Lhra;->c(I)V

    move v0, v1

    .line 284
    goto/16 :goto_0

    .line 286
    :cond_a
    iget-object v2, p0, Lhra;->f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->b()Z

    move-result v2

    if-nez v2, :cond_b

    invoke-interface {v0}, Ljgn;->i()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 287
    :cond_b
    invoke-direct {p0}, Lhra;->i()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 288
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 289
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "reject "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for non-wifi connection"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :cond_c
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhra;->c(I)V

    move v0, v1

    .line 292
    goto/16 :goto_0

    .line 293
    :cond_d
    iget-object v0, p0, Lhra;->f:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->c()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-direct {p0}, Lhra;->j()Z

    move-result v0

    if-nez v0, :cond_f

    .line 294
    const-string v0, "iu.SyncTask"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 295
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "reject "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for roaming"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    :cond_e
    invoke-direct {p0, v5}, Lhra;->c(I)V

    move v0, v1

    .line 298
    goto/16 :goto_0

    .line 302
    :cond_f
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method protected f()V
    .locals 5

    .prologue
    .line 362
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "--- CANCEL sync "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; task: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    :cond_0
    iget-object v1, p0, Lhra;->l:Lhrm;

    monitor-enter v1

    .line 366
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lhra;->b:Z

    .line 367
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lhra;->b(I)V

    .line 369
    return-void

    .line 367
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected g()V
    .locals 5

    .prologue
    .line 376
    const-string v0, "iu.SyncTask"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lhra;->k:Ljava/lang/String;

    iget-object v1, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "--- STOP sync "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; task: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    :cond_0
    iget-object v1, p0, Lhra;->l:Lhrm;

    monitor-enter v1

    .line 380
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lhra;->b:Z

    .line 381
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lhra;->b(I)V

    .line 383
    return-void

    .line 381
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 161
    const-string v0, "[%s; reason: %s, id: %d, accountId: %d]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 162
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lhra;->k:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lhra;->h:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    iget-wide v4, v3, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lhra;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 161
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
