.class public final Lmkv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lovx;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21058
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21061
    iput-object v0, p0, Lmkv;->apiHeader:Llyr;

    .line 21064
    iput-object v0, p0, Lmkv;->a:Lovx;

    .line 21058
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 21081
    const/4 v0, 0x0

    .line 21082
    iget-object v1, p0, Lmkv;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 21083
    const/4 v0, 0x1

    iget-object v1, p0, Lmkv;->apiHeader:Llyr;

    .line 21084
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21086
    :cond_0
    iget-object v1, p0, Lmkv;->a:Lovx;

    if-eqz v1, :cond_1

    .line 21087
    const/4 v1, 0x2

    iget-object v2, p0, Lmkv;->a:Lovx;

    .line 21088
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21090
    :cond_1
    iget-object v1, p0, Lmkv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21091
    iput v0, p0, Lmkv;->ai:I

    .line 21092
    return v0
.end method

.method public a(Loxn;)Lmkv;
    .locals 2

    .prologue
    .line 21100
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 21101
    sparse-switch v0, :sswitch_data_0

    .line 21105
    iget-object v1, p0, Lmkv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 21106
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkv;->ah:Ljava/util/List;

    .line 21109
    :cond_1
    iget-object v1, p0, Lmkv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 21111
    :sswitch_0
    return-object p0

    .line 21116
    :sswitch_1
    iget-object v0, p0, Lmkv;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 21117
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkv;->apiHeader:Llyr;

    .line 21119
    :cond_2
    iget-object v0, p0, Lmkv;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21123
    :sswitch_2
    iget-object v0, p0, Lmkv;->a:Lovx;

    if-nez v0, :cond_3

    .line 21124
    new-instance v0, Lovx;

    invoke-direct {v0}, Lovx;-><init>()V

    iput-object v0, p0, Lmkv;->a:Lovx;

    .line 21126
    :cond_3
    iget-object v0, p0, Lmkv;->a:Lovx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21101
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 21069
    iget-object v0, p0, Lmkv;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 21070
    const/4 v0, 0x1

    iget-object v1, p0, Lmkv;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21072
    :cond_0
    iget-object v0, p0, Lmkv;->a:Lovx;

    if-eqz v0, :cond_1

    .line 21073
    const/4 v0, 0x2

    iget-object v1, p0, Lmkv;->a:Lovx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21075
    :cond_1
    iget-object v0, p0, Lmkv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 21077
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 21054
    invoke-virtual {p0, p1}, Lmkv;->a(Loxn;)Lmkv;

    move-result-object v0

    return-object v0
.end method
