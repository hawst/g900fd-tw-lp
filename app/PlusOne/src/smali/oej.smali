.class public final Loej;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:Loeg;

.field public e:I

.field private f:Loef;

.field private g:Loeh;

.field private h:Loem;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Loxq;-><init>()V

    .line 40
    iput v1, p0, Loej;->b:I

    .line 43
    iput v1, p0, Loej;->c:I

    .line 46
    iput-object v0, p0, Loej;->f:Loef;

    .line 49
    iput-object v0, p0, Loej;->g:Loeh;

    .line 52
    iput-object v0, p0, Loej;->h:Loem;

    .line 55
    iput-object v0, p0, Loej;->d:Loeg;

    .line 58
    iput v1, p0, Loej;->e:I

    .line 16
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 93
    const/4 v0, 0x0

    .line 94
    iget-object v1, p0, Loej;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 95
    const/4 v0, 0x1

    iget-object v1, p0, Loej;->a:Ljava/lang/String;

    .line 96
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 98
    :cond_0
    iget v1, p0, Loej;->b:I

    if-eq v1, v3, :cond_1

    .line 99
    const/4 v1, 0x2

    iget v2, p0, Loej;->b:I

    .line 100
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_1
    iget v1, p0, Loej;->c:I

    if-eq v1, v3, :cond_2

    .line 103
    const/4 v1, 0x3

    iget v2, p0, Loej;->c:I

    .line 104
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_2
    iget-object v1, p0, Loej;->f:Loef;

    if-eqz v1, :cond_3

    .line 107
    const/4 v1, 0x4

    iget-object v2, p0, Loej;->f:Loef;

    .line 108
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_3
    iget-object v1, p0, Loej;->g:Loeh;

    if-eqz v1, :cond_4

    .line 111
    const/4 v1, 0x5

    iget-object v2, p0, Loej;->g:Loeh;

    .line 112
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    :cond_4
    iget-object v1, p0, Loej;->h:Loem;

    if-eqz v1, :cond_5

    .line 115
    const/4 v1, 0x6

    iget-object v2, p0, Loej;->h:Loem;

    .line 116
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_5
    iget-object v1, p0, Loej;->d:Loeg;

    if-eqz v1, :cond_6

    .line 119
    const/4 v1, 0x7

    iget-object v2, p0, Loej;->d:Loeg;

    .line 120
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    :cond_6
    iget v1, p0, Loej;->e:I

    if-eq v1, v3, :cond_7

    .line 123
    const/16 v1, 0x8

    iget v2, p0, Loej;->e:I

    .line 124
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_7
    iget-object v1, p0, Loej;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    iput v0, p0, Loej;->ai:I

    .line 128
    return v0
.end method

.method public a(Loxn;)Loej;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 136
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 137
    sparse-switch v0, :sswitch_data_0

    .line 141
    iget-object v1, p0, Loej;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 142
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loej;->ah:Ljava/util/List;

    .line 145
    :cond_1
    iget-object v1, p0, Loej;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    :sswitch_0
    return-object p0

    .line 152
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loej;->a:Ljava/lang/String;

    goto :goto_0

    .line 156
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 157
    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    .line 168
    :cond_2
    iput v0, p0, Loej;->b:I

    goto :goto_0

    .line 170
    :cond_3
    iput v2, p0, Loej;->b:I

    goto :goto_0

    .line 175
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 176
    if-eq v0, v2, :cond_4

    if-ne v0, v3, :cond_5

    .line 178
    :cond_4
    iput v0, p0, Loej;->c:I

    goto :goto_0

    .line 180
    :cond_5
    iput v2, p0, Loej;->c:I

    goto :goto_0

    .line 185
    :sswitch_4
    iget-object v0, p0, Loej;->f:Loef;

    if-nez v0, :cond_6

    .line 186
    new-instance v0, Loef;

    invoke-direct {v0}, Loef;-><init>()V

    iput-object v0, p0, Loej;->f:Loef;

    .line 188
    :cond_6
    iget-object v0, p0, Loej;->f:Loef;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 192
    :sswitch_5
    iget-object v0, p0, Loej;->g:Loeh;

    if-nez v0, :cond_7

    .line 193
    new-instance v0, Loeh;

    invoke-direct {v0}, Loeh;-><init>()V

    iput-object v0, p0, Loej;->g:Loeh;

    .line 195
    :cond_7
    iget-object v0, p0, Loej;->g:Loeh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 199
    :sswitch_6
    iget-object v0, p0, Loej;->h:Loem;

    if-nez v0, :cond_8

    .line 200
    new-instance v0, Loem;

    invoke-direct {v0}, Loem;-><init>()V

    iput-object v0, p0, Loej;->h:Loem;

    .line 202
    :cond_8
    iget-object v0, p0, Loej;->h:Loem;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 206
    :sswitch_7
    iget-object v0, p0, Loej;->d:Loeg;

    if-nez v0, :cond_9

    .line 207
    new-instance v0, Loeg;

    invoke-direct {v0}, Loeg;-><init>()V

    iput-object v0, p0, Loej;->d:Loeg;

    .line 209
    :cond_9
    iget-object v0, p0, Loej;->d:Loeg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 213
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 214
    if-eqz v0, :cond_a

    if-eq v0, v2, :cond_a

    if-ne v0, v3, :cond_b

    .line 217
    :cond_a
    iput v0, p0, Loej;->e:I

    goto/16 :goto_0

    .line 219
    :cond_b
    const/4 v0, 0x0

    iput v0, p0, Loej;->e:I

    goto/16 :goto_0

    .line 137
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 63
    iget-object v0, p0, Loej;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x1

    iget-object v1, p0, Loej;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 66
    :cond_0
    iget v0, p0, Loej;->b:I

    if-eq v0, v2, :cond_1

    .line 67
    const/4 v0, 0x2

    iget v1, p0, Loej;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 69
    :cond_1
    iget v0, p0, Loej;->c:I

    if-eq v0, v2, :cond_2

    .line 70
    const/4 v0, 0x3

    iget v1, p0, Loej;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 72
    :cond_2
    iget-object v0, p0, Loej;->f:Loef;

    if-eqz v0, :cond_3

    .line 73
    const/4 v0, 0x4

    iget-object v1, p0, Loej;->f:Loef;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 75
    :cond_3
    iget-object v0, p0, Loej;->g:Loeh;

    if-eqz v0, :cond_4

    .line 76
    const/4 v0, 0x5

    iget-object v1, p0, Loej;->g:Loeh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 78
    :cond_4
    iget-object v0, p0, Loej;->h:Loem;

    if-eqz v0, :cond_5

    .line 79
    const/4 v0, 0x6

    iget-object v1, p0, Loej;->h:Loem;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 81
    :cond_5
    iget-object v0, p0, Loej;->d:Loeg;

    if-eqz v0, :cond_6

    .line 82
    const/4 v0, 0x7

    iget-object v1, p0, Loej;->d:Loeg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 84
    :cond_6
    iget v0, p0, Loej;->e:I

    if-eq v0, v2, :cond_7

    .line 85
    const/16 v0, 0x8

    iget v1, p0, Loej;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 87
    :cond_7
    iget-object v0, p0, Loej;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 89
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Loej;->a(Loxn;)Loej;

    move-result-object v0

    return-object v0
.end method
