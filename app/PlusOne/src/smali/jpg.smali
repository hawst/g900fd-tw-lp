.class public final Ljpg;
.super Llin;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$SelectionBoundsAdjuster;
.implements Lkdd;
.implements Lljh;


# static fields
.field private static ap:Landroid/graphics/drawable/Drawable;

.field private static aq:Landroid/graphics/drawable/Drawable;

.field private static ar:Landroid/graphics/Bitmap;

.field private static as:Landroid/graphics/drawable/Drawable;

.field private static at:Landroid/graphics/drawable/Drawable;

.field private static au:I

.field private static av:I

.field private static f:Lhso;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Ljava/lang/String;

.field private D:Z

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private final G:Landroid/widget/TextView;

.field private final H:Landroid/text/SpannableStringBuilder;

.field private final I:Landroid/text/SpannableStringBuilder;

.field private final J:Landroid/widget/TextView;

.field private final K:Landroid/graphics/drawable/Drawable;

.field private final L:I

.field private M:I

.field private N:Landroid/graphics/Bitmap;

.field private final O:Landroid/graphics/Paint;

.field private final P:Landroid/graphics/Rect;

.field private final Q:Landroid/graphics/Rect;

.field private R:Landroid/widget/TextView;

.field private S:Landroid/widget/TextView;

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field private final aa:I

.field private ab:Z

.field private ac:Landroid/widget/TextView;

.field private ad:Landroid/widget/ImageView;

.field private final ae:I

.field private af:I

.field private ag:Z

.field private ah:I

.field private ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

.field private aj:Ljph;

.field private ak:Z

.field private al:Landroid/graphics/Bitmap;

.field private am:Landroid/widget/ImageView;

.field private an:Z

.field private ao:Z

.field private g:Lhxh;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:I

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Lkda;

.field private y:I

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x0

    const/4 v4, -0x2

    const/4 v3, 0x1

    .line 153
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Llin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    iput-boolean v3, p0, Ljpg;->w:Z

    .line 76
    iput-boolean v3, p0, Ljpg;->A:Z

    .line 84
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Ljpg;->H:Landroid/text/SpannableStringBuilder;

    .line 85
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Ljpg;->I:Landroid/text/SpannableStringBuilder;

    .line 92
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ljpg;->P:Landroid/graphics/Rect;

    .line 93
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ljpg;->Q:Landroid/graphics/Rect;

    .line 155
    sget-object v0, Ljpg;->f:Lhso;

    if-nez v0, :cond_0

    .line 156
    const-class v0, Lhso;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    sput-object v0, Ljpg;->f:Lhso;

    .line 159
    :cond_0
    invoke-virtual {p0}, Ljpg;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 160
    const v1, 0x7f0d0204

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Ljpg;->i:I

    .line 161
    invoke-static {p1}, Lhss;->c(Landroid/content/Context;)I

    move-result v1

    iget v2, p0, Ljpg;->i:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Ljpg;->h:I

    .line 162
    const v1, 0x7f0d0205

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Ljpg;->j:I

    .line 164
    const v1, 0x7f0205e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Ljpg;->K:Landroid/graphics/drawable/Drawable;

    .line 166
    const v1, 0x7f0d0206

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ljpg;->L:I

    .line 168
    invoke-virtual {p0}, Ljpg;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a7

    invoke-direct {p0, v1, v2}, Ljpg;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Ljpg;->al:Landroid/graphics/Bitmap;

    .line 170
    const v1, 0x7f0d0207

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Ljpg;->k:I

    .line 172
    const v1, 0x7f0d0208

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Ljpg;->l:I

    .line 174
    const v1, 0x7f0d0209

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Ljpg;->m:I

    .line 176
    const v1, 0x7f0205e6

    iput v1, p0, Ljpg;->aa:I

    .line 177
    const v1, 0x7f0d020a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ljpg;->ae:I

    .line 179
    const v1, 0x7f0d020b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ljpg;->p:I

    .line 181
    const v1, 0x7f0d020c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Ljpg;->o:I

    .line 183
    const v1, 0x7f0d020d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Ljpg;->q:I

    .line 185
    const v1, 0x7f0d020e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Ljpg;->r:I

    .line 188
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    .line 189
    iget-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 190
    iget-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 191
    iget-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 192
    iget-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 194
    iget-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    const/16 v1, 0x1b

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 195
    iget-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Ljpg;->addView(Landroid/view/View;)V

    .line 197
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    .line 198
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 199
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 200
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 201
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 203
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    const/16 v1, 0xc

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 204
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Ljpg;->addView(Landroid/view/View;)V

    .line 206
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ljpg;->O:Landroid/graphics/Paint;

    .line 208
    sget-object v0, Ljpg;->ar:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 209
    invoke-static {p1, v3}, Lhss;->c(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Ljpg;->ar:Landroid/graphics/Bitmap;

    .line 211
    :cond_1
    sget-object v0, Ljpg;->ar:Landroid/graphics/Bitmap;

    iput-object v0, p0, Ljpg;->N:Landroid/graphics/Bitmap;

    .line 213
    sget-object v0, Ljpg;->aq:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 214
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ljpg;->aq:Landroid/graphics/drawable/Drawable;

    .line 218
    :cond_2
    sget-object v0, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_3

    .line 219
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0204a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    .line 223
    :cond_3
    sget-object v0, Ljpg;->at:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_4

    .line 224
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0204ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ljpg;->at:Landroid/graphics/drawable/Drawable;

    .line 228
    :cond_4
    sget v0, Ljpg;->au:I

    if-nez v0, :cond_5

    .line 229
    invoke-static {p1}, Lhss;->c(Landroid/content/Context;)I

    move-result v0

    sput v0, Ljpg;->au:I

    .line 230
    invoke-static {p1}, Lhss;->a(Landroid/content/Context;)I

    move-result v0

    sput v0, Ljpg;->av:I

    .line 233
    :cond_5
    sget-object v0, Ljpg;->ar:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget v1, Ljpg;->au:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ljpg;->n:I

    .line 235
    iget v0, p0, Ljpg;->n:I

    sget v1, Ljpg;->au:I

    if-le v0, v1, :cond_6

    .line 236
    iput v3, p0, Ljpg;->y:I

    .line 237
    iget-object v0, p0, Ljpg;->O:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 249
    :goto_0
    return-void

    .line 238
    :cond_6
    iget v0, p0, Ljpg;->n:I

    sget v1, Ljpg;->au:I

    if-ne v0, v1, :cond_7

    .line 239
    iput v3, p0, Ljpg;->y:I

    goto :goto_0

    .line 240
    :cond_7
    iget v0, p0, Ljpg;->n:I

    sget v1, Ljpg;->av:I

    if-le v0, v1, :cond_8

    .line 241
    iput v3, p0, Ljpg;->y:I

    .line 242
    iget-object v0, p0, Ljpg;->O:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    goto :goto_0

    .line 243
    :cond_8
    iget v0, p0, Ljpg;->n:I

    sget v1, Ljpg;->av:I

    if-ne v0, v1, :cond_9

    .line 244
    iput v5, p0, Ljpg;->y:I

    goto :goto_0

    .line 246
    :cond_9
    iput v5, p0, Ljpg;->y:I

    .line 247
    iget-object v0, p0, Ljpg;->O:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    goto :goto_0
.end method

.method private a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 261
    :try_start_0
    invoke-static {p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 264
    :goto_0
    return-object v0

    .line 262
    :catch_0
    move-exception v0

    .line 263
    const-string v1, "PeopleListItemView"

    const-string v2, "ImageUtils#decodeResource(Resources, int) threw an OOME"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 264
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljpg;
    .locals 1

    .prologue
    .line 149
    new-instance v0, Ljpg;

    invoke-direct {v0, p0}, Ljpg;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Ljpg;->S:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljpg;->S:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 329
    iget-object v0, p0, Ljpg;->S:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Ljpg;->addView(Landroid/view/View;)V

    .line 331
    :cond_0
    iget-object v0, p0, Ljpg;->am:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljpg;->am:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 332
    iget-object v0, p0, Ljpg;->am:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Ljpg;->addView(Landroid/view/View;)V

    .line 334
    :cond_1
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Ljpg;->S:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Ljpg;->removeView(Landroid/view/View;)V

    .line 338
    iget-object v0, p0, Ljpg;->am:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Ljpg;->removeView(Landroid/view/View;)V

    .line 339
    return-void
.end method

.method private n()V
    .locals 6

    .prologue
    .line 413
    iget-object v0, p0, Ljpg;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    iget-object v1, p0, Ljpg;->z:Ljava/lang/String;

    iget-object v2, p0, Ljpg;->H:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Ljpg;->F:Ljava/lang/String;

    sget-object v4, Ljpg;->d:Landroid/text/style/StyleSpan;

    sget-object v5, Ljpg;->e:Landroid/text/style/ForegroundColorSpan;

    invoke-static/range {v0 .. v5}, Llhv;->a(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 421
    :goto_0
    return-void

    .line 418
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->B:Z

    .line 419
    iget-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    iget-object v1, p0, Ljpg;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1194
    invoke-virtual {p0}, Ljpg;->c()V

    .line 1195
    invoke-direct {p0}, Ljpg;->m()V

    .line 1196
    iput-object v1, p0, Ljpg;->s:Ljava/lang/String;

    .line 1197
    iput-object v1, p0, Ljpg;->t:Ljava/lang/String;

    .line 1198
    iput-object v1, p0, Ljpg;->v:Ljava/lang/String;

    .line 1199
    iput-boolean v0, p0, Ljpg;->w:Z

    .line 1200
    iput-object v1, p0, Ljpg;->z:Ljava/lang/String;

    .line 1201
    iput-boolean v0, p0, Ljpg;->A:Z

    .line 1203
    iput-boolean v2, p0, Ljpg;->B:Z

    .line 1204
    iput-object v1, p0, Ljpg;->C:Ljava/lang/String;

    .line 1205
    iput-boolean v2, p0, Ljpg;->D:Z

    .line 1206
    iput-object v1, p0, Ljpg;->E:Ljava/lang/String;

    .line 1207
    iput-object v1, p0, Ljpg;->F:Ljava/lang/String;

    .line 1208
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1209
    iput-boolean v2, p0, Ljpg;->ak:Z

    .line 1210
    invoke-virtual {p0, v2}, Ljpg;->b(Z)V

    .line 1211
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 685
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 686
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Ljpg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    .line 687
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    const v1, 0x7f0205e6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 688
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 689
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    iget v1, p0, Ljpg;->o:I

    iget v2, p0, Ljpg;->o:I

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 690
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 691
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Ljpg;->addView(Landroid/view/View;)V

    .line 694
    :cond_0
    invoke-virtual {p0}, Ljpg;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 695
    iget-object v1, p0, Ljpg;->ac:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 696
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1151
    iget-boolean v0, p0, Ljpg;->ag:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ljpg;->ah:I

    .line 1152
    :goto_0
    invoke-virtual {p0}, Ljpg;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Ljpg;->getHeight()I

    move-result v3

    invoke-virtual {p2, v1, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1153
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1154
    return-void

    :cond_0
    move v0, v1

    .line 1151
    goto :goto_0
.end method

.method public a(Lhxh;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Ljpg;->g:Lhxh;

    .line 504
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 346
    iput-object p1, p0, Ljpg;->s:Ljava/lang/String;

    .line 347
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Ljpg;->t:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ljpg;->w:Z

    if-eqz v0, :cond_0

    .line 355
    invoke-virtual {p0}, Ljpg;->c()V

    .line 356
    iput-object p1, p0, Ljpg;->t:Ljava/lang/String;

    .line 357
    iput-object p2, p0, Ljpg;->u:Ljava/lang/String;

    .line 358
    invoke-virtual {p0}, Ljpg;->b()V

    .line 360
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Ljpg;->t:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljpg;->v:Ljava/lang/String;

    .line 368
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Ljpg;->w:Z

    if-eqz v0, :cond_1

    .line 370
    invoke-virtual {p0}, Ljpg;->c()V

    .line 371
    iput-object p1, p0, Ljpg;->t:Ljava/lang/String;

    .line 372
    iput-object p2, p0, Ljpg;->v:Ljava/lang/String;

    .line 373
    iput-object p3, p0, Ljpg;->u:Ljava/lang/String;

    .line 374
    invoke-virtual {p0}, Ljpg;->b()V

    .line 376
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 531
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpg;->V:Z

    .line 532
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 533
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->T:Z

    .line 534
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpg;->U:Z

    .line 535
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-static {p4}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    invoke-virtual {p0}, Ljpg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p5}, Ljoa;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 537
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 538
    iget-object v1, p0, Ljpg;->R:Landroid/widget/TextView;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljpg;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Ljpg;->R:Landroid/widget/TextView;

    iget-object v2, p0, Ljpg;->R:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v2, p0, Ljpg;->R:Landroid/widget/TextView;

    const v3, 0x1030044

    invoke-virtual {v2, v1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v2, p0, Ljpg;->R:Landroid/widget/TextView;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Ljpg;->R:Landroid/widget/TextView;

    const/16 v3, 0xc

    invoke-static {v1, v2, v3}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v1, p0, Ljpg;->R:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Ljpg;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Ljpg;->R:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 539
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->V:Z

    .line 595
    :cond_1
    :goto_0
    iget-object v1, p0, Ljpg;->J:Landroid/widget/TextView;

    iget-boolean v0, p0, Ljpg;->T:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 596
    iget-object v0, p0, Ljpg;->R:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 597
    iget-object v1, p0, Ljpg;->R:Landroid/widget/TextView;

    iget-boolean v0, p0, Ljpg;->V:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 599
    :cond_2
    return-void

    .line 541
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 542
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->T:Z

    .line 543
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 545
    const/4 v1, 0x1

    .line 546
    const/4 v0, 0x0

    .line 548
    :goto_3
    const/16 v2, 0x7c

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 549
    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    .line 550
    add-int/lit8 v1, v1, 0x1

    .line 553
    add-int/lit8 v0, v0, 0x1

    .line 554
    goto :goto_3

    .line 555
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "|"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 556
    invoke-virtual {p0}, Ljpg;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f11002b

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 557
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    .line 556
    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 559
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 561
    invoke-virtual {p0}, Ljpg;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f11002b

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 562
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    .line 561
    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 565
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    iget-object v2, p0, Ljpg;->I:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Ljpg;->F:Ljava/lang/String;

    sget-object v4, Ljpg;->d:Landroid/text/style/StyleSpan;

    sget-object v5, Ljpg;->e:Landroid/text/style/ForegroundColorSpan;

    invoke-static/range {v0 .. v5}, Llhv;->a(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 568
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->U:Z

    goto/16 :goto_0

    .line 570
    :cond_5
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    iget-object v2, p0, Ljpg;->I:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Ljpg;->F:Ljava/lang/String;

    sget-object v4, Ljpg;->d:Landroid/text/style/StyleSpan;

    sget-object v5, Ljpg;->e:Landroid/text/style/ForegroundColorSpan;

    move-object v1, p3

    invoke-static/range {v0 .. v5}, Llhv;->a(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 572
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpg;->U:Z

    goto/16 :goto_0

    .line 574
    :cond_6
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 575
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->T:Z

    .line 576
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->U:Z

    .line 577
    iget-object v0, p0, Ljpg;->g:Lhxh;

    if-eqz v0, :cond_1

    .line 578
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    iget-object v1, p0, Ljpg;->g:Lhxh;

    .line 579
    invoke-virtual {v1, p1}, Lhxh;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 578
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 581
    :cond_7
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 582
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->T:Z

    .line 583
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpg;->U:Z

    .line 584
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 585
    :cond_8
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 586
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->T:Z

    .line 587
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpg;->U:Z

    .line 588
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-static {p6}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 590
    :cond_9
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpg;->T:Z

    .line 591
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpg;->U:Z

    .line 592
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 595
    :cond_a
    const/16 v0, 0x8

    goto/16 :goto_1

    .line 597
    :cond_b
    const/16 v0, 0x8

    goto/16 :goto_2
.end method

.method public a(Ljph;)V
    .locals 0

    .prologue
    .line 758
    iput-object p1, p0, Ljpg;->aj:Ljph;

    .line 759
    return-void
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 799
    invoke-virtual {p0}, Ljpg;->invalidate()V

    .line 800
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 406
    iput-boolean p1, p0, Ljpg;->ao:Z

    .line 407
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 396
    iput-boolean p1, p0, Ljpg;->ak:Z

    .line 397
    iget-boolean v0, p0, Ljpg;->ak:Z

    if-eqz v0, :cond_1

    .line 398
    invoke-direct {p0}, Ljpg;->l()V

    .line 399
    iget-object v0, p0, Ljpg;->S:Landroid/widget/TextView;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljpg;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Ljpg;->S:Landroid/widget/TextView;

    iget-object v1, p0, Ljpg;->S:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v1, p0, Ljpg;->S:Landroid/widget/TextView;

    const v2, 0x1030044

    invoke-virtual {v1, v0, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v1, p0, Ljpg;->S:Landroid/widget/TextView;

    const/16 v2, 0xc

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v1, p0, Ljpg;->S:Landroid/widget/TextView;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    new-instance v1, Landroid/widget/ImageView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Ljpg;->am:Landroid/widget/ImageView;

    iget-object v0, p0, Ljpg;->am:Landroid/widget/ImageView;

    const v1, 0x7f10005e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    iget-object v0, p0, Ljpg;->am:Landroid/widget/ImageView;

    iget-object v1, p0, Ljpg;->al:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Ljpg;->am:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Ljpg;->am:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Ljpg;->l()V

    :cond_0
    iget-object v0, p0, Ljpg;->S:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    :goto_0
    return-void

    .line 401
    :cond_1
    invoke-direct {p0}, Ljpg;->m()V

    goto :goto_0
.end method

.method public adjustListItemSelectionBounds(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 763
    iget-boolean v0, p0, Ljpg;->ag:Z

    if-eqz v0, :cond_0

    .line 764
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Ljpg;->ah:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 766
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 782
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljpg;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 783
    sget-object v0, Ljpg;->f:Lhso;

    iget-object v1, p0, Ljpg;->u:Ljava/lang/String;

    iget v2, p0, Ljpg;->y:I

    const/4 v3, 0x1

    .line 784
    invoke-interface {v0, v1, v2, v3, p0}, Lhso;->a(Ljava/lang/String;IILkdd;)Lkda;

    move-result-object v0

    iput-object v0, p0, Ljpg;->x:Lkda;

    .line 787
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 383
    if-nez p1, :cond_0

    .line 384
    const/4 v0, 0x0

    iput-object v0, p0, Ljpg;->F:Ljava/lang/String;

    .line 388
    :goto_0
    return-void

    .line 386
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpg;->F:Ljava/lang/String;

    goto :goto_0
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 481
    if-eqz p1, :cond_0

    .line 482
    invoke-virtual {p0}, Ljpg;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ljpg;->J:Landroid/widget/TextView;

    const/16 v2, 0x1a

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 488
    :goto_0
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f000000    # 0.5f

    .line 489
    :goto_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2

    .line 490
    invoke-virtual {p0, v0}, Ljpg;->setAlpha(F)V

    .line 497
    :goto_2
    return-void

    .line 485
    :cond_0
    invoke-virtual {p0}, Ljpg;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ljpg;->J:Landroid/widget/TextView;

    const/16 v2, 0xc

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    goto :goto_0

    .line 488
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_1

    .line 492
    :cond_2
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v0, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 493
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 494
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 495
    invoke-virtual {p0, v1}, Ljpg;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2
.end method

.method public c()V
    .locals 1

    .prologue
    .line 791
    iget-object v0, p0, Ljpg;->x:Lkda;

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Ljpg;->x:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 793
    const/4 v0, 0x0

    iput-object v0, p0, Ljpg;->x:Lkda;

    .line 795
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Ljpg;->z:Ljava/lang/String;

    .line 392
    invoke-direct {p0}, Ljpg;->n()V

    .line 393
    return-void
.end method

.method public c(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 605
    iget-boolean v0, p0, Ljpg;->W:Z

    if-ne v0, p1, :cond_1

    .line 625
    :cond_0
    :goto_0
    return-void

    .line 609
    :cond_1
    iput-boolean p1, p0, Ljpg;->W:Z

    .line 610
    iget-boolean v0, p0, Ljpg;->W:Z

    if-eqz v0, :cond_4

    .line 611
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    if-nez v0, :cond_3

    .line 612
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Ljpg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    .line 613
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    const v1, 0x7f0205e6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 614
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 615
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 616
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    sget-object v1, Ljpg;->ap:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Ljpg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02015c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Ljpg;->ap:Landroid/graphics/drawable/Drawable;

    :cond_2
    sget-object v1, Ljpg;->ap:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 617
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 618
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    invoke-virtual {p0}, Ljpg;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0519

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 619
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Ljpg;->addView(Landroid/view/View;)V

    .line 621
    :cond_3
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 622
    :cond_4
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 623
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Ljpg;->s:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Ljpg;->C:Ljava/lang/String;

    .line 449
    invoke-direct {p0}, Ljpg;->n()V

    .line 450
    return-void
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 702
    iget-boolean v0, p0, Ljpg;->ab:Z

    if-ne v0, p1, :cond_1

    .line 715
    :cond_0
    :goto_0
    return-void

    .line 706
    :cond_1
    iput-boolean p1, p0, Ljpg;->ab:Z

    .line 707
    iget-boolean v0, p0, Ljpg;->ab:Z

    if-eqz v0, :cond_3

    .line 708
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    if-nez v0, :cond_2

    .line 709
    const v0, 0x7f0a0519

    invoke-virtual {p0, v0}, Ljpg;->a(I)V

    .line 711
    :cond_2
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 712
    :cond_3
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 713
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1094
    iget-boolean v0, p0, Ljpg;->B:Z

    if-eqz v0, :cond_3

    .line 1095
    sget-object v0, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1096
    sget-object v1, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1097
    iget-object v2, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v3, p0, Ljpg;->n:I

    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 1098
    iget-object v3, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Ljpg;->n:I

    sub-int/2addr v4, v1

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 1099
    sget-object v4, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1100
    sget-object v0, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1134
    :cond_0
    :goto_0
    iget-boolean v0, p0, Ljpg;->ab:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Ljpg;->W:Z

    if-eqz v0, :cond_2

    .line 1136
    :cond_1
    iget-boolean v0, p0, Ljpg;->ag:Z

    if-eqz v0, :cond_8

    iget v0, p0, Ljpg;->ah:I

    iget v1, p0, Ljpg;->o:I

    add-int/2addr v0, v1

    .line 1138
    :goto_1
    sget-object v1, Ljpg;->aq:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Ljpg;->af:I

    iget v3, p0, Ljpg;->af:I

    iget v4, p0, Ljpg;->p:I

    add-int/2addr v3, v4

    .line 1140
    invoke-virtual {p0}, Ljpg;->getHeight()I

    move-result v4

    iget v5, p0, Ljpg;->o:I

    sub-int/2addr v4, v5

    .line 1138
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1141
    sget-object v0, Ljpg;->aq:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1143
    :cond_2
    invoke-super {p0, p1}, Llin;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1144
    return-void

    .line 1101
    :cond_3
    iget-boolean v0, p0, Ljpg;->D:Z

    if-eqz v0, :cond_4

    .line 1102
    sget-object v0, Ljpg;->at:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1103
    sget-object v1, Ljpg;->at:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1104
    iget-object v2, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v3, p0, Ljpg;->n:I

    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 1105
    iget-object v3, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Ljpg;->n:I

    sub-int/2addr v4, v1

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 1106
    sget-object v4, Ljpg;->at:Landroid/graphics/drawable/Drawable;

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1107
    sget-object v0, Ljpg;->at:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1108
    :cond_4
    iget-boolean v0, p0, Ljpg;->A:Z

    if-eqz v0, :cond_0

    .line 1109
    iget-boolean v0, p0, Ljpg;->U:Z

    if-eqz v0, :cond_5

    .line 1110
    iget-object v0, p0, Ljpg;->K:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1113
    :cond_5
    iget-boolean v0, p0, Ljpg;->w:Z

    if-eqz v0, :cond_7

    .line 1114
    iget-object v0, p0, Ljpg;->N:Landroid/graphics/Bitmap;

    .line 1115
    iget-object v1, p0, Ljpg;->x:Lkda;

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljpg;->x:Lkda;

    .line 1116
    invoke-virtual {v1}, Lkda;->getStatus()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 1117
    iget-object v0, p0, Ljpg;->x:Lkda;

    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1120
    :cond_6
    iget-object v1, p0, Ljpg;->P:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1121
    iget-object v1, p0, Ljpg;->P:Landroid/graphics/Rect;

    iget-object v2, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iget-object v3, p0, Ljpg;->O:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1124
    :cond_7
    iget-object v0, p0, Ljpg;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1125
    sget-object v0, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 1126
    sget-object v1, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 1127
    iget-object v2, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLeft()I

    move-result v2

    sub-int/2addr v2, v0

    iget v3, p0, Ljpg;->r:I

    sub-int/2addr v2, v3

    .line 1128
    iget v3, p0, Ljpg;->q:I

    .line 1129
    sget-object v4, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1130
    sget-object v0, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 1136
    :cond_8
    iget v0, p0, Ljpg;->o:I

    goto/16 :goto_1
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Ljpg;->t:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->D:Z

    .line 462
    iput-object p1, p0, Ljpg;->E:Ljava/lang/String;

    .line 463
    iget-object v0, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 464
    return-void
.end method

.method public e(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 721
    iput-boolean p1, p0, Ljpg;->ag:Z

    .line 722
    iget-boolean v0, p0, Ljpg;->ag:Z

    if-eqz v0, :cond_2

    .line 723
    iget-object v0, p0, Ljpg;->ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    if-nez v0, :cond_1

    .line 724
    invoke-virtual {p0}, Ljpg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 725
    const v1, 0x7f0401d0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    iput-object v0, p0, Ljpg;->ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    .line 728
    iget-object v0, p0, Ljpg;->ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    invoke-virtual {p0, v0}, Ljpg;->addView(Landroid/view/View;)V

    .line 735
    :cond_0
    :goto_0
    return-void

    .line 730
    :cond_1
    iget-object v0, p0, Ljpg;->ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->setVisibility(I)V

    goto :goto_0

    .line 732
    :cond_2
    iget-object v0, p0, Ljpg;->ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Ljpg;->ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->setVisibility(I)V

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Ljpg;->z:Ljava/lang/String;

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpg;->T:Z

    .line 476
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljpg;->U:Z

    .line 477
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    return-void
.end method

.method public f(Z)V
    .locals 2

    .prologue
    .line 750
    iput-boolean p1, p0, Ljpg;->A:Z

    .line 751
    iget-object v1, p0, Ljpg;->G:Landroid/widget/TextView;

    iget-boolean v0, p0, Ljpg;->A:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 752
    return-void

    .line 751
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Ljpg;->u:Ljava/lang/String;

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 507
    iget-object v0, p0, Ljpg;->g:Lhxh;

    if-eqz v0, :cond_0

    .line 508
    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ljpg;->T:Z

    .line 509
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Ljpg;->U:Z

    .line 510
    iget-object v0, p0, Ljpg;->J:Landroid/widget/TextView;

    iget-object v1, p0, Ljpg;->g:Lhxh;

    .line 511
    invoke-virtual {v1, p1}, Lhxh;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 510
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 508
    goto :goto_0

    :cond_2
    move v1, v2

    .line 509
    goto :goto_1
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 440
    iget-boolean v0, p0, Ljpg;->ak:Z

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Ljpg;->C:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Ljpg;->E:Ljava/lang/String;

    return-object v0
.end method

.method public k()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1176
    invoke-virtual {p0}, Ljpg;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1178
    iget-object v1, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 1179
    iget-boolean v2, p0, Ljpg;->T:Z

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 1180
    const v2, 0x7f0a051c

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Ljpg;->z:Ljava/lang/String;

    aput-object v4, v3, v5

    aput-object v1, v3, v6

    .line 1181
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1180
    invoke-virtual {p0, v0}, Ljpg;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1190
    :cond_0
    :goto_0
    return-void

    .line 1183
    :cond_1
    iget-object v1, p0, Ljpg;->z:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1184
    const v1, 0x7f0a0526

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Ljpg;->z:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljpg;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1186
    :cond_2
    iget-object v1, p0, Ljpg;->C:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1187
    const v1, 0x7f0a0527

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Ljpg;->C:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljpg;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 770
    invoke-super {p0}, Llin;->onAttachedToWindow()V

    .line 771
    invoke-virtual {p0}, Ljpg;->b()V

    .line 772
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1158
    iget-object v0, p0, Ljpg;->aj:Ljph;

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Ljpg;->ad:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    .line 1160
    iget-object v0, p0, Ljpg;->aj:Ljph;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Ljph;->a(Ljpg;I)V

    .line 1169
    :cond_0
    :goto_0
    return-void

    .line 1161
    :cond_1
    if-nez p1, :cond_2

    .line 1162
    iget-object v0, p0, Ljpg;->aj:Ljph;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Ljph;->a(Ljpg;I)V

    goto :goto_0

    .line 1163
    :cond_2
    if-nez p1, :cond_3

    .line 1164
    iget-object v0, p0, Ljpg;->aj:Ljph;

    const/4 v1, 0x2

    invoke-interface {v0, p0, v1}, Ljph;->a(Ljpg;I)V

    goto :goto_0

    .line 1165
    :cond_3
    iget-object v0, p0, Ljpg;->ac:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 1166
    iget-object v0, p0, Ljpg;->aj:Ljph;

    const/4 v1, 0x3

    invoke-interface {v0, p0, v1}, Ljph;->a(Ljpg;I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 776
    invoke-super {p0}, Llin;->onDetachedFromWindow()V

    .line 777
    invoke-virtual {p0}, Ljpg;->c()V

    .line 778
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 13

    .prologue
    .line 943
    sub-int v7, p5, p3

    .line 944
    const/4 v0, 0x0

    .line 945
    iget-boolean v1, p0, Ljpg;->ag:Z

    if-eqz v1, :cond_0

    .line 946
    iget-object v0, p0, Ljpg;->ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sub-int v3, p4, p2

    iget v4, p0, Ljpg;->ah:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->layout(IIII)V

    .line 947
    iget v0, p0, Ljpg;->ah:I

    add-int/lit8 v0, v0, 0x0

    .line 950
    :cond_0
    iget v1, p0, Ljpg;->i:I

    .line 952
    iget-boolean v2, p0, Ljpg;->w:Z

    if-eqz v2, :cond_1

    .line 953
    iget-object v2, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iput v1, v2, Landroid/graphics/Rect;->left:I

    .line 954
    iget-object v2, p0, Ljpg;->Q:Landroid/graphics/Rect;

    sub-int v3, v7, v0

    iget v4, p0, Ljpg;->n:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 955
    iget-object v2, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iget-object v3, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Ljpg;->n:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 956
    iget-object v2, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iget-object v3, p0, Ljpg;->Q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Ljpg;->n:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 958
    iget v2, p0, Ljpg;->n:I

    iget v3, p0, Ljpg;->j:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 961
    :cond_1
    sub-int v2, p4, p2

    iget v3, p0, Ljpg;->i:I

    sub-int/2addr v2, v3

    .line 963
    iget-boolean v3, p0, Ljpg;->ab:Z

    if-eqz v3, :cond_2

    .line 964
    iget-object v3, p0, Ljpg;->ac:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    .line 965
    sub-int v4, v2, v3

    .line 966
    iget v5, p0, Ljpg;->p:I

    sub-int v5, v4, v5

    iput v5, p0, Ljpg;->af:I

    .line 967
    iget-object v5, p0, Ljpg;->ac:Landroid/widget/TextView;

    add-int v6, v4, v3

    invoke-virtual {v5, v4, v0, v6, v7}, Landroid/widget/TextView;->layout(IIII)V

    .line 969
    sub-int/2addr v2, v3

    .line 972
    :cond_2
    iget-boolean v3, p0, Ljpg;->W:Z

    if-eqz v3, :cond_3

    .line 989
    iget-object v3, p0, Ljpg;->ad:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    .line 991
    sub-int v4, v2, v3

    iput v4, p0, Ljpg;->af:I

    .line 993
    iget v4, p0, Ljpg;->af:I

    iget v5, p0, Ljpg;->p:I

    add-int/2addr v4, v5

    .line 995
    iget-object v5, p0, Ljpg;->ad:Landroid/widget/ImageView;

    add-int v6, v4, v3

    invoke-virtual {v5, v4, v0, v6, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 998
    sub-int/2addr v2, v3

    .line 1001
    :cond_3
    iget-boolean v3, p0, Ljpg;->b:Z

    if-eqz v3, :cond_4

    .line 1002
    iget-object v3, p0, Ljpg;->c:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v3

    .line 1003
    iget-object v4, p0, Ljpg;->c:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v4

    .line 1004
    sub-int v5, v7, v0

    sub-int/2addr v5, v4

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v0

    .line 1005
    iget-object v6, p0, Ljpg;->c:Landroid/widget/CheckBox;

    sub-int v8, v2, v3

    sget v9, Ljpg;->a:I

    sub-int/2addr v8, v9

    sget v9, Ljpg;->a:I

    sub-int v9, v2, v9

    add-int/2addr v4, v5

    invoke-virtual {v6, v8, v5, v9, v4}, Landroid/widget/CheckBox;->layout(IIII)V

    .line 1007
    sub-int/2addr v2, v3

    .line 1010
    :cond_4
    iget-boolean v3, p0, Ljpg;->ab:Z

    if-nez v3, :cond_5

    iget-boolean v3, p0, Ljpg;->W:Z

    if-nez v3, :cond_5

    iget-boolean v3, p0, Ljpg;->b:Z

    if-eqz v3, :cond_6

    .line 1012
    :cond_5
    iget v3, p0, Ljpg;->m:I

    sub-int/2addr v2, v3

    .line 1015
    :cond_6
    iget-boolean v3, p0, Ljpg;->V:Z

    if-eqz v3, :cond_a

    iget-object v3, p0, Ljpg;->R:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    .line 1016
    :goto_0
    const/4 v6, 0x0

    .line 1017
    const/4 v5, 0x0

    .line 1018
    const/4 v4, 0x0

    .line 1019
    iget-boolean v8, p0, Ljpg;->ak:Z

    if-eqz v8, :cond_7

    .line 1020
    iget-boolean v4, p0, Ljpg;->an:Z

    if-eqz v4, :cond_b

    iget-object v4, p0, Ljpg;->S:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    .line 1021
    :goto_1
    iget-object v5, p0, Ljpg;->al:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 1022
    iget-boolean v5, p0, Ljpg;->ak:Z

    if-eqz v5, :cond_c

    iget v5, p0, Ljpg;->l:I

    add-int/2addr v5, v6

    add-int/2addr v5, v4

    :goto_2
    move v12, v6

    move v6, v5

    move v5, v4

    move v4, v12

    .line 1025
    :cond_7
    iget-boolean v8, p0, Ljpg;->A:Z

    if-nez v8, :cond_d

    .line 1026
    iget-object v4, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    .line 1027
    sub-int v5, v7, v0

    sub-int/2addr v5, v4

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    .line 1028
    iget-boolean v5, p0, Ljpg;->V:Z

    if-eqz v5, :cond_8

    .line 1029
    iget-object v5, p0, Ljpg;->R:Landroid/widget/TextView;

    sub-int v7, v2, v3

    sub-int/2addr v7, v6

    add-int v8, v0, v4

    invoke-virtual {v5, v7, v0, v2, v8}, Landroid/widget/TextView;->layout(IIII)V

    .line 1031
    add-int/2addr v3, v6

    iget v5, p0, Ljpg;->l:I

    add-int/2addr v3, v5

    sub-int/2addr v2, v3

    .line 1034
    :cond_8
    iget-object v3, p0, Ljpg;->J:Landroid/widget/TextView;

    add-int/2addr v4, v0

    invoke-virtual {v3, v1, v0, v2, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 1090
    :cond_9
    :goto_3
    return-void

    .line 1015
    :cond_a
    const/4 v3, 0x0

    goto :goto_0

    .line 1020
    :cond_b
    const/4 v4, 0x0

    goto :goto_1

    .line 1022
    :cond_c
    const/4 v5, 0x0

    goto :goto_2

    .line 1035
    :cond_d
    iget-boolean v6, p0, Ljpg;->T:Z

    if-nez v6, :cond_e

    iget-boolean v6, p0, Ljpg;->ak:Z

    if-eqz v6, :cond_14

    .line 1036
    :cond_e
    iget-object v6, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    .line 1037
    iget-boolean v6, p0, Ljpg;->T:Z

    if-eqz v6, :cond_13

    iget-object v6, p0, Ljpg;->J:Landroid/widget/TextView;

    .line 1038
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    .line 1041
    :goto_4
    iget-boolean v7, p0, Ljpg;->U:Z

    if-eqz v7, :cond_16

    .line 1042
    iget v7, p0, Ljpg;->L:I

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 1044
    :goto_5
    add-int/2addr v7, v8

    .line 1046
    iget v9, p0, Ljpg;->h:I

    sub-int v7, v9, v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v0

    .line 1049
    iget-object v0, p0, Ljpg;->v:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 1050
    sget-object v0, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget v9, p0, Ljpg;->r:I

    add-int/2addr v0, v9

    add-int/2addr v0, v1

    .line 1052
    :goto_6
    iget-object v9, p0, Ljpg;->G:Landroid/widget/TextView;

    iget-object v10, p0, Ljpg;->G:Landroid/widget/TextView;

    .line 1053
    invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v10, v0

    add-int v11, v7, v8

    .line 1052
    invoke-virtual {v9, v0, v7, v10, v11}, Landroid/widget/TextView;->layout(IIII)V

    .line 1055
    iget v0, p0, Ljpg;->k:I

    add-int/2addr v0, v8

    add-int/2addr v0, v7

    .line 1057
    iget-boolean v7, p0, Ljpg;->T:Z

    if-eqz v7, :cond_f

    iget-boolean v7, p0, Ljpg;->U:Z

    if-eqz v7, :cond_f

    .line 1058
    iget v7, p0, Ljpg;->M:I

    iget v8, p0, Ljpg;->L:I

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v0

    .line 1059
    iget-object v8, p0, Ljpg;->K:Landroid/graphics/drawable/Drawable;

    iget v9, p0, Ljpg;->L:I

    add-int/2addr v9, v1

    iget v10, p0, Ljpg;->L:I

    add-int/2addr v10, v7

    invoke-virtual {v8, v1, v7, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1061
    iget v7, p0, Ljpg;->L:I

    iget v8, p0, Ljpg;->l:I

    add-int/2addr v7, v8

    add-int/2addr v1, v7

    .line 1064
    :cond_f
    iget v7, p0, Ljpg;->M:I

    sub-int/2addr v7, v6

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v0, v7

    .line 1065
    iget-boolean v7, p0, Ljpg;->ak:Z

    if-eqz v7, :cond_11

    .line 1066
    iget-boolean v7, p0, Ljpg;->an:Z

    if-eqz v7, :cond_10

    .line 1067
    iget-object v7, p0, Ljpg;->S:Landroid/widget/TextView;

    sub-int v8, v2, v5

    add-int v9, v0, v6

    invoke-virtual {v7, v8, v0, v2, v9}, Landroid/widget/TextView;->layout(IIII)V

    .line 1069
    iget v7, p0, Ljpg;->l:I

    add-int/2addr v5, v7

    sub-int/2addr v2, v5

    .line 1071
    :cond_10
    iget-object v5, p0, Ljpg;->am:Landroid/widget/ImageView;

    sub-int v7, v2, v4

    add-int v8, v0, v6

    invoke-virtual {v5, v7, v0, v2, v8}, Landroid/widget/ImageView;->layout(IIII)V

    .line 1073
    iget v5, p0, Ljpg;->l:I

    add-int/2addr v4, v5

    sub-int/2addr v2, v4

    .line 1075
    :cond_11
    iget-boolean v4, p0, Ljpg;->T:Z

    if-eqz v4, :cond_12

    iget-boolean v4, p0, Ljpg;->V:Z

    if-eqz v4, :cond_12

    .line 1076
    iget-object v4, p0, Ljpg;->R:Landroid/widget/TextView;

    sub-int v5, v2, v3

    add-int v7, v0, v6

    invoke-virtual {v4, v5, v0, v2, v7}, Landroid/widget/TextView;->layout(IIII)V

    .line 1078
    iget v4, p0, Ljpg;->l:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    .line 1080
    :cond_12
    iget-boolean v3, p0, Ljpg;->T:Z

    if-eqz v3, :cond_9

    .line 1081
    iget-object v3, p0, Ljpg;->J:Landroid/widget/TextView;

    add-int v4, v0, v6

    invoke-virtual {v3, v1, v0, v2, v4}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_3

    .line 1038
    :cond_13
    iget-object v6, p0, Ljpg;->S:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    goto/16 :goto_4

    .line 1085
    :cond_14
    iget-object v2, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    .line 1086
    sub-int v3, v7, v0

    sub-int/2addr v3, v2

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    .line 1087
    iget-object v3, p0, Ljpg;->G:Landroid/widget/TextView;

    iget-object v4, p0, Ljpg;->G:Landroid/widget/TextView;

    .line 1088
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    add-int/2addr v2, v0

    .line 1087
    invoke-virtual {v3, v1, v0, v4, v2}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_3

    :cond_15
    move v0, v1

    goto/16 :goto_6

    :cond_16
    move v7, v6

    goto/16 :goto_5
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 804
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 805
    if-nez v0, :cond_9

    move v0, v1

    .line 809
    :goto_0
    iget v2, p0, Ljpg;->i:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    .line 811
    iget-boolean v3, p0, Ljpg;->w:Z

    if-eqz v3, :cond_0

    .line 812
    iget v3, p0, Ljpg;->n:I

    iget v4, p0, Ljpg;->j:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    .line 816
    :cond_0
    iget-boolean v3, p0, Ljpg;->ab:Z

    if-eqz v3, :cond_f

    .line 817
    iget-object v3, p0, Ljpg;->ac:Landroid/widget/TextView;

    invoke-virtual {v3, v1, p2}, Landroid/widget/TextView;->measure(II)V

    .line 818
    iget-object v3, p0, Ljpg;->ac:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    .line 819
    iget-object v4, p0, Ljpg;->ac:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    .line 820
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 821
    iget v5, p0, Ljpg;->p:I

    add-int/2addr v5, v3

    sub-int/2addr v2, v5

    move v9, v3

    move v3, v2

    move v2, v9

    .line 824
    :goto_1
    iget-boolean v5, p0, Ljpg;->W:Z

    if-eqz v5, :cond_1

    .line 825
    iget-object v5, p0, Ljpg;->ad:Landroid/widget/ImageView;

    iget v6, p0, Ljpg;->ae:I

    .line 826
    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 825
    invoke-virtual {v5, v6, p2}, Landroid/widget/ImageView;->measure(II)V

    .line 828
    iget-object v5, p0, Ljpg;->ad:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 829
    iget v5, p0, Ljpg;->ae:I

    iget v6, p0, Ljpg;->p:I

    add-int/2addr v5, v6

    sub-int/2addr v3, v5

    .line 832
    :cond_1
    iget-boolean v5, p0, Ljpg;->b:Z

    if-eqz v5, :cond_2

    .line 849
    iget-object v5, p0, Ljpg;->c:Landroid/widget/CheckBox;

    invoke-virtual {v5, v1, p2}, Landroid/widget/CheckBox;->measure(II)V

    .line 850
    iget-object v5, p0, Ljpg;->c:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    .line 851
    iget-object v4, p0, Ljpg;->c:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    .line 854
    :cond_2
    iget-boolean v4, p0, Ljpg;->ab:Z

    if-nez v4, :cond_3

    iget-boolean v4, p0, Ljpg;->W:Z

    if-nez v4, :cond_3

    iget-boolean v4, p0, Ljpg;->b:Z

    if-eqz v4, :cond_4

    .line 856
    :cond_3
    iget v4, p0, Ljpg;->m:I

    sub-int/2addr v3, v4

    .line 860
    :cond_4
    iget-object v4, p0, Ljpg;->v:Ljava/lang/String;

    if-eqz v4, :cond_e

    .line 861
    sget-object v4, Ljpg;->as:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget v5, p0, Ljpg;->l:I

    add-int/2addr v4, v5

    sub-int v4, v3, v4

    .line 863
    :goto_2
    iget-boolean v5, p0, Ljpg;->ao:Z

    if-eqz v5, :cond_a

    .line 864
    iget-object v5, p0, Ljpg;->G:Landroid/widget/TextView;

    const v6, 0x7f0203b2

    invoke-virtual {v5, v1, v1, v6, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 866
    iget-object v5, p0, Ljpg;->G:Landroid/widget/TextView;

    iget v6, p0, Ljpg;->l:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 870
    :goto_3
    iget-object v5, p0, Ljpg;->G:Landroid/widget/TextView;

    const/high16 v6, -0x80000000

    .line 871
    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 870
    invoke-virtual {v5, v4, p2}, Landroid/widget/TextView;->measure(II)V

    .line 874
    iget v4, p0, Ljpg;->n:I

    .line 878
    iget-boolean v5, p0, Ljpg;->U:Z

    if-eqz v5, :cond_5

    .line 879
    iget v5, p0, Ljpg;->L:I

    iget v6, p0, Ljpg;->l:I

    add-int/2addr v5, v6

    sub-int/2addr v3, v5

    .line 882
    :cond_5
    iget-boolean v5, p0, Ljpg;->ak:Z

    if-eqz v5, :cond_c

    .line 883
    iget-object v5, p0, Ljpg;->S:Landroid/widget/TextView;

    invoke-virtual {v5, v1, v1}, Landroid/widget/TextView;->measure(II)V

    .line 885
    iget-object v5, p0, Ljpg;->al:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget v6, p0, Ljpg;->l:I

    add-int/2addr v5, v6

    .line 886
    iget-object v6, p0, Ljpg;->S:Landroid/widget/TextView;

    .line 887
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v5

    iget v7, p0, Ljpg;->l:I

    add-int/2addr v6, v7

    sub-int v7, v3, v5

    .line 886
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 892
    div-int/lit8 v7, v3, 0x2

    if-ge v6, v7, :cond_b

    .line 893
    sub-int/2addr v3, v6

    .line 894
    const/4 v5, 0x1

    iput-boolean v5, p0, Ljpg;->an:Z

    .line 901
    :goto_4
    iget-object v5, p0, Ljpg;->al:Landroid/graphics/Bitmap;

    .line 902
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    iget-object v6, p0, Ljpg;->S:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Ljpg;->M:I

    .line 903
    iget-object v5, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    iget v6, p0, Ljpg;->k:I

    add-int/2addr v5, v6

    iget v6, p0, Ljpg;->M:I

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 910
    :cond_6
    :goto_5
    iget-boolean v5, p0, Ljpg;->T:Z

    if-eqz v5, :cond_7

    .line 911
    iget-object v5, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v5, v1, v1}, Landroid/widget/TextView;->measure(II)V

    .line 913
    iget-object v5, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 914
    iget v5, p0, Ljpg;->L:I

    iget-object v6, p0, Ljpg;->J:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Ljpg;->M:I

    .line 916
    iget-object v5, p0, Ljpg;->J:Landroid/widget/TextView;

    .line 917
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v6, p0, Ljpg;->M:I

    .line 918
    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 916
    invoke-virtual {v5, v3, v6}, Landroid/widget/TextView;->measure(II)V

    .line 920
    iget-object v3, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    iget v5, p0, Ljpg;->k:I

    add-int/2addr v3, v5

    iget v5, p0, Ljpg;->M:I

    add-int/2addr v3, v5

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 924
    :cond_7
    iget v3, p0, Ljpg;->i:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v4

    .line 925
    iget v4, p0, Ljpg;->h:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 927
    iget-boolean v4, p0, Ljpg;->ab:Z

    if-eqz v4, :cond_8

    .line 928
    iget-object v4, p0, Ljpg;->ac:Landroid/widget/TextView;

    .line 929
    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 930
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 928
    invoke-virtual {v4, v2, v5}, Landroid/widget/TextView;->measure(II)V

    .line 933
    :cond_8
    iget-boolean v2, p0, Ljpg;->ag:Z

    if-eqz v2, :cond_d

    .line 934
    iget-object v2, p0, Ljpg;->ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    invoke-virtual {v2, p1, v1}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->measure(II)V

    .line 935
    iget-object v1, p0, Ljpg;->ai:Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Ljpg;->ah:I

    .line 936
    iget v1, p0, Ljpg;->ah:I

    add-int/2addr v1, v3

    .line 938
    :goto_6
    invoke-virtual {p0, v0, v1}, Ljpg;->setMeasuredDimension(II)V

    .line 939
    return-void

    .line 806
    :cond_9
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    goto/16 :goto_0

    .line 868
    :cond_a
    iget-object v5, p0, Ljpg;->G:Landroid/widget/TextView;

    invoke-virtual {v5, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_3

    .line 896
    :cond_b
    sub-int/2addr v3, v5

    .line 897
    iput-boolean v1, p0, Ljpg;->an:Z

    goto/16 :goto_4

    .line 905
    :cond_c
    iget-boolean v5, p0, Ljpg;->V:Z

    if-eqz v5, :cond_6

    .line 906
    iget-object v5, p0, Ljpg;->R:Landroid/widget/TextView;

    invoke-virtual {v5, v1, v1}, Landroid/widget/TextView;->measure(II)V

    .line 907
    iget-object v5, p0, Ljpg;->R:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    iget v6, p0, Ljpg;->l:I

    add-int/2addr v5, v6

    sub-int/2addr v3, v5

    goto/16 :goto_5

    :cond_d
    move v1, v3

    goto :goto_6

    :cond_e
    move v4, v3

    goto/16 :goto_2

    :cond_f
    move v3, v2

    move v4, v1

    move v2, v1

    goto/16 :goto_1
.end method
