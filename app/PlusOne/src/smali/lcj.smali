.class public Llcj;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private a:Llct;

.field private b:Ljava/lang/String;

.field private c:Landroid/graphics/Bitmap;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    iget-object v0, p0, Llcj;->a:Llct;

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Llcj;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    iput-object v0, p0, Llcj;->a:Llct;

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 147
    iput-object v0, p0, Llcj;->b:Ljava/lang/String;

    .line 148
    iput-object v0, p0, Llcj;->c:Landroid/graphics/Bitmap;

    .line 150
    iput-object v0, p0, Llcj;->i:Landroid/text/StaticLayout;

    .line 151
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 158
    iput p1, p0, Llcj;->f:I

    .line 159
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Llcj;->a(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 61
    return-void
.end method

.method public a(ILjava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 64
    iput-object p2, p0, Llcj;->b:Ljava/lang/String;

    .line 65
    iput-object p3, p0, Llcj;->c:Landroid/graphics/Bitmap;

    .line 66
    iput p1, p0, Llcj;->d:I

    .line 67
    const/16 v0, 0x22

    iput v0, p0, Llcj;->e:I

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Llcj;->h:Z

    .line 70
    iget-object v0, p0, Llcj;->a:Llct;

    iget v0, v0, Llct;->ao:I

    iput v0, p0, Llcj;->g:I

    .line 71
    iget-object v0, p0, Llcj;->a:Llct;

    iget v0, v0, Llct;->m:I

    iput v0, p0, Llcj;->f:I

    .line 73
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 74
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 140
    iget-object v0, p0, Llcj;->a:Llct;

    iget-object v0, v0, Llct;->am:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    iget v1, p0, Llcj;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 141
    iget-object v0, p0, Llcj;->a:Llct;

    iget-object v0, v0, Llct;->am:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, v2, v2, p2, p3}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 142
    iget-object v0, p0, Llcj;->a:Llct;

    iget-object v0, v0, Llct;->am:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 143
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 181
    iput-boolean p1, p0, Llcj;->h:Z

    .line 182
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 166
    iput p1, p0, Llcj;->g:I

    .line 167
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 173
    iput p1, p0, Llcj;->e:I

    .line 174
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Llcj;->b:Ljava/lang/String;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 105
    invoke-virtual {p0}, Llcj;->getWidth()I

    move-result v0

    .line 106
    invoke-virtual {p0}, Llcj;->getHeight()I

    move-result v2

    .line 108
    invoke-virtual {p0, p1, v0, v2}, Llcj;->a(Landroid/graphics/Canvas;II)V

    .line 110
    iget-object v1, p0, Llcj;->c:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 112
    iget-boolean v1, p0, Llcj;->h:Z

    if-eqz v1, :cond_2

    .line 113
    iget-object v0, p0, Llcj;->a:Llct;

    iget v1, v0, Llct;->m:I

    .line 115
    iget v0, p0, Llcj;->f:I

    sub-int v0, v2, v0

    iget-object v3, p0, Llcj;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v0, v3

    .line 120
    :goto_0
    iget-object v3, p0, Llcj;->c:Landroid/graphics/Bitmap;

    int-to-float v1, v1

    int-to-float v0, v0

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v1, v0, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 123
    :cond_0
    iget-object v0, p0, Llcj;->i:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    .line 125
    iget-boolean v0, p0, Llcj;->h:Z

    if-eqz v0, :cond_3

    .line 126
    iget-object v0, p0, Llcj;->a:Llct;

    iget v0, v0, Llct;->m:I

    iget-object v1, p0, Llcj;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Llcj;->f:I

    add-int/2addr v1, v0

    .line 128
    iget-object v0, p0, Llcj;->i:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sub-int v0, v2, v0

    iget-object v2, p0, Llcj;->a:Llct;

    iget v2, v2, Llct;->m:I

    sub-int/2addr v0, v2

    .line 133
    :goto_1
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 134
    iget-object v2, p0, Llcj;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 135
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 137
    :cond_1
    return-void

    .line 117
    :cond_2
    iget-object v1, p0, Llcj;->a:Llct;

    iget v1, v1, Llct;->m:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Llcj;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 118
    iget-object v0, p0, Llcj;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 130
    :cond_3
    iget-object v0, p0, Llcj;->a:Llct;

    iget v1, v0, Llct;->m:I

    .line 131
    iget-object v0, p0, Llcj;->i:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 84
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 85
    iget-object v0, p0, Llcj;->a:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v3, v8, v0

    .line 86
    iget-object v0, p0, Llcj;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Llcj;->a:Llct;

    iget v0, v0, Llct;->m:I

    sub-int v0, v3, v0

    iget-object v1, p0, Llcj;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v3, v0, v1

    .line 90
    :cond_0
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Llcj;->b:Ljava/lang/String;

    .line 91
    invoke-virtual {p0}, Llcj;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v4, p0, Llcj;->e:I

    invoke-static {v2, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Llcj;->i:Landroid/text/StaticLayout;

    .line 95
    iget-object v0, p0, Llcj;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Llcj;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    :cond_1
    iget v0, p0, Llcj;->g:I

    iget-object v1, p0, Llcj;->i:Landroid/text/StaticLayout;

    .line 97
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    iget-object v2, p0, Llcj;->a:Llct;

    iget v2, v2, Llct;->m:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 96
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 95
    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 98
    invoke-virtual {p0, v8, v0}, Llcj;->setMeasuredDimension(II)V

    .line 99
    return-void
.end method
