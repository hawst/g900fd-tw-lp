.class final Lidu;
.super Ljava/io/FilterOutputStream;
.source "PG"


# instance fields
.field private a:Lidq;

.field private b:I

.field private c:I

.field private d:I

.field private e:[B

.field private f:Ljava/nio/ByteBuffer;

.field private final g:Lidp;


# direct methods
.method protected constructor <init>(Ljava/io/OutputStream;Lidp;)V
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/io/BufferedOutputStream;

    const/high16 v1, 0x10000

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    invoke-direct {p0, v0}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lidu;->b:I

    .line 82
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lidu;->e:[B

    .line 83
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    .line 88
    iput-object p2, p0, Lidu;->g:Lidp;

    .line 89
    return-void
.end method

.method private a(I[BII)I
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    sub-int v0, p1, v0

    .line 109
    if-le p4, v0, :cond_0

    move p4, v0

    .line 110
    :cond_0
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p2, p3, p4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 111
    return p4
.end method

.method private a(Lieb;I)I
    .locals 8

    .prologue
    .line 315
    invoke-virtual {p1}, Lieb;->d()I

    move-result v0

    mul-int/lit8 v0, v0, 0xc

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x4

    add-int v1, p2, v0

    .line 316
    invoke-virtual {p1}, Lieb;->b()[Liea;

    move-result-object v2

    .line 317
    array-length v3, v2

    const/4 v0, 0x0

    move v7, v0

    move v0, v1

    move v1, v7

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 318
    invoke-virtual {v4}, Liea;->d()I

    move-result v5

    const/4 v6, 0x4

    if-le v5, v6, :cond_0

    .line 319
    invoke-virtual {v4, v0}, Liea;->g(I)V

    .line 320
    invoke-virtual {v4}, Liea;->d()I

    move-result v4

    add-int/2addr v0, v4

    .line 317
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 323
    :cond_1
    return v0
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lidu;->a:Lidq;

    if-nez v0, :cond_1

    .line 242
    :cond_0
    return-void

    .line 217
    :cond_1
    iget-object v3, p0, Lidu;->a:Lidq;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lidq;->f()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_3

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {v3}, Lidq;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liea;

    invoke-virtual {v0}, Liea;->l()Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_2

    invoke-virtual {v0}, Liea;->b()S

    move-result v6

    invoke-static {v6}, Lidp;->a(S)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v0}, Liea;->b()S

    move-result v6

    invoke-virtual {v0}, Liea;->a()I

    move-result v7

    invoke-virtual {v3, v6, v7}, Lidq;->b(SI)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 218
    :cond_3
    invoke-direct {p0}, Lidu;->b()V

    .line 219
    invoke-direct {p0}, Lidu;->c()I

    move-result v0

    .line 220
    add-int/lit8 v1, v0, 0x8

    const v3, 0xffff

    if-le v1, v3, :cond_4

    .line 221
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Exif header is too large (>64Kb)"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_4
    new-instance v1, Lied;

    iget-object v3, p0, Lidu;->out:Ljava/io/OutputStream;

    invoke-direct {v1, v3}, Lied;-><init>(Ljava/io/OutputStream;)V

    .line 224
    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v3}, Lied;->a(Ljava/nio/ByteOrder;)Lied;

    .line 225
    const/16 v3, -0x1f

    invoke-virtual {v1, v3}, Lied;->a(S)Lied;

    .line 226
    add-int/lit8 v0, v0, 0x8

    int-to-short v0, v0

    invoke-virtual {v1, v0}, Lied;->a(S)Lied;

    .line 227
    const v0, 0x45786966

    invoke-virtual {v1, v0}, Lied;->a(I)Lied;

    .line 228
    invoke-virtual {v1, v2}, Lied;->a(S)Lied;

    .line 229
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0}, Lidq;->e()Ljava/nio/ByteOrder;

    move-result-object v0

    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    if-ne v0, v3, :cond_8

    .line 230
    const/16 v0, 0x4d4d

    invoke-virtual {v1, v0}, Lied;->a(S)Lied;

    .line 234
    :goto_1
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0}, Lidq;->e()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-virtual {v1, v0}, Lied;->a(Ljava/nio/ByteOrder;)Lied;

    .line 235
    const/16 v0, 0x2a

    invoke-virtual {v1, v0}, Lied;->a(S)Lied;

    .line 236
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lied;->a(I)Lied;

    .line 237
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0, v2}, Lidq;->b(I)Lieb;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lidu;->a(Lieb;Lied;)V

    iget-object v0, p0, Lidu;->a:Lidq;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lidq;->b(I)Lieb;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lidu;->a(Lieb;Lied;)V

    iget-object v0, p0, Lidu;->a:Lidq;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lidq;->b(I)Lieb;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v0, v1}, Lidu;->a(Lieb;Lied;)V

    :cond_5
    iget-object v0, p0, Lidu;->a:Lidq;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lidq;->b(I)Lieb;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-direct {p0, v0, v1}, Lidu;->a(Lieb;Lied;)V

    :cond_6
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0, v8}, Lidq;->b(I)Lieb;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0, v8}, Lidq;->b(I)Lieb;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lidu;->a(Lieb;Lied;)V

    .line 238
    :cond_7
    invoke-direct {p0, v1}, Lidu;->a(Lied;)V

    .line 239
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liea;

    .line 240
    iget-object v2, p0, Lidu;->a:Lidq;

    invoke-virtual {v2, v0}, Lidq;->a(Liea;)Liea;

    goto :goto_2

    .line 232
    :cond_8
    const/16 v0, 0x4949

    invoke-virtual {v1, v0}, Lied;->a(S)Lied;

    goto :goto_1
.end method

.method static a(Liea;Lied;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 489
    invoke-virtual {p0}, Liea;->f()Z

    move-result v1

    if-nez v1, :cond_1

    .line 528
    :cond_0
    :goto_0
    return-void

    .line 493
    :cond_1
    invoke-virtual {p0}, Liea;->c()S

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 518
    :pswitch_1
    invoke-virtual {p0}, Liea;->e()I

    move-result v0

    new-array v0, v0, [B

    .line 519
    invoke-virtual {p0, v0}, Liea;->b([B)V

    .line 520
    invoke-virtual {p1, v0}, Lied;->write([B)V

    goto :goto_0

    .line 495
    :pswitch_2
    invoke-virtual {p0}, Liea;->n()[B

    move-result-object v1

    .line 496
    array-length v2, v1

    invoke-virtual {p0}, Liea;->e()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 497
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aput-byte v0, v1, v2

    .line 498
    invoke-virtual {p1, v1}, Lied;->write([B)V

    goto :goto_0

    .line 500
    :cond_2
    invoke-virtual {p1, v1}, Lied;->write([B)V

    .line 501
    invoke-virtual {p1, v0}, Lied;->write(I)V

    goto :goto_0

    .line 506
    :pswitch_3
    invoke-virtual {p0}, Liea;->e()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_0

    .line 507
    invoke-virtual {p0, v0}, Liea;->e(I)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {p1, v2}, Lied;->a(I)Lied;

    .line 506
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 512
    :pswitch_4
    invoke-virtual {p0}, Liea;->e()I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_0

    .line 513
    invoke-virtual {p0, v0}, Liea;->f(I)Liee;

    move-result-object v2

    invoke-virtual {p1, v2}, Lied;->a(Liee;)Lied;

    .line 512
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 523
    :pswitch_5
    invoke-virtual {p0}, Liea;->e()I

    move-result v1

    :goto_3
    if-ge v0, v1, :cond_0

    .line 524
    invoke-virtual {p0, v0}, Liea;->e(I)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {p1, v2}, Lied;->a(S)Lied;

    .line 523
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 493
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lieb;Lied;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v0, 0x0

    .line 288
    invoke-virtual {p1}, Lieb;->b()[Liea;

    move-result-object v3

    .line 289
    array-length v1, v3

    int-to-short v1, v1

    invoke-virtual {p2, v1}, Lied;->a(S)Lied;

    .line 290
    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v1, v3, v2

    .line 291
    invoke-virtual {v1}, Liea;->b()S

    move-result v5

    invoke-virtual {p2, v5}, Lied;->a(S)Lied;

    .line 292
    invoke-virtual {v1}, Liea;->c()S

    move-result v5

    invoke-virtual {p2, v5}, Lied;->a(S)Lied;

    .line 293
    invoke-virtual {v1}, Liea;->e()I

    move-result v5

    invoke-virtual {p2, v5}, Lied;->a(I)Lied;

    .line 297
    invoke-virtual {v1}, Liea;->d()I

    move-result v5

    if-le v5, v6, :cond_1

    .line 298
    invoke-virtual {v1}, Liea;->o()I

    move-result v1

    invoke-virtual {p2, v1}, Lied;->a(I)Lied;

    .line 290
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 300
    :cond_1
    invoke-static {v1, p2}, Lidu;->a(Liea;Lied;)V

    .line 301
    invoke-virtual {v1}, Liea;->d()I

    move-result v1

    rsub-int/lit8 v5, v1, 0x4

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_0

    .line 302
    invoke-virtual {p2, v0}, Lied;->write(I)V

    .line 301
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 306
    :cond_2
    invoke-virtual {p1}, Lieb;->e()I

    move-result v1

    invoke-virtual {p2, v1}, Lied;->a(I)Lied;

    .line 307
    array-length v1, v3

    :goto_2
    if-ge v0, v1, :cond_4

    aget-object v2, v3, v0

    .line 308
    invoke-virtual {v2}, Liea;->d()I

    move-result v4

    if-le v4, v6, :cond_3

    .line 309
    invoke-static {v2, p2}, Lidu;->a(Liea;Lied;)V

    .line 307
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 312
    :cond_4
    return-void
.end method

.method private a(Lied;)V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0}, Lidq;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0}, Lidq;->a()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lied;->write([B)V

    .line 267
    :cond_0
    return-void

    .line 262
    :cond_1
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0}, Lidq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1}, Lidq;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 264
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1, v0}, Lidq;->a(I)[B

    move-result-object v1

    invoke-virtual {p1, v1}, Lied;->write([B)V

    .line 263
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/16 v6, 0x2f

    .line 328
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0, v2}, Lidq;->b(I)Lieb;

    move-result-object v0

    .line 329
    if-nez v0, :cond_0

    .line 330
    new-instance v0, Lieb;

    invoke-direct {v0, v2}, Lieb;-><init>(I)V

    .line 331
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1, v0}, Lidq;->a(Lieb;)V

    .line 333
    :cond_0
    iget-object v1, p0, Lidu;->g:Lidp;

    sget v3, Lidp;->j:I

    invoke-virtual {v1, v3}, Lidp;->i(I)Liea;

    move-result-object v1

    .line 334
    if-nez v1, :cond_1

    .line 335
    new-instance v0, Ljava/io/IOException;

    sget v1, Lidp;->j:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No definition for crucial exif tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_1
    invoke-virtual {v0, v1}, Lieb;->a(Liea;)Liea;

    .line 341
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1, v4}, Lidq;->b(I)Lieb;

    move-result-object v1

    .line 342
    if-nez v1, :cond_2

    .line 343
    new-instance v1, Lieb;

    invoke-direct {v1, v4}, Lieb;-><init>(I)V

    .line 344
    iget-object v3, p0, Lidu;->a:Lidq;

    invoke-virtual {v3, v1}, Lidq;->a(Lieb;)V

    .line 348
    :cond_2
    iget-object v3, p0, Lidu;->a:Lidq;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lidq;->b(I)Lieb;

    move-result-object v3

    .line 349
    if-eqz v3, :cond_4

    .line 350
    iget-object v3, p0, Lidu;->g:Lidp;

    sget v4, Lidp;->k:I

    invoke-virtual {v3, v4}, Lidp;->i(I)Liea;

    move-result-object v3

    .line 351
    if-nez v3, :cond_3

    .line 352
    new-instance v0, Ljava/io/IOException;

    sget v1, Lidp;->k:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No definition for crucial exif tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 355
    :cond_3
    invoke-virtual {v0, v3}, Lieb;->a(Liea;)Liea;

    .line 359
    :cond_4
    iget-object v0, p0, Lidu;->a:Lidq;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lidq;->b(I)Lieb;

    move-result-object v0

    .line 360
    if-eqz v0, :cond_6

    .line 361
    iget-object v0, p0, Lidu;->g:Lidp;

    sget v3, Lidp;->r:I

    .line 362
    invoke-virtual {v0, v3}, Lidp;->i(I)Liea;

    move-result-object v0

    .line 363
    if-nez v0, :cond_5

    .line 364
    new-instance v0, Ljava/io/IOException;

    sget v1, Lidp;->r:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No definition for crucial exif tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_5
    invoke-virtual {v1, v0}, Lieb;->a(Liea;)Liea;

    .line 370
    :cond_6
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0, v5}, Lidq;->b(I)Lieb;

    move-result-object v0

    .line 373
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1}, Lidq;->b()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 375
    if-nez v0, :cond_7

    .line 376
    new-instance v0, Lieb;

    invoke-direct {v0, v5}, Lieb;-><init>(I)V

    .line 377
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1, v0}, Lidq;->a(Lieb;)V

    .line 380
    :cond_7
    iget-object v1, p0, Lidu;->g:Lidp;

    sget v2, Lidp;->l:I

    .line 381
    invoke-virtual {v1, v2}, Lidp;->i(I)Liea;

    move-result-object v1

    .line 382
    if-nez v1, :cond_8

    .line 383
    new-instance v0, Ljava/io/IOException;

    sget v1, Lidp;->l:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No definition for crucial exif tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_8
    invoke-virtual {v0, v1}, Lieb;->a(Liea;)Liea;

    .line 388
    iget-object v1, p0, Lidu;->g:Lidp;

    sget v2, Lidp;->m:I

    .line 389
    invoke-virtual {v1, v2}, Lidp;->i(I)Liea;

    move-result-object v1

    .line 390
    if-nez v1, :cond_9

    .line 391
    new-instance v0, Ljava/io/IOException;

    sget v1, Lidp;->m:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No definition for crucial exif tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 395
    :cond_9
    iget-object v2, p0, Lidu;->a:Lidq;

    invoke-virtual {v2}, Lidq;->a()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Liea;->d(I)Z

    .line 396
    invoke-virtual {v0, v1}, Lieb;->a(Liea;)Liea;

    .line 399
    sget v1, Lidp;->e:I

    invoke-static {v1}, Lidp;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lieb;->b(S)V

    .line 400
    sget v1, Lidp;->g:I

    invoke-static {v1}, Lidp;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lieb;->b(S)V

    .line 437
    :cond_a
    :goto_0
    return-void

    .line 401
    :cond_b
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1}, Lidq;->d()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 402
    if-nez v0, :cond_c

    .line 403
    new-instance v0, Lieb;

    invoke-direct {v0, v5}, Lieb;-><init>(I)V

    .line 404
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1, v0}, Lidq;->a(Lieb;)V

    .line 406
    :cond_c
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1}, Lidq;->c()I

    move-result v1

    .line 407
    iget-object v3, p0, Lidu;->g:Lidp;

    sget v4, Lidp;->e:I

    invoke-virtual {v3, v4}, Lidp;->i(I)Liea;

    move-result-object v3

    .line 408
    if-nez v3, :cond_d

    .line 409
    new-instance v0, Ljava/io/IOException;

    sget v1, Lidp;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No definition for crucial exif tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 412
    :cond_d
    iget-object v4, p0, Lidu;->g:Lidp;

    sget v5, Lidp;->g:I

    .line 413
    invoke-virtual {v4, v5}, Lidp;->i(I)Liea;

    move-result-object v4

    .line 414
    if-nez v4, :cond_e

    .line 415
    new-instance v0, Ljava/io/IOException;

    sget v1, Lidp;->g:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No definition for crucial exif tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 418
    :cond_e
    new-array v5, v1, [J

    move v1, v2

    .line 419
    :goto_1
    iget-object v2, p0, Lidu;->a:Lidq;

    invoke-virtual {v2}, Lidq;->c()I

    move-result v2

    if-ge v1, v2, :cond_f

    .line 420
    iget-object v2, p0, Lidu;->a:Lidq;

    invoke-virtual {v2, v1}, Lidq;->a(I)[B

    move-result-object v2

    array-length v2, v2

    int-to-long v6, v2

    aput-wide v6, v5, v1

    .line 419
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 422
    :cond_f
    invoke-virtual {v4, v5}, Liea;->a([J)Z

    .line 423
    invoke-virtual {v0, v3}, Lieb;->a(Liea;)Liea;

    .line 424
    invoke-virtual {v0, v4}, Lieb;->a(Liea;)Liea;

    .line 426
    sget v1, Lidp;->l:I

    invoke-static {v1}, Lidp;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lieb;->b(S)V

    .line 427
    sget v1, Lidp;->m:I

    .line 428
    invoke-static {v1}, Lidp;->a(I)S

    move-result v1

    .line 427
    invoke-virtual {v0, v1}, Lieb;->b(S)V

    goto/16 :goto_0

    .line 429
    :cond_10
    if-eqz v0, :cond_a

    .line 431
    sget v1, Lidp;->e:I

    invoke-static {v1}, Lidp;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lieb;->b(S)V

    .line 432
    sget v1, Lidp;->g:I

    invoke-static {v1}, Lidp;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lieb;->b(S)V

    .line 433
    sget v1, Lidp;->l:I

    invoke-static {v1}, Lidp;->a(I)S

    move-result v1

    invoke-virtual {v0, v1}, Lieb;->b(S)V

    .line 434
    sget v1, Lidp;->m:I

    .line 435
    invoke-static {v1}, Lidp;->a(I)S

    move-result v1

    .line 434
    invoke-virtual {v0, v1}, Lieb;->b(S)V

    goto/16 :goto_0
.end method

.method private c()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 440
    iget-object v0, p0, Lidu;->a:Lidq;

    invoke-virtual {v0, v1}, Lidq;->b(I)Lieb;

    move-result-object v2

    .line 442
    const/16 v0, 0x8

    invoke-direct {p0, v2, v0}, Lidu;->a(Lieb;I)I

    move-result v0

    .line 443
    sget v3, Lidp;->j:I

    invoke-static {v3}, Lidp;->a(I)S

    move-result v3

    invoke-virtual {v2, v3}, Lieb;->a(S)Liea;

    move-result-object v3

    invoke-virtual {v3, v0}, Liea;->d(I)Z

    .line 445
    iget-object v3, p0, Lidu;->a:Lidq;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lidq;->b(I)Lieb;

    move-result-object v3

    .line 446
    invoke-direct {p0, v3, v0}, Lidu;->a(Lieb;I)I

    move-result v0

    .line 448
    iget-object v4, p0, Lidu;->a:Lidq;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lidq;->b(I)Lieb;

    move-result-object v4

    .line 449
    if-eqz v4, :cond_0

    .line 450
    sget v5, Lidp;->r:I

    invoke-static {v5}, Lidp;->a(I)S

    move-result v5

    invoke-virtual {v3, v5}, Lieb;->a(S)Liea;

    move-result-object v3

    .line 451
    invoke-virtual {v3, v0}, Liea;->d(I)Z

    .line 452
    invoke-direct {p0, v4, v0}, Lidu;->a(Lieb;I)I

    move-result v0

    .line 455
    :cond_0
    iget-object v3, p0, Lidu;->a:Lidq;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lidq;->b(I)Lieb;

    move-result-object v3

    .line 456
    if-eqz v3, :cond_1

    .line 457
    sget v4, Lidp;->k:I

    invoke-static {v4}, Lidp;->a(I)S

    move-result v4

    invoke-virtual {v2, v4}, Lieb;->a(S)Liea;

    move-result-object v4

    invoke-virtual {v4, v0}, Liea;->d(I)Z

    .line 458
    invoke-direct {p0, v3, v0}, Lidu;->a(Lieb;I)I

    move-result v0

    .line 461
    :cond_1
    iget-object v3, p0, Lidu;->a:Lidq;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lidq;->b(I)Lieb;

    move-result-object v3

    .line 462
    if-eqz v3, :cond_2

    .line 463
    invoke-virtual {v2, v0}, Lieb;->a(I)V

    .line 464
    invoke-direct {p0, v3, v0}, Lidu;->a(Lieb;I)I

    move-result v0

    .line 468
    :cond_2
    iget-object v2, p0, Lidu;->a:Lidq;

    invoke-virtual {v2}, Lidq;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 469
    sget v1, Lidp;->l:I

    invoke-static {v1}, Lidp;->a(I)S

    move-result v1

    invoke-virtual {v3, v1}, Lieb;->a(S)Liea;

    move-result-object v1

    .line 470
    invoke-virtual {v1, v0}, Liea;->d(I)Z

    .line 471
    iget-object v1, p0, Lidu;->a:Lidq;

    invoke-virtual {v1}, Lidq;->a()[B

    move-result-object v1

    array-length v1, v1

    add-int/2addr v1, v0

    .line 482
    :goto_0
    return v1

    .line 472
    :cond_3
    iget-object v2, p0, Lidu;->a:Lidq;

    invoke-virtual {v2}, Lidq;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 473
    iget-object v2, p0, Lidu;->a:Lidq;

    invoke-virtual {v2}, Lidq;->c()I

    move-result v2

    .line 474
    new-array v2, v2, [J

    move v6, v1

    move v1, v0

    move v0, v6

    .line 475
    :goto_1
    iget-object v4, p0, Lidu;->a:Lidq;

    invoke-virtual {v4}, Lidq;->c()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 476
    int-to-long v4, v1

    aput-wide v4, v2, v0

    .line 477
    iget-object v4, p0, Lidu;->a:Lidq;

    invoke-virtual {v4, v0}, Lidq;->a(I)[B

    move-result-object v4

    array-length v4, v4

    add-int/2addr v1, v4

    .line 475
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 479
    :cond_4
    sget v0, Lidp;->e:I

    invoke-static {v0}, Lidp;->a(I)S

    move-result v0

    invoke-virtual {v3, v0}, Lieb;->a(S)Liea;

    move-result-object v0

    invoke-virtual {v0, v2}, Liea;->a([J)Z

    goto :goto_0

    :cond_5
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lidq;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lidu;->a:Lidq;

    .line 97
    return-void
.end method

.method public write(I)V
    .locals 3

    .prologue
    .line 198
    iget-object v0, p0, Lidu;->e:[B

    const/4 v1, 0x0

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 199
    iget-object v0, p0, Lidu;->e:[B

    invoke-virtual {p0, v0}, Lidu;->write([B)V

    .line 200
    return-void
.end method

.method public write([B)V
    .locals 2

    .prologue
    .line 207
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lidu;->write([BII)V

    .line 208
    return-void
.end method

.method public write([BII)V
    .locals 6

    .prologue
    const v5, 0xffff

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 120
    :goto_0
    iget v0, p0, Lidu;->c:I

    if-gtz v0, :cond_0

    iget v0, p0, Lidu;->d:I

    if-gtz v0, :cond_0

    iget v0, p0, Lidu;->b:I

    if-eq v0, v2, :cond_b

    :cond_0
    if-lez p3, :cond_b

    .line 122
    iget v0, p0, Lidu;->c:I

    if-lez v0, :cond_1

    .line 123
    iget v0, p0, Lidu;->c:I

    if-le p3, v0, :cond_4

    iget v0, p0, Lidu;->c:I

    .line 124
    :goto_1
    sub-int/2addr p3, v0

    .line 125
    iget v1, p0, Lidu;->c:I

    sub-int/2addr v1, v0

    iput v1, p0, Lidu;->c:I

    .line 126
    add-int/2addr p2, v0

    .line 128
    :cond_1
    iget v0, p0, Lidu;->d:I

    if-lez v0, :cond_2

    .line 129
    iget v0, p0, Lidu;->d:I

    if-le p3, v0, :cond_5

    iget v0, p0, Lidu;->d:I

    .line 130
    :goto_2
    iget-object v1, p0, Lidu;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 131
    sub-int/2addr p3, v0

    .line 132
    iget v1, p0, Lidu;->d:I

    sub-int/2addr v1, v0

    iput v1, p0, Lidu;->d:I

    .line 133
    add-int/2addr p2, v0

    .line 135
    :cond_2
    if-nez p3, :cond_6

    .line 190
    :cond_3
    :goto_3
    return-void

    :cond_4
    move v0, p3

    .line 123
    goto :goto_1

    :cond_5
    move v0, p3

    .line 129
    goto :goto_2

    .line 138
    :cond_6
    iget v0, p0, Lidu;->b:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 140
    :pswitch_0
    invoke-direct {p0, v2, p1, p2, p3}, Lidu;->a(I[BII)I

    move-result v0

    .line 141
    add-int/2addr p2, v0

    .line 142
    sub-int/2addr p3, v0

    .line 143
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lt v0, v2, :cond_3

    .line 146
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 147
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    const/16 v1, -0x28

    if-eq v0, v1, :cond_7

    .line 148
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Not a valid jpeg image, cannot write exif"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :cond_7
    iget-object v0, p0, Lidu;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 151
    const/4 v0, 0x1

    iput v0, p0, Lidu;->b:I

    .line 152
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 153
    invoke-direct {p0}, Lidu;->a()V

    goto :goto_0

    .line 158
    :pswitch_1
    invoke-direct {p0, v4, p1, p2, p3}, Lidu;->a(I[BII)I

    move-result v0

    .line 159
    add-int/2addr p2, v0

    .line 160
    sub-int/2addr p3, v0

    .line 162
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-ne v0, v2, :cond_8

    .line 163
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 164
    const/16 v1, -0x27

    if-ne v0, v1, :cond_8

    .line 165
    iget-object v0, p0, Lidu;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 166
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 169
    :cond_8
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lt v0, v4, :cond_3

    .line 172
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 173
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 174
    const/16 v1, -0x1f

    if-ne v0, v1, :cond_9

    .line 175
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v5

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lidu;->c:I

    .line 176
    iput v2, p0, Lidu;->b:I

    .line 184
    :goto_4
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto/16 :goto_0

    .line 177
    :cond_9
    invoke-static {v0}, Liec;->a(S)Z

    move-result v0

    if-nez v0, :cond_a

    .line 178
    iget-object v0, p0, Lidu;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 179
    iget-object v0, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    and-int/2addr v0, v5

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lidu;->d:I

    goto :goto_4

    .line 181
    :cond_a
    iget-object v0, p0, Lidu;->out:Ljava/io/OutputStream;

    iget-object v1, p0, Lidu;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 182
    iput v2, p0, Lidu;->b:I

    goto :goto_4

    .line 187
    :cond_b
    if-lez p3, :cond_3

    .line 188
    iget-object v0, p0, Lidu;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    goto/16 :goto_3

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
