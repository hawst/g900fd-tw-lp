.class public final Lnvl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Logp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3216
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3221
    sget-object v0, Logp;->a:[Logp;

    iput-object v0, p0, Lnvl;->b:[Logp;

    .line 3216
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3242
    .line 3243
    iget-object v0, p0, Lnvl;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3244
    const/4 v0, 0x1

    iget-object v2, p0, Lnvl;->a:Ljava/lang/String;

    .line 3245
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3247
    :goto_0
    iget-object v2, p0, Lnvl;->b:[Logp;

    if-eqz v2, :cond_1

    .line 3248
    iget-object v2, p0, Lnvl;->b:[Logp;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3249
    if-eqz v4, :cond_0

    .line 3250
    const/4 v5, 0x2

    .line 3251
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3248
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3255
    :cond_1
    iget-object v1, p0, Lnvl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3256
    iput v0, p0, Lnvl;->ai:I

    .line 3257
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnvl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3265
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3266
    sparse-switch v0, :sswitch_data_0

    .line 3270
    iget-object v2, p0, Lnvl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 3271
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnvl;->ah:Ljava/util/List;

    .line 3274
    :cond_1
    iget-object v2, p0, Lnvl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3276
    :sswitch_0
    return-object p0

    .line 3281
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvl;->a:Ljava/lang/String;

    goto :goto_0

    .line 3285
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3286
    iget-object v0, p0, Lnvl;->b:[Logp;

    if-nez v0, :cond_3

    move v0, v1

    .line 3287
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Logp;

    .line 3288
    iget-object v3, p0, Lnvl;->b:[Logp;

    if-eqz v3, :cond_2

    .line 3289
    iget-object v3, p0, Lnvl;->b:[Logp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3291
    :cond_2
    iput-object v2, p0, Lnvl;->b:[Logp;

    .line 3292
    :goto_2
    iget-object v2, p0, Lnvl;->b:[Logp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 3293
    iget-object v2, p0, Lnvl;->b:[Logp;

    new-instance v3, Logp;

    invoke-direct {v3}, Logp;-><init>()V

    aput-object v3, v2, v0

    .line 3294
    iget-object v2, p0, Lnvl;->b:[Logp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3295
    invoke-virtual {p1}, Loxn;->a()I

    .line 3292
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3286
    :cond_3
    iget-object v0, p0, Lnvl;->b:[Logp;

    array-length v0, v0

    goto :goto_1

    .line 3298
    :cond_4
    iget-object v2, p0, Lnvl;->b:[Logp;

    new-instance v3, Logp;

    invoke-direct {v3}, Logp;-><init>()V

    aput-object v3, v2, v0

    .line 3299
    iget-object v2, p0, Lnvl;->b:[Logp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3266
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3226
    iget-object v0, p0, Lnvl;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3227
    const/4 v0, 0x1

    iget-object v1, p0, Lnvl;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3229
    :cond_0
    iget-object v0, p0, Lnvl;->b:[Logp;

    if-eqz v0, :cond_2

    .line 3230
    iget-object v1, p0, Lnvl;->b:[Logp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 3231
    if-eqz v3, :cond_1

    .line 3232
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 3230
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3236
    :cond_2
    iget-object v0, p0, Lnvl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3238
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3212
    invoke-virtual {p0, p1}, Lnvl;->a(Loxn;)Lnvl;

    move-result-object v0

    return-object v0
.end method
