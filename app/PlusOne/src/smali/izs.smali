.class public Lizs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lkdv;

.field private b:Lizy;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lizs;->b:Lizy;

    .line 28
    const-class v0, Lkdv;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    iput-object v0, p0, Lizs;->a:Lkdv;

    .line 29
    return-void
.end method

.method private a(Lizy;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lizs;->a:Lkdv;

    invoke-interface {v0, p1}, Lkdv;->a(Lkdc;)Lkda;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/MediaResource;

    .line 233
    if-nez v0, :cond_0

    .line 234
    new-instance v0, Lcom/google/android/libraries/social/media/MediaResource;

    iget-object v1, p0, Lizs;->a:Lkdv;

    invoke-direct {v0, v1, p1}, Lcom/google/android/libraries/social/media/MediaResource;-><init>(Lkdv;Lizy;)V

    .line 235
    iget-object v1, p0, Lizs;->a:Lkdv;

    invoke-interface {v1, v0}, Lkdv;->c(Lkda;)V

    .line 243
    :goto_0
    return-object v0

    .line 239
    :cond_0
    iget-object v1, p0, Lizs;->b:Lizy;

    invoke-virtual {p1, v1}, Lizy;->a(Lizy;)V

    .line 240
    iput-object p1, p0, Lizs;->b:Lizy;

    goto :goto_0
.end method

.method private a(Lizy;Lkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lizs;->a:Lkdv;

    invoke-interface {v0, p1}, Lkdv;->a(Lkdc;)Lkda;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/MediaResource;

    .line 217
    if-nez v0, :cond_0

    .line 218
    new-instance v0, Lcom/google/android/libraries/social/media/MediaResource;

    iget-object v1, p0, Lizs;->a:Lkdv;

    invoke-direct {v0, v1, p1}, Lcom/google/android/libraries/social/media/MediaResource;-><init>(Lkdv;Lizy;)V

    .line 224
    :goto_0
    iget-object v1, p0, Lizs;->a:Lkdv;

    invoke-interface {v1, v0, p2}, Lkdv;->a(Lkda;Lkdd;)V

    .line 225
    return-object v0

    .line 221
    :cond_0
    iget-object v1, p0, Lizs;->b:Lizy;

    invoke-virtual {p1, v1}, Lizy;->a(Lizy;)V

    .line 222
    iput-object p1, p0, Lizs;->b:Lizy;

    goto :goto_0
.end method

.method private a(Lizu;IIIILandroid/graphics/RectF;Lizo;I)Lizy;
    .locals 9

    .prologue
    .line 200
    iget-object v0, p0, Lizs;->b:Lizy;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lizs;->b:Lizy;

    .line 202
    iget-object v1, p0, Lizs;->b:Lizy;

    invoke-virtual {v1}, Lizy;->c()Lizy;

    move-result-object v1

    iput-object v1, p0, Lizs;->b:Lizy;

    .line 203
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lizy;->a(Lizy;)V

    :goto_0
    move/from16 v1, p8

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    .line 208
    invoke-virtual/range {v0 .. v8}, Lizy;->a(ILizu;IIIILandroid/graphics/RectF;Lizo;)V

    .line 209
    return-object v0

    .line 205
    :cond_0
    new-instance v0, Lizy;

    invoke-direct {v0}, Lizy;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public a(Lizu;I)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 172
    iget-object v0, p0, Lizs;->a:Lkdv;

    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    .line 175
    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 176
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    .line 178
    :goto_0
    if-eqz v0, :cond_1

    move v3, v2

    .line 179
    :goto_1
    if-eqz v0, :cond_2

    iget v4, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 181
    :goto_2
    or-int/lit16 v8, p2, 0x240

    .line 185
    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v7, v6

    invoke-direct/range {v0 .. v8}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;I)Lizy;

    move-result-object v0

    .line 187
    invoke-direct {p0, v0}, Lizs;->a(Lizy;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 176
    goto :goto_0

    .line 178
    :cond_1
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_1

    :cond_2
    move v4, v2

    .line 179
    goto :goto_2
.end method

.method public a(Lizu;III)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 163
    const/4 v2, 0x0

    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v7, v6

    move v8, p4

    invoke-direct/range {v0 .. v8}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;I)Lizy;

    move-result-object v0

    .line 165
    invoke-direct {p0, v0}, Lizs;->a(Lizy;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public a(Lizu;IIIILandroid/graphics/RectF;Lizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 1

    .prologue
    .line 71
    invoke-direct/range {p0 .. p8}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;I)Lizy;

    move-result-object v0

    .line 73
    invoke-direct {p0, v0, p9}, Lizs;->a(Lizy;Lkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public a(Lizu;IIIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 10

    .prologue
    .line 98
    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v8, p5

    move-object/from16 v9, p6

    invoke-virtual/range {v0 .. v9}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public a(Lizu;IIILizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 10

    .prologue
    .line 64
    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v8, p5

    move/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;I)Lizy;

    move-result-object v1

    .line 66
    move-object/from16 v0, p7

    invoke-direct {p0, v1, v0}, Lizs;->a(Lizy;Lkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v1

    return-object v1
.end method

.method public a(Lizu;IIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 8

    .prologue
    .line 89
    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v6, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lizs;->a(Lizu;IIILizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public a(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 6

    .prologue
    .line 52
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;ILizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public a(Lizu;ILizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 57
    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, v3

    move-object v7, p3

    move v8, p4

    invoke-direct/range {v0 .. v8}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;I)Lizy;

    move-result-object v0

    .line 59
    invoke-direct {p0, v0, p5}, Lizs;->a(Lizy;Lkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public a(Lizu;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lizs;->a(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public a(Lizu;IIII)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 129
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lizs;->a(Lizu;IIIILkdf;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lizu;IIIILkdf;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 138
    new-instance v0, Lizt;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lizt;-><init>(Lizs;Lkdf;Lizu;IIII)V

    .line 145
    invoke-virtual {v0}, Lizt;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lizu;II)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 106
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, v3

    move-object v7, v6

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;I)Lizy;

    move-result-object v0

    .line 109
    new-instance v1, Lcom/google/android/libraries/social/media/MediaResource;

    iget-object v2, p0, Lizs;->a:Lkdv;

    invoke-direct {v1, v2, v0}, Lcom/google/android/libraries/social/media/MediaResource;-><init>(Lkdv;Lizy;)V

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lkdv;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lizs;->a:Lkdv;

    return-object v0
.end method

.method public a(Lcom/google/android/libraries/social/media/MediaResource;)V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lizs;->a:Lkdv;

    invoke-interface {v0, p1}, Lkdv;->d(Lkda;)V

    .line 192
    return-void
.end method

.method public b()Landroid/graphics/Bitmap$Config;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lizs;->a:Lkdv;

    invoke-interface {v0}, Lkdv;->i()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    return-object v0
.end method

.method public b(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 6

    .prologue
    .line 81
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public b(Lizu;II)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 117
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, v3

    move-object v7, v6

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;I)Lizy;

    move-result-object v0

    .line 120
    new-instance v1, Lcom/google/android/libraries/social/media/MediaResource;

    iget-object v2, p0, Lizs;->a:Lkdv;

    invoke-direct {v1, v2, v0}, Lcom/google/android/libraries/social/media/MediaResource;-><init>(Lkdv;Lizy;)V

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/MediaResource;->getPartialDownloadFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Lizu;II)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 152
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, v3

    move-object v7, v6

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lizs;->a(Lizu;IIIILandroid/graphics/RectF;Lizo;I)Lizy;

    move-result-object v0

    .line 155
    invoke-direct {p0, v0}, Lizs;->a(Lizy;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method
