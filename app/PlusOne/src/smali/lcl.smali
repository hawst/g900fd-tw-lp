.class public final Llcl;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:[I

.field private static final b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Llcl;->a:[I

    .line 89
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Llcl;->b:[I

    return-void

    .line 70
    nop

    :array_0
    .array-data 4
        -0x80000000
        0x68
        0x67
        0x69
        0x66
        0x72
        0x7c
        0x70
        0x74
        0x71
        0x6a
        0x8b
        0xa3
        0xa4
    .end array-data

    .line 89
    :array_1
    .array-data 4
        0x68
        0x67
        0x69
        0x66
        0x72
        0x7c
        0x70
        0x74
        0x71
        0x6a
        0x8b
    .end array-data
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 455
    packed-switch p0, :pswitch_data_0

    .line 483
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 457
    :pswitch_0
    const/16 v0, 0x68

    goto :goto_0

    .line 459
    :pswitch_1
    const/16 v0, 0x67

    goto :goto_0

    .line 461
    :pswitch_2
    const/16 v0, 0x69

    goto :goto_0

    .line 463
    :pswitch_3
    const/16 v0, 0x66

    goto :goto_0

    .line 465
    :pswitch_4
    const/16 v0, 0x72

    goto :goto_0

    .line 467
    :pswitch_5
    const/16 v0, 0x7c

    goto :goto_0

    .line 469
    :pswitch_6
    const/16 v0, 0x70

    goto :goto_0

    .line 471
    :pswitch_7
    const/16 v0, 0x74

    goto :goto_0

    .line 473
    :pswitch_8
    const/16 v0, 0x71

    goto :goto_0

    .line 475
    :pswitch_9
    const/16 v0, 0x6a

    goto :goto_0

    .line 477
    :pswitch_a
    const/16 v0, 0xa3

    goto :goto_0

    .line 479
    :pswitch_b
    const/16 v0, 0x8b

    goto :goto_0

    .line 481
    :pswitch_c
    const/16 v0, 0xa4

    goto :goto_0

    .line 455
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_a
        :pswitch_c
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;ILpxv;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 156
    if-nez p2, :cond_0

    .line 168
    :goto_0
    return-object v3

    :cond_0
    move v1, v2

    move-object v0, v3

    .line 161
    :goto_1
    sget-object v4, Llcl;->a:[I

    const/16 v4, 0xe

    if-ge v1, v4, :cond_9

    .line 162
    iget v0, p2, Lpxv;->a:I

    sget-object v4, Llcl;->a:[I

    aget v4, v4, v1

    if-eq v0, v4, :cond_1

    move-object v4, v3

    .line 163
    :goto_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 164
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v4

    goto :goto_1

    .line 162
    :cond_1
    const-class v0, Llcg;

    invoke-static {p0, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llcg;

    if-eqz v0, :cond_b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Llcg;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Llcf;

    if-eqz v0, :cond_b

    invoke-interface {v0}, Llcf;->d()Ljava/lang/Integer;

    move-result-object v4

    :goto_3
    if-nez v4, :cond_2

    packed-switch v1, :pswitch_data_0

    :cond_2
    move-object v0, v4

    :goto_4
    if-nez v0, :cond_3

    const-string v0, "StreamPromosData"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x1f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unknown promo type! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    goto :goto_2

    :pswitch_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_4

    :pswitch_1
    sget-object v0, Lnlw;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lnlw;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_4

    :pswitch_2
    sget-object v0, Lnmf;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lnmf;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_4

    :pswitch_3
    sget-object v0, Lnmb;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnmb;

    if-eqz v0, :cond_2

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_4

    :pswitch_4
    sget-object v0, Lnnk;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lnnk;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_5
    sget-object v0, Lnmo;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lnmo;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_6
    sget-object v0, Lnrv;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lnrv;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_7
    sget-object v0, Lnrr;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnrr;

    if-eqz v0, :cond_2

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_8
    sget-object v0, Lmqv;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lmqv;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_9
    sget-object v0, Lmrc;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lmrc;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_a
    sget-object v0, Lpyc;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lpyc;->a:Loxr;

    invoke-virtual {p2, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_4

    :cond_3
    const-string v4, "promo"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "type"

    invoke-static {v7, v4}, Llbk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    const-string v8, "subtype"

    invoke-static {v8, v5}, Llbk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    const-string v9, "ID"

    invoke-static {v9, v0}, Llbk;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-nez v7, :cond_4

    if-eqz v8, :cond_8

    :cond_4
    const-string v10, "~typeprefix~"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v7, :cond_5

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v4, "~"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v8, :cond_6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const-string v4, "~"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v9, :cond_7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    :goto_5
    invoke-static {v6}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    :cond_8
    if-eqz v9, :cond_7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_9
    move-object v4, v0

    :cond_a
    move-object v3, v4

    .line 168
    goto/16 :goto_0

    :cond_b
    move-object v4, v3

    goto/16 :goto_3

    .line 162
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_2
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/ArrayList",
            "<",
            "Lpxv;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    const/4 v2, 0x0

    .line 302
    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    :cond_0
    return-void

    .line 307
    :cond_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 308
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    .line 309
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxv;

    .line 310
    invoke-static {p0, p1, v0}, Llcl;->a(Landroid/content/Context;ILpxv;)Ljava/lang/String;

    move-result-object v5

    .line 312
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 313
    const-string v0, "StreamPromosData"

    const/4 v5, 0x6

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 314
    const-string v0, "StreamPromosData"

    const-string v5, ">>>>> could not determine activity id"

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_2
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 319
    :cond_3
    const-string v6, "StreamPromosData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 320
    const-string v6, ">>>>> promo internal activity id: "

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 323
    :cond_4
    :goto_2
    const-string v6, "unique_activity_id"

    invoke-virtual {v3, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const-string v6, "activity_id"

    invoke-virtual {v3, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const-string v5, "author_id"

    const-string v6, "~promo"

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v5, "total_comment_count"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 327
    const-string v5, "created"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 328
    const-string v5, "modified"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 329
    const-string v5, "data_state"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 330
    const-string v5, "content_flags"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 331
    const-string v5, "activity_flags"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 332
    const-string v5, "payload"

    invoke-static {p0, v0}, Llcl;->a(Landroid/content/Context;Lpxv;)[B

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 334
    const-string v0, "activities"

    const-string v5, "unique_activity_id"

    const/4 v6, 0x5

    invoke-virtual {p2, v0, v5, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 336
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    goto :goto_1

    .line 320
    :cond_5
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 136
    const-string v0, "promo"

    invoke-static {p0}, Llbk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;Lpxv;)[B
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 341
    move v2, v3

    move-object v0, v4

    .line 342
    :goto_0
    sget-object v1, Llcl;->a:[I

    const/16 v1, 0xe

    if-ge v2, v1, :cond_2

    .line 343
    iget v0, p1, Lpxv;->a:I

    sget-object v1, Llcl;->a:[I

    aget v1, v1, v2

    if-eq v0, v1, :cond_0

    move-object v1, v4

    .line 344
    :goto_1
    if-nez v1, :cond_3

    .line 345
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v1

    goto :goto_0

    .line 343
    :cond_0
    packed-switch v2, :pswitch_data_0

    move-object v0, v4

    :goto_2
    move-object v1, v0

    goto :goto_1

    :pswitch_0
    move-object v1, v4

    goto :goto_1

    :pswitch_1
    new-instance v1, Llbx;

    sget-object v0, Lnlw;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnlw;

    invoke-direct {v1, v0}, Llbx;-><init>(Lnlw;)V

    invoke-static {v1}, Llbx;->a(Llbx;)[B

    move-result-object v0

    goto :goto_2

    :pswitch_2
    new-instance v5, Llbx;

    sget-object v0, Lnmf;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnmf;

    sget-object v1, Lnmd;->a:Loxr;

    invoke-virtual {p1, v1}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnmd;

    invoke-direct {v5, p0, v0, v1}, Llbx;-><init>(Landroid/content/Context;Lnmf;Lnmd;)V

    invoke-static {v5}, Llbx;->a(Llbx;)[B

    move-result-object v0

    goto :goto_2

    :pswitch_3
    new-instance v5, Llbx;

    sget-object v0, Lnmb;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnmb;

    sget-object v1, Lnly;->a:Loxr;

    invoke-virtual {p1, v1}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnly;

    invoke-direct {v5, p0, v0, v1}, Llbx;-><init>(Landroid/content/Context;Lnmb;Lnly;)V

    invoke-static {v5}, Llbx;->a(Llbx;)[B

    move-result-object v0

    goto :goto_2

    :pswitch_4
    new-instance v5, Llbx;

    sget-object v0, Lnmf;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnmf;

    sget-object v1, Lnmd;->a:Loxr;

    invoke-virtual {p1, v1}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnmd;

    invoke-direct {v5, p0, v0, v1}, Llbx;-><init>(Landroid/content/Context;Lnmf;Lnmd;)V

    invoke-static {v5}, Llbx;->a(Llbx;)[B

    move-result-object v0

    goto :goto_2

    :pswitch_5
    sget-object v0, Lnnk;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnnk;

    new-instance v1, Llby;

    invoke-direct {v1, v0}, Llby;-><init>(Lnnk;)V

    invoke-static {v1}, Llby;->a(Llby;)[B

    move-result-object v0

    goto :goto_2

    :pswitch_6
    sget-object v0, Lnmo;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnmo;

    new-instance v1, Llcc;

    invoke-direct {v1, v0}, Llcc;-><init>(Lnmo;)V

    invoke-static {v1}, Llcc;->a(Llcc;)[B

    move-result-object v0

    goto/16 :goto_2

    :pswitch_7
    sget-object v0, Lnrv;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnrv;

    new-instance v1, Llca;

    invoke-direct {v1, v0}, Llca;-><init>(Lnrv;)V

    invoke-static {v1}, Llca;->a(Llca;)[B

    move-result-object v0

    goto/16 :goto_2

    :pswitch_8
    sget-object v0, Lnrr;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnrr;

    new-instance v1, Llca;

    invoke-direct {v1, v0}, Llca;-><init>(Lnrr;)V

    invoke-static {v1}, Llca;->a(Llca;)[B

    move-result-object v0

    goto/16 :goto_2

    :pswitch_9
    sget-object v0, Lmqv;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmqv;

    new-instance v1, Lide;

    invoke-direct {v1, v0}, Lide;-><init>(Lmqv;)V

    invoke-static {v1}, Lide;->a(Lide;)[B

    move-result-object v0

    goto/16 :goto_2

    :pswitch_a
    new-instance v5, Llbx;

    sget-object v0, Lnmf;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnmf;

    sget-object v1, Lnmd;->a:Loxr;

    invoke-virtual {p1, v1}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnmd;

    invoke-direct {v5, p0, v0, v1}, Llbx;-><init>(Landroid/content/Context;Lnmf;Lnmd;)V

    invoke-static {v5}, Llbx;->a(Llbx;)[B

    move-result-object v0

    goto/16 :goto_2

    :pswitch_b
    new-instance v1, Llcb;

    sget-object v0, Lmrc;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmrc;

    invoke-direct {v1, v0}, Llcb;-><init>(Lmrc;)V

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x100

    invoke-direct {v5, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v6, Ljava/io/DataOutputStream;

    invoke-direct {v6, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v0, v1, Llcb;->a:[Ljava/lang/String;

    array-length v7, v0

    invoke-virtual {v6, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v0, v3

    :goto_3
    if-ge v0, v7, :cond_1

    iget-object v8, v1, Llcb;->a:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-static {v6, v8}, Llcb;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v8, v1, Llcb;->b:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-static {v6, v8}, Llcb;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v8, v1, Llcb;->c:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-static {v6, v8}, Llcb;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_1
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v6}, Ljava/io/DataOutputStream;->close()V

    goto/16 :goto_2

    :pswitch_c
    new-instance v1, Llcd;

    sget-object v0, Lpyc;->a:Loxr;

    invoke-virtual {p1, v0}, Lpxv;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpyc;

    invoke-direct {v1, v0}, Llcd;-><init>(Lpyc;)V

    invoke-static {v1}, Llcd;->a(Llcd;)[B

    move-result-object v0

    goto/16 :goto_2

    :cond_2
    move-object v1, v0

    .line 348
    :cond_3
    return-object v1

    .line 343
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;I)[I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 104
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 106
    sget-object v0, Llcl;->b:[I

    move v0, v2

    :goto_0
    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 107
    sget-object v1, Llcl;->b:[I

    aget v1, v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    :cond_0
    const-class v0, Llcg;

    invoke-static {p0, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llcg;

    .line 113
    if-eqz v0, :cond_2

    .line 114
    invoke-virtual {v0}, Llcg;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 115
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_2

    .line 116
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Llcg;->a(Ljava/lang/Object;)Llno;

    move-result-object v1

    check-cast v1, Llcf;

    .line 117
    invoke-interface {v1}, Llcf;->c()Ljava/lang/Integer;

    move-result-object v1

    .line 118
    if-eqz v1, :cond_1

    .line 119
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 124
    :cond_2
    const-class v0, Lkzl;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    invoke-interface {v0, p1}, Lkzl;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 125
    const/16 v0, 0xa3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [I

    .line 129
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_4

    .line 130
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 129
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 132
    :cond_4
    return-object v3
.end method

.method public static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 140
    invoke-static {p0}, Llcl;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 151
    :cond_0
    :goto_0
    return v0

    .line 145
    :cond_1
    :try_start_0
    invoke-static {p0}, Llbk;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 146
    if-ltz v1, :cond_0

    const/16 v2, 0xe

    if-gt v1, v2, :cond_0

    move v0, v1

    .line 149
    goto :goto_0

    .line 151
    :catch_0
    move-exception v1

    goto :goto_0
.end method
