.class public final Lekm;
.super Leku;
.source "PG"


# instance fields
.field private N:Leky;

.field private O:Z

.field private final ad:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Leku;-><init>()V

    .line 42
    new-instance v0, Lekn;

    invoke-direct {v0, p0}, Lekn;-><init>(Lekm;)V

    iput-object v0, p0, Lekm;->ad:Lbc;

    .line 72
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lhmw;->r:Lhmw;

    return-object v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 127
    const/16 v0, 0x3e

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lekm;->N:Leky;

    invoke-virtual {p0, v1}, Lekm;->a(Landroid/widget/ListAdapter;)V

    .line 92
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekm;->X:Z

    .line 79
    new-instance v0, Leky;

    invoke-direct {v0, p0}, Leky;-><init>(Leku;)V

    iput-object v0, p0, Lekm;->N:Leky;

    .line 80
    return-void
.end method

.method public a(Lcom/google/android/apps/plus/views/PeopleListRowView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekm;->O:Z

    .line 160
    invoke-super {p0, p1, p2}, Leku;->a(Lcom/google/android/apps/plus/views/PeopleListRowView;Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 105
    const v0, 0x7f0a09b9

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 106
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lekm;->N:Leky;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Leky;->a(Ljava/util/ArrayList;I)V

    .line 98
    iget-object v0, p0, Lekm;->N:Leky;

    invoke-virtual {v0}, Leky;->notifyDataSetChanged()V

    .line 99
    invoke-virtual {p0}, Lekm;->x()Landroid/view/View;

    invoke-virtual {p0}, Lekm;->aa()V

    .line 100
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected am_()I
    .locals 1

    .prologue
    .line 84
    const v0, 0x7f040164

    return v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 132
    const/16 v0, 0x7c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 143
    iget-boolean v0, p0, Lekm;->O:Z

    if-eqz v0, :cond_0

    .line 144
    sget-object v0, Lhmv;->ca:Lhmv;

    .line 145
    const/4 v1, 0x0

    iput-boolean v1, p0, Lekm;->O:Z

    move-object v1, v0

    .line 149
    :goto_0
    iget-object v0, p0, Lekm;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 150
    iget-object v0, p0, Lekm;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lekm;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 152
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 150
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 154
    invoke-super {p0, p1}, Leku;->c(Landroid/view/View;)V

    .line 155
    return-void

    .line 147
    :cond_0
    sget-object v0, Lhmv;->bZ:Lhmv;

    move-object v1, v0

    goto :goto_0
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekm;->R:Z

    .line 116
    invoke-virtual {p0}, Lekm;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lekm;->ad:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 117
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekm;->R:Z

    .line 122
    invoke-virtual {p0}, Lekm;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lekm;->ad:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 123
    return-void
.end method
