.class public final Lffr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lohh;",
            ">;"
        }
    .end annotation
.end field

.field private g:J

.field private h:Z

.field private final i:Lkfp;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "times_contacted"

    aput-object v1, v0, v2

    const-string v1, "last_time_contacted"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    const-string v1, "data_set"

    aput-object v1, v0, v5

    sput-object v0, Lffr;->a:[Ljava/lang/String;

    .line 50
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "times_contacted"

    aput-object v1, v0, v2

    const-string v1, "last_time_contacted"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    sput-object v0, Lffr;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILkfp;)V
    .locals 2

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lffr;->g:J

    .line 116
    iput-object p1, p0, Lffr;->c:Landroid/content/Context;

    .line 117
    iput p2, p0, Lffr;->d:I

    .line 118
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 119
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffr;->e:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lffr;->f:Ljava/util/List;

    .line 121
    iput-object p3, p0, Lffr;->i:Lkfp;

    .line 122
    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    .line 218
    :cond_0
    :goto_0
    iget-object v0, p0, Lffr;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 219
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lffr;->g:J

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lffr;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x14

    if-ge v0, v2, :cond_2

    iget-object v0, p0, Lffr;->f:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lohh;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-wide v2, p0, Lffr;->g:J

    iget-object v4, v0, Lohh;->c:Lohi;

    iget-object v4, v4, Lohi;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    iget-object v0, v0, Lohh;->c:Lohi;

    iget-object v0, v0, Lohi;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lffr;->g:J

    goto :goto_1

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lohh;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lohh;

    iget-object v0, p0, Lffr;->i:Lkfp;

    const-string v1, "ContactsStatsSync:PartialUpload"

    invoke-virtual {v0, v1}, Lkfp;->c(Ljava/lang/String;)V

    new-instance v0, Ldnr;

    iget-object v1, p0, Lffr;->c:Landroid/content/Context;

    new-instance v2, Lkfo;

    iget-object v3, p0, Lffr;->c:Landroid/content/Context;

    iget v4, p0, Lffr;->d:I

    iget-object v6, p0, Lffr;->i:Lkfp;

    invoke-direct {v2, v3, v4, v6}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    iget v3, p0, Lffr;->d:I

    iget-object v4, p0, Lffr;->c:Landroid/content/Context;

    invoke-static {v4}, Lfug;->a(Landroid/content/Context;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Ldnr;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;[Lohh;I)V

    invoke-virtual {v0}, Ldnr;->l()V

    iget-object v1, p0, Lffr;->i:Lkfp;

    invoke-virtual {v1}, Lkfp;->f()V

    invoke-virtual {v0}, Ldnr;->t()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "ContactsStatsSync"

    invoke-virtual {v0, v1}, Ldnr;->d(Ljava/lang/String;)V

    .line 221
    :cond_3
    return-void

    .line 219
    :cond_4
    iget v0, p0, Lffr;->d:I

    iget-object v1, p0, Lffr;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Ldhv;->e(Landroid/content/Context;I)V

    iget-object v1, p0, Lffr;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Ldhv;->f(Landroid/content/Context;I)J

    move-result-wide v2

    iget-wide v4, p0, Lffr;->g:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    iget-boolean v1, p0, Lffr;->h:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lffr;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_5
    iget-object v1, p0, Lffr;->c:Landroid/content/Context;

    iget-wide v2, p0, Lffr;->g:J

    invoke-static {v1, v0, v2, v3}, Ldhv;->a(Landroid/content/Context;IJ)V

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 96
    new-instance v11, Ldnr;

    new-instance v0, Lkfo;

    const/4 v4, 0x1

    move-object v1, p0

    move v2, p1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkfo;-><init>(Landroid/content/Context;ILkey;ZLkfg;)V

    .line 105
    invoke-static {p0}, Lfug;->a(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x4

    move-object v4, v11

    move-object v5, p0

    move-object v6, v0

    move v7, p1

    move-object v9, v3

    invoke-direct/range {v4 .. v10}, Ldnr;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;[Lohh;I)V

    .line 108
    invoke-virtual {v11}, Ldnr;->l()V

    .line 110
    return-void
.end method

.method public static a(Landroid/content/Context;ILkfp;)V
    .locals 18

    .prologue
    .line 79
    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 90
    :goto_0
    return-void

    .line 82
    :cond_0
    new-instance v10, Lffr;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v10, v0, v1, v2}, Lffr;-><init>(Landroid/content/Context;ILkfp;)V

    .line 87
    sget-object v4, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "account_name"

    iget-object v6, v10, Lffr;->e:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "account_type"

    const-string v6, "com.google"

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    const-string v7, "times_contacted > 0"

    iget-object v4, v10, Lffr;->c:Landroid/content/Context;

    iget v6, v10, Lffr;->d:I

    invoke-static {v4, v6}, Ldhv;->f(Landroid/content/Context;I)J

    move-result-wide v12

    const-wide/16 v8, 0x0

    cmp-long v4, v12, v8

    if-gtz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    iput-boolean v4, v10, Lffr;->h:Z

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xe

    if-ge v4, v6, :cond_3

    sget-object v6, Lffr;->b:[Ljava/lang/String;

    :goto_2
    :try_start_0
    iget-object v4, v10, Lffr;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v8, 0x0

    const-string v9, "last_time_contacted"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-boolean v6, v10, Lffr;->h:Z

    if-eqz v6, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    const-wide/16 v14, 0x2

    div-long/2addr v8, v14

    sub-long/2addr v4, v8

    :cond_1
    :goto_3
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_7

    new-instance v8, Lohh;

    invoke-direct {v8}, Lohh;-><init>()V

    new-instance v6, Lohp;

    invoke-direct {v6}, Lohp;-><init>()V

    iput-object v6, v8, Lohh;->b:Lohp;

    new-instance v6, Lohi;

    invoke-direct {v6}, Lohi;-><init>()V

    iput-object v6, v8, Lohh;->c:Lohi;

    iget-object v6, v8, Lohh;->c:Lohi;

    const/4 v9, 0x0

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iput-object v9, v6, Lohi;->a:Ljava/lang/Integer;

    iget-object v6, v8, Lohh;->c:Lohi;

    const/4 v9, 0x1

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iput-object v9, v6, Lohi;->b:Ljava/lang/Long;

    iget-object v6, v8, Lohh;->c:Lohi;

    iget-object v6, v6, Lohi;->b:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v6, v14, v16

    if-gtz v6, :cond_4

    iget-object v6, v8, Lohh;->c:Lohi;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iput-object v9, v6, Lohi;->b:Ljava/lang/Long;

    :goto_4
    const/4 v6, 0x2

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7}, Landroid/database/Cursor;->getColumnCount()I

    move-result v6

    const/4 v11, 0x3

    if-le v6, v11, :cond_5

    const/4 v6, 0x3

    invoke-interface {v7, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_5

    const-string v6, "plus"

    const/4 v11, 0x3

    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    const/4 v6, 0x1

    :goto_5
    if-eqz v6, :cond_6

    iget-object v6, v8, Lohh;->b:Lohp;

    iput-object v9, v6, Lohp;->d:Ljava/lang/String;

    :goto_6
    iget-object v6, v10, Lffr;->f:Ljava/util/List;

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v4

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_3
    sget-object v6, Lffr;->a:[Ljava/lang/String;

    goto/16 :goto_2

    :catch_0
    move-exception v4

    const-string v5, "ContactsStatsSync"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1d

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Query on RawContacts failed. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :goto_7
    invoke-direct {v10}, Lffr;->a()V

    goto/16 :goto_0

    .line 87
    :cond_4
    :try_start_2
    iget-object v6, v8, Lohh;->c:Lohi;

    iget-object v6, v6, Lohi;->b:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    cmp-long v6, v14, v12

    if-lez v6, :cond_1

    goto :goto_4

    :cond_5
    const/4 v6, 0x0

    goto :goto_5

    :cond_6
    iget-object v6, v8, Lohh;->b:Lohp;

    iput-object v9, v6, Lohp;->c:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    :cond_7
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_7
.end method
