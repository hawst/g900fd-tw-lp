.class public final Llcr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhuk;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Llcr;-><init>(Landroid/content/Context;Z)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-direct {p0, p1, p2, p3}, Llcr;->a(Landroid/content/Context;II)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-boolean p2, p0, Llcr;->g:Z

    .line 33
    invoke-static {p1}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 34
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {p0, p1, v1, v0}, Llcr;->a(Landroid/content/Context;II)V

    .line 35
    return-void
.end method

.method private a(Landroid/content/Context;II)V
    .locals 5

    .prologue
    const v4, 0x7f0d0185

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 42
    iput p2, p0, Llcr;->b:I

    .line 43
    iput p3, p0, Llcr;->c:I

    .line 44
    iget-boolean v0, p0, Llcr;->g:Z

    if-eqz v0, :cond_0

    .line 45
    iput v3, p0, Llcr;->a:I

    .line 46
    iput v1, p0, Llcr;->d:I

    .line 47
    iget v0, p0, Llcr;->b:I

    iput v0, p0, Llcr;->e:I

    .line 48
    iput v1, p0, Llcr;->f:I

    .line 65
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 52
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Llcr;->f:I

    .line 53
    iget v1, p0, Llcr;->b:I

    iget v2, p0, Llcr;->f:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, p0, Llcr;->b:I

    .line 55
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Llcr;->d:I

    .line 56
    const v1, 0x7f0d0186

    .line 57
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 58
    iget v1, p0, Llcr;->b:I

    sub-int/2addr v1, v0

    iget v2, p0, Llcr;->d:I

    add-int/2addr v2, v0

    div-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 60
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Llcr;->a:I

    .line 62
    iget v1, p0, Llcr;->b:I

    if-ge v1, v0, :cond_1

    iget v0, p0, Llcr;->b:I

    :goto_1
    iput v0, p0, Llcr;->e:I

    goto :goto_0

    :cond_1
    iget v0, p0, Llcr;->b:I

    iget v1, p0, Llcr;->a:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Llcr;->d:I

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    iget v1, p0, Llcr;->a:I

    div-int/2addr v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Llcr;->b:I

    return v0
.end method

.method public a(I)I
    .locals 2

    .prologue
    .line 84
    iget-boolean v0, p0, Llcr;->g:Z

    if-eqz v0, :cond_0

    .line 85
    iget v0, p0, Llcr;->b:I

    .line 87
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Llcr;->e:I

    iget v1, p0, Llcr;->d:I

    add-int/2addr v0, v1

    mul-int/2addr v0, p1

    iget v1, p0, Llcr;->d:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public b(I)I
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 69
    iget-boolean v1, p0, Llcr;->g:Z

    if-eqz v1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    iget v1, p0, Llcr;->e:I

    iget v2, p0, Llcr;->d:I

    add-int/2addr v1, v2

    iget v2, p0, Llcr;->a:I

    mul-int/2addr v1, v2

    iget v2, p0, Llcr;->d:I

    sub-int v2, v1, v2

    .line 73
    iget v1, p0, Llcr;->a:I

    :goto_1
    const/4 v3, 0x2

    if-lt v1, v3, :cond_0

    .line 74
    if-le p1, v2, :cond_2

    move v0, v1

    .line 75
    goto :goto_0

    .line 77
    :cond_2
    iget v3, p0, Llcr;->e:I

    iget v4, p0, Llcr;->d:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    .line 73
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method
