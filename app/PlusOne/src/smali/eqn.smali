.class final Leqn;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Leql;


# direct methods
.method constructor <init>(Leql;)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 441
    iput-object p1, p0, Leqn;->a:Leql;

    .line 442
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 446
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 470
    :cond_0
    :goto_0
    return-object v7

    .line 450
    :cond_1
    iget-object v0, p0, Leqn;->a:Leql;

    invoke-virtual {v0}, Leql;->n()Lz;

    move-result-object v1

    .line 451
    if-eqz v1, :cond_0

    .line 455
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 456
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v0, "_id IN ("

    invoke-direct {v3, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 458
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_2

    .line 459
    aget-object v4, p1, v0

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 460
    const-string v6, "?,"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 461
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 463
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    .line 464
    add-int/lit8 v4, v0, -0x1

    const-string v5, ")"

    invoke-virtual {v3, v4, v0, v5}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 466
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 467
    invoke-static {v1}, Lhqv;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 468
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 467
    invoke-virtual {v4, v1, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 475
    iget-object v0, p0, Leqn;->a:Leql;

    invoke-virtual {v0}, Leql;->p()Lae;

    move-result-object v0

    const-string v1, "dialog_pending"

    .line 476
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 477
    if-eqz v0, :cond_0

    .line 478
    invoke-virtual {v0}, Lt;->a()V

    .line 480
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 437
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Leqn;->a([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 437
    invoke-virtual {p0}, Leqn;->a()V

    return-void
.end method
