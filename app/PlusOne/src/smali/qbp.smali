.class public final Lqbp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lqbh;

.field public b:Lqbb;

.field public c:Lqba;

.field public d:Lqbm;

.field public e:Lqax;

.field public f:Lqay;

.field public g:Lqbd;

.field public h:I

.field private i:Lqbn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 119
    invoke-direct {p0}, Loxq;-><init>()V

    .line 122
    iput-object v0, p0, Lqbp;->a:Lqbh;

    .line 125
    iput-object v0, p0, Lqbp;->b:Lqbb;

    .line 128
    iput-object v0, p0, Lqbp;->i:Lqbn;

    .line 131
    iput-object v0, p0, Lqbp;->c:Lqba;

    .line 134
    iput-object v0, p0, Lqbp;->d:Lqbm;

    .line 137
    iput-object v0, p0, Lqbp;->e:Lqax;

    .line 140
    iput-object v0, p0, Lqbp;->f:Lqay;

    .line 143
    iput-object v0, p0, Lqbp;->g:Lqbd;

    .line 146
    const/high16 v0, -0x80000000

    iput v0, p0, Lqbp;->h:I

    .line 119
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 184
    const/4 v0, 0x0

    .line 185
    iget-object v1, p0, Lqbp;->a:Lqbh;

    if-eqz v1, :cond_0

    .line 186
    const/4 v0, 0x1

    iget-object v1, p0, Lqbp;->a:Lqbh;

    .line 187
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 189
    :cond_0
    iget-object v1, p0, Lqbp;->b:Lqbb;

    if-eqz v1, :cond_1

    .line 190
    const/4 v1, 0x2

    iget-object v2, p0, Lqbp;->b:Lqbb;

    .line 191
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_1
    iget-object v1, p0, Lqbp;->i:Lqbn;

    if-eqz v1, :cond_2

    .line 194
    const/4 v1, 0x3

    iget-object v2, p0, Lqbp;->i:Lqbn;

    .line 195
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    :cond_2
    iget-object v1, p0, Lqbp;->c:Lqba;

    if-eqz v1, :cond_3

    .line 198
    const/4 v1, 0x4

    iget-object v2, p0, Lqbp;->c:Lqba;

    .line 199
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    :cond_3
    iget-object v1, p0, Lqbp;->d:Lqbm;

    if-eqz v1, :cond_4

    .line 202
    const/4 v1, 0x5

    iget-object v2, p0, Lqbp;->d:Lqbm;

    .line 203
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    :cond_4
    iget-object v1, p0, Lqbp;->e:Lqax;

    if-eqz v1, :cond_5

    .line 206
    const/4 v1, 0x6

    iget-object v2, p0, Lqbp;->e:Lqax;

    .line 207
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 209
    :cond_5
    iget-object v1, p0, Lqbp;->f:Lqay;

    if-eqz v1, :cond_6

    .line 210
    const/4 v1, 0x7

    iget-object v2, p0, Lqbp;->f:Lqay;

    .line 211
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    :cond_6
    iget-object v1, p0, Lqbp;->g:Lqbd;

    if-eqz v1, :cond_7

    .line 214
    const/16 v1, 0x8

    iget-object v2, p0, Lqbp;->g:Lqbd;

    .line 215
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    :cond_7
    iget v1, p0, Lqbp;->h:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_8

    .line 218
    const/16 v1, 0x9

    iget v2, p0, Lqbp;->h:I

    .line 219
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    :cond_8
    iget-object v1, p0, Lqbp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    iput v0, p0, Lqbp;->ai:I

    .line 223
    return v0
.end method

.method public a(Loxn;)Lqbp;
    .locals 2

    .prologue
    .line 231
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 232
    sparse-switch v0, :sswitch_data_0

    .line 236
    iget-object v1, p0, Lqbp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 237
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbp;->ah:Ljava/util/List;

    .line 240
    :cond_1
    iget-object v1, p0, Lqbp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    :sswitch_0
    return-object p0

    .line 247
    :sswitch_1
    iget-object v0, p0, Lqbp;->a:Lqbh;

    if-nez v0, :cond_2

    .line 248
    new-instance v0, Lqbh;

    invoke-direct {v0}, Lqbh;-><init>()V

    iput-object v0, p0, Lqbp;->a:Lqbh;

    .line 250
    :cond_2
    iget-object v0, p0, Lqbp;->a:Lqbh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 254
    :sswitch_2
    iget-object v0, p0, Lqbp;->b:Lqbb;

    if-nez v0, :cond_3

    .line 255
    new-instance v0, Lqbb;

    invoke-direct {v0}, Lqbb;-><init>()V

    iput-object v0, p0, Lqbp;->b:Lqbb;

    .line 257
    :cond_3
    iget-object v0, p0, Lqbp;->b:Lqbb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 261
    :sswitch_3
    iget-object v0, p0, Lqbp;->i:Lqbn;

    if-nez v0, :cond_4

    .line 262
    new-instance v0, Lqbn;

    invoke-direct {v0}, Lqbn;-><init>()V

    iput-object v0, p0, Lqbp;->i:Lqbn;

    .line 264
    :cond_4
    iget-object v0, p0, Lqbp;->i:Lqbn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 268
    :sswitch_4
    iget-object v0, p0, Lqbp;->c:Lqba;

    if-nez v0, :cond_5

    .line 269
    new-instance v0, Lqba;

    invoke-direct {v0}, Lqba;-><init>()V

    iput-object v0, p0, Lqbp;->c:Lqba;

    .line 271
    :cond_5
    iget-object v0, p0, Lqbp;->c:Lqba;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 275
    :sswitch_5
    iget-object v0, p0, Lqbp;->d:Lqbm;

    if-nez v0, :cond_6

    .line 276
    new-instance v0, Lqbm;

    invoke-direct {v0}, Lqbm;-><init>()V

    iput-object v0, p0, Lqbp;->d:Lqbm;

    .line 278
    :cond_6
    iget-object v0, p0, Lqbp;->d:Lqbm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 282
    :sswitch_6
    iget-object v0, p0, Lqbp;->e:Lqax;

    if-nez v0, :cond_7

    .line 283
    new-instance v0, Lqax;

    invoke-direct {v0}, Lqax;-><init>()V

    iput-object v0, p0, Lqbp;->e:Lqax;

    .line 285
    :cond_7
    iget-object v0, p0, Lqbp;->e:Lqax;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 289
    :sswitch_7
    iget-object v0, p0, Lqbp;->f:Lqay;

    if-nez v0, :cond_8

    .line 290
    new-instance v0, Lqay;

    invoke-direct {v0}, Lqay;-><init>()V

    iput-object v0, p0, Lqbp;->f:Lqay;

    .line 292
    :cond_8
    iget-object v0, p0, Lqbp;->f:Lqay;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 296
    :sswitch_8
    iget-object v0, p0, Lqbp;->g:Lqbd;

    if-nez v0, :cond_9

    .line 297
    new-instance v0, Lqbd;

    invoke-direct {v0}, Lqbd;-><init>()V

    iput-object v0, p0, Lqbp;->g:Lqbd;

    .line 299
    :cond_9
    iget-object v0, p0, Lqbp;->g:Lqbd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 303
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 304
    if-eqz v0, :cond_a

    const/4 v1, 0x1

    if-eq v0, v1, :cond_a

    const/4 v1, 0x2

    if-eq v0, v1, :cond_a

    const/4 v1, 0x3

    if-eq v0, v1, :cond_a

    const/4 v1, 0x4

    if-ne v0, v1, :cond_b

    .line 309
    :cond_a
    iput v0, p0, Lqbp;->h:I

    goto/16 :goto_0

    .line 311
    :cond_b
    const/4 v0, 0x0

    iput v0, p0, Lqbp;->h:I

    goto/16 :goto_0

    .line 232
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lqbp;->a:Lqbh;

    if-eqz v0, :cond_0

    .line 152
    const/4 v0, 0x1

    iget-object v1, p0, Lqbp;->a:Lqbh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 154
    :cond_0
    iget-object v0, p0, Lqbp;->b:Lqbb;

    if-eqz v0, :cond_1

    .line 155
    const/4 v0, 0x2

    iget-object v1, p0, Lqbp;->b:Lqbb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 157
    :cond_1
    iget-object v0, p0, Lqbp;->i:Lqbn;

    if-eqz v0, :cond_2

    .line 158
    const/4 v0, 0x3

    iget-object v1, p0, Lqbp;->i:Lqbn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 160
    :cond_2
    iget-object v0, p0, Lqbp;->c:Lqba;

    if-eqz v0, :cond_3

    .line 161
    const/4 v0, 0x4

    iget-object v1, p0, Lqbp;->c:Lqba;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 163
    :cond_3
    iget-object v0, p0, Lqbp;->d:Lqbm;

    if-eqz v0, :cond_4

    .line 164
    const/4 v0, 0x5

    iget-object v1, p0, Lqbp;->d:Lqbm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 166
    :cond_4
    iget-object v0, p0, Lqbp;->e:Lqax;

    if-eqz v0, :cond_5

    .line 167
    const/4 v0, 0x6

    iget-object v1, p0, Lqbp;->e:Lqax;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 169
    :cond_5
    iget-object v0, p0, Lqbp;->f:Lqay;

    if-eqz v0, :cond_6

    .line 170
    const/4 v0, 0x7

    iget-object v1, p0, Lqbp;->f:Lqay;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 172
    :cond_6
    iget-object v0, p0, Lqbp;->g:Lqbd;

    if-eqz v0, :cond_7

    .line 173
    const/16 v0, 0x8

    iget-object v1, p0, Lqbp;->g:Lqbd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 175
    :cond_7
    iget v0, p0, Lqbp;->h:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_8

    .line 176
    const/16 v0, 0x9

    iget v1, p0, Lqbp;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 178
    :cond_8
    iget-object v0, p0, Lqbp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 180
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lqbp;->a(Loxn;)Lqbp;

    move-result-object v0

    return-object v0
.end method
