.class public final Lmju;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfa;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22921
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22924
    iput-object v0, p0, Lmju;->apiHeader:Llyq;

    .line 22927
    iput-object v0, p0, Lmju;->a:Lnfa;

    .line 22921
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 22944
    const/4 v0, 0x0

    .line 22945
    iget-object v1, p0, Lmju;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 22946
    const/4 v0, 0x1

    iget-object v1, p0, Lmju;->apiHeader:Llyq;

    .line 22947
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22949
    :cond_0
    iget-object v1, p0, Lmju;->a:Lnfa;

    if-eqz v1, :cond_1

    .line 22950
    const/4 v1, 0x2

    iget-object v2, p0, Lmju;->a:Lnfa;

    .line 22951
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22953
    :cond_1
    iget-object v1, p0, Lmju;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22954
    iput v0, p0, Lmju;->ai:I

    .line 22955
    return v0
.end method

.method public a(Loxn;)Lmju;
    .locals 2

    .prologue
    .line 22963
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 22964
    sparse-switch v0, :sswitch_data_0

    .line 22968
    iget-object v1, p0, Lmju;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 22969
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmju;->ah:Ljava/util/List;

    .line 22972
    :cond_1
    iget-object v1, p0, Lmju;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22974
    :sswitch_0
    return-object p0

    .line 22979
    :sswitch_1
    iget-object v0, p0, Lmju;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 22980
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmju;->apiHeader:Llyq;

    .line 22982
    :cond_2
    iget-object v0, p0, Lmju;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22986
    :sswitch_2
    iget-object v0, p0, Lmju;->a:Lnfa;

    if-nez v0, :cond_3

    .line 22987
    new-instance v0, Lnfa;

    invoke-direct {v0}, Lnfa;-><init>()V

    iput-object v0, p0, Lmju;->a:Lnfa;

    .line 22989
    :cond_3
    iget-object v0, p0, Lmju;->a:Lnfa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22964
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22932
    iget-object v0, p0, Lmju;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 22933
    const/4 v0, 0x1

    iget-object v1, p0, Lmju;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22935
    :cond_0
    iget-object v0, p0, Lmju;->a:Lnfa;

    if-eqz v0, :cond_1

    .line 22936
    const/4 v0, 0x2

    iget-object v1, p0, Lmju;->a:Lnfa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22938
    :cond_1
    iget-object v0, p0, Lmju;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 22940
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22917
    invoke-virtual {p0, p1}, Lmju;->a(Loxn;)Lmju;

    move-result-object v0

    return-object v0
.end method
