.class public final Lpvj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpvj;


# instance fields
.field private b:I

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x0

    new-array v0, v0, [Lpvj;

    sput-object v0, Lpvj;->a:[Lpvj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 322
    invoke-direct {p0}, Loxq;-><init>()V

    .line 325
    const/high16 v0, -0x80000000

    iput v0, p0, Lpvj;->b:I

    .line 322
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 354
    const/4 v0, 0x0

    .line 355
    iget v1, p0, Lpvj;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 356
    const/4 v0, 0x1

    iget v1, p0, Lpvj;->b:I

    .line 357
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 359
    :cond_0
    iget-object v1, p0, Lpvj;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 360
    const/4 v1, 0x2

    iget-object v2, p0, Lpvj;->c:Ljava/lang/Integer;

    .line 361
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_1
    iget-object v1, p0, Lpvj;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 364
    const/4 v1, 0x3

    iget-object v2, p0, Lpvj;->d:Ljava/lang/String;

    .line 365
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_2
    iget-object v1, p0, Lpvj;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 368
    const/4 v1, 0x4

    iget-object v2, p0, Lpvj;->e:Ljava/lang/String;

    .line 369
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_3
    iget-object v1, p0, Lpvj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 372
    iput v0, p0, Lpvj;->ai:I

    .line 373
    return v0
.end method

.method public a(Loxn;)Lpvj;
    .locals 2

    .prologue
    .line 381
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 382
    sparse-switch v0, :sswitch_data_0

    .line 386
    iget-object v1, p0, Lpvj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 387
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpvj;->ah:Ljava/util/List;

    .line 390
    :cond_1
    iget-object v1, p0, Lpvj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 392
    :sswitch_0
    return-object p0

    .line 397
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 398
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 402
    :cond_2
    iput v0, p0, Lpvj;->b:I

    goto :goto_0

    .line 404
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpvj;->b:I

    goto :goto_0

    .line 409
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpvj;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 413
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvj;->d:Ljava/lang/String;

    goto :goto_0

    .line 417
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvj;->e:Ljava/lang/String;

    goto :goto_0

    .line 382
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 336
    iget v0, p0, Lpvj;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 337
    const/4 v0, 0x1

    iget v1, p0, Lpvj;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 339
    :cond_0
    iget-object v0, p0, Lpvj;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 340
    const/4 v0, 0x2

    iget-object v1, p0, Lpvj;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 342
    :cond_1
    iget-object v0, p0, Lpvj;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 343
    const/4 v0, 0x3

    iget-object v1, p0, Lpvj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 345
    :cond_2
    iget-object v0, p0, Lpvj;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 346
    const/4 v0, 0x4

    iget-object v1, p0, Lpvj;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 348
    :cond_3
    iget-object v0, p0, Lpvj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 350
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 318
    invoke-virtual {p0, p1}, Lpvj;->a(Loxn;)Lpvj;

    move-result-object v0

    return-object v0
.end method
