.class public final Ljdc;
.super Ljava/lang/Exception;
.source "PG"


# static fields
.field private static final serialVersionUID:J = -0x23be304bee53fe09L


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1136
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 1137
    iput-object p2, p0, Ljdc;->a:Ljava/lang/String;

    .line 1138
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljdc;->b:Z

    .line 1139
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1120
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljdc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1130
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1131
    iput-object p2, p0, Ljdc;->a:Ljava/lang/String;

    .line 1132
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljdc;->b:Z

    .line 1133
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1124
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1125
    iput-boolean p2, p0, Ljdc;->b:Z

    .line 1126
    const/4 v0, 0x0

    iput-object v0, p0, Ljdc;->a:Ljava/lang/String;

    .line 1127
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1142
    iget-object v0, p0, Ljdc;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1146
    iget-boolean v0, p0, Ljdc;->b:Z

    return v0
.end method
