.class public final Lpoy;
.super Loxq;
.source "PG"


# static fields
.field private static a:[Lpoy;


# instance fields
.field private b:I

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:[Lpoy;

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lpoy;

    sput-object v0, Lpoy;->a:[Lpoy;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    iput v1, p0, Lpoy;->b:I

    .line 46
    sget-object v0, Lpoy;->a:[Lpoy;

    iput-object v0, p0, Lpoy;->o:[Lpoy;

    .line 49
    iput v1, p0, Lpoy;->p:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, -0x80000000

    .line 109
    .line 110
    iget v0, p0, Lpoy;->b:I

    if-eq v0, v6, :cond_f

    .line 111
    const/4 v0, 0x1

    iget v2, p0, Lpoy;->b:I

    .line 112
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 114
    :goto_0
    iget-object v2, p0, Lpoy;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 115
    const/4 v2, 0x2

    iget-object v3, p0, Lpoy;->c:Ljava/lang/Long;

    .line 116
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 118
    :cond_0
    iget-object v2, p0, Lpoy;->e:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 119
    const/4 v2, 0x3

    iget-object v3, p0, Lpoy;->e:Ljava/lang/Long;

    .line 120
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 122
    :cond_1
    iget-object v2, p0, Lpoy;->f:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 123
    const/4 v2, 0x4

    iget-object v3, p0, Lpoy;->f:Ljava/lang/String;

    .line 124
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 126
    :cond_2
    iget-object v2, p0, Lpoy;->g:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 127
    const/4 v2, 0x5

    iget-object v3, p0, Lpoy;->g:Ljava/lang/String;

    .line 128
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 130
    :cond_3
    iget-object v2, p0, Lpoy;->h:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 131
    const/4 v2, 0x6

    iget-object v3, p0, Lpoy;->h:Ljava/lang/String;

    .line 132
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 134
    :cond_4
    iget-object v2, p0, Lpoy;->o:[Lpoy;

    if-eqz v2, :cond_6

    .line 135
    iget-object v2, p0, Lpoy;->o:[Lpoy;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 136
    if-eqz v4, :cond_5

    .line 137
    const/4 v5, 0x7

    .line 138
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 135
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 142
    :cond_6
    iget-object v1, p0, Lpoy;->n:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 143
    const/16 v1, 0x8

    iget-object v2, p0, Lpoy;->n:Ljava/lang/String;

    .line 144
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    :cond_7
    iget-object v1, p0, Lpoy;->l:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 147
    const/16 v1, 0x9

    iget-object v2, p0, Lpoy;->l:Ljava/lang/String;

    .line 148
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_8
    iget-object v1, p0, Lpoy;->i:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 151
    const/16 v1, 0xa

    iget-object v2, p0, Lpoy;->i:Ljava/lang/String;

    .line 152
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_9
    iget-object v1, p0, Lpoy;->j:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 155
    const/16 v1, 0xb

    iget-object v2, p0, Lpoy;->j:Ljava/lang/String;

    .line 156
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    :cond_a
    iget-object v1, p0, Lpoy;->k:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 159
    const/16 v1, 0xc

    iget-object v2, p0, Lpoy;->k:Ljava/lang/String;

    .line 160
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_b
    iget-object v1, p0, Lpoy;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 163
    const/16 v1, 0xd

    iget-object v2, p0, Lpoy;->m:Ljava/lang/String;

    .line 164
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_c
    iget v1, p0, Lpoy;->p:I

    if-eq v1, v6, :cond_d

    .line 167
    const/16 v1, 0xe

    iget v2, p0, Lpoy;->p:I

    .line 168
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_d
    iget-object v1, p0, Lpoy;->d:Ljava/lang/Long;

    if-eqz v1, :cond_e

    .line 171
    const/16 v1, 0xf

    iget-object v2, p0, Lpoy;->d:Ljava/lang/Long;

    .line 172
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_e
    iget-object v1, p0, Lpoy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    iput v0, p0, Lpoy;->ai:I

    .line 176
    return v0

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpoy;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 184
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 185
    sparse-switch v0, :sswitch_data_0

    .line 189
    iget-object v2, p0, Lpoy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 190
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpoy;->ah:Ljava/util/List;

    .line 193
    :cond_1
    iget-object v2, p0, Lpoy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    :sswitch_0
    return-object p0

    .line 200
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 201
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    .line 204
    :cond_2
    iput v0, p0, Lpoy;->b:I

    goto :goto_0

    .line 206
    :cond_3
    iput v1, p0, Lpoy;->b:I

    goto :goto_0

    .line 211
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpoy;->c:Ljava/lang/Long;

    goto :goto_0

    .line 215
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpoy;->e:Ljava/lang/Long;

    goto :goto_0

    .line 219
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoy;->f:Ljava/lang/String;

    goto :goto_0

    .line 223
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoy;->g:Ljava/lang/String;

    goto :goto_0

    .line 227
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoy;->h:Ljava/lang/String;

    goto :goto_0

    .line 231
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 232
    iget-object v0, p0, Lpoy;->o:[Lpoy;

    if-nez v0, :cond_5

    move v0, v1

    .line 233
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpoy;

    .line 234
    iget-object v3, p0, Lpoy;->o:[Lpoy;

    if-eqz v3, :cond_4

    .line 235
    iget-object v3, p0, Lpoy;->o:[Lpoy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 237
    :cond_4
    iput-object v2, p0, Lpoy;->o:[Lpoy;

    .line 238
    :goto_2
    iget-object v2, p0, Lpoy;->o:[Lpoy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 239
    iget-object v2, p0, Lpoy;->o:[Lpoy;

    new-instance v3, Lpoy;

    invoke-direct {v3}, Lpoy;-><init>()V

    aput-object v3, v2, v0

    .line 240
    iget-object v2, p0, Lpoy;->o:[Lpoy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 241
    invoke-virtual {p1}, Loxn;->a()I

    .line 238
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 232
    :cond_5
    iget-object v0, p0, Lpoy;->o:[Lpoy;

    array-length v0, v0

    goto :goto_1

    .line 244
    :cond_6
    iget-object v2, p0, Lpoy;->o:[Lpoy;

    new-instance v3, Lpoy;

    invoke-direct {v3}, Lpoy;-><init>()V

    aput-object v3, v2, v0

    .line 245
    iget-object v2, p0, Lpoy;->o:[Lpoy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 249
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoy;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 253
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoy;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 257
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoy;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 261
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoy;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 265
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoy;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 269
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoy;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 273
    :sswitch_e
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 274
    if-eqz v0, :cond_7

    if-eq v0, v4, :cond_7

    if-eq v0, v5, :cond_7

    const/4 v2, 0x3

    if-ne v0, v2, :cond_8

    .line 278
    :cond_7
    iput v0, p0, Lpoy;->p:I

    goto/16 :goto_0

    .line 280
    :cond_8
    iput v1, p0, Lpoy;->p:I

    goto/16 :goto_0

    .line 285
    :sswitch_f
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpoy;->d:Ljava/lang/Long;

    goto/16 :goto_0

    .line 185
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 54
    iget v0, p0, Lpoy;->b:I

    if-eq v0, v5, :cond_0

    .line 55
    const/4 v0, 0x1

    iget v1, p0, Lpoy;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 57
    :cond_0
    iget-object v0, p0, Lpoy;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 58
    const/4 v0, 0x2

    iget-object v1, p0, Lpoy;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 60
    :cond_1
    iget-object v0, p0, Lpoy;->e:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 61
    const/4 v0, 0x3

    iget-object v1, p0, Lpoy;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 63
    :cond_2
    iget-object v0, p0, Lpoy;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 64
    const/4 v0, 0x4

    iget-object v1, p0, Lpoy;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 66
    :cond_3
    iget-object v0, p0, Lpoy;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 67
    const/4 v0, 0x5

    iget-object v1, p0, Lpoy;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 69
    :cond_4
    iget-object v0, p0, Lpoy;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 70
    const/4 v0, 0x6

    iget-object v1, p0, Lpoy;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 72
    :cond_5
    iget-object v0, p0, Lpoy;->o:[Lpoy;

    if-eqz v0, :cond_7

    .line 73
    iget-object v1, p0, Lpoy;->o:[Lpoy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 74
    if-eqz v3, :cond_6

    .line 75
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 73
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    :cond_7
    iget-object v0, p0, Lpoy;->n:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 80
    const/16 v0, 0x8

    iget-object v1, p0, Lpoy;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 82
    :cond_8
    iget-object v0, p0, Lpoy;->l:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 83
    const/16 v0, 0x9

    iget-object v1, p0, Lpoy;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 85
    :cond_9
    iget-object v0, p0, Lpoy;->i:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 86
    const/16 v0, 0xa

    iget-object v1, p0, Lpoy;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 88
    :cond_a
    iget-object v0, p0, Lpoy;->j:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 89
    const/16 v0, 0xb

    iget-object v1, p0, Lpoy;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 91
    :cond_b
    iget-object v0, p0, Lpoy;->k:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 92
    const/16 v0, 0xc

    iget-object v1, p0, Lpoy;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 94
    :cond_c
    iget-object v0, p0, Lpoy;->m:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 95
    const/16 v0, 0xd

    iget-object v1, p0, Lpoy;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 97
    :cond_d
    iget v0, p0, Lpoy;->p:I

    if-eq v0, v5, :cond_e

    .line 98
    const/16 v0, 0xe

    iget v1, p0, Lpoy;->p:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 100
    :cond_e
    iget-object v0, p0, Lpoy;->d:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 101
    const/16 v0, 0xf

    iget-object v1, p0, Lpoy;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 103
    :cond_f
    iget-object v0, p0, Lpoy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 105
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpoy;->a(Loxn;)Lpoy;

    move-result-object v0

    return-object v0
.end method
