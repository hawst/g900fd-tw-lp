.class public abstract Lhvs;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lkdd;
.implements Lljh;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Lloz;

.field private static b:Lhwd;


# instance fields
.field private c:Landroid/graphics/drawable/Drawable;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private e:Llip;

.field private f:Z

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:Landroid/graphics/Rect;

.field public r:Ljava/lang/String;

.field public s:I

.field public t:I

.field public u:Z

.field public v:Z

.field public w:I

.field public x:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lloz;

    const-string v1, "enable_debug_stream"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhvs;->a:Lloz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    iput v0, p0, Lhvs;->o:I

    .line 48
    iput v0, p0, Lhvs;->p:I

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhvs;->d:Ljava/util/List;

    .line 76
    invoke-virtual {p0, p1}, Lhvs;->a(Landroid/content/Context;)V

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhvs;->setWillNotDraw(Z)V

    .line 79
    return-void
.end method


# virtual methods
.method public A()I
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lhvs;->p:I

    return v0
.end method

.method public B()V
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvs;->u:Z

    .line 491
    return-void
.end method

.method public a(IIII)I
    .locals 0

    .prologue
    .line 157
    return p2
.end method

.method public a(Landroid/graphics/Canvas;IIIII)I
    .locals 0

    .prologue
    .line 166
    return p6
.end method

.method public a(Lhuk;I)I
    .locals 2

    .prologue
    .line 325
    invoke-interface {p1, p2}, Lhuk;->a(I)I

    move-result v0

    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public a()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 340
    invoke-virtual {p0}, Lhvs;->c()V

    .line 341
    invoke-virtual {p0}, Lhvs;->removeAllViews()V

    .line 343
    iget-object v0, p0, Lhvs;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 344
    iput-object v4, p0, Lhvs;->e:Llip;

    .line 345
    iget-boolean v0, p0, Lhvs;->f:Z

    if-eqz v0, :cond_0

    .line 346
    invoke-virtual {p0, v3}, Lhvs;->setClickable(Z)V

    .line 347
    iput-boolean v1, p0, Lhvs;->f:Z

    .line 350
    :cond_0
    iput v1, p0, Lhvs;->m:I

    .line 351
    iput v1, p0, Lhvs;->n:I

    .line 352
    iput v1, p0, Lhvs;->t:I

    .line 353
    iput-object v4, p0, Lhvs;->r:Ljava/lang/String;

    .line 354
    iput v3, p0, Lhvs;->o:I

    .line 355
    iput v3, p0, Lhvs;->p:I

    .line 356
    iput v1, p0, Lhvs;->w:I

    .line 357
    iput-boolean v1, p0, Lhvs;->u:Z

    .line 358
    iput-boolean v1, p0, Lhvs;->v:Z

    .line 360
    invoke-virtual {p0, v4}, Lhvs;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lhvs;->setTranslationY(F)V

    invoke-virtual {p0, v2}, Lhvs;->setRotationX(F)V

    invoke-virtual {p0, v2}, Lhvs;->setRotationY(F)V

    invoke-virtual {p0, v5}, Lhvs;->setScaleX(F)V

    invoke-virtual {p0, v5}, Lhvs;->setScaleY(F)V

    .line 363
    :cond_1
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 484
    sget-object v0, Lhvs;->b:Lhwd;

    if-nez v0, :cond_0

    .line 485
    invoke-static {p1}, Lhwd;->a(Landroid/content/Context;)Lhwd;

    move-result-object v0

    sput-object v0, Lhvs;->b:Lhwd;

    .line 487
    :cond_0
    return-void
.end method

.method public a(Lhuk;)V
    .locals 4

    .prologue
    .line 119
    invoke-interface {p1}, Lhuk;->a()I

    move-result v0

    if-gtz v0, :cond_0

    .line 135
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-virtual {p0, p1}, Lhvs;->b(Lhuk;)I

    move-result v0

    iput v0, p0, Lhvs;->m:I

    .line 124
    invoke-virtual {p0}, Lhvs;->v()I

    move-result v0

    iput v0, p0, Lhvs;->t:I

    .line 126
    iget-object v0, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sget-object v1, Lhvs;->b:Lhwd;

    iget v1, v1, Lhwd;->k:I

    add-int/2addr v0, v1

    .line 127
    invoke-virtual {p0}, Lhvs;->w()I

    move-result v1

    iput v1, p0, Lhvs;->s:I

    .line 129
    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lhvs;->n:I

    .line 132
    iget v1, p0, Lhvs;->m:I

    iget v2, p0, Lhvs;->n:I

    iget v3, p0, Lhvs;->s:I

    invoke-virtual {p0, v1, v2, v0, v3}, Lhvs;->a(IIII)I

    move-result v0

    iput v0, p0, Lhvs;->n:I

    .line 134
    iget v0, p0, Lhvs;->n:I

    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iput v0, p0, Lhvs;->n:I

    goto :goto_0
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 391
    invoke-virtual {p0}, Lhvs;->invalidate()V

    .line 392
    return-void
.end method

.method public a(Llip;)V
    .locals 1

    .prologue
    .line 241
    if-eqz p1, :cond_0

    iget-object v0, p0, Lhvs;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lhvs;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 469
    return-void
.end method

.method public a(ZIIII)V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public aG_()Z
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x1

    return v0
.end method

.method protected b(Lhuk;)I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lhvs;->o:I

    invoke-interface {p1, v0}, Lhuk;->a(I)I

    move-result v0

    return v0
.end method

.method public b(Z)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 476
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 382
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 387
    return-void
.end method

.method public d()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 418
    invoke-virtual {p0}, Lhvs;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 420
    invoke-virtual {p0}, Lhvs;->aG_()Z

    move-result v1

    if-nez v1, :cond_0

    .line 425
    :goto_0
    return-object v0

    .line 423
    :cond_0
    invoke-virtual {p0}, Lhvs;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020069

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 425
    sget-object v2, Lhvs;->b:Lhwd;

    iget v2, v2, Lhwd;->c:I

    invoke-static {v0, v2, v1}, Lkct;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 334
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 335
    invoke-virtual {p0}, Lhvs;->invalidate()V

    .line 336
    return-void
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 441
    iget-boolean v0, p0, Lhvs;->u:Z

    if-eq v0, p1, :cond_0

    .line 442
    iput-boolean p1, p0, Lhvs;->u:Z

    .line 443
    iget-boolean v0, p0, Lhvs;->v:Z

    if-nez v0, :cond_0

    .line 444
    invoke-virtual {p0}, Lhvs;->z()Z

    move-result v0

    invoke-virtual {p0, v0}, Lhvs;->a(Z)V

    .line 447
    :cond_0
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x1

    return v0
.end method

.method public o()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 398
    sget-object v0, Lhvs;->b:Lhwd;

    iget-object v0, v0, Lhwd;->u:Landroid/graphics/Rect;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 0

    .prologue
    .line 375
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 376
    invoke-virtual {p0}, Lhvs;->b()V

    .line 377
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 367
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 368
    invoke-virtual {p0}, Lhvs;->c()V

    .line 370
    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    .line 371
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 189
    invoke-virtual {p0}, Lhvs;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 195
    invoke-virtual {p0}, Lhvs;->getWidth()I

    move-result v0

    .line 198
    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    .line 201
    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int v1, v0, v1

    iget-object v3, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int v3, v1, v3

    .line 203
    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget-object v4, Lhvs;->b:Lhwd;

    iget v4, v4, Lhwd;->k:I

    add-int/2addr v4, v1

    .line 204
    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    sget-object v1, Lhvs;->b:Lhwd;

    iget v1, v1, Lhwd;->k:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v5, v0, v1

    .line 207
    iget-object v0, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move-object v1, p1

    .line 209
    invoke-virtual/range {v0 .. v6}, Lhvs;->a(Landroid/graphics/Canvas;IIIII)I

    .line 212
    iget-object v0, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 214
    sget-object v0, Lhvs;->a:Lloz;

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 184
    invoke-virtual/range {p0 .. p5}, Lhvs;->a(ZIIII)V

    .line 185
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 175
    invoke-virtual {p0}, Lhvs;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 176
    invoke-virtual {p0, v1, v1}, Lhvs;->setMeasuredDimension(II)V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget v0, p0, Lhvs;->m:I

    if-lez v0, :cond_0

    iget v0, p0, Lhvs;->n:I

    if-lez v0, :cond_0

    .line 178
    iget v0, p0, Lhvs;->m:I

    iget v1, p0, Lhvs;->n:I

    invoke-virtual {p0, v0, v1}, Lhvs;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 256
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 257
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 261
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v0, v1

    .line 308
    :goto_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 312
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v6, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, Lhvs;->x:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lhvs;->x:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 317
    :cond_1
    return v6

    .line 263
    :pswitch_1
    iget-object v0, p0, Lhvs;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_4

    .line 264
    iget-object v0, p0, Lhvs;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 265
    invoke-interface {v0, v3, v4, v1}, Llip;->a(III)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 266
    iput-object v0, p0, Lhvs;->e:Llip;

    .line 268
    invoke-virtual {p0}, Lhvs;->isClickable()Z

    move-result v0

    iput-boolean v0, p0, Lhvs;->f:Z

    .line 269
    invoke-virtual {p0, v1}, Lhvs;->setClickable(Z)V

    .line 302
    :cond_2
    :goto_3
    invoke-virtual {p0}, Lhvs;->invalidate()V

    goto :goto_0

    .line 263
    :cond_3
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    :cond_4
    move v0, v1

    .line 274
    goto :goto_1

    .line 278
    :pswitch_2
    iget-object v0, p0, Lhvs;->e:Llip;

    if-eqz v0, :cond_6

    .line 279
    iput-object v5, p0, Lhvs;->e:Llip;

    .line 280
    iget-boolean v0, p0, Lhvs;->f:Z

    if-eqz v0, :cond_5

    .line 281
    invoke-virtual {p0, v6}, Lhvs;->setClickable(Z)V

    .line 282
    iput-boolean v1, p0, Lhvs;->f:Z

    .line 284
    :cond_5
    invoke-virtual {p0, v1}, Lhvs;->setPressed(Z)V

    .line 285
    invoke-virtual {p0}, Lhvs;->invalidate()V

    .line 287
    :cond_6
    iget-object v0, p0, Lhvs;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_4
    if-ltz v2, :cond_7

    .line 288
    iget-object v0, p0, Lhvs;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 289
    invoke-interface {v0, v3, v4, v6}, Llip;->a(III)Z

    move-result v0

    or-int/2addr v1, v0

    .line 287
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_4

    :cond_7
    move v0, v1

    .line 291
    goto :goto_1

    .line 295
    :pswitch_3
    iget-object v0, p0, Lhvs;->e:Llip;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lhvs;->e:Llip;

    const/4 v2, 0x3

    invoke-interface {v0, v3, v4, v2}, Llip;->a(III)Z

    .line 297
    iput-object v5, p0, Lhvs;->e:Llip;

    .line 298
    iget-boolean v0, p0, Lhvs;->f:Z

    if-eqz v0, :cond_2

    .line 299
    invoke-virtual {p0, v6}, Lhvs;->setClickable(Z)V

    .line 300
    iput-boolean v1, p0, Lhvs;->f:Z

    goto :goto_3

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lhvs;->x:Landroid/view/View$OnClickListener;

    .line 233
    return-void
.end method

.method public t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lhvs;->r:Ljava/lang/String;

    return-object v0
.end method

.method public u()V
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lhvs;->w:I

    if-nez v0, :cond_0

    .line 90
    invoke-virtual {p0}, Lhvs;->y()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhvs;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    sget-object v0, Lhvs;->b:Lhwd;

    iget v0, v0, Lhwd;->t:I

    invoke-virtual {p0, v0}, Lhvs;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method protected v()I
    .locals 2

    .prologue
    .line 142
    iget v0, p0, Lhvs;->m:I

    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    return v0
.end method

.method protected w()I
    .locals 2

    .prologue
    .line 146
    iget v0, p0, Lhvs;->m:I

    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    sget-object v1, Lhvs;->b:Lhwd;

    iget v1, v1, Lhwd;->k:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lhvs;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public x()I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lhvs;->o:I

    return v0
.end method

.method protected y()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lhvs;->c:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 407
    invoke-virtual {p0}, Lhvs;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lhvs;->c:Landroid/graphics/drawable/Drawable;

    .line 409
    :cond_0
    iget-object v0, p0, Lhvs;->c:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 437
    iget-boolean v0, p0, Lhvs;->u:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhvs;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
