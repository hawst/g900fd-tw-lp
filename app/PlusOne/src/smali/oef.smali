.class public final Loef;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Loho;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, Loxq;-><init>()V

    .line 244
    const/high16 v0, -0x80000000

    iput v0, p0, Loef;->a:I

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Loef;->b:Loho;

    .line 233
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 264
    const/4 v0, 0x0

    .line 265
    iget v1, p0, Loef;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 266
    const/4 v0, 0x1

    iget v1, p0, Loef;->a:I

    .line 267
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 269
    :cond_0
    iget-object v1, p0, Loef;->b:Loho;

    if-eqz v1, :cond_1

    .line 270
    const/4 v1, 0x2

    iget-object v2, p0, Loef;->b:Loho;

    .line 271
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_1
    iget-object v1, p0, Loef;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Loef;->ai:I

    .line 275
    return v0
.end method

.method public a(Loxn;)Loef;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 283
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 284
    sparse-switch v0, :sswitch_data_0

    .line 288
    iget-object v1, p0, Loef;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 289
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loef;->ah:Ljava/util/List;

    .line 292
    :cond_1
    iget-object v1, p0, Loef;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    :sswitch_0
    return-object p0

    .line 299
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 300
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 305
    :cond_2
    iput v0, p0, Loef;->a:I

    goto :goto_0

    .line 307
    :cond_3
    iput v2, p0, Loef;->a:I

    goto :goto_0

    .line 312
    :sswitch_2
    iget-object v0, p0, Loef;->b:Loho;

    if-nez v0, :cond_4

    .line 313
    new-instance v0, Loho;

    invoke-direct {v0}, Loho;-><init>()V

    iput-object v0, p0, Loef;->b:Loho;

    .line 315
    :cond_4
    iget-object v0, p0, Loef;->b:Loho;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 284
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 252
    iget v0, p0, Loef;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 253
    const/4 v0, 0x1

    iget v1, p0, Loef;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 255
    :cond_0
    iget-object v0, p0, Loef;->b:Loho;

    if-eqz v0, :cond_1

    .line 256
    const/4 v0, 0x2

    iget-object v1, p0, Loef;->b:Loho;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 258
    :cond_1
    iget-object v0, p0, Loef;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 260
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0, p1}, Loef;->a(Loxn;)Loef;

    move-result-object v0

    return-object v0
.end method
