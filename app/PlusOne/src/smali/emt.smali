.class public final Lemt;
.super Lepp;
.source "PG"


# instance fields
.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILept;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lepp;-><init>(Landroid/content/Context;ILept;)V

    .line 26
    return-void
.end method

.method static synthetic a(Lemt;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lemt;->d:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic a(Lemt;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lemt;->d:Ljava/lang/Integer;

    return-object p1
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lemt;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 75
    :goto_0
    return-void

    .line 48
    :cond_0
    new-instance v0, Lnyq;

    invoke-direct {v0}, Lnyq;-><init>()V

    .line 49
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyq;->f:Ljava/lang/Boolean;

    .line 50
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyq;->g:Ljava/lang/Boolean;

    .line 51
    new-instance v1, Lemu;

    invoke-direct {v1, p0}, Lemu;-><init>(Lemt;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 74
    iget-object v1, p0, Lemt;->c:Landroid/content/Context;

    iget v2, p0, Lemt;->b:I

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILnyq;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lemt;->d:Ljava/lang/Integer;

    goto :goto_0
.end method

.method protected a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 87
    iget-object v0, p0, Lemt;->c:Landroid/content/Context;

    const v1, 0x7f0a0679

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lemt;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 88
    const v0, 0x7f0a067a

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lemt;->c:Landroid/content/Context;

    const-string v4, "google_drive"

    .line 90
    const-string v5, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v3, v4, v5}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 88
    invoke-virtual {p0, p1, v0, v1}, Lemt;->a(Landroid/view/View;I[Ljava/lang/Object;)V

    .line 92
    return-void
.end method

.method public a(Lnyq;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-super {p0, p1}, Lepp;->a(Lnyq;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 38
    :goto_0
    return v0

    .line 34
    :cond_0
    iget-object v0, p0, Lemt;->c:Landroid/content/Context;

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v2, p0, Lemt;->b:I

    .line 35
    invoke-interface {v0, v2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "is_dasher_account"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    .line 36
    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p1, Lnyq;->f:Ljava/lang/Boolean;

    .line 38
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lemt;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p0}, Lemt;->f()V

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 96
    const v0, 0x7f040194

    return v0
.end method

.method public e()Lepn;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lepn;->c:Lepn;

    return-object v0
.end method
