.class public final Lcjm;
.super Llol;
.source "PG"


# instance fields
.field private N:Z

.field private O:Lcom/google/android/apps/photoeditor/views/FitImageView;

.field private P:Landroid/widget/LinearLayout;

.field private Q:Landroid/view/View;

.field private R:Landroid/widget/ProgressBar;

.field private S:Lcjt;

.field private T:Z

.field private U:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Llol;-><init>()V

    .line 478
    return-void
.end method

.method static synthetic a(Lcjm;)Llnl;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcjm;->at:Llnl;

    return-object v0
.end method

.method static synthetic a(Lcjm;Z)V
    .locals 3

    .prologue
    .line 59
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcjm;->O:Lcom/google/android/apps/photoeditor/views/FitImageView;

    invoke-direct {p0}, Lcjm;->d()Lchp;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lchp;->a(Z)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photoeditor/views/FitImageView;->a(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcjm;->O:Lcom/google/android/apps/photoeditor/views/FitImageView;

    invoke-direct {p0}, Lcjm;->d()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photoeditor/views/FitImageView;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method static synthetic b(Lcjm;)Llnl;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcjm;->at:Llnl;

    return-object v0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 456
    iget-boolean v0, p0, Lcjm;->U:Z

    if-ne v0, p1, :cond_0

    .line 465
    :goto_0
    return-void

    .line 460
    :cond_0
    iput-boolean p1, p0, Lcjm;->U:Z

    .line 462
    iget-object v1, p0, Lcjm;->Q:Landroid/view/View;

    iget-boolean v0, p0, Lcjm;->U:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 464
    invoke-virtual {p0}, Lcjm;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->invalidateOptionsMenu()V

    goto :goto_0

    .line 462
    :cond_1
    const/4 v0, 0x4

    goto :goto_1
.end method

.method static synthetic c(Lcjm;)Lcjt;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcjm;->S:Lcjt;

    return-object v0
.end method

.method private d()Lchp;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcjm;->n()Lz;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-interface {v0}, Lcky;->i()Lchp;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcjm;)Llnl;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcjm;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Lcjm;)Llnl;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcjm;->at:Llnl;

    return-object v0
.end method

.method private e()V
    .locals 21

    .prologue
    .line 301
    move-object/from16 v0, p0

    iget-object v3, v0, Lcjm;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 303
    new-instance v9, Lcjq;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcjq;-><init>(Lcjm;)V

    .line 316
    move-object/from16 v0, p0

    iget-object v3, v0, Lcjm;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/widget/HorizontalScrollView;

    .line 317
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v4, v5, :cond_0

    .line 318
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/HorizontalScrollView;->setLayoutDirection(I)V

    .line 321
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcjm;->n()Lz;

    move-result-object v10

    .line 322
    invoke-static {v10}, Lhea;->b(Landroid/content/Context;)Z

    move-result v11

    .line 324
    invoke-virtual/range {p0 .. p0}, Lcjm;->o()Landroid/content/res/Resources;

    move-result-object v12

    .line 325
    const v4, 0x7f0d0028

    invoke-virtual {v12, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    .line 326
    const v4, 0x7f0d0029

    invoke-virtual {v12, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    .line 327
    const/16 v5, 0x3e8

    .line 330
    invoke-virtual/range {p0 .. p0}, Lcjm;->n()Lz;

    move-result-object v4

    check-cast v4, Lcky;

    invoke-interface {v4}, Lcky;->h()Z

    move-result v15

    .line 331
    sget-object v16, Lcht;->a:[Lchu;

    const/4 v4, 0x0

    move v7, v4

    move v8, v5

    :goto_0
    const/16 v4, 0xf

    if-ge v7, v4, :cond_7

    aget-object v17, v16, v7

    .line 332
    if-nez v15, :cond_2

    invoke-virtual/range {v17 .. v17}, Lchu;->a()I

    move-result v4

    const/4 v5, 0x6

    if-eq v4, v5, :cond_1

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0x12

    if-ne v4, v5, :cond_3

    :cond_1
    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_4

    :cond_2
    const/4 v4, 0x1

    .line 334
    :goto_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lchu;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 335
    if-nez v4, :cond_9

    .line 336
    new-instance v5, Landroid/graphics/drawable/LayerDrawable;

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v6, v18, v19

    const/4 v6, 0x1

    new-instance v19, Landroid/graphics/drawable/ColorDrawable;

    const v20, 0x7fafafaf

    invoke-direct/range {v19 .. v20}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v19, v18, v6

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 340
    :goto_3
    new-instance v6, Lcmq;

    .line 341
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lchu;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v17}, Lchu;->b()Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v6, v10, v5, v0, v1}, Lcmq;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Z)V

    .line 342
    add-int/lit8 v5, v8, 0x1

    invoke-virtual {v6, v8}, Lcmq;->setId(I)V

    .line 343
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v18, -0x2

    const/16 v19, -0x2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v8, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 344
    div-int/lit8 v18, v13, 0x2

    const/16 v19, 0x0

    div-int/lit8 v20, v13, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v8, v0, v1, v2, v14}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 345
    invoke-virtual {v6, v9}, Lcmq;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 346
    invoke-virtual/range {v17 .. v17}, Lchu;->a()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcmq;->setTag(Ljava/lang/Object;)V

    .line 347
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Lcmq;->setDuplicateParentStateEnabled(Z)V

    .line 348
    invoke-virtual {v6, v4}, Lcmq;->setEnabled(Z)V

    .line 349
    if-eqz v4, :cond_5

    const/high16 v4, 0x3f800000    # 1.0f

    :goto_4
    invoke-virtual {v6, v4}, Lcmq;->setAlpha(F)V

    .line 351
    if-eqz v11, :cond_6

    .line 352
    move-object/from16 v0, p0

    iget-object v4, v0, Lcjm;->P:Landroid/widget/LinearLayout;

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v4, v6, v0, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 331
    :goto_5
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v8, v5

    goto/16 :goto_0

    .line 332
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 349
    :cond_5
    const v4, 0x3ecccccd    # 0.4f

    goto :goto_4

    .line 354
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcjm;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_5

    .line 358
    :cond_7
    invoke-static {v10}, Lhea;->b(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 359
    new-instance v4, Lcjr;

    invoke-direct {v4, v3}, Lcjr;-><init>(Landroid/widget/HorizontalScrollView;)V

    invoke-virtual {v3, v4}, Landroid/widget/HorizontalScrollView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 368
    :cond_8
    return-void

    :cond_9
    move-object v5, v6

    goto/16 :goto_3
.end method

.method static synthetic f(Lcjm;)Llnl;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcjm;->at:Llnl;

    return-object v0
.end method

.method static synthetic g(Lcjm;)Llnl;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcjm;->at:Llnl;

    return-object v0
.end method

.method static synthetic h(Lcjm;)Lchp;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcjm;->d()Lchp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 92
    iget-object v0, p0, Lcjm;->at:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 94
    const v1, 0x7f0400a7

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 95
    if-nez v1, :cond_0

    .line 96
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to inflate the root view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_0
    const v0, 0x7f100296

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photoeditor/views/FitImageView;

    iput-object v0, p0, Lcjm;->O:Lcom/google/android/apps/photoeditor/views/FitImageView;

    .line 100
    const v0, 0x7f100295

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcjm;->P:Landroid/widget/LinearLayout;

    .line 101
    const v0, 0x7f100297

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcjm;->Q:Landroid/view/View;

    .line 102
    const v0, 0x7f100298

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcjm;->R:Landroid/widget/ProgressBar;

    .line 104
    iget-object v0, p0, Lcjm;->Q:Landroid/view/View;

    new-instance v2, Lcjn;

    invoke-direct {v2}, Lcjn;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 111
    invoke-virtual {p0, v4}, Lcjm;->a(Z)V

    .line 113
    invoke-direct {p0}, Lcjm;->e()V

    .line 115
    if-eqz p3, :cond_1

    .line 116
    const-string v0, "FilterSelectorFragment.filterListAnimated"

    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcjm;->N:Z

    .line 119
    :cond_1
    iget-boolean v0, p0, Lcjm;->N:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcjm;->at:Llnl;

    invoke-static {v0}, Lhea;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 120
    iget-object v0, p0, Lcjm;->P:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcjm;->at:Llnl;

    const v3, 0x7f05001c

    .line 121
    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadLayoutAnimation(Landroid/content/Context;I)Landroid/view/animation/LayoutAnimationController;

    move-result-object v2

    .line 120
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 122
    iput-boolean v4, p0, Lcjm;->N:Z

    .line 127
    :goto_0
    return-object v1

    .line 124
    :cond_2
    iput-boolean v3, p0, Lcjm;->N:Z

    goto :goto_0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 278
    invoke-direct {p0}, Lcjm;->d()Lchp;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lchp;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    invoke-virtual {v0}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v1

    if-nez v1, :cond_1

    .line 281
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcjm;->T:Z

    .line 294
    :goto_0
    return-void

    .line 285
    :cond_1
    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 286
    iget-object v2, p0, Lcjm;->O:Lcom/google/android/apps/photoeditor/views/FitImageView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/photoeditor/views/FitImageView;->a(Landroid/graphics/Bitmap;)V

    .line 288
    invoke-virtual {v0}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v0

    invoke-static {v0}, Lhbg;->a(I)F

    move-result v0

    .line 289
    iget-object v1, p0, Lcjm;->O:Lcom/google/android/apps/photoeditor/views/FitImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/photoeditor/views/FitImageView;->a(F)V

    .line 291
    invoke-virtual {p0}, Lcjm;->c()V

    .line 293
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcjm;->T:Z

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 86
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcjm;->e(Z)V

    .line 87
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 5

    .prologue
    const v4, 0x7f1006f4

    const v3, 0x7f1006f3

    const/4 v2, 0x0

    .line 155
    invoke-virtual {p0}, Lcjm;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 156
    if-nez v0, :cond_0

    .line 157
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Could not get action bar"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    .line 161
    new-instance v1, Lcjo;

    invoke-direct {v1, p0}, Lcjo;-><init>(Lcjm;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    const v0, 0x7f1006f0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcjm;->U:Z

    if-nez v1, :cond_1

    .line 175
    invoke-direct {p0}, Lcjm;->d()Lchp;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcjm;->d()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->j()Z

    move-result v1

    if-nez v1, :cond_2

    .line 176
    :cond_1
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 180
    :cond_2
    const v0, 0x7f1006ef

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_3

    .line 182
    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 183
    const v1, 0x7f100177

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_3

    .line 185
    iget-boolean v1, p0, Lcjm;->U:Z

    if-nez v1, :cond_6

    .line 186
    if-eqz v0, :cond_3

    new-instance v1, Lcjp;

    invoke-direct {v1, p0}, Lcjp;-><init>(Lcjm;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 222
    :cond_3
    :goto_0
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 223
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    .line 226
    :cond_4
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 227
    invoke-interface {p1, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 230
    :cond_5
    return-void

    .line 188
    :cond_6
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lcjm;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f040034

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 140
    const v0, 0x7f120010

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 141
    return-void
.end method

.method public a(Lcjt;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcjm;->S:Lcjt;

    .line 298
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Lcjm;->R:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 440
    :cond_1
    iget-object v0, p0, Lcjm;->R:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 441
    if-eqz p1, :cond_0

    .line 442
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcjm;->b(Z)V

    goto :goto_0
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 234
    invoke-super {p0}, Llol;->aO_()V

    .line 236
    iget-boolean v0, p0, Lcjm;->T:Z

    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {p0}, Lcjm;->a()V

    .line 239
    :cond_0
    return-void
.end method

.method public a_(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 145
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1006f0

    if-ne v1, v2, :cond_1

    .line 146
    invoke-direct {p0}, Lcjm;->d()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcjs;

    invoke-direct {v1, p0}, Lcjs;-><init>(Lcjm;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcjm;->n()Lz;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a002f

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v3, 0x7f0a002d

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v3, 0x1040000

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 150
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Llol;->a_(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 447
    iget-object v0, p0, Lcjm;->R:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    .line 453
    :goto_0
    return-void

    .line 451
    :cond_0
    iget-object v0, p0, Lcjm;->R:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 452
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcjm;->b(Z)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 134
    const-string v0, "FilterSelectorFragment.filterListAnimated"

    iget-boolean v1, p0, Lcjm;->N:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 135
    return-void
.end method
