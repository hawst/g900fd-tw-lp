.class public final Lmdi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnca;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20167
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20170
    iput-object v0, p0, Lmdi;->apiHeader:Llyq;

    .line 20173
    iput-object v0, p0, Lmdi;->a:Lnca;

    .line 20167
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 20190
    const/4 v0, 0x0

    .line 20191
    iget-object v1, p0, Lmdi;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 20192
    const/4 v0, 0x1

    iget-object v1, p0, Lmdi;->apiHeader:Llyq;

    .line 20193
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20195
    :cond_0
    iget-object v1, p0, Lmdi;->a:Lnca;

    if-eqz v1, :cond_1

    .line 20196
    const/4 v1, 0x2

    iget-object v2, p0, Lmdi;->a:Lnca;

    .line 20197
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20199
    :cond_1
    iget-object v1, p0, Lmdi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20200
    iput v0, p0, Lmdi;->ai:I

    .line 20201
    return v0
.end method

.method public a(Loxn;)Lmdi;
    .locals 2

    .prologue
    .line 20209
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 20210
    sparse-switch v0, :sswitch_data_0

    .line 20214
    iget-object v1, p0, Lmdi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 20215
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdi;->ah:Ljava/util/List;

    .line 20218
    :cond_1
    iget-object v1, p0, Lmdi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20220
    :sswitch_0
    return-object p0

    .line 20225
    :sswitch_1
    iget-object v0, p0, Lmdi;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 20226
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmdi;->apiHeader:Llyq;

    .line 20228
    :cond_2
    iget-object v0, p0, Lmdi;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20232
    :sswitch_2
    iget-object v0, p0, Lmdi;->a:Lnca;

    if-nez v0, :cond_3

    .line 20233
    new-instance v0, Lnca;

    invoke-direct {v0}, Lnca;-><init>()V

    iput-object v0, p0, Lmdi;->a:Lnca;

    .line 20235
    :cond_3
    iget-object v0, p0, Lmdi;->a:Lnca;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20210
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 20178
    iget-object v0, p0, Lmdi;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 20179
    const/4 v0, 0x1

    iget-object v1, p0, Lmdi;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20181
    :cond_0
    iget-object v0, p0, Lmdi;->a:Lnca;

    if-eqz v0, :cond_1

    .line 20182
    const/4 v0, 0x2

    iget-object v1, p0, Lmdi;->a:Lnca;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20184
    :cond_1
    iget-object v0, p0, Lmdi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 20186
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 20163
    invoke-virtual {p0, p1}, Lmdi;->a(Loxn;)Lmdi;

    move-result-object v0

    return-object v0
.end method
