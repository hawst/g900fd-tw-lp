.class public final Lmzw;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 735
    invoke-direct {p0}, Loxq;-><init>()V

    .line 738
    const/high16 v0, -0x80000000

    iput v0, p0, Lmzw;->a:I

    .line 735
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 757
    const/4 v0, 0x0

    .line 758
    iget v1, p0, Lmzw;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 759
    const/4 v0, 0x1

    iget v1, p0, Lmzw;->a:I

    .line 760
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 762
    :cond_0
    iget-object v1, p0, Lmzw;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 763
    const/4 v1, 0x2

    iget-object v2, p0, Lmzw;->b:Ljava/lang/String;

    .line 764
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 766
    :cond_1
    iget-object v1, p0, Lmzw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 767
    iput v0, p0, Lmzw;->ai:I

    .line 768
    return v0
.end method

.method public a(Loxn;)Lmzw;
    .locals 2

    .prologue
    .line 776
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 777
    sparse-switch v0, :sswitch_data_0

    .line 781
    iget-object v1, p0, Lmzw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 782
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmzw;->ah:Ljava/util/List;

    .line 785
    :cond_1
    iget-object v1, p0, Lmzw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 787
    :sswitch_0
    return-object p0

    .line 792
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 793
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 801
    :cond_2
    iput v0, p0, Lmzw;->a:I

    goto :goto_0

    .line 803
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmzw;->a:I

    goto :goto_0

    .line 808
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmzw;->b:Ljava/lang/String;

    goto :goto_0

    .line 777
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 745
    iget v0, p0, Lmzw;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 746
    const/4 v0, 0x1

    iget v1, p0, Lmzw;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 748
    :cond_0
    iget-object v0, p0, Lmzw;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 749
    const/4 v0, 0x2

    iget-object v1, p0, Lmzw;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 751
    :cond_1
    iget-object v0, p0, Lmzw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 753
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 731
    invoke-virtual {p0, p1}, Lmzw;->a(Loxn;)Lmzw;

    move-result-object v0

    return-object v0
.end method
