.class public final Lman;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmln;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32398
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32401
    iput-object v0, p0, Lman;->apiHeader:Llyr;

    .line 32404
    iput-object v0, p0, Lman;->a:Lmln;

    .line 32398
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32421
    const/4 v0, 0x0

    .line 32422
    iget-object v1, p0, Lman;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 32423
    const/4 v0, 0x1

    iget-object v1, p0, Lman;->apiHeader:Llyr;

    .line 32424
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32426
    :cond_0
    iget-object v1, p0, Lman;->a:Lmln;

    if-eqz v1, :cond_1

    .line 32427
    const/4 v1, 0x2

    iget-object v2, p0, Lman;->a:Lmln;

    .line 32428
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32430
    :cond_1
    iget-object v1, p0, Lman;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32431
    iput v0, p0, Lman;->ai:I

    .line 32432
    return v0
.end method

.method public a(Loxn;)Lman;
    .locals 2

    .prologue
    .line 32440
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32441
    sparse-switch v0, :sswitch_data_0

    .line 32445
    iget-object v1, p0, Lman;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32446
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lman;->ah:Ljava/util/List;

    .line 32449
    :cond_1
    iget-object v1, p0, Lman;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32451
    :sswitch_0
    return-object p0

    .line 32456
    :sswitch_1
    iget-object v0, p0, Lman;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 32457
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lman;->apiHeader:Llyr;

    .line 32459
    :cond_2
    iget-object v0, p0, Lman;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32463
    :sswitch_2
    iget-object v0, p0, Lman;->a:Lmln;

    if-nez v0, :cond_3

    .line 32464
    new-instance v0, Lmln;

    invoke-direct {v0}, Lmln;-><init>()V

    iput-object v0, p0, Lman;->a:Lmln;

    .line 32466
    :cond_3
    iget-object v0, p0, Lman;->a:Lmln;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32441
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32409
    iget-object v0, p0, Lman;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 32410
    const/4 v0, 0x1

    iget-object v1, p0, Lman;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32412
    :cond_0
    iget-object v0, p0, Lman;->a:Lmln;

    if-eqz v0, :cond_1

    .line 32413
    const/4 v0, 0x2

    iget-object v1, p0, Lman;->a:Lmln;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32415
    :cond_1
    iget-object v0, p0, Lman;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32417
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 32394
    invoke-virtual {p0, p1}, Lman;->a(Loxn;)Lman;

    move-result-object v0

    return-object v0
.end method
