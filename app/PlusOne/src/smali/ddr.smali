.class public abstract Lddr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lddl;


# instance fields
.field public final A:J

.field public B:Lctz;

.field public C:Lctq;

.field private D:Lcnt;

.field private E:Ljem;

.field public final a:Landroid/os/Bundle;

.field public final b:I

.field public final c:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lddt;",
            "Lizu;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lnzi;

.field public final e:Lnzi;

.field public final f:Lnzb;

.field public final g:Lnzb;

.field public final h:Lnym;

.field public final i:Ljcn;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;

.field public final p:Ljava/lang/Integer;

.field public final q:Z

.field public final r:Z

.field public final s:Z

.field public final t:Z

.field public final u:Z

.field public final v:Z

.field public final w:J

.field public final x:J

.field public final y:J

.field public final z:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487
    const/4 v0, 0x0

    iput-object v0, p0, Lddr;->a:Landroid/os/Bundle;

    .line 488
    const/4 v0, -0x1

    iput v0, p0, Lddr;->b:I

    .line 490
    new-instance v0, Ljava/util/EnumMap;

    const-class v3, Lddt;

    invoke-direct {v0, v3}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lddr;->c:Ljava/util/EnumMap;

    .line 491
    const-class v0, Lizu;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    .line 492
    if-eqz v0, :cond_0

    .line 493
    iget-object v3, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v4, Lddt;->a:Lddt;

    invoke-virtual {v3, v4, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    :cond_0
    const-class v0, Lizu;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    .line 496
    if-eqz v0, :cond_1

    .line 497
    iget-object v3, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v4, Lddt;->d:Lddt;

    invoke-virtual {v3, v4, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    :cond_1
    const-class v0, Lizu;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    .line 500
    if-eqz v0, :cond_2

    .line 501
    iget-object v3, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v4, Lddt;->b:Lddt;

    invoke-virtual {v3, v4, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    :cond_2
    const-class v0, Lizu;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    .line 504
    if-eqz v0, :cond_3

    .line 505
    iget-object v3, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v4, Lddt;->c:Lddt;

    invoke-virtual {v3, v4, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    :cond_3
    const-class v0, Ljcn;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljcn;

    iput-object v0, p0, Lddr;->i:Ljcn;

    .line 508
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->j:Ljava/lang/String;

    .line 509
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->k:Ljava/lang/String;

    .line 510
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->l:Ljava/lang/String;

    .line 511
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->m:Ljava/lang/String;

    .line 512
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->n:Ljava/lang/String;

    .line 513
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->o:Ljava/lang/String;

    .line 514
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lddr;->p:Ljava/lang/Integer;

    .line 515
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lddr;->q:Z

    .line 516
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lddr;->r:Z

    .line 517
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lddr;->s:Z

    .line 518
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lddr;->t:Z

    .line 519
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lddr;->u:Z

    .line 520
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    :goto_5
    iput-boolean v1, p0, Lddr;->v:Z

    .line 521
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lddr;->w:J

    .line 522
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lddr;->x:J

    .line 523
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lddr;->y:J

    .line 524
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddr;->z:Ljava/lang/String;

    .line 525
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lddr;->A:J

    .line 527
    new-instance v0, Lnym;

    invoke-direct {v0}, Lnym;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lddr;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;

    iput-object v0, p0, Lddr;->h:Lnym;

    .line 528
    new-instance v0, Lnzi;

    invoke-direct {v0}, Lnzi;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lddr;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzi;

    iput-object v0, p0, Lddr;->d:Lnzi;

    .line 529
    new-instance v0, Lnzi;

    invoke-direct {v0}, Lnzi;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lddr;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzi;

    iput-object v0, p0, Lddr;->e:Lnzi;

    .line 530
    new-instance v0, Lnzb;

    invoke-direct {v0}, Lnzb;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lddr;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzb;

    iput-object v0, p0, Lddr;->f:Lnzb;

    .line 531
    new-instance v0, Lnzb;

    invoke-direct {v0}, Lnzb;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lddr;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzb;

    iput-object v0, p0, Lddr;->g:Lnzb;

    .line 532
    return-void

    :cond_4
    move v0, v2

    .line 515
    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 516
    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 517
    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 518
    goto/16 :goto_3

    :cond_8
    move v0, v2

    .line 519
    goto/16 :goto_4

    :cond_9
    move v1, v2

    .line 520
    goto/16 :goto_5
.end method

.method protected constructor <init>(Ldds;)V
    .locals 2

    .prologue
    .line 456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457
    iget-object v0, p1, Ldds;->d:Landroid/os/Bundle;

    iput-object v0, p0, Lddr;->a:Landroid/os/Bundle;

    .line 458
    iget v0, p1, Ldds;->e:I

    iput v0, p0, Lddr;->b:I

    .line 459
    new-instance v0, Ljava/util/EnumMap;

    iget-object v1, p1, Ldds;->f:Ljava/util/EnumMap;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/util/EnumMap;)V

    iput-object v0, p0, Lddr;->c:Ljava/util/EnumMap;

    .line 460
    iget-object v0, p1, Ldds;->g:Lnzi;

    iput-object v0, p0, Lddr;->d:Lnzi;

    .line 461
    iget-object v0, p1, Ldds;->h:Lnzi;

    iput-object v0, p0, Lddr;->e:Lnzi;

    .line 462
    iget-object v0, p1, Ldds;->i:Lnzb;

    iput-object v0, p0, Lddr;->f:Lnzb;

    .line 463
    iget-object v0, p1, Ldds;->j:Lnzb;

    iput-object v0, p0, Lddr;->g:Lnzb;

    .line 464
    iget-object v0, p1, Ldds;->k:Lnym;

    iput-object v0, p0, Lddr;->h:Lnym;

    .line 465
    iget-object v0, p1, Ldds;->l:Ljcn;

    iput-object v0, p0, Lddr;->i:Ljcn;

    .line 466
    iget-object v0, p1, Ldds;->m:Ljava/lang/String;

    iput-object v0, p0, Lddr;->j:Ljava/lang/String;

    .line 467
    iget-object v0, p1, Ldds;->n:Ljava/lang/String;

    iput-object v0, p0, Lddr;->k:Ljava/lang/String;

    .line 468
    iget-object v0, p1, Ldds;->o:Ljava/lang/String;

    iput-object v0, p0, Lddr;->l:Ljava/lang/String;

    .line 469
    iget-object v0, p1, Ldds;->p:Ljava/lang/String;

    iput-object v0, p0, Lddr;->m:Ljava/lang/String;

    .line 470
    iget-object v0, p1, Ldds;->q:Ljava/lang/String;

    iput-object v0, p0, Lddr;->n:Ljava/lang/String;

    .line 471
    iget-object v0, p1, Ldds;->r:Ljava/lang/String;

    iput-object v0, p0, Lddr;->o:Ljava/lang/String;

    .line 472
    iget-object v0, p1, Ldds;->s:Ljava/lang/Integer;

    iput-object v0, p0, Lddr;->p:Ljava/lang/Integer;

    .line 473
    iget-boolean v0, p1, Ldds;->t:Z

    iput-boolean v0, p0, Lddr;->q:Z

    .line 474
    iget-boolean v0, p1, Ldds;->u:Z

    iput-boolean v0, p0, Lddr;->r:Z

    .line 475
    iget-boolean v0, p1, Ldds;->v:Z

    iput-boolean v0, p0, Lddr;->s:Z

    .line 476
    iget-boolean v0, p1, Ldds;->w:Z

    iput-boolean v0, p0, Lddr;->t:Z

    .line 477
    iget-boolean v0, p1, Ldds;->x:Z

    iput-boolean v0, p0, Lddr;->u:Z

    .line 478
    iget-boolean v0, p1, Ldds;->y:Z

    iput-boolean v0, p0, Lddr;->v:Z

    .line 479
    iget-wide v0, p1, Ldds;->z:J

    iput-wide v0, p0, Lddr;->w:J

    .line 480
    iget-wide v0, p1, Ldds;->A:J

    iput-wide v0, p0, Lddr;->x:J

    .line 481
    iget-wide v0, p1, Ldds;->B:J

    iput-wide v0, p0, Lddr;->y:J

    .line 482
    iget-object v0, p1, Ldds;->C:Ljava/lang/String;

    iput-object v0, p0, Lddr;->z:Ljava/lang/String;

    .line 483
    iget-wide v0, p1, Ldds;->D:J

    iput-wide v0, p0, Lddr;->A:J

    .line 484
    return-void
.end method

.method private Z()Lnzi;
    .locals 3

    .prologue
    .line 714
    invoke-virtual {p0}, Lddr;->c()Ljuf;

    move-result-object v1

    .line 715
    const/4 v0, 0x0

    .line 716
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljuf;->h()Lnzi;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 717
    invoke-interface {v1}, Ljuf;->h()Lnzi;

    move-result-object v0

    .line 719
    :cond_0
    return-object v0
.end method

.method private a(Loxu;[B)Loxu;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">(TT;[B)TT;"
        }
    .end annotation

    .prologue
    .line 535
    if-eqz p2, :cond_0

    .line 537
    :try_start_0
    invoke-static {p1, p2}, Loxu;->a(Loxu;[B)Loxu;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 542
    :goto_0
    return-object v0

    .line 538
    :catch_0
    move-exception v0

    .line 539
    const-string v1, "1upMediaProxyBase"

    const-string v2, "Failed to deserialize EditInfo."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 542
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public B()Z
    .locals 4

    .prologue
    .line 912
    iget-object v0, p0, Lddr;->h:Lnym;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lddr;->x:J

    const-wide/32 v2, 0x10000000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 918
    iget-boolean v0, p0, Lddr;->v:Z

    return v0
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 923
    invoke-virtual {p0}, Lddr;->N()Z

    move-result v0

    return v0
.end method

.method public F()Z
    .locals 2

    .prologue
    .line 928
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v1, Ljac;->b:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public G()Z
    .locals 2

    .prologue
    .line 938
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v1, Ljac;->c:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()Z
    .locals 2

    .prologue
    .line 943
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v1, Ljac;->d:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public I()Z
    .locals 4

    .prologue
    .line 948
    iget-wide v0, p0, Lddr;->w:J

    const-wide/32 v2, 0x80000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()Z
    .locals 1

    .prologue
    .line 953
    iget-boolean v0, p0, Lddr;->q:Z

    return v0
.end method

.method public K()Z
    .locals 1

    .prologue
    .line 958
    iget-boolean v0, p0, Lddr;->r:Z

    return v0
.end method

.method public L()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 963
    iget-object v1, p0, Lddr;->C:Lctq;

    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lddr;->a:Landroid/os/Bundle;

    const-string v2, "selected_only"

    .line 964
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public M()Z
    .locals 1

    .prologue
    .line 969
    invoke-virtual {p0}, Lddr;->e()Lnzi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lddr;->c(Lnzi;)Z

    move-result v0

    return v0
.end method

.method public O()Z
    .locals 1

    .prologue
    .line 974
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 975
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v0

    iget-object v0, v0, Lnym;->r:Loae;

    if-eqz v0, :cond_0

    .line 976
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v0

    iget-object v0, v0, Lnym;->r:Loae;

    iget-object v0, v0, Loae;->c:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public P()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 981
    iget-object v2, p0, Lddr;->f:Lnzb;

    if-eqz v2, :cond_3

    .line 982
    iget-object v2, p0, Lddr;->f:Lnzb;

    iget v2, v2, Lnzb;->b:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lddr;->f:Lnzb;

    iget v2, v2, Lnzb;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    move v2, v0

    .line 985
    :goto_0
    if-eqz v2, :cond_2

    iget-object v2, p0, Lddr;->f:Lnzb;

    invoke-static {v2}, Llmz;->a(Lnzb;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 988
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 982
    goto :goto_0

    :cond_2
    move v0, v1

    .line 985
    goto :goto_1

    :cond_3
    move v0, v1

    .line 988
    goto :goto_1
.end method

.method public Q()Z
    .locals 1

    .prologue
    .line 993
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public R()I
    .locals 1

    .prologue
    .line 998
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 999
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v0

    iget-object v0, v0, Lnym;->r:Loae;

    if-eqz v0, :cond_0

    .line 1000
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v0

    iget-object v0, v0, Lnym;->r:Loae;

    iget-object v0, v0, Loae;->e:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1001
    :cond_0
    const/4 v0, 0x0

    .line 1004
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v0

    iget-object v0, v0, Lnym;->r:Loae;

    iget-object v0, v0, Loae;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public U()Z
    .locals 6

    .prologue
    .line 1009
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v0

    .line 1010
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v1

    invoke-virtual {v1}, Lizu;->g()Ljac;

    move-result-object v1

    sget-object v2, Ljac;->b:Ljac;

    if-ne v1, v2, :cond_0

    iget-wide v2, p0, Lddr;->w:J

    const-wide/16 v4, 0x100

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lnym;->G:Lnzi;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lnym;->G:Lnzi;

    iget-object v1, v1, Lnzi;->d:Lopf;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lnym;->G:Lnzi;

    iget-object v1, v1, Lnzi;->d:Lopf;

    iget v1, v1, Lopf;->b:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lnym;->G:Lnzi;

    iget-object v1, v1, Lnzi;->d:Lopf;

    iget-object v1, v1, Lopf;->c:Lpxr;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lddr;->E:Ljem;

    iget-object v0, v0, Lnym;->G:Lnzi;

    iget-object v0, v0, Lnzi;->d:Lopf;

    iget-object v0, v0, Lopf;->c:Lpxr;

    iget-object v0, v0, Lpxr;->a:Lood;

    .line 1017
    invoke-virtual {v1, v0}, Ljem;->a(Lood;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract V()Ldds;
.end method

.method public W()Ljava/lang/String;
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lddr;->m:Ljava/lang/String;

    return-object v0
.end method

.method public X()J
    .locals 2

    .prologue
    .line 801
    iget-wide v0, p0, Lddr;->x:J

    return-wide v0
.end method

.method public Y()Z
    .locals 2

    .prologue
    .line 933
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v1, Ljac;->a:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 632
    invoke-virtual {p0}, Lddr;->e()Lnzi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lddr;->a(Landroid/content/Context;ILnzi;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;ILnzi;Z)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 1054
    invoke-virtual {p0}, Lddr;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1055
    iget-object v0, p0, Lddr;->D:Lcnt;

    .line 1056
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v1

    iget-object v2, p0, Lddr;->z:Ljava/lang/String;

    .line 1055
    invoke-virtual {v0, v1, v2}, Lcnt;->a(Lizu;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1058
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v1

    iget-object v0, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v2, Lddt;->b:Lddt;

    .line 1059
    invoke-virtual {v0, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {p0}, Lddr;->W()Ljava/lang/String;

    move-result-object v3

    .line 1060
    iget-object v2, p0, Lddr;->a:Landroid/os/Bundle;

    const-string v4, "force_return_edit_list"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 1058
    if-eqz v0, :cond_2

    :goto_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    sget-object v5, Licc;->b:Ljava/lang/String;

    invoke-virtual {v2, p1, v5}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "account_id"

    invoke-virtual {v2, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v5, "photo_ref"

    invoke-virtual {v2, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "base_photo_media_ref"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "auto_enhanced_photo_url"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "save_photo_edits"

    invoke-virtual {v2, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "force_return_edit_list"

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz p3, :cond_1

    const-string v0, "edit_info"

    invoke-static {p3}, Loxu;->a(Loxu;)[B

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_1
    move-object v0, v2

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method protected a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1029
    if-nez p2, :cond_0

    .line 1030
    const/4 v0, 0x0

    .line 1045
    :goto_0
    return-object v0

    .line 1033
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ATTACH_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1034
    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1038
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1039
    const-string v1, "mimeType"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1041
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1042
    const-string v1, "com.android.camera.action.CROP"

    invoke-static {p1, p2, v1, p3}, Lcom/google/android/apps/photos/content/GooglePhotosImageProvider;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a()Lizu;
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v1, Lddt;->a:Lddt;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 759
    .line 760
    iget-object v1, p0, Lddr;->n:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 761
    iget-object v1, p0, Lddr;->n:Ljava/lang/String;

    .line 767
    :goto_0
    if-nez v1, :cond_2

    .line 780
    :goto_1
    return-object v0

    .line 762
    :cond_0
    iget-object v1, p0, Lddr;->f:Lnzb;

    if-eqz v1, :cond_1

    .line 763
    iget-object v1, p0, Lddr;->f:Lnzb;

    iget-object v1, v1, Lnzb;->e:Ljava/lang/String;

    goto :goto_0

    .line 764
    :cond_1
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 765
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v1

    invoke-virtual {v1}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 771
    :cond_2
    invoke-static {v1}, Ljbd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 774
    const/16 v0, 0x246

    invoke-static {v1, v0, p1}, Ljbd;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 780
    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public a(Lnzi;)Ljuf;
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0}, Lddr;->c()Ljuf;

    move-result-object v0

    .line 660
    if-nez v0, :cond_0

    .line 661
    invoke-virtual {p0}, Lddr;->d()Ljuf;

    move-result-object v0

    .line 663
    :cond_0
    invoke-interface {v0, p1}, Ljuf;->a(Lnzi;)Ljuf;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 592
    const-class v0, Lctz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lddr;->B:Lctz;

    .line 593
    const-class v0, Lfyp;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 594
    const-class v0, Lctq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    iput-object v0, p0, Lddr;->C:Lctq;

    .line 595
    const-class v0, Lcnt;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    iput-object v0, p0, Lddr;->D:Lcnt;

    .line 596
    invoke-static {p1}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    iput-object v0, p0, Lddr;->E:Ljem;

    .line 597
    return-void
.end method

.method public b(Lnzi;)Lddl;
    .locals 1

    .prologue
    .line 668
    invoke-virtual {p0}, Lddr;->V()Ldds;

    move-result-object v0

    .line 669
    invoke-virtual {v0, p0}, Ldds;->a(Lddl;)Ldds;

    move-result-object v0

    .line 670
    invoke-virtual {v0, p1}, Ldds;->b(Lnzi;)Ldds;

    move-result-object v0

    .line 671
    invoke-virtual {v0}, Ldds;->a()Lddl;

    move-result-object v0

    return-object v0
.end method

.method public b()Lizu;
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v1, Lddt;->d:Lddt;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    return-object v0
.end method

.method public c(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 637
    iget-object v0, p0, Lddr;->a:Landroid/os/Bundle;

    .line 638
    const-string v1, "view_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 639
    iget v1, p0, Lddr;->b:I

    .line 640
    invoke-static {p1, v1}, Leyq;->h(Landroid/content/Context;I)Leyx;

    move-result-object v1

    .line 642
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v2

    invoke-virtual {v1, v2}, Leyx;->a(Lizu;)Leyx;

    move-result-object v1

    .line 643
    invoke-virtual {v1, v0}, Leyx;->a(Ljava/lang/String;)Leyx;

    move-result-object v0

    iget-object v1, p0, Lddr;->k:Ljava/lang/String;

    .line 644
    invoke-virtual {v0, v1}, Leyx;->b(Ljava/lang/String;)Leyx;

    move-result-object v0

    const/4 v1, 0x1

    .line 645
    invoke-virtual {v0, v1}, Leyx;->a(I)Leyx;

    move-result-object v0

    .line 646
    invoke-virtual {v0}, Leyx;->a()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected final c(Lnzi;)Z
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1064
    if-eqz p1, :cond_2

    .line 1065
    if-eqz p1, :cond_0

    iget-object v2, p1, Lnzi;->b:Lpla;

    if-nez v2, :cond_4

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 1066
    invoke-static {p1}, Ljub;->a(Lnzi;)Z

    move-result v2

    if-nez v2, :cond_c

    move v2, v0

    :cond_1
    :goto_1
    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    return v0

    .line 1065
    :cond_4
    if-eqz p1, :cond_5

    iget-object v2, p1, Lnzi;->b:Lpla;

    if-nez v2, :cond_7

    :cond_5
    move v2, v1

    :goto_2
    if-eqz v2, :cond_b

    iget-object v2, p1, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->a:[Lpme;

    if-eqz v2, :cond_6

    iget-object v2, p1, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->a:[Lpme;

    array-length v2, v2

    if-nez v2, :cond_b

    :cond_6
    move v2, v1

    goto :goto_0

    :cond_7
    iget-object v2, p1, Lnzi;->b:Lpla;

    iget v2, v2, Lpla;->d:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_8

    iget-object v2, p1, Lnzi;->b:Lpla;

    iget v2, v2, Lpla;->d:I

    if-eqz v2, :cond_8

    move v2, v0

    goto :goto_2

    :cond_8
    iget-object v2, p1, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->c:Lpjd;

    if-eqz v2, :cond_a

    iget-object v2, p1, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    if-eqz v2, :cond_a

    iget-object v2, p1, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    iget-object v2, v2, Lpsi;->a:Ljava/lang/Float;

    invoke-static {v2, v4}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-nez v2, :cond_9

    iget-object v2, p1, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    iget-object v2, v2, Lpsi;->c:Ljava/lang/Float;

    invoke-static {v2, v5}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    cmpl-float v2, v2, v5

    if-nez v2, :cond_9

    iget-object v2, p1, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    iget-object v2, v2, Lpsi;->b:Ljava/lang/Float;

    invoke-static {v2, v4}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-nez v2, :cond_9

    iget-object v2, p1, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    iget-object v2, v2, Lpsi;->d:Ljava/lang/Float;

    invoke-static {v2, v5}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    cmpl-float v2, v2, v5

    if-eqz v2, :cond_a

    :cond_9
    move v2, v0

    goto :goto_2

    :cond_a
    move v2, v1

    goto :goto_2

    :cond_b
    move v2, v0

    goto/16 :goto_0

    .line 1066
    :cond_c
    iget-object v2, p1, Lnzi;->b:Lpla;

    iget-object v4, v2, Lpla;->a:[Lpme;

    array-length v5, v4

    move v3, v0

    move v2, v0

    :goto_3
    if-ge v3, v5, :cond_1

    aget-object v2, v4, v3

    invoke-static {v2, v1}, Ljub;->a(Lpme;I)Z

    move-result v2

    if-eqz v2, :cond_d

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_3

    :cond_d
    move v2, v0

    goto/16 :goto_1
.end method

.method public d(Landroid/content/Context;)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 651
    invoke-virtual {p0}, Lddr;->h()Lnzb;

    move-result-object v0

    invoke-static {v0}, Lnzb;->a(Loxu;)[B

    move-result-object v0

    .line 652
    iget v1, p0, Lddr;->b:I

    .line 654
    invoke-virtual {p0}, Lddr;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lddr;->o()J

    move-result-wide v4

    .line 653
    new-instance v3, Landroid/content/Intent;

    const-class v6, Lcom/google/android/apps/plus/phone/VideoViewActivity;

    invoke-direct {v3, p1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "account_id"

    invoke-virtual {v3, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "owner_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v3, v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "video_data"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    return-object v3
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 547
    const/4 v0, 0x0

    return v0
.end method

.method public e()Lnzi;
    .locals 1

    .prologue
    .line 676
    invoke-direct {p0}, Lddr;->Z()Lnzi;

    move-result-object v0

    .line 679
    if-eqz v0, :cond_0

    .line 685
    :goto_0
    return-object v0

    .line 682
    :cond_0
    iget-object v0, p0, Lddr;->d:Lnzi;

    goto :goto_0
.end method

.method public f()Lnzi;
    .locals 2

    .prologue
    .line 690
    invoke-direct {p0}, Lddr;->Z()Lnzi;

    move-result-object v0

    .line 693
    if-eqz v0, :cond_0

    iget-object v1, p0, Lddr;->d:Lnzi;

    invoke-static {v1, v0}, Loxu;->a(Loxu;Loxu;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 710
    :goto_0
    return-object v0

    .line 698
    :cond_0
    iget-object v0, p0, Lddr;->d:Lnzi;

    iget-object v1, p0, Lddr;->e:Lnzi;

    invoke-static {v0, v1}, Loxu;->a(Loxu;Loxu;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 703
    iget-object v0, p0, Lddr;->d:Lnzi;

    goto :goto_0

    .line 708
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lnym;
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lddr;->h:Lnym;

    return-object v0
.end method

.method public h()Lnzb;
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lddr;->f:Lnzb;

    return-object v0
.end method

.method public i()Lnzb;
    .locals 1

    .prologue
    .line 734
    iget-object v0, p0, Lddr;->g:Lnzb;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lddr;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lddr;->k:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lddr;->l:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 786
    iget-object v0, p0, Lddr;->o:Ljava/lang/String;

    return-object v0
.end method

.method public n()J
    .locals 2

    .prologue
    .line 796
    iget-wide v0, p0, Lddr;->w:J

    return-wide v0
.end method

.method public o()J
    .locals 2

    .prologue
    .line 806
    iget-wide v0, p0, Lddr;->y:J

    return-wide v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811
    iget-object v0, p0, Lddr;->z:Ljava/lang/String;

    return-object v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 816
    iget-wide v0, p0, Lddr;->A:J

    return-wide v0
.end method

.method public final r()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 821
    invoke-virtual {p0}, Lddr;->Y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 822
    invoke-virtual {p0}, Lddr;->S()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Lddr;->T()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Licc;->a(Ljava/lang/Integer;Ljava/lang/Integer;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 823
    :goto_0
    invoke-virtual {p0}, Lddr;->U()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lddr;->E:Ljem;

    invoke-virtual {v3}, Ljem;->j()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v3

    invoke-virtual {v3}, Ljfb;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v1

    :goto_1
    if-eqz v3, :cond_3

    move v3, v1

    .line 825
    :goto_2
    iget-boolean v4, p0, Lddr;->t:Z

    if-eqz v4, :cond_4

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_4

    iget-wide v4, p0, Lddr;->x:J

    const-wide v6, 0x400000000L

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_4

    .line 828
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 829
    invoke-virtual {p0}, Lddr;->I()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lddr;->a:Landroid/os/Bundle;

    const-string v5, "prevent_edit"

    .line 830
    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_4

    if-nez v0, :cond_0

    if-eqz v3, :cond_4

    :cond_0
    :goto_3
    return v1

    :cond_1
    move v0, v2

    .line 822
    goto :goto_0

    :cond_2
    move v3, v2

    .line 823
    goto :goto_1

    :cond_3
    move v3, v2

    goto :goto_2

    :cond_4
    move v1, v2

    .line 830
    goto :goto_3
.end method

.method public s()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 836
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v2

    iget-object v2, v2, Lnym;->m:Lnzb;

    if-eqz v2, :cond_0

    .line 837
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v2

    iget-object v2, v2, Lnym;->m:Lnzb;

    iget v2, v2, Lnzb;->b:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    move v2, v0

    .line 838
    :goto_0
    if-nez v2, :cond_1

    iget-wide v2, p0, Lddr;->x:J

    const-wide v4, 0x1000000000L

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 837
    goto :goto_0

    :cond_1
    move v0, v1

    .line 838
    goto :goto_1
.end method

.method public t()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 844
    iget-wide v2, p0, Lddr;->x:J

    const-wide v4, 0x800000000L

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    iget-wide v2, p0, Lddr;->x:J

    const-wide/32 v4, 0x20000000

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lddr;->a:Landroid/os/Bundle;

    const-string v2, "prevent_share"

    .line 846
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public u()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 851
    iget-wide v2, p0, Lddr;->x:J

    const-wide v4, 0x200000000L

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lddr;->w:J

    const-wide/32 v4, 0x80000

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    iget-object v1, p0, Lddr;->a:Landroid/os/Bundle;

    const-string v2, "prevent_delete"

    .line 853
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public v()Z
    .locals 2

    .prologue
    .line 858
    iget-boolean v0, p0, Lddr;->u:Z

    if-eqz v0, :cond_0

    .line 859
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Ljac;->a:Ljac;

    .line 860
    invoke-virtual {p0}, Lddr;->a()Lizu;

    move-result-object v1

    invoke-virtual {v1}, Lizu;->g()Ljac;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 861
    invoke-static {}, Lfb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 866
    iget-wide v2, p0, Lddr;->x:J

    const-wide/32 v4, 0x4000000

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lddr;->a:Landroid/os/Bundle;

    const-string v2, "disable_photo_comments"

    .line 867
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 552
    iget-object v0, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v4, Lddt;->a:Lddt;

    invoke-virtual {v0, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 553
    iget-object v0, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v4, Lddt;->d:Lddt;

    invoke-virtual {v0, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 554
    iget-object v0, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v4, Lddt;->b:Lddt;

    invoke-virtual {v0, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 555
    iget-object v0, p0, Lddr;->c:Ljava/util/EnumMap;

    sget-object v4, Lddt;->c:Lddt;

    invoke-virtual {v0, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 556
    iget-object v0, p0, Lddr;->i:Ljcn;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 557
    iget-object v0, p0, Lddr;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 558
    iget-object v0, p0, Lddr;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lddr;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 560
    iget-object v0, p0, Lddr;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 561
    iget-object v0, p0, Lddr;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 562
    iget-object v0, p0, Lddr;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lddr;->p:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 564
    iget-boolean v0, p0, Lddr;->q:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 565
    iget-boolean v0, p0, Lddr;->r:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 566
    iget-boolean v0, p0, Lddr;->s:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 567
    iget-boolean v0, p0, Lddr;->t:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 568
    iget-boolean v0, p0, Lddr;->u:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 569
    iget-boolean v0, p0, Lddr;->v:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 570
    iget-wide v0, p0, Lddr;->w:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 571
    iget-wide v0, p0, Lddr;->x:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 572
    iget-wide v0, p0, Lddr;->y:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 573
    iget-object v0, p0, Lddr;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 574
    iget-wide v0, p0, Lddr;->A:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 576
    iget-object v0, p0, Lddr;->h:Lnym;

    if-nez v0, :cond_6

    move-object v0, v3

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 577
    iget-object v0, p0, Lddr;->d:Lnzi;

    if-nez v0, :cond_7

    move-object v0, v3

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 578
    iget-object v0, p0, Lddr;->e:Lnzi;

    if-nez v0, :cond_8

    move-object v0, v3

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 580
    iget-object v0, p0, Lddr;->f:Lnzb;

    if-nez v0, :cond_9

    move-object v0, v3

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 581
    iget-object v0, p0, Lddr;->g:Lnzb;

    if-nez v0, :cond_a

    :goto_a
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 583
    return-void

    :cond_0
    move v0, v2

    .line 564
    goto :goto_0

    :cond_1
    move v0, v2

    .line 565
    goto :goto_1

    :cond_2
    move v0, v2

    .line 566
    goto :goto_2

    :cond_3
    move v0, v2

    .line 567
    goto :goto_3

    :cond_4
    move v0, v2

    .line 568
    goto :goto_4

    :cond_5
    move v1, v2

    .line 569
    goto :goto_5

    .line 576
    :cond_6
    iget-object v0, p0, Lddr;->h:Lnym;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_6

    .line 577
    :cond_7
    iget-object v0, p0, Lddr;->d:Lnzi;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_7

    .line 578
    :cond_8
    iget-object v0, p0, Lddr;->e:Lnzi;

    .line 579
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_8

    .line 580
    :cond_9
    iget-object v0, p0, Lddr;->f:Lnzb;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_9

    .line 581
    :cond_a
    iget-object v0, p0, Lddr;->g:Lnzb;

    .line 582
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v3

    goto :goto_a
.end method

.method public x()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 872
    iget-boolean v1, p0, Lddr;->q:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lddr;->a:Landroid/os/Bundle;

    const-string v2, "disable_photo_comments"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public y()Z
    .locals 4

    .prologue
    .line 877
    iget-wide v0, p0, Lddr;->x:J

    const-wide/32 v2, 0x8000000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 882
    iget-wide v4, p0, Lddr;->x:J

    const-wide/32 v6, 0x40000000

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    move v0, v1

    .line 886
    :goto_0
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 887
    invoke-virtual {p0}, Lddr;->g()Lnym;

    move-result-object v3

    iget-object v6, v3, Lnym;->g:[Lnys;

    .line 888
    if-eqz v6, :cond_1

    array-length v3, v6

    if-lez v3, :cond_1

    .line 889
    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    move v5, v3

    move v4, v2

    move v3, v2

    :goto_1
    if-ltz v5, :cond_2

    .line 890
    aget-object v7, v6, v5

    iget v7, v7, Lnys;->d:I

    packed-switch v7, :pswitch_data_0

    .line 889
    :goto_2
    :pswitch_0
    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 882
    goto :goto_0

    :pswitch_1
    move v3, v1

    .line 893
    goto :goto_2

    :pswitch_2
    move v4, v1

    .line 899
    goto :goto_2

    :cond_1
    move v3, v2

    move v4, v2

    .line 907
    :cond_2
    if-eqz v0, :cond_3

    if-nez v4, :cond_4

    :cond_3
    if-eqz v3, :cond_5

    :cond_4
    :goto_3
    return v1

    :cond_5
    move v1, v2

    goto :goto_3

    .line 890
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
