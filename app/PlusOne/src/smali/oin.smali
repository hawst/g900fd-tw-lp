.class public final Loin;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3708
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3711
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loin;->a:[Ljava/lang/String;

    .line 3708
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3727
    .line 3728
    iget-object v1, p0, Loin;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Loin;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 3730
    iget-object v2, p0, Loin;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 3732
    invoke-static {v4}, Loxo;->b(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 3730
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3735
    :cond_0
    iget-object v0, p0, Loin;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 3737
    :cond_1
    iget-object v1, p0, Loin;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3738
    iput v0, p0, Loin;->ai:I

    .line 3739
    return v0
.end method

.method public a(Loxn;)Loin;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3747
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3748
    sparse-switch v0, :sswitch_data_0

    .line 3752
    iget-object v1, p0, Loin;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3753
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loin;->ah:Ljava/util/List;

    .line 3756
    :cond_1
    iget-object v1, p0, Loin;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3758
    :sswitch_0
    return-object p0

    .line 3763
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 3764
    iget-object v0, p0, Loin;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 3765
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 3766
    iget-object v2, p0, Loin;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3767
    iput-object v1, p0, Loin;->a:[Ljava/lang/String;

    .line 3768
    :goto_1
    iget-object v1, p0, Loin;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 3769
    iget-object v1, p0, Loin;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3770
    invoke-virtual {p1}, Loxn;->a()I

    .line 3768
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3773
    :cond_2
    iget-object v1, p0, Loin;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 3748
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3716
    iget-object v0, p0, Loin;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3717
    iget-object v1, p0, Loin;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 3718
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 3717
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3721
    :cond_0
    iget-object v0, p0, Loin;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3723
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3704
    invoke-virtual {p0, p1}, Loin;->a(Loxn;)Loin;

    move-result-object v0

    return-object v0
.end method
