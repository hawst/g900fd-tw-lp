.class public abstract Lfvi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:I


# instance fields
.field public a:Landroid/view/ViewGroup;

.field private c:Z

.field private d:Z

.field private e:F

.field private f:F

.field private final g:Landroid/content/Context;

.field private h:Lfvq;

.field private i:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, -0x1

    sput v0, Lfvi;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lfvq;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean v0, p0, Lfvi;->c:Z

    .line 46
    iput-boolean v0, p0, Lfvi;->d:Z

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lfvi;->e:F

    .line 48
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lfvi;->f:F

    .line 94
    new-instance v0, Lfvj;

    invoke-direct {v0, p0}, Lfvj;-><init>(Lfvi;)V

    iput-object v0, p0, Lfvi;->i:Landroid/view/View$OnTouchListener;

    .line 64
    iput-object p1, p0, Lfvi;->g:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lfvi;->a:Landroid/view/ViewGroup;

    .line 66
    iput-object p3, p0, Lfvi;->h:Lfvq;

    .line 68
    sget v0, Lfvi;->b:I

    if-gez v0, :cond_0

    .line 69
    iget-object v0, p0, Lfvi;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    sput v0, Lfvi;->b:I

    .line 71
    :cond_0
    return-void
.end method

.method private a(Landroid/animation/Animator;Ljava/lang/Runnable;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 508
    if-eqz p2, :cond_0

    .line 509
    new-instance v0, Lfvn;

    invoke-direct {v0, p2}, Lfvn;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 516
    :cond_0
    return-void
.end method

.method private a(Landroid/view/animation/Animation;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 519
    if-eqz p2, :cond_0

    .line 520
    new-instance v0, Lfvo;

    invoke-direct {v0, p2}, Lfvo;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 527
    :cond_0
    return-void
.end method

.method static synthetic a(Lfvi;Landroid/view/View;F)V
    .locals 5

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 36
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {}, Lfvi;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    sub-float v0, v3, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v1, p2, p2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput p2, p0, Lfvi;->e:F

    sub-float v0, v3, v0

    iput v0, p0, Lfvi;->f:F

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    iget v2, p0, Lfvi;->f:F

    iget v3, p0, Lfvi;->f:F

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->setFillEnabled(Z)V

    invoke-virtual {p1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private static a()Z
    .locals 2

    .prologue
    .line 91
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 553
    iget-object v1, p0, Lfvi;->a:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 554
    iget-object v1, p0, Lfvi;->a:Landroid/view/ViewGroup;

    const v2, 0x7f1000b3

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    const/4 v0, 0x1

    .line 556
    :cond_0
    return v0
.end method

.method static synthetic a(Lfvi;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lfvi;->d:Z

    return v0
.end method

.method static synthetic a(Lfvi;Landroid/view/View;)Z
    .locals 4

    .prologue
    const v3, 0x7f1000b3

    const/4 v1, 0x2

    .line 36
    iget-object v0, p0, Lfvi;->a:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfvi;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "SwipeToDismissHelper"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; obtainListViewLock(): obtained"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lfvi;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3, p1}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const-string v0, "SwipeToDismissHelper"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfvi;->a:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lfvi;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; obtainListViewLock(): already have it!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; obtainListViewLock(): denied!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lfvi;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lfvi;->c:Z

    return p1
.end method

.method static synthetic b(Lfvi;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lfvi;->c:Z

    return v0
.end method

.method static synthetic b(Lfvi;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lfvi;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lfvi;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lfvi;->d:Z

    return p1
.end method

.method static synthetic c(Lfvi;)Lfvq;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lfvi;->h:Lfvq;

    return-object v0
.end method

.method static synthetic c(Lfvi;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const v4, 0x7f1000b3

    const/4 v2, 0x2

    .line 36
    iget-object v0, p0, Lfvi;->a:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfvi;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    if-ne v0, p1, :cond_3

    const-string v0, "SwipeToDismissHelper"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; releaseListViewLock(): released"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lfvi;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4, v1}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    const-string v0, "SwipeToDismissHelper"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; releaseListViewLock(): not owner!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method static synthetic d(Lfvi;Landroid/view/View;)F
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lfvi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e()I
    .locals 1

    .prologue
    .line 36
    sget v0, Lfvi;->b:I

    return v0
.end method

.method public static g(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 347
    if-eqz p0, :cond_0

    .line 348
    invoke-static {}, Lfvi;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349
    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->clearAnimation()V

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/widget/ListView;ILjava/util/HashMap;)Ljava/lang/Float;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Float;",
            ">;)",
            "Ljava/lang/Float;"
        }
    .end annotation
.end method

.method protected a(Landroid/view/View;FFFFLjava/lang/Runnable;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const-wide/16 v4, 0x96

    const/4 v2, 0x0

    .line 464
    invoke-static {}, Lfvi;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 465
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    cmpl-float v0, p2, p3

    if-eqz v0, :cond_0

    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v1, v6, [F

    aput p2, v1, v2

    aput p3, v1, v3

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    invoke-direct {p0, v0, p6}, Lfvi;->a(Landroid/animation/Animator;Ljava/lang/Runnable;)V

    const/4 p6, 0x0

    :cond_0
    cmpl-float v0, p4, p5

    if-eqz v0, :cond_1

    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v1, v6, [F

    aput p4, v1, v2

    aput p5, v1, v3

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    invoke-direct {p0, v0, p6}, Lfvi;->a(Landroid/animation/Animator;Ljava/lang/Runnable;)V

    .line 469
    :cond_1
    :goto_0
    iput-boolean v2, p0, Lfvi;->c:Z

    .line 470
    iput-boolean v2, p0, Lfvi;->d:Z

    .line 471
    return-void

    .line 467
    :cond_2
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, p2, p3, p4, p5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    if-eqz p6, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-direct {p0, v0, p6}, Lfvi;->a(Landroid/view/animation/Animation;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;FJZ)V
    .locals 7

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 254
    iput-boolean v5, p0, Lfvi;->d:Z

    .line 255
    invoke-static {}, Lfvi;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 256
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz p5, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lfvk;

    invoke-direct {v1, p0, p5, p1}, Lfvk;-><init>(Lfvi;ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 260
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 256
    goto :goto_0

    .line 258
    :cond_1
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget v3, p0, Lfvi;->e:F

    invoke-direct {v2, v3, p2, v0, v0}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    new-instance v3, Landroid/view/animation/AlphaAnimation;

    iget v4, p0, Lfvi;->f:F

    if-eqz p5, :cond_2

    :goto_2
    invoke-direct {v3, v4, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    new-instance v0, Landroid/view/animation/AnimationSet;

    invoke-direct {v0, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v0, p3, p4}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    invoke-virtual {v0, v5}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    invoke-virtual {v0, v5}, Landroid/view/animation/AnimationSet;->setFillEnabled(Z)V

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v1, Lfvl;

    invoke-direct {v1, p0, p5, p1}, Lfvl;-><init>(Lfvi;ZLandroid/view/View;)V

    invoke-direct {p0, v0, v1}, Lfvi;->a(Landroid/view/animation/Animation;Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public abstract a(Landroid/view/View;Z)V
.end method

.method public abstract a(Landroid/view/ViewGroup;)V
.end method

.method public a(Landroid/widget/ListView;Ljava/util/HashMap;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Float;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 369
    invoke-virtual {p2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370
    iput-boolean v1, p0, Lfvi;->d:Z

    .line 371
    iput-boolean v1, p0, Lfvi;->c:Z

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 375
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    .line 376
    if-eqz v2, :cond_0

    .line 380
    new-instance v0, Lfvm;

    move-object v1, p0

    move-object v3, p1

    move v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lfvm;-><init>(Lfvi;Landroid/view/ViewTreeObserver;Landroid/widget/ListView;ZLjava/util/HashMap;)V

    invoke-virtual {v2, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0
.end method

.method public abstract a(F)Z
.end method

.method public a(Z)Z
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lfvi;->c:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    return p1
.end method

.method public b()Landroid/view/View$OnTouchListener;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lfvi;->i:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfvi;->d:Z

    .line 79
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 530
    iget-object v0, p0, Lfvi;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1000b3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    .line 531
    iput-boolean v3, p0, Lfvi;->d:Z

    .line 532
    iput-boolean v3, p0, Lfvi;->c:Z

    .line 533
    return-void
.end method

.method public abstract d(Landroid/view/View;)Z
.end method

.method public e(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 82
    if-eqz p1, :cond_0

    .line 83
    iget-object v0, p0, Lfvi;->i:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 85
    :cond_0
    return-void
.end method

.method public f(Landroid/view/View;)F
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 250
    invoke-static {}, Lfvi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method
