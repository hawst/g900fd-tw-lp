.class public Lms;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lng;


# static fields
.field private static final a:[I

.field private static final b:Z

.field private static w:Lmv;


# instance fields
.field private final c:Lmu;

.field private d:I

.field private e:I

.field private f:F

.field private g:Landroid/graphics/Paint;

.field private final h:Lok;

.field private final i:Lok;

.field private final j:Lnc;

.field private final k:Lnc;

.field private l:I

.field private m:Z

.field private n:Z

.field private o:I

.field private p:I

.field private q:Z

.field private r:Lmy;

.field private s:F

.field private t:F

.field private u:Ljava/lang/Object;

.field private v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 159
    new-array v2, v0, [I

    const v3, 0x10100b3

    aput v3, v2, v1

    sput-object v2, Lms;->a:[I

    .line 164
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    :goto_0
    sput-boolean v0, Lms;->b:Z

    .line 303
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 304
    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 305
    new-instance v0, Lmw;

    invoke-direct {v0}, Lmw;-><init>()V

    sput-object v0, Lms;->w:Lmv;

    .line 309
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 164
    goto :goto_0

    .line 307
    :cond_1
    new-instance v0, Lmx;

    invoke-direct {v0}, Lmx;-><init>()V

    sput-object v0, Lms;->w:Lmv;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lms;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 315
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lms;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 319
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    .line 322
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 166
    new-instance v0, Lmu;

    invoke-direct {v0}, Lmu;-><init>()V

    iput-object v0, p0, Lms;->c:Lmu;

    .line 171
    const/high16 v0, -0x67000000

    iput v0, p0, Lms;->e:I

    .line 173
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lms;->g:Landroid/graphics/Paint;

    .line 181
    iput-boolean v3, p0, Lms;->n:Z

    .line 323
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lms;->setDescendantFocusability(I)V

    .line 324
    invoke-virtual {p0}, Lms;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 325
    const/high16 v1, 0x42800000    # 64.0f

    mul-float/2addr v1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lms;->d:I

    .line 326
    const/high16 v1, 0x43c80000    # 400.0f

    mul-float/2addr v0, v1

    .line 328
    new-instance v1, Lnc;

    const/4 v2, 0x3

    invoke-direct {v1, p0, v2}, Lnc;-><init>(Lms;I)V

    iput-object v1, p0, Lms;->j:Lnc;

    .line 329
    new-instance v1, Lnc;

    const/4 v2, 0x5

    invoke-direct {v1, p0, v2}, Lnc;-><init>(Lms;I)V

    iput-object v1, p0, Lms;->k:Lnc;

    .line 331
    iget-object v1, p0, Lms;->j:Lnc;

    invoke-static {p0, v4, v1}, Lok;->a(Landroid/view/ViewGroup;FLon;)Lok;

    move-result-object v1

    iput-object v1, p0, Lms;->h:Lok;

    .line 332
    iget-object v1, p0, Lms;->h:Lok;

    invoke-virtual {v1, v3}, Lok;->a(I)V

    .line 333
    iget-object v1, p0, Lms;->h:Lok;

    invoke-virtual {v1, v0}, Lok;->a(F)V

    .line 334
    iget-object v1, p0, Lms;->j:Lnc;

    iget-object v2, p0, Lms;->h:Lok;

    invoke-virtual {v1, v2}, Lnc;->a(Lok;)V

    .line 336
    iget-object v1, p0, Lms;->k:Lnc;

    invoke-static {p0, v4, v1}, Lok;->a(Landroid/view/ViewGroup;FLon;)Lok;

    move-result-object v1

    iput-object v1, p0, Lms;->i:Lok;

    .line 337
    iget-object v1, p0, Lms;->i:Lok;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lok;->a(I)V

    .line 338
    iget-object v1, p0, Lms;->i:Lok;

    invoke-virtual {v1, v0}, Lok;->a(F)V

    .line 339
    iget-object v0, p0, Lms;->k:Lnc;

    iget-object v1, p0, Lms;->i:Lok;

    invoke-virtual {v0, v1}, Lnc;->a(Lok;)V

    .line 342
    invoke-virtual {p0, v3}, Lms;->setFocusableInTouchMode(Z)V

    .line 344
    invoke-static {p0, v3}, Liu;->c(Landroid/view/View;I)V

    .line 347
    new-instance v0, Lmt;

    invoke-direct {v0, p0}, Lmt;-><init>(Lms;)V

    invoke-static {p0, v0}, Liu;->a(Landroid/view/View;Lgw;)V

    .line 348
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ljo;->a(Landroid/view/ViewGroup;Z)V

    .line 349
    invoke-static {p0}, Liu;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    sget-object v0, Lms;->w:Lmv;

    invoke-interface {v0, p0}, Lmv;->a(Landroid/view/View;)V

    .line 352
    :cond_0
    return-void
.end method

.method static synthetic a(Lms;)Landroid/view/View;
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lms;->f()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 666
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v1

    .line 667
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 668
    invoke-virtual {p0, v0}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 669
    if-nez p2, :cond_0

    invoke-virtual {p0, v2}, Lms;->g(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    if-ne v2, p1, :cond_2

    .line 673
    :cond_1
    const/4 v3, 0x1

    invoke-static {v2, v3}, Liu;->c(Landroid/view/View;I)V

    .line 667
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 676
    :cond_2
    const/4 v3, 0x4

    invoke-static {v2, v3}, Liu;->c(Landroid/view/View;I)V

    goto :goto_1

    .line 680
    :cond_3
    return-void
.end method

.method static d(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 766
    and-int/lit8 v0, p0, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 767
    const-string v0, "LEFT"

    .line 772
    :goto_0
    return-object v0

    .line 769
    :cond_0
    and-int/lit8 v0, p0, 0x5

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 770
    const-string v0, "RIGHT"

    goto :goto_0

    .line 772
    :cond_1
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d()[I
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lms;->a:[I

    return-object v0
.end method

.method static synthetic e()Z
    .locals 1

    .prologue
    .line 86
    sget-boolean v0, Lms;->b:Z

    return v0
.end method

.method private f()Landroid/view/View;
    .locals 4

    .prologue
    .line 1444
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v2

    .line 1445
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1446
    invoke-virtual {p0, v1}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1447
    invoke-virtual {p0, v0}, Lms;->g(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v0}, Lms;->k(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1451
    :goto_1
    return-object v0

    .line 1445
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1451
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic l(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 86
    invoke-static {p0}, Liu;->d(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Liu;->d(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 545
    invoke-virtual {p0, p1}, Lms;->e(Landroid/view/View;)I

    move-result v0

    .line 546
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 547
    iget v0, p0, Lms;->o:I

    .line 551
    :goto_0
    return v0

    .line 548
    :cond_0
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 549
    iget v0, p0, Lms;->p:I

    goto :goto_0

    .line 551
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a()Landroid/view/View;
    .locals 4

    .prologue
    .line 717
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v3

    .line 718
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 719
    invoke-virtual {p0, v2}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 720
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget-boolean v0, v0, Lmz;->d:Z

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 724
    :goto_1
    return-object v0

    .line 718
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 724
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 437
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lms;->a(II)V

    .line 438
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lms;->a(II)V

    .line 439
    return-void
.end method

.method public a(II)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 461
    invoke-static {p0}, Liu;->e(Landroid/view/View;)I

    move-result v0

    invoke-static {p2, v0}, Lhr;->a(II)I

    move-result v1

    .line 463
    if-ne v1, v2, :cond_3

    .line 464
    iput p1, p0, Lms;->o:I

    .line 468
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 470
    if-ne v1, v2, :cond_4

    iget-object v0, p0, Lms;->h:Lok;

    .line 471
    :goto_1
    invoke-virtual {v0}, Lok;->e()V

    .line 473
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 488
    :cond_2
    :goto_2
    return-void

    .line 465
    :cond_3
    const/4 v0, 0x5

    if-ne v1, v0, :cond_0

    .line 466
    iput p1, p0, Lms;->p:I

    goto :goto_0

    .line 470
    :cond_4
    iget-object v0, p0, Lms;->i:Lok;

    goto :goto_1

    .line 475
    :pswitch_0
    invoke-virtual {p0, v1}, Lms;->c(I)Landroid/view/View;

    move-result-object v0

    .line 476
    if-eqz v0, :cond_2

    .line 477
    invoke-virtual {p0, v0}, Lms;->h(Landroid/view/View;)V

    goto :goto_2

    .line 481
    :pswitch_1
    invoke-virtual {p0, v1}, Lms;->c(I)Landroid/view/View;

    move-result-object v0

    .line 482
    if-eqz v0, :cond_2

    .line 483
    invoke-virtual {p0, v0}, Lms;->i(Landroid/view/View;)V

    goto :goto_2

    .line 473
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(ILandroid/view/View;)V
    .locals 3

    .prologue
    .line 509
    invoke-virtual {p0, p2}, Lms;->g(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 510
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a drawer with appropriate layout_gravity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 513
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget v0, v0, Lmz;->a:I

    .line 514
    invoke-virtual {p0, p1, v0}, Lms;->a(II)V

    .line 515
    return-void
.end method

.method a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lms;->r:Lmy;

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lms;->r:Lmy;

    invoke-interface {v0, p1, p2}, Lmy;->a(Landroid/view/View;F)V

    .line 686
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 360
    iput-object p1, p0, Lms;->u:Ljava/lang/Object;

    .line 361
    iput-boolean p2, p0, Lms;->v:Z

    .line 362
    if-nez p2, :cond_0

    invoke-virtual {p0}, Lms;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lms;->setWillNotDraw(Z)V

    .line 363
    invoke-virtual {p0}, Lms;->requestLayout()V

    .line 364
    return-void

    .line 362
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmy;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lms;->r:Lmy;

    .line 421
    return-void
.end method

.method a(Z)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1226
    .line 1227
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v4

    move v2, v3

    move v1, v3

    .line 1228
    :goto_0
    if-ge v2, v4, :cond_3

    .line 1229
    invoke-virtual {p0, v2}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1230
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    .line 1232
    invoke-virtual {p0, v5}, Lms;->g(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz p1, :cond_0

    iget-boolean v6, v0, Lmz;->c:Z

    if-eqz v6, :cond_1

    .line 1233
    :cond_0
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 1238
    const/4 v7, 0x3

    invoke-virtual {p0, v5, v7}, Lms;->a(Landroid/view/View;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1239
    iget-object v7, p0, Lms;->h:Lok;

    neg-int v6, v6

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v7, v5, v6, v8}, Lok;->a(Landroid/view/View;II)Z

    move-result v5

    or-int/2addr v1, v5

    .line 1246
    :goto_1
    iput-boolean v3, v0, Lmz;->c:Z

    .line 1228
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1242
    :cond_2
    iget-object v6, p0, Lms;->i:Lok;

    invoke-virtual {p0}, Lms;->getWidth()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v6, v5, v7, v8}, Lok;->a(Landroid/view/View;II)Z

    move-result v5

    or-int/2addr v1, v5

    goto :goto_1

    .line 1249
    :cond_3
    iget-object v0, p0, Lms;->j:Lnc;

    invoke-virtual {v0}, Lnc;->a()V

    .line 1250
    iget-object v0, p0, Lms;->k:Lnc;

    invoke-virtual {v0}, Lnc;->a()V

    .line 1252
    if-eqz v1, :cond_4

    .line 1253
    invoke-virtual {p0}, Lms;->invalidate()V

    .line 1255
    :cond_4
    return-void
.end method

.method a(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 712
    invoke-virtual {p0, p1}, Lms;->e(Landroid/view/View;)I

    move-result v0

    .line 713
    and-int/2addr v0, p2

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 1524
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1526
    invoke-virtual {p0}, Lms;->a()Landroid/view/View;

    move-result-object v0

    .line 1527
    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lms;->g(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1530
    :cond_0
    const/4 v0, 0x4

    invoke-static {p1, v0}, Liu;->c(Landroid/view/View;I)V

    .line 1541
    :goto_0
    sget-boolean v0, Lms;->b:Z

    if-nez v0, :cond_1

    .line 1542
    iget-object v0, p0, Lms;->c:Lmu;

    invoke-static {p1, v0}, Liu;->a(Landroid/view/View;Lgw;)V

    .line 1544
    :cond_1
    return-void

    .line 1535
    :cond_2
    const/4 v0, 0x1

    invoke-static {p1, v0}, Liu;->c(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public b(I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 584
    invoke-static {p0}, Liu;->e(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Lhr;->a(II)I

    move-result v0

    .line 586
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 591
    :cond_0
    :goto_0
    return-object v2

    .line 588
    :cond_1
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1222
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lms;->a(Z)V

    .line 1223
    return-void
.end method

.method b(ILandroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 599
    iget-object v2, p0, Lms;->h:Lok;

    invoke-virtual {v2}, Lok;->a()I

    move-result v2

    .line 600
    iget-object v3, p0, Lms;->i:Lok;

    invoke-virtual {v3}, Lok;->a()I

    move-result v3

    .line 603
    if-eq v2, v1, :cond_0

    if-ne v3, v1, :cond_3

    .line 611
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    if-nez p1, :cond_1

    .line 612
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    .line 613
    iget v2, v0, Lmz;->b:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_6

    .line 614
    invoke-virtual {p0, p2}, Lms;->b(Landroid/view/View;)V

    .line 620
    :cond_1
    :goto_1
    iget v0, p0, Lms;->l:I

    if-eq v1, v0, :cond_2

    .line 621
    iput v1, p0, Lms;->l:I

    .line 623
    iget-object v0, p0, Lms;->r:Lmy;

    if-eqz v0, :cond_2

    .line 624
    iget-object v0, p0, Lms;->r:Lmy;

    .line 627
    :cond_2
    return-void

    .line 605
    :cond_3
    if-eq v2, v0, :cond_4

    if-ne v3, v0, :cond_5

    :cond_4
    move v1, v0

    .line 606
    goto :goto_0

    .line 608
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 615
    :cond_6
    iget v0, v0, Lmz;->b:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    .line 616
    invoke-virtual {p0, p2}, Lms;->c(Landroid/view/View;)V

    goto :goto_1
.end method

.method b(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 630
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    .line 631
    iget-boolean v1, v0, Lmz;->d:Z

    if-eqz v1, :cond_1

    .line 632
    iput-boolean v2, v0, Lmz;->d:Z

    .line 633
    iget-object v0, p0, Lms;->r:Lmy;

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lms;->r:Lmy;

    invoke-interface {v0, p1}, Lmy;->b(Landroid/view/View;)V

    .line 637
    :cond_0
    invoke-direct {p0, p1, v2}, Lms;->a(Landroid/view/View;Z)V

    .line 642
    invoke-virtual {p0}, Lms;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 643
    invoke-virtual {p0}, Lms;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 644
    if-eqz v0, :cond_1

    .line 645
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 649
    :cond_1
    return-void
.end method

.method b(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 689
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    .line 690
    iget v1, v0, Lmz;->b:F

    cmpl-float v1, p2, v1

    if-nez v1, :cond_0

    .line 696
    :goto_0
    return-void

    .line 694
    :cond_0
    iput p2, v0, Lmz;->b:F

    .line 695
    invoke-virtual {p0, p1, p2}, Lms;->a(Landroid/view/View;F)V

    goto :goto_0
.end method

.method c(I)Landroid/view/View;
    .locals 5

    .prologue
    .line 746
    invoke-static {p0}, Liu;->e(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Lhr;->a(II)I

    move-result v0

    and-int/lit8 v2, v0, 0x7

    .line 748
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v3

    .line 749
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 750
    invoke-virtual {p0, v1}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 751
    invoke-virtual {p0, v0}, Lms;->e(Landroid/view/View;)I

    move-result v4

    .line 752
    and-int/lit8 v4, v4, 0x7

    if-ne v4, v2, :cond_0

    .line 756
    :goto_1
    return-object v0

    .line 749
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 756
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method c()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1456
    iget-boolean v0, p0, Lms;->q:Z

    if-nez v0, :cond_1

    .line 1457
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1458
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1460
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v1

    .line 1461
    :goto_0
    if-ge v7, v1, :cond_0

    .line 1462
    invoke-virtual {p0, v7}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1461
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1464
    :cond_0
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1465
    const/4 v0, 0x1

    iput-boolean v0, p0, Lms;->q:Z

    .line 1467
    :cond_1
    return-void
.end method

.method c(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 652
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    .line 653
    iget-boolean v1, v0, Lmz;->d:Z

    if-nez v1, :cond_1

    .line 654
    iput-boolean v2, v0, Lmz;->d:Z

    .line 655
    iget-object v0, p0, Lms;->r:Lmy;

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lms;->r:Lmy;

    invoke-interface {v0, p1}, Lmy;->a(Landroid/view/View;)V

    .line 659
    :cond_0
    invoke-direct {p0, p1, v2}, Lms;->a(Landroid/view/View;Z)V

    .line 661
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 663
    :cond_1
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1431
    instance-of v0, p1, Lmz;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 968
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v3

    .line 969
    const/4 v1, 0x0

    .line 970
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 971
    invoke-virtual {p0, v1}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget v0, v0, Lmz;->b:F

    .line 972
    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 970
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 974
    :cond_0
    iput v2, p0, Lms;->f:F

    .line 977
    iget-object v0, p0, Lms;->h:Lok;

    invoke-virtual {v0, v4}, Lok;->a(Z)Z

    move-result v0

    iget-object v1, p0, Lms;->i:Lok;

    invoke-virtual {v1, v4}, Lok;->a(Z)Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 978
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 980
    :cond_1
    return-void
.end method

.method d(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 699
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget v0, v0, Lmz;->b:F

    return v0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 11

    .prologue
    .line 1035
    invoke-virtual {p0}, Lms;->getHeight()I

    move-result v4

    .line 1036
    invoke-virtual {p0, p2}, Lms;->f(Landroid/view/View;)Z

    move-result v5

    .line 1037
    const/4 v1, 0x0

    invoke-virtual {p0}, Lms;->getWidth()I

    move-result v2

    .line 1039
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v6

    .line 1040
    if-eqz v5, :cond_6

    .line 1041
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v7

    .line 1042
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v7, :cond_5

    .line 1043
    invoke-virtual {p0, v3}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1044
    if-eq v8, p2, :cond_4

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v8}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    const/4 v9, -0x1

    if-ne v0, v9, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p0, v8}, Lms;->g(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v0

    if-lt v0, v4, :cond_4

    .line 1047
    const/4 v0, 0x3

    invoke-virtual {p0, v8, v0}, Lms;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1051
    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v0

    .line 1052
    if-le v0, v1, :cond_8

    :goto_2
    move v1, v0

    move v0, v2

    .line 1042
    :cond_0
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 1044
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1054
    :cond_3
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 1055
    if-lt v0, v2, :cond_0

    :cond_4
    move v0, v2

    goto :goto_3

    .line 1058
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0}, Lms;->getHeight()I

    move-result v3

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    :cond_6
    move v0, v2

    .line 1060
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v7

    .line 1061
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1063
    iget v2, p0, Lms;->f:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_7

    if-eqz v5, :cond_7

    .line 1064
    iget v2, p0, Lms;->e:I

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    ushr-int/lit8 v2, v2, 0x18

    .line 1065
    int-to-float v2, v2

    iget v3, p0, Lms;->f:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1066
    shl-int/lit8 v2, v2, 0x18

    iget v3, p0, Lms;->e:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    .line 1067
    iget-object v3, p0, Lms;->g:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1069
    int-to-float v1, v1

    const/4 v2, 0x0

    int-to-float v3, v0

    invoke-virtual {p0}, Lms;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lms;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1070
    :cond_7
    return v7

    :cond_8
    move v0, v1

    goto :goto_2
.end method

.method e(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 707
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget v0, v0, Lmz;->a:I

    .line 708
    invoke-static {p0}, Liu;->e(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Lhr;->a(II)I

    move-result v0

    return v0
.end method

.method public e(I)V
    .locals 3

    .prologue
    .line 1291
    invoke-virtual {p0, p1}, Lms;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1292
    if-nez v0, :cond_0

    .line 1293
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No drawer view found with gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lms;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1296
    :cond_0
    invoke-virtual {p0, v0}, Lms;->h(Landroid/view/View;)V

    .line 1297
    return-void
.end method

.method public f(I)V
    .locals 3

    .prologue
    .line 1331
    invoke-virtual {p0, p1}, Lms;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1332
    if-nez v0, :cond_0

    .line 1333
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No drawer view found with gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lms;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1336
    :cond_0
    invoke-virtual {p0, v0}, Lms;->i(Landroid/view/View;)V

    .line 1337
    return-void
.end method

.method f(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1096
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget v0, v0, Lmz;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(I)Z
    .locals 1

    .prologue
    .line 1366
    invoke-virtual {p0, p1}, Lms;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1367
    if-eqz v0, :cond_0

    .line 1368
    invoke-virtual {p0, v0}, Lms;->j(Landroid/view/View;)Z

    move-result v0

    .line 1370
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method g(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 1100
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget v0, v0, Lmz;->a:I

    .line 1101
    invoke-static {p1}, Liu;->e(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Lhr;->a(II)I

    move-result v0

    .line 1103
    and-int/lit8 v0, v0, 0x7

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1417
    new-instance v0, Lmz;

    invoke-direct {v0, v1, v1}, Lmz;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 1436
    new-instance v0, Lmz;

    invoke-virtual {p0}, Lms;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lmz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1422
    instance-of v0, p1, Lmz;

    if-eqz v0, :cond_0

    new-instance v0, Lmz;

    check-cast p1, Lmz;

    invoke-direct {v0, p1}, Lmz;-><init>(Lmz;)V

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    new-instance v0, Lmz;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Lmz;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lmz;

    invoke-direct {v0, p1}, Lmz;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public h(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1263
    invoke-virtual {p0, p1}, Lms;->g(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1264
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a sliding drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1267
    :cond_0
    iget-boolean v0, p0, Lms;->n:Z

    if-eqz v0, :cond_1

    .line 1268
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    .line 1269
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lmz;->b:F

    .line 1270
    iput-boolean v2, v0, Lmz;->d:Z

    .line 1272
    invoke-direct {p0, p1, v2}, Lms;->a(Landroid/view/View;Z)V

    .line 1281
    :goto_0
    invoke-virtual {p0}, Lms;->invalidate()V

    .line 1282
    return-void

    .line 1274
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lms;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1275
    iget-object v0, p0, Lms;->h:Lok;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lok;->a(Landroid/view/View;II)Z

    goto :goto_0

    .line 1277
    :cond_2
    iget-object v0, p0, Lms;->i:Lok;

    invoke-virtual {p0}, Lms;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lok;->a(Landroid/view/View;II)Z

    goto :goto_0
.end method

.method public h(I)Z
    .locals 1

    .prologue
    .line 1397
    invoke-virtual {p0, p1}, Lms;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1398
    if-eqz v0, :cond_0

    .line 1399
    invoke-virtual {p0, v0}, Lms;->k(Landroid/view/View;)Z

    move-result v0

    .line 1401
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1305
    invoke-virtual {p0, p1}, Lms;->g(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1306
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a sliding drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1309
    :cond_0
    iget-boolean v0, p0, Lms;->n:Z

    if-eqz v0, :cond_1

    .line 1310
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    .line 1311
    const/4 v1, 0x0

    iput v1, v0, Lmz;->b:F

    .line 1312
    const/4 v1, 0x0

    iput-boolean v1, v0, Lmz;->d:Z

    .line 1321
    :goto_0
    invoke-virtual {p0}, Lms;->invalidate()V

    .line 1322
    return-void

    .line 1314
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lms;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1315
    iget-object v0, p0, Lms;->h:Lok;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lok;->a(Landroid/view/View;II)Z

    goto :goto_0

    .line 1318
    :cond_2
    iget-object v0, p0, Lms;->i:Lok;

    invoke-virtual {p0}, Lms;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lok;->a(Landroid/view/View;II)Z

    goto :goto_0
.end method

.method public j(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 1350
    invoke-virtual {p0, p1}, Lms;->g(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1351
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1353
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget-boolean v0, v0, Lmz;->d:Z

    return v0
.end method

.method public k(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 1382
    invoke-virtual {p0, p1}, Lms;->g(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1383
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1385
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget v0, v0, Lmz;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 783
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 784
    const/4 v0, 0x1

    iput-boolean v0, p0, Lms;->n:Z

    .line 785
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 777
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 778
    const/4 v0, 0x1

    iput-boolean v0, p0, Lms;->n:Z

    .line 779
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1023
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 1024
    iget-boolean v0, p0, Lms;->v:Z

    .line 1025
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1108
    invoke-static {p1}, Lik;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1111
    iget-object v3, p0, Lms;->h:Lok;

    invoke-virtual {v3, p1}, Lok;->a(Landroid/view/MotionEvent;)Z

    move-result v3

    iget-object v4, p0, Lms;->i:Lok;

    invoke-virtual {v4, p1}, Lok;->a(Landroid/view/MotionEvent;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 1116
    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v2

    .line 1150
    :goto_1
    if-nez v3, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_4

    invoke-virtual {p0, v3}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget-boolean v0, v0, Lmz;->c:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lms;->q:Z

    if-eqz v0, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    return v2

    .line 1118
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 1120
    iput v0, p0, Lms;->s:F

    .line 1121
    iput v4, p0, Lms;->t:F

    .line 1122
    iget v5, p0, Lms;->f:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    .line 1123
    iget-object v5, p0, Lms;->h:Lok;

    float-to-int v0, v0

    float-to-int v4, v4

    invoke-virtual {v5, v0, v4}, Lok;->d(II)Landroid/view/View;

    move-result-object v0

    .line 1124
    if-eqz v0, :cond_5

    invoke-virtual {p0, v0}, Lms;->f(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 1128
    :goto_4
    iput-boolean v2, p0, Lms;->q:Z

    goto :goto_1

    .line 1135
    :pswitch_1
    iget-object v0, p0, Lms;->h:Lok;

    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Lok;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1136
    iget-object v0, p0, Lms;->j:Lnc;

    invoke-virtual {v0}, Lnc;->a()V

    .line 1137
    iget-object v0, p0, Lms;->k:Lnc;

    invoke-virtual {v0}, Lnc;->a()V

    move v0, v2

    goto :goto_1

    .line 1144
    :pswitch_2
    invoke-virtual {p0, v1}, Lms;->a(Z)V

    .line 1145
    iput-boolean v2, p0, Lms;->q:Z

    goto :goto_0

    .line 1150
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_4

    .line 1116
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1471
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    invoke-direct {p0}, Lms;->f()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    if-eqz v1, :cond_1

    .line 1472
    invoke-static {p2}, Lhv;->b(Landroid/view/KeyEvent;)V

    .line 1475
    :goto_1
    return v0

    .line 1471
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1475
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 1480
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 1481
    invoke-direct {p0}, Lms;->f()Landroid/view/View;

    move-result-object v0

    .line 1482
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lms;->a(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_0

    .line 1483
    invoke-virtual {p0}, Lms;->b()V

    .line 1485
    :cond_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1487
    :goto_0
    return v0

    .line 1485
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1487
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 14

    .prologue
    .line 877
    const/4 v0, 0x1

    iput-boolean v0, p0, Lms;->m:Z

    .line 878
    sub-int v6, p4, p2

    .line 879
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v7

    .line 880
    const/4 v0, 0x0

    move v5, v0

    :goto_0
    if-ge v5, v7, :cond_8

    .line 881
    invoke-virtual {p0, v5}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 883
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 884
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    .line 889
    invoke-virtual {p0, v8}, Lms;->f(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 890
    iget v1, v0, Lmz;->leftMargin:I

    iget v2, v0, Lmz;->topMargin:I

    iget v3, v0, Lmz;->leftMargin:I

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v0, v0, Lmz;->topMargin:I

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v8, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 880
    :cond_0
    :goto_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 894
    :cond_1
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 895
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 899
    const/4 v1, 0x3

    invoke-virtual {p0, v8, v1}, Lms;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 900
    neg-int v1, v9

    int-to-float v2, v9

    iget v3, v0, Lmz;->b:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v2, v1

    .line 901
    add-int v1, v9, v2

    int-to-float v1, v1

    int-to-float v3, v9

    div-float/2addr v1, v3

    .line 907
    :goto_2
    iget v3, v0, Lmz;->b:F

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    .line 909
    :goto_3
    iget v4, v0, Lmz;->a:I

    and-int/lit8 v4, v4, 0x70

    .line 911
    sparse-switch v4, :sswitch_data_0

    .line 914
    iget v4, v0, Lmz;->topMargin:I

    add-int/2addr v9, v2

    iget v11, v0, Lmz;->topMargin:I

    add-int/2addr v10, v11

    invoke-virtual {v8, v2, v4, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 941
    :goto_4
    if-eqz v3, :cond_2

    .line 946
    invoke-virtual {p0, v8, v1}, Lms;->b(Landroid/view/View;F)V

    .line 949
    :cond_2
    iget v0, v0, Lmz;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    const/4 v0, 0x0

    .line 950
    :goto_5
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 951
    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 903
    :cond_3
    int-to-float v1, v9

    iget v2, v0, Lmz;->b:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int v2, v6, v1

    .line 904
    sub-int v1, v6, v2

    int-to-float v1, v1

    int-to-float v3, v9

    div-float/2addr v1, v3

    goto :goto_2

    .line 907
    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    .line 920
    :sswitch_0
    sub-int v4, p5, p3

    .line 921
    iget v10, v0, Lmz;->bottomMargin:I

    sub-int v10, v4, v10

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    sub-int/2addr v10, v11

    add-int/2addr v9, v2

    iget v11, v0, Lmz;->bottomMargin:I

    sub-int/2addr v4, v11

    invoke-virtual {v8, v2, v10, v9, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 929
    :sswitch_1
    sub-int v11, p5, p3

    .line 930
    sub-int v4, v11, v10

    div-int/lit8 v4, v4, 0x2

    .line 934
    iget v12, v0, Lmz;->topMargin:I

    if-ge v4, v12, :cond_6

    .line 935
    iget v4, v0, Lmz;->topMargin:I

    .line 939
    :cond_5
    :goto_6
    add-int/2addr v9, v2

    add-int/2addr v10, v4

    invoke-virtual {v8, v2, v4, v9, v10}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 936
    :cond_6
    add-int v12, v4, v10

    iget v13, v0, Lmz;->bottomMargin:I

    sub-int v13, v11, v13

    if-le v12, v13, :cond_5

    .line 937
    iget v4, v0, Lmz;->bottomMargin:I

    sub-int v4, v11, v4

    sub-int/2addr v4, v10

    goto :goto_6

    .line 949
    :cond_7
    const/4 v0, 0x4

    goto :goto_5

    .line 955
    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lms;->m:Z

    .line 956
    const/4 v0, 0x0

    iput-boolean v0, p0, Lms;->n:Z

    .line 957
    return-void

    .line 911
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/16 v1, 0x12c

    const/4 v4, 0x0

    const/high16 v7, -0x80000000

    const/high16 v11, 0x40000000    # 2.0f

    .line 789
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 790
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 791
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 792
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 794
    if-ne v3, v11, :cond_0

    if-eq v5, v11, :cond_b

    .line 795
    :cond_0
    invoke-virtual {p0}, Lms;->isInEditMode()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 800
    if-eq v3, v7, :cond_1

    .line 801
    if-nez v3, :cond_1

    move v2, v1

    .line 806
    :cond_1
    if-eq v5, v7, :cond_b

    .line 807
    if-nez v5, :cond_b

    .line 819
    :goto_0
    invoke-virtual {p0, v2, v1}, Lms;->setMeasuredDimension(II)V

    .line 821
    iget-object v0, p0, Lms;->u:Ljava/lang/Object;

    if-eqz v0, :cond_5

    invoke-static {p0}, Liu;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    move v3, v0

    .line 822
    :goto_1
    invoke-static {p0}, Liu;->e(Landroid/view/View;)I

    move-result v5

    .line 825
    invoke-virtual {p0}, Lms;->getChildCount()I

    move-result v6

    .line 827
    :goto_2
    if-ge v4, v6, :cond_a

    .line 828
    invoke-virtual {p0, v4}, Lms;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 830
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v8, 0x8

    if-eq v0, v8, :cond_3

    .line 831
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    .line 836
    if-eqz v3, :cond_2

    .line 837
    iget v8, v0, Lmz;->a:I

    invoke-static {v8, v5}, Lhr;->a(II)I

    move-result v8

    .line 838
    invoke-static {v7}, Liu;->m(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 839
    sget-object v9, Lms;->w:Lmv;

    iget-object v10, p0, Lms;->u:Ljava/lang/Object;

    invoke-interface {v9, v7, v10, v8}, Lmv;->a(Landroid/view/View;Ljava/lang/Object;I)V

    .line 845
    :cond_2
    :goto_3
    invoke-virtual {p0, v7}, Lms;->f(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 847
    iget v8, v0, Lmz;->leftMargin:I

    sub-int v8, v2, v8

    iget v9, v0, Lmz;->rightMargin:I

    sub-int/2addr v8, v9

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 849
    iget v9, v0, Lmz;->topMargin:I

    sub-int v9, v1, v9

    iget v0, v0, Lmz;->bottomMargin:I

    sub-int v0, v9, v0

    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 851
    invoke-virtual {v7, v8, v0}, Landroid/view/View;->measure(II)V

    .line 827
    :cond_3
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 814
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DrawerLayout must be measured with MeasureSpec.EXACTLY."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v3, v4

    .line 821
    goto :goto_1

    .line 841
    :cond_6
    sget-object v9, Lms;->w:Lmv;

    iget-object v10, p0, Lms;->u:Ljava/lang/Object;

    invoke-interface {v9, v0, v10, v8}, Lmv;->a(Landroid/view/ViewGroup$MarginLayoutParams;Ljava/lang/Object;I)V

    goto :goto_3

    .line 852
    :cond_7
    invoke-virtual {p0, v7}, Lms;->g(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 853
    invoke-virtual {p0, v7}, Lms;->e(Landroid/view/View;)I

    move-result v8

    and-int/lit8 v8, v8, 0x7

    .line 855
    and-int/lit8 v9, v8, 0x0

    if-eqz v9, :cond_8

    .line 856
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Child drawer has absolute gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lms;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but this DrawerLayout already has a drawer view along that edge"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 860
    :cond_8
    iget v8, p0, Lms;->d:I

    iget v9, v0, Lmz;->leftMargin:I

    add-int/2addr v8, v9

    iget v9, v0, Lmz;->rightMargin:I

    add-int/2addr v8, v9

    iget v9, v0, Lmz;->width:I

    invoke-static {p1, v8, v9}, Lms;->getChildMeasureSpec(III)I

    move-result v8

    .line 863
    iget v9, v0, Lmz;->topMargin:I

    iget v10, v0, Lmz;->bottomMargin:I

    add-int/2addr v9, v10

    iget v0, v0, Lmz;->height:I

    invoke-static {p2, v9, v0}, Lms;->getChildMeasureSpec(III)I

    move-result v0

    .line 866
    invoke-virtual {v7, v8, v0}, Landroid/view/View;->measure(II)V

    goto :goto_4

    .line 868
    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Child "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not have a valid layout_gravity - must be Gravity.LEFT, Gravity.RIGHT or Gravity.NO_GRAVITY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 873
    :cond_a
    return-void

    :cond_b
    move v1, v0

    goto/16 :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1492
    check-cast p1, Lna;

    .line 1493
    invoke-virtual {p1}, Lna;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1495
    iget v0, p1, Lna;->a:I

    if-eqz v0, :cond_0

    .line 1496
    iget v0, p1, Lna;->a:I

    invoke-virtual {p0, v0}, Lms;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1497
    if-eqz v0, :cond_0

    .line 1498
    invoke-virtual {p0, v0}, Lms;->h(Landroid/view/View;)V

    .line 1502
    :cond_0
    iget v0, p1, Lna;->b:I

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lms;->a(II)V

    .line 1503
    iget v0, p1, Lna;->c:I

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lms;->a(II)V

    .line 1504
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1508
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1509
    new-instance v1, Lna;

    invoke-direct {v1, v0}, Lna;-><init>(Landroid/os/Parcelable;)V

    .line 1511
    invoke-virtual {p0}, Lms;->a()Landroid/view/View;

    move-result-object v0

    .line 1512
    if-eqz v0, :cond_0

    .line 1513
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lmz;

    iget v0, v0, Lmz;->a:I

    iput v0, v1, Lna;->a:I

    .line 1516
    :cond_0
    iget v0, p0, Lms;->o:I

    iput v0, v1, Lna;->b:I

    .line 1517
    iget v0, p0, Lms;->p:I

    iput v0, v1, Lna;->c:I

    .line 1519
    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1155
    iget-object v2, p0, Lms;->h:Lok;

    invoke-virtual {v2, p1}, Lok;->b(Landroid/view/MotionEvent;)V

    .line 1156
    iget-object v2, p0, Lms;->i:Lok;

    invoke-virtual {v2, p1}, Lok;->b(Landroid/view/MotionEvent;)V

    .line 1158
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 1159
    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 1202
    :goto_0
    :pswitch_0
    return v1

    .line 1163
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 1164
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1165
    iput v2, p0, Lms;->s:F

    .line 1166
    iput v3, p0, Lms;->t:F

    .line 1167
    iput-boolean v0, p0, Lms;->q:Z

    goto :goto_0

    .line 1173
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 1174
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1176
    iget-object v4, p0, Lms;->h:Lok;

    float-to-int v5, v2

    float-to-int v6, v3

    invoke-virtual {v4, v5, v6}, Lok;->d(II)Landroid/view/View;

    move-result-object v4

    .line 1177
    if-eqz v4, :cond_1

    invoke-virtual {p0, v4}, Lms;->f(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1178
    iget v4, p0, Lms;->s:F

    sub-float/2addr v2, v4

    .line 1179
    iget v4, p0, Lms;->t:F

    sub-float/2addr v3, v4

    .line 1180
    iget-object v4, p0, Lms;->h:Lok;

    invoke-virtual {v4}, Lok;->d()I

    move-result v4

    .line 1181
    mul-float/2addr v2, v2

    mul-float/2addr v3, v3

    add-float/2addr v2, v3

    mul-int v3, v4, v4

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 1183
    invoke-virtual {p0}, Lms;->a()Landroid/view/View;

    move-result-object v2

    .line 1184
    if-eqz v2, :cond_1

    .line 1185
    invoke-virtual {p0, v2}, Lms;->a(Landroid/view/View;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 1189
    :cond_0
    :goto_1
    invoke-virtual {p0, v0}, Lms;->a(Z)V

    goto :goto_0

    .line 1195
    :pswitch_3
    invoke-virtual {p0, v1}, Lms;->a(Z)V

    .line 1196
    iput-boolean v0, p0, Lms;->q:Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 1159
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    .prologue
    .line 1210
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 1212
    if-eqz p1, :cond_0

    .line 1214
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lms;->a(Z)V

    .line 1216
    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 961
    iget-boolean v0, p0, Lms;->m:Z

    if-nez v0, :cond_0

    .line 962
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 964
    :cond_0
    return-void
.end method
