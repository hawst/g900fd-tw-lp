.class public final Lmuf;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3436
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 3452
    const/4 v0, 0x0

    .line 3453
    iget-object v1, p0, Lmuf;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 3454
    const/4 v0, 0x1

    iget-object v1, p0, Lmuf;->a:Ljava/lang/Long;

    .line 3455
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3457
    :cond_0
    iget-object v1, p0, Lmuf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3458
    iput v0, p0, Lmuf;->ai:I

    .line 3459
    return v0
.end method

.method public a(Loxn;)Lmuf;
    .locals 2

    .prologue
    .line 3467
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3468
    sparse-switch v0, :sswitch_data_0

    .line 3472
    iget-object v1, p0, Lmuf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3473
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmuf;->ah:Ljava/util/List;

    .line 3476
    :cond_1
    iget-object v1, p0, Lmuf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3478
    :sswitch_0
    return-object p0

    .line 3483
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmuf;->a:Ljava/lang/Long;

    goto :goto_0

    .line 3468
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 3443
    iget-object v0, p0, Lmuf;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 3444
    const/4 v0, 0x1

    iget-object v1, p0, Lmuf;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 3446
    :cond_0
    iget-object v0, p0, Lmuf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3448
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3432
    invoke-virtual {p0, p1}, Lmuf;->a(Loxn;)Lmuf;

    move-result-object v0

    return-object v0
.end method
