.class public final Lofw;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lofw;


# instance fields
.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x0

    new-array v0, v0, [Lofw;

    sput-object v0, Lofw;->a:[Lofw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 399
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 430
    const/4 v0, 0x0

    .line 431
    iget-object v1, p0, Lofw;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 432
    const/4 v0, 0x1

    iget-object v1, p0, Lofw;->b:Ljava/lang/String;

    .line 433
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 435
    :cond_0
    iget-object v1, p0, Lofw;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 436
    const/4 v1, 0x2

    iget-object v2, p0, Lofw;->c:Ljava/lang/String;

    .line 437
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 439
    :cond_1
    iget-object v1, p0, Lofw;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 440
    const/4 v1, 0x3

    iget-object v2, p0, Lofw;->d:Ljava/lang/Integer;

    .line 441
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 443
    :cond_2
    iget-object v1, p0, Lofw;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 444
    const/4 v1, 0x4

    iget-object v2, p0, Lofw;->e:Ljava/lang/Integer;

    .line 445
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    :cond_3
    iget-object v1, p0, Lofw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 448
    iput v0, p0, Lofw;->ai:I

    .line 449
    return v0
.end method

.method public a(Loxn;)Lofw;
    .locals 2

    .prologue
    .line 457
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 458
    sparse-switch v0, :sswitch_data_0

    .line 462
    iget-object v1, p0, Lofw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 463
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofw;->ah:Ljava/util/List;

    .line 466
    :cond_1
    iget-object v1, p0, Lofw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 468
    :sswitch_0
    return-object p0

    .line 473
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofw;->b:Ljava/lang/String;

    goto :goto_0

    .line 477
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofw;->c:Ljava/lang/String;

    goto :goto_0

    .line 481
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofw;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 485
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofw;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 458
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lofw;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 413
    const/4 v0, 0x1

    iget-object v1, p0, Lofw;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 415
    :cond_0
    iget-object v0, p0, Lofw;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 416
    const/4 v0, 0x2

    iget-object v1, p0, Lofw;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 418
    :cond_1
    iget-object v0, p0, Lofw;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 419
    const/4 v0, 0x3

    iget-object v1, p0, Lofw;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 421
    :cond_2
    iget-object v0, p0, Lofw;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 422
    const/4 v0, 0x4

    iget-object v1, p0, Lofw;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 424
    :cond_3
    iget-object v0, p0, Lofw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 426
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p0, p1}, Lofw;->a(Loxn;)Lofw;

    move-result-object v0

    return-object v0
.end method
