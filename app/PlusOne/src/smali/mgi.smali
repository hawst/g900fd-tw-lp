.class public final Lmgi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lned;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24055
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24058
    iput-object v0, p0, Lmgi;->apiHeader:Llyq;

    .line 24061
    iput-object v0, p0, Lmgi;->a:Lned;

    .line 24055
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 24078
    const/4 v0, 0x0

    .line 24079
    iget-object v1, p0, Lmgi;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 24080
    const/4 v0, 0x1

    iget-object v1, p0, Lmgi;->apiHeader:Llyq;

    .line 24081
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24083
    :cond_0
    iget-object v1, p0, Lmgi;->a:Lned;

    if-eqz v1, :cond_1

    .line 24084
    const/4 v1, 0x2

    iget-object v2, p0, Lmgi;->a:Lned;

    .line 24085
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24087
    :cond_1
    iget-object v1, p0, Lmgi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24088
    iput v0, p0, Lmgi;->ai:I

    .line 24089
    return v0
.end method

.method public a(Loxn;)Lmgi;
    .locals 2

    .prologue
    .line 24097
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 24098
    sparse-switch v0, :sswitch_data_0

    .line 24102
    iget-object v1, p0, Lmgi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 24103
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgi;->ah:Ljava/util/List;

    .line 24106
    :cond_1
    iget-object v1, p0, Lmgi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24108
    :sswitch_0
    return-object p0

    .line 24113
    :sswitch_1
    iget-object v0, p0, Lmgi;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 24114
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmgi;->apiHeader:Llyq;

    .line 24116
    :cond_2
    iget-object v0, p0, Lmgi;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24120
    :sswitch_2
    iget-object v0, p0, Lmgi;->a:Lned;

    if-nez v0, :cond_3

    .line 24121
    new-instance v0, Lned;

    invoke-direct {v0}, Lned;-><init>()V

    iput-object v0, p0, Lmgi;->a:Lned;

    .line 24123
    :cond_3
    iget-object v0, p0, Lmgi;->a:Lned;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24098
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24066
    iget-object v0, p0, Lmgi;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 24067
    const/4 v0, 0x1

    iget-object v1, p0, Lmgi;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24069
    :cond_0
    iget-object v0, p0, Lmgi;->a:Lned;

    if-eqz v0, :cond_1

    .line 24070
    const/4 v0, 0x2

    iget-object v1, p0, Lmgi;->a:Lned;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24072
    :cond_1
    iget-object v0, p0, Lmgi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 24074
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 24051
    invoke-virtual {p0, p1}, Lmgi;->a(Loxn;)Lmgi;

    move-result-object v0

    return-object v0
.end method
