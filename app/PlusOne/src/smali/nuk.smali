.class public final Lnuk;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lnuc;

.field private c:Locx;

.field private d:Lnue;

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 550
    invoke-direct {p0}, Loxq;-><init>()V

    .line 553
    const/high16 v0, -0x80000000

    iput v0, p0, Lnuk;->a:I

    .line 556
    iput-object v1, p0, Lnuk;->b:Lnuc;

    .line 559
    iput-object v1, p0, Lnuk;->c:Locx;

    .line 562
    iput-object v1, p0, Lnuk;->d:Lnue;

    .line 550
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 590
    const/4 v0, 0x0

    .line 591
    iget v1, p0, Lnuk;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 592
    const/4 v0, 0x1

    iget v1, p0, Lnuk;->a:I

    .line 593
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 595
    :cond_0
    iget-object v1, p0, Lnuk;->b:Lnuc;

    if-eqz v1, :cond_1

    .line 596
    const/4 v1, 0x2

    iget-object v2, p0, Lnuk;->b:Lnuc;

    .line 597
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 599
    :cond_1
    iget-object v1, p0, Lnuk;->c:Locx;

    if-eqz v1, :cond_2

    .line 600
    const/4 v1, 0x3

    iget-object v2, p0, Lnuk;->c:Locx;

    .line 601
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 603
    :cond_2
    iget-object v1, p0, Lnuk;->d:Lnue;

    if-eqz v1, :cond_3

    .line 604
    const/4 v1, 0x4

    iget-object v2, p0, Lnuk;->d:Lnue;

    .line 605
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 607
    :cond_3
    iget-object v1, p0, Lnuk;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 608
    const/4 v1, 0x5

    iget-object v2, p0, Lnuk;->e:Ljava/lang/Boolean;

    .line 609
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 611
    :cond_4
    iget-object v1, p0, Lnuk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 612
    iput v0, p0, Lnuk;->ai:I

    .line 613
    return v0
.end method

.method public a(Loxn;)Lnuk;
    .locals 2

    .prologue
    .line 621
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 622
    sparse-switch v0, :sswitch_data_0

    .line 626
    iget-object v1, p0, Lnuk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 627
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnuk;->ah:Ljava/util/List;

    .line 630
    :cond_1
    iget-object v1, p0, Lnuk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 632
    :sswitch_0
    return-object p0

    .line 637
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 638
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 641
    :cond_2
    iput v0, p0, Lnuk;->a:I

    goto :goto_0

    .line 643
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnuk;->a:I

    goto :goto_0

    .line 648
    :sswitch_2
    iget-object v0, p0, Lnuk;->b:Lnuc;

    if-nez v0, :cond_4

    .line 649
    new-instance v0, Lnuc;

    invoke-direct {v0}, Lnuc;-><init>()V

    iput-object v0, p0, Lnuk;->b:Lnuc;

    .line 651
    :cond_4
    iget-object v0, p0, Lnuk;->b:Lnuc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 655
    :sswitch_3
    iget-object v0, p0, Lnuk;->c:Locx;

    if-nez v0, :cond_5

    .line 656
    new-instance v0, Locx;

    invoke-direct {v0}, Locx;-><init>()V

    iput-object v0, p0, Lnuk;->c:Locx;

    .line 658
    :cond_5
    iget-object v0, p0, Lnuk;->c:Locx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 662
    :sswitch_4
    iget-object v0, p0, Lnuk;->d:Lnue;

    if-nez v0, :cond_6

    .line 663
    new-instance v0, Lnue;

    invoke-direct {v0}, Lnue;-><init>()V

    iput-object v0, p0, Lnuk;->d:Lnue;

    .line 665
    :cond_6
    iget-object v0, p0, Lnuk;->d:Lnue;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 669
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnuk;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 622
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 569
    iget v0, p0, Lnuk;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 570
    const/4 v0, 0x1

    iget v1, p0, Lnuk;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 572
    :cond_0
    iget-object v0, p0, Lnuk;->b:Lnuc;

    if-eqz v0, :cond_1

    .line 573
    const/4 v0, 0x2

    iget-object v1, p0, Lnuk;->b:Lnuc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 575
    :cond_1
    iget-object v0, p0, Lnuk;->c:Locx;

    if-eqz v0, :cond_2

    .line 576
    const/4 v0, 0x3

    iget-object v1, p0, Lnuk;->c:Locx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 578
    :cond_2
    iget-object v0, p0, Lnuk;->d:Lnue;

    if-eqz v0, :cond_3

    .line 579
    const/4 v0, 0x4

    iget-object v1, p0, Lnuk;->d:Lnue;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 581
    :cond_3
    iget-object v0, p0, Lnuk;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 582
    const/4 v0, 0x5

    iget-object v1, p0, Lnuk;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 584
    :cond_4
    iget-object v0, p0, Lnuk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 586
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 546
    invoke-virtual {p0, p1}, Lnuk;->a(Loxn;)Lnuk;

    move-result-object v0

    return-object v0
.end method
