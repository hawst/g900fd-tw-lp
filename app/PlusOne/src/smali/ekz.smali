.class public final Lekz;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:[Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:Z

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 60
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lekz;->b:Ldp;

    .line 61
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lekz;->a(Landroid/net/Uri;)V

    .line 62
    iput p2, p0, Lekz;->c:I

    .line 63
    iput-object p3, p0, Lekz;->d:[Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lekz;->e:Ljava/lang/String;

    .line 65
    iput-boolean p6, p0, Lekz;->f:Z

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lekz;->g:Z

    .line 67
    iput-object p5, p0, Lekz;->h:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 80
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lekz;->b:Ldp;

    .line 81
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lekz;->a(Landroid/net/Uri;)V

    .line 82
    iput p2, p0, Lekz;->c:I

    .line 83
    iput-object p3, p0, Lekz;->d:[Ljava/lang/String;

    .line 84
    iput-object v1, p0, Lekz;->e:Ljava/lang/String;

    .line 85
    iput-boolean p4, p0, Lekz;->f:Z

    .line 86
    iput-boolean p5, p0, Lekz;->g:Z

    .line 87
    iput-object v1, p0, Lekz;->h:Ljava/lang/String;

    .line 88
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 95
    const-string v0, "in_my_circles=1"

    .line 96
    iget-boolean v1, p0, Lekz;->g:Z

    if-eqz v1, :cond_0

    .line 97
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, " AND gaia_id IS NOT NULL"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lekz;->f:Z

    if-nez v1, :cond_4

    .line 100
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, " AND profile_type!=2"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v4, v0

    .line 103
    :goto_2
    iget v1, p0, Lekz;->c:I

    .line 104
    invoke-virtual {p0}, Lekz;->n()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lekz;->e:Ljava/lang/String;

    iget-object v3, p0, Lekz;->d:[Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lekz;->h:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_1

    .line 107
    iget-object v1, p0, Lekz;->b:Ldp;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 110
    :cond_1
    return-object v0

    .line 97
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 100
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move-object v4, v0

    goto :goto_2
.end method
