.class public Lktw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lktm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lktm;

    const-string v1, "squares_metadata"

    invoke-direct {v0, p1, v1}, Lktm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lktw;->a:Lktm;

    .line 28
    return-void
.end method


# virtual methods
.method public a(IJ)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lktw;->a:Lktm;

    const-string v1, "last_squares_sync_time"

    invoke-virtual {v0, p1, v1, p2, p3}, Lktm;->a(ILjava/lang/String;J)V

    .line 49
    return-void
.end method

.method public a(I)Z
    .locals 6

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lktw;->b(I)J

    move-result-wide v0

    .line 32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x124f80

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)J
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Lktw;->a:Lktm;

    const-string v1, "last_squares_sync_time"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, p1, v1, v2, v3}, Lktm;->b(ILjava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(IJ)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lktw;->a:Lktm;

    const-string v1, "last_suggestions_sync_time"

    invoke-virtual {v0, p1, v1, p2, p3}, Lktm;->a(ILjava/lang/String;J)V

    .line 71
    return-void
.end method
