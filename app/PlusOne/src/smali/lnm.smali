.class public final Llnm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private volatile a:Llnh;

.field private final b:Ljava/lang/Object;

.field private final c:Llnn;

.field private final d:Z


# direct methods
.method public constructor <init>(Llnn;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Llnm;-><init>(ZLlnn;)V

    .line 41
    return-void
.end method

.method constructor <init>(ZLlnn;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Llnm;->b:Ljava/lang/Object;

    .line 51
    iput-boolean p1, p0, Llnm;->d:Z

    .line 52
    iput-object p2, p0, Llnm;->c:Llnn;

    .line 53
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Llnh;
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Llnm;->a:Llnh;

    if-nez v0, :cond_3

    .line 62
    iget-object v1, p0, Llnm;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v0, p0, Llnm;->a:Llnh;

    if-nez v0, :cond_2

    .line 64
    new-instance v0, Llnh;

    invoke-direct {v0, p1}, Llnh;-><init>(Landroid/content/Context;)V

    .line 66
    iget-boolean v2, p0, Llnm;->d:Z

    if-eqz v2, :cond_0

    .line 67
    invoke-static {p1}, Llnh;->c(Landroid/content/Context;)Llnh;

    move-result-object v2

    invoke-virtual {v0, v2}, Llnh;->a(Llnh;)V

    .line 70
    :cond_0
    iget-object v2, p0, Llnm;->c:Llnn;

    if-eqz v2, :cond_1

    .line 71
    iget-object v2, p0, Llnm;->c:Llnn;

    invoke-interface {v2, p1, v0}, Llnn;->a(Landroid/content/Context;Llnh;)V

    .line 74
    :cond_1
    iput-object v0, p0, Llnm;->a:Llnh;

    .line 76
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :cond_3
    iget-object v0, p0, Llnm;->a:Llnh;

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
