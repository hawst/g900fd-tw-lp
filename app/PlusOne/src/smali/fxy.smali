.class public final Lfxy;
.super Lfye;
.source "PG"

# interfaces
.implements Lfxz;


# static fields
.field private static a:Z

.field private static b:I

.field private static c:Landroid/graphics/Paint;

.field private static d:I


# instance fields
.field private e:Lfxf;

.field private f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

.field private g:Lfya;

.field private h:Liwk;

.field private i:I

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lfye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    sget-boolean v0, Lfxy;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b02ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lfxy;->b:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lfxy;->c:Landroid/graphics/Paint;

    const v2, 0x7f0b030b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lfxy;->c:Landroid/graphics/Paint;

    const v2, 0x7f0d02b9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const v1, 0x7f0d02bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfxy;->d:I

    const/4 v0, 0x1

    sput-boolean v0, Lfxy;->a:Z

    :cond_0
    sget v0, Lfxy;->b:I

    invoke-virtual {p0, v0}, Lfxy;->setBackgroundColor(I)V

    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    new-instance v1, Liwk;

    invoke-direct {v1, p1, v0}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v0, Lixj;

    invoke-virtual {v1, v0}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lfxy;->h:Liwk;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04009b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iput-object v0, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget-object v0, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {p0, v0}, Lfxy;->addView(Landroid/view/View;)V

    new-instance v0, Lfya;

    invoke-direct {v0, p1, p2, p3}, Lfya;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxy;->g:Lfya;

    iget-object v0, p0, Lfxy;->g:Lfya;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lfya;->setVisibility(I)V

    iget-object v0, p0, Lfxy;->g:Lfya;

    invoke-virtual {p0, v0}, Lfxy;->addView(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfxy;->setWillNotDraw(Z)V

    .line 56
    return-void
.end method

.method private a(IZ)V
    .locals 8

    .prologue
    const/4 v4, 0x4

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 165
    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-ne p1, v0, :cond_3

    :cond_0
    move v0, v2

    .line 167
    :goto_0
    const/4 v3, 0x5

    if-eq p1, v3, :cond_4

    iget-boolean v3, p0, Lfxy;->j:Z

    if-eqz v3, :cond_1

    if-nez v0, :cond_4

    .line 169
    :cond_1
    iget-object v0, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->getVisibility()I

    move-result v0

    .line 170
    iget-object v3, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->setVisibility(I)V

    .line 171
    iget-object v3, p0, Lfxy;->g:Lfya;

    invoke-virtual {v3, v1}, Lfya;->setVisibility(I)V

    .line 173
    if-eqz p2, :cond_2

    if-nez v0, :cond_2

    .line 174
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v7, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 175
    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 176
    invoke-virtual {v0, v2}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 177
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 178
    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 179
    invoke-virtual {v1, v2}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 181
    iget-object v2, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 182
    iget-object v0, p0, Lfxy;->g:Lfya;

    invoke-virtual {v0, v1}, Lfya;->startAnimation(Landroid/view/animation/Animation;)V

    .line 188
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    .line 165
    goto :goto_0

    .line 185
    :cond_4
    iget-object v0, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lfxy;->g:Lfya;

    invoke-virtual {v0, v4}, Lfya;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public a(Lidh;Leaq;Lfxf;)V
    .locals 2

    .prologue
    .line 133
    iput-object p3, p0, Lfxy;->e:Lfxf;

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 135
    invoke-static {p1, v0, v1}, Ldrm;->a(Lidh;J)Z

    move-result v0

    iput-boolean v0, p0, Lfxy;->j:Z

    .line 136
    iget-object v0, p0, Lfxy;->g:Lfya;

    invoke-virtual {v0, p1, p2, p0, p3}, Lfya;->a(Lidh;Leaq;Lfxz;Lfxf;)V

    .line 137
    iget-object v0, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget-boolean v1, p0, Lfxy;->j:Z

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->a(Lfxz;Z)V

    .line 138
    invoke-static {p1}, Ldrm;->a(Lidh;)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lfxy;->a(IZ)V

    .line 139
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lfxy;->e:Lfxf;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lfxy;->h:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    invoke-virtual {p0}, Lfxy;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfxy;->h:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lfxy;->a(IZ)V

    .line 154
    iget-object v0, p0, Lfxy;->e:Lfxf;

    invoke-interface {v0, p1}, Lfxf;->d(I)V

    goto :goto_0
.end method

.method protected measureChildren(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 94
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lfxy;->i:I

    .line 95
    sget v0, Lfxy;->d:I

    .line 99
    iget-object v1, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget v2, p0, Lfxy;->i:I

    invoke-static {v1, v2, v4, v3, v3}, Lfxy;->a(Landroid/view/View;IIII)V

    .line 100
    iget-object v1, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 102
    iget-object v1, p0, Lfxy;->g:Lfya;

    iget v2, p0, Lfxy;->i:I

    invoke-static {v1, v2, v4, v3, v3}, Lfxy;->a(Landroid/view/View;IIII)V

    .line 104
    iget-object v1, p0, Lfxy;->g:Lfya;

    invoke-virtual {v1}, Lfya;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 106
    iget-object v1, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget v2, p0, Lfxy;->i:I

    invoke-static {v1, v2, v4, v0, v4}, Lfxy;->a(Landroid/view/View;IIII)V

    .line 108
    iget-object v1, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget-object v2, p0, Lfxy;->f:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    .line 109
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 108
    invoke-static {v1, v3, v2}, Lfxy;->a(Landroid/view/View;II)V

    .line 110
    iget-object v1, p0, Lfxy;->g:Lfya;

    iget v2, p0, Lfxy;->i:I

    invoke-static {v1, v2, v4, v0, v4}, Lfxy;->a(Landroid/view/View;IIII)V

    .line 112
    iget-object v1, p0, Lfxy;->g:Lfya;

    iget-object v2, p0, Lfxy;->g:Lfya;

    .line 113
    invoke-virtual {v2}, Lfya;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 112
    invoke-static {v1, v3, v0}, Lfxy;->a(Landroid/view/View;II)V

    .line 115
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-super {p0, p1}, Lfye;->onDraw(Landroid/graphics/Canvas;)V

    .line 121
    iget v0, p0, Lfxy;->i:I

    int-to-float v3, v0

    sget-object v5, Lfxy;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 122
    return-void
.end method
