.class public final Lmnd;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5156
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5182
    const/4 v0, 0x0

    .line 5183
    iget-object v1, p0, Lmnd;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 5184
    const/4 v0, 0x1

    iget-object v1, p0, Lmnd;->a:Ljava/lang/Float;

    .line 5185
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 5187
    :cond_0
    iget-object v1, p0, Lmnd;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 5188
    const/4 v1, 0x2

    iget-object v2, p0, Lmnd;->b:Ljava/lang/Float;

    .line 5189
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 5191
    :cond_1
    iget-object v1, p0, Lmnd;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 5192
    const/4 v1, 0x3

    iget-object v2, p0, Lmnd;->c:Ljava/lang/Float;

    .line 5193
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 5195
    :cond_2
    iget-object v1, p0, Lmnd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5196
    iput v0, p0, Lmnd;->ai:I

    .line 5197
    return v0
.end method

.method public a(Loxn;)Lmnd;
    .locals 2

    .prologue
    .line 5205
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5206
    sparse-switch v0, :sswitch_data_0

    .line 5210
    iget-object v1, p0, Lmnd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5211
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnd;->ah:Ljava/util/List;

    .line 5214
    :cond_1
    iget-object v1, p0, Lmnd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5216
    :sswitch_0
    return-object p0

    .line 5221
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnd;->a:Ljava/lang/Float;

    goto :goto_0

    .line 5225
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnd;->b:Ljava/lang/Float;

    goto :goto_0

    .line 5229
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnd;->c:Ljava/lang/Float;

    goto :goto_0

    .line 5206
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5167
    iget-object v0, p0, Lmnd;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 5168
    const/4 v0, 0x1

    iget-object v1, p0, Lmnd;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 5170
    :cond_0
    iget-object v0, p0, Lmnd;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 5171
    const/4 v0, 0x2

    iget-object v1, p0, Lmnd;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 5173
    :cond_1
    iget-object v0, p0, Lmnd;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 5174
    const/4 v0, 0x3

    iget-object v1, p0, Lmnd;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 5176
    :cond_2
    iget-object v0, p0, Lmnd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5178
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5152
    invoke-virtual {p0, p1}, Lmnd;->a(Loxn;)Lmnd;

    move-result-object v0

    return-object v0
.end method
