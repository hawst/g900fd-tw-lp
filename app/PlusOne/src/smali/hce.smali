.class public final Lhce;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lnzi;Landroid/graphics/Bitmap;)Lcom/google/android/libraries/photoeditor/core/FilterChain;
    .locals 13

    .prologue
    .line 91
    iget-object v7, p0, Lnzi;->b:Lpla;

    .line 92
    if-nez v7, :cond_1

    .line 93
    const/4 v0, 0x0

    .line 136
    :cond_0
    :goto_0
    return-object v0

    .line 97
    :cond_1
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-direct {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;-><init>()V

    .line 101
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 100
    new-instance v1, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3, v4, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget v2, v7, Lpla;->d:I

    const/high16 v8, -0x80000000

    if-eq v2, v8, :cond_2

    iget v2, v7, Lpla;->d:I

    packed-switch v2, :pswitch_data_0

    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_9

    .line 102
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    :pswitch_0
    iget v4, v7, Lpla;->d:I

    :cond_2
    iget-object v2, v7, Lpla;->c:Lpjd;

    if-eqz v2, :cond_6

    iget-object v2, v7, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    if-nez v2, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    iget-object v2, v7, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    iget-object v2, v2, Lpsi;->a:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/4 v8, 0x0

    cmpg-float v2, v2, v8

    if-ltz v2, :cond_4

    iget-object v2, v7, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    iget-object v2, v2, Lpsi;->c:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v8

    if-gtz v2, :cond_4

    iget-object v2, v7, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    iget-object v2, v2, Lpsi;->b:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/4 v8, 0x0

    cmpg-float v2, v2, v8

    if-ltz v2, :cond_4

    iget-object v2, v7, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    iget-object v2, v2, Lpsi;->d:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v8

    if-lez v2, :cond_5

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    iget-object v2, v7, Lpla;->c:Lpjd;

    iget-object v2, v2, Lpjd;->a:Lpsi;

    iget-object v2, v2, Lpsi;->a:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v8, v7, Lpla;->c:Lpjd;

    iget-object v8, v8, Lpjd;->a:Lpsi;

    iget-object v8, v8, Lpsi;->b:Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    iget-object v9, v7, Lpla;->c:Lpjd;

    iget-object v9, v9, Lpjd;->a:Lpsi;

    iget-object v9, v9, Lpsi;->c:Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget-object v10, v7, Lpla;->c:Lpjd;

    iget-object v10, v10, Lpjd;->a:Lpsi;

    iget-object v10, v10, Lpsi;->d:Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    invoke-virtual {v1, v2, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_6
    iget-object v2, v7, Lpla;->c:Lpjd;

    if-eqz v2, :cond_7

    iget-object v2, v7, Lpla;->c:Lpjd;

    iget v2, v2, Lpjd;->b:I

    const/4 v8, 0x2

    if-ne v2, v8, :cond_8

    const/4 v3, 0x2

    :cond_7
    :goto_2
    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Landroid/graphics/RectF;FIIII)V

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_8
    iget-object v2, v7, Lpla;->c:Lpjd;

    iget v2, v2, Lpjd;->b:I

    const/4 v8, 0x1

    if-ne v2, v8, :cond_7

    const/4 v3, 0x1

    goto :goto_2

    .line 106
    :cond_9
    iget-object v1, v7, Lpla;->a:[Lpme;

    if-eqz v1, :cond_0

    .line 111
    iget-object v8, v7, Lpla;->a:[Lpme;

    array-length v9, v8

    const/4 v1, 0x0

    move v7, v1

    :goto_3
    if-ge v7, v9, :cond_0

    aget-object v1, v8, v7

    .line 112
    invoke-static {v1}, Lhbt;->a(Lpme;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 113
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a()V

    .line 111
    :cond_a
    :goto_4
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_3

    .line 117
    :cond_b
    invoke-static {v1}, Lhbt;->b(Lpme;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 123
    sget-object v2, Lpgi;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_e

    sget-object v2, Lpgi;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpgi;

    iget-object v2, v1, Lpgi;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_c

    iget-object v2, v1, Lpgi;->d:Ljava/lang/Float;

    if-eqz v2, :cond_c

    iget-object v2, v1, Lpgi;->b:Ljava/lang/Float;

    if-eqz v2, :cond_c

    iget-object v2, v1, Lpgi;->c:Ljava/lang/Float;

    if-nez v2, :cond_d

    :cond_c
    const/4 v1, 0x0

    .line 124
    :goto_5
    if-nez v1, :cond_49

    .line 126
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 123
    :cond_d
    const/16 v2, 0x64

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/4 v3, 0x3

    iget-object v4, v1, Lpgi;->e:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x2

    iget-object v4, v1, Lpgi;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x0

    iget-object v4, v1, Lpgi;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xc

    iget-object v1, v1, Lpgi;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto :goto_5

    :cond_e
    sget-object v2, Lphk;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_11

    sget-object v2, Lphk;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lphk;

    iget-object v2, v1, Lphk;->b:Ljava/lang/Float;

    if-eqz v2, :cond_f

    iget-object v2, v1, Lphk;->c:Ljava/lang/Float;

    if-eqz v2, :cond_f

    iget-object v2, v1, Lphk;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_f

    iget-object v2, v1, Lphk;->e:Ljava/lang/Float;

    if-nez v2, :cond_10

    :cond_f
    const/4 v1, 0x0

    goto :goto_5

    :cond_10
    const/4 v2, 0x7

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/4 v3, 0x7

    const/16 v4, 0xf1

    invoke-static {v3, v4}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->getMaxValue(II)I

    move-result v3

    iget-object v4, v1, Lphk;->d:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x0

    invoke-static {v4, v5, v3}, Lhcg;->a(III)I

    move-result v3

    const/16 v4, 0xf1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    const/4 v3, 0x0

    iget-object v4, v1, Lphk;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x1

    iget-object v4, v1, Lphk;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xe

    iget-object v1, v1, Lphk;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_11
    sget-object v2, Lphn;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_16

    sget-object v2, Lphn;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lphn;

    iget-object v2, v1, Lphn;->e:Lpxa;

    if-eqz v2, :cond_48

    iget-object v2, v1, Lphn;->e:Lpxa;

    iget-object v2, v2, Lpxa;->a:Ljava/lang/Float;

    if-eqz v2, :cond_12

    iget-object v2, v1, Lphn;->e:Lpxa;

    iget-object v2, v2, Lpxa;->b:Ljava/lang/Float;

    if-nez v2, :cond_13

    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_13
    iget-object v2, v1, Lphn;->b:Ljava/lang/Float;

    if-eqz v2, :cond_14

    iget-object v2, v1, Lphn;->c:Ljava/lang/Float;

    if-eqz v2, :cond_14

    iget-object v2, v1, Lphn;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_14

    iget-object v2, v1, Lphn;->f:Ljava/lang/Float;

    if-eqz v2, :cond_14

    iget-object v2, v1, Lphn;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_14

    iget-object v2, v1, Lphn;->h:Ljava/lang/Float;

    if-eqz v2, :cond_14

    iget-object v2, v1, Lphn;->i:Ljava/lang/Float;

    if-eqz v2, :cond_14

    iget-object v2, v1, Lphn;->j:Ljava/lang/Float;

    if-nez v2, :cond_15

    :cond_14
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_15
    const/16 v2, 0x6b

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/16 v3, 0x18

    const v4, 0x477fff00    # 65535.0f

    iget-object v5, v1, Lphn;->e:Lpxa;

    iget-object v5, v5, Lpxa;->a:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x19

    const v4, 0x477fff00    # 65535.0f

    iget-object v5, v1, Lphn;->e:Lpxa;

    iget-object v5, v5, Lpxa;->b:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x11

    iget-object v4, v1, Lphn;->j:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x13

    iget-object v4, v1, Lphn;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xca

    const/4 v4, 0x2

    iget-object v5, v1, Lphn;->h:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xcb

    const/4 v4, 0x2

    iget-object v5, v1, Lphn;->i:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x12

    iget-object v4, v1, Lphn;->f:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x6

    iget-object v4, v1, Lphn;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xcc

    iget-object v4, v1, Lphn;->g:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x3

    iget-object v1, v1, Lphn;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_16
    sget-object v2, Lphq;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_17

    sget-object v2, Lphq;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lphq;

    invoke-static {v1}, Lhce;->a(Lphq;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    goto/16 :goto_5

    :cond_17
    sget-object v2, Lphy;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1b

    sget-object v2, Lphy;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lphy;

    iget-object v2, v1, Lphy;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_18

    iget-object v2, v1, Lphy;->c:Ljava/lang/Float;

    if-eqz v2, :cond_18

    iget-object v2, v1, Lphy;->d:Ljava/lang/Float;

    if-eqz v2, :cond_18

    iget-object v2, v1, Lphy;->e:Ljava/lang/Float;

    if-eqz v2, :cond_18

    iget-object v2, v1, Lphy;->f:Lpxa;

    if-eqz v2, :cond_18

    iget-object v2, v1, Lphy;->f:Lpxa;

    iget-object v2, v2, Lpxa;->a:Ljava/lang/Float;

    if-eqz v2, :cond_18

    iget-object v2, v1, Lphy;->f:Lpxa;

    iget-object v2, v2, Lpxa;->b:Ljava/lang/Float;

    if-eqz v2, :cond_18

    iget-object v2, v1, Lphy;->g:Ljava/lang/Float;

    if-nez v2, :cond_19

    :cond_18
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_19
    const/16 v2, 0xb

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v3

    const/16 v4, 0xc

    iget-object v2, v1, Lphy;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_1a

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_6
    invoke-virtual {v3, v4, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v2, 0x18

    const v4, 0x477fff00    # 65535.0f

    iget-object v5, v1, Lphy;->f:Lpxa;

    iget-object v5, v5, Lpxa;->a:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v2, 0x19

    const v4, 0x477fff00    # 65535.0f

    iget-object v5, v1, Lphy;->f:Lpxa;

    iget-object v5, v5, Lpxa;->b:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v2, 0x4

    iget-object v4, v1, Lphy;->g:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v2, 0x16

    iget-object v4, v1, Lphy;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v2, 0x17

    iget-object v4, v1, Lphy;->e:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v2, 0x13

    iget-object v1, v1, Lphy;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v3, v2, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v3

    goto/16 :goto_5

    :cond_1a
    const/4 v2, 0x0

    goto :goto_6

    :cond_1b
    sget-object v2, Lpiu;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1e

    sget-object v2, Lpiu;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpiu;

    iget-object v2, v1, Lpiu;->b:Ljava/lang/Float;

    if-eqz v2, :cond_1c

    iget-object v2, v1, Lpiu;->c:Ljava/lang/Float;

    if-eqz v2, :cond_1c

    iget-object v2, v1, Lpiu;->d:Ljava/lang/Float;

    if-nez v2, :cond_1d

    :cond_1c
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_1d
    const/16 v2, 0x72

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/16 v3, 0x12

    iget-object v4, v1, Lpiu;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x32

    const/high16 v4, 0x447a0000    # 1000.0f

    iget-object v5, v1, Lpiu;->c:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x33

    const/high16 v4, 0x447a0000    # 1000.0f

    iget-object v1, v1, Lpiu;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_1e
    sget-object v2, Lpkr;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_21

    sget-object v2, Lpkr;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpkr;

    iget-object v2, v1, Lpkr;->b:Ljava/lang/Float;

    if-eqz v2, :cond_1f

    iget-object v2, v1, Lpkr;->c:Ljava/lang/Float;

    if-nez v2, :cond_20

    :cond_1f
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_20
    const/16 v2, 0xd

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/16 v3, 0xf

    iget-object v4, v1, Lpkr;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x10

    iget-object v1, v1, Lpkr;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_21
    sget-object v2, Lpku;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_24

    sget-object v2, Lpku;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpku;

    iget-object v2, v1, Lpku;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_22

    iget-object v2, v1, Lpku;->c:Ljava/lang/Float;

    if-eqz v2, :cond_22

    iget-object v2, v1, Lpku;->b:Ljava/lang/Float;

    if-nez v2, :cond_23

    :cond_22
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_23
    const/16 v2, 0x9

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/4 v3, 0x3

    iget-object v4, v1, Lpku;->d:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x2

    iget-object v4, v1, Lpku;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xc

    iget-object v1, v1, Lpku;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_24
    sget-object v2, Lpma;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_27

    sget-object v2, Lpma;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpma;

    iget-object v2, v1, Lpma;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_25

    iget-object v2, v1, Lpma;->c:Ljava/lang/Float;

    if-eqz v2, :cond_25

    iget-object v2, v1, Lpma;->d:Ljava/lang/Float;

    if-eqz v2, :cond_25

    iget-object v2, v1, Lpma;->e:Ljava/lang/Float;

    if-eqz v2, :cond_25

    iget-object v2, v1, Lpma;->f:Ljava/lang/Float;

    if-eqz v2, :cond_25

    iget-object v2, v1, Lpma;->g:Ljava/lang/Float;

    if-nez v2, :cond_26

    :cond_25
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_26
    const/16 v2, 0xc8

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/4 v3, 0x3

    iget-object v4, v1, Lpma;->b:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x9

    iget-object v4, v1, Lpma;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x6

    iget-object v4, v1, Lpma;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x0

    iget-object v4, v1, Lpma;->f:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x2

    iget-object v4, v1, Lpma;->g:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x13

    iget-object v1, v1, Lpma;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_27
    sget-object v2, Lpmj;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2a

    sget-object v2, Lpmj;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpmj;

    iget-object v2, v1, Lpmj;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_28

    iget-object v2, v1, Lpmj;->c:Ljava/lang/Float;

    if-eqz v2, :cond_28

    iget-object v2, v1, Lpmj;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_28

    iget-object v2, v1, Lpmj;->e:Ljava/lang/Integer;

    if-nez v2, :cond_29

    :cond_28
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_29
    const/16 v2, 0x11

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/16 v3, 0xe0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xdf

    iget-object v4, v1, Lpmj;->b:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x9

    iget-object v4, v1, Lpmj;->d:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x67

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x71

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xdd

    iget-object v4, v1, Lpmj;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x69

    iget-object v1, v1, Lpmj;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_2a
    sget-object v2, Lpnc;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2d

    sget-object v2, Lpnc;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpnc;

    iget-object v2, v1, Lpnc;->b:Ljava/lang/Float;

    if-eqz v2, :cond_2b

    iget-object v2, v1, Lpnc;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2b

    iget-object v2, v1, Lpnc;->d:Ljava/lang/Float;

    if-nez v2, :cond_2c

    :cond_2b
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_2c
    const/16 v2, 0xd0

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/4 v3, 0x3

    iget-object v4, v1, Lpnc;->c:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x9

    iget-object v4, v1, Lpnc;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xe

    iget-object v1, v1, Lpnc;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_2d
    sget-object v2, Lpqb;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_30

    sget-object v2, Lpqb;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpqb;

    iget-object v2, v1, Lpqb;->b:Ljava/lang/Float;

    if-eqz v2, :cond_2e

    iget-object v2, v1, Lpqb;->c:Ljava/lang/Float;

    if-eqz v2, :cond_2e

    iget-object v2, v1, Lpqb;->d:Ljava/lang/Float;

    if-eqz v2, :cond_2e

    iget-object v2, v1, Lpqb;->e:Ljava/lang/Float;

    if-eqz v2, :cond_2e

    iget-object v2, v1, Lpqb;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_2e

    iget-object v2, v1, Lpqb;->g:Ljava/lang/Float;

    if-nez v2, :cond_2f

    :cond_2e
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_2f
    const/16 v2, 0xcf

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/4 v3, 0x3

    iget-object v4, v1, Lpqb;->f:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x9

    iget-object v4, v1, Lpqb;->g:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xa

    iget-object v4, v1, Lpqb;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xe

    iget-object v4, v1, Lpqb;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x0

    iget-object v4, v1, Lpqb;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x1

    iget-object v1, v1, Lpqb;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_30
    sget-object v2, Lpss;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_33

    sget-object v2, Lpss;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpss;

    iget-object v2, v1, Lpss;->b:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->c:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->d:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->e:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->f:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->g:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->h:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->i:Ljava/lang/Integer;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->j:Ljava/lang/Integer;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->k:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->l:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->m:Ljava/lang/Float;

    if-eqz v2, :cond_31

    iget-object v2, v1, Lpss;->n:Ljava/lang/Float;

    if-nez v2, :cond_32

    :cond_31
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_32
    const/16 v2, 0x10

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/4 v3, 0x3

    iget-object v4, v1, Lpss;->k:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x9

    iget-object v4, v1, Lpss;->n:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x0

    iget-object v4, v1, Lpss;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x1

    iget-object v4, v1, Lpss;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x2

    iget-object v4, v1, Lpss;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x13

    iget-object v4, v1, Lpss;->e:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x6

    iget-object v4, v1, Lpss;->f:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xe8

    iget-object v4, v1, Lpss;->g:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xe9

    iget-object v4, v1, Lpss;->h:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x66

    iget-object v4, v1, Lpss;->i:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x69

    iget-object v4, v1, Lpss;->j:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xeb

    iget-object v4, v1, Lpss;->m:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xea

    iget-object v1, v1, Lpss;->l:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_33
    sget-object v2, Lptp;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_34

    sget-object v2, Lptp;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lptp;

    invoke-static {v1, p1}, Lhce;->a(Lptp;Landroid/graphics/Bitmap;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    goto/16 :goto_5

    :cond_34
    sget-object v2, Lpun;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_39

    sget-object v2, Lpun;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lpun;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    iget-object v1, v4, Lpun;->c:Lpjd;

    if-eqz v1, :cond_48

    iget-object v1, v4, Lpun;->c:Lpjd;

    iget-object v1, v1, Lpjd;->a:Lpsi;

    if-eqz v1, :cond_48

    iget-object v1, v4, Lpun;->c:Lpjd;

    iget-object v1, v1, Lpjd;->a:Lpsi;

    iget-object v1, v1, Lpsi;->a:Ljava/lang/Float;

    if-eqz v1, :cond_35

    iget-object v1, v4, Lpun;->c:Lpjd;

    iget-object v1, v1, Lpjd;->a:Lpsi;

    iget-object v1, v1, Lpsi;->b:Ljava/lang/Float;

    if-eqz v1, :cond_35

    iget-object v1, v4, Lpun;->c:Lpjd;

    iget-object v1, v1, Lpjd;->a:Lpsi;

    iget-object v1, v1, Lpsi;->c:Ljava/lang/Float;

    if-eqz v1, :cond_35

    iget-object v1, v4, Lpun;->c:Lpjd;

    iget-object v1, v1, Lpjd;->a:Lpsi;

    iget-object v1, v1, Lpsi;->d:Ljava/lang/Float;

    if-nez v1, :cond_36

    :cond_35
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_36
    iget-object v1, v4, Lpun;->b:Ljava/lang/Float;

    if-eqz v1, :cond_48

    const/4 v3, 0x0

    iget-object v1, v4, Lpun;->c:Lpjd;

    iget v1, v1, Lpjd;->b:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_38

    const/4 v3, 0x2

    :cond_37
    :goto_7
    iget-object v1, v4, Lpun;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v2, -0x3dcc0000    # -45.0f

    const/high16 v10, 0x42340000    # 45.0f

    invoke-static {v1, v2, v10}, Lhcg;->a(FFF)F

    move-result v2

    new-instance v1, Landroid/graphics/RectF;

    iget-object v10, v4, Lpun;->c:Lpjd;

    iget-object v10, v10, Lpjd;->a:Lpsi;

    iget-object v10, v10, Lpsi;->a:Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iget-object v11, v4, Lpun;->c:Lpjd;

    iget-object v11, v11, Lpjd;->a:Lpsi;

    iget-object v11, v11, Lpsi;->b:Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    iget-object v12, v4, Lpun;->c:Lpjd;

    iget-object v12, v12, Lpjd;->a:Lpsi;

    iget-object v12, v12, Lpsi;->c:Ljava/lang/Float;

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v12

    iget-object v4, v4, Lpun;->c:Lpjd;

    iget-object v4, v4, Lpjd;->a:Lpsi;

    iget-object v4, v4, Lpsi;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-direct {v1, v10, v11, v12, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Landroid/graphics/RectF;FIIII)V

    const/16 v1, 0x14

    invoke-static {v1}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    goto/16 :goto_5

    :cond_38
    iget-object v1, v4, Lpun;->c:Lpjd;

    iget v1, v1, Lpjd;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_37

    const/4 v3, 0x1

    goto :goto_7

    :cond_39
    sget-object v2, Lpvv;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3e

    sget-object v2, Lpvv;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpvv;

    iget-object v2, v1, Lpvv;->d:Lpxa;

    if-eqz v2, :cond_48

    iget-object v2, v1, Lpvv;->d:Lpxa;

    iget-object v2, v2, Lpxa;->a:Ljava/lang/Float;

    if-eqz v2, :cond_3a

    iget-object v2, v1, Lpvv;->d:Lpxa;

    iget-object v2, v2, Lpxa;->b:Ljava/lang/Float;

    if-nez v2, :cond_3b

    :cond_3a
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_3b
    iget-object v2, v1, Lpvv;->b:Ljava/lang/Float;

    if-eqz v2, :cond_3c

    iget-object v2, v1, Lpvv;->c:Ljava/lang/Float;

    if-eqz v2, :cond_3c

    iget-object v2, v1, Lpvv;->e:Ljava/lang/Float;

    if-eqz v2, :cond_3c

    iget-object v2, v1, Lpvv;->f:Ljava/lang/Float;

    if-eqz v2, :cond_3c

    iget-object v2, v1, Lpvv;->g:Ljava/lang/Float;

    if-eqz v2, :cond_3c

    iget-object v2, v1, Lpvv;->h:Ljava/lang/Float;

    if-eqz v2, :cond_3c

    iget-object v2, v1, Lpvv;->i:Ljava/lang/Float;

    if-eqz v2, :cond_3c

    iget-object v2, v1, Lpvv;->j:Ljava/lang/Float;

    if-eqz v2, :cond_3c

    iget-object v2, v1, Lpvv;->k:Ljava/lang/Integer;

    if-nez v2, :cond_3d

    :cond_3c
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_3d
    const/16 v2, 0xe

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/16 v3, 0x18

    const v4, 0x477fff00    # 65535.0f

    iget-object v5, v1, Lpvv;->d:Lpxa;

    iget-object v5, v5, Lpxa;->a:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x19

    const v4, 0x477fff00    # 65535.0f

    iget-object v5, v1, Lpvv;->d:Lpxa;

    iget-object v5, v5, Lpxa;->b:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x11

    iget-object v4, v1, Lpvv;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x0

    iget-object v4, v1, Lpvv;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x1

    iget-object v4, v1, Lpvv;->e:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xc9

    const/4 v4, 0x2

    iget-object v5, v1, Lpvv;->f:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xca

    const/4 v4, 0x2

    iget-object v5, v1, Lpvv;->g:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x12

    iget-object v4, v1, Lpvv;->h:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const v5, 0x49742400    # 1000000.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x2

    iget-object v4, v1, Lpvv;->i:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x13

    iget-object v4, v1, Lpvv;->j:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x3

    iget-object v1, v1, Lpvv;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_3e
    sget-object v2, Lpwd;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_41

    sget-object v2, Lpwd;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpwd;

    iget-object v2, v1, Lpwd;->b:Ljava/lang/Float;

    if-eqz v2, :cond_3f

    iget-object v2, v1, Lpwd;->c:Ljava/lang/Float;

    if-eqz v2, :cond_3f

    iget-object v2, v1, Lpwd;->d:Ljava/lang/Float;

    if-eqz v2, :cond_3f

    iget-object v2, v1, Lpwd;->e:Ljava/lang/Float;

    if-eqz v2, :cond_3f

    iget-object v2, v1, Lpwd;->f:Ljava/lang/Float;

    if-nez v2, :cond_40

    :cond_3f
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_40
    const/16 v2, 0x35

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/16 v3, 0x1a

    iget-object v4, v1, Lpwd;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x1b

    iget-object v4, v1, Lpwd;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x1c

    iget-object v4, v1, Lpwd;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x1d

    iget-object v4, v1, Lpwd;->e:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x1e

    iget-object v1, v1, Lpwd;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_41
    sget-object v2, Lpwp;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_44

    sget-object v2, Lpwp;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpwp;

    iget-object v2, v1, Lpwp;->b:Ljava/lang/Float;

    if-eqz v2, :cond_42

    iget-object v2, v1, Lpwp;->c:Ljava/lang/Float;

    if-eqz v2, :cond_42

    iget-object v2, v1, Lpwp;->d:Ljava/lang/Float;

    if-eqz v2, :cond_42

    iget-object v2, v1, Lpwp;->e:Ljava/lang/Float;

    if-eqz v2, :cond_42

    iget-object v2, v1, Lpwp;->f:Ljava/lang/Float;

    if-nez v2, :cond_43

    :cond_42
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_43
    const/4 v2, 0x4

    invoke-static {v2}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, v1, Lpwp;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x1

    iget-object v4, v1, Lpwp;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v3, 0x2

    iget-object v4, v1, Lpwp;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0x14

    iget-object v4, v1, Lpwp;->e:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v3, 0xb

    iget-object v1, v1, Lpwp;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v2

    goto/16 :goto_5

    :cond_44
    sget-object v2, Lpxg;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_48

    sget-object v2, Lpxg;->a:Loxr;

    invoke-virtual {v1, v2}, Lpme;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxg;

    iget-object v2, v1, Lpxg;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_45

    iget-object v2, v1, Lpxg;->c:Ljava/lang/Float;

    if-eqz v2, :cond_45

    iget-object v2, v1, Lpxg;->d:Ljava/lang/Float;

    if-eqz v2, :cond_45

    iget-object v2, v1, Lpxg;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_45

    iget-object v2, v1, Lpxg;->f:Ljava/lang/Float;

    if-eqz v2, :cond_45

    iget-object v2, v1, Lpxg;->g:Ljava/lang/Float;

    if-nez v2, :cond_46

    :cond_45
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_46
    const/4 v2, 0x0

    iget-object v3, v1, Lpxg;->e:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_47

    const/high16 v2, 0x3f000000    # 0.5f

    :cond_47
    const/16 v3, 0xca

    invoke-static {v3}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v3

    const/16 v4, 0x320

    iget-object v5, v1, Lpxg;->b:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v4, 0x9

    iget-object v5, v1, Lpxg;->c:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v4, 0x6

    iget-object v5, v1, Lpxg;->d:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v4, 0x0

    iget-object v5, v1, Lpxg;->f:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/4 v4, 0x2

    iget-object v1, v1, Lpxg;->g:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v3, v4, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    const/16 v1, 0x13

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v3, v1, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-object v1, v3

    goto/16 :goto_5

    :cond_48
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 129
    :cond_49
    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v2

    const/16 v3, 0x14

    if-eq v2, v3, :cond_a

    .line 132
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    goto/16 :goto_4

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lphq;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 522
    const/16 v0, 0x16

    .line 523
    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    .line 524
    iget-object v0, p0, Lphq;->b:[Lphs;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 559
    :goto_0
    return-object v0

    .line 528
    :cond_0
    iget-object v4, p0, Lphq;->b:[Lphs;

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_5

    aget-object v6, v4, v3

    .line 529
    const/16 v0, 0x192

    .line 530
    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v7

    .line 531
    if-nez v7, :cond_1

    move-object v0, v2

    .line 532
    goto :goto_0

    .line 534
    :cond_1
    if-nez v6, :cond_2

    move-object v0, v2

    .line 535
    goto :goto_0

    .line 537
    :cond_2
    iget-object v0, v6, Lphs;->b:[B

    if-nez v0, :cond_3

    move-object v0, v2

    .line 538
    goto :goto_0

    .line 540
    :cond_3
    const/16 v0, 0x391

    iget-object v8, v6, Lphs;->b:[B

    invoke-virtual {v7, v0, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterBuffer(I[B)Z

    .line 541
    const/16 v0, 0x392

    iget-object v8, v6, Lphs;->b:[B

    array-length v8, v8

    .line 542
    invoke-virtual {v7, v0, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 543
    const/16 v0, 0x385

    iget-object v8, v6, Lphs;->c:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7, v0, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 544
    const/16 v0, 0x38f

    iget-object v8, v6, Lphs;->d:Ljava/lang/Integer;

    .line 545
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7, v0, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 546
    const/16 v0, 0x390

    iget-object v8, v6, Lphs;->e:Ljava/lang/Float;

    .line 547
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v7, v0, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 548
    const/16 v0, 0x393

    iget-object v8, v6, Lphs;->f:Ljava/lang/Float;

    .line 549
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v7, v0, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 550
    const/16 v8, 0x394

    iget-object v0, v6, Lphs;->g:Ljava/lang/Boolean;

    .line 552
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    const/high16 v0, 0x3f800000    # 1.0f

    .line 551
    :goto_2
    invoke-virtual {v7, v8, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 553
    const/16 v0, 0x399

    iget-object v6, v6, Lphs;->h:Ljava/lang/Float;

    .line 554
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v7, v0, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 556
    invoke-virtual {v1, v7}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->addSubParameters(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 528
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 552
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    move-object v0, v1

    .line 559
    goto :goto_0
.end method

.method private static a(Lptp;Landroid/graphics/Bitmap;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v4, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v11, 0x42c80000    # 100.0f

    const/4 v10, 0x0

    .line 827
    iget-object v0, p0, Lptp;->b:[Lptr;

    if-nez v0, :cond_0

    .line 828
    const/4 v0, 0x3

    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 888
    :goto_0
    return-object v0

    .line 831
    :cond_0
    iget-object v0, p0, Lptp;->b:[Lptr;

    array-length v0, v0

    const/16 v1, 0x8

    if-le v0, v1, :cond_1

    .line 832
    const/4 v0, 0x0

    goto :goto_0

    .line 835
    :cond_1
    const/4 v0, 0x3

    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    .line 837
    iget-object v5, p0, Lptp;->b:[Lptr;

    array-length v6, v5

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_6

    aget-object v7, v5, v3

    .line 838
    iget-object v0, v7, Lptr;->b:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, v7, Lptr;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, v7, Lptr;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, v7, Lptr;->e:Lpxa;

    if-eqz v0, :cond_2

    iget-object v0, v7, Lptr;->e:Lpxa;

    iget-object v0, v0, Lpxa;->a:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, v7, Lptr;->e:Lpxa;

    iget-object v0, v0, Lpxa;->b:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, v7, Lptr;->f:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, v7, Lptr;->e:Lpxa;

    iget-object v0, v0, Lpxa;->a:Ljava/lang/Float;

    .line 845
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v0, v10

    if-ltz v0, :cond_2

    iget-object v0, v7, Lptr;->e:Lpxa;

    iget-object v0, v0, Lpxa;->a:Ljava/lang/Float;

    .line 846
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v0, v12

    if-gtz v0, :cond_2

    iget-object v0, v7, Lptr;->e:Lpxa;

    iget-object v0, v0, Lpxa;->b:Ljava/lang/Float;

    .line 847
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v0, v10

    if-ltz v0, :cond_2

    iget-object v0, v7, Lptr;->e:Lpxa;

    iget-object v0, v0, Lpxa;->b:Ljava/lang/Float;

    .line 848
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v0, v12

    if-lez v0, :cond_3

    .line 849
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 853
    :cond_3
    const/16 v0, 0x12c

    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v8

    .line 854
    const/16 v0, 0xca

    invoke-virtual {v8, v0, v10}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 855
    const/16 v0, 0xcb

    invoke-virtual {v8, v0, v10}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 856
    iget-object v0, v7, Lptr;->b:Ljava/lang/Float;

    .line 857
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v11

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 856
    invoke-virtual {v8, v4, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 858
    iget-object v0, v7, Lptr;->c:Ljava/lang/Float;

    .line 859
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v11

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 858
    invoke-virtual {v8, v13, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 860
    const/4 v0, 0x2

    iget-object v1, v7, Lptr;->d:Ljava/lang/Float;

    .line 861
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v11

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    .line 860
    invoke-virtual {v8, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 862
    const/16 v0, 0x1f5

    iget-object v1, v7, Lptr;->e:Lpxa;

    iget-object v1, v1, Lpxa;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v8, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 863
    const/16 v0, 0x1f6

    iget-object v1, v7, Lptr;->e:Lpxa;

    iget-object v1, v1, Lpxa;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v8, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 866
    iget-object v0, v7, Lptr;->e:Lpxa;

    iget-object v0, v0, Lpxa;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 867
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-lt v0, v1, :cond_4

    .line 868
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 871
    :cond_4
    iget-object v1, v7, Lptr;->e:Lpxa;

    iget-object v1, v1, Lpxa;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v1, v9

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 872
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    if-lt v1, v9, :cond_5

    .line 873
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 877
    :cond_5
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    .line 879
    const/16 v1, 0xc9

    const v9, 0xffffff

    and-int/2addr v0, v9

    int-to-float v0, v0

    invoke-virtual {v8, v1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 881
    iget-object v0, v7, Lptr;->f:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v11

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 882
    const/16 v1, 0x64

    invoke-static {v0, v13, v1}, Lhcg;->a(III)I

    move-result v0

    .line 883
    const/4 v1, 0x4

    int-to-float v0, v0

    invoke-virtual {v8, v1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    .line 885
    invoke-virtual {v2, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->addSubParameters(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 837
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    :cond_6
    move-object v0, v2

    .line 888
    goto/16 :goto_0
.end method

.method public static a([B)Lnzi;
    .locals 3

    .prologue
    .line 1667
    :try_start_0
    new-instance v0, Lnzi;

    invoke-direct {v0}, Lnzi;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzi;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 1670
    :goto_0
    return-object v0

    .line 1668
    :catch_0
    move-exception v0

    .line 1669
    const-string v1, "ProtoPacker"

    const-string v2, "Failed to deserialize EditInfo."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1670
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)Lpme;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 1248
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    const/16 v1, 0x16

    if-eq v0, v1, :cond_0

    move-object v0, v4

    .line 1286
    :goto_0
    return-object v0

    .line 1252
    :cond_0
    new-instance v5, Lphq;

    invoke-direct {v5}, Lphq;-><init>()V

    .line 1254
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getSubParameters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lphs;

    iput-object v0, v5, Lphq;->b:[Lphs;

    .line 1256
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getSubParameters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 1257
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v3

    const/16 v7, 0x192

    if-eq v3, v7, :cond_1

    move-object v0, v4

    .line 1258
    goto :goto_0

    .line 1261
    :cond_1
    new-instance v7, Lphs;

    invoke-direct {v7}, Lphs;-><init>()V

    .line 1262
    const/16 v3, 0x392

    .line 1263
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, v7, Lphs;->b:[B

    .line 1264
    const/16 v3, 0x391

    .line 1265
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterBuffer(I)[B

    move-result-object v3

    iget-object v8, v7, Lphs;->b:[B

    iget-object v9, v7, Lphs;->b:[B

    array-length v9, v9

    .line 1264
    invoke-static {v3, v2, v8, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1267
    const/16 v3, 0x385

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v7, Lphs;->c:Ljava/lang/Integer;

    .line 1268
    const/16 v3, 0x38f

    .line 1269
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v3

    .line 1268
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v7, Lphs;->d:Ljava/lang/Integer;

    .line 1270
    const/16 v3, 0x390

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v7, Lphs;->e:Ljava/lang/Float;

    .line 1271
    const/16 v3, 0x393

    .line 1272
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v3

    .line 1271
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v7, Lphs;->f:Ljava/lang/Float;

    .line 1273
    const/16 v3, 0x394

    .line 1275
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    .line 1274
    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v7, Lphs;->g:Ljava/lang/Boolean;

    .line 1277
    const/16 v3, 0x399

    .line 1278
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v0

    .line 1277
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphs;->h:Ljava/lang/Float;

    .line 1280
    iget-object v3, v5, Lphq;->b:[Lphs;

    add-int/lit8 v0, v1, 0x1

    aput-object v7, v3, v1

    move v1, v0

    .line 1281
    goto/16 :goto_1

    :cond_2
    move v3, v2

    .line 1275
    goto :goto_2

    .line 1283
    :cond_3
    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    .line 1284
    sget-object v1, Lphq;->a:Loxr;

    invoke-virtual {v0, v1, v5}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private static a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Ljava/util/ArrayList;Lnzi;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;",
            "Ljava/util/ArrayList",
            "<",
            "Lpme;",
            ">;",
            "Lnzi;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v1, 0xca

    const/16 v0, 0xc9

    .line 1145
    const/16 v2, 0xc

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v2

    .line 1146
    if-nez v2, :cond_4

    .line 1148
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterBuffer(I)[B

    move-result-object v0

    .line 1149
    if-eqz v0, :cond_0

    array-length v2, v0

    if-nez v2, :cond_1

    .line 1150
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterBuffer(I)[B

    move-result-object v0

    .line 1152
    :cond_1
    if-eqz v0, :cond_2

    array-length v1, v0

    if-nez v1, :cond_3

    .line 1184
    :cond_2
    :goto_0
    return-void

    .line 1157
    :cond_3
    invoke-static {v0}, Lhce;->b([B)Lpla;

    move-result-object v0

    .line 1158
    if-eqz v0, :cond_2

    .line 1162
    iget-object v1, v0, Lpla;->e:Lplb;

    if-eqz v1, :cond_2

    .line 1163
    iget-object v1, p2, Lnzi;->b:Lpla;

    iget-object v0, v0, Lpla;->e:Lplb;

    iput-object v0, v1, Lpla;->e:Lplb;

    goto :goto_0

    .line 1167
    :cond_4
    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterBuffer(I)[B

    move-result-object v0

    .line 1170
    invoke-static {v0}, Lhce;->b([B)Lpla;

    move-result-object v0

    .line 1171
    if-eqz v0, :cond_2

    .line 1175
    iget-object v1, v0, Lpla;->e:Lplb;

    if-eqz v1, :cond_5

    .line 1176
    iget-object v1, p2, Lnzi;->b:Lpla;

    iget-object v2, v0, Lpla;->e:Lplb;

    iput-object v2, v1, Lpla;->e:Lplb;

    .line 1178
    :cond_5
    iget-object v1, v0, Lpla;->a:[Lpme;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1179
    invoke-static {v3}, Lhbt;->a(Lpme;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1180
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1178
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    move v0, v1

    .line 1167
    goto :goto_1
.end method

.method public static a(Lcom/google/android/libraries/photoeditor/core/FilterChain;Lnzi;)Z
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v10, 0x42c80000    # 100.0f

    .line 143
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 194
    :cond_0
    :goto_0
    return v2

    .line 147
    :cond_1
    new-instance v0, Lpla;

    invoke-direct {v0}, Lpla;-><init>()V

    iput-object v0, p1, Lnzi;->b:Lpla;

    .line 149
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v0

    .line 153
    if-eqz v0, :cond_2

    if-eq v0, v1, :cond_2

    if-eq v0, v11, :cond_2

    if-ne v0, v12, :cond_0

    .line 157
    :cond_2
    iget-object v3, p1, Lnzi;->b:Lpla;

    iput v0, v3, Lpla;->d:I

    .line 161
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->f()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 164
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getFilterNodes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v3, v0

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    .line 165
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;->getFilterParameter()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v6

    .line 166
    invoke-virtual {v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    const/16 v7, 0x12

    if-ne v0, v7, :cond_6

    .line 167
    invoke-static {v6, v4, p1}, Lhce;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Ljava/util/ArrayList;Lnzi;)V

    .line 168
    if-eqz v3, :cond_8

    .line 169
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_2

    .line 161
    :cond_3
    new-instance v3, Lpun;

    invoke-direct {v3}, Lpun;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getRotationAngle()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v3, Lpun;->b:Ljava/lang/Float;

    new-instance v0, Lpjd;

    invoke-direct {v0}, Lpjd;-><init>()V

    iput-object v0, v3, Lpun;->c:Lpjd;

    iget-object v0, v3, Lpun;->c:Lpjd;

    iput v2, v0, Lpjd;->b:I

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getAspectRatio()I

    move-result v0

    if-ne v0, v11, :cond_5

    iget-object v0, v3, Lpun;->c:Lpjd;

    iput v11, v0, Lpjd;->b:I

    :cond_4
    :goto_3
    iget-object v0, v3, Lpun;->c:Lpjd;

    new-instance v5, Lpsi;

    invoke-direct {v5}, Lpsi;-><init>()V

    iput-object v5, v0, Lpjd;->a:Lpsi;

    iget-object v0, v3, Lpun;->c:Lpjd;

    iget-object v0, v0, Lpjd;->a:Lpsi;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectX()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    iput-object v5, v0, Lpsi;->a:Ljava/lang/Float;

    iget-object v0, v3, Lpun;->c:Lpjd;

    iget-object v0, v0, Lpjd;->a:Lpsi;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectY()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    iput-object v5, v0, Lpsi;->b:Ljava/lang/Float;

    iget-object v0, v3, Lpun;->c:Lpjd;

    iget-object v0, v0, Lpjd;->a:Lpsi;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectX()F

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectWidth()F

    move-result v6

    add-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    iput-object v5, v0, Lpsi;->c:Ljava/lang/Float;

    iget-object v0, v3, Lpun;->c:Lpjd;

    iget-object v0, v0, Lpjd;->a:Lpsi;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectY()F

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectHeight()F

    move-result v6

    add-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    iput-object v5, v0, Lpsi;->d:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v5, Lpun;->a:Loxr;

    invoke-virtual {v0, v5, v3}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getAspectRatio()I

    move-result v0

    if-ne v0, v1, :cond_4

    iget-object v0, v3, Lpun;->c:Lpjd;

    iput v1, v0, Lpjd;->b:I

    goto :goto_3

    .line 173
    :cond_6
    if-eqz v3, :cond_7

    .line 174
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    const/4 v3, 0x0

    .line 177
    :cond_7
    invoke-virtual {v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :sswitch_0
    const/4 v0, 0x0

    .line 178
    :goto_4
    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    move-object v0, v3

    move-object v3, v0

    .line 183
    goto/16 :goto_2

    .line 177
    :sswitch_1
    new-instance v7, Lphk;

    invoke-direct {v7}, Lphk;-><init>()V

    const/16 v0, 0xf1

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lphk;->d:Ljava/lang/Integer;

    invoke-virtual {v6, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphk;->b:Ljava/lang/Float;

    invoke-virtual {v6, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphk;->c:Ljava/lang/Float;

    const/16 v0, 0xe

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphk;->e:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lphk;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto :goto_4

    :sswitch_2
    new-instance v7, Lphn;

    invoke-direct {v7}, Lphn;-><init>()V

    new-instance v0, Lpxa;

    invoke-direct {v0}, Lpxa;-><init>()V

    iput-object v0, v7, Lphn;->e:Lpxa;

    const/16 v0, 0x13

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphn;->b:Ljava/lang/Float;

    const/16 v0, 0x11

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphn;->j:Ljava/lang/Float;

    const/4 v0, 0x6

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphn;->c:Ljava/lang/Float;

    iget-object v0, v7, Lphn;->e:Lpxa;

    const/16 v8, 0x18

    invoke-virtual {v6, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    iput-object v8, v0, Lpxa;->a:Ljava/lang/Float;

    iget-object v0, v7, Lphn;->e:Lpxa;

    const/16 v8, 0x19

    invoke-virtual {v6, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    iput-object v8, v0, Lpxa;->b:Ljava/lang/Float;

    const/16 v0, 0xca

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphn;->h:Ljava/lang/Float;

    const/16 v0, 0xcb

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphn;->i:Ljava/lang/Float;

    invoke-virtual {v6, v12}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lphn;->d:Ljava/lang/Integer;

    const/16 v0, 0xcc

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lphn;->g:Ljava/lang/Integer;

    const/16 v0, 0x12

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphn;->f:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lphn;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_3
    invoke-static {v6}, Lhce;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)Lpme;

    move-result-object v0

    goto/16 :goto_4

    :sswitch_4
    new-instance v7, Lphy;

    invoke-direct {v7}, Lphy;-><init>()V

    const/16 v0, 0xc

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    if-lez v0, :cond_9

    move v0, v1

    :goto_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lphy;->b:Ljava/lang/Integer;

    new-instance v0, Lpxa;

    invoke-direct {v0}, Lpxa;-><init>()V

    iput-object v0, v7, Lphy;->f:Lpxa;

    iget-object v0, v7, Lphy;->f:Lpxa;

    const/16 v8, 0x18

    invoke-virtual {v6, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    iput-object v8, v0, Lpxa;->a:Ljava/lang/Float;

    iget-object v0, v7, Lphy;->f:Lpxa;

    const/16 v8, 0x19

    invoke-virtual {v6, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    iput-object v8, v0, Lpxa;->b:Ljava/lang/Float;

    const/4 v0, 0x4

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphy;->g:Ljava/lang/Float;

    const/16 v0, 0x16

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphy;->d:Ljava/lang/Float;

    const/16 v0, 0x17

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphy;->e:Ljava/lang/Float;

    const/16 v0, 0x13

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lphy;->c:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lphy;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_9
    move v0, v2

    goto :goto_5

    :sswitch_5
    new-instance v7, Lpiu;

    invoke-direct {v7}, Lpiu;-><init>()V

    const/16 v0, 0x12

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpiu;->b:Ljava/lang/Float;

    const/16 v0, 0x32

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v8, 0x447a0000    # 1000.0f

    div-float/2addr v0, v8

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpiu;->c:Ljava/lang/Float;

    const/16 v0, 0x33

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpiu;->d:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpiu;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_6
    new-instance v7, Lpkr;

    invoke-direct {v7}, Lpkr;-><init>()V

    const/16 v0, 0xf

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpkr;->b:Ljava/lang/Float;

    const/16 v0, 0x10

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpkr;->c:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpkr;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_7
    new-instance v7, Lpku;

    invoke-direct {v7}, Lpku;-><init>()V

    invoke-virtual {v6, v12}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpku;->d:Ljava/lang/Integer;

    invoke-virtual {v6, v11}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpku;->c:Ljava/lang/Float;

    const/16 v0, 0xc

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpku;->b:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpku;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_8
    new-instance v7, Lpma;

    invoke-direct {v7}, Lpma;-><init>()V

    invoke-virtual {v6, v12}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpma;->b:Ljava/lang/Integer;

    const/16 v0, 0x9

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpma;->c:Ljava/lang/Float;

    const/4 v0, 0x6

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpma;->d:Ljava/lang/Float;

    const/16 v0, 0x13

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpma;->e:Ljava/lang/Float;

    invoke-virtual {v6, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpma;->f:Ljava/lang/Float;

    invoke-virtual {v6, v11}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpma;->g:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpma;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_9
    new-instance v7, Lpmj;

    invoke-direct {v7}, Lpmj;-><init>()V

    const/16 v0, 0xdf

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpmj;->b:Ljava/lang/Integer;

    const/16 v0, 0x9

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    if-lez v0, :cond_a

    move v0, v1

    :goto_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpmj;->d:Ljava/lang/Integer;

    const/16 v0, 0xdd

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpmj;->c:Ljava/lang/Float;

    const/16 v0, 0x69

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpmj;->e:Ljava/lang/Integer;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpmj;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_a
    move v0, v2

    goto :goto_6

    :sswitch_a
    new-instance v7, Lpgi;

    invoke-direct {v7}, Lpgi;-><init>()V

    invoke-virtual {v6, v12}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpgi;->e:Ljava/lang/Integer;

    invoke-virtual {v6, v11}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpgi;->d:Ljava/lang/Float;

    const/16 v0, 0xc

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpgi;->b:Ljava/lang/Float;

    invoke-virtual {v6, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpgi;->c:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpgi;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_b
    new-instance v7, Lpnc;

    invoke-direct {v7}, Lpnc;-><init>()V

    invoke-virtual {v6, v12}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpnc;->c:Ljava/lang/Integer;

    const/16 v0, 0x9

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpnc;->d:Ljava/lang/Float;

    const/16 v0, 0xe

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpnc;->b:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpnc;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_c
    new-instance v7, Lpqb;

    invoke-direct {v7}, Lpqb;-><init>()V

    invoke-virtual {v6, v12}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpqb;->f:Ljava/lang/Integer;

    const/16 v0, 0x9

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpqb;->g:Ljava/lang/Float;

    invoke-virtual {v6, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpqb;->d:Ljava/lang/Float;

    invoke-virtual {v6, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpqb;->e:Ljava/lang/Float;

    const/16 v0, 0xa

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpqb;->b:Ljava/lang/Float;

    const/16 v0, 0xe

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpqb;->c:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpqb;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_d
    new-instance v7, Lpss;

    invoke-direct {v7}, Lpss;-><init>()V

    invoke-virtual {v6, v12}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->k:Ljava/lang/Float;

    const/16 v0, 0x9

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->n:Ljava/lang/Float;

    invoke-virtual {v6, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->b:Ljava/lang/Float;

    invoke-virtual {v6, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->c:Ljava/lang/Float;

    invoke-virtual {v6, v11}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->d:Ljava/lang/Float;

    const/16 v0, 0x13

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->e:Ljava/lang/Float;

    const/4 v0, 0x6

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->f:Ljava/lang/Float;

    const/16 v0, 0xe8

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->g:Ljava/lang/Float;

    const/16 v0, 0xe9

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->h:Ljava/lang/Float;

    const/16 v0, 0x66

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpss;->i:Ljava/lang/Integer;

    const/16 v0, 0x69

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpss;->j:Ljava/lang/Integer;

    const/16 v0, 0xea

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->l:Ljava/lang/Float;

    const/16 v0, 0xeb

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpss;->m:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpss;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_e
    new-instance v7, Lpvv;

    invoke-direct {v7}, Lpvv;-><init>()V

    new-instance v0, Lpxa;

    invoke-direct {v0}, Lpxa;-><init>()V

    iput-object v0, v7, Lpvv;->d:Lpxa;

    iget-object v0, v7, Lpvv;->d:Lpxa;

    const/16 v8, 0x18

    invoke-virtual {v6, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    iput-object v8, v0, Lpxa;->a:Ljava/lang/Float;

    iget-object v0, v7, Lpvv;->d:Lpxa;

    const/16 v8, 0x19

    invoke-virtual {v6, v8}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x477fff00    # 65535.0f

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    iput-object v8, v0, Lpxa;->b:Ljava/lang/Float;

    const/16 v0, 0x11

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpvv;->b:Ljava/lang/Float;

    invoke-virtual {v6, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpvv;->c:Ljava/lang/Float;

    invoke-virtual {v6, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpvv;->e:Ljava/lang/Float;

    invoke-virtual {v6, v11}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpvv;->i:Ljava/lang/Float;

    const/16 v0, 0xc9

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpvv;->f:Ljava/lang/Float;

    const/16 v0, 0xca

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpvv;->g:Ljava/lang/Float;

    const/16 v0, 0x12

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    const v8, 0x358637bd    # 1.0E-6f

    mul-float/2addr v0, v8

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpvv;->h:Ljava/lang/Float;

    const/16 v0, 0x13

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpvv;->j:Ljava/lang/Float;

    invoke-virtual {v6, v12}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpvv;->k:Ljava/lang/Integer;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpvv;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_f
    new-instance v7, Lpwd;

    invoke-direct {v7}, Lpwd;-><init>()V

    const/16 v0, 0x1a

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwd;->b:Ljava/lang/Float;

    const/16 v0, 0x1b

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwd;->c:Ljava/lang/Float;

    const/16 v0, 0x1c

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwd;->d:Ljava/lang/Float;

    const/16 v0, 0x1d

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwd;->e:Ljava/lang/Float;

    const/16 v0, 0x1e

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwd;->f:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpwd;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_10
    new-instance v7, Lpwp;

    invoke-direct {v7}, Lpwp;-><init>()V

    invoke-virtual {v6, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwp;->b:Ljava/lang/Float;

    invoke-virtual {v6, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwp;->c:Ljava/lang/Float;

    invoke-virtual {v6, v11}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwp;->d:Ljava/lang/Float;

    const/16 v0, 0x14

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwp;->e:Ljava/lang/Float;

    const/16 v0, 0xb

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpwp;->f:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpwp;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_11
    new-instance v7, Lpxg;

    invoke-direct {v7}, Lpxg;-><init>()V

    const/16 v0, 0x320

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpxg;->b:Ljava/lang/Integer;

    const/16 v0, 0x9

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpxg;->c:Ljava/lang/Float;

    const/4 v0, 0x6

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpxg;->d:Ljava/lang/Float;

    const/16 v0, 0x13

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/4 v8, 0x0

    cmpl-float v0, v0, v8

    if-nez v0, :cond_b

    move v0, v2

    :goto_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Lpxg;->e:Ljava/lang/Integer;

    invoke-virtual {v6, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpxg;->f:Ljava/lang/Float;

    invoke-virtual {v6, v11}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v7, Lpxg;->g:Ljava/lang/Float;

    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    sget-object v6, Lpxg;->a:Loxr;

    invoke-virtual {v0, v6, v7}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_b
    move v0, v1

    goto :goto_7

    :sswitch_12
    invoke-static {v6}, Lhce;->b(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)Lpme;

    move-result-object v0

    goto/16 :goto_4

    .line 186
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->size()I

    move-result v0

    if-nez v0, :cond_d

    if-eqz v3, :cond_d

    .line 187
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_d
    iget-object v2, p1, Lnzi;->b:Lpla;

    .line 192
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lpme;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lpme;

    iput-object v0, v2, Lpla;->a:[Lpme;

    move v2, v1

    .line 194
    goto/16 :goto_0

    .line 177
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_12
        0x4 -> :sswitch_10
        0x7 -> :sswitch_1
        0x9 -> :sswitch_7
        0xb -> :sswitch_4
        0xd -> :sswitch_6
        0xe -> :sswitch_e
        0x10 -> :sswitch_d
        0x11 -> :sswitch_9
        0x12 -> :sswitch_0
        0x16 -> :sswitch_3
        0x35 -> :sswitch_f
        0x64 -> :sswitch_a
        0x6b -> :sswitch_2
        0x72 -> :sswitch_5
        0xc8 -> :sswitch_8
        0xca -> :sswitch_11
        0xcf -> :sswitch_c
        0xd0 -> :sswitch_b
    .end sparse-switch
.end method

.method public static b([B)Lpla;
    .locals 3

    .prologue
    .line 1679
    :try_start_0
    new-instance v0, Lpla;

    invoke-direct {v0}, Lpla;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lpla;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 1682
    :goto_0
    return-object v0

    .line 1680
    :catch_0
    move-exception v0

    .line 1681
    const-string v1, "ProtoPacker"

    const-string v2, "Failed to deserialize EditList."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1682
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)Lpme;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/high16 v7, 0x42c80000    # 100.0f

    .line 1631
    new-instance v3, Lptp;

    invoke-direct {v3}, Lptp;-><init>()V

    .line 1632
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getSubParameters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 1633
    new-array v0, v4, [Lptr;

    iput-object v0, v3, Lptp;->b:[Lptr;

    move v1, v2

    .line 1635
    :goto_0
    if-ge v1, v4, :cond_0

    .line 1636
    iget-object v0, v3, Lptp;->b:[Lptr;

    new-instance v5, Lptr;

    invoke-direct {v5}, Lptr;-><init>()V

    aput-object v5, v0, v1

    .line 1637
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getSubParameters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 1638
    iget-object v5, v3, Lptp;->b:[Lptr;

    aget-object v5, v5, v1

    .line 1639
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    iput-object v6, v5, Lptr;->b:Ljava/lang/Float;

    .line 1640
    iget-object v5, v3, Lptp;->b:[Lptr;

    aget-object v5, v5, v1

    const/4 v6, 0x1

    .line 1641
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    iput-object v6, v5, Lptr;->c:Ljava/lang/Float;

    .line 1642
    iget-object v5, v3, Lptp;->b:[Lptr;

    aget-object v5, v5, v1

    const/4 v6, 0x2

    .line 1643
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    iput-object v6, v5, Lptr;->d:Ljava/lang/Float;

    .line 1644
    iget-object v5, v3, Lptp;->b:[Lptr;

    aget-object v5, v5, v1

    new-instance v6, Lpxa;

    invoke-direct {v6}, Lpxa;-><init>()V

    iput-object v6, v5, Lptr;->e:Lpxa;

    .line 1645
    iget-object v5, v3, Lptp;->b:[Lptr;

    aget-object v5, v5, v1

    iget-object v5, v5, Lptr;->e:Lpxa;

    const/16 v6, 0x1f5

    .line 1646
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    iput-object v6, v5, Lpxa;->a:Ljava/lang/Float;

    .line 1647
    iget-object v5, v3, Lptp;->b:[Lptr;

    aget-object v5, v5, v1

    iget-object v5, v5, Lptr;->e:Lpxa;

    const/16 v6, 0x1f6

    .line 1648
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    iput-object v6, v5, Lpxa;->b:Ljava/lang/Float;

    .line 1651
    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    .line 1652
    iget-object v5, v3, Lptp;->b:[Lptr;

    aget-object v5, v5, v1

    int-to-float v0, v0

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v5, Lptr;->f:Ljava/lang/Float;

    .line 1635
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 1655
    :cond_0
    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    .line 1656
    sget-object v1, Lptp;->a:Loxr;

    invoke-virtual {v0, v1, v3}, Lpme;->a(Loxr;Ljava/lang/Object;)V

    .line 1659
    return-object v0
.end method
