.class public final Lizl;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private a:I

.field private b:Lhei;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/google/android/libraries/social/avatars/ui/AvatarView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-virtual {p0}, Lizl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040017

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f100130

    invoke-virtual {p0, v0}, Lizl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lizl;->c:Landroid/view/View;

    const v0, 0x7f100131

    invoke-virtual {p0, v0}, Lizl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lizl;->d:Landroid/view/View;

    const v0, 0x7f100133

    invoke-virtual {p0, v0}, Lizl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lizl;->e:Landroid/widget/TextView;

    const v0, 0x7f100134

    invoke-virtual {p0, v0}, Lizl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lizl;->f:Landroid/widget/TextView;

    const v0, 0x7f100132

    invoke-virtual {p0, v0}, Lizl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lizl;->g:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lizl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lizl;->b:Lhei;

    .line 32
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 63
    iput p1, p0, Lizl;->a:I

    .line 64
    iget-object v0, p0, Lizl;->b:Lhei;

    iget v1, p0, Lizl;->a:I

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lizl;->b:Lhei;

    iget v1, p0, Lizl;->a:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    iget-object v1, p0, Lizl;->c:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lizl;->d:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const-string v1, "display_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "profile_photo_url"

    invoke-interface {v0, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, p0, Lizl;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lizl;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v1, p0, Lizl;->g:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v1, v3, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :goto_1
    return-void

    .line 64
    :cond_0
    iget-object v4, p0, Lizl;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lizl;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lizl;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lizl;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
