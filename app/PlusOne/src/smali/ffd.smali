.class public final Lffd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lleu;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lffd;->a:Landroid/content/Context;

    .line 36
    return-void
.end method


# virtual methods
.method public a()Llev;
    .locals 4

    .prologue
    .line 39
    new-instance v0, Ller;

    invoke-direct {v0}, Ller;-><init>()V

    const-string v1, "analytics log"

    .line 40
    invoke-virtual {v0, v1}, Ller;->a(Ljava/lang/String;)Ller;

    move-result-object v0

    const/16 v1, 0x13

    .line 41
    invoke-virtual {v0, v1}, Ller;->a(I)Ller;

    move-result-object v0

    const/16 v1, 0x14

    .line 42
    invoke-virtual {v0, v1}, Ller;->b(I)Ller;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x4

    .line 43
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ller;->a(J)Ller;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Ller;->a()Ller;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Ller;->b()Llev;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkfp;ILles;)V
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Lffd;->a:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 51
    invoke-interface {v0, p2}, Lhms;->a(I)Lkff;

    move-result-object v0

    .line 52
    if-eqz p3, :cond_0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p3, v0}, Lles;->a(Lkff;)V

    .line 55
    :cond_0
    invoke-virtual {p1}, Lkfp;->f()V

    .line 57
    iget-object v0, p0, Lffd;->a:Landroid/content/Context;

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 57
    invoke-static {v0, p2, v2, v3}, Lhmt;->a(Landroid/content/Context;IJ)V

    .line 59
    return-void
.end method
