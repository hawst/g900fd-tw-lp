.class public final Lmwo;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmwo;


# instance fields
.field private b:I

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9024
    const/4 v0, 0x0

    new-array v0, v0, [Lmwo;

    sput-object v0, Lmwo;->a:[Lmwo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9025
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9037
    const/high16 v0, -0x80000000

    iput v0, p0, Lmwo;->b:I

    .line 9025
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 9061
    const/4 v0, 0x0

    .line 9062
    iget v1, p0, Lmwo;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 9063
    const/4 v0, 0x1

    iget v1, p0, Lmwo;->b:I

    .line 9064
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9066
    :cond_0
    iget-object v1, p0, Lmwo;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 9067
    const/4 v1, 0x2

    iget-object v2, p0, Lmwo;->c:Ljava/lang/Long;

    .line 9068
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9070
    :cond_1
    iget-object v1, p0, Lmwo;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 9071
    const/4 v1, 0x3

    iget-object v2, p0, Lmwo;->d:Ljava/lang/Long;

    .line 9072
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9074
    :cond_2
    iget-object v1, p0, Lmwo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9075
    iput v0, p0, Lmwo;->ai:I

    .line 9076
    return v0
.end method

.method public a(Loxn;)Lmwo;
    .locals 2

    .prologue
    .line 9084
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9085
    sparse-switch v0, :sswitch_data_0

    .line 9089
    iget-object v1, p0, Lmwo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9090
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwo;->ah:Ljava/util/List;

    .line 9093
    :cond_1
    iget-object v1, p0, Lmwo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9095
    :sswitch_0
    return-object p0

    .line 9100
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 9101
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 9107
    :cond_2
    iput v0, p0, Lmwo;->b:I

    goto :goto_0

    .line 9109
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmwo;->b:I

    goto :goto_0

    .line 9114
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwo;->c:Ljava/lang/Long;

    goto :goto_0

    .line 9118
    :sswitch_3
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwo;->d:Ljava/lang/Long;

    goto :goto_0

    .line 9085
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 9046
    iget v0, p0, Lmwo;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 9047
    const/4 v0, 0x1

    iget v1, p0, Lmwo;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9049
    :cond_0
    iget-object v0, p0, Lmwo;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 9050
    const/4 v0, 0x2

    iget-object v1, p0, Lmwo;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 9052
    :cond_1
    iget-object v0, p0, Lmwo;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 9053
    const/4 v0, 0x3

    iget-object v1, p0, Lmwo;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 9055
    :cond_2
    iget-object v0, p0, Lmwo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9057
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9021
    invoke-virtual {p0, p1}, Lmwo;->a(Loxn;)Lmwo;

    move-result-object v0

    return-object v0
.end method
