.class public final Liva;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Livc;


# instance fields
.field private final a:[Livc;


# direct methods
.method varargs constructor <init>([Livc;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Liva;->a:[Livc;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 48
    iget-object v1, p0, Liva;->a:[Livc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 49
    invoke-interface {v3, p1}, Livc;->a(Ljava/lang/String;)V

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    return-void
.end method

.method public close()V
    .locals 6

    .prologue
    .line 55
    const/4 v1, 0x0

    .line 56
    iget-object v2, p0, Liva;->a:[Livc;

    array-length v3, v2

    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 58
    :try_start_0
    invoke-interface {v4}, Livc;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    :cond_0
    if-eqz v0, :cond_1

    .line 64
    throw v0

    .line 59
    :catch_0
    move-exception v0

    goto :goto_1

    .line 66
    :cond_1
    return-void
.end method
