.class public final Lmcz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnto;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15550
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15553
    iput-object v0, p0, Lmcz;->apiHeader:Llyr;

    .line 15556
    iput-object v0, p0, Lmcz;->a:Lnto;

    .line 15550
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 15573
    const/4 v0, 0x0

    .line 15574
    iget-object v1, p0, Lmcz;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 15575
    const/4 v0, 0x1

    iget-object v1, p0, Lmcz;->apiHeader:Llyr;

    .line 15576
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15578
    :cond_0
    iget-object v1, p0, Lmcz;->a:Lnto;

    if-eqz v1, :cond_1

    .line 15579
    const/4 v1, 0x2

    iget-object v2, p0, Lmcz;->a:Lnto;

    .line 15580
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15582
    :cond_1
    iget-object v1, p0, Lmcz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15583
    iput v0, p0, Lmcz;->ai:I

    .line 15584
    return v0
.end method

.method public a(Loxn;)Lmcz;
    .locals 2

    .prologue
    .line 15592
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 15593
    sparse-switch v0, :sswitch_data_0

    .line 15597
    iget-object v1, p0, Lmcz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 15598
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcz;->ah:Ljava/util/List;

    .line 15601
    :cond_1
    iget-object v1, p0, Lmcz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15603
    :sswitch_0
    return-object p0

    .line 15608
    :sswitch_1
    iget-object v0, p0, Lmcz;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 15609
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmcz;->apiHeader:Llyr;

    .line 15611
    :cond_2
    iget-object v0, p0, Lmcz;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15615
    :sswitch_2
    iget-object v0, p0, Lmcz;->a:Lnto;

    if-nez v0, :cond_3

    .line 15616
    new-instance v0, Lnto;

    invoke-direct {v0}, Lnto;-><init>()V

    iput-object v0, p0, Lmcz;->a:Lnto;

    .line 15618
    :cond_3
    iget-object v0, p0, Lmcz;->a:Lnto;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15593
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 15561
    iget-object v0, p0, Lmcz;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 15562
    const/4 v0, 0x1

    iget-object v1, p0, Lmcz;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15564
    :cond_0
    iget-object v0, p0, Lmcz;->a:Lnto;

    if-eqz v0, :cond_1

    .line 15565
    const/4 v0, 0x2

    iget-object v1, p0, Lmcz;->a:Lnto;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15567
    :cond_1
    iget-object v0, p0, Lmcz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 15569
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 15546
    invoke-virtual {p0, p1}, Lmcz;->a(Loxn;)Lmcz;

    move-result-object v0

    return-object v0
.end method
