.class public final Liut;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdd;


# static fields
.field static a:I

.field public static b:I

.field public static c:I

.field public static d:Landroid/graphics/PointF;

.field private static e:Z

.field private static f:Landroid/graphics/Bitmap;

.field private static g:Landroid/graphics/Bitmap;

.field private static h:I

.field private static i:I

.field private static j:I

.field private static k:Landroid/graphics/Bitmap;

.field private static l:Landroid/graphics/drawable/NinePatchDrawable;

.field private static m:Landroid/graphics/drawable/NinePatchDrawable;

.field private static n:Landroid/graphics/Rect;

.field private static o:Landroid/graphics/Bitmap;

.field private static p:Landroid/graphics/Bitmap;

.field private static q:Landroid/graphics/Paint;

.field private static r:Landroid/graphics/Paint;

.field private static s:Landroid/text/TextPaint;

.field private static t:Landroid/text/TextPaint;


# instance fields
.field private A:Ljava/lang/String;

.field private u:Landroid/content/Context;

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkda;",
            ">;"
        }
    .end annotation
.end field

.field private w:Liuu;

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;ZZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-static {p1}, Liut;->b(Landroid/content/Context;)V

    .line 91
    iput-object p1, p0, Liut;->u:Landroid/content/Context;

    .line 92
    const-class v0, Lhso;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    .line 94
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    .line 95
    const/4 v1, 0x4

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 96
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Liut;->v:Ljava/util/List;

    .line 97
    iput-boolean p3, p0, Liut;->y:Z

    .line 98
    iput-boolean p4, p0, Liut;->z:Z

    .line 100
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    .line 101
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnhm;

    .line 102
    iget-object v5, v1, Lnhm;->e:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 104
    iget-object v1, p0, Liut;->v:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 106
    :cond_0
    iget-object v1, v1, Lnhm;->e:Ljava/lang/String;

    invoke-static {v1}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    const/4 v5, 0x2

    invoke-interface {v0, v1, v6, v5, p0}, Lhso;->a(Ljava/lang/String;IILkdd;)Lkda;

    move-result-object v1

    .line 110
    iget-object v5, p0, Liut;->v:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 114
    :cond_1
    if-le v3, v6, :cond_2

    .line 115
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liut;->A:Ljava/lang/String;

    .line 117
    :cond_2
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 134
    invoke-static {p0}, Liut;->b(Landroid/content/Context;)V

    .line 135
    sget-object v0, Liut;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    return v0
.end method

.method private static a(F)Landroid/graphics/ColorMatrixColorFilter;
    .locals 2

    .prologue
    .line 285
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 286
    invoke-virtual {v0, p0}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 287
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    return-object v1
.end method

.method public static b(Landroid/content/Context;)V
    .locals 6

    .prologue
    const v5, 0x7f0203f9

    const/16 v4, 0x80

    const/4 v3, 0x1

    .line 291
    sget-boolean v0, Liut;->e:Z

    if-eqz v0, :cond_0

    .line 354
    :goto_0
    return-void

    .line 294
    :cond_0
    sput-boolean v3, Liut;->e:Z

    .line 296
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 298
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 299
    sput-object v0, Liut;->q:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 302
    sget-object v0, Liut;->q:Landroid/graphics/Paint;

    const v2, 0x3f7d70a4    # 0.99f

    invoke-static {v2}, Liut;->a(F)Landroid/graphics/ColorMatrixColorFilter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 304
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 305
    sput-object v0, Liut;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 306
    sget-object v0, Liut;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 307
    sget-object v0, Liut;->r:Landroid/graphics/Paint;

    const v2, 0x3e4ccccd    # 0.2f

    invoke-static {v2}, Liut;->a(F)Landroid/graphics/ColorMatrixColorFilter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 309
    const/16 v0, 0x1f

    invoke-static {p0, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    sput-object v0, Liut;->s:Landroid/text/TextPaint;

    .line 310
    new-instance v0, Landroid/text/TextPaint;

    sget-object v2, Liut;->s:Landroid/text/TextPaint;

    invoke-direct {v0, v2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 311
    sput-object v0, Liut;->t:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 313
    invoke-static {p0}, Lhss;->c(Landroid/content/Context;)I

    move-result v0

    sput v0, Liut;->a:I

    .line 315
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lhss;->c(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Liut;->k:Landroid/graphics/Bitmap;

    .line 318
    const v0, 0x7f0203fa

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 319
    sput-object v0, Liut;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 320
    sget v2, Liut;->a:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    sput v0, Liut;->h:I

    .line 322
    const v0, 0x7f020466

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Liut;->g:Landroid/graphics/Bitmap;

    .line 324
    const v0, 0x7f0d0179

    .line 325
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Liut;->i:I

    .line 326
    const v0, 0x7f0d017a

    .line 327
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Liut;->j:I

    .line 329
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Liut;->l:Landroid/graphics/drawable/NinePatchDrawable;

    .line 330
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    .line 331
    sput-object v0, Liut;->m:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/NinePatchDrawable;->setAlpha(I)V

    .line 332
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Liut;->n:Landroid/graphics/Rect;

    .line 333
    sget-object v0, Liut;->l:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v2, Liut;->n:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/NinePatchDrawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 335
    const v0, 0x7f020469

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Liut;->o:Landroid/graphics/Bitmap;

    .line 336
    const v0, 0x7f020468

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Liut;->p:Landroid/graphics/Bitmap;

    .line 338
    const v0, 0x7f0d017b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 340
    const v2, 0x7f0d017c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 343
    sget-object v2, Liut;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget v3, Liut;->i:I

    add-int/2addr v2, v3

    .line 344
    sub-int v0, v2, v0

    .line 346
    sget-object v3, Liut;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 348
    sget-object v3, Liut;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sget v4, Liut;->j:I

    add-int/2addr v3, v4

    sput v3, Liut;->b:I

    .line 349
    sget-object v3, Liut;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int/2addr v1, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    sput v1, Liut;->c:I

    .line 351
    sget-object v1, Liut;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 352
    int-to-float v0, v0

    .line 353
    new-instance v2, Landroid/graphics/PointF;

    sget v3, Liut;->b:I

    int-to-float v3, v3

    div-float/2addr v1, v3

    sget v3, Liut;->c:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-direct {v2, v1, v0}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v2, Liut;->d:Landroid/graphics/PointF;

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 151
    invoke-virtual {p0}, Liut;->c()V

    .line 152
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 163
    iput p1, p0, Liut;->x:I

    .line 164
    return-void
.end method

.method public a(Liuu;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Liut;->w:Liuu;

    .line 156
    return-void
.end method

.method public a(Lkda;)V
    .locals 2

    .prologue
    .line 201
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 202
    iget-object v0, p0, Liut;->w:Liuu;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Liut;->w:Liuu;

    invoke-interface {v0, p0}, Liuu;->b(Liut;)V

    .line 206
    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Liut;->v:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 190
    iget-object v0, p0, Liut;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkda;

    .line 191
    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    goto :goto_0

    .line 195
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Liut;->v:Ljava/util/List;

    .line 197
    :cond_2
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Liut;->x:I

    return v0
.end method

.method public e()Landroid/graphics/Bitmap;
    .locals 14

    .prologue
    .line 213
    iget v0, p0, Liut;->x:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 214
    sget-object v2, Liut;->r:Landroid/graphics/Paint;

    .line 215
    sget-object v1, Liut;->t:Landroid/text/TextPaint;

    .line 216
    sget-object v0, Liut;->m:Landroid/graphics/drawable/NinePatchDrawable;

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 224
    :goto_0
    sget v0, Liut;->b:I

    sget v4, Liut;->c:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 225
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 228
    iget-boolean v0, p0, Liut;->y:Z

    if-nez v0, :cond_1

    iget v0, p0, Liut;->x:I

    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    iget-boolean v0, p0, Liut;->z:Z

    if-eqz v0, :cond_1

    .line 229
    :cond_0
    sget-object v0, Liut;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 230
    sget-object v4, Liut;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 231
    sget-object v7, Liut;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int v0, v7, v0

    div-int/lit8 v7, v0, 0x2

    .line 232
    sget v0, Liut;->c:I

    sub-int v4, v0, v4

    .line 233
    iget v0, p0, Liut;->x:I

    const/4 v8, 0x2

    if-ne v0, v8, :cond_4

    sget-object v0, Liut;->o:Landroid/graphics/Bitmap;

    .line 234
    :goto_1
    int-to-float v7, v7

    int-to-float v4, v4

    invoke-virtual {v6, v0, v7, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 238
    :cond_1
    iget-boolean v0, p0, Liut;->y:Z

    if-eqz v0, :cond_5

    sget-object v0, Liut;->g:Landroid/graphics/Bitmap;

    .line 239
    :goto_2
    const/4 v4, 0x0

    sget v7, Liut;->i:I

    int-to-float v7, v7

    invoke-virtual {v6, v0, v4, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 242
    iget-object v0, p0, Liut;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 243
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 244
    const/4 v0, 0x0

    move v4, v0

    :goto_3
    if-ge v4, v7, :cond_9

    .line 245
    iget-object v0, p0, Liut;->v:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Liut;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v4, v0, :cond_6

    :cond_2
    sget-object v0, Liut;->k:Landroid/graphics/Bitmap;

    :goto_4
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    .line 218
    :cond_3
    sget-object v2, Liut;->q:Landroid/graphics/Paint;

    .line 219
    sget-object v1, Liut;->s:Landroid/text/TextPaint;

    .line 220
    sget-object v0, Liut;->l:Landroid/graphics/drawable/NinePatchDrawable;

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 233
    :cond_4
    sget-object v0, Liut;->p:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 238
    :cond_5
    sget-object v0, Liut;->f:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 245
    :cond_6
    iget-object v0, p0, Liut;->v:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkda;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    :goto_5
    instance-of v9, v0, Landroid/graphics/Bitmap;

    if-eqz v9, :cond_8

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    goto :goto_5

    :cond_8
    sget-object v0, Liut;->k:Landroid/graphics/Bitmap;

    goto :goto_4

    .line 248
    :cond_9
    sget v4, Liut;->h:I

    .line 249
    sget v0, Liut;->h:I

    sget v7, Liut;->i:I

    add-int/2addr v7, v0

    .line 250
    int-to-float v0, v4

    int-to-float v9, v7

    invoke-virtual {v6, v0, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 251
    iget-boolean v0, p0, Liut;->y:Z

    if-eqz v0, :cond_b

    .line 252
    iget-object v9, p0, Liut;->u:Landroid/content/Context;

    sget-object v10, Liut;->q:Landroid/graphics/Paint;

    .line 253
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v0, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v12, Landroid/graphics/Canvas;

    invoke-direct {v12, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v13, 0x0

    invoke-static {v12, v8, v10, v13}, Lhss;->a(Landroid/graphics/Canvas;Ljava/util/List;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    invoke-static {v9, v12, v11}, Llho;->a(Landroid/content/Context;Landroid/graphics/Canvas;I)V

    .line 254
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v6, v0, v8, v9, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 259
    :goto_6
    neg-int v0, v4

    int-to-float v0, v0

    neg-int v3, v7

    int-to-float v3, v3

    invoke-virtual {v6, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 262
    iget-object v0, p0, Liut;->A:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 263
    iget-object v0, p0, Liut;->A:Ljava/lang/String;

    invoke-static {v2, v0}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    .line 264
    invoke-static {v2}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v3

    .line 266
    sget-object v4, Liut;->n:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    sget-object v4, Liut;->n:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    float-to-int v0, v0

    .line 268
    sget-object v4, Liut;->n:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v3

    sget-object v7, Liut;->n:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v7

    .line 271
    sget v7, Liut;->b:I

    sub-int/2addr v7, v0

    .line 272
    const/4 v8, 0x0

    add-int/2addr v0, v7

    invoke-virtual {v1, v7, v8, v0, v4}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 274
    invoke-virtual {v1, v6}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 276
    sget-object v0, Liut;->n:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v7

    .line 277
    const/4 v1, 0x0

    sub-int v3, v4, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 278
    iget-object v3, p0, Liut;->A:Ljava/lang/String;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v6, v3, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 281
    :cond_a
    return-object v5

    .line 256
    :cond_b
    const/4 v0, 0x0

    invoke-static {v6, v8, v3, v0}, Lhss;->a(Landroid/graphics/Canvas;Ljava/util/List;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_6
.end method
