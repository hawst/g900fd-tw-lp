.class public final Lfxw;
.super Lfye;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static a:I

.field private static b:Z

.field private static c:I

.field private static d:F

.field private static e:I

.field private static f:Ljava/lang/String;


# instance fields
.field private g:Lfxf;

.field private h:Lfxx;

.field private i:Landroid/widget/TextView;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 51
    invoke-direct {p0, p1, p2, p3}, Lfye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    sget-boolean v0, Lfxw;->b:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxw;->a:I

    const v1, 0x7f0b00d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lfxw;->c:I

    const v1, 0x7f0a0742

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lfxw;->f:Ljava/lang/String;

    const v1, 0x7f0d024c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lfxw;->d:F

    const v1, 0x7f0d02c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfxw;->e:I

    sput-boolean v2, Lfxw;->b:Z

    :cond_0
    new-instance v0, Lfxx;

    invoke-direct {v0, p1, p2, p3}, Lfxx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxw;->h:Lfxx;

    iget-object v0, p0, Lfxw;->h:Lfxx;

    invoke-virtual {p0, v0}, Lfxw;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfxw;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    sget-object v1, Lfxw;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    sget v1, Lfxw;->d:F

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    sget v1, Lfxw;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lfxw;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020416

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    sget v0, Lfxw;->a:I

    invoke-virtual {p0, v3, v3, v3, v0}, Lfxw;->a(IIII)V

    .line 53
    return-void
.end method


# virtual methods
.method public a(Lfxx;III)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 103
    .line 104
    invoke-virtual {p1}, Lfxx;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 105
    sget v0, Lfxw;->a:I

    add-int/lit8 v0, v0, 0x0

    .line 106
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p1, p2, v2, v1, v1}, Lfxw;->a(Landroid/view/View;IIII)V

    .line 107
    invoke-static {p1, p3, p4}, Lfxw;->a(Landroid/view/View;II)V

    .line 108
    invoke-virtual {p1}, Lfxx;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 109
    invoke-virtual {p1, v1}, Lfxx;->setVisibility(I)V

    .line 114
    :goto_0
    return v0

    .line 111
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lfxx;->setVisibility(I)V

    move v0, v1

    goto :goto_0
.end method

.method public a(Lidh;Leaq;Lfxf;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 120
    invoke-virtual {p1}, Lidh;->n()J

    .line 122
    invoke-virtual {p1}, Lidh;->i()Lpbj;

    move-result-object v2

    .line 123
    iget-object v0, v2, Lpbj;->a:Lpbe;

    .line 124
    if-eqz v0, :cond_1

    iget-object v3, v0, Lpbe;->c:Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lpbe;->c:Ljava/lang/Boolean;

    .line 125
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p2, Leaq;->k:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfxw;->j:Z

    .line 127
    iget-boolean v0, p0, Lfxw;->j:Z

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 128
    :cond_0
    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lfxw;->h:Lfxx;

    invoke-virtual {v0, v1}, Lfxx;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lfxw;->h:Lfxx;

    iget-object v1, v2, Lpbj;->c:[Lpay;

    invoke-virtual {v0, v1, p3}, Lfxx;->a([Lpay;Lfxf;)V

    .line 132
    iput-object p3, p0, Lfxw;->g:Lfxf;

    .line 134
    invoke-virtual {p0}, Lfxw;->invalidate()V

    .line 135
    return-void

    :cond_1
    move v0, v1

    .line 125
    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lfxw;->h:Lfxx;

    invoke-virtual {v0}, Lfxx;->c()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lfxw;->g:Lfxf;

    .line 140
    return-void
.end method

.method protected measureChildren(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 89
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 90
    iget-boolean v1, p0, Lfxw;->j:Z

    if-nez v1, :cond_0

    .line 94
    iget-object v1, p0, Lfxw;->h:Lfxx;

    invoke-virtual {p0, v1, v0, v4, v4}, Lfxw;->a(Lfxx;III)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 96
    iget-object v2, p0, Lfxw;->i:Landroid/widget/TextView;

    sget v3, Lfxw;->e:I

    invoke-static {v2, v0, v5, v3, v5}, Lfxw;->a(Landroid/view/View;IIII)V

    .line 98
    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    invoke-static {v0, v4, v1}, Lfxw;->a(Landroid/view/View;II)V

    .line 100
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lfxw;->i:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lfxw;->g:Lfxf;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lfxw;->g:Lfxf;

    invoke-interface {v0}, Lfxf;->X()V

    .line 147
    :cond_0
    return-void
.end method
