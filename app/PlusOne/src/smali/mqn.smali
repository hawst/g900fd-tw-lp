.class public final Lmqn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmqn;


# instance fields
.field public b:[Lnym;

.field private c:[Lnzx;

.field private d:I

.field private e:Lpaf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 944
    const/4 v0, 0x0

    new-array v0, v0, [Lmqn;

    sput-object v0, Lmqn;->a:[Lmqn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 945
    invoke-direct {p0}, Loxq;-><init>()V

    .line 948
    sget-object v0, Lnym;->a:[Lnym;

    iput-object v0, p0, Lmqn;->b:[Lnym;

    .line 951
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lmqn;->c:[Lnzx;

    .line 954
    const/high16 v0, -0x80000000

    iput v0, p0, Lmqn;->d:I

    .line 957
    const/4 v0, 0x0

    iput-object v0, p0, Lmqn;->e:Lpaf;

    .line 945
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 988
    .line 989
    iget-object v0, p0, Lmqn;->b:[Lnym;

    if-eqz v0, :cond_1

    .line 990
    iget-object v3, p0, Lmqn;->b:[Lnym;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 991
    if-eqz v5, :cond_0

    .line 992
    const/4 v6, 0x1

    .line 993
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 990
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 997
    :cond_2
    iget v2, p0, Lmqn;->d:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    .line 998
    const/4 v2, 0x2

    iget v3, p0, Lmqn;->d:I

    .line 999
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1001
    :cond_3
    iget-object v2, p0, Lmqn;->e:Lpaf;

    if-eqz v2, :cond_4

    .line 1002
    const/4 v2, 0x3

    iget-object v3, p0, Lmqn;->e:Lpaf;

    .line 1003
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1005
    :cond_4
    iget-object v2, p0, Lmqn;->c:[Lnzx;

    if-eqz v2, :cond_6

    .line 1006
    iget-object v2, p0, Lmqn;->c:[Lnzx;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 1007
    if-eqz v4, :cond_5

    .line 1008
    const/4 v5, 0x4

    .line 1009
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1006
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1013
    :cond_6
    iget-object v1, p0, Lmqn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1014
    iput v0, p0, Lmqn;->ai:I

    .line 1015
    return v0
.end method

.method public a(Loxn;)Lmqn;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1023
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1024
    sparse-switch v0, :sswitch_data_0

    .line 1028
    iget-object v2, p0, Lmqn;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1029
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmqn;->ah:Ljava/util/List;

    .line 1032
    :cond_1
    iget-object v2, p0, Lmqn;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1034
    :sswitch_0
    return-object p0

    .line 1039
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1040
    iget-object v0, p0, Lmqn;->b:[Lnym;

    if-nez v0, :cond_3

    move v0, v1

    .line 1041
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnym;

    .line 1042
    iget-object v3, p0, Lmqn;->b:[Lnym;

    if-eqz v3, :cond_2

    .line 1043
    iget-object v3, p0, Lmqn;->b:[Lnym;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1045
    :cond_2
    iput-object v2, p0, Lmqn;->b:[Lnym;

    .line 1046
    :goto_2
    iget-object v2, p0, Lmqn;->b:[Lnym;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1047
    iget-object v2, p0, Lmqn;->b:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 1048
    iget-object v2, p0, Lmqn;->b:[Lnym;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1049
    invoke-virtual {p1}, Loxn;->a()I

    .line 1046
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1040
    :cond_3
    iget-object v0, p0, Lmqn;->b:[Lnym;

    array-length v0, v0

    goto :goto_1

    .line 1052
    :cond_4
    iget-object v2, p0, Lmqn;->b:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 1053
    iget-object v2, p0, Lmqn;->b:[Lnym;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1057
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1058
    if-eq v0, v4, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_5

    const/4 v2, 0x6

    if-eq v0, v2, :cond_5

    const/4 v2, 0x7

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8

    if-ne v0, v2, :cond_6

    .line 1066
    :cond_5
    iput v0, p0, Lmqn;->d:I

    goto/16 :goto_0

    .line 1068
    :cond_6
    iput v4, p0, Lmqn;->d:I

    goto/16 :goto_0

    .line 1073
    :sswitch_3
    iget-object v0, p0, Lmqn;->e:Lpaf;

    if-nez v0, :cond_7

    .line 1074
    new-instance v0, Lpaf;

    invoke-direct {v0}, Lpaf;-><init>()V

    iput-object v0, p0, Lmqn;->e:Lpaf;

    .line 1076
    :cond_7
    iget-object v0, p0, Lmqn;->e:Lpaf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1080
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1081
    iget-object v0, p0, Lmqn;->c:[Lnzx;

    if-nez v0, :cond_9

    move v0, v1

    .line 1082
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 1083
    iget-object v3, p0, Lmqn;->c:[Lnzx;

    if-eqz v3, :cond_8

    .line 1084
    iget-object v3, p0, Lmqn;->c:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1086
    :cond_8
    iput-object v2, p0, Lmqn;->c:[Lnzx;

    .line 1087
    :goto_4
    iget-object v2, p0, Lmqn;->c:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 1088
    iget-object v2, p0, Lmqn;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 1089
    iget-object v2, p0, Lmqn;->c:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1090
    invoke-virtual {p1}, Loxn;->a()I

    .line 1087
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1081
    :cond_9
    iget-object v0, p0, Lmqn;->c:[Lnzx;

    array-length v0, v0

    goto :goto_3

    .line 1093
    :cond_a
    iget-object v2, p0, Lmqn;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 1094
    iget-object v2, p0, Lmqn;->c:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1024
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 962
    iget-object v1, p0, Lmqn;->b:[Lnym;

    if-eqz v1, :cond_1

    .line 963
    iget-object v2, p0, Lmqn;->b:[Lnym;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 964
    if-eqz v4, :cond_0

    .line 965
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 963
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 969
    :cond_1
    iget v1, p0, Lmqn;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 970
    const/4 v1, 0x2

    iget v2, p0, Lmqn;->d:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 972
    :cond_2
    iget-object v1, p0, Lmqn;->e:Lpaf;

    if-eqz v1, :cond_3

    .line 973
    const/4 v1, 0x3

    iget-object v2, p0, Lmqn;->e:Lpaf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 975
    :cond_3
    iget-object v1, p0, Lmqn;->c:[Lnzx;

    if-eqz v1, :cond_5

    .line 976
    iget-object v1, p0, Lmqn;->c:[Lnzx;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 977
    if-eqz v3, :cond_4

    .line 978
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 976
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 982
    :cond_5
    iget-object v0, p0, Lmqn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 984
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 941
    invoke-virtual {p0, p1}, Lmqn;->a(Loxn;)Lmqn;

    move-result-object v0

    return-object v0
.end method
