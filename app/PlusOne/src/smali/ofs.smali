.class public final Lofs;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lofs;


# instance fields
.field private A:Lpcp;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/Integer;

.field private n:Ljava/lang/Integer;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/Integer;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/Boolean;

.field private t:Ljava/lang/Double;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1184
    const/4 v0, 0x0

    new-array v0, v0, [Lofs;

    sput-object v0, Lofs;->a:[Lofs;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1185
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1238
    const/4 v0, 0x0

    iput-object v0, p0, Lofs;->A:Lpcp;

    .line 1185
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1327
    const/4 v0, 0x0

    .line 1328
    iget-object v1, p0, Lofs;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1329
    const/4 v0, 0x1

    iget-object v1, p0, Lofs;->f:Ljava/lang/String;

    .line 1330
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1332
    :cond_0
    iget-object v1, p0, Lofs;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1333
    const/4 v1, 0x2

    iget-object v2, p0, Lofs;->g:Ljava/lang/String;

    .line 1334
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1336
    :cond_1
    iget-object v1, p0, Lofs;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1337
    const/4 v1, 0x3

    iget-object v2, p0, Lofs;->b:Ljava/lang/String;

    .line 1338
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1340
    :cond_2
    iget-object v1, p0, Lofs;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1341
    const/4 v1, 0x4

    iget-object v2, p0, Lofs;->h:Ljava/lang/Integer;

    .line 1342
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1344
    :cond_3
    iget-object v1, p0, Lofs;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1345
    const/4 v1, 0x5

    iget-object v2, p0, Lofs;->i:Ljava/lang/Integer;

    .line 1346
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1348
    :cond_4
    iget-object v1, p0, Lofs;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 1349
    const/4 v1, 0x6

    iget-object v2, p0, Lofs;->j:Ljava/lang/Integer;

    .line 1350
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1352
    :cond_5
    iget-object v1, p0, Lofs;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 1353
    const/4 v1, 0x7

    iget-object v2, p0, Lofs;->k:Ljava/lang/Integer;

    .line 1354
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1356
    :cond_6
    iget-object v1, p0, Lofs;->c:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1357
    const/16 v1, 0x8

    iget-object v2, p0, Lofs;->c:Ljava/lang/String;

    .line 1358
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1360
    :cond_7
    iget-object v1, p0, Lofs;->l:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 1361
    const/16 v1, 0x9

    iget-object v2, p0, Lofs;->l:Ljava/lang/String;

    .line 1362
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1364
    :cond_8
    iget-object v1, p0, Lofs;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 1365
    const/16 v1, 0xa

    iget-object v2, p0, Lofs;->m:Ljava/lang/Integer;

    .line 1366
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1368
    :cond_9
    iget-object v1, p0, Lofs;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 1369
    const/16 v1, 0xb

    iget-object v2, p0, Lofs;->n:Ljava/lang/Integer;

    .line 1370
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1372
    :cond_a
    iget-object v1, p0, Lofs;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 1373
    const/16 v1, 0xc

    iget-object v2, p0, Lofs;->o:Ljava/lang/Integer;

    .line 1374
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1376
    :cond_b
    iget-object v1, p0, Lofs;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 1377
    const/16 v1, 0xd

    iget-object v2, p0, Lofs;->p:Ljava/lang/Integer;

    .line 1378
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1380
    :cond_c
    iget-object v1, p0, Lofs;->q:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 1381
    const/16 v1, 0xe

    iget-object v2, p0, Lofs;->q:Ljava/lang/String;

    .line 1382
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1384
    :cond_d
    iget-object v1, p0, Lofs;->r:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 1385
    const/16 v1, 0xf

    iget-object v2, p0, Lofs;->r:Ljava/lang/String;

    .line 1386
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1388
    :cond_e
    iget-object v1, p0, Lofs;->s:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    .line 1389
    const/16 v1, 0x10

    iget-object v2, p0, Lofs;->s:Ljava/lang/Boolean;

    .line 1390
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1392
    :cond_f
    iget-object v1, p0, Lofs;->t:Ljava/lang/Double;

    if-eqz v1, :cond_10

    .line 1393
    const/16 v1, 0x11

    iget-object v2, p0, Lofs;->t:Ljava/lang/Double;

    .line 1394
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1396
    :cond_10
    iget-object v1, p0, Lofs;->d:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 1397
    const/16 v1, 0x12

    iget-object v2, p0, Lofs;->d:Ljava/lang/String;

    .line 1398
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1400
    :cond_11
    iget-object v1, p0, Lofs;->e:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 1401
    const/16 v1, 0x13

    iget-object v2, p0, Lofs;->e:Ljava/lang/String;

    .line 1402
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1404
    :cond_12
    iget-object v1, p0, Lofs;->u:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 1405
    const/16 v1, 0x14

    iget-object v2, p0, Lofs;->u:Ljava/lang/String;

    .line 1406
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1408
    :cond_13
    iget-object v1, p0, Lofs;->v:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 1409
    const/16 v1, 0x15

    iget-object v2, p0, Lofs;->v:Ljava/lang/String;

    .line 1410
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1412
    :cond_14
    iget-object v1, p0, Lofs;->w:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 1413
    const/16 v1, 0x16

    iget-object v2, p0, Lofs;->w:Ljava/lang/String;

    .line 1414
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1416
    :cond_15
    iget-object v1, p0, Lofs;->x:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 1417
    const/16 v1, 0x17

    iget-object v2, p0, Lofs;->x:Ljava/lang/String;

    .line 1418
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1420
    :cond_16
    iget-object v1, p0, Lofs;->y:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 1421
    const/16 v1, 0x18

    iget-object v2, p0, Lofs;->y:Ljava/lang/String;

    .line 1422
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1424
    :cond_17
    iget-object v1, p0, Lofs;->z:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 1425
    const/16 v1, 0x19

    iget-object v2, p0, Lofs;->z:Ljava/lang/String;

    .line 1426
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1428
    :cond_18
    iget-object v1, p0, Lofs;->A:Lpcp;

    if-eqz v1, :cond_19

    .line 1429
    const/16 v1, 0x1b

    iget-object v2, p0, Lofs;->A:Lpcp;

    .line 1430
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1432
    :cond_19
    iget-object v1, p0, Lofs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1433
    iput v0, p0, Lofs;->ai:I

    .line 1434
    return v0
.end method

.method public a(Loxn;)Lofs;
    .locals 2

    .prologue
    .line 1442
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1443
    sparse-switch v0, :sswitch_data_0

    .line 1447
    iget-object v1, p0, Lofs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1448
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofs;->ah:Ljava/util/List;

    .line 1451
    :cond_1
    iget-object v1, p0, Lofs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1453
    :sswitch_0
    return-object p0

    .line 1458
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->f:Ljava/lang/String;

    goto :goto_0

    .line 1462
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->g:Ljava/lang/String;

    goto :goto_0

    .line 1466
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->b:Ljava/lang/String;

    goto :goto_0

    .line 1470
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofs;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 1474
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofs;->i:Ljava/lang/Integer;

    goto :goto_0

    .line 1478
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofs;->j:Ljava/lang/Integer;

    goto :goto_0

    .line 1482
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofs;->k:Ljava/lang/Integer;

    goto :goto_0

    .line 1486
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->c:Ljava/lang/String;

    goto :goto_0

    .line 1490
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->l:Ljava/lang/String;

    goto :goto_0

    .line 1494
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofs;->m:Ljava/lang/Integer;

    goto :goto_0

    .line 1498
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofs;->n:Ljava/lang/Integer;

    goto :goto_0

    .line 1502
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofs;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1506
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lofs;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1510
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 1514
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 1518
    :sswitch_10
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofs;->s:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1522
    :sswitch_11
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lofs;->t:Ljava/lang/Double;

    goto/16 :goto_0

    .line 1526
    :sswitch_12
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 1530
    :sswitch_13
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 1534
    :sswitch_14
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->u:Ljava/lang/String;

    goto/16 :goto_0

    .line 1538
    :sswitch_15
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->v:Ljava/lang/String;

    goto/16 :goto_0

    .line 1542
    :sswitch_16
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 1546
    :sswitch_17
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 1550
    :sswitch_18
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 1554
    :sswitch_19
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofs;->z:Ljava/lang/String;

    goto/16 :goto_0

    .line 1558
    :sswitch_1a
    iget-object v0, p0, Lofs;->A:Lpcp;

    if-nez v0, :cond_2

    .line 1559
    new-instance v0, Lpcp;

    invoke-direct {v0}, Lpcp;-><init>()V

    iput-object v0, p0, Lofs;->A:Lpcp;

    .line 1561
    :cond_2
    iget-object v0, p0, Lofs;->A:Lpcp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1443
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
        0x89 -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xda -> :sswitch_1a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 1243
    iget-object v0, p0, Lofs;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1244
    const/4 v0, 0x1

    iget-object v1, p0, Lofs;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1246
    :cond_0
    iget-object v0, p0, Lofs;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1247
    const/4 v0, 0x2

    iget-object v1, p0, Lofs;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1249
    :cond_1
    iget-object v0, p0, Lofs;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1250
    const/4 v0, 0x3

    iget-object v1, p0, Lofs;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1252
    :cond_2
    iget-object v0, p0, Lofs;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1253
    const/4 v0, 0x4

    iget-object v1, p0, Lofs;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1255
    :cond_3
    iget-object v0, p0, Lofs;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1256
    const/4 v0, 0x5

    iget-object v1, p0, Lofs;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1258
    :cond_4
    iget-object v0, p0, Lofs;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1259
    const/4 v0, 0x6

    iget-object v1, p0, Lofs;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1261
    :cond_5
    iget-object v0, p0, Lofs;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1262
    const/4 v0, 0x7

    iget-object v1, p0, Lofs;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1264
    :cond_6
    iget-object v0, p0, Lofs;->c:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1265
    const/16 v0, 0x8

    iget-object v1, p0, Lofs;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1267
    :cond_7
    iget-object v0, p0, Lofs;->l:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1268
    const/16 v0, 0x9

    iget-object v1, p0, Lofs;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1270
    :cond_8
    iget-object v0, p0, Lofs;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 1271
    const/16 v0, 0xa

    iget-object v1, p0, Lofs;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1273
    :cond_9
    iget-object v0, p0, Lofs;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 1274
    const/16 v0, 0xb

    iget-object v1, p0, Lofs;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1276
    :cond_a
    iget-object v0, p0, Lofs;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 1277
    const/16 v0, 0xc

    iget-object v1, p0, Lofs;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1279
    :cond_b
    iget-object v0, p0, Lofs;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 1280
    const/16 v0, 0xd

    iget-object v1, p0, Lofs;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1282
    :cond_c
    iget-object v0, p0, Lofs;->q:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 1283
    const/16 v0, 0xe

    iget-object v1, p0, Lofs;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1285
    :cond_d
    iget-object v0, p0, Lofs;->r:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 1286
    const/16 v0, 0xf

    iget-object v1, p0, Lofs;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1288
    :cond_e
    iget-object v0, p0, Lofs;->s:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 1289
    const/16 v0, 0x10

    iget-object v1, p0, Lofs;->s:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1291
    :cond_f
    iget-object v0, p0, Lofs;->t:Ljava/lang/Double;

    if-eqz v0, :cond_10

    .line 1292
    const/16 v0, 0x11

    iget-object v1, p0, Lofs;->t:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 1294
    :cond_10
    iget-object v0, p0, Lofs;->d:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 1295
    const/16 v0, 0x12

    iget-object v1, p0, Lofs;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1297
    :cond_11
    iget-object v0, p0, Lofs;->e:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 1298
    const/16 v0, 0x13

    iget-object v1, p0, Lofs;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1300
    :cond_12
    iget-object v0, p0, Lofs;->u:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 1301
    const/16 v0, 0x14

    iget-object v1, p0, Lofs;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1303
    :cond_13
    iget-object v0, p0, Lofs;->v:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 1304
    const/16 v0, 0x15

    iget-object v1, p0, Lofs;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1306
    :cond_14
    iget-object v0, p0, Lofs;->w:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 1307
    const/16 v0, 0x16

    iget-object v1, p0, Lofs;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1309
    :cond_15
    iget-object v0, p0, Lofs;->x:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 1310
    const/16 v0, 0x17

    iget-object v1, p0, Lofs;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1312
    :cond_16
    iget-object v0, p0, Lofs;->y:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 1313
    const/16 v0, 0x18

    iget-object v1, p0, Lofs;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1315
    :cond_17
    iget-object v0, p0, Lofs;->z:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 1316
    const/16 v0, 0x19

    iget-object v1, p0, Lofs;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1318
    :cond_18
    iget-object v0, p0, Lofs;->A:Lpcp;

    if-eqz v0, :cond_19

    .line 1319
    const/16 v0, 0x1b

    iget-object v1, p0, Lofs;->A:Lpcp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1321
    :cond_19
    iget-object v0, p0, Lofs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1323
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1181
    invoke-virtual {p0, p1}, Lofs;->a(Loxn;)Lofs;

    move-result-object v0

    return-object v0
.end method
