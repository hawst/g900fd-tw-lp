.class public final Lkjs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private synthetic a:Lkjv;

.field private synthetic b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;Lkjv;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iput-object p2, p0, Lkjs;->a:Lkjv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 295
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 255
    instance-of v0, p1, Landroid/text/Spannable;

    if-eqz v0, :cond_2

    .line 256
    check-cast p1, Landroid/text/Spannable;

    .line 257
    add-int v0, p2, p3

    add-int/lit8 v0, v0, -0x1

    .line 261
    iget-object v2, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a()Ljava/util/List;

    move-result-object v3

    .line 262
    const-class v2, Landroid/text/style/URLSpan;

    invoke-interface {p1, p2, v0, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v4, v0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    .line 263
    invoke-static {v5}, Lljd;->a(Landroid/text/style/URLSpan;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 264
    invoke-interface {p1, v5}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 265
    const/4 v1, 0x1

    .line 262
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 269
    :cond_1
    if-eqz v1, :cond_2

    .line 270
    iget-object v0, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-static {v0, v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;Ljava/util/List;)V

    .line 274
    :cond_2
    iget-object v0, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 275
    iget-object v0, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d023e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 277
    iget-object v1, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v2, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->b()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setDropDownVerticalOffset(I)V

    .line 279
    :cond_3
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 283
    iget-object v0, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    .line 284
    if-lt p3, v1, :cond_0

    if-lt p4, v1, :cond_0

    add-int v1, p2, p4

    add-int/lit8 v1, v1, -0x1

    .line 285
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v2, 0x200b

    if-ne v1, v2, :cond_0

    .line 286
    iget-object v1, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-static {v1}, Llsn;->c(Landroid/view/View;)V

    .line 288
    :cond_0
    iget-object v1, p0, Lkjs;->b:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v2, p0, Lkjs;->a:Lkjv;

    invoke-virtual {v2, p1, v0}, Lkjv;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-gt v2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;Z)V

    .line 289
    return-void

    .line 288
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
