.class public final Lmew;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnod;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29401
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29404
    iput-object v0, p0, Lmew;->apiHeader:Llyq;

    .line 29407
    iput-object v0, p0, Lmew;->a:Lnod;

    .line 29401
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29424
    const/4 v0, 0x0

    .line 29425
    iget-object v1, p0, Lmew;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 29426
    const/4 v0, 0x1

    iget-object v1, p0, Lmew;->apiHeader:Llyq;

    .line 29427
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29429
    :cond_0
    iget-object v1, p0, Lmew;->a:Lnod;

    if-eqz v1, :cond_1

    .line 29430
    const/4 v1, 0x2

    iget-object v2, p0, Lmew;->a:Lnod;

    .line 29431
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29433
    :cond_1
    iget-object v1, p0, Lmew;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29434
    iput v0, p0, Lmew;->ai:I

    .line 29435
    return v0
.end method

.method public a(Loxn;)Lmew;
    .locals 2

    .prologue
    .line 29443
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 29444
    sparse-switch v0, :sswitch_data_0

    .line 29448
    iget-object v1, p0, Lmew;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 29449
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmew;->ah:Ljava/util/List;

    .line 29452
    :cond_1
    iget-object v1, p0, Lmew;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29454
    :sswitch_0
    return-object p0

    .line 29459
    :sswitch_1
    iget-object v0, p0, Lmew;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 29460
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmew;->apiHeader:Llyq;

    .line 29462
    :cond_2
    iget-object v0, p0, Lmew;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29466
    :sswitch_2
    iget-object v0, p0, Lmew;->a:Lnod;

    if-nez v0, :cond_3

    .line 29467
    new-instance v0, Lnod;

    invoke-direct {v0}, Lnod;-><init>()V

    iput-object v0, p0, Lmew;->a:Lnod;

    .line 29469
    :cond_3
    iget-object v0, p0, Lmew;->a:Lnod;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29444
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29412
    iget-object v0, p0, Lmew;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 29413
    const/4 v0, 0x1

    iget-object v1, p0, Lmew;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29415
    :cond_0
    iget-object v0, p0, Lmew;->a:Lnod;

    if-eqz v0, :cond_1

    .line 29416
    const/4 v0, 0x2

    iget-object v1, p0, Lmew;->a:Lnod;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29418
    :cond_1
    iget-object v0, p0, Lmew;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29420
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29397
    invoke-virtual {p0, p1}, Lmew;->a(Loxn;)Lmew;

    move-result-object v0

    return-object v0
.end method
