.class public final Llej;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lkdd;
.implements Llcz;
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Llci;

.field private c:Lkzm;

.field private d:Landroid/text/StaticLayout;

.field private e:Landroid/text/StaticLayout;

.field private f:Lkda;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Llej;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Llej;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    sget-object v0, Llej;->a:Llct;

    if-nez v0, :cond_0

    .line 65
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Llej;->a:Llct;

    .line 68
    :cond_0
    invoke-virtual {p0, v1}, Llej;->setFocusable(Z)V

    .line 69
    invoke-virtual {p0, v1}, Llej;->setClickable(Z)V

    .line 70
    invoke-virtual {p0, p0}, Llej;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 118
    sget-object v0, Llej;->a:Llct;

    if-nez v0, :cond_0

    .line 119
    invoke-static {p0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Llej;->a:Llct;

    .line 122
    :cond_0
    invoke-static {p0}, Lhss;->e(Landroid/content/Context;)I

    move-result v1

    .line 123
    const/16 v0, 0x14

    invoke-static {p0, v0}, Llib;->a(Landroid/content/Context;I)I

    move-result v0

    .line 125
    const/16 v2, 0x19

    invoke-static {p0, v2}, Llib;->a(Landroid/content/Context;I)I

    move-result v2

    .line 127
    add-int/2addr v0, v2

    sget-object v2, Llej;->a:Llct;

    iget v2, v2, Llct;->l:I

    add-int/2addr v0, v2

    .line 128
    sget-object v2, Llej;->a:Llct;

    iget v2, v2, Llct;->m:I

    add-int/2addr v2, v0

    if-le v2, v1, :cond_1

    .line 129
    sget-object v2, Llej;->a:Llct;

    iget v2, v2, Llct;->m:I

    add-int/2addr v0, v2

    .line 131
    :cond_1
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget-object v1, Llej;->a:Llct;

    iget v1, v1, Llct;->m:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 203
    invoke-virtual {p0}, Llej;->clearAnimation()V

    .line 204
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    .line 206
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Llej;->setAlpha(F)V

    .line 208
    :cond_0
    invoke-virtual {p0}, Llej;->c()V

    .line 209
    iput-object v1, p0, Llej;->b:Llci;

    .line 210
    iput-object v1, p0, Llej;->c:Lkzm;

    .line 211
    iput-object v1, p0, Llej;->d:Landroid/text/StaticLayout;

    .line 212
    iput-object v1, p0, Llej;->e:Landroid/text/StaticLayout;

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Llej;->i:Z

    .line 214
    return-void
.end method

.method public a(Ljava/lang/String;Llci;Lkzm;Z)V
    .locals 1

    .prologue
    .line 75
    iput-object p1, p0, Llej;->g:Ljava/lang/String;

    .line 76
    iput-object p2, p0, Llej;->b:Llci;

    .line 77
    iput-object p3, p0, Llej;->c:Lkzm;

    .line 78
    invoke-virtual {p0}, Llej;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llej;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Llej;->h:I

    .line 79
    iput-boolean p4, p0, Llej;->i:Z

    .line 81
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 82
    return-void
.end method

.method public a(Lkda;)V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Llej;->f:Lkda;

    if-ne p1, v0, :cond_0

    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 248
    invoke-virtual {p0}, Llej;->invalidate()V

    .line 250
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 230
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {p0}, Llej;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhso;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    iget-object v1, p0, Llej;->c:Lkzm;

    .line 232
    invoke-virtual {v1}, Lkzm;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 231
    invoke-interface {v0, v1, v2, v3, p0}, Lhso;->a(Ljava/lang/String;IILkdd;)Lkda;

    move-result-object v0

    iput-object v0, p0, Llej;->f:Lkda;

    .line 235
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Llej;->f:Lkda;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Llej;->f:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Llej;->f:Lkda;

    .line 243
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x1

    return v0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 197
    invoke-virtual {p0}, Llej;->invalidate()V

    .line 198
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 199
    return-void
.end method

.method public e()V
    .locals 5

    .prologue
    .line 259
    iget-object v0, p0, Llej;->b:Llci;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Llej;->b:Llci;

    iget-object v1, p0, Llej;->c:Lkzm;

    invoke-virtual {v1}, Lkzm;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x5

    iget-object v4, p0, Llej;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Llci;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 264
    :cond_0
    return-void
.end method

.method public f()Llja;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Llej;->c:Lkzm;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 5
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 87
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 88
    new-array v1, v3, [Ljava/lang/CharSequence;

    iget-object v2, p0, Llej;->c:Lkzm;

    invoke-virtual {v2}, Lkzm;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 89
    new-array v1, v3, [Ljava/lang/CharSequence;

    .line 90
    invoke-virtual {p0}, Llej;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0489

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 89
    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 91
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 224
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 225
    invoke-virtual {p0}, Llej;->b()V

    .line 226
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Llej;->b:Llci;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Llej;->b:Llci;

    iget-object v1, p0, Llej;->c:Lkzm;

    invoke-interface {v0, v1}, Llci;->a(Lkzm;)V

    .line 193
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 218
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 219
    invoke-virtual {p0}, Llej;->c()V

    .line 220
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 136
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 138
    invoke-virtual {p0}, Llej;->getWidth()I

    move-result v7

    .line 141
    invoke-virtual {p0}, Llej;->getHeight()I

    move-result v8

    .line 142
    invoke-virtual {p0}, Llej;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 144
    iget-object v0, p0, Llej;->f:Lkda;

    if-nez v0, :cond_5

    move-object v0, v3

    .line 145
    :goto_0
    if-nez v0, :cond_0

    .line 146
    invoke-static {v2, v1}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 150
    :cond_0
    invoke-static {v2}, Lhss;->e(Landroid/content/Context;)I

    move-result v2

    .line 151
    sget-object v4, Llej;->a:Llct;

    iget v4, v4, Llct;->m:I

    int-to-float v4, v4

    sub-int v5, v8, v2

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sget-object v9, Llej;->a:Llct;

    iget-object v9, v9, Llct;->I:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v5, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 154
    sget-object v0, Llej;->a:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    add-int v4, v0, v2

    .line 155
    iget-object v0, p0, Llej;->d:Landroid/text/StaticLayout;

    if-eqz v0, :cond_6

    move v0, v1

    .line 156
    :goto_1
    iget-object v2, p0, Llej;->e:Landroid/text/StaticLayout;

    if-eqz v2, :cond_7

    move v2, v1

    .line 157
    :goto_2
    if-eqz v0, :cond_8

    iget-object v1, p0, Llej;->d:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    :goto_3
    sub-int v5, v8, v1

    if-eqz v2, :cond_9

    iget-object v1, p0, Llej;->e:Landroid/text/StaticLayout;

    .line 158
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget-object v9, Llej;->a:Llct;

    iget v9, v9, Llct;->l:I

    add-int/2addr v1, v9

    :goto_4
    sub-int v1, v5, v1

    div-int/lit8 v1, v1, 0x2

    .line 160
    if-eqz v0, :cond_a

    .line 161
    int-to-float v0, v4

    int-to-float v5, v1

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 162
    iget-object v0, p0, Llej;->d:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 163
    neg-int v0, v4

    int-to-float v0, v0

    neg-int v5, v1

    int-to-float v5, v5

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 164
    iget-object v0, p0, Llej;->d:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v5, Llej;->a:Llct;

    iget v5, v5, Llct;->l:I

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    .line 167
    :goto_5
    if-eqz v2, :cond_1

    .line 168
    sget-object v1, Llej;->a:Llct;

    iget-object v1, v1, Llct;->ay:Landroid/graphics/Bitmap;

    int-to-float v2, v4

    int-to-float v5, v0

    invoke-virtual {p1, v1, v2, v5, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 169
    sget-object v1, Llej;->a:Llct;

    iget-object v1, v1, Llct;->ay:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget-object v2, Llej;->a:Llct;

    iget v2, v2, Llct;->k:I

    add-int/2addr v1, v2

    add-int/2addr v1, v4

    .line 170
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 171
    iget-object v2, p0, Llej;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 172
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 175
    :cond_1
    iget-boolean v0, p0, Llej;->i:Z

    if-eqz v0, :cond_2

    .line 176
    sget-object v0, Llej;->a:Llct;

    iget-object v0, v0, Llct;->w:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    float-to-int v0, v0

    .line 177
    const/4 v1, 0x0

    sub-int v2, v8, v0

    int-to-float v2, v2

    int-to-float v3, v7

    sub-int v0, v8, v0

    int-to-float v4, v0

    sget-object v0, Llej;->a:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 181
    :cond_2
    invoke-virtual {p0}, Llej;->isPressed()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Llej;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 182
    :cond_3
    sget-object v0, Llej;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v6, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 184
    sget-object v0, Llej;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 186
    :cond_4
    return-void

    .line 144
    :cond_5
    iget-object v0, p0, Llej;->f:Lkda;

    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto/16 :goto_0

    :cond_6
    move v0, v6

    .line 155
    goto/16 :goto_1

    :cond_7
    move v2, v6

    .line 156
    goto/16 :goto_2

    :cond_8
    move v1, v6

    .line 157
    goto/16 :goto_3

    :cond_9
    move v1, v6

    .line 158
    goto/16 :goto_4

    :cond_a
    move v0, v1

    goto :goto_5
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 96
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 97
    invoke-virtual {p0}, Llej;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 98
    invoke-static {v1}, Lhss;->e(Landroid/content/Context;)I

    move-result v2

    .line 100
    sget-object v3, Llej;->a:Llct;

    iget v3, v3, Llct;->m:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    sub-int v2, v3, v2

    .line 103
    const/16 v3, 0x14

    .line 104
    invoke-static {v1, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    iget-object v4, p0, Llej;->c:Lkzm;

    .line 105
    invoke-virtual {v4}, Lkzm;->b()Ljava/lang/String;

    move-result-object v4

    .line 103
    invoke-static {v3, v4, v2, v5}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v3

    iput-object v3, p0, Llej;->d:Landroid/text/StaticLayout;

    .line 107
    sget-object v3, Llej;->a:Llct;

    iget-object v3, v3, Llct;->ay:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    sget-object v3, Llej;->a:Llct;

    iget v3, v3, Llct;->k:I

    sub-int/2addr v2, v3

    .line 109
    const/16 v3, 0x19

    .line 110
    invoke-static {v1, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    const v4, 0x7f0a0489

    .line 111
    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v3, v1, v2, v5}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Llej;->e:Landroid/text/StaticLayout;

    .line 114
    iget v1, p0, Llej;->h:I

    invoke-virtual {p0, v0, v1}, Llej;->setMeasuredDimension(II)V

    .line 115
    return-void
.end method
