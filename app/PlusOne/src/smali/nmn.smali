.class public final Lnmn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnmn;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 564
    const/4 v0, 0x0

    new-array v0, v0, [Lnmn;

    sput-object v0, Lnmn;->a:[Lnmn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 565
    invoke-direct {p0}, Loxq;-><init>()V

    .line 578
    const/high16 v0, -0x80000000

    iput v0, p0, Lnmn;->g:I

    .line 565
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 599
    const/4 v0, 0x1

    iget-object v1, p0, Lnmn;->b:Ljava/lang/String;

    .line 601
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 602
    const/4 v1, 0x2

    iget-object v2, p0, Lnmn;->c:Ljava/lang/String;

    .line 603
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 604
    const/4 v1, 0x3

    iget-object v2, p0, Lnmn;->e:Ljava/lang/Integer;

    .line 605
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 606
    const/4 v1, 0x4

    iget-object v2, p0, Lnmn;->f:Ljava/lang/Integer;

    .line 607
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 608
    iget-object v1, p0, Lnmn;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 609
    const/4 v1, 0x5

    iget-object v2, p0, Lnmn;->d:Ljava/lang/String;

    .line 610
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 612
    :cond_0
    iget v1, p0, Lnmn;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 613
    const/4 v1, 0x6

    iget v2, p0, Lnmn;->g:I

    .line 614
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 616
    :cond_1
    iget-object v1, p0, Lnmn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 617
    iput v0, p0, Lnmn;->ai:I

    .line 618
    return v0
.end method

.method public a(Loxn;)Lnmn;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 626
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 627
    sparse-switch v0, :sswitch_data_0

    .line 631
    iget-object v1, p0, Lnmn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 632
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnmn;->ah:Ljava/util/List;

    .line 635
    :cond_1
    iget-object v1, p0, Lnmn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 637
    :sswitch_0
    return-object p0

    .line 642
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmn;->b:Ljava/lang/String;

    goto :goto_0

    .line 646
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmn;->c:Ljava/lang/String;

    goto :goto_0

    .line 650
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnmn;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 654
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnmn;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 658
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmn;->d:Ljava/lang/String;

    goto :goto_0

    .line 662
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 663
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 666
    :cond_2
    iput v0, p0, Lnmn;->g:I

    goto :goto_0

    .line 668
    :cond_3
    iput v2, p0, Lnmn;->g:I

    goto :goto_0

    .line 627
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 583
    const/4 v0, 0x1

    iget-object v1, p0, Lnmn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 584
    const/4 v0, 0x2

    iget-object v1, p0, Lnmn;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 585
    const/4 v0, 0x3

    iget-object v1, p0, Lnmn;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 586
    const/4 v0, 0x4

    iget-object v1, p0, Lnmn;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 587
    iget-object v0, p0, Lnmn;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 588
    const/4 v0, 0x5

    iget-object v1, p0, Lnmn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 590
    :cond_0
    iget v0, p0, Lnmn;->g:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 591
    const/4 v0, 0x6

    iget v1, p0, Lnmn;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 593
    :cond_1
    iget-object v0, p0, Lnmn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 595
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 561
    invoke-virtual {p0, p1}, Lnmn;->a(Loxn;)Lnmn;

    move-result-object v0

    return-object v0
.end method
