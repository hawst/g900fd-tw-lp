.class public final Lcrp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnTouchListener;"
    }
.end annotation


# instance fields
.field private a:F

.field private b:I

.field private c:Z

.field private d:Z

.field private e:F

.field private f:F

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/widget/ListView;

.field private final i:Lcry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcry",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final j:[Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListView;[Landroid/view/View;Lcry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/ListView;",
            "[",
            "Landroid/view/View;",
            "Lcry",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcrp;->f:F

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcrp;->g:Ljava/util/Map;

    .line 65
    iput-object p2, p0, Lcrp;->h:Landroid/widget/ListView;

    .line 67
    iput-object p4, p0, Lcrp;->i:Lcry;

    .line 68
    iput-object p3, p0, Lcrp;->j:[Landroid/view/View;

    .line 69
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcrp;->b:I

    .line 70
    return-void
.end method

.method static synthetic a(Lcrp;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcrp;->h:Landroid/widget/ListView;

    return-object v0
.end method

.method private a(Landroid/animation/Animator;Ljava/lang/Runnable;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 337
    if-eqz p2, :cond_0

    .line 338
    new-instance v0, Lcrv;

    invoke-direct {v0, p2}, Lcrv;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 345
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;F)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 215
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 216
    invoke-static {}, Lcrp;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 218
    sub-float v0, v3, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 232
    :goto_0
    return-void

    .line 221
    :cond_0
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v1, p2, p2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 222
    iput p2, p0, Lcrp;->e:F

    .line 223
    sub-float v0, v3, v0

    iput v0, p0, Lcrp;->f:F

    .line 224
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    iget v2, p0, Lcrp;->f:F

    iget v3, p0, Lcrp;->f:F

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 225
    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 226
    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 227
    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 228
    invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 229
    invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->setFillEnabled(Z)V

    .line 230
    invoke-virtual {p1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method static synthetic a(Lcrp;Landroid/view/View;FFFFLjava/lang/Runnable;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x96

    .line 35
    invoke-static {}, Lcrp;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    cmpl-float v0, p2, p3

    if-eqz v0, :cond_0

    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v1, v6, [F

    aput p2, v1, v4

    aput p3, v1, v5

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    invoke-direct {p0, v0, p6}, Lcrp;->a(Landroid/animation/Animator;Ljava/lang/Runnable;)V

    const/4 p6, 0x0

    :cond_0
    cmpl-float v0, p4, p5

    if-eqz v0, :cond_1

    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v1, v6, [F

    aput p4, v1, v4

    aput p5, v1, v5

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    invoke-direct {p0, v0, p6}, Lcrp;->a(Landroid/animation/Animator;Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, p2, p3, p4, p5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    if-eqz p6, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcru;

    invoke-direct {v1, p6}, Lcru;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method

.method static synthetic a(Lcrp;Landroid/widget/ListView;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 35
    invoke-virtual {p1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    add-int v3, v1, v0

    iget-object v4, p0, Lcrp;->i:Lcry;

    invoke-interface {v4, v3}, Lcry;->getItemId(I)J

    move-result-wide v4

    if-eq v2, p2, :cond_0

    iget-object v3, p0, Lcrp;->g:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcrp;->h:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcrp;->i:Lcry;

    iget-object v2, p0, Lcrp;->i:Lcry;

    invoke-interface {v2, v0}, Lcry;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Lcry;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcrs;

    invoke-direct {v1, p0, v0, p1}, Lcrs;-><init>(Lcrp;Landroid/view/ViewTreeObserver;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    return-void
.end method

.method private static a()Z
    .locals 2

    .prologue
    .line 381
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcrp;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcrp;->d:Z

    return p1
.end method

.method static synthetic b(Lcrp;)Lcry;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcrp;->i:Lcry;

    return-object v0
.end method

.method static synthetic b(Lcrp;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcrp;->c:Z

    return p1
.end method

.method static synthetic c(Lcrp;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcrp;->g:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 75
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v1, v2

    .line 153
    :cond_0
    :goto_0
    return v1

    .line 77
    :pswitch_0
    iget-boolean v0, p0, Lcrp;->c:Z

    if-nez v0, :cond_0

    .line 81
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcrp;->a:F

    goto :goto_0

    .line 85
    :pswitch_1
    invoke-direct {p0, p1, v4}, Lcrp;->a(Landroid/view/View;F)V

    .line 86
    iput-boolean v2, p0, Lcrp;->d:Z

    goto :goto_0

    .line 90
    :pswitch_2
    iget-boolean v0, p0, Lcrp;->c:Z

    if-nez v0, :cond_0

    .line 93
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 94
    invoke-static {}, Lcrp;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 95
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v3

    add-float/2addr v0, v3

    .line 97
    :cond_1
    iget v3, p0, Lcrp;->a:F

    sub-float v3, v0, v3

    .line 98
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 99
    iget-boolean v4, p0, Lcrp;->d:Z

    if-nez v4, :cond_2

    .line 100
    iget v4, p0, Lcrp;->b:I

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    .line 101
    iput-boolean v1, p0, Lcrp;->d:Z

    .line 102
    iget-object v0, p0, Lcrp;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 103
    iget-object v4, p0, Lcrp;->j:[Landroid/view/View;

    array-length v5, v4

    move v0, v2

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    .line 104
    invoke-virtual {v6, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 108
    :cond_2
    iget-boolean v0, p0, Lcrp;->d:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0, p1, v3}, Lcrp;->a(Landroid/view/View;F)V

    goto :goto_0

    .line 114
    :pswitch_3
    iget-boolean v0, p0, Lcrp;->c:Z

    if-nez v0, :cond_0

    .line 118
    iget-boolean v0, p0, Lcrp;->d:Z

    if-eqz v0, :cond_4

    .line 119
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 120
    invoke-static {}, Lcrp;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 121
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v3

    add-float/2addr v0, v3

    .line 123
    :cond_3
    iget v3, p0, Lcrp;->a:F

    sub-float/2addr v0, v3

    .line 124
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 128
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x4

    int-to-float v5, v5

    cmpl-float v5, v3, v5

    if-lez v5, :cond_6

    .line 130
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v3, v5

    .line 131
    cmpg-float v0, v0, v4

    if-gez v0, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    :goto_2
    move v5, v3

    move v3, v0

    move v0, v1

    .line 140
    :goto_3
    sub-float v5, v6, v5

    const/high16 v7, 0x437a0000    # 250.0f

    mul-float/2addr v5, v7

    float-to-int v5, v5

    int-to-long v8, v5

    .line 141
    iput-boolean v1, p0, Lcrp;->c:Z

    iget-object v5, p0, Lcrp;->h:Landroid/widget/ListView;

    invoke-virtual {v5, v2}, Landroid/widget/ListView;->setEnabled(Z)V

    invoke-static {}, Lcrp;->a()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    if-eqz v0, :cond_7

    :goto_4
    invoke-virtual {v5, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    new-instance v4, Lcrq;

    invoke-direct {v4, p0, p1, v0}, Lcrq;-><init>(Lcrp;Landroid/view/View;Z)V

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 142
    :cond_4
    :goto_5
    iget-object v0, p0, Lcrp;->j:[Landroid/view/View;

    array-length v3, v0

    :goto_6
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 146
    invoke-virtual {v4, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 145
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 131
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    goto :goto_2

    .line 135
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v3, v0

    sub-float v0, v6, v0

    move v3, v4

    move v5, v0

    move v0, v2

    .line 137
    goto :goto_3

    :cond_7
    move v4, v6

    .line 141
    goto :goto_4

    :cond_8
    new-instance v5, Landroid/view/animation/TranslateAnimation;

    iget v7, p0, Lcrp;->e:F

    invoke-direct {v5, v7, v3, v4, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    new-instance v3, Landroid/view/animation/AlphaAnimation;

    iget v7, p0, Lcrp;->f:F

    if-eqz v0, :cond_9

    :goto_7
    invoke-direct {v3, v7, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    new-instance v4, Landroid/view/animation/AnimationSet;

    invoke-direct {v4, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    invoke-virtual {v4, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v4, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v4, v8, v9}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    invoke-virtual {p1, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v3, Lcrr;

    invoke-direct {v3, p0, v0, p1}, Lcrr;-><init>(Lcrp;ZLandroid/view/View;)V

    new-instance v0, Lcrw;

    invoke-direct {v0, v3}, Lcrw;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_5

    :cond_9
    move v4, v6

    goto :goto_7

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
