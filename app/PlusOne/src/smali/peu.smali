.class public final Lpeu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Llug;

.field public c:Ljava/lang/Integer;

.field public d:[I

.field public e:[B

.field private f:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x466c765

    new-instance v1, Lpev;

    invoke-direct {v1}, Lpev;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lpeu;->b:Llug;

    .line 24
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lpeu;->f:[I

    .line 27
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lpeu;->d:[I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 62
    .line 63
    iget-object v0, p0, Lpeu;->a:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 64
    const/4 v0, 0x1

    iget-object v2, p0, Lpeu;->a:Ljava/lang/String;

    .line 65
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 67
    :goto_0
    iget-object v2, p0, Lpeu;->b:Llug;

    if-eqz v2, :cond_0

    .line 68
    const/4 v2, 0x2

    iget-object v3, p0, Lpeu;->b:Llug;

    .line 69
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 71
    :cond_0
    iget-object v2, p0, Lpeu;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 72
    const/4 v2, 0x3

    iget-object v3, p0, Lpeu;->c:Ljava/lang/Integer;

    .line 73
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 75
    :cond_1
    iget-object v2, p0, Lpeu;->f:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lpeu;->f:[I

    array-length v2, v2

    if-lez v2, :cond_3

    .line 77
    iget-object v4, p0, Lpeu;->f:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget v6, v4, v2

    .line 79
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 77
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 81
    :cond_2
    add-int/2addr v0, v3

    .line 82
    iget-object v2, p0, Lpeu;->f:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 84
    :cond_3
    iget-object v2, p0, Lpeu;->d:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Lpeu;->d:[I

    array-length v2, v2

    if-lez v2, :cond_5

    .line 86
    iget-object v3, p0, Lpeu;->d:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_4

    aget v5, v3, v1

    .line 88
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 90
    :cond_4
    add-int/2addr v0, v2

    .line 91
    iget-object v1, p0, Lpeu;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 93
    :cond_5
    iget-object v1, p0, Lpeu;->e:[B

    if-eqz v1, :cond_6

    .line 94
    const/4 v1, 0x7

    iget-object v2, p0, Lpeu;->e:[B

    .line 95
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_6
    iget-object v1, p0, Lpeu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    iput v0, p0, Lpeu;->ai:I

    .line 99
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpeu;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 107
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 108
    sparse-switch v0, :sswitch_data_0

    .line 112
    iget-object v1, p0, Lpeu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 113
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpeu;->ah:Ljava/util/List;

    .line 116
    :cond_1
    iget-object v1, p0, Lpeu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    :sswitch_0
    return-object p0

    .line 123
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpeu;->a:Ljava/lang/String;

    goto :goto_0

    .line 127
    :sswitch_2
    iget-object v0, p0, Lpeu;->b:Llug;

    if-nez v0, :cond_2

    .line 128
    new-instance v0, Llug;

    invoke-direct {v0}, Llug;-><init>()V

    iput-object v0, p0, Lpeu;->b:Llug;

    .line 130
    :cond_2
    iget-object v0, p0, Lpeu;->b:Llug;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 134
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpeu;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 138
    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 139
    iget-object v0, p0, Lpeu;->f:[I

    array-length v0, v0

    .line 140
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 141
    iget-object v2, p0, Lpeu;->f:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    iput-object v1, p0, Lpeu;->f:[I

    .line 143
    :goto_1
    iget-object v1, p0, Lpeu;->f:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 144
    iget-object v1, p0, Lpeu;->f:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 145
    invoke-virtual {p1}, Loxn;->a()I

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 148
    :cond_3
    iget-object v1, p0, Lpeu;->f:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 152
    :sswitch_5
    const/16 v0, 0x28

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 153
    iget-object v0, p0, Lpeu;->d:[I

    array-length v0, v0

    .line 154
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 155
    iget-object v2, p0, Lpeu;->d:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 156
    iput-object v1, p0, Lpeu;->d:[I

    .line 157
    :goto_2
    iget-object v1, p0, Lpeu;->d:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 158
    iget-object v1, p0, Lpeu;->d:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 159
    invoke-virtual {p1}, Loxn;->a()I

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 162
    :cond_4
    iget-object v1, p0, Lpeu;->d:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 166
    :sswitch_6
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpeu;->e:[B

    goto/16 :goto_0

    .line 108
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 34
    iget-object v1, p0, Lpeu;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35
    const/4 v1, 0x1

    iget-object v2, p0, Lpeu;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 37
    :cond_0
    iget-object v1, p0, Lpeu;->b:Llug;

    if-eqz v1, :cond_1

    .line 38
    const/4 v1, 0x2

    iget-object v2, p0, Lpeu;->b:Llug;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 40
    :cond_1
    iget-object v1, p0, Lpeu;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 41
    const/4 v1, 0x3

    iget-object v2, p0, Lpeu;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 43
    :cond_2
    iget-object v1, p0, Lpeu;->f:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lpeu;->f:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 44
    iget-object v2, p0, Lpeu;->f:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget v4, v2, v1

    .line 45
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48
    :cond_3
    iget-object v1, p0, Lpeu;->d:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lpeu;->d:[I

    array-length v1, v1

    if-lez v1, :cond_4

    .line 49
    iget-object v1, p0, Lpeu;->d:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget v3, v1, v0

    .line 50
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 53
    :cond_4
    iget-object v0, p0, Lpeu;->e:[B

    if-eqz v0, :cond_5

    .line 54
    const/4 v0, 0x7

    iget-object v1, p0, Lpeu;->e:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 56
    :cond_5
    iget-object v0, p0, Lpeu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 58
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpeu;->a(Loxn;)Lpeu;

    move-result-object v0

    return-object v0
.end method
