.class public final Lncd;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lndj;

.field private c:Lncf;

.field private d:Ljava/lang/String;

.field private e:Lovt;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20
    const/high16 v0, -0x80000000

    iput v0, p0, Lncd;->a:I

    .line 23
    iput-object v1, p0, Lncd;->b:Lndj;

    .line 26
    iput-object v1, p0, Lncd;->c:Lncf;

    .line 31
    iput-object v1, p0, Lncd;->e:Lovt;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x0

    .line 58
    iget v1, p0, Lncd;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 59
    const/4 v0, 0x1

    iget v1, p0, Lncd;->a:I

    .line 60
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 62
    :cond_0
    iget-object v1, p0, Lncd;->b:Lndj;

    if-eqz v1, :cond_1

    .line 63
    const/4 v1, 0x3

    iget-object v2, p0, Lncd;->b:Lndj;

    .line 64
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_1
    iget-object v1, p0, Lncd;->c:Lncf;

    if-eqz v1, :cond_2

    .line 67
    const/4 v1, 0x4

    iget-object v2, p0, Lncd;->c:Lncf;

    .line 68
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_2
    iget-object v1, p0, Lncd;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 71
    const/4 v1, 0x5

    iget-object v2, p0, Lncd;->d:Ljava/lang/String;

    .line 72
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_3
    iget-object v1, p0, Lncd;->e:Lovt;

    if-eqz v1, :cond_4

    .line 75
    const/4 v1, 0x6

    iget-object v2, p0, Lncd;->e:Lovt;

    .line 76
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_4
    iget-object v1, p0, Lncd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    iput v0, p0, Lncd;->ai:I

    .line 80
    return v0
.end method

.method public a(Loxn;)Lncd;
    .locals 2

    .prologue
    .line 88
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 89
    sparse-switch v0, :sswitch_data_0

    .line 93
    iget-object v1, p0, Lncd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lncd;->ah:Ljava/util/List;

    .line 97
    :cond_1
    iget-object v1, p0, Lncd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    :sswitch_0
    return-object p0

    .line 104
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 105
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 109
    :cond_2
    iput v0, p0, Lncd;->a:I

    goto :goto_0

    .line 111
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lncd;->a:I

    goto :goto_0

    .line 116
    :sswitch_2
    iget-object v0, p0, Lncd;->b:Lndj;

    if-nez v0, :cond_4

    .line 117
    new-instance v0, Lndj;

    invoke-direct {v0}, Lndj;-><init>()V

    iput-object v0, p0, Lncd;->b:Lndj;

    .line 119
    :cond_4
    iget-object v0, p0, Lncd;->b:Lndj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 123
    :sswitch_3
    iget-object v0, p0, Lncd;->c:Lncf;

    if-nez v0, :cond_5

    .line 124
    new-instance v0, Lncf;

    invoke-direct {v0}, Lncf;-><init>()V

    iput-object v0, p0, Lncd;->c:Lncf;

    .line 126
    :cond_5
    iget-object v0, p0, Lncd;->c:Lncf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 130
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncd;->d:Ljava/lang/String;

    goto :goto_0

    .line 134
    :sswitch_5
    iget-object v0, p0, Lncd;->e:Lovt;

    if-nez v0, :cond_6

    .line 135
    new-instance v0, Lovt;

    invoke-direct {v0}, Lovt;-><init>()V

    iput-object v0, p0, Lncd;->e:Lovt;

    .line 137
    :cond_6
    iget-object v0, p0, Lncd;->e:Lovt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 89
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 36
    iget v0, p0, Lncd;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 37
    const/4 v0, 0x1

    iget v1, p0, Lncd;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 39
    :cond_0
    iget-object v0, p0, Lncd;->b:Lndj;

    if-eqz v0, :cond_1

    .line 40
    const/4 v0, 0x3

    iget-object v1, p0, Lncd;->b:Lndj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 42
    :cond_1
    iget-object v0, p0, Lncd;->c:Lncf;

    if-eqz v0, :cond_2

    .line 43
    const/4 v0, 0x4

    iget-object v1, p0, Lncd;->c:Lncf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 45
    :cond_2
    iget-object v0, p0, Lncd;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 46
    const/4 v0, 0x5

    iget-object v1, p0, Lncd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 48
    :cond_3
    iget-object v0, p0, Lncd;->e:Lovt;

    if-eqz v0, :cond_4

    .line 49
    const/4 v0, 0x6

    iget-object v1, p0, Lncd;->e:Lovt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 51
    :cond_4
    iget-object v0, p0, Lncd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 53
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lncd;->a(Loxn;)Lncd;

    move-result-object v0

    return-object v0
.end method
