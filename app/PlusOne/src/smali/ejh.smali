.class public final Lejh;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private N:Ljava/lang/String;

.field private O:I

.field private P:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lt;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 40
    invoke-virtual {p0}, Lejh;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 41
    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lejh;->N:Ljava/lang/String;

    .line 42
    const-string v1, "gender"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lejh;->O:I

    .line 43
    const-string v1, "target_mute"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lejh;->P:Z

    .line 45
    invoke-virtual {p0}, Lejh;->n()Lz;

    move-result-object v1

    .line 46
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 47
    iget-boolean v0, p0, Lejh;->P:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a0856

    :goto_0
    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lejh;->N:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, v3}, Lejh;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 51
    const v0, 0x104000a

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 52
    const/high16 v0, 0x1040000

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 53
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 56
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04007e

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 58
    const v0, 0x7f100139

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 60
    iget v1, p0, Lejh;->O:I

    if-ne v1, v6, :cond_2

    .line 61
    iget-boolean v1, p0, Lejh;->P:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0a0857

    :goto_1
    invoke-virtual {p0, v1}, Lejh;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 70
    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const v0, 0x7f100240

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 73
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 76
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 47
    :cond_0
    const v0, 0x7f0a085b

    goto :goto_0

    .line 61
    :cond_1
    const v1, 0x7f0a085c

    goto :goto_1

    .line 63
    :cond_2
    iget v1, p0, Lejh;->O:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_4

    .line 64
    iget-boolean v1, p0, Lejh;->P:Z

    if-eqz v1, :cond_3

    const v1, 0x7f0a0858

    :goto_3
    invoke-virtual {p0, v1}, Lejh;->e_(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    const v1, 0x7f0a085d

    goto :goto_3

    .line 67
    :cond_4
    iget-boolean v1, p0, Lejh;->P:Z

    if-eqz v1, :cond_5

    const v1, 0x7f0a0859

    :goto_4
    invoke-virtual {p0, v1}, Lejh;->e_(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_5
    const v1, 0x7f0a085e

    goto :goto_4
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 81
    packed-switch p2, :pswitch_data_0

    .line 91
    :goto_0
    return-void

    .line 83
    :pswitch_0
    invoke-virtual {p0}, Lejh;->u_()Lu;

    move-result-object v0

    check-cast v0, Leet;

    iget-boolean v1, p0, Lejh;->P:Z

    invoke-virtual {v0, v1}, Leet;->c(Z)V

    goto :goto_0

    .line 87
    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
