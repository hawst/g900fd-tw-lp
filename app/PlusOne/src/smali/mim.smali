.class public final Lmim;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvs;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6559
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6562
    iput-object v0, p0, Lmim;->apiHeader:Llyq;

    .line 6565
    iput-object v0, p0, Lmim;->a:Lnvs;

    .line 6559
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6582
    const/4 v0, 0x0

    .line 6583
    iget-object v1, p0, Lmim;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 6584
    const/4 v0, 0x1

    iget-object v1, p0, Lmim;->apiHeader:Llyq;

    .line 6585
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6587
    :cond_0
    iget-object v1, p0, Lmim;->a:Lnvs;

    if-eqz v1, :cond_1

    .line 6588
    const/4 v1, 0x2

    iget-object v2, p0, Lmim;->a:Lnvs;

    .line 6589
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6591
    :cond_1
    iget-object v1, p0, Lmim;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6592
    iput v0, p0, Lmim;->ai:I

    .line 6593
    return v0
.end method

.method public a(Loxn;)Lmim;
    .locals 2

    .prologue
    .line 6601
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6602
    sparse-switch v0, :sswitch_data_0

    .line 6606
    iget-object v1, p0, Lmim;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6607
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmim;->ah:Ljava/util/List;

    .line 6610
    :cond_1
    iget-object v1, p0, Lmim;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6612
    :sswitch_0
    return-object p0

    .line 6617
    :sswitch_1
    iget-object v0, p0, Lmim;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 6618
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmim;->apiHeader:Llyq;

    .line 6620
    :cond_2
    iget-object v0, p0, Lmim;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6624
    :sswitch_2
    iget-object v0, p0, Lmim;->a:Lnvs;

    if-nez v0, :cond_3

    .line 6625
    new-instance v0, Lnvs;

    invoke-direct {v0}, Lnvs;-><init>()V

    iput-object v0, p0, Lmim;->a:Lnvs;

    .line 6627
    :cond_3
    iget-object v0, p0, Lmim;->a:Lnvs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6602
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6570
    iget-object v0, p0, Lmim;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 6571
    const/4 v0, 0x1

    iget-object v1, p0, Lmim;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6573
    :cond_0
    iget-object v0, p0, Lmim;->a:Lnvs;

    if-eqz v0, :cond_1

    .line 6574
    const/4 v0, 0x2

    iget-object v1, p0, Lmim;->a:Lnvs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6576
    :cond_1
    iget-object v0, p0, Lmim;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6578
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6555
    invoke-virtual {p0, p1}, Lmim;->a(Loxn;)Lmim;

    move-result-object v0

    return-object v0
.end method
