.class public final Lhyr;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Loxu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/String;

.field private c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lhyr;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhyr;->b:Ljava/lang/String;

    .line 50
    return-void
.end method

.method private a([B)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 106
    sget-object v2, Lhyr;->a:Ljava/util/Map;

    monitor-enter v2

    .line 107
    :try_start_0
    sget-object v0, Lhyr;->a:Ljava/util/Map;

    iget-object v3, p0, Lhyr;->b:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    .line 108
    if-ne v0, v1, :cond_0

    .line 109
    monitor-exit v2

    .line 120
    :goto_0
    return-void

    .line 113
    :cond_0
    array-length v3, p1

    const/high16 v4, 0x80000

    if-le v3, v4, :cond_1

    move v0, v1

    .line 119
    :goto_1
    sget-object v1, Lhyr;->a:Ljava/util/Map;

    iget-object v3, p0, Lhyr;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 117
    :cond_1
    :try_start_1
    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1
.end method

.method public static a(Loxu;Ljava/lang/Class;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">(TT;",
            "Ljava/lang/Class",
            "<TT;>;)[B"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lhyr;

    invoke-direct {v0, p1}, Lhyr;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v0, p0}, Lhyr;->a(Loxu;)[B

    move-result-object v0

    return-object v0
.end method

.method private b(Loxu;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[B"
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {p1}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 101
    invoke-direct {p0, v0}, Lhyr;->a([B)V

    .line 102
    return-object v0
.end method


# virtual methods
.method public a(Loxu;)[B
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[B"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lhyr;->c:[B

    if-nez v0, :cond_0

    .line 70
    sget-object v1, Lhyr;->a:Ljava/util/Map;

    monitor-enter v1

    .line 71
    :try_start_0
    sget-object v0, Lhyr;->a:Ljava/util/Map;

    iget-object v2, p0, Lhyr;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    .line 72
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    if-lez v0, :cond_1

    .line 74
    const/high16 v1, 0x80000

    shl-int/lit8 v0, v0, 0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 75
    new-array v0, v0, [B

    iput-object v0, p0, Lhyr;->c:[B

    .line 82
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhyr;->c:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lhyr;->c:[B

    array-length v2, v2

    .line 83
    invoke-static {v0, v1, v2}, Loxo;->a([BII)Loxo;

    move-result-object v0

    .line 84
    invoke-virtual {p1, v0}, Loxu;->a(Loxo;)V

    .line 86
    iget-object v1, p0, Lhyr;->c:[B

    iget-object v2, p0, Lhyr;->c:[B

    array-length v2, v2

    invoke-virtual {v0}, Loxo;->a()I

    move-result v0

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    .line 87
    invoke-direct {p0, v0}, Lhyr;->a([B)V
    :try_end_1
    .catch Loxp; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 91
    :goto_0
    return-object v0

    .line 72
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 77
    :cond_1
    invoke-direct {p0, p1}, Lhyr;->b(Loxu;)[B

    move-result-object v0

    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    invoke-direct {p0, p1}, Lhyr;->b(Loxu;)[B

    move-result-object v0

    goto :goto_0

    .line 93
    :catch_1
    move-exception v0

    .line 94
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
