.class public final Lotm;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Lots;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 834
    invoke-direct {p0}, Loxq;-><init>()V

    .line 839
    const/4 v0, 0x0

    iput-object v0, p0, Lotm;->b:Lots;

    .line 834
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 856
    const/4 v0, 0x0

    .line 857
    iget-object v1, p0, Lotm;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 858
    const/4 v0, 0x1

    iget-object v1, p0, Lotm;->a:Ljava/lang/Boolean;

    .line 859
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 861
    :cond_0
    iget-object v1, p0, Lotm;->b:Lots;

    if-eqz v1, :cond_1

    .line 862
    const/4 v1, 0x2

    iget-object v2, p0, Lotm;->b:Lots;

    .line 863
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 865
    :cond_1
    iget-object v1, p0, Lotm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 866
    iput v0, p0, Lotm;->ai:I

    .line 867
    return v0
.end method

.method public a(Loxn;)Lotm;
    .locals 2

    .prologue
    .line 875
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 876
    sparse-switch v0, :sswitch_data_0

    .line 880
    iget-object v1, p0, Lotm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 881
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lotm;->ah:Ljava/util/List;

    .line 884
    :cond_1
    iget-object v1, p0, Lotm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 886
    :sswitch_0
    return-object p0

    .line 891
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lotm;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 895
    :sswitch_2
    iget-object v0, p0, Lotm;->b:Lots;

    if-nez v0, :cond_2

    .line 896
    new-instance v0, Lots;

    invoke-direct {v0}, Lots;-><init>()V

    iput-object v0, p0, Lotm;->b:Lots;

    .line 898
    :cond_2
    iget-object v0, p0, Lotm;->b:Lots;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 876
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 844
    iget-object v0, p0, Lotm;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 845
    const/4 v0, 0x1

    iget-object v1, p0, Lotm;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 847
    :cond_0
    iget-object v0, p0, Lotm;->b:Lots;

    if-eqz v0, :cond_1

    .line 848
    const/4 v0, 0x2

    iget-object v1, p0, Lotm;->b:Lots;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 850
    :cond_1
    iget-object v0, p0, Lotm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 852
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 830
    invoke-virtual {p0, p1}, Lotm;->a(Loxn;)Lotm;

    move-result-object v0

    return-object v0
.end method
