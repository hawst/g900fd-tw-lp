.class public final Lpzq;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpzq;


# instance fields
.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lpzr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [Lpzq;

    sput-object v0, Lpzq;->a:[Lpzq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lpzq;->d:Lpzr;

    .line 26
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, Lpzq;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55
    const/4 v0, 0x1

    iget-object v1, p0, Lpzq;->b:Ljava/lang/String;

    .line 56
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 58
    :cond_0
    iget-object v1, p0, Lpzq;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 59
    const/4 v1, 0x2

    iget-object v2, p0, Lpzq;->c:Ljava/lang/String;

    .line 60
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_1
    iget-object v1, p0, Lpzq;->d:Lpzr;

    if-eqz v1, :cond_2

    .line 63
    const/4 v1, 0x3

    iget-object v2, p0, Lpzq;->d:Lpzr;

    .line 64
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_2
    iget-object v1, p0, Lpzq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    iput v0, p0, Lpzq;->ai:I

    .line 68
    return v0
.end method

.method public a(Loxn;)Lpzq;
    .locals 2

    .prologue
    .line 76
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 77
    sparse-switch v0, :sswitch_data_0

    .line 81
    iget-object v1, p0, Lpzq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpzq;->ah:Ljava/util/List;

    .line 85
    :cond_1
    iget-object v1, p0, Lpzq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    :sswitch_0
    return-object p0

    .line 92
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpzq;->b:Ljava/lang/String;

    goto :goto_0

    .line 96
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpzq;->c:Ljava/lang/String;

    goto :goto_0

    .line 100
    :sswitch_3
    iget-object v0, p0, Lpzq;->d:Lpzr;

    if-nez v0, :cond_2

    .line 101
    new-instance v0, Lpzr;

    invoke-direct {v0}, Lpzr;-><init>()V

    iput-object v0, p0, Lpzq;->d:Lpzr;

    .line 103
    :cond_2
    iget-object v0, p0, Lpzq;->d:Lpzr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lpzq;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-object v1, p0, Lpzq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 41
    :cond_0
    iget-object v0, p0, Lpzq;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 42
    const/4 v0, 0x2

    iget-object v1, p0, Lpzq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 44
    :cond_1
    iget-object v0, p0, Lpzq;->d:Lpzr;

    if-eqz v0, :cond_2

    .line 45
    const/4 v0, 0x3

    iget-object v1, p0, Lpzq;->d:Lpzr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 47
    :cond_2
    iget-object v0, p0, Lpzq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 49
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lpzq;->a(Loxn;)Lpzq;

    move-result-object v0

    return-object v0
.end method
