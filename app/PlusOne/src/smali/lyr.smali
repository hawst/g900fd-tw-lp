.class public final Llyr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpwg;

.field public b:Ljava/lang/String;

.field public c:Lltc;

.field private d:Lpiy;

.field private e:Lpyx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Loxq;-><init>()V

    .line 116
    iput-object v0, p0, Llyr;->d:Lpiy;

    .line 119
    iput-object v0, p0, Llyr;->a:Lpwg;

    .line 124
    iput-object v0, p0, Llyr;->e:Lpyx;

    .line 127
    iput-object v0, p0, Llyr;->c:Lltc;

    .line 113
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 153
    const/4 v0, 0x0

    .line 154
    iget-object v1, p0, Llyr;->d:Lpiy;

    if-eqz v1, :cond_0

    .line 155
    const/4 v0, 0x1

    iget-object v1, p0, Llyr;->d:Lpiy;

    .line 156
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 158
    :cond_0
    iget-object v1, p0, Llyr;->a:Lpwg;

    if-eqz v1, :cond_1

    .line 159
    const/4 v1, 0x2

    iget-object v2, p0, Llyr;->a:Lpwg;

    .line 160
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_1
    iget-object v1, p0, Llyr;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 163
    const/4 v1, 0x3

    iget-object v2, p0, Llyr;->b:Ljava/lang/String;

    .line 164
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_2
    iget-object v1, p0, Llyr;->e:Lpyx;

    if-eqz v1, :cond_3

    .line 167
    const/4 v1, 0x4

    iget-object v2, p0, Llyr;->e:Lpyx;

    .line 168
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_3
    iget-object v1, p0, Llyr;->c:Lltc;

    if-eqz v1, :cond_4

    .line 171
    const/4 v1, 0x5

    iget-object v2, p0, Llyr;->c:Lltc;

    .line 172
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_4
    iget-object v1, p0, Llyr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    iput v0, p0, Llyr;->ai:I

    .line 176
    return v0
.end method

.method public a(Loxn;)Llyr;
    .locals 2

    .prologue
    .line 184
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 185
    sparse-switch v0, :sswitch_data_0

    .line 189
    iget-object v1, p0, Llyr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 190
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyr;->ah:Ljava/util/List;

    .line 193
    :cond_1
    iget-object v1, p0, Llyr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    :sswitch_0
    return-object p0

    .line 200
    :sswitch_1
    iget-object v0, p0, Llyr;->d:Lpiy;

    if-nez v0, :cond_2

    .line 201
    new-instance v0, Lpiy;

    invoke-direct {v0}, Lpiy;-><init>()V

    iput-object v0, p0, Llyr;->d:Lpiy;

    .line 203
    :cond_2
    iget-object v0, p0, Llyr;->d:Lpiy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 207
    :sswitch_2
    iget-object v0, p0, Llyr;->a:Lpwg;

    if-nez v0, :cond_3

    .line 208
    new-instance v0, Lpwg;

    invoke-direct {v0}, Lpwg;-><init>()V

    iput-object v0, p0, Llyr;->a:Lpwg;

    .line 210
    :cond_3
    iget-object v0, p0, Llyr;->a:Lpwg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 214
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llyr;->b:Ljava/lang/String;

    goto :goto_0

    .line 218
    :sswitch_4
    iget-object v0, p0, Llyr;->e:Lpyx;

    if-nez v0, :cond_4

    .line 219
    new-instance v0, Lpyx;

    invoke-direct {v0}, Lpyx;-><init>()V

    iput-object v0, p0, Llyr;->e:Lpyx;

    .line 221
    :cond_4
    iget-object v0, p0, Llyr;->e:Lpyx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 225
    :sswitch_5
    iget-object v0, p0, Llyr;->c:Lltc;

    if-nez v0, :cond_5

    .line 226
    new-instance v0, Lltc;

    invoke-direct {v0}, Lltc;-><init>()V

    iput-object v0, p0, Llyr;->c:Lltc;

    .line 228
    :cond_5
    iget-object v0, p0, Llyr;->c:Lltc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 185
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Llyr;->d:Lpiy;

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x1

    iget-object v1, p0, Llyr;->d:Lpiy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 135
    :cond_0
    iget-object v0, p0, Llyr;->a:Lpwg;

    if-eqz v0, :cond_1

    .line 136
    const/4 v0, 0x2

    iget-object v1, p0, Llyr;->a:Lpwg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 138
    :cond_1
    iget-object v0, p0, Llyr;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 139
    const/4 v0, 0x3

    iget-object v1, p0, Llyr;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 141
    :cond_2
    iget-object v0, p0, Llyr;->e:Lpyx;

    if-eqz v0, :cond_3

    .line 142
    const/4 v0, 0x4

    iget-object v1, p0, Llyr;->e:Lpyx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 144
    :cond_3
    iget-object v0, p0, Llyr;->c:Lltc;

    if-eqz v0, :cond_4

    .line 145
    const/4 v0, 0x5

    iget-object v1, p0, Llyr;->c:Lltc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 147
    :cond_4
    iget-object v0, p0, Llyr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 149
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0, p1}, Llyr;->a(Loxn;)Llyr;

    move-result-object v0

    return-object v0
.end method
