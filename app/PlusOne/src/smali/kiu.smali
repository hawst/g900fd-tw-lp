.class public final Lkiu;
.super Llol;
.source "PG"

# interfaces
.implements Lkhf;


# instance fields
.field private N:Landroid/net/Uri;

.field private O:Landroid/net/Uri;

.field private P:Lkhe;

.field private Q:Lkhr;

.field private R:Lkiy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Llol;-><init>()V

    .line 37
    new-instance v0, Lkhe;

    iget-object v1, p0, Lkiu;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    iput-object v0, p0, Lkiu;->P:Lkhe;

    .line 48
    return-void
.end method

.method static synthetic a(Lkiu;)Lkiy;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lkiu;->R:Lkiy;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 172
    new-instance v0, Lkhr;

    iget-object v1, p0, Lkiu;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkiu;->Q:Lkhr;

    .line 173
    iget-object v0, p0, Lkiu;->N:Landroid/net/Uri;

    iget-object v1, p0, Lkiu;->O:Landroid/net/Uri;

    invoke-virtual {p0, v0, v1}, Lkiu;->a(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 174
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lkiu;->O:Landroid/net/Uri;

    .line 74
    return-void
.end method

.method public a(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 89
    invoke-virtual {p0}, Lkiu;->d()V

    .line 90
    invoke-virtual {p0}, Lkiu;->e()V

    .line 91
    if-eqz p2, :cond_0

    .line 92
    invoke-virtual {p0, p2}, Lkiu;->c(Landroid/net/Uri;)V

    .line 94
    :cond_0
    if-eqz p1, :cond_1

    .line 95
    invoke-virtual {p0, p1}, Lkiu;->d(Landroid/net/Uri;)V

    .line 97
    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 59
    if-eqz p1, :cond_0

    .line 60
    const-string v0, "privacy_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lkiu;->O:Landroid/net/Uri;

    .line 61
    const-string v0, "terms_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lkiu;->N:Landroid/net/Uri;

    .line 63
    :cond_0
    return-void
.end method

.method public b(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lkiu;->N:Landroid/net/Uri;

    .line 78
    return-void
.end method

.method public c(Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 133
    const v0, 0x7f0a0566

    invoke-virtual {p0, v0}, Lkiu;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 135
    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 137
    iget-object v2, p0, Lkiu;->P:Lkhe;

    iget-object v3, p0, Lkiu;->Q:Lkhr;

    const-string v4, ""

    .line 138
    invoke-virtual {v3, v0, v4, v1}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v0

    .line 137
    invoke-virtual {v2, v0}, Lkhe;->a(Lkhl;)Lkhl;

    move-result-object v0

    .line 139
    const-string v1, "about_privacy_pref_key"

    invoke-virtual {v0, v1}, Lkhl;->d(Ljava/lang/String;)V

    .line 140
    new-instance v1, Lkiw;

    invoke-direct {v1, p0}, Lkiw;-><init>(Lkiu;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 149
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 53
    iget-object v0, p0, Lkiu;->au:Llnh;

    const-class v1, Lkiy;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkiy;

    iput-object v0, p0, Lkiu;->R:Lkiy;

    .line 54
    return-void
.end method

.method public d()V
    .locals 5

    .prologue
    .line 100
    const v0, 0x7f0a0565

    invoke-virtual {p0, v0}, Lkiu;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 101
    const-string v0, "?"

    .line 103
    :try_start_0
    iget-object v2, p0, Lkiu;->at:Llnl;

    .line 104
    invoke-virtual {v2}, Llnl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lkiu;->at:Llnl;

    invoke-virtual {v3}, Llnl;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 105
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    iget-object v2, p0, Lkiu;->P:Lkhe;

    iget-object v3, p0, Lkiu;->Q:Lkhr;

    invoke-virtual {v3, v1, v0}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 111
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public d(Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 152
    const v0, 0x7f0a0567

    invoke-virtual {p0, v0}, Lkiu;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 153
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 154
    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 156
    iget-object v2, p0, Lkiu;->P:Lkhe;

    iget-object v3, p0, Lkiu;->Q:Lkhr;

    const-string v4, ""

    .line 157
    invoke-virtual {v3, v0, v4, v1}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v0

    .line 156
    invoke-virtual {v2, v0}, Lkhe;->a(Lkhl;)Lkhl;

    move-result-object v0

    .line 158
    const-string v1, "about_terms_pref_key"

    invoke-virtual {v0, v1}, Lkhl;->d(Ljava/lang/String;)V

    .line 159
    new-instance v1, Lkix;

    invoke-direct {v1, p0}, Lkix;-><init>(Lkiu;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 168
    return-void
.end method

.method public e()V
    .locals 5

    .prologue
    .line 114
    const v0, 0x7f0a0577

    invoke-virtual {p0, v0}, Lkiu;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 115
    const v1, 0x7f0a0578

    invoke-virtual {p0, v1}, Lkiu;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 116
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lkiu;->at:Llnl;

    const-class v4, Lcom/google/android/libraries/social/licenses/LicenseMenuActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 118
    iget-object v3, p0, Lkiu;->P:Lkhe;

    iget-object v4, p0, Lkiu;->Q:Lkhr;

    .line 119
    invoke-virtual {v4, v0, v1, v2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v0

    .line 118
    invoke-virtual {v3, v0}, Lkhe;->a(Lkhl;)Lkhl;

    move-result-object v0

    .line 120
    const-string v1, "about_license_pref_key"

    invoke-virtual {v0, v1}, Lkhl;->d(Ljava/lang/String;)V

    .line 121
    new-instance v1, Lkiv;

    invoke-direct {v1, p0}, Lkiv;-><init>(Lkiu;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 130
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 68
    const-string v0, "privacy_uri"

    iget-object v1, p0, Lkiu;->O:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    const-string v0, "terms_uri"

    iget-object v1, p0, Lkiu;->N:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 70
    return-void
.end method
