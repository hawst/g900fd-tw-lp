.class public final Loru;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lort;

.field private b:Lorv;

.field private c:Lorw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1183
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1186
    iput-object v0, p0, Loru;->a:Lort;

    .line 1189
    iput-object v0, p0, Loru;->b:Lorv;

    .line 1192
    iput-object v0, p0, Loru;->c:Lorw;

    .line 1183
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1212
    const/4 v0, 0x0

    .line 1213
    iget-object v1, p0, Loru;->a:Lort;

    if-eqz v1, :cond_0

    .line 1214
    const/4 v0, 0x1

    iget-object v1, p0, Loru;->a:Lort;

    .line 1215
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1217
    :cond_0
    iget-object v1, p0, Loru;->b:Lorv;

    if-eqz v1, :cond_1

    .line 1218
    const/4 v1, 0x2

    iget-object v2, p0, Loru;->b:Lorv;

    .line 1219
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1221
    :cond_1
    iget-object v1, p0, Loru;->c:Lorw;

    if-eqz v1, :cond_2

    .line 1222
    const/4 v1, 0x3

    iget-object v2, p0, Loru;->c:Lorw;

    .line 1223
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1225
    :cond_2
    iget-object v1, p0, Loru;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1226
    iput v0, p0, Loru;->ai:I

    .line 1227
    return v0
.end method

.method public a(Loxn;)Loru;
    .locals 2

    .prologue
    .line 1235
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1236
    sparse-switch v0, :sswitch_data_0

    .line 1240
    iget-object v1, p0, Loru;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1241
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loru;->ah:Ljava/util/List;

    .line 1244
    :cond_1
    iget-object v1, p0, Loru;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1246
    :sswitch_0
    return-object p0

    .line 1251
    :sswitch_1
    iget-object v0, p0, Loru;->a:Lort;

    if-nez v0, :cond_2

    .line 1252
    new-instance v0, Lort;

    invoke-direct {v0}, Lort;-><init>()V

    iput-object v0, p0, Loru;->a:Lort;

    .line 1254
    :cond_2
    iget-object v0, p0, Loru;->a:Lort;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1258
    :sswitch_2
    iget-object v0, p0, Loru;->b:Lorv;

    if-nez v0, :cond_3

    .line 1259
    new-instance v0, Lorv;

    invoke-direct {v0}, Lorv;-><init>()V

    iput-object v0, p0, Loru;->b:Lorv;

    .line 1261
    :cond_3
    iget-object v0, p0, Loru;->b:Lorv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1265
    :sswitch_3
    iget-object v0, p0, Loru;->c:Lorw;

    if-nez v0, :cond_4

    .line 1266
    new-instance v0, Lorw;

    invoke-direct {v0}, Lorw;-><init>()V

    iput-object v0, p0, Loru;->c:Lorw;

    .line 1268
    :cond_4
    iget-object v0, p0, Loru;->c:Lorw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1236
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1197
    iget-object v0, p0, Loru;->a:Lort;

    if-eqz v0, :cond_0

    .line 1198
    const/4 v0, 0x1

    iget-object v1, p0, Loru;->a:Lort;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1200
    :cond_0
    iget-object v0, p0, Loru;->b:Lorv;

    if-eqz v0, :cond_1

    .line 1201
    const/4 v0, 0x2

    iget-object v1, p0, Loru;->b:Lorv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1203
    :cond_1
    iget-object v0, p0, Loru;->c:Lorw;

    if-eqz v0, :cond_2

    .line 1204
    const/4 v0, 0x3

    iget-object v1, p0, Loru;->c:Lorw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1206
    :cond_2
    iget-object v0, p0, Loru;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1208
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1179
    invoke-virtual {p0, p1}, Loru;->a(Loxn;)Loru;

    move-result-object v0

    return-object v0
.end method
