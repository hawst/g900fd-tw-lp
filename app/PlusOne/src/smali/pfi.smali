.class public final Lpfi;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpfi;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    new-array v0, v0, [Lpfi;

    sput-object v0, Lpfi;->a:[Lpfi;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 203
    const/4 v0, 0x0

    .line 204
    iget-object v1, p0, Lpfi;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 205
    const/4 v0, 0x1

    iget-object v1, p0, Lpfi;->b:Ljava/lang/String;

    .line 206
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 208
    :cond_0
    iget-object v1, p0, Lpfi;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 209
    const/4 v1, 0x2

    iget-object v2, p0, Lpfi;->c:Ljava/lang/Long;

    .line 210
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_1
    iget-object v1, p0, Lpfi;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 213
    const/4 v1, 0x3

    iget-object v2, p0, Lpfi;->d:Ljava/lang/String;

    .line 214
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_2
    iget-object v1, p0, Lpfi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    iput v0, p0, Lpfi;->ai:I

    .line 218
    return v0
.end method

.method public a(Loxn;)Lpfi;
    .locals 2

    .prologue
    .line 226
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 227
    sparse-switch v0, :sswitch_data_0

    .line 231
    iget-object v1, p0, Lpfi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 232
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpfi;->ah:Ljava/util/List;

    .line 235
    :cond_1
    iget-object v1, p0, Lpfi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    :sswitch_0
    return-object p0

    .line 242
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfi;->b:Ljava/lang/String;

    goto :goto_0

    .line 246
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpfi;->c:Ljava/lang/Long;

    goto :goto_0

    .line 250
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfi;->d:Ljava/lang/String;

    goto :goto_0

    .line 227
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 188
    iget-object v0, p0, Lpfi;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 189
    const/4 v0, 0x1

    iget-object v1, p0, Lpfi;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 191
    :cond_0
    iget-object v0, p0, Lpfi;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 192
    const/4 v0, 0x2

    iget-object v1, p0, Lpfi;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 194
    :cond_1
    iget-object v0, p0, Lpfi;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 195
    const/4 v0, 0x3

    iget-object v1, p0, Lpfi;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 197
    :cond_2
    iget-object v0, p0, Lpfi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 199
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lpfi;->a(Loxn;)Lpfi;

    move-result-object v0

    return-object v0
.end method
