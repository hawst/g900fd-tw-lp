.class final Lekc;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private a:[I

.field private b:[Lois;

.field private synthetic c:Lejy;


# direct methods
.method constructor <init>(Lejy;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lekc;->c:Lejy;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Lois;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lekc;->b:[Lois;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a([Lois;)V
    .locals 4

    .prologue
    const v1, 0x7f0f001a

    .line 262
    iput-object p1, p0, Lekc;->b:[Lois;

    .line 263
    iget-object v0, p0, Lekc;->c:Lejy;

    invoke-static {v0}, Lejy;->a(Lejy;)Llnl;

    move-result-object v0

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lekc;->a:[I

    .line 265
    iget-object v0, p0, Lekc;->c:Lejy;

    invoke-static {v0}, Lejy;->b(Lejy;)Llnl;

    move-result-object v0

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 267
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->length()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lekc;->a:[I

    .line 268
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 269
    iget-object v2, p0, Lekc;->a:[I

    const v3, 0x7f0b00de

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    aput v3, v2, v0

    .line 268
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 271
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 272
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lekc;->b:[Lois;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lekc;->b:[Lois;

    array-length v0, v0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 256
    invoke-virtual {p0, p1}, Lekc;->a(I)Lois;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 296
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const v9, 0x7f1000ae

    const/4 v5, -0x2

    const/4 v8, 0x1

    .line 302
    invoke-virtual {p0, p1}, Lekc;->a(I)Lois;

    move-result-object v3

    .line 305
    if-nez p2, :cond_2

    .line 306
    iget-object v0, p0, Lekc;->c:Lejy;

    invoke-virtual {v0, v2}, Lejy;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040061

    .line 307
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;

    .line 309
    const v1, 0x7f1001e8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 310
    iget-object v2, p0, Lekc;->c:Lejy;

    invoke-virtual {v2}, Lejy;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0d03a8

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 311
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 315
    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 316
    const/4 v2, 0x0

    :goto_0
    const/4 v5, 0x6

    if-ge v2, v5, :cond_0

    .line 317
    new-instance v5, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v6, p0, Lekc;->c:Lejy;

    invoke-static {v6}, Lejy;->c(Lejy;)Llnl;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    .line 318
    invoke-virtual {v5, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 319
    invoke-virtual {v5, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 320
    invoke-virtual {v5, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 321
    invoke-virtual {v5, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 322
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 323
    new-instance v6, Lhmi;

    iget-object v7, p0, Lekc;->c:Lejy;

    invoke-direct {v6, v7}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    new-instance v6, Lhmk;

    sget-object v7, Long;->i:Lhmn;

    invoke-direct {v6, v7}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v5, v6}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 316
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    move-object p2, v0

    .line 332
    :goto_1
    invoke-virtual {p2, v3}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->a(Lois;)V

    .line 334
    iget-object v0, p0, Lekc;->a:[I

    iget-object v1, p0, Lekc;->a:[I

    array-length v1, v1

    rem-int v1, p1, v1

    aget v0, v0, v1

    .line 335
    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->a(I)V

    .line 337
    const v0, 0x7f1001ec

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 338
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 339
    const v1, 0x7f1000ad

    iget v2, v3, Lois;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 340
    const v1, 0x7f1000af

    iget-object v2, v3, Lois;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 341
    new-instance v1, Lhmi;

    iget-object v2, p0, Lekc;->c:Lejy;

    invoke-direct {v1, v2}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 342
    new-instance v1, Lhmk;

    sget-object v2, Lona;->s:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 345
    const v0, 0x7f1001ed

    .line 346
    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    .line 347
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setTag(ILjava/lang/Object;)V

    .line 348
    iget-object v1, p0, Lekc;->c:Lejy;

    iget-object v2, p0, Lekc;->c:Lejy;

    invoke-static {v2, v3}, Lejy;->a(Lejy;Lois;)Z

    move-result v2

    invoke-static {v1, v0, v2}, Lejy;->a(Lejy;Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Z)V

    .line 349
    new-instance v1, Lhmi;

    iget-object v2, p0, Lekc;->c:Lejy;

    invoke-direct {v1, v2}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 352
    iget-object v0, p0, Lekc;->c:Lejy;

    invoke-static {v0}, Lejy;->d(Lejy;)I

    move-result v0

    if-le p1, v0, :cond_1

    .line 353
    iget-object v0, p0, Lekc;->c:Lejy;

    invoke-static {v0, p1}, Lejy;->a(Lejy;I)I

    .line 354
    new-instance v0, Lkqt;

    sget-object v1, Lona;->d:Lhmn;

    iget v2, v3, Lois;->b:I

    invoke-direct {v0, v1, v2, p1}, Lkqt;-><init>(Lhmn;II)V

    invoke-static {p2, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 356
    invoke-static {p2}, Lhmc;->a(Landroid/view/View;)V

    .line 359
    :cond_1
    return-object p2

    .line 329
    :cond_2
    check-cast p2, Lcom/google/android/apps/plus/views/CelebrityCategoryCardView;

    goto/16 :goto_1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lekc;->b:[Lois;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lekc;->b:[Lois;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    return v0
.end method
