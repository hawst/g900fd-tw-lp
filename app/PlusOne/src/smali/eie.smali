.class final Leie;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhnv;


# instance fields
.field private synthetic a:Lehu;


# direct methods
.method constructor <init>(Lehu;)V
    .locals 0

    .prologue
    .line 564
    iput-object p1, p0, Leie;->a:Lehu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 566
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "activity_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lehu;->a(Lehu;Ljava/lang/String;)Ljava/lang/String;

    .line 567
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "square_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lehu;->b(Lehu;Ljava/lang/String;)Ljava/lang/String;

    .line 568
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "square_membership"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lehu;->a(Lehu;I)I

    .line 570
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "square_joinability"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lehu;->b(Lehu;I)I

    .line 572
    const-string v0, "context_specific_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 573
    if-eqz v0, :cond_0

    .line 574
    iget-object v1, p0, Leie;->a:Lehu;

    invoke-static {v0}, Lkzp;->a([B)Lkzp;

    move-result-object v0

    invoke-static {v1, v0}, Lehu;->a(Lehu;Lkzp;)Lkzp;

    .line 576
    :cond_0
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "restrict_to_domain"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lehu;->a(Lehu;Z)Z

    .line 577
    iget-object v0, p0, Leie;->a:Lehu;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lehu;->a(Lehu;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 578
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "show_keyboard"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lehu;->b(Lehu;Z)Z

    .line 579
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "host_mode"

    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lehu;->c(Lehu;I)I

    .line 580
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "force_full_bleed"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lehu;->c(Lehu;Z)Z

    .line 581
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "max_span"

    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lehu;->d(Lehu;I)I

    .line 582
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "scroll_to_last_comment"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lehu;->d(Lehu;Z)Z

    .line 583
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "promoted_post_data"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lehu;->a(Lehu;[B)[B

    .line 585
    iget-object v0, p0, Leie;->a:Lehu;

    const-string v1, "com.google.android.apps.plus.HIDE_ACTION_BAR_LOGO"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lehu;->e(Lehu;Z)Z

    .line 586
    return-void
.end method
