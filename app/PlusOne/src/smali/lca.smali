.class public final Llca;
.super Lllq;
.source "PG"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llbz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lllq;-><init>()V

    .line 32
    return-void
.end method

.method public constructor <init>(Lnrr;)V
    .locals 14

    .prologue
    const/4 v8, 0x0

    .line 53
    invoke-direct {p0}, Lllq;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lnrr;->b:[Lnsq;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Llca;->a:Ljava/util/ArrayList;

    move v12, v8

    .line 55
    :goto_0
    iget-object v0, p1, Lnrr;->b:[Lnsq;

    array-length v0, v0

    if-ge v12, v0, :cond_3

    .line 56
    iget-object v0, p1, Lnrr;->b:[Lnsq;

    aget-object v11, v0, v12

    .line 57
    iget-object v0, v11, Lnsq;->b:Lnsr;

    invoke-static {v0}, Llca;->a(Lnsr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, v11, Lnsq;->b:Lnsr;

    iget-object v10, v0, Lnsr;->b:Lnsb;

    .line 61
    iget-object v13, p0, Llca;->a:Ljava/util/ArrayList;

    new-instance v0, Llbz;

    iget-object v1, v10, Lnsb;->a:Ljava/lang/String;

    iget-object v2, v10, Lnsb;->b:Lnsc;

    iget-object v2, v2, Lnsc;->a:Ljava/lang/String;

    iget-object v3, v10, Lnsb;->b:Lnsc;

    iget-object v3, v3, Lnsc;->b:Ljava/lang/String;

    iget-object v4, v10, Lnsb;->b:Lnsc;

    iget-object v4, v4, Lnsc;->c:Ljava/lang/String;

    .line 62
    invoke-static {v4}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v11, Lnsq;->b:Lnsr;

    iget-object v5, v5, Lnsr;->d:Lnsv;

    if-nez v5, :cond_1

    move v5, v8

    .line 64
    :goto_1
    const/4 v6, 0x0

    iget-object v7, v11, Lnsq;->c:Ljava/lang/String;

    iget-object v9, v10, Lnsb;->b:Lnsc;

    iget-object v9, v9, Lnsc;->g:Ljava/lang/String;

    if-eqz v9, :cond_2

    const/4 v9, 0x1

    :goto_2
    iget v10, v10, Lnsb;->d:I

    iget-object v11, v11, Lnsq;->b:Lnsr;

    iget v11, v11, Lnsr;->f:I

    invoke-direct/range {v0 .. v11}, Llbz;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZII)V

    .line 61
    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_0
    add-int/lit8 v0, v12, 0x1

    move v12, v0

    goto :goto_0

    .line 62
    :cond_1
    iget-object v5, v11, Lnsq;->b:Lnsr;

    iget-object v5, v5, Lnsr;->d:Lnsv;

    iget-object v5, v5, Lnsv;->a:Ljava/lang/Integer;

    .line 64
    invoke-static {v5}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v5

    goto :goto_1

    :cond_2
    move v9, v8

    goto :goto_2

    .line 69
    :cond_3
    return-void
.end method

.method public constructor <init>(Lnrv;)V
    .locals 14

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 34
    invoke-direct {p0}, Lllq;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lnrv;->b:[Lnru;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Llca;->a:Ljava/util/ArrayList;

    move v12, v8

    .line 36
    :goto_0
    iget-object v0, p1, Lnrv;->b:[Lnru;

    array-length v0, v0

    if-ge v12, v0, :cond_4

    .line 37
    iget-object v0, p1, Lnrv;->b:[Lnru;

    aget-object v11, v0, v12

    .line 38
    iget-object v0, v11, Lnru;->b:Lnsr;

    invoke-static {v0}, Llca;->a(Lnsr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, v11, Lnru;->b:Lnsr;

    iget-object v10, v0, Lnsr;->b:Lnsb;

    .line 42
    iget-object v13, p0, Llca;->a:Ljava/util/ArrayList;

    new-instance v0, Llbz;

    iget-object v1, v10, Lnsb;->a:Ljava/lang/String;

    iget-object v2, v10, Lnsb;->b:Lnsc;

    iget-object v2, v2, Lnsc;->a:Ljava/lang/String;

    iget-object v3, v10, Lnsb;->b:Lnsc;

    iget-object v3, v3, Lnsc;->b:Ljava/lang/String;

    iget-object v4, v10, Lnsb;->b:Lnsc;

    iget-object v4, v4, Lnsc;->c:Ljava/lang/String;

    .line 43
    invoke-static {v4}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v11, Lnru;->b:Lnsr;

    iget-object v5, v5, Lnsr;->d:Lnsv;

    if-nez v5, :cond_1

    move v5, v8

    .line 45
    :goto_1
    iget-object v6, v11, Lnru;->c:[Lnsh;

    if-eqz v6, :cond_2

    iget-object v6, v11, Lnru;->c:[Lnsh;

    array-length v6, v6

    if-lez v6, :cond_2

    iget-object v6, v11, Lnru;->c:[Lnsh;

    aget-object v6, v6, v8

    iget-object v6, v6, Lnsh;->d:Ljava/lang/String;

    :goto_2
    iget-object v9, v10, Lnsb;->b:Lnsc;

    iget-object v9, v9, Lnsc;->g:Ljava/lang/String;

    if-eqz v9, :cond_3

    const/4 v9, 0x1

    :goto_3
    iget v10, v10, Lnsb;->d:I

    iget-object v11, v11, Lnru;->b:Lnsr;

    iget v11, v11, Lnsr;->f:I

    invoke-direct/range {v0 .. v11}, Llbz;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZII)V

    .line 42
    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    :cond_0
    add-int/lit8 v0, v12, 0x1

    move v12, v0

    goto :goto_0

    .line 43
    :cond_1
    iget-object v5, v11, Lnru;->b:Lnsr;

    iget-object v5, v5, Lnsr;->d:Lnsv;

    iget-object v5, v5, Lnsv;->a:Ljava/lang/Integer;

    .line 45
    invoke-static {v5}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v5

    goto :goto_1

    :cond_2
    move-object v6, v7

    goto :goto_2

    :cond_3
    move v9, v8

    goto :goto_3

    .line 51
    :cond_4
    return-void
.end method

.method public static a([B)Llca;
    .locals 18

    .prologue
    .line 100
    if-nez p0, :cond_0

    .line 101
    const/4 v1, 0x0

    .line 115
    :goto_0
    return-object v1

    .line 104
    :cond_0
    invoke-static/range {p0 .. p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v15

    .line 105
    new-instance v13, Llca;

    invoke-direct {v13}, Llca;-><init>()V

    .line 107
    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v16

    .line 109
    new-instance v1, Ljava/util/ArrayList;

    move/from16 v0, v16

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, v13, Llca;->a:Ljava/util/ArrayList;

    .line 111
    const/4 v1, 0x0

    move v14, v1

    :goto_1
    move/from16 v0, v16

    if-ge v14, v0, :cond_3

    .line 112
    iget-object v0, v13, Llca;->a:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-static {v15}, Llbz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v15}, Llbz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v15}, Llbz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v15}, Llbz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    invoke-static {v15}, Llbz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v15}, Llbz;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    const/4 v9, 0x1

    if-ne v1, v9, :cond_1

    const/4 v9, 0x1

    :goto_2
    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    const/4 v10, 0x1

    if-ne v1, v10, :cond_2

    const/4 v10, 0x1

    :goto_3
    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v11

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    new-instance v1, Llbz;

    invoke-direct/range {v1 .. v12}, Llbz;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZII)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    goto :goto_1

    .line 112
    :cond_1
    const/4 v9, 0x0

    goto :goto_2

    :cond_2
    const/4 v10, 0x0

    goto :goto_3

    :cond_3
    move-object v1, v13

    .line 115
    goto :goto_0
.end method

.method private static a(Lnsr;)Z
    .locals 1

    .prologue
    .line 151
    if-eqz p0, :cond_0

    iget-object v0, p0, Lnsr;->b:Lnsb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnsr;->b:Lnsb;

    iget-object v0, v0, Lnsb;->b:Lnsc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Llca;)[B
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x100

    invoke-direct {v4, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 82
    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 84
    :try_start_0
    invoke-virtual {p0}, Llca;->a()I

    move-result v6

    .line 87
    invoke-virtual {v5, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v3, v2

    .line 88
    :goto_0
    if-ge v3, v6, :cond_2

    .line 89
    invoke-virtual {p0, v3}, Llca;->a(I)Llbz;

    move-result-object v7

    iget-object v0, v7, Llbz;->a:Ljava/lang/String;

    invoke-static {v5, v0}, Llbz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, v7, Llbz;->b:Ljava/lang/String;

    invoke-static {v5, v0}, Llbz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, v7, Llbz;->c:Ljava/lang/String;

    invoke-static {v5, v0}, Llbz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, v7, Llbz;->d:Ljava/lang/String;

    invoke-static {v5, v0}, Llbz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget v0, v7, Llbz;->e:I

    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, v7, Llbz;->f:Ljava/lang/String;

    invoke-static {v5, v0}, Llbz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, v7, Llbz;->g:Ljava/lang/String;

    invoke-static {v5, v0}, Llbz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-boolean v0, v7, Llbz;->h:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    iget-boolean v0, v7, Llbz;->i:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    iget v0, v7, Llbz;->j:I

    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v0, v7, Llbz;->k:I

    invoke-virtual {v5, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 88
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 89
    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_2

    .line 91
    :cond_2
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 93
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V

    .line 96
    return-object v0

    .line 93
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Llca;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Llbz;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Llca;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llbz;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 127
    iget-object v0, p0, Llca;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    .line 128
    iget-object v0, p0, Llca;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llbz;

    .line 129
    invoke-virtual {v0}, Llbz;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 130
    invoke-virtual {v0}, Llbz;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 131
    invoke-virtual {v0, v1}, Llbz;->a(Z)V

    move v0, v1

    .line 135
    :goto_1
    return v0

    .line 127
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 135
    goto :goto_1
.end method

.method public b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Llbz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Llca;->a:Ljava/util/ArrayList;

    return-object v0
.end method
