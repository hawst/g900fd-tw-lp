.class final Lhnl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field private a:Lhnj;

.field private synthetic b:Lhni;


# direct methods
.method public constructor <init>(Lhni;Lhnj;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lhnl;->b:Lhni;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    iput-object p2, p0, Lhnl;->a:Lhnj;

    .line 200
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 220
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 18

    .prologue
    .line 204
    move-object/from16 v0, p0

    iget-object v6, v0, Lhnl;->b:Lhni;

    move-object/from16 v0, p0

    iget-object v7, v0, Lhnl;->a:Lhnj;

    :try_start_0
    iget-object v2, v7, Lhnj;->c:Lorg/json/JSONObject;

    const-string v3, "animations"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    const/4 v2, 0x0

    move v5, v2

    :goto_0
    if-ge v5, v9, :cond_4

    invoke-virtual {v8, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    iget-object v3, v7, Lhnj;->d:Ljava/util/Map;

    const-string v4, "id"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_3

    const-string v3, "initialValues"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "initialValues"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    invoke-virtual {v11}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, v6, Lhni;->c:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v6, Lhni;->b:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/util/Property;

    iget-object v4, v6, Lhni;->c:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    if-nez v4, :cond_1

    const/4 v13, 0x0

    aget-object v13, v3, v13

    invoke-virtual {v7, v13}, Lhnj;->a(Landroid/util/Property;)Z

    move-result v13

    if-eqz v13, :cond_1

    const/4 v4, 0x0

    aget-object v4, v3, v4

    const/4 v13, 0x0

    aget-object v3, v3, v13

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v14

    invoke-static {v7, v3, v14, v15}, Lhni;->a(Lhnj;Landroid/util/Property;D)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v4, v10, v2}, Landroid/util/Property;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    new-instance v3, Lhnk;

    invoke-direct {v3, v2}, Lhnk;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :cond_1
    :try_start_1
    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    const/4 v2, 0x0

    :goto_2
    array-length v14, v4

    if-ge v2, v14, :cond_0

    aget-object v14, v3, v2

    invoke-virtual {v7, v14}, Lhnj;->a(Landroid/util/Property;)Z

    move-result v14

    if-eqz v14, :cond_2

    aget-object v14, v4, v2

    invoke-virtual {v13, v14}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    aget-object v14, v3, v2

    aget-object v15, v3, v2

    aget-object v16, v4, v2

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-static {v7, v15, v0, v1}, Lhni;->a(Lhnj;Landroid/util/Property;D)F

    move-result v15

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v15

    invoke-virtual {v14, v10, v15}, Landroid/util/Property;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_0

    :cond_4
    return-void
.end method
