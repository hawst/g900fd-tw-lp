.class public final Llmo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lllx;


# instance fields
.field private final a:Lllx;

.field private final b:Lllx;

.field private final c:I


# direct methods
.method public constructor <init>(Lllx;Lllx;)V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, "firstChunkStore cannot be null"

    .line 18
    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllx;

    iput-object v0, p0, Llmo;->a:Lllx;

    .line 19
    const-string v0, "restChunkStore cannot be null"

    .line 20
    invoke-static {p2, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllx;

    iput-object v0, p0, Llmo;->b:Lllx;

    .line 22
    invoke-interface {p1}, Lllx;->a()I

    move-result v0

    invoke-interface {p2}, Lllx;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "firstChunkStore and restChunkStore must use the same chunk size"

    .line 21
    invoke-static {v0, v1}, Llsk;->a(ZLjava/lang/Object;)V

    .line 24
    invoke-interface {p1}, Lllx;->a()I

    move-result v0

    iput v0, p0, Llmo;->c:I

    .line 25
    return-void

    .line 22
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Llmo;->c:I

    return v0
.end method

.method public a(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, Llmo;->a:Lllx;

    invoke-interface {v0, p1}, Lllx;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 83
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 86
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Llmo;->b:Lllx;

    invoke-interface {v0, p1}, Lllx;->a(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I[B)V
    .locals 1

    .prologue
    .line 29
    if-nez p2, :cond_0

    .line 30
    iget-object v0, p0, Llmo;->a:Lllx;

    invoke-interface {v0, p1, p2, p3}, Lllx;->a(Ljava/lang/String;I[B)V

    .line 34
    :goto_0
    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Llmo;->b:Lllx;

    invoke-interface {v0, p1, p2, p3}, Lllx;->a(Ljava/lang/String;I[B)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I[BJ)V
    .locals 6

    .prologue
    .line 38
    if-nez p2, :cond_0

    .line 39
    iget-object v0, p0, Llmo;->a:Lllx;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lllx;->a(Ljava/lang/String;I[BJ)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Llmo;->b:Lllx;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lllx;->a(Ljava/lang/String;I[BJ)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 62
    if-nez p2, :cond_0

    .line 63
    iget-object v0, p0, Llmo;->a:Lllx;

    invoke-interface {v0, p1, p2}, Lllx;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 65
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Llmo;->b:Lllx;

    invoke-interface {v0, p1, p2}, Lllx;->a(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;I[B)I
    .locals 4

    .prologue
    .line 47
    if-nez p2, :cond_0

    .line 48
    iget-object v0, p0, Llmo;->a:Lllx;

    invoke-interface {v0, p1, p2, p3}, Lllx;->b(Ljava/lang/String;I[B)I

    move-result v0

    .line 56
    :goto_0
    return v0

    .line 51
    :cond_0
    iget-object v0, p0, Llmo;->a:Lllx;

    invoke-interface {v0, p1}, Lllx;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 52
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    iget v2, p0, Llmo;->c:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 54
    const/4 v0, -0x1

    goto :goto_0

    .line 56
    :cond_1
    iget-object v0, p0, Llmo;->b:Lllx;

    invoke-interface {v0, p1, p2, p3}, Lllx;->b(Ljava/lang/String;I[B)I

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Llmo;->a:Lllx;

    invoke-interface {v0}, Lllx;->b()V

    .line 77
    iget-object v0, p0, Llmo;->b:Lllx;

    invoke-interface {v0}, Lllx;->b()V

    .line 78
    return-void
.end method
