.class public final Lnkc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnkc;


# instance fields
.field public b:Lnkb;

.field public c:Lnkb;

.field private d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 740
    const/4 v0, 0x0

    new-array v0, v0, [Lnkc;

    sput-object v0, Lnkc;->a:[Lnkc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 741
    invoke-direct {p0}, Loxq;-><init>()V

    .line 744
    iput-object v0, p0, Lnkc;->b:Lnkb;

    .line 747
    iput-object v0, p0, Lnkc;->c:Lnkb;

    .line 741
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 769
    const/4 v0, 0x0

    .line 770
    iget-object v1, p0, Lnkc;->b:Lnkb;

    if-eqz v1, :cond_0

    .line 771
    const/4 v0, 0x1

    iget-object v1, p0, Lnkc;->b:Lnkb;

    .line 772
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 774
    :cond_0
    iget-object v1, p0, Lnkc;->c:Lnkb;

    if-eqz v1, :cond_1

    .line 775
    const/4 v1, 0x2

    iget-object v2, p0, Lnkc;->c:Lnkb;

    .line 776
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 778
    :cond_1
    iget-object v1, p0, Lnkc;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 779
    const/4 v1, 0x3

    iget-object v2, p0, Lnkc;->d:Ljava/lang/Boolean;

    .line 780
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 782
    :cond_2
    iget-object v1, p0, Lnkc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 783
    iput v0, p0, Lnkc;->ai:I

    .line 784
    return v0
.end method

.method public a(Loxn;)Lnkc;
    .locals 2

    .prologue
    .line 792
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 793
    sparse-switch v0, :sswitch_data_0

    .line 797
    iget-object v1, p0, Lnkc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 798
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnkc;->ah:Ljava/util/List;

    .line 801
    :cond_1
    iget-object v1, p0, Lnkc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 803
    :sswitch_0
    return-object p0

    .line 808
    :sswitch_1
    iget-object v0, p0, Lnkc;->b:Lnkb;

    if-nez v0, :cond_2

    .line 809
    new-instance v0, Lnkb;

    invoke-direct {v0}, Lnkb;-><init>()V

    iput-object v0, p0, Lnkc;->b:Lnkb;

    .line 811
    :cond_2
    iget-object v0, p0, Lnkc;->b:Lnkb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 815
    :sswitch_2
    iget-object v0, p0, Lnkc;->c:Lnkb;

    if-nez v0, :cond_3

    .line 816
    new-instance v0, Lnkb;

    invoke-direct {v0}, Lnkb;-><init>()V

    iput-object v0, p0, Lnkc;->c:Lnkb;

    .line 818
    :cond_3
    iget-object v0, p0, Lnkc;->c:Lnkb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 822
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnkc;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 793
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 754
    iget-object v0, p0, Lnkc;->b:Lnkb;

    if-eqz v0, :cond_0

    .line 755
    const/4 v0, 0x1

    iget-object v1, p0, Lnkc;->b:Lnkb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 757
    :cond_0
    iget-object v0, p0, Lnkc;->c:Lnkb;

    if-eqz v0, :cond_1

    .line 758
    const/4 v0, 0x2

    iget-object v1, p0, Lnkc;->c:Lnkb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 760
    :cond_1
    iget-object v0, p0, Lnkc;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 761
    const/4 v0, 0x3

    iget-object v1, p0, Lnkc;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 763
    :cond_2
    iget-object v0, p0, Lnkc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 765
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 737
    invoke-virtual {p0, p1}, Lnkc;->a(Loxn;)Lnkc;

    move-result-object v0

    return-object v0
.end method
