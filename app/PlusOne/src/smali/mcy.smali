.class public final Lmcy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lntc;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15469
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15472
    iput-object v0, p0, Lmcy;->apiHeader:Llyq;

    .line 15475
    iput-object v0, p0, Lmcy;->a:Lntc;

    .line 15469
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 15492
    const/4 v0, 0x0

    .line 15493
    iget-object v1, p0, Lmcy;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 15494
    const/4 v0, 0x1

    iget-object v1, p0, Lmcy;->apiHeader:Llyq;

    .line 15495
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15497
    :cond_0
    iget-object v1, p0, Lmcy;->a:Lntc;

    if-eqz v1, :cond_1

    .line 15498
    const/4 v1, 0x2

    iget-object v2, p0, Lmcy;->a:Lntc;

    .line 15499
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15501
    :cond_1
    iget-object v1, p0, Lmcy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15502
    iput v0, p0, Lmcy;->ai:I

    .line 15503
    return v0
.end method

.method public a(Loxn;)Lmcy;
    .locals 2

    .prologue
    .line 15511
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 15512
    sparse-switch v0, :sswitch_data_0

    .line 15516
    iget-object v1, p0, Lmcy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 15517
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcy;->ah:Ljava/util/List;

    .line 15520
    :cond_1
    iget-object v1, p0, Lmcy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15522
    :sswitch_0
    return-object p0

    .line 15527
    :sswitch_1
    iget-object v0, p0, Lmcy;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 15528
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmcy;->apiHeader:Llyq;

    .line 15530
    :cond_2
    iget-object v0, p0, Lmcy;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15534
    :sswitch_2
    iget-object v0, p0, Lmcy;->a:Lntc;

    if-nez v0, :cond_3

    .line 15535
    new-instance v0, Lntc;

    invoke-direct {v0}, Lntc;-><init>()V

    iput-object v0, p0, Lmcy;->a:Lntc;

    .line 15537
    :cond_3
    iget-object v0, p0, Lmcy;->a:Lntc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15512
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 15480
    iget-object v0, p0, Lmcy;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 15481
    const/4 v0, 0x1

    iget-object v1, p0, Lmcy;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15483
    :cond_0
    iget-object v0, p0, Lmcy;->a:Lntc;

    if-eqz v0, :cond_1

    .line 15484
    const/4 v0, 0x2

    iget-object v1, p0, Lmcy;->a:Lntc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15486
    :cond_1
    iget-object v0, p0, Lmcy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 15488
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 15465
    invoke-virtual {p0, p1}, Lmcy;->a(Loxn;)Lmcy;

    move-result-object v0

    return-object v0
.end method
