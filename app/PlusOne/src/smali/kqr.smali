.class public final Lkqr;
.super Lhmk;
.source "PG"


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:I


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-super {p0, p1}, Lhmk;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    check-cast p1, Lkqr;

    .line 54
    iget v1, p0, Lkqr;->c:I

    iget v2, p1, Lkqr;->c:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lkqr;->b:Ljava/lang/String;

    iget-object v2, p1, Lkqr;->b:Ljava/lang/String;

    .line 55
    invoke-static {v1, v2}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 57
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lkqr;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lkqr;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-super {p0}, Lhmk;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
