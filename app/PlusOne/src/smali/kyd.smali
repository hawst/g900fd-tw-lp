.class public final Lkyd;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Lkfo;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 38
    const-string v0, "SetSquarePinnedStateTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 39
    iput p2, p0, Lkyd;->a:I

    .line 40
    new-instance v0, Lkfo;

    iget v1, p0, Lkyd;->a:I

    invoke-direct {v0, p1, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkyd;->b:Lkfo;

    .line 41
    iput-object p3, p0, Lkyd;->c:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lkyd;->d:Ljava/lang/String;

    .line 43
    iput p5, p0, Lkyd;->e:I

    .line 44
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 7

    .prologue
    const v6, 0x7f0a058e

    .line 48
    new-instance v0, Lkyc;

    invoke-virtual {p0}, Lkyd;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lkyd;->b:Lkfo;

    iget-object v3, p0, Lkyd;->c:Ljava/lang/String;

    iget-object v4, p0, Lkyd;->d:Ljava/lang/String;

    iget v5, p0, Lkyd;->e:I

    invoke-direct/range {v0 .. v5}, Lkyc;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 50
    invoke-virtual {v0}, Lkyc;->l()V

    .line 51
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    .line 52
    invoke-virtual {v0}, Lkyc;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lkyd;->e:I

    packed-switch v0, :pswitch_data_0

    move v0, v6

    :goto_0
    invoke-virtual {p0}, Lkyd;->f()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 53
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "pinned_state"

    iget v3, p0, Lkyd;->e:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    return-object v1

    .line 52
    :pswitch_0
    const v0, 0x7f0a0460

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0a0461

    goto :goto_0

    :cond_0
    iget v0, p0, Lkyd;->e:I

    packed-switch v0, :pswitch_data_1

    :goto_2
    invoke-virtual {p0}, Lkyd;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const v6, 0x7f0a0462

    goto :goto_2

    :pswitch_3
    const v6, 0x7f0a0463

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lkyd;->e:I

    packed-switch v0, :pswitch_data_0

    .line 68
    const-string v0, ""

    .line 70
    :goto_0
    return-object v0

    .line 62
    :pswitch_0
    const v0, 0x7f0a045e

    .line 70
    :goto_1
    invoke-virtual {p0}, Lkyd;->f()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 65
    :pswitch_1
    const v0, 0x7f0a045f

    .line 66
    goto :goto_1

    .line 60
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
