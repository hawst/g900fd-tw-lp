.class public final Lfwq;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:Z

.field private static b:I

.field private static c:I

.field private static d:I

.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:I

.field private static k:F

.field private static l:I

.field private static m:I

.field private static n:Landroid/graphics/drawable/Drawable;


# instance fields
.field private o:Landroid/content/Context;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Landroid/widget/TextView;

.field private s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private u:I

.field private v:I

.field private w:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfwq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfwq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    invoke-direct {p0, p1}, Lfwq;->a(Landroid/content/Context;)V

    .line 72
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 112
    iput-object p1, p0, Lfwq;->o:Landroid/content/Context;

    .line 114
    sget-boolean v0, Lfwq;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0246

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lfwq;->b:I

    const v2, 0x7f0d0247

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lfwq;->c:I

    const v2, 0x7f0d0248

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lfwq;->e:I

    const v2, 0x7f0d0249

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lfwq;->f:I

    sget v2, Lfwq;->b:I

    sget v3, Lfwq;->c:I

    add-int/2addr v2, v3

    sput v2, Lfwq;->g:I

    sget v2, Lfwq;->e:I

    sget v3, Lfwq;->f:I

    add-int/2addr v2, v3

    sput v2, Lfwq;->h:I

    const v2, 0x7f0d0339

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lfwq;->d:I

    const v2, 0x7f0d033b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lfwq;->i:I

    const v2, 0x7f0d033a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lfwq;->j:I

    const v2, 0x7f0d024d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sput v2, Lfwq;->k:F

    const v2, 0x7f0d033c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lfwq;->l:I

    const v2, 0x7f0b00d6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lfwq;->m:I

    const v2, 0x7f020067

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfwq;->n:Landroid/graphics/drawable/Drawable;

    sput-boolean v5, Lfwq;->a:Z

    .line 116
    :cond_0
    invoke-virtual {p0}, Lfwq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020416

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfwq;->w:Landroid/graphics/drawable/Drawable;

    .line 118
    iget-object v0, p0, Lfwq;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 120
    sget-object v0, Lfwq;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lfwq;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 121
    sget v0, Lfwq;->b:I

    sget v2, Lfwq;->d:I

    add-int/2addr v0, v2

    sget v2, Lfwq;->e:I

    sget v3, Lfwq;->d:I

    add-int/2addr v2, v3

    sget v3, Lfwq;->c:I

    sget v4, Lfwq;->d:I

    add-int/2addr v3, v4

    sget v4, Lfwq;->f:I

    invoke-virtual {p0, v0, v2, v3, v4}, Lfwq;->setPadding(IIII)V

    .line 126
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 127
    iget-object v0, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 128
    iget-object v0, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setId(I)V

    .line 129
    iget-object v0, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 130
    iget-object v0, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lfwq;->addView(Landroid/view/View;)V

    .line 132
    new-array v0, v6, [Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    move v0, v1

    .line 133
    :goto_0
    if-ge v0, v6, :cond_1

    .line 134
    iget-object v2, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    new-instance v3, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v3, p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    aput-object v3, v2, v0

    .line 135
    iget-object v2, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 136
    iget-object v2, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v2, v2, v0

    add-int/lit8 v3, v0, 0x65

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setId(I)V

    .line 137
    iget-object v2, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 139
    iget-object v2, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Lfwq;->addView(Landroid/view/View;)V

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_1
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    .line 143
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    sget v2, Lfwq;->k:F

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 144
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    sget v1, Lfwq;->m:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 145
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLines(I)V

    .line 146
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 147
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 148
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfwq;->addView(Landroid/view/View;)V

    .line 149
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 153
    iput-object v1, p0, Lfwq;->p:Ljava/lang/String;

    .line 154
    iput-object v1, p0, Lfwq;->q:Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d()V

    .line 157
    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 158
    iget-object v1, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d()V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_0
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 163
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfwq;->p:Ljava/lang/String;

    .line 164
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfwq;->q:Ljava/lang/String;

    .line 166
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    iget-object v3, p0, Lfwq;->q:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    iget-object v3, p0, Lfwq;->q:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 169
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 172
    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 175
    iget-object v4, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-static {v0}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    if-eqz v3, :cond_1

    .line 177
    iget-object v0, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    :cond_0
    move-object v3, v0

    move v0, v2

    .line 180
    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c(Z)V

    move v0, v1

    .line 183
    :goto_1
    if-gt v0, v7, :cond_3

    .line 184
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x4

    .line 186
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 188
    add-int/lit8 v3, v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 190
    iget-object v5, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    add-int/lit8 v6, v0, -0x1

    aget-object v5, v5, v6

    invoke-static {v4}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    if-eqz v4, :cond_2

    .line 192
    iget-object v3, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c(Z)V

    .line 183
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 179
    :cond_1
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 180
    iget-object v0, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-nez v3, :cond_0

    move-object v3, v0

    move v0, v1

    goto :goto_0

    .line 194
    :cond_2
    iget-object v3, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c(Z)V

    goto :goto_2

    .line 197
    :cond_3
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lfwq;->p:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lfwq;->q:Ljava/lang/String;

    return-object v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 269
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 271
    invoke-virtual {p0}, Lfwq;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lfwq;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 272
    :cond_0
    iget-object v0, p0, Lfwq;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 274
    :cond_1
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lfwq;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lfwq;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 287
    invoke-virtual {p0}, Lfwq;->invalidate()V

    .line 289
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 290
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 241
    sget v0, Lfwq;->b:I

    sget v1, Lfwq;->d:I

    add-int/2addr v0, v1

    .line 242
    iget v1, p0, Lfwq;->u:I

    add-int/2addr v1, v0

    sget v2, Lfwq;->j:I

    add-int/2addr v1, v2

    .line 243
    iget v2, p0, Lfwq;->v:I

    add-int/2addr v2, v1

    sget v3, Lfwq;->j:I

    add-int/2addr v2, v3

    .line 245
    iget v3, p0, Lfwq;->u:I

    add-int/2addr v3, v0

    .line 246
    iget v4, p0, Lfwq;->v:I

    add-int/2addr v4, v1

    .line 247
    iget v5, p0, Lfwq;->v:I

    add-int/2addr v5, v2

    .line 249
    sget v6, Lfwq;->e:I

    sget v7, Lfwq;->d:I

    add-int/2addr v6, v7

    .line 250
    iget v7, p0, Lfwq;->v:I

    add-int/2addr v7, v6

    sget v8, Lfwq;->j:I

    add-int/2addr v7, v8

    .line 252
    iget v8, p0, Lfwq;->v:I

    add-int/2addr v8, v6

    .line 253
    iget v9, p0, Lfwq;->v:I

    add-int/2addr v9, v7

    .line 254
    sub-int v10, p5, p3

    sget v11, Lfwq;->f:I

    sub-int/2addr v10, v11

    .line 256
    iget-object v11, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v11, v0, v6, v3, v9}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 257
    iget-object v3, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v11, 0x0

    aget-object v3, v3, v11

    invoke-virtual {v3, v1, v6, v4, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 258
    iget-object v3, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v11, 0x1

    aget-object v3, v3, v11

    invoke-virtual {v3, v2, v6, v5, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 259
    iget-object v3, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v6, 0x2

    aget-object v3, v3, v6

    invoke-virtual {v3, v1, v7, v4, v9}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 260
    iget-object v1, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v3, 0x3

    aget-object v1, v1, v3

    invoke-virtual {v1, v2, v7, v5, v9}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 262
    iget-object v1, p0, Lfwq;->r:Landroid/widget/TextView;

    sget v2, Lfwq;->l:I

    sub-int v2, v10, v2

    invoke-virtual {v1, v0, v9, v5, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 264
    iget-object v0, p0, Lfwq;->w:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sub-int v3, p4, p2

    sub-int v4, p5, p3

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 265
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 214
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 215
    sget v0, Lfwq;->d:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    sget v3, Lfwq;->j:I

    mul-int/lit8 v3, v3, 0x3

    sub-int/2addr v0, v3

    sget v3, Lfwq;->g:I

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lfwq;->v:I

    .line 217
    iget v0, p0, Lfwq;->v:I

    mul-int/lit8 v0, v0, 0x2

    sget v3, Lfwq;->j:I

    add-int/2addr v0, v3

    iput v0, p0, Lfwq;->u:I

    .line 218
    sget v0, Lfwq;->d:I

    iget v3, p0, Lfwq;->u:I

    add-int/2addr v0, v3

    sget v3, Lfwq;->i:I

    add-int/2addr v0, v3

    sget v3, Lfwq;->h:I

    add-int/2addr v3, v0

    .line 220
    iget-object v0, p0, Lfwq;->s:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget v4, p0, Lfwq;->u:I

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, p0, Lfwq;->u:I

    .line 221
    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 220
    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 223
    iget v0, p0, Lfwq;->v:I

    iget-object v4, p0, Lfwq;->o:Landroid/content/Context;

    invoke-static {v4}, Lhss;->a(Landroid/content/Context;)I

    move-result v4

    if-gt v0, v4, :cond_0

    move v0, v1

    .line 224
    :goto_0
    const/4 v4, 0x4

    if-ge v1, v4, :cond_3

    .line 225
    iget-object v4, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v4, v4, v1

    invoke-virtual {v4, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 226
    iget-object v4, p0, Lfwq;->t:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v4, v4, v1

    iget v5, p0, Lfwq;->v:I

    .line 227
    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget v6, p0, Lfwq;->v:I

    .line 228
    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 226
    invoke-virtual {v4, v5, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 223
    :cond_0
    iget-object v4, p0, Lfwq;->o:Landroid/content/Context;

    invoke-static {v4}, Lhss;->c(Landroid/content/Context;)I

    move-result v4

    if-gt v0, v4, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lfwq;->o:Landroid/content/Context;

    invoke-static {v4}, Lhss;->e(Landroid/content/Context;)I

    move-result v4

    if-gt v0, v4, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lfwq;->o:Landroid/content/Context;

    invoke-static {v0}, Lhss;->g(Landroid/content/Context;)I

    const/4 v0, 0x3

    goto :goto_0

    .line 231
    :cond_3
    iget-object v0, p0, Lfwq;->r:Landroid/widget/TextView;

    sget v1, Lfwq;->d:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    sget v4, Lfwq;->b:I

    sub-int/2addr v1, v4

    sget v4, Lfwq;->c:I

    sub-int/2addr v1, v4

    .line 232
    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    sget v4, Lfwq;->i:I

    .line 234
    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 231
    invoke-virtual {v0, v1, v4}, Landroid/widget/TextView;->measure(II)V

    .line 236
    invoke-virtual {p0, v2, v3}, Lfwq;->setMeasuredDimension(II)V

    .line 237
    return-void
.end method

.method public requestLayout()V
    .locals 0

    .prologue
    .line 297
    invoke-virtual {p0}, Lfwq;->forceLayout()V

    .line 298
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lfwq;->w:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    .line 279
    const/4 v0, 0x1

    .line 281
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
