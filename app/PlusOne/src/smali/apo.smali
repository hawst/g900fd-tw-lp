.class public final Lapo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lani;


# direct methods
.method public constructor <init>(Lani;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "inputLimits"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lani;

    iput-object v0, p0, Lapo;->a:Lani;

    .line 37
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 44
    const-wide/16 v0, 0x0

    move-wide v4, v0

    move v1, v2

    .line 45
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 46
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbon;

    invoke-virtual {v0}, Lbon;->g()Lboo;

    move-result-object v0

    iget-wide v6, v0, Lboo;->f:J

    .line 47
    iget-object v0, p0, Lapo;->a:Lani;

    invoke-interface {v0}, Lani;->y()I

    move-result v0

    int-to-long v8, v0

    cmp-long v0, v6, v8

    if-gez v0, :cond_0

    .line 48
    add-long/2addr v4, v6

    .line 45
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    .line 53
    const-wide/32 v6, 0x3938700

    cmp-long v1, v4, v6

    if-ltz v1, :cond_2

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    div-int/lit8 v0, v0, 0x5

    int-to-long v0, v0

    cmp-long v0, v4, v0

    if-ltz v0, :cond_2

    const/4 v2, 0x1

    :cond_2
    return v2
.end method
