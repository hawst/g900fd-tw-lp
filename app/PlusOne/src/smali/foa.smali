.class public final Lfoa;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/stories"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lfoa;->a:Landroid/net/Uri;

    .line 84
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "data"

    aput-object v2, v0, v1

    sput-object v0, Lfoa;->b:[Ljava/lang/String;

    return-void
.end method

.method public static a(Lmmq;)Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 203
    if-eqz p0, :cond_0

    iget-object v0, p0, Lmmq;->b:Lodo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmmq;->b:Lodo;

    iget-object v0, v0, Lodo;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 210
    :goto_0
    return-object v0

    .line 206
    :cond_1
    iget-object v0, p0, Lmmq;->l:Lnzx;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmmq;->l:Lnzx;

    sget-object v2, Lnzr;->a:Loxr;

    invoke-virtual {v0, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    iget-object v0, v0, Lnzr;->b:Lnxr;

    iget-object v0, v0, Lnxr;->c:Ljava/lang/String;

    .line 207
    :goto_1
    if-nez v0, :cond_8

    move-object v0, v1

    .line 208
    goto :goto_0

    .line 206
    :cond_2
    iget-object v6, p0, Lmmq;->e:[Lmma;

    array-length v7, v6

    move v5, v3

    :goto_2
    if-ge v5, v7, :cond_7

    aget-object v0, v6, v5

    iget-object v8, v0, Lmma;->b:[Lmmp;

    array-length v9, v8

    move v4, v3

    :goto_3
    if-ge v4, v9, :cond_6

    aget-object v0, v8, v4

    iget-object v10, v0, Lmmp;->b:[Lmml;

    array-length v11, v10

    move v2, v3

    :goto_4
    if-ge v2, v11, :cond_5

    aget-object v0, v10, v2

    iget-object v12, v0, Lmml;->f:Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    if-nez v12, :cond_4

    iget v12, v0, Lmml;->c:I

    if-ne v12, v13, :cond_4

    iget-object v12, v0, Lmml;->d:Lnzx;

    if-eqz v12, :cond_3

    iget-object v0, v0, Lmml;->d:Lnzx;

    sget-object v12, Lnzu;->a:Loxr;

    invoke-virtual {v0, v12}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    :goto_5
    if-eqz v0, :cond_4

    iget-object v12, v0, Lnzu;->b:Lnym;

    if-eqz v12, :cond_4

    iget-object v12, v0, Lnzu;->b:Lnym;

    iget-object v12, v12, Lnym;->l:Lnyb;

    if-eqz v12, :cond_4

    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->l:Lnyb;

    iget-object v0, v0, Lnyb;->d:Ljava/lang/String;

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_5

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_6
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_1

    .line 210
    :cond_8
    const/4 v2, 0x3

    new-array v4, v13, [Ljava/lang/String;

    iget-object v5, p0, Lmmq;->b:Lodo;

    iget-object v5, v5, Lodo;->c:Ljava/lang/String;

    const-string v6, "ALBUM"

    .line 211
    invoke-static {v1, v5, v0, v6}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    .line 210
    invoke-static {v2, v4}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)Lmmq;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 96
    .line 97
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 98
    if-nez v0, :cond_0

    .line 116
    :goto_0
    return-object v5

    .line 102
    :cond_0
    const-string v1, "stories"

    sget-object v2, Lfoa;->b:[Ljava/lang/String;

    const-string v3, "story_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 106
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    new-instance v0, Lmmq;

    invoke-direct {v0}, Lmmq;-><init>()V

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmmq;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v5, v0

    .line 114
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 110
    :try_start_1
    const-string v2, "EsStoryData"

    const-string v3, "Unable to deserialize Story.  This should not happen because the object was previously serialized as a byte array."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILmmq;)V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lfoa;->a(Landroid/content/Context;ILmmq;Z)V

    .line 121
    return-void
.end method

.method public static a(Landroid/content/Context;ILmmq;Z)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 126
    if-eqz p2, :cond_1

    iget-object v0, p2, Lmmq;->a:Lmok;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lmmq;->a:Lmok;

    iget-object v0, v0, Lmok;->a:Ljava/lang/String;

    .line 127
    :goto_0
    if-nez v0, :cond_2

    .line 199
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v0, v6

    .line 126
    goto :goto_0

    .line 135
    :cond_2
    iget-object v1, p2, Lmmq;->f:Lmoi;

    if-eqz v1, :cond_4

    iget-object v1, p2, Lmmq;->f:Lmoi;

    iget-object v1, v1, Lmoi;->a:Ljava/lang/Boolean;

    .line 136
    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p2, Lmmq;->f:Lmoi;

    iget-object v1, v1, Lmoi;->b:Ljava/lang/Boolean;

    .line 137
    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 138
    invoke-static {p0, p1, v0}, Lfoa;->a(Landroid/content/Context;ILjava/lang/String;)Lmmq;

    move-result-object v1

    .line 139
    if-eqz v1, :cond_4

    iget-object v2, p2, Lmmq;->a:Lmok;

    iget-object v3, v1, Lmmq;->a:Lmok;

    .line 140
    invoke-static {v2, v3}, Loxu;->a(Loxu;Loxu;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v1, Lmmq;->f:Lmoi;

    if-eqz v2, :cond_4

    iget-object v2, v1, Lmmq;->f:Lmoi;

    iget-object v2, v2, Lmoi;->a:Ljava/lang/Boolean;

    .line 142
    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v1, Lmmq;->f:Lmoi;

    iget-object v2, v2, Lmoi;->b:Ljava/lang/Boolean;

    .line 143
    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 144
    :cond_3
    iget-object v2, p2, Lmmq;->f:Lmoi;

    iget-object v3, v1, Lmmq;->f:Lmoi;

    iget-object v3, v3, Lmoi;->b:Ljava/lang/Boolean;

    iput-object v3, v2, Lmoi;->b:Ljava/lang/Boolean;

    .line 145
    iget-object v2, p2, Lmmq;->f:Lmoi;

    iget-object v3, v1, Lmmq;->f:Lmoi;

    iget-object v3, v3, Lmoi;->a:Ljava/lang/Boolean;

    iput-object v3, v2, Lmoi;->a:Ljava/lang/Boolean;

    .line 146
    iget-object v2, v1, Lmmq;->e:[Lmma;

    iput-object v2, p2, Lmmq;->e:[Lmma;

    .line 147
    iget-object v2, v1, Lmmq;->h:[Lmmt;

    iput-object v2, p2, Lmmq;->h:[Lmmt;

    .line 148
    iget-object v2, p2, Lmmq;->l:Lnzx;

    if-nez v2, :cond_4

    .line 149
    iget-object v1, v1, Lmmq;->l:Lnzx;

    iput-object v1, p2, Lmmq;->l:Lnzx;

    .line 155
    :cond_4
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 156
    if-eqz v1, :cond_0

    .line 160
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 161
    const-string v3, "story_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v0, "data"

    invoke-static {p2}, Loxu;->a(Loxu;)[B

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 163
    const-string v0, "refresh_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 164
    if-eqz p3, :cond_5

    .line 165
    const-string v0, "requested_freeze"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 167
    :cond_5
    const-string v0, "stories"

    const/4 v3, 0x5

    invoke-virtual {v1, v0, v6, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 169
    iget-object v0, p2, Lmmq;->g:Lmlz;

    if-eqz v0, :cond_6

    iget-object v0, p2, Lmmq;->g:Lmlz;

    iget-object v0, v0, Lmlz;->b:Logr;

    if-eqz v0, :cond_6

    .line 171
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Logr;

    const/4 v1, 0x0

    iget-object v2, p2, Lmmq;->g:Lmlz;

    iget-object v2, v2, Lmlz;->b:Logr;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {p0, p1, v0, v1, v2}, Llap;->b(Landroid/content/Context;I[Logr;IZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :cond_6
    :goto_2
    invoke-static {p2}, Lfoa;->b(Lmmq;)Lnzx;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_7

    .line 184
    invoke-static {p2}, Lfoa;->a(Lmmq;)Ljava/lang/String;

    move-result-object v2

    .line 187
    invoke-static {p0, p1, v2}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 190
    new-array v3, v4, [Lnzx;

    aput-object v0, v3, v5

    move-object v0, p0

    move v1, p1

    move v7, v4

    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 193
    :cond_7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lfoa;->a:Landroid/net/Uri;

    iget-object v2, p2, Lmmq;->a:Lmok;

    iget-object v2, v2, Lmok;->a:Ljava/lang/String;

    .line 198
    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 197
    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_1

    .line 173
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 297
    invoke-static {p0}, Lfoa;->c(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    if-eq v3, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 298
    const-string v0, "DROP INDEX IF EXISTS stories_idx"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS stories"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 299
    const-string v0, "CREATE TABLE IF NOT EXISTS stories (_id INTEGER PRIMARY KEY AUTOINCREMENT, story_id TEXT UNIQUE NOT NULL, data BLOB, refresh_timestamp INT, requested_freeze INT);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 300
    const-string v0, "CREATE INDEX IF NOT EXISTS stories_idx ON stories(story_id)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 301
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "table_name"

    const-string v2, "stories"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "version"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "table_versions"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 303
    :cond_0
    return-void

    .line 297
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILmmq;[Lmmr;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 284
    iget-object v1, p2, Lmmq;->a:Lmok;

    iget-object v1, v1, Lmok;->a:Ljava/lang/String;

    invoke-static {p0, p1, v1}, Lfoa;->a(Landroid/content/Context;ILjava/lang/String;)Lmmq;

    move-result-object v1

    .line 285
    invoke-static {v1, p2, p3}, Lfss;->a(Lmmq;Lmmq;[Lmmr;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286
    invoke-static {p0, p1, v1, v0}, Lfoa;->a(Landroid/content/Context;ILmmq;Z)V

    .line 287
    const/4 v0, 0x1

    .line 289
    :cond_0
    return v0
.end method

.method private static b(Lmmq;)Lnzx;
    .locals 15

    .prologue
    const/4 v2, 0x0

    .line 217
    iget-object v1, p0, Lmmq;->l:Lnzx;

    .line 218
    if-nez v1, :cond_0

    .line 219
    const/4 v0, 0x0

    .line 233
    :goto_0
    return-object v0

    .line 221
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 222
    iget-object v6, p0, Lmmq;->e:[Lmma;

    array-length v7, v6

    move v4, v2

    :goto_1
    if-ge v4, v7, :cond_4

    aget-object v0, v6, v4

    .line 223
    iget-object v8, v0, Lmma;->b:[Lmmp;

    array-length v9, v8

    move v3, v2

    :goto_2
    if-ge v3, v9, :cond_3

    aget-object v0, v8, v3

    .line 224
    iget-object v10, v0, Lmmp;->b:[Lmml;

    array-length v11, v10

    move v0, v2

    :goto_3
    if-ge v0, v11, :cond_2

    aget-object v12, v10, v0

    .line 225
    iget-object v13, v12, Lmml;->f:Ljava/lang/Boolean;

    invoke-static {v13}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v13

    if-nez v13, :cond_1

    iget v13, v12, Lmml;->c:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    iget-object v13, v12, Lmml;->d:Lnzx;

    if-eqz v13, :cond_1

    iget-object v13, v12, Lmml;->d:Lnzx;

    iget-object v13, v13, Lnzx;->b:Ljava/lang/String;

    if-eqz v13, :cond_1

    .line 227
    iget-object v12, v12, Lmml;->d:Lnzx;

    invoke-interface {v5, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 223
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 222
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 232
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lnzx;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnzx;

    iput-object v0, v1, Lnzx;->j:[Lnzx;

    move-object v0, v1

    .line 233
    goto :goto_0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 263
    .line 264
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 265
    if-nez v0, :cond_0

    .line 272
    :goto_0
    return-void

    .line 269
    :cond_0
    const-string v1, "stories"

    const-string v2, "story_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 270
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lfoa;->a:Landroid/net/Uri;

    .line 271
    invoke-static {v1, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    .line 270
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 355
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 357
    const-string v1, "stories"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "story_id"

    aput-object v0, v2, v4

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 361
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 366
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 369
    const-string v0, "SELECT DISTINCT content_url FROM all_tiles WHERE media_attr & 4194304 != 0"

    invoke-virtual {p0, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 375
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lfss;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 379
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 382
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 383
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 384
    const-string v0, "story_id IN ( "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 385
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    :goto_2
    if-ltz v0, :cond_2

    .line 386
    const-string v2, "?, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 385
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 388
    :cond_2
    const-string v0, "? )"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 390
    const-string v2, "stories"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v8, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0, v2, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 392
    :cond_3
    return-void
.end method

.method private static c(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 329
    const-string v1, "table_versions"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "version"

    aput-object v0, v2, v8

    const-string v3, "table_name=?"

    new-array v4, v4, [Ljava/lang/String;

    const-string v0, "stories"

    aput-object v0, v4, v8

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 337
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 341
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 343
    return v0

    .line 341
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v0, v8

    goto :goto_0
.end method
