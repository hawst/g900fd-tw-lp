.class public final Ldko;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmcw;",
        "Lmcx;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lhgw;

.field private b:Ljava/lang/String;

.field private p:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 6

    .prologue
    .line 29
    const-string v3, "getsources"

    new-instance v4, Lmcw;

    invoke-direct {v4}, Lmcw;-><init>()V

    new-instance v5, Lmcx;

    invoke-direct {v5}, Lmcx;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected a(Lmcw;)V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lmov;

    invoke-direct {v0}, Lmov;-><init>()V

    iput-object v0, p1, Lmcw;->a:Lmov;

    .line 36
    iget-object v0, p1, Lmcw;->a:Lmov;

    .line 37
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmov;->a:Ljava/lang/Boolean;

    .line 38
    return-void
.end method

.method protected a(Lmcx;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 42
    iget-object v0, p1, Lmcx;->a:Lmoy;

    .line 43
    iget-object v4, v0, Lmoy;->a:[Lmpd;

    .line 45
    if-eqz v4, :cond_1

    .line 46
    array-length v5, v4

    move v3, v1

    move-object v0, v2

    .line 47
    :goto_0
    if-ge v3, v5, :cond_2

    .line 48
    aget-object v6, v4, v3

    .line 51
    iget-object v7, v6, Lmpd;->b:Lmpb;

    if-eqz v7, :cond_0

    iget-object v7, v6, Lmpd;->b:Lmpb;

    iget-object v7, v7, Lmpb;->a:Lltu;

    if-eqz v7, :cond_0

    iget-object v7, v6, Lmpd;->b:Lmpb;

    iget-object v7, v7, Lmpb;->a:Lltu;

    iget v7, v7, Lltu;->a:I

    if-ne v7, v8, :cond_0

    .line 54
    iget-object v0, v6, Lmpd;->b:Lmpb;

    iget-object v0, v0, Lmpb;->b:Locf;

    .line 47
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 59
    :cond_2
    if-eqz v0, :cond_5

    .line 60
    iget-object v3, p0, Ldko;->f:Landroid/content/Context;

    iget v4, p0, Ldko;->c:I

    invoke-static {v3, v4, v0, v8, v8}, Lhft;->a(Landroid/content/Context;ILocf;ZZ)Lhgw;

    move-result-object v3

    iput-object v3, p0, Ldko;->a:Lhgw;

    .line 62
    if-eqz v0, :cond_4

    iget-object v3, v0, Locf;->d:Locd;

    if-eqz v3, :cond_4

    .line 63
    iget-object v2, v0, Locf;->d:Locd;

    iget-object v2, v2, Locd;->b:Ljava/lang/String;

    iput-object v2, p0, Ldko;->b:Ljava/lang/String;

    .line 68
    :goto_1
    iget-object v2, v0, Locf;->b:[Locb;

    .line 69
    if-eqz v2, :cond_6

    .line 70
    array-length v3, v2

    move v0, v1

    .line 71
    :goto_2
    if-ge v0, v3, :cond_6

    .line 72
    aget-object v1, v2, v0

    .line 73
    iget v4, v1, Locb;->b:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 74
    iget-object v1, v1, Locb;->c:[I

    iput-object v1, p0, Ldko;->p:[I

    .line 71
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 65
    :cond_4
    iput-object v2, p0, Ldko;->b:Ljava/lang/String;

    goto :goto_1

    .line 79
    :cond_5
    iput-object v2, p0, Ldko;->a:Lhgw;

    .line 80
    iput-object v2, p0, Ldko;->b:Ljava/lang/String;

    .line 83
    :cond_6
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmcw;

    invoke-virtual {p0, p1}, Ldko;->a(Lmcw;)V

    return-void
.end method

.method public b()Lhgw;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Ldko;->a:Lhgw;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmcx;

    invoke-virtual {p0, p1}, Ldko;->a(Lmcx;)V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Ldko;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()[I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Ldko;->p:[I

    return-object v0
.end method
