.class public abstract Leku;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lbc;
.implements Ldid;
.implements Leqk;
.implements Lfvq;
.implements Lfze;
.implements Lhjj;
.implements Lhmq;
.implements Lhob;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AbsListView$RecyclerListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ldid;",
        "Leqk;",
        "Lfvq;",
        "Lfze;",
        "Lhjj;",
        "Lhmq;",
        "Lhob;",
        "Llgs;"
    }
.end annotation


# static fields
.field private static N:I

.field public static final P:[Ljava/lang/String;


# instance fields
.field private O:Landroid/widget/ListView;

.field public final Q:Lhje;

.field public R:Z

.field public S:Landroid/widget/ListAdapter;

.field public T:Landroid/database/Cursor;

.field public U:Lhxh;

.field public V:Ljava/lang/Integer;

.field public W:Ljava/lang/String;

.field public X:Z

.field public Y:Lfuw;

.field public Z:Lhee;

.field public aa:Ldib;

.field public final ab:Licq;

.field public ac:Z

.field private ad:Lhxb;

.field private ae:Liax;

.field private af:Ljpb;

.field private ag:I

.field private ah:Llcr;

.field private ai:Z

.field private aj:Z

.field private ak:Llfn;

.field private al:Liwk;

.field private am:Ljmg;

.field private an:Lknu;

.field private final ao:Landroid/database/DataSetObserver;

.field private final ap:Lfhh;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 153
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "circle_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "circle_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "contact_count"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "type"

    aput-object v2, v0, v1

    sput-object v0, Leku;->P:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 331
    invoke-direct {p0}, Llol;-><init>()V

    .line 162
    new-instance v0, Lhje;

    iget-object v1, p0, Leku;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Leku;->Q:Lhje;

    .line 169
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 181
    const/4 v0, -0x1

    iput v0, p0, Leku;->ag:I

    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Leku;->aj:Z

    .line 192
    new-instance v0, Licq;

    iget-object v1, p0, Leku;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    iput-object v0, p0, Leku;->ab:Licq;

    .line 195
    new-instance v0, Lekv;

    invoke-direct {v0, p0}, Lekv;-><init>(Leku;)V

    iput-object v0, p0, Leku;->ao:Landroid/database/DataSetObserver;

    .line 320
    new-instance v0, Lekw;

    invoke-direct {v0, p0}, Lekw;-><init>(Leku;)V

    iput-object v0, p0, Leku;->ap:Lfhh;

    .line 335
    new-instance v0, Liay;

    iget-object v1, p0, Leku;->av:Llqm;

    const v2, 0x7f1002b2

    invoke-direct {v0, v1, v2}, Liay;-><init>(Llqr;I)V

    .line 332
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;II)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 847
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 848
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const-string v1, "person_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    const-string v1, "for_sharing"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 851
    const-string v1, "suggestion_id"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    const-string v1, "suggestion_type"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 853
    const-string v1, "action_source"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 854
    return-object v0
.end method

.method private a(III)Ljava/util/HashMap;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 927
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 928
    :goto_0
    if-gt p2, p3, :cond_0

    .line 929
    iget-object v1, p0, Leku;->O:Landroid/widget/ListView;

    invoke-virtual {v1, p2}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Leku;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 930
    iget-object v2, p0, Leku;->O:Landroid/widget/ListView;

    sub-int v3, p2, p1

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 928
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 932
    :cond_0
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;IZ)V
    .locals 10

    .prologue
    .line 765
    iget-object v0, p0, Leku;->al:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 766
    iget-object v0, p0, Leku;->at:Llnl;

    iget-object v1, p0, Leku;->al:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Llnl;->startActivity(Landroid/content/Intent;)V

    .line 843
    :goto_0
    return-void

    .line 769
    :cond_0
    invoke-virtual {p0, p5}, Leku;->e(I)I

    move-result v6

    .line 771
    iget-object v0, p0, Leku;->af:Ljpb;

    iget-object v1, p0, Leku;->at:Llnl;

    iget-object v2, p0, Leku;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljpb;->d(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    .line 772
    invoke-direct/range {v0 .. v6}, Leku;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;II)Landroid/os/Bundle;

    move-result-object v0

    .line 774
    iget-object v1, p0, Leku;->af:Ljpb;

    iget-object v2, p0, Leku;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const-string v3, "first_circle_add"

    invoke-interface {v1, p0, v2, v3, v0}, Ljpb;->a(Lu;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 779
    :cond_1
    if-nez p2, :cond_2

    .line 780
    const-string v6, "add_email_dialog"

    iget-object v0, p0, Leku;->at:Llnl;

    const v1, 0x7f0a087d

    invoke-virtual {v0, v1}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Leku;->at:Llnl;

    const v3, 0x7f0a087e

    invoke-virtual {v2, v3}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Leku;->at:Llnl;

    const v4, 0x104000a

    invoke-virtual {v3, v4}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Leku;->at:Llnl;

    const/high16 v5, 0x1040000

    invoke-virtual {v4, v5}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Leag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Leag;

    move-result-object v0

    invoke-virtual {v0}, Leag;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "person_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "for_sharing"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "person_suggestion_id"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "person_suggestion_type"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Leag;->a(Lu;I)V

    invoke-virtual {p0}, Leku;->p()Lae;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Leag;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 785
    :cond_2
    const/4 v0, 0x0

    .line 786
    if-eqz p6, :cond_3

    .line 787
    iget-object v0, p0, Leku;->at:Llnl;

    iget-object v1, p0, Leku;->T:Landroid/database/Cursor;

    invoke-static {v0, v1, p3}, Ldsm;->a(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v0

    .line 790
    :cond_3
    const/4 v2, 0x0

    .line 791
    const/4 v1, 0x0

    .line 792
    iget-boolean v3, p0, Leku;->ai:Z

    if-eqz v3, :cond_7

    if-eqz p3, :cond_7

    if-eqz v0, :cond_7

    .line 793
    const/4 v1, 0x1

    .line 794
    iget-object v2, p0, Leku;->at:Llnl;

    invoke-static {v2, p3}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v7

    .line 795
    iget-object v2, p0, Leku;->af:Ljpb;

    iget-object v3, p0, Leku;->at:Llnl;

    iget-object v4, p0, Leku;->Z:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-interface {v2, v3, v4}, Ljpb;->e(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    .line 796
    invoke-direct/range {v0 .. v6}, Leku;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;II)Landroid/os/Bundle;

    move-result-object v6

    .line 798
    iget-object v0, p0, Leku;->af:Ljpb;

    iget-object v1, p0, Leku;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v2

    const-string v3, "first_circle_add_one_click"

    move-object v1, p0

    move-object v4, p2

    move-object v5, v7

    invoke-interface/range {v0 .. v6}, Ljpb;->a(Lu;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_4
    move v3, v1

    move-object v1, v7

    .line 804
    :goto_1
    if-nez v0, :cond_5

    .line 806
    invoke-virtual {p0, p1, p2, p4, p5}, Leku;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 808
    :cond_5
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 809
    iget-object v2, p0, Leku;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 812
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 814
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v4, Lhoc;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 815
    new-instance v4, Ldpj;

    iget-object v5, p0, Leku;->at:Llnl;

    invoke-direct {v4, v5}, Ldpj;-><init>(Landroid/content/Context;)V

    .line 817
    invoke-virtual {v4, v2}, Ldpj;->a(I)Ldpj;

    move-result-object v4

    .line 818
    invoke-virtual {v4, p1}, Ldpj;->a(Ljava/lang/String;)Ldpj;

    move-result-object v4

    .line 819
    invoke-virtual {v4, p2}, Ldpj;->b(Ljava/lang/String;)Ldpj;

    move-result-object v4

    .line 820
    invoke-virtual {v4, v6}, Ldpj;->b(I)Ldpj;

    move-result-object v4

    .line 821
    invoke-virtual {v4, v7}, Ldpj;->a(Ljava/util/ArrayList;)Ldpj;

    move-result-object v4

    const/4 v5, 0x0

    .line 822
    invoke-virtual {v4, v5}, Ldpj;->b(Ljava/util/ArrayList;)Ldpj;

    move-result-object v4

    const/4 v5, 0x1

    .line 823
    invoke-virtual {v4, v5}, Ldpj;->a(Z)Ldpj;

    move-result-object v4

    const/4 v5, 0x0

    .line 824
    invoke-virtual {v4, v5}, Ldpj;->b(Z)Ldpj;

    move-result-object v4

    const/4 v5, 0x1

    .line 825
    invoke-virtual {v4, v5}, Ldpj;->c(Z)Ldpj;

    move-result-object v4

    .line 826
    invoke-virtual {v4}, Ldpj;->a()Ldpi;

    move-result-object v4

    invoke-virtual {v0, v4}, Lhoc;->b(Lhny;)V

    .line 827
    if-eqz v3, :cond_6

    .line 828
    const v0, 0x7f0a0524

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p0, v0, v3}, Leku;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 830
    iget-object v1, p0, Leku;->at:Llnl;

    const/4 v3, 0x1

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 831
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 833
    :cond_6
    iget-object v0, p0, Leku;->ae:Liax;

    iget-object v1, p0, Leku;->at:Llnl;

    .line 834
    invoke-virtual {p0}, Leku;->Z()I

    move-result v5

    move-object v3, p1

    move-object v4, p4

    .line 833
    invoke-interface/range {v0 .. v6}, Liax;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II)V

    .line 835
    invoke-virtual {p0}, Leku;->ac()V

    .line 836
    invoke-virtual {p0}, Leku;->e()V

    .line 838
    const/4 v0, 0x4

    invoke-virtual {p0, p5, v0}, Leku;->a(II)Ldid;

    move-result-object v9

    .line 840
    new-instance v3, Ldib;

    iget-object v4, p0, Leku;->at:Llnl;

    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lhms;

    const/4 v8, 0x0

    move-object v6, p1

    invoke-direct/range {v3 .. v9}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    .line 841
    invoke-virtual {v3, v2}, Ldib;->a(I)V

    goto/16 :goto_0

    :cond_7
    move v3, v2

    goto/16 :goto_1
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 573
    sget-object v0, Lhmw;->r:Lhmw;

    return-object v0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 716
    const/4 v0, 0x0

    return v0
.end method

.method protected Y()I
    .locals 1

    .prologue
    .line 450
    const v0, 0x7f040161

    return v0
.end method

.method protected Z()I
    .locals 1

    .prologue
    .line 516
    const/16 v0, 0x9

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 1078
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 400
    invoke-virtual {p0}, Leku;->Y()I

    move-result v0

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 402
    const v0, 0x7f1002b2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Leku;->O:Landroid/widget/ListView;

    .line 404
    iget-object v0, p0, Leku;->O:Landroid/widget/ListView;

    sget v2, Leku;->N:I

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 405
    iget-object v0, p0, Leku;->O:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 407
    invoke-virtual {p0}, Leku;->w()Lbb;

    move-result-object v0

    .line 408
    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 409
    invoke-virtual {p0}, Leku;->d()V

    .line 411
    new-instance v2, Lhxh;

    iget-object v3, p0, Leku;->at:Llnl;

    iget-object v4, p0, Leku;->Z:Lhee;

    .line 412
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v2, v3, v0, v4}, Lhxh;-><init>(Landroid/content/Context;Lbb;I)V

    iput-object v2, p0, Leku;->U:Lhxh;

    .line 413
    iget-object v0, p0, Leku;->U:Lhxh;

    iget-object v2, p0, Leku;->ao:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v2}, Lhxh;->a(Landroid/database/DataSetObserver;)V

    .line 414
    iget-object v0, p0, Leku;->U:Lhxh;

    invoke-virtual {v0}, Lhxh;->b()V

    .line 416
    new-instance v0, Lfuw;

    iget-object v2, p0, Leku;->at:Llnl;

    iget-object v3, p0, Leku;->O:Landroid/widget/ListView;

    invoke-direct {v0, v2, v3, p0}, Lfuw;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lfvq;)V

    iput-object v0, p0, Leku;->Y:Lfuw;

    .line 418
    const v0, 0x7f10025f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 419
    if-eqz v0, :cond_0

    .line 420
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 423
    :cond_0
    invoke-virtual {p0}, Leku;->aa()V

    .line 425
    return-object v1
.end method

.method protected a(II)Ldid;
    .locals 1

    .prologue
    .line 1034
    invoke-static {p1, p2}, Ldib;->a(II)Ldid;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521
    packed-switch p1, :pswitch_data_0

    .line 527
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 523
    :pswitch_0
    new-instance v0, Lhxg;

    iget-object v1, p0, Leku;->at:Llnl;

    iget-object v2, p0, Leku;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x1

    sget-object v4, Leku;->P:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    goto :goto_0

    .line 521
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 577
    if-nez p1, :cond_0

    move-object p1, v0

    .line 591
    :goto_0
    return-object p1

    .line 580
    :cond_0
    instance-of v1, p1, Lohv;

    if-eqz v1, :cond_1

    .line 581
    check-cast p1, Lohv;

    iget-object v0, p1, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->b(Lohp;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 582
    :cond_1
    instance-of v1, p1, Lnrp;

    if-eqz v1, :cond_3

    .line 583
    check-cast p1, Lnrp;

    .line 584
    iget-object v1, p1, Lnrp;->b:Lohv;

    if-nez v1, :cond_2

    move-object p1, v0

    .line 585
    goto :goto_0

    .line 587
    :cond_2
    iget-object v0, p1, Lnrp;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->b(Lohp;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 588
    :cond_3
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 589
    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_4
    move-object p1, v0

    .line 591
    goto :goto_0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 948
    if-nez p1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 949
    new-instance v0, Lkoe;

    const/16 v1, 0x65

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Leku;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    iget-object v0, p0, Leku;->ak:Llfn;

    iget-object v1, p0, Leku;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "AddToCircle"

    const-wide/16 v4, 0x9

    invoke-interface {v0, v1, v2, v4, v5}, Llfn;->a(ILjava/lang/String;J)V

    const-string v0, "person_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "display_name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "suggestion_id"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "original_circle_ids"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "selected_circle_ids"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Leku;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 950
    :cond_0
    invoke-super {p0, p1, p2, p3}, Llol;->a(IILandroid/content/Intent;)V

    .line 955
    return-void
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 668
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 672
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 340
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 341
    sget v0, Leku;->N:I

    if-nez v0, :cond_0

    .line 343
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Leku;->N:I

    .line 345
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 370
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 372
    if-eqz p1, :cond_0

    .line 373
    const-string v0, "suggestion_type"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Leku;->ag:I

    .line 374
    iget-object v1, p0, Leku;->at:Llnl;

    iget-object v0, p0, Leku;->au:Llnh;

    const-class v2, Lhms;

    .line 375
    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 374
    invoke-static {v1, v0, p1}, Ldib;->a(Landroid/content/Context;Lhms;Landroid/os/Bundle;)Ldib;

    move-result-object v0

    iput-object v0, p0, Leku;->aa:Ldib;

    .line 376
    const-string v0, "unblock_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    const-string v0, "unblock_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leku;->V:Ljava/lang/Integer;

    .line 381
    :cond_0
    iget-object v0, p0, Leku;->Z:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "domain_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leku;->W:Ljava/lang/String;

    .line 383
    iget-boolean v0, p0, Leku;->ac:Z

    if-eqz v0, :cond_1

    .line 384
    iget-object v0, p0, Leku;->at:Llnl;

    invoke-static {v0}, Ldwn;->a(Landroid/content/Context;)Ldwn;

    move-result-object v0

    invoke-virtual {v0}, Ldwn;->a()V

    .line 385
    invoke-static {}, Lemi;->l()V

    .line 386
    const/4 v0, 0x0

    iput-boolean v0, p0, Leku;->ac:Z

    .line 389
    :cond_1
    new-instance v0, Llcr;

    iget-object v1, p0, Leku;->at:Llnl;

    invoke-direct {v0, v1}, Llcr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leku;->ah:Llcr;

    .line 390
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 640
    const-string v0, "add_email_dialog"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 641
    const-string v0, "message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 642
    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 643
    const-string v0, "for_sharing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 644
    const-string v0, "person_suggestion_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 645
    const-string v0, "person_suggestion_type"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    move-object v0, p0

    .line 646
    invoke-virtual/range {v0 .. v5}, Leku;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 647
    :cond_1
    const-string v0, "first_circle_add"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "first_circle_add_one_click"

    .line 648
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 649
    :cond_2
    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 650
    const-string v0, "person_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 651
    const-string v0, "for_sharing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 652
    const-string v0, "suggestion_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 653
    const-string v0, "suggestion_type"

    const/4 v5, -0x1

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    move-object v0, p0

    .line 654
    invoke-virtual/range {v0 .. v5}, Leku;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0
.end method

.method protected a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 393
    iput-object p1, p0, Leku;->S:Landroid/widget/ListAdapter;

    .line 394
    iget-object v0, p0, Leku;->O:Landroid/widget/ListView;

    iget-object v1, p0, Leku;->S:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 395
    return-void
.end method

.method public a(Lcom/google/android/apps/plus/views/PeopleListRowView;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1107
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->g()Landroid/view/View;

    move-result-object v2

    .line 1108
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->h()I

    move-result v0

    int-to-float v3, v0

    const-wide/16 v4, 0x12c

    const/4 v6, 0x1

    move-object v1, p1

    .line 1107
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Landroid/view/View;FJZ)V

    .line 1109
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 541
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 532
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 537
    :goto_0
    return-void

    .line 534
    :pswitch_0
    iput-object p2, p0, Leku;->T:Landroid/database/Cursor;

    goto :goto_0

    .line 532
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 105
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Leku;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method protected a(Lfib;)V
    .locals 3

    .prologue
    .line 1039
    invoke-virtual {p0}, Leku;->ae()V

    .line 1040
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1041
    iget-object v0, p0, Leku;->at:Llnl;

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1042
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1044
    :cond_0
    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 464
    const v0, 0x7f100406

    new-instance v1, Lfkx;

    invoke-direct {v1}, Lfkx;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 465
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 466
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 467
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1114
    const-string v0, "ModifyCircleMembershipsTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1115
    iget-object v0, p0, Leku;->aa:Ldib;

    if-eqz v0, :cond_0

    .line 1116
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1117
    iget-object v0, p0, Leku;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1118
    iget-object v1, p0, Leku;->aa:Ldib;

    invoke-virtual {v1, v0}, Ldib;->a(I)V

    .line 1122
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Leku;->aa:Ldib;

    .line 1124
    :cond_0
    new-instance v0, Lkoe;

    const/16 v1, 0x66

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Leku;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 1125
    iget-object v0, p0, Leku;->ak:Llfn;

    iget-object v1, p0, Leku;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "AddToCircle"

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Llfn;->a(I[Ljava/lang/String;)V

    .line 1127
    :cond_1
    return-void

    .line 1120
    :cond_2
    iget-object v0, p0, Leku;->at:Llnl;

    invoke-virtual {p2}, Lhoz;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 680
    iget-object v0, p0, Leku;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 682
    if-eqz p3, :cond_0

    .line 683
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Leku;->at:Llnl;

    invoke-direct {v1, v3, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->eT:Lhmv;

    .line 685
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 686
    invoke-virtual {v1, p3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 683
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 690
    :cond_0
    const/4 v0, 0x0

    .line 692
    invoke-virtual {p0}, Leku;->U()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Leku;->an:Lknu;

    invoke-interface {v1, v2}, Lknu;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 693
    new-instance v0, Lknt;

    iget-object v1, p0, Leku;->at:Llnl;

    invoke-direct {v0, v1}, Lknt;-><init>(Landroid/content/Context;)V

    .line 694
    invoke-virtual {v0, p1}, Lknt;->a(Ljava/lang/String;)Lknt;

    move-result-object v0

    .line 695
    invoke-virtual {v0}, Lknt;->a()Landroid/content/Intent;

    move-result-object v0

    move v3, v4

    .line 698
    :goto_0
    if-nez v0, :cond_3

    .line 700
    iget-object v0, p0, Leku;->am:Ljmg;

    iget-object v1, p0, Leku;->at:Llnl;

    invoke-interface {v0, v1}, Ljmg;->a(Landroid/content/Context;)Ljmh;

    move-result-object v0

    .line 701
    invoke-interface {v0, p1}, Ljmh;->a(Ljava/lang/String;)Ljmh;

    move-result-object v0

    .line 702
    invoke-interface {v0}, Ljmh;->a()Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 704
    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-lt v0, v6, :cond_1

    if-eqz v3, :cond_2

    .line 706
    :cond_1
    invoke-virtual {p0, v1, v4}, Leku;->a(Landroid/content/Intent;I)V

    .line 711
    :goto_2
    iget-object v0, p0, Leku;->ae:Liax;

    iget-object v1, p0, Leku;->at:Llnl;

    .line 712
    invoke-virtual {p0}, Leku;->Z()I

    move-result v5

    invoke-virtual {p0, p4}, Leku;->e(I)I

    move-result v6

    move-object v3, p1

    move-object v4, p2

    .line 711
    invoke-interface/range {v0 .. v6}, Liax;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II)V

    .line 713
    return-void

    .line 708
    :cond_2
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v3, Lhkr;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    invoke-virtual {v0}, Lhkr;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, Leku;->n()Lz;

    move-result-object v3

    invoke-virtual {v3, v1, v5, v0}, Lz;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_1

    :cond_4
    move v3, v5

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 870
    invoke-virtual {p0, p1, p2, p3, p4}, Leku;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 871
    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 978
    iget-object v0, p0, Leku;->at:Llnl;

    iget-object v1, p0, Leku;->T:Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Ldsm;->a(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v2

    .line 980
    const/4 v1, 0x0

    .line 982
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 983
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 984
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 985
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 989
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 990
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 991
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 992
    const/4 v1, 0x1

    .line 994
    :cond_3
    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 995
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 999
    :cond_4
    iget-object v0, p0, Leku;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v6

    .line 1000
    iget-boolean v0, p0, Leku;->ai:Z

    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    .line 1001
    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    move v1, v0

    .line 1002
    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    .line 1003
    if-eqz p3, :cond_5

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_5
    const/4 v0, 0x1

    move v3, v0

    .line 1004
    :goto_3
    if-eqz p4, :cond_6

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_6
    const/4 v0, 0x1

    move v2, v0

    .line 1006
    :goto_4
    iget v0, p0, Leku;->ag:I

    invoke-virtual {p0, v0}, Leku;->e(I)I

    move-result v7

    .line 1007
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v8, Lhoc;

    invoke-virtual {v0, v8}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 1008
    new-instance v8, Ldpj;

    iget-object v9, p0, Leku;->at:Llnl;

    invoke-direct {v8, v9}, Ldpj;-><init>(Landroid/content/Context;)V

    .line 1010
    invoke-virtual {v8, v6}, Ldpj;->a(I)Ldpj;

    move-result-object v6

    .line 1011
    invoke-virtual {v6, p1}, Ldpj;->a(Ljava/lang/String;)Ldpj;

    move-result-object v6

    .line 1012
    invoke-virtual {v6, p2}, Ldpj;->b(Ljava/lang/String;)Ldpj;

    move-result-object v6

    .line 1013
    invoke-virtual {v6, v7}, Ldpj;->b(I)Ldpj;

    move-result-object v6

    .line 1014
    invoke-virtual {v6, v4}, Ldpj;->a(Ljava/util/ArrayList;)Ldpj;

    move-result-object v6

    .line 1015
    invoke-virtual {v6, v5}, Ldpj;->b(Ljava/util/ArrayList;)Ldpj;

    move-result-object v6

    .line 1016
    invoke-virtual {v6, v3}, Ldpj;->a(Z)Ldpj;

    move-result-object v3

    .line 1017
    invoke-virtual {v3, v2}, Ldpj;->b(Z)Ldpj;

    move-result-object v2

    const/4 v3, 0x0

    .line 1018
    invoke-virtual {v2, v3}, Ldpj;->c(Z)Ldpj;

    move-result-object v2

    .line 1019
    if-eqz v1, :cond_7

    .line 1020
    iget-object v1, p0, Leku;->at:Llnl;

    const/4 v3, 0x1

    invoke-static {v1, v3}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    .line 1021
    iget-object v3, p0, Leku;->at:Llnl;

    const v6, 0x7f0a0525

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    .line 1022
    invoke-virtual {v3, v6, v7}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1021
    invoke-virtual {v2, v1}, Ldpj;->e(Ljava/lang/String;)Ldpj;

    .line 1024
    :cond_7
    invoke-virtual {v2}, Ldpj;->a()Ldpi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhoc;->c(Lhny;)V

    .line 1026
    iget v0, p0, Leku;->ag:I

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Leku;->a(II)Ldid;

    move-result-object v6

    .line 1028
    new-instance v0, Ldib;

    iget-object v1, p0, Leku;->at:Llnl;

    iget-object v2, p0, Leku;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v2, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhms;

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    iput-object v0, p0, Leku;->aa:Ldib;

    .line 1030
    return-void

    .line 1001
    :cond_8
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_2

    .line 1003
    :cond_9
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_3

    .line 1004
    :cond_a
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_4
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V
    .locals 7

    .prologue
    .line 744
    iget-boolean v0, p0, Leku;->ai:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_1

    :cond_0
    const/4 v6, 0x1

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    .line 745
    invoke-direct/range {v0 .. v6}, Leku;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;IZ)V

    .line 747
    return-void

    .line 744
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 876
    new-instance v0, Leqj;

    invoke-direct {v0, p1, p2}, Leqj;-><init>(Ljava/lang/String;Z)V

    .line 877
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Leqj;->a(Lu;I)V

    .line 878
    invoke-virtual {p0}, Leku;->p()Lae;

    move-result-object v1

    const-string v2, "unblock_person"

    invoke-virtual {v0, v1, v2}, Leqj;->a(Lae;Ljava/lang/String;)V

    .line 879
    return-void
.end method

.method public a(Loo;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 455
    invoke-virtual {p1, v3}, Loo;->d(Z)V

    .line 456
    invoke-virtual {p0}, Leku;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "disable_up_button"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 457
    invoke-virtual {p1, v3}, Loo;->c(Z)V

    .line 458
    invoke-static {p1, v3}, Lley;->a(Loo;Z)V

    .line 460
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1153
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 471
    const/4 v0, 0x0

    return v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 489
    invoke-super {p0}, Llol;->aO_()V

    .line 490
    iget-object v0, p0, Leku;->ap:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 492
    iget-object v0, p0, Leku;->ad:Lhxb;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lhxb;->a(Z)V

    .line 494
    iget-object v0, p0, Leku;->V:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Leku;->V:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 496
    iget-object v0, p0, Leku;->V:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 497
    iget-object v1, p0, Leku;->V:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    invoke-virtual {p0, v0}, Leku;->a(Lfib;)V

    .line 498
    const/4 v0, 0x0

    iput-object v0, p0, Leku;->V:Ljava/lang/Integer;

    .line 502
    :cond_0
    invoke-virtual {p0}, Leku;->x()Landroid/view/View;

    invoke-virtual {p0}, Leku;->aa()V

    .line 503
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1102
    const/4 v0, 0x0

    return-object v0
.end method

.method protected aa()V
    .locals 2

    .prologue
    .line 555
    invoke-virtual {p0}, Leku;->ab()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556
    iget-boolean v0, p0, Leku;->R:Z

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Leku;->ab:Licq;

    invoke-virtual {v0}, Licq;->a()V

    .line 565
    :goto_0
    return-void

    .line 559
    :cond_0
    iget-object v0, p0, Leku;->ab:Licq;

    invoke-virtual {p0}, Leku;->al_()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Licq;->a(Ljava/lang/CharSequence;)Licq;

    .line 560
    iget-object v0, p0, Leku;->ab:Licq;

    invoke-virtual {v0}, Licq;->d()V

    goto :goto_0

    .line 563
    :cond_1
    iget-object v0, p0, Leku;->ab:Licq;

    invoke-virtual {v0}, Licq;->e()V

    goto :goto_0
.end method

.method protected ab()Z
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Leku;->S:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leku;->S:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ac()V
    .locals 0

    .prologue
    .line 865
    return-void
.end method

.method public ad()V
    .locals 0

    .prologue
    .line 893
    return-void
.end method

.method protected ae()V
    .locals 2

    .prologue
    .line 1054
    invoke-virtual {p0}, Leku;->p()Lae;

    move-result-object v0

    const-string v1, "pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 1056
    if-eqz v0, :cond_0

    .line 1057
    invoke-virtual {v0}, Lt;->a()V

    .line 1059
    :cond_0
    return-void
.end method

.method public af()Z
    .locals 1

    .prologue
    .line 1062
    invoke-virtual {p0}, Leku;->ab()Z

    move-result v0

    return v0
.end method

.method public ag()Z
    .locals 2

    .prologue
    .line 1066
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected al_()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 551
    const v0, 0x7f0a07ec

    invoke-virtual {p0, v0}, Leku;->e_(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected am_()I
    .locals 1

    .prologue
    .line 317
    const v0, 0x7f040164

    return v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1094
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 660
    return-void
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 937
    iput p4, p0, Leku;->ag:I

    .line 938
    iget-object v0, p0, Leku;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 939
    iget-object v0, p0, Leku;->at:Llnl;

    const/4 v5, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    .line 940
    invoke-static/range {v0 .. v5}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    .line 939
    invoke-virtual {p0, v0, v1}, Leku;->a(Landroid/content/Intent;I)V

    .line 944
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 752
    iget-boolean v0, p0, Leku;->ai:Z

    if-nez v0, :cond_0

    .line 760
    :goto_0
    return v6

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    .line 758
    invoke-direct/range {v0 .. v6}, Leku;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;IZ)V

    .line 760
    const/4 v6, 0x1

    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 349
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 350
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 351
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Leku;->Z:Lhee;

    .line 353
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 355
    iget-object v0, p0, Leku;->au:Llnh;

    iget-object v0, p0, Leku;->at:Llnl;

    const-class v1, Llfn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llfn;

    iput-object v0, p0, Leku;->ak:Llfn;

    .line 356
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Lhxb;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxb;

    iput-object v0, p0, Leku;->ad:Lhxb;

    .line 357
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Liax;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liax;

    iput-object v0, p0, Leku;->ae:Liax;

    .line 358
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Ljpb;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpb;

    iput-object v0, p0, Leku;->af:Ljpb;

    .line 360
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Liaw;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liaw;

    iget-object v1, p0, Leku;->Z:Lhee;

    .line 361
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-interface {v0, v1}, Liaw;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Leku;->ai:Z

    .line 362
    new-instance v0, Liwk;

    iget-object v1, p0, Leku;->at:Llnl;

    iget-object v2, p0, Leku;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v1, Lixj;

    .line 363
    invoke-virtual {v0, v1}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Leku;->al:Liwk;

    .line 364
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Ljmg;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmg;

    iput-object v0, p0, Leku;->am:Ljmg;

    .line 365
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Lknu;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lknu;

    iput-object v0, p0, Leku;->an:Lknu;

    .line 366
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 664
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 1130
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 1131
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->c()Ljava/lang/String;

    move-result-object v5

    .line 1132
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->d()Ljava/lang/String;

    move-result-object v6

    .line 1134
    new-instance v1, Ldpx;

    iget-object v2, p0, Leku;->at:Llnl;

    iget-object v3, p0, Leku;->Z:Lhee;

    .line 1135
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v1, v2, v3, v5}, Ldpx;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 1136
    iget-object v2, p0, Leku;->at:Llnl;

    invoke-static {v2, v1}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 1138
    invoke-virtual {p0}, Leku;->ag()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1139
    iget-object v1, p0, Leku;->O:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    iget-object v1, p0, Leku;->O:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v3

    iget-boolean v1, p0, Leku;->aj:Z

    if-eqz v1, :cond_2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    add-int/lit8 v4, v3, 0x1

    iget-object v7, p0, Leku;->O:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/ListView;->getCount()I

    move-result v7

    if-ne v4, v7, :cond_0

    add-int/lit8 v3, v3, -0x1

    :cond_0
    :goto_1
    invoke-virtual {p0}, Leku;->ag()Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    :goto_2
    if-gt v1, v3, :cond_3

    iget-object v7, p0, Leku;->O:Landroid/widget/ListView;

    invoke-virtual {v7, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {p0, v7}, Leku;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Leku;->O:Landroid/widget/ListView;

    sub-int v9, v1, v2

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move-object v1, v4

    :goto_3
    iget-object v2, p0, Leku;->Y:Lfuw;

    iget-object v3, p0, Leku;->O:Landroid/widget/ListView;

    iget-boolean v4, p0, Leku;->aj:Z

    invoke-virtual {v2, v3, v1, v4}, Lfuw;->a(Landroid/widget/ListView;Ljava/util/HashMap;Z)V

    .line 1142
    :cond_4
    invoke-static {v5}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lemi;->a(Ljava/lang/String;)V

    .line 1143
    iget-object v1, p0, Leku;->S:Landroid/widget/ListAdapter;

    instance-of v1, v1, Lekx;

    if-eqz v1, :cond_5

    .line 1144
    iget-object v1, p0, Leku;->S:Landroid/widget/ListAdapter;

    check-cast v1, Lekx;

    invoke-virtual {v1}, Lekx;->a()V

    .line 1147
    :cond_5
    iget-object v1, p0, Leku;->ae:Liax;

    .line 1148
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->e()I

    move-result v0

    invoke-virtual {p0, v0}, Leku;->e(I)I

    move-result v0

    .line 1147
    invoke-interface {v1, v5, v6, v0}, Liax;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1149
    return-void

    .line 1139
    :cond_6
    invoke-direct {p0, v2, v1, v3}, Leku;->a(III)Ljava/util/HashMap;

    move-result-object v1

    goto :goto_3
.end method

.method protected d(Landroid/view/View;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 429
    const v0, 0x7f1002b2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Leku;->O:Landroid/widget/ListView;

    .line 431
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Leku;->at:Llnl;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 432
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    iget-object v2, p0, Leku;->ah:Llcr;

    iget v2, v2, Llcr;->d:I

    iget-object v3, p0, Leku;->ah:Llcr;

    iget v3, v3, Llcr;->d:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 435
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 436
    iget-object v1, p0, Leku;->O:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 437
    iget-object v1, p0, Leku;->O:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 438
    const/4 v0, 0x1

    iput-boolean v0, p0, Leku;->aj:Z

    .line 440
    iget-object v0, p0, Leku;->at:Llnl;

    invoke-static {v0}, Llsc;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leku;->ah:Llcr;

    iget v0, v0, Llcr;->d:I

    move v1, v0

    .line 442
    :goto_0
    iget-object v0, p0, Leku;->O:Landroid/widget/ListView;

    .line 443
    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 444
    invoke-virtual {v0, v1, v4, v1, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 446
    return-object p1

    .line 440
    :cond_0
    iget-object v0, p0, Leku;->ah:Llcr;

    iget v0, v0, Llcr;->f:I

    move v1, v0

    goto :goto_0
.end method

.method protected abstract d()V
.end method

.method protected d(I)V
    .locals 3

    .prologue
    .line 1047
    const/4 v0, 0x0

    .line 1048
    invoke-virtual {p0, p1}, Leku;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 1047
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 1050
    invoke-virtual {p0}, Leku;->p()Lae;

    move-result-object v1

    const-string v2, "pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 1051
    return-void
.end method

.method protected e(I)I
    .locals 1

    .prologue
    .line 1086
    invoke-virtual {p0}, Leku;->a()I

    move-result v0

    return v0
.end method

.method protected abstract e()V
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 476
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 477
    const-string v0, "force_cache"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 478
    const-string v0, "suggestion_type"

    iget v1, p0, Leku;->ag:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 479
    iget-object v0, p0, Leku;->aa:Ldib;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Leku;->aa:Ldib;

    invoke-virtual {v0, p1}, Ldib;->a(Landroid/os/Bundle;)V

    .line 482
    :cond_0
    iget-object v0, p0, Leku;->V:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 483
    const-string v0, "unblock_request_id"

    iget-object v1, p0, Leku;->V:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 485
    :cond_1
    return-void
.end method

.method public o(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 883
    iget-object v0, p0, Leku;->at:Llnl;

    iget-object v1, p0, Leku;->Z:Lhee;

    .line 884
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leku;->V:Ljava/lang/Integer;

    .line 885
    iget-object v0, p0, Leku;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Leku;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->bN:Lhmv;

    .line 886
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 885
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 887
    const v0, 0x7f0a0853

    invoke-virtual {p0, v0}, Leku;->d(I)V

    .line 888
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 616
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 617
    const v1, 0x7f100493

    if-ne v0, v1, :cond_1

    .line 618
    const v0, 0x7f1000ab

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 619
    packed-switch v0, :pswitch_data_0

    .line 634
    :cond_0
    :goto_0
    return-void

    .line 621
    :pswitch_0
    const v0, 0x7f1000ac

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 622
    iget-object v1, p0, Leku;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 623
    iget-object v2, p0, Leku;->at:Llnl;

    const/4 v3, 0x0

    invoke-static {v2, v1, v0, v3}, Leyq;->a(Landroid/content/Context;IILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 625
    invoke-virtual {p0, v0}, Leku;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 628
    :cond_1
    instance-of v0, p1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    if-eqz v0, :cond_0

    .line 629
    check-cast p1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 630
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->d()Ljava/lang/String;

    move-result-object v1

    .line 631
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a()Landroid/os/Bundle;

    move-result-object v2

    .line 632
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->e()I

    move-result v3

    invoke-virtual {p0, v3}, Leku;->e(I)I

    move-result v3

    .line 630
    invoke-virtual {p0, v0, v1, v2, v3}, Leku;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)V

    goto :goto_0

    .line 619
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 545
    instance-of v0, p1, Landroid/widget/AbsListView$RecyclerListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 546
    check-cast v0, Landroid/widget/AbsListView$RecyclerListener;

    invoke-interface {v0, p1}, Landroid/widget/AbsListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    .line 548
    :cond_0
    return-void
.end method

.method public z()V
    .locals 4

    .prologue
    .line 507
    invoke-super {p0}, Llol;->z()V

    .line 508
    iget-object v0, p0, Leku;->ap:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 510
    iget-object v0, p0, Leku;->ad:Lhxb;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lhxb;->a(Z)V

    .line 512
    iget-object v0, p0, Leku;->ae:Liax;

    iget-object v1, p0, Leku;->at:Llnl;

    iget-object v2, p0, Leku;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-virtual {p0}, Leku;->Z()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Liax;->a(Landroid/content/Context;II)V

    .line 513
    return-void
.end method
