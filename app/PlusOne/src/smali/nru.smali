.class public final Lnru;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnru;


# instance fields
.field public b:Lnsr;

.field public c:[Lnsh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2940
    const/4 v0, 0x0

    new-array v0, v0, [Lnru;

    sput-object v0, Lnru;->a:[Lnru;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2941
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2944
    const/4 v0, 0x0

    iput-object v0, p0, Lnru;->b:Lnsr;

    .line 2947
    sget-object v0, Lnsh;->a:[Lnsh;

    iput-object v0, p0, Lnru;->c:[Lnsh;

    .line 2941
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2968
    .line 2969
    iget-object v0, p0, Lnru;->b:Lnsr;

    if-eqz v0, :cond_2

    .line 2970
    const/4 v0, 0x1

    iget-object v2, p0, Lnru;->b:Lnsr;

    .line 2971
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2973
    :goto_0
    iget-object v2, p0, Lnru;->c:[Lnsh;

    if-eqz v2, :cond_1

    .line 2974
    iget-object v2, p0, Lnru;->c:[Lnsh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2975
    if-eqz v4, :cond_0

    .line 2976
    const/4 v5, 0x2

    .line 2977
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2974
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2981
    :cond_1
    iget-object v1, p0, Lnru;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2982
    iput v0, p0, Lnru;->ai:I

    .line 2983
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnru;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2991
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2992
    sparse-switch v0, :sswitch_data_0

    .line 2996
    iget-object v2, p0, Lnru;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2997
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnru;->ah:Ljava/util/List;

    .line 3000
    :cond_1
    iget-object v2, p0, Lnru;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3002
    :sswitch_0
    return-object p0

    .line 3007
    :sswitch_1
    iget-object v0, p0, Lnru;->b:Lnsr;

    if-nez v0, :cond_2

    .line 3008
    new-instance v0, Lnsr;

    invoke-direct {v0}, Lnsr;-><init>()V

    iput-object v0, p0, Lnru;->b:Lnsr;

    .line 3010
    :cond_2
    iget-object v0, p0, Lnru;->b:Lnsr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3014
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3015
    iget-object v0, p0, Lnru;->c:[Lnsh;

    if-nez v0, :cond_4

    move v0, v1

    .line 3016
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsh;

    .line 3017
    iget-object v3, p0, Lnru;->c:[Lnsh;

    if-eqz v3, :cond_3

    .line 3018
    iget-object v3, p0, Lnru;->c:[Lnsh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3020
    :cond_3
    iput-object v2, p0, Lnru;->c:[Lnsh;

    .line 3021
    :goto_2
    iget-object v2, p0, Lnru;->c:[Lnsh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 3022
    iget-object v2, p0, Lnru;->c:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 3023
    iget-object v2, p0, Lnru;->c:[Lnsh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3024
    invoke-virtual {p1}, Loxn;->a()I

    .line 3021
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3015
    :cond_4
    iget-object v0, p0, Lnru;->c:[Lnsh;

    array-length v0, v0

    goto :goto_1

    .line 3027
    :cond_5
    iget-object v2, p0, Lnru;->c:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 3028
    iget-object v2, p0, Lnru;->c:[Lnsh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2992
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2952
    iget-object v0, p0, Lnru;->b:Lnsr;

    if-eqz v0, :cond_0

    .line 2953
    const/4 v0, 0x1

    iget-object v1, p0, Lnru;->b:Lnsr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2955
    :cond_0
    iget-object v0, p0, Lnru;->c:[Lnsh;

    if-eqz v0, :cond_2

    .line 2956
    iget-object v1, p0, Lnru;->c:[Lnsh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2957
    if-eqz v3, :cond_1

    .line 2958
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2956
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2962
    :cond_2
    iget-object v0, p0, Lnru;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2964
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2937
    invoke-virtual {p0, p1}, Lnru;->a(Loxn;)Lnru;

    move-result-object v0

    return-object v0
.end method
