.class public final Lmgm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lndb;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28429
    invoke-direct {p0}, Loxq;-><init>()V

    .line 28432
    iput-object v0, p0, Lmgm;->apiHeader:Llyq;

    .line 28435
    iput-object v0, p0, Lmgm;->a:Lndb;

    .line 28429
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 28452
    const/4 v0, 0x0

    .line 28453
    iget-object v1, p0, Lmgm;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 28454
    const/4 v0, 0x1

    iget-object v1, p0, Lmgm;->apiHeader:Llyq;

    .line 28455
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 28457
    :cond_0
    iget-object v1, p0, Lmgm;->a:Lndb;

    if-eqz v1, :cond_1

    .line 28458
    const/4 v1, 0x2

    iget-object v2, p0, Lmgm;->a:Lndb;

    .line 28459
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28461
    :cond_1
    iget-object v1, p0, Lmgm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28462
    iput v0, p0, Lmgm;->ai:I

    .line 28463
    return v0
.end method

.method public a(Loxn;)Lmgm;
    .locals 2

    .prologue
    .line 28471
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 28472
    sparse-switch v0, :sswitch_data_0

    .line 28476
    iget-object v1, p0, Lmgm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 28477
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgm;->ah:Ljava/util/List;

    .line 28480
    :cond_1
    iget-object v1, p0, Lmgm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28482
    :sswitch_0
    return-object p0

    .line 28487
    :sswitch_1
    iget-object v0, p0, Lmgm;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 28488
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmgm;->apiHeader:Llyq;

    .line 28490
    :cond_2
    iget-object v0, p0, Lmgm;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28494
    :sswitch_2
    iget-object v0, p0, Lmgm;->a:Lndb;

    if-nez v0, :cond_3

    .line 28495
    new-instance v0, Lndb;

    invoke-direct {v0}, Lndb;-><init>()V

    iput-object v0, p0, Lmgm;->a:Lndb;

    .line 28497
    :cond_3
    iget-object v0, p0, Lmgm;->a:Lndb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28472
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28440
    iget-object v0, p0, Lmgm;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 28441
    const/4 v0, 0x1

    iget-object v1, p0, Lmgm;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28443
    :cond_0
    iget-object v0, p0, Lmgm;->a:Lndb;

    if-eqz v0, :cond_1

    .line 28444
    const/4 v0, 0x2

    iget-object v1, p0, Lmgm;->a:Lndb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28446
    :cond_1
    iget-object v0, p0, Lmgm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 28448
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 28425
    invoke-virtual {p0, p1}, Lmgm;->a(Loxn;)Lmgm;

    move-result-object v0

    return-object v0
.end method
