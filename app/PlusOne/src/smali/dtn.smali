.class final enum Ldtn;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldtn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldtn;

.field public static final enum b:Ldtn;

.field public static final enum c:Ldtn;

.field private static final synthetic d:[Ldtn;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144
    new-instance v0, Ldtn;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v2}, Ldtn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtn;->a:Ldtn;

    .line 145
    new-instance v0, Ldtn;

    const-string v1, "LIMIT"

    invoke-direct {v0, v1, v3}, Ldtn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtn;->b:Ldtn;

    .line 146
    new-instance v0, Ldtn;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v4}, Ldtn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtn;->c:Ldtn;

    .line 143
    const/4 v0, 0x3

    new-array v0, v0, [Ldtn;

    sget-object v1, Ldtn;->a:Ldtn;

    aput-object v1, v0, v2

    sget-object v1, Ldtn;->b:Ldtn;

    aput-object v1, v0, v3

    sget-object v1, Ldtn;->c:Ldtn;

    aput-object v1, v0, v4

    sput-object v0, Ldtn;->d:[Ldtn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldtn;
    .locals 1

    .prologue
    .line 143
    const-class v0, Ldtn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldtn;

    return-object v0
.end method

.method public static values()[Ldtn;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Ldtn;->d:[Ldtn;

    invoke-virtual {v0}, [Ldtn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldtn;

    return-object v0
.end method
