.class public final Lnfl;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/String;

.field private b:[Ljava/lang/String;

.field private c:[Lnfm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1570
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1672
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnfl;->a:[Ljava/lang/String;

    .line 1675
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnfl;->b:[Ljava/lang/String;

    .line 1678
    sget-object v0, Lnfm;->a:[Lnfm;

    iput-object v0, p0, Lnfl;->c:[Lnfm;

    .line 1570
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1706
    .line 1707
    iget-object v0, p0, Lnfl;->a:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lnfl;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1709
    iget-object v3, p0, Lnfl;->a:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 1711
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1709
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1714
    :cond_0
    iget-object v0, p0, Lnfl;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 1716
    :goto_1
    iget-object v2, p0, Lnfl;->b:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnfl;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1718
    iget-object v4, p0, Lnfl;->b:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 1720
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1718
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1722
    :cond_1
    add-int/2addr v0, v3

    .line 1723
    iget-object v2, p0, Lnfl;->b:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1725
    :cond_2
    iget-object v2, p0, Lnfl;->c:[Lnfm;

    if-eqz v2, :cond_4

    .line 1726
    iget-object v2, p0, Lnfl;->c:[Lnfm;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1727
    if-eqz v4, :cond_3

    .line 1728
    const/4 v5, 0x3

    .line 1729
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1726
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1733
    :cond_4
    iget-object v1, p0, Lnfl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1734
    iput v0, p0, Lnfl;->ai:I

    .line 1735
    return v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lnfl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1743
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1744
    sparse-switch v0, :sswitch_data_0

    .line 1748
    iget-object v2, p0, Lnfl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1749
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnfl;->ah:Ljava/util/List;

    .line 1752
    :cond_1
    iget-object v2, p0, Lnfl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1754
    :sswitch_0
    return-object p0

    .line 1759
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1760
    iget-object v0, p0, Lnfl;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 1761
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1762
    iget-object v3, p0, Lnfl;->a:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1763
    iput-object v2, p0, Lnfl;->a:[Ljava/lang/String;

    .line 1764
    :goto_1
    iget-object v2, p0, Lnfl;->a:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 1765
    iget-object v2, p0, Lnfl;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1766
    invoke-virtual {p1}, Loxn;->a()I

    .line 1764
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1769
    :cond_2
    iget-object v2, p0, Lnfl;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 1773
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1774
    iget-object v0, p0, Lnfl;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 1775
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1776
    iget-object v3, p0, Lnfl;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1777
    iput-object v2, p0, Lnfl;->b:[Ljava/lang/String;

    .line 1778
    :goto_2
    iget-object v2, p0, Lnfl;->b:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 1779
    iget-object v2, p0, Lnfl;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1780
    invoke-virtual {p1}, Loxn;->a()I

    .line 1778
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1783
    :cond_3
    iget-object v2, p0, Lnfl;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 1787
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1788
    iget-object v0, p0, Lnfl;->c:[Lnfm;

    if-nez v0, :cond_5

    move v0, v1

    .line 1789
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnfm;

    .line 1790
    iget-object v3, p0, Lnfl;->c:[Lnfm;

    if-eqz v3, :cond_4

    .line 1791
    iget-object v3, p0, Lnfl;->c:[Lnfm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1793
    :cond_4
    iput-object v2, p0, Lnfl;->c:[Lnfm;

    .line 1794
    :goto_4
    iget-object v2, p0, Lnfl;->c:[Lnfm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1795
    iget-object v2, p0, Lnfl;->c:[Lnfm;

    new-instance v3, Lnfm;

    invoke-direct {v3}, Lnfm;-><init>()V

    aput-object v3, v2, v0

    .line 1796
    iget-object v2, p0, Lnfl;->c:[Lnfm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1797
    invoke-virtual {p1}, Loxn;->a()I

    .line 1794
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1788
    :cond_5
    iget-object v0, p0, Lnfl;->c:[Lnfm;

    array-length v0, v0

    goto :goto_3

    .line 1800
    :cond_6
    iget-object v2, p0, Lnfl;->c:[Lnfm;

    new-instance v3, Lnfm;

    invoke-direct {v3}, Lnfm;-><init>()V

    aput-object v3, v2, v0

    .line 1801
    iget-object v2, p0, Lnfl;->c:[Lnfm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1744
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1683
    iget-object v1, p0, Lnfl;->a:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1684
    iget-object v2, p0, Lnfl;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1685
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1684
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1688
    :cond_0
    iget-object v1, p0, Lnfl;->b:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1689
    iget-object v2, p0, Lnfl;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1690
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1689
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1693
    :cond_1
    iget-object v1, p0, Lnfl;->c:[Lnfm;

    if-eqz v1, :cond_3

    .line 1694
    iget-object v1, p0, Lnfl;->c:[Lnfm;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1695
    if-eqz v3, :cond_2

    .line 1696
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1694
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1700
    :cond_3
    iget-object v0, p0, Lnfl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1702
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1566
    invoke-virtual {p0, p1}, Lnfl;->a(Loxn;)Lnfl;

    move-result-object v0

    return-object v0
.end method
