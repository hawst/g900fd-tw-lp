.class public final Lnmb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lnmb;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 276
    const v0, 0x296da23

    new-instance v1, Lnmc;

    invoke-direct {v1}, Lnmc;-><init>()V

    .line 281
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lnmb;->a:Loxr;

    .line 280
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 277
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 307
    const/4 v0, 0x0

    .line 308
    iget-object v1, p0, Lnmb;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 309
    const/4 v0, 0x1

    iget-object v1, p0, Lnmb;->b:Ljava/lang/String;

    .line 310
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 312
    :cond_0
    iget-object v1, p0, Lnmb;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 313
    const/4 v1, 0x2

    iget-object v2, p0, Lnmb;->c:Ljava/lang/String;

    .line 314
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 316
    :cond_1
    iget-object v1, p0, Lnmb;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 317
    const/4 v1, 0x3

    iget-object v2, p0, Lnmb;->d:Ljava/lang/String;

    .line 318
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 320
    :cond_2
    iget-object v1, p0, Lnmb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    iput v0, p0, Lnmb;->ai:I

    .line 322
    return v0
.end method

.method public a(Loxn;)Lnmb;
    .locals 2

    .prologue
    .line 330
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 331
    sparse-switch v0, :sswitch_data_0

    .line 335
    iget-object v1, p0, Lnmb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 336
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnmb;->ah:Ljava/util/List;

    .line 339
    :cond_1
    iget-object v1, p0, Lnmb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    :sswitch_0
    return-object p0

    .line 346
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmb;->b:Ljava/lang/String;

    goto :goto_0

    .line 350
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmb;->c:Ljava/lang/String;

    goto :goto_0

    .line 354
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmb;->d:Ljava/lang/String;

    goto :goto_0

    .line 331
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lnmb;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 293
    const/4 v0, 0x1

    iget-object v1, p0, Lnmb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 295
    :cond_0
    iget-object v0, p0, Lnmb;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 296
    const/4 v0, 0x2

    iget-object v1, p0, Lnmb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 298
    :cond_1
    iget-object v0, p0, Lnmb;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 299
    const/4 v0, 0x3

    iget-object v1, p0, Lnmb;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 301
    :cond_2
    iget-object v0, p0, Lnmb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 303
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0, p1}, Lnmb;->a(Loxn;)Lnmb;

    move-result-object v0

    return-object v0
.end method
