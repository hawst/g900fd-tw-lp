.class public Lahk;
.super Lloa;
.source "PG"


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private f:Z

.field private g:Z

.field private h:Ljava/util/concurrent/Executor;

.field private i:Landroid/os/HandlerThread;

.field private j:Landroid/os/Handler;

.field private k:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/lang/Runnable;

.field private final m:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lahk;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lahk;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Lloa;-><init>()V

    .line 62
    iput-boolean v0, p0, Lahk;->f:Z

    .line 64
    iput-boolean v0, p0, Lahk;->g:Z

    .line 85
    new-instance v0, Lahl;

    invoke-direct {v0, p0}, Lahl;-><init>(Lahk;)V

    iput-object v0, p0, Lahk;->l:Ljava/lang/Runnable;

    .line 100
    new-instance v0, Lahm;

    invoke-direct {v0, p0}, Lahm;-><init>(Lahk;)V

    iput-object v0, p0, Lahk;->m:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lahk;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lahk;->f:Z

    return v0
.end method

.method static synthetic a(Lahk;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lahk;->g:Z

    return p1
.end method

.method static synthetic b(Lahk;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lahk;->g:Z

    return v0
.end method

.method static synthetic c(Lahk;)Ljava/util/concurrent/FutureTask;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lahk;->k:Ljava/util/concurrent/FutureTask;

    return-object v0
.end method

.method static synthetic d(Lahk;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lahk;->l:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lahk;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 122
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 123
    iget-object v0, p0, Lahk;->x:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/16 v2, 0xab

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 124
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 128
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 130
    invoke-static {p0}, Lapm;->a(Landroid/content/Context;)Lapm;

    move-result-object v0

    invoke-virtual {v0}, Lapm;->a()Lamm;

    move-result-object v0

    .line 131
    invoke-interface {v0}, Lamm;->h()Ljava/util/concurrent/Executor;

    move-result-object v1

    iput-object v1, p0, Lahk;->h:Ljava/util/concurrent/Executor;

    .line 133
    new-instance v1, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Wait"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lahk;->i:Landroid/os/HandlerThread;

    .line 134
    iget-object v1, p0, Lahk;->i:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 136
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lahk;->i:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lahk;->j:Landroid/os/Handler;

    .line 138
    const/4 v1, 0x5

    new-array v1, v1, [Lalh;

    const/4 v2, 0x0

    .line 139
    invoke-interface {v0}, Lamm;->o()Laqx;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 140
    invoke-interface {v0}, Lamm;->p()Lanh;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 141
    invoke-interface {v0}, Lamm;->k()Lcdu;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 142
    invoke-interface {v0}, Lamm;->l()Lavj;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 143
    invoke-interface {v0}, Lamm;->m()Laxp;

    move-result-object v0

    aput-object v0, v1, v2

    .line 145
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v2, Lahn;

    invoke-direct {v2, v1}, Lahn;-><init>([Lalh;)V

    invoke-direct {v0, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Lahk;->k:Ljava/util/concurrent/FutureTask;

    .line 164
    iget-object v0, p0, Lahk;->h:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lahk;->k:Ljava/util/concurrent/FutureTask;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 165
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lahk;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->interrupt()V

    .line 170
    iget-object v0, p0, Lahk;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 171
    invoke-super {p0}, Lloa;->onDestroy()V

    .line 172
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lahk;->f:Z

    .line 185
    iget-object v0, p0, Lahk;->j:Landroid/os/Handler;

    iget-object v1, p0, Lahk;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 186
    invoke-super {p0}, Lloa;->onPause()V

    .line 187
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 176
    invoke-super {p0}, Lloa;->onResume()V

    .line 177
    const/4 v0, 0x0

    iput-boolean v0, p0, Lahk;->g:Z

    .line 178
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahk;->f:Z

    .line 179
    iget-object v0, p0, Lahk;->j:Landroid/os/Handler;

    iget-object v1, p0, Lahk;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 180
    return-void
.end method
