.class public final Lpvo;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpvo;


# instance fields
.field private b:Lpvq;

.field private c:Lpvp;

.field private d:I

.field private e:Lpvr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 794
    const/4 v0, 0x0

    new-array v0, v0, [Lpvo;

    sput-object v0, Lpvo;->a:[Lpvo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 795
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1035
    iput-object v1, p0, Lpvo;->b:Lpvq;

    .line 1038
    iput-object v1, p0, Lpvo;->c:Lpvp;

    .line 1041
    const/high16 v0, -0x80000000

    iput v0, p0, Lpvo;->d:I

    .line 1044
    iput-object v1, p0, Lpvo;->e:Lpvr;

    .line 795
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1067
    const/4 v0, 0x0

    .line 1068
    iget-object v1, p0, Lpvo;->b:Lpvq;

    if-eqz v1, :cond_0

    .line 1069
    const/4 v0, 0x1

    iget-object v1, p0, Lpvo;->b:Lpvq;

    .line 1070
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1072
    :cond_0
    iget-object v1, p0, Lpvo;->c:Lpvp;

    if-eqz v1, :cond_1

    .line 1073
    const/4 v1, 0x2

    iget-object v2, p0, Lpvo;->c:Lpvp;

    .line 1074
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1076
    :cond_1
    iget v1, p0, Lpvo;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 1077
    const/4 v1, 0x3

    iget v2, p0, Lpvo;->d:I

    .line 1078
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1080
    :cond_2
    iget-object v1, p0, Lpvo;->e:Lpvr;

    if-eqz v1, :cond_3

    .line 1081
    const/4 v1, 0x4

    iget-object v2, p0, Lpvo;->e:Lpvr;

    .line 1082
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1084
    :cond_3
    iget-object v1, p0, Lpvo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1085
    iput v0, p0, Lpvo;->ai:I

    .line 1086
    return v0
.end method

.method public a(Loxn;)Lpvo;
    .locals 2

    .prologue
    .line 1094
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1095
    sparse-switch v0, :sswitch_data_0

    .line 1099
    iget-object v1, p0, Lpvo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1100
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpvo;->ah:Ljava/util/List;

    .line 1103
    :cond_1
    iget-object v1, p0, Lpvo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1105
    :sswitch_0
    return-object p0

    .line 1110
    :sswitch_1
    iget-object v0, p0, Lpvo;->b:Lpvq;

    if-nez v0, :cond_2

    .line 1111
    new-instance v0, Lpvq;

    invoke-direct {v0}, Lpvq;-><init>()V

    iput-object v0, p0, Lpvo;->b:Lpvq;

    .line 1113
    :cond_2
    iget-object v0, p0, Lpvo;->b:Lpvq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1117
    :sswitch_2
    iget-object v0, p0, Lpvo;->c:Lpvp;

    if-nez v0, :cond_3

    .line 1118
    new-instance v0, Lpvp;

    invoke-direct {v0}, Lpvp;-><init>()V

    iput-object v0, p0, Lpvo;->c:Lpvp;

    .line 1120
    :cond_3
    iget-object v0, p0, Lpvo;->c:Lpvp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1124
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1125
    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 1129
    :cond_4
    iput v0, p0, Lpvo;->d:I

    goto :goto_0

    .line 1131
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Lpvo;->d:I

    goto :goto_0

    .line 1136
    :sswitch_4
    iget-object v0, p0, Lpvo;->e:Lpvr;

    if-nez v0, :cond_6

    .line 1137
    new-instance v0, Lpvr;

    invoke-direct {v0}, Lpvr;-><init>()V

    iput-object v0, p0, Lpvo;->e:Lpvr;

    .line 1139
    :cond_6
    iget-object v0, p0, Lpvo;->e:Lpvr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1095
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1049
    iget-object v0, p0, Lpvo;->b:Lpvq;

    if-eqz v0, :cond_0

    .line 1050
    const/4 v0, 0x1

    iget-object v1, p0, Lpvo;->b:Lpvq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1052
    :cond_0
    iget-object v0, p0, Lpvo;->c:Lpvp;

    if-eqz v0, :cond_1

    .line 1053
    const/4 v0, 0x2

    iget-object v1, p0, Lpvo;->c:Lpvp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1055
    :cond_1
    iget v0, p0, Lpvo;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 1056
    const/4 v0, 0x3

    iget v1, p0, Lpvo;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1058
    :cond_2
    iget-object v0, p0, Lpvo;->e:Lpvr;

    if-eqz v0, :cond_3

    .line 1059
    const/4 v0, 0x4

    iget-object v1, p0, Lpvo;->e:Lpvr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1061
    :cond_3
    iget-object v0, p0, Lpvo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1063
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 791
    invoke-virtual {p0, p1}, Lpvo;->a(Loxn;)Lpvo;

    move-result-object v0

    return-object v0
.end method
