.class public final Lokr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lokr;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2490
    const/4 v0, 0x0

    new-array v0, v0, [Lokr;

    sput-object v0, Lokr;->a:[Lokr;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2491
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2511
    const/4 v0, 0x1

    iget-object v1, p0, Lokr;->b:Ljava/lang/Integer;

    .line 2513
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2514
    const/4 v1, 0x2

    iget-object v2, p0, Lokr;->c:Ljava/lang/Integer;

    .line 2515
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2516
    const/4 v1, 0x3

    iget-object v2, p0, Lokr;->d:Ljava/lang/Integer;

    .line 2517
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2518
    iget-object v1, p0, Lokr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2519
    iput v0, p0, Lokr;->ai:I

    .line 2520
    return v0
.end method

.method public a(Loxn;)Lokr;
    .locals 2

    .prologue
    .line 2528
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2529
    sparse-switch v0, :sswitch_data_0

    .line 2533
    iget-object v1, p0, Lokr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2534
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lokr;->ah:Ljava/util/List;

    .line 2537
    :cond_1
    iget-object v1, p0, Lokr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2539
    :sswitch_0
    return-object p0

    .line 2544
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokr;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2548
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokr;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 2552
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokr;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2529
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2502
    const/4 v0, 0x1

    iget-object v1, p0, Lokr;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2503
    const/4 v0, 0x2

    iget-object v1, p0, Lokr;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2504
    const/4 v0, 0x3

    iget-object v1, p0, Lokr;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2505
    iget-object v0, p0, Lokr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2507
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2487
    invoke-virtual {p0, p1}, Lokr;->a(Loxn;)Lokr;

    move-result-object v0

    return-object v0
.end method
