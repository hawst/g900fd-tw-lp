.class public final Ldnb;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmks;",
        "Lmkt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 40
    const-string v3, "updatephotoalbum"

    new-instance v4, Lmks;

    invoke-direct {v4}, Lmks;-><init>()V

    new-instance v5, Lmkt;

    invoke-direct {v5}, Lmkt;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 43
    iput-object p3, p0, Ldnb;->p:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Ldnb;->a:Ljava/lang/String;

    .line 45
    iput-object p5, p0, Ldnb;->b:Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method protected a(Lmks;)V
    .locals 4

    .prologue
    .line 50
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 51
    iget-object v1, p0, Ldnb;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v1, p0, Ldnb;->f:Landroid/content/Context;

    iget v2, p0, Ldnb;->c:I

    invoke-static {v1, v2, v0}, Ljvj;->a(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 53
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljad;

    invoke-virtual {v0}, Ljad;->b()J

    move-result-wide v0

    .line 55
    new-instance v2, Lnex;

    invoke-direct {v2}, Lnex;-><init>()V

    iput-object v2, p1, Lmks;->a:Lnex;

    .line 56
    iget-object v2, p1, Lmks;->a:Lnex;

    .line 57
    iget-object v3, p0, Ldnb;->a:Ljava/lang/String;

    invoke-static {v3}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnex;->a:Ljava/lang/String;

    .line 58
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lnex;->b:Ljava/lang/String;

    .line 59
    return-void
.end method

.method protected a(Lmkt;)V
    .locals 4

    .prologue
    .line 63
    iget-object v0, p1, Lmkt;->a:Lnfy;

    .line 64
    iget-object v0, v0, Lnfy;->a:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lkfm;

    const-string v1, "Failed to set album cover photo"

    invoke-direct {v0, v1}, Lkfm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    iget-object v0, p0, Ldnb;->f:Landroid/content/Context;

    iget v1, p0, Ldnb;->c:I

    iget-object v2, p0, Ldnb;->p:Ljava/lang/String;

    iget-object v3, p0, Ldnb;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Ljvj;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmks;

    invoke-virtual {p0, p1}, Ldnb;->a(Lmks;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmkt;

    invoke-virtual {p0, p1}, Ldnb;->a(Lmkt;)V

    return-void
.end method
