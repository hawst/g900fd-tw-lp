.class public final Lfb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lfh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    invoke-static {}, Lfb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    new-instance v0, Lfd;

    invoke-direct {v0, p1}, Lfd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfb;->a:Lfh;

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_0
    new-instance v0, Lfg;

    invoke-direct {v0}, Lfg;-><init>()V

    iput-object v0, p0, Lfb;->a:Lfh;

    goto :goto_0
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 81
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 83
    const/4 v0, 0x1

    .line 85
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lfb;->a:Lfh;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lfh;->a(Ljava/lang/String;Landroid/graphics/Bitmap;Lfc;)V

    .line 313
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lfb;->a:Lfh;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lfh;->a(Ljava/lang/String;Landroid/net/Uri;Lfc;)V

    .line 337
    return-void
.end method
