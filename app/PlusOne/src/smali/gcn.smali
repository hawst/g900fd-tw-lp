.class public final Lgcn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lnhm;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgcp;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lizs;

.field private f:Lhso;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lgcn;->a:Landroid/content/Context;

    .line 94
    const-string v0, "appWidgetId"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgcn;->b:I

    .line 96
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgcn;->c:Ljava/util/HashMap;

    .line 97
    return-void
.end method

.method private a(Lgcp;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 122
    :try_start_0
    iget-object v0, p0, Lgcn;->a:Landroid/content/Context;

    iget-object v1, p1, Lgcp;->d:Lnij;

    iget-object v1, v1, Lnij;->h:Ljava/lang/String;

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v0, v1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 124
    iget-object v0, p0, Lgcn;->e:Lizs;

    const/4 v2, 0x0

    const/16 v3, 0x122

    const/16 v4, 0xd2

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    .line 136
    :goto_0
    return-object v0

    .line 131
    :catch_0
    move-exception v0

    const-string v0, "Cannot download map"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v6

    .line 132
    goto :goto_0

    .line 134
    :catch_1
    move-exception v0

    move-object v0, v6

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lnhm;)Lgcp;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 318
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 329
    :cond_0
    :goto_0
    return-object v0

    .line 321
    :cond_1
    new-instance v1, Lgcp;

    invoke-direct {v1}, Lgcp;-><init>()V

    .line 322
    iget-object v2, p2, Lnhm;->c:[Lnij;

    invoke-static {v2}, Liuo;->a([Lnij;)Lnij;

    move-result-object v2

    iput-object v2, v1, Lgcp;->d:Lnij;

    .line 323
    iget-object v2, v1, Lgcp;->d:Lnij;

    if-eqz v2, :cond_0

    .line 326
    iput-object p1, v1, Lgcp;->a:Ljava/lang/String;

    .line 327
    iget-object v0, p2, Lnhm;->d:Ljava/lang/String;

    iput-object v0, v1, Lgcp;->b:Ljava/lang/String;

    .line 328
    iget-object v0, p2, Lnhm;->e:Ljava/lang/String;

    invoke-static {v0}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lgcp;->c:Ljava/lang/String;

    move-object v0, v1

    .line 329
    goto :goto_0
.end method

.method private a(ILjava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lgcp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 333
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "gaia_id"

    aput-object v0, v3, v1

    .line 336
    iget-object v0, p0, Lgcn;->a:Landroid/content/Context;

    .line 339
    iget-object v1, p0, Lgcn;->c:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgcn;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const-string v4, "0"

    :goto_0
    move v1, p1

    move-object v2, p2

    move-object v6, v5

    .line 338
    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 342
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 344
    :cond_1
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 345
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 347
    iget-object v0, p0, Lgcn;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    invoke-direct {p0, v3, v0}, Lgcn;->a(Ljava/lang/String;Lnhm;)Lgcp;

    move-result-object v0

    .line 348
    if-eqz v0, :cond_1

    .line 349
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 353
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 339
    :cond_2
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "gaia_id IN ("

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgcn;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    const/16 v1, 0x2c

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lgcn;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_4

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_4
    const/16 v1, 0x29

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 353
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 355
    return-object v2
.end method

.method static synthetic a(Lgcn;ILjava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lgcn;->a(ILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lgcn;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lgcn;->d:Ljava/util/List;

    return-object p1
.end method

.method private static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 87
    const-string v0, "LWStackViewsFactory"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 90
    :cond_0
    return-void
.end method

.method private b(Lgcp;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 142
    :try_start_0
    iget-object v0, p0, Lgcn;->f:Lhso;

    iget-object v2, p1, Lgcp;->c:Ljava/lang/String;

    const/4 v3, 0x2

    const/4 v4, 0x2

    invoke-interface {v0, v2, v3, v4}, Lhso;->a(Ljava/lang/String;II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lkdn; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lkde; {:try_start_0 .. :try_end_0} :catch_1

    .line 151
    :goto_0
    return-object v0

    .line 146
    :catch_0
    move-exception v0

    const-string v0, "Cannot download avatar"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 147
    goto :goto_0

    .line 149
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lgcn;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x0

    .line 116
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lgcn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lgcn;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcp;

    .line 213
    iget-object v0, v0, Lgcp;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 2

    .prologue
    .line 200
    const-string v0, "getLoadingView"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 156
    const-string v0, "getViewAt(%d)"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v2}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    iget-object v0, p0, Lgcn;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgcn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 161
    :cond_0
    const-string v0, "Invalid mWidgetItems when getCount() = %d"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lgcn;->getCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v2}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 195
    :goto_0
    return-object v0

    .line 166
    :cond_1
    iget-object v0, p0, Lgcn;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcp;

    .line 167
    const-string v2, "%s, %s, %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Lgcp;->b:Ljava/lang/String;

    aput-object v4, v3, v8

    iget-object v4, v0, Lgcp;->d:Lnij;

    iget-object v4, v4, Lnij;->g:Ljava/lang/String;

    aput-object v4, v3, v9

    iget-object v4, v0, Lgcp;->c:Ljava/lang/String;

    aput-object v4, v3, v10

    invoke-static {v2, v3}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169
    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v3, p0, Lgcn;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0400fe

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 171
    const v3, 0x7f10035d

    iget-object v4, v0, Lgcp;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 172
    const v3, 0x7f10035e

    iget-object v4, v0, Lgcp;->d:Lnij;

    iget-object v4, v4, Lnij;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 173
    const v3, 0x7f10035f

    iget-object v4, p0, Lgcn;->a:Landroid/content/Context;

    iget-object v5, v0, Lgcp;->d:Lnij;

    iget-object v5, v5, Lnij;->e:Ljava/lang/Long;

    .line 174
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lgcq;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 173
    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 176
    invoke-direct {p0, v0}, Lgcn;->a(Lgcp;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 177
    if-nez v3, :cond_2

    .line 178
    const-string v2, "Error downloading map (%d): %s"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    iget-object v0, v0, Lgcp;->b:Ljava/lang/String;

    aput-object v0, v3, v9

    invoke-static {v2, v3}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 179
    goto :goto_0

    .line 181
    :cond_2
    invoke-direct {p0, v0}, Lgcn;->b(Lgcp;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 182
    const v4, 0x7f100356

    invoke-virtual {v2, v4, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 183
    const v3, 0x7f10035c

    invoke-virtual {v2, v3, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 187
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 188
    const-string v3, "appWidgetId"

    iget v4, p0, Lgcn;->b:I

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 189
    const-string v3, "gaia_id"

    iget-object v0, v0, Lgcp;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    const v0, 0x7f100354

    invoke-virtual {v2, v0, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 192
    const-string v0, "getViewAt(%d) finishing"

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v8

    invoke-static {v0, v1}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v2

    .line 195
    goto/16 :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 101
    const-string v0, "onCreate"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lgcn;->a:Landroid/content/Context;

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iput-object v0, p0, Lgcn;->e:Lizs;

    .line 103
    iget-object v0, p0, Lgcn;->a:Landroid/content/Context;

    const-class v1, Lhso;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    iput-object v0, p0, Lgcn;->f:Lhso;

    .line 104
    return-void
.end method

.method public onDataSetChanged()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 227
    const-string v0, "onDataSetChanged"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    iget-object v0, p0, Lgcn;->a:Landroid/content/Context;

    iget v2, p0, Lgcn;->b:I

    invoke-static {v0, v2}, Lgcq;->a(Landroid/content/Context;I)Lgcr;

    move-result-object v2

    .line 232
    if-nez v2, :cond_0

    .line 233
    iget-object v0, p0, Lgcn;->a:Landroid/content/Context;

    iget v1, p0, Lgcn;->b:I

    invoke-static {v0, v1, v8}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetProvider;->a(Landroid/content/Context;IZ)V

    .line 292
    :goto_0
    return-void

    .line 237
    :cond_0
    iget v0, v2, Lgcr;->a:I

    .line 239
    const-string v3, "Configuration OK. Getting friends sharing."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 242
    new-instance v3, Litx;

    iget-object v4, p0, Lgcn;->a:Landroid/content/Context;

    invoke-direct {v3, v4, v0, v1, v1}, Litx;-><init>(Landroid/content/Context;IIZ)V

    .line 245
    invoke-virtual {v3}, Litx;->l()V

    .line 246
    invoke-virtual {v3}, Litx;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lgcn;->a:Landroid/content/Context;

    iget v1, p0, Lgcn;->b:I

    invoke-static {v0, v1, v8}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetProvider;->a(Landroid/content/Context;IZ)V

    goto :goto_0

    .line 253
    :cond_1
    invoke-virtual {v3}, Litx;->i()[Lnhm;

    move-result-object v3

    .line 255
    iget-object v0, p0, Lgcn;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 256
    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 257
    iget-object v6, p0, Lgcn;->c:Ljava/util/HashMap;

    iget-object v7, v5, Lnhm;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 260
    :cond_2
    const-string v0, "%d friends currently sharing"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lgcn;->c:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 262
    iget-object v0, v2, Lgcr;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 263
    iget-object v0, p0, Lgcn;->c:Ljava/util/HashMap;

    iget-object v1, v2, Lgcr;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 264
    iget-object v1, v2, Lgcr;->c:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lgcn;->a(Ljava/lang/String;Lnhm;)Lgcp;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_3

    .line 266
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgcn;->d:Ljava/util/List;

    .line 267
    iget-object v1, p0, Lgcn;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    :cond_3
    :goto_2
    iget-object v0, p0, Lgcn;->a:Landroid/content/Context;

    iget v1, p0, Lgcn;->b:I

    invoke-static {v0, v1, v8}, Lcom/google/android/apps/plus/widget/locations/LocationsWidgetProvider;->a(Landroid/content/Context;IZ)V

    goto :goto_0

    .line 269
    :cond_4
    iget-object v0, v2, Lgcr;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 276
    new-instance v0, Lgco;

    invoke-direct {v0, p0, v2}, Lgco;-><init>(Lgcn;Lgcr;)V

    .line 282
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 284
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :goto_3
    const-string v0, "%d friends in selected circle"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lgcn;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 108
    const-string v0, "onDestroy"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lgcn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    return-void
.end method
