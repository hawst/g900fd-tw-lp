.class public final Lmks;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnex;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25027
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25030
    iput-object v0, p0, Lmks;->apiHeader:Llyq;

    .line 25033
    iput-object v0, p0, Lmks;->a:Lnex;

    .line 25027
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 25050
    const/4 v0, 0x0

    .line 25051
    iget-object v1, p0, Lmks;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 25052
    const/4 v0, 0x1

    iget-object v1, p0, Lmks;->apiHeader:Llyq;

    .line 25053
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25055
    :cond_0
    iget-object v1, p0, Lmks;->a:Lnex;

    if-eqz v1, :cond_1

    .line 25056
    const/4 v1, 0x2

    iget-object v2, p0, Lmks;->a:Lnex;

    .line 25057
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25059
    :cond_1
    iget-object v1, p0, Lmks;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25060
    iput v0, p0, Lmks;->ai:I

    .line 25061
    return v0
.end method

.method public a(Loxn;)Lmks;
    .locals 2

    .prologue
    .line 25069
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 25070
    sparse-switch v0, :sswitch_data_0

    .line 25074
    iget-object v1, p0, Lmks;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 25075
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmks;->ah:Ljava/util/List;

    .line 25078
    :cond_1
    iget-object v1, p0, Lmks;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25080
    :sswitch_0
    return-object p0

    .line 25085
    :sswitch_1
    iget-object v0, p0, Lmks;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 25086
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmks;->apiHeader:Llyq;

    .line 25088
    :cond_2
    iget-object v0, p0, Lmks;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25092
    :sswitch_2
    iget-object v0, p0, Lmks;->a:Lnex;

    if-nez v0, :cond_3

    .line 25093
    new-instance v0, Lnex;

    invoke-direct {v0}, Lnex;-><init>()V

    iput-object v0, p0, Lmks;->a:Lnex;

    .line 25095
    :cond_3
    iget-object v0, p0, Lmks;->a:Lnex;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25070
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25038
    iget-object v0, p0, Lmks;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 25039
    const/4 v0, 0x1

    iget-object v1, p0, Lmks;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25041
    :cond_0
    iget-object v0, p0, Lmks;->a:Lnex;

    if-eqz v0, :cond_1

    .line 25042
    const/4 v0, 0x2

    iget-object v1, p0, Lmks;->a:Lnex;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25044
    :cond_1
    iget-object v0, p0, Lmks;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 25046
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 25023
    invoke-virtual {p0, p1}, Lmks;->a(Loxn;)Lmks;

    move-result-object v0

    return-object v0
.end method
