.class public final Lksb;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lksb;->a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/16 v6, 0x15

    const/4 v2, 0x0

    const/high16 v5, 0x42f00000    # 120.0f

    const/4 v1, 0x1

    .line 92
    iget-object v0, p0, Lksb;->a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 93
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_2

    .line 94
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    .line 95
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    cmpl-float v3, v3, v5

    if-lez v3, :cond_3

    .line 102
    :goto_1
    if-eqz v0, :cond_0

    iget-object v2, p0, Lksb;->a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-static {v2}, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;->a(Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;)Lkrs;

    move-result-object v2

    invoke-interface {v2}, Lkrs;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lksb;->a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-static {v2, v6}, Lhly;->a(Landroid/view/View;I)V

    iget-object v2, p0, Lksb;->a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-static {v2}, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;->a(Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;)Lkrs;

    move-result-object v2

    invoke-interface {v2}, Lkrs;->q()V

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lksb;->a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;->a(Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;)Lkrs;

    move-result-object v0

    invoke-interface {v0}, Lkrs;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lksb;->a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-static {v0, v6}, Lhly;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lksb;->a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;->a(Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;)Lkrs;

    move-result-object v0

    invoke-interface {v0}, Lkrs;->r()V

    .line 104
    :cond_1
    return v1

    :cond_2
    move v0, v2

    .line 94
    goto :goto_0

    .line 99
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    cmpl-float v3, v3, v5

    if-lez v3, :cond_1

    .line 102
    if-nez v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 125
    iget-object v0, p0, Lksb;->a:Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;->a(Lcom/google/android/libraries/social/socialcast/impl/SocialCastMediaView;Z)Z

    .line 126
    return v1
.end method
