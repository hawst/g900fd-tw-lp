.class public final Lcup;
.super Lcqi;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/photos/service/PhotosService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/photos/service/PhotosService;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcqi;-><init>()V

    .line 74
    iput-object p1, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    .line 75
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    invoke-virtual {v0}, Lcom/google/android/apps/photos/service/PhotosService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 79
    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    const-string v2, "com.google.android.gms"

    invoke-static {v0, v2}, Lcom/google/android/apps/photos/service/PhotosService;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    const-class v2, Lija;

    .line 82
    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lija;

    .line 84
    const-string v2, "com.google.android.gms"

    invoke-interface {v0, v1, v2}, Lija;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 85
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 7

    .prologue
    .line 161
    invoke-direct {p0}, Lcup;->i()V

    .line 162
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 164
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    const-class v4, Lcom/google/android/apps/plus/phone/AutoBackupPromoActivity;

    invoke-direct {v1, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165
    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    const-class v4, Lihf;

    .line 166
    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lihf;

    .line 168
    iget-object v4, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    new-instance v5, Lihg;

    const/4 v6, 0x0

    invoke-direct {v5, p1, v6}, Lihg;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v4, v1, v5}, Lihf;->a(Landroid/content/Context;Landroid/content/Intent;Lihg;)V

    .line 169
    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    const/4 v4, 0x0

    const/high16 v5, 0x8000000

    invoke-static {v0, v4, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 172
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public a()Z
    .locals 4

    .prologue
    .line 91
    invoke-direct {p0}, Lcup;->i()V

    .line 92
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 94
    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    const-class v1, Lhpu;

    .line 95
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 98
    :try_start_0
    invoke-virtual {v0}, Lhpu;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 100
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 106
    invoke-direct {p0}, Lcup;->i()V

    .line 107
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 111
    :try_start_0
    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    const-class v1, Lhpu;

    .line 112
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 115
    invoke-virtual {v0}, Lhpu;->e()Ljava/util/List;

    move-result-object v1

    .line 116
    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 117
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 124
    :goto_0
    return-object v2

    .line 119
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    .line 120
    const-class v3, Lhei;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const/4 v3, 0x0

    .line 121
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_1

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 124
    :goto_1
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move-object v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 122
    goto :goto_1

    .line 124
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public c()Landroid/app/PendingIntent;
    .locals 6

    .prologue
    .line 148
    invoke-direct {p0}, Lcup;->i()V

    .line 149
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 151
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    const-class v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    iget-object v1, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    const/4 v4, 0x0

    const/high16 v5, 0x8000000

    invoke-static {v1, v4, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 155
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public d()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    invoke-direct {p0}, Lcup;->i()V

    .line 179
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 181
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Landroid/os/Bundle;

    const/4 v1, 0x0

    iget-object v4, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    invoke-static {v4}, Lhrz;->a(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 183
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 189
    invoke-direct {p0}, Lcup;->i()V

    .line 190
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 192
    :try_start_0
    iget-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    invoke-static {v0}, Lhrz;->b(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 195
    return-void

    .line 194
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public g()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcup;->a:Lcom/google/android/apps/photos/service/PhotosService;

    .line 200
    return-void
.end method
