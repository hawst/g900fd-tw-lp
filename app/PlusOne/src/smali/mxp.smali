.class public final Lmxp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmxp;


# instance fields
.field public b:Ljava/lang/String;

.field public c:I

.field public d:Lmxq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    new-array v0, v0, [Lmxp;

    sput-object v0, Lmxp;->a:[Lmxp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 255
    invoke-direct {p0}, Loxq;-><init>()V

    .line 367
    const/high16 v0, -0x80000000

    iput v0, p0, Lmxp;->c:I

    .line 370
    const/4 v0, 0x0

    iput-object v0, p0, Lmxp;->d:Lmxq;

    .line 255
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 390
    const/4 v0, 0x0

    .line 391
    iget-object v1, p0, Lmxp;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 392
    const/4 v0, 0x1

    iget-object v1, p0, Lmxp;->b:Ljava/lang/String;

    .line 393
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 395
    :cond_0
    iget v1, p0, Lmxp;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 396
    const/4 v1, 0x2

    iget v2, p0, Lmxp;->c:I

    .line 397
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 399
    :cond_1
    iget-object v1, p0, Lmxp;->d:Lmxq;

    if-eqz v1, :cond_2

    .line 400
    const/4 v1, 0x3

    iget-object v2, p0, Lmxp;->d:Lmxq;

    .line 401
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 403
    :cond_2
    iget-object v1, p0, Lmxp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    iput v0, p0, Lmxp;->ai:I

    .line 405
    return v0
.end method

.method public a(Loxn;)Lmxp;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 413
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 414
    sparse-switch v0, :sswitch_data_0

    .line 418
    iget-object v1, p0, Lmxp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 419
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmxp;->ah:Ljava/util/List;

    .line 422
    :cond_1
    iget-object v1, p0, Lmxp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    :sswitch_0
    return-object p0

    .line 429
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmxp;->b:Ljava/lang/String;

    goto :goto_0

    .line 433
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 434
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 439
    :cond_2
    iput v0, p0, Lmxp;->c:I

    goto :goto_0

    .line 441
    :cond_3
    iput v2, p0, Lmxp;->c:I

    goto :goto_0

    .line 446
    :sswitch_3
    iget-object v0, p0, Lmxp;->d:Lmxq;

    if-nez v0, :cond_4

    .line 447
    new-instance v0, Lmxq;

    invoke-direct {v0}, Lmxq;-><init>()V

    iput-object v0, p0, Lmxp;->d:Lmxq;

    .line 449
    :cond_4
    iget-object v0, p0, Lmxp;->d:Lmxq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 414
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lmxp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 376
    const/4 v0, 0x1

    iget-object v1, p0, Lmxp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 378
    :cond_0
    iget v0, p0, Lmxp;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 379
    const/4 v0, 0x2

    iget v1, p0, Lmxp;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 381
    :cond_1
    iget-object v0, p0, Lmxp;->d:Lmxq;

    if-eqz v0, :cond_2

    .line 382
    const/4 v0, 0x3

    iget-object v1, p0, Lmxp;->d:Lmxq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 384
    :cond_2
    iget-object v0, p0, Lmxp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 386
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 251
    invoke-virtual {p0, p1}, Lmxp;->a(Loxn;)Lmxp;

    move-result-object v0

    return-object v0
.end method
