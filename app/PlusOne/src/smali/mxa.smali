.class public final Lmxa;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmwe;

.field public b:Lmtp;

.field public c:Lmtr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12997
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13000
    iput-object v0, p0, Lmxa;->a:Lmwe;

    .line 13003
    iput-object v0, p0, Lmxa;->b:Lmtp;

    .line 13006
    iput-object v0, p0, Lmxa;->c:Lmtr;

    .line 12997
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 13026
    const/4 v0, 0x0

    .line 13027
    iget-object v1, p0, Lmxa;->a:Lmwe;

    if-eqz v1, :cond_0

    .line 13028
    const/4 v0, 0x1

    iget-object v1, p0, Lmxa;->a:Lmwe;

    .line 13029
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 13031
    :cond_0
    iget-object v1, p0, Lmxa;->b:Lmtp;

    if-eqz v1, :cond_1

    .line 13032
    const/4 v1, 0x2

    iget-object v2, p0, Lmxa;->b:Lmtp;

    .line 13033
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13035
    :cond_1
    iget-object v1, p0, Lmxa;->c:Lmtr;

    if-eqz v1, :cond_2

    .line 13036
    const/4 v1, 0x3

    iget-object v2, p0, Lmxa;->c:Lmtr;

    .line 13037
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13039
    :cond_2
    iget-object v1, p0, Lmxa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13040
    iput v0, p0, Lmxa;->ai:I

    .line 13041
    return v0
.end method

.method public a(Loxn;)Lmxa;
    .locals 2

    .prologue
    .line 13049
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 13050
    sparse-switch v0, :sswitch_data_0

    .line 13054
    iget-object v1, p0, Lmxa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 13055
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmxa;->ah:Ljava/util/List;

    .line 13058
    :cond_1
    iget-object v1, p0, Lmxa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13060
    :sswitch_0
    return-object p0

    .line 13065
    :sswitch_1
    iget-object v0, p0, Lmxa;->a:Lmwe;

    if-nez v0, :cond_2

    .line 13066
    new-instance v0, Lmwe;

    invoke-direct {v0}, Lmwe;-><init>()V

    iput-object v0, p0, Lmxa;->a:Lmwe;

    .line 13068
    :cond_2
    iget-object v0, p0, Lmxa;->a:Lmwe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 13072
    :sswitch_2
    iget-object v0, p0, Lmxa;->b:Lmtp;

    if-nez v0, :cond_3

    .line 13073
    new-instance v0, Lmtp;

    invoke-direct {v0}, Lmtp;-><init>()V

    iput-object v0, p0, Lmxa;->b:Lmtp;

    .line 13075
    :cond_3
    iget-object v0, p0, Lmxa;->b:Lmtp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 13079
    :sswitch_3
    iget-object v0, p0, Lmxa;->c:Lmtr;

    if-nez v0, :cond_4

    .line 13080
    new-instance v0, Lmtr;

    invoke-direct {v0}, Lmtr;-><init>()V

    iput-object v0, p0, Lmxa;->c:Lmtr;

    .line 13082
    :cond_4
    iget-object v0, p0, Lmxa;->c:Lmtr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 13050
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 13011
    iget-object v0, p0, Lmxa;->a:Lmwe;

    if-eqz v0, :cond_0

    .line 13012
    const/4 v0, 0x1

    iget-object v1, p0, Lmxa;->a:Lmwe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 13014
    :cond_0
    iget-object v0, p0, Lmxa;->b:Lmtp;

    if-eqz v0, :cond_1

    .line 13015
    const/4 v0, 0x2

    iget-object v1, p0, Lmxa;->b:Lmtp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 13017
    :cond_1
    iget-object v0, p0, Lmxa;->c:Lmtr;

    if-eqz v0, :cond_2

    .line 13018
    const/4 v0, 0x3

    iget-object v1, p0, Lmxa;->c:Lmtr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 13020
    :cond_2
    iget-object v0, p0, Lmxa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 13022
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12993
    invoke-virtual {p0, p1}, Lmxa;->a(Loxn;)Lmxa;

    move-result-object v0

    return-object v0
.end method
