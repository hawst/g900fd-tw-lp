.class public final Levf;
.super Lfbs;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lfbs;-><init>(Landroid/content/Context;I)V

    .line 25
    const v0, 0x7f0a0b09

    iput v0, p0, Levf;->a:I

    .line 26
    const v0, 0x7f0a08ac

    iput v0, p0, Levf;->b:I

    .line 27
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 31
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 32
    const v1, 0x7f04004e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 37
    check-cast p2, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;

    .line 39
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 40
    const/16 v1, 0x9

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 41
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 42
    const/4 v4, 0x5

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 43
    invoke-static {v2, v3}, Ljvj;->a(J)Ljac;

    move-result-object v5

    .line 44
    iget-object v6, p0, Levf;->k:Landroid/content/Context;

    invoke-static {v6, v1, v4, v5}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 45
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 47
    const v5, 0x7f10007f

    invoke-virtual {p2, v5, v4}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->setTag(ILjava/lang/Object;)V

    .line 48
    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->a(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->a(Lizu;)V

    .line 50
    invoke-virtual {p0}, Levf;->c()Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    const v0, 0x7f100093

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->setTag(ILjava/lang/Object;)V

    .line 55
    new-instance v0, Llka;

    const/4 v1, -0x3

    .line 57
    invoke-virtual {p0}, Levf;->b()I

    move-result v2

    invoke-direct {v0, v8, v1, v2, v7}, Llka;-><init>(IIII)V

    .line 58
    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    return-void
.end method
