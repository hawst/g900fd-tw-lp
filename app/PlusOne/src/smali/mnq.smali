.class public final Lmnq;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lmod;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3834
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3843
    const/high16 v0, -0x80000000

    iput v0, p0, Lmnq;->a:I

    .line 3846
    const/4 v0, 0x0

    iput-object v0, p0, Lmnq;->b:Lmod;

    .line 3834
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3863
    const/4 v0, 0x0

    .line 3864
    iget v1, p0, Lmnq;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 3865
    const/4 v0, 0x1

    iget v1, p0, Lmnq;->a:I

    .line 3866
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3868
    :cond_0
    iget-object v1, p0, Lmnq;->b:Lmod;

    if-eqz v1, :cond_1

    .line 3869
    const/4 v1, 0x2

    iget-object v2, p0, Lmnq;->b:Lmod;

    .line 3870
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3872
    :cond_1
    iget-object v1, p0, Lmnq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3873
    iput v0, p0, Lmnq;->ai:I

    .line 3874
    return v0
.end method

.method public a(Loxn;)Lmnq;
    .locals 2

    .prologue
    .line 3882
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3883
    sparse-switch v0, :sswitch_data_0

    .line 3887
    iget-object v1, p0, Lmnq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3888
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnq;->ah:Ljava/util/List;

    .line 3891
    :cond_1
    iget-object v1, p0, Lmnq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3893
    :sswitch_0
    return-object p0

    .line 3898
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3899
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 3902
    :cond_2
    iput v0, p0, Lmnq;->a:I

    goto :goto_0

    .line 3904
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmnq;->a:I

    goto :goto_0

    .line 3909
    :sswitch_2
    iget-object v0, p0, Lmnq;->b:Lmod;

    if-nez v0, :cond_4

    .line 3910
    new-instance v0, Lmod;

    invoke-direct {v0}, Lmod;-><init>()V

    iput-object v0, p0, Lmnq;->b:Lmod;

    .line 3912
    :cond_4
    iget-object v0, p0, Lmnq;->b:Lmod;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3883
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3851
    iget v0, p0, Lmnq;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 3852
    const/4 v0, 0x1

    iget v1, p0, Lmnq;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3854
    :cond_0
    iget-object v0, p0, Lmnq;->b:Lmod;

    if-eqz v0, :cond_1

    .line 3855
    const/4 v0, 0x2

    iget-object v1, p0, Lmnq;->b:Lmod;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3857
    :cond_1
    iget-object v0, p0, Lmnq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3859
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3830
    invoke-virtual {p0, p1}, Lmnq;->a(Loxn;)Lmnq;

    move-result-object v0

    return-object v0
.end method
