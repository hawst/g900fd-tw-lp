.class public final Lpbe;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1846
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1892
    const/4 v0, 0x0

    .line 1893
    iget-object v1, p0, Lpbe;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 1894
    const/4 v0, 0x1

    iget-object v1, p0, Lpbe;->a:Ljava/lang/Boolean;

    .line 1895
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1897
    :cond_0
    iget-object v1, p0, Lpbe;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1898
    const/4 v1, 0x2

    iget-object v2, p0, Lpbe;->b:Ljava/lang/Boolean;

    .line 1899
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1901
    :cond_1
    iget-object v1, p0, Lpbe;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1902
    const/4 v1, 0x3

    iget-object v2, p0, Lpbe;->c:Ljava/lang/Boolean;

    .line 1903
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1905
    :cond_2
    iget-object v1, p0, Lpbe;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1906
    const/4 v1, 0x4

    iget-object v2, p0, Lpbe;->d:Ljava/lang/Boolean;

    .line 1907
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1909
    :cond_3
    iget-object v1, p0, Lpbe;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1910
    const/4 v1, 0x5

    iget-object v2, p0, Lpbe;->e:Ljava/lang/Boolean;

    .line 1911
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1913
    :cond_4
    iget-object v1, p0, Lpbe;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 1914
    const/4 v1, 0x6

    iget-object v2, p0, Lpbe;->f:Ljava/lang/Boolean;

    .line 1915
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1917
    :cond_5
    iget-object v1, p0, Lpbe;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 1918
    const/4 v1, 0x7

    iget-object v2, p0, Lpbe;->g:Ljava/lang/Boolean;

    .line 1919
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1921
    :cond_6
    iget-object v1, p0, Lpbe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1922
    iput v0, p0, Lpbe;->ai:I

    .line 1923
    return v0
.end method

.method public a(Loxn;)Lpbe;
    .locals 2

    .prologue
    .line 1931
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1932
    sparse-switch v0, :sswitch_data_0

    .line 1936
    iget-object v1, p0, Lpbe;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1937
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpbe;->ah:Ljava/util/List;

    .line 1940
    :cond_1
    iget-object v1, p0, Lpbe;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1942
    :sswitch_0
    return-object p0

    .line 1947
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbe;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 1951
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbe;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1955
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbe;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1959
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbe;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 1963
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbe;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 1967
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbe;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 1971
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbe;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 1932
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1865
    iget-object v0, p0, Lpbe;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1866
    const/4 v0, 0x1

    iget-object v1, p0, Lpbe;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1868
    :cond_0
    iget-object v0, p0, Lpbe;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1869
    const/4 v0, 0x2

    iget-object v1, p0, Lpbe;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1871
    :cond_1
    iget-object v0, p0, Lpbe;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1872
    const/4 v0, 0x3

    iget-object v1, p0, Lpbe;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1874
    :cond_2
    iget-object v0, p0, Lpbe;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1875
    const/4 v0, 0x4

    iget-object v1, p0, Lpbe;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1877
    :cond_3
    iget-object v0, p0, Lpbe;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1878
    const/4 v0, 0x5

    iget-object v1, p0, Lpbe;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1880
    :cond_4
    iget-object v0, p0, Lpbe;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1881
    const/4 v0, 0x6

    iget-object v1, p0, Lpbe;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1883
    :cond_5
    iget-object v0, p0, Lpbe;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 1884
    const/4 v0, 0x7

    iget-object v1, p0, Lpbe;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1886
    :cond_6
    iget-object v0, p0, Lpbe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1888
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1842
    invoke-virtual {p0, p1}, Lpbe;->a(Loxn;)Lpbe;

    move-result-object v0

    return-object v0
.end method
