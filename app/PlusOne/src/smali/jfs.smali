.class public final Ljfs;
.super Llol;
.source "PG"

# interfaces
.implements Lbc;
.implements Lhjj;
.implements Lhsw;
.implements Ljfn;
.implements Ljgf;
.implements Ljgi;
.implements Llhc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lbc",
        "<",
        "Ljava/lang/Integer;",
        ">;",
        "Lhjj;",
        "Lhsw;",
        "Ljfn;",
        "Ljgf;",
        "Ljgi;",
        "Llhc;"
    }
.end annotation


# instance fields
.field private N:Landroid/view/ViewGroup;

.field private O:Landroid/widget/TextView;

.field private P:Landroid/widget/ListView;

.field private Q:Landroid/view/ViewGroup;

.field private R:Landroid/widget/ImageView;

.field private S:Landroid/view/View;

.field private T:I

.field private U:I

.field private V:Llhj;

.field private W:Ljfz;

.field private X:Z

.field private Y:Ljfw;

.field private Z:Ljfp;

.field private aa:Ljfy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Llol;-><init>()V

    .line 72
    const/4 v0, 0x2

    iput v0, p0, Ljfs;->U:I

    .line 78
    new-instance v0, Ljfz;

    invoke-direct {v0}, Ljfz;-><init>()V

    iput-object v0, p0, Ljfs;->W:Ljfz;

    .line 82
    new-instance v0, Ljfp;

    invoke-direct {v0, p0}, Ljfp;-><init>(Ljfn;)V

    iput-object v0, p0, Ljfs;->Z:Ljfp;

    .line 126
    new-instance v0, Lhsx;

    iget-object v1, p0, Ljfs;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lhsx;-><init>(Llqr;Lhsw;)V

    .line 127
    new-instance v0, Lhje;

    iget-object v1, p0, Ljfs;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    .line 130
    return-void
.end method

.method private U()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Ljfs;->V:Llhj;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Ljfs;->V:Llhj;

    invoke-virtual {v0}, Llhj;->b()V

    .line 208
    invoke-direct {p0}, Ljfs;->ad()V

    .line 210
    :cond_0
    return-void
.end method

.method private W()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Ljfs;->V:Llhj;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Ljfs;->V:Llhj;

    invoke-virtual {v0}, Llhj;->d()V

    .line 222
    invoke-direct {p0}, Ljfs;->ad()V

    .line 224
    :cond_0
    return-void
.end method

.method private X()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 269
    .line 270
    invoke-virtual {p0}, Ljfs;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 271
    if-eqz v1, :cond_0

    .line 272
    const-string v2, "account_id"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 274
    :cond_0
    return v0
.end method

.method private Y()V
    .locals 1

    .prologue
    .line 411
    invoke-direct {p0}, Ljfs;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    invoke-direct {p0}, Ljfs;->e()V

    .line 416
    :goto_0
    return-void

    .line 414
    :cond_0
    invoke-direct {p0}, Ljfs;->aa()V

    goto :goto_0
.end method

.method private Z()Z
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Ljfs;->P:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ljfs;)I
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljfs;->X()I

    move-result v0

    return v0
.end method

.method private a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 469
    invoke-virtual {p0}, Ljfs;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 470
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 471
    :cond_0
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 475
    :goto_0
    return-void

    .line 473
    :cond_1
    invoke-virtual {p1, p4, p3, p2, p5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Ljfy;J)V
    .locals 2

    .prologue
    .line 423
    if-nez p1, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    iget-object v0, p0, Ljfs;->W:Ljfz;

    invoke-virtual {v0}, Ljfz;->a()V

    .line 428
    iget-object v0, p0, Ljfs;->W:Ljfz;

    invoke-virtual {p1, v0, p2, p3}, Ljfy;->a(Ljfz;J)V

    .line 429
    iget-object v0, p0, Ljfs;->W:Ljfz;

    iget-object v0, v0, Ljfz;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Ljfs;->O:Landroid/widget/TextView;

    iget-object v1, p0, Ljfs;->W:Ljfz;

    iget-object v1, v1, Ljfz;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    iget-object v0, p0, Ljfs;->R:Landroid/widget/ImageView;

    iget-object v1, p0, Ljfs;->W:Ljfz;

    iget v1, v1, Ljfz;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 432
    iget-object v0, p0, Ljfs;->R:Landroid/widget/ImageView;

    iget-object v1, p0, Ljfs;->W:Ljfz;

    iget-object v1, v1, Ljfz;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 433
    iget-object v0, p0, Ljfs;->W:Ljfz;

    iget v0, v0, Ljfz;->b:I

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Ljfs;->X:Z

    .line 435
    iget-boolean v0, p0, Ljfs;->X:Z

    if-eqz v0, :cond_3

    .line 436
    invoke-direct {p0}, Ljfs;->ab()V

    .line 441
    :goto_2
    iget-object v0, p0, Ljfs;->Y:Ljfw;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Ljfs;->Y:Ljfw;

    iget-object v1, p0, Ljfs;->W:Ljfz;

    iget-object v1, v1, Ljfz;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljfw;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 433
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 438
    :cond_3
    invoke-direct {p0}, Ljfs;->ac()V

    goto :goto_2
.end method

.method private aa()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 479
    iget-object v0, p0, Ljfs;->P:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 494
    :goto_0
    return-void

    .line 482
    :cond_0
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    invoke-static {v0}, Lhmc;->a(Landroid/view/View;)V

    .line 483
    iget-object v0, p0, Ljfs;->P:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 484
    iget-object v0, p0, Ljfs;->P:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 486
    iget-object v0, p0, Ljfs;->aa:Ljfy;

    if-eqz v0, :cond_1

    iget v0, p0, Ljfs;->T:I

    if-lez v0, :cond_1

    .line 487
    iget-object v0, p0, Ljfs;->Q:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 490
    :cond_1
    invoke-direct {p0}, Ljfs;->ac()V

    .line 491
    iget-object v1, p0, Ljfs;->O:Landroid/widget/TextView;

    .line 492
    invoke-virtual {p0}, Ljfs;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0204c2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object v0, p0

    move-object v3, v2

    move-object v5, v2

    .line 491
    invoke-direct/range {v0 .. v5}, Ljfs;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 493
    iget-object v0, p0, Ljfs;->P:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    goto :goto_0
.end method

.method private ab()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 513
    iget-object v0, p0, Ljfs;->R:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 514
    iget-object v0, p0, Ljfs;->S:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 515
    return-void
.end method

.method private ac()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 518
    iget-object v0, p0, Ljfs;->R:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 519
    iget-object v0, p0, Ljfs;->S:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 520
    return-void
.end method

.method private ad()V
    .locals 3

    .prologue
    .line 531
    iget v0, p0, Ljfs;->U:I

    .line 533
    iget v1, p0, Ljfs;->U:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 534
    iget-object v1, p0, Ljfs;->V:Llhj;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljfs;->V:Llhj;

    invoke-virtual {v1}, Llhj;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 535
    :cond_0
    const/4 v1, 0x2

    iput v1, p0, Ljfs;->U:I

    .line 541
    :cond_1
    :goto_0
    const-string v1, "NavFrag"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Ljfs;->U:I

    if-eq v0, v1, :cond_2

    .line 542
    iget v0, p0, Ljfs;->U:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x36

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "updateNavigationVisibility Menu visibility="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 544
    :cond_2
    return-void

    .line 537
    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Ljfs;->U:I

    goto :goto_0
.end method

.method private ae()V
    .locals 14

    .prologue
    const/16 v13, 0x1a

    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 595
    iget-object v0, p0, Ljfs;->aa:Ljfy;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljfs;->Q:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 596
    iget-object v0, p0, Ljfs;->aa:Ljfy;

    invoke-virtual {v0}, Ljfy;->a()I

    move-result v10

    .line 597
    iget-object v0, p0, Ljfs;->Q:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 599
    iput v9, p0, Ljfs;->T:I

    move v8, v9

    move v7, v9

    .line 600
    :goto_0
    if-ge v7, v11, :cond_2

    if-ge v8, v10, :cond_2

    .line 602
    iget-object v0, p0, Ljfs;->aa:Ljfy;

    invoke-virtual {v0, v8}, Ljfy;->d(I)I

    move-result v0

    .line 604
    if-nez v0, :cond_3

    .line 605
    iget-object v0, p0, Ljfs;->Q:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/ViewGroup;

    .line 606
    const v0, 0x7f100392

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 607
    iget-object v0, p0, Ljfs;->aa:Ljfy;

    invoke-virtual {v0, v8}, Ljfy;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 608
    iget v0, p0, Ljfs;->T:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfs;->T:I

    .line 609
    new-instance v12, Ljfv;

    invoke-direct {v12, v1}, Ljfv;-><init>(Landroid/view/View;)V

    .line 610
    iget-object v0, p0, Ljfs;->aa:Ljfy;

    invoke-virtual {v0, v8, v12}, Ljfy;->a(ILjgb;)V

    .line 611
    iget v0, v12, Ljfv;->a:I

    if-lez v0, :cond_0

    .line 612
    invoke-virtual {p0}, Ljfs;->o()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, v12, Ljfv;->a:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    .line 613
    invoke-direct/range {v0 .. v5}, Ljfs;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 615
    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d01f2

    .line 616
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 617
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 619
    :cond_0
    iget-object v0, v12, Ljfv;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 621
    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v0

    invoke-static {v0, v1, v13}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 623
    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v0

    invoke-static {v0, v13}, Llib;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 626
    invoke-virtual {v6, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 627
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0}, Ljfp;->h()Ljfq;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 630
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 634
    :goto_1
    add-int/lit8 v0, v7, 0x1

    .line 601
    :goto_2
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v7, v0

    goto/16 :goto_0

    .line 632
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 638
    :cond_2
    return-void

    :cond_3
    move v0, v7

    goto :goto_2
.end method

.method static synthetic b(Ljfs;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljfs;->Y()V

    return-void
.end method

.method static synthetic c(Ljfs;)Ljfz;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ljfs;->W:Ljfz;

    return-object v0
.end method

.method private e()V
    .locals 6

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 183
    iget-object v0, p0, Ljfs;->P:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Ljfs;->P:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Ljfs;->Q:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 189
    iget-object v1, p0, Ljfs;->O:Landroid/widget/TextView;

    .line 190
    invoke-virtual {p0}, Ljfs;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0204c0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object v0, p0

    move-object v3, v2

    move-object v5, v2

    .line 189
    invoke-direct/range {v0 .. v5}, Ljfs;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 191
    iget-boolean v0, p0, Ljfs;->X:Z

    if-eqz v0, :cond_1

    .line 192
    invoke-direct {p0}, Ljfs;->ab()V

    .line 194
    :cond_1
    iget-object v0, p0, Ljfs;->P:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    .line 195
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    invoke-static {v0}, Lhmc;->b(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public V()Z
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Ljfs;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    invoke-direct {p0}, Ljfs;->e()V

    .line 175
    const/4 v0, 0x1

    .line 177
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const-wide/16 v10, 0x96

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    .line 315
    const v0, 0x7f040118

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    .line 316
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    new-instance v1, Lhmk;

    sget-object v2, Lomy;->j:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 317
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Ljfs;->P:Landroid/widget/ListView;

    .line 318
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    const v1, 0x7f1001eb

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ljfs;->Q:Landroid/view/ViewGroup;

    .line 319
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    const v1, 0x1020006

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ljfs;->R:Landroid/widget/ImageView;

    .line 320
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ljfs;->O:Landroid/widget/TextView;

    .line 321
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    const v1, 0x7f10016d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ljfs;->S:Landroid/view/View;

    .line 323
    iget-object v0, p0, Ljfs;->O:Landroid/widget/TextView;

    new-instance v1, Ljft;

    invoke-direct {v1, p0}, Ljft;-><init>(Ljfs;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0}, Ljfp;->j()Ljfm;

    move-result-object v0

    .line 331
    iget-object v1, p0, Ljfs;->P:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 332
    iget-object v1, p0, Ljfs;->P:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 334
    iget-object v0, p0, Ljfs;->R:Landroid/widget/ImageView;

    new-instance v1, Ljfu;

    invoke-direct {v1, p0}, Ljfu;-><init>(Ljfs;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 350
    new-instance v0, Llhj;

    iget-object v1, p0, Ljfs;->N:Landroid/view/ViewGroup;

    const-wide/16 v2, 0x190

    invoke-direct {v0, v1, v7, v2, v3}, Llhj;-><init>(Landroid/view/View;ZJ)V

    iput-object v0, p0, Ljfs;->V:Llhj;

    .line 352
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 353
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    const v1, 0x1020010

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 354
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v1

    .line 355
    invoke-virtual {v1, v7, v4, v5}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 356
    invoke-virtual {v1, v8, v4, v5}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 357
    invoke-virtual {v1, v10, v11}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 358
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 360
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 362
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    const v1, 0x7f100284

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 363
    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    .line 364
    invoke-virtual {v1, v7, v4, v5}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 365
    invoke-virtual {v1, v8, v4, v5}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 366
    invoke-virtual {v1, v10, v11}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 367
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 371
    :cond_0
    invoke-direct {p0}, Ljfs;->X()I

    .line 372
    new-instance v0, Ljfw;

    invoke-direct {v0, p0}, Ljfw;-><init>(Ljfs;)V

    iput-object v0, p0, Ljfs;->Y:Ljfw;

    .line 379
    iget-object v0, p0, Ljfs;->Y:Ljfw;

    iget-object v1, p0, Ljfs;->N:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Ljfw;->a(Landroid/view/View;)V

    .line 381
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-direct {p0}, Ljfs;->X()I

    move-result v1

    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljfp;->a(ILandroid/content/Context;)V

    .line 382
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0}, Ljfp;->g()Ljfy;

    move-result-object v0

    iput-object v0, p0, Ljfs;->aa:Ljfy;

    .line 383
    invoke-direct {p0}, Ljfs;->ae()V

    .line 385
    invoke-virtual {p0}, Ljfs;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v6, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    move-result-object v0

    .line 388
    invoke-virtual {v0}, Ldo;->t()V

    .line 390
    if-eqz p3, :cond_2

    .line 391
    const-string v0, "menu_visibility"

    invoke-virtual {p3, v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ljfs;->U:I

    .line 392
    const-string v0, "NavFrag"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    iget v0, p0, Ljfs;->U:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "SavedInstanceState Menu visibility="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 395
    :cond_1
    const-string v0, "action_icon_shown"

    invoke-virtual {p3, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Ljfs;->X:Z

    .line 399
    :cond_2
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0}, Ljfp;->c()Ljfy;

    move-result-object v0

    iget-object v1, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v1}, Ljfp;->d()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Ljfs;->a(Ljfy;J)V

    .line 401
    if-eqz p3, :cond_3

    const-string v0, "menu_opened"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 402
    invoke-direct {p0}, Ljfs;->aa()V

    .line 407
    :goto_0
    iget-object v0, p0, Ljfs;->N:Landroid/view/ViewGroup;

    return-object v0

    .line 404
    :cond_3
    invoke-direct {p0}, Ljfs;->e()V

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 580
    new-instance v0, Ljfo;

    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Ljfs;->Z:Ljfp;

    invoke-direct {v0, v1, v2}, Ljfo;-><init>(Landroid/content/Context;Ljfp;)V

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 228
    const-string v0, "NavFrag"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    iget v0, p0, Ljfs;->U:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "saveNavigationVisibilityAndHide Menu visibility="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 231
    :cond_0
    iget v0, p0, Ljfs;->U:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 232
    const/4 v0, 0x1

    iput v0, p0, Ljfs;->U:I

    .line 233
    invoke-direct {p0}, Ljfs;->W()V

    .line 234
    invoke-direct {p0}, Ljfs;->U()V

    .line 236
    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 261
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 262
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljfs;->d(Z)V

    .line 263
    if-eqz p1, :cond_0

    .line 264
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0, p1}, Ljfp;->a(Landroid/os/Bundle;)V

    .line 266
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 524
    iget-object v0, p0, Ljfs;->V:Llhj;

    if-eqz v0, :cond_0

    iget v0, p0, Ljfs;->U:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 525
    iget-object v0, p0, Ljfs;->V:Llhj;

    invoke-virtual {v0, p1, p2}, Llhj;->a(Landroid/view/View;I)V

    .line 526
    invoke-direct {p0}, Ljfs;->ad()V

    .line 528
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;III)V
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Ljfs;->V:Llhj;

    if-eqz v0, :cond_0

    iget v0, p0, Ljfs;->U:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 549
    iget-object v0, p0, Ljfs;->V:Llhj;

    invoke-virtual {v0, p1, p2, p3, p4}, Llhj;->a(Landroid/view/View;III)V

    .line 550
    invoke-direct {p0}, Ljfs;->ad()V

    .line 552
    :cond_0
    return-void
.end method

.method public a(Ldo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 591
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0}, Ljfp;->i()V

    .line 592
    return-void
.end method

.method public synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p0}, Ljfs;->d()V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 653
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 643
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 657
    invoke-direct {p0}, Ljfs;->e()V

    .line 658
    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljava/lang/String;J)Z
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0, p1, p2, p3}, Ljfp;->a(Ljava/lang/String;J)Z

    move-result v0

    .line 456
    if-eqz v0, :cond_0

    .line 457
    iget-object v1, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v1, p1}, Ljfp;->a(Ljava/lang/String;)Ljfy;

    move-result-object v1

    .line 458
    if-eqz v1, :cond_0

    .line 459
    invoke-direct {p0, v1, p2, p3}, Ljfs;->a(Ljfy;J)V

    .line 462
    :cond_0
    return v0
.end method

.method public aO_()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 279
    iget-object v1, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v1}, Ljfp;->a()V

    .line 280
    iget v1, p0, Ljfs;->U:I

    if-ne v1, v0, :cond_0

    invoke-direct {p0}, Ljfs;->W()V

    :cond_0
    iget v1, p0, Ljfs;->U:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    :goto_0
    const-string v1, "NavFrag"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "ensureNavigationVisibility - shouldBeVisible="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz v0, :cond_6

    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v0

    instance-of v1, v0, Los;

    if-eqz v1, :cond_5

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Loo;->g()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Loo;->e()V

    :cond_2
    iget-object v0, p0, Ljfs;->V:Llhj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljfs;->V:Llhj;

    invoke-virtual {v0}, Llhj;->a()V

    invoke-direct {p0}, Ljfs;->ad()V

    .line 281
    :cond_3
    :goto_2
    invoke-super {p0}, Llol;->aO_()V

    .line 282
    return-void

    .line 280
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Ljfs;->U()V

    goto :goto_2
.end method

.method public b()V
    .locals 3

    .prologue
    .line 240
    iget v0, p0, Ljfs;->U:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 241
    const/4 v0, 0x2

    iput v0, p0, Ljfs;->U:I

    .line 242
    const-string v0, "NavFrag"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget v0, p0, Ljfs;->U:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "restoreSavedNavigationVisibility Menu visibility="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 245
    :cond_0
    iget-object v0, p0, Ljfs;->V:Llhj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljfs;->V:Llhj;

    invoke-virtual {v0}, Llhj;->c()V

    invoke-direct {p0}, Ljfs;->ad()V

    .line 247
    :cond_1
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 648
    return-void
.end method

.method public b(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 498
    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v0

    instance-of v0, v0, Ljfn;

    if-eqz v0, :cond_1

    .line 499
    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v0

    check-cast v0, Ljfn;

    .line 500
    invoke-interface {v0, p1}, Ljfn;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    invoke-direct {p0}, Ljfs;->Y()V

    move v0, v1

    .line 509
    :goto_0
    return v0

    .line 503
    :cond_0
    if-eqz p1, :cond_1

    .line 504
    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lz;->startActivity(Landroid/content/Intent;)V

    .line 505
    invoke-direct {p0}, Ljfs;->Y()V

    move v0, v1

    .line 506
    goto :goto_0

    .line 509
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 251
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 253
    iget-object v0, p0, Ljfs;->au:Llnh;

    const-class v1, Llhe;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llhe;

    .line 254
    if-eqz v0, :cond_0

    .line 255
    invoke-virtual {v0, p0}, Llhe;->a(Llhc;)V

    .line 257
    :cond_0
    return-void
.end method

.method public d()V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 585
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0}, Ljfp;->e()V

    .line 586
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0}, Ljfp;->c()Ljfy;

    move-result-object v0

    iget-object v1, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v1}, Ljfp;->d()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Ljfs;->a(Ljfy;J)V

    .line 587
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 303
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 304
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0, p1}, Ljfp;->b(Landroid/os/Bundle;)V

    .line 306
    const-string v0, "menu_opened"

    invoke-direct {p0}, Ljfs;->Z()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 307
    const-string v0, "menu_visibility"

    iget v1, p0, Ljfs;->U:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 308
    const-string v0, "action_icon_shown"

    iget-boolean v1, p0, Ljfs;->X:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 309
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 288
    invoke-direct {p0}, Ljfs;->X()I

    move-result v1

    .line 289
    invoke-virtual {p0}, Ljfs;->n()Lz;

    move-result-object v0

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    invoke-virtual {p0}, Ljfs;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 292
    :cond_0
    invoke-super {p0}, Llol;->g()V

    .line 293
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Ljfs;->Z:Ljfp;

    invoke-virtual {v0}, Ljfp;->b()V

    .line 298
    invoke-super {p0}, Llol;->z()V

    .line 299
    return-void
.end method
