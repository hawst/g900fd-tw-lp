.class public final Lebb;
.super Lepp;
.source "PG"


# instance fields
.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILept;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lepp;-><init>(Landroid/content/Context;ILept;)V

    .line 30
    return-void
.end method

.method static synthetic a(Lebb;)I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lebb;->d:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 50
    iget-boolean v0, p0, Lebb;->e:Z

    if-eqz v0, :cond_0

    .line 79
    :goto_0
    return-void

    .line 53
    :cond_0
    iput-boolean v2, p0, Lebb;->e:Z

    .line 55
    new-instance v0, Lnyq;

    invoke-direct {v0}, Lnyq;-><init>()V

    .line 56
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyq;->c:Ljava/lang/Boolean;

    .line 57
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnyq;->d:Ljava/lang/Boolean;

    .line 58
    new-instance v1, Lebc;

    invoke-direct {v1, p0}, Lebc;-><init>(Lebb;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 78
    iget-object v1, p0, Lebb;->c:Landroid/content/Context;

    iget v2, p0, Lebb;->b:I

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILnyq;)I

    move-result v0

    iput v0, p0, Lebb;->d:I

    goto :goto_0
.end method

.method protected a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 88
    iget-object v0, p0, Lebb;->c:Landroid/content/Context;

    const v1, 0x7f0a0616

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lebb;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 89
    const v0, 0x7f0a0617

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lebb;->c:Landroid/content/Context;

    const-string v4, "find_my_face"

    .line 91
    const-string v5, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v3, v4, v5}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 89
    invoke-virtual {p0, p1, v0, v1}, Lebb;->a(Landroid/view/View;I[Ljava/lang/Object;)V

    .line 93
    return-void
.end method

.method public a(Lnyq;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 34
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-le v0, v2, :cond_0

    .line 35
    invoke-super {p0, p1}, Lepp;->a(Lnyq;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 44
    :goto_0
    return v0

    .line 39
    :cond_1
    iget-object v0, p0, Lebb;->c:Landroid/content/Context;

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v2, p0, Lebb;->b:I

    .line 40
    invoke-interface {v0, v2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "is_plus_page"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    .line 41
    if-eqz p1, :cond_2

    if-nez v0, :cond_2

    iget-object v0, p1, Lnyq;->b:Ljava/lang/Boolean;

    .line 43
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lnyq;->c:Ljava/lang/Boolean;

    .line 44
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 83
    invoke-virtual {p0}, Lebb;->f()V

    .line 84
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 97
    const v0, 0x7f0400a8

    return v0
.end method

.method public e()Lepn;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lepn;->a:Lepn;

    return-object v0
.end method
