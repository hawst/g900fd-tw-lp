.class public final enum Lkoc;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lkoc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lkoc;

.field public static final enum b:Lkoc;

.field public static final enum c:Lkoc;

.field public static final enum d:Lkoc;

.field private static final synthetic e:[Lkoc;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 223
    new-instance v0, Lkoc;

    const-string v1, "MediaItemRemote"

    invoke-direct {v0, v1, v2}, Lkoc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkoc;->a:Lkoc;

    new-instance v0, Lkoc;

    const-string v1, "MediaItemLocal"

    invoke-direct {v0, v1, v3}, Lkoc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkoc;->b:Lkoc;

    new-instance v0, Lkoc;

    const-string v1, "Album"

    invoke-direct {v0, v1, v4}, Lkoc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkoc;->c:Lkoc;

    new-instance v0, Lkoc;

    const-string v1, "Story"

    invoke-direct {v0, v1, v5}, Lkoc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkoc;->d:Lkoc;

    .line 222
    const/4 v0, 0x4

    new-array v0, v0, [Lkoc;

    sget-object v1, Lkoc;->a:Lkoc;

    aput-object v1, v0, v2

    sget-object v1, Lkoc;->b:Lkoc;

    aput-object v1, v0, v3

    sget-object v1, Lkoc;->c:Lkoc;

    aput-object v1, v0, v4

    sget-object v1, Lkoc;->d:Lkoc;

    aput-object v1, v0, v5

    sput-object v0, Lkoc;->e:[Lkoc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 222
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkoc;
    .locals 1

    .prologue
    .line 222
    const-class v0, Lkoc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkoc;

    return-object v0
.end method

.method public static values()[Lkoc;
    .locals 1

    .prologue
    .line 222
    sget-object v0, Lkoc;->e:[Lkoc;

    invoke-virtual {v0}, [Lkoc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkoc;

    return-object v0
.end method
