.class public final Llzi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnuu;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6397
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6400
    iput-object v0, p0, Llzi;->apiHeader:Llyq;

    .line 6403
    iput-object v0, p0, Llzi;->a:Lnuu;

    .line 6397
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6420
    const/4 v0, 0x0

    .line 6421
    iget-object v1, p0, Llzi;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 6422
    const/4 v0, 0x1

    iget-object v1, p0, Llzi;->apiHeader:Llyq;

    .line 6423
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6425
    :cond_0
    iget-object v1, p0, Llzi;->a:Lnuu;

    if-eqz v1, :cond_1

    .line 6426
    const/4 v1, 0x2

    iget-object v2, p0, Llzi;->a:Lnuu;

    .line 6427
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6429
    :cond_1
    iget-object v1, p0, Llzi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6430
    iput v0, p0, Llzi;->ai:I

    .line 6431
    return v0
.end method

.method public a(Loxn;)Llzi;
    .locals 2

    .prologue
    .line 6439
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6440
    sparse-switch v0, :sswitch_data_0

    .line 6444
    iget-object v1, p0, Llzi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6445
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzi;->ah:Ljava/util/List;

    .line 6448
    :cond_1
    iget-object v1, p0, Llzi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6450
    :sswitch_0
    return-object p0

    .line 6455
    :sswitch_1
    iget-object v0, p0, Llzi;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 6456
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzi;->apiHeader:Llyq;

    .line 6458
    :cond_2
    iget-object v0, p0, Llzi;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6462
    :sswitch_2
    iget-object v0, p0, Llzi;->a:Lnuu;

    if-nez v0, :cond_3

    .line 6463
    new-instance v0, Lnuu;

    invoke-direct {v0}, Lnuu;-><init>()V

    iput-object v0, p0, Llzi;->a:Lnuu;

    .line 6465
    :cond_3
    iget-object v0, p0, Llzi;->a:Lnuu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6440
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6408
    iget-object v0, p0, Llzi;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 6409
    const/4 v0, 0x1

    iget-object v1, p0, Llzi;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6411
    :cond_0
    iget-object v0, p0, Llzi;->a:Lnuu;

    if-eqz v0, :cond_1

    .line 6412
    const/4 v0, 0x2

    iget-object v1, p0, Llzi;->a:Lnuu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6414
    :cond_1
    iget-object v0, p0, Llzi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6416
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6393
    invoke-virtual {p0, p1}, Llzi;->a(Loxn;)Llzi;

    move-result-object v0

    return-object v0
.end method
