.class public final Ldyq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const-string v0, ""

    iput-object v0, p0, Ldyq;->a:Ljava/lang/String;

    .line 79
    const-string v0, ""

    iput-object v0, p0, Ldyq;->b:Ljava/lang/String;

    .line 80
    iput-boolean v1, p0, Ldyq;->f:Z

    .line 81
    const/4 v0, 0x2

    iput v0, p0, Ldyq;->c:I

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldyq;->d:Z

    .line 83
    iput-boolean v1, p0, Ldyq;->e:Z

    .line 84
    return-void
.end method

.method public constructor <init>(IZZ)V
    .locals 7

    .prologue
    .line 90
    const-string v1, ""

    const-string v2, ""

    const/4 v6, 0x1

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Ldyq;-><init>(Ljava/lang/String;Ljava/lang/String;IZZZ)V

    .line 91
    return-void
.end method

.method public constructor <init>(Ldyq;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    if-eqz p1, :cond_0

    .line 110
    iget-object v0, p1, Ldyq;->a:Ljava/lang/String;

    iput-object v0, p0, Ldyq;->a:Ljava/lang/String;

    .line 111
    iget-object v0, p1, Ldyq;->b:Ljava/lang/String;

    iput-object v0, p0, Ldyq;->b:Ljava/lang/String;

    .line 112
    iget-boolean v0, p1, Ldyq;->f:Z

    iput-boolean v0, p0, Ldyq;->f:Z

    .line 113
    iget v0, p1, Ldyq;->c:I

    iput v0, p0, Ldyq;->c:I

    .line 114
    iget-boolean v0, p1, Ldyq;->d:Z

    iput-boolean v0, p0, Ldyq;->d:Z

    .line 115
    iget-boolean v0, p1, Ldyq;->e:Z

    iput-boolean v0, p0, Ldyq;->e:Z

    .line 117
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 7

    .prologue
    .line 95
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Ldyq;-><init>(Ljava/lang/String;Ljava/lang/String;IZZZ)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZZZ)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-object p1, p0, Ldyq;->a:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Ldyq;->b:Ljava/lang/String;

    .line 102
    iput-boolean p6, p0, Ldyq;->f:Z

    .line 103
    iput p3, p0, Ldyq;->c:I

    .line 104
    iput-boolean p4, p0, Ldyq;->d:Z

    .line 105
    iput-boolean p5, p0, Ldyq;->e:Z

    .line 106
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 121
    instance-of v1, p1, Ldyq;

    if-eqz v1, :cond_0

    .line 122
    check-cast p1, Ldyq;

    .line 123
    iget-object v1, p0, Ldyq;->a:Ljava/lang/String;

    iget-object v2, p1, Ldyq;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldyq;->b:Ljava/lang/String;

    iget-object v2, p1, Ldyq;->b:Ljava/lang/String;

    .line 124
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Ldyq;->f:Z

    iget-boolean v2, p1, Ldyq;->f:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Ldyq;->c:I

    iget v2, p1, Ldyq;->c:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Ldyq;->e:Z

    iget-boolean v2, p1, Ldyq;->e:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 129
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 134
    iget-object v0, p0, Ldyq;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 137
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Ldyq;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 138
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Ldyq;->f:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 139
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Ldyq;->c:I

    add-int/2addr v0, v3

    .line 140
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Ldyq;->e:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 142
    return v0

    :cond_0
    move v0, v2

    .line 138
    goto :goto_0

    :cond_1
    move v1, v2

    .line 140
    goto :goto_1
.end method
