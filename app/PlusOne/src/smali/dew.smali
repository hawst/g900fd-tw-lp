.class public final Ldew;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Intent;

.field private final b:I

.field private c:Ljava/lang/String;

.field private d:Lizu;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Long;

.field private h:Ljcn;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Boolean;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/Integer;

.field private r:Ljava/lang/Integer;

.field private s:Ljava/lang/Boolean;

.field private t:Ljava/lang/Integer;

.field private u:Ljava/lang/Boolean;

.field private v:Ljava/lang/Boolean;

.field private w:Ljava/lang/Boolean;

.field private x:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/HostStreamPhotoPagerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Ldew;->a:Landroid/content/Intent;

    .line 334
    iput p2, p0, Ldew;->b:I

    .line 335
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 513
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 514
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "account_id"

    iget v2, p0, Ldew;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 516
    iget-object v0, p0, Ldew;->t:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "all_photos_offset"

    iget-object v2, p0, Ldew;->t:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 519
    :cond_0
    iget-object v0, p0, Ldew;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 520
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "tile_id"

    iget-object v2, p0, Ldew;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 522
    :cond_1
    iget-object v0, p0, Ldew;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 523
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "photo_id"

    iget-object v2, p0, Ldew;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 525
    :cond_2
    iget-object v0, p0, Ldew;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 526
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "view_id"

    iget-object v2, p0, Ldew;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 528
    :cond_3
    iget-boolean v0, p0, Ldew;->x:Z

    if-eqz v0, :cond_4

    .line 529
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "photo_picker_selected"

    iget-object v2, p0, Ldew;->h:Ljcn;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 530
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "photo_picker_mode"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 532
    :cond_4
    iget-object v0, p0, Ldew;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 533
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "is_for_get_content"

    iget-object v2, p0, Ldew;->i:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 535
    :cond_5
    iget-object v0, p0, Ldew;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 536
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "is_for_movie_maker_launch"

    iget-object v2, p0, Ldew;->j:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 538
    :cond_6
    iget-object v0, p0, Ldew;->k:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 539
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "movie_maker_session_id"

    iget-object v2, p0, Ldew;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 541
    :cond_7
    iget-object v0, p0, Ldew;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 542
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "refresh_collection"

    iget-object v2, p0, Ldew;->l:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 544
    :cond_8
    iget-object v0, p0, Ldew;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 545
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "selected_only"

    iget-object v2, p0, Ldew;->m:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 547
    iget-object v0, p0, Ldew;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Ldew;->h:Ljcn;

    if-nez v0, :cond_9

    .line 548
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must have a selection to restrict to."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 551
    :cond_9
    iget-object v0, p0, Ldew;->d:Lizu;

    if-eqz v0, :cond_a

    .line 552
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "photo_ref"

    iget-object v2, p0, Ldew;->d:Lizu;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 554
    :cond_a
    iget-object v0, p0, Ldew;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 555
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "force_return_edit_list"

    iget-object v2, p0, Ldew;->n:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 557
    :cond_b
    iget-object v0, p0, Ldew;->w:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 558
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "show_oob_tile"

    iget-object v2, p0, Ldew;->w:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 560
    :cond_c
    iget-object v0, p0, Ldew;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    .line 561
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "disable_photo_comments"

    iget-object v2, p0, Ldew;->o:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 563
    :cond_d
    iget-object v0, p0, Ldew;->p:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 567
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "auth_key"

    iget-object v2, p0, Ldew;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 569
    :cond_e
    iget-object v0, p0, Ldew;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 573
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "picker_mode"

    iget-object v2, p0, Ldew;->q:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 575
    :cond_f
    iget-object v0, p0, Ldew;->r:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    .line 588
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "filter"

    iget-object v2, p0, Ldew;->r:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 590
    :cond_10
    iget-object v0, p0, Ldew;->s:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    .line 591
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "disable_chromecast"

    iget-object v2, p0, Ldew;->s:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 593
    :cond_11
    iget-object v0, p0, Ldew;->g:Ljava/lang/Long;

    if-eqz v0, :cond_12

    .line 594
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "all_photos_row_id"

    iget-object v2, p0, Ldew;->g:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 596
    :cond_12
    iget-object v0, p0, Ldew;->u:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    .line 597
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "launch_comments_at_start"

    iget-object v2, p0, Ldew;->u:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 599
    :cond_13
    iget-object v0, p0, Ldew;->v:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    .line 600
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-string v1, "up_as_back"

    iget-object v2, p0, Ldew;->v:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 603
    :cond_14
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a(I)Ldew;
    .locals 1

    .prologue
    .line 344
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldew;->t:Ljava/lang/Integer;

    .line 345
    return-object p0
.end method

.method public a(J)Ldew;
    .locals 1

    .prologue
    .line 404
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ldew;->g:Ljava/lang/Long;

    .line 405
    return-object p0
.end method

.method public a(Landroid/content/Context;)Ldew;
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Ldew;->a:Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/photos/viewer/pager/HostPhotoPagerActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 340
    return-object p0
.end method

.method public a(Lizu;)Ldew;
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Ldew;->d:Lizu;

    .line 375
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ldew;
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Ldew;->c:Ljava/lang/String;

    .line 351
    return-object p0
.end method

.method public a(Ljcn;)Ldew;
    .locals 0

    .prologue
    .line 410
    iput-object p1, p0, Ldew;->h:Ljcn;

    .line 411
    return-object p0
.end method

.method public a(Z)Ldew;
    .locals 0

    .prologue
    .line 415
    iput-boolean p1, p0, Ldew;->x:Z

    .line 416
    return-object p0
.end method

.method public b(I)Ldew;
    .locals 1

    .prologue
    .line 465
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldew;->q:Ljava/lang/Integer;

    .line 466
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ldew;
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Ldew;->e:Ljava/lang/String;

    .line 357
    return-object p0
.end method

.method public b(Z)Ldew;
    .locals 1

    .prologue
    .line 420
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->i:Ljava/lang/Boolean;

    .line 421
    return-object p0
.end method

.method public c(I)Ldew;
    .locals 1

    .prologue
    .line 492
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldew;->r:Ljava/lang/Integer;

    .line 493
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ldew;
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Ldew;->p:Ljava/lang/String;

    .line 369
    return-object p0
.end method

.method public c(Z)Ldew;
    .locals 1

    .prologue
    .line 425
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->j:Ljava/lang/Boolean;

    .line 426
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ldew;
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Ldew;->f:Ljava/lang/String;

    .line 400
    return-object p0
.end method

.method public d(Z)Ldew;
    .locals 1

    .prologue
    .line 435
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->m:Ljava/lang/Boolean;

    .line 436
    return-object p0
.end method

.method public e(Ljava/lang/String;)Ldew;
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Ldew;->k:Ljava/lang/String;

    .line 431
    return-object p0
.end method

.method public e(Z)Ldew;
    .locals 1

    .prologue
    .line 441
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->o:Ljava/lang/Boolean;

    .line 442
    return-object p0
.end method

.method public f(Z)Ldew;
    .locals 1

    .prologue
    .line 447
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->l:Ljava/lang/Boolean;

    .line 448
    return-object p0
.end method

.method public g(Z)Ldew;
    .locals 1

    .prologue
    .line 457
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->n:Ljava/lang/Boolean;

    .line 458
    return-object p0
.end method

.method public h(Z)Ldew;
    .locals 1

    .prologue
    .line 477
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->w:Ljava/lang/Boolean;

    .line 478
    return-object p0
.end method

.method public i(Z)Ldew;
    .locals 1

    .prologue
    .line 497
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->s:Ljava/lang/Boolean;

    .line 498
    return-object p0
.end method

.method public j(Z)Ldew;
    .locals 1

    .prologue
    .line 502
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->u:Ljava/lang/Boolean;

    .line 503
    return-object p0
.end method

.method public k(Z)Ldew;
    .locals 1

    .prologue
    .line 507
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldew;->v:Ljava/lang/Boolean;

    .line 508
    return-object p0
.end method
