.class public final Lmac;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnuz;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6883
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6886
    iput-object v0, p0, Lmac;->apiHeader:Llyq;

    .line 6889
    iput-object v0, p0, Lmac;->a:Lnuz;

    .line 6883
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6906
    const/4 v0, 0x0

    .line 6907
    iget-object v1, p0, Lmac;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 6908
    const/4 v0, 0x1

    iget-object v1, p0, Lmac;->apiHeader:Llyq;

    .line 6909
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6911
    :cond_0
    iget-object v1, p0, Lmac;->a:Lnuz;

    if-eqz v1, :cond_1

    .line 6912
    const/4 v1, 0x2

    iget-object v2, p0, Lmac;->a:Lnuz;

    .line 6913
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6915
    :cond_1
    iget-object v1, p0, Lmac;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6916
    iput v0, p0, Lmac;->ai:I

    .line 6917
    return v0
.end method

.method public a(Loxn;)Lmac;
    .locals 2

    .prologue
    .line 6925
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6926
    sparse-switch v0, :sswitch_data_0

    .line 6930
    iget-object v1, p0, Lmac;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6931
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmac;->ah:Ljava/util/List;

    .line 6934
    :cond_1
    iget-object v1, p0, Lmac;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6936
    :sswitch_0
    return-object p0

    .line 6941
    :sswitch_1
    iget-object v0, p0, Lmac;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 6942
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmac;->apiHeader:Llyq;

    .line 6944
    :cond_2
    iget-object v0, p0, Lmac;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6948
    :sswitch_2
    iget-object v0, p0, Lmac;->a:Lnuz;

    if-nez v0, :cond_3

    .line 6949
    new-instance v0, Lnuz;

    invoke-direct {v0}, Lnuz;-><init>()V

    iput-object v0, p0, Lmac;->a:Lnuz;

    .line 6951
    :cond_3
    iget-object v0, p0, Lmac;->a:Lnuz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6926
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6894
    iget-object v0, p0, Lmac;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 6895
    const/4 v0, 0x1

    iget-object v1, p0, Lmac;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6897
    :cond_0
    iget-object v0, p0, Lmac;->a:Lnuz;

    if-eqz v0, :cond_1

    .line 6898
    const/4 v0, 0x2

    iget-object v1, p0, Lmac;->a:Lnuz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6900
    :cond_1
    iget-object v0, p0, Lmac;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6902
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6879
    invoke-virtual {p0, p1}, Lmac;->a(Loxn;)Lmac;

    move-result-object v0

    return-object v0
.end method
