.class public final Lpgs;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lpgq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33
    sget-object v0, Lpgq;->a:[Lpgq;

    iput-object v0, p0, Lpgs;->a:[Lpgq;

    .line 30
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 51
    .line 52
    iget-object v1, p0, Lpgs;->a:[Lpgq;

    if-eqz v1, :cond_1

    .line 53
    iget-object v2, p0, Lpgs;->a:[Lpgq;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 54
    if-eqz v4, :cond_0

    .line 55
    const/4 v5, 0x1

    .line 56
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 53
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, p0, Lpgs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    iput v0, p0, Lpgs;->ai:I

    .line 62
    return v0
.end method

.method public a(Loxn;)Lpgs;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 70
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 71
    sparse-switch v0, :sswitch_data_0

    .line 75
    iget-object v2, p0, Lpgs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 76
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpgs;->ah:Ljava/util/List;

    .line 79
    :cond_1
    iget-object v2, p0, Lpgs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    :sswitch_0
    return-object p0

    .line 86
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 87
    iget-object v0, p0, Lpgs;->a:[Lpgq;

    if-nez v0, :cond_3

    move v0, v1

    .line 88
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpgq;

    .line 89
    iget-object v3, p0, Lpgs;->a:[Lpgq;

    if-eqz v3, :cond_2

    .line 90
    iget-object v3, p0, Lpgs;->a:[Lpgq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    :cond_2
    iput-object v2, p0, Lpgs;->a:[Lpgq;

    .line 93
    :goto_2
    iget-object v2, p0, Lpgs;->a:[Lpgq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 94
    iget-object v2, p0, Lpgs;->a:[Lpgq;

    new-instance v3, Lpgq;

    invoke-direct {v3}, Lpgq;-><init>()V

    aput-object v3, v2, v0

    .line 95
    iget-object v2, p0, Lpgs;->a:[Lpgq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 96
    invoke-virtual {p1}, Loxn;->a()I

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 87
    :cond_3
    iget-object v0, p0, Lpgs;->a:[Lpgq;

    array-length v0, v0

    goto :goto_1

    .line 99
    :cond_4
    iget-object v2, p0, Lpgs;->a:[Lpgq;

    new-instance v3, Lpgq;

    invoke-direct {v3}, Lpgq;-><init>()V

    aput-object v3, v2, v0

    .line 100
    iget-object v2, p0, Lpgs;->a:[Lpgq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 38
    iget-object v0, p0, Lpgs;->a:[Lpgq;

    if-eqz v0, :cond_1

    .line 39
    iget-object v1, p0, Lpgs;->a:[Lpgq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 40
    if-eqz v3, :cond_0

    .line 41
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 39
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_1
    iget-object v0, p0, Lpgs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 47
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lpgs;->a(Loxn;)Lpgs;

    move-result-object v0

    return-object v0
.end method
