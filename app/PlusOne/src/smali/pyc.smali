.class public final Lpyc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpyc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lpxy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const v0, 0x3d53d64

    new-instance v1, Lpyd;

    invoke-direct {v1}, Lpyd;-><init>()V

    .line 44
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpyc;->a:Loxr;

    .line 43
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Loxq;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lpyc;->b:Lpxy;

    .line 40
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 61
    const/4 v0, 0x0

    .line 62
    iget-object v1, p0, Lpyc;->b:Lpxy;

    if-eqz v1, :cond_0

    .line 63
    const/4 v0, 0x1

    iget-object v1, p0, Lpyc;->b:Lpxy;

    .line 64
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 66
    :cond_0
    iget-object v1, p0, Lpyc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    iput v0, p0, Lpyc;->ai:I

    .line 68
    return v0
.end method

.method public a(Loxn;)Lpyc;
    .locals 2

    .prologue
    .line 76
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 77
    sparse-switch v0, :sswitch_data_0

    .line 81
    iget-object v1, p0, Lpyc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpyc;->ah:Ljava/util/List;

    .line 85
    :cond_1
    iget-object v1, p0, Lpyc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    :sswitch_0
    return-object p0

    .line 92
    :sswitch_1
    iget-object v0, p0, Lpyc;->b:Lpxy;

    if-nez v0, :cond_2

    .line 93
    new-instance v0, Lpxy;

    invoke-direct {v0}, Lpxy;-><init>()V

    iput-object v0, p0, Lpyc;->b:Lpxy;

    .line 95
    :cond_2
    iget-object v0, p0, Lpyc;->b:Lpxy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lpyc;->b:Lpxy;

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x1

    iget-object v1, p0, Lpyc;->b:Lpxy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 55
    :cond_0
    iget-object v0, p0, Lpyc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 57
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lpyc;->a(Loxn;)Lpyc;

    move-result-object v0

    return-object v0
.end method
