.class public final Ldvw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public a:I

.field public b:Lohl;

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method public constructor <init>(ILohl;Ljava/util/ArrayList;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lohl;",
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p1, p0, Ldvw;->a:I

    .line 36
    iput-object p2, p0, Ldvw;->b:Lohl;

    .line 37
    iput-object p3, p0, Ldvw;->c:Ljava/util/ArrayList;

    .line 38
    iput-boolean p4, p0, Ldvw;->e:Z

    .line 39
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Ldvw;->a:I

    .line 43
    const/4 v0, 0x3

    iget v2, p0, Ldvw;->a:I

    if-eq v0, v2, :cond_0

    const/4 v0, 0x2

    iget v2, p0, Ldvw;->a:I

    if-ne v0, v2, :cond_1

    .line 44
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 45
    new-array v0, v0, [B

    .line 46
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 48
    :try_start_0
    new-instance v2, Lohl;

    invoke-direct {v2}, Lohl;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lohl;

    iput-object v0, p0, Ldvw;->b:Lohl;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ldvw;->c:Ljava/util/ArrayList;

    move v2, v1

    .line 55
    :goto_1
    if-ge v2, v3, :cond_2

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 57
    new-array v0, v0, [B

    .line 58
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 60
    :try_start_1
    new-instance v4, Lnrp;

    invoke-direct {v4}, Lnrp;-><init>()V

    invoke-static {v4, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnrp;

    .line 61
    iget-object v4, p0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_1

    .line 55
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 50
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Ldvw;->b:Lohl;

    goto :goto_0

    .line 66
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Ldvw;->e:Z

    .line 67
    return-void

    :cond_3
    move v0, v1

    .line 66
    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public a()[B
    .locals 2

    .prologue
    .line 93
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 94
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Ldvw;->writeToParcel(Landroid/os/Parcel;I)V

    .line 95
    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B

    move-result-object v1

    .line 96
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 97
    return-object v1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Ldvw;->d:Ljava/util/ArrayList;

    .line 102
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Ldvw;->d:Ljava/util/ArrayList;

    .line 106
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 76
    iget v0, p0, Ldvw;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    const/4 v0, 0x3

    iget v2, p0, Ldvw;->a:I

    if-eq v0, v2, :cond_0

    const/4 v0, 0x2

    iget v2, p0, Ldvw;->a:I

    if-ne v0, v2, :cond_1

    .line 78
    :cond_0
    iget-object v0, p0, Ldvw;->b:Lohl;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 79
    array-length v2, v0

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 80
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 82
    :cond_1
    iget-object v0, p0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 83
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    move v2, v1

    .line 84
    :goto_0
    if-ge v2, v3, :cond_2

    .line 85
    iget-object v0, p0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 86
    array-length v4, v0

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 84
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 89
    :cond_2
    iget-boolean v0, p0, Ldvw;->e:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    return-void

    :cond_3
    move v0, v1

    .line 89
    goto :goto_1
.end method
