.class public final Lmhw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lndq;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20491
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20494
    iput-object v0, p0, Lmhw;->apiHeader:Llyq;

    .line 20497
    iput-object v0, p0, Lmhw;->a:Lndq;

    .line 20491
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 20514
    const/4 v0, 0x0

    .line 20515
    iget-object v1, p0, Lmhw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 20516
    const/4 v0, 0x1

    iget-object v1, p0, Lmhw;->apiHeader:Llyq;

    .line 20517
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20519
    :cond_0
    iget-object v1, p0, Lmhw;->a:Lndq;

    if-eqz v1, :cond_1

    .line 20520
    const/4 v1, 0x2

    iget-object v2, p0, Lmhw;->a:Lndq;

    .line 20521
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20523
    :cond_1
    iget-object v1, p0, Lmhw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20524
    iput v0, p0, Lmhw;->ai:I

    .line 20525
    return v0
.end method

.method public a(Loxn;)Lmhw;
    .locals 2

    .prologue
    .line 20533
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 20534
    sparse-switch v0, :sswitch_data_0

    .line 20538
    iget-object v1, p0, Lmhw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 20539
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhw;->ah:Ljava/util/List;

    .line 20542
    :cond_1
    iget-object v1, p0, Lmhw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20544
    :sswitch_0
    return-object p0

    .line 20549
    :sswitch_1
    iget-object v0, p0, Lmhw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 20550
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmhw;->apiHeader:Llyq;

    .line 20552
    :cond_2
    iget-object v0, p0, Lmhw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20556
    :sswitch_2
    iget-object v0, p0, Lmhw;->a:Lndq;

    if-nez v0, :cond_3

    .line 20557
    new-instance v0, Lndq;

    invoke-direct {v0}, Lndq;-><init>()V

    iput-object v0, p0, Lmhw;->a:Lndq;

    .line 20559
    :cond_3
    iget-object v0, p0, Lmhw;->a:Lndq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20534
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 20502
    iget-object v0, p0, Lmhw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 20503
    const/4 v0, 0x1

    iget-object v1, p0, Lmhw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20505
    :cond_0
    iget-object v0, p0, Lmhw;->a:Lndq;

    if-eqz v0, :cond_1

    .line 20506
    const/4 v0, 0x2

    iget-object v1, p0, Lmhw;->a:Lndq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20508
    :cond_1
    iget-object v0, p0, Lmhw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 20510
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 20487
    invoke-virtual {p0, p1}, Lmhw;->a(Loxn;)Lmhw;

    move-result-object v0

    return-object v0
.end method
