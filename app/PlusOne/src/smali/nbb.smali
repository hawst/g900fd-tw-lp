.class public final Lnbb;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/Long;

.field public b:I

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field private e:[Ljava/lang/String;

.field private f:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lnbb;->a:[Ljava/lang/Long;

    .line 23
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnbb;->e:[Ljava/lang/String;

    .line 26
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnbb;->f:[Ljava/lang/String;

    .line 29
    const/high16 v0, -0x80000000

    iput v0, p0, Lnbb;->b:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 68
    .line 69
    iget-object v0, p0, Lnbb;->a:[Ljava/lang/Long;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lnbb;->a:[Ljava/lang/Long;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 71
    iget-object v3, p0, Lnbb;->a:[Ljava/lang/Long;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 73
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    iget-object v0, p0, Lnbb;->a:[Ljava/lang/Long;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 78
    :goto_1
    iget-object v2, p0, Lnbb;->e:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnbb;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 80
    iget-object v4, p0, Lnbb;->e:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 82
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 80
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 84
    :cond_1
    add-int/2addr v0, v3

    .line 85
    iget-object v2, p0, Lnbb;->e:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 87
    :cond_2
    iget v2, p0, Lnbb;->b:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    .line 88
    const/4 v2, 0x3

    iget v3, p0, Lnbb;->b:I

    .line 89
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 91
    :cond_3
    iget-object v2, p0, Lnbb;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 92
    const/4 v2, 0x4

    iget-object v3, p0, Lnbb;->c:Ljava/lang/Boolean;

    .line 93
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 95
    :cond_4
    iget-object v2, p0, Lnbb;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 96
    const/4 v2, 0x5

    iget-object v3, p0, Lnbb;->d:Ljava/lang/Boolean;

    .line 97
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 99
    :cond_5
    iget-object v2, p0, Lnbb;->f:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lnbb;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 101
    iget-object v3, p0, Lnbb;->f:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    .line 103
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 105
    :cond_6
    add-int/2addr v0, v2

    .line 106
    iget-object v1, p0, Lnbb;->f:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 108
    :cond_7
    iget-object v1, p0, Lnbb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    iput v0, p0, Lnbb;->ai:I

    .line 110
    return v0

    :cond_8
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lnbb;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 119
    sparse-switch v0, :sswitch_data_0

    .line 123
    iget-object v1, p0, Lnbb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbb;->ah:Ljava/util/List;

    .line 127
    :cond_1
    iget-object v1, p0, Lnbb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    :sswitch_0
    return-object p0

    .line 134
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 135
    iget-object v0, p0, Lnbb;->a:[Ljava/lang/Long;

    array-length v0, v0

    .line 136
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    .line 137
    iget-object v2, p0, Lnbb;->a:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 138
    iput-object v1, p0, Lnbb;->a:[Ljava/lang/Long;

    .line 139
    :goto_1
    iget-object v1, p0, Lnbb;->a:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 140
    iget-object v1, p0, Lnbb;->a:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 141
    invoke-virtual {p1}, Loxn;->a()I

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 144
    :cond_2
    iget-object v1, p0, Lnbb;->a:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 148
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 149
    iget-object v0, p0, Lnbb;->e:[Ljava/lang/String;

    array-length v0, v0

    .line 150
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 151
    iget-object v2, p0, Lnbb;->e:[Ljava/lang/String;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 152
    iput-object v1, p0, Lnbb;->e:[Ljava/lang/String;

    .line 153
    :goto_2
    iget-object v1, p0, Lnbb;->e:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 154
    iget-object v1, p0, Lnbb;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 155
    invoke-virtual {p1}, Loxn;->a()I

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 158
    :cond_3
    iget-object v1, p0, Lnbb;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 162
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 163
    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 167
    :cond_4
    iput v0, p0, Lnbb;->b:I

    goto/16 :goto_0

    .line 169
    :cond_5
    iput v4, p0, Lnbb;->b:I

    goto/16 :goto_0

    .line 174
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnbb;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 178
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnbb;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 182
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 183
    iget-object v0, p0, Lnbb;->f:[Ljava/lang/String;

    array-length v0, v0

    .line 184
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 185
    iget-object v2, p0, Lnbb;->f:[Ljava/lang/String;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    iput-object v1, p0, Lnbb;->f:[Ljava/lang/String;

    .line 187
    :goto_3
    iget-object v1, p0, Lnbb;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    .line 188
    iget-object v1, p0, Lnbb;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 189
    invoke-virtual {p1}, Loxn;->a()I

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 192
    :cond_6
    iget-object v1, p0, Lnbb;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 119
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Lnbb;->a:[Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 39
    iget-object v2, p0, Lnbb;->a:[Ljava/lang/Long;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 40
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v5, v6, v7}, Loxo;->b(IJ)V

    .line 39
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    :cond_0
    iget-object v1, p0, Lnbb;->e:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 44
    iget-object v2, p0, Lnbb;->e:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 45
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 48
    :cond_1
    iget v1, p0, Lnbb;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 49
    const/4 v1, 0x3

    iget v2, p0, Lnbb;->b:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 51
    :cond_2
    iget-object v1, p0, Lnbb;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 52
    const/4 v1, 0x4

    iget-object v2, p0, Lnbb;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 54
    :cond_3
    iget-object v1, p0, Lnbb;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 55
    const/4 v1, 0x5

    iget-object v2, p0, Lnbb;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 57
    :cond_4
    iget-object v1, p0, Lnbb;->f:[Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 58
    iget-object v1, p0, Lnbb;->f:[Ljava/lang/String;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 59
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 62
    :cond_5
    iget-object v0, p0, Lnbb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 64
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnbb;->a(Loxn;)Lnbb;

    move-result-object v0

    return-object v0
.end method
