.class public final Lmfx;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmyv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3076
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3079
    iput-object v0, p0, Lmfx;->apiHeader:Llyr;

    .line 3082
    iput-object v0, p0, Lmfx;->a:Lmyv;

    .line 3076
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3099
    const/4 v0, 0x0

    .line 3100
    iget-object v1, p0, Lmfx;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 3101
    const/4 v0, 0x1

    iget-object v1, p0, Lmfx;->apiHeader:Llyr;

    .line 3102
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3104
    :cond_0
    iget-object v1, p0, Lmfx;->a:Lmyv;

    if-eqz v1, :cond_1

    .line 3105
    const/4 v1, 0x2

    iget-object v2, p0, Lmfx;->a:Lmyv;

    .line 3106
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3108
    :cond_1
    iget-object v1, p0, Lmfx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3109
    iput v0, p0, Lmfx;->ai:I

    .line 3110
    return v0
.end method

.method public a(Loxn;)Lmfx;
    .locals 2

    .prologue
    .line 3118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3119
    sparse-switch v0, :sswitch_data_0

    .line 3123
    iget-object v1, p0, Lmfx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfx;->ah:Ljava/util/List;

    .line 3127
    :cond_1
    iget-object v1, p0, Lmfx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3129
    :sswitch_0
    return-object p0

    .line 3134
    :sswitch_1
    iget-object v0, p0, Lmfx;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 3135
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfx;->apiHeader:Llyr;

    .line 3137
    :cond_2
    iget-object v0, p0, Lmfx;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3141
    :sswitch_2
    iget-object v0, p0, Lmfx;->a:Lmyv;

    if-nez v0, :cond_3

    .line 3142
    new-instance v0, Lmyv;

    invoke-direct {v0}, Lmyv;-><init>()V

    iput-object v0, p0, Lmfx;->a:Lmyv;

    .line 3144
    :cond_3
    iget-object v0, p0, Lmfx;->a:Lmyv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3119
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3087
    iget-object v0, p0, Lmfx;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 3088
    const/4 v0, 0x1

    iget-object v1, p0, Lmfx;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3090
    :cond_0
    iget-object v0, p0, Lmfx;->a:Lmyv;

    if-eqz v0, :cond_1

    .line 3091
    const/4 v0, 0x2

    iget-object v1, p0, Lmfx;->a:Lmyv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3093
    :cond_1
    iget-object v0, p0, Lmfx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3095
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3072
    invoke-virtual {p0, p1}, Lmfx;->a(Loxn;)Lmfx;

    move-result-object v0

    return-object v0
.end method
