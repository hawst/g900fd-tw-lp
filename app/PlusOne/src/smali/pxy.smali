.class public final Lpxy;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[Lpxz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 297
    invoke-direct {p0}, Loxq;-><init>()V

    .line 444
    const/high16 v0, -0x80000000

    iput v0, p0, Lpxy;->a:I

    .line 453
    sget-object v0, Lpxz;->a:[Lpxz;

    iput-object v0, p0, Lpxy;->e:[Lpxz;

    .line 297
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 483
    .line 484
    iget v0, p0, Lpxy;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_5

    .line 485
    const/4 v0, 0x1

    iget v2, p0, Lpxy;->a:I

    .line 486
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 488
    :goto_0
    iget-object v2, p0, Lpxy;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 489
    const/4 v2, 0x2

    iget-object v3, p0, Lpxy;->b:Ljava/lang/String;

    .line 490
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 492
    :cond_0
    iget-object v2, p0, Lpxy;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 493
    const/4 v2, 0x3

    iget-object v3, p0, Lpxy;->c:Ljava/lang/String;

    .line 494
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 496
    :cond_1
    iget-object v2, p0, Lpxy;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 497
    const/4 v2, 0x4

    iget-object v3, p0, Lpxy;->d:Ljava/lang/String;

    .line 498
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 500
    :cond_2
    iget-object v2, p0, Lpxy;->e:[Lpxz;

    if-eqz v2, :cond_4

    .line 501
    iget-object v2, p0, Lpxy;->e:[Lpxz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 502
    if-eqz v4, :cond_3

    .line 503
    const/4 v5, 0x5

    .line 504
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 501
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 508
    :cond_4
    iget-object v1, p0, Lpxy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 509
    iput v0, p0, Lpxy;->ai:I

    .line 510
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpxy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 519
    sparse-switch v0, :sswitch_data_0

    .line 523
    iget-object v2, p0, Lpxy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 524
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpxy;->ah:Ljava/util/List;

    .line 527
    :cond_1
    iget-object v2, p0, Lpxy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 529
    :sswitch_0
    return-object p0

    .line 534
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 535
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 537
    :cond_2
    iput v0, p0, Lpxy;->a:I

    goto :goto_0

    .line 539
    :cond_3
    iput v1, p0, Lpxy;->a:I

    goto :goto_0

    .line 544
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxy;->b:Ljava/lang/String;

    goto :goto_0

    .line 548
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxy;->c:Ljava/lang/String;

    goto :goto_0

    .line 552
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxy;->d:Ljava/lang/String;

    goto :goto_0

    .line 556
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 557
    iget-object v0, p0, Lpxy;->e:[Lpxz;

    if-nez v0, :cond_5

    move v0, v1

    .line 558
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpxz;

    .line 559
    iget-object v3, p0, Lpxy;->e:[Lpxz;

    if-eqz v3, :cond_4

    .line 560
    iget-object v3, p0, Lpxy;->e:[Lpxz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 562
    :cond_4
    iput-object v2, p0, Lpxy;->e:[Lpxz;

    .line 563
    :goto_2
    iget-object v2, p0, Lpxy;->e:[Lpxz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 564
    iget-object v2, p0, Lpxy;->e:[Lpxz;

    new-instance v3, Lpxz;

    invoke-direct {v3}, Lpxz;-><init>()V

    aput-object v3, v2, v0

    .line 565
    iget-object v2, p0, Lpxy;->e:[Lpxz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 566
    invoke-virtual {p1}, Loxn;->a()I

    .line 563
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 557
    :cond_5
    iget-object v0, p0, Lpxy;->e:[Lpxz;

    array-length v0, v0

    goto :goto_1

    .line 569
    :cond_6
    iget-object v2, p0, Lpxy;->e:[Lpxz;

    new-instance v3, Lpxz;

    invoke-direct {v3}, Lpxz;-><init>()V

    aput-object v3, v2, v0

    .line 570
    iget-object v2, p0, Lpxy;->e:[Lpxz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 519
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 458
    iget v0, p0, Lpxy;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 459
    const/4 v0, 0x1

    iget v1, p0, Lpxy;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 461
    :cond_0
    iget-object v0, p0, Lpxy;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 462
    const/4 v0, 0x2

    iget-object v1, p0, Lpxy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 464
    :cond_1
    iget-object v0, p0, Lpxy;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 465
    const/4 v0, 0x3

    iget-object v1, p0, Lpxy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 467
    :cond_2
    iget-object v0, p0, Lpxy;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 468
    const/4 v0, 0x4

    iget-object v1, p0, Lpxy;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 470
    :cond_3
    iget-object v0, p0, Lpxy;->e:[Lpxz;

    if-eqz v0, :cond_5

    .line 471
    iget-object v1, p0, Lpxy;->e:[Lpxz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 472
    if-eqz v3, :cond_4

    .line 473
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 471
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 477
    :cond_5
    iget-object v0, p0, Lpxy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 479
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 293
    invoke-virtual {p0, p1}, Lpxy;->a(Loxn;)Lpxy;

    move-result-object v0

    return-object v0
.end method
