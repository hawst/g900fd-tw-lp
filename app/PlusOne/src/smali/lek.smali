.class public final Llek;
.super Llch;
.source "PG"

# interfaces
.implements Llda;


# instance fields
.field private c:Llcc;

.field private d:Llck;

.field private e:Llcu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Llek;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Llek;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Llch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Llck;

    invoke-direct {v0, p1, p2, p3}, Llck;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Llek;->d:Llck;

    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Llek;->setClickable(Z)V

    .line 46
    new-instance v0, Llcu;

    const v1, 0x7f0a048a

    invoke-direct {v0, p0, v1}, Llcu;-><init>(Llda;I)V

    iput-object v0, p0, Llek;->e:Llcu;

    .line 47
    return-void
.end method


# virtual methods
.method protected a(IIII)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 93
    .line 94
    iget v1, p0, Llek;->t:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 96
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 97
    :goto_0
    invoke-virtual {p0}, Llek;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 98
    invoke-virtual {p0, v0}, Llek;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 99
    invoke-virtual {v3, v1, v2}, Landroid/view/View;->measure(II)V

    .line 100
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr p2, v3

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_0
    return p2
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Llek;->getHeight()I

    move-result v0

    add-int/2addr v0, p6

    return v0
.end method

.method public a(IZ)Landroid/view/View;
    .locals 5

    .prologue
    .line 78
    new-instance v1, Llej;

    invoke-virtual {p0}, Llek;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Llej;-><init>(Landroid/content/Context;)V

    .line 79
    iget-object v2, p0, Llek;->r:Ljava/lang/String;

    iget-object v3, p0, Llek;->a:Llci;

    iget-object v0, p0, Llek;->c:Llcc;

    .line 80
    invoke-virtual {v0, p1}, Llcc;->a(I)Lkzm;

    move-result-object v4

    if-nez p2, :cond_0

    const/4 v0, 0x1

    .line 79
    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, Llej;->a(Ljava/lang/String;Llci;Lkzm;Z)V

    .line 82
    return-object v1

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0}, Llch;->a()V

    .line 133
    invoke-virtual {p0}, Llek;->m()V

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Llek;->c:Llcc;

    .line 136
    iget-object v0, p0, Llek;->e:Llcu;

    invoke-virtual {v0}, Llcu;->a()V

    .line 137
    return-void
.end method

.method protected a(Landroid/database/Cursor;Llcr;I)V
    .locals 3

    .prologue
    .line 51
    invoke-super {p0, p1, p2, p3}, Llch;->a(Landroid/database/Cursor;Llcr;I)V

    .line 53
    const/16 v0, 0x1b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 54
    invoke-static {v0}, Llcc;->a([B)Llcc;

    move-result-object v0

    iput-object v0, p0, Llek;->c:Llcc;

    .line 55
    iget-object v0, p0, Llek;->c:Llcc;

    invoke-virtual {v0}, Llcc;->a()I

    move-result v0

    .line 56
    iget-object v1, p0, Llek;->e:Llcu;

    iget-object v2, p0, Llek;->c:Llcc;

    invoke-virtual {v2}, Llcc;->b()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Llcu;->a(Ljava/util/List;I)V

    .line 57
    return-void
.end method

.method protected a(ZIIII)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-super/range {p0 .. p5}, Llch;->a(ZIIII)V

    .line 109
    iget-object v1, p0, Llek;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 110
    iget v2, p0, Llek;->t:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 112
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 113
    invoke-virtual {p0}, Llek;->getChildCount()I

    move-result v4

    :goto_0
    if-ge v0, v4, :cond_0

    .line 114
    invoke-virtual {p0, v0}, Llek;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 115
    invoke-virtual {v5, v2, v3}, Landroid/view/View;->measure(II)V

    .line 116
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 117
    iget-object v7, p0, Llek;->q:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Llek;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget v9, p0, Llek;->t:I

    add-int/2addr v8, v9

    add-int v9, v1, v6

    invoke-virtual {v5, v7, v1, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 119
    add-int/2addr v1, v6

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Llek;->d:Llck;

    sget-object v1, Llek;->z:Llct;

    iget v1, v1, Llct;->ax:I

    .line 62
    invoke-virtual {p0}, Llek;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a048b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Llek;->z:Llct;

    iget-object v3, v3, Llct;->aD:Landroid/graphics/drawable/NinePatchDrawable;

    .line 61
    invoke-virtual {v0, v1, v2, v3}, Llck;->a(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 64
    iget-object v0, p0, Llek;->d:Llck;

    invoke-virtual {p0, v0}, Llek;->addView(Landroid/view/View;)V

    .line 65
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public h()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Llek;->c:Llcc;

    invoke-virtual {v0}, Llcc;->a()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Llek;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llej;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public j()Llcu;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Llek;->e:Llcu;

    return-object v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Llek;->e:Llcu;

    invoke-virtual {v0, p1}, Llcu;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Llek;->e:Llcu;

    invoke-virtual {v0, p1}, Llcu;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
