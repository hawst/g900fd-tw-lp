.class public final Lkyi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:I

.field private synthetic b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;I)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    iput p2, p0, Lkyi;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 472
    iget-object v0, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-static {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 473
    iget-object v0, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    iget v1, p0, Lkyi;->a:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;I)I

    .line 474
    iget-object v0, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-static {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Lkyk;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-static {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Lkyk;

    move-result-object v0

    iget v1, p0, Lkyi;->a:I

    invoke-interface {v0, v1}, Lkyk;->c(I)V

    .line 477
    :cond_0
    iget-object v0, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->k:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 478
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    .line 479
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/view/View;)Lhml;

    move-result-object v1

    .line 477
    invoke-static {v0, v4, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 485
    :goto_0
    iget-object v1, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    iget-object v0, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-static {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;Z)Z

    .line 486
    iget-object v0, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-static {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Lkyk;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 487
    iget-object v0, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-static {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Lkyk;

    move-result-object v0

    iget-object v1, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-static {v1}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c(Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;)Z

    move-result v1

    invoke-interface {v0, v1}, Lkyk;->b(Z)V

    .line 489
    :cond_1
    return-void

    .line 481
    :cond_2
    iget-object v0, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lhml;

    invoke-direct {v1}, Lhml;-><init>()V

    new-instance v2, Lhmk;

    sget-object v3, Lomv;->l:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    .line 482
    invoke-virtual {v1, v2}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v1

    iget-object v2, p0, Lkyi;->b:Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    .line 483
    invoke-virtual {v1, v2}, Lhml;->a(Landroid/view/View;)Lhml;

    move-result-object v1

    .line 481
    invoke-static {v0, v4, v1}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto :goto_0

    .line 485
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
