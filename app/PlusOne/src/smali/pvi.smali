.class public final Lpvi;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpvi;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lpgx;

.field public d:Lpmm;

.field public e:[Lpvf;

.field public f:Ljava/lang/String;

.field private g:Lpqw;

.field private h:Ljava/lang/Long;

.field private i:Lpvk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lpvi;

    sput-object v0, Lpvi;->a:[Lpvi;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    iput-object v1, p0, Lpvi;->c:Lpgx;

    .line 18
    iput-object v1, p0, Lpvi;->g:Lpqw;

    .line 21
    iput-object v1, p0, Lpvi;->d:Lpmm;

    .line 24
    sget-object v0, Lpvf;->a:[Lpvf;

    iput-object v0, p0, Lpvi;->e:[Lpvf;

    .line 29
    iput-object v1, p0, Lpvi;->i:Lpvk;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 70
    .line 71
    iget-object v0, p0, Lpvi;->c:Lpgx;

    if-eqz v0, :cond_8

    .line 72
    const/4 v0, 0x1

    iget-object v2, p0, Lpvi;->c:Lpgx;

    .line 73
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 75
    :goto_0
    iget-object v2, p0, Lpvi;->g:Lpqw;

    if-eqz v2, :cond_0

    .line 76
    const/4 v2, 0x2

    iget-object v3, p0, Lpvi;->g:Lpqw;

    .line 77
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 79
    :cond_0
    iget-object v2, p0, Lpvi;->e:[Lpvf;

    if-eqz v2, :cond_2

    .line 80
    iget-object v2, p0, Lpvi;->e:[Lpvf;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 81
    if-eqz v4, :cond_1

    .line 82
    const/4 v5, 0x3

    .line 83
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 80
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 87
    :cond_2
    iget-object v1, p0, Lpvi;->h:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 88
    const/4 v1, 0x4

    iget-object v2, p0, Lpvi;->h:Ljava/lang/Long;

    .line 89
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_3
    iget-object v1, p0, Lpvi;->b:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 92
    const/4 v1, 0x5

    iget-object v2, p0, Lpvi;->b:Ljava/lang/String;

    .line 93
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_4
    iget-object v1, p0, Lpvi;->d:Lpmm;

    if-eqz v1, :cond_5

    .line 96
    const/4 v1, 0x6

    iget-object v2, p0, Lpvi;->d:Lpmm;

    .line 97
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_5
    iget-object v1, p0, Lpvi;->i:Lpvk;

    if-eqz v1, :cond_6

    .line 100
    const/4 v1, 0x7

    iget-object v2, p0, Lpvi;->i:Lpvk;

    .line 101
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_6
    iget-object v1, p0, Lpvi;->f:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 104
    const/16 v1, 0x8

    iget-object v2, p0, Lpvi;->f:Ljava/lang/String;

    .line 105
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_7
    iget-object v1, p0, Lpvi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    iput v0, p0, Lpvi;->ai:I

    .line 109
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpvi;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 117
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 118
    sparse-switch v0, :sswitch_data_0

    .line 122
    iget-object v2, p0, Lpvi;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 123
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpvi;->ah:Ljava/util/List;

    .line 126
    :cond_1
    iget-object v2, p0, Lpvi;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    :sswitch_0
    return-object p0

    .line 133
    :sswitch_1
    iget-object v0, p0, Lpvi;->c:Lpgx;

    if-nez v0, :cond_2

    .line 134
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lpvi;->c:Lpgx;

    .line 136
    :cond_2
    iget-object v0, p0, Lpvi;->c:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 140
    :sswitch_2
    iget-object v0, p0, Lpvi;->g:Lpqw;

    if-nez v0, :cond_3

    .line 141
    new-instance v0, Lpqw;

    invoke-direct {v0}, Lpqw;-><init>()V

    iput-object v0, p0, Lpvi;->g:Lpqw;

    .line 143
    :cond_3
    iget-object v0, p0, Lpvi;->g:Lpqw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 147
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 148
    iget-object v0, p0, Lpvi;->e:[Lpvf;

    if-nez v0, :cond_5

    move v0, v1

    .line 149
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpvf;

    .line 150
    iget-object v3, p0, Lpvi;->e:[Lpvf;

    if-eqz v3, :cond_4

    .line 151
    iget-object v3, p0, Lpvi;->e:[Lpvf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    :cond_4
    iput-object v2, p0, Lpvi;->e:[Lpvf;

    .line 154
    :goto_2
    iget-object v2, p0, Lpvi;->e:[Lpvf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 155
    iget-object v2, p0, Lpvi;->e:[Lpvf;

    new-instance v3, Lpvf;

    invoke-direct {v3}, Lpvf;-><init>()V

    aput-object v3, v2, v0

    .line 156
    iget-object v2, p0, Lpvi;->e:[Lpvf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 157
    invoke-virtual {p1}, Loxn;->a()I

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 148
    :cond_5
    iget-object v0, p0, Lpvi;->e:[Lpvf;

    array-length v0, v0

    goto :goto_1

    .line 160
    :cond_6
    iget-object v2, p0, Lpvi;->e:[Lpvf;

    new-instance v3, Lpvf;

    invoke-direct {v3}, Lpvf;-><init>()V

    aput-object v3, v2, v0

    .line 161
    iget-object v2, p0, Lpvi;->e:[Lpvf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 165
    :sswitch_4
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpvi;->h:Ljava/lang/Long;

    goto/16 :goto_0

    .line 169
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvi;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 173
    :sswitch_6
    iget-object v0, p0, Lpvi;->d:Lpmm;

    if-nez v0, :cond_7

    .line 174
    new-instance v0, Lpmm;

    invoke-direct {v0}, Lpmm;-><init>()V

    iput-object v0, p0, Lpvi;->d:Lpmm;

    .line 176
    :cond_7
    iget-object v0, p0, Lpvi;->d:Lpmm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 180
    :sswitch_7
    iget-object v0, p0, Lpvi;->i:Lpvk;

    if-nez v0, :cond_8

    .line 181
    new-instance v0, Lpvk;

    invoke-direct {v0}, Lpvk;-><init>()V

    iput-object v0, p0, Lpvi;->i:Lpvk;

    .line 183
    :cond_8
    iget-object v0, p0, Lpvi;->i:Lpvk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 187
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvi;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 118
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 36
    iget-object v0, p0, Lpvi;->c:Lpgx;

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x1

    iget-object v1, p0, Lpvi;->c:Lpgx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 39
    :cond_0
    iget-object v0, p0, Lpvi;->g:Lpqw;

    if-eqz v0, :cond_1

    .line 40
    const/4 v0, 0x2

    iget-object v1, p0, Lpvi;->g:Lpqw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 42
    :cond_1
    iget-object v0, p0, Lpvi;->e:[Lpvf;

    if-eqz v0, :cond_3

    .line 43
    iget-object v1, p0, Lpvi;->e:[Lpvf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 44
    if-eqz v3, :cond_2

    .line 45
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 43
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_3
    iget-object v0, p0, Lpvi;->h:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 50
    const/4 v0, 0x4

    iget-object v1, p0, Lpvi;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 52
    :cond_4
    iget-object v0, p0, Lpvi;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 53
    const/4 v0, 0x5

    iget-object v1, p0, Lpvi;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 55
    :cond_5
    iget-object v0, p0, Lpvi;->d:Lpmm;

    if-eqz v0, :cond_6

    .line 56
    const/4 v0, 0x6

    iget-object v1, p0, Lpvi;->d:Lpmm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 58
    :cond_6
    iget-object v0, p0, Lpvi;->i:Lpvk;

    if-eqz v0, :cond_7

    .line 59
    const/4 v0, 0x7

    iget-object v1, p0, Lpvi;->i:Lpvk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 61
    :cond_7
    iget-object v0, p0, Lpvi;->f:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 62
    const/16 v0, 0x8

    iget-object v1, p0, Lpvi;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 64
    :cond_8
    iget-object v0, p0, Lpvi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 66
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpvi;->a(Loxn;)Lpvi;

    move-result-object v0

    return-object v0
.end method
