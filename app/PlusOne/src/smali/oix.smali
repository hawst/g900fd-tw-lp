.class public final Loix;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:[Lohp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3976
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3987
    sget-object v0, Lohp;->a:[Lohp;

    iput-object v0, p0, Loix;->e:[Lohp;

    .line 3976
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4017
    .line 4018
    iget-object v0, p0, Loix;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 4019
    const/4 v0, 0x1

    iget-object v2, p0, Loix;->b:Ljava/lang/Integer;

    .line 4020
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4022
    :goto_0
    iget-object v2, p0, Loix;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 4023
    const/4 v2, 0x2

    iget-object v3, p0, Loix;->a:Ljava/lang/Integer;

    .line 4024
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4026
    :cond_0
    iget-object v2, p0, Loix;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 4027
    const/4 v2, 0x5

    iget-object v3, p0, Loix;->c:Ljava/lang/Boolean;

    .line 4028
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4030
    :cond_1
    iget-object v2, p0, Loix;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 4031
    const/4 v2, 0x6

    iget-object v3, p0, Loix;->d:Ljava/lang/Boolean;

    .line 4032
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4034
    :cond_2
    iget-object v2, p0, Loix;->e:[Lohp;

    if-eqz v2, :cond_4

    .line 4035
    iget-object v2, p0, Loix;->e:[Lohp;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 4036
    if-eqz v4, :cond_3

    .line 4037
    const/4 v5, 0x7

    .line 4038
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4035
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4042
    :cond_4
    iget-object v1, p0, Loix;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4043
    iput v0, p0, Loix;->ai:I

    .line 4044
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loix;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4052
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4053
    sparse-switch v0, :sswitch_data_0

    .line 4057
    iget-object v2, p0, Loix;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4058
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loix;->ah:Ljava/util/List;

    .line 4061
    :cond_1
    iget-object v2, p0, Loix;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4063
    :sswitch_0
    return-object p0

    .line 4068
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loix;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 4072
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loix;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 4076
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loix;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 4080
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loix;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 4084
    :sswitch_5
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4085
    iget-object v0, p0, Loix;->e:[Lohp;

    if-nez v0, :cond_3

    move v0, v1

    .line 4086
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohp;

    .line 4087
    iget-object v3, p0, Loix;->e:[Lohp;

    if-eqz v3, :cond_2

    .line 4088
    iget-object v3, p0, Loix;->e:[Lohp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4090
    :cond_2
    iput-object v2, p0, Loix;->e:[Lohp;

    .line 4091
    :goto_2
    iget-object v2, p0, Loix;->e:[Lohp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 4092
    iget-object v2, p0, Loix;->e:[Lohp;

    new-instance v3, Lohp;

    invoke-direct {v3}, Lohp;-><init>()V

    aput-object v3, v2, v0

    .line 4093
    iget-object v2, p0, Loix;->e:[Lohp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4094
    invoke-virtual {p1}, Loxn;->a()I

    .line 4091
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4085
    :cond_3
    iget-object v0, p0, Loix;->e:[Lohp;

    array-length v0, v0

    goto :goto_1

    .line 4097
    :cond_4
    iget-object v2, p0, Loix;->e:[Lohp;

    new-instance v3, Lohp;

    invoke-direct {v3}, Lohp;-><init>()V

    aput-object v3, v2, v0

    .line 4098
    iget-object v2, p0, Loix;->e:[Lohp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4053
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x28 -> :sswitch_3
        0x30 -> :sswitch_4
        0x3a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3992
    iget-object v0, p0, Loix;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3993
    const/4 v0, 0x1

    iget-object v1, p0, Loix;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3995
    :cond_0
    iget-object v0, p0, Loix;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3996
    const/4 v0, 0x2

    iget-object v1, p0, Loix;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3998
    :cond_1
    iget-object v0, p0, Loix;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3999
    const/4 v0, 0x5

    iget-object v1, p0, Loix;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 4001
    :cond_2
    iget-object v0, p0, Loix;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 4002
    const/4 v0, 0x6

    iget-object v1, p0, Loix;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 4004
    :cond_3
    iget-object v0, p0, Loix;->e:[Lohp;

    if-eqz v0, :cond_5

    .line 4005
    iget-object v1, p0, Loix;->e:[Lohp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 4006
    if-eqz v3, :cond_4

    .line 4007
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 4005
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4011
    :cond_5
    iget-object v0, p0, Loix;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4013
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3972
    invoke-virtual {p0, p1}, Loix;->a(Loxn;)Loix;

    move-result-object v0

    return-object v0
.end method
