.class public final Lmxb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:I

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 9425
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9430
    iput v0, p0, Lmxb;->b:I

    .line 9433
    iput v0, p0, Lmxb;->c:I

    .line 9436
    iput v0, p0, Lmxb;->d:I

    .line 9439
    iput v0, p0, Lmxb;->e:I

    .line 9425
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 9465
    const/4 v0, 0x0

    .line 9466
    iget v1, p0, Lmxb;->b:I

    if-eq v1, v3, :cond_0

    .line 9467
    const/4 v0, 0x1

    iget v1, p0, Lmxb;->b:I

    .line 9468
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9470
    :cond_0
    iget v1, p0, Lmxb;->d:I

    if-eq v1, v3, :cond_1

    .line 9471
    const/4 v1, 0x2

    iget v2, p0, Lmxb;->d:I

    .line 9472
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9474
    :cond_1
    iget-object v1, p0, Lmxb;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 9475
    const/4 v1, 0x3

    iget-object v2, p0, Lmxb;->a:Ljava/lang/Integer;

    .line 9476
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9478
    :cond_2
    iget v1, p0, Lmxb;->c:I

    if-eq v1, v3, :cond_3

    .line 9479
    const/4 v1, 0x4

    iget v2, p0, Lmxb;->c:I

    .line 9480
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9482
    :cond_3
    iget v1, p0, Lmxb;->e:I

    if-eq v1, v3, :cond_4

    .line 9483
    const/4 v1, 0x5

    iget v2, p0, Lmxb;->e:I

    .line 9484
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9486
    :cond_4
    iget-object v1, p0, Lmxb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9487
    iput v0, p0, Lmxb;->ai:I

    .line 9488
    return v0
.end method

.method public a(Loxn;)Lmxb;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9496
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9497
    sparse-switch v0, :sswitch_data_0

    .line 9501
    iget-object v1, p0, Lmxb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9502
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmxb;->ah:Ljava/util/List;

    .line 9505
    :cond_1
    iget-object v1, p0, Lmxb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9507
    :sswitch_0
    return-object p0

    .line 9512
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 9513
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 9519
    :cond_2
    iput v0, p0, Lmxb;->b:I

    goto :goto_0

    .line 9521
    :cond_3
    iput v2, p0, Lmxb;->b:I

    goto :goto_0

    .line 9526
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 9527
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x5

    if-ne v0, v1, :cond_5

    .line 9533
    :cond_4
    iput v0, p0, Lmxb;->d:I

    goto :goto_0

    .line 9535
    :cond_5
    iput v2, p0, Lmxb;->d:I

    goto :goto_0

    .line 9540
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmxb;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 9544
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 9545
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-ne v0, v4, :cond_7

    .line 9548
    :cond_6
    iput v0, p0, Lmxb;->c:I

    goto :goto_0

    .line 9550
    :cond_7
    iput v2, p0, Lmxb;->c:I

    goto :goto_0

    .line 9555
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 9556
    if-eqz v0, :cond_8

    if-eq v0, v3, :cond_8

    if-ne v0, v4, :cond_9

    .line 9559
    :cond_8
    iput v0, p0, Lmxb;->e:I

    goto :goto_0

    .line 9561
    :cond_9
    iput v2, p0, Lmxb;->e:I

    goto :goto_0

    .line 9497
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 9444
    iget v0, p0, Lmxb;->b:I

    if-eq v0, v2, :cond_0

    .line 9445
    const/4 v0, 0x1

    iget v1, p0, Lmxb;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9447
    :cond_0
    iget v0, p0, Lmxb;->d:I

    if-eq v0, v2, :cond_1

    .line 9448
    const/4 v0, 0x2

    iget v1, p0, Lmxb;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9450
    :cond_1
    iget-object v0, p0, Lmxb;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 9451
    const/4 v0, 0x3

    iget-object v1, p0, Lmxb;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9453
    :cond_2
    iget v0, p0, Lmxb;->c:I

    if-eq v0, v2, :cond_3

    .line 9454
    const/4 v0, 0x4

    iget v1, p0, Lmxb;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9456
    :cond_3
    iget v0, p0, Lmxb;->e:I

    if-eq v0, v2, :cond_4

    .line 9457
    const/4 v0, 0x5

    iget v1, p0, Lmxb;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9459
    :cond_4
    iget-object v0, p0, Lmxb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9461
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9421
    invoke-virtual {p0, p1}, Lmxb;->a(Loxn;)Lmxb;

    move-result-object v0

    return-object v0
.end method
