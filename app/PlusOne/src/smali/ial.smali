.class public final Lial;
.super Lkik;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/debug/settings/DeveloperSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/debug/settings/DeveloperSettingsActivity;Los;Llqr;)V
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lial;->a:Lcom/google/android/libraries/social/debug/settings/DeveloperSettingsActivity;

    invoke-direct {p0, p2, p3}, Lkik;-><init>(Los;Llqr;)V

    return-void
.end method


# virtual methods
.method protected a()Lkhs;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 21
    iget-object v0, p0, Lial;->a:Lcom/google/android/libraries/social/debug/settings/DeveloperSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/debug/settings/DeveloperSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 22
    new-instance v1, Liam;

    invoke-direct {v1}, Liam;-><init>()V

    .line 25
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 26
    const-string v3, "args_browse_experiments_intent"

    const-string v4, "args_browse_experiments_intent"

    .line 27
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .line 26
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 28
    const-string v3, "args_account_status_intent"

    const-string v4, "args_account_status_intent"

    .line 29
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .line 28
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 30
    const-string v3, "args_network_requests_intent"

    const-string v4, "args_network_requests_intent"

    .line 31
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .line 30
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 32
    const-string v3, "args_network_stats_intent"

    const-string v4, "args_network_stats_intent"

    .line 33
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .line 32
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 34
    const-string v3, "extra_upload_stats_intent"

    const-string v4, "extra_upload_stats_intent"

    .line 35
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .line 34
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 36
    const-string v3, "args_show_apiary_pref"

    const-string v4, "args_show_apiary_pref"

    .line 37
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 36
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 38
    const-string v3, "args_show_override_pref"

    const-string v4, "args_show_override_pref"

    .line 39
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 38
    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 41
    const-string v3, "args_show_tracing_pref"

    const-string v4, "args_show_tracing_pref"

    .line 42
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 41
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 43
    invoke-virtual {v1, v2}, Liam;->f(Landroid/os/Bundle;)V

    .line 44
    return-object v1
.end method
