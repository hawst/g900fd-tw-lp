.class public final Lnbn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnbn;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lpme;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1127
    const/4 v0, 0x0

    new-array v0, v0, [Lnbn;

    sput-object v0, Lnbn;->a:[Lnbn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1128
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1133
    const/4 v0, 0x0

    iput-object v0, p0, Lnbn;->c:Lpme;

    .line 1128
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1150
    const/4 v0, 0x0

    .line 1151
    iget-object v1, p0, Lnbn;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1152
    const/4 v0, 0x1

    iget-object v1, p0, Lnbn;->b:Ljava/lang/String;

    .line 1153
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1155
    :cond_0
    iget-object v1, p0, Lnbn;->c:Lpme;

    if-eqz v1, :cond_1

    .line 1156
    const/4 v1, 0x2

    iget-object v2, p0, Lnbn;->c:Lpme;

    .line 1157
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1159
    :cond_1
    iget-object v1, p0, Lnbn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1160
    iput v0, p0, Lnbn;->ai:I

    .line 1161
    return v0
.end method

.method public a(Loxn;)Lnbn;
    .locals 2

    .prologue
    .line 1169
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1170
    sparse-switch v0, :sswitch_data_0

    .line 1174
    iget-object v1, p0, Lnbn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1175
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbn;->ah:Ljava/util/List;

    .line 1178
    :cond_1
    iget-object v1, p0, Lnbn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1180
    :sswitch_0
    return-object p0

    .line 1185
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnbn;->b:Ljava/lang/String;

    goto :goto_0

    .line 1189
    :sswitch_2
    iget-object v0, p0, Lnbn;->c:Lpme;

    if-nez v0, :cond_2

    .line 1190
    new-instance v0, Lpme;

    invoke-direct {v0}, Lpme;-><init>()V

    iput-object v0, p0, Lnbn;->c:Lpme;

    .line 1192
    :cond_2
    iget-object v0, p0, Lnbn;->c:Lpme;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1170
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1138
    iget-object v0, p0, Lnbn;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1139
    const/4 v0, 0x1

    iget-object v1, p0, Lnbn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1141
    :cond_0
    iget-object v0, p0, Lnbn;->c:Lpme;

    if-eqz v0, :cond_1

    .line 1142
    const/4 v0, 0x2

    iget-object v1, p0, Lnbn;->c:Lpme;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1144
    :cond_1
    iget-object v0, p0, Lnbn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1146
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1124
    invoke-virtual {p0, p1}, Lnbn;->a(Loxn;)Lnbn;

    move-result-object v0

    return-object v0
.end method
