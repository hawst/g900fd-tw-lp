.class public final Lcwd;
.super Lt;
.source "PG"


# instance fields
.field private N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcwn;",
            ">;"
        }
    .end annotation
.end field

.field private O:Lcwl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lt;-><init>()V

    .line 765
    return-void
.end method

.method public static a(Landroid/content/Context;Lnym;Landroid/net/Uri;)Lcwd;
    .locals 6

    .prologue
    .line 88
    if-eqz p1, :cond_6

    .line 89
    new-instance v1, Lcwk;

    invoke-direct {v1}, Lcwk;-><init>()V

    iget-object v0, p1, Lnym;->v:Lnyh;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lnym;->v:Lnyh;

    iget-object v0, v0, Lnyh;->a:Ljava/lang/Double;

    iput-object v0, v1, Lcwk;->a:Ljava/lang/Double;

    iget-object v0, p1, Lnym;->v:Lnyh;

    iget-object v0, v0, Lnyh;->b:Ljava/lang/Double;

    iput-object v0, v1, Lcwk;->b:Ljava/lang/Double;

    :cond_0
    iget-object v0, p1, Lnym;->j:Ljava/lang/String;

    iput-object v0, v1, Lcwk;->g:Ljava/lang/String;

    iget-object v0, p1, Lnym;->n:Ljava/lang/Double;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lcwk;->c:Ljava/lang/Long;

    iget-object v0, p1, Lnym;->b:Lnyl;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lnym;->b:Lnyl;

    iget-object v0, v0, Lnyl;->c:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lcwk;->d:Ljava/lang/Long;

    iget-object v0, p1, Lnym;->b:Lnyl;

    iget-object v0, v0, Lnyl;->d:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lcwk;->e:Ljava/lang/Long;

    :cond_1
    iget-object v0, p1, Lnym;->w:Ljava/lang/Long;

    iput-object v0, v1, Lcwk;->i:Ljava/lang/Long;

    iget-object v0, p1, Lnym;->m:Lnzb;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lnym;->m:Lnzb;

    iget-object v0, v0, Lnzb;->d:Ljava/lang/Long;

    iput-object v0, v1, Lcwk;->q:Ljava/lang/Long;

    :cond_2
    iget-object v0, p1, Lnym;->u:Lnyg;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->h:Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lcwk;->c:Ljava/lang/Long;

    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->i:Ljava/lang/Integer;

    iput-object v0, v1, Lcwk;->f:Ljava/lang/Integer;

    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->a:Ljava/lang/String;

    iput-object v0, v1, Lcwk;->n:Ljava/lang/String;

    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->b:Ljava/lang/String;

    iput-object v0, v1, Lcwk;->o:Ljava/lang/String;

    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcwk;->p:Ljava/lang/Integer;

    :cond_3
    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->f:Ljava/lang/Float;

    iput-object v0, v1, Lcwk;->j:Ljava/lang/Float;

    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->e:Ljava/lang/Float;

    iput-object v0, v1, Lcwk;->k:Ljava/lang/Float;

    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->d:Ljava/lang/Float;

    iput-object v0, v1, Lcwk;->l:Ljava/lang/Float;

    iget-object v0, p1, Lnym;->u:Lnyg;

    iget-object v0, v0, Lnyg;->c:Ljava/lang/Integer;

    iput-object v0, v1, Lcwk;->m:Ljava/lang/Integer;

    :cond_4
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v0, "info_list"

    invoke-static {p0, v1}, Lcwd;->b(Landroid/content/Context;Lcwk;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    new-instance v0, Lcwd;

    invoke-direct {v0}, Lcwd;-><init>()V

    invoke-virtual {v0, v2}, Lcwd;->f(Landroid/os/Bundle;)V

    .line 91
    :goto_1
    return-object v0

    .line 89
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 91
    :cond_6
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "local_uri"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v0, Lcwd;

    invoke-direct {v0}, Lcwd;-><init>()V

    invoke-virtual {v0, v1}, Lcwd;->f(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method static synthetic a(Lcwd;)Lcwl;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcwd;->O:Lcwl;

    return-object v0
.end method

.method static synthetic a(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v0

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;Lcwk;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 61
    invoke-static {p0, p1}, Lcwd;->b(Landroid/content/Context;Lcwk;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Lcwk;)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcwk;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcwn;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    const-wide v10, 0x416312d000000000L    # 1.0E7

    const-wide v8, 0x4066800000000000L    # 180.0

    const-wide v6, 0x4056800000000000L    # 90.0

    const-wide/16 v4, 0x0

    .line 173
    new-instance v0, Lcwr;

    invoke-direct {v0, p0}, Lcwr;-><init>(Landroid/content/Context;)V

    .line 174
    if-nez p1, :cond_0

    .line 175
    invoke-virtual {v0}, Lcwr;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    .line 178
    :cond_0
    iget-object v1, p1, Lcwk;->a:Ljava/lang/Double;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcwk;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpl-double v1, v2, v6

    if-lez v1, :cond_1

    iget-object v1, p1, Lcwk;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    div-double/2addr v2, v10

    cmpl-double v1, v2, v6

    if-lez v1, :cond_6

    const/4 v1, 0x0

    iput-object v1, p1, Lcwk;->a:Ljava/lang/Double;

    :cond_1
    :goto_1
    iget-object v1, p1, Lcwk;->b:Ljava/lang/Double;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcwk;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpl-double v1, v2, v8

    if-lez v1, :cond_2

    iget-object v1, p1, Lcwk;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    div-double/2addr v2, v10

    cmpl-double v1, v2, v8

    if-lez v1, :cond_7

    const/4 v1, 0x0

    iput-object v1, p1, Lcwk;->b:Ljava/lang/Double;

    .line 180
    :cond_2
    :goto_2
    iget-object v1, p1, Lcwk;->a:Ljava/lang/Double;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v2

    cmpl-double v1, v2, v4

    if-nez v1, :cond_3

    iget-object v1, p1, Lcwk;->b:Ljava/lang/Double;

    .line 181
    invoke-static {v1}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v2

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_4

    .line 182
    :cond_3
    iget-object v1, p1, Lcwk;->a:Ljava/lang/Double;

    iget-object v2, p1, Lcwk;->b:Ljava/lang/Double;

    invoke-virtual {v0, v1, v2}, Lcwr;->a(Ljava/lang/Double;Ljava/lang/Double;)V

    .line 183
    const v1, 0x7f0a0660

    const-string v2, "%.3f, %.3f"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcwk;->a:Ljava/lang/Double;

    aput-object v4, v3, v12

    const/4 v4, 0x1

    iget-object v5, p1, Lcwk;->b:Ljava/lang/Double;

    aput-object v5, v3, v4

    .line 184
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 183
    invoke-virtual {v0, v1, v2}, Lcwr;->a(ILjava/lang/Object;)V

    .line 186
    :cond_4
    const v1, 0x7f0a065f

    iget-object v2, p1, Lcwk;->c:Ljava/lang/Long;

    new-instance v3, Lcwe;

    invoke-direct {v3}, Lcwe;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcwr;->a(ILjava/lang/Object;Lcwq;)V

    .line 193
    iget-object v1, p1, Lcwk;->d:Ljava/lang/Long;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcwk;->e:Ljava/lang/Long;

    .line 194
    invoke-static {v1}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 195
    const v1, 0x7f0a0661

    const-string v2, "%d x %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcwk;->d:Ljava/lang/Long;

    aput-object v4, v3, v12

    const/4 v4, 0x1

    iget-object v5, p1, Lcwk;->e:Ljava/lang/Long;

    aput-object v5, v3, v4

    .line 196
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 195
    invoke-virtual {v0, v1, v2}, Lcwr;->a(ILjava/lang/Object;)V

    .line 198
    :cond_5
    const v1, 0x7f0a0673

    iget-object v2, p1, Lcwk;->q:Ljava/lang/Long;

    new-instance v3, Lcwj;

    invoke-direct {v3}, Lcwj;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcwr;->a(ILjava/lang/Object;Lcwq;)V

    .line 199
    const v1, 0x7f0a0662

    iget-object v2, p1, Lcwk;->f:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Lcwr;->a(ILjava/lang/Object;)V

    .line 200
    const v1, 0x7f0a065e

    iget-object v2, p1, Lcwk;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcwr;->a(ILjava/lang/Object;)V

    .line 201
    const v1, 0x7f0a0663

    iget-object v2, p1, Lcwk;->i:Ljava/lang/Long;

    new-instance v3, Lcws;

    invoke-direct {v3, p0}, Lcws;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2, v3}, Lcwr;->a(ILjava/lang/Object;Lcwq;)V

    .line 202
    const v1, 0x7f0a0669

    iget-object v2, p1, Lcwk;->j:Ljava/lang/Float;

    new-instance v3, Lcwf;

    invoke-direct {v3, p0}, Lcwf;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2, v3}, Lcwr;->a(ILjava/lang/Object;Lcwq;)V

    .line 209
    const v1, 0x7f0a066b

    iget-object v2, p1, Lcwk;->k:Ljava/lang/Float;

    new-instance v3, Lcwg;

    invoke-direct {v3, p0}, Lcwg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2, v3}, Lcwr;->a(ILjava/lang/Object;Lcwq;)V

    .line 216
    const v1, 0x7f0a066d

    iget-object v2, p1, Lcwk;->l:Ljava/lang/Float;

    new-instance v3, Lcwh;

    invoke-direct {v3, p0}, Lcwh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2, v3}, Lcwr;->a(ILjava/lang/Object;Lcwq;)V

    .line 228
    const v1, 0x7f0a0670

    iget-object v2, p1, Lcwk;->m:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Lcwr;->a(ILjava/lang/Object;)V

    .line 229
    const v1, 0x7f0a0664

    iget-object v2, p1, Lcwk;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcwr;->a(ILjava/lang/Object;)V

    .line 230
    const v1, 0x7f0a0665

    iget-object v2, p1, Lcwk;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcwr;->a(ILjava/lang/Object;)V

    .line 231
    const v1, 0x7f0a0666

    iget-object v2, p1, Lcwk;->p:Ljava/lang/Integer;

    new-instance v3, Lcwi;

    invoke-direct {v3, p0}, Lcwi;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2, v3}, Lcwr;->a(ILjava/lang/Object;Lcwq;)V

    .line 238
    const v1, 0x7f0a0671

    iget-object v2, p1, Lcwk;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcwr;->a(ILjava/lang/Object;)V

    .line 240
    invoke-virtual {v0}, Lcwr;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto/16 :goto_0

    .line 178
    :cond_6
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p1, Lcwk;->a:Ljava/lang/Double;

    goto/16 :goto_1

    :cond_7
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p1, Lcwk;->b:Ljava/lang/Double;

    goto/16 :goto_2
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 157
    const v0, 0x7f0400a0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 158
    const v0, 0x7f100287

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 159
    const v2, 0x7f100286

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 160
    new-instance v2, Lcwl;

    iget-object v3, p0, Lcwd;->N:Ljava/util/List;

    invoke-direct {v2, v3, p1}, Lcwl;-><init>(Ljava/util/List;Landroid/view/LayoutInflater;)V

    iput-object v2, p0, Lcwd;->O:Lcwl;

    .line 161
    iget-object v2, p0, Lcwd;->O:Lcwl;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 163
    invoke-virtual {p0}, Lcwd;->c()Landroid/app/Dialog;

    move-result-object v0

    const v2, 0x7f0a0672

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(I)V

    .line 165
    return-object v1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 145
    invoke-super {p0, p1}, Lt;->a(Landroid/os/Bundle;)V

    .line 146
    invoke-virtual {p0}, Lcwd;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "local_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lcwd;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    .line 148
    invoke-virtual {p0}, Lcwd;->k()Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Lcwp;

    invoke-virtual {p0}, Lcwd;->n()Lz;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcwp;-><init>(Lcwd;Landroid/content/Context;)V

    .line 147
    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 152
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-virtual {p0}, Lcwd;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "info_list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcwd;->N:Ljava/util/List;

    goto :goto_0
.end method
