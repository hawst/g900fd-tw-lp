.class public final Lfaj;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 55
    iput-object p3, p0, Lfaj;->b:Ljava/util/ArrayList;

    .line 56
    iput p2, p0, Lfaj;->e:I

    .line 57
    iput-boolean p6, p0, Lfaj;->f:Z

    .line 58
    iput-object p4, p0, Lfaj;->c:Ljava/util/ArrayList;

    .line 59
    iput-object p5, p0, Lfaj;->d:Ljava/util/ArrayList;

    .line 60
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 22

    .prologue
    .line 64
    invoke-virtual/range {p0 .. p0}, Lfaj;->n()Landroid/content/Context;

    move-result-object v7

    .line 65
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 66
    new-instance v9, Lhym;

    sget-object v2, Lfak;->a:[Ljava/lang/String;

    invoke-direct {v9, v2}, Lhym;-><init>([Ljava/lang/String;)V

    .line 67
    sget-object v2, Lfak;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    new-array v10, v2, [Ljava/lang/Object;

    .line 68
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 69
    new-instance v12, Ljava/util/HashSet;

    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    .line 71
    move-object/from16 v0, p0

    iget-object v2, v0, Lfaj;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 72
    const/4 v2, 0x0

    move v6, v2

    :goto_0
    if-ge v6, v13, :cond_8

    .line 73
    move-object/from16 v0, p0

    iget-object v2, v0, Lfaj;->b:Ljava/util/ArrayList;

    .line 75
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    const/high16 v3, 0x10000

    .line 74
    invoke-virtual {v8, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 76
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 78
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 82
    const v3, 0x7f0a024d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 83
    const v3, 0x7f0a05e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 85
    move-object/from16 v0, p0

    iget v2, v0, Lfaj;->e:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    if-nez v6, :cond_0

    .line 86
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_1
    if-ltz v3, :cond_0

    .line 88
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v2, v8}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 87
    invoke-static {v15, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89
    invoke-interface {v5, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 90
    const/4 v3, 0x0

    invoke-interface {v14, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 96
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lfaj;->f:Z

    if-eqz v2, :cond_3

    .line 97
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v17

    .line 98
    const/4 v2, 0x0

    move v4, v2

    :goto_2
    move/from16 v0, v17

    if-ge v4, v0, :cond_3

    .line 99
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 100
    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v12, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 102
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v3, v8}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 101
    move-object/from16 v0, v16

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 103
    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 86
    :cond_2
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_1

    .line 108
    :cond_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 109
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v17

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lfaj;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 113
    const/4 v3, 0x0

    move v5, v3

    :goto_3
    move/from16 v0, v17

    if-ge v5, v0, :cond_7

    .line 114
    invoke-interface {v14, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 115
    invoke-virtual {v3, v8}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v18

    .line 116
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v19

    .line 119
    move-object/from16 v0, v18

    invoke-static {v0, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lfaj;->d:Ljava/util/ArrayList;

    .line 118
    :goto_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_6

    const-string v20, "android.intent.extra.STREAM"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 120
    :cond_4
    :goto_5
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 121
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/ResolveInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122
    invoke-virtual/range {v19 .. v19}, Landroid/os/Parcel;->marshall()[B

    move-result-object v4

    .line 124
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-static {v10, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 125
    const/16 v19, 0x0

    add-int v20, v16, v5

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v10, v19

    .line 126
    const/16 v19, 0x1

    aput-object v18, v10, v19

    .line 127
    const/16 v18, 0x2

    aput-object v4, v10, v18

    .line 128
    invoke-virtual {v9, v10}, Lhym;->a([Ljava/lang/Object;)V

    .line 130
    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v12, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    .line 119
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lfaj;->c:Ljava/util/ArrayList;

    goto :goto_4

    .line 118
    :cond_6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    const-string v20, "android.intent.extra.STREAM"

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Parcelable;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_5

    .line 134
    :cond_7
    invoke-virtual {v11, v14}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 72
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_0

    .line 137
    :cond_8
    return-object v9
.end method
