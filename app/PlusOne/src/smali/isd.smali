.class public final Lisd;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# instance fields
.field private a:Liso;

.field private b:I

.field private c:Landroid/view/LayoutInflater;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 48
    const/4 v0, 0x2

    iput v0, p0, Lisd;->b:I

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lisd;->d:I

    .line 54
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lisd;->c:Landroid/view/LayoutInflater;

    .line 56
    return-void
.end method


# virtual methods
.method public a()Liso;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lisd;->a:Liso;

    return-object v0
.end method

.method public a(Liso;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lisd;->a:Liso;

    .line 60
    invoke-virtual {p0}, Lisd;->notifyDataSetChanged()V

    .line 61
    return-void
.end method

.method public a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 129
    invoke-virtual {p0, p1}, Lisd;->getItemViewType(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method

.method public b(I)I
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lisd;->a:Liso;

    if-nez v0, :cond_0

    .line 198
    const/4 v0, -0x1

    .line 200
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lisd;->a:Liso;

    iget v1, p0, Lisd;->b:I

    invoke-virtual {v0, p1, v1}, Liso;->c(II)I

    move-result v0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lisd;->a:Liso;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lisd;->a:Liso;

    invoke-virtual {v0}, Liso;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lisd;->a:Liso;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lisd;->a:Liso;

    invoke-virtual {v0}, Liso;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lisd;->a:Liso;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lisd;->a:Liso;

    invoke-virtual {v0}, Liso;->f()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lisd;->a:Liso;

    iget v1, p0, Lisd;->b:I

    invoke-virtual {v0, p1, v1}, Liso;->a(II)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 109
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lisd;->getSectionForPosition(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lisd;->getPositionForSection(I)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 122
    const/4 v0, 0x1

    .line 124
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPositionForSection(I)I
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lisd;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 166
    const/4 v0, 0x0

    .line 172
    :goto_0
    return v0

    .line 168
    :cond_0
    invoke-virtual {p0}, Lisd;->getSections()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    .line 169
    if-lt p1, v0, :cond_1

    .line 170
    add-int/lit8 p1, v0, -0x1

    .line 172
    :cond_1
    iget-object v0, p0, Lisd;->a:Liso;

    iget v1, p0, Lisd;->b:I

    invoke-virtual {v0, p1, v1}, Liso;->e(II)I

    move-result v0

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p0}, Lisd;->getCount()I

    move-result v0

    .line 178
    if-nez v0, :cond_0

    .line 179
    const/4 v0, 0x0

    .line 184
    :goto_0
    return v0

    .line 181
    :cond_0
    if-lt p1, v0, :cond_1

    .line 182
    add-int/lit8 p1, v0, -0x1

    .line 184
    :cond_1
    iget-object v0, p0, Lisd;->a:Liso;

    iget v1, p0, Lisd;->b:I

    invoke-virtual {v0, p1, v1}, Liso;->f(II)I

    move-result v0

    goto :goto_0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 189
    invoke-virtual {p0}, Lisd;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lisd;->a:Liso;

    iget v1, p0, Lisd;->b:I

    invoke-virtual {v0, v1}, Liso;->b(I)[Lisg;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 138
    invoke-virtual {p0, p1}, Lisd;->getItemViewType(I)I

    move-result v0

    .line 139
    if-nez v0, :cond_1

    .line 141
    if-nez p2, :cond_0

    .line 142
    iget-object v0, p0, Lisd;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0400ec

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ingest/ui/MtpThumbnailTileView;

    move-object p2, v0

    .line 147
    :goto_0
    iget-object v0, p0, Lisd;->a:Liso;

    invoke-virtual {v0}, Liso;->b()Landroid/mtp/MtpDevice;

    move-result-object v1

    .line 148
    invoke-virtual {p0, p1}, Lisd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lisj;

    iget v2, p0, Lisd;->d:I

    .line 147
    invoke-virtual {p2, v1, v0, v2}, Lcom/google/android/libraries/social/ingest/ui/MtpThumbnailTileView;->a(Landroid/mtp/MtpDevice;Lisj;I)V

    .line 159
    :goto_1
    return-object p2

    .line 145
    :cond_0
    check-cast p2, Lcom/google/android/libraries/social/ingest/ui/MtpThumbnailTileView;

    goto :goto_0

    .line 152
    :cond_1
    if-nez p2, :cond_2

    .line 153
    iget-object v0, p0, Lisd;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0400ea

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ingest/ui/DateTileView;

    move-object p2, v0

    .line 158
    :goto_2
    invoke-virtual {p0, p1}, Lisd;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lisu;

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/ingest/ui/DateTileView;->a(Lisu;)V

    goto :goto_1

    .line 156
    :cond_2
    check-cast p2, Lcom/google/android/libraries/social/ingest/ui/DateTileView;

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lisd;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lisd;->d:I

    .line 70
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 71
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lisd;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lisd;->d:I

    .line 76
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    .line 77
    return-void
.end method
