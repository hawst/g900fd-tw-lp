.class public final Locm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Locm;


# instance fields
.field public b:Locn;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Locc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 738
    const/4 v0, 0x0

    new-array v0, v0, [Locm;

    sput-object v0, Locm;->a:[Locm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 739
    invoke-direct {p0}, Loxq;-><init>()V

    .line 742
    iput-object v0, p0, Locm;->b:Locn;

    .line 749
    iput-object v0, p0, Locm;->e:Locc;

    .line 739
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 772
    const/4 v0, 0x0

    .line 773
    iget-object v1, p0, Locm;->b:Locn;

    if-eqz v1, :cond_0

    .line 774
    const/4 v0, 0x1

    iget-object v1, p0, Locm;->b:Locn;

    .line 775
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 777
    :cond_0
    iget-object v1, p0, Locm;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 778
    const/4 v1, 0x2

    iget-object v2, p0, Locm;->c:Ljava/lang/String;

    .line 779
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 781
    :cond_1
    iget-object v1, p0, Locm;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 782
    const/4 v1, 0x3

    iget-object v2, p0, Locm;->d:Ljava/lang/String;

    .line 783
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 785
    :cond_2
    iget-object v1, p0, Locm;->e:Locc;

    if-eqz v1, :cond_3

    .line 786
    const/4 v1, 0x4

    iget-object v2, p0, Locm;->e:Locc;

    .line 787
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 789
    :cond_3
    iget-object v1, p0, Locm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 790
    iput v0, p0, Locm;->ai:I

    .line 791
    return v0
.end method

.method public a(Loxn;)Locm;
    .locals 2

    .prologue
    .line 799
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 800
    sparse-switch v0, :sswitch_data_0

    .line 804
    iget-object v1, p0, Locm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 805
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Locm;->ah:Ljava/util/List;

    .line 808
    :cond_1
    iget-object v1, p0, Locm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 810
    :sswitch_0
    return-object p0

    .line 815
    :sswitch_1
    iget-object v0, p0, Locm;->b:Locn;

    if-nez v0, :cond_2

    .line 816
    new-instance v0, Locn;

    invoke-direct {v0}, Locn;-><init>()V

    iput-object v0, p0, Locm;->b:Locn;

    .line 818
    :cond_2
    iget-object v0, p0, Locm;->b:Locn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 822
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Locm;->c:Ljava/lang/String;

    goto :goto_0

    .line 826
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Locm;->d:Ljava/lang/String;

    goto :goto_0

    .line 830
    :sswitch_4
    iget-object v0, p0, Locm;->e:Locc;

    if-nez v0, :cond_3

    .line 831
    new-instance v0, Locc;

    invoke-direct {v0}, Locc;-><init>()V

    iput-object v0, p0, Locm;->e:Locc;

    .line 833
    :cond_3
    iget-object v0, p0, Locm;->e:Locc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 800
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 754
    iget-object v0, p0, Locm;->b:Locn;

    if-eqz v0, :cond_0

    .line 755
    const/4 v0, 0x1

    iget-object v1, p0, Locm;->b:Locn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 757
    :cond_0
    iget-object v0, p0, Locm;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 758
    const/4 v0, 0x2

    iget-object v1, p0, Locm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 760
    :cond_1
    iget-object v0, p0, Locm;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 761
    const/4 v0, 0x3

    iget-object v1, p0, Locm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 763
    :cond_2
    iget-object v0, p0, Locm;->e:Locc;

    if-eqz v0, :cond_3

    .line 764
    const/4 v0, 0x4

    iget-object v1, p0, Locm;->e:Locc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 766
    :cond_3
    iget-object v0, p0, Locm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 768
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 735
    invoke-virtual {p0, p1}, Locm;->a(Loxn;)Locm;

    move-result-object v0

    return-object v0
.end method
