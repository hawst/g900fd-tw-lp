.class public final Loss;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loss;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lota;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lose;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    new-array v0, v0, [Loss;

    sput-object v0, Loss;->a:[Loss;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 169
    invoke-direct {p0}, Loxq;-><init>()V

    .line 174
    iput-object v0, p0, Loss;->c:Lota;

    .line 183
    iput-object v0, p0, Loss;->g:Lose;

    .line 169
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 212
    const/4 v0, 0x0

    .line 213
    iget-object v1, p0, Loss;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 214
    const/4 v0, 0x1

    iget-object v1, p0, Loss;->b:Ljava/lang/String;

    .line 215
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 217
    :cond_0
    iget-object v1, p0, Loss;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 218
    const/4 v1, 0x2

    iget-object v2, p0, Loss;->d:Ljava/lang/String;

    .line 219
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    :cond_1
    iget-object v1, p0, Loss;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 222
    const/4 v1, 0x3

    iget-object v2, p0, Loss;->e:Ljava/lang/String;

    .line 223
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    :cond_2
    iget-object v1, p0, Loss;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 226
    const/4 v1, 0x4

    iget-object v2, p0, Loss;->f:Ljava/lang/String;

    .line 227
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    :cond_3
    iget-object v1, p0, Loss;->g:Lose;

    if-eqz v1, :cond_4

    .line 230
    const/4 v1, 0x5

    iget-object v2, p0, Loss;->g:Lose;

    .line 231
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_4
    iget-object v1, p0, Loss;->c:Lota;

    if-eqz v1, :cond_5

    .line 234
    const/4 v1, 0x6

    iget-object v2, p0, Loss;->c:Lota;

    .line 235
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_5
    iget-object v1, p0, Loss;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    iput v0, p0, Loss;->ai:I

    .line 239
    return v0
.end method

.method public a(Loxn;)Loss;
    .locals 2

    .prologue
    .line 247
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 248
    sparse-switch v0, :sswitch_data_0

    .line 252
    iget-object v1, p0, Loss;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loss;->ah:Ljava/util/List;

    .line 256
    :cond_1
    iget-object v1, p0, Loss;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    :sswitch_0
    return-object p0

    .line 263
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loss;->b:Ljava/lang/String;

    goto :goto_0

    .line 267
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loss;->d:Ljava/lang/String;

    goto :goto_0

    .line 271
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loss;->e:Ljava/lang/String;

    goto :goto_0

    .line 275
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loss;->f:Ljava/lang/String;

    goto :goto_0

    .line 279
    :sswitch_5
    iget-object v0, p0, Loss;->g:Lose;

    if-nez v0, :cond_2

    .line 280
    new-instance v0, Lose;

    invoke-direct {v0}, Lose;-><init>()V

    iput-object v0, p0, Loss;->g:Lose;

    .line 282
    :cond_2
    iget-object v0, p0, Loss;->g:Lose;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 286
    :sswitch_6
    iget-object v0, p0, Loss;->c:Lota;

    if-nez v0, :cond_3

    .line 287
    new-instance v0, Lota;

    invoke-direct {v0}, Lota;-><init>()V

    iput-object v0, p0, Loss;->c:Lota;

    .line 289
    :cond_3
    iget-object v0, p0, Loss;->c:Lota;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 248
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Loss;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 189
    const/4 v0, 0x1

    iget-object v1, p0, Loss;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 191
    :cond_0
    iget-object v0, p0, Loss;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 192
    const/4 v0, 0x2

    iget-object v1, p0, Loss;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 194
    :cond_1
    iget-object v0, p0, Loss;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 195
    const/4 v0, 0x3

    iget-object v1, p0, Loss;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 197
    :cond_2
    iget-object v0, p0, Loss;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 198
    const/4 v0, 0x4

    iget-object v1, p0, Loss;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 200
    :cond_3
    iget-object v0, p0, Loss;->g:Lose;

    if-eqz v0, :cond_4

    .line 201
    const/4 v0, 0x5

    iget-object v1, p0, Loss;->g:Lose;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 203
    :cond_4
    iget-object v0, p0, Loss;->c:Lota;

    if-eqz v0, :cond_5

    .line 204
    const/4 v0, 0x6

    iget-object v1, p0, Loss;->c:Lota;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 206
    :cond_5
    iget-object v0, p0, Loss;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 208
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Loss;->a(Loxn;)Loss;

    move-result-object v0

    return-object v0
.end method
