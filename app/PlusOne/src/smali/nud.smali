.class public final Lnud;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnud;


# instance fields
.field private b:I

.field private c:Lpym;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1097
    const/4 v0, 0x0

    new-array v0, v0, [Lnud;

    sput-object v0, Lnud;->a:[Lnud;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1098
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1101
    const/high16 v0, -0x80000000

    iput v0, p0, Lnud;->b:I

    .line 1104
    const/4 v0, 0x0

    iput-object v0, p0, Lnud;->c:Lpym;

    .line 1098
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1121
    const/4 v0, 0x0

    .line 1122
    iget v1, p0, Lnud;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1123
    const/4 v0, 0x1

    iget v1, p0, Lnud;->b:I

    .line 1124
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1126
    :cond_0
    iget-object v1, p0, Lnud;->c:Lpym;

    if-eqz v1, :cond_1

    .line 1127
    const/4 v1, 0x2

    iget-object v2, p0, Lnud;->c:Lpym;

    .line 1128
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1130
    :cond_1
    iget-object v1, p0, Lnud;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1131
    iput v0, p0, Lnud;->ai:I

    .line 1132
    return v0
.end method

.method public a(Loxn;)Lnud;
    .locals 2

    .prologue
    .line 1140
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1141
    sparse-switch v0, :sswitch_data_0

    .line 1145
    iget-object v1, p0, Lnud;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1146
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnud;->ah:Ljava/util/List;

    .line 1149
    :cond_1
    iget-object v1, p0, Lnud;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1151
    :sswitch_0
    return-object p0

    .line 1156
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1157
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1160
    :cond_2
    iput v0, p0, Lnud;->b:I

    goto :goto_0

    .line 1162
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnud;->b:I

    goto :goto_0

    .line 1167
    :sswitch_2
    iget-object v0, p0, Lnud;->c:Lpym;

    if-nez v0, :cond_4

    .line 1168
    new-instance v0, Lpym;

    invoke-direct {v0}, Lpym;-><init>()V

    iput-object v0, p0, Lnud;->c:Lpym;

    .line 1170
    :cond_4
    iget-object v0, p0, Lnud;->c:Lpym;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1141
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1109
    iget v0, p0, Lnud;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1110
    const/4 v0, 0x1

    iget v1, p0, Lnud;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1112
    :cond_0
    iget-object v0, p0, Lnud;->c:Lpym;

    if-eqz v0, :cond_1

    .line 1113
    const/4 v0, 0x2

    iget-object v1, p0, Lnud;->c:Lpym;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1115
    :cond_1
    iget-object v0, p0, Lnud;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1117
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1094
    invoke-virtual {p0, p1}, Lnud;->a(Loxn;)Lnud;

    move-result-object v0

    return-object v0
.end method
