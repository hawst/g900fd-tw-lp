.class public final Lfob;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lmmq;

.field public b:Lfod;

.field public c:J

.field public d:Z

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 19

    .prologue
    .line 37
    move-object/from16 v0, p1

    instance-of v1, v0, Lfob;

    if-eqz v1, :cond_b

    if-eqz p1, :cond_b

    move-object/from16 v1, p1

    .line 38
    check-cast v1, Lfob;

    iget-object v10, v1, Lfob;->a:Lmmq;

    .line 39
    move-object/from16 v0, p0

    iget-object v11, v0, Lfob;->a:Lmmq;

    if-eqz v10, :cond_0

    iget-object v1, v10, Lmmq;->a:Lmok;

    if-eqz v1, :cond_0

    iget-object v1, v10, Lmmq;->a:Lmok;

    iget-object v1, v1, Lmok;->b:Lmol;

    if-eqz v1, :cond_0

    if-eqz v11, :cond_0

    iget-object v1, v11, Lmmq;->a:Lmok;

    if-eqz v1, :cond_0

    iget-object v1, v11, Lmmq;->a:Lmok;

    iget-object v1, v1, Lmok;->b:Lmol;

    if-eqz v1, :cond_0

    iget-object v1, v10, Lmmq;->a:Lmok;

    iget-object v2, v11, Lmmq;->a:Lmok;

    iget-object v2, v2, Lmok;->b:Lmol;

    iput-object v2, v1, Lmok;->b:Lmol;

    :cond_0
    if-eqz v10, :cond_1

    iget-object v1, v10, Lmmq;->g:Lmlz;

    if-eqz v1, :cond_1

    iget-object v1, v10, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    if-eqz v1, :cond_1

    iget-object v1, v10, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    iget-object v1, v1, Logr;->C:Loae;

    if-eqz v1, :cond_1

    iget-object v1, v10, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    iget-object v1, v1, Logr;->C:Loae;

    iget-object v1, v1, Loae;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    if-eqz v11, :cond_1

    iget-object v1, v11, Lmmq;->g:Lmlz;

    if-eqz v1, :cond_1

    iget-object v1, v11, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    if-eqz v1, :cond_1

    iget-object v1, v11, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    iget-object v1, v1, Logr;->C:Loae;

    if-eqz v1, :cond_1

    iget-object v1, v11, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    iget-object v1, v1, Logr;->C:Loae;

    iget-object v1, v1, Loae;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v10, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->b:Logr;

    iget-object v1, v1, Logr;->C:Loae;

    iget-object v2, v11, Lmmq;->g:Lmlz;

    iget-object v2, v2, Lmlz;->b:Logr;

    iget-object v2, v2, Logr;->C:Loae;

    iget-object v2, v2, Loae;->d:Ljava/lang/String;

    iput-object v2, v1, Loae;->d:Ljava/lang/String;

    :cond_1
    if-eqz v10, :cond_5

    iget-object v1, v10, Lmmq;->c:Lmoo;

    if-eqz v1, :cond_5

    iget-object v1, v10, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->c:[Lnym;

    if-eqz v1, :cond_5

    if-eqz v11, :cond_5

    iget-object v1, v11, Lmmq;->c:Lmoo;

    if-eqz v1, :cond_5

    iget-object v1, v11, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->c:[Lnym;

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v1, v11, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->c:[Lnym;

    array-length v1, v1

    if-ge v2, v1, :cond_5

    iget-object v1, v10, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->c:[Lnym;

    array-length v1, v1

    if-ge v2, v1, :cond_5

    iget-object v1, v10, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->c:[Lnym;

    aget-object v1, v1, v2

    iget-object v1, v1, Lnym;->r:Loae;

    if-eqz v1, :cond_2

    iget-object v1, v10, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->c:[Lnym;

    aget-object v1, v1, v2

    iget-object v1, v1, Lnym;->r:Loae;

    iget-object v1, v1, Loae;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v11, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->c:[Lnym;

    aget-object v1, v1, v2

    iget-object v1, v1, Lnym;->r:Loae;

    if-eqz v1, :cond_2

    iget-object v1, v11, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->c:[Lnym;

    aget-object v1, v1, v2

    iget-object v1, v1, Lnym;->r:Loae;

    iget-object v1, v1, Loae;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v10, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->c:[Lnym;

    aget-object v1, v1, v2

    iget-object v1, v1, Lnym;->r:Loae;

    iget-object v3, v11, Lmmq;->c:Lmoo;

    iget-object v3, v3, Lmoo;->c:[Lnym;

    aget-object v3, v3, v2

    iget-object v3, v3, Lnym;->r:Loae;

    iget-object v3, v3, Loae;->d:Ljava/lang/String;

    iput-object v3, v1, Loae;->d:Ljava/lang/String;

    :cond_2
    const/4 v3, 0x0

    iget-object v1, v11, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->d:[Lnzx;

    aget-object v1, v1, v2

    if-eqz v1, :cond_3

    iget-object v1, v11, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->d:[Lnzx;

    aget-object v1, v1, v2

    sget-object v4, Lnzu;->a:Loxr;

    invoke-virtual {v1, v4}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzu;

    iget-object v1, v1, Lnzu;->b:Lnym;

    if-eqz v1, :cond_3

    iget-object v4, v1, Lnym;->r:Loae;

    if-eqz v4, :cond_3

    iget-object v1, v1, Lnym;->r:Loae;

    move-object v3, v1

    :cond_3
    const/4 v4, 0x0

    iget-object v1, v10, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->d:[Lnzx;

    aget-object v1, v1, v2

    if-eqz v1, :cond_12

    iget-object v1, v10, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->d:[Lnzx;

    aget-object v1, v1, v2

    sget-object v5, Lnzu;->a:Loxr;

    invoke-virtual {v1, v5}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzu;

    iget-object v1, v1, Lnzu;->b:Lnym;

    if-eqz v1, :cond_12

    iget-object v5, v1, Lnym;->r:Loae;

    if-eqz v5, :cond_12

    iget-object v1, v1, Lnym;->r:Loae;

    :goto_1
    if-eqz v3, :cond_4

    iget-object v4, v3, Loae;->d:Ljava/lang/String;

    if-eqz v4, :cond_4

    if-eqz v1, :cond_4

    iget-object v1, v1, Loae;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, v10, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->d:[Lnzx;

    aget-object v1, v1, v2

    sget-object v4, Lnzu;->a:Loxr;

    invoke-virtual {v1, v4}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzu;

    iget-object v4, v1, Lnzu;->b:Lnym;

    iget-object v4, v4, Lnym;->r:Loae;

    iget-object v3, v3, Loae;->d:Ljava/lang/String;

    iput-object v3, v4, Loae;->d:Ljava/lang/String;

    iget-object v3, v10, Lmmq;->c:Lmoo;

    iget-object v3, v3, Lmoo;->d:[Lnzx;

    aget-object v3, v3, v2

    sget-object v4, Lnzu;->a:Loxr;

    invoke-virtual {v3, v4, v1}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    iget-object v1, v11, Lmmq;->c:Lmoo;

    iget-object v1, v1, Lmoo;->d:[Lnzx;

    aget-object v1, v1, v2

    sget-object v3, Lnzu;->a:Loxr;

    iget-object v4, v11, Lmmq;->c:Lmoo;

    iget-object v4, v4, Lmoo;->d:[Lnzx;

    aget-object v4, v4, v2

    sget-object v5, Lnzu;->a:Loxr;

    invoke-virtual {v4, v5}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    move v2, v1

    :goto_2
    iget-object v1, v11, Lmmq;->e:[Lmma;

    array-length v1, v1

    if-ge v2, v1, :cond_b

    iget-object v1, v10, Lmmq;->e:[Lmma;

    array-length v1, v1

    if-ge v2, v1, :cond_b

    iget-object v1, v11, Lmmq;->e:[Lmma;

    aget-object v12, v1, v2

    iget-object v1, v10, Lmmq;->e:[Lmma;

    aget-object v13, v1, v2

    const/4 v1, 0x0

    move v3, v1

    :goto_3
    iget-object v1, v12, Lmma;->b:[Lmmp;

    array-length v1, v1

    if-ge v3, v1, :cond_a

    iget-object v1, v13, Lmma;->b:[Lmmp;

    array-length v1, v1

    if-ge v3, v1, :cond_a

    iget-object v1, v12, Lmma;->b:[Lmmp;

    aget-object v14, v1, v3

    iget-object v1, v13, Lmma;->b:[Lmmp;

    aget-object v15, v1, v3

    const/4 v1, 0x0

    move v4, v1

    :goto_4
    iget-object v1, v14, Lmmp;->b:[Lmml;

    array-length v1, v1

    if-ge v4, v1, :cond_9

    iget-object v1, v15, Lmmp;->b:[Lmml;

    array-length v1, v1

    if-ge v4, v1, :cond_9

    iget-object v1, v14, Lmmp;->b:[Lmml;

    aget-object v16, v1, v4

    iget-object v1, v15, Lmmp;->b:[Lmml;

    aget-object v17, v1, v4

    const/4 v5, 0x0

    const/4 v1, 0x0

    if-eqz v16, :cond_11

    move-object/from16 v0, v16

    iget v6, v0, Lmml;->c:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_11

    move-object/from16 v0, v16

    iget-object v1, v0, Lmml;->d:Lnzx;

    sget-object v6, Lnzu;->a:Loxr;

    invoke-virtual {v1, v6}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzu;

    iget-object v6, v1, Lnzu;->b:Lnym;

    iget-object v6, v6, Lnym;->r:Loae;

    if-eqz v6, :cond_10

    iget-object v5, v1, Lnzu;->b:Lnym;

    iget-object v5, v5, Lnym;->r:Loae;

    move-object v8, v1

    move-object v9, v5

    :goto_5
    const/4 v5, 0x0

    const/4 v1, 0x0

    if-eqz v17, :cond_f

    move-object/from16 v0, v17

    iget v6, v0, Lmml;->c:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_f

    move-object/from16 v0, v17

    iget-object v1, v0, Lmml;->d:Lnzx;

    sget-object v6, Lnzu;->a:Loxr;

    invoke-virtual {v1, v6}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzu;

    iget-object v6, v1, Lnzu;->b:Lnym;

    iget-object v6, v6, Lnym;->r:Loae;

    if-eqz v6, :cond_e

    iget-object v5, v1, Lnzu;->b:Lnym;

    iget-object v5, v5, Lnym;->r:Loae;

    move-object v6, v1

    move-object v7, v5

    :goto_6
    const/4 v1, 0x0

    if-eqz v6, :cond_d

    iget-object v5, v6, Lnzu;->b:Lnym;

    if-eqz v5, :cond_d

    iget-object v5, v6, Lnzu;->b:Lnym;

    iget-object v5, v5, Lnym;->m:Lnzb;

    if-eqz v5, :cond_d

    iget-object v5, v6, Lnzu;->b:Lnym;

    iget-object v5, v5, Lnym;->m:Lnzb;

    iget-object v5, v5, Lnzb;->a:Ljava/lang/String;

    if-eqz v5, :cond_d

    if-eqz v8, :cond_d

    iget-object v5, v8, Lnzu;->b:Lnym;

    if-eqz v5, :cond_d

    iget-object v5, v8, Lnzu;->b:Lnym;

    iget-object v5, v5, Lnym;->m:Lnzb;

    if-eqz v5, :cond_d

    iget-object v5, v8, Lnzu;->b:Lnym;

    iget-object v5, v5, Lnym;->m:Lnzb;

    iget-object v5, v5, Lnzb;->a:Ljava/lang/String;

    if-eqz v5, :cond_d

    iget-object v5, v6, Lnzu;->b:Lnym;

    iget-object v5, v5, Lnym;->m:Lnzb;

    iget-object v5, v5, Lnzb;->a:Ljava/lang/String;

    iget-object v0, v8, Lnzu;->b:Lnym;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lnym;->m:Lnzb;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lnzb;->a:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    iget-object v1, v6, Lnzu;->b:Lnym;

    iget-object v5, v8, Lnzu;->b:Lnym;

    iget-object v5, v5, Lnym;->m:Lnzb;

    iput-object v5, v1, Lnym;->m:Lnzb;

    const/4 v1, 0x1

    move v5, v1

    :goto_7
    const/4 v1, 0x0

    if-eqz v9, :cond_6

    iget-object v0, v9, Loae;->d:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    if-eqz v7, :cond_6

    iget-object v0, v7, Loae;->d:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    iget-object v1, v9, Loae;->d:Ljava/lang/String;

    iput-object v1, v7, Loae;->d:Ljava/lang/String;

    const/4 v1, 0x1

    :cond_6
    if-nez v5, :cond_7

    if-eqz v1, :cond_8

    :cond_7
    move-object/from16 v0, v16

    iget-object v1, v0, Lmml;->d:Lnzx;

    sget-object v5, Lnzu;->a:Loxr;

    invoke-virtual {v1, v5, v8}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    move-object/from16 v0, v17

    iget-object v1, v0, Lmml;->d:Lnzx;

    sget-object v5, Lnzu;->a:Loxr;

    invoke-virtual {v1, v5, v6}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    :cond_8
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_4

    :cond_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_3

    :cond_a
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_2

    .line 42
    :cond_b
    move-object/from16 v0, p1

    instance-of v1, v0, Lfob;

    if-eqz v1, :cond_c

    move-object/from16 v1, p1

    check-cast v1, Lfob;

    iget v1, v1, Lfob;->e:I

    move-object/from16 v0, p0

    iget v2, v0, Lfob;->e:I

    if-ne v1, v2, :cond_c

    move-object/from16 v1, p1

    check-cast v1, Lfob;

    iget-object v1, v1, Lfob;->b:Lfod;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfob;->b:Lfod;

    .line 44
    invoke-virtual {v1, v2}, Lfod;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    check-cast p1, Lfob;

    move-object/from16 v0, p1

    iget-object v1, v0, Lfob;->a:Lmmq;

    move-object/from16 v0, p0

    iget-object v2, v0, Lfob;->a:Lmmq;

    .line 45
    invoke-static {v1, v2}, Loxu;->a(Loxu;Loxu;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x1

    :goto_8
    return v1

    :cond_c
    const/4 v1, 0x0

    goto :goto_8

    :cond_d
    move v5, v1

    goto :goto_7

    :cond_e
    move-object v6, v1

    move-object v7, v5

    goto/16 :goto_6

    :cond_f
    move-object v6, v1

    move-object v7, v5

    goto/16 :goto_6

    :cond_10
    move-object v8, v1

    move-object v9, v5

    goto/16 :goto_5

    :cond_11
    move-object v8, v1

    move-object v9, v5

    goto/16 :goto_5

    :cond_12
    move-object v1, v4

    goto/16 :goto_1
.end method
