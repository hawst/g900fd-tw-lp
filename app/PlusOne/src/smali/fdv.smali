.class public final Lfdv;
.super Lfbx;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lfbx;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 26
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lfdv;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f04018e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTileView;

    .line 32
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Z)V

    .line 33
    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 38
    check-cast p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    .line 39
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 41
    new-instance v4, Ldwu;

    invoke-direct {v4, v0}, Ldwu;-><init>(Ljava/lang/String;)V

    .line 42
    const/4 v3, 0x0

    const/4 v0, 0x4

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/16 v8, 0x20

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const/4 v5, 0x2

    invoke-interface {p3, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lfdv;->c:Landroid/content/Context;

    iget-object v6, p0, Lfdv;->c:Landroid/content/Context;

    invoke-static {v6, v3}, Ldwq;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v0, :cond_1

    sget-object v0, Ljac;->b:Ljac;

    :goto_1
    invoke-static {v5, v3, v0}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    .line 44
    :goto_2
    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/PhotoTileView;->n(Z)V

    .line 45
    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Z)V

    .line 46
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    .line 47
    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljcl;)V

    .line 48
    iget-object v0, p0, Lfdv;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget-object v0, p0, Lfdv;->i:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 50
    return-void

    :cond_0
    move v0, v2

    .line 42
    goto :goto_0

    :cond_1
    sget-object v0, Ljac;->a:Ljac;

    goto :goto_1

    :cond_2
    invoke-interface {p3, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v3, p0, Lfdv;->c:Landroid/content/Context;

    invoke-interface {p3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v0, :cond_3

    sget-object v0, Ljac;->b:Ljac;

    :goto_3
    invoke-static {v3, v5, v0}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    goto :goto_2

    :cond_3
    sget-object v0, Ljac;->a:Ljac;

    goto :goto_3

    :cond_4
    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Trash entry must have remote url or local path"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-object v0, v3

    goto :goto_2
.end method

.method public c()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfdv;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 78
    iget-object v1, p0, Lfdv;->b:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfdv;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-object v0

    .line 82
    :cond_1
    iget-object v1, p0, Lfdv;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 84
    iget-object v2, p0, Lfdv;->b:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 86
    :cond_2
    iget-object v2, p0, Lfdv;->b:Landroid/database/Cursor;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v2, p0, Lfdv;->b:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 90
    :cond_3
    iget-object v2, p0, Lfdv;->b:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0
.end method
