.class public final Lkno;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmhi;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[Lknp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lkno;->a:Lmhi;

    .line 95
    sget-object v0, Lknp;->a:[Lknp;

    iput-object v0, p0, Lkno;->e:[Lknp;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 125
    .line 126
    iget-object v0, p0, Lkno;->a:Lmhi;

    if-eqz v0, :cond_5

    .line 127
    const/4 v0, 0x1

    iget-object v2, p0, Lkno;->a:Lmhi;

    .line 128
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 130
    :goto_0
    iget-object v2, p0, Lkno;->b:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 131
    const/4 v2, 0x2

    iget-object v3, p0, Lkno;->b:Ljava/lang/Long;

    .line 132
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 134
    :cond_0
    iget-object v2, p0, Lkno;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 135
    const/4 v2, 0x3

    iget-object v3, p0, Lkno;->c:Ljava/lang/String;

    .line 136
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 138
    :cond_1
    iget-object v2, p0, Lkno;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 139
    const/4 v2, 0x4

    iget-object v3, p0, Lkno;->d:Ljava/lang/String;

    .line 140
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 142
    :cond_2
    iget-object v2, p0, Lkno;->e:[Lknp;

    if-eqz v2, :cond_4

    .line 143
    iget-object v2, p0, Lkno;->e:[Lknp;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 144
    if-eqz v4, :cond_3

    .line 145
    const/4 v5, 0x5

    .line 146
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 143
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 150
    :cond_4
    iget-object v1, p0, Lkno;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    iput v0, p0, Lkno;->ai:I

    .line 152
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lkno;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 160
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 161
    sparse-switch v0, :sswitch_data_0

    .line 165
    iget-object v2, p0, Lkno;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 166
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lkno;->ah:Ljava/util/List;

    .line 169
    :cond_1
    iget-object v2, p0, Lkno;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    :sswitch_0
    return-object p0

    .line 176
    :sswitch_1
    iget-object v0, p0, Lkno;->a:Lmhi;

    if-nez v0, :cond_2

    .line 177
    new-instance v0, Lmhi;

    invoke-direct {v0}, Lmhi;-><init>()V

    iput-object v0, p0, Lkno;->a:Lmhi;

    .line 179
    :cond_2
    iget-object v0, p0, Lkno;->a:Lmhi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 183
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lkno;->b:Ljava/lang/Long;

    goto :goto_0

    .line 187
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkno;->c:Ljava/lang/String;

    goto :goto_0

    .line 191
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkno;->d:Ljava/lang/String;

    goto :goto_0

    .line 195
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 196
    iget-object v0, p0, Lkno;->e:[Lknp;

    if-nez v0, :cond_4

    move v0, v1

    .line 197
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lknp;

    .line 198
    iget-object v3, p0, Lkno;->e:[Lknp;

    if-eqz v3, :cond_3

    .line 199
    iget-object v3, p0, Lkno;->e:[Lknp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 201
    :cond_3
    iput-object v2, p0, Lkno;->e:[Lknp;

    .line 202
    :goto_2
    iget-object v2, p0, Lkno;->e:[Lknp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 203
    iget-object v2, p0, Lkno;->e:[Lknp;

    new-instance v3, Lknp;

    invoke-direct {v3}, Lknp;-><init>()V

    aput-object v3, v2, v0

    .line 204
    iget-object v2, p0, Lkno;->e:[Lknp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 205
    invoke-virtual {p1}, Loxn;->a()I

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 196
    :cond_4
    iget-object v0, p0, Lkno;->e:[Lknp;

    array-length v0, v0

    goto :goto_1

    .line 208
    :cond_5
    iget-object v2, p0, Lkno;->e:[Lknp;

    new-instance v3, Lknp;

    invoke-direct {v3}, Lknp;-><init>()V

    aput-object v3, v2, v0

    .line 209
    iget-object v2, p0, Lkno;->e:[Lknp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 161
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 100
    iget-object v0, p0, Lkno;->a:Lmhi;

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x1

    iget-object v1, p0, Lkno;->a:Lmhi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lkno;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 104
    const/4 v0, 0x2

    iget-object v1, p0, Lkno;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 106
    :cond_1
    iget-object v0, p0, Lkno;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 107
    const/4 v0, 0x3

    iget-object v1, p0, Lkno;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 109
    :cond_2
    iget-object v0, p0, Lkno;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 110
    const/4 v0, 0x4

    iget-object v1, p0, Lkno;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 112
    :cond_3
    iget-object v0, p0, Lkno;->e:[Lknp;

    if-eqz v0, :cond_5

    .line 113
    iget-object v1, p0, Lkno;->e:[Lknp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 114
    if-eqz v3, :cond_4

    .line 115
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 113
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_5
    iget-object v0, p0, Lkno;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 121
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lkno;->a(Loxn;)Lkno;

    move-result-object v0

    return-object v0
.end method
