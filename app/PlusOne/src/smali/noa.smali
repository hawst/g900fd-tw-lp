.class public final Lnoa;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Lpee;

.field public c:Lnob;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Loxq;-><init>()V

    .line 79
    iput-object v0, p0, Lnoa;->b:Lpee;

    .line 82
    iput-object v0, p0, Lnoa;->c:Lnob;

    .line 74
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 108
    iget-object v1, p0, Lnoa;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 109
    const/4 v0, 0x1

    iget-object v1, p0, Lnoa;->a:Ljava/lang/Boolean;

    .line 110
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 112
    :cond_0
    iget-object v1, p0, Lnoa;->b:Lpee;

    if-eqz v1, :cond_1

    .line 113
    const/4 v1, 0x2

    iget-object v2, p0, Lnoa;->b:Lpee;

    .line 114
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    :cond_1
    iget-object v1, p0, Lnoa;->c:Lnob;

    if-eqz v1, :cond_2

    .line 117
    const/4 v1, 0x3

    iget-object v2, p0, Lnoa;->c:Lnob;

    .line 118
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_2
    iget-object v1, p0, Lnoa;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 121
    const/4 v1, 0x4

    iget-object v2, p0, Lnoa;->d:Ljava/lang/Boolean;

    .line 122
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 124
    :cond_3
    iget-object v1, p0, Lnoa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    iput v0, p0, Lnoa;->ai:I

    .line 126
    return v0
.end method

.method public a(Loxn;)Lnoa;
    .locals 2

    .prologue
    .line 134
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 135
    sparse-switch v0, :sswitch_data_0

    .line 139
    iget-object v1, p0, Lnoa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnoa;->ah:Ljava/util/List;

    .line 143
    :cond_1
    iget-object v1, p0, Lnoa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    :sswitch_0
    return-object p0

    .line 150
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoa;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 154
    :sswitch_2
    iget-object v0, p0, Lnoa;->b:Lpee;

    if-nez v0, :cond_2

    .line 155
    new-instance v0, Lpee;

    invoke-direct {v0}, Lpee;-><init>()V

    iput-object v0, p0, Lnoa;->b:Lpee;

    .line 157
    :cond_2
    iget-object v0, p0, Lnoa;->b:Lpee;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 161
    :sswitch_3
    iget-object v0, p0, Lnoa;->c:Lnob;

    if-nez v0, :cond_3

    .line 162
    new-instance v0, Lnob;

    invoke-direct {v0}, Lnob;-><init>()V

    iput-object v0, p0, Lnoa;->c:Lnob;

    .line 164
    :cond_3
    iget-object v0, p0, Lnoa;->c:Lnob;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 168
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoa;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 135
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lnoa;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x1

    iget-object v1, p0, Lnoa;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 92
    :cond_0
    iget-object v0, p0, Lnoa;->b:Lpee;

    if-eqz v0, :cond_1

    .line 93
    const/4 v0, 0x2

    iget-object v1, p0, Lnoa;->b:Lpee;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 95
    :cond_1
    iget-object v0, p0, Lnoa;->c:Lnob;

    if-eqz v0, :cond_2

    .line 96
    const/4 v0, 0x3

    iget-object v1, p0, Lnoa;->c:Lnob;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 98
    :cond_2
    iget-object v0, p0, Lnoa;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 99
    const/4 v0, 0x4

    iget-object v1, p0, Lnoa;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 101
    :cond_3
    iget-object v0, p0, Lnoa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 103
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lnoa;->a(Loxn;)Lnoa;

    move-result-object v0

    return-object v0
.end method
