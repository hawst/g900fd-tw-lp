.class public final Leum;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 87
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 88
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 90
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    const-string v1, "notifications_ringtone"

    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    .line 95
    :goto_0
    invoke-interface {v0}, Lhek;->c()I

    .line 96
    return-void

    .line 93
    :cond_0
    const-string v1, "notifications_ringtone"

    invoke-interface {v0, v1}, Lhek;->e(Ljava/lang/String;)Lhek;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 43
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 45
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 46
    const-string v1, "push_notifications"

    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    .line 47
    invoke-interface {v0}, Lhek;->c()I

    .line 48
    return-void
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    .line 55
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 56
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 57
    const v2, 0x7f0e0012

    .line 58
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 59
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 60
    const-string v2, "push_notifications"

    invoke-interface {v0, v2, v1}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    .line 66
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 67
    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    .line 68
    const-string v1, "notifications_vibrate"

    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    .line 69
    invoke-interface {v0}, Lhek;->c()I

    .line 70
    return-void
.end method

.method public static b(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    .line 77
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 78
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 80
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 81
    const v2, 0x7f0e0013

    .line 82
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 83
    const-string v2, "notifications_vibrate"

    invoke-interface {v0, v2, v1}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 103
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 104
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 106
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 107
    const v2, 0x7f0a05b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    const-string v2, "notifications_ringtone"

    .line 110
    invoke-interface {v0, v2, v1}, Lhej;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    .line 118
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 119
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 120
    const-string v1, "notifications_ringtone"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Lhem;)V
    .locals 3

    .prologue
    .line 33
    const-string v0, "notifications_ringtone"

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    .line 35
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lheq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Leun;

    invoke-direct {v0}, Leun;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method
