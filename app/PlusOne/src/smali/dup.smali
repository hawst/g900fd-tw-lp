.class public final Ldup;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lduo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ldup;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljcn;

.field private b:[Ldwj;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 193
    new-instance v0, Lduq;

    invoke-direct {v0}, Lduq;-><init>()V

    sput-object v0, Ldup;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-class v0, Ljcn;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljcn;

    iput-object v0, p0, Ldup;->a:Ljcn;

    .line 44
    sget-object v0, Ldwj;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldwj;

    iput-object v0, p0, Ldup;->b:[Ldwj;

    .line 45
    return-void
.end method

.method public constructor <init>(Ljcn;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Ldup;-><init>(Ljcn;Z)V

    .line 35
    return-void
.end method

.method public constructor <init>(Ljcn;Z)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Ldup;->a:Ljcn;

    .line 39
    iput-boolean p2, p0, Ldup;->c:Z

    .line 40
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ldwj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    const/4 v0, 0x0

    .line 177
    if-eqz p2, :cond_1

    .line 178
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 181
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 182
    new-instance v3, Ldwj;

    .line 183
    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v4

    invoke-direct {v3, p3, p1, v4, v5}, Ldwj;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 182
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 187
    :cond_1
    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldup;->a:Ljcn;

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;I)[Ldwj;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 84
    iget-object v0, p0, Ldup;->b:[Ldwj;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Ldup;->b:[Ldwj;

    .line 103
    :goto_0
    return-object v0

    .line 87
    :cond_0
    iget-object v0, p0, Ldup;->a:Ljcn;

    const-class v1, Ljcl;

    invoke-virtual {v0, v1}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v3

    .line 88
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 90
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_a

    .line 91
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcl;

    .line 93
    instance-of v1, v0, Ljuc;

    if-eqz v1, :cond_5

    .line 94
    check-cast v0, Ljuc;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljuc;->f()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {p1}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v5

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lhrx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ldwj;

    invoke-direct {v6, v5, v0}, Ldwj;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 90
    :cond_2
    :goto_3
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 94
    :cond_3
    invoke-virtual {v0}, Lizu;->k()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lizu;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, p2, v5}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Lizu;->c()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Lizu;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-direct {p0, v11, v5, v11}, Ldup;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 95
    :cond_5
    instance-of v1, v0, Ldqm;

    if-eqz v1, :cond_2

    .line 96
    check-cast v0, Ldqm;

    iget-boolean v5, p0, Ldup;->c:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ldqm;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    sget-object v7, Lcph;->a:Lcph;

    invoke-static {p1, p2, v6, v7}, Lcpe;->a(Landroid/content/Context;ILjava/lang/Long;Lcph;)Lcpg;

    move-result-object v6

    if-nez v6, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v10}, Ljava/util/ArrayList;-><init>(I)V

    :goto_4
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_6
    iget-object v7, v6, Lcpg;->e:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    invoke-static {p1}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v7

    iget-object v8, v6, Lcpg;->e:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lhrx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ldwj;

    iget-object v9, v6, Lcpg;->e:Ljava/lang/String;

    invoke-direct {v8, v7, v9}, Ldwj;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {v0}, Ldqm;->a()J

    move-result-wide v8

    invoke-static {p1, p2, v8, v9}, Ljvd;->b(Landroid/content/Context;IJ)Ljava/lang/String;

    move-result-object v7

    iget-object v6, v6, Lcpg;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ldqm;->a()J

    move-result-wide v8

    invoke-static {p1, p2, v8, v9}, Ljvd;->a(Landroid/content/Context;IJ)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v6, v0, v7}, Ldup;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    if-eqz v5, :cond_9

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_8
    :goto_5
    move-object v0, v1

    goto :goto_4

    :cond_9
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 101
    :cond_a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ldwj;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldwj;

    iput-object v0, p0, Ldup;->b:[Ldwj;

    .line 103
    iget-object v0, p0, Ldup;->b:[Ldwj;

    goto/16 :goto_0
.end method

.method public b()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Ldup;->a:Ljcn;

    const-class v1, Ljcl;

    invoke-virtual {v0, v1}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    .line 69
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 71
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcl;

    .line 72
    instance-of v3, v0, Ljuc;

    if-eqz v3, :cond_1

    .line 73
    check-cast v0, Ljuc;

    invoke-virtual {v0}, Ljuc;->f()Lizu;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :cond_1
    instance-of v3, v0, Ldqm;

    if-eqz v3, :cond_0

    .line 75
    check-cast v0, Ldqm;

    invoke-virtual {v0}, Ldqm;->f()Lizu;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    :cond_2
    return-object v1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ldup;->a:Ljcn;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 55
    iget-object v0, p0, Ldup;->b:[Ldwj;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 56
    return-void
.end method
