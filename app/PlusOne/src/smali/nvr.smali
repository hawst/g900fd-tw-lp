.class public final Lnvr;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Llth;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2820
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2823
    sget-object v0, Llth;->a:[Llth;

    iput-object v0, p0, Lnvr;->a:[Llth;

    .line 2820
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2841
    .line 2842
    iget-object v1, p0, Lnvr;->a:[Llth;

    if-eqz v1, :cond_1

    .line 2843
    iget-object v2, p0, Lnvr;->a:[Llth;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2844
    if-eqz v4, :cond_0

    .line 2845
    const/4 v5, 0x1

    .line 2846
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2843
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2850
    :cond_1
    iget-object v1, p0, Lnvr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2851
    iput v0, p0, Lnvr;->ai:I

    .line 2852
    return v0
.end method

.method public a(Loxn;)Lnvr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2860
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2861
    sparse-switch v0, :sswitch_data_0

    .line 2865
    iget-object v2, p0, Lnvr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2866
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnvr;->ah:Ljava/util/List;

    .line 2869
    :cond_1
    iget-object v2, p0, Lnvr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2871
    :sswitch_0
    return-object p0

    .line 2876
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2877
    iget-object v0, p0, Lnvr;->a:[Llth;

    if-nez v0, :cond_3

    move v0, v1

    .line 2878
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llth;

    .line 2879
    iget-object v3, p0, Lnvr;->a:[Llth;

    if-eqz v3, :cond_2

    .line 2880
    iget-object v3, p0, Lnvr;->a:[Llth;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2882
    :cond_2
    iput-object v2, p0, Lnvr;->a:[Llth;

    .line 2883
    :goto_2
    iget-object v2, p0, Lnvr;->a:[Llth;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2884
    iget-object v2, p0, Lnvr;->a:[Llth;

    new-instance v3, Llth;

    invoke-direct {v3}, Llth;-><init>()V

    aput-object v3, v2, v0

    .line 2885
    iget-object v2, p0, Lnvr;->a:[Llth;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2886
    invoke-virtual {p1}, Loxn;->a()I

    .line 2883
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2877
    :cond_3
    iget-object v0, p0, Lnvr;->a:[Llth;

    array-length v0, v0

    goto :goto_1

    .line 2889
    :cond_4
    iget-object v2, p0, Lnvr;->a:[Llth;

    new-instance v3, Llth;

    invoke-direct {v3}, Llth;-><init>()V

    aput-object v3, v2, v0

    .line 2890
    iget-object v2, p0, Lnvr;->a:[Llth;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2861
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2828
    iget-object v0, p0, Lnvr;->a:[Llth;

    if-eqz v0, :cond_1

    .line 2829
    iget-object v1, p0, Lnvr;->a:[Llth;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2830
    if-eqz v3, :cond_0

    .line 2831
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2829
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2835
    :cond_1
    iget-object v0, p0, Lnvr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2837
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2816
    invoke-virtual {p0, p1}, Lnvr;->a(Loxn;)Lnvr;

    move-result-object v0

    return-object v0
.end method
