.class public Lanu;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljdz;",
        "Laoa;",
        "Laob;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lboo;


# instance fields
.field private final c:Ljfb;

.field private final d:Lbig;

.field private final e:Lanw;

.field private final f:Lkfd;

.field private final g:Lcdu;

.field private final h:Lanz;

.field private final i:Lcgw;

.field private final j:Ljava/util/concurrent/ExecutorService;

.field private final k:Lart;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 64
    const-class v0, Lanu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lanu;->a:Ljava/lang/String;

    .line 181
    new-instance v1, Lboo;

    const/16 v5, 0x500

    const/16 v6, 0x2d0

    move v7, v4

    move-wide v8, v2

    invoke-direct/range {v1 .. v9}, Lboo;-><init>(JIIIZJ)V

    sput-object v1, Lanu;->b:Lboo;

    return-void
.end method

.method constructor <init>(Ljfb;Lbig;Lanw;Lkfd;Ljava/util/concurrent/ExecutorService;Lcdu;Lart;Lanz;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 228
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 229
    const-string v0, "plusDataProvider"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Lanu;->c:Ljfb;

    .line 230
    const-string v0, "mediaExtractorFactory"

    .line 231
    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbig;

    iput-object v0, p0, Lanu;->d:Lbig;

    .line 232
    const-string v0, "assetUriProvider"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanw;

    iput-object v0, p0, Lanu;->e:Lanw;

    .line 233
    const-string v0, "httpExecutor"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lanu;->f:Lkfd;

    .line 234
    const-string v0, "listener"

    invoke-static {p8, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanz;

    iput-object v0, p0, Lanu;->h:Lanz;

    .line 235
    const-string v0, "metadataLoadingExecutor"

    .line 236
    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lanu;->j:Ljava/util/concurrent/ExecutorService;

    .line 237
    const-string v0, "analyticsSession"

    invoke-static {p6, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lanu;->g:Lcdu;

    .line 238
    const-string v0, "predownloadStatusProvider"

    .line 239
    invoke-static {p7, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lart;

    iput-object v0, p0, Lanu;->k:Lart;

    .line 240
    new-instance v0, Lcgw;

    sget-object v1, Lanu;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcgw;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lanu;->i:Lcgw;

    .line 241
    return-void
.end method

.method public static a(Ljfb;Lbig;Lkfd;Ljava/util/concurrent/ExecutorService;Lcdu;)Lany;
    .locals 6

    .prologue
    .line 201
    new-instance v0, Lanv;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lanv;-><init>(Ljfb;Lbig;Lkfd;Ljava/util/concurrent/ExecutorService;Lcdu;)V

    return-object v0
.end method

.method private a(Landroid/net/Uri;)Lbmx;
    .locals 2

    .prologue
    .line 492
    iget-object v0, p0, Lanu;->d:Lbig;

    .line 493
    invoke-interface {v0, p1}, Lbig;->b(Landroid/net/Uri;)Lbij;

    move-result-object v1

    .line 495
    :try_start_0
    invoke-interface {v1}, Lbij;->i()Lbmu;

    move-result-object v0

    check-cast v0, Lbmx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 497
    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    throw v0
.end method

.method private a(Ljava/util/Map;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 473
    .line 474
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v3

    .line 475
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 476
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljej;

    .line 477
    iget-object v2, p0, Lanu;->c:Ljfb;

    iget-object v5, v1, Ljej;->a:Landroid/net/Uri;

    invoke-virtual {v2, v5}, Ljfb;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 479
    iget-object v5, v1, Ljej;->b:Ljel;

    sget-object v6, Ljel;->a:Ljel;

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lanu;->c:Ljfb;

    invoke-virtual {v5, v2}, Ljfb;->b(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 480
    iget-object v5, p0, Lanu;->c:Ljfb;

    invoke-virtual {v5, v2}, Ljfb;->c(Landroid/net/Uri;)Z

    move-result v5

    .line 481
    if-nez v5, :cond_0

    .line 482
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Couldn\'t download: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 484
    :cond_0
    iget-object v5, p0, Lanu;->c:Ljfb;

    invoke-virtual {v5, v2}, Ljfb;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 486
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    new-instance v5, Ljej;

    iget-object v1, v1, Ljej;->b:Ljel;

    invoke-direct {v5, v2, v1}, Ljej;-><init>(Landroid/net/Uri;Ljel;)V

    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 488
    :cond_2
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljfd;)Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljfd;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 355
    new-instance v3, Ljava/util/HashSet;

    iget-object v0, p1, Ljfd;->b:Lood;

    iget-object v0, v0, Lood;->b:[Lonr;

    array-length v0, v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 357
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 359
    iget-object v0, p1, Ljfd;->b:Lood;

    iget-object v1, v0, Lood;->b:[Lonr;

    array-length v5, v1

    move v0, v2

    :goto_0
    if-ge v0, v5, :cond_3

    aget-object v6, v1, v0

    .line 360
    iget v7, v6, Lonr;->b:I

    const/4 v8, 0x3

    if-eq v7, v8, :cond_0

    iget v7, v6, Lonr;->b:I

    if-ne v7, v12, :cond_2

    .line 361
    :cond_0
    iget-object v6, v6, Lonr;->c:Lonw;

    iget-object v6, v6, Lonw;->a:Lonz;

    invoke-static {v6}, Ljdz;->a(Lonz;)Ljdz;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 359
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 362
    :cond_2
    iget v7, v6, Lonr;->b:I

    const/4 v8, 0x7

    if-ne v7, v8, :cond_1

    .line 363
    iget-object v7, v6, Lonr;->c:Lonw;

    iget-object v7, v7, Lonw;->d:Lonx;

    iget-object v7, v7, Lonx;->a:Ljava/lang/Long;

    .line 364
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljdx;->a(J)Ljdx;

    move-result-object v7

    iget-object v8, p0, Lanu;->e:Lanw;

    iget-object v6, v6, Lonr;->c:Lonw;

    iget-object v6, v6, Lonw;->d:Lonx;

    iget-object v6, v6, Lonx;->a:Ljava/lang/Long;

    .line 365
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-interface {v8, v10, v11}, Lanw;->a(J)Ljej;

    move-result-object v6

    .line 363
    invoke-interface {v4, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 369
    :cond_3
    iget-object v0, p0, Lanu;->c:Ljfb;

    iget-object v1, p0, Lanu;->f:Lkfd;

    .line 370
    invoke-virtual {v0, v3, v1}, Ljfb;->a(Ljava/lang/Iterable;Lkfd;)Ljava/util/Map;

    move-result-object v5

    .line 371
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 373
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdz;

    invoke-static {v0}, Ljdx;->a(Ljdz;)Ljdx;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 372
    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 376
    :cond_4
    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 378
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdz;

    .line 379
    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 381
    :cond_5
    new-instance v0, Lcga;

    const-string v1, "Storyboard contained media which was not found: %s"

    new-array v4, v12, [Ljava/lang/Object;

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcga;-><init>(Ljava/lang/String;)V

    throw v0

    .line 384
    :cond_6
    return-object v4
.end method

.method private a([Lonr;Ljava/util/Map;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lonr;",
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Lbmu;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x1

    .line 395
    array-length v0, p1

    .line 396
    invoke-static {v0}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v2

    .line 398
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 401
    array-length v4, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_5

    aget-object v5, p1, v1

    .line 402
    iget v0, v5, Lonr;->b:I

    if-eq v0, v11, :cond_0

    iget v0, v5, Lonr;->b:I

    if-ne v0, v10, :cond_4

    .line 403
    :cond_0
    iget-object v0, v5, Lonr;->c:Lonw;

    invoke-static {v0}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v6

    .line 404
    invoke-interface {p2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    .line 405
    if-nez v0, :cond_1

    .line 406
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x10

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Missing URI for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 408
    :cond_1
    invoke-interface {v2, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 409
    iget-object v7, v5, Lonr;->c:Lonw;

    iget-object v8, v0, Ljej;->a:Landroid/net/Uri;

    iget v9, v5, Lonr;->b:I

    .line 410
    invoke-static {v7, v8, v9}, Ljeg;->a(Lonw;Landroid/net/Uri;I)Ljeg;

    move-result-object v7

    .line 411
    iget v8, v5, Lonr;->b:I

    if-ne v8, v11, :cond_3

    .line 415
    :try_start_0
    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lanu;->a(Landroid/net/Uri;)Lbmx;

    move-result-object v0

    .line 416
    invoke-interface {v2, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    :cond_2
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 417
    :catch_0
    move-exception v0

    .line 418
    sget-object v1, Lanu;->a:Ljava/lang/String;

    const-string v2, "Failed to load metadata for cloud photo."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 419
    throw v0

    .line 421
    :cond_3
    iget v0, v5, Lonr;->b:I

    if-ne v0, v10, :cond_2

    .line 423
    iget-object v0, p0, Lanu;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lanx;

    iget-object v8, p0, Lanu;->d:Lbig;

    invoke-direct {v5, v8, v7}, Lanx;-><init>(Lbig;Ljeg;)V

    invoke-interface {v0, v5}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 426
    invoke-static {v6, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 429
    :cond_4
    iget v0, v5, Lonr;->b:I

    const/4 v6, 0x7

    if-ne v0, v6, :cond_2

    .line 431
    iget-object v0, v5, Lonr;->c:Lonw;

    .line 432
    invoke-static {v0}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v0

    sget-object v5, Lanu;->b:Lboo;

    .line 431
    invoke-interface {v2, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 438
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 439
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljdx;

    .line 440
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/concurrent/Future;

    .line 442
    :try_start_1
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboo;

    .line 443
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    .line 444
    :catch_1
    move-exception v0

    .line 445
    sget-object v1, Lanu;->a:Ljava/lang/String;

    const-string v2, "Unable to load video metadata."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 446
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 447
    instance-of v2, v1, Ljava/io/IOException;

    if-eqz v2, :cond_6

    .line 448
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 449
    :cond_6
    instance-of v1, v1, Lbpr;

    if-eqz v1, :cond_7

    .line 450
    const-string v1, "Unexpected local media exception for cloud media."

    invoke-static {v1, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 453
    :cond_7
    const-string v1, "Unexpected exception"

    invoke-static {v1, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 456
    :catch_2
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_2

    .line 460
    :cond_8
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;[Lonr;Ljava/util/Map;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "[",
            "Lonr;",
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 505
    const/4 v3, 0x0

    .line 506
    move-object/from16 v0, p2

    array-length v5, v0

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, p2, v4

    .line 507
    iget v2, v2, Lonr;->b:I

    const/4 v6, 0x1

    if-ne v2, v6, :cond_8

    .line 508
    add-int/lit8 v2, v3, 0x1

    .line 506
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_0

    .line 512
    :cond_0
    const/4 v8, 0x0

    .line 513
    const-wide/16 v6, 0x0

    .line 514
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 515
    move-object/from16 v0, p2

    array-length v12, v0

    const/4 v2, 0x0

    move v9, v2

    :goto_2
    if-ge v9, v12, :cond_4

    aget-object v13, p2, v9

    .line 516
    iget v2, v13, Lonr;->b:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_7

    .line 517
    iget-object v2, v13, Lonr;->c:Lonw;

    invoke-static {v2}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v4

    .line 518
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljej;

    .line 519
    if-nez v2, :cond_1

    .line 520
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Missing URI for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v2

    throw v2

    .line 522
    :cond_1
    iget-object v4, v13, Lonr;->c:Lonw;

    iget-object v2, v2, Ljej;->a:Landroid/net/Uri;

    iget v5, v13, Lonr;->b:I

    .line 523
    invoke-static {v4, v2, v5}, Ljeg;->a(Lonw;Landroid/net/Uri;I)Ljeg;

    move-result-object v2

    .line 524
    const/4 v4, 0x0

    .line 526
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lanu;->d:Lbig;

    invoke-interface {v5, v2}, Lbig;->b(Ljeg;)Lbih;
    :try_end_0
    .catch Lbpr; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    .line 527
    :try_start_1
    invoke-interface {v5}, Lbih;->f()I

    move-result v14

    .line 528
    const/4 v2, -0x1

    .line 529
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v14, :cond_3

    .line 530
    invoke-interface {v5, v4}, Lbih;->a(I)Landroid/media/MediaFormat;

    move-result-object v15

    invoke-static {v15}, Lbkf;->b(Landroid/media/MediaFormat;)Z

    move-result v15

    if-eqz v15, :cond_2

    move v2, v4

    .line 529
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 534
    :cond_3
    const/4 v4, -0x1

    if-eq v2, v4, :cond_5

    .line 535
    invoke-interface {v5, v2}, Lbih;->b(I)V

    .line 539
    iget-object v2, v13, Lonr;->d:Lons;

    iget-object v2, v2, Lons;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-static {v5, v14, v15}, Lbke;->a(Lbih;J)V

    .line 540
    iget-object v2, v13, Lonr;->d:Lons;

    iget-object v2, v2, Lons;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-interface {v5, v14, v15}, Lbih;->a(J)V
    :try_end_1
    .catch Lbpr; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 545
    invoke-static {v5}, Lcgl;->a(Lcgk;)V

    .line 549
    iget-object v2, v13, Lonr;->d:Lons;

    iget-object v2, v2, Lons;->c:Ljava/lang/Long;

    .line 550
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v2, v13, Lonr;->d:Lons;

    iget-object v2, v2, Lons;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sub-long/2addr v4, v14

    add-long/2addr v4, v6

    .line 551
    const-wide/32 v6, 0x989680

    cmp-long v2, v4, v6

    if-ltz v2, :cond_6

    .line 553
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v10

    const-wide/16 v14, 0x3e8

    mul-long/2addr v6, v14

    .line 554
    long-to-float v2, v6

    long-to-float v6, v4

    div-float/2addr v2, v6

    const/high16 v6, 0x3f000000    # 0.5f

    cmpg-float v2, v2, v6

    if-gtz v2, :cond_6

    .line 556
    const/4 v2, 0x1

    new-array v2, v2, [Laoa;

    const/4 v3, 0x0

    new-instance v4, Laoa;

    const/16 v5, 0x64

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v5}, Laoa;-><init>(Landroid/graphics/Bitmap;I)V

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lanu;->publishProgress([Ljava/lang/Object;)V

    .line 566
    :cond_4
    return-void

    .line 537
    :cond_5
    :try_start_2
    new-instance v2, Ljava/io/IOException;

    const-string v3, "No video track found in available tracks"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Lbpr; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 541
    :catch_0
    move-exception v2

    move-object v3, v5

    .line 542
    :goto_4
    :try_start_3
    const-string v4, "Unexpected local media exception for cloud media."

    invoke-static {v4, v2}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v2

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 545
    :catchall_0
    move-exception v2

    move-object v5, v3

    :goto_5
    invoke-static {v5}, Lcgl;->a(Lcgk;)V

    throw v2

    .line 561
    :cond_6
    add-int/lit8 v2, v8, 0x1

    .line 562
    const/4 v6, 0x1

    new-array v6, v6, [Laoa;

    const/4 v7, 0x0

    new-instance v8, Laoa;

    mul-int/lit8 v13, v2, 0x3c

    div-int/2addr v13, v3

    add-int/lit8 v13, v13, 0x28

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v13}, Laoa;-><init>(Landroid/graphics/Bitmap;I)V

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lanu;->publishProgress([Ljava/lang/Object;)V

    .line 515
    :goto_6
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    move v8, v2

    move-wide v6, v4

    goto/16 :goto_2

    .line 545
    :catchall_1
    move-exception v2

    move-object v5, v4

    goto :goto_5

    :catchall_2
    move-exception v2

    goto :goto_5

    .line 541
    :catch_1
    move-exception v2

    move-object v3, v4

    goto :goto_4

    :cond_7
    move-wide v4, v6

    move v2, v8

    goto :goto_6

    :cond_8
    move v2, v3

    goto/16 :goto_1
.end method


# virtual methods
.method protected varargs a([Ljdz;)Laob;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 245
    const-string v0, "paramsArray"

    invoke-static {p1, v0, v9}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 246
    array-length v0, p1

    const-string v1, "paramsArray.length"

    invoke-static {v0, v1, v8, v9}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 247
    aget-object v0, p1, v2

    const-string v1, "params"

    invoke-static {v0, v1, v9}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdz;

    .line 248
    iget-object v1, v0, Ljdz;->b:Ljava/lang/String;

    const-string v3, "userId"

    invoke-static {v1, v3, v9}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 249
    iget-object v0, v0, Ljdz;->a:Ljava/lang/Long;

    const-string v3, "photoId"

    invoke-static {v0, v3, v9}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 250
    iget-object v0, p0, Lanu;->i:Lcgw;

    const-string v3, "doInBackground()"

    invoke-virtual {v0, v3}, Lcgw;->a(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lanu;->c:Ljfb;

    .line 256
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljfb;->a(Ljava/lang/String;Ljava/lang/Long;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 257
    new-array v0, v8, [Laoa;

    new-instance v6, Laoa;

    const/16 v7, 0xa

    invoke-direct {v6, v3, v7}, Laoa;-><init>(Landroid/graphics/Bitmap;I)V

    aput-object v6, v0, v2

    invoke-virtual {p0, v0}, Lanu;->publishProgress([Ljava/lang/Object;)V

    .line 258
    iget-object v0, p0, Lanu;->i:Lcgw;

    const-string v6, "get poster"

    invoke-virtual {v0, v6}, Lcgw;->b(Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lanu;->c:Ljfb;

    .line 261
    invoke-virtual {v0, v1, v4, v5}, Ljfb;->a(Ljava/lang/String;J)Ljfd;

    move-result-object v1

    .line 262
    const-string v0, "storyboardResult"

    invoke-static {v1, v0, v9}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 263
    new-array v0, v8, [Laoa;

    new-instance v4, Laoa;

    const/16 v5, 0x14

    invoke-direct {v4, v3, v5}, Laoa;-><init>(Landroid/graphics/Bitmap;I)V

    aput-object v4, v0, v2

    invoke-virtual {p0, v0}, Lanu;->publishProgress([Ljava/lang/Object;)V

    .line 264
    iget-object v0, p0, Lanu;->i:Lcgw;

    const-string v4, "load storyboard"

    invoke-virtual {v0, v4}, Lcgw;->b(Ljava/lang/String;)V

    .line 267
    iget-object v0, v1, Ljfd;->a:Ljff;

    sget-object v4, Ljff;->a:Ljff;

    if-eq v0, v4, :cond_0

    .line 268
    sget-object v0, Lanu;->a:Ljava/lang/String;

    iget-object v2, v1, Ljfd;->a:Ljff;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x31

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Error status code when loading cloud storyboard: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    iget-object v0, p0, Lanu;->i:Lcgw;

    invoke-virtual {v0, v8}, Lcgw;->a(Z)V

    .line 271
    invoke-static {v1, v9}, Laob;->a(Ljfd;Lche;)Laob;

    move-result-object v0

    .line 336
    :goto_0
    return-object v0

    .line 275
    :cond_0
    :try_start_0
    iget-object v0, v1, Ljfd;->b:Lood;

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catch Lche; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {v5, v0, v4}, Laya;->a(Ljava/lang/StringBuilder;Lood;Z)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lche; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Lche;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lche;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lche; {:try_start_2 .. :try_end_2} :catch_0

    .line 276
    :catch_0
    move-exception v0

    .line 277
    sget-object v2, Lanu;->a:Ljava/lang/String;

    const-string v3, "Invalid storyboard from server."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 278
    iget-object v2, p0, Lanu;->i:Lcgw;

    invoke-virtual {v2, v8}, Lcgw;->a(Z)V

    .line 279
    iget-object v2, p0, Lanu;->g:Lcdu;

    new-instance v3, Lcdx;

    invoke-direct {v3, v0}, Lcdx;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v2, v3}, Lcdu;->c(Lcdx;)V

    .line 280
    invoke-static {v1, v0}, Laob;->a(Ljfd;Lche;)Laob;

    move-result-object v0

    goto :goto_0

    .line 275
    :catch_1
    move-exception v0

    :try_start_3
    const-string v2, "Parsing storyboard caused RuntimeException. Errors before crash: <%s>"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lche;

    invoke-direct {v3, v2, v0}, Lche;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catch Lche; {:try_start_3 .. :try_end_3} :catch_0

    .line 282
    :cond_1
    iget-object v0, p0, Lanu;->i:Lcgw;

    const-string v4, "validate storyboard"

    invoke-virtual {v0, v4}, Lcgw;->b(Ljava/lang/String;)V

    .line 286
    :try_start_4
    invoke-direct {p0, v1}, Lanu;->a(Ljfd;)Ljava/util/Map;
    :try_end_4
    .catch Lche; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    .line 292
    iget-object v4, p0, Lanu;->i:Lcgw;

    const-string v5, "get media URIs"

    invoke-virtual {v4, v5}, Lcgw;->b(Ljava/lang/String;)V

    .line 296
    :try_start_5
    invoke-direct {p0, v0}, Lanu;->a(Ljava/util/Map;)Ljava/util/Map;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    move-result-object v4

    .line 301
    new-array v0, v8, [Laoa;

    new-instance v5, Laoa;

    const/16 v6, 0x1e

    invoke-direct {v5, v3, v6}, Laoa;-><init>(Landroid/graphics/Bitmap;I)V

    aput-object v5, v0, v2

    invoke-virtual {p0, v0}, Lanu;->publishProgress([Ljava/lang/Object;)V

    .line 302
    iget-object v0, p0, Lanu;->i:Lcgw;

    const-string v5, "download photos"

    invoke-virtual {v0, v5}, Lcgw;->b(Ljava/lang/String;)V

    .line 305
    :try_start_6
    iget-object v0, v1, Ljfd;->b:Lood;

    iget-object v0, v0, Lood;->b:[Lonr;

    .line 308
    invoke-direct {p0, v0, v4}, Lanu;->a([Lonr;Ljava/util/Map;)Ljava/util/Map;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    move-result-object v5

    .line 313
    new-array v0, v8, [Laoa;

    new-instance v6, Laoa;

    const/16 v7, 0x28

    invoke-direct {v6, v3, v7}, Laoa;-><init>(Landroid/graphics/Bitmap;I)V

    aput-object v6, v0, v2

    invoke-virtual {p0, v0}, Lanu;->publishProgress([Ljava/lang/Object;)V

    .line 314
    iget-object v0, p0, Lanu;->i:Lcgw;

    const-string v6, "get input metadata"

    invoke-virtual {v0, v6}, Lcgw;->b(Ljava/lang/String;)V

    .line 319
    :try_start_7
    iget-object v0, p0, Lanu;->k:Lart;

    invoke-interface {v0}, Lart;->a()Z
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_5

    move-result v0

    .line 324
    :goto_1
    if-eqz v0, :cond_2

    .line 326
    :try_start_8
    iget-object v0, v1, Ljfd;->b:Lood;

    iget-object v0, v0, Lood;->b:[Lonr;

    invoke-direct {p0, v3, v0, v4}, Lanu;->a(Landroid/graphics/Bitmap;[Lonr;Ljava/util/Map;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 332
    iget-object v0, p0, Lanu;->i:Lcgw;

    const-string v2, "pre-download videos"

    invoke-virtual {v0, v2}, Lcgw;->b(Ljava/lang/String;)V

    .line 334
    :cond_2
    iget-object v0, p0, Lanu;->i:Lcgw;

    invoke-virtual {v0, v8}, Lcgw;->a(Z)V

    .line 336
    new-instance v0, Laob;

    invoke-direct {v0, v1, v4, v5, v9}, Laob;-><init>(Ljfd;Ljava/util/Map;Ljava/util/Map;Lche;)V

    goto/16 :goto_0

    .line 287
    :catch_2
    move-exception v0

    .line 288
    sget-object v2, Lanu;->a:Ljava/lang/String;

    const-string v3, "Failed to get media uris"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 289
    iget-object v2, p0, Lanu;->i:Lcgw;

    invoke-virtual {v2, v8}, Lcgw;->a(Z)V

    .line 290
    invoke-static {v1, v0}, Laob;->a(Ljfd;Lche;)Laob;

    move-result-object v0

    goto/16 :goto_0

    .line 297
    :catch_3
    move-exception v0

    .line 298
    iget-object v2, p0, Lanu;->i:Lcgw;

    invoke-virtual {v2, v8}, Lcgw;->a(Z)V

    .line 299
    new-instance v2, Lche;

    invoke-direct {v2, v0}, Lche;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Laob;->a(Ljfd;Lche;)Laob;

    move-result-object v0

    goto/16 :goto_0

    .line 309
    :catch_4
    move-exception v0

    .line 310
    iget-object v2, p0, Lanu;->i:Lcgw;

    invoke-virtual {v2, v8}, Lcgw;->a(Z)V

    .line 311
    new-instance v2, Lche;

    invoke-direct {v2, v0}, Lche;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Laob;->a(Ljfd;Lche;)Laob;

    move-result-object v0

    goto/16 :goto_0

    .line 321
    :catch_5
    move-exception v0

    .line 322
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    move v0, v2

    goto :goto_1

    .line 328
    :catch_6
    move-exception v0

    .line 329
    iget-object v2, p0, Lanu;->i:Lcgw;

    invoke-virtual {v2, v8}, Lcgw;->a(Z)V

    .line 330
    new-instance v2, Lche;

    invoke-direct {v2, v0}, Lche;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Laob;->a(Ljfd;Lche;)Laob;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected a(Laob;)V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lanu;->h:Lanz;

    invoke-interface {v0, p0, p1}, Lanz;->a(Lanu;Laob;)V

    .line 349
    return-void
.end method

.method protected varargs a([Laoa;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 341
    const-string v0, "progressArray"

    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 342
    array-length v0, p1

    const-string v1, "progressArray.length"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 343
    iget-object v1, p0, Lanu;->h:Lanz;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    const-string v2, "progressArray[0]"

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoa;

    invoke-interface {v1, v0}, Lanz;->a(Laoa;)V

    .line 344
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    check-cast p1, [Ljdz;

    invoke-virtual {p0, p1}, Lanu;->a([Ljdz;)Laob;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 61
    check-cast p1, Laob;

    invoke-virtual {p0, p1}, Lanu;->a(Laob;)V

    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 61
    check-cast p1, [Laoa;

    invoke-virtual {p0, p1}, Lanu;->a([Laoa;)V

    return-void
.end method
