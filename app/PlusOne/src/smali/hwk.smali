.class final Lhwk;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private synthetic a:Lhwi;


# direct methods
.method constructor <init>(Lhwi;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lhwk;->a:Lhwi;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lhwk;->a:Lhwi;

    invoke-static {v0}, Lhwi;->a(Lhwi;)I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 183
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 188
    iget-object v0, p0, Lhwk;->a:Lhwi;

    iget-object v0, v0, Lhwi;->a:Lhwn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwk;->a:Lhwi;

    iget-object v0, v0, Lhwi;->a:Lhwn;

    invoke-virtual {v0}, Lhwn;->b()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhwk;->a:Lhwi;

    invoke-static {v0}, Lhwi;->a(Lhwi;)I

    move-result v0

    if-gt v0, p1, :cond_1

    :cond_0
    move-object p2, v3

    .line 249
    :goto_0
    return-object p2

    .line 192
    :cond_1
    iget-object v0, p0, Lhwk;->a:Lhwi;

    invoke-virtual {v0}, Lhwi;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 195
    instance-of v0, p2, Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v0, :cond_4

    .line 196
    check-cast p2, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 201
    :goto_1
    iget-object v0, p0, Lhwk;->a:Lhwi;

    .line 202
    invoke-static {v0}, Lhwi;->a(Lhwi;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_5

    iget-object v0, p0, Lhwk;->a:Lhwi;

    invoke-static {v0}, Lhwi;->b(Lhwi;)Lhvx;

    move-result-object v0

    iget v0, v0, Lhvx;->j:I

    .line 201
    :goto_2
    invoke-virtual {p2, v1, v1, v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setPadding(IIII)V

    .line 204
    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 205
    iget-object v0, p0, Lhwk;->a:Lhwi;

    invoke-static {v0}, Lhwi;->a(Lhwi;)I

    move-result v0

    if-ne v0, v2, :cond_6

    .line 206
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f110024

    new-array v5, v2, [Ljava/lang/Object;

    .line 207
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    .line 206
    invoke-virtual {v0, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 214
    :goto_3
    iget-object v4, p0, Lhwk;->a:Lhwi;

    invoke-static {v4}, Lhwi;->a(Lhwi;)I

    move-result v4

    if-ne v4, v2, :cond_7

    .line 215
    invoke-virtual {p2, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 216
    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {p2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(F)V

    .line 217
    invoke-virtual {p2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 218
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Z)V

    .line 228
    :goto_4
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    .line 229
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->l(Z)V

    .line 230
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->setTag(Ljava/lang/Object;)V

    .line 231
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v0, p0, Lhwk;->a:Lhwi;

    iget-object v0, v0, Lhwi;->a:Lhwn;

    invoke-virtual {v0, p1}, Lhwn;->a(I)Lhwm;

    move-result-object v0

    iget-object v0, v0, Lhwm;->a:Lizu;

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 235
    iget-object v0, p0, Lhwk;->a:Lhwi;

    invoke-static {v0}, Lhwi;->a(Lhwi;)I

    move-result v0

    if-le v0, p1, :cond_2

    .line 236
    iget-object v0, p0, Lhwk;->a:Lhwi;

    iget-object v0, v0, Lhwi;->a:Lhwn;

    invoke-virtual {v0, p1}, Lhwn;->a(I)Lhwm;

    move-result-object v0

    iget-object v0, v0, Lhwm;->b:Lhjy;

    if-eqz v0, :cond_2

    .line 237
    iget-object v3, p0, Lhwk;->a:Lhwi;

    .line 240
    :cond_2
    if-eqz v3, :cond_8

    move v0, v2

    :goto_5
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setClickable(Z)V

    .line 241
    invoke-virtual {p2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    iget-object v0, p0, Lhwk;->a:Lhwi;

    iget-object v0, v0, Lhwi;->a:Lhwn;

    invoke-virtual {v0, p1}, Lhwn;->a(I)Lhwm;

    move-result-object v0

    iget v0, v0, Lhwm;->d:I

    iget-object v2, p0, Lhwk;->a:Lhwi;

    .line 244
    invoke-static {v2}, Lhwi;->a(Lhwi;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge p1, v2, :cond_3

    iget-object v1, p0, Lhwk;->a:Lhwi;

    invoke-static {v1}, Lhwi;->b(Lhwi;)Lhvx;

    move-result-object v1

    iget v1, v1, Lhvx;->j:I

    :cond_3
    add-int/2addr v0, v1

    .line 245
    new-instance v1, Llji;

    iget-object v2, p0, Lhwk;->a:Lhwi;

    .line 246
    invoke-static {v2}, Lhwi;->c(Lhwi;)I

    move-result v2

    invoke-direct {v1, v0, v2}, Llji;-><init>(II)V

    .line 247
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 248
    iget-object v0, p0, Lhwk;->a:Lhwi;

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Ljbb;)V

    goto/16 :goto_0

    .line 198
    :cond_4
    new-instance p2, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {p2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_5
    move v0, v1

    .line 202
    goto/16 :goto_2

    .line 210
    :cond_6
    const v0, 0x7f0a047c

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v6, p1, 0x1

    .line 211
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget-object v6, p0, Lhwk;->a:Lhwi;

    invoke-static {v6}, Lhwi;->a(Lhwi;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    .line 210
    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 220
    :cond_7
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 221
    const v4, 0x3ecccccd    # 0.4f

    invoke-virtual {p2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(F)V

    .line 222
    iget-object v4, p0, Lhwk;->a:Lhwi;

    .line 223
    invoke-static {v4}, Lhwi;->b(Lhwi;)Lhvx;

    move-result-object v4

    iget-object v4, v4, Lhvx;->l:Landroid/graphics/drawable/Drawable;

    .line 222
    invoke-virtual {p2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 224
    invoke-virtual {p2, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Z)V

    goto/16 :goto_4

    :cond_8
    move v0, v1

    .line 240
    goto :goto_5
.end method
