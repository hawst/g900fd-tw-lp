.class final Ljww;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljxd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljxd",
        "<[B>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Landroid/os/ParcelFileDescriptor;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 714
    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Ljww;->a(Landroid/os/ParcelFileDescriptor;[B)V

    return-void
.end method

.method public a(Landroid/os/ParcelFileDescriptor;[B)V
    .locals 4

    .prologue
    .line 717
    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v1, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 719
    :try_start_0
    invoke-virtual {v1, p2}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 725
    invoke-static {v1}, Lifu;->a(Ljava/io/Closeable;)V

    .line 726
    :goto_0
    return-void

    .line 720
    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pipe closed early by caller? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 725
    invoke-static {v1}, Lifu;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 723
    :catch_1
    move-exception v0

    invoke-static {v1}, Lifu;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 725
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lifu;->a(Ljava/io/Closeable;)V

    throw v0
.end method
