.class public final Lnqf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lnqh;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 553
    invoke-direct {p0}, Loxq;-><init>()V

    .line 564
    const/high16 v0, -0x80000000

    iput v0, p0, Lnqf;->c:I

    .line 567
    const/4 v0, 0x0

    iput-object v0, p0, Lnqf;->b:Lnqh;

    .line 553
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 587
    const/4 v0, 0x0

    .line 588
    iget-object v1, p0, Lnqf;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 589
    const/4 v0, 0x1

    iget-object v1, p0, Lnqf;->a:Ljava/lang/String;

    .line 590
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 592
    :cond_0
    iget v1, p0, Lnqf;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 593
    const/4 v1, 0x4

    iget v2, p0, Lnqf;->c:I

    .line 594
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 596
    :cond_1
    iget-object v1, p0, Lnqf;->b:Lnqh;

    if-eqz v1, :cond_2

    .line 597
    const/4 v1, 0x5

    iget-object v2, p0, Lnqf;->b:Lnqh;

    .line 598
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 600
    :cond_2
    iget-object v1, p0, Lnqf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 601
    iput v0, p0, Lnqf;->ai:I

    .line 602
    return v0
.end method

.method public a(Loxn;)Lnqf;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 610
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 611
    sparse-switch v0, :sswitch_data_0

    .line 615
    iget-object v1, p0, Lnqf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 616
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnqf;->ah:Ljava/util/List;

    .line 619
    :cond_1
    iget-object v1, p0, Lnqf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 621
    :sswitch_0
    return-object p0

    .line 626
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqf;->a:Ljava/lang/String;

    goto :goto_0

    .line 630
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 631
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 634
    :cond_2
    iput v0, p0, Lnqf;->c:I

    goto :goto_0

    .line 636
    :cond_3
    iput v2, p0, Lnqf;->c:I

    goto :goto_0

    .line 641
    :sswitch_3
    iget-object v0, p0, Lnqf;->b:Lnqh;

    if-nez v0, :cond_4

    .line 642
    new-instance v0, Lnqh;

    invoke-direct {v0}, Lnqh;-><init>()V

    iput-object v0, p0, Lnqf;->b:Lnqh;

    .line 644
    :cond_4
    iget-object v0, p0, Lnqf;->b:Lnqh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 611
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x20 -> :sswitch_2
        0x2a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lnqf;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 573
    const/4 v0, 0x1

    iget-object v1, p0, Lnqf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 575
    :cond_0
    iget v0, p0, Lnqf;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 576
    const/4 v0, 0x4

    iget v1, p0, Lnqf;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 578
    :cond_1
    iget-object v0, p0, Lnqf;->b:Lnqh;

    if-eqz v0, :cond_2

    .line 579
    const/4 v0, 0x5

    iget-object v1, p0, Lnqf;->b:Lnqh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 581
    :cond_2
    iget-object v0, p0, Lnqf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 583
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 549
    invoke-virtual {p0, p1}, Lnqf;->a(Loxn;)Lnqf;

    move-result-object v0

    return-object v0
.end method
