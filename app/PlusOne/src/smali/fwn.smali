.class public final Lfwn;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljv;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/graphics/Point;

.field private e:Landroid/graphics/Point;

.field private f:Landroid/graphics/Point;

.field private g:Lfxf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-virtual {p0, p1, p2, p3}, Lfwn;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 168
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lfwn;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 47
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfwn;->e:Landroid/graphics/Point;

    .line 48
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    .line 49
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    const/16 v1, 0xf

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 52
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 53
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 54
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfwn;->addView(Landroid/view/View;)V

    .line 56
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfwn;->d:Landroid/graphics/Point;

    .line 57
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    .line 58
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 60
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    const/16 v1, 0xa

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 61
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 62
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 63
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfwn;->addView(Landroid/view/View;)V

    .line 65
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfwn;->f:Landroid/graphics/Point;

    .line 66
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfwn;->c:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lfwn;->c:Landroid/widget/TextView;

    const/16 v1, 0x19

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 69
    iget-object v0, p0, Lfwn;->c:Landroid/widget/TextView;

    invoke-static {}, Lljw;->a()Lljw;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 70
    iget-object v0, p0, Lfwn;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfwn;->addView(Landroid/view/View;)V

    .line 71
    return-void
.end method

.method public a(Landroid/text/style/URLSpan;)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lfwn;->g:Lfxf;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lfwn;->g:Lfxf;

    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lfxf;->c(Ljava/lang/String;)V

    .line 178
    :cond_0
    return-void
.end method

.method public a(Lfxf;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lfwn;->g:Lfxf;

    .line 98
    return-void
.end method

.method public a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    if-eqz p3, :cond_0

    .line 84
    iget-object v0, p0, Lfwn;->c:Landroid/widget/TextView;

    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lfwn;->c:Landroid/widget/TextView;

    .line 86
    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Llju;->a(Ljava/lang/String;Lljv;)Landroid/text/Spanned;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    if-eqz p4, :cond_0

    .line 88
    iget-object v0, p0, Lfwn;->c:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 91
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 154
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lfwn;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lfwn;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lfwn;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lfwn;->b:Landroid/widget/TextView;

    .line 155
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lfwn;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lfwn;->b:Landroid/widget/TextView;

    .line 156
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 154
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 158
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lfwn;->d:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lfwn;->d:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lfwn;->d:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lfwn;->a:Landroid/widget/TextView;

    .line 159
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lfwn;->d:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lfwn;->a:Landroid/widget/TextView;

    .line 160
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 158
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 162
    iget-object v0, p0, Lfwn;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lfwn;->f:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lfwn;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lfwn;->f:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lfwn;->c:Landroid/widget/TextView;

    .line 163
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lfwn;->f:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lfwn;->c:Landroid/widget/TextView;

    .line 164
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 162
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 165
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, -0x80000000

    const/4 v8, 0x0

    .line 102
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 103
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 104
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 105
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 109
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 108
    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->measure(II)V

    .line 111
    iget-object v0, p0, Lfwn;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 112
    iget-object v4, p0, Lfwn;->a:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    .line 114
    iget-object v5, p0, Lfwn;->d:Landroid/graphics/Point;

    sub-int v0, v1, v0

    iput v0, v5, Landroid/graphics/Point;->x:I

    .line 115
    iget-object v0, p0, Lfwn;->d:Landroid/graphics/Point;

    iput v8, v0, Landroid/graphics/Point;->y:I

    .line 118
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    iget-object v5, p0, Lfwn;->d:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    .line 119
    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 120
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 118
    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 122
    iget-object v0, p0, Lfwn;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    .line 124
    iget-object v5, p0, Lfwn;->e:Landroid/graphics/Point;

    iput v8, v5, Landroid/graphics/Point;->x:I

    .line 125
    iget-object v5, p0, Lfwn;->e:Landroid/graphics/Point;

    iput v8, v5, Landroid/graphics/Point;->y:I

    .line 128
    iget-object v5, p0, Lfwn;->e:Landroid/graphics/Point;

    iget v6, v5, Landroid/graphics/Point;->y:I

    sub-int v7, v4, v0

    invoke-static {v8, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Point;->y:I

    .line 129
    iget-object v5, p0, Lfwn;->d:Landroid/graphics/Point;

    iget v6, v5, Landroid/graphics/Point;->y:I

    sub-int v4, v0, v4

    invoke-static {v8, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/2addr v4, v6

    iput v4, v5, Landroid/graphics/Point;->y:I

    .line 132
    iget-object v4, p0, Lfwn;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x0

    .line 134
    iget-object v4, p0, Lfwn;->c:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 135
    iget-object v4, p0, Lfwn;->c:Landroid/widget/TextView;

    .line 136
    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    sub-int/2addr v2, v0

    .line 137
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 135
    invoke-virtual {v4, v5, v2}, Landroid/widget/TextView;->measure(II)V

    .line 139
    iget-object v2, p0, Lfwn;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    .line 141
    iget-object v3, p0, Lfwn;->f:Landroid/graphics/Point;

    iput v8, v3, Landroid/graphics/Point;->x:I

    .line 142
    iget-object v3, p0, Lfwn;->f:Landroid/graphics/Point;

    iput v0, v3, Landroid/graphics/Point;->y:I

    .line 144
    add-int/2addr v0, v2

    .line 147
    :cond_0
    invoke-static {v1, p1}, Lfwn;->resolveSize(II)I

    move-result v1

    .line 148
    invoke-static {v0, p2}, Lfwn;->resolveSize(II)I

    move-result v0

    .line 147
    invoke-virtual {p0, v1, v0}, Lfwn;->setMeasuredDimension(II)V

    .line 149
    return-void
.end method
