.class public final Lnmy;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnmx;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 389
    const v0, 0x2a5ddb6

    new-instance v1, Lnmz;

    invoke-direct {v1}, Lnmz;-><init>()V

    .line 394
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 393
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 390
    invoke-direct {p0}, Loxq;-><init>()V

    .line 397
    sget-object v0, Lnmx;->a:[Lnmx;

    iput-object v0, p0, Lnmy;->a:[Lnmx;

    .line 390
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 430
    .line 431
    iget-object v1, p0, Lnmy;->a:[Lnmx;

    if-eqz v1, :cond_1

    .line 432
    iget-object v2, p0, Lnmy;->a:[Lnmx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 433
    if-eqz v4, :cond_0

    .line 434
    const/4 v5, 0x1

    .line 435
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 432
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 439
    :cond_1
    iget-object v1, p0, Lnmy;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 440
    const/4 v1, 0x3

    iget-object v2, p0, Lnmy;->b:Ljava/lang/String;

    .line 441
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 443
    :cond_2
    iget-object v1, p0, Lnmy;->c:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 444
    const/4 v1, 0x4

    iget-object v2, p0, Lnmy;->c:Ljava/lang/Long;

    .line 445
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    :cond_3
    iget-object v1, p0, Lnmy;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 448
    const/4 v1, 0x5

    iget-object v2, p0, Lnmy;->d:Ljava/lang/Boolean;

    .line 449
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 451
    :cond_4
    iget-object v1, p0, Lnmy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 452
    iput v0, p0, Lnmy;->ai:I

    .line 453
    return v0
.end method

.method public a(Loxn;)Lnmy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 461
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 462
    sparse-switch v0, :sswitch_data_0

    .line 466
    iget-object v2, p0, Lnmy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 467
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnmy;->ah:Ljava/util/List;

    .line 470
    :cond_1
    iget-object v2, p0, Lnmy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    :sswitch_0
    return-object p0

    .line 477
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 478
    iget-object v0, p0, Lnmy;->a:[Lnmx;

    if-nez v0, :cond_3

    move v0, v1

    .line 479
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnmx;

    .line 480
    iget-object v3, p0, Lnmy;->a:[Lnmx;

    if-eqz v3, :cond_2

    .line 481
    iget-object v3, p0, Lnmy;->a:[Lnmx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 483
    :cond_2
    iput-object v2, p0, Lnmy;->a:[Lnmx;

    .line 484
    :goto_2
    iget-object v2, p0, Lnmy;->a:[Lnmx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 485
    iget-object v2, p0, Lnmy;->a:[Lnmx;

    new-instance v3, Lnmx;

    invoke-direct {v3}, Lnmx;-><init>()V

    aput-object v3, v2, v0

    .line 486
    iget-object v2, p0, Lnmy;->a:[Lnmx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 487
    invoke-virtual {p1}, Loxn;->a()I

    .line 484
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 478
    :cond_3
    iget-object v0, p0, Lnmy;->a:[Lnmx;

    array-length v0, v0

    goto :goto_1

    .line 490
    :cond_4
    iget-object v2, p0, Lnmy;->a:[Lnmx;

    new-instance v3, Lnmx;

    invoke-direct {v3}, Lnmx;-><init>()V

    aput-object v3, v2, v0

    .line 491
    iget-object v2, p0, Lnmy;->a:[Lnmx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 495
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmy;->b:Ljava/lang/String;

    goto :goto_0

    .line 499
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnmy;->c:Ljava/lang/Long;

    goto :goto_0

    .line 503
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnmy;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 462
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 408
    iget-object v0, p0, Lnmy;->a:[Lnmx;

    if-eqz v0, :cond_1

    .line 409
    iget-object v1, p0, Lnmy;->a:[Lnmx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 410
    if-eqz v3, :cond_0

    .line 411
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 409
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 415
    :cond_1
    iget-object v0, p0, Lnmy;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 416
    const/4 v0, 0x3

    iget-object v1, p0, Lnmy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 418
    :cond_2
    iget-object v0, p0, Lnmy;->c:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 419
    const/4 v0, 0x4

    iget-object v1, p0, Lnmy;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 421
    :cond_3
    iget-object v0, p0, Lnmy;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 422
    const/4 v0, 0x5

    iget-object v1, p0, Lnmy;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 424
    :cond_4
    iget-object v0, p0, Lnmy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 426
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0, p1}, Lnmy;->a(Loxn;)Lnmy;

    move-result-object v0

    return-object v0
.end method
