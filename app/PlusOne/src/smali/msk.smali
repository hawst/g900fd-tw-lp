.class public final Lmsk;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 1686
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1710
    iput v0, p0, Lmsk;->b:I

    .line 1719
    iput v0, p0, Lmsk;->f:I

    .line 1686
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 1768
    const/4 v0, 0x0

    .line 1769
    iget-object v1, p0, Lmsk;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1770
    const/4 v0, 0x1

    iget-object v1, p0, Lmsk;->a:Ljava/lang/String;

    .line 1771
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1773
    :cond_0
    iget v1, p0, Lmsk;->b:I

    if-eq v1, v3, :cond_1

    .line 1774
    const/4 v1, 0x2

    iget v2, p0, Lmsk;->b:I

    .line 1775
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1777
    :cond_1
    iget-object v1, p0, Lmsk;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1778
    const/4 v1, 0x3

    iget-object v2, p0, Lmsk;->c:Ljava/lang/String;

    .line 1779
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1781
    :cond_2
    iget-object v1, p0, Lmsk;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1782
    const/4 v1, 0x4

    iget-object v2, p0, Lmsk;->d:Ljava/lang/String;

    .line 1783
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1785
    :cond_3
    iget-object v1, p0, Lmsk;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1786
    const/4 v1, 0x5

    iget-object v2, p0, Lmsk;->e:Ljava/lang/String;

    .line 1787
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1789
    :cond_4
    iget v1, p0, Lmsk;->f:I

    if-eq v1, v3, :cond_5

    .line 1790
    const/4 v1, 0x6

    iget v2, p0, Lmsk;->f:I

    .line 1791
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1793
    :cond_5
    iget-object v1, p0, Lmsk;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1794
    const/4 v1, 0x7

    iget-object v2, p0, Lmsk;->g:Ljava/lang/String;

    .line 1795
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1797
    :cond_6
    iget-object v1, p0, Lmsk;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 1798
    const/16 v1, 0x8

    iget-object v2, p0, Lmsk;->h:Ljava/lang/Integer;

    .line 1799
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1801
    :cond_7
    iget-object v1, p0, Lmsk;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 1802
    const/16 v1, 0xa

    iget-object v2, p0, Lmsk;->i:Ljava/lang/Integer;

    .line 1803
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1805
    :cond_8
    iget-object v1, p0, Lmsk;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 1806
    const/16 v1, 0xb

    iget-object v2, p0, Lmsk;->j:Ljava/lang/Integer;

    .line 1807
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1809
    :cond_9
    iget-object v1, p0, Lmsk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1810
    iput v0, p0, Lmsk;->ai:I

    .line 1811
    return v0
.end method

.method public a(Loxn;)Lmsk;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1819
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1820
    sparse-switch v0, :sswitch_data_0

    .line 1824
    iget-object v1, p0, Lmsk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1825
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmsk;->ah:Ljava/util/List;

    .line 1828
    :cond_1
    iget-object v1, p0, Lmsk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1830
    :sswitch_0
    return-object p0

    .line 1835
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmsk;->a:Ljava/lang/String;

    goto :goto_0

    .line 1839
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1840
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 1848
    :cond_2
    iput v0, p0, Lmsk;->b:I

    goto :goto_0

    .line 1850
    :cond_3
    iput v2, p0, Lmsk;->b:I

    goto :goto_0

    .line 1855
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmsk;->c:Ljava/lang/String;

    goto :goto_0

    .line 1859
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmsk;->d:Ljava/lang/String;

    goto :goto_0

    .line 1863
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmsk;->e:Ljava/lang/String;

    goto :goto_0

    .line 1867
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1868
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-ne v0, v6, :cond_5

    .line 1873
    :cond_4
    iput v0, p0, Lmsk;->f:I

    goto :goto_0

    .line 1875
    :cond_5
    iput v2, p0, Lmsk;->f:I

    goto :goto_0

    .line 1880
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmsk;->g:Ljava/lang/String;

    goto :goto_0

    .line 1884
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsk;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 1888
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsk;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1892
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsk;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1820
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 1732
    iget-object v0, p0, Lmsk;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1733
    const/4 v0, 0x1

    iget-object v1, p0, Lmsk;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1735
    :cond_0
    iget v0, p0, Lmsk;->b:I

    if-eq v0, v2, :cond_1

    .line 1736
    const/4 v0, 0x2

    iget v1, p0, Lmsk;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1738
    :cond_1
    iget-object v0, p0, Lmsk;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1739
    const/4 v0, 0x3

    iget-object v1, p0, Lmsk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1741
    :cond_2
    iget-object v0, p0, Lmsk;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1742
    const/4 v0, 0x4

    iget-object v1, p0, Lmsk;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1744
    :cond_3
    iget-object v0, p0, Lmsk;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1745
    const/4 v0, 0x5

    iget-object v1, p0, Lmsk;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1747
    :cond_4
    iget v0, p0, Lmsk;->f:I

    if-eq v0, v2, :cond_5

    .line 1748
    const/4 v0, 0x6

    iget v1, p0, Lmsk;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1750
    :cond_5
    iget-object v0, p0, Lmsk;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1751
    const/4 v0, 0x7

    iget-object v1, p0, Lmsk;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1753
    :cond_6
    iget-object v0, p0, Lmsk;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 1754
    const/16 v0, 0x8

    iget-object v1, p0, Lmsk;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1756
    :cond_7
    iget-object v0, p0, Lmsk;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 1757
    const/16 v0, 0xa

    iget-object v1, p0, Lmsk;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1759
    :cond_8
    iget-object v0, p0, Lmsk;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 1760
    const/16 v0, 0xb

    iget-object v1, p0, Lmsk;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1762
    :cond_9
    iget-object v0, p0, Lmsk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1764
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1682
    invoke-virtual {p0, p1}, Lmsk;->a(Loxn;)Lmsk;

    move-result-object v0

    return-object v0
.end method
