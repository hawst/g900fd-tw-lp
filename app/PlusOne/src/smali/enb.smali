.class final Lenb;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Ljava/util/List",
        "<",
        "Lofi;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:Ljava/lang/String;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lofi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 204
    iput p2, p0, Lenb;->b:I

    .line 205
    iput-object p3, p0, Lenb;->c:Ljava/lang/String;

    .line 206
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lofi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    iput-object p1, p0, Lenb;->d:Ljava/util/List;

    .line 220
    invoke-virtual {p0}, Lenb;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    invoke-super {p0, p1}, Lhxz;->b(Ljava/lang/Object;)V

    .line 223
    :cond_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 195
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lenb;->a(Ljava/util/List;)V

    return-void
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lenb;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lenb;->d:Ljava/util/List;

    invoke-virtual {p0, v0}, Lenb;->a(Ljava/util/List;)V

    .line 215
    :goto_0
    return-void

    .line 213
    :cond_0
    invoke-virtual {p0}, Lenb;->t()V

    goto :goto_0
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lenb;->l()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lofi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    iget v0, p0, Lenb;->b:I

    .line 228
    new-instance v1, Ldjz;

    invoke-virtual {p0}, Lenb;->n()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lenb;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3}, Ldjz;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 230
    invoke-virtual {v1}, Ldjz;->l()V

    .line 231
    invoke-virtual {v1}, Ldjz;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    const-string v0, "PostEngagementLoader"

    invoke-virtual {v1, v0}, Ldjz;->d(Ljava/lang/String;)V

    .line 233
    const/4 v0, 0x0

    .line 235
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Ldjz;->b()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
