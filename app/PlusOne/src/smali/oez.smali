.class public final Loez;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[Loey;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7852
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7857
    sget-object v0, Loey;->a:[Loey;

    iput-object v0, p0, Loez;->b:[Loey;

    .line 7852
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 7878
    .line 7879
    iget-object v0, p0, Loez;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 7880
    const/4 v0, 0x1

    iget-object v2, p0, Loez;->a:Ljava/lang/Integer;

    .line 7881
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7883
    :goto_0
    iget-object v2, p0, Loez;->b:[Loey;

    if-eqz v2, :cond_1

    .line 7884
    iget-object v2, p0, Loez;->b:[Loey;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 7885
    if-eqz v4, :cond_0

    .line 7886
    const/4 v5, 0x2

    .line 7887
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 7884
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7891
    :cond_1
    iget-object v1, p0, Loez;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7892
    iput v0, p0, Loez;->ai:I

    .line 7893
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loez;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7901
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7902
    sparse-switch v0, :sswitch_data_0

    .line 7906
    iget-object v2, p0, Loez;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 7907
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loez;->ah:Ljava/util/List;

    .line 7910
    :cond_1
    iget-object v2, p0, Loez;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7912
    :sswitch_0
    return-object p0

    .line 7917
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loez;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 7921
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7922
    iget-object v0, p0, Loez;->b:[Loey;

    if-nez v0, :cond_3

    move v0, v1

    .line 7923
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loey;

    .line 7924
    iget-object v3, p0, Loez;->b:[Loey;

    if-eqz v3, :cond_2

    .line 7925
    iget-object v3, p0, Loez;->b:[Loey;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7927
    :cond_2
    iput-object v2, p0, Loez;->b:[Loey;

    .line 7928
    :goto_2
    iget-object v2, p0, Loez;->b:[Loey;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 7929
    iget-object v2, p0, Loez;->b:[Loey;

    new-instance v3, Loey;

    invoke-direct {v3}, Loey;-><init>()V

    aput-object v3, v2, v0

    .line 7930
    iget-object v2, p0, Loez;->b:[Loey;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7931
    invoke-virtual {p1}, Loxn;->a()I

    .line 7928
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 7922
    :cond_3
    iget-object v0, p0, Loez;->b:[Loey;

    array-length v0, v0

    goto :goto_1

    .line 7934
    :cond_4
    iget-object v2, p0, Loez;->b:[Loey;

    new-instance v3, Loey;

    invoke-direct {v3}, Loey;-><init>()V

    aput-object v3, v2, v0

    .line 7935
    iget-object v2, p0, Loez;->b:[Loey;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7902
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 7862
    iget-object v0, p0, Loez;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 7863
    const/4 v0, 0x1

    iget-object v1, p0, Loez;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7865
    :cond_0
    iget-object v0, p0, Loez;->b:[Loey;

    if-eqz v0, :cond_2

    .line 7866
    iget-object v1, p0, Loez;->b:[Loey;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 7867
    if-eqz v3, :cond_1

    .line 7868
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 7866
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7872
    :cond_2
    iget-object v0, p0, Loez;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7874
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7848
    invoke-virtual {p0, p1}, Loez;->a(Loxn;)Loez;

    move-result-object v0

    return-object v0
.end method
