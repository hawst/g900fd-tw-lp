.class public final Lddw;
.super Ldds;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ldds;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lddl;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 82
    invoke-virtual {p0, v0}, Lddw;->b(Z)Ldds;

    .line 83
    invoke-virtual {p0, v0}, Lddw;->d(Z)Ldds;

    .line 84
    invoke-virtual {p0, v0}, Lddw;->c(Z)Ldds;

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lddw;->e(Z)Ldds;

    .line 87
    const-wide v2, 0x420000000L

    .line 89
    iget-object v0, p0, Lddw;->f:Ljava/util/EnumMap;

    sget-object v1, Lddt;->a:Lddt;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 91
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Llsb;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const-wide v0, 0x620000000L

    .line 96
    :goto_0
    invoke-virtual {p0, v0, v1}, Lddw;->c(J)Ldds;

    .line 98
    new-instance v0, Lddu;

    invoke-direct {v0, p0}, Lddu;-><init>(Ldds;)V

    return-object v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/os/Bundle;)Ldds;
    .locals 1

    .prologue
    .line 75
    invoke-super {p0, p1, p2}, Ldds;->a(Landroid/content/Context;Landroid/os/Bundle;)Ldds;

    .line 76
    const-string v0, "selection_cluster_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddw;->a:Ljava/lang/String;

    .line 77
    return-object p0
.end method

.method public a(Lddl;)Ldds;
    .locals 1

    .prologue
    .line 48
    invoke-super {p0, p1}, Ldds;->a(Lddl;)Ldds;

    .line 49
    instance-of v0, p1, Lddu;

    if-eqz v0, :cond_0

    .line 50
    check-cast p1, Lddu;

    .line 51
    invoke-static {p1}, Lddu;->a(Lddu;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lddw;->a:Ljava/lang/String;

    .line 52
    invoke-static {p1}, Lddu;->b(Lddu;)I

    move-result v0

    iput v0, p0, Lddw;->b:I

    .line 53
    invoke-static {p1}, Lddu;->c(Lddu;)I

    move-result v0

    iput v0, p0, Lddw;->c:I

    .line 55
    :cond_0
    return-object p0
.end method

.method public a(I)Lddw;
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Lddw;->b:I

    .line 65
    return-object p0
.end method

.method public b(I)Lddw;
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lddw;->c:I

    .line 70
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lddw;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lddw;->a:Ljava/lang/String;

    .line 60
    return-object p0
.end method
