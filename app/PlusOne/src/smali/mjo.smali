.class public final Lmjo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvu;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8017
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8020
    iput-object v0, p0, Lmjo;->apiHeader:Llyq;

    .line 8023
    iput-object v0, p0, Lmjo;->a:Lnvu;

    .line 8017
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8040
    const/4 v0, 0x0

    .line 8041
    iget-object v1, p0, Lmjo;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 8042
    const/4 v0, 0x1

    iget-object v1, p0, Lmjo;->apiHeader:Llyq;

    .line 8043
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8045
    :cond_0
    iget-object v1, p0, Lmjo;->a:Lnvu;

    if-eqz v1, :cond_1

    .line 8046
    const/4 v1, 0x2

    iget-object v2, p0, Lmjo;->a:Lnvu;

    .line 8047
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8049
    :cond_1
    iget-object v1, p0, Lmjo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8050
    iput v0, p0, Lmjo;->ai:I

    .line 8051
    return v0
.end method

.method public a(Loxn;)Lmjo;
    .locals 2

    .prologue
    .line 8059
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8060
    sparse-switch v0, :sswitch_data_0

    .line 8064
    iget-object v1, p0, Lmjo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8065
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjo;->ah:Ljava/util/List;

    .line 8068
    :cond_1
    iget-object v1, p0, Lmjo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8070
    :sswitch_0
    return-object p0

    .line 8075
    :sswitch_1
    iget-object v0, p0, Lmjo;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 8076
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmjo;->apiHeader:Llyq;

    .line 8078
    :cond_2
    iget-object v0, p0, Lmjo;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8082
    :sswitch_2
    iget-object v0, p0, Lmjo;->a:Lnvu;

    if-nez v0, :cond_3

    .line 8083
    new-instance v0, Lnvu;

    invoke-direct {v0}, Lnvu;-><init>()V

    iput-object v0, p0, Lmjo;->a:Lnvu;

    .line 8085
    :cond_3
    iget-object v0, p0, Lmjo;->a:Lnvu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8060
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8028
    iget-object v0, p0, Lmjo;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 8029
    const/4 v0, 0x1

    iget-object v1, p0, Lmjo;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8031
    :cond_0
    iget-object v0, p0, Lmjo;->a:Lnvu;

    if-eqz v0, :cond_1

    .line 8032
    const/4 v0, 0x2

    iget-object v1, p0, Lmjo;->a:Lnvu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8034
    :cond_1
    iget-object v0, p0, Lmjo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8036
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8013
    invoke-virtual {p0, p1}, Lmjo;->a(Loxn;)Lmjo;

    move-result-object v0

    return-object v0
.end method
