.class public final Lmee;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lode;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9799
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9802
    iput-object v0, p0, Lmee;->apiHeader:Llyq;

    .line 9805
    iput-object v0, p0, Lmee;->a:Lode;

    .line 9799
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9822
    const/4 v0, 0x0

    .line 9823
    iget-object v1, p0, Lmee;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 9824
    const/4 v0, 0x1

    iget-object v1, p0, Lmee;->apiHeader:Llyq;

    .line 9825
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9827
    :cond_0
    iget-object v1, p0, Lmee;->a:Lode;

    if-eqz v1, :cond_1

    .line 9828
    const/4 v1, 0x2

    iget-object v2, p0, Lmee;->a:Lode;

    .line 9829
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9831
    :cond_1
    iget-object v1, p0, Lmee;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9832
    iput v0, p0, Lmee;->ai:I

    .line 9833
    return v0
.end method

.method public a(Loxn;)Lmee;
    .locals 2

    .prologue
    .line 9841
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9842
    sparse-switch v0, :sswitch_data_0

    .line 9846
    iget-object v1, p0, Lmee;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9847
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmee;->ah:Ljava/util/List;

    .line 9850
    :cond_1
    iget-object v1, p0, Lmee;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9852
    :sswitch_0
    return-object p0

    .line 9857
    :sswitch_1
    iget-object v0, p0, Lmee;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 9858
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmee;->apiHeader:Llyq;

    .line 9860
    :cond_2
    iget-object v0, p0, Lmee;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9864
    :sswitch_2
    iget-object v0, p0, Lmee;->a:Lode;

    if-nez v0, :cond_3

    .line 9865
    new-instance v0, Lode;

    invoke-direct {v0}, Lode;-><init>()V

    iput-object v0, p0, Lmee;->a:Lode;

    .line 9867
    :cond_3
    iget-object v0, p0, Lmee;->a:Lode;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9842
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9810
    iget-object v0, p0, Lmee;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 9811
    const/4 v0, 0x1

    iget-object v1, p0, Lmee;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9813
    :cond_0
    iget-object v0, p0, Lmee;->a:Lode;

    if-eqz v0, :cond_1

    .line 9814
    const/4 v0, 0x2

    iget-object v1, p0, Lmee;->a:Lode;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9816
    :cond_1
    iget-object v0, p0, Lmee;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9818
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9795
    invoke-virtual {p0, p1}, Lmee;->a(Loxn;)Lmee;

    move-result-object v0

    return-object v0
.end method
