.class public final Lnce;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Lnzx;

.field public d:[Lnyz;

.field private e:[Lnzx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 364
    invoke-direct {p0}, Loxq;-><init>()V

    .line 371
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lnce;->c:[Lnzx;

    .line 374
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lnce;->e:[Lnzx;

    .line 377
    sget-object v0, Lnyz;->a:[Lnyz;

    iput-object v0, p0, Lnce;->d:[Lnyz;

    .line 364
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 415
    .line 416
    iget-object v0, p0, Lnce;->a:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 417
    const/4 v0, 0x1

    iget-object v2, p0, Lnce;->a:Ljava/lang/String;

    .line 418
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 420
    :goto_0
    iget-object v2, p0, Lnce;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 421
    const/4 v2, 0x2

    iget-object v3, p0, Lnce;->b:Ljava/lang/String;

    .line 422
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 424
    :cond_0
    iget-object v2, p0, Lnce;->c:[Lnzx;

    if-eqz v2, :cond_2

    .line 425
    iget-object v3, p0, Lnce;->c:[Lnzx;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 426
    if-eqz v5, :cond_1

    .line 427
    const/4 v6, 0x3

    .line 428
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 425
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 432
    :cond_2
    iget-object v2, p0, Lnce;->e:[Lnzx;

    if-eqz v2, :cond_4

    .line 433
    iget-object v3, p0, Lnce;->e:[Lnzx;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 434
    if-eqz v5, :cond_3

    .line 435
    const/4 v6, 0x4

    .line 436
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 433
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 440
    :cond_4
    iget-object v2, p0, Lnce;->d:[Lnyz;

    if-eqz v2, :cond_6

    .line 441
    iget-object v2, p0, Lnce;->d:[Lnyz;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 442
    if-eqz v4, :cond_5

    .line 443
    const/4 v5, 0x5

    .line 444
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 441
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 448
    :cond_6
    iget-object v1, p0, Lnce;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 449
    iput v0, p0, Lnce;->ai:I

    .line 450
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnce;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 458
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 459
    sparse-switch v0, :sswitch_data_0

    .line 463
    iget-object v2, p0, Lnce;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 464
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnce;->ah:Ljava/util/List;

    .line 467
    :cond_1
    iget-object v2, p0, Lnce;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    :sswitch_0
    return-object p0

    .line 474
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnce;->a:Ljava/lang/String;

    goto :goto_0

    .line 478
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnce;->b:Ljava/lang/String;

    goto :goto_0

    .line 482
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 483
    iget-object v0, p0, Lnce;->c:[Lnzx;

    if-nez v0, :cond_3

    move v0, v1

    .line 484
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 485
    iget-object v3, p0, Lnce;->c:[Lnzx;

    if-eqz v3, :cond_2

    .line 486
    iget-object v3, p0, Lnce;->c:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 488
    :cond_2
    iput-object v2, p0, Lnce;->c:[Lnzx;

    .line 489
    :goto_2
    iget-object v2, p0, Lnce;->c:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 490
    iget-object v2, p0, Lnce;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 491
    iget-object v2, p0, Lnce;->c:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 492
    invoke-virtual {p1}, Loxn;->a()I

    .line 489
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 483
    :cond_3
    iget-object v0, p0, Lnce;->c:[Lnzx;

    array-length v0, v0

    goto :goto_1

    .line 495
    :cond_4
    iget-object v2, p0, Lnce;->c:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 496
    iget-object v2, p0, Lnce;->c:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 500
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 501
    iget-object v0, p0, Lnce;->e:[Lnzx;

    if-nez v0, :cond_6

    move v0, v1

    .line 502
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 503
    iget-object v3, p0, Lnce;->e:[Lnzx;

    if-eqz v3, :cond_5

    .line 504
    iget-object v3, p0, Lnce;->e:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 506
    :cond_5
    iput-object v2, p0, Lnce;->e:[Lnzx;

    .line 507
    :goto_4
    iget-object v2, p0, Lnce;->e:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 508
    iget-object v2, p0, Lnce;->e:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 509
    iget-object v2, p0, Lnce;->e:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 510
    invoke-virtual {p1}, Loxn;->a()I

    .line 507
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 501
    :cond_6
    iget-object v0, p0, Lnce;->e:[Lnzx;

    array-length v0, v0

    goto :goto_3

    .line 513
    :cond_7
    iget-object v2, p0, Lnce;->e:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 514
    iget-object v2, p0, Lnce;->e:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 518
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 519
    iget-object v0, p0, Lnce;->d:[Lnyz;

    if-nez v0, :cond_9

    move v0, v1

    .line 520
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyz;

    .line 521
    iget-object v3, p0, Lnce;->d:[Lnyz;

    if-eqz v3, :cond_8

    .line 522
    iget-object v3, p0, Lnce;->d:[Lnyz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 524
    :cond_8
    iput-object v2, p0, Lnce;->d:[Lnyz;

    .line 525
    :goto_6
    iget-object v2, p0, Lnce;->d:[Lnyz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 526
    iget-object v2, p0, Lnce;->d:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 527
    iget-object v2, p0, Lnce;->d:[Lnyz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 528
    invoke-virtual {p1}, Loxn;->a()I

    .line 525
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 519
    :cond_9
    iget-object v0, p0, Lnce;->d:[Lnyz;

    array-length v0, v0

    goto :goto_5

    .line 531
    :cond_a
    iget-object v2, p0, Lnce;->d:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 532
    iget-object v2, p0, Lnce;->d:[Lnyz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 459
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 382
    iget-object v1, p0, Lnce;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 383
    const/4 v1, 0x1

    iget-object v2, p0, Lnce;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 385
    :cond_0
    iget-object v1, p0, Lnce;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 386
    const/4 v1, 0x2

    iget-object v2, p0, Lnce;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 388
    :cond_1
    iget-object v1, p0, Lnce;->c:[Lnzx;

    if-eqz v1, :cond_3

    .line 389
    iget-object v2, p0, Lnce;->c:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 390
    if-eqz v4, :cond_2

    .line 391
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 389
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 395
    :cond_3
    iget-object v1, p0, Lnce;->e:[Lnzx;

    if-eqz v1, :cond_5

    .line 396
    iget-object v2, p0, Lnce;->e:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 397
    if-eqz v4, :cond_4

    .line 398
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 396
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 402
    :cond_5
    iget-object v1, p0, Lnce;->d:[Lnyz;

    if-eqz v1, :cond_7

    .line 403
    iget-object v1, p0, Lnce;->d:[Lnyz;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 404
    if-eqz v3, :cond_6

    .line 405
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 403
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 409
    :cond_7
    iget-object v0, p0, Lnce;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 411
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 360
    invoke-virtual {p0, p1}, Lnce;->a(Loxn;)Lnce;

    move-result-object v0

    return-object v0
.end method
