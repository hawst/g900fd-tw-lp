.class public final Lnno;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnnp;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Lnmr;

.field public f:Loxz;

.field private g:Lofl;

.field private h:Logu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-direct {p0}, Loxq;-><init>()V

    .line 253
    iput-object v1, p0, Lnno;->a:Lnnp;

    .line 260
    const/high16 v0, -0x80000000

    iput v0, p0, Lnno;->d:I

    .line 263
    iput-object v1, p0, Lnno;->g:Lofl;

    .line 266
    iput-object v1, p0, Lnno;->h:Logu;

    .line 269
    iput-object v1, p0, Lnno;->e:Lnmr;

    .line 272
    iput-object v1, p0, Lnno;->f:Loxz;

    .line 177
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 307
    const/4 v0, 0x0

    .line 308
    iget-object v1, p0, Lnno;->a:Lnnp;

    if-eqz v1, :cond_0

    .line 309
    const/4 v0, 0x1

    iget-object v1, p0, Lnno;->a:Lnnp;

    .line 310
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 312
    :cond_0
    iget-object v1, p0, Lnno;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 313
    const/4 v1, 0x2

    iget-object v2, p0, Lnno;->b:Ljava/lang/Integer;

    .line 314
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 316
    :cond_1
    iget-object v1, p0, Lnno;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 317
    const/4 v1, 0x3

    iget-object v2, p0, Lnno;->c:Ljava/lang/String;

    .line 318
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 320
    :cond_2
    iget v1, p0, Lnno;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 321
    const/4 v1, 0x4

    iget v2, p0, Lnno;->d:I

    .line 322
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 324
    :cond_3
    iget-object v1, p0, Lnno;->g:Lofl;

    if-eqz v1, :cond_4

    .line 325
    const/4 v1, 0x5

    iget-object v2, p0, Lnno;->g:Lofl;

    .line 326
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 328
    :cond_4
    iget-object v1, p0, Lnno;->h:Logu;

    if-eqz v1, :cond_5

    .line 329
    const/4 v1, 0x6

    iget-object v2, p0, Lnno;->h:Logu;

    .line 330
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 332
    :cond_5
    iget-object v1, p0, Lnno;->e:Lnmr;

    if-eqz v1, :cond_6

    .line 333
    const/4 v1, 0x7

    iget-object v2, p0, Lnno;->e:Lnmr;

    .line 334
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 336
    :cond_6
    iget-object v1, p0, Lnno;->f:Loxz;

    if-eqz v1, :cond_7

    .line 337
    const/16 v1, 0x8

    iget-object v2, p0, Lnno;->f:Loxz;

    .line 338
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 340
    :cond_7
    iget-object v1, p0, Lnno;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 341
    iput v0, p0, Lnno;->ai:I

    .line 342
    return v0
.end method

.method public a(Loxn;)Lnno;
    .locals 2

    .prologue
    .line 350
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 351
    sparse-switch v0, :sswitch_data_0

    .line 355
    iget-object v1, p0, Lnno;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 356
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnno;->ah:Ljava/util/List;

    .line 359
    :cond_1
    iget-object v1, p0, Lnno;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 361
    :sswitch_0
    return-object p0

    .line 366
    :sswitch_1
    iget-object v0, p0, Lnno;->a:Lnnp;

    if-nez v0, :cond_2

    .line 367
    new-instance v0, Lnnp;

    invoke-direct {v0}, Lnnp;-><init>()V

    iput-object v0, p0, Lnno;->a:Lnnp;

    .line 369
    :cond_2
    iget-object v0, p0, Lnno;->a:Lnnp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 373
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnno;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 377
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnno;->c:Ljava/lang/String;

    goto :goto_0

    .line 381
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 382
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 385
    :cond_3
    iput v0, p0, Lnno;->d:I

    goto :goto_0

    .line 387
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lnno;->d:I

    goto :goto_0

    .line 392
    :sswitch_5
    iget-object v0, p0, Lnno;->g:Lofl;

    if-nez v0, :cond_5

    .line 393
    new-instance v0, Lofl;

    invoke-direct {v0}, Lofl;-><init>()V

    iput-object v0, p0, Lnno;->g:Lofl;

    .line 395
    :cond_5
    iget-object v0, p0, Lnno;->g:Lofl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 399
    :sswitch_6
    iget-object v0, p0, Lnno;->h:Logu;

    if-nez v0, :cond_6

    .line 400
    new-instance v0, Logu;

    invoke-direct {v0}, Logu;-><init>()V

    iput-object v0, p0, Lnno;->h:Logu;

    .line 402
    :cond_6
    iget-object v0, p0, Lnno;->h:Logu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 406
    :sswitch_7
    iget-object v0, p0, Lnno;->e:Lnmr;

    if-nez v0, :cond_7

    .line 407
    new-instance v0, Lnmr;

    invoke-direct {v0}, Lnmr;-><init>()V

    iput-object v0, p0, Lnno;->e:Lnmr;

    .line 409
    :cond_7
    iget-object v0, p0, Lnno;->e:Lnmr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 413
    :sswitch_8
    iget-object v0, p0, Lnno;->f:Loxz;

    if-nez v0, :cond_8

    .line 414
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, p0, Lnno;->f:Loxz;

    .line 416
    :cond_8
    iget-object v0, p0, Lnno;->f:Loxz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 351
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lnno;->a:Lnnp;

    if-eqz v0, :cond_0

    .line 278
    const/4 v0, 0x1

    iget-object v1, p0, Lnno;->a:Lnnp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 280
    :cond_0
    iget-object v0, p0, Lnno;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 281
    const/4 v0, 0x2

    iget-object v1, p0, Lnno;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 283
    :cond_1
    iget-object v0, p0, Lnno;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 284
    const/4 v0, 0x3

    iget-object v1, p0, Lnno;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 286
    :cond_2
    iget v0, p0, Lnno;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 287
    const/4 v0, 0x4

    iget v1, p0, Lnno;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 289
    :cond_3
    iget-object v0, p0, Lnno;->g:Lofl;

    if-eqz v0, :cond_4

    .line 290
    const/4 v0, 0x5

    iget-object v1, p0, Lnno;->g:Lofl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 292
    :cond_4
    iget-object v0, p0, Lnno;->h:Logu;

    if-eqz v0, :cond_5

    .line 293
    const/4 v0, 0x6

    iget-object v1, p0, Lnno;->h:Logu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 295
    :cond_5
    iget-object v0, p0, Lnno;->e:Lnmr;

    if-eqz v0, :cond_6

    .line 296
    const/4 v0, 0x7

    iget-object v1, p0, Lnno;->e:Lnmr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 298
    :cond_6
    iget-object v0, p0, Lnno;->f:Loxz;

    if-eqz v0, :cond_7

    .line 299
    const/16 v0, 0x8

    iget-object v1, p0, Lnno;->f:Loxz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 301
    :cond_7
    iget-object v0, p0, Lnno;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 303
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lnno;->a(Loxn;)Lnno;

    move-result-object v0

    return-object v0
.end method
