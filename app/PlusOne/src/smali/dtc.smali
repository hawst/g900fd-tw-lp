.class public final Ldtc;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static a(Landroid/database/sqlite/SQLiteDatabase;Lnzx;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 307
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 308
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v0, v0, Lnzu;->b:Lnym;

    .line 309
    iget-object v2, v0, Lnym;->h:Lnyz;

    iget-object v2, v2, Lnyz;->c:Ljava/lang/String;

    iget-object v3, v0, Lnym;->l:Lnyb;

    iget-object v3, v3, Lnyb;->d:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 312
    new-array v3, v6, [Ljava/lang/String;

    aput-object v2, v3, v5

    invoke-static {v4, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 314
    :try_start_0
    const-string v3, "SELECT tile_id FROM all_tiles WHERE view_id = ?  AND owner_id = ?  AND photo_id = ?  AND media_attr & 512 == 0"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    iget-object v5, v0, Lnym;->h:Lnyz;

    iget-object v5, v5, Lnyz;->c:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x2

    iget-object v0, v0, Lnym;->e:Ljava/lang/String;

    aput-object v0, v4, v2

    invoke-static {p0, v3, v4}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 346
    :goto_0
    return-object v0

    .line 319
    :cond_0
    sget-object v0, Lnzr;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 320
    sget-object v0, Lnzr;->a:Loxr;

    .line 321
    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    iget-object v0, v0, Lnzr;->b:Lnxr;

    .line 322
    iget-object v2, v0, Lnxr;->f:Ljava/lang/String;

    iget-object v3, v0, Lnxr;->c:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 325
    new-array v3, v6, [Ljava/lang/String;

    aput-object v2, v3, v5

    invoke-static {v4, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 327
    :try_start_1
    const-string v3, "SELECT tile_id FROM all_tiles WHERE view_id = ?  AND owner_id = ?  AND photo_id IS NULL  AND media_attr & 512 == 0"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    iget-object v0, v0, Lnxr;->f:Ljava/lang/String;

    aput-object v0, v4, v2

    invoke-static {p0, v3, v4}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 332
    :cond_1
    sget-object v0, Lnzo;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 333
    sget-object v0, Lnzo;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzo;

    iget-object v0, v0, Lnzo;->b:Lnyb;

    .line 334
    iget-object v2, v0, Lnyb;->f:Lnyz;

    iget-object v2, v2, Lnyz;->c:Ljava/lang/String;

    iget-object v3, v0, Lnyb;->d:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 336
    new-array v3, v6, [Ljava/lang/String;

    aput-object v2, v3, v5

    invoke-static {v4, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 338
    :try_start_2
    const-string v3, "SELECT tile_id FROM all_tiles WHERE view_id = ?  AND owner_id = ?  AND photo_id IS NULL  AND media_attr & 512 == 0"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    iget-object v0, v0, Lnyb;->f:Lnyz;

    iget-object v0, v0, Lnyz;->c:Ljava/lang/String;

    aput-object v0, v4, v2

    invoke-static {p0, v3, v4}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_0

    .line 344
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tile must be a known type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 343
    :catch_0
    move-exception v0

    :goto_1
    move-object v0, v1

    .line 346
    goto :goto_0

    .line 332
    :catch_1
    move-exception v0

    goto :goto_1

    .line 319
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lepn;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    const-string v0, "~promo:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lepn;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lkzv;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 116
    const/4 v0, 0x0

    .line 117
    invoke-virtual {p0}, Lkzv;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lkzv;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ALBUM"

    .line 116
    invoke-static {v0, v1, v2, v3}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    const/4 v1, 0x3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lkff;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 75
    if-nez p2, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must specify non-null cluster ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    const-string v0, ":"

    const/4 v2, 0x2

    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 80
    aget-object v2, v0, v4

    const-string v3, "best"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 81
    const-string v0, "EsTileData"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    const-string v0, "[GET_REFRESH_OP] best photos op; cluster: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 84
    :cond_1
    :goto_0
    new-instance v0, Ldkp;

    invoke-direct {v0, p0, p1, v1, p3}, Ldkp;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 111
    :goto_1
    return-object v0

    .line 82
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :cond_3
    aget-object v2, v0, v4

    const-string v3, "trash"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 88
    const-string v0, "EsTileData"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 89
    const-string v0, "[GET_REFRESH_OP] trash photos op; cluster: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 91
    :cond_4
    :goto_2
    new-instance v0, Ldnx;

    invoke-direct {v0, p0, p1, p3, p4}, Ldnx;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    goto :goto_1

    .line 89
    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 93
    :cond_6
    aget-object v2, v0, v4

    const-string v3, "all"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 94
    const-string v0, "EsTileData"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 95
    const-string v0, "[GET_REFRESH_OP] all photos op; cluster: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 97
    :cond_7
    :goto_3
    new-instance v0, Ldih;

    invoke-direct {v0, p0, p1, p3}, Ldih;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_1

    .line 95
    :cond_8
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 99
    :cond_9
    aget-object v2, v0, v4

    const-string v3, "album"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 100
    const-string v0, "EsTileData"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 101
    const-string v0, "[GET_REFRESH_OP] collection op; cluster: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 103
    :cond_a
    :goto_4
    new-instance v0, Ldip;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p5

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Ldip;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 101
    :cond_b
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 107
    :cond_c
    const-string v2, "EsTileData"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 108
    aget-object v0, v0, v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2f

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "[GET_REFRESH_OP] no op found; view: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", cluster: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    move-object v0, v1

    .line 111
    goto/16 :goto_1
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;Lhtc;Lnyb;)Lnzx;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 584
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-wide v4, p1, Lhtc;->f:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhtc;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhtc;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 586
    :cond_0
    const-string v0, "EsTileData"

    const-string v1, "Invalid DbEmbedMedia object; cannot insert into the database"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    const/4 v0, 0x0

    .line 630
    :goto_0
    return-object v0

    .line 590
    :cond_1
    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    .line 591
    iget-object v3, p1, Lhtc;->d:Ljava/lang/String;

    iput-object v3, v0, Lnyz;->c:Ljava/lang/String;

    .line 593
    new-instance v3, Lnyl;

    invoke-direct {v3}, Lnyl;-><init>()V

    .line 594
    iget-short v4, p1, Lhtc;->h:S

    if-eqz v4, :cond_2

    .line 595
    iget-short v4, p1, Lhtc;->h:S

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lnyl;->d:Ljava/lang/Integer;

    .line 597
    :cond_2
    iget-short v4, p1, Lhtc;->g:S

    if-eqz v4, :cond_3

    .line 598
    iget-short v4, p1, Lhtc;->g:S

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lnyl;->c:Ljava/lang/Integer;

    .line 600
    :cond_3
    iget-object v4, p1, Lhtc;->c:Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 601
    iget-object v4, p1, Lhtc;->c:Ljava/lang/String;

    invoke-static {v4}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lnyl;->b:Ljava/lang/String;

    .line 604
    :cond_4
    new-instance v4, Lnym;

    invoke-direct {v4}, Lnym;-><init>()V

    .line 605
    iput-object p2, v4, Lnym;->l:Lnyb;

    .line 606
    iget-wide v6, p1, Lhtc;->f:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lnym;->e:Ljava/lang/String;

    .line 607
    iput-object v3, v4, Lnym;->b:Lnyl;

    .line 608
    iput-object v0, v4, Lnym;->h:Lnyz;

    .line 609
    iget-object v0, p1, Lhtc;->b:Ljava/lang/String;

    iput-object v0, v4, Lnym;->d:Ljava/lang/String;

    .line 611
    iget v0, p1, Lhtc;->i:I

    if-ne v0, v8, :cond_7

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Lnym;->M:Ljava/lang/Boolean;

    .line 612
    iget v0, p1, Lhtc;->i:I

    const/4 v5, 0x3

    if-ne v0, v5, :cond_8

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Lnym;->B:Ljava/lang/Boolean;

    .line 613
    iget v0, p1, Lhtc;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 614
    new-instance v0, Lnzb;

    invoke-direct {v0}, Lnzb;-><init>()V

    iput-object v0, v4, Lnym;->m:Lnzb;

    .line 617
    :cond_5
    new-instance v0, Lnzu;

    invoke-direct {v0}, Lnzu;-><init>()V

    .line 618
    iput-object v4, v0, Lnzu;->b:Lnym;

    .line 620
    new-instance v1, Lnzx;

    invoke-direct {v1}, Lnzx;-><init>()V

    .line 621
    iput v8, v1, Lnzx;->k:I

    .line 622
    sget-object v2, Lnzu;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 623
    iput-object v3, v1, Lnzx;->f:Lnyl;

    .line 624
    invoke-static {p0, v1}, Ldtc;->a(Landroid/database/sqlite/SQLiteDatabase;Lnzx;)Ljava/lang/String;

    move-result-object v0

    .line 625
    if-nez v0, :cond_6

    .line 626
    invoke-static {v1}, Ljvj;->a(Lnzx;)Ljava/lang/String;

    move-result-object v0

    .line 628
    :cond_6
    iput-object v0, v1, Lnzx;->b:Ljava/lang/String;

    move-object v0, v1

    .line 630
    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 611
    goto :goto_1

    :cond_8
    move v1, v2

    .line 612
    goto :goto_2
.end method

.method static a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 10

    .prologue
    .line 155
    const-string v0, "all_tiles"

    const-string v1, "view_id LIKE \'albums:%\' AND media_attr & 16384 == 0"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 161
    const-string v0, "all_tiles"

    const-string v1, "view_id LIKE \'"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "%"

    aput-object v5, v3, v4

    .line 162
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "view_id"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x5

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "#autoawesome"

    aput-object v7, v5, v6

    .line 164
    invoke-static {v4, v5}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "view_id"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "#videos"

    aput-object v9, v7, v8

    .line 166
    invoke-static {v6, v7}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x17

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 161
    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 170
    const-string v0, "all_tiles"

    const-string v1, "view_id LIKE \'event:%\'"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 175
    const-string v0, "all_tiles"

    const-string v1, "view_id LIKE \'album:%\' AND view_id NOT IN (  SELECT cluster_id FROM all_tiles WHERE view_id = \'"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 181
    invoke-static {p0, p2}, Ljvj;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "type"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "media_attr & 512 == 0"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1b

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = 2 ) AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 175
    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 186
    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 188
    sget-object v1, Ldxd;->i:Lief;

    invoke-interface {v0, v1, p2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 190
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 191
    const-string v0, "SELECT DISTINCT view_id FROM all_tiles WHERE view_id LIKE \'notification:%\'"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 199
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 205
    const-string v1, "guns"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "key"

    aput-object v3, v2, v0

    const-string v3, "PHOTOS NOT NULL "

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 209
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 211
    const/4 v2, 0x6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 212
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 211
    invoke-interface {v8, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 215
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 217
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 218
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 219
    const-string v0, "view_id IN ( "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 220
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    :goto_2
    if-ltz v0, :cond_2

    .line 221
    const-string v2, "?, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 220
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 223
    :cond_2
    const-string v0, "? )"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 225
    const-string v2, "all_tiles"

    .line 226
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v8, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 225
    invoke-virtual {p1, v2, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 231
    :cond_3
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 232
    const-string v0, "SELECT DISTINCT view_id FROM all_tiles WHERE view_id LIKE \'album:%\' AND media_attr & 512 != 0"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 238
    :goto_3
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 239
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_3

    .line 242
    :catchall_2
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 245
    const-string v1, "activities"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "embed"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "content_flags"

    aput-object v3, v2, v0

    const-string v3, "embed NOT NULL  AND (content_flags & 96) != 0"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 253
    :cond_5
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 254
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 255
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 257
    const-wide/16 v4, 0x20

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_6

    .line 258
    invoke-static {v0}, Lkzv;->a([B)Lkzv;

    move-result-object v4

    .line 259
    invoke-static {v4}, Ldtc;->a(Lkzv;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v8, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 262
    :cond_6
    const-wide/16 v4, 0x40

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 263
    invoke-static {v0}, Lkzr;->a([B)Lkzr;

    move-result-object v2

    .line 264
    invoke-virtual {v2}, Lkzr;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_4
    if-ltz v0, :cond_5

    .line 265
    invoke-virtual {v2, v0}, Lkzr;->a(I)Lkzv;

    move-result-object v3

    .line 266
    invoke-static {v3}, Ldtc;->a(Lkzv;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 264
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    .line 271
    :cond_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 273
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_9

    .line 274
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 275
    const-string v0, "view_id IN ( "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 276
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    :goto_5
    if-ltz v0, :cond_8

    .line 277
    const-string v2, "?, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 276
    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    .line 271
    :catchall_3
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 279
    :cond_8
    const-string v0, "? )"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 281
    const-string v2, "all_tiles"

    .line 282
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v8, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 281
    invoke-virtual {p1, v2, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 286
    :cond_9
    const-string v0, "DELETE FROM tile_requests WHERE view_id NOT IN ( SELECT DISTINCT view_id FROM all_tiles WHERE media_attr & 512 == 0 )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 293
    const-string v0, "DELETE FROM tile_requests WHERE view_id LIKE \'best:%\' AND view_id != \'"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 296
    invoke-static {p0, p2}, Ljvj;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 293
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 298
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Lhsz;Ljava/lang/String;)[Lnzx;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 363
    if-nez p1, :cond_0

    .line 364
    const-string v0, "EsTileData"

    const-string v1, "Null embedAlbum object; cannot insert into the database"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 397
    :goto_0
    return-object v0

    .line 368
    :cond_0
    iget v1, p1, Lhsz;->b:I

    .line 369
    if-nez v1, :cond_1

    .line 370
    const-string v0, "EsTileData"

    const-string v1, "Invalid photo list; no photos"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 371
    goto :goto_0

    .line 374
    :cond_1
    new-array v4, v1, [Lnzx;

    move v0, v2

    .line 375
    :goto_1
    if-ge v0, v1, :cond_3

    .line 376
    iget-object v5, p1, Lhsz;->g:[Lhtc;

    aget-object v5, v5, v0

    .line 377
    new-instance v6, Lnyz;

    invoke-direct {v6}, Lnyz;-><init>()V

    .line 378
    iget-object v7, v5, Lhtc;->d:Ljava/lang/String;

    iput-object v7, v6, Lnyz;->c:Ljava/lang/String;

    .line 380
    new-instance v7, Lnyb;

    invoke-direct {v7}, Lnyb;-><init>()V

    .line 381
    iget-object v8, v5, Lhtc;->e:Ljava/lang/String;

    iput-object v8, v7, Lnyb;->d:Ljava/lang/String;

    .line 385
    iget-object v8, v7, Lnyb;->d:Ljava/lang/String;

    if-nez v8, :cond_2

    .line 386
    const-string v8, ""

    iput-object v8, v7, Lnyb;->d:Ljava/lang/String;

    .line 388
    :cond_2
    iput-object v6, v7, Lnyb;->f:Lnyz;

    .line 389
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v7, Lnyb;->c:Ljava/lang/Integer;

    .line 390
    const/4 v6, 0x3

    iput v6, v7, Lnyb;->e:I

    .line 391
    const-string v6, ""

    iput-object v6, v7, Lnyb;->b:Ljava/lang/String;

    .line 393
    invoke-static {p0, v5, v7}, Ldtc;->a(Landroid/database/sqlite/SQLiteDatabase;Lhtc;Lnyb;)Lnzx;

    move-result-object v5

    .line 394
    aput-object v5, v4, v0

    .line 375
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 397
    :cond_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v4

    goto :goto_0

    :cond_4
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "pids"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object v0, v4

    goto :goto_0

    :cond_5
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    array-length v0, v8

    array-length v1, v4

    if-eq v0, v1, :cond_6

    move-object v0, v4

    goto :goto_0

    :cond_6
    array-length v0, v4

    new-array v5, v0, [Lnzx;

    move v1, v2

    :goto_2
    array-length v0, v8

    if-ge v1, v0, :cond_a

    aget-object v9, v8, v1

    array-length v10, v4

    move v7, v2

    :goto_3
    if-ge v7, v10, :cond_8

    aget-object v6, v4, v7

    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v6, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->e:Ljava/lang/String;

    invoke-static {v0, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, v6

    :goto_4
    if-nez v0, :cond_9

    move-object v0, v4

    goto/16 :goto_0

    :cond_7
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_3

    :cond_8
    move-object v0, v3

    goto :goto_4

    :cond_9
    aput-object v0, v5, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_a
    move-object v0, v5

    goto/16 :goto_0
.end method
