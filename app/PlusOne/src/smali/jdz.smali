.class public final Ljdz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljdz;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/Long;

.field public final b:Ljava/lang/String;

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Ljea;

    invoke-direct {v0}, Ljea;-><init>()V

    sput-object v0, Ljdz;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Ljdz;->a:Ljava/lang/Long;

    .line 35
    iput-object p2, p0, Ljdz;->b:Ljava/lang/String;

    .line 36
    const/16 v0, 0x11

    .line 38
    invoke-static {p2, v0}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 37
    invoke-static {p1, v0}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v0

    iput v0, p0, Ljdz;->c:I

    .line 39
    return-void
.end method

.method public static a(Lonz;)Ljdz;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Ljdz;

    iget-object v1, p0, Lonz;->c:Ljava/lang/Long;

    iget-object v2, p0, Lonz;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljdz;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a([B)Ljdz;
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lonz;

    invoke-direct {v0}, Lonz;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lonz;

    invoke-static {v0}, Ljdz;->a(Lonz;)Ljdz;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lonz;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lonz;

    invoke-direct {v0}, Lonz;-><init>()V

    .line 52
    iget-object v1, p0, Ljdz;->b:Ljava/lang/String;

    iput-object v1, v0, Lonz;->b:Ljava/lang/String;

    .line 53
    iget-object v1, p0, Ljdz;->a:Ljava/lang/Long;

    iput-object v1, v0, Lonz;->c:Ljava/lang/Long;

    .line 54
    return-object v0
.end method

.method public b()[B
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Ljdz;->a()Lonz;

    move-result-object v0

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 64
    instance-of v2, p1, Ljdz;

    if-nez v2, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_2
    check-cast p1, Ljdz;

    .line 71
    iget-object v2, p0, Ljdz;->a:Ljava/lang/Long;

    iget-object v3, p1, Ljdz;->a:Ljava/lang/Long;

    invoke-static {v2, v3}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Ljdz;->b:Ljava/lang/String;

    iget-object v3, p1, Ljdz;->b:Ljava/lang/String;

    .line 72
    invoke-static {v2, v3}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Ljdz;->c:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 92
    const-string v0, "CloudPhotoId"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Ljdz;->a:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Ljdz;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Llsh;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Ljdz;->b()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 78
    return-void
.end method
