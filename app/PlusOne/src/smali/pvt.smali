.class public final Lpvt;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpvt;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lpgx;

.field public d:[Lpvi;

.field public e:Ljava/lang/String;

.field public f:Lpvs;

.field public g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/Long;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/String;

.field private n:Lpvn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x0

    new-array v0, v0, [Lpvt;

    sput-object v0, Lpvt;->a:[Lpvt;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 430
    invoke-direct {p0}, Loxq;-><init>()V

    .line 435
    iput-object v1, p0, Lpvt;->c:Lpgx;

    .line 440
    sget-object v0, Lpvi;->a:[Lpvi;

    iput-object v0, p0, Lpvt;->d:[Lpvi;

    .line 455
    iput-object v1, p0, Lpvt;->n:Lpvn;

    .line 458
    iput-object v1, p0, Lpvt;->f:Lpvs;

    .line 430
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 514
    .line 515
    iget-object v0, p0, Lpvt;->b:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 516
    const/4 v0, 0x1

    iget-object v2, p0, Lpvt;->b:Ljava/lang/String;

    .line 517
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 519
    :goto_0
    iget-object v2, p0, Lpvt;->c:Lpgx;

    if-eqz v2, :cond_0

    .line 520
    const/4 v2, 0x2

    iget-object v3, p0, Lpvt;->c:Lpgx;

    .line 521
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 523
    :cond_0
    iget-object v2, p0, Lpvt;->h:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 524
    const/4 v2, 0x3

    iget-object v3, p0, Lpvt;->h:Ljava/lang/String;

    .line 525
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 527
    :cond_1
    iget-object v2, p0, Lpvt;->d:[Lpvi;

    if-eqz v2, :cond_3

    .line 528
    iget-object v2, p0, Lpvt;->d:[Lpvi;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 529
    if-eqz v4, :cond_2

    .line 530
    const/4 v5, 0x4

    .line 531
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 528
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 535
    :cond_3
    iget-object v1, p0, Lpvt;->i:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 536
    const/4 v1, 0x5

    iget-object v2, p0, Lpvt;->i:Ljava/lang/Long;

    .line 537
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 539
    :cond_4
    iget-object v1, p0, Lpvt;->j:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 540
    const/4 v1, 0x6

    iget-object v2, p0, Lpvt;->j:Ljava/lang/String;

    .line 541
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 543
    :cond_5
    iget-object v1, p0, Lpvt;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 544
    const/4 v1, 0x7

    iget-object v2, p0, Lpvt;->k:Ljava/lang/Boolean;

    .line 545
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 547
    :cond_6
    iget-object v1, p0, Lpvt;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 548
    const/16 v1, 0x8

    iget-object v2, p0, Lpvt;->l:Ljava/lang/Boolean;

    .line 549
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 551
    :cond_7
    iget-object v1, p0, Lpvt;->m:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 552
    const/16 v1, 0x9

    iget-object v2, p0, Lpvt;->m:Ljava/lang/String;

    .line 553
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 555
    :cond_8
    iget-object v1, p0, Lpvt;->n:Lpvn;

    if-eqz v1, :cond_9

    .line 556
    const/16 v1, 0xa

    iget-object v2, p0, Lpvt;->n:Lpvn;

    .line 557
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 559
    :cond_9
    iget-object v1, p0, Lpvt;->f:Lpvs;

    if-eqz v1, :cond_a

    .line 560
    const/16 v1, 0xb

    iget-object v2, p0, Lpvt;->f:Lpvs;

    .line 561
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 563
    :cond_a
    iget-object v1, p0, Lpvt;->e:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 564
    const/16 v1, 0xc

    iget-object v2, p0, Lpvt;->e:Ljava/lang/String;

    .line 565
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_b
    iget-object v1, p0, Lpvt;->g:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 568
    const/16 v1, 0xd

    iget-object v2, p0, Lpvt;->g:Ljava/lang/String;

    .line 569
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_c
    iget-object v1, p0, Lpvt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 572
    iput v0, p0, Lpvt;->ai:I

    .line 573
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpvt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 581
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 582
    sparse-switch v0, :sswitch_data_0

    .line 586
    iget-object v2, p0, Lpvt;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 587
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpvt;->ah:Ljava/util/List;

    .line 590
    :cond_1
    iget-object v2, p0, Lpvt;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 592
    :sswitch_0
    return-object p0

    .line 597
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvt;->b:Ljava/lang/String;

    goto :goto_0

    .line 601
    :sswitch_2
    iget-object v0, p0, Lpvt;->c:Lpgx;

    if-nez v0, :cond_2

    .line 602
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lpvt;->c:Lpgx;

    .line 604
    :cond_2
    iget-object v0, p0, Lpvt;->c:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 608
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvt;->h:Ljava/lang/String;

    goto :goto_0

    .line 612
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 613
    iget-object v0, p0, Lpvt;->d:[Lpvi;

    if-nez v0, :cond_4

    move v0, v1

    .line 614
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpvi;

    .line 615
    iget-object v3, p0, Lpvt;->d:[Lpvi;

    if-eqz v3, :cond_3

    .line 616
    iget-object v3, p0, Lpvt;->d:[Lpvi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 618
    :cond_3
    iput-object v2, p0, Lpvt;->d:[Lpvi;

    .line 619
    :goto_2
    iget-object v2, p0, Lpvt;->d:[Lpvi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 620
    iget-object v2, p0, Lpvt;->d:[Lpvi;

    new-instance v3, Lpvi;

    invoke-direct {v3}, Lpvi;-><init>()V

    aput-object v3, v2, v0

    .line 621
    iget-object v2, p0, Lpvt;->d:[Lpvi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 622
    invoke-virtual {p1}, Loxn;->a()I

    .line 619
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 613
    :cond_4
    iget-object v0, p0, Lpvt;->d:[Lpvi;

    array-length v0, v0

    goto :goto_1

    .line 625
    :cond_5
    iget-object v2, p0, Lpvt;->d:[Lpvi;

    new-instance v3, Lpvi;

    invoke-direct {v3}, Lpvi;-><init>()V

    aput-object v3, v2, v0

    .line 626
    iget-object v2, p0, Lpvt;->d:[Lpvi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 630
    :sswitch_5
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpvt;->i:Ljava/lang/Long;

    goto/16 :goto_0

    .line 634
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvt;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 638
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpvt;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 642
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpvt;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 646
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvt;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 650
    :sswitch_a
    iget-object v0, p0, Lpvt;->n:Lpvn;

    if-nez v0, :cond_6

    .line 651
    new-instance v0, Lpvn;

    invoke-direct {v0}, Lpvn;-><init>()V

    iput-object v0, p0, Lpvt;->n:Lpvn;

    .line 653
    :cond_6
    iget-object v0, p0, Lpvt;->n:Lpvn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 657
    :sswitch_b
    iget-object v0, p0, Lpvt;->f:Lpvs;

    if-nez v0, :cond_7

    .line 658
    new-instance v0, Lpvs;

    invoke-direct {v0}, Lpvs;-><init>()V

    iput-object v0, p0, Lpvt;->f:Lpvs;

    .line 660
    :cond_7
    iget-object v0, p0, Lpvt;->f:Lpvs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 664
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvt;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 668
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvt;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 582
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 465
    iget-object v0, p0, Lpvt;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 466
    const/4 v0, 0x1

    iget-object v1, p0, Lpvt;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 468
    :cond_0
    iget-object v0, p0, Lpvt;->c:Lpgx;

    if-eqz v0, :cond_1

    .line 469
    const/4 v0, 0x2

    iget-object v1, p0, Lpvt;->c:Lpgx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 471
    :cond_1
    iget-object v0, p0, Lpvt;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 472
    const/4 v0, 0x3

    iget-object v1, p0, Lpvt;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 474
    :cond_2
    iget-object v0, p0, Lpvt;->d:[Lpvi;

    if-eqz v0, :cond_4

    .line 475
    iget-object v1, p0, Lpvt;->d:[Lpvi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 476
    if-eqz v3, :cond_3

    .line 477
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 475
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 481
    :cond_4
    iget-object v0, p0, Lpvt;->i:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 482
    const/4 v0, 0x5

    iget-object v1, p0, Lpvt;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 484
    :cond_5
    iget-object v0, p0, Lpvt;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 485
    const/4 v0, 0x6

    iget-object v1, p0, Lpvt;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 487
    :cond_6
    iget-object v0, p0, Lpvt;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 488
    const/4 v0, 0x7

    iget-object v1, p0, Lpvt;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 490
    :cond_7
    iget-object v0, p0, Lpvt;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 491
    const/16 v0, 0x8

    iget-object v1, p0, Lpvt;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 493
    :cond_8
    iget-object v0, p0, Lpvt;->m:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 494
    const/16 v0, 0x9

    iget-object v1, p0, Lpvt;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 496
    :cond_9
    iget-object v0, p0, Lpvt;->n:Lpvn;

    if-eqz v0, :cond_a

    .line 497
    const/16 v0, 0xa

    iget-object v1, p0, Lpvt;->n:Lpvn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 499
    :cond_a
    iget-object v0, p0, Lpvt;->f:Lpvs;

    if-eqz v0, :cond_b

    .line 500
    const/16 v0, 0xb

    iget-object v1, p0, Lpvt;->f:Lpvs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 502
    :cond_b
    iget-object v0, p0, Lpvt;->e:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 503
    const/16 v0, 0xc

    iget-object v1, p0, Lpvt;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 505
    :cond_c
    iget-object v0, p0, Lpvt;->g:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 506
    const/16 v0, 0xd

    iget-object v1, p0, Lpvt;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 508
    :cond_d
    iget-object v0, p0, Lpvt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 510
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 426
    invoke-virtual {p0, p1}, Lpvt;->a(Loxn;)Lpvt;

    move-result-object v0

    return-object v0
.end method
