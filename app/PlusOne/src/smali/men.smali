.class public final Lmen;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lokb;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11338
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11341
    iput-object v0, p0, Lmen;->apiHeader:Llyr;

    .line 11344
    iput-object v0, p0, Lmen;->a:Lokb;

    .line 11338
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11361
    const/4 v0, 0x0

    .line 11362
    iget-object v1, p0, Lmen;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 11363
    const/4 v0, 0x1

    iget-object v1, p0, Lmen;->apiHeader:Llyr;

    .line 11364
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11366
    :cond_0
    iget-object v1, p0, Lmen;->a:Lokb;

    if-eqz v1, :cond_1

    .line 11367
    const/4 v1, 0x2

    iget-object v2, p0, Lmen;->a:Lokb;

    .line 11368
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11370
    :cond_1
    iget-object v1, p0, Lmen;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11371
    iput v0, p0, Lmen;->ai:I

    .line 11372
    return v0
.end method

.method public a(Loxn;)Lmen;
    .locals 2

    .prologue
    .line 11380
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11381
    sparse-switch v0, :sswitch_data_0

    .line 11385
    iget-object v1, p0, Lmen;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11386
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmen;->ah:Ljava/util/List;

    .line 11389
    :cond_1
    iget-object v1, p0, Lmen;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11391
    :sswitch_0
    return-object p0

    .line 11396
    :sswitch_1
    iget-object v0, p0, Lmen;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 11397
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmen;->apiHeader:Llyr;

    .line 11399
    :cond_2
    iget-object v0, p0, Lmen;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11403
    :sswitch_2
    iget-object v0, p0, Lmen;->a:Lokb;

    if-nez v0, :cond_3

    .line 11404
    new-instance v0, Lokb;

    invoke-direct {v0}, Lokb;-><init>()V

    iput-object v0, p0, Lmen;->a:Lokb;

    .line 11406
    :cond_3
    iget-object v0, p0, Lmen;->a:Lokb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11381
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11349
    iget-object v0, p0, Lmen;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 11350
    const/4 v0, 0x1

    iget-object v1, p0, Lmen;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11352
    :cond_0
    iget-object v0, p0, Lmen;->a:Lokb;

    if-eqz v0, :cond_1

    .line 11353
    const/4 v0, 0x2

    iget-object v1, p0, Lmen;->a:Lokb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11355
    :cond_1
    iget-object v0, p0, Lmen;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11357
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11334
    invoke-virtual {p0, p1}, Lmen;->a(Loxn;)Lmen;

    move-result-object v0

    return-object v0
.end method
