.class public final Lfxo;
.super Lfxu;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static f:Landroid/graphics/drawable/Drawable;

.field private static g:Landroid/graphics/drawable/Drawable;

.field private static h:Landroid/graphics/drawable/Drawable;

.field private static i:Landroid/graphics/drawable/Drawable;

.field private static j:Landroid/graphics/drawable/Drawable;


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/ImageView;

.field private c:Z

.field private d:Lfxf;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lfxu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lfxo;->c:Z

    if-eqz v0, :cond_2

    .line 127
    if-nez p1, :cond_1

    .line 128
    iget-object v0, p0, Lfxo;->d:Lfxf;

    invoke-interface {v0}, Lfxf;->e()V

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 130
    iget-object v0, p0, Lfxo;->d:Lfxf;

    invoke-interface {v0}, Lfxf;->U()V

    goto :goto_0

    .line 133
    :cond_2
    iget-object v0, p0, Lfxo;->d:Lfxf;

    invoke-interface {v0}, Lfxf;->V()V

    goto :goto_0
.end method

.method public a(ILpao;Loyy;Lfxf;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 76
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfxo;->c:Z

    .line 77
    iput-object p4, p0, Lfxo;->d:Lfxf;

    .line 79
    packed-switch p1, :pswitch_data_0

    .line 119
    :goto_1
    return-void

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :pswitch_0
    iget-object v0, p0, Lfxo;->a:Landroid/widget/ImageView;

    sget-object v2, Lfxo;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 82
    iget-object v0, p0, Lfxo;->b:Landroid/widget/ImageView;

    sget-object v2, Lfxo;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 87
    iget-object v0, p2, Lpao;->d:Loya;

    if-eqz v0, :cond_1

    .line 88
    iget-object v1, p2, Lpao;->c:Ljava/lang/String;

    .line 89
    iget-object v0, p2, Lpao;->d:Loya;

    sget-object v2, Lpcg;->a:Loxr;

    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpcg;

    iget-object v0, v0, Lpcg;->c:Ljava/lang/String;

    .line 94
    :goto_2
    iget-object v2, p0, Lfxo;->a:Landroid/widget/ImageView;

    iget-object v3, p0, Lfxo;->b:Landroid/widget/ImageView;

    invoke-super {p0, v1, v0, v2, v3}, Lfxu;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/view/View;)V

    goto :goto_1

    .line 91
    :cond_1
    iget-object v0, p2, Lpao;->b:Ljava/lang/String;

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_2

    .line 100
    :pswitch_1
    invoke-virtual {p0}, Lfxo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 102
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 103
    iget-object v0, p0, Lfxo;->a:Landroid/widget/ImageView;

    sget-object v2, Lfxo;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 104
    const v0, 0x7f0a073e

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    :goto_3
    const v2, 0x7f0a0740

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 112
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p3, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 113
    iget-object v1, p0, Lfxo;->b:Landroid/widget/ImageView;

    sget-object v2, Lfxo;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    iget-object v2, p0, Lfxo;->b:Landroid/widget/ImageView;

    .line 115
    const v1, 0x7f0a0741

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 118
    :goto_4
    iget-object v3, p0, Lfxo;->a:Landroid/widget/ImageView;

    invoke-super {p0, v0, v1, v3, v2}, Lfxu;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/view/View;)V

    goto :goto_1

    .line 106
    :cond_2
    iget-object v0, p0, Lfxo;->a:Landroid/widget/ImageView;

    sget-object v2, Lfxo;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 107
    const v0, 0x7f0a073f

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_3
    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    goto :goto_4

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 56
    invoke-super {p0, p1, p2, p3}, Lfxu;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    iget-boolean v0, p0, Lfxo;->e:Z

    if-nez v0, :cond_0

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 60
    const v1, 0x7f0203c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfxo;->f:Landroid/graphics/drawable/Drawable;

    .line 61
    const v1, 0x7f0203c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfxo;->g:Landroid/graphics/drawable/Drawable;

    .line 62
    const v1, 0x7f0203c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfxo;->h:Landroid/graphics/drawable/Drawable;

    .line 63
    const v1, 0x7f02026b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfxo;->i:Landroid/graphics/drawable/Drawable;

    .line 64
    const v1, 0x7f0203c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfxo;->j:Landroid/graphics/drawable/Drawable;

    .line 65
    iput-boolean v2, p0, Lfxo;->e:Z

    .line 68
    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxo;->a:Landroid/widget/ImageView;

    .line 69
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxo;->b:Landroid/widget/ImageView;

    .line 70
    invoke-virtual {p0, v2, v2}, Lfxo;->a(ZZ)V

    .line 71
    return-void
.end method
