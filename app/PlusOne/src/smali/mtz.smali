.class public final Lmtz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Ljava/lang/Integer;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 4434
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4458
    iput v0, p0, Lmtz;->b:I

    .line 4467
    iput v0, p0, Lmtz;->d:I

    .line 4474
    iput v0, p0, Lmtz;->i:I

    .line 4434
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 4522
    const/4 v0, 0x0

    .line 4523
    iget-object v1, p0, Lmtz;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4524
    const/4 v0, 0x1

    iget-object v1, p0, Lmtz;->a:Ljava/lang/String;

    .line 4525
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4527
    :cond_0
    iget v1, p0, Lmtz;->b:I

    if-eq v1, v4, :cond_1

    .line 4528
    const/4 v1, 0x2

    iget v2, p0, Lmtz;->b:I

    .line 4529
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4531
    :cond_1
    iget-object v1, p0, Lmtz;->f:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 4532
    const/4 v1, 0x3

    iget-object v2, p0, Lmtz;->f:Ljava/lang/Long;

    .line 4533
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4535
    :cond_2
    iget-object v1, p0, Lmtz;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4536
    const/4 v1, 0x4

    iget-object v2, p0, Lmtz;->g:Ljava/lang/String;

    .line 4537
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4539
    :cond_3
    iget-object v1, p0, Lmtz;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 4540
    const/4 v1, 0x5

    iget-object v2, p0, Lmtz;->c:Ljava/lang/String;

    .line 4541
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4543
    :cond_4
    iget v1, p0, Lmtz;->d:I

    if-eq v1, v4, :cond_5

    .line 4544
    const/4 v1, 0x6

    iget v2, p0, Lmtz;->d:I

    .line 4545
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4547
    :cond_5
    iget-object v1, p0, Lmtz;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 4548
    const/4 v1, 0x7

    iget-object v2, p0, Lmtz;->h:Ljava/lang/String;

    .line 4549
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4551
    :cond_6
    iget-object v1, p0, Lmtz;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 4552
    const/16 v1, 0x8

    iget-object v2, p0, Lmtz;->e:Ljava/lang/Integer;

    .line 4553
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4555
    :cond_7
    iget v1, p0, Lmtz;->i:I

    if-eq v1, v4, :cond_8

    .line 4556
    const/16 v1, 0x9

    iget v2, p0, Lmtz;->i:I

    .line 4557
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4559
    :cond_8
    iget-object v1, p0, Lmtz;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 4560
    const/16 v1, 0xa

    iget-object v2, p0, Lmtz;->j:Ljava/lang/Integer;

    .line 4561
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4563
    :cond_9
    iget-object v1, p0, Lmtz;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 4564
    const/16 v1, 0xb

    iget-object v2, p0, Lmtz;->k:Ljava/lang/Integer;

    .line 4565
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4567
    :cond_a
    iget-object v1, p0, Lmtz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4568
    iput v0, p0, Lmtz;->ai:I

    .line 4569
    return v0
.end method

.method public a(Loxn;)Lmtz;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4577
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4578
    sparse-switch v0, :sswitch_data_0

    .line 4582
    iget-object v1, p0, Lmtz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4583
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtz;->ah:Ljava/util/List;

    .line 4586
    :cond_1
    iget-object v1, p0, Lmtz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4588
    :sswitch_0
    return-object p0

    .line 4593
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtz;->a:Ljava/lang/String;

    goto :goto_0

    .line 4597
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4598
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 4606
    :cond_2
    iput v0, p0, Lmtz;->b:I

    goto :goto_0

    .line 4608
    :cond_3
    iput v2, p0, Lmtz;->b:I

    goto :goto_0

    .line 4613
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmtz;->f:Ljava/lang/Long;

    goto :goto_0

    .line 4617
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtz;->g:Ljava/lang/String;

    goto :goto_0

    .line 4621
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtz;->c:Ljava/lang/String;

    goto :goto_0

    .line 4625
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4626
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-ne v0, v6, :cond_5

    .line 4631
    :cond_4
    iput v0, p0, Lmtz;->d:I

    goto :goto_0

    .line 4633
    :cond_5
    iput v2, p0, Lmtz;->d:I

    goto :goto_0

    .line 4638
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtz;->h:Ljava/lang/String;

    goto :goto_0

    .line 4642
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtz;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 4646
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4647
    if-nez v0, :cond_6

    .line 4648
    iput v0, p0, Lmtz;->i:I

    goto/16 :goto_0

    .line 4650
    :cond_6
    iput v2, p0, Lmtz;->i:I

    goto/16 :goto_0

    .line 4655
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtz;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 4659
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtz;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 4578
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 4483
    iget-object v0, p0, Lmtz;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4484
    const/4 v0, 0x1

    iget-object v1, p0, Lmtz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4486
    :cond_0
    iget v0, p0, Lmtz;->b:I

    if-eq v0, v4, :cond_1

    .line 4487
    const/4 v0, 0x2

    iget v1, p0, Lmtz;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4489
    :cond_1
    iget-object v0, p0, Lmtz;->f:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 4490
    const/4 v0, 0x3

    iget-object v1, p0, Lmtz;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 4492
    :cond_2
    iget-object v0, p0, Lmtz;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4493
    const/4 v0, 0x4

    iget-object v1, p0, Lmtz;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4495
    :cond_3
    iget-object v0, p0, Lmtz;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 4496
    const/4 v0, 0x5

    iget-object v1, p0, Lmtz;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4498
    :cond_4
    iget v0, p0, Lmtz;->d:I

    if-eq v0, v4, :cond_5

    .line 4499
    const/4 v0, 0x6

    iget v1, p0, Lmtz;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4501
    :cond_5
    iget-object v0, p0, Lmtz;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 4502
    const/4 v0, 0x7

    iget-object v1, p0, Lmtz;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4504
    :cond_6
    iget-object v0, p0, Lmtz;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 4505
    const/16 v0, 0x8

    iget-object v1, p0, Lmtz;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4507
    :cond_7
    iget v0, p0, Lmtz;->i:I

    if-eq v0, v4, :cond_8

    .line 4508
    const/16 v0, 0x9

    iget v1, p0, Lmtz;->i:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4510
    :cond_8
    iget-object v0, p0, Lmtz;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 4511
    const/16 v0, 0xa

    iget-object v1, p0, Lmtz;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4513
    :cond_9
    iget-object v0, p0, Lmtz;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 4514
    const/16 v0, 0xb

    iget-object v1, p0, Lmtz;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4516
    :cond_a
    iget-object v0, p0, Lmtz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4518
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4430
    invoke-virtual {p0, p1}, Lmtz;->a(Loxn;)Lmtz;

    move-result-object v0

    return-object v0
.end method
