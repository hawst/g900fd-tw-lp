.class public final Lcqa;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private a:Lcqc;

.field private b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcqd;",
            "Lcqe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lcqc;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 49
    iput-object p2, p0, Lcqa;->a:Lcqc;

    .line 50
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcqd;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcqa;->b:Ljava/util/EnumMap;

    .line 51
    return-void
.end method

.method static synthetic a(Lcqa;)Lcqc;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcqa;->a:Lcqc;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-virtual {p0}, Lcqa;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 106
    iput-object v1, p0, Lcqa;->a:Lcqc;

    .line 107
    iput-object v1, p0, Lcqa;->b:Ljava/util/EnumMap;

    .line 108
    return-void
.end method

.method public a(Lcpu;)V
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcqa;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 65
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 66
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 68
    invoke-virtual {p0, v0}, Lcqa;->sendMessage(Landroid/os/Message;)Z

    .line 69
    return-void
.end method

.method public a(Lcqd;Lcqe;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcqa;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 73
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 75
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcpu;

    iget-object v1, p0, Lcqa;->a:Lcqc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcqa;->b:Ljava/util/EnumMap;

    if-eqz v1, :cond_0

    new-instance v3, Ljava/util/EnumMap;

    const-class v1, Lcqd;

    invoke-direct {v3, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iget-object v1, p0, Lcqa;->b:Ljava/util/EnumMap;

    invoke-virtual {v1}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Enum;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcqe;

    invoke-interface {v1, v0}, Lcqe;->a(Lcpu;)Lcpx;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    new-instance v0, Lcqb;

    invoke-direct {v0, p0, v3}, Lcqb;-><init>(Lcqa;Ljava/util/EnumMap;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
