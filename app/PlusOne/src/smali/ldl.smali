.class public final Lldl;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lljg;

.field private d:Landroid/graphics/Path;

.field private e:Landroid/graphics/Paint;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lldl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lldl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    sget-object v0, Lldl;->a:Llct;

    if-nez v0, :cond_0

    .line 50
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lldl;->a:Llct;

    .line 52
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lldl;->setClickable(Z)V

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lldl;->e:Landroid/graphics/Paint;

    .line 55
    iget-object v0, p0, Lldl;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lldl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b011c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    iget-object v0, p0, Lldl;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 57
    iget-object v0, p0, Lldl;->e:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 58
    iget-object v0, p0, Lldl;->e:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 59
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lldl;->d:Landroid/graphics/Path;

    .line 60
    return-void
.end method


# virtual methods
.method public a(ZLjava/lang/String;I)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-virtual {p0}, Lldl;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 64
    invoke-virtual {p0}, Lldl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lldl;->b:Ljava/lang/String;

    .line 69
    if-eqz p1, :cond_1

    const v0, 0x7f0a0499

    .line 72
    :goto_0
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, ">>NAME_MARKER<<"

    aput-object v5, v4, v1

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 73
    const-string v3, ">>NAME_MARKER<<"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 74
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v4

    .line 75
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 76
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    add-int/lit8 v0, v3, 0xf

    invoke-virtual {v4, v3, v0, p2}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lldl;->b:Ljava/lang/String;

    .line 82
    :cond_0
    iget-object v0, p0, Lldl;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 83
    const/16 v0, 0x24

    invoke-static {v2, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    .line 85
    sget-object v1, Lldl;->a:Llct;

    iget v1, v1, Llct;->ag:I

    invoke-static {v0}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v0

    mul-int/2addr v0, v1

    .line 88
    :goto_1
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 89
    add-int/2addr v0, p3

    sget-object v1, Lldl;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    return v0

    .line 69
    :cond_1
    const v0, 0x7f0a0498

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 94
    invoke-virtual {p0, v0}, Lldl;->setClickable(Z)V

    .line 96
    iput-object v1, p0, Lldl;->b:Ljava/lang/String;

    .line 97
    iput-object v1, p0, Lldl;->c:Lljg;

    .line 99
    iput v0, p0, Lldl;->f:I

    .line 100
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 142
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 144
    invoke-virtual {p0}, Lldl;->getWidth()I

    move-result v0

    .line 145
    invoke-virtual {p0}, Lldl;->getHeight()I

    move-result v1

    .line 147
    sget-object v2, Lldl;->a:Llct;

    iget v2, v2, Llct;->m:I

    sub-int/2addr v1, v2

    .line 152
    sget-object v2, Lldl;->a:Llct;

    iget v2, v2, Llct;->m:I

    .line 153
    sget-object v3, Lldl;->a:Llct;

    iget-object v3, v3, Llct;->am:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    .line 154
    invoke-virtual {p0}, Lldl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b011c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 153
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    sget-object v3, Lldl;->a:Llct;

    iget-object v3, v3, Llct;->am:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v3, v6, v6, v0, v1}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 156
    sget-object v3, Lldl;->a:Llct;

    iget-object v3, v3, Llct;->am:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 158
    iget-object v3, p0, Lldl;->c:Lljg;

    if-eqz v3, :cond_0

    .line 159
    iget-object v3, p0, Lldl;->c:Lljg;

    invoke-virtual {v3}, Lljg;->getHeight()I

    move-result v3

    .line 160
    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x0

    .line 161
    int-to-float v3, v2

    int-to-float v4, v1

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 162
    iget-object v3, p0, Lldl;->c:Lljg;

    invoke-virtual {v3, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    .line 163
    neg-int v3, v2

    int-to-float v3, v3

    neg-int v4, v1

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 165
    iget-object v3, p0, Lldl;->c:Lljg;

    invoke-virtual {v3, v2, v1}, Lljg;->a(II)V

    .line 168
    :cond_0
    const/4 v1, 0x0

    iget v2, p0, Lldl;->f:I

    int-to-float v2, v2

    int-to-float v3, v0

    iget v0, p0, Lldl;->f:I

    int-to-float v4, v0

    sget-object v0, Lldl;->a:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 171
    iget-object v0, p0, Lldl;->d:Landroid/graphics/Path;

    iget-object v1, p0, Lldl;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 172
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 104
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 107
    sget-object v1, Lldl;->a:Llct;

    iget v1, v1, Llct;->m:I

    sub-int v1, v11, v1

    sget-object v2, Lldl;->a:Llct;

    iget v2, v2, Llct;->m:I

    sub-int v2, v1, v2

    .line 108
    invoke-virtual {p0}, Lldl;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v3, 0x24

    invoke-static {v1, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v8

    .line 110
    iget-object v1, p0, Lldl;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    invoke-static {v8}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v12

    .line 112
    iget-object v7, p0, Lldl;->b:Ljava/lang/String;

    const/4 v9, 0x1

    sget-object v1, Lldl;->a:Llct;

    iget v10, v1, Llct;->ag:I

    move v1, v0

    move v3, v0

    move-object v5, v4

    move v6, v0

    invoke-static/range {v0 .. v10}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/text/TextPaint;ZI)Lljg;

    move-result-object v0

    iput-object v0, p0, Lldl;->c:Lljg;

    .line 116
    sget-object v0, Lldl;->a:Llct;

    iget v0, v0, Llct;->ag:I

    mul-int/2addr v0, v12

    .line 119
    :cond_0
    sget-object v1, Lldl;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    .line 120
    sget-object v1, Lldl;->a:Llct;

    iget v1, v1, Llct;->m:I

    sub-int v1, v0, v1

    iput v1, p0, Lldl;->f:I

    .line 125
    sget-object v1, Lldl;->a:Llct;

    iget v1, v1, Llct;->m:I

    sub-int v1, v11, v1

    sget-object v2, Lldl;->a:Llct;

    iget v2, v2, Llct;->m:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 126
    sget-object v2, Lldl;->a:Llct;

    iget v2, v2, Llct;->be:I

    sub-int v2, v1, v2

    .line 127
    sub-int v3, v1, v2

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v2

    .line 128
    iget v4, p0, Lldl;->f:I

    sget-object v5, Lldl;->a:Llct;

    iget-object v5, v5, Llct;->w:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v4, v5

    .line 129
    sget-object v5, Lldl;->a:Llct;

    iget v5, v5, Llct;->bf:I

    add-int/2addr v5, v4

    .line 131
    iget-object v6, p0, Lldl;->d:Landroid/graphics/Path;

    sget-object v7, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v6, v7}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 132
    iget-object v6, p0, Lldl;->d:Landroid/graphics/Path;

    int-to-float v7, v2

    int-to-float v8, v4

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 133
    iget-object v6, p0, Lldl;->d:Landroid/graphics/Path;

    int-to-float v1, v1

    int-to-float v7, v4

    invoke-virtual {v6, v1, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 134
    iget-object v1, p0, Lldl;->d:Landroid/graphics/Path;

    int-to-float v3, v3

    int-to-float v5, v5

    invoke-virtual {v1, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 135
    iget-object v1, p0, Lldl;->d:Landroid/graphics/Path;

    int-to-float v2, v2

    int-to-float v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 136
    iget-object v1, p0, Lldl;->d:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 137
    invoke-virtual {p0, v11, v0}, Lldl;->setMeasuredDimension(II)V

    .line 138
    return-void
.end method
