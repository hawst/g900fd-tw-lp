.class public final Lmkz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lncn;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25756
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25759
    iput-object v0, p0, Lmkz;->apiHeader:Llyr;

    .line 25762
    iput-object v0, p0, Lmkz;->a:Lncn;

    .line 25756
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 25779
    const/4 v0, 0x0

    .line 25780
    iget-object v1, p0, Lmkz;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 25781
    const/4 v0, 0x1

    iget-object v1, p0, Lmkz;->apiHeader:Llyr;

    .line 25782
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25784
    :cond_0
    iget-object v1, p0, Lmkz;->a:Lncn;

    if-eqz v1, :cond_1

    .line 25785
    const/4 v1, 0x2

    iget-object v2, p0, Lmkz;->a:Lncn;

    .line 25786
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25788
    :cond_1
    iget-object v1, p0, Lmkz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25789
    iput v0, p0, Lmkz;->ai:I

    .line 25790
    return v0
.end method

.method public a(Loxn;)Lmkz;
    .locals 2

    .prologue
    .line 25798
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 25799
    sparse-switch v0, :sswitch_data_0

    .line 25803
    iget-object v1, p0, Lmkz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 25804
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkz;->ah:Ljava/util/List;

    .line 25807
    :cond_1
    iget-object v1, p0, Lmkz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25809
    :sswitch_0
    return-object p0

    .line 25814
    :sswitch_1
    iget-object v0, p0, Lmkz;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 25815
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkz;->apiHeader:Llyr;

    .line 25817
    :cond_2
    iget-object v0, p0, Lmkz;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25821
    :sswitch_2
    iget-object v0, p0, Lmkz;->a:Lncn;

    if-nez v0, :cond_3

    .line 25822
    new-instance v0, Lncn;

    invoke-direct {v0}, Lncn;-><init>()V

    iput-object v0, p0, Lmkz;->a:Lncn;

    .line 25824
    :cond_3
    iget-object v0, p0, Lmkz;->a:Lncn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25799
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25767
    iget-object v0, p0, Lmkz;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 25768
    const/4 v0, 0x1

    iget-object v1, p0, Lmkz;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25770
    :cond_0
    iget-object v0, p0, Lmkz;->a:Lncn;

    if-eqz v0, :cond_1

    .line 25771
    const/4 v0, 0x2

    iget-object v1, p0, Lmkz;->a:Lncn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25773
    :cond_1
    iget-object v0, p0, Lmkz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 25775
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 25752
    invoke-virtual {p0, p1}, Lmkz;->a(Loxn;)Lmkz;

    move-result-object v0

    return-object v0
.end method
