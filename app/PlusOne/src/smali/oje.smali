.class public final Loje;
.super Loxq;
.source "PG"


# instance fields
.field public a:Loii;

.field private b:Ljava/lang/Boolean;

.field private c:Llwr;

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v0, p0, Loje;->a:Loii;

    .line 18
    iput-object v0, p0, Loje;->c:Llwr;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, Loje;->a:Loii;

    if-eqz v1, :cond_0

    .line 45
    const/4 v0, 0x1

    iget-object v1, p0, Loje;->a:Loii;

    .line 46
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 48
    :cond_0
    iget-object v1, p0, Loje;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 49
    const/4 v1, 0x2

    iget-object v2, p0, Loje;->b:Ljava/lang/Boolean;

    .line 50
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 52
    :cond_1
    iget-object v1, p0, Loje;->c:Llwr;

    if-eqz v1, :cond_2

    .line 53
    const/4 v1, 0x3

    iget-object v2, p0, Loje;->c:Llwr;

    .line 54
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_2
    iget-object v1, p0, Loje;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 57
    const/4 v1, 0x4

    iget-object v2, p0, Loje;->d:Ljava/lang/Boolean;

    .line 58
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 60
    :cond_3
    iget-object v1, p0, Loje;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    iput v0, p0, Loje;->ai:I

    .line 62
    return v0
.end method

.method public a(Loxn;)Loje;
    .locals 2

    .prologue
    .line 70
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 71
    sparse-switch v0, :sswitch_data_0

    .line 75
    iget-object v1, p0, Loje;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 76
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loje;->ah:Ljava/util/List;

    .line 79
    :cond_1
    iget-object v1, p0, Loje;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    :sswitch_0
    return-object p0

    .line 86
    :sswitch_1
    iget-object v0, p0, Loje;->a:Loii;

    if-nez v0, :cond_2

    .line 87
    new-instance v0, Loii;

    invoke-direct {v0}, Loii;-><init>()V

    iput-object v0, p0, Loje;->a:Loii;

    .line 89
    :cond_2
    iget-object v0, p0, Loje;->a:Loii;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 93
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loje;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 97
    :sswitch_3
    iget-object v0, p0, Loje;->c:Llwr;

    if-nez v0, :cond_3

    .line 98
    new-instance v0, Llwr;

    invoke-direct {v0}, Llwr;-><init>()V

    iput-object v0, p0, Loje;->c:Llwr;

    .line 100
    :cond_3
    iget-object v0, p0, Loje;->c:Llwr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 104
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loje;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Loje;->a:Loii;

    if-eqz v0, :cond_0

    .line 26
    const/4 v0, 0x1

    iget-object v1, p0, Loje;->a:Loii;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28
    :cond_0
    iget-object v0, p0, Loje;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 29
    const/4 v0, 0x2

    iget-object v1, p0, Loje;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 31
    :cond_1
    iget-object v0, p0, Loje;->c:Llwr;

    if-eqz v0, :cond_2

    .line 32
    const/4 v0, 0x3

    iget-object v1, p0, Loje;->c:Llwr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 34
    :cond_2
    iget-object v0, p0, Loje;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 35
    const/4 v0, 0x4

    iget-object v1, p0, Loje;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 37
    :cond_3
    iget-object v0, p0, Loje;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 39
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loje;->a(Loxn;)Loje;

    move-result-object v0

    return-object v0
.end method
