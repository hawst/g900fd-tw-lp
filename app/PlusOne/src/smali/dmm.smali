.class public final Ldmm;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmhs;",
        "Lmht;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Z

.field private q:Lhgw;

.field private r:Lhgw;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 36
    const-string v3, "collectionreadaudience"

    new-instance v4, Lmhs;

    invoke-direct {v4}, Lmhs;-><init>()V

    new-instance v5, Lmht;

    invoke-direct {v5}, Lmht;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 39
    iput-object p3, p0, Ldmm;->a:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Ldmm;->b:Ljava/lang/String;

    .line 41
    iput-boolean p5, p0, Ldmm;->p:Z

    .line 42
    return-void
.end method


# virtual methods
.method protected a(Lmhs;)V
    .locals 3

    .prologue
    .line 46
    new-instance v0, Lmzl;

    invoke-direct {v0}, Lmzl;-><init>()V

    iput-object v0, p1, Lmhs;->a:Lmzl;

    .line 47
    iget-object v0, p1, Lmhs;->a:Lmzl;

    .line 48
    iget-object v1, p0, Ldmm;->a:Ljava/lang/String;

    iget-object v2, p0, Ldmm;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;)Lmzo;

    move-result-object v1

    iput-object v1, v0, Lmzl;->a:Lmzo;

    .line 51
    iget-boolean v1, p0, Ldmm;->p:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmzl;->b:Ljava/lang/Boolean;

    .line 52
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmzl;->c:Ljava/lang/Boolean;

    .line 54
    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lmzl;->d:Ljava/lang/Integer;

    .line 55
    return-void
.end method

.method protected a(Lmht;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 59
    iget-object v0, p1, Lmht;->a:Lmzm;

    .line 60
    iget-boolean v1, p0, Ldmm;->p:Z

    if-eqz v1, :cond_0

    .line 61
    iget-object v1, p0, Ldmm;->f:Landroid/content/Context;

    invoke-virtual {p0}, Ldmm;->k()I

    move-result v2

    iget-object v4, v0, Lmzm;->a:Locf;

    invoke-static {v1, v2, v4, v6, v6}, Lhft;->a(Landroid/content/Context;ILocf;ZZ)Lhgw;

    move-result-object v1

    iput-object v1, p0, Ldmm;->q:Lhgw;

    .line 64
    :cond_0
    iget-object v7, v0, Lmzm;->c:[Lnyz;

    iget-object v0, v0, Lmzm;->b:Ljava/lang/Integer;

    .line 65
    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v8

    .line 64
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    if-eqz v7, :cond_2

    move v6, v5

    :goto_0
    array-length v0, v7

    if-ge v6, v0, :cond_2

    aget-object v2, v7, v6

    iget-object v0, v2, Lnyz;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v2, Lnyz;->e:Ljava/lang/String;

    invoke-static {v0}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljqs;

    iget-object v1, v2, Lnyz;->c:Ljava/lang/String;

    iget-object v2, v2, Lnyz;->d:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_2
    new-instance v0, Lhgw;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v0, v9, v3, v1}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;I)V

    iput-object v0, p0, Ldmm;->r:Lhgw;

    .line 66
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lmhs;

    invoke-virtual {p0, p1}, Ldmm;->a(Lmhs;)V

    return-void
.end method

.method public b()Lhgw;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Ldmm;->q:Lhgw;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lmht;

    invoke-virtual {p0, p1}, Ldmm;->a(Lmht;)V

    return-void
.end method

.method public c()Lhgw;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Ldmm;->r:Lhgw;

    return-object v0
.end method
