.class public final Lpqf;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lpqg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 425
    invoke-direct {p0}, Loxq;-><init>()V

    .line 430
    sget-object v0, Lpqg;->a:[Lpqg;

    iput-object v0, p0, Lpqf;->b:[Lpqg;

    .line 425
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 451
    .line 452
    iget-object v0, p0, Lpqf;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 453
    const/4 v0, 0x1

    iget-object v2, p0, Lpqf;->a:Ljava/lang/String;

    .line 454
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 456
    :goto_0
    iget-object v2, p0, Lpqf;->b:[Lpqg;

    if-eqz v2, :cond_1

    .line 457
    iget-object v2, p0, Lpqf;->b:[Lpqg;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 458
    if-eqz v4, :cond_0

    .line 459
    const/4 v5, 0x2

    .line 460
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 457
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 464
    :cond_1
    iget-object v1, p0, Lpqf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 465
    iput v0, p0, Lpqf;->ai:I

    .line 466
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpqf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 474
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 475
    sparse-switch v0, :sswitch_data_0

    .line 479
    iget-object v2, p0, Lpqf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 480
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpqf;->ah:Ljava/util/List;

    .line 483
    :cond_1
    iget-object v2, p0, Lpqf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 485
    :sswitch_0
    return-object p0

    .line 490
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqf;->a:Ljava/lang/String;

    goto :goto_0

    .line 494
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 495
    iget-object v0, p0, Lpqf;->b:[Lpqg;

    if-nez v0, :cond_3

    move v0, v1

    .line 496
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpqg;

    .line 497
    iget-object v3, p0, Lpqf;->b:[Lpqg;

    if-eqz v3, :cond_2

    .line 498
    iget-object v3, p0, Lpqf;->b:[Lpqg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 500
    :cond_2
    iput-object v2, p0, Lpqf;->b:[Lpqg;

    .line 501
    :goto_2
    iget-object v2, p0, Lpqf;->b:[Lpqg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 502
    iget-object v2, p0, Lpqf;->b:[Lpqg;

    new-instance v3, Lpqg;

    invoke-direct {v3}, Lpqg;-><init>()V

    aput-object v3, v2, v0

    .line 503
    iget-object v2, p0, Lpqf;->b:[Lpqg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 504
    invoke-virtual {p1}, Loxn;->a()I

    .line 501
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 495
    :cond_3
    iget-object v0, p0, Lpqf;->b:[Lpqg;

    array-length v0, v0

    goto :goto_1

    .line 507
    :cond_4
    iget-object v2, p0, Lpqf;->b:[Lpqg;

    new-instance v3, Lpqg;

    invoke-direct {v3}, Lpqg;-><init>()V

    aput-object v3, v2, v0

    .line 508
    iget-object v2, p0, Lpqf;->b:[Lpqg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 475
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 435
    iget-object v0, p0, Lpqf;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 436
    const/4 v0, 0x1

    iget-object v1, p0, Lpqf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 438
    :cond_0
    iget-object v0, p0, Lpqf;->b:[Lpqg;

    if-eqz v0, :cond_2

    .line 439
    iget-object v1, p0, Lpqf;->b:[Lpqg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 440
    if-eqz v3, :cond_1

    .line 441
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 439
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 445
    :cond_2
    iget-object v0, p0, Lpqf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 447
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 421
    invoke-virtual {p0, p1}, Lpqf;->a(Loxn;)Lpqf;

    move-result-object v0

    return-object v0
.end method
