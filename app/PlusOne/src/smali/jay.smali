.class public final Ljay;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdd;


# instance fields
.field private a:Lcom/google/android/libraries/social/media/MediaResource;

.field private b:Z

.field private synthetic c:Lcom/google/android/libraries/social/media/ui/MediaView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkda;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 211
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 212
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c(Lcom/google/android/libraries/social/media/ui/MediaView;)V

    .line 214
    invoke-virtual {p1}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    .line 215
    instance-of v1, v0, Lirp;

    if-eqz v1, :cond_1

    .line 216
    iget-object v1, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    new-instance v2, Liro;

    check-cast v0, Lirp;

    invoke-static {}, Lcom/google/android/libraries/social/media/ui/MediaView;->v()Lizs;

    move-result-object v3

    invoke-virtual {v3}, Lizs;->b()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Liro;-><init>(Lirp;Landroid/graphics/Bitmap$Config;)V

    invoke-static {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lcom/google/android/libraries/social/media/ui/MediaView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 217
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(Lcom/google/android/libraries/social/media/ui/MediaView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Liro;

    invoke-virtual {v0, v4}, Liro;->b(Z)V

    .line 218
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(Lcom/google/android/libraries/social/media/ui/MediaView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Liro;

    iget-object v1, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->e(Lcom/google/android/libraries/social/media/ui/MediaView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Liro;->a(Z)V

    .line 219
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->f(Lcom/google/android/libraries/social/media/ui/MediaView;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 220
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(Lcom/google/android/libraries/social/media/ui/MediaView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Liro;

    iget-object v1, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->f(Lcom/google/android/libraries/social/media/ui/MediaView;)I

    move-result v1

    invoke-virtual {v0, v1}, Liro;->a(I)V

    .line 222
    :cond_0
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(Lcom/google/android/libraries/social/media/ui/MediaView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 223
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Lcom/google/android/libraries/social/media/ui/MediaView;Z)Z

    .line 224
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lcom/google/android/libraries/social/media/ui/MediaView;Z)Z

    .line 227
    :cond_1
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 228
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 182
    iput-boolean p1, p0, Ljay;->b:Z

    .line 183
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lcom/google/android/libraries/social/media/ui/MediaView;)I

    move-result v0

    .line 188
    iget-boolean v1, p0, Ljay;->b:Z

    if-eqz v1, :cond_0

    .line 191
    or-int/lit16 v0, v0, 0x80

    .line 193
    :cond_0
    iget-object v1, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v1, v0, p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lcom/google/android/libraries/social/media/ui/MediaView;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Ljay;->a:Lcom/google/android/libraries/social/media/MediaResource;

    .line 194
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Ljay;->a:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Ljay;->a:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Ljay;->a:Lcom/google/android/libraries/social/media/MediaResource;

    .line 202
    :cond_0
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Lcom/google/android/libraries/social/media/ui/MediaView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lcom/google/android/libraries/social/media/ui/MediaView;Z)Z

    .line 204
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->invalidate()V

    .line 206
    :cond_1
    iget-object v0, p0, Ljay;->c:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-static {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c(Lcom/google/android/libraries/social/media/ui/MediaView;)V

    .line 207
    return-void
.end method
