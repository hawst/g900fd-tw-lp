.class public final Ldnv;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmki;",
        "Lmkj;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Landroid/text/Spanned;

.field private p:Landroid/text/Spanned;

.field private q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/text/Spanned;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 34
    const-string v3, "translateupdatetext"

    new-instance v4, Lmki;

    invoke-direct {v4}, Lmki;-><init>()V

    new-instance v5, Lmkj;

    invoke-direct {v5}, Lmkj;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 36
    iput-object p3, p0, Ldnv;->a:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Lmki;)V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lnvv;

    invoke-direct {v0}, Lnvv;-><init>()V

    iput-object v0, p1, Lmki;->a:Lnvv;

    .line 56
    iget-object v0, p1, Lmki;->a:Lnvv;

    iget-object v1, p0, Ldnv;->a:Ljava/lang/String;

    iput-object v1, v0, Lnvv;->a:Ljava/lang/String;

    .line 57
    iget-object v0, p1, Lmki;->a:Lnvv;

    const/4 v1, 0x2

    iput v1, v0, Lnvv;->b:I

    .line 58
    return-void
.end method

.method protected a(Lmkj;)V
    .locals 6

    .prologue
    .line 62
    iget-object v0, p1, Lmkj;->a:Lnwk;

    .line 63
    iget-object v1, v0, Lnwk;->a:Lnwl;

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, v0, Lnwk;->a:Lnwl;

    iget-object v1, v1, Lnwl;->c:Lpeo;

    invoke-static {v1}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    iput-object v1, p0, Ldnv;->b:Landroid/text/Spanned;

    .line 67
    :cond_0
    iget-object v1, v0, Lnwk;->b:Lnwl;

    if-eqz v1, :cond_1

    .line 68
    iget-object v1, v0, Lnwk;->b:Lnwl;

    iget-object v1, v1, Lnwl;->c:Lpeo;

    invoke-static {v1}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    iput-object v1, p0, Ldnv;->p:Landroid/text/Spanned;

    .line 71
    :cond_1
    iget-object v1, v0, Lnwk;->c:[Lnwl;

    .line 72
    if-eqz v1, :cond_2

    array-length v0, v1

    if-lez v0, :cond_2

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldnv;->q:Ljava/util/HashMap;

    .line 74
    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_2

    .line 75
    aget-object v3, v1, v0

    iget-object v3, v3, Lnwl;->c:Lpeo;

    invoke-static {v3}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 77
    iget-object v4, p0, Ldnv;->q:Ljava/util/HashMap;

    aget-object v5, v1, v0

    iget-object v5, v5, Lnwl;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_2
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmki;

    invoke-virtual {p0, p1}, Ldnv;->a(Lmki;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmkj;

    invoke-virtual {p0, p1}, Ldnv;->a(Lmkj;)V

    return-void
.end method

.method public d()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ldnv;->b:Landroid/text/Spanned;

    return-object v0
.end method

.method public e()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldnv;->p:Landroid/text/Spanned;

    return-object v0
.end method

.method public f()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/text/Spanned;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Ldnv;->q:Ljava/util/HashMap;

    return-object v0
.end method
