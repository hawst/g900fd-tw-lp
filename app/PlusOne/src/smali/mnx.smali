.class public final Lmnx;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmny;

.field public b:[Lmns;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6274
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6437
    sget-object v0, Lmny;->a:[Lmny;

    iput-object v0, p0, Lmnx;->a:[Lmny;

    .line 6440
    sget-object v0, Lmns;->a:[Lmns;

    iput-object v0, p0, Lmnx;->b:[Lmns;

    .line 6274
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 6470
    .line 6471
    iget-object v0, p0, Lmnx;->a:[Lmny;

    if-eqz v0, :cond_1

    .line 6472
    iget-object v3, p0, Lmnx;->a:[Lmny;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 6473
    if-eqz v5, :cond_0

    .line 6474
    const/4 v6, 0x1

    .line 6475
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 6472
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 6479
    :cond_2
    iget-object v2, p0, Lmnx;->b:[Lmns;

    if-eqz v2, :cond_4

    .line 6480
    iget-object v2, p0, Lmnx;->b:[Lmns;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 6481
    if-eqz v4, :cond_3

    .line 6482
    const/4 v5, 0x2

    .line 6483
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6480
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6487
    :cond_4
    iget-object v1, p0, Lmnx;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 6488
    const/4 v1, 0x3

    iget-object v2, p0, Lmnx;->c:Ljava/lang/Integer;

    .line 6489
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6491
    :cond_5
    iget-object v1, p0, Lmnx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6492
    iput v0, p0, Lmnx;->ai:I

    .line 6493
    return v0
.end method

.method public a(Loxn;)Lmnx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6501
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6502
    sparse-switch v0, :sswitch_data_0

    .line 6506
    iget-object v2, p0, Lmnx;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 6507
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmnx;->ah:Ljava/util/List;

    .line 6510
    :cond_1
    iget-object v2, p0, Lmnx;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6512
    :sswitch_0
    return-object p0

    .line 6517
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6518
    iget-object v0, p0, Lmnx;->a:[Lmny;

    if-nez v0, :cond_3

    move v0, v1

    .line 6519
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmny;

    .line 6520
    iget-object v3, p0, Lmnx;->a:[Lmny;

    if-eqz v3, :cond_2

    .line 6521
    iget-object v3, p0, Lmnx;->a:[Lmny;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6523
    :cond_2
    iput-object v2, p0, Lmnx;->a:[Lmny;

    .line 6524
    :goto_2
    iget-object v2, p0, Lmnx;->a:[Lmny;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 6525
    iget-object v2, p0, Lmnx;->a:[Lmny;

    new-instance v3, Lmny;

    invoke-direct {v3}, Lmny;-><init>()V

    aput-object v3, v2, v0

    .line 6526
    iget-object v2, p0, Lmnx;->a:[Lmny;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6527
    invoke-virtual {p1}, Loxn;->a()I

    .line 6524
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6518
    :cond_3
    iget-object v0, p0, Lmnx;->a:[Lmny;

    array-length v0, v0

    goto :goto_1

    .line 6530
    :cond_4
    iget-object v2, p0, Lmnx;->a:[Lmny;

    new-instance v3, Lmny;

    invoke-direct {v3}, Lmny;-><init>()V

    aput-object v3, v2, v0

    .line 6531
    iget-object v2, p0, Lmnx;->a:[Lmny;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6535
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6536
    iget-object v0, p0, Lmnx;->b:[Lmns;

    if-nez v0, :cond_6

    move v0, v1

    .line 6537
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmns;

    .line 6538
    iget-object v3, p0, Lmnx;->b:[Lmns;

    if-eqz v3, :cond_5

    .line 6539
    iget-object v3, p0, Lmnx;->b:[Lmns;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6541
    :cond_5
    iput-object v2, p0, Lmnx;->b:[Lmns;

    .line 6542
    :goto_4
    iget-object v2, p0, Lmnx;->b:[Lmns;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 6543
    iget-object v2, p0, Lmnx;->b:[Lmns;

    new-instance v3, Lmns;

    invoke-direct {v3}, Lmns;-><init>()V

    aput-object v3, v2, v0

    .line 6544
    iget-object v2, p0, Lmnx;->b:[Lmns;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6545
    invoke-virtual {p1}, Loxn;->a()I

    .line 6542
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 6536
    :cond_6
    iget-object v0, p0, Lmnx;->b:[Lmns;

    array-length v0, v0

    goto :goto_3

    .line 6548
    :cond_7
    iget-object v2, p0, Lmnx;->b:[Lmns;

    new-instance v3, Lmns;

    invoke-direct {v3}, Lmns;-><init>()V

    aput-object v3, v2, v0

    .line 6549
    iget-object v2, p0, Lmnx;->b:[Lmns;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6553
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmnx;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6502
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6447
    iget-object v1, p0, Lmnx;->a:[Lmny;

    if-eqz v1, :cond_1

    .line 6448
    iget-object v2, p0, Lmnx;->a:[Lmny;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6449
    if-eqz v4, :cond_0

    .line 6450
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 6448
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6454
    :cond_1
    iget-object v1, p0, Lmnx;->b:[Lmns;

    if-eqz v1, :cond_3

    .line 6455
    iget-object v1, p0, Lmnx;->b:[Lmns;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 6456
    if-eqz v3, :cond_2

    .line 6457
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 6455
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6461
    :cond_3
    iget-object v0, p0, Lmnx;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 6462
    const/4 v0, 0x3

    iget-object v1, p0, Lmnx;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6464
    :cond_4
    iget-object v0, p0, Lmnx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6466
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6270
    invoke-virtual {p0, p1}, Lmnx;->a(Loxn;)Lmnx;

    move-result-object v0

    return-object v0
.end method
