.class public final Lorc;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:[Lord;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 939
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1153
    const/high16 v0, -0x80000000

    iput v0, p0, Lorc;->b:I

    .line 1156
    sget-object v0, Lord;->a:[Lord;

    iput-object v0, p0, Lorc;->c:[Lord;

    .line 939
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1180
    .line 1181
    iget-object v0, p0, Lorc;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1182
    const/4 v0, 0x1

    iget-object v2, p0, Lorc;->a:Ljava/lang/String;

    .line 1183
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1185
    :goto_0
    iget v2, p0, Lorc;->b:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_0

    .line 1186
    const/4 v2, 0x2

    iget v3, p0, Lorc;->b:I

    .line 1187
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1189
    :cond_0
    iget-object v2, p0, Lorc;->c:[Lord;

    if-eqz v2, :cond_2

    .line 1190
    iget-object v2, p0, Lorc;->c:[Lord;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1191
    if-eqz v4, :cond_1

    .line 1192
    const/4 v5, 0x3

    .line 1193
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1190
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1197
    :cond_2
    iget-object v1, p0, Lorc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1198
    iput v0, p0, Lorc;->ai:I

    .line 1199
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lorc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1207
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1208
    sparse-switch v0, :sswitch_data_0

    .line 1212
    iget-object v2, p0, Lorc;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1213
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lorc;->ah:Ljava/util/List;

    .line 1216
    :cond_1
    iget-object v2, p0, Lorc;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1218
    :sswitch_0
    return-object p0

    .line 1223
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorc;->a:Ljava/lang/String;

    goto :goto_0

    .line 1227
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1228
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    .line 1234
    :cond_2
    iput v0, p0, Lorc;->b:I

    goto :goto_0

    .line 1236
    :cond_3
    iput v1, p0, Lorc;->b:I

    goto :goto_0

    .line 1241
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1242
    iget-object v0, p0, Lorc;->c:[Lord;

    if-nez v0, :cond_5

    move v0, v1

    .line 1243
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lord;

    .line 1244
    iget-object v3, p0, Lorc;->c:[Lord;

    if-eqz v3, :cond_4

    .line 1245
    iget-object v3, p0, Lorc;->c:[Lord;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1247
    :cond_4
    iput-object v2, p0, Lorc;->c:[Lord;

    .line 1248
    :goto_2
    iget-object v2, p0, Lorc;->c:[Lord;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1249
    iget-object v2, p0, Lorc;->c:[Lord;

    new-instance v3, Lord;

    invoke-direct {v3}, Lord;-><init>()V

    aput-object v3, v2, v0

    .line 1250
    iget-object v2, p0, Lorc;->c:[Lord;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1251
    invoke-virtual {p1}, Loxn;->a()I

    .line 1248
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1242
    :cond_5
    iget-object v0, p0, Lorc;->c:[Lord;

    array-length v0, v0

    goto :goto_1

    .line 1254
    :cond_6
    iget-object v2, p0, Lorc;->c:[Lord;

    new-instance v3, Lord;

    invoke-direct {v3}, Lord;-><init>()V

    aput-object v3, v2, v0

    .line 1255
    iget-object v2, p0, Lorc;->c:[Lord;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1208
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1161
    iget-object v0, p0, Lorc;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1162
    const/4 v0, 0x1

    iget-object v1, p0, Lorc;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1164
    :cond_0
    iget v0, p0, Lorc;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 1165
    const/4 v0, 0x2

    iget v1, p0, Lorc;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1167
    :cond_1
    iget-object v0, p0, Lorc;->c:[Lord;

    if-eqz v0, :cond_3

    .line 1168
    iget-object v1, p0, Lorc;->c:[Lord;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1169
    if-eqz v3, :cond_2

    .line 1170
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1168
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1174
    :cond_3
    iget-object v0, p0, Lorc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1176
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 935
    invoke-virtual {p0, p1}, Lorc;->a(Loxn;)Lorc;

    move-result-object v0

    return-object v0
.end method
