.class public final Lfnw;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 41
    const-string v0, "PrefetchStoryTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lfnw;->a:Landroid/content/Context;

    .line 43
    iput p2, p0, Lfnw;->b:I

    .line 44
    iput-object p3, p0, Lfnw;->c:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lfnw;->d:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Lfnw;->e:Ljava/lang/String;

    .line 47
    iput-boolean p6, p0, Lfnw;->f:Z

    .line 48
    iput-boolean p7, p0, Lfnw;->h:Z

    .line 49
    return-void
.end method

.method private a(Lhmv;)V
    .locals 5

    .prologue
    .line 98
    iget-object v0, p0, Lfnw;->a:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lfnw;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 99
    invoke-virtual {v1, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    iget v2, p0, Lfnw;->b:I

    .line 101
    invoke-virtual {v1, v2}, Lhmr;->a(I)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->Q:Lhmw;

    .line 102
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v2

    .line 103
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lfnw;->a:Landroid/content/Context;

    const-class v4, Lhei;

    invoke-static {v1, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    iget v4, p0, Lfnw;->b:I

    invoke-interface {v1, v4}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v4, "gaia_id"

    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "extra_gaia_id"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "story_id"

    iget-object v4, p0, Lfnw;->c:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 98
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 104
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 53
    sget-object v0, Lhmv;->fj:Lhmv;

    invoke-direct {p0, v0}, Lfnw;->a(Lhmv;)V

    .line 54
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 55
    iget-object v0, p0, Lfnw;->a:Landroid/content/Context;

    iget v1, p0, Lfnw;->b:I

    iget-object v2, p0, Lfnw;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lfss;->a(Landroid/content/Context;ILjava/lang/String;)Lmmq;

    move-result-object v0

    .line 56
    if-nez v0, :cond_1

    .line 62
    :try_start_0
    iget-object v0, p0, Lfnw;->a:Landroid/content/Context;

    .line 63
    invoke-static {v0}, Lfss;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 62
    invoke-static {v0}, Lfss;->a(Landroid/graphics/Point;)[Landroid/graphics/Point;

    move-result-object v8

    .line 64
    iget-object v0, p0, Lfnw;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Lfnw;->b:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v3

    .line 69
    new-instance v0, Lfnm;

    iget-object v1, p0, Lfnw;->a:Landroid/content/Context;

    new-instance v2, Lkfo;

    const-string v4, "account_name"

    .line 70
    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lfnw;->c:Ljava/lang/String;

    iget-object v4, p0, Lfnw;->d:Ljava/lang/String;

    iget-object v5, p0, Lfnw;->e:Ljava/lang/String;

    iget-boolean v6, p0, Lfnw;->f:Z

    iget-boolean v7, p0, Lfnw;->h:Z

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lfnm;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;Z)V

    .line 73
    iget-object v1, p0, Lfnw;->a:Landroid/content/Context;

    const-class v2, Lkfd;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 74
    invoke-virtual {v0}, Lfnm;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 75
    invoke-virtual {v0}, Lfnm;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmdh;

    iget-object v0, v0, Lmdh;->a:Lmlr;

    iget-object v0, v0, Lmlr;->a:Lmmq;

    .line 76
    iget-object v1, p0, Lfnw;->a:Landroid/content/Context;

    iget v2, p0, Lfnw;->b:I

    invoke-static {v1, v2, v0}, Lfoa;->a(Landroid/content/Context;ILmmq;)V

    .line 77
    sget-object v0, Lhmv;->fk:Lhmv;

    invoke-direct {p0, v0}, Lfnw;->a(Lhmv;)V

    .line 78
    new-instance v0, Lhoz;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lhoz;-><init>(Z)V

    .line 94
    :goto_0
    return-object v0

    .line 80
    :cond_0
    sget-object v0, Lhmv;->fl:Lhmv;

    invoke-direct {p0, v0}, Lfnw;->a(Lhmv;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_1
    new-instance v0, Lhoz;

    invoke-direct {v0, v10}, Lhoz;-><init>(Z)V

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    sget-object v0, Lhmv;->fl:Lhmv;

    invoke-direct {p0, v0}, Lfnw;->a(Lhmv;)V

    goto :goto_1

    .line 88
    :cond_1
    sget-object v0, Lhmv;->fm:Lhmv;

    invoke-direct {p0, v0}, Lfnw;->a(Lhmv;)V

    goto :goto_1

    .line 92
    :cond_2
    sget-object v0, Lhmv;->fm:Lhmv;

    invoke-direct {p0, v0}, Lfnw;->a(Lhmv;)V

    goto :goto_1
.end method
