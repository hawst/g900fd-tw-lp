.class public final Lpwl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpwl;


# instance fields
.field private b:Lppd;

.field private c:Ljava/lang/String;

.field private d:[Lpwn;

.field private e:Ljava/lang/String;

.field private f:Lppf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x0

    new-array v0, v0, [Lpwl;

    sput-object v0, Lpwl;->a:[Lpwl;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298
    invoke-direct {p0}, Loxq;-><init>()V

    .line 301
    iput-object v1, p0, Lpwl;->b:Lppd;

    .line 306
    sget-object v0, Lpwn;->a:[Lpwn;

    iput-object v0, p0, Lpwl;->d:[Lpwn;

    .line 311
    iput-object v1, p0, Lpwl;->f:Lppf;

    .line 298
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 341
    .line 342
    iget-object v0, p0, Lpwl;->b:Lppd;

    if-eqz v0, :cond_5

    .line 343
    const/4 v0, 0x1

    iget-object v2, p0, Lpwl;->b:Lppd;

    .line 344
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 346
    :goto_0
    iget-object v2, p0, Lpwl;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 347
    const/4 v2, 0x2

    iget-object v3, p0, Lpwl;->c:Ljava/lang/String;

    .line 348
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 350
    :cond_0
    iget-object v2, p0, Lpwl;->d:[Lpwn;

    if-eqz v2, :cond_2

    .line 351
    iget-object v2, p0, Lpwl;->d:[Lpwn;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 352
    if-eqz v4, :cond_1

    .line 353
    const/4 v5, 0x3

    .line 354
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 351
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 358
    :cond_2
    iget-object v1, p0, Lpwl;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 359
    const/4 v1, 0x4

    iget-object v2, p0, Lpwl;->e:Ljava/lang/String;

    .line 360
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    :cond_3
    iget-object v1, p0, Lpwl;->f:Lppf;

    if-eqz v1, :cond_4

    .line 363
    const/4 v1, 0x5

    iget-object v2, p0, Lpwl;->f:Lppf;

    .line 364
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    :cond_4
    iget-object v1, p0, Lpwl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    iput v0, p0, Lpwl;->ai:I

    .line 368
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpwl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 377
    sparse-switch v0, :sswitch_data_0

    .line 381
    iget-object v2, p0, Lpwl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 382
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpwl;->ah:Ljava/util/List;

    .line 385
    :cond_1
    iget-object v2, p0, Lpwl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    :sswitch_0
    return-object p0

    .line 392
    :sswitch_1
    iget-object v0, p0, Lpwl;->b:Lppd;

    if-nez v0, :cond_2

    .line 393
    new-instance v0, Lppd;

    invoke-direct {v0}, Lppd;-><init>()V

    iput-object v0, p0, Lpwl;->b:Lppd;

    .line 395
    :cond_2
    iget-object v0, p0, Lpwl;->b:Lppd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 399
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpwl;->c:Ljava/lang/String;

    goto :goto_0

    .line 403
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 404
    iget-object v0, p0, Lpwl;->d:[Lpwn;

    if-nez v0, :cond_4

    move v0, v1

    .line 405
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpwn;

    .line 406
    iget-object v3, p0, Lpwl;->d:[Lpwn;

    if-eqz v3, :cond_3

    .line 407
    iget-object v3, p0, Lpwl;->d:[Lpwn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 409
    :cond_3
    iput-object v2, p0, Lpwl;->d:[Lpwn;

    .line 410
    :goto_2
    iget-object v2, p0, Lpwl;->d:[Lpwn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 411
    iget-object v2, p0, Lpwl;->d:[Lpwn;

    new-instance v3, Lpwn;

    invoke-direct {v3}, Lpwn;-><init>()V

    aput-object v3, v2, v0

    .line 412
    iget-object v2, p0, Lpwl;->d:[Lpwn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 413
    invoke-virtual {p1}, Loxn;->a()I

    .line 410
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 404
    :cond_4
    iget-object v0, p0, Lpwl;->d:[Lpwn;

    array-length v0, v0

    goto :goto_1

    .line 416
    :cond_5
    iget-object v2, p0, Lpwl;->d:[Lpwn;

    new-instance v3, Lpwn;

    invoke-direct {v3}, Lpwn;-><init>()V

    aput-object v3, v2, v0

    .line 417
    iget-object v2, p0, Lpwl;->d:[Lpwn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 421
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpwl;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 425
    :sswitch_5
    iget-object v0, p0, Lpwl;->f:Lppf;

    if-nez v0, :cond_6

    .line 426
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpwl;->f:Lppf;

    .line 428
    :cond_6
    iget-object v0, p0, Lpwl;->f:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 377
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 316
    iget-object v0, p0, Lpwl;->b:Lppd;

    if-eqz v0, :cond_0

    .line 317
    const/4 v0, 0x1

    iget-object v1, p0, Lpwl;->b:Lppd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 319
    :cond_0
    iget-object v0, p0, Lpwl;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 320
    const/4 v0, 0x2

    iget-object v1, p0, Lpwl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 322
    :cond_1
    iget-object v0, p0, Lpwl;->d:[Lpwn;

    if-eqz v0, :cond_3

    .line 323
    iget-object v1, p0, Lpwl;->d:[Lpwn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 324
    if-eqz v3, :cond_2

    .line 325
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 323
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 329
    :cond_3
    iget-object v0, p0, Lpwl;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 330
    const/4 v0, 0x4

    iget-object v1, p0, Lpwl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 332
    :cond_4
    iget-object v0, p0, Lpwl;->f:Lppf;

    if-eqz v0, :cond_5

    .line 333
    const/4 v0, 0x5

    iget-object v1, p0, Lpwl;->f:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 335
    :cond_5
    iget-object v0, p0, Lpwl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 337
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 294
    invoke-virtual {p0, p1}, Lpwl;->a(Loxn;)Lpwl;

    move-result-object v0

    return-object v0
.end method
