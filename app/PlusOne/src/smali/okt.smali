.class public final Lokt;
.super Loxq;
.source "PG"


# instance fields
.field private a:[B

.field private b:[B

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Long;

.field private j:Ljava/lang/Long;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 591
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 678
    const/4 v0, 0x1

    iget-object v1, p0, Lokt;->a:[B

    .line 680
    invoke-static {v0, v1}, Loxo;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 681
    iget-object v1, p0, Lokt;->b:[B

    if-eqz v1, :cond_0

    .line 682
    const/4 v1, 0x2

    iget-object v2, p0, Lokt;->b:[B

    .line 683
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 685
    :cond_0
    const/4 v1, 0x3

    iget-object v2, p0, Lokt;->c:Ljava/lang/Integer;

    .line 686
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 687
    iget-object v1, p0, Lokt;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 688
    const/4 v1, 0x5

    iget-object v2, p0, Lokt;->d:Ljava/lang/Boolean;

    .line 689
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 691
    :cond_1
    iget-object v1, p0, Lokt;->i:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 692
    const/4 v1, 0x6

    iget-object v2, p0, Lokt;->i:Ljava/lang/Long;

    .line 693
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 695
    :cond_2
    iget-object v1, p0, Lokt;->f:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 696
    const/16 v1, 0x8

    iget-object v2, p0, Lokt;->f:Ljava/lang/Long;

    .line 697
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 699
    :cond_3
    iget-object v1, p0, Lokt;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 700
    const/16 v1, 0x9

    iget-object v2, p0, Lokt;->e:Ljava/lang/Long;

    .line 701
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 703
    :cond_4
    iget-object v1, p0, Lokt;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 704
    const/16 v1, 0xa

    iget-object v2, p0, Lokt;->g:Ljava/lang/Integer;

    .line 705
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 707
    :cond_5
    iget-object v1, p0, Lokt;->j:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 708
    const/16 v1, 0xb

    iget-object v2, p0, Lokt;->j:Ljava/lang/Long;

    .line 709
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 711
    :cond_6
    iget-object v1, p0, Lokt;->k:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 712
    const/16 v1, 0xc

    iget-object v2, p0, Lokt;->k:Ljava/lang/String;

    .line 713
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 715
    :cond_7
    iget-object v1, p0, Lokt;->l:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 716
    const/16 v1, 0xd

    iget-object v2, p0, Lokt;->l:Ljava/lang/String;

    .line 717
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 719
    :cond_8
    iget-object v1, p0, Lokt;->m:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 720
    const/16 v1, 0xe

    iget-object v2, p0, Lokt;->m:Ljava/lang/String;

    .line 721
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 723
    :cond_9
    iget-object v1, p0, Lokt;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 724
    const/16 v1, 0xf

    iget-object v2, p0, Lokt;->h:Ljava/lang/Boolean;

    .line 725
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 727
    :cond_a
    iget-object v1, p0, Lokt;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 728
    const/16 v1, 0x10

    iget-object v2, p0, Lokt;->n:Ljava/lang/String;

    .line 729
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 731
    :cond_b
    iget-object v1, p0, Lokt;->o:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 732
    const/16 v1, 0x11

    iget-object v2, p0, Lokt;->o:Ljava/lang/String;

    .line 733
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 735
    :cond_c
    iget-object v1, p0, Lokt;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 736
    const/16 v1, 0x12

    iget-object v2, p0, Lokt;->p:Ljava/lang/Integer;

    .line 737
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 739
    :cond_d
    iget-object v1, p0, Lokt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 740
    iput v0, p0, Lokt;->ai:I

    .line 741
    return v0
.end method

.method public a(Loxn;)Lokt;
    .locals 2

    .prologue
    .line 749
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 750
    sparse-switch v0, :sswitch_data_0

    .line 754
    iget-object v1, p0, Lokt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 755
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lokt;->ah:Ljava/util/List;

    .line 758
    :cond_1
    iget-object v1, p0, Lokt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 760
    :sswitch_0
    return-object p0

    .line 765
    :sswitch_1
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lokt;->a:[B

    goto :goto_0

    .line 769
    :sswitch_2
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lokt;->b:[B

    goto :goto_0

    .line 773
    :sswitch_3
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokt;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 777
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokt;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 781
    :sswitch_5
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lokt;->i:Ljava/lang/Long;

    goto :goto_0

    .line 785
    :sswitch_6
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lokt;->f:Ljava/lang/Long;

    goto :goto_0

    .line 789
    :sswitch_7
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lokt;->e:Ljava/lang/Long;

    goto :goto_0

    .line 793
    :sswitch_8
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokt;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 797
    :sswitch_9
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lokt;->j:Ljava/lang/Long;

    goto :goto_0

    .line 801
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokt;->k:Ljava/lang/String;

    goto :goto_0

    .line 805
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokt;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 809
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokt;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 813
    :sswitch_d
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lokt;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 817
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokt;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 821
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokt;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 825
    :sswitch_10
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokt;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 750
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x40 -> :sswitch_6
        0x48 -> :sswitch_7
        0x50 -> :sswitch_8
        0x58 -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x78 -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
        0x90 -> :sswitch_10
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 628
    const/4 v0, 0x1

    iget-object v1, p0, Lokt;->a:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 629
    iget-object v0, p0, Lokt;->b:[B

    if-eqz v0, :cond_0

    .line 630
    const/4 v0, 0x2

    iget-object v1, p0, Lokt;->b:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 632
    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Lokt;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 633
    iget-object v0, p0, Lokt;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 634
    const/4 v0, 0x5

    iget-object v1, p0, Lokt;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 636
    :cond_1
    iget-object v0, p0, Lokt;->i:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 637
    const/4 v0, 0x6

    iget-object v1, p0, Lokt;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 639
    :cond_2
    iget-object v0, p0, Lokt;->f:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 640
    const/16 v0, 0x8

    iget-object v1, p0, Lokt;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 642
    :cond_3
    iget-object v0, p0, Lokt;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 643
    const/16 v0, 0x9

    iget-object v1, p0, Lokt;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 645
    :cond_4
    iget-object v0, p0, Lokt;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 646
    const/16 v0, 0xa

    iget-object v1, p0, Lokt;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 648
    :cond_5
    iget-object v0, p0, Lokt;->j:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 649
    const/16 v0, 0xb

    iget-object v1, p0, Lokt;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 651
    :cond_6
    iget-object v0, p0, Lokt;->k:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 652
    const/16 v0, 0xc

    iget-object v1, p0, Lokt;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 654
    :cond_7
    iget-object v0, p0, Lokt;->l:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 655
    const/16 v0, 0xd

    iget-object v1, p0, Lokt;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 657
    :cond_8
    iget-object v0, p0, Lokt;->m:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 658
    const/16 v0, 0xe

    iget-object v1, p0, Lokt;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 660
    :cond_9
    iget-object v0, p0, Lokt;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 661
    const/16 v0, 0xf

    iget-object v1, p0, Lokt;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 663
    :cond_a
    iget-object v0, p0, Lokt;->n:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 664
    const/16 v0, 0x10

    iget-object v1, p0, Lokt;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 666
    :cond_b
    iget-object v0, p0, Lokt;->o:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 667
    const/16 v0, 0x11

    iget-object v1, p0, Lokt;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 669
    :cond_c
    iget-object v0, p0, Lokt;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 670
    const/16 v0, 0x12

    iget-object v1, p0, Lokt;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 672
    :cond_d
    iget-object v0, p0, Lokt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 674
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 587
    invoke-virtual {p0, p1}, Lokt;->a(Loxn;)Lokt;

    move-result-object v0

    return-object v0
.end method
