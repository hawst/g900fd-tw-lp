.class public final Loda;
.super Loxq;
.source "PG"


# instance fields
.field public a:Logi;

.field public b:Loge;

.field public c:Lodc;

.field public d:Lodb;

.field private e:Locr;

.field private f:Loya;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1991
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1994
    iput-object v0, p0, Loda;->a:Logi;

    .line 1997
    iput-object v0, p0, Loda;->b:Loge;

    .line 2000
    iput-object v0, p0, Loda;->e:Locr;

    .line 2003
    iput-object v0, p0, Loda;->f:Loya;

    .line 2006
    iput-object v0, p0, Loda;->c:Lodc;

    .line 2009
    iput-object v0, p0, Loda;->d:Lodb;

    .line 1991
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2038
    const/4 v0, 0x0

    .line 2039
    iget-object v1, p0, Loda;->a:Logi;

    if-eqz v1, :cond_0

    .line 2040
    const/4 v0, 0x1

    iget-object v1, p0, Loda;->a:Logi;

    .line 2041
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2043
    :cond_0
    iget-object v1, p0, Loda;->b:Loge;

    if-eqz v1, :cond_1

    .line 2044
    const/4 v1, 0x2

    iget-object v2, p0, Loda;->b:Loge;

    .line 2045
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2047
    :cond_1
    iget-object v1, p0, Loda;->e:Locr;

    if-eqz v1, :cond_2

    .line 2048
    const/4 v1, 0x3

    iget-object v2, p0, Loda;->e:Locr;

    .line 2049
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2051
    :cond_2
    iget-object v1, p0, Loda;->f:Loya;

    if-eqz v1, :cond_3

    .line 2052
    const/4 v1, 0x4

    iget-object v2, p0, Loda;->f:Loya;

    .line 2053
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2055
    :cond_3
    iget-object v1, p0, Loda;->c:Lodc;

    if-eqz v1, :cond_4

    .line 2056
    const/4 v1, 0x5

    iget-object v2, p0, Loda;->c:Lodc;

    .line 2057
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2059
    :cond_4
    iget-object v1, p0, Loda;->d:Lodb;

    if-eqz v1, :cond_5

    .line 2060
    const/4 v1, 0x6

    iget-object v2, p0, Loda;->d:Lodb;

    .line 2061
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2063
    :cond_5
    iget-object v1, p0, Loda;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2064
    iput v0, p0, Loda;->ai:I

    .line 2065
    return v0
.end method

.method public a(Loxn;)Loda;
    .locals 2

    .prologue
    .line 2073
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2074
    sparse-switch v0, :sswitch_data_0

    .line 2078
    iget-object v1, p0, Loda;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2079
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loda;->ah:Ljava/util/List;

    .line 2082
    :cond_1
    iget-object v1, p0, Loda;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2084
    :sswitch_0
    return-object p0

    .line 2089
    :sswitch_1
    iget-object v0, p0, Loda;->a:Logi;

    if-nez v0, :cond_2

    .line 2090
    new-instance v0, Logi;

    invoke-direct {v0}, Logi;-><init>()V

    iput-object v0, p0, Loda;->a:Logi;

    .line 2092
    :cond_2
    iget-object v0, p0, Loda;->a:Logi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2096
    :sswitch_2
    iget-object v0, p0, Loda;->b:Loge;

    if-nez v0, :cond_3

    .line 2097
    new-instance v0, Loge;

    invoke-direct {v0}, Loge;-><init>()V

    iput-object v0, p0, Loda;->b:Loge;

    .line 2099
    :cond_3
    iget-object v0, p0, Loda;->b:Loge;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2103
    :sswitch_3
    iget-object v0, p0, Loda;->e:Locr;

    if-nez v0, :cond_4

    .line 2104
    new-instance v0, Locr;

    invoke-direct {v0}, Locr;-><init>()V

    iput-object v0, p0, Loda;->e:Locr;

    .line 2106
    :cond_4
    iget-object v0, p0, Loda;->e:Locr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2110
    :sswitch_4
    iget-object v0, p0, Loda;->f:Loya;

    if-nez v0, :cond_5

    .line 2111
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loda;->f:Loya;

    .line 2113
    :cond_5
    iget-object v0, p0, Loda;->f:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2117
    :sswitch_5
    iget-object v0, p0, Loda;->c:Lodc;

    if-nez v0, :cond_6

    .line 2118
    new-instance v0, Lodc;

    invoke-direct {v0}, Lodc;-><init>()V

    iput-object v0, p0, Loda;->c:Lodc;

    .line 2120
    :cond_6
    iget-object v0, p0, Loda;->c:Lodc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2124
    :sswitch_6
    iget-object v0, p0, Loda;->d:Lodb;

    if-nez v0, :cond_7

    .line 2125
    new-instance v0, Lodb;

    invoke-direct {v0}, Lodb;-><init>()V

    iput-object v0, p0, Loda;->d:Lodb;

    .line 2127
    :cond_7
    iget-object v0, p0, Loda;->d:Lodb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2074
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2014
    iget-object v0, p0, Loda;->a:Logi;

    if-eqz v0, :cond_0

    .line 2015
    const/4 v0, 0x1

    iget-object v1, p0, Loda;->a:Logi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2017
    :cond_0
    iget-object v0, p0, Loda;->b:Loge;

    if-eqz v0, :cond_1

    .line 2018
    const/4 v0, 0x2

    iget-object v1, p0, Loda;->b:Loge;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2020
    :cond_1
    iget-object v0, p0, Loda;->e:Locr;

    if-eqz v0, :cond_2

    .line 2021
    const/4 v0, 0x3

    iget-object v1, p0, Loda;->e:Locr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2023
    :cond_2
    iget-object v0, p0, Loda;->f:Loya;

    if-eqz v0, :cond_3

    .line 2024
    const/4 v0, 0x4

    iget-object v1, p0, Loda;->f:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2026
    :cond_3
    iget-object v0, p0, Loda;->c:Lodc;

    if-eqz v0, :cond_4

    .line 2027
    const/4 v0, 0x5

    iget-object v1, p0, Loda;->c:Lodc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2029
    :cond_4
    iget-object v0, p0, Loda;->d:Lodb;

    if-eqz v0, :cond_5

    .line 2030
    const/4 v0, 0x6

    iget-object v1, p0, Loda;->d:Lodb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2032
    :cond_5
    iget-object v0, p0, Loda;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2034
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1987
    invoke-virtual {p0, p1}, Loda;->a(Loxn;)Loda;

    move-result-object v0

    return-object v0
.end method
