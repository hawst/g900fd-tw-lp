.class public final Lklo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lklo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Loya;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lklp;

    invoke-direct {v0}, Lklp;-><init>()V

    sput-object v0, Lklo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Loya;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lklo;->a:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lklo;->b:Loya;

    .line 25
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lklo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Loya;
    .locals 2

    .prologue
    .line 33
    if-eqz p1, :cond_0

    iget-object v0, p0, Lklo;->b:Loya;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lklo;->b:Loya;

    new-instance v1, Loyp;

    invoke-direct {v1}, Loyp;-><init>()V

    iput-object v1, v0, Loya;->d:Loyp;

    .line 35
    iget-object v0, p0, Lklo;->b:Loya;

    iget-object v0, v0, Loya;->d:Loyp;

    iput-object p1, v0, Loyp;->b:Ljava/lang/String;

    .line 37
    :cond_0
    iget-object v0, p0, Lklo;->b:Loya;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    iget-object v0, p0, Lklo;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lklo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    :goto_0
    iget-object v0, p0, Lklo;->b:Loya;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lklo;->b:Loya;

    invoke-static {v0}, Loya;->a(Loxu;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 61
    :goto_1
    return-void

    .line 54
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1
.end method
