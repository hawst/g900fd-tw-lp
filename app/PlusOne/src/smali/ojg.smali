.class public final Lojg;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lohn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Loxq;-><init>()V

    .line 237
    sget-object v0, Lohn;->a:[Lohn;

    iput-object v0, p0, Lojg;->a:[Lohn;

    .line 234
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 255
    .line 256
    iget-object v1, p0, Lojg;->a:[Lohn;

    if-eqz v1, :cond_1

    .line 257
    iget-object v2, p0, Lojg;->a:[Lohn;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 258
    if-eqz v4, :cond_0

    .line 259
    const/4 v5, 0x1

    .line 260
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 257
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 264
    :cond_1
    iget-object v1, p0, Lojg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    iput v0, p0, Lojg;->ai:I

    .line 266
    return v0
.end method

.method public a(Loxn;)Lojg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 274
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 275
    sparse-switch v0, :sswitch_data_0

    .line 279
    iget-object v2, p0, Lojg;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 280
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lojg;->ah:Ljava/util/List;

    .line 283
    :cond_1
    iget-object v2, p0, Lojg;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    :sswitch_0
    return-object p0

    .line 290
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 291
    iget-object v0, p0, Lojg;->a:[Lohn;

    if-nez v0, :cond_3

    move v0, v1

    .line 292
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohn;

    .line 293
    iget-object v3, p0, Lojg;->a:[Lohn;

    if-eqz v3, :cond_2

    .line 294
    iget-object v3, p0, Lojg;->a:[Lohn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 296
    :cond_2
    iput-object v2, p0, Lojg;->a:[Lohn;

    .line 297
    :goto_2
    iget-object v2, p0, Lojg;->a:[Lohn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 298
    iget-object v2, p0, Lojg;->a:[Lohn;

    new-instance v3, Lohn;

    invoke-direct {v3}, Lohn;-><init>()V

    aput-object v3, v2, v0

    .line 299
    iget-object v2, p0, Lojg;->a:[Lohn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 300
    invoke-virtual {p1}, Loxn;->a()I

    .line 297
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 291
    :cond_3
    iget-object v0, p0, Lojg;->a:[Lohn;

    array-length v0, v0

    goto :goto_1

    .line 303
    :cond_4
    iget-object v2, p0, Lojg;->a:[Lohn;

    new-instance v3, Lohn;

    invoke-direct {v3}, Lohn;-><init>()V

    aput-object v3, v2, v0

    .line 304
    iget-object v2, p0, Lojg;->a:[Lohn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 275
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 242
    iget-object v0, p0, Lojg;->a:[Lohn;

    if-eqz v0, :cond_1

    .line 243
    iget-object v1, p0, Lojg;->a:[Lohn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 244
    if-eqz v3, :cond_0

    .line 245
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 243
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 249
    :cond_1
    iget-object v0, p0, Lojg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 251
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 230
    invoke-virtual {p0, p1}, Lojg;->a(Loxn;)Lojg;

    move-result-object v0

    return-object v0
.end method
