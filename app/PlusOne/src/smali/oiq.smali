.class public final Loiq;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Loir;

.field private c:I

.field private d:Loip;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 646
    invoke-direct {p0}, Loxq;-><init>()V

    .line 651
    iput-object v1, p0, Loiq;->b:Loir;

    .line 654
    const/high16 v0, -0x80000000

    iput v0, p0, Loiq;->c:I

    .line 657
    iput-object v1, p0, Loiq;->d:Loip;

    .line 646
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 680
    const/4 v0, 0x0

    .line 681
    iget-object v1, p0, Loiq;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 682
    const/4 v0, 0x1

    iget-object v1, p0, Loiq;->a:Ljava/lang/String;

    .line 683
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 685
    :cond_0
    iget-object v1, p0, Loiq;->b:Loir;

    if-eqz v1, :cond_1

    .line 686
    const/4 v1, 0x2

    iget-object v2, p0, Loiq;->b:Loir;

    .line 687
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 689
    :cond_1
    iget v1, p0, Loiq;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 690
    const/4 v1, 0x3

    iget v2, p0, Loiq;->c:I

    .line 691
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 693
    :cond_2
    iget-object v1, p0, Loiq;->d:Loip;

    if-eqz v1, :cond_3

    .line 694
    const/4 v1, 0x4

    iget-object v2, p0, Loiq;->d:Loip;

    .line 695
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 697
    :cond_3
    iget-object v1, p0, Loiq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 698
    iput v0, p0, Loiq;->ai:I

    .line 699
    return v0
.end method

.method public a(Loxn;)Loiq;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 707
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 708
    sparse-switch v0, :sswitch_data_0

    .line 712
    iget-object v1, p0, Loiq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 713
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loiq;->ah:Ljava/util/List;

    .line 716
    :cond_1
    iget-object v1, p0, Loiq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 718
    :sswitch_0
    return-object p0

    .line 723
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loiq;->a:Ljava/lang/String;

    goto :goto_0

    .line 727
    :sswitch_2
    iget-object v0, p0, Loiq;->b:Loir;

    if-nez v0, :cond_2

    .line 728
    new-instance v0, Loir;

    invoke-direct {v0}, Loir;-><init>()V

    iput-object v0, p0, Loiq;->b:Loir;

    .line 730
    :cond_2
    iget-object v0, p0, Loiq;->b:Loir;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 734
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 735
    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 737
    :cond_3
    iput v0, p0, Loiq;->c:I

    goto :goto_0

    .line 739
    :cond_4
    iput v2, p0, Loiq;->c:I

    goto :goto_0

    .line 744
    :sswitch_4
    iget-object v0, p0, Loiq;->d:Loip;

    if-nez v0, :cond_5

    .line 745
    new-instance v0, Loip;

    invoke-direct {v0}, Loip;-><init>()V

    iput-object v0, p0, Loiq;->d:Loip;

    .line 747
    :cond_5
    iget-object v0, p0, Loiq;->d:Loip;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 708
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 662
    iget-object v0, p0, Loiq;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 663
    const/4 v0, 0x1

    iget-object v1, p0, Loiq;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 665
    :cond_0
    iget-object v0, p0, Loiq;->b:Loir;

    if-eqz v0, :cond_1

    .line 666
    const/4 v0, 0x2

    iget-object v1, p0, Loiq;->b:Loir;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 668
    :cond_1
    iget v0, p0, Loiq;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 669
    const/4 v0, 0x3

    iget v1, p0, Loiq;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 671
    :cond_2
    iget-object v0, p0, Loiq;->d:Loip;

    if-eqz v0, :cond_3

    .line 672
    const/4 v0, 0x4

    iget-object v1, p0, Loiq;->d:Loip;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 674
    :cond_3
    iget-object v0, p0, Loiq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 676
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 642
    invoke-virtual {p0, p1}, Loiq;->a(Loxn;)Loiq;

    move-result-object v0

    return-object v0
.end method
