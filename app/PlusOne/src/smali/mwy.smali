.class public final Lmwy;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/Boolean;

.field private c:I

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 5141
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5163
    iput v0, p0, Lmwy;->a:I

    .line 5168
    iput v0, p0, Lmwy;->c:I

    .line 5141
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 5223
    const/4 v0, 0x0

    .line 5224
    iget v1, p0, Lmwy;->a:I

    if-eq v1, v3, :cond_0

    .line 5225
    const/4 v0, 0x1

    iget v1, p0, Lmwy;->a:I

    .line 5226
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5228
    :cond_0
    iget-object v1, p0, Lmwy;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 5229
    const/4 v1, 0x2

    iget-object v2, p0, Lmwy;->b:Ljava/lang/Boolean;

    .line 5230
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5232
    :cond_1
    iget v1, p0, Lmwy;->c:I

    if-eq v1, v3, :cond_2

    .line 5233
    const/4 v1, 0x3

    iget v2, p0, Lmwy;->c:I

    .line 5234
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5236
    :cond_2
    iget-object v1, p0, Lmwy;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 5237
    const/4 v1, 0x4

    iget-object v2, p0, Lmwy;->d:Ljava/lang/Boolean;

    .line 5238
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5240
    :cond_3
    iget-object v1, p0, Lmwy;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 5241
    const/4 v1, 0x5

    iget-object v2, p0, Lmwy;->e:Ljava/lang/Boolean;

    .line 5242
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5244
    :cond_4
    iget-object v1, p0, Lmwy;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 5245
    const/4 v1, 0x6

    iget-object v2, p0, Lmwy;->f:Ljava/lang/String;

    .line 5246
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5248
    :cond_5
    iget-object v1, p0, Lmwy;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 5249
    const/4 v1, 0x7

    iget-object v2, p0, Lmwy;->g:Ljava/lang/Integer;

    .line 5250
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5252
    :cond_6
    iget-object v1, p0, Lmwy;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 5253
    const/16 v1, 0x8

    iget-object v2, p0, Lmwy;->h:Ljava/lang/Integer;

    .line 5254
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5256
    :cond_7
    iget-object v1, p0, Lmwy;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 5257
    const/16 v1, 0x9

    iget-object v2, p0, Lmwy;->i:Ljava/lang/Boolean;

    .line 5258
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5260
    :cond_8
    iget-object v1, p0, Lmwy;->j:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 5261
    const/16 v1, 0xa

    iget-object v2, p0, Lmwy;->j:Ljava/lang/String;

    .line 5262
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5264
    :cond_9
    iget-object v1, p0, Lmwy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5265
    iput v0, p0, Lmwy;->ai:I

    .line 5266
    return v0
.end method

.method public a(Loxn;)Lmwy;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5274
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5275
    sparse-switch v0, :sswitch_data_0

    .line 5279
    iget-object v1, p0, Lmwy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5280
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwy;->ah:Ljava/util/List;

    .line 5283
    :cond_1
    iget-object v1, p0, Lmwy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5285
    :sswitch_0
    return-object p0

    .line 5290
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5291
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-ne v0, v4, :cond_3

    .line 5294
    :cond_2
    iput v0, p0, Lmwy;->a:I

    goto :goto_0

    .line 5296
    :cond_3
    iput v2, p0, Lmwy;->a:I

    goto :goto_0

    .line 5301
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwy;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 5305
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5306
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-ne v0, v1, :cond_5

    .line 5316
    :cond_4
    iput v0, p0, Lmwy;->c:I

    goto :goto_0

    .line 5318
    :cond_5
    iput v2, p0, Lmwy;->c:I

    goto :goto_0

    .line 5323
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwy;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 5327
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwy;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 5331
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwy;->f:Ljava/lang/String;

    goto :goto_0

    .line 5335
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwy;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 5339
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmwy;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 5343
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwy;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 5347
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwy;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 5275
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 5187
    iget v0, p0, Lmwy;->a:I

    if-eq v0, v2, :cond_0

    .line 5188
    const/4 v0, 0x1

    iget v1, p0, Lmwy;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5190
    :cond_0
    iget-object v0, p0, Lmwy;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 5191
    const/4 v0, 0x2

    iget-object v1, p0, Lmwy;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5193
    :cond_1
    iget v0, p0, Lmwy;->c:I

    if-eq v0, v2, :cond_2

    .line 5194
    const/4 v0, 0x3

    iget v1, p0, Lmwy;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5196
    :cond_2
    iget-object v0, p0, Lmwy;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 5197
    const/4 v0, 0x4

    iget-object v1, p0, Lmwy;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5199
    :cond_3
    iget-object v0, p0, Lmwy;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 5200
    const/4 v0, 0x5

    iget-object v1, p0, Lmwy;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5202
    :cond_4
    iget-object v0, p0, Lmwy;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 5203
    const/4 v0, 0x6

    iget-object v1, p0, Lmwy;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5205
    :cond_5
    iget-object v0, p0, Lmwy;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 5206
    const/4 v0, 0x7

    iget-object v1, p0, Lmwy;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5208
    :cond_6
    iget-object v0, p0, Lmwy;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 5209
    const/16 v0, 0x8

    iget-object v1, p0, Lmwy;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5211
    :cond_7
    iget-object v0, p0, Lmwy;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 5212
    const/16 v0, 0x9

    iget-object v1, p0, Lmwy;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5214
    :cond_8
    iget-object v0, p0, Lmwy;->j:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 5215
    const/16 v0, 0xa

    iget-object v1, p0, Lmwy;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5217
    :cond_9
    iget-object v0, p0, Lmwy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5219
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5137
    invoke-virtual {p0, p1}, Lmwy;->a(Loxn;)Lmwy;

    move-result-object v0

    return-object v0
.end method
