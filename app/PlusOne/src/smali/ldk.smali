.class public final Lldk;
.super Lldq;
.source "PG"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 27
    invoke-direct {p0, p1, v5}, Lldq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    sget-object v0, Lldk;->z:Llct;

    iget v0, v0, Llct;->m:I

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, v5, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lldk;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lldk;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lldk;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04b0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lldk;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v0, v0, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v1, p0, Lldk;->a:Landroid/widget/TextView;

    const/16 v2, 0x15

    invoke-static {p1, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, v5, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lldk;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lldk;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lldk;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lldk;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v4, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lldk;->b:Landroid/widget/TextView;

    const/16 v1, 0xa

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    iget-object v0, p0, Lldk;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lldk;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lldk;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lldk;->addView(Landroid/view/View;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected a(IIII)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    sget-object v0, Lldk;->z:Llct;

    iget v0, v0, Llct;->m:I

    .line 68
    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    .line 69
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 70
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 72
    iget-object v2, p0, Lldk;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 73
    iget-object v2, p0, Lldk;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 75
    iget-object v0, p0, Lldk;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    iget-object v1, p0, Lldk;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lldk;->n:I

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 115
    invoke-super {p0}, Lldq;->a()V

    .line 117
    iput-object v0, p0, Lldk;->a:Landroid/widget/TextView;

    .line 118
    iput-object v0, p0, Lldk;->b:Landroid/widget/TextView;

    .line 119
    return-void
.end method

.method protected a(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 83
    iget-object v0, p0, Lldk;->a:Landroid/widget/TextView;

    iget v1, p0, Lldk;->m:I

    iget-object v2, p0, Lldk;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 86
    iget-object v0, p0, Lldk;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 88
    iget-object v1, p0, Lldk;->b:Landroid/widget/TextView;

    iget v2, p0, Lldk;->m:I

    iget-object v3, p0, Lldk;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 89
    return-void
.end method

.method protected o()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lldk;->z:Llct;

    iget-object v0, v0, Llct;->B:Landroid/graphics/Rect;

    return-object v0
.end method
