.class public final Lmtp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 12750
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12767
    iput v0, p0, Lmtp;->b:I

    .line 12770
    iput v0, p0, Lmtp;->c:I

    .line 12773
    iput v0, p0, Lmtp;->d:I

    .line 12750
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 12811
    const/4 v0, 0x0

    .line 12812
    iget-object v1, p0, Lmtp;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 12813
    const/4 v0, 0x1

    iget-object v1, p0, Lmtp;->a:Ljava/lang/Boolean;

    .line 12814
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 12816
    :cond_0
    iget v1, p0, Lmtp;->b:I

    if-eq v1, v3, :cond_1

    .line 12817
    const/4 v1, 0x2

    iget v2, p0, Lmtp;->b:I

    .line 12818
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12820
    :cond_1
    iget v1, p0, Lmtp;->c:I

    if-eq v1, v3, :cond_2

    .line 12821
    const/4 v1, 0x3

    iget v2, p0, Lmtp;->c:I

    .line 12822
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12824
    :cond_2
    iget v1, p0, Lmtp;->d:I

    if-eq v1, v3, :cond_3

    .line 12825
    const/4 v1, 0x4

    iget v2, p0, Lmtp;->d:I

    .line 12826
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12828
    :cond_3
    iget-object v1, p0, Lmtp;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 12829
    const/4 v1, 0x5

    iget-object v2, p0, Lmtp;->e:Ljava/lang/Boolean;

    .line 12830
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12832
    :cond_4
    iget-object v1, p0, Lmtp;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 12833
    const/4 v1, 0x6

    iget-object v2, p0, Lmtp;->f:Ljava/lang/Boolean;

    .line 12834
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12836
    :cond_5
    iget-object v1, p0, Lmtp;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 12837
    const/4 v1, 0x7

    iget-object v2, p0, Lmtp;->g:Ljava/lang/Boolean;

    .line 12838
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12840
    :cond_6
    iget-object v1, p0, Lmtp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12841
    iput v0, p0, Lmtp;->ai:I

    .line 12842
    return v0
.end method

.method public a(Loxn;)Lmtp;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12850
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12851
    sparse-switch v0, :sswitch_data_0

    .line 12855
    iget-object v1, p0, Lmtp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12856
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtp;->ah:Ljava/util/List;

    .line 12859
    :cond_1
    iget-object v1, p0, Lmtp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12861
    :sswitch_0
    return-object p0

    .line 12866
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtp;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 12870
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 12871
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-ne v0, v4, :cond_3

    .line 12874
    :cond_2
    iput v0, p0, Lmtp;->b:I

    goto :goto_0

    .line 12876
    :cond_3
    iput v2, p0, Lmtp;->b:I

    goto :goto_0

    .line 12881
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 12882
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-ne v0, v4, :cond_5

    .line 12885
    :cond_4
    iput v0, p0, Lmtp;->c:I

    goto :goto_0

    .line 12887
    :cond_5
    iput v2, p0, Lmtp;->c:I

    goto :goto_0

    .line 12892
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 12893
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-ne v0, v4, :cond_7

    .line 12896
    :cond_6
    iput v0, p0, Lmtp;->d:I

    goto :goto_0

    .line 12898
    :cond_7
    iput v2, p0, Lmtp;->d:I

    goto :goto_0

    .line 12903
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtp;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 12907
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtp;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 12911
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtp;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 12851
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 12784
    iget-object v0, p0, Lmtp;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 12785
    const/4 v0, 0x1

    iget-object v1, p0, Lmtp;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12787
    :cond_0
    iget v0, p0, Lmtp;->b:I

    if-eq v0, v2, :cond_1

    .line 12788
    const/4 v0, 0x2

    iget v1, p0, Lmtp;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 12790
    :cond_1
    iget v0, p0, Lmtp;->c:I

    if-eq v0, v2, :cond_2

    .line 12791
    const/4 v0, 0x3

    iget v1, p0, Lmtp;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 12793
    :cond_2
    iget v0, p0, Lmtp;->d:I

    if-eq v0, v2, :cond_3

    .line 12794
    const/4 v0, 0x4

    iget v1, p0, Lmtp;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 12796
    :cond_3
    iget-object v0, p0, Lmtp;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 12797
    const/4 v0, 0x5

    iget-object v1, p0, Lmtp;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12799
    :cond_4
    iget-object v0, p0, Lmtp;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 12800
    const/4 v0, 0x6

    iget-object v1, p0, Lmtp;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12802
    :cond_5
    iget-object v0, p0, Lmtp;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 12803
    const/4 v0, 0x7

    iget-object v1, p0, Lmtp;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12805
    :cond_6
    iget-object v0, p0, Lmtp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12807
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12746
    invoke-virtual {p0, p1}, Lmtp;->a(Loxn;)Lmtp;

    move-result-object v0

    return-object v0
.end method
