.class public final Lpes;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpyf;

.field private b:Ljava/lang/String;

.field private c:Lper;

.field private d:I

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v0, p0, Lpes;->a:Lpyf;

    .line 18
    iput-object v0, p0, Lpes;->c:Lper;

    .line 21
    const/high16 v0, -0x80000000

    iput v0, p0, Lpes;->d:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 50
    iget-object v1, p0, Lpes;->a:Lpyf;

    if-eqz v1, :cond_0

    .line 51
    const/4 v0, 0x1

    iget-object v1, p0, Lpes;->a:Lpyf;

    .line 52
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 54
    :cond_0
    iget-object v1, p0, Lpes;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 55
    const/4 v1, 0x2

    iget-object v2, p0, Lpes;->b:Ljava/lang/String;

    .line 56
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_1
    iget-object v1, p0, Lpes;->c:Lper;

    if-eqz v1, :cond_2

    .line 59
    const/4 v1, 0x3

    iget-object v2, p0, Lpes;->c:Lper;

    .line 60
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_2
    iget v1, p0, Lpes;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 63
    const/4 v1, 0x4

    iget v2, p0, Lpes;->d:I

    .line 64
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_3
    iget-object v1, p0, Lpes;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 67
    const/4 v1, 0x5

    iget-object v2, p0, Lpes;->e:Ljava/lang/String;

    .line 68
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_4
    iget-object v1, p0, Lpes;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    iput v0, p0, Lpes;->ai:I

    .line 72
    return v0
.end method

.method public a(Loxn;)Lpes;
    .locals 2

    .prologue
    .line 80
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 81
    sparse-switch v0, :sswitch_data_0

    .line 85
    iget-object v1, p0, Lpes;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 86
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpes;->ah:Ljava/util/List;

    .line 89
    :cond_1
    iget-object v1, p0, Lpes;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    :sswitch_0
    return-object p0

    .line 96
    :sswitch_1
    iget-object v0, p0, Lpes;->a:Lpyf;

    if-nez v0, :cond_2

    .line 97
    new-instance v0, Lpyf;

    invoke-direct {v0}, Lpyf;-><init>()V

    iput-object v0, p0, Lpes;->a:Lpyf;

    .line 99
    :cond_2
    iget-object v0, p0, Lpes;->a:Lpyf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 103
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpes;->b:Ljava/lang/String;

    goto :goto_0

    .line 107
    :sswitch_3
    iget-object v0, p0, Lpes;->c:Lper;

    if-nez v0, :cond_3

    .line 108
    new-instance v0, Lper;

    invoke-direct {v0}, Lper;-><init>()V

    iput-object v0, p0, Lpes;->c:Lper;

    .line 110
    :cond_3
    iget-object v0, p0, Lpes;->c:Lper;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 114
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 115
    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 119
    :cond_4
    iput v0, p0, Lpes;->d:I

    goto :goto_0

    .line 121
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Lpes;->d:I

    goto :goto_0

    .line 126
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpes;->e:Ljava/lang/String;

    goto :goto_0

    .line 81
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lpes;->a:Lpyf;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v1, p0, Lpes;->a:Lpyf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_0
    iget-object v0, p0, Lpes;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Lpes;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 34
    :cond_1
    iget-object v0, p0, Lpes;->c:Lper;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x3

    iget-object v1, p0, Lpes;->c:Lper;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_2
    iget v0, p0, Lpes;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 38
    const/4 v0, 0x4

    iget v1, p0, Lpes;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 40
    :cond_3
    iget-object v0, p0, Lpes;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 41
    const/4 v0, 0x5

    iget-object v1, p0, Lpes;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 43
    :cond_4
    iget-object v0, p0, Lpes;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 45
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpes;->a(Loxn;)Lpes;

    move-result-object v0

    return-object v0
.end method
