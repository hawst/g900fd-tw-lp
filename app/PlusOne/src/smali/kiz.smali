.class public final Lkiz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lkiz;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/text/style/URLSpan;

.field private final b:I

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lkja;

    invoke-direct {v0}, Lkja;-><init>()V

    sput-object v0, Lkiz;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-class v0, Landroid/text/style/URLSpan;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/text/style/URLSpan;

    iput-object v0, p0, Lkiz;->a:Landroid/text/style/URLSpan;

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lkiz;->b:I

    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lkiz;->c:I

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/text/style/URLSpan;II)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lkiz;->a:Landroid/text/style/URLSpan;

    .line 17
    iput p2, p0, Lkiz;->b:I

    .line 18
    iput p3, p0, Lkiz;->c:I

    .line 19
    return-void
.end method


# virtual methods
.method public a()Landroid/text/style/URLSpan;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lkiz;->a:Landroid/text/style/URLSpan;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lkiz;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lkiz;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lkiz;->a:Landroid/text/style/URLSpan;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 35
    iget v0, p0, Lkiz;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 36
    iget v0, p0, Lkiz;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 37
    return-void
.end method
