.class public Lhvb;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static g:Landroid/view/animation/Interpolator;

.field private static h:Lhwd;


# instance fields
.field public a:Lhts;

.field public b:Landroid/text/StaticLayout;

.field public c:Lhvd;

.field public d:I

.field public e:Z

.field public f:I

.field private i:Z

.field private j:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lhvb;->g:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvb;->e:Z

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvb;->i:Z

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lhvb;->f:I

    .line 76
    new-instance v0, Lhvc;

    invoke-direct {v0, p0}, Lhvc;-><init>(Lhvb;)V

    iput-object v0, p0, Lhvb;->j:Ljava/lang/Runnable;

    .line 174
    return-void
.end method

.method static synthetic a(Lhvb;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lhvb;->e()V

    return-void
.end method

.method static synthetic d()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lhvb;->g:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 257
    iget-object v1, p0, Lhvb;->a:Lhts;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhvb;->a:Lhts;

    invoke-interface {v1}, Lhts;->a()I

    move-result v1

    if-nez v1, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    invoke-virtual {p0}, Lhvb;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lhvb;->h:Lhwd;

    const/16 v2, 0x19

    invoke-static {v1, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    .line 264
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 266
    iget-object v3, p0, Lhvb;->a:Lhts;

    iget v4, p0, Lhvb;->d:I

    invoke-interface {v3, v4}, Lhts;->c(I)Ljava/lang/String;

    move-result-object v3

    .line 269
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 270
    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 271
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x11

    invoke-virtual {v2, v4, v0, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 274
    sget-object v0, Lhvb;->h:Lhwd;

    iget-object v0, v0, Lhwd;->a:Lfo;

    invoke-virtual {v0, v3}, Lfo;->a(Ljava/lang/String;)Z

    move-result v0

    .line 279
    :goto_1
    iget-object v3, p0, Lhvb;->a:Lhts;

    iget v4, p0, Lhvb;->d:I

    invoke-interface {v3, v4}, Lhts;->d(I)Landroid/text/Spanned;

    move-result-object v3

    .line 280
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 281
    sget-object v4, Lhvb;->h:Lhwd;

    iget-object v4, v4, Lhwd;->a:Lfo;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lfo;->a(Ljava/lang/String;)Z

    move-result v4

    .line 283
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 286
    if-ne v0, v4, :cond_5

    .line 287
    const-string v0, " "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 293
    :cond_2
    :goto_2
    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 297
    :cond_3
    invoke-virtual {p0}, Lhvb;->getMeasuredWidth()I

    move-result v0

    sget-object v3, Lhvb;->h:Lhwd;

    iget v3, v3, Lhwd;->q:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 296
    invoke-static {v1, v2, v0, v3, v4}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lhvb;->b:Landroid/text/StaticLayout;

    goto :goto_0

    .line 276
    :cond_4
    const-string v3, "CardCommentsView"

    iget v4, p0, Lhvb;->d:I

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x35

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Received empty name for comment at index: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 289
    :cond_5
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2
.end method


# virtual methods
.method public a(Lhts;ILhvd;Z)I
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Lhvb;->c()V

    .line 192
    sget-object v0, Lhvb;->h:Lhwd;

    if-nez v0, :cond_0

    .line 193
    invoke-virtual {p0}, Lhvb;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 194
    invoke-static {v0}, Lhwd;->a(Landroid/content/Context;)Lhwd;

    move-result-object v0

    sput-object v0, Lhvb;->h:Lhwd;

    .line 197
    :cond_0
    iput-object p1, p0, Lhvb;->a:Lhts;

    .line 198
    iput-object p3, p0, Lhvb;->c:Lhvd;

    .line 199
    iget-object v0, p0, Lhvb;->a:Lhts;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhvb;->a:Lhts;

    invoke-interface {v0}, Lhts;->a()I

    move-result v0

    if-lez v0, :cond_1

    .line 202
    invoke-virtual {p0}, Lhvb;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lhvb;->h:Lhwd;

    const/16 v1, 0x19

    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    .line 203
    invoke-static {v0}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v0

    .line 204
    sget-object v1, Lhvb;->h:Lhwd;

    iget v1, v1, Lhwd;->q:I

    mul-int/2addr v0, v1

    sget-object v1, Lhvb;->h:Lhwd;

    iget v1, v1, Lhwd;->k:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    add-int/2addr p2, v0

    .line 207
    :cond_1
    iput-boolean p4, p0, Lhvb;->i:Z

    .line 208
    iget-boolean v0, p0, Lhvb;->i:Z

    if-eqz v0, :cond_2

    .line 209
    invoke-virtual {p0}, Lhvb;->b()V

    .line 211
    :cond_2
    return p2
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 216
    invoke-virtual {p0}, Lhvb;->c()V

    .line 217
    iput-object v0, p0, Lhvb;->a:Lhts;

    .line 218
    iput-object v0, p0, Lhvb;->b:Landroid/text/StaticLayout;

    .line 219
    iput-object v0, p0, Lhvb;->c:Lhvd;

    .line 220
    iput v1, p0, Lhvb;->d:I

    .line 221
    const/4 v0, -0x1

    iput v0, p0, Lhvb;->f:I

    .line 222
    iput-boolean v1, p0, Lhvb;->i:Z

    .line 223
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 314
    iput-boolean p1, p0, Lhvb;->i:Z

    .line 315
    iget-boolean v0, p0, Lhvb;->i:Z

    if-eqz v0, :cond_0

    .line 316
    invoke-virtual {p0}, Lhvb;->b()V

    .line 320
    :goto_0
    return-void

    .line 318
    :cond_0
    invoke-virtual {p0}, Lhvb;->c()V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 328
    iget-boolean v0, p0, Lhvb;->i:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lhvb;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 330
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvb;->a:Lhts;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lhvb;->a:Lhts;

    invoke-interface {v0}, Lhts;->a()I

    move-result v0

    .line 334
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 335
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvb;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 338
    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 344
    iget v0, p0, Lhvb;->f:I

    if-eq v0, v2, :cond_1

    .line 345
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvb;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 346
    invoke-virtual {p0}, Lhvb;->clearAnimation()V

    .line 347
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lhvb;->setAlpha(F)V

    .line 348
    :cond_0
    iput v2, p0, Lhvb;->f:I

    .line 350
    :cond_1
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 227
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 228
    invoke-virtual {p0}, Lhvb;->b()V

    .line 229
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 233
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 234
    invoke-virtual {p0}, Lhvb;->c()V

    .line 235
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 302
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 304
    iget-object v0, p0, Lhvb;->a:Lhts;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvb;->a:Lhts;

    invoke-interface {v0}, Lhts;->a()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lhvb;->b:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    .line 306
    invoke-virtual {p0}, Lhvb;->getHeight()I

    move-result v0

    iget-object v1, p0, Lhvb;->b:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 307
    int-to-float v1, v0

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 308
    iget-object v1, p0, Lhvb;->b:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 309
    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 311
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 239
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 240
    const/4 v0, 0x0

    .line 242
    iget-object v2, p0, Lhvb;->a:Lhts;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhvb;->a:Lhts;

    invoke-interface {v2}, Lhts;->a()I

    move-result v2

    if-lez v2, :cond_0

    .line 244
    invoke-virtual {p0}, Lhvb;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lhvb;->h:Lhwd;

    const/16 v2, 0x19

    invoke-static {v0, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    .line 245
    invoke-static {v0}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v0

    .line 246
    sget-object v2, Lhvb;->h:Lhwd;

    iget v2, v2, Lhwd;->q:I

    mul-int/2addr v0, v2

    sget-object v2, Lhvb;->h:Lhwd;

    iget v2, v2, Lhwd;->k:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 249
    :cond_0
    invoke-virtual {p0, v1, v0}, Lhvb;->setMeasuredDimension(II)V

    .line 250
    invoke-direct {p0}, Lhvb;->e()V

    .line 251
    return-void
.end method
