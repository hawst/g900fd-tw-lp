.class public final Lpwm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpwm;


# instance fields
.field private b:[Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:[Lpwl;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    new-array v0, v0, [Lpwm;

    sput-object v0, Lpwm;->a:[Lpwm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 441
    invoke-direct {p0}, Loxq;-><init>()V

    .line 444
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpwm;->b:[Ljava/lang/String;

    .line 449
    sget-object v0, Lpwl;->a:[Lpwl;

    iput-object v0, p0, Lpwm;->d:[Lpwl;

    .line 441
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 485
    .line 486
    iget-object v0, p0, Lpwm;->b:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lpwm;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 488
    iget-object v3, p0, Lpwm;->b:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 490
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 488
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 493
    :cond_0
    iget-object v0, p0, Lpwm;->b:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 495
    :goto_1
    iget-object v2, p0, Lpwm;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 496
    const/4 v2, 0x2

    iget-object v3, p0, Lpwm;->c:Ljava/lang/String;

    .line 497
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 499
    :cond_1
    iget-object v2, p0, Lpwm;->d:[Lpwl;

    if-eqz v2, :cond_3

    .line 500
    iget-object v2, p0, Lpwm;->d:[Lpwl;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 501
    if-eqz v4, :cond_2

    .line 502
    const/4 v5, 0x3

    .line 503
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 500
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 507
    :cond_3
    iget-object v1, p0, Lpwm;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 508
    const/4 v1, 0x4

    iget-object v2, p0, Lpwm;->e:Ljava/lang/Boolean;

    .line 509
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 511
    :cond_4
    iget-object v1, p0, Lpwm;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 512
    const/4 v1, 0x5

    iget-object v2, p0, Lpwm;->f:Ljava/lang/Boolean;

    .line 513
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 515
    :cond_5
    iget-object v1, p0, Lpwm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 516
    iput v0, p0, Lpwm;->ai:I

    .line 517
    return v0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lpwm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 525
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 526
    sparse-switch v0, :sswitch_data_0

    .line 530
    iget-object v2, p0, Lpwm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 531
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpwm;->ah:Ljava/util/List;

    .line 534
    :cond_1
    iget-object v2, p0, Lpwm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 536
    :sswitch_0
    return-object p0

    .line 541
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 542
    iget-object v0, p0, Lpwm;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 543
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 544
    iget-object v3, p0, Lpwm;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 545
    iput-object v2, p0, Lpwm;->b:[Ljava/lang/String;

    .line 546
    :goto_1
    iget-object v2, p0, Lpwm;->b:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 547
    iget-object v2, p0, Lpwm;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 548
    invoke-virtual {p1}, Loxn;->a()I

    .line 546
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 551
    :cond_2
    iget-object v2, p0, Lpwm;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 555
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpwm;->c:Ljava/lang/String;

    goto :goto_0

    .line 559
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 560
    iget-object v0, p0, Lpwm;->d:[Lpwl;

    if-nez v0, :cond_4

    move v0, v1

    .line 561
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lpwl;

    .line 562
    iget-object v3, p0, Lpwm;->d:[Lpwl;

    if-eqz v3, :cond_3

    .line 563
    iget-object v3, p0, Lpwm;->d:[Lpwl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 565
    :cond_3
    iput-object v2, p0, Lpwm;->d:[Lpwl;

    .line 566
    :goto_3
    iget-object v2, p0, Lpwm;->d:[Lpwl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 567
    iget-object v2, p0, Lpwm;->d:[Lpwl;

    new-instance v3, Lpwl;

    invoke-direct {v3}, Lpwl;-><init>()V

    aput-object v3, v2, v0

    .line 568
    iget-object v2, p0, Lpwm;->d:[Lpwl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 569
    invoke-virtual {p1}, Loxn;->a()I

    .line 566
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 560
    :cond_4
    iget-object v0, p0, Lpwm;->d:[Lpwl;

    array-length v0, v0

    goto :goto_2

    .line 572
    :cond_5
    iget-object v2, p0, Lpwm;->d:[Lpwl;

    new-instance v3, Lpwl;

    invoke-direct {v3}, Lpwl;-><init>()V

    aput-object v3, v2, v0

    .line 573
    iget-object v2, p0, Lpwm;->d:[Lpwl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 577
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpwm;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 581
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpwm;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 526
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 458
    iget-object v1, p0, Lpwm;->b:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 459
    iget-object v2, p0, Lpwm;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 460
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 459
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 463
    :cond_0
    iget-object v1, p0, Lpwm;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 464
    const/4 v1, 0x2

    iget-object v2, p0, Lpwm;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 466
    :cond_1
    iget-object v1, p0, Lpwm;->d:[Lpwl;

    if-eqz v1, :cond_3

    .line 467
    iget-object v1, p0, Lpwm;->d:[Lpwl;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 468
    if-eqz v3, :cond_2

    .line 469
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 467
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 473
    :cond_3
    iget-object v0, p0, Lpwm;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 474
    const/4 v0, 0x4

    iget-object v1, p0, Lpwm;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 476
    :cond_4
    iget-object v0, p0, Lpwm;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 477
    const/4 v0, 0x5

    iget-object v1, p0, Lpwm;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 479
    :cond_5
    iget-object v0, p0, Lpwm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 481
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 437
    invoke-virtual {p0, p1}, Lpwm;->a(Loxn;)Lpwm;

    move-result-object v0

    return-object v0
.end method
