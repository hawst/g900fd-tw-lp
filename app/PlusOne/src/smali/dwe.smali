.class public final Ldwe;
.super Ldtw;
.source "PG"


# static fields
.field private static a:Ldwe;


# direct methods
.method private constructor <init>(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Ldtw;-><init>(Ljava/io/File;)V

    .line 44
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Ldwe;
    .locals 3

    .prologue
    .line 32
    const-class v1, Ldwe;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldwe;->a:Ldwe;

    if-nez v0, :cond_0

    .line 33
    const-string v0, "profile"

    invoke-static {p0, v0}, Ldtw;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 34
    new-instance v2, Ldwe;

    invoke-direct {v2, v0}, Ldwe;-><init>(Ljava/io/File;)V

    sput-object v2, Ldwe;->a:Ldwe;

    .line 36
    :cond_0
    sget-object v0, Ldwe;->a:Ldwe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 82
    if-nez p2, :cond_0

    .line 83
    const-string v0, "bad_type"

    .line 101
    :goto_0
    return-object v0

    .line 86
    :cond_0
    const/4 v0, 0x0

    .line 87
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 89
    :pswitch_0
    const-string v0, "people_in_common_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :pswitch_1
    const-string v0, "visible_circle_members_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :pswitch_2
    const-string v0, "owner_incoming_members_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ldwe;->b(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-super {p0, v0}, Ldtw;->a(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public a(ILjava/lang/String;Loxu;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ldwe;->b(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-super {p0, v0, p3}, Ldtw;->a(Ljava/lang/String;Loxu;)V

    .line 56
    return-void
.end method

.method public a(ILjava/lang/String;J)[B
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ldwe;->b(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-super {p0, v0, p3, p4}, Ldtw;->a(Ljava/lang/String;J)[B

    move-result-object v0

    return-object v0
.end method
