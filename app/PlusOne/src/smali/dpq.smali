.class public final Ldpq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Ldpq;->a:Landroid/content/Context;

    .line 49
    return-void
.end method


# virtual methods
.method public a()Ldpp;
    .locals 15

    .prologue
    .line 117
    new-instance v0, Ldpp;

    iget-object v1, p0, Ldpq;->a:Landroid/content/Context;

    iget v2, p0, Ldpq;->b:I

    iget-object v3, p0, Ldpq;->c:Ljava/lang/String;

    iget-object v4, p0, Ldpq;->d:Ljava/lang/String;

    iget v5, p0, Ldpq;->g:I

    iget-object v6, p0, Ldpq;->e:Ljava/lang/String;

    iget-object v7, p0, Ldpq;->f:Ljava/lang/String;

    iget-object v8, p0, Ldpq;->h:Ljava/util/ArrayList;

    iget-object v9, p0, Ldpq;->i:Ljava/util/ArrayList;

    iget-object v10, p0, Ldpq;->j:Ljava/util/ArrayList;

    iget-object v11, p0, Ldpq;->k:Ljava/util/ArrayList;

    iget-boolean v12, p0, Ldpq;->l:Z

    iget-object v13, p0, Ldpq;->m:Ljava/lang/String;

    iget-object v14, p0, Ldpq;->n:Ljava/lang/String;

    invoke-direct/range {v0 .. v14}, Ldpp;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(I)Ldpq;
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Ldpq;->b:I

    .line 53
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ldpq;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Ldpq;->c:Ljava/lang/String;

    .line 58
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Ldpq;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ldpq;"
        }
    .end annotation

    .prologue
    .line 82
    iput-object p1, p0, Ldpq;->h:Ljava/util/ArrayList;

    .line 83
    return-object p0
.end method

.method public a(Z)Ldpq;
    .locals 0

    .prologue
    .line 102
    iput-boolean p1, p0, Ldpq;->l:Z

    .line 103
    return-object p0
.end method

.method public b(I)Ldpq;
    .locals 0

    .prologue
    .line 67
    iput p1, p0, Ldpq;->g:I

    .line 68
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ldpq;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Ldpq;->d:Ljava/lang/String;

    .line 63
    return-object p0
.end method

.method public b(Ljava/util/ArrayList;)Ldpq;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ldpq;"
        }
    .end annotation

    .prologue
    .line 87
    iput-object p1, p0, Ldpq;->i:Ljava/util/ArrayList;

    .line 88
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ldpq;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Ldpq;->e:Ljava/lang/String;

    .line 73
    return-object p0
.end method

.method public c(Ljava/util/ArrayList;)Ldpq;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ldpq;"
        }
    .end annotation

    .prologue
    .line 92
    iput-object p1, p0, Ldpq;->j:Ljava/util/ArrayList;

    .line 93
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ldpq;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Ldpq;->f:Ljava/lang/String;

    .line 78
    return-object p0
.end method

.method public d(Ljava/util/ArrayList;)Ldpq;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ldpq;"
        }
    .end annotation

    .prologue
    .line 97
    iput-object p1, p0, Ldpq;->k:Ljava/util/ArrayList;

    .line 98
    return-object p0
.end method

.method public e(Ljava/lang/String;)Ldpq;
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Ldpq;->m:Ljava/lang/String;

    .line 108
    return-object p0
.end method

.method public f(Ljava/lang/String;)Ldpq;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Ldpq;->n:Ljava/lang/String;

    .line 113
    return-object p0
.end method
