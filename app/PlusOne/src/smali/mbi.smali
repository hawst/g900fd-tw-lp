.class public final Lmbi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmxj;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30211
    invoke-direct {p0}, Loxq;-><init>()V

    .line 30214
    iput-object v0, p0, Lmbi;->apiHeader:Llyq;

    .line 30217
    iput-object v0, p0, Lmbi;->a:Lmxj;

    .line 30211
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 30234
    const/4 v0, 0x0

    .line 30235
    iget-object v1, p0, Lmbi;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 30236
    const/4 v0, 0x1

    iget-object v1, p0, Lmbi;->apiHeader:Llyq;

    .line 30237
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 30239
    :cond_0
    iget-object v1, p0, Lmbi;->a:Lmxj;

    if-eqz v1, :cond_1

    .line 30240
    const/4 v1, 0x2

    iget-object v2, p0, Lmbi;->a:Lmxj;

    .line 30241
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30243
    :cond_1
    iget-object v1, p0, Lmbi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30244
    iput v0, p0, Lmbi;->ai:I

    .line 30245
    return v0
.end method

.method public a(Loxn;)Lmbi;
    .locals 2

    .prologue
    .line 30253
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 30254
    sparse-switch v0, :sswitch_data_0

    .line 30258
    iget-object v1, p0, Lmbi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 30259
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbi;->ah:Ljava/util/List;

    .line 30262
    :cond_1
    iget-object v1, p0, Lmbi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30264
    :sswitch_0
    return-object p0

    .line 30269
    :sswitch_1
    iget-object v0, p0, Lmbi;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 30270
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmbi;->apiHeader:Llyq;

    .line 30272
    :cond_2
    iget-object v0, p0, Lmbi;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30276
    :sswitch_2
    iget-object v0, p0, Lmbi;->a:Lmxj;

    if-nez v0, :cond_3

    .line 30277
    new-instance v0, Lmxj;

    invoke-direct {v0}, Lmxj;-><init>()V

    iput-object v0, p0, Lmbi;->a:Lmxj;

    .line 30279
    :cond_3
    iget-object v0, p0, Lmbi;->a:Lmxj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 30254
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 30222
    iget-object v0, p0, Lmbi;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 30223
    const/4 v0, 0x1

    iget-object v1, p0, Lmbi;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30225
    :cond_0
    iget-object v0, p0, Lmbi;->a:Lmxj;

    if-eqz v0, :cond_1

    .line 30226
    const/4 v0, 0x2

    iget-object v1, p0, Lmbi;->a:Lmxj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30228
    :cond_1
    iget-object v0, p0, Lmbi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 30230
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 30207
    invoke-virtual {p0, p1}, Lmbi;->a(Loxn;)Lmbi;

    move-result-object v0

    return-object v0
.end method
