.class public Llcn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkzh;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Llcn;->a:Landroid/content/Context;

    .line 22
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkzs;)V
    .locals 8

    .prologue
    .line 50
    if-eqz p5, :cond_1

    .line 51
    invoke-virtual {p5}, Lkzs;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    invoke-virtual {p5}, Lkzs;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    iget-object v0, p0, Llcn;->a:Landroid/content/Context;

    const-class v1, Litj;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Litj;

    .line 55
    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {p5}, Lkzs;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p5}, Lkzs;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p3

    .line 56
    invoke-interface/range {v0 .. v7}, Litj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Llcn;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 70
    iget-object v1, p0, Llcn;->a:Landroid/content/Context;

    invoke-static {v1, v0, p4, p2, p1}, Litm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkzs;)V
    .locals 8

    .prologue
    .line 27
    if-eqz p4, :cond_0

    .line 29
    invoke-virtual {p4}, Lkzs;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 30
    invoke-virtual {p4}, Lkzs;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 32
    iget-object v0, p0, Llcn;->a:Landroid/content/Context;

    const-class v1, Litj;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Litj;

    .line 33
    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p4}, Lkzs;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p4}, Lkzs;->b()Ljava/lang/String;

    move-result-object v4

    .line 36
    invoke-virtual {p4}, Lkzs;->c()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    .line 34
    invoke-interface/range {v0 .. v7}, Litj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-virtual {p4}, Lkzs;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Llcn;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 41
    iget-object v1, p0, Llcn;->a:Landroid/content/Context;

    invoke-virtual {p4}, Lkzs;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2, p2, p1}, Litm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
