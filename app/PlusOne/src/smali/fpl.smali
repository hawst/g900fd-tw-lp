.class public final Lfpl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private final a:Lu;

.field private final b:Lfpm;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Lu;Llqr;Lfpm;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lfpl;->a:Lu;

    .line 26
    iput-object p3, p0, Lfpl;->b:Lfpm;

    .line 27
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lfpl;->a:Lu;

    invoke-virtual {v0}, Lu;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lfpl;->c:I

    .line 33
    if-eqz p1, :cond_0

    .line 34
    const-string v0, "orientation_change_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfpl;->d:I

    .line 35
    iget v0, p0, Lfpl;->c:I

    const-string v1, "orientation"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 36
    iget v0, p0, Lfpl;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfpl;->d:I

    .line 37
    iget-object v0, p0, Lfpl;->b:Lfpm;

    iget v1, p0, Lfpl;->c:I

    iget v1, p0, Lfpl;->d:I

    invoke-interface {v0, v1}, Lfpm;->d(I)V

    .line 40
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    const-string v0, "orientation"

    iget v1, p0, Lfpl;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 45
    const-string v0, "orientation_change_count"

    iget v1, p0, Lfpl;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 46
    return-void
.end method
