.class public final Lmvo;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/Integer;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11865
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11868
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lmvo;->a:[Ljava/lang/Integer;

    .line 11865
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 11894
    .line 11895
    iget-object v1, p0, Lmvo;->a:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmvo;->a:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 11897
    iget-object v2, p0, Lmvo;->a:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 11899
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 11897
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11902
    :cond_0
    iget-object v0, p0, Lmvo;->a:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 11904
    :cond_1
    iget-object v1, p0, Lmvo;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 11905
    const/4 v1, 0x2

    iget-object v2, p0, Lmvo;->b:Ljava/lang/Boolean;

    .line 11906
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11908
    :cond_2
    iget-object v1, p0, Lmvo;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 11909
    const/4 v1, 0x3

    iget-object v2, p0, Lmvo;->c:Ljava/lang/Boolean;

    .line 11910
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11912
    :cond_3
    iget-object v1, p0, Lmvo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11913
    iput v0, p0, Lmvo;->ai:I

    .line 11914
    return v0
.end method

.method public a(Loxn;)Lmvo;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 11922
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11923
    sparse-switch v0, :sswitch_data_0

    .line 11927
    iget-object v1, p0, Lmvo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11928
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvo;->ah:Ljava/util/List;

    .line 11931
    :cond_1
    iget-object v1, p0, Lmvo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11933
    :sswitch_0
    return-object p0

    .line 11938
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 11939
    iget-object v0, p0, Lmvo;->a:[Ljava/lang/Integer;

    array-length v0, v0

    .line 11940
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 11941
    iget-object v2, p0, Lmvo;->a:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 11942
    iput-object v1, p0, Lmvo;->a:[Ljava/lang/Integer;

    .line 11943
    :goto_1
    iget-object v1, p0, Lmvo;->a:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 11944
    iget-object v1, p0, Lmvo;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 11945
    invoke-virtual {p1}, Loxn;->a()I

    .line 11943
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 11948
    :cond_2
    iget-object v1, p0, Lmvo;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 11952
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmvo;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 11956
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmvo;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 11923
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 11877
    iget-object v0, p0, Lmvo;->a:[Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 11878
    iget-object v1, p0, Lmvo;->a:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 11879
    const/4 v4, 0x1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 11878
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11882
    :cond_0
    iget-object v0, p0, Lmvo;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 11883
    const/4 v0, 0x2

    iget-object v1, p0, Lmvo;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 11885
    :cond_1
    iget-object v0, p0, Lmvo;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 11886
    const/4 v0, 0x3

    iget-object v1, p0, Lmvo;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 11888
    :cond_2
    iget-object v0, p0, Lmvo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11890
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11861
    invoke-virtual {p0, p1}, Lmvo;->a(Loxn;)Lmvo;

    move-result-object v0

    return-object v0
.end method
