.class final Leok;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private synthetic a:Leog;


# direct methods
.method constructor <init>(Leog;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Leok;->a:Leog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 134
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 135
    if-eqz v1, :cond_1

    .line 136
    iget-object v2, p0, Leok;->a:Leog;

    invoke-static {v2}, Leog;->d(Leog;)Landroid/widget/RadioButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 142
    :cond_0
    :goto_0
    iget-object v2, p0, Leok;->a:Leog;

    if-nez v1, :cond_2

    :goto_1
    invoke-static {v2, v0}, Leog;->c(Leog;Z)V

    .line 144
    iget-object v0, p0, Leok;->a:Leog;

    invoke-static {v0}, Leog;->c(Leog;)V

    .line 145
    return-void

    .line 138
    :cond_1
    iget-object v2, p0, Leok;->a:Leog;

    invoke-static {v2}, Leog;->e(Leog;)I

    move-result v2

    iget-object v3, p0, Leok;->a:Leog;

    invoke-static {v3}, Leog;->f(Leog;)I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 139
    iget-object v2, p0, Leok;->a:Leog;

    iget-object v3, p0, Leok;->a:Leog;

    invoke-static {v3}, Leog;->e(Leog;)I

    move-result v3

    invoke-static {v2, v3}, Leog;->b(Leog;I)V

    goto :goto_0

    .line 142
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 130
    return-void
.end method
