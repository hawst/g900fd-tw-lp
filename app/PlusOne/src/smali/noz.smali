.class public final Lnoz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnoz;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1408
    const/4 v0, 0x0

    new-array v0, v0, [Lnoz;

    sput-object v0, Lnoz;->a:[Lnoz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1409
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1424
    const/high16 v0, -0x80000000

    iput v0, p0, Lnoz;->d:I

    .line 1409
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1444
    const/4 v0, 0x0

    .line 1445
    iget-object v1, p0, Lnoz;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1446
    const/4 v0, 0x1

    iget-object v1, p0, Lnoz;->b:Ljava/lang/String;

    .line 1447
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1449
    :cond_0
    iget-object v1, p0, Lnoz;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1450
    const/4 v1, 0x2

    iget-object v2, p0, Lnoz;->c:Ljava/lang/Boolean;

    .line 1451
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1453
    :cond_1
    iget v1, p0, Lnoz;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 1454
    const/4 v1, 0x3

    iget v2, p0, Lnoz;->d:I

    .line 1455
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1457
    :cond_2
    iget-object v1, p0, Lnoz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1458
    iput v0, p0, Lnoz;->ai:I

    .line 1459
    return v0
.end method

.method public a(Loxn;)Lnoz;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1467
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1468
    sparse-switch v0, :sswitch_data_0

    .line 1472
    iget-object v1, p0, Lnoz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1473
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnoz;->ah:Ljava/util/List;

    .line 1476
    :cond_1
    iget-object v1, p0, Lnoz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1478
    :sswitch_0
    return-object p0

    .line 1483
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnoz;->b:Ljava/lang/String;

    goto :goto_0

    .line 1487
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoz;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1491
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1492
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 1497
    :cond_2
    iput v0, p0, Lnoz;->d:I

    goto :goto_0

    .line 1499
    :cond_3
    iput v2, p0, Lnoz;->d:I

    goto :goto_0

    .line 1468
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1429
    iget-object v0, p0, Lnoz;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1430
    const/4 v0, 0x1

    iget-object v1, p0, Lnoz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1432
    :cond_0
    iget-object v0, p0, Lnoz;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1433
    const/4 v0, 0x2

    iget-object v1, p0, Lnoz;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1435
    :cond_1
    iget v0, p0, Lnoz;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 1436
    const/4 v0, 0x3

    iget v1, p0, Lnoz;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1438
    :cond_2
    iget-object v0, p0, Lnoz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1440
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1405
    invoke-virtual {p0, p1}, Lnoz;->a(Loxn;)Lnoz;

    move-result-object v0

    return-object v0
.end method
