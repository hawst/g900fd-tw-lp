.class public final Lkzu;
.super Lllq;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:I

.field f:F

.field g:I

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:J


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lllq;-><init>()V

    .line 37
    return-void
.end method

.method public constructor <init>(Lozy;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 45
    invoke-direct {p0}, Lllq;-><init>()V

    .line 46
    iget-object v0, p1, Lozy;->b:Ljava/lang/String;

    iput-object v0, p0, Lkzu;->a:Ljava/lang/String;

    .line 48
    iget-object v0, p1, Lozy;->d:Loya;

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p1, Lozy;->d:Loya;

    sget-object v1, Lpcg;->a:Loxr;

    .line 50
    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpcg;

    .line 52
    if-eqz v0, :cond_1

    .line 53
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 54
    iget-object v2, v0, Lpcg;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 55
    iget-object v2, v0, Lpcg;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    :cond_0
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkzu;->b:Ljava/lang/String;

    .line 58
    iget-object v0, v0, Lpcg;->b:Ljava/lang/String;

    iput-object v0, p0, Lkzu;->d:Ljava/lang/String;

    .line 62
    :cond_1
    iget-object v0, p1, Lozy;->c:Lpdi;

    if-eqz v0, :cond_2

    .line 63
    iget-object v0, p1, Lozy;->c:Lpdi;

    iget-object v0, v0, Lpdi;->a:Ljava/lang/String;

    iput-object v0, p0, Lkzu;->c:Ljava/lang/String;

    .line 66
    :cond_2
    iget-object v0, p1, Lozy;->g:Loya;

    if-eqz v0, :cond_3

    .line 67
    iget-object v0, p1, Lozy;->g:Loya;

    sget-object v1, Loyc;->a:Loxr;

    .line 68
    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyc;

    .line 70
    if-eqz v0, :cond_3

    .line 71
    iget-object v1, v0, Loyc;->b:Ljava/lang/Integer;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    iput v1, p0, Lkzu;->e:I

    .line 72
    iget-object v0, v0, Loyc;->c:Ljava/lang/Float;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    iput v0, p0, Lkzu;->f:F

    .line 76
    :cond_3
    iput p2, p0, Lkzu;->g:I

    .line 77
    iput-object p3, p0, Lkzu;->h:Ljava/lang/String;

    .line 78
    iget-object v0, p1, Lozy;->e:Ljava/lang/String;

    iput-object v0, p0, Lkzu;->i:Ljava/lang/String;

    .line 79
    iget-object v0, p1, Lozy;->f:Ljava/lang/String;

    iput-object v0, p0, Lkzu;->j:Ljava/lang/String;

    .line 80
    iput-object p4, p0, Lkzu;->k:Ljava/lang/String;

    .line 81
    iput-object p5, p0, Lkzu;->l:Ljava/lang/String;

    .line 82
    iput-wide p6, p0, Lkzu;->m:J

    .line 83
    return-void
.end method

.method public static a([B)Lkzu;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 135
    if-nez p0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-object v0

    .line 139
    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 140
    if-eqz v1, :cond_0

    new-instance v0, Lkzu;

    invoke-direct {v0}, Lkzu;-><init>()V

    invoke-static {v1}, Lkzu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzu;->a:Ljava/lang/String;

    invoke-static {v1}, Lkzu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzu;->b:Ljava/lang/String;

    invoke-static {v1}, Lkzu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzu;->c:Ljava/lang/String;

    invoke-static {v1}, Lkzu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzu;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v0, Lkzu;->e:I

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v2

    iput v2, v0, Lkzu;->f:F

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v0, Lkzu;->g:I

    invoke-static {v1}, Lkzu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzu;->h:Ljava/lang/String;

    invoke-static {v1}, Lkzu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzu;->i:Ljava/lang/String;

    invoke-static {v1}, Lkzu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzu;->j:Ljava/lang/String;

    invoke-static {v1}, Lkzu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzu;->k:Ljava/lang/String;

    invoke-static {v1}, Lkzu;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzu;->l:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    iput-wide v2, v0, Lkzu;->m:J

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lkzu;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lkzu;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lkzu;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lkzu;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lkzu;->e:I

    return v0
.end method

.method public f()F
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lkzu;->f:F

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lkzu;->g:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lkzu;->h:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lkzu;->i:Ljava/lang/String;

    return-object v0
.end method
