.class public final Ljvj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[I

.field private static h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static j:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x4

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 278
    const-string v1, "content://EsTileData/view"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sput-object v1, Ljvj;->a:Landroid/net/Uri;

    .line 280
    const-string v1, "content://EsTileData/tile"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sput-object v1, Ljvj;->b:Landroid/net/Uri;

    .line 459
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v0

    sput-object v1, Ljvj;->c:[Ljava/lang/String;

    .line 462
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "tile_id"

    aput-object v2, v1, v0

    sput-object v1, Ljvj;->d:[Ljava/lang/String;

    .line 466
    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "parent_id"

    aput-object v2, v1, v0

    const-string v2, "count(distinct photo_id)"

    aput-object v2, v1, v6

    sput-object v1, Ljvj;->e:[Ljava/lang/String;

    .line 471
    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "resume_token"

    aput-object v2, v1, v0

    const-string v2, "last_refresh_time"

    aput-object v2, v1, v6

    sput-object v1, Ljvj;->f:[Ljava/lang/String;

    .line 476
    new-array v1, v8, [I

    fill-array-data v1, :array_0

    sput-object v1, Ljvj;->g:[I

    .line 488
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 489
    sput-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 490
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const-wide/16 v2, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 491
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const-wide/16 v2, 0x4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v9, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 492
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x16

    const-wide/16 v4, 0x8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 493
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const-wide/16 v2, 0x10

    .line 494
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 493
    invoke-virtual {v1, v8, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 495
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/4 v2, 0x5

    const-wide/16 v4, 0x20

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 496
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/4 v2, 0x6

    const-wide/16 v4, 0x40

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 497
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/4 v2, 0x7

    const-wide/16 v4, 0x80

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 498
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x8

    const-wide/16 v4, 0x100

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 499
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x9

    const-wide/16 v4, 0x200

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 500
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0xa

    const-wide/16 v4, 0x400

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 501
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0xb

    const-wide/16 v4, 0x800

    .line 502
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 501
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 503
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0xc

    const-wide/16 v4, 0x1000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 504
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0xd

    const-wide/16 v4, 0x2000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 505
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0xe

    const-wide/16 v4, 0x4000

    .line 506
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 505
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 507
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0xf

    const-wide/32 v4, 0x8000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 508
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x10

    const-wide/32 v4, 0x10000

    .line 509
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 508
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 510
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x11

    const-wide/32 v4, 0x20000

    .line 511
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 510
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 512
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x12

    const-wide/32 v4, 0x40000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 513
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x13

    const-wide/32 v4, 0x80000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 514
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x14

    const-wide/32 v4, 0x100000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 515
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x15

    const-wide/32 v4, 0x200000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 516
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x17

    const-wide/32 v4, 0x400000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 517
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x18

    const-wide/32 v4, 0x800000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 518
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x19

    const-wide/32 v4, 0x1000000

    .line 519
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 518
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 520
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x1f

    const-wide v4, 0x2000000000L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 521
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x1e

    const-wide v4, 0x4000000000L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 522
    sget-object v1, Ljvj;->h:Landroid/util/SparseArray;

    const/16 v2, 0x20

    const-wide v4, 0x8000000000L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 524
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 525
    sput-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const-wide/32 v2, 0x2000000

    .line 526
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 525
    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 527
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const-wide/32 v2, 0x4000000

    .line 528
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 527
    invoke-virtual {v1, v6, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 529
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const-wide/32 v2, 0x8000000

    .line 530
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 529
    invoke-virtual {v1, v7, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 531
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const-wide/32 v2, 0x10000000

    .line 532
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 531
    invoke-virtual {v1, v9, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 533
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const-wide/32 v2, 0x20000000

    .line 534
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 533
    invoke-virtual {v1, v8, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 535
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const/4 v2, 0x5

    const-wide/32 v4, 0x40000000

    .line 536
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 535
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 537
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const/4 v2, 0x6

    const-wide v4, 0x80000000L

    .line 538
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 537
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 539
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const/16 v2, 0xb

    const-wide v4, 0x100000000L

    .line 540
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 539
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 541
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const/16 v2, 0xc

    const-wide v4, 0x200000000L

    .line 542
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 541
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 543
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const/16 v2, 0xd

    const-wide v4, 0x400000000L

    .line 544
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 543
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 545
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const/16 v2, 0x9

    const-wide v4, 0x800000000L

    .line 546
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 545
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 547
    sget-object v1, Ljvj;->i:Landroid/util/SparseArray;

    const/16 v2, 0x8

    const-wide v4, 0x1000000000L

    .line 548
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 547
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 550
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sput-object v1, Ljvj;->j:Ljava/lang/Long;

    .line 551
    sget-object v2, Ljvj;->g:[I

    move v1, v0

    :goto_0
    if-ge v1, v8, :cond_0

    aget v0, v2, v1

    .line 552
    sget-object v3, Ljvj;->j:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v3, Ljvj;->i:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v6

    or-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Ljvj;->j:Ljava/lang/Long;

    .line 551
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 554
    :cond_0
    return-void

    .line 476
    nop

    :array_0
    .array-data 4
        0xc
        0x4
        0xd
        0x2
    .end array-data
.end method

.method public static a(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 648
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 649
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "View ID must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 651
    :cond_0
    const-string v2, ":"

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    .line 652
    aget-object v2, v2, v0

    .line 653
    const-string v3, "best"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 672
    :goto_0
    return v0

    .line 655
    :cond_1
    const-string v0, "all"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 656
    const/4 v0, 0x1

    goto :goto_0

    .line 657
    :cond_2
    const-string v0, "search"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 658
    const/4 v0, 0x5

    goto :goto_0

    .line 659
    :cond_3
    const-string v0, "notification"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 660
    const/4 v0, 0x6

    goto :goto_0

    .line 661
    :cond_4
    const-string v0, "albums"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 662
    goto :goto_0

    .line 663
    :cond_5
    const-string v0, "album"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 664
    const/4 v0, 0x3

    goto :goto_0

    .line 665
    :cond_6
    const-string v0, "event"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 666
    const/4 v0, 0x4

    goto :goto_0

    .line 667
    :cond_7
    const-string v0, "trash"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 668
    const/4 v0, 0x7

    goto :goto_0

    .line 669
    :cond_8
    const-string v0, "story"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 670
    const/16 v0, 0x8

    goto :goto_0

    .line 672
    :cond_9
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static a(Lnxl;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 2527
    if-nez p0, :cond_0

    .line 2545
    :goto_0
    return v0

    .line 2530
    :cond_0
    iget v1, p0, Lnxl;->a:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2534
    :pswitch_0
    iget-object v0, p0, Lnxl;->b:Lnxm;

    if-eqz v0, :cond_1

    .line 2535
    const/4 v0, 0x3

    goto :goto_0

    .line 2532
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 2537
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2540
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 2543
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 2530
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnzx;JLjava/util/List;Ljava/lang/String;Ljava/util/Set;)J
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "[",
            "Lnzx;",
            "J",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 2062
    if-eqz p4, :cond_3

    move-object/from16 v0, p4

    array-length v2, v0

    move/from16 v17, v2

    .line 2063
    :goto_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 2065
    const/4 v2, 0x0

    move/from16 v18, v2

    move-wide/from16 v12, p5

    :goto_1
    move/from16 v0, v18

    move/from16 v1, v17

    if-ge v0, v1, :cond_4

    .line 2066
    aget-object v4, p4, v18

    .line 2068
    const/4 v2, 0x3

    const-string v3, "EsTileData"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljvj;->a(Lnzx;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2070
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    move-object/from16 v2, p0

    move/from16 v3, p1

    move-object/from16 v5, p8

    move-object/from16 v7, p9

    .line 2072
    invoke-static/range {v2 .. v7}, Ljvj;->a(Landroid/content/Context;ILnzx;Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/Set;)V

    .line 2073
    const-string v2, "view_id"

    move-object/from16 v0, p3

    invoke-virtual {v6, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2074
    const-string v2, "view_order"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2075
    const-string v2, "media_attr"

    invoke-virtual {v6, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2076
    const-wide/16 v8, 0x200

    or-long/2addr v2, v8

    .line 2077
    const-string v5, "media_attr"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2078
    const-wide/16 v2, 0x1

    add-long/2addr v12, v2

    .line 2086
    iget-object v2, v4, Lnzx;->b:Ljava/lang/String;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v2}, Ljvj;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v8, -0x1

    cmp-long v2, v2, v8

    if-nez v2, :cond_0

    const-string v2, "all_tiles"

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2088
    :cond_0
    if-eqz p7, :cond_1

    .line 2089
    iget-object v2, v4, Lnzx;->b:Ljava/lang/String;

    invoke-static {v2}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2092
    :cond_1
    iget-object v2, v4, Lnzx;->j:[Lnzx;

    if-eqz v2, :cond_2

    .line 2093
    iget-object v11, v4, Lnzx;->j:[Lnzx;

    const/4 v15, 0x0

    move-object/from16 v7, p0

    move/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v14, p7

    move-object/from16 v16, p9

    invoke-static/range {v7 .. v16}, Ljvj;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnzx;JLjava/util/List;Ljava/lang/String;Ljava/util/Set;)J

    move-result-wide v2

    add-long/2addr v12, v2

    .line 2065
    :cond_2
    add-int/lit8 v2, v18, 0x1

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2062
    :cond_3
    const/4 v2, 0x0

    move/from16 v17, v2

    goto/16 :goto_0

    .line 2097
    :cond_4
    sub-long v2, v12, p5

    return-wide v2
.end method

.method public static a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnzx;JLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)J
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "[",
            "Lnzx;",
            "J",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljvk;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1938
    if-eqz p4, :cond_a

    move-object/from16 v0, p4

    array-length v2, v0

    move/from16 v19, v2

    .line 1939
    :goto_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 1941
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    const/4 v2, 0x1

    move/from16 v20, v2

    .line 1942
    :goto_1
    invoke-static/range {p2 .. p3}, Ljvj;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v13

    .line 1944
    const/4 v2, 0x0

    move/from16 v21, v2

    move-wide/from16 v8, p5

    :goto_2
    move/from16 v0, v21

    move/from16 v1, v19

    if-ge v0, v1, :cond_14

    .line 1945
    aget-object v4, p4, v21

    .line 1947
    const/4 v2, 0x3

    const-string v3, "EsTileData"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljvj;->a(Lnzx;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v5}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1949
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 1951
    const-string v2, "view_id"

    move-object/from16 v0, p3

    invoke-virtual {v6, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1952
    const-string v2, "view_order"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1953
    if-eqz v20, :cond_c

    .line 1954
    const-string v2, "parent_id"

    move-object/from16 v0, p10

    invoke-virtual {v6, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    move-object/from16 v2, p0

    move/from16 v3, p1

    move-object/from16 v5, p9

    move-object/from16 v7, p11

    .line 1958
    invoke-static/range {v2 .. v7}, Ljvj;->a(Landroid/content/Context;ILnzx;Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/Set;)V

    .line 1962
    const/4 v2, 0x2

    :try_start_0
    const-string v3, "type"

    invoke-virtual {v6, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 1963
    const-string v2, "tile_id"

    invoke-virtual {v6, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1964
    const-string v3, "SELECT media_attr FROM all_tiles WHERE view_id = ? AND tile_id = ?  AND media_attr & 512 == 0"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v5, v7

    const/4 v7, 0x1

    aput-object v2, v5, v7

    move-object/from16 v0, p2

    invoke-static {v0, v3, v5}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    .line 1969
    const-string v5, "media_attr"

    invoke-virtual {v6, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 1970
    const-string v5, "media_attr"

    const-wide/32 v14, 0x100000

    and-long/2addr v2, v14

    or-long/2addr v2, v10

    .line 1971
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1970
    invoke-virtual {v6, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1977
    :cond_0
    :goto_4
    const-wide/16 v2, 0x1

    add-long v22, v8, v2

    .line 1985
    if-nez v21, :cond_d

    sget-object v2, Lnzr;->a:Loxr;

    .line 1986
    invoke-virtual {v4, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 1987
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v2, v4}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/Long;Lnzx;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    .line 1988
    :goto_5
    if-nez v20, :cond_e

    if-eqz v2, :cond_e

    const/4 v12, 0x1

    .line 1989
    :goto_6
    iget-object v9, v4, Lnzx;->b:Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object v10, v6

    invoke-static/range {v7 .. v12}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 1992
    if-eqz v13, :cond_16

    .line 1995
    invoke-static {v4}, Ljvj;->a(Lnzx;)Ljava/lang/String;

    move-result-object v2

    .line 1996
    invoke-virtual {v13, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 1997
    const-string v3, "media_attr"

    invoke-virtual {v6, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 1998
    const-wide/16 v10, 0x200

    or-long/2addr v8, v10

    .line 1999
    const-string v3, "media_attr"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v6, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2001
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v2}, Ljvj;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v3, v8, v10

    if-eqz v3, :cond_1

    const-string v3, "view_order"

    invoke-virtual {v6, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v3, "all_tiles"

    const-string v5, "_id = ? "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v6, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2002
    :cond_1
    if-eqz p7, :cond_2

    .line 2003
    invoke-static {v2}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2007
    :cond_2
    invoke-virtual {v13}, Ljava/util/HashSet;->size()I

    move-result v2

    if-nez v2, :cond_16

    .line 2008
    const/4 v2, 0x0

    move-object v5, v2

    .line 2013
    :goto_7
    if-eqz p7, :cond_3

    .line 2014
    iget-object v2, v4, Lnzx;->b:Ljava/lang/String;

    invoke-static {v2}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2017
    :cond_3
    if-eqz p8, :cond_5

    .line 2018
    sget-object v2, Lnzu;->a:Loxr;

    invoke-virtual {v4, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzu;

    if-eqz v2, :cond_4

    iget-object v3, v2, Lnzu;->b:Lnym;

    if-nez v3, :cond_f

    :cond_4
    const/4 v2, 0x0

    .line 2019
    :goto_8
    if-eqz v2, :cond_5

    .line 2020
    move-object/from16 v0, p8

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2024
    :cond_5
    iget-object v2, v4, Lnzx;->j:[Lnzx;

    if-eqz v2, :cond_15

    .line 2025
    const-string v2, "cluster_id"

    invoke-virtual {v6, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 2026
    iget-object v11, v4, Lnzx;->j:[Lnzx;

    const/16 v16, 0x0

    iget-object v2, v4, Lnzx;->c:Ljava/lang/String;

    move-object/from16 v7, p0

    move/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-wide/from16 v12, v22

    move-object/from16 v14, p7

    move-object/from16 v15, p8

    move-object/from16 v18, p11

    invoke-static/range {v7 .. v18}, Ljvj;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnzx;JLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)J

    move-result-wide v2

    add-long v2, v2, v22

    .line 2032
    :goto_9
    const-string v4, "cluster_id"

    invoke-virtual {v6, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2033
    const-string v7, "cluster_id"

    invoke-virtual {v6, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 2034
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 2035
    const-string v8, "cluster_count"

    invoke-virtual {v6, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2036
    const-string v8, "cluster_count"

    const-string v9, "cluster_count"

    .line 2037
    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    .line 2036
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2039
    :cond_6
    const-string v8, "title"

    invoke-virtual {v6, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2040
    const-string v8, "title"

    const-string v9, "title"

    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042
    :cond_7
    const-string v8, "acl"

    invoke-virtual {v6, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2043
    const-string v8, "acl"

    const-string v9, "acl"

    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2045
    :cond_8
    invoke-virtual {v7}, Landroid/content/ContentValues;->size()I

    move-result v8

    if-lez v8, :cond_9

    .line 2046
    const-string v8, "all_tiles"

    const-string v9, "cluster_id = ? AND type = ?"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    const/4 v4, 0x1

    const-string v11, "2"

    .line 2048
    aput-object v11, v10, v4

    .line 2046
    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v7, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1944
    :cond_9
    add-int/lit8 v4, v21, 0x1

    move/from16 v21, v4

    move-object v13, v5

    move-wide v8, v2

    goto/16 :goto_2

    .line 1938
    :cond_a
    const/4 v2, 0x0

    move/from16 v19, v2

    goto/16 :goto_0

    .line 1941
    :cond_b
    const/4 v2, 0x0

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1956
    :cond_c
    const-string v2, "parent_id"

    invoke-virtual {v6, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1987
    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 1988
    :cond_e
    const/4 v12, 0x0

    goto/16 :goto_6

    .line 2018
    :cond_f
    iget-object v2, v2, Lnzu;->b:Lnym;

    if-nez v2, :cond_10

    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_10
    invoke-static {v2}, Ljvd;->a(Lnym;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_11

    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_11
    iget-object v3, v2, Lnym;->b:Lnyl;

    iget-object v3, v3, Lnyl;->b:Ljava/lang/String;

    invoke-static {v3}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_12

    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_12
    iget-object v3, v2, Lnym;->e:Ljava/lang/String;

    if-eqz v3, :cond_13

    iget-object v3, v2, Lnym;->e:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    :cond_13
    iget-object v2, v2, Lnym;->h:Lnyz;

    iget-object v2, v2, Lnyz;->c:Ljava/lang/String;

    new-instance v2, Ljvk;

    invoke-direct {v2}, Ljvk;-><init>()V

    goto/16 :goto_8

    .line 2053
    :cond_14
    sub-long v2, v8, p5

    return-wide v2

    :catch_0
    move-exception v2

    goto/16 :goto_4

    :cond_15
    move-wide/from16 v2, v22

    goto/16 :goto_9

    :cond_16
    move-object v5, v13

    goto/16 :goto_7
.end method

.method public static a(Landroid/content/Context;Lnzx;I)J
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2645
    if-nez p1, :cond_0

    move-wide v0, v2

    .line 2670
    :goto_0
    return-wide v0

    .line 2649
    :cond_0
    sget-object v0, Lnzr;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    .line 2650
    if-eqz v0, :cond_9

    iget-object v4, v0, Lnzr;->d:Lnxh;

    if-eqz v4, :cond_9

    iget-object v4, v0, Lnzr;->d:Lnxh;

    iget-object v4, v4, Lnxh;->a:[I

    if-eqz v4, :cond_9

    iget-object v0, v0, Lnzr;->d:Lnxh;

    iget-object v0, v0, Lnxh;->a:[I

    move-object v5, v0

    :goto_1
    if-eqz v5, :cond_7

    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move-wide v6, v2

    :goto_2
    if-ltz v4, :cond_1

    aget v0, v5, v4

    sget-object v8, Ljvj;->h:Landroid/util/SparseArray;

    invoke-virtual {v8, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v8

    or-long/2addr v6, v8

    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_2

    :cond_1
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2652
    :goto_3
    if-nez v0, :cond_2

    .line 2653
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 2654
    invoke-static {p0, v0, p2}, Ljvj;->a(Landroid/content/Context;Lnzu;I)Ljava/lang/Long;

    move-result-object v0

    .line 2657
    :cond_2
    if-nez v0, :cond_4

    .line 2658
    sget-object v0, Lnzo;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzo;

    .line 2659
    if-eqz v0, :cond_8

    iget-object v4, v0, Lnzo;->b:Lnyb;

    if-eqz v4, :cond_8

    iget-object v4, v0, Lnzo;->b:Lnyb;

    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v5, "gaia_id"

    invoke-interface {v0, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, v4, Lnyb;->f:Lnyz;

    if-eqz v5, :cond_8

    iget-object v5, v4, Lnyb;->f:Lnyz;

    iget-object v5, v5, Lnyz;->c:Ljava/lang/String;

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, v4, Lnyb;->e:I

    const/4 v5, 0x3

    if-eq v0, v5, :cond_3

    iget v0, v4, Lnyb;->e:I

    const/4 v4, 0x7

    if-ne v0, v4, :cond_8

    :cond_3
    const-wide/16 v0, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2662
    :cond_4
    :goto_4
    if-nez v0, :cond_5

    .line 2663
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2666
    :cond_5
    const-string v1, "~local"

    iget-object v2, p1, Lnzx;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x4

    iget v2, p1, Lnzx;->k:I

    if-ne v1, v2, :cond_6

    .line 2667
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Ljvj;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    or-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2670
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    .line 2650
    goto :goto_3

    :cond_8
    move-object v0, v1

    goto :goto_4

    :cond_9
    move-object v5, v1

    goto/16 :goto_1
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2736
    const-string v1, "all_tiles"

    sget-object v2, Ljvj;->c:[Ljava/lang/String;

    const-string v3, "view_id = ? AND tile_id = ?  AND media_attr & 512 == 0"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v6

    const/4 v0, 0x1

    aput-object p2, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2741
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2743
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 2741
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2743
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Z)J
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2722
    const-string v1, "all_tiles"

    sget-object v2, Ljvj;->c:[Ljava/lang/String;

    if-eqz p3, :cond_0

    const-string v3, "view_id = ? AND cluster_id = ?  AND media_attr & 512 != 0"

    :goto_0
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v6

    const/4 v0, 0x1

    aput-object p2, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2729
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2731
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 2722
    :cond_0
    const-string v3, "view_id = ? AND cluster_id = ?  AND media_attr & 512 == 0"

    goto :goto_0

    .line 2729
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_1

    .line 2731
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)J
    .locals 2

    .prologue
    .line 1537
    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v1, v0

    .line 1538
    if-eqz p2, :cond_0

    const-string v0, "SELECT view_order FROM all_tiles WHERE view_id = ? ORDER BY view_order ASC  LIMIT 1"

    .line 1541
    :goto_0
    invoke-static {p0, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 1545
    :goto_1
    return-wide v0

    .line 1538
    :cond_0
    const-string v0, "SELECT view_order FROM all_tiles WHERE view_id = ? ORDER BY view_order DESC  LIMIT 1"
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1545
    :catch_0
    move-exception v0

    const-wide/32 v0, 0xf4240

    goto :goto_1
.end method

.method public static a(Lnym;Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 3099
    iget-object v0, p0, Lnym;->h:Lnyz;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lnym;->h:Lnyz;

    iget-object v0, v0, Lnyz;->c:Ljava/lang/String;

    move-object v2, v0

    .line 3100
    :goto_0
    const-wide/16 v0, 0x0

    .line 3102
    if-eqz v2, :cond_0

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3103
    const-wide/16 v0, 0x4000

    .line 3105
    :cond_0
    iget-object v2, p0, Lnym;->M:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3106
    const-wide/16 v2, 0x80

    or-long/2addr v0, v2

    .line 3108
    :cond_1
    iget-object v2, p0, Lnym;->B:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3109
    const-wide/16 v2, 0x40

    or-long/2addr v0, v2

    .line 3111
    :cond_2
    iget-object v2, p0, Lnym;->C:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3112
    const-wide/32 v2, 0x800000

    or-long/2addr v0, v2

    .line 3114
    :cond_3
    iget-object v2, p0, Lnym;->m:Lnzb;

    if-eqz v2, :cond_4

    .line 3115
    const-wide/16 v2, 0x20

    or-long/2addr v0, v2

    .line 3117
    :cond_4
    iget-object v2, p0, Lnym;->K:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 3118
    const-wide/16 v2, 0x100

    or-long/2addr v0, v2

    .line 3120
    :cond_5
    iget-object v2, p0, Lnym;->L:[Lnxj;

    if-eqz v2, :cond_7

    .line 3121
    iget-object v2, p0, Lnym;->L:[Lnxj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_7

    .line 3122
    iget-object v3, p0, Lnym;->L:[Lnxj;

    aget-object v3, v3, v2

    iget v3, v3, Lnxj;->b:I

    packed-switch v3, :pswitch_data_0

    .line 3140
    :goto_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 3099
    :cond_6
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_0

    .line 3124
    :pswitch_0
    const-wide/16 v4, 0x1

    or-long/2addr v0, v4

    .line 3125
    goto :goto_2

    .line 3127
    :pswitch_1
    const-wide/16 v4, 0x2

    or-long/2addr v0, v4

    .line 3128
    goto :goto_2

    .line 3130
    :pswitch_2
    const-wide/16 v4, 0x4

    or-long/2addr v0, v4

    .line 3131
    goto :goto_2

    .line 3133
    :pswitch_3
    const-wide/16 v4, 0x8

    or-long/2addr v0, v4

    .line 3134
    goto :goto_2

    .line 3136
    :pswitch_4
    const-wide/16 v4, 0x10

    or-long/2addr v0, v4

    .line 3137
    goto :goto_2

    .line 3139
    :pswitch_5
    const-wide/32 v4, 0x400000

    or-long/2addr v0, v4

    goto :goto_2

    .line 3147
    :cond_7
    iget-object v2, p0, Lnym;->F:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3148
    const-wide/32 v2, 0x80000

    or-long/2addr v0, v2

    .line 3151
    :cond_8
    return-wide v0

    .line 3122
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(J)Ljac;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2513
    const-wide/16 v0, 0x40

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    sget-object v0, Ljac;->c:Ljac;

    :goto_0
    return-object v0

    :cond_0
    const-wide/16 v0, 0x20

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    sget-object v0, Ljac;->b:Ljac;

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    sget-object v0, Ljac;->d:Ljac;

    goto :goto_0

    :cond_2
    sget-object v0, Ljac;->a:Ljac;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lnzu;I)Ljava/lang/Long;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v7, 0xd

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2586
    .line 2587
    if-eqz p1, :cond_6

    iget-object v0, p1, Lnzu;->b:Lnym;

    if-eqz v0, :cond_6

    .line 2588
    iget-object v5, p1, Lnzu;->b:Lnym;

    .line 2589
    iget-object v0, v5, Lnym;->q:Lnxg;

    if-eqz v0, :cond_5

    .line 2590
    iget-object v0, p1, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->q:Lnxg;

    iget-object v0, v0, Lnxg;->a:[I

    move-object v1, v0

    .line 2593
    :goto_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 2594
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v6, "gaia_id"

    .line 2595
    invoke-interface {v0, v6}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2596
    iget-object v6, v5, Lnym;->h:Lnyz;

    if-eqz v6, :cond_4

    iget-object v6, v5, Lnym;->h:Lnyz;

    iget-object v6, v6, Lnyz;->c:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v5, Lnym;->l:Lnyb;

    .line 2597
    if-eqz v0, :cond_0

    iget v5, v0, Lnyb;->e:I

    const/4 v6, 0x4

    if-eq v5, v6, :cond_0

    iget v0, v0, Lnyb;->e:I

    if-eq v0, v3, :cond_0

    move v0, v3

    :goto_1
    if-eqz v0, :cond_4

    .line 2598
    if-nez v1, :cond_1

    .line 2599
    new-array v1, v3, [I

    aput v7, v1, v4

    move-object v4, v1

    .line 2611
    :goto_2
    if-eqz v4, :cond_3

    .line 2612
    const-wide/16 v2, 0x0

    .line 2613
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_2

    .line 2614
    aget v0, v4, v1

    .line 2615
    sget-object v5, Ljvj;->i:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v6

    or-long/2addr v2, v6

    .line 2613
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    :cond_0
    move v0, v4

    .line 2597
    goto :goto_1

    .line 2601
    :cond_1
    array-length v0, v1

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    .line 2602
    array-length v3, v1

    invoke-static {v1, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2604
    array-length v1, v1

    aput v7, v0, v1

    move-object v4, v0

    .line 2606
    goto :goto_2

    .line 2617
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2619
    :cond_3
    return-object v2

    :cond_4
    move-object v4, v1

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto :goto_0

    :cond_6
    move-object v4, v2

    goto :goto_2
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 994
    const-string v1, "all_tiles"

    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "cluster_count"

    aput-object v0, v2, v6

    const-string v3, "cluster_id = ? AND type = ?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v6

    const-string v0, "2"

    .line 998
    aput-object v0, v4, v7

    const-string v7, "cluster_count DESC LIMIT 1"

    move-object v0, p0

    move-object v6, v5

    .line 994
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1003
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1004
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 1007
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1010
    :goto_0
    return-object v5

    .line 1007
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 894
    const/4 v0, 0x0

    invoke-static {v0}, Ljvj;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 563
    const/4 v0, 0x0

    .line 564
    packed-switch p1, :pswitch_data_0

    .line 570
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {p0, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 566
    :pswitch_0
    const-string v0, "SEARCH_MY_PHOTOS"

    goto :goto_0

    .line 569
    :pswitch_1
    const-string v0, "SEARCH_MY_CIRCLES"

    goto :goto_0

    .line 564
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static varargs a(I[Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 595
    packed-switch p0, :pswitch_data_0

    .line 643
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown view: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 598
    :pswitch_0
    if-eqz p1, :cond_0

    array-length v0, p1

    if-ne v0, v1, :cond_0

    aget-object v0, p1, v2

    if-nez v0, :cond_1

    .line 599
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BEST_PHOTOS_VIEW requires one argument"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 601
    :cond_1
    const-string v0, "best:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aget-object v0, p1, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 639
    :goto_0
    return-object v0

    .line 601
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 603
    :pswitch_1
    const-string v0, "trash"

    goto :goto_0

    .line 605
    :pswitch_2
    const-string v0, "all"

    goto :goto_0

    .line 607
    :pswitch_3
    if-eqz p1, :cond_3

    array-length v0, p1

    if-ne v0, v1, :cond_3

    aget-object v0, p1, v2

    if-nez v0, :cond_4

    .line 608
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SEARCH_PHOTOS_VIEW requires one argument"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 610
    :cond_4
    const-string v0, "search:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aget-object v0, p1, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 612
    :pswitch_4
    if-eqz p1, :cond_6

    array-length v0, p1

    if-ne v0, v1, :cond_6

    aget-object v0, p1, v2

    if-nez v0, :cond_7

    .line 613
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "NOTIFICATION_VIEW requires one argument"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 615
    :cond_7
    const-string v0, "notification:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aget-object v0, p1, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 617
    :pswitch_5
    if-eqz p1, :cond_9

    array-length v0, p1

    if-ne v0, v1, :cond_9

    aget-object v0, p1, v2

    if-nez v0, :cond_a

    .line 618
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ALL_ALBUMS_VIEW requires one argument"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 620
    :cond_a
    const-string v0, "albums:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aget-object v0, p1, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 622
    :pswitch_6
    if-eqz p1, :cond_c

    array-length v0, p1

    if-ne v0, v1, :cond_c

    aget-object v0, p1, v2

    if-nez v0, :cond_d

    .line 623
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ALBUM_VIEW requires one argument"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 625
    :cond_d
    const-string v0, "album:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aget-object v0, p1, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_e
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 627
    :pswitch_7
    if-eqz p1, :cond_f

    array-length v0, p1

    if-ne v0, v1, :cond_f

    aget-object v0, p1, v2

    if-nez v0, :cond_10

    .line 628
    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EVENT_PHOTOS_VIEW requires two arguments"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 630
    :cond_10
    const-string v0, "event:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aget-object v0, p1, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_11

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_11
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 632
    :pswitch_8
    if-eqz p1, :cond_12

    array-length v0, p1

    if-ne v0, v1, :cond_12

    aget-object v0, p1, v2

    if-nez v0, :cond_13

    .line 633
    :cond_12
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "STORY_PHOTOS_VIEW requires one argument"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 635
    :cond_13
    const-string v0, "story:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aget-object v0, p1, v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_14
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 637
    :pswitch_9
    const-string v0, "manual_awesome"

    goto/16 :goto_0

    .line 639
    :pswitch_a
    const-string v0, "story_element_picker"

    goto/16 :goto_0

    .line 595
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 583
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 584
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BEST_PHOTOS requires a valid account"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 586
    :cond_0
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 587
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 588
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    aput-object v0, v1, v2

    invoke-static {v2, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;I[Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1290
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1291
    const-string v0, "owner_id = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1292
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1293
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "gaia_id"

    .line 1294
    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1295
    invoke-static {v1, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 1296
    const-string v0, " AND "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1297
    const-string v0, "photo_id"

    new-instance v2, Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1, v0, v2}, Ljvj;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1298
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3321
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 3322
    const-string v1, "~post:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3323
    if-eqz p0, :cond_0

    .line 3324
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3326
    :cond_0
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3327
    if-eqz p1, :cond_1

    .line 3328
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3330
    :cond_1
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3331
    if-eqz p2, :cond_2

    .line 3332
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3334
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 714
    const-string v0, "UNKNOWN"

    .line 715
    packed-switch p3, :pswitch_data_0

    .line 727
    :goto_0
    invoke-static {p0, p1, p2, v0}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 717
    :pswitch_0
    const-string v0, "PLUS_EVENT"

    goto :goto_0

    .line 720
    :pswitch_1
    const-string v0, "PHOTO_COLLECTION"

    goto :goto_0

    .line 723
    :pswitch_2
    const-string v0, "ALBUM"

    goto :goto_0

    .line 726
    :pswitch_3
    const-string v0, "AD_HOC"

    goto :goto_0

    .line 715
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 689
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 690
    if-eqz p1, :cond_0

    .line 691
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 693
    :cond_0
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    if-eqz p2, :cond_1

    .line 695
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    :cond_1
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    if-eqz p0, :cond_2

    .line 699
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    :cond_2
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    if-eqz p3, :cond_3

    .line 703
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 705
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lnzx;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3344
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p0, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3345
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p0, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v0, v0, Lnzu;->b:Lnym;

    .line 3346
    iget-object v1, v0, Lnym;->h:Lnyz;

    iget-object v1, v1, Lnyz;->c:Ljava/lang/String;

    iget-object v0, v0, Lnym;->e:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3359
    :goto_0
    return-object v0

    .line 3347
    :cond_0
    sget-object v0, Lnzr;->a:Loxr;

    invoke-virtual {p0, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3348
    sget-object v0, Lnzr;->a:Loxr;

    .line 3349
    invoke-virtual {p0, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    iget-object v0, v0, Lnzr;->b:Lnxr;

    .line 3350
    iget-object v1, v0, Lnxr;->f:Ljava/lang/String;

    iget-object v0, v0, Lnxr;->c:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 3351
    :cond_1
    sget-object v0, Lnzo;->a:Loxr;

    invoke-virtual {p0, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3352
    sget-object v0, Lnzo;->a:Loxr;

    invoke-virtual {p0, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzo;

    iget-object v0, v0, Lnzo;->b:Lnyb;

    .line 3353
    iget-object v1, v0, Lnyb;->f:Lnyz;

    iget-object v1, v1, Lnyz;->c:Ljava/lang/String;

    iget-object v0, v0, Lnyb;->d:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 3354
    :cond_2
    iget-object v0, p0, Lnzx;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lnzx;->b:Ljava/lang/String;

    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3355
    iget-object v0, p0, Lnzx;->b:Ljava/lang/String;

    goto :goto_0

    .line 3357
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tile must be a known type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lnzx;I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 3251
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 3252
    if-lez p1, :cond_0

    move v1, v0

    .line 3253
    :goto_0
    if-ge v1, p1, :cond_0

    .line 3254
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3253
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3257
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3258
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3260
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "TILE [id: "

    .line 3261
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3262
    iget-object v4, p0, Lnzx;->b:Ljava/lang/String;

    .line 3263
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", type: "

    .line 3264
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3265
    iget v4, p0, Lnzx;->k:I

    .line 3266
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", colour: "

    .line 3267
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3268
    iget-object v4, p0, Lnzx;->g:Ljava/lang/String;

    .line 3269
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3270
    iget-object v3, p0, Lnzx;->c:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 3271
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3272
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "      title: "

    .line 3273
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 3274
    iget-object v3, p0, Lnzx;->c:Ljava/lang/String;

    .line 3275
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3277
    :cond_1
    iget-object v1, p0, Lnzx;->j:[Lnzx;

    if-eqz v1, :cond_2

    .line 3278
    iget-object v1, p0, Lnzx;->j:[Lnzx;

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    .line 3279
    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, p1, 0x2

    .line 3280
    invoke-static {v4, v6}, Ljvj;->a(Lnzx;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3278
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3283
    :cond_2
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3285
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljad;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 2461
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2462
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2506
    :goto_0
    return-object v0

    .line 2465
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2466
    const-string v0, "tile_id IN ("

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2467
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    move v2, v4

    :goto_1
    if-ge v2, v3, :cond_3

    .line 2468
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2470
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2471
    invoke-static {v6, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 2476
    add-int/lit8 v0, v3, -0x1

    if-ge v2, v0, :cond_2

    .line 2477
    const/16 v0, 0x2c

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2467
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2481
    :cond_3
    const/16 v0, 0x29

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2483
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 2484
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2486
    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/String;

    const-string v2, "owner_id"

    aput-object v2, v3, v4

    const-string v2, "photo_id"

    aput-object v2, v3, v1

    .line 2492
    const-string v2, "all_tiles"

    .line 2493
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    move-object v9, v5

    .line 2492
    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2496
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2498
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2499
    new-instance v2, Ljad;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, Ljad;-><init>(Ljava/lang/String;J)V

    .line 2500
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 2503
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/util/ArrayList;)Ljuy;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljuy;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1029
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1030
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1031
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v3, v1, [Ljava/lang/String;

    .line 1032
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1034
    const/4 v1, 0x0

    :goto_0
    array-length v2, v3

    if-ge v1, v2, :cond_0

    .line 1035
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    .line 1034
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1039
    :cond_0
    const-string v1, "all_tiles"

    .line 1040
    invoke-static {v0}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1, v3}, Ljvj;->a(Landroid/content/Context;I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    .line 1039
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1044
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1048
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1049
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1050
    invoke-static {v1, v3}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 1052
    const-string v5, "_id"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1053
    const-string v5, "parent_id"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1055
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1056
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1059
    :cond_1
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1062
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1066
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1067
    const-string v1, "type = "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v1, "2"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v5, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1068
    const-string v1, " AND "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1069
    const-string v1, "cluster_id"

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v3, v1, v5}, Ljvj;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1071
    const-string v1, "all_tiles"

    .line 1072
    invoke-static {v0}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    .line 1071
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1077
    :goto_3
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1078
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1079
    invoke-static {v1, v0}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 1081
    const-string v2, "_id"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1082
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_3

    .line 1085
    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1067
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 1085
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1088
    new-instance v1, Ljuy;

    invoke-direct {v1}, Ljuy;-><init>()V

    .line 1089
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljuy;->a(I)V

    .line 1090
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/content/ContentValues;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/ContentValues;

    invoke-virtual {v1, v0}, Ljuy;->a([Landroid/content/ContentValues;)V

    .line 1092
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lmzo;
    .locals 3

    .prologue
    .line 2683
    new-instance v0, Lmzo;

    invoke-direct {v0}, Lmzo;-><init>()V

    .line 2684
    invoke-static {p0}, Ljvj;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2685
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2686
    iput-object v1, v0, Lmzo;->a:Ljava/lang/String;

    .line 2688
    :cond_0
    invoke-static {p0}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2689
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2690
    iput-object v1, v0, Lmzo;->c:Ljava/lang/String;

    .line 2692
    :cond_1
    invoke-static {p0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2693
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2694
    iput-object v1, v0, Lmzo;->b:Ljava/lang/String;

    .line 2696
    :cond_2
    invoke-static {p0}, Ljvj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2701
    const-string v2, "PLUS_EVENT"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2702
    const/4 v1, 0x1

    iput v1, v0, Lmzo;->e:I

    .line 2715
    :cond_3
    :goto_0
    iput-object p1, v0, Lmzo;->d:Ljava/lang/String;

    .line 2717
    return-object v0

    .line 2703
    :cond_4
    const-string v2, "PHOTO_COLLECTION"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2704
    const/4 v1, 0x2

    iput v1, v0, Lmzo;->e:I

    goto :goto_0

    .line 2705
    :cond_5
    const-string v2, "ALBUM"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2706
    const/4 v1, 0x3

    iput v1, v0, Lmzo;->e:I

    goto :goto_0

    .line 2707
    :cond_6
    const-string v2, "AD_HOC"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2708
    const/4 v1, 0x4

    iput v1, v0, Lmzo;->e:I

    goto :goto_0

    .line 2709
    :cond_7
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2710
    const/4 v1, 0x0

    iput v1, v0, Lmzo;->e:I

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)V
    .locals 16

    .prologue
    .line 2359
    invoke-static/range {p0 .. p1}, Ljvj;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 2362
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 2363
    const-string v3, "cluster_count"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2364
    const-string v3, "all_tiles"

    const-string v4, "cluster_id = ? AND type = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v5, v7

    const/4 v7, 0x1

    const-string v8, "2"

    .line 2365
    aput-object v8, v5, v7

    .line 2364
    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2367
    const-string v2, "SELECT count(*) FROM all_tiles WHERE type = ? AND view_id = ? AND parent_id = ?"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "4"

    .line 2369
    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v6, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    .line 2367
    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    .line 2372
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 2373
    sub-long v2, p4, v2

    .line 2374
    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v5, "title"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "101"

    aput-object v8, v5, v7

    const/4 v7, 0x1

    aput-object v6, v5, v7

    const/4 v7, 0x2

    aput-object p3, v5, v7

    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-nez v7, :cond_1

    const-string v2, "all_tiles"

    const-string v3, "type = ? AND view_id = ? AND parent_id = ?"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2377
    :cond_0
    :goto_0
    return-void

    .line 2374
    :cond_1
    const-string v7, "all_tiles"

    const-string v8, "type = ? AND view_id = ? AND parent_id = ?"

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v4, v8, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p3

    invoke-static {v0, v2, v3}, Ljvb;->a(Ljava/lang/String;J)Lnzx;

    move-result-object v2

    const-string v3, "SELECT view_order FROM all_tiles WHERE view_id = ? AND parent_id = ? ORDER BY view_order DESC limit 1"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    move-object/from16 v0, p2

    invoke-static {v0, v3, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v8, 0x1

    add-long/2addr v8, v4

    const-string v3, "UPDATE all_tiles SET view_order = view_order + 1 WHERE view_id = ? AND view_order >= ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v3, 0x1

    new-array v7, v3, [Lnzx;

    const/4 v3, 0x0

    aput-object v2, v7, v3

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v3, p0

    move/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v13, p3

    invoke-static/range {v3 .. v14}, Ljvj;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnzx;JLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)J

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lnzx;Ljava/util/HashSet;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lnzx;",
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 2130
    iget-object v8, p3, Lnzx;->b:Ljava/lang/String;

    .line 2132
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2133
    invoke-static {p0, p1, p3, v0}, Ljvj;->a(Landroid/content/Context;ILnzx;Landroid/content/ContentValues;)V

    .line 2134
    const-wide/16 v2, -0x1

    cmp-long v1, p5, v2

    if-eqz v1, :cond_0

    .line 2135
    const-string v1, "last_refresh_time"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2137
    :cond_0
    const-string v1, "media_attr"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2138
    const-wide/16 v4, 0x2000

    or-long/2addr v2, v4

    .line 2139
    const-string v1, "media_attr"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2141
    const-string v1, "all_tiles"

    const-string v2, "tile_id = ? AND media_attr & 512 == 0"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v8, v3, v4

    invoke-virtual {p2, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2144
    invoke-static {p2, p3, p4}, Lidg;->a(Landroid/database/sqlite/SQLiteDatabase;Lnzx;Ljava/util/HashSet;)V

    .line 2147
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "view_id"

    aput-object v1, v2, v0

    .line 2150
    const-string v1, "all_tiles"

    const-string v3, "tile_id = ? AND media_attr & 512 == 0"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v8, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2154
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2155
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2156
    if-eqz p4, :cond_1

    .line 2157
    invoke-static {v0}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2161
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2163
    if-eqz p4, :cond_3

    .line 2164
    invoke-static {v8}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2166
    :cond_3
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x0

    .line 1616
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1617
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1618
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1622
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1625
    :try_start_0
    const-string v0, "all_tiles"

    const-string v3, "cluster_id = ? OR parent_id = ? OR view_id = ?"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x2

    aput-object p2, v6, v7

    invoke-virtual {v2, v0, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, 0x0

    .line 1629
    const-string v0, "tile_requests"

    const-string v3, "view_id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    invoke-virtual {v2, v0, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1632
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1634
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1635
    const-string v0, "EsTileData"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1636
    const-string v0, "[DELETE_ALBUM], count: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1638
    invoke-static {v4, v5}, Llse;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x17

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1643
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1644
    sget-object v1, Ljvj;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1645
    return-void

    .line 1634
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1635
    const-string v2, "EsTileData"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1636
    const-string v2, "[DELETE_ALBUM], count: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1638
    invoke-static {v4, v5}, Llse;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x17

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1636
    :cond_1
    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;J)V
    .locals 7

    .prologue
    .line 1909
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1910
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1911
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1912
    invoke-static {v2, p2}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v0

    .line 1913
    add-long v4, v0, p3

    move-object v0, p0

    move v1, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Ljvj;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)V

    .line 1916
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 1917
    const-string v0, "tile_requests"

    const-string v1, "view_id = ? "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v2, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1921
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1922
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1923
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x4

    const/4 v1, 0x0

    .line 1499
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1500
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1501
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1505
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1507
    :try_start_0
    const-string v0, "all_tiles"

    const-string v3, "tile_id = ? OR parent_id = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    invoke-virtual {v2, v0, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1510
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1513
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1514
    const-string v0, "EsTileData"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1515
    const-string v0, "[DELETE_TILE_AND_CHILDREN], count: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1517
    invoke-static {v4, v5}, Llse;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x17

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1521
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1522
    invoke-static {p2}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1523
    invoke-static {p3}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1524
    return-void

    .line 1513
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1514
    const-string v2, "EsTileData"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1515
    const-string v2, "[DELETE_TILE_AND_CHILDREN], count: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1517
    invoke-static {v4, v5}, Llse;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x17

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1515
    :cond_1
    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ILjava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 1847
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1848
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1850
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1851
    const-string v1, "comment_count"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1853
    invoke-static {p0, p1, p2, p3, v8}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v1

    .line 1856
    if-eqz v1, :cond_1

    .line 1858
    :try_start_0
    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    invoke-static {v3, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v1

    check-cast v1, Lnym;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 1863
    :goto_0
    if-eqz v1, :cond_0

    .line 1864
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lnym;->k:Ljava/lang/Integer;

    .line 1865
    const-string v3, "data"

    invoke-static {v1}, Loxu;->a(Loxu;)[B

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1868
    :cond_0
    const-string v1, "all_tiles"

    const-string v3, "tile_id = ?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object p2, v4, v8

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1870
    if-eqz p5, :cond_3

    .line 1871
    invoke-static {p2}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {p5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1873
    const-string v1, "all_tiles"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "view_id"

    aput-object v3, v2, v8

    const-string v3, "tile_id = ?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object p2, v4, v8

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1877
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1878
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {p5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1859
    :catch_0
    move-exception v1

    .line 1860
    const-string v3, "EsTileData"

    const-string v4, "unable to parse photo proto"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    move-object v1, v5

    goto :goto_0

    .line 1881
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1883
    :cond_3
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 1673
    :try_start_0
    new-instance v0, Lnym;

    invoke-direct {v0}, Lnym;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1674
    invoke-static {p0, p1, p2, v1, v2}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v1

    .line 1673
    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 1680
    if-nez v0, :cond_1

    .line 1696
    :goto_0
    return-void

    .line 1676
    :catch_0
    move-exception v0

    const-string v0, "Couldn\'t parse photo data for photoId "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1684
    :cond_1
    iput-object p3, v0, Lnym;->d:Ljava/lang/String;

    .line 1685
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 1686
    const-string v2, "data"

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1688
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1689
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1690
    const-string v2, "all_tiles"

    const-string v3, "tile_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1693
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1694
    invoke-static {p4}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1695
    invoke-static {p2}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 11

    .prologue
    const/4 v9, 0x2

    const/4 v10, 0x0

    .line 1748
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1749
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1751
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1753
    const/4 v1, 0x4

    :try_start_0
    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "best"

    aput-object v2, v4, v1

    const/4 v1, 0x1

    aput-object p2, v4, v1

    const/4 v1, 0x2

    aput-object p2, v4, v1

    const/4 v1, 0x3

    aput-object p2, v4, v1

    .line 1766
    new-instance v8, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1767
    const-string v1, "all_tiles"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "media_attr"

    aput-object v5, v2, v3

    const-string v3, "(view_id = ? AND (parent_id = ? OR cluster_id = ?)) OR (view_id = ? AND cluster_id IS NOT NULL)"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1771
    :goto_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1772
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1775
    :catchall_0
    move-exception v1

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1804
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 1775
    :cond_0
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1778
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v9, :cond_1

    .line 1779
    const-string v1, "EsTileData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1780
    const-string v1, "EsTileData"

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x31

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Too many matching tiles: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for viewId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1785
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1786
    if-eqz p3, :cond_2

    .line 1787
    const-wide/16 v6, 0x800

    or-long/2addr v2, v6

    .line 1792
    :goto_2
    const-wide/16 v6, 0x200

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_3

    const-string v1, "media_attr & 512 != 0"

    .line 1795
    :goto_3
    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 1796
    const-string v7, "media_attr"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1798
    const-string v2, "all_tiles"

    const-string v3, "(view_id = ? AND (parent_id = ? OR cluster_id = ?)) OR (view_id = ? AND cluster_id IS NOT NULL) AND "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-virtual {v0, v2, v6, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 1789
    :cond_2
    const-wide/16 v6, -0x801

    and-long/2addr v2, v6

    goto :goto_2

    .line 1792
    :cond_3
    const-string v1, "media_attr & 512 == 0"

    goto :goto_3

    .line 1798
    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 1802
    :cond_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1804
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1807
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1808
    invoke-static {p2}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1809
    const-string v1, "best"

    invoke-static {v1}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1810
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;[Lnzx;Ljava/lang/String;)V
    .locals 16

    .prologue
    .line 1559
    const-class v2, Lhzr;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhzr;

    .line 1560
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v2, v0, v1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 1561
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 1562
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1563
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    .line 1567
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1569
    const-wide/32 v8, 0xf4240

    .line 1571
    if-eqz p3, :cond_2

    .line 1573
    :try_start_0
    const-string v3, "SELECT view_order FROM all_tiles WHERE view_id = ? AND media_attr & 512 != 0 ORDER BY view_order DESC  LIMIT 1"

    invoke-static {v5, v3, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 1579
    :goto_0
    :try_start_1
    move-object/from16 v0, p3

    array-length v2, v0

    int-to-long v2, v2

    sub-long v6, v8, v2

    .line 1580
    move-object/from16 v0, p3

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    move-wide v2, v6

    :goto_1
    if-ltz v4, :cond_1

    .line 1581
    aget-object v6, p3, v4

    .line 1582
    iget-object v7, v6, Lnzx;->j:[Lnzx;

    if-eqz v7, :cond_0

    .line 1583
    iget-object v6, v6, Lnzx;->j:[Lnzx;

    array-length v6, v6

    int-to-long v6, v6

    sub-long/2addr v2, v6

    .line 1580
    :cond_0
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    :cond_1
    move-wide v8, v2

    .line 1589
    :cond_2
    new-instance v12, Ljava/util/HashSet;

    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v3, p0

    move/from16 v4, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v11, p4

    invoke-static/range {v3 .. v12}, Ljvj;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnzx;JLjava/util/List;Ljava/lang/String;Ljava/util/Set;)J

    .line 1593
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1595
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1596
    const-string v2, "EsTileData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1597
    const-string v2, "[INSERT_OOB_TILES], view: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz p3, :cond_4

    move-object/from16 v0, p3

    array-length v2, v0

    .line 1600
    :goto_2
    invoke-static {v14, v15}, Llse;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x24

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", num tiles: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1605
    :cond_3
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 1606
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 1607
    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_3

    .line 1597
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 1595
    :catchall_0
    move-exception v2

    move-object v3, v2

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1596
    const-string v2, "EsTileData"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1597
    const-string v2, "[INSERT_OOB_TILES], view: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-eqz p3, :cond_6

    move-object/from16 v0, p3

    array-length v2, v0

    .line 1600
    :goto_4
    invoke-static {v14, v15}, Llse;->a(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x24

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", num tiles: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", duration: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1597
    :cond_5
    throw v3

    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 1609
    :cond_7
    invoke-static/range {p2 .. p2}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1610
    return-void

    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V
    .locals 18

    .prologue
    .line 1440
    const-class v2, Lhzr;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhzr;

    .line 1441
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v2, v0, v1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 1442
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 1443
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1444
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1446
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1448
    if-eqz p4, :cond_0

    .line 1449
    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    .line 1450
    const-string v3, "all_tiles"

    const-string v4, "view_id = ? AND media_attr & 512 == 0"

    invoke-virtual {v5, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1453
    :cond_0
    move-object/from16 v0, p2

    move/from16 v1, p5

    invoke-static {v5, v0, v1}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)J

    move-result-wide v2

    .line 1454
    if-eqz p5, :cond_4

    if-eqz p3, :cond_4

    .line 1455
    move-object/from16 v0, p3

    array-length v4, v0

    int-to-long v6, v4

    sub-long v6, v2, v6

    .line 1456
    move-object/from16 v0, p3

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    move-wide v2, v6

    :goto_0
    if-ltz v4, :cond_2

    .line 1457
    aget-object v6, p3, v4

    .line 1458
    iget-object v7, v6, Lnzx;->j:[Lnzx;

    if-eqz v7, :cond_1

    .line 1459
    iget-object v6, v6, Lnzx;->j:[Lnzx;

    array-length v6, v6

    int-to-long v6, v6

    sub-long/2addr v2, v6

    .line 1456
    :cond_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_2
    move-wide v8, v2

    .line 1467
    :goto_1
    const/4 v13, 0x0

    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v3, p0

    move/from16 v4, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v12, p6

    invoke-static/range {v3 .. v14}, Ljvj;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Lnzx;JLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)J

    .line 1471
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1473
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1474
    const-string v2, "EsTileData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1475
    const-string v2, "[INSERT_TILES], view: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    array-length v2, v0

    .line 1478
    :goto_2
    invoke-static/range {v16 .. v17}, Llse;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x24

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", num tiles: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1483
    :cond_3
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 1484
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 1485
    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_3

    .line 1464
    :cond_4
    const-wide/16 v6, 0x1

    add-long v8, v2, v6

    goto/16 :goto_1

    .line 1475
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 1473
    :catchall_0
    move-exception v2

    move-object v3, v2

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1474
    const-string v2, "EsTileData"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1475
    const-string v2, "[INSERT_TILES], view: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-eqz p3, :cond_7

    move-object/from16 v0, p3

    array-length v2, v0

    .line 1478
    :goto_4
    invoke-static/range {v16 .. v17}, Llse;->a(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x24

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", num tiles: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", duration: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1475
    :cond_6
    throw v3

    :cond_7
    const/4 v2, 0x0

    goto :goto_4

    .line 1488
    :cond_8
    if-eqz p7, :cond_9

    .line 1489
    invoke-static/range {p2 .. p2}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1491
    :cond_9
    return-void
.end method

.method public static a(Landroid/content/Context;ILnzx;I)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    .line 1385
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1386
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1387
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 1388
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 1390
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1393
    packed-switch p3, :pswitch_data_0

    .line 1402
    const-wide/16 v6, -0x1

    :goto_0
    move-object v1, p0

    move v2, p1

    move-object v4, p2

    .line 1406
    :try_start_0
    invoke-static/range {v1 .. v7}, Ljvj;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lnzx;Ljava/util/HashSet;J)V

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    .line 1408
    invoke-static/range {v1 .. v7}, Ljvj;->b(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lnzx;Ljava/util/HashSet;J)V

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    .line 1410
    invoke-static/range {v1 .. v7}, Ljvj;->c(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lnzx;Ljava/util/HashSet;J)V

    .line 1412
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1414
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1415
    const-string v0, "EsTileData"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1416
    const-string v0, "[UPDATE_PHOTO_TILE], tile: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lnzx;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1418
    invoke-static {v8, v9}, Llse;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xc

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v4, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v4, v6

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1423
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1424
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1425
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    .line 1395
    :pswitch_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v6

    goto/16 :goto_0

    .line 1398
    :pswitch_1
    const-wide/16 v6, 0x0

    .line 1399
    goto/16 :goto_0

    .line 1414
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1415
    const-string v1, "EsTileData"

    invoke-static {v1, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1416
    const-string v1, "[UPDATE_PHOTO_TILE], tile: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lnzx;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1418
    invoke-static {v8, v9}, Llse;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1416
    :cond_1
    throw v0

    .line 1427
    :cond_2
    return-void

    .line 1393
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;ILnzx;Landroid/content/ContentValues;)V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/4 v8, 0x0

    .line 3161
    .line 3162
    iget-object v0, p2, Lnzx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3163
    const-string v0, "tile_id"

    iget-object v1, p2, Lnzx;->b:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3166
    :cond_0
    iget v0, p2, Lnzx;->k:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 3167
    const-string v0, "type"

    iget v1, p2, Lnzx;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3169
    :cond_1
    iget-object v0, p2, Lnzx;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3170
    const-string v0, "title"

    iget-object v1, p2, Lnzx;->c:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3172
    :cond_2
    iget-object v0, p2, Lnzx;->d:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lnzx;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 3173
    const-string v0, "subtitle"

    const-string v1, " \u2022 "

    iget-object v4, p2, Lnzx;->d:[Ljava/lang/String;

    .line 3174
    invoke-static {v1, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3173
    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3176
    :cond_3
    iget-object v0, p2, Lnzx;->f:Lnyl;

    if-eqz v0, :cond_6

    .line 3177
    iget-object v0, p2, Lnzx;->f:Lnyl;

    iget-object v0, v0, Lnyl;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3178
    const-string v0, "image_url"

    iget-object v1, p2, Lnzx;->f:Lnyl;

    iget-object v1, v1, Lnyl;->b:Ljava/lang/String;

    .line 3179
    invoke-static {v1}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3178
    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3181
    :cond_4
    iget-object v0, p2, Lnzx;->f:Lnyl;

    iget-object v0, v0, Lnyl;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 3182
    const-string v0, "image_width"

    iget-object v1, p2, Lnzx;->f:Lnyl;

    iget-object v1, v1, Lnyl;->c:Ljava/lang/Integer;

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3184
    :cond_5
    iget-object v0, p2, Lnzx;->f:Lnyl;

    iget-object v0, v0, Lnyl;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 3185
    const-string v0, "image_height"

    iget-object v1, p2, Lnzx;->f:Lnyl;

    iget-object v1, v1, Lnyl;->d:Ljava/lang/Integer;

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3188
    :cond_6
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p2, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_e

    sget-object v0, Lnzu;->a:Loxr;

    .line 3189
    invoke-virtual {p2, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v0, v0, Lnzu;->b:Lnym;

    if-eqz v0, :cond_e

    .line 3190
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p2, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v4, v0, Lnzu;->b:Lnym;

    .line 3193
    iget-object v0, v4, Lnym;->b:Lnyl;

    if-eqz v0, :cond_7

    .line 3194
    iget-object v0, v4, Lnym;->b:Lnyl;

    iget-object v1, v4, Lnym;->b:Lnyl;

    iget-object v1, v1, Lnyl;->b:Ljava/lang/String;

    .line 3195
    invoke-static {v1}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnyl;->b:Ljava/lang/String;

    .line 3197
    :cond_7
    iget-object v0, v4, Lnym;->s:Lnyl;

    if-eqz v0, :cond_8

    .line 3198
    iget-object v0, v4, Lnym;->s:Lnyl;

    iget-object v1, v4, Lnym;->s:Lnyl;

    iget-object v1, v1, Lnyl;->b:Ljava/lang/String;

    .line 3199
    invoke-static {v1}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnyl;->b:Ljava/lang/String;

    .line 3201
    :cond_8
    iget-object v0, v4, Lnym;->t:Lnyl;

    if-eqz v0, :cond_9

    .line 3202
    iget-object v0, v4, Lnym;->t:Lnyl;

    iget-object v1, v4, Lnym;->t:Lnyl;

    iget-object v1, v1, Lnyl;->b:Ljava/lang/String;

    .line 3203
    invoke-static {v1}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnyl;->b:Ljava/lang/String;

    .line 3206
    :cond_9
    iget-object v0, v4, Lnym;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 3207
    const-string v0, "comment_count"

    iget-object v1, v4, Lnym;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3210
    :cond_a
    iget-object v0, v4, Lnym;->r:Loae;

    if-eqz v0, :cond_b

    .line 3211
    const-string v0, "plusone_count"

    iget-object v1, v4, Lnym;->r:Loae;

    iget-object v1, v1, Loae;->e:Ljava/lang/Integer;

    .line 3212
    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 3211
    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3215
    :cond_b
    invoke-static {v4}, Llmz;->a(Lnym;)Ljava/lang/String;

    move-result-object v0

    .line 3216
    if-eqz v0, :cond_c

    .line 3217
    const-string v1, "content_url"

    invoke-virtual {p3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3221
    :cond_c
    new-array v0, v8, [Lnyf;

    iput-object v0, v4, Lnym;->o:[Lnyf;

    .line 3223
    const-string v0, "data"

    invoke-static {v4}, Loxu;->a(Loxu;)[B

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3225
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 3226
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 3227
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3228
    invoke-static {v4, v0}, Ljvj;->a(Lnym;Ljava/lang/String;)J

    move-result-wide v0

    or-long/2addr v0, v2

    .line 3230
    iget-object v2, v4, Lnym;->n:Ljava/lang/Double;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v2

    .line 3231
    const-string v5, "timestamp"

    const-wide v6, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v6

    double-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p3, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3233
    iget-object v2, v4, Lnym;->y:[Lnxr;

    if-eqz v2, :cond_d

    iget-object v2, v4, Lnym;->y:[Lnxr;

    array-length v2, v2

    if-eqz v2, :cond_d

    iget-object v2, v4, Lnym;->y:[Lnxr;

    aget-object v2, v2, v8

    iget-object v2, v2, Lnxr;->l:Lnxl;

    if-eqz v2, :cond_d

    .line 3235
    const-string v2, "acl"

    iget-object v3, v4, Lnym;->y:[Lnxr;

    aget-object v3, v3, v8

    iget-object v3, v3, Lnxr;->l:Lnxl;

    invoke-static {v3}, Ljvj;->a(Lnxl;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3239
    :cond_d
    :goto_0
    const-string v2, "media_attr"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3241
    invoke-static {p0, p2, p1}, Ljvj;->a(Landroid/content/Context;Lnzx;I)J

    move-result-wide v0

    .line 3242
    const-string v2, "user_actions"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3243
    return-void

    :cond_e
    move-wide v0, v2

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILnzx;Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/Set;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lnzx;",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2769
    const-wide/16 v4, 0x0

    .line 2770
    const-string v2, "tile_id"

    iget-object v3, p2, Lnzx;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772
    iget v2, p2, Lnzx;->k:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_0

    .line 2773
    const-string v2, "type"

    iget v3, p2, Lnzx;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2776
    :cond_0
    iget-object v2, p2, Lnzx;->h:Lnyz;

    if-eqz v2, :cond_1

    .line 2777
    const-string v2, "photographer_gaia_id"

    iget-object v3, p2, Lnzx;->h:Lnyz;

    iget-object v3, v3, Lnyz;->c:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2778
    const-string v2, "photographer_avatar_url"

    iget-object v3, p2, Lnzx;->h:Lnyz;

    iget-object v3, v3, Lnyz;->e:Ljava/lang/String;

    .line 2779
    invoke-static {v3}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2778
    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2782
    :cond_1
    const-string v2, "title"

    iget-object v3, p2, Lnzx;->c:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2783
    iget-object v2, p2, Lnzx;->d:[Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p2, Lnzx;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 2784
    const-string v2, "subtitle"

    const-string v3, " \u2022 "

    iget-object v6, p2, Lnzx;->d:[Ljava/lang/String;

    .line 2785
    invoke-static {v3, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2784
    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2789
    :goto_0
    iget-object v2, p2, Lnzx;->f:Lnyl;

    if-eqz v2, :cond_9

    .line 2790
    const-string v2, "image_url"

    iget-object v3, p2, Lnzx;->f:Lnyl;

    iget-object v3, v3, Lnyl;->b:Ljava/lang/String;

    invoke-static {v3}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2791
    const-string v2, "image_width"

    iget-object v3, p2, Lnzx;->f:Lnyl;

    iget-object v3, v3, Lnyl;->c:Ljava/lang/Integer;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2792
    const-string v2, "image_height"

    iget-object v3, p2, Lnzx;->f:Lnyl;

    iget-object v3, v3, Lnyl;->d:Ljava/lang/Integer;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2796
    :goto_1
    invoke-static {p0, p2, p1}, Ljvj;->a(Landroid/content/Context;Lnzx;I)J

    move-result-wide v2

    .line 2797
    const-string v6, "user_actions"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2799
    const-string v3, "background_color"

    iget-object v2, p2, Lnzx;->g:Ljava/lang/String;

    if-eqz v2, :cond_b

    const-string v6, "#"

    iget-object v2, p2, Lnzx;->g:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_a

    invoke-virtual {v6, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2800
    :goto_2
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    .line 2799
    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2802
    const/4 v3, 0x0

    .line 2804
    sget-object v2, Lnzo;->a:Loxr;

    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 2805
    sget-object v2, Lnzo;->a:Loxr;

    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzo;

    iget-object v7, v2, Lnzo;->b:Lnyb;

    .line 2808
    iget-object v2, v7, Lnyb;->h:[Lnxr;

    if-eqz v2, :cond_e

    iget-object v2, v7, Lnyb;->h:[Lnxr;

    array-length v2, v2

    if-eqz v2, :cond_e

    .line 2809
    iget-object v2, v7, Lnyb;->h:[Lnxr;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 2811
    const-string v3, "media_key"

    iget-object v6, v2, Lnxr;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2812
    const-string v3, "equivalence_token"

    iget-object v6, v2, Lnxr;->o:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2813
    iget-object v6, v2, Lnxr;->f:Ljava/lang/String;

    .line 2814
    iget-object v3, v2, Lnxr;->b:Ljava/lang/String;

    iget-object v8, v2, Lnxr;->c:Ljava/lang/String;

    iget v9, v2, Lnxr;->d:I

    invoke-static {v3, v6, v8, v9}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 2816
    const/4 v8, 0x3

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v3, v9, v10

    invoke-static {v8, v9}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2817
    const-string v8, "cluster_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2818
    iget-object v8, v2, Lnxr;->l:Lnxl;

    if-eqz v8, :cond_d

    .line 2819
    iget-object v2, v2, Lnxr;->l:Lnxl;

    invoke-static {v2}, Ljvj;->a(Lnxl;)I

    move-result v2

    .line 2820
    const-string v8, "acl"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2821
    const/4 v8, 0x2

    if-eq v2, v8, :cond_2

    const/4 v8, -0x1

    if-ne v2, v8, :cond_c

    .line 2822
    :cond_2
    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2823
    const-wide/32 v2, 0x1000000

    .line 2833
    :goto_4
    new-instance v4, Ljux;

    iget-object v5, v7, Lnyb;->d:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-direct {v4, v0, v5, p2}, Ljux;-><init>(Ljava/lang/String;Ljava/lang/String;Lnzx;)V

    .line 2835
    const-string v5, "data"

    invoke-static {v4}, Ljux;->a(Ljux;)[B

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-wide v4, v2

    move-object v2, v6

    .line 2852
    :goto_5
    iget v3, v7, Lnyb;->e:I

    const/4 v6, 0x3

    if-eq v3, v6, :cond_3

    iget v3, v7, Lnyb;->e:I

    const/4 v6, 0x7

    if-ne v3, v6, :cond_11

    .line 2853
    :cond_3
    const-string v3, "cluster_count"

    iget-object v6, v7, Lnyb;->c:Ljava/lang/Integer;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object v12, v2

    move-wide v2, v4

    move-object v4, v12

    .line 3061
    :goto_6
    if-eqz v4, :cond_28

    .line 3062
    const-string v5, "owner_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3068
    :goto_7
    const-string v4, "~local"

    iget-object v5, p2, Lnzx;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 3069
    const-wide/32 v4, 0x40000

    or-long/2addr v2, v4

    .line 3072
    iget-object v4, p2, Lnzx;->f:Lnyl;

    if-eqz v4, :cond_29

    iget-object v4, p2, Lnzx;->f:Lnyl;

    iget-object v4, v4, Lnyl;->b:Ljava/lang/String;

    if-eqz v4, :cond_29

    iget-object v4, p2, Lnzx;->f:Lnyl;

    iget-object v4, v4, Lnyl;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Llsb;->c(Landroid/net/Uri;)Z

    move-result v4

    :goto_8
    if-eqz v4, :cond_4

    .line 3073
    const-wide/16 v4, 0x20

    or-long/2addr v2, v4

    .line 3076
    :cond_4
    const-string v4, "parent_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3077
    if-eqz v4, :cond_5

    move-object/from16 v0, p5

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 3078
    const-wide/32 v4, 0x1000000

    or-long/2addr v2, v4

    .line 3081
    :cond_5
    const-string v4, "media_attr"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3084
    iget v2, p2, Lnzx;->k:I

    const/16 v3, 0x65

    if-ne v2, v3, :cond_6

    .line 3086
    const-string v2, "cluster_id"

    iget-object v3, p2, Lnzx;->e:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3092
    :cond_6
    iget v2, p2, Lnzx;->k:I

    const/16 v3, 0x6e

    if-ne v2, v3, :cond_7

    .line 3093
    const-string v2, "cluster_id"

    iget-object v3, p2, Lnzx;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3095
    :cond_7
    return-void

    .line 2787
    :cond_8
    const-string v2, "subtitle"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2794
    :cond_9
    const-string v2, "image_url"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2799
    :cond_a
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2800
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 2825
    :cond_c
    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-wide v2, v4

    .line 2827
    goto/16 :goto_4

    .line 2828
    :cond_d
    const-string v2, "acl"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2829
    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2830
    const-wide/32 v2, 0x1000000

    goto/16 :goto_4

    .line 2838
    :cond_e
    iget-object v2, v7, Lnyb;->f:Lnyz;

    iget-object v2, v2, Lnyz;->c:Ljava/lang/String;

    .line 2839
    const/4 v3, 0x0

    iget-object v6, v7, Lnyb;->d:Ljava/lang/String;

    const-string v8, "ALBUM"

    invoke-static {v3, v2, v6, v8}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2841
    const/4 v6, 0x3

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    invoke-static {v6, v8}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2842
    iget v6, v7, Lnyb;->g:I

    invoke-static {v6}, Ljvd;->b(I)I

    move-result v6

    .line 2843
    const-string v8, "cluster_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2844
    const-string v8, "acl"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2845
    const/4 v8, 0x2

    if-eq v6, v8, :cond_f

    const/4 v8, -0x1

    if-ne v6, v8, :cond_10

    .line 2846
    :cond_f
    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2847
    const-wide/32 v4, 0x1000000

    goto/16 :goto_5

    .line 2849
    :cond_10
    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 2855
    :cond_11
    const-string v3, "cluster_count"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    move-object v12, v2

    move-wide v2, v4

    move-object v4, v12

    .line 2857
    goto/16 :goto_6

    :cond_12
    sget-object v2, Lnzr;->a:Loxr;

    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1e

    .line 2858
    sget-object v2, Lnzr;->a:Loxr;

    .line 2859
    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzr;

    iget-object v7, v2, Lnzr;->b:Lnxr;

    .line 2860
    iget-object v6, v7, Lnxr;->f:Ljava/lang/String;

    .line 2862
    iget-object v2, v7, Lnxr;->m:Lnzf;

    if-eqz v2, :cond_13

    iget-object v2, v7, Lnxr;->m:Lnzf;

    iget-object v2, v2, Lnzf;->a:Ljava/lang/Long;

    if-eqz v2, :cond_13

    .line 2863
    const-string v2, "timestamp"

    iget-object v3, v7, Lnxr;->m:Lnzf;

    iget-object v3, v3, Lnzf;->a:Ljava/lang/Long;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2866
    :cond_13
    iget-object v2, v7, Lnxr;->m:Lnzf;

    if-eqz v2, :cond_14

    iget-object v2, v7, Lnxr;->m:Lnzf;

    iget-object v2, v2, Lnzf;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_14

    .line 2867
    const-string v2, "duration_days"

    iget-object v3, v7, Lnxr;->m:Lnzf;

    iget-object v3, v3, Lnzf;->b:Ljava/lang/Integer;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2872
    :cond_14
    const-wide/16 v2, 0x0

    const-wide/16 v8, 0x0

    cmp-long v2, v2, v8

    if-eqz v2, :cond_1b

    .line 2873
    iget-object v2, v7, Lnxr;->c:Ljava/lang/String;

    invoke-static {v2}, Ljvj;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2879
    :goto_9
    const-string v3, "cluster_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2881
    const-string v3, "media_key"

    iget-object v8, v7, Lnxr;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2882
    const-string v3, "equivalence_token"

    iget-object v8, v7, Lnxr;->o:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2883
    const-string v3, "cluster_count"

    iget-object v8, v7, Lnxr;->i:Ljava/lang/Long;

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2884
    const-string v3, "subtitle"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_15

    iget-object v3, v7, Lnxr;->n:[Lnzk;

    if-eqz v3, :cond_15

    iget-object v3, v7, Lnxr;->n:[Lnzk;

    array-length v3, v3

    if-eqz v3, :cond_15

    .line 2887
    iget-object v3, v7, Lnxr;->n:[Lnzk;

    const/4 v8, 0x0

    aget-object v3, v3, v8

    .line 2888
    const-string v8, "subtitle"

    iget-object v3, v3, Lnzk;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2890
    :cond_15
    iget-object v3, v7, Lnxr;->l:Lnxl;

    if-eqz v3, :cond_1d

    .line 2891
    iget-object v3, v7, Lnxr;->l:Lnxl;

    invoke-static {v3}, Ljvj;->a(Lnxl;)I

    move-result v3

    .line 2892
    const-string v8, "acl"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2893
    const/4 v8, 0x2

    if-eq v3, v8, :cond_16

    const/4 v8, -0x1

    if-ne v3, v8, :cond_1c

    .line 2894
    :cond_16
    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2895
    const-wide/32 v4, 0x1000000

    .line 2905
    :goto_a
    iget-object v2, v7, Lnxr;->e:Lnxj;

    if-eqz v2, :cond_17

    .line 2906
    iget-object v2, v7, Lnxr;->e:Lnxj;

    iget v2, v2, Lnxj;->b:I

    sparse-switch v2, :sswitch_data_0

    .line 2929
    :cond_17
    :goto_b
    sget-object v2, Lnzr;->a:Loxr;

    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzr;

    iget-object v2, v2, Lnzr;->e:Lnxo;

    if-eqz v2, :cond_18

    .line 2937
    sget-object v2, Lnzr;->a:Loxr;

    .line 2938
    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzr;

    iget-object v2, v2, Lnzr;->e:Lnxo;

    .line 2940
    iget-object v3, v2, Lnxo;->a:Lnxp;

    if-eqz v3, :cond_18

    .line 2941
    iget-object v3, v2, Lnxo;->a:Lnxp;

    iget v3, v3, Lnxp;->c:I

    packed-switch v3, :pswitch_data_0

    .line 2947
    :goto_c
    iget-object v2, v2, Lnxo;->a:Lnxp;

    iget-object v2, v2, Lnxp;->a:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 2953
    const-wide/32 v2, 0x10000

    or-long/2addr v4, v2

    .line 2957
    :cond_18
    sget-object v2, Lnzr;->a:Loxr;

    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzr;

    iget-object v2, v2, Lnzr;->f:Lnxb;

    if-eqz v2, :cond_19

    .line 2959
    sget-object v2, Lnzr;->a:Loxr;

    .line 2960
    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzr;

    iget-object v2, v2, Lnzr;->f:Lnxb;

    iget-object v2, v2, Lnxb;->a:Lnxa;

    .line 2962
    if-eqz v2, :cond_19

    iget v2, v2, Lnxa;->a:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_19

    .line 2964
    const-wide/32 v2, 0x8000

    or-long/2addr v4, v2

    .line 2969
    :cond_19
    const/4 v3, 0x0

    .line 2970
    sget-object v2, Lnzr;->a:Loxr;

    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzr;

    iget-object v2, v2, Lnzr;->d:Lnxh;

    if-eqz v2, :cond_2b

    sget-object v2, Lnzr;->a:Loxr;

    .line 2971
    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzr;

    iget-object v2, v2, Lnzr;->d:Lnxh;

    iget-object v2, v2, Lnxh;->b:[Ljava/lang/String;

    if-eqz v2, :cond_2b

    .line 2973
    sget-object v2, Lnzr;->a:Loxr;

    .line 2974
    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzr;

    iget-object v2, v2, Lnzr;->d:Lnxh;

    iget-object v2, v2, Lnxh;->b:[Ljava/lang/String;

    .line 2976
    array-length v8, v2

    if-eqz v8, :cond_2b

    .line 2977
    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 2981
    :goto_d
    iget-object v3, v7, Lnxr;->k:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1a

    .line 2982
    const-wide/32 v8, 0x20000

    or-long/2addr v4, v8

    .line 2985
    :cond_1a
    new-instance v3, Ljux;

    move-object/from16 v0, p3

    invoke-direct {v3, v0, v2, p2}, Ljux;-><init>(Ljava/lang/String;Ljava/lang/String;Lnzx;)V

    .line 2987
    const-string v2, "data"

    invoke-static {v3}, Ljux;->a(Ljux;)[B

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-wide v2, v4

    move-object v4, v6

    .line 2988
    goto/16 :goto_6

    .line 2875
    :cond_1b
    const/4 v2, 0x3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, v7, Lnxr;->b:Ljava/lang/String;

    iget-object v10, v7, Lnxr;->c:Ljava/lang/String;

    iget v11, v7, Lnxr;->d:I

    invoke-static {v9, v6, v10, v11}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v3, v8

    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_9

    .line 2897
    :cond_1c
    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_a

    .line 2900
    :cond_1d
    const-string v3, "acl"

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2901
    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2902
    const-wide/32 v4, 0x1000000

    goto/16 :goto_a

    .line 2908
    :sswitch_0
    const-wide/16 v2, 0x1

    or-long/2addr v4, v2

    .line 2909
    goto/16 :goto_b

    .line 2911
    :sswitch_1
    const-wide/16 v2, 0x2

    or-long/2addr v4, v2

    .line 2912
    goto/16 :goto_b

    .line 2914
    :sswitch_2
    const-wide/16 v2, 0x4

    or-long/2addr v4, v2

    .line 2915
    goto/16 :goto_b

    .line 2917
    :sswitch_3
    const-wide/16 v2, 0x8

    or-long/2addr v4, v2

    .line 2918
    goto/16 :goto_b

    .line 2920
    :sswitch_4
    const-wide/16 v2, 0x10

    or-long/2addr v4, v2

    .line 2921
    goto/16 :goto_b

    .line 2923
    :sswitch_5
    const-wide/32 v2, 0x40000

    or-long/2addr v4, v2

    .line 2924
    goto/16 :goto_b

    .line 2926
    :sswitch_6
    const-wide/32 v2, 0x400000

    or-long/2addr v4, v2

    .line 2928
    const-string v2, "content_url"

    iget-object v3, v7, Lnxr;->e:Lnxj;

    iget-object v3, v3, Lnxj;->c:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 2943
    :pswitch_0
    const-wide/16 v8, 0x800

    or-long/2addr v4, v8

    .line 2944
    goto/16 :goto_c

    .line 2946
    :pswitch_1
    const-wide/16 v8, 0x400

    or-long/2addr v4, v8

    goto/16 :goto_c

    .line 2988
    :cond_1e
    sget-object v2, Lnzu;->a:Loxr;

    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2a

    .line 2989
    sget-object v2, Lnzu;->a:Loxr;

    invoke-virtual {p2, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzu;

    iget-object v6, v2, Lnzu;->b:Lnym;

    .line 2991
    const-string v2, "media_key"

    iget-object v4, v6, Lnym;->f:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2994
    iget-object v2, v6, Lnym;->b:Lnyl;

    if-eqz v2, :cond_1f

    .line 2995
    iget-object v2, v6, Lnym;->b:Lnyl;

    iget-object v4, v6, Lnym;->b:Lnyl;

    iget-object v4, v4, Lnyl;->b:Ljava/lang/String;

    .line 2996
    invoke-static {v4}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lnyl;->b:Ljava/lang/String;

    .line 2998
    :cond_1f
    iget-object v2, v6, Lnym;->s:Lnyl;

    if-eqz v2, :cond_20

    .line 2999
    iget-object v2, v6, Lnym;->s:Lnyl;

    iget-object v4, v6, Lnym;->s:Lnyl;

    iget-object v4, v4, Lnyl;->b:Ljava/lang/String;

    .line 3000
    invoke-static {v4}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lnyl;->b:Ljava/lang/String;

    .line 3002
    :cond_20
    iget-object v2, v6, Lnym;->t:Lnyl;

    if-eqz v2, :cond_21

    .line 3003
    iget-object v2, v6, Lnym;->t:Lnyl;

    iget-object v4, v6, Lnym;->t:Lnyl;

    iget-object v4, v4, Lnyl;->b:Ljava/lang/String;

    .line 3004
    invoke-static {v4}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lnyl;->b:Ljava/lang/String;

    .line 3007
    :cond_21
    iget-object v2, v6, Lnym;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_23

    .line 3008
    const-string v2, "comment_count"

    iget-object v4, v6, Lnym;->k:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3012
    :goto_e
    iget-object v2, v6, Lnym;->r:Loae;

    if-eqz v2, :cond_24

    iget-object v2, v6, Lnym;->r:Loae;

    iget-object v2, v2, Loae;->e:Ljava/lang/Integer;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    if-lez v2, :cond_24

    .line 3013
    const-string v2, "plusone_count"

    iget-object v4, v6, Lnym;->r:Loae;

    iget-object v4, v4, Loae;->e:Ljava/lang/Integer;

    .line 3014
    invoke-static {v4}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 3013
    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3019
    :goto_f
    iget-object v2, v6, Lnym;->y:[Lnxr;

    if-eqz v2, :cond_25

    iget-object v2, v6, Lnym;->y:[Lnxr;

    array-length v2, v2

    if-eqz v2, :cond_25

    iget-object v2, v6, Lnym;->y:[Lnxr;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget-object v2, v2, Lnxr;->l:Lnxl;

    if-eqz v2, :cond_25

    .line 3021
    iget-object v2, v6, Lnym;->y:[Lnxr;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget-object v2, v2, Lnxr;->l:Lnxl;

    invoke-static {v2}, Ljvj;->a(Lnxl;)I

    move-result v2

    .line 3022
    const-string v4, "acl"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3027
    :goto_10
    iget-object v2, v6, Lnym;->e:Ljava/lang/String;

    if-eqz v2, :cond_26

    .line 3028
    const-string v2, "photo_id"

    iget-object v4, v6, Lnym;->e:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3033
    :goto_11
    iget-object v2, v6, Lnym;->h:Lnyz;

    if-eqz v2, :cond_22

    .line 3034
    iget-object v2, v6, Lnym;->h:Lnyz;

    iget-object v2, v2, Lnyz;->c:Ljava/lang/String;

    move-object v3, v2

    .line 3037
    :cond_22
    invoke-static {v6}, Llmz;->a(Lnym;)Ljava/lang/String;

    move-result-object v2

    .line 3038
    if-eqz v2, :cond_27

    .line 3039
    const-string v4, "content_url"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3044
    :goto_12
    const-string v2, "cluster_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 3045
    const-string v2, "cluster_count"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 3048
    const/4 v2, 0x0

    new-array v2, v2, [Lnyf;

    iput-object v2, v6, Lnym;->o:[Lnyf;

    .line 3050
    const-string v2, "data"

    invoke-static {v6}, Loxu;->a(Loxu;)[B

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3052
    const-class v2, Lhei;

    invoke-static {p0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhei;

    .line 3053
    invoke-interface {v2, p1}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v4, "gaia_id"

    .line 3054
    invoke-interface {v2, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3055
    const-wide/16 v4, 0x0

    invoke-static {v6, v2}, Ljvj;->a(Lnym;Ljava/lang/String;)J

    move-result-wide v8

    or-long/2addr v4, v8

    .line 3057
    iget-object v2, v6, Lnym;->n:Ljava/lang/Double;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Double;)D

    move-result-wide v6

    .line 3058
    const-string v2, "timestamp"

    const-wide v8, 0x408f400000000000L    # 1000.0

    mul-double/2addr v6, v8

    double-to-long v6, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object v12, v3

    move-wide v2, v4

    move-object v4, v12

    goto/16 :goto_6

    .line 3010
    :cond_23
    const-string v2, "comment_count"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 3016
    :cond_24
    const-string v2, "plusone_count"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_f

    .line 3024
    :cond_25
    const-string v2, "acl"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_10

    .line 3030
    :cond_26
    const-string v2, "photo_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_11

    .line 3041
    :cond_27
    const-string v2, "content_url"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_12

    .line 3064
    :cond_28
    const-string v4, "owner_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 3072
    :cond_29
    const/4 v4, 0x0

    goto/16 :goto_8

    :cond_2a
    move-object v12, v3

    move-wide v2, v4

    move-object v4, v12

    goto/16 :goto_6

    :cond_2b
    move-object v2, v3

    goto/16 :goto_d

    .line 2906
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_6
        0x64 -> :sswitch_5
    .end sparse-switch

    .line 2941
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;I[Ljava/lang/String;Z)V
    .locals 12

    .prologue
    .line 1150
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1151
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1152
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1154
    if-eqz p2, :cond_0

    array-length v1, p2

    if-nez v1, :cond_1

    .line 1232
    :cond_0
    :goto_0
    return-void

    .line 1158
    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1161
    :try_start_0
    invoke-static {p0, p1, p2}, Ljvj;->a(Landroid/content/Context;I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1164
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 1166
    if-eqz p3, :cond_6

    .line 1168
    const-string v1, "all_tiles"

    sget-object v2, Ljvj;->e:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "parent_id"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1174
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1175
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1176
    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1178
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1179
    invoke-static {v0, v1}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    .line 1181
    if-eqz v6, :cond_2

    .line 1182
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 1186
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-gtz v6, :cond_4

    .line 1187
    const-string v4, "all_tiles"

    const-string v5, "cluster_id = ? OR parent_id = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1197
    :catchall_0
    move-exception v1

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1221
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1222
    const-string v0, "EsTileData"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1223
    const-string v0, "[DELETE_TILE], photoIds: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ","

    .line 1224
    invoke-static {v2, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1225
    invoke-static {v10, v11}, Llse;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", duration: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1223
    :cond_3
    throw v1

    .line 1191
    :cond_4
    :try_start_3
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v9, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 1197
    :cond_5
    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1202
    :cond_6
    const-string v1, "all_tiles"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1206
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_8

    .line 1207
    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1208
    invoke-virtual {v9, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v2, p0

    move v3, p1

    move-object v4, v0

    .line 1210
    invoke-static/range {v2 .. v7}, Ljvj;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)V

    goto :goto_2

    .line 1214
    :cond_7
    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1215
    invoke-static {p0, v0, v1}, Ljvj;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_3

    .line 1219
    :cond_8
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1221
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1222
    const-string v0, "EsTileData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1223
    const-string v0, "[DELETE_TILE], photoIds: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    .line 1224
    invoke-static {v1, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1225
    invoke-static {v10, v11}, Llse;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xc

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1230
    :cond_9
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1231
    sget-object v1, Ljvj;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;I[Ljuy;)V
    .locals 13

    .prologue
    .line 1099
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1100
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1101
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 1102
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1104
    :try_start_0
    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    move v7, v1

    :goto_0
    if-ltz v7, :cond_4

    .line 1105
    aget-object v1, p2, v7

    .line 1106
    invoke-virtual {v1}, Ljuy;->a()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    invoke-virtual {v1}, Ljuy;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1107
    invoke-virtual {v1}, Ljuy;->c()[Landroid/content/ContentValues;

    move-result-object v9

    .line 1108
    array-length v1, v9

    add-int/lit8 v1, v1, -0x1

    move v6, v1

    :goto_1
    if-ltz v6, :cond_3

    .line 1109
    aget-object v3, v9, v6

    .line 1110
    const-string v1, "view_id"

    invoke-virtual {v3, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1111
    const-string v2, "media_attr"

    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 1113
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v10, 0x200

    and-long/2addr v4, v10

    const-wide/16 v10, 0x0

    cmp-long v2, v4, v10

    if-eqz v2, :cond_0

    const/4 v4, 0x1

    .line 1114
    :goto_2
    const-string v2, "type"

    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_2

    .line 1115
    const-string v2, "cluster_id"

    .line 1117
    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1115
    invoke-static {v0, v1, v2, v4}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Z)J

    move-result-wide v4

    const-wide/16 v10, -0x1

    cmp-long v2, v4, v10

    if-nez v2, :cond_1

    const-string v2, "all_tiles"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1130
    :goto_3
    invoke-interface {v8, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1108
    add-int/lit8 v1, v6, -0x1

    move v6, v1

    goto :goto_1

    .line 1113
    :cond_0
    const/4 v4, 0x0

    goto :goto_2

    .line 1115
    :cond_1
    const-string v2, "view_order"

    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v2, "all_tiles"

    const-string v10, "_id = ? "

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v11, v12

    invoke-virtual {v0, v2, v3, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 1141
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 1122
    :cond_2
    :try_start_1
    const-string v2, "tile_id"

    .line 1124
    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    .line 1122
    invoke-static/range {v0 .. v5}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    goto :goto_3

    .line 1104
    :cond_3
    add-int/lit8 v1, v7, -0x1

    move v7, v1

    goto/16 :goto_0

    .line 1134
    :cond_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1135
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1137
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1138
    invoke-static {v1}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 1141
    :cond_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1142
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 1238
    const-string v1, "all_tiles"

    sget-object v2, Ljvj;->d:[Ljava/lang/String;

    const-string v3, "cluster_id = ? AND type = ?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    const/4 v0, 0x1

    const-string v5, "2"

    .line 1240
    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p1

    .line 1238
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1244
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1245
    const/4 v9, 0x0

    .line 1246
    const-string v0, "tile_id"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1249
    const-string v1, "all_tiles"

    sget-object v2, Ljvj;->d:[Ljava/lang/String;

    const-string v3, "type = ? AND view_id = ? AND parent_id = ?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "4"

    .line 1251
    aput-object v5, v4, v0

    const/4 v0, 0x1

    aput-object p2, v4, v0

    const/4 v0, 0x2

    aput-object p2, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "view_order"

    const-string v8, "1"

    move-object v0, p1

    .line 1249
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1255
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1256
    const-string v0, "tile_id"

    .line 1257
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1256
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1260
    :goto_1
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1263
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1264
    invoke-static {p0, p1, v11, v0}, Ljvj;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1269
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1260
    :catchall_1
    move-exception v0

    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1269
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1270
    return-void

    :cond_2
    move-object v0, v9

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 1711
    .line 1714
    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "image_url"

    aput-object v0, v2, v8

    const-string v0, "image_width"

    aput-object v0, v2, v9

    const-string v0, "image_height"

    aput-object v0, v2, v1

    .line 1719
    const-string v1, "all_tiles"

    const-string v3, "tile_id = ? AND media_attr & 512 == 0"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p3, v4, v8

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1723
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1724
    new-instance v0, Landroid/content/ContentValues;

    const/4 v2, 0x3

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 1725
    const-string v2, "image_url"

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1726
    const-string v2, "image_width"

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1727
    const-string v2, "image_height"

    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1730
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1733
    if-eqz v0, :cond_0

    .line 1734
    const-string v1, "all_tiles"

    const-string v2, "tile_id = ?"

    new-array v3, v9, [Ljava/lang/String;

    aput-object p2, v3, v8

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1737
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1738
    sget-object v1, Ljvj;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1739
    invoke-static {p2}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1741
    :cond_0
    return-void

    .line 1730
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    move-object v0, v5

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;ZZ)V
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 2321
    if-eqz p5, :cond_1

    if-eqz p4, :cond_0

    .line 2322
    invoke-static {p0, p1, p2}, Ljvj;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 2324
    :goto_0
    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 2325
    const-string v0, "all_tiles"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2332
    :goto_1
    return-void

    .line 2322
    :cond_0
    invoke-static {p0, p1, p2}, Ljvj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :cond_1
    move-wide v0, v2

    goto :goto_0

    .line 2328
    :cond_2
    const-string v2, "view_order"

    invoke-virtual {p3, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2329
    const-string v2, "all_tiles"

    const-string v3, "_id = ? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 2330
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 2329
    invoke-virtual {p0, v2, p3, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1275
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " IN ("

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1276
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 1277
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 1278
    const-string v2, "\'"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1280
    if-lez v0, :cond_0

    .line 1281
    const-string v1, ","

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1276
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1285
    :cond_1
    const-string v0, ")"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1286
    return-void
.end method

.method public static a(Lnzx;Landroid/content/Context;I)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 3425
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p0, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lnzu;

    .line 3426
    if-eqz v6, :cond_0

    iget-object v0, v6, Lnzu;->b:Lnym;

    if-nez v0, :cond_2

    :cond_0
    move-object v4, v3

    .line 3428
    :goto_0
    iget-object v2, p0, Lnzx;->b:Ljava/lang/String;

    move-object v0, p1

    move v1, p2

    invoke-static/range {v0 .. v5}, Ljvd;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Lnyf;Z)V

    .line 3431
    invoke-static {p1, p2, p0, v7}, Ljvj;->a(Landroid/content/Context;ILnzx;I)V

    .line 3434
    if-eqz v6, :cond_1

    iget-object v0, v6, Lnzu;->b:Lnym;

    if-eqz v0, :cond_1

    .line 3435
    new-array v2, v7, [Lnzx;

    aput-object p0, v2, v5

    new-array v3, v5, [Lotf;

    iget-object v0, v6, Lnzu;->b:Lnym;

    iget-object v4, v0, Lnym;->h:Lnyz;

    move-object v0, p1

    move v1, p2

    move v5, v7

    invoke-static/range {v0 .. v5}, Ljvd;->a(Landroid/content/Context;I[Lnzx;[Lotf;Lnyz;Z)V

    .line 3439
    :cond_1
    return-void

    .line 3426
    :cond_2
    iget-object v0, v6, Lnzu;->b:Lnym;

    iget-object v4, v0, Lnym;->o:[Lnyf;

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/Long;Lnzx;)Z
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2103
    sget-object v0, Lnzr;->a:Loxr;

    .line 2104
    invoke-virtual {p3, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    iget-object v1, v0, Lnzr;->b:Lnxr;

    .line 2108
    const/4 v0, 0x0

    .line 2110
    :try_start_0
    const-string v2, "SELECT cluster_id FROM all_tiles WHERE view_id = ?  AND type = ?  AND view_order < ?  ORDER BY view_order DESC  LIMIT 1"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x2

    .line 2111
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 2112
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 2110
    invoke-static {p0, v2, v3}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2116
    :goto_0
    iget-object v2, v1, Lnxr;->b:Ljava/lang/String;

    iget-object v3, v1, Lnxr;->f:Ljava/lang/String;

    iget-object v4, v1, Lnxr;->c:Ljava/lang/String;

    iget v1, v1, Lnxr;->d:I

    invoke-static {v2, v3, v4, v1}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 2118
    new-array v2, v9, [Ljava/lang/String;

    aput-object v1, v2, v8

    invoke-static {v10, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2119
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)[B
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 941
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 942
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 943
    new-array v2, v7, [Ljava/lang/String;

    const-string v1, "data"

    aput-object v1, v2, v6

    .line 949
    if-eqz p3, :cond_1

    .line 950
    const-string v3, "view_id = ? AND tile_id = ?  AND media_attr & 512 == 0"

    .line 951
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    aput-object p3, v4, v6

    aput-object p2, v4, v7

    .line 957
    :goto_0
    if-eqz p4, :cond_0

    .line 958
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v1, " AND media_attr & 512 == 0"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v3, v1

    .line 961
    :cond_0
    const-string v1, "all_tiles"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 966
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 967
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 970
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 973
    :goto_2
    return-object v5

    .line 953
    :cond_1
    const-string v3, "tile_id = ?"

    .line 954
    new-array v4, v7, [Ljava/lang/String;

    aput-object p2, v4, v6

    goto :goto_0

    .line 958
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 970
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1014
    const-string v1, "all_tiles"

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1018
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1020
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static a(Lmlt;)[Lnzx;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1309
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    move v0, v3

    .line 1310
    :goto_0
    iget-object v1, p0, Lmlt;->c:[Lnyz;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1311
    iget-object v1, p0, Lmlt;->c:[Lnyz;

    aget-object v1, v1, v0

    iget-object v1, v1, Lnyz;->b:Ljava/lang/String;

    iget-object v2, p0, Lmlt;->c:[Lnyz;

    aget-object v2, v2, v0

    invoke-virtual {v5, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1310
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1315
    :cond_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move v1, v3

    .line 1316
    :goto_1
    iget-object v0, p0, Lmlt;->a:[Lnzx;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 1317
    iget-object v0, p0, Lmlt;->a:[Lnzx;

    aget-object v0, v0, v1

    sget-object v2, Lnzr;->a:Loxr;

    .line 1318
    invoke-virtual {v0, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    .line 1319
    iget-object v0, v0, Lnzr;->b:Lnxr;

    .line 1320
    iget-object v0, v0, Lnxr;->b:Ljava/lang/String;

    iget-object v2, p0, Lmlt;->a:[Lnzx;

    aget-object v2, v2, v1

    invoke-virtual {v6, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1316
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1324
    :cond_1
    new-instance v7, Ljava/util/LinkedHashMap;

    invoke-direct {v7}, Ljava/util/LinkedHashMap;-><init>()V

    move v2, v3

    .line 1326
    :goto_2
    iget-object v0, p0, Lmlt;->b:[Lnzx;

    array-length v0, v0

    if-ge v2, v0, :cond_5

    .line 1327
    iget-object v0, p0, Lmlt;->b:[Lnzx;

    aget-object v0, v0, v2

    sget-object v1, Lnzu;->a:Loxr;

    .line 1328
    invoke-virtual {v0, v1}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 1329
    iget-object v8, v0, Lnzu;->b:Lnym;

    .line 1330
    iget-object v1, v8, Lnym;->z:[Lnxt;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 1331
    iget-object v1, v8, Lnym;->z:[Lnxt;

    aget-object v1, v1, v3

    iget-object v9, v1, Lnxt;->b:Ljava/lang/String;

    .line 1333
    invoke-virtual {v7, v9}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1334
    if-nez v1, :cond_2

    .line 1335
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1336
    invoke-virtual {v7, v9, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338
    :cond_2
    iget-object v9, p0, Lmlt;->b:[Lnzx;

    aget-object v9, v9, v2

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1342
    :cond_3
    iget-object v1, v8, Lnym;->i:Lnzm;

    if-eqz v1, :cond_4

    iget-object v1, v8, Lnym;->i:Lnzm;

    iget-object v1, v1, Lnzm;->b:Ljava/lang/String;

    :goto_3
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnyz;

    iput-object v1, v8, Lnym;->h:Lnyz;

    .line 1343
    iget-object v1, p0, Lmlt;->b:[Lnzx;

    aget-object v1, v1, v2

    sget-object v8, Lnzu;->a:Loxr;

    invoke-virtual {v1, v8, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 1326
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    move-object v1, v4

    .line 1342
    goto :goto_3

    .line 1348
    :cond_5
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1349
    invoke-virtual {v7}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_6
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1350
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzx;

    .line 1351
    if-nez v1, :cond_7

    .line 1352
    const-string v0, "EsTileData"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_4

    .line 1358
    :cond_7
    invoke-virtual {v7, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1359
    iget-object v2, v1, Lnzx;->f:Lnyl;

    if-nez v2, :cond_8

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1360
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzx;

    iget-object v2, v2, Lnzx;->f:Lnyl;

    iput-object v2, v1, Lnzx;->f:Lnyl;

    .line 1362
    :cond_8
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lnzx;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnzx;

    iput-object v0, v1, Lnzx;->j:[Lnzx;

    .line 1365
    sget-object v0, Lnzr;->a:Loxr;

    .line 1366
    invoke-virtual {v1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    .line 1367
    iget-object v2, v0, Lnzr;->b:Lnxr;

    iget-object v2, v2, Lnxr;->g:Lnzm;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lnzr;->b:Lnxr;

    iget-object v2, v2, Lnxr;->g:Lnzm;

    iget-object v2, v2, Lnzm;->b:Ljava/lang/String;

    :goto_5
    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnyz;

    .line 1369
    iget-object v10, v0, Lnzr;->b:Lnxr;

    if-nez v2, :cond_a

    move-object v2, v4

    :goto_6
    iput-object v2, v10, Lnxr;->f:Ljava/lang/String;

    .line 1371
    sget-object v2, Lnzr;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 1373
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    move-object v2, v4

    .line 1367
    goto :goto_5

    .line 1369
    :cond_a
    iget-object v2, v2, Lnyz;->c:Ljava/lang/String;

    goto :goto_6

    .line 1375
    :cond_b
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lnzx;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnzx;

    return-object v0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)J
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2748
    const-string v1, "all_tiles"

    sget-object v2, Ljvj;->c:[Ljava/lang/String;

    const-string v3, "view_id = ? AND tile_id = ?  AND media_attr & 512 != 0"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v6

    const/4 v0, 0x1

    aput-object p2, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2753
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2755
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 2753
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2755
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 901
    const-string v0, "~"

    invoke-static {v0}, Ljvj;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 739
    if-nez p0, :cond_0

    .line 740
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cluster ID must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 742
    :cond_0
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 743
    if-gez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/HashSet;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 3295
    new-array v4, v2, [Ljava/lang/String;

    aput-object p1, v4, v3

    .line 3297
    const-string v0, "SELECT count(*) FROM all_tiles WHERE view_id = ? AND media_attr & 512 != 0 AND tile_id LIKE \'~post:%\'"

    invoke-static {p0, v0, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v8, v0

    .line 3301
    if-lez v8, :cond_1

    .line 3302
    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "tile_id"

    aput-object v0, v2, v3

    .line 3303
    const-string v1, "all_tiles"

    const-string v3, "view_id = ? AND media_attr & 512 != 0 AND tile_id LIKE \'~post:%\'"

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3307
    :try_start_0
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5, v8}, Ljava/util/HashSet;-><init>(I)V

    .line 3308
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3309
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3312
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3315
    :cond_1
    return-object v5
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2439
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2440
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2442
    invoke-static {p0, p1, v0}, Ljvj;->a(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 2443
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2445
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 2446
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljad;

    .line 2447
    invoke-virtual {v0}, Ljad;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2445
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 2450
    :cond_0
    return-object v3
.end method

.method public static b(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 3450
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 3451
    const-string v0, "tile_id IN ("

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3454
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v4

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 3455
    invoke-virtual {v0}, Lizu;->j()Z

    move-result v7

    if-nez v7, :cond_8

    invoke-virtual {v0}, Lizu;->k()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 3456
    invoke-virtual {v0}, Lizu;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 3457
    const/16 v0, 0x2c

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    :goto_1
    move v2, v0

    .line 3461
    goto :goto_0

    .line 3463
    :cond_0
    if-nez v2, :cond_1

    .line 3520
    :goto_2
    return-object p2

    .line 3467
    :cond_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3468
    const/16 v0, 0x29

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3470
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 3471
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3473
    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/String;

    const-string v2, "tile_id"

    aput-object v2, v3, v4

    const-string v2, "owner_id"

    aput-object v2, v3, v1

    const-string v2, "photo_id"

    aput-object v2, v3, v8

    .line 3480
    const-string v2, "all_tiles"

    .line 3481
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    move-object v9, v5

    .line 3480
    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3484
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 3487
    :goto_3
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3488
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3489
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3490
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 3492
    new-instance v3, Landroid/util/Pair;

    .line 3493
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3492
    invoke-interface {v8, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 3496
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3499
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 3500
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 3501
    invoke-virtual {v0}, Lizu;->a()Ljava/lang/String;

    move-result-object v3

    .line 3502
    invoke-virtual {v0}, Lizu;->j()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v0}, Lizu;->k()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3503
    invoke-interface {v8, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/util/Pair;

    .line 3504
    if-nez v2, :cond_5

    .line 3505
    const-string v1, "EsTileData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3506
    const-string v2, "EsTileData"

    const-string v4, "No photo ID found for tile ID: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3508
    :cond_3
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 3506
    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 3510
    :cond_5
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    .line 3511
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 3512
    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v6

    move-object v0, p0

    .line 3511
    invoke-static/range {v0 .. v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    .line 3513
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 3516
    :cond_6
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    move-object p2, v7

    .line 3520
    goto/16 :goto_2

    :cond_8
    move v0, v2

    goto/16 :goto_1
.end method

.method private static b(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lnzx;Ljava/util/HashSet;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lnzx;",
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;J)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2177
    invoke-static {p3}, Ljvj;->a(Lnzx;)Ljava/lang/String;

    move-result-object v8

    .line 2178
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2179
    invoke-static {p0, p1, p3, v2}, Ljvj;->a(Landroid/content/Context;ILnzx;Landroid/content/ContentValues;)V

    .line 2180
    const-wide/16 v0, -0x1

    cmp-long v0, p5, v0

    if-eqz v0, :cond_0

    .line 2181
    const-string v0, "last_refresh_time"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2183
    :cond_0
    const-string v0, "media_attr"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2184
    const-wide/16 v4, 0x200

    or-long/2addr v0, v4

    .line 2185
    const-wide/16 v4, 0x2000

    or-long/2addr v0, v4

    .line 2186
    const-string v3, "media_attr"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2190
    const-string v0, "tile_id"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2191
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2192
    invoke-static {v5, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2194
    :goto_0
    const-string v3, "all_tiles"

    if-eqz v0, :cond_3

    const-string v1, "(tile_id = ? OR tile_id = ?) AND media_attr & 512 != 0"

    :goto_1
    if-eqz v0, :cond_4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v8, v0, v4

    const/4 v4, 0x1

    aput-object v5, v0, v4

    :goto_2
    invoke-virtual {p2, v3, v2, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2199
    invoke-static {p2, p3, p4}, Lidg;->a(Landroid/database/sqlite/SQLiteDatabase;Lnzx;Ljava/util/HashSet;)V

    .line 2202
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "view_id"

    aput-object v1, v2, v0

    .line 2205
    const-string v1, "all_tiles"

    const-string v3, "tile_id = ? AND media_attr & 512 != 0"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2210
    :cond_1
    :goto_3
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2211
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2212
    invoke-static {v0}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2213
    if-eqz p4, :cond_1

    .line 2214
    invoke-virtual {p4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 2218
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2192
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2194
    :cond_3
    const-string v1, "tile_id = ? AND media_attr & 512 != 0"

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v8, v0, v4

    goto :goto_2

    .line 2218
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2220
    if-eqz p4, :cond_6

    .line 2221
    invoke-static {v8}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2223
    :cond_6
    return-void
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 1652
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1653
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1655
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 1656
    const-string v2, "title"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1658
    const-string v2, "all_tiles"

    const-string v3, "tile_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1661
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1662
    sget-object v1, Ljvj;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1663
    invoke-static {p2}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1664
    return-void
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    .prologue
    .line 1818
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1819
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1820
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 1822
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 1823
    const-string v3, "view_id"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1825
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1826
    const-string v3, "resume_token"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1830
    :goto_0
    if-eqz p4, :cond_0

    .line 1831
    const-string v3, "last_refresh_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1834
    :cond_0
    const-string v3, "SELECT count(*) FROM tile_requests WHERE view_id = ?"

    invoke-static {v0, v3, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 1835
    const-string v1, "tile_requests"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1840
    :goto_1
    return-void

    .line 1828
    :cond_1
    const-string v3, "resume_token"

    invoke-virtual {v2, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1837
    :cond_2
    const-string v3, "tile_requests"

    const-string v4, "view_id = ?"

    invoke-virtual {v0, v3, v2, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static c()J
    .locals 2

    .prologue
    .line 2677
    sget-object v0, Ljvj;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;)Landroid/util/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 3371
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 3372
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3373
    new-array v4, v2, [Ljava/lang/String;

    aput-object p2, v4, v1

    .line 3374
    const-string v1, "tile_requests"

    sget-object v2, Ljvj;->f:[Ljava/lang/String;

    const-string v3, "view_id = ?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3379
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3380
    new-instance v5, Landroid/util/Pair;

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v5, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3385
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v5

    .line 3382
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 751
    if-nez p0, :cond_0

    .line 752
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cluster ID must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 754
    :cond_0
    const-string v1, ":"

    invoke-static {p0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 755
    const-string v2, "album"

    aget-object v3, v1, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    array-length v2, v1

    if-lt v2, v6, :cond_2

    .line 756
    array-length v2, v1

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 757
    aget-object v0, v1, v5

    .line 763
    :cond_1
    :goto_0
    return-object v0

    .line 760
    :cond_2
    const-string v2, "albums"

    aget-object v3, v1, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    array-length v2, v1

    if-ne v2, v6, :cond_1

    .line 761
    aget-object v0, v1, v5

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lnzx;Ljava/util/HashSet;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lnzx;",
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;J)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2237
    invoke-static {p3}, Ljvj;->a(Lnzx;)Ljava/lang/String;

    move-result-object v8

    .line 2238
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2239
    invoke-static {p0, p1, p3, v0}, Ljvj;->a(Landroid/content/Context;ILnzx;Landroid/content/ContentValues;)V

    .line 2240
    const-wide/16 v2, -0x1

    cmp-long v1, p5, v2

    if-eqz v1, :cond_0

    .line 2241
    const-string v1, "last_refresh_time"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2243
    :cond_0
    const-string v1, "media_attr"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2244
    const-wide/16 v4, 0x2000

    or-long/2addr v2, v4

    .line 2245
    const-string v1, "media_attr"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2247
    const-string v1, "all_tiles"

    const-string v2, "view_id LIKE \'notification:%\' AND tile_id = ? AND media_attr & 512 == 0"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v8, v3, v4

    invoke-virtual {p2, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 2250
    if-lez v1, :cond_3

    .line 2251
    const-string v1, "tile_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2252
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "view_id"

    aput-object v3, v2, v1

    .line 2255
    const-string v1, "all_tiles"

    const-string v3, "view_id LIKE \'notification:%\' AND tile_id = ? AND media_attr & 512 == 0"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2259
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2260
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2261
    invoke-static {v0}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2262
    if-eqz p4, :cond_1

    .line 2263
    invoke-virtual {p4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2267
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2269
    if-eqz p4, :cond_3

    .line 2270
    invoke-static {v8}, Ljvj;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2273
    :cond_3
    return-void
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1703
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 1704
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1706
    invoke-static {p0, v0, p2, p3}, Ljvj;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1707
    return-void
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 771
    if-nez p0, :cond_0

    .line 772
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cluster ID must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 774
    :cond_0
    const-string v0, ":"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 775
    const-string v1, "album"

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    array-length v1, v0

    if-lt v1, v3, :cond_2

    .line 776
    array-length v1, v0

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 777
    aget-object v0, v0, v3

    .line 781
    :goto_0
    return-object v0

    .line 779
    :cond_1
    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 781
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;ILjava/lang/String;)Ljava/util/Set;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 3391
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3417
    :goto_0
    return-object v4

    .line 3395
    :cond_0
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 3396
    const-class v0, Lhzr;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzr;

    .line 3397
    invoke-virtual {v0, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3398
    const-string v1, "all_tiles"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "photo_id"

    aput-object v3, v2, v5

    const-string v3, "view_id=\'"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3407
    :cond_1
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3408
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3409
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 3410
    invoke-virtual {v8, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 3414
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v4, v8

    .line 3417
    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 804
    if-nez p0, :cond_0

    .line 805
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cluster ID must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 807
    :cond_0
    const-string v0, ":"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 808
    const-string v1, "album"

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    array-length v1, v0

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 809
    const/4 v1, 0x3

    aget-object v0, v0, v1

    .line 811
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 819
    invoke-static {p0}, Ljvj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 820
    const/4 v0, 0x0

    .line 821
    const-string v2, "PLUS_EVENT"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 822
    const/4 v0, 0x1

    .line 830
    :cond_0
    :goto_0
    return v0

    .line 823
    :cond_1
    const-string v2, "PHOTO_COLLECTION"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 824
    const/4 v0, 0x2

    goto :goto_0

    .line 825
    :cond_2
    const-string v2, "ALBUM"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 826
    const/4 v0, 0x3

    goto :goto_0

    .line 827
    :cond_3
    const-string v2, "AD_HOC"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 828
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static g(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 840
    if-nez p0, :cond_0

    .line 841
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cluster ID must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 843
    :cond_0
    const-string v0, ":"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 844
    const-string v1, "album"

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    array-length v1, v0

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 845
    const/4 v1, 0x4

    aget-object v0, v0, v1

    .line 847
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 855
    if-nez p0, :cond_0

    .line 856
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "album ID must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 859
    :cond_0
    const-string v0, "@"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 860
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const-string v1, "~folder"

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 861
    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 863
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 871
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Ljvj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 872
    :goto_0
    const-string v1, "PLUS_EVENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 871
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 880
    if-nez p0, :cond_0

    .line 881
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cluster ID must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 883
    :cond_0
    invoke-static {p0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 884
    if-eqz v0, :cond_1

    .line 885
    invoke-static {v0}, Ljvj;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 887
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 908
    const/4 v1, 0x3

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 909
    const-string v0, "~folder@"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, ""

    :cond_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v4, "ALBUM"

    .line 908
    invoke-static {v6, v6, v0, v4}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 909
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static l(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 917
    invoke-static {p0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 918
    invoke-static {p0}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljvj;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 925
    invoke-static {p0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 927
    if-nez v1, :cond_1

    .line 932
    :cond_0
    :goto_0
    return v0

    .line 931
    :cond_1
    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    const/4 v3, 0x7

    if-ne v2, v3, :cond_0

    const-string v2, "~folder"

    .line 932
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static n(Ljava/lang/String;)Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1891
    invoke-static {p0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "~pending_photos_of_user"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1893
    invoke-static {p0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "~approved_photos_of_user"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1899
    const/4 v0, 0x0

    const-string v1, "~photos_of_user_home"

    const-string v2, "ALBUM"

    invoke-static {v0, p0, v1, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1901
    const/4 v1, 0x3

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static p(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2417
    sget-object v0, Ljvj;->b:Landroid/net/Uri;

    .line 2418
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2419
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2420
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static q(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2424
    sget-object v0, Ljvj;->a:Landroid/net/Uri;

    .line 2425
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2426
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2427
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
