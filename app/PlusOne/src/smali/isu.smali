.class public final Lisu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lisu;",
        ">;"
    }
.end annotation


# static fields
.field private static f:Ljava/util/Calendar;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:J

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lisu;->f:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lisu;->b:I

    return v0
.end method

.method public a(Lisu;)I
    .locals 2

    .prologue
    .line 94
    iget v0, p0, Lisu;->c:I

    invoke-virtual {p1}, Lisu;->c()I

    move-result v1

    sub-int/2addr v0, v1

    .line 95
    if-eqz v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    iget v0, p0, Lisu;->a:I

    invoke-virtual {p1}, Lisu;->b()I

    move-result v1

    sub-int/2addr v0, v1

    .line 99
    if-nez v0, :cond_0

    .line 102
    iget v0, p0, Lisu;->b:I

    invoke-virtual {p1}, Lisu;->a()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 46
    sget-object v1, Lisu;->f:Ljava/util/Calendar;

    monitor-enter v1

    .line 48
    :try_start_0
    sget-object v0, Lisu;->f:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 49
    sget-object v0, Lisu;->f:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lisu;->b:I

    .line 50
    sget-object v0, Lisu;->f:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lisu;->a:I

    .line 51
    sget-object v0, Lisu;->f:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lisu;->c:I

    .line 52
    iput-wide p1, p0, Lisu;->d:J

    .line 53
    const/4 v0, 0x3

    .line 54
    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lisu;->e:Ljava/lang/String;

    .line 55
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lisu;->a:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lisu;->c:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lisu;

    invoke-virtual {p0, p1}, Lisu;->a(Lisu;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 70
    if-ne p0, p1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 74
    goto :goto_0

    .line 76
    :cond_2
    instance-of v2, p1, Lisu;

    if-nez v2, :cond_3

    move v0, v1

    .line 77
    goto :goto_0

    .line 79
    :cond_3
    check-cast p1, Lisu;

    .line 80
    iget v2, p0, Lisu;->c:I

    iget v3, p1, Lisu;->c:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 81
    goto :goto_0

    .line 83
    :cond_4
    iget v2, p0, Lisu;->a:I

    iget v3, p1, Lisu;->a:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_5
    iget v2, p0, Lisu;->b:I

    iget v3, p1, Lisu;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 87
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lisu;->b:I

    add-int/lit8 v0, v0, 0x1f

    .line 63
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lisu;->a:I

    add-int/2addr v0, v1

    .line 64
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lisu;->c:I

    add-int/2addr v0, v1

    .line 65
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lisu;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 122
    const/4 v0, 0x3

    .line 123
    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iget-wide v2, p0, Lisu;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lisu;->e:Ljava/lang/String;

    .line 125
    :cond_0
    iget-object v0, p0, Lisu;->e:Ljava/lang/String;

    return-object v0
.end method
