.class public Lhin;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhio;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhin;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lhin;->b:Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;

    .line 28
    invoke-virtual {p1, p0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 29
    return-void
.end method

.method public a(Lhio;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lhin;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    iget-object v0, p0, Lhin;->b:Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lhin;->b:Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->a()Z

    move-result v0

    invoke-interface {p1, v0}, Lhio;->a(Z)V

    .line 36
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lhin;->b:Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhin;->b:Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/acl2/DomainRestrictionToggleView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    .line 44
    invoke-static {}, Lhis;->a()V

    .line 45
    iget-object v0, p0, Lhin;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 46
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 47
    iget-object v0, p0, Lhin;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhio;

    invoke-interface {v0, p2}, Lhio;->a(Z)V

    .line 46
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 49
    :cond_0
    invoke-static {}, Lhis;->b()V

    .line 50
    return-void
.end method
