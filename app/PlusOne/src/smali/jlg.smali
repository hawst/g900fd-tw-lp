.class public final Ljlg;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lbc;
.implements Ljit;
.implements Ljlk;
.implements Lkch;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Landroid/widget/AdapterView$OnItemLongClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ljit;",
        "Ljlk;",
        "Lkch;"
    }
.end annotation


# instance fields
.field private N:Lhee;

.field private O:Landroid/widget/ListView;

.field private P:Ljlf;

.field private Q:Z

.field private R:Z

.field private S:Lkci;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Llol;-><init>()V

    .line 47
    iput-boolean v0, p0, Ljlg;->Q:Z

    .line 48
    iput-boolean v0, p0, Ljlg;->R:Z

    return-void
.end method


# virtual methods
.method public K_()V
    .locals 5

    .prologue
    .line 207
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljlg;->R:Z

    .line 208
    iget-object v0, p0, Ljlg;->S:Lkci;

    invoke-virtual {v0}, Lkci;->c()V

    .line 209
    invoke-virtual {p0}, Ljlg;->n()Lz;

    move-result-object v0

    const-class v1, Ljio;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljio;

    .line 210
    iget-object v1, p0, Ljlg;->N:Lhee;

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    .line 212
    const-string v2, "account_name"

    .line 213
    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "effective_gaia_id"

    .line 214
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Ljin;->a:Ljin;

    sget-object v4, Ljix;->d:Ljix;

    .line 212
    invoke-interface {v0, v2, v1, v3, v4}, Ljio;->a(Ljava/lang/String;Ljava/lang/String;Ljin;Ljix;)V

    .line 216
    return-void
.end method

.method public U()I
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method

.method public V()V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public W()V
    .locals 4

    .prologue
    .line 220
    iget-object v0, p0, Ljlg;->N:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v1

    .line 221
    invoke-virtual {p0}, Ljlg;->n()Lz;

    move-result-object v0

    const-class v2, Ljio;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljio;

    .line 222
    const-string v2, "account_name"

    .line 223
    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "effective_gaia_id"

    .line 224
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 222
    invoke-interface {v0, v2, v1}, Ljio;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    return-void
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Ljlg;->O:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a(Landroid/widget/AbsListView;)Z

    move-result v0

    .line 192
    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 70
    const v0, 0x7f0400c1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 72
    const v0, 0x7f1002f4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Ljlg;->O:Landroid/widget/ListView;

    .line 73
    iget-object v0, p0, Ljlg;->O:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 74
    iget-object v0, p0, Ljlg;->O:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 76
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    invoke-static {p1}, Ljle;->a(I)Ljle;

    move-result-object v0

    .line 126
    iget-object v1, p0, Ljlg;->N:Lhee;

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    .line 127
    new-instance v2, Ljjl;

    iget-object v3, p0, Ljlg;->at:Llnl;

    const-string v4, "account_name"

    .line 128
    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "effective_gaia_id"

    .line 129
    invoke-interface {v1, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v4, v1, v0}, Ljjl;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljle;)V

    return-object v2
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 62
    if-eqz p1, :cond_0

    .line 63
    const-string v0, "is_active"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ljlg;->Q:Z

    .line 65
    :cond_0
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 135
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    invoke-static {v0}, Ljle;->a(I)Ljle;

    move-result-object v0

    .line 136
    iget-object v1, p0, Ljlg;->P:Ljlf;

    new-instance v2, Ljkx;

    invoke-direct {v2, p2}, Ljkx;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v1, v0, v2}, Ljlf;->a(Ljle;Ljkx;)V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljlg;->R:Z

    .line 141
    iget-object v0, p0, Ljlg;->S:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 142
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Ljlg;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public aQ_()Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Ljlg;->R:Z

    return v0
.end method

.method public aa()I
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method public ab()Z
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 166
    iput-boolean p1, p0, Ljlg;->Q:Z

    .line 167
    iget-object v0, p0, Ljlg;->S:Lkci;

    invoke-virtual {v0, p1}, Lkci;->b(Z)V

    .line 169
    if-eqz p1, :cond_0

    .line 171
    invoke-virtual {p0}, Ljlg;->n()Lz;

    move-result-object v0

    const-class v1, Ljio;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljio;

    .line 172
    iget-object v1, p0, Ljlg;->N:Lhee;

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    .line 173
    const-string v2, "account_name"

    .line 174
    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "effective_gaia_id"

    .line 175
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 173
    invoke-interface {v0, v2, v1}, Ljio;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 56
    iget-object v0, p0, Ljlg;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Ljlg;->N:Lhee;

    .line 57
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 81
    invoke-super {p0, p1}, Llol;->d(Landroid/os/Bundle;)V

    .line 82
    iget-object v0, p0, Ljlg;->N:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v1

    .line 84
    new-instance v0, Ljlf;

    invoke-virtual {p0}, Ljlg;->n()Lz;

    move-result-object v2

    invoke-direct {v0, v2, p0}, Ljlf;-><init>(Landroid/content/Context;Ljlk;)V

    iput-object v0, p0, Ljlg;->P:Ljlf;

    .line 85
    iget-object v0, p0, Ljlg;->O:Landroid/widget/ListView;

    iget-object v2, p0, Ljlg;->P:Ljlf;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 88
    invoke-virtual {p0}, Ljlg;->w()Lbb;

    move-result-object v0

    sget-object v2, Ljle;->a:Ljle;

    invoke-virtual {v2}, Ljle;->a()I

    move-result v2

    invoke-virtual {v0, v2, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 89
    invoke-virtual {p0}, Ljlg;->w()Lbb;

    move-result-object v0

    sget-object v2, Ljle;->b:Ljle;

    invoke-virtual {v2}, Ljle;->a()I

    move-result v2

    invoke-virtual {v0, v2, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 90
    invoke-virtual {p0}, Ljlg;->w()Lbb;

    move-result-object v0

    sget-object v2, Ljle;->c:Ljle;

    invoke-virtual {v2}, Ljle;->a()I

    move-result v2

    invoke-virtual {v0, v2, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 93
    new-instance v0, Lkci;

    invoke-virtual {p0}, Ljlg;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Ljlg;->av:Llqm;

    const v4, 0x7f1002f3

    invoke-direct {v0, v2, v3, v4, p0}, Lkci;-><init>(Landroid/app/Activity;Llqr;ILkch;)V

    iput-object v0, p0, Ljlg;->S:Lkci;

    .line 95
    invoke-virtual {p0}, Ljlg;->n()Lz;

    move-result-object v0

    const-class v2, Lkcj;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 96
    iget-object v0, p0, Ljlg;->S:Lkci;

    iget-boolean v2, p0, Ljlg;->Q:Z

    invoke-virtual {v0, v2}, Lkci;->b(Z)V

    .line 99
    invoke-virtual {p0}, Ljlg;->n()Lz;

    move-result-object v0

    const-class v2, Ljio;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljio;

    .line 100
    const-string v2, "account_name"

    .line 101
    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "effective_gaia_id"

    .line 102
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Ljin;->a:Ljin;

    sget-object v4, Ljix;->d:Ljix;

    .line 100
    invoke-interface {v0, v2, v1, v3, v4}, Ljio;->a(Ljava/lang/String;Ljava/lang/String;Ljin;Ljix;)V

    .line 104
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 187
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 108
    const-string v0, "is_active"

    iget-boolean v1, p0, Ljlg;->Q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 110
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 114
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 119
    const/4 v0, 0x1

    return v0
.end method
