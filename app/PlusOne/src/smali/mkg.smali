.class public final Lmkg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnup;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5587
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5590
    iput-object v0, p0, Lmkg;->apiHeader:Llyq;

    .line 5593
    iput-object v0, p0, Lmkg;->a:Lnup;

    .line 5587
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5610
    const/4 v0, 0x0

    .line 5611
    iget-object v1, p0, Lmkg;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 5612
    const/4 v0, 0x1

    iget-object v1, p0, Lmkg;->apiHeader:Llyq;

    .line 5613
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5615
    :cond_0
    iget-object v1, p0, Lmkg;->a:Lnup;

    if-eqz v1, :cond_1

    .line 5616
    const/4 v1, 0x2

    iget-object v2, p0, Lmkg;->a:Lnup;

    .line 5617
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5619
    :cond_1
    iget-object v1, p0, Lmkg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5620
    iput v0, p0, Lmkg;->ai:I

    .line 5621
    return v0
.end method

.method public a(Loxn;)Lmkg;
    .locals 2

    .prologue
    .line 5629
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5630
    sparse-switch v0, :sswitch_data_0

    .line 5634
    iget-object v1, p0, Lmkg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5635
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkg;->ah:Ljava/util/List;

    .line 5638
    :cond_1
    iget-object v1, p0, Lmkg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5640
    :sswitch_0
    return-object p0

    .line 5645
    :sswitch_1
    iget-object v0, p0, Lmkg;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 5646
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmkg;->apiHeader:Llyq;

    .line 5648
    :cond_2
    iget-object v0, p0, Lmkg;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5652
    :sswitch_2
    iget-object v0, p0, Lmkg;->a:Lnup;

    if-nez v0, :cond_3

    .line 5653
    new-instance v0, Lnup;

    invoke-direct {v0}, Lnup;-><init>()V

    iput-object v0, p0, Lmkg;->a:Lnup;

    .line 5655
    :cond_3
    iget-object v0, p0, Lmkg;->a:Lnup;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5630
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5598
    iget-object v0, p0, Lmkg;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 5599
    const/4 v0, 0x1

    iget-object v1, p0, Lmkg;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5601
    :cond_0
    iget-object v0, p0, Lmkg;->a:Lnup;

    if-eqz v0, :cond_1

    .line 5602
    const/4 v0, 0x2

    iget-object v1, p0, Lmkg;->a:Lnup;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5604
    :cond_1
    iget-object v0, p0, Lmkg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5606
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5583
    invoke-virtual {p0, p1}, Lmkg;->a(Loxn;)Lmkg;

    move-result-object v0

    return-object v0
.end method
