.class public final Lmfa;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvl;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8989
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8992
    iput-object v0, p0, Lmfa;->apiHeader:Llyq;

    .line 8995
    iput-object v0, p0, Lmfa;->a:Lnvl;

    .line 8989
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9012
    const/4 v0, 0x0

    .line 9013
    iget-object v1, p0, Lmfa;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 9014
    const/4 v0, 0x1

    iget-object v1, p0, Lmfa;->apiHeader:Llyq;

    .line 9015
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9017
    :cond_0
    iget-object v1, p0, Lmfa;->a:Lnvl;

    if-eqz v1, :cond_1

    .line 9018
    const/4 v1, 0x2

    iget-object v2, p0, Lmfa;->a:Lnvl;

    .line 9019
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9021
    :cond_1
    iget-object v1, p0, Lmfa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9022
    iput v0, p0, Lmfa;->ai:I

    .line 9023
    return v0
.end method

.method public a(Loxn;)Lmfa;
    .locals 2

    .prologue
    .line 9031
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9032
    sparse-switch v0, :sswitch_data_0

    .line 9036
    iget-object v1, p0, Lmfa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9037
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfa;->ah:Ljava/util/List;

    .line 9040
    :cond_1
    iget-object v1, p0, Lmfa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9042
    :sswitch_0
    return-object p0

    .line 9047
    :sswitch_1
    iget-object v0, p0, Lmfa;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 9048
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfa;->apiHeader:Llyq;

    .line 9050
    :cond_2
    iget-object v0, p0, Lmfa;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9054
    :sswitch_2
    iget-object v0, p0, Lmfa;->a:Lnvl;

    if-nez v0, :cond_3

    .line 9055
    new-instance v0, Lnvl;

    invoke-direct {v0}, Lnvl;-><init>()V

    iput-object v0, p0, Lmfa;->a:Lnvl;

    .line 9057
    :cond_3
    iget-object v0, p0, Lmfa;->a:Lnvl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9032
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9000
    iget-object v0, p0, Lmfa;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 9001
    const/4 v0, 0x1

    iget-object v1, p0, Lmfa;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9003
    :cond_0
    iget-object v0, p0, Lmfa;->a:Lnvl;

    if-eqz v0, :cond_1

    .line 9004
    const/4 v0, 0x2

    iget-object v1, p0, Lmfa;->a:Lnvl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9006
    :cond_1
    iget-object v0, p0, Lmfa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9008
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8985
    invoke-virtual {p0, p1}, Lmfa;->a(Loxn;)Lmfa;

    move-result-object v0

    return-object v0
.end method
