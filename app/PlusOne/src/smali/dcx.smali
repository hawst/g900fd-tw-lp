.class final Ldcx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/graphics/Point;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Ldcw;


# direct methods
.method constructor <init>(Ldcw;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Ldcx;->a:Ldcw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Ldcx;->a:Ldcw;

    invoke-virtual {v0}, Ldcw;->V()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->a()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    .line 148
    new-instance v1, Ldcv;

    iget-object v2, p0, Ldcx;->a:Ldcw;

    invoke-virtual {v2}, Ldcw;->n()Lz;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ldcv;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    return-object v1
.end method

.method public a(Landroid/graphics/Point;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Point;",
            ")V"
        }
    .end annotation

    .prologue
    .line 153
    if-nez p1, :cond_0

    .line 154
    iget-object v0, p0, Ldcx;->a:Ldcw;

    invoke-virtual {v0}, Ldcw;->U()V

    .line 165
    :goto_0
    return-void

    .line 158
    :cond_0
    new-instance v0, Lddp;

    invoke-direct {v0}, Lddp;-><init>()V

    .line 159
    iget-object v1, p0, Ldcx;->a:Ldcw;

    invoke-virtual {v1}, Ldcw;->V()Lddl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lddp;->a(Lddl;)Ldds;

    .line 160
    iget v1, p1, Landroid/graphics/Point;->x:I

    invoke-virtual {v0, v1}, Lddp;->a(I)Lddp;

    .line 161
    iget v1, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1}, Lddp;->b(I)Lddp;

    .line 162
    iget-object v1, p0, Ldcx;->a:Ldcw;

    invoke-virtual {v0}, Lddp;->a()Lddl;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldcw;->a(Lddl;)V

    .line 164
    iget-object v0, p0, Ldcx;->a:Ldcw;

    iget-object v1, p0, Ldcx;->a:Ldcw;

    invoke-virtual {v1}, Ldcw;->V()Lddl;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcw;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/graphics/Point;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 144
    check-cast p2, Landroid/graphics/Point;

    invoke-virtual {p0, p2}, Ldcx;->a(Landroid/graphics/Point;)V

    return-void
.end method
