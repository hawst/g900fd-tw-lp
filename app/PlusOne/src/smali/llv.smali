.class public final Lllv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lllt;


# instance fields
.field private final a:Lllw;

.field private final b:I

.field private c:I

.field private final d:Ljava/nio/ByteBuffer;

.field private e:I


# direct methods
.method public constructor <init>(Lllw;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "chunkSource cannot be null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllw;

    iput-object v0, p0, Lllv;->a:Lllw;

    .line 26
    invoke-interface {p1}, Lllw;->a()I

    move-result v0

    iput v0, p0, Lllv;->b:I

    .line 27
    iget v0, p0, Lllv;->b:I

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lllv;->d:Ljava/nio/ByteBuffer;

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lllv;->c:I

    .line 29
    return-void
.end method


# virtual methods
.method public a(JLjava/nio/ByteBuffer;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 33
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "startOffset must be non-negative"

    invoke-static {v0, v2}, Llsk;->a(ZLjava/lang/Object;)V

    .line 34
    const-string v0, "buffer must not be null"

    invoke-static {p3, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    :goto_1
    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget v0, p0, Lllv;->b:I

    int-to-long v2, v0

    div-long v2, p1, v2

    long-to-int v0, v2

    .line 41
    iget v2, p0, Lllv;->c:I

    if-eq v0, v2, :cond_3

    .line 42
    iget-object v2, p0, Lllv;->a:Lllw;

    iget-object v3, p0, Lllv;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lllw;->a(I[B)I

    move-result v2

    .line 45
    if-nez v2, :cond_2

    .line 73
    :cond_0
    return v1

    :cond_1
    move v0, v1

    .line 33
    goto :goto_0

    .line 50
    :cond_2
    iput v0, p0, Lllv;->c:I

    .line 51
    iput v2, p0, Lllv;->e:I

    .line 55
    :cond_3
    iget v0, p0, Lllv;->b:I

    int-to-long v2, v0

    rem-long v2, p1, v2

    long-to-int v0, v2

    .line 56
    iget v2, p0, Lllv;->e:I

    sub-int/2addr v2, v0

    .line 60
    iget v3, p0, Lllv;->e:I

    iget v4, p0, Lllv;->b:I

    if-ge v3, v4, :cond_4

    if-lez v2, :cond_0

    .line 64
    :cond_4
    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 65
    iget-object v3, p0, Lllv;->d:Ljava/nio/ByteBuffer;

    add-int v4, v0, v2

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 66
    iget-object v3, p0, Lllv;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 67
    iget-object v0, p0, Lllv;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {p3, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 70
    int-to-long v4, v2

    add-long/2addr p1, v4

    .line 71
    add-int/2addr v1, v2

    .line 72
    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lllv;->a:Lllw;

    invoke-interface {v0}, Lllw;->b()V

    .line 84
    return-void
.end method

.method public a(JJ)Z
    .locals 7

    .prologue
    .line 88
    iget v0, p0, Lllv;->b:I

    int-to-long v0, v0

    div-long v0, p1, v0

    long-to-int v0, v0

    .line 89
    iget v1, p0, Lllv;->c:I

    if-ne v1, v0, :cond_0

    .line 90
    add-int/lit8 v0, v0, 0x1

    .line 92
    :cond_0
    add-long v2, p1, p3

    iget v1, p0, Lllv;->b:I

    int-to-long v4, v1

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 93
    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    .line 94
    if-gtz v1, :cond_1

    .line 95
    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lllv;->a:Lllw;

    invoke-interface {v2, v0, v1}, Lllw;->a(II)Z

    move-result v0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lllv;->a:Lllw;

    invoke-static {v0}, Llrz;->a(Ljava/io/Closeable;)V

    .line 79
    return-void
.end method
