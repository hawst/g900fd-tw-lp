.class public final Lbro;
.super Landroid/database/AbstractCursor;
.source "PG"


# instance fields
.field private a:[Landroid/database/Cursor;

.field private b:[I

.field private final c:I

.field private d:I

.field private e:Landroid/database/Cursor;

.field private final f:I

.field private final g:[I

.field private final h:[I

.field private final i:[[I

.field private j:I

.field private k:I

.field private final l:Landroid/os/Bundle;

.field private m:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>([Landroid/database/Cursor;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 67
    const/16 v0, 0x80

    invoke-direct {p0, p1, p2, p3, v0}, Lbro;-><init>([Landroid/database/Cursor;Ljava/lang/String;II)V

    .line 68
    return-void
.end method

.method private constructor <init>([Landroid/database/Cursor;Ljava/lang/String;II)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v10, -0x1

    const/4 v2, 0x0

    .line 79
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 42
    iput v10, p0, Lbro;->j:I

    .line 45
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lbro;->l:Landroid/os/Bundle;

    .line 47
    new-instance v0, Lbrp;

    invoke-direct {v0, p0}, Lbrp;-><init>(Lbro;)V

    iput-object v0, p0, Lbro;->m:Landroid/database/DataSetObserver;

    .line 80
    iput-object p1, p0, Lbro;->a:[Landroid/database/Cursor;

    .line 81
    iput p3, p0, Lbro;->c:I

    .line 82
    const/4 v0, 0x2

    if-ge p4, v0, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "rowCacheSize must be >= 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_0
    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    array-length v8, v0

    .line 87
    new-array v0, v8, [I

    iput-object v0, p0, Lbro;->b:[I

    move v0, v2

    .line 88
    :goto_0
    if-ge v0, v8, :cond_2

    .line 89
    iget-object v3, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    .line 90
    iget-object v3, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v3, v3, v0

    iget-object v4, p0, Lbro;->m:Landroid/database/DataSetObserver;

    invoke-interface {v3, v4}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 93
    iget-object v3, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v3, v3, v0

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 94
    iget-object v3, p0, Lbro;->b:[I

    iget-object v4, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v4, v4, v0

    invoke-interface {v4, p2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    aput v4, v3, v0

    .line 88
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_2
    iput v10, p0, Lbro;->d:I

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    .line 99
    const-wide/16 v4, 0x0

    move v3, v2

    .line 100
    :goto_1
    if-ge v3, v8, :cond_8

    .line 101
    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v0, v0, v3

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v0, v0, v3

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_4

    .line 102
    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v0, v0, v3

    iget-object v6, p0, Lbro;->b:[I

    aget v6, v6, v3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 105
    iget v0, p0, Lbro;->c:I

    if-ne v0, v1, :cond_6

    cmp-long v0, v6, v4

    if-gez v0, :cond_5

    move v0, v1

    .line 107
    :goto_2
    iget v9, p0, Lbro;->d:I

    if-eq v9, v10, :cond_3

    if-eqz v0, :cond_4

    .line 109
    :cond_3
    iput v3, p0, Lbro;->d:I

    .line 110
    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v0, v0, v3

    iput-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    move-wide v4, v6

    .line 100
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_5
    move v0, v2

    .line 105
    goto :goto_2

    :cond_6
    cmp-long v0, v6, v4

    if-lez v0, :cond_7

    move v0, v1

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_2

    .line 114
    :cond_8
    iput p4, p0, Lbro;->f:I

    .line 115
    iget v0, p0, Lbro;->f:I

    new-array v0, v0, [I

    iput-object v0, p0, Lbro;->g:[I

    .line 116
    iget v0, p0, Lbro;->f:I

    new-array v0, v0, [I

    iput-object v0, p0, Lbro;->h:[I

    .line 117
    invoke-direct {p0}, Lbro;->b()V

    .line 118
    iget v0, p0, Lbro;->f:I

    filled-new-array {v0, v8}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lbro;->i:[[I

    .line 119
    return-void
.end method

.method static synthetic a(Lbro;I)I
    .locals 0

    .prologue
    .line 19
    iput p1, p0, Lbro;->mPos:I

    return p1
.end method

.method static synthetic a(Lbro;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lbro;->b()V

    return-void
.end method

.method static synthetic b(Lbro;I)I
    .locals 0

    .prologue
    .line 19
    iput p1, p0, Lbro;->mPos:I

    return p1
.end method

.method private b()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 122
    iget-object v0, p0, Lbro;->g:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 123
    iget-object v1, p0, Lbro;->g:[I

    aput v2, v1, v0

    .line 122
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 125
    :cond_0
    iput v2, p0, Lbro;->k:I

    .line 126
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lbro;->d:I

    return v0
.end method

.method public close()V
    .locals 3

    .prologue
    .line 296
    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    array-length v1, v0

    .line 297
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 298
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 299
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 297
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 303
    :cond_1
    return-void
.end method

.method public getBlob(I)[B
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 279
    iget v0, p0, Lbro;->d:I

    if-ltz v0, :cond_0

    .line 280
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 287
    :goto_0
    return-object v0

    .line 284
    :cond_0
    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    array-length v1, v0

    .line 285
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    .line 286
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    .line 287
    iget-object v1, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v0, v1, v0

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 285
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 290
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No cursor that can return names"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCount()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 130
    iget v1, p0, Lbro;->k:I

    if-ltz v1, :cond_0

    .line 131
    iget v0, p0, Lbro;->k:I

    .line 141
    :goto_0
    return v0

    .line 134
    :cond_0
    iget-object v1, p0, Lbro;->a:[Landroid/database/Cursor;

    array-length v2, v1

    move v1, v0

    .line 135
    :goto_1
    if-ge v1, v2, :cond_2

    .line 136
    iget-object v3, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    if-eqz v3, :cond_1

    .line 137
    iget-object v3, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 135
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 140
    :cond_2
    iput v0, p0, Lbro;->k:I

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lbro;->l:Landroid/os/Bundle;

    return-object v0
.end method

.method public getFloat(I)F
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getShort(I)S
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType(I)I
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 12

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 146
    if-ne p1, p2, :cond_0

    .line 229
    :goto_0
    return v8

    .line 150
    :cond_0
    iget v0, p0, Lbro;->f:I

    rem-int v0, p2, v0

    .line 152
    iget-object v2, p0, Lbro;->g:[I

    aget v2, v2, v0

    if-ne v2, p2, :cond_1

    .line 153
    iget-object v2, p0, Lbro;->h:[I

    aget v2, v2, v0

    .line 154
    iput v2, p0, Lbro;->d:I

    .line 155
    iget-object v3, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v3, v3, v2

    iput-object v3, p0, Lbro;->e:Landroid/database/Cursor;

    .line 156
    iget-object v3, p0, Lbro;->e:Landroid/database/Cursor;

    if-eqz v3, :cond_1

    .line 157
    iget-object v1, p0, Lbro;->e:Landroid/database/Cursor;

    iget-object v3, p0, Lbro;->i:[[I

    aget-object v3, v3, v0

    aget v2, v3, v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 160
    iput v0, p0, Lbro;->j:I

    goto :goto_0

    .line 165
    :cond_1
    iput v9, p0, Lbro;->d:I

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    .line 167
    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    array-length v11, v0

    .line 169
    if-lt p2, p1, :cond_2

    if-ne p1, v9, :cond_8

    :cond_2
    move v0, v1

    .line 170
    :goto_1
    if-ge v0, v11, :cond_4

    .line 171
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_3

    .line 172
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 170
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 186
    :goto_2
    if-gez v0, :cond_5

    move v0, v1

    :cond_5
    move v10, v0

    move v0, v9

    .line 192
    :goto_3
    if-gt v10, p2, :cond_11

    .line 193
    const-wide/16 v2, 0x0

    move v6, v1

    move v0, v9

    .line 195
    :goto_4
    if-ge v6, v11, :cond_d

    .line 196
    iget-object v4, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v4, v4, v6

    if-eqz v4, :cond_7

    iget-object v4, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v4, v4, v6

    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_7

    .line 197
    iget-object v4, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v4, v4, v6

    iget-object v5, p0, Lbro;->b:[I

    aget v5, v5, v6

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 200
    iget v7, p0, Lbro;->c:I

    if-ne v7, v8, :cond_b

    cmp-long v7, v4, v2

    if-gez v7, :cond_a

    move v7, v8

    .line 202
    :goto_5
    if-ltz v0, :cond_6

    if-eqz v7, :cond_7

    :cond_6
    move-wide v2, v4

    move v0, v6

    .line 195
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 177
    :cond_8
    iget v0, p0, Lbro;->j:I

    if-ltz v0, :cond_12

    move v0, v1

    .line 178
    :goto_6
    if-ge v0, v11, :cond_12

    .line 179
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_9

    .line 180
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    iget-object v3, p0, Lbro;->i:[[I

    iget v4, p0, Lbro;->j:I

    aget-object v3, v3, v4

    aget v3, v3, v0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 178
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    move v7, v1

    .line 200
    goto :goto_5

    :cond_b
    cmp-long v7, v4, v2

    if-lez v7, :cond_c

    move v7, v8

    goto :goto_5

    :cond_c
    move v7, v1

    goto :goto_5

    .line 208
    :cond_d
    sub-int v2, p2, v10

    iget v3, p0, Lbro;->f:I

    if-gt v2, v3, :cond_f

    .line 209
    iget v2, p0, Lbro;->f:I

    rem-int v3, v10, v2

    .line 210
    iget-object v2, p0, Lbro;->g:[I

    aput v10, v2, v3

    .line 211
    iget-object v2, p0, Lbro;->h:[I

    aput v0, v2, v3

    move v2, v1

    .line 212
    :goto_7
    if-ge v2, v11, :cond_f

    .line 213
    iget-object v4, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v4, v4, v2

    if-eqz v4, :cond_e

    .line 214
    iget-object v4, p0, Lbro;->i:[[I

    aget-object v4, v4, v3

    iget-object v5, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v5, v5, v2

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    aput v5, v4, v2

    .line 212
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 219
    :cond_f
    if-eq v10, p2, :cond_11

    .line 220
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_10

    .line 223
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 192
    :cond_10
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto/16 :goto_3

    .line 226
    :cond_11
    iput v0, p0, Lbro;->d:I

    .line 227
    iget-object v1, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v0, v1, v0

    iput-object v0, p0, Lbro;->e:Landroid/database/Cursor;

    .line 228
    iput v9, p0, Lbro;->j:I

    goto/16 :goto_0

    :cond_12
    move v0, p1

    goto/16 :goto_2
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3

    .prologue
    .line 307
    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    array-length v1, v0

    .line 308
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 309
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 310
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 308
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 313
    :cond_1
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3

    .prologue
    .line 317
    iget-object v0, p0, Lbro;->a:[Landroid/database/Cursor;

    array-length v1, v0

    .line 318
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 319
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 320
    iget-object v2, p0, Lbro;->a:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 318
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 323
    :cond_1
    return-void
.end method
