.class public final Lney;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Boolean;

.field public c:[I

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1143
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1150
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lney;->c:[I

    .line 1159
    const/high16 v0, -0x80000000

    iput v0, p0, Lney;->f:I

    .line 1143
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1213
    .line 1214
    iget-object v0, p0, Lney;->a:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1215
    const/4 v0, 0x1

    iget-object v2, p0, Lney;->a:Ljava/lang/String;

    .line 1216
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1218
    :goto_0
    iget-object v2, p0, Lney;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 1219
    const/4 v2, 0x2

    iget-object v3, p0, Lney;->b:Ljava/lang/Boolean;

    .line 1220
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1222
    :cond_0
    iget-object v2, p0, Lney;->c:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lney;->c:[I

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1224
    iget-object v3, p0, Lney;->c:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    .line 1226
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 1224
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1228
    :cond_1
    add-int/2addr v0, v2

    .line 1229
    iget-object v1, p0, Lney;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1231
    :cond_2
    iget-object v1, p0, Lney;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1232
    const/4 v1, 0x4

    iget-object v2, p0, Lney;->d:Ljava/lang/Integer;

    .line 1233
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1235
    :cond_3
    iget-object v1, p0, Lney;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1236
    const/4 v1, 0x5

    iget-object v2, p0, Lney;->e:Ljava/lang/Integer;

    .line 1237
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1239
    :cond_4
    iget-object v1, p0, Lney;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 1240
    const/4 v1, 0x6

    iget-object v2, p0, Lney;->i:Ljava/lang/Integer;

    .line 1241
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1243
    :cond_5
    iget v1, p0, Lney;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_6

    .line 1244
    const/4 v1, 0x7

    iget v2, p0, Lney;->f:I

    .line 1245
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1247
    :cond_6
    iget-object v1, p0, Lney;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1248
    const/16 v1, 0x8

    iget-object v2, p0, Lney;->g:Ljava/lang/String;

    .line 1249
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1251
    :cond_7
    iget-object v1, p0, Lney;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 1252
    const/16 v1, 0x9

    iget-object v2, p0, Lney;->j:Ljava/lang/Boolean;

    .line 1253
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1255
    :cond_8
    iget-object v1, p0, Lney;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 1256
    const/16 v1, 0xa

    iget-object v2, p0, Lney;->k:Ljava/lang/Boolean;

    .line 1257
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1259
    :cond_9
    iget-object v1, p0, Lney;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 1260
    const/16 v1, 0xb

    iget-object v2, p0, Lney;->h:Ljava/lang/Boolean;

    .line 1261
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1263
    :cond_a
    iget-object v1, p0, Lney;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1264
    iput v0, p0, Lney;->ai:I

    .line 1265
    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lney;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1273
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1274
    sparse-switch v0, :sswitch_data_0

    .line 1278
    iget-object v1, p0, Lney;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1279
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lney;->ah:Ljava/util/List;

    .line 1282
    :cond_1
    iget-object v1, p0, Lney;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1284
    :sswitch_0
    return-object p0

    .line 1289
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lney;->a:Ljava/lang/String;

    goto :goto_0

    .line 1293
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lney;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1297
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1298
    iget-object v0, p0, Lney;->c:[I

    array-length v0, v0

    .line 1299
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 1300
    iget-object v2, p0, Lney;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1301
    iput-object v1, p0, Lney;->c:[I

    .line 1302
    :goto_1
    iget-object v1, p0, Lney;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1303
    iget-object v1, p0, Lney;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 1304
    invoke-virtual {p1}, Loxn;->a()I

    .line 1302
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1307
    :cond_2
    iget-object v1, p0, Lney;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 1311
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lney;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 1315
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lney;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 1319
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lney;->i:Ljava/lang/Integer;

    goto :goto_0

    .line 1323
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1324
    if-eq v0, v4, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 1326
    :cond_3
    iput v0, p0, Lney;->f:I

    goto/16 :goto_0

    .line 1328
    :cond_4
    iput v4, p0, Lney;->f:I

    goto/16 :goto_0

    .line 1333
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lney;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 1337
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lney;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1341
    :sswitch_a
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lney;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1345
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lney;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1274
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1172
    iget-object v0, p0, Lney;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1173
    const/4 v0, 0x1

    iget-object v1, p0, Lney;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1175
    :cond_0
    iget-object v0, p0, Lney;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1176
    const/4 v0, 0x2

    iget-object v1, p0, Lney;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1178
    :cond_1
    iget-object v0, p0, Lney;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lney;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1179
    iget-object v1, p0, Lney;->c:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget v3, v1, v0

    .line 1180
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 1179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1183
    :cond_2
    iget-object v0, p0, Lney;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1184
    const/4 v0, 0x4

    iget-object v1, p0, Lney;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1186
    :cond_3
    iget-object v0, p0, Lney;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 1187
    const/4 v0, 0x5

    iget-object v1, p0, Lney;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1189
    :cond_4
    iget-object v0, p0, Lney;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1190
    const/4 v0, 0x6

    iget-object v1, p0, Lney;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1192
    :cond_5
    iget v0, p0, Lney;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_6

    .line 1193
    const/4 v0, 0x7

    iget v1, p0, Lney;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1195
    :cond_6
    iget-object v0, p0, Lney;->g:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1196
    const/16 v0, 0x8

    iget-object v1, p0, Lney;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1198
    :cond_7
    iget-object v0, p0, Lney;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 1199
    const/16 v0, 0x9

    iget-object v1, p0, Lney;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1201
    :cond_8
    iget-object v0, p0, Lney;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 1202
    const/16 v0, 0xa

    iget-object v1, p0, Lney;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1204
    :cond_9
    iget-object v0, p0, Lney;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 1205
    const/16 v0, 0xb

    iget-object v1, p0, Lney;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1207
    :cond_a
    iget-object v0, p0, Lney;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1209
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1139
    invoke-virtual {p0, p1}, Lney;->a(Loxn;)Lney;

    move-result-object v0

    return-object v0
.end method
