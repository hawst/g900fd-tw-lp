.class public final Losh;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Losh;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Losi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1437
    const/4 v0, 0x0

    new-array v0, v0, [Losh;

    sput-object v0, Losh;->a:[Losh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1438
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1539
    const/high16 v0, -0x80000000

    iput v0, p0, Losh;->b:I

    .line 1544
    const/4 v0, 0x0

    iput-object v0, p0, Losh;->d:Losi;

    .line 1438
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1564
    const/4 v0, 0x0

    .line 1565
    iget v1, p0, Losh;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1566
    const/4 v0, 0x1

    iget v1, p0, Losh;->b:I

    .line 1567
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1569
    :cond_0
    iget-object v1, p0, Losh;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1570
    const/4 v1, 0x2

    iget-object v2, p0, Losh;->c:Ljava/lang/String;

    .line 1571
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1573
    :cond_1
    iget-object v1, p0, Losh;->d:Losi;

    if-eqz v1, :cond_2

    .line 1574
    const/4 v1, 0x3

    iget-object v2, p0, Losh;->d:Losi;

    .line 1575
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1577
    :cond_2
    iget-object v1, p0, Losh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1578
    iput v0, p0, Losh;->ai:I

    .line 1579
    return v0
.end method

.method public a(Loxn;)Losh;
    .locals 2

    .prologue
    .line 1587
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1588
    sparse-switch v0, :sswitch_data_0

    .line 1592
    iget-object v1, p0, Losh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1593
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Losh;->ah:Ljava/util/List;

    .line 1596
    :cond_1
    iget-object v1, p0, Losh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1598
    :sswitch_0
    return-object p0

    .line 1603
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1604
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1613
    :cond_2
    iput v0, p0, Losh;->b:I

    goto :goto_0

    .line 1615
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Losh;->b:I

    goto :goto_0

    .line 1620
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Losh;->c:Ljava/lang/String;

    goto :goto_0

    .line 1624
    :sswitch_3
    iget-object v0, p0, Losh;->d:Losi;

    if-nez v0, :cond_4

    .line 1625
    new-instance v0, Losi;

    invoke-direct {v0}, Losi;-><init>()V

    iput-object v0, p0, Losh;->d:Losi;

    .line 1627
    :cond_4
    iget-object v0, p0, Losh;->d:Losi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1588
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1549
    iget v0, p0, Losh;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1550
    const/4 v0, 0x1

    iget v1, p0, Losh;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1552
    :cond_0
    iget-object v0, p0, Losh;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1553
    const/4 v0, 0x2

    iget-object v1, p0, Losh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1555
    :cond_1
    iget-object v0, p0, Losh;->d:Losi;

    if-eqz v0, :cond_2

    .line 1556
    const/4 v0, 0x3

    iget-object v1, p0, Losh;->d:Losi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1558
    :cond_2
    iget-object v0, p0, Losh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1560
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1434
    invoke-virtual {p0, p1}, Losh;->a(Loxn;)Losh;

    move-result-object v0

    return-object v0
.end method
