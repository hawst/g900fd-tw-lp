.class public final Lmrr;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lmru;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 327
    invoke-direct {p0}, Loxq;-><init>()V

    .line 332
    const/4 v0, 0x0

    iput-object v0, p0, Lmrr;->b:Lmru;

    .line 327
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 349
    const/4 v0, 0x0

    .line 350
    iget-object v1, p0, Lmrr;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 351
    const/4 v0, 0x1

    iget-object v1, p0, Lmrr;->a:Ljava/lang/String;

    .line 352
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 354
    :cond_0
    iget-object v1, p0, Lmrr;->b:Lmru;

    if-eqz v1, :cond_1

    .line 355
    const/4 v1, 0x2

    iget-object v2, p0, Lmrr;->b:Lmru;

    .line 356
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 358
    :cond_1
    iget-object v1, p0, Lmrr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    iput v0, p0, Lmrr;->ai:I

    .line 360
    return v0
.end method

.method public a(Loxn;)Lmrr;
    .locals 2

    .prologue
    .line 368
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 369
    sparse-switch v0, :sswitch_data_0

    .line 373
    iget-object v1, p0, Lmrr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 374
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmrr;->ah:Ljava/util/List;

    .line 377
    :cond_1
    iget-object v1, p0, Lmrr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    :sswitch_0
    return-object p0

    .line 384
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmrr;->a:Ljava/lang/String;

    goto :goto_0

    .line 388
    :sswitch_2
    iget-object v0, p0, Lmrr;->b:Lmru;

    if-nez v0, :cond_2

    .line 389
    new-instance v0, Lmru;

    invoke-direct {v0}, Lmru;-><init>()V

    iput-object v0, p0, Lmrr;->b:Lmru;

    .line 391
    :cond_2
    iget-object v0, p0, Lmrr;->b:Lmru;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 369
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lmrr;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 338
    const/4 v0, 0x1

    iget-object v1, p0, Lmrr;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 340
    :cond_0
    iget-object v0, p0, Lmrr;->b:Lmru;

    if-eqz v0, :cond_1

    .line 341
    const/4 v0, 0x2

    iget-object v1, p0, Lmrr;->b:Lmru;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 343
    :cond_1
    iget-object v0, p0, Lmrr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 345
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0, p1}, Lmrr;->a(Loxn;)Lmrr;

    move-result-object v0

    return-object v0
.end method
