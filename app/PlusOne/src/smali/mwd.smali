.class public final Lmwd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmwd;


# instance fields
.field public b:[Ljava/lang/Integer;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2738
    const/4 v0, 0x0

    new-array v0, v0, [Lmwd;

    sput-object v0, Lmwd;->a:[Lmwd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2739
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2742
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lmwd;->b:[Ljava/lang/Integer;

    .line 2739
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2763
    .line 2764
    iget-object v1, p0, Lmwd;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmwd;->b:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 2766
    iget-object v2, p0, Lmwd;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2768
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 2766
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2771
    :cond_0
    iget-object v0, p0, Lmwd;->b:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 2773
    :cond_1
    iget-object v1, p0, Lmwd;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2774
    const/4 v1, 0x2

    iget-object v2, p0, Lmwd;->c:Ljava/lang/String;

    .line 2775
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2777
    :cond_2
    iget-object v1, p0, Lmwd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2778
    iput v0, p0, Lmwd;->ai:I

    .line 2779
    return v0
.end method

.method public a(Loxn;)Lmwd;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2787
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2788
    sparse-switch v0, :sswitch_data_0

    .line 2792
    iget-object v1, p0, Lmwd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2793
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwd;->ah:Ljava/util/List;

    .line 2796
    :cond_1
    iget-object v1, p0, Lmwd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2798
    :sswitch_0
    return-object p0

    .line 2803
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 2804
    iget-object v0, p0, Lmwd;->b:[Ljava/lang/Integer;

    array-length v0, v0

    .line 2805
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 2806
    iget-object v2, p0, Lmwd;->b:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2807
    iput-object v1, p0, Lmwd;->b:[Ljava/lang/Integer;

    .line 2808
    :goto_1
    iget-object v1, p0, Lmwd;->b:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 2809
    iget-object v1, p0, Lmwd;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 2810
    invoke-virtual {p1}, Loxn;->a()I

    .line 2808
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2813
    :cond_2
    iget-object v1, p0, Lmwd;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 2817
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmwd;->c:Ljava/lang/String;

    goto :goto_0

    .line 2788
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2749
    iget-object v0, p0, Lmwd;->b:[Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2750
    iget-object v1, p0, Lmwd;->b:[Ljava/lang/Integer;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2751
    const/4 v4, 0x1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 2750
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2754
    :cond_0
    iget-object v0, p0, Lmwd;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2755
    const/4 v0, 0x2

    iget-object v1, p0, Lmwd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2757
    :cond_1
    iget-object v0, p0, Lmwd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2759
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2735
    invoke-virtual {p0, p1}, Lmwd;->a(Loxn;)Lmwd;

    move-result-object v0

    return-object v0
.end method
