.class public final Lmjp;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnwj;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8098
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8101
    iput-object v0, p0, Lmjp;->apiHeader:Llyr;

    .line 8104
    iput-object v0, p0, Lmjp;->a:Lnwj;

    .line 8098
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8121
    const/4 v0, 0x0

    .line 8122
    iget-object v1, p0, Lmjp;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 8123
    const/4 v0, 0x1

    iget-object v1, p0, Lmjp;->apiHeader:Llyr;

    .line 8124
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8126
    :cond_0
    iget-object v1, p0, Lmjp;->a:Lnwj;

    if-eqz v1, :cond_1

    .line 8127
    const/4 v1, 0x2

    iget-object v2, p0, Lmjp;->a:Lnwj;

    .line 8128
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8130
    :cond_1
    iget-object v1, p0, Lmjp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8131
    iput v0, p0, Lmjp;->ai:I

    .line 8132
    return v0
.end method

.method public a(Loxn;)Lmjp;
    .locals 2

    .prologue
    .line 8140
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8141
    sparse-switch v0, :sswitch_data_0

    .line 8145
    iget-object v1, p0, Lmjp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8146
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjp;->ah:Ljava/util/List;

    .line 8149
    :cond_1
    iget-object v1, p0, Lmjp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8151
    :sswitch_0
    return-object p0

    .line 8156
    :sswitch_1
    iget-object v0, p0, Lmjp;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 8157
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjp;->apiHeader:Llyr;

    .line 8159
    :cond_2
    iget-object v0, p0, Lmjp;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8163
    :sswitch_2
    iget-object v0, p0, Lmjp;->a:Lnwj;

    if-nez v0, :cond_3

    .line 8164
    new-instance v0, Lnwj;

    invoke-direct {v0}, Lnwj;-><init>()V

    iput-object v0, p0, Lmjp;->a:Lnwj;

    .line 8166
    :cond_3
    iget-object v0, p0, Lmjp;->a:Lnwj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8141
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8109
    iget-object v0, p0, Lmjp;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 8110
    const/4 v0, 0x1

    iget-object v1, p0, Lmjp;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8112
    :cond_0
    iget-object v0, p0, Lmjp;->a:Lnwj;

    if-eqz v0, :cond_1

    .line 8113
    const/4 v0, 0x2

    iget-object v1, p0, Lmjp;->a:Lnwj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8115
    :cond_1
    iget-object v0, p0, Lmjp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8117
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8094
    invoke-virtual {p0, p1}, Lmjp;->a(Loxn;)Lmjp;

    move-result-object v0

    return-object v0
.end method
