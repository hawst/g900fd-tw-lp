.class public final Lqar;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Lqas;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 889
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1359
    const/high16 v0, -0x80000000

    iput v0, p0, Lqar;->a:I

    .line 1362
    const/4 v0, 0x0

    iput-object v0, p0, Lqar;->b:Lqas;

    .line 889
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1379
    const/4 v0, 0x0

    .line 1380
    iget v1, p0, Lqar;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1381
    const/4 v0, 0x1

    iget v1, p0, Lqar;->a:I

    .line 1382
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1384
    :cond_0
    iget-object v1, p0, Lqar;->b:Lqas;

    if-eqz v1, :cond_1

    .line 1385
    const/4 v1, 0x2

    iget-object v2, p0, Lqar;->b:Lqas;

    .line 1386
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1388
    :cond_1
    iget-object v1, p0, Lqar;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1389
    iput v0, p0, Lqar;->ai:I

    .line 1390
    return v0
.end method

.method public a(Loxn;)Lqar;
    .locals 2

    .prologue
    .line 1398
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1399
    sparse-switch v0, :sswitch_data_0

    .line 1403
    iget-object v1, p0, Lqar;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1404
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqar;->ah:Ljava/util/List;

    .line 1407
    :cond_1
    iget-object v1, p0, Lqar;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1409
    :sswitch_0
    return-object p0

    .line 1414
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1415
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1418
    :cond_2
    iput v0, p0, Lqar;->a:I

    goto :goto_0

    .line 1420
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lqar;->a:I

    goto :goto_0

    .line 1425
    :sswitch_2
    iget-object v0, p0, Lqar;->b:Lqas;

    if-nez v0, :cond_4

    .line 1426
    new-instance v0, Lqas;

    invoke-direct {v0}, Lqas;-><init>()V

    iput-object v0, p0, Lqar;->b:Lqas;

    .line 1428
    :cond_4
    iget-object v0, p0, Lqar;->b:Lqas;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1399
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1367
    iget v0, p0, Lqar;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1368
    const/4 v0, 0x1

    iget v1, p0, Lqar;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1370
    :cond_0
    iget-object v0, p0, Lqar;->b:Lqas;

    if-eqz v0, :cond_1

    .line 1371
    const/4 v0, 0x2

    iget-object v1, p0, Lqar;->b:Lqas;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1373
    :cond_1
    iget-object v0, p0, Lqar;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1375
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 885
    invoke-virtual {p0, p1}, Lqar;->a(Loxn;)Lqar;

    move-result-object v0

    return-object v0
.end method
