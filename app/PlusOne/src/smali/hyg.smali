.class public abstract Lhyg;
.super Lip;
.source "PG"


# instance fields
.field a:Lat;

.field private final b:Lae;

.field private c:Lu;

.field private d:Lhyi;

.field private e:Lgl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgl",
            "<",
            "Ljava/lang/String;",
            "Lu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lae;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Lip;-><init>()V

    .line 49
    iput-object v0, p0, Lhyg;->a:Lat;

    .line 50
    iput-object v0, p0, Lhyg;->c:Lu;

    .line 53
    new-instance v0, Lhyh;

    const/4 v1, 0x5

    invoke-direct {v0, p0, v1}, Lhyh;-><init>(Lhyg;I)V

    iput-object v0, p0, Lhyg;->e:Lgl;

    .line 56
    iput-object p1, p0, Lhyg;->b:Lae;

    .line 57
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/view/View;I)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v3, 0x2

    .line 70
    iget-object v0, p0, Lhyg;->a:Lat;

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lhyg;->b:Lae;

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    iput-object v0, p0, Lhyg;->a:Lat;

    .line 75
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lhyg;->a(II)Ljava/lang/String;

    move-result-object v1

    .line 78
    iget-object v0, p0, Lhyg;->e:Lgl;

    invoke-virtual {v0, v1}, Lgl;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lhyg;->b:Lae;

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_3

    .line 82
    const-string v1, "FragmentPagerAdapter"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Attaching item #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": f="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    :cond_1
    iget-object v1, p0, Lhyg;->a:Lat;

    invoke-virtual {v1, v0}, Lat;->c(Lu;)Lat;

    .line 93
    :goto_0
    iget-object v1, p0, Lhyg;->c:Lu;

    if-eq v0, v1, :cond_2

    .line 94
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lu;->i_(Z)V

    .line 97
    :cond_2
    return-object v0

    .line 87
    :cond_3
    invoke-virtual {p0, p2}, Lhyg;->a(I)Lu;

    move-result-object v0

    .line 88
    const-string v2, "FragmentPagerAdapter"

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 89
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Adding item #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": f="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :cond_4
    iget-object v2, p0, Lhyg;->a:Lat;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3, v0, v1}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    goto :goto_0
.end method

.method protected a(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "android:switcher:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(I)Lu;
.end method

.method public a(Landroid/view/View;ILjava/lang/Object;)V
    .locals 5

    .prologue
    .line 102
    iget-object v0, p0, Lhyg;->a:Lat;

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lhyg;->b:Lae;

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    iput-object v0, p0, Lhyg;->a:Lat;

    .line 105
    :cond_0
    const-string v0, "FragmentPagerAdapter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, p3

    check-cast v0, Lu;

    .line 107
    invoke-virtual {v0}, Lu;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x22

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Detaching item #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": f="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " v="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_1
    check-cast p3, Lu;

    .line 111
    invoke-virtual {p3}, Lu;->j()Ljava/lang/String;

    move-result-object v0

    .line 112
    if-nez v0, :cond_2

    .line 116
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lhyg;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 119
    :cond_2
    invoke-virtual {p0, p3}, Lhyg;->a(Lu;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 120
    iget-object v1, p0, Lhyg;->e:Lgl;

    invoke-virtual {v1, v0, p3}, Lgl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, Lhyg;->a:Lat;

    invoke-virtual {v0, p3}, Lat;->b(Lu;)Lat;

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_3
    iget-object v0, p0, Lhyg;->a:Lat;

    invoke-virtual {v0, p3}, Lat;->a(Lu;)Lat;

    goto :goto_0
.end method

.method public a(Lhyi;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lhyg;->d:Lhyi;

    .line 186
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 165
    check-cast p2, Lu;

    invoke-virtual {p2}, Lu;->x()Landroid/view/View;

    move-result-object v1

    move-object v0, p1

    .line 166
    :goto_0
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_1

    .line 167
    if-ne v0, v1, :cond_0

    .line 168
    const/4 v0, 0x1

    .line 171
    :goto_1
    return v0

    .line 166
    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 171
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected a(Lu;)Z
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    return v0
.end method

.method public b(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 137
    check-cast p2, Lu;

    .line 138
    iget-object v0, p0, Lhyg;->c:Lu;

    if-eq p2, v0, :cond_2

    .line 139
    iget-object v0, p0, Lhyg;->c:Lu;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lhyg;->c:Lu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lu;->i_(Z)V

    .line 142
    :cond_0
    if-eqz p2, :cond_1

    .line 143
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lu;->i_(Z)V

    .line 145
    :cond_1
    iput-object p2, p0, Lhyg;->c:Lu;

    .line 148
    :cond_2
    iget-object v0, p0, Lhyg;->d:Lhyi;

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lhyg;->d:Lhyi;

    invoke-interface {v0, p2, p1}, Lhyi;->b(Lu;I)V

    .line 151
    :cond_3
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lhyg;->a:Lat;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lhyg;->a:Lat;

    invoke-virtual {v0}, Lat;->c()I

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lhyg;->a:Lat;

    .line 158
    iget-object v0, p0, Lhyg;->b:Lae;

    invoke-virtual {v0}, Lae;->b()Z

    .line 160
    :cond_0
    return-void
.end method
