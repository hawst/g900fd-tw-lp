.class public final Lpmh;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/String;

.field private b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpmh;->a:[Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 38
    .line 39
    iget-object v1, p0, Lpmh;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lpmh;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 41
    iget-object v2, p0, Lpmh;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 43
    invoke-static {v4}, Loxo;->b(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_0
    iget-object v0, p0, Lpmh;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 48
    :cond_1
    iget-object v1, p0, Lpmh;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 49
    const/4 v1, 0x2

    iget-object v2, p0, Lpmh;->b:Ljava/lang/Boolean;

    .line 50
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 52
    :cond_2
    iget-object v1, p0, Lpmh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    iput v0, p0, Lpmh;->ai:I

    .line 54
    return v0
.end method

.method public a(Loxn;)Lpmh;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 62
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 63
    sparse-switch v0, :sswitch_data_0

    .line 67
    iget-object v1, p0, Lpmh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpmh;->ah:Ljava/util/List;

    .line 71
    :cond_1
    iget-object v1, p0, Lpmh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    :sswitch_0
    return-object p0

    .line 78
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 79
    iget-object v0, p0, Lpmh;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 80
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 81
    iget-object v2, p0, Lpmh;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    iput-object v1, p0, Lpmh;->a:[Ljava/lang/String;

    .line 83
    :goto_1
    iget-object v1, p0, Lpmh;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 84
    iget-object v1, p0, Lpmh;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 85
    invoke-virtual {p1}, Loxn;->a()I

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 88
    :cond_2
    iget-object v1, p0, Lpmh;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 92
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpmh;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 63
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 24
    iget-object v0, p0, Lpmh;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 25
    iget-object v1, p0, Lpmh;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 26
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 25
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29
    :cond_0
    iget-object v0, p0, Lpmh;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 30
    const/4 v0, 0x2

    iget-object v1, p0, Lpmh;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 32
    :cond_1
    iget-object v0, p0, Lpmh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 34
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpmh;->a(Loxn;)Lpmh;

    move-result-object v0

    return-object v0
.end method
