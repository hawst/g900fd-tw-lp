.class public final Lnsr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnsr;


# instance fields
.field public b:Lnsb;

.field public c:Lnsw;

.field public d:Lnsv;

.field public e:Lnsu;

.field public f:I

.field public g:I

.field public h:Lnst;

.field public i:Lnsi;

.field private j:I

.field private k:Ljava/lang/Integer;

.field private l:[Lnsh;

.field private m:Lnsx;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1824
    const/4 v0, 0x0

    new-array v0, v0, [Lnsr;

    sput-object v0, Lnsr;->a:[Lnsr;

    .line 1828
    const v0, 0x2a1aef2

    new-instance v1, Lnss;

    invoke-direct {v1}, Lnss;-><init>()V

    .line 1829
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 1828
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v0, -0x80000000

    const/4 v1, 0x0

    .line 1825
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2334
    iput-object v1, p0, Lnsr;->b:Lnsb;

    .line 2337
    iput-object v1, p0, Lnsr;->c:Lnsw;

    .line 2340
    iput-object v1, p0, Lnsr;->d:Lnsv;

    .line 2343
    iput-object v1, p0, Lnsr;->e:Lnsu;

    .line 2346
    iput v0, p0, Lnsr;->f:I

    .line 2349
    iput v0, p0, Lnsr;->g:I

    .line 2352
    iput v0, p0, Lnsr;->j:I

    .line 2355
    iput-object v1, p0, Lnsr;->h:Lnst;

    .line 2360
    sget-object v0, Lnsh;->a:[Lnsh;

    iput-object v0, p0, Lnsr;->l:[Lnsh;

    .line 2363
    iput-object v1, p0, Lnsr;->i:Lnsi;

    .line 2366
    iput-object v1, p0, Lnsr;->m:Lnsx;

    .line 1825
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, -0x80000000

    .line 2417
    .line 2418
    iget-object v0, p0, Lnsr;->b:Lnsb;

    if-eqz v0, :cond_c

    .line 2419
    const/4 v0, 0x1

    iget-object v2, p0, Lnsr;->b:Lnsb;

    .line 2420
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2422
    :goto_0
    iget-object v2, p0, Lnsr;->c:Lnsw;

    if-eqz v2, :cond_0

    .line 2423
    const/4 v2, 0x3

    iget-object v3, p0, Lnsr;->c:Lnsw;

    .line 2424
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2426
    :cond_0
    iget-object v2, p0, Lnsr;->d:Lnsv;

    if-eqz v2, :cond_1

    .line 2427
    const/4 v2, 0x4

    iget-object v3, p0, Lnsr;->d:Lnsv;

    .line 2428
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2430
    :cond_1
    iget-object v2, p0, Lnsr;->e:Lnsu;

    if-eqz v2, :cond_2

    .line 2431
    const/4 v2, 0x5

    iget-object v3, p0, Lnsr;->e:Lnsu;

    .line 2432
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2434
    :cond_2
    iget v2, p0, Lnsr;->f:I

    if-eq v2, v6, :cond_3

    .line 2435
    const/4 v2, 0x6

    iget v3, p0, Lnsr;->f:I

    .line 2436
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2438
    :cond_3
    iget v2, p0, Lnsr;->g:I

    if-eq v2, v6, :cond_4

    .line 2439
    const/4 v2, 0x7

    iget v3, p0, Lnsr;->g:I

    .line 2440
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2442
    :cond_4
    iget-object v2, p0, Lnsr;->h:Lnst;

    if-eqz v2, :cond_5

    .line 2443
    const/16 v2, 0x8

    iget-object v3, p0, Lnsr;->h:Lnst;

    .line 2444
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2446
    :cond_5
    iget-object v2, p0, Lnsr;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 2447
    const/16 v2, 0x9

    iget-object v3, p0, Lnsr;->k:Ljava/lang/Integer;

    .line 2448
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2450
    :cond_6
    iget-object v2, p0, Lnsr;->l:[Lnsh;

    if-eqz v2, :cond_8

    .line 2451
    iget-object v2, p0, Lnsr;->l:[Lnsh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 2452
    if-eqz v4, :cond_7

    .line 2453
    const/16 v5, 0xa

    .line 2454
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2451
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2458
    :cond_8
    iget-object v1, p0, Lnsr;->i:Lnsi;

    if-eqz v1, :cond_9

    .line 2459
    const/16 v1, 0xb

    iget-object v2, p0, Lnsr;->i:Lnsi;

    .line 2460
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2462
    :cond_9
    iget v1, p0, Lnsr;->j:I

    if-eq v1, v6, :cond_a

    .line 2463
    const/16 v1, 0xc

    iget v2, p0, Lnsr;->j:I

    .line 2464
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2466
    :cond_a
    iget-object v1, p0, Lnsr;->m:Lnsx;

    if-eqz v1, :cond_b

    .line 2467
    const/16 v1, 0xd

    iget-object v2, p0, Lnsr;->m:Lnsx;

    .line 2468
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2470
    :cond_b
    iget-object v1, p0, Lnsr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2471
    iput v0, p0, Lnsr;->ai:I

    .line 2472
    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnsr;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2480
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2481
    sparse-switch v0, :sswitch_data_0

    .line 2485
    iget-object v2, p0, Lnsr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2486
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnsr;->ah:Ljava/util/List;

    .line 2489
    :cond_1
    iget-object v2, p0, Lnsr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2491
    :sswitch_0
    return-object p0

    .line 2496
    :sswitch_1
    iget-object v0, p0, Lnsr;->b:Lnsb;

    if-nez v0, :cond_2

    .line 2497
    new-instance v0, Lnsb;

    invoke-direct {v0}, Lnsb;-><init>()V

    iput-object v0, p0, Lnsr;->b:Lnsb;

    .line 2499
    :cond_2
    iget-object v0, p0, Lnsr;->b:Lnsb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2503
    :sswitch_2
    iget-object v0, p0, Lnsr;->c:Lnsw;

    if-nez v0, :cond_3

    .line 2504
    new-instance v0, Lnsw;

    invoke-direct {v0}, Lnsw;-><init>()V

    iput-object v0, p0, Lnsr;->c:Lnsw;

    .line 2506
    :cond_3
    iget-object v0, p0, Lnsr;->c:Lnsw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2510
    :sswitch_3
    iget-object v0, p0, Lnsr;->d:Lnsv;

    if-nez v0, :cond_4

    .line 2511
    new-instance v0, Lnsv;

    invoke-direct {v0}, Lnsv;-><init>()V

    iput-object v0, p0, Lnsr;->d:Lnsv;

    .line 2513
    :cond_4
    iget-object v0, p0, Lnsr;->d:Lnsv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2517
    :sswitch_4
    iget-object v0, p0, Lnsr;->e:Lnsu;

    if-nez v0, :cond_5

    .line 2518
    new-instance v0, Lnsu;

    invoke-direct {v0}, Lnsu;-><init>()V

    iput-object v0, p0, Lnsr;->e:Lnsu;

    .line 2520
    :cond_5
    iget-object v0, p0, Lnsr;->e:Lnsu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2524
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2525
    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-eq v0, v6, :cond_6

    if-eq v0, v7, :cond_6

    const/4 v2, 0x5

    if-eq v0, v2, :cond_6

    const/4 v2, 0x6

    if-eq v0, v2, :cond_6

    const/4 v2, 0x7

    if-ne v0, v2, :cond_7

    .line 2533
    :cond_6
    iput v0, p0, Lnsr;->f:I

    goto :goto_0

    .line 2535
    :cond_7
    iput v1, p0, Lnsr;->f:I

    goto :goto_0

    .line 2540
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2541
    if-eqz v0, :cond_8

    if-ne v0, v4, :cond_9

    .line 2543
    :cond_8
    iput v0, p0, Lnsr;->g:I

    goto/16 :goto_0

    .line 2545
    :cond_9
    iput v1, p0, Lnsr;->g:I

    goto/16 :goto_0

    .line 2550
    :sswitch_7
    iget-object v0, p0, Lnsr;->h:Lnst;

    if-nez v0, :cond_a

    .line 2551
    new-instance v0, Lnst;

    invoke-direct {v0}, Lnst;-><init>()V

    iput-object v0, p0, Lnsr;->h:Lnst;

    .line 2553
    :cond_a
    iget-object v0, p0, Lnsr;->h:Lnst;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2557
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnsr;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2561
    :sswitch_9
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2562
    iget-object v0, p0, Lnsr;->l:[Lnsh;

    if-nez v0, :cond_c

    move v0, v1

    .line 2563
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsh;

    .line 2564
    iget-object v3, p0, Lnsr;->l:[Lnsh;

    if-eqz v3, :cond_b

    .line 2565
    iget-object v3, p0, Lnsr;->l:[Lnsh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2567
    :cond_b
    iput-object v2, p0, Lnsr;->l:[Lnsh;

    .line 2568
    :goto_2
    iget-object v2, p0, Lnsr;->l:[Lnsh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 2569
    iget-object v2, p0, Lnsr;->l:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 2570
    iget-object v2, p0, Lnsr;->l:[Lnsh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2571
    invoke-virtual {p1}, Loxn;->a()I

    .line 2568
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2562
    :cond_c
    iget-object v0, p0, Lnsr;->l:[Lnsh;

    array-length v0, v0

    goto :goto_1

    .line 2574
    :cond_d
    iget-object v2, p0, Lnsr;->l:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 2575
    iget-object v2, p0, Lnsr;->l:[Lnsh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2579
    :sswitch_a
    iget-object v0, p0, Lnsr;->i:Lnsi;

    if-nez v0, :cond_e

    .line 2580
    new-instance v0, Lnsi;

    invoke-direct {v0}, Lnsi;-><init>()V

    iput-object v0, p0, Lnsr;->i:Lnsi;

    .line 2582
    :cond_e
    iget-object v0, p0, Lnsr;->i:Lnsi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2586
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2587
    if-eqz v0, :cond_f

    if-eq v0, v4, :cond_f

    if-eq v0, v5, :cond_f

    if-eq v0, v6, :cond_f

    if-ne v0, v7, :cond_10

    .line 2592
    :cond_f
    iput v0, p0, Lnsr;->j:I

    goto/16 :goto_0

    .line 2594
    :cond_10
    iput v1, p0, Lnsr;->j:I

    goto/16 :goto_0

    .line 2599
    :sswitch_c
    iget-object v0, p0, Lnsr;->m:Lnsx;

    if-nez v0, :cond_11

    .line 2600
    new-instance v0, Lnsx;

    invoke-direct {v0}, Lnsx;-><init>()V

    iput-object v0, p0, Lnsr;->m:Lnsx;

    .line 2602
    :cond_11
    iget-object v0, p0, Lnsr;->m:Lnsx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2481
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x60 -> :sswitch_b
        0x6a -> :sswitch_c
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 2371
    iget-object v0, p0, Lnsr;->b:Lnsb;

    if-eqz v0, :cond_0

    .line 2372
    const/4 v0, 0x1

    iget-object v1, p0, Lnsr;->b:Lnsb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2374
    :cond_0
    iget-object v0, p0, Lnsr;->c:Lnsw;

    if-eqz v0, :cond_1

    .line 2375
    const/4 v0, 0x3

    iget-object v1, p0, Lnsr;->c:Lnsw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2377
    :cond_1
    iget-object v0, p0, Lnsr;->d:Lnsv;

    if-eqz v0, :cond_2

    .line 2378
    const/4 v0, 0x4

    iget-object v1, p0, Lnsr;->d:Lnsv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2380
    :cond_2
    iget-object v0, p0, Lnsr;->e:Lnsu;

    if-eqz v0, :cond_3

    .line 2381
    const/4 v0, 0x5

    iget-object v1, p0, Lnsr;->e:Lnsu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2383
    :cond_3
    iget v0, p0, Lnsr;->f:I

    if-eq v0, v5, :cond_4

    .line 2384
    const/4 v0, 0x6

    iget v1, p0, Lnsr;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2386
    :cond_4
    iget v0, p0, Lnsr;->g:I

    if-eq v0, v5, :cond_5

    .line 2387
    const/4 v0, 0x7

    iget v1, p0, Lnsr;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2389
    :cond_5
    iget-object v0, p0, Lnsr;->h:Lnst;

    if-eqz v0, :cond_6

    .line 2390
    const/16 v0, 0x8

    iget-object v1, p0, Lnsr;->h:Lnst;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2392
    :cond_6
    iget-object v0, p0, Lnsr;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 2393
    const/16 v0, 0x9

    iget-object v1, p0, Lnsr;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2395
    :cond_7
    iget-object v0, p0, Lnsr;->l:[Lnsh;

    if-eqz v0, :cond_9

    .line 2396
    iget-object v1, p0, Lnsr;->l:[Lnsh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 2397
    if-eqz v3, :cond_8

    .line 2398
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2396
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2402
    :cond_9
    iget-object v0, p0, Lnsr;->i:Lnsi;

    if-eqz v0, :cond_a

    .line 2403
    const/16 v0, 0xb

    iget-object v1, p0, Lnsr;->i:Lnsi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2405
    :cond_a
    iget v0, p0, Lnsr;->j:I

    if-eq v0, v5, :cond_b

    .line 2406
    const/16 v0, 0xc

    iget v1, p0, Lnsr;->j:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2408
    :cond_b
    iget-object v0, p0, Lnsr;->m:Lnsx;

    if-eqz v0, :cond_c

    .line 2409
    const/16 v0, 0xd

    iget-object v1, p0, Lnsr;->m:Lnsx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2411
    :cond_c
    iget-object v0, p0, Lnsr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2413
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1821
    invoke-virtual {p0, p1}, Lnsr;->a(Loxn;)Lnsr;

    move-result-object v0

    return-object v0
.end method
