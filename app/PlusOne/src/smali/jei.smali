.class public final enum Ljei;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ljei;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ljei;

.field public static final enum b:Ljei;

.field public static final enum c:Ljei;

.field private static final synthetic d:[Ljei;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Ljei;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, Ljei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljei;->a:Ljei;

    new-instance v0, Ljei;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, Ljei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljei;->b:Ljei;

    new-instance v0, Ljei;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v4}, Ljei;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ljei;->c:Ljei;

    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [Ljei;

    sget-object v1, Ljei;->a:Ljei;

    aput-object v1, v0, v2

    sget-object v1, Ljei;->b:Ljei;

    aput-object v1, v0, v3

    sget-object v1, Ljei;->c:Ljei;

    aput-object v1, v0, v4

    sput-object v0, Ljei;->d:[Ljei;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ljei;
    .locals 1

    .prologue
    .line 30
    const-class v0, Ljei;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ljei;

    return-object v0
.end method

.method public static values()[Ljei;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Ljei;->d:[Ljei;

    invoke-virtual {v0}, [Ljei;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljei;

    return-object v0
.end method
