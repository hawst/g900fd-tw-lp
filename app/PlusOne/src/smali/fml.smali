.class public final Lfml;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lfmh;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Landroid/content/Context;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 28
    iput-object p1, p0, Lfml;->b:Landroid/content/Context;

    .line 29
    iput p2, p0, Lfml;->c:I

    .line 30
    iput-boolean p3, p0, Lfml;->d:Z

    .line 31
    return-void
.end method


# virtual methods
.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lfml;->l()Lfmh;

    move-result-object v0

    return-object v0
.end method

.method public l()Lfmh;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 35
    iget-object v0, p0, Lfml;->b:Landroid/content/Context;

    iget v1, p0, Lfml;->c:I

    .line 36
    invoke-static {v0, v1}, Ldsm;->f(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v5

    .line 38
    iget-object v0, p0, Lfml;->b:Landroid/content/Context;

    iget v1, p0, Lfml;->c:I

    invoke-static {v0, v1}, Lkgi;->a(Landroid/content/Context;I)Lkfu;

    move-result-object v0

    .line 39
    new-instance v1, Ldjv;

    iget-object v2, p0, Lfml;->b:Landroid/content/Context;

    iget v3, p0, Lfml;->c:I

    invoke-direct {v1, v2, v3}, Ldjv;-><init>(Landroid/content/Context;I)V

    .line 41
    invoke-virtual {v0, v1}, Lkfu;->a(Lkff;)V

    .line 43
    iget-boolean v1, p0, Lfml;->d:Z

    if-eqz v1, :cond_0

    .line 44
    new-instance v1, Ldko;

    iget-object v2, p0, Lfml;->b:Landroid/content/Context;

    iget v3, p0, Lfml;->c:I

    invoke-direct {v1, v2, v3}, Ldko;-><init>(Landroid/content/Context;I)V

    .line 45
    invoke-virtual {v0, v1}, Lkfu;->a(Lkff;)V

    .line 48
    :cond_0
    invoke-virtual {v0}, Lkfu;->l()V

    .line 49
    invoke-virtual {v0}, Lkfu;->t()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    :cond_1
    :goto_0
    return-object v6

    .line 58
    :cond_2
    invoke-virtual {v0}, Lkfu;->j()Ljava/util/ArrayList;

    move-result-object v4

    .line 63
    const/4 v0, 0x0

    move v3, v0

    move-object v1, v6

    move-object v2, v6

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 64
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ldjv;

    if-eqz v0, :cond_3

    .line 65
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldjv;

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    .line 63
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_1

    .line 66
    :cond_3
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ldko;

    if-eqz v0, :cond_7

    .line 67
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldko;

    move-object v1, v2

    goto :goto_2

    .line 70
    :cond_4
    invoke-virtual {v2}, Ldjv;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 72
    iget-boolean v0, p0, Lfml;->d:Z

    if-eqz v0, :cond_6

    .line 73
    invoke-virtual {v1}, Ldko;->b()Lhgw;

    move-result-object v2

    .line 74
    invoke-virtual {v1}, Ldko;->c()Ljava/lang/String;

    move-result-object v3

    .line 75
    invoke-virtual {v1}, Ldko;->d()[I

    move-result-object v4

    .line 82
    :goto_3
    if-nez v7, :cond_5

    if-eqz v2, :cond_1

    .line 86
    :cond_5
    new-instance v0, Lfmh;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct/range {v0 .. v5}, Lfmh;-><init>(ZLhgw;Ljava/lang/String;[ILjava/util/ArrayList;)V

    move-object v6, v0

    goto :goto_0

    :cond_6
    move-object v4, v6

    move-object v3, v6

    move-object v2, v6

    .line 79
    goto :goto_3

    :cond_7
    move-object v0, v1

    move-object v1, v2

    goto :goto_2
.end method
