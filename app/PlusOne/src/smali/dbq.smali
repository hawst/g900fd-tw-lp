.class public final enum Ldbq;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldbq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldbq;

.field public static final enum b:Ldbq;

.field public static final enum c:Ldbq;

.field private static final synthetic d:[Ldbq;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Ldbq;

    const-string v1, "TRASH_CAN"

    invoke-direct {v0, v1, v2}, Ldbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbq;->a:Ldbq;

    .line 43
    new-instance v0, Ldbq;

    const-string v1, "YES_BUTTON"

    invoke-direct {v0, v1, v3}, Ldbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbq;->b:Ldbq;

    .line 44
    new-instance v0, Ldbq;

    const-string v1, "NO_BUTTON"

    invoke-direct {v0, v1, v4}, Ldbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbq;->c:Ldbq;

    .line 41
    const/4 v0, 0x3

    new-array v0, v0, [Ldbq;

    sget-object v1, Ldbq;->a:Ldbq;

    aput-object v1, v0, v2

    sget-object v1, Ldbq;->b:Ldbq;

    aput-object v1, v0, v3

    sget-object v1, Ldbq;->c:Ldbq;

    aput-object v1, v0, v4

    sput-object v0, Ldbq;->d:[Ldbq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbq;
    .locals 1

    .prologue
    .line 41
    const-class v0, Ldbq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbq;

    return-object v0
.end method

.method public static values()[Ldbq;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Ldbq;->d:[Ldbq;

    invoke-virtual {v0}, [Ldbq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbq;

    return-object v0
.end method
