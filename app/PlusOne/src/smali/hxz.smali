.class public abstract Lhxz;
.super Ldf;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ldf",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Ldf;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final C()V
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lhxz;->c:Z

    if-nez v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lhxz;->D_()Z

    move-result v0

    iput-boolean v0, p0, Lhxz;->c:Z

    .line 91
    :cond_0
    return-void
.end method

.method public final D()V
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lhxz;->c:Z

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lhxz;->k()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lhxz;->c:Z

    .line 97
    :cond_0
    return-void

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D_()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 56
    iget-boolean v0, p0, Lhxz;->b:Z

    if-nez v0, :cond_0

    .line 57
    invoke-super {p0, p1}, Ldf;->b(Ljava/lang/Object;)V

    .line 59
    :cond_0
    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 38
    iget-boolean v0, p0, Lhxz;->b:Z

    if-nez v0, :cond_0

    .line 40
    :try_start_0
    invoke-virtual {p0}, Lhxz;->j()Ljava/lang/Object;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhxz;->b:Z

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0}, Ldf;->g()V

    .line 70
    invoke-virtual {p0}, Lhxz;->t()V

    .line 71
    invoke-virtual {p0}, Lhxz;->C()V

    .line 72
    return-void
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Ldf;->h()V

    .line 64
    invoke-virtual {p0}, Lhxz;->b()Z

    .line 65
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 76
    invoke-virtual {p0}, Lhxz;->b()Z

    .line 77
    invoke-super {p0}, Ldf;->i()V

    .line 78
    invoke-virtual {p0}, Lhxz;->D()V

    .line 79
    return-void
.end method

.method public abstract j()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x1

    return v0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 83
    invoke-super {p0}, Ldf;->w()V

    .line 84
    invoke-virtual {p0}, Lhxz;->D()V

    .line 85
    return-void
.end method
