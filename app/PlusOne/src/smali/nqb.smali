.class public final Lnqb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnqb;


# instance fields
.field public b:Lnqc;

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1137
    const/4 v0, 0x0

    new-array v0, v0, [Lnqb;

    sput-object v0, Lnqb;->a:[Lnqb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1138
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1141
    const/4 v0, 0x0

    iput-object v0, p0, Lnqb;->b:Lnqc;

    .line 1138
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1160
    const/4 v0, 0x0

    .line 1161
    iget-object v1, p0, Lnqb;->b:Lnqc;

    if-eqz v1, :cond_0

    .line 1162
    const/4 v0, 0x1

    iget-object v1, p0, Lnqb;->b:Lnqc;

    .line 1163
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1165
    :cond_0
    iget-object v1, p0, Lnqb;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1166
    const/4 v1, 0x2

    iget-object v2, p0, Lnqb;->c:Ljava/lang/String;

    .line 1167
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1169
    :cond_1
    iget-object v1, p0, Lnqb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1170
    iput v0, p0, Lnqb;->ai:I

    .line 1171
    return v0
.end method

.method public a(Loxn;)Lnqb;
    .locals 2

    .prologue
    .line 1179
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1180
    sparse-switch v0, :sswitch_data_0

    .line 1184
    iget-object v1, p0, Lnqb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1185
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnqb;->ah:Ljava/util/List;

    .line 1188
    :cond_1
    iget-object v1, p0, Lnqb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1190
    :sswitch_0
    return-object p0

    .line 1195
    :sswitch_1
    iget-object v0, p0, Lnqb;->b:Lnqc;

    if-nez v0, :cond_2

    .line 1196
    new-instance v0, Lnqc;

    invoke-direct {v0}, Lnqc;-><init>()V

    iput-object v0, p0, Lnqb;->b:Lnqc;

    .line 1198
    :cond_2
    iget-object v0, p0, Lnqb;->b:Lnqc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1202
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqb;->c:Ljava/lang/String;

    goto :goto_0

    .line 1180
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1148
    iget-object v0, p0, Lnqb;->b:Lnqc;

    if-eqz v0, :cond_0

    .line 1149
    const/4 v0, 0x1

    iget-object v1, p0, Lnqb;->b:Lnqc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1151
    :cond_0
    iget-object v0, p0, Lnqb;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1152
    const/4 v0, 0x2

    iget-object v1, p0, Lnqb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1154
    :cond_1
    iget-object v0, p0, Lnqb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1156
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1134
    invoke-virtual {p0, p1}, Lnqb;->a(Loxn;)Lnqb;

    move-result-object v0

    return-object v0
.end method
