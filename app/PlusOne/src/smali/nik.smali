.class public final Lnik;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnij;

.field public b:Ljava/lang/Boolean;

.field private c:Lnja;

.field private d:Lnja;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2475
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2478
    iput-object v0, p0, Lnik;->c:Lnja;

    .line 2481
    iput-object v0, p0, Lnik;->d:Lnja;

    .line 2484
    sget-object v0, Lnij;->a:[Lnij;

    iput-object v0, p0, Lnik;->a:[Lnij;

    .line 2475
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2513
    .line 2514
    iget-object v0, p0, Lnik;->c:Lnja;

    if-eqz v0, :cond_4

    .line 2515
    const/4 v0, 0x1

    iget-object v2, p0, Lnik;->c:Lnja;

    .line 2516
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2518
    :goto_0
    iget-object v2, p0, Lnik;->a:[Lnij;

    if-eqz v2, :cond_1

    .line 2519
    iget-object v2, p0, Lnik;->a:[Lnij;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2520
    if-eqz v4, :cond_0

    .line 2521
    const/4 v5, 0x2

    .line 2522
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2519
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2526
    :cond_1
    iget-object v1, p0, Lnik;->d:Lnja;

    if-eqz v1, :cond_2

    .line 2527
    const/4 v1, 0x3

    iget-object v2, p0, Lnik;->d:Lnja;

    .line 2528
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2530
    :cond_2
    iget-object v1, p0, Lnik;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2531
    const/4 v1, 0x4

    iget-object v2, p0, Lnik;->b:Ljava/lang/Boolean;

    .line 2532
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2534
    :cond_3
    iget-object v1, p0, Lnik;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2535
    iput v0, p0, Lnik;->ai:I

    .line 2536
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnik;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2544
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2545
    sparse-switch v0, :sswitch_data_0

    .line 2549
    iget-object v2, p0, Lnik;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2550
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnik;->ah:Ljava/util/List;

    .line 2553
    :cond_1
    iget-object v2, p0, Lnik;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2555
    :sswitch_0
    return-object p0

    .line 2560
    :sswitch_1
    iget-object v0, p0, Lnik;->c:Lnja;

    if-nez v0, :cond_2

    .line 2561
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnik;->c:Lnja;

    .line 2563
    :cond_2
    iget-object v0, p0, Lnik;->c:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2567
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2568
    iget-object v0, p0, Lnik;->a:[Lnij;

    if-nez v0, :cond_4

    move v0, v1

    .line 2569
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnij;

    .line 2570
    iget-object v3, p0, Lnik;->a:[Lnij;

    if-eqz v3, :cond_3

    .line 2571
    iget-object v3, p0, Lnik;->a:[Lnij;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2573
    :cond_3
    iput-object v2, p0, Lnik;->a:[Lnij;

    .line 2574
    :goto_2
    iget-object v2, p0, Lnik;->a:[Lnij;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2575
    iget-object v2, p0, Lnik;->a:[Lnij;

    new-instance v3, Lnij;

    invoke-direct {v3}, Lnij;-><init>()V

    aput-object v3, v2, v0

    .line 2576
    iget-object v2, p0, Lnik;->a:[Lnij;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2577
    invoke-virtual {p1}, Loxn;->a()I

    .line 2574
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2568
    :cond_4
    iget-object v0, p0, Lnik;->a:[Lnij;

    array-length v0, v0

    goto :goto_1

    .line 2580
    :cond_5
    iget-object v2, p0, Lnik;->a:[Lnij;

    new-instance v3, Lnij;

    invoke-direct {v3}, Lnij;-><init>()V

    aput-object v3, v2, v0

    .line 2581
    iget-object v2, p0, Lnik;->a:[Lnij;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2585
    :sswitch_3
    iget-object v0, p0, Lnik;->d:Lnja;

    if-nez v0, :cond_6

    .line 2586
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnik;->d:Lnja;

    .line 2588
    :cond_6
    iget-object v0, p0, Lnik;->d:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2592
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnik;->b:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 2545
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2491
    iget-object v0, p0, Lnik;->c:Lnja;

    if-eqz v0, :cond_0

    .line 2492
    const/4 v0, 0x1

    iget-object v1, p0, Lnik;->c:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2494
    :cond_0
    iget-object v0, p0, Lnik;->a:[Lnij;

    if-eqz v0, :cond_2

    .line 2495
    iget-object v1, p0, Lnik;->a:[Lnij;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2496
    if-eqz v3, :cond_1

    .line 2497
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2495
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2501
    :cond_2
    iget-object v0, p0, Lnik;->d:Lnja;

    if-eqz v0, :cond_3

    .line 2502
    const/4 v0, 0x3

    iget-object v1, p0, Lnik;->d:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2504
    :cond_3
    iget-object v0, p0, Lnik;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 2505
    const/4 v0, 0x4

    iget-object v1, p0, Lnik;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2507
    :cond_4
    iget-object v0, p0, Lnik;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2509
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2471
    invoke-virtual {p0, p1}, Lnik;->a(Loxn;)Lnik;

    move-result-object v0

    return-object v0
.end method
