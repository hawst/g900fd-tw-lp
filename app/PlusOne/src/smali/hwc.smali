.class public final Lhwc;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lkdd;
.implements Lljh;


# instance fields
.field private final a:Lhvx;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/graphics/Rect;

.field private f:Lizu;

.field private g:Lcom/google/android/libraries/social/media/MediaResource;

.field private h:I

.field private i:Z

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 84
    invoke-virtual {p0}, Lhwc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhvx;->a(Landroid/content/Context;)Lhvx;

    move-result-object v0

    iput-object v0, p0, Lhwc;->a:Lhvx;

    .line 85
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhwc;->e:Landroid/graphics/Rect;

    .line 86
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhwc;->n:Landroid/graphics/Rect;

    .line 87
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhwc;->setWillNotDraw(Z)V

    .line 70
    return-void
.end method

.method private a(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 438
    if-nez p1, :cond_1

    .line 441
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 371
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 374
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwc;->removeView(Landroid/view/View;)V

    .line 377
    :cond_0
    iget-object v0, p0, Lhwc;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 378
    iget-object v0, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    iget-object v0, p0, Lhwc;->c:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 380
    iget-object v0, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwc;->removeView(Landroid/view/View;)V

    .line 383
    :cond_1
    iget-object v0, p0, Lhwc;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 384
    iget-object v0, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    iget-object v0, p0, Lhwc;->d:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 386
    iget-object v0, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwc;->removeView(Landroid/view/View;)V

    .line 389
    :cond_2
    iput-object v2, p0, Lhwc;->f:Lizu;

    .line 391
    iget-object v0, p0, Lhwc;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 392
    iget-object v0, p0, Lhwc;->n:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 394
    iput v3, p0, Lhwc;->j:I

    .line 395
    iput v3, p0, Lhwc;->k:I

    .line 396
    iput v3, p0, Lhwc;->h:I

    .line 397
    return-void
.end method


# virtual methods
.method protected a(I)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 197
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 198
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 202
    iget-object v3, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lhwc;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 203
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->measure(II)V

    .line 204
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iget-object v3, p0, Lhwc;->a:Lhvx;

    iget v3, v3, Lhvx;->d:I

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 207
    :cond_0
    iget-object v3, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lhwc;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 208
    iget-object v3, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v2, v1}, Landroid/widget/TextView;->measure(II)V

    .line 209
    iget-object v3, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lhwc;->a:Lhvx;

    iget v4, v4, Lhvx;->d:I

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 212
    :cond_1
    iget-object v3, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lhwc;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 213
    iget-object v3, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v2, v1}, Landroid/widget/TextView;->measure(II)V

    .line 214
    iget-object v1, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lhwc;->a:Lhvx;

    iget v2, v2, Lhvx;->d:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 217
    :cond_2
    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwc;->i:Z

    .line 418
    invoke-virtual {p0}, Lhwc;->c()V

    .line 419
    invoke-virtual {p0}, Lhwc;->removeAllViews()V

    .line 420
    invoke-direct {p0}, Lhwc;->e()V

    .line 421
    return-void
.end method

.method public a(Lhui;)V
    .locals 6

    .prologue
    const-wide v4, 0x3ffaaaaaaaaaaaabL    # 1.6666666666666667

    .line 106
    invoke-direct {p0}, Lhwc;->e()V

    .line 108
    if-nez p1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwc;->i:Z

    .line 113
    invoke-virtual {p0, p1}, Lhwc;->b(Lhui;)V

    .line 114
    invoke-virtual {p1}, Lhui;->d()Ljava/lang/String;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_2

    .line 117
    invoke-virtual {p0}, Lhwc;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v1, v0, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lhwc;->f:Lizu;

    .line 119
    invoke-virtual {p1}, Lhui;->f()I

    move-result v0

    iput v0, p0, Lhwc;->j:I

    .line 120
    invoke-virtual {p1}, Lhui;->g()I

    move-result v0

    iput v0, p0, Lhwc;->k:I

    .line 124
    iget v0, p0, Lhwc;->j:I

    int-to-double v0, v0

    iget v2, p0, Lhwc;->k:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    cmpg-double v0, v0, v4

    if-gez v0, :cond_0

    .line 125
    iget v0, p0, Lhwc;->j:I

    int-to-double v0, v0

    div-double/2addr v0, v4

    double-to-int v0, v0

    iput v0, p0, Lhwc;->k:I

    goto :goto_0

    .line 128
    :cond_2
    invoke-virtual {p0}, Lhwc;->c()V

    goto :goto_0
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 474
    invoke-virtual {p0}, Lhwc;->invalidate()V

    .line 475
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 449
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lhwc;->f:Lizu;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lhwc;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lhwc;->l:I

    if-lez v0, :cond_0

    iget v0, p0, Lhwc;->m:I

    if-lez v0, :cond_0

    .line 451
    invoke-virtual {p0}, Lhwc;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iget-object v1, p0, Lhwc;->f:Lizu;

    iget v2, p0, Lhwc;->l:I

    iget v3, p0, Lhwc;->m:I

    const/16 v4, 0x40

    move-object v5, p0

    .line 452
    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lhwc;->g:Lcom/google/android/libraries/social/media/MediaResource;

    .line 456
    :cond_0
    return-void
.end method

.method protected b(Lhui;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 136
    invoke-virtual {p0}, Lhwc;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 138
    iget-object v0, p0, Lhwc;->a:Lhvx;

    iget v2, v0, Lhvx;->q:I

    .line 140
    invoke-virtual {p1}, Lhui;->b()Ljava/lang/CharSequence;

    move-result-object v3

    .line 142
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 144
    const/16 v0, 0x1a

    invoke-static {v1, v6, v5, v0}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    .line 146
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 147
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwc;->addView(Landroid/view/View;)V

    .line 154
    :cond_1
    iget-object v0, p0, Lhwc;->a:Lhvx;

    iget v0, v0, Lhvx;->s:I

    .line 158
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLineCount()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 159
    add-int/lit8 v0, v0, -0x1

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 162
    :cond_2
    invoke-virtual {p1}, Lhui;->c()Ljava/lang/CharSequence;

    move-result-object v2

    .line 164
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 165
    iget-object v3, p0, Lhwc;->c:Landroid/widget/TextView;

    if-nez v3, :cond_3

    .line 166
    const/4 v3, 0x7

    invoke-static {v1, v6, v5, v3}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v3

    iput-object v3, p0, Lhwc;->c:Landroid/widget/TextView;

    .line 168
    iget-object v3, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 169
    iget-object v0, p0, Lhwc;->c:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 171
    :cond_3
    iget-object v0, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v0, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwc;->addView(Landroid/view/View;)V

    .line 176
    :cond_4
    invoke-virtual {p1}, Lhui;->e()Ljava/lang/CharSequence;

    move-result-object v0

    .line 178
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 179
    iget-object v2, p0, Lhwc;->d:Landroid/widget/TextView;

    if-nez v2, :cond_5

    .line 180
    const/16 v2, 0xa

    invoke-static {v1, v6, v5, v2}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lhwc;->d:Landroid/widget/TextView;

    .line 182
    iget-object v1, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 183
    iget-object v1, p0, Lhwc;->d:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 185
    :cond_5
    iget-object v1, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v1, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 187
    iget-object v0, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhwc;->addView(Landroid/view/View;)V

    .line 189
    :cond_6
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lhwc;->g:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lhwc;->g:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 465
    const/4 v0, 0x0

    iput-object v0, p0, Lhwc;->g:Lcom/google/android/libraries/social/media/MediaResource;

    .line 467
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lhwc;->f:Lizu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 482
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 483
    invoke-virtual {p0}, Lhwc;->b()V

    .line 484
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 340
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 343
    iget-object v0, p0, Lhwc;->g:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_2

    .line 348
    iget-object v0, p0, Lhwc;->g:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 349
    iget-object v3, p0, Lhwc;->g:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/media/MediaResource;->getWidth()I

    .line 350
    iget-object v3, p0, Lhwc;->g:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/media/MediaResource;->getHeight()I

    .line 353
    :goto_0
    if-eqz v0, :cond_0

    .line 354
    iget-object v3, p0, Lhwc;->n:Landroid/graphics/Rect;

    iget-object v4, p0, Lhwc;->a:Lhvx;

    iget-object v4, v4, Lhvx;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 355
    iget-object v3, p0, Lhwc;->e:Landroid/graphics/Rect;

    iget-object v4, p0, Lhwc;->a:Lhvx;

    iget-object v4, v4, Lhvx;->r:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 356
    iget-object v0, p0, Lhwc;->n:Landroid/graphics/Rect;

    iget-object v1, p0, Lhwc;->a:Lhvx;

    iget-object v1, v1, Lhvx;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 359
    :cond_0
    invoke-virtual {p0}, Lhwc;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    .line 360
    iget-object v0, p0, Lhwc;->a:Lhvx;

    iget v0, v0, Lhvx;->d:I

    .line 363
    int-to-float v1, v0

    invoke-virtual {p0}, Lhwc;->getWidth()I

    move-result v3

    sub-int v0, v3, v0

    int-to-float v3, v0

    iget-object v0, p0, Lhwc;->a:Lhvx;

    iget-object v5, v0, Lhvx;->t:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 365
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 307
    iget-object v0, p0, Lhwc;->a:Lhvx;

    iget v1, v0, Lhvx;->d:I

    .line 310
    mul-int/lit8 v2, v1, 0x2

    .line 312
    invoke-virtual {p0}, Lhwc;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 313
    iget v0, p0, Lhwc;->m:I

    add-int/2addr v0, v1

    add-int/2addr v0, v1

    .line 316
    :goto_0
    iget-object v3, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lhwc;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 317
    iget-object v3, p0, Lhwc;->b:Landroid/widget/TextView;

    iget-object v4, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    iget-object v5, p0, Lhwc;->b:Landroid/widget/TextView;

    .line 318
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    .line 317
    invoke-virtual {v3, v2, v0, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 320
    iget-object v3, p0, Lhwc;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v1

    add-int/2addr v0, v3

    .line 323
    :cond_0
    iget-object v3, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lhwc;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 324
    iget-object v3, p0, Lhwc;->c:Landroid/widget/TextView;

    iget-object v4, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    iget-object v5, p0, Lhwc;->c:Landroid/widget/TextView;

    .line 325
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    .line 324
    invoke-virtual {v3, v2, v0, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 327
    iget-object v3, p0, Lhwc;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 330
    :cond_1
    iget-object v1, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lhwc;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 331
    iget-object v1, p0, Lhwc;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lhwc;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v2

    iget-object v4, p0, Lhwc;->d:Landroid/widget/TextView;

    .line 332
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 331
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 334
    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 232
    iget-boolean v0, p0, Lhwc;->i:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " expected to have been bound with valid data. Was bind() called?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_0
    invoke-virtual {p0}, Lhwc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lhvj;->a(Landroid/content/Context;I)I

    move-result v0

    .line 235
    iget-object v1, p0, Lhwc;->a:Lhvx;

    iget v1, v1, Lhvx;->d:I

    mul-int/lit8 v1, v1, 0x4

    sub-int v1, v0, v1

    .line 237
    invoke-virtual {p0}, Lhwc;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 238
    iget v2, p0, Lhwc;->j:I

    if-lez v2, :cond_1

    .line 239
    iget-object v2, p0, Lhwc;->a:Lhvx;

    iget v2, v2, Lhvx;->d:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    iput v2, p0, Lhwc;->l:I

    .line 240
    iget v2, p0, Lhwc;->l:I

    iget v3, p0, Lhwc;->k:I

    mul-int/2addr v2, v3

    iget v3, p0, Lhwc;->j:I

    div-int/2addr v2, v3

    iput v2, p0, Lhwc;->m:I

    .line 244
    :cond_1
    invoke-virtual {p0, v1}, Lhwc;->a(I)I

    move-result v1

    .line 245
    if-nez v1, :cond_2

    invoke-virtual {p0}, Lhwc;->d()Z

    move-result v2

    if-nez v2, :cond_2

    .line 249
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhwc;->setVisibility(I)V

    .line 250
    invoke-virtual {p0, v4, v4}, Lhwc;->setMeasuredDimension(II)V

    .line 300
    :goto_0
    return-void

    .line 255
    :cond_2
    iget v2, p0, Lhwc;->l:I

    if-eqz v2, :cond_3

    iget v2, p0, Lhwc;->m:I

    if-nez v2, :cond_4

    .line 256
    :cond_3
    invoke-virtual {p0, v0, v1}, Lhwc;->setMeasuredDimension(II)V

    goto :goto_0

    .line 261
    :cond_4
    iget-object v2, p0, Lhwc;->a:Lhvx;

    iget v2, v2, Lhvx;->d:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v1

    .line 263
    iget-object v3, p0, Lhwc;->a:Lhvx;

    iget v3, v3, Lhvx;->d:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    .line 264
    iget v4, p0, Lhwc;->k:I

    mul-int/2addr v4, v3

    iget v5, p0, Lhwc;->j:I

    div-int/2addr v4, v5

    .line 267
    if-lez v3, :cond_6

    if-lez v4, :cond_6

    .line 268
    iget v5, p0, Lhwc;->l:I

    if-ne v3, v5, :cond_5

    iget v5, p0, Lhwc;->m:I

    if-ne v4, v5, :cond_5

    iget-object v5, p0, Lhwc;->g:Lcom/google/android/libraries/social/media/MediaResource;

    if-nez v5, :cond_6

    .line 270
    :cond_5
    iput v3, p0, Lhwc;->l:I

    .line 271
    iput v4, p0, Lhwc;->m:I

    .line 272
    invoke-virtual {p0}, Lhwc;->b()V

    .line 276
    :cond_6
    invoke-virtual {p0}, Lhwc;->d()Z

    move-result v3

    if-nez v3, :cond_7

    .line 277
    iget-object v3, p0, Lhwc;->a:Lhvx;

    iget v3, v3, Lhvx;->p:I

    iget-object v4, p0, Lhwc;->a:Lhvx;

    iget v4, v4, Lhvx;->d:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 279
    sub-int/2addr v3, v2

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lhwc;->a:Lhvx;

    iget v4, v4, Lhvx;->d:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lhwc;->h:I

    .line 284
    :cond_7
    iget v3, p0, Lhwc;->h:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, Lhwc;->m:I

    add-int/2addr v3, v1

    iget-object v4, p0, Lhwc;->a:Lhvx;

    iget v4, v4, Lhvx;->d:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 287
    invoke-virtual {p0}, Lhwc;->d()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 288
    iget-object v3, p0, Lhwc;->e:Landroid/graphics/Rect;

    iget-object v4, p0, Lhwc;->a:Lhvx;

    iget v4, v4, Lhvx;->d:I

    iget-object v5, p0, Lhwc;->a:Lhvx;

    iget v5, v5, Lhvx;->d:I

    iget-object v6, p0, Lhwc;->a:Lhvx;

    iget v6, v6, Lhvx;->d:I

    iget v7, p0, Lhwc;->l:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lhwc;->a:Lhvx;

    iget v7, v7, Lhvx;->d:I

    iget v8, p0, Lhwc;->m:I

    add-int/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 292
    iget-object v3, p0, Lhwc;->a:Lhvx;

    iget-object v3, v3, Lhvx;->g:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    float-to-int v3, v3

    .line 293
    iget-object v4, p0, Lhwc;->n:Landroid/graphics/Rect;

    iget-object v5, p0, Lhwc;->e:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    div-int/lit8 v6, v3, 0x2

    add-int/2addr v5, v6

    iget-object v6, p0, Lhwc;->e:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    div-int/lit8 v7, v3, 0x2

    add-int/2addr v6, v7

    iget-object v7, p0, Lhwc;->e:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    div-int/lit8 v8, v3, 0x2

    sub-int/2addr v7, v8

    iget-object v8, p0, Lhwc;->e:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v8

    iget-object v8, p0, Lhwc;->a:Lhvx;

    iget v8, v8, Lhvx;->d:I

    add-int/2addr v1, v8

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    invoke-virtual {v4, v5, v6, v7, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 299
    :cond_8
    invoke-virtual {p0, v0, v2}, Lhwc;->setMeasuredDimension(II)V

    goto/16 :goto_0
.end method
