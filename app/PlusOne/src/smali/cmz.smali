.class public final Lcmz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqz;
.implements Llrb;
.implements Llrc;
.implements Llrd;
.implements Llrg;


# instance fields
.field private a:Lae;

.field private b:Lz;

.field private c:Lu;

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lu;Llqr;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcmz;->c:Lu;

    .line 48
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 49
    return-void
.end method

.method public constructor <init>(Lz;Llqr;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcmz;->b:Lz;

    .line 42
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 43
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 52
    iget-boolean v0, p0, Lcmz;->d:Z

    if-nez v0, :cond_0

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmz;->e:Z

    .line 64
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcmz;->a:Lae;

    const-string v1, "com.google.android.apps.photos.ProgressDialogMixin.Pending"

    .line 58
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 59
    if-eqz v0, :cond_1

    .line 60
    invoke-virtual {v0}, Lt;->a()V

    .line 63
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcmz;->e:Z

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcmz;->b:Lz;

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcmz;->b:Lz;

    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    iput-object v0, p0, Lcmz;->a:Lae;

    .line 95
    :goto_0
    if-eqz p1, :cond_0

    .line 96
    const-string v0, "com.google.android.apps.photos.ProgressDialogMixin.DismissOnResume"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcmz;->e:Z

    .line 98
    :cond_0
    return-void

    .line 92
    :cond_1
    iget-object v0, p0, Lcmz;->c:Lu;

    invoke-virtual {v0}, Lu;->p()Lae;

    move-result-object v0

    iput-object v0, p0, Lcmz;->a:Lae;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 77
    iget-boolean v0, p0, Lcmz;->d:Z

    if-nez v0, :cond_0

    .line 85
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-virtual {p0}, Lcmz;->a()V

    .line 82
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 83
    invoke-static {v0, p1, v1}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 84
    iget-object v1, p0, Lcmz;->a:Lae;

    const-string v2, "com.google.android.apps.photos.ProgressDialogMixin.Pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmz;->d:Z

    .line 103
    iget-boolean v0, p0, Lcmz;->e:Z

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lcmz;->a()V

    .line 106
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 115
    const-string v0, "com.google.android.apps.photos.ProgressDialogMixin.DismissOnResume"

    iget-boolean v1, p0, Lcmz;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcmz;->d:Z

    .line 111
    return-void
.end method
