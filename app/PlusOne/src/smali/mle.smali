.class public final Lmle;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmry;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31183
    invoke-direct {p0}, Loxq;-><init>()V

    .line 31186
    iput-object v0, p0, Lmle;->apiHeader:Llyq;

    .line 31189
    iput-object v0, p0, Lmle;->a:Lmry;

    .line 31183
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 31206
    const/4 v0, 0x0

    .line 31207
    iget-object v1, p0, Lmle;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 31208
    const/4 v0, 0x1

    iget-object v1, p0, Lmle;->apiHeader:Llyq;

    .line 31209
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 31211
    :cond_0
    iget-object v1, p0, Lmle;->a:Lmry;

    if-eqz v1, :cond_1

    .line 31212
    const/4 v1, 0x2

    iget-object v2, p0, Lmle;->a:Lmry;

    .line 31213
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31215
    :cond_1
    iget-object v1, p0, Lmle;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31216
    iput v0, p0, Lmle;->ai:I

    .line 31217
    return v0
.end method

.method public a(Loxn;)Lmle;
    .locals 2

    .prologue
    .line 31225
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 31226
    sparse-switch v0, :sswitch_data_0

    .line 31230
    iget-object v1, p0, Lmle;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 31231
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmle;->ah:Ljava/util/List;

    .line 31234
    :cond_1
    iget-object v1, p0, Lmle;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 31236
    :sswitch_0
    return-object p0

    .line 31241
    :sswitch_1
    iget-object v0, p0, Lmle;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 31242
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmle;->apiHeader:Llyq;

    .line 31244
    :cond_2
    iget-object v0, p0, Lmle;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 31248
    :sswitch_2
    iget-object v0, p0, Lmle;->a:Lmry;

    if-nez v0, :cond_3

    .line 31249
    new-instance v0, Lmry;

    invoke-direct {v0}, Lmry;-><init>()V

    iput-object v0, p0, Lmle;->a:Lmry;

    .line 31251
    :cond_3
    iget-object v0, p0, Lmle;->a:Lmry;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 31226
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 31194
    iget-object v0, p0, Lmle;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 31195
    const/4 v0, 0x1

    iget-object v1, p0, Lmle;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31197
    :cond_0
    iget-object v0, p0, Lmle;->a:Lmry;

    if-eqz v0, :cond_1

    .line 31198
    const/4 v0, 0x2

    iget-object v1, p0, Lmle;->a:Lmry;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31200
    :cond_1
    iget-object v0, p0, Lmle;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 31202
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 31179
    invoke-virtual {p0, p1}, Lmle;->a(Loxn;)Lmle;

    move-result-object v0

    return-object v0
.end method
