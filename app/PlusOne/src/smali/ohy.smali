.class public final Lohy;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lohz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2513
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2518
    sget-object v0, Lohz;->a:[Lohz;

    iput-object v0, p0, Lohy;->b:[Lohz;

    .line 2513
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2539
    .line 2540
    iget-object v0, p0, Lohy;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2541
    const/4 v0, 0x1

    iget-object v2, p0, Lohy;->a:Ljava/lang/String;

    .line 2542
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2544
    :goto_0
    iget-object v2, p0, Lohy;->b:[Lohz;

    if-eqz v2, :cond_1

    .line 2545
    iget-object v2, p0, Lohy;->b:[Lohz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2546
    if-eqz v4, :cond_0

    .line 2547
    const/4 v5, 0x2

    .line 2548
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2545
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2552
    :cond_1
    iget-object v1, p0, Lohy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2553
    iput v0, p0, Lohy;->ai:I

    .line 2554
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lohy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2562
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2563
    sparse-switch v0, :sswitch_data_0

    .line 2567
    iget-object v2, p0, Lohy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2568
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lohy;->ah:Ljava/util/List;

    .line 2571
    :cond_1
    iget-object v2, p0, Lohy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2573
    :sswitch_0
    return-object p0

    .line 2578
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohy;->a:Ljava/lang/String;

    goto :goto_0

    .line 2582
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2583
    iget-object v0, p0, Lohy;->b:[Lohz;

    if-nez v0, :cond_3

    move v0, v1

    .line 2584
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohz;

    .line 2585
    iget-object v3, p0, Lohy;->b:[Lohz;

    if-eqz v3, :cond_2

    .line 2586
    iget-object v3, p0, Lohy;->b:[Lohz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2588
    :cond_2
    iput-object v2, p0, Lohy;->b:[Lohz;

    .line 2589
    :goto_2
    iget-object v2, p0, Lohy;->b:[Lohz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2590
    iget-object v2, p0, Lohy;->b:[Lohz;

    new-instance v3, Lohz;

    invoke-direct {v3}, Lohz;-><init>()V

    aput-object v3, v2, v0

    .line 2591
    iget-object v2, p0, Lohy;->b:[Lohz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2592
    invoke-virtual {p1}, Loxn;->a()I

    .line 2589
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2583
    :cond_3
    iget-object v0, p0, Lohy;->b:[Lohz;

    array-length v0, v0

    goto :goto_1

    .line 2595
    :cond_4
    iget-object v2, p0, Lohy;->b:[Lohz;

    new-instance v3, Lohz;

    invoke-direct {v3}, Lohz;-><init>()V

    aput-object v3, v2, v0

    .line 2596
    iget-object v2, p0, Lohy;->b:[Lohz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2563
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2523
    iget-object v0, p0, Lohy;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2524
    const/4 v0, 0x1

    iget-object v1, p0, Lohy;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2526
    :cond_0
    iget-object v0, p0, Lohy;->b:[Lohz;

    if-eqz v0, :cond_2

    .line 2527
    iget-object v1, p0, Lohy;->b:[Lohz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2528
    if-eqz v3, :cond_1

    .line 2529
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2527
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2533
    :cond_2
    iget-object v0, p0, Lohy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2535
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2509
    invoke-virtual {p0, p1}, Lohy;->a(Loxn;)Lohy;

    move-result-object v0

    return-object v0
.end method
