.class public final Llex;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:J


# instance fields
.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lleu;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Llev;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private final e:Lhei;

.field private final f:Llet;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 33
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Llex;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Llex;->b:Ljava/util/HashMap;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Llex;->c:Ljava/util/HashMap;

    .line 44
    iput-object p1, p0, Llex;->d:Landroid/content/Context;

    .line 45
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Llex;->e:Lhei;

    .line 46
    const-class v0, Llet;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llet;

    iput-object v0, p0, Llex;->f:Llet;

    .line 48
    const-class v0, Lleu;

    invoke-static {p1, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    move v1, v2

    .line 49
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 50
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lleu;

    .line 51
    invoke-interface {v0}, Lleu;->a()Llev;

    move-result-object v5

    .line 52
    iget-object v6, p0, Llex;->b:Ljava/util/HashMap;

    invoke-interface {v5}, Llev;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v3

    :goto_1
    const-string v6, "Two synclets with same name: %s"

    new-array v7, v3, [Ljava/lang/Object;

    .line 53
    invoke-interface {v5}, Llev;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    .line 52
    invoke-static {v0, v6, v7}, Llsk;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 54
    iget-object v0, p0, Llex;->c:Ljava/util/HashMap;

    invoke-interface {v5}, Llev;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 52
    goto :goto_1

    .line 56
    :cond_1
    return-void
.end method

.method private a(ILlev;)J
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 147
    iget-object v0, p0, Llex;->d:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 149
    const-string v1, "synclet_status"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "last_sync"

    aput-object v3, v2, v7

    const-string v3, "synclet_name = ?"

    new-array v4, v4, [Ljava/lang/String;

    .line 152
    invoke-interface {p2}, Llev;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    .line 149
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 156
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 158
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 156
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    :try_start_0
    iget-object v0, p0, Llex;->e:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lhel; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lleu;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Llex;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lleu;

    return-object v0
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Llex;->d:Landroid/content/Context;

    invoke-static {v0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 142
    const-string v1, "synclet_status"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 143
    return-void
.end method

.method public a(Ljava/lang/String;Lkfp;ILles;)V
    .locals 5

    .prologue
    .line 70
    invoke-direct {p0, p3}, Llex;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 71
    if-nez v2, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v0, p0, Llex;->f:Llet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Llex;->f:Llet;

    .line 77
    invoke-interface {v0, p3}, Llet;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    .line 78
    :goto_1
    invoke-virtual {p0, p1}, Llex;->b(Ljava/lang/String;)Llev;

    move-result-object v3

    .line 79
    if-eqz v0, :cond_3

    .line 80
    new-instance v1, Lkoe;

    .line 81
    invoke-interface {v3}, Llev;->b()I

    move-result v4

    invoke-direct {v1, v4}, Lkoe;-><init>(I)V

    invoke-virtual {v1, v2}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v1

    iget-object v4, p0, Llex;->d:Landroid/content/Context;

    invoke-virtual {v1, v4}, Lkoe;->a(Landroid/content/Context;)V

    .line 85
    :cond_3
    :try_start_0
    invoke-virtual {p0, p1}, Llex;->a(Ljava/lang/String;)Lleu;

    move-result-object v1

    invoke-interface {v1, p2, p3, p4}, Lleu;->a(Lkfp;ILles;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    if-eqz v0, :cond_0

    .line 88
    new-instance v0, Lkoe;

    .line 89
    invoke-interface {v3}, Llev;->c()I

    move-result v1

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    invoke-virtual {v0, v2}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Llex;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 77
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 87
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_5

    .line 88
    new-instance v0, Lkoe;

    .line 89
    invoke-interface {v3}, Llev;->c()I

    move-result v3

    invoke-direct {v0, v3}, Lkoe;-><init>(I)V

    invoke-virtual {v0, v2}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v2, p0, Llex;->d:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lkoe;->a(Landroid/content/Context;)V

    :cond_5
    throw v1
.end method

.method public a(Lkfp;ILles;)V
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Llex;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llev;

    .line 107
    invoke-interface {v0}, Llev;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    invoke-interface {v0}, Llev;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Llex;->a(Ljava/lang/String;Lkfp;ILles;)V

    goto :goto_0

    .line 113
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)Llev;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Llex;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llev;

    return-object v0
.end method

.method public b(Lkfp;ILles;)V
    .locals 8

    .prologue
    .line 118
    iget-object v0, p0, Llex;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llev;

    .line 120
    invoke-interface {v0, p2}, Llev;->a(I)Ljava/lang/Long;

    move-result-object v2

    .line 121
    if-eqz v2, :cond_0

    .line 122
    invoke-interface {v0}, Llev;->a()Ljava/lang/String;

    .line 126
    invoke-direct {p0, p2, v0}, Llex;->a(ILlev;)J

    move-result-wide v4

    .line 127
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 128
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-wide v6, Llex;->a:J

    sub-long/2addr v2, v6

    cmp-long v2, v4, v2

    if-lez v2, :cond_0

    .line 129
    invoke-virtual {p1}, Lkfp;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 137
    :cond_1
    return-void

    .line 132
    :cond_2
    invoke-interface {v0}, Llev;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, p1, p2, p3}, Llex;->a(Ljava/lang/String;Lkfp;ILles;)V

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Llex;->d:Landroid/content/Context;

    invoke-static {v4, p2}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "synclet_name"

    invoke-interface {v0}, Llev;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "last_sync"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "synclet_status"

    const/4 v2, 0x0

    const/4 v3, 0x5

    invoke-virtual {v4, v0, v2, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_0
.end method
