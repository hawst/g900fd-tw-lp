.class public final Lmje;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyn;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2509
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2512
    iput-object v0, p0, Lmje;->apiHeader:Llyq;

    .line 2515
    iput-object v0, p0, Lmje;->a:Lmyn;

    .line 2509
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2532
    const/4 v0, 0x0

    .line 2533
    iget-object v1, p0, Lmje;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 2534
    const/4 v0, 0x1

    iget-object v1, p0, Lmje;->apiHeader:Llyq;

    .line 2535
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2537
    :cond_0
    iget-object v1, p0, Lmje;->a:Lmyn;

    if-eqz v1, :cond_1

    .line 2538
    const/4 v1, 0x2

    iget-object v2, p0, Lmje;->a:Lmyn;

    .line 2539
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2541
    :cond_1
    iget-object v1, p0, Lmje;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2542
    iput v0, p0, Lmje;->ai:I

    .line 2543
    return v0
.end method

.method public a(Loxn;)Lmje;
    .locals 2

    .prologue
    .line 2551
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2552
    sparse-switch v0, :sswitch_data_0

    .line 2556
    iget-object v1, p0, Lmje;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2557
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmje;->ah:Ljava/util/List;

    .line 2560
    :cond_1
    iget-object v1, p0, Lmje;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2562
    :sswitch_0
    return-object p0

    .line 2567
    :sswitch_1
    iget-object v0, p0, Lmje;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 2568
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmje;->apiHeader:Llyq;

    .line 2570
    :cond_2
    iget-object v0, p0, Lmje;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2574
    :sswitch_2
    iget-object v0, p0, Lmje;->a:Lmyn;

    if-nez v0, :cond_3

    .line 2575
    new-instance v0, Lmyn;

    invoke-direct {v0}, Lmyn;-><init>()V

    iput-object v0, p0, Lmje;->a:Lmyn;

    .line 2577
    :cond_3
    iget-object v0, p0, Lmje;->a:Lmyn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2552
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2520
    iget-object v0, p0, Lmje;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 2521
    const/4 v0, 0x1

    iget-object v1, p0, Lmje;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2523
    :cond_0
    iget-object v0, p0, Lmje;->a:Lmyn;

    if-eqz v0, :cond_1

    .line 2524
    const/4 v0, 0x2

    iget-object v1, p0, Lmje;->a:Lmyn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2526
    :cond_1
    iget-object v0, p0, Lmje;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2528
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2505
    invoke-virtual {p0, p1}, Lmje;->a(Loxn;)Lmje;

    move-result-object v0

    return-object v0
.end method
