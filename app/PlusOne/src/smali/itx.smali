.class public final Litx;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmbq;",
        "Lmbr;",
        ">;"
    }
.end annotation


# instance fields
.field private a:[Lnhm;

.field private b:I

.field private c:Z

.field private p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIZ)V
    .locals 6

    .prologue
    .line 31
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v3, "getdevicelocations"

    new-instance v4, Lmbq;

    invoke-direct {v4}, Lmbq;-><init>()V

    new-instance v5, Lmbr;

    invoke-direct {v5}, Lmbr;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Litx;->p:I

    .line 33
    iput p3, p0, Litx;->b:I

    .line 34
    iput-boolean p4, p0, Litx;->c:Z

    .line 35
    return-void
.end method


# virtual methods
.method protected a(Lmbq;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 39
    iget-object v1, p1, Lmbq;->apiHeader:Llyq;

    if-nez v1, :cond_0

    .line 40
    new-instance v1, Llyq;

    invoke-direct {v1}, Llyq;-><init>()V

    iput-object v1, p1, Lmbq;->apiHeader:Llyq;

    .line 42
    :cond_0
    iget-object v1, p1, Lmbq;->apiHeader:Llyq;

    iget-object v1, v1, Llyq;->b:Lpyu;

    if-nez v1, :cond_1

    .line 43
    iget-object v1, p1, Lmbq;->apiHeader:Llyq;

    new-instance v2, Lpyu;

    invoke-direct {v2}, Lpyu;-><init>()V

    iput-object v2, v1, Llyq;->b:Lpyu;

    .line 45
    :cond_1
    iget-object v1, p1, Lmbq;->apiHeader:Llyq;

    iget-object v1, v1, Llyq;->b:Lpyu;

    .line 46
    new-instance v2, Lpyv;

    invoke-direct {v2}, Lpyv;-><init>()V

    iput-object v2, v1, Lpyu;->g:Lpyv;

    .line 47
    iget-object v1, v1, Lpyu;->g:Lpyv;

    iget v2, p0, Litx;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lpyv;->a:Ljava/lang/Integer;

    .line 49
    new-instance v1, Lnho;

    invoke-direct {v1}, Lnho;-><init>()V

    iput-object v1, p1, Lmbq;->a:Lnho;

    .line 50
    iget-object v1, p1, Lmbq;->a:Lnho;

    iput v0, v1, Lnho;->a:I

    .line 51
    iget-object v1, p1, Lmbq;->a:Lnho;

    new-instance v2, Lnhj;

    invoke-direct {v2}, Lnhj;-><init>()V

    iput-object v2, v1, Lnho;->b:Lnhj;

    .line 52
    iget-object v1, p1, Lmbq;->a:Lnho;

    iget-object v1, v1, Lnho;->b:Lnhj;

    iget-boolean v2, p0, Litx;->c:Z

    if-eqz v2, :cond_2

    :goto_0
    iput v0, v1, Lnhj;->a:I

    .line 54
    return-void

    .line 52
    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method protected a(Lmbr;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p1, Lmbr;->a:Lnhr;

    iget-object v0, v0, Lnhr;->a:[Lnhm;

    iput-object v0, p0, Litx;->a:[Lnhm;

    .line 59
    iget-object v0, p1, Lmbr;->a:Lnhr;

    iget-object v0, v0, Lnhr;->b:Lnhk;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p1, Lmbr;->a:Lnhr;

    iget-object v0, v0, Lnhr;->b:Lnhk;

    iget-object v0, v0, Lnhk;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Litx;->p:I

    .line 62
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmbq;

    invoke-virtual {p0, p1}, Litx;->a(Lmbq;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmbr;

    invoke-virtual {p0, p1}, Litx;->a(Lmbr;)V

    return-void
.end method

.method public i()[Lnhm;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Litx;->a:[Lnhm;

    return-object v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Litx;->p:I

    return v0
.end method
