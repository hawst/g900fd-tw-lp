.class public Lajx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajv;


# instance fields
.field private c:Lbvy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbvy",
            "<",
            "Laii;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lajx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbvy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbvy",
            "<",
            "Laii;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lajx;->c:Lbvy;

    .line 27
    return-void
.end method

.method private static a(Landroid/net/Uri;Lahy;)Lail;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lajy;

    invoke-direct {v0, p1, p0}, Lajy;-><init>(Lahy;Landroid/net/Uri;)V

    return-object v0
.end method

.method private static a(Lahy;Landroid/net/Uri;Laic;Lbkr;)V
    .locals 1

    .prologue
    .line 140
    invoke-interface {p3}, Lbkr;->c()Lbkn;

    move-result-object v0

    invoke-virtual {v0}, Lbkn;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    invoke-interface {p0, p1, p2, p3}, Lahy;->a(Landroid/net/Uri;Laic;Lbkr;)V

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-interface {p0, p1, p2, p3}, Lahy;->b(Landroid/net/Uri;Laic;Lbkr;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 100
    :try_start_0
    iget-object v0, p0, Lajx;->c:Lbvy;

    invoke-virtual {v0}, Lbvy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laii;

    .line 106
    if-eqz v0, :cond_0

    .line 107
    invoke-interface {v0}, Laii;->a()V
    :try_end_0
    .catch Lbwa; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;Laic;Lahy;)V
    .locals 2

    .prologue
    .line 33
    :try_start_0
    iget-object v0, p2, Laic;->a:Laib;

    iget-boolean v0, v0, Laib;->f:Z

    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lajx;->a()V

    .line 36
    :cond_0
    invoke-static {p1, p3}, Lajx;->a(Landroid/net/Uri;Lahy;)Lail;

    move-result-object v1

    .line 37
    iget-object v0, p0, Lajx;->c:Lbvy;

    .line 38
    invoke-virtual {v0}, Lbvy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laii;

    invoke-interface {v0, p1, p2, v1}, Laii;->a(Landroid/net/Uri;Laic;Lail;)Lbkr;

    move-result-object v0

    .line 39
    invoke-static {p3, p1, p2, v0}, Lajx;->a(Lahy;Landroid/net/Uri;Laic;Lbkr;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbwa; {:try_start_0 .. :try_end_0} :catch_2

    .line 50
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    invoke-interface {p3, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    goto :goto_0

    .line 42
    :catch_1
    move-exception v0

    .line 43
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    invoke-interface {p3, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    goto :goto_0

    .line 44
    :catch_2
    move-exception v0

    .line 48
    invoke-interface {p3, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;Laic;Lbkr;Lahy;)V
    .locals 2

    .prologue
    .line 57
    :try_start_0
    iget-object v0, p2, Laic;->a:Laib;

    iget-boolean v0, v0, Laib;->f:Z

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lajx;->a()V

    .line 60
    :cond_0
    invoke-static {p1, p4}, Lajx;->a(Landroid/net/Uri;Lahy;)Lail;

    move-result-object v1

    .line 61
    iget-object v0, p0, Lajx;->c:Lbvy;

    .line 62
    invoke-virtual {v0}, Lbvy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laii;

    invoke-interface {v0, p1, p3, p2, v1}, Laii;->a(Landroid/net/Uri;Lbkr;Laic;Lail;)Lbkr;

    move-result-object v0

    .line 63
    invoke-static {p4, p1, p2, v0}, Lajx;->a(Lahy;Landroid/net/Uri;Laic;Lbkr;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbwa; {:try_start_0 .. :try_end_0} :catch_2

    .line 74
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    invoke-interface {p4, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    goto :goto_0

    .line 66
    :catch_1
    move-exception v0

    .line 67
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    invoke-interface {p4, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    goto :goto_0

    .line 68
    :catch_2
    move-exception v0

    .line 72
    invoke-interface {p4, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public b(Landroid/net/Uri;Laic;Lahy;)V
    .locals 1

    .prologue
    .line 80
    :try_start_0
    iget-object v0, p2, Laic;->a:Laib;

    iget-boolean v0, v0, Laib;->f:Z

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lajx;->a()V

    .line 83
    :cond_0
    iget-object v0, p0, Lajx;->c:Lbvy;

    invoke-virtual {v0}, Lbvy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laii;

    invoke-interface {v0, p1, p2}, Laii;->a(Landroid/net/Uri;Laic;)Lbkr;

    move-result-object v0

    .line 84
    invoke-static {p3, p1, p2, v0}, Lajx;->a(Lahy;Landroid/net/Uri;Laic;Lbkr;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbwa; {:try_start_0 .. :try_end_0} :catch_2

    .line 95
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    invoke-interface {p3, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    goto :goto_0

    .line 87
    :catch_1
    move-exception v0

    .line 88
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    invoke-interface {p3, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    goto :goto_0

    .line 89
    :catch_2
    move-exception v0

    .line 93
    invoke-interface {p3, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    goto :goto_0
.end method
