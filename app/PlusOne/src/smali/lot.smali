.class public final Llot;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqk;
.implements Llre;
.implements Llrf;
.implements Llrg;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llov;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llot;->a:Ljava/util/List;

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Llot;->d:Z

    .line 38
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 39
    return-void
.end method

.method constructor <init>(Llqr;B)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llot;->a:Ljava/util/List;

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Llot;->d:Z

    .line 42
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 43
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 84
    iget-boolean v0, p0, Llot;->b:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Llot;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 85
    :goto_0
    iget-boolean v0, p0, Llot;->c:Z

    if-ne v1, v0, :cond_2

    .line 91
    :cond_0
    return-void

    .line 84
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 89
    :cond_2
    iput-boolean v1, p0, Llot;->c:Z

    .line 90
    iget-object v0, p0, Llot;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llov;

    invoke-interface {v0, v1}, Llov;->a(Z)V

    goto :goto_1
.end method


# virtual methods
.method public a(Llov;)Llov;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Llov;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Llot;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    return-object p1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Llot;->b:Z

    .line 65
    invoke-direct {p0}, Llot;->c()V

    .line 66
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 58
    iput-boolean p1, p0, Llot;->d:Z

    .line 59
    invoke-direct {p0}, Llot;->c()V

    .line 60
    return-void
.end method

.method public aP_()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Llot;->b:Z

    .line 71
    invoke-direct {p0}, Llot;->c()V

    .line 72
    return-void
.end method
