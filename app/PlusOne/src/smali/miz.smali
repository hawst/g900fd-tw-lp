.class public final Lmiz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnnu;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12634
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12637
    iput-object v0, p0, Lmiz;->apiHeader:Llyr;

    .line 12640
    iput-object v0, p0, Lmiz;->a:Lnnu;

    .line 12634
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12657
    const/4 v0, 0x0

    .line 12658
    iget-object v1, p0, Lmiz;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 12659
    const/4 v0, 0x1

    iget-object v1, p0, Lmiz;->apiHeader:Llyr;

    .line 12660
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12662
    :cond_0
    iget-object v1, p0, Lmiz;->a:Lnnu;

    if-eqz v1, :cond_1

    .line 12663
    const/4 v1, 0x2

    iget-object v2, p0, Lmiz;->a:Lnnu;

    .line 12664
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12666
    :cond_1
    iget-object v1, p0, Lmiz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12667
    iput v0, p0, Lmiz;->ai:I

    .line 12668
    return v0
.end method

.method public a(Loxn;)Lmiz;
    .locals 2

    .prologue
    .line 12676
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12677
    sparse-switch v0, :sswitch_data_0

    .line 12681
    iget-object v1, p0, Lmiz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12682
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmiz;->ah:Ljava/util/List;

    .line 12685
    :cond_1
    iget-object v1, p0, Lmiz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12687
    :sswitch_0
    return-object p0

    .line 12692
    :sswitch_1
    iget-object v0, p0, Lmiz;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 12693
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmiz;->apiHeader:Llyr;

    .line 12695
    :cond_2
    iget-object v0, p0, Lmiz;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12699
    :sswitch_2
    iget-object v0, p0, Lmiz;->a:Lnnu;

    if-nez v0, :cond_3

    .line 12700
    new-instance v0, Lnnu;

    invoke-direct {v0}, Lnnu;-><init>()V

    iput-object v0, p0, Lmiz;->a:Lnnu;

    .line 12702
    :cond_3
    iget-object v0, p0, Lmiz;->a:Lnnu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12677
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12645
    iget-object v0, p0, Lmiz;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 12646
    const/4 v0, 0x1

    iget-object v1, p0, Lmiz;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12648
    :cond_0
    iget-object v0, p0, Lmiz;->a:Lnnu;

    if-eqz v0, :cond_1

    .line 12649
    const/4 v0, 0x2

    iget-object v1, p0, Lmiz;->a:Lnnu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12651
    :cond_1
    iget-object v0, p0, Lmiz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12653
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12630
    invoke-virtual {p0, p1}, Lmiz;->a(Loxn;)Lmiz;

    move-result-object v0

    return-object v0
.end method
