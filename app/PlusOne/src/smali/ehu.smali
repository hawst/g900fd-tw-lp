.class public final Lehu;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Ldid;
.implements Ldys;
.implements Lhjj;
.implements Lhmm;
.implements Lhmq;
.implements Lhob;
.implements Lhsw;
.implements Lkvs;
.implements Lkxx;
.implements Lkxz;
.implements Llgs;
.implements Llgv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ldid;",
        "Ldys;",
        "Lhjj;",
        "Lhmm;",
        "Lhmq;",
        "Lhob;",
        "Lhsw;",
        "Lkvs;",
        "Lkxx;",
        "Lkxz;",
        "Llgs;",
        "Llgv;"
    }
.end annotation


# static fields
.field private static final N:[Ljava/lang/String;

.field private static final O:[Ljava/lang/String;

.field private static final bx:Landroid/animation/TimeInterpolator;

.field private static by:Landroid/animation/TimeInterpolator;

.field private static bz:Landroid/graphics/drawable/Drawable;


# instance fields
.field private final P:Lhje;

.field private final Q:Lein;

.field private R:Lhee;

.field private S:Llhd;

.field private T:Liwk;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:Lfdr;

.field private Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

.field private Z:Z

.field private aA:Z

.field private aB:Ljava/lang/String;

.field private aC:Ljava/lang/String;

.field private aD:Ljava/lang/String;

.field private aE:Llah;

.field private aF:I

.field private aG:I

.field private aH:Lkzu;

.field private aI:Z

.field private aJ:Z

.field private aK:Z

.field private aL:Z

.field private aM:Z

.field private aN:Z

.field private aO:Z

.field private aP:Z

.field private aQ:Z

.field private aR:Lhgw;

.field private aS:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aT:Landroid/view/View;

.field private aU:Lcom/google/android/libraries/social/squares/membership/JoinButton;

.field private aV:Lkvq;

.field private aW:Landroid/view/View;

.field private aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

.field private aY:Landroid/view/View;

.field private aZ:Z

.field private aa:Z

.field private ab:Ldib;

.field private ac:Lfdp;

.field private ad:Lldh;

.field private ae:Ljava/lang/Integer;

.field private af:I

.field private ag:Z

.field private ah:Z

.field private ai:Ljava/lang/String;

.field private aj:Z

.field private ak:I

.field private al:I

.field private am:Z

.field private an:I

.field private ao:Lkzp;

.field private ap:Lkzw;

.field private aq:Leij;

.field private ar:Z

.field private as:Z

.field private aw:Llae;

.field private ax:Llai;

.field private ay:[Z

.field private az:Landroid/text/Spanned;

.field private bA:I

.field private bB:Leil;

.field private bC:I

.field private bD:I

.field private bE:I

.field private bF:Ljava/lang/String;

.field private bG:Ljava/lang/String;

.field private bH:Lfcu;

.field private bI:Ljava/lang/String;

.field private bJ:Lhoc;

.field private bK:Ljxq;

.field private bL:Ljxs;

.field private bM:Lhjz;

.field private bN:Llep;

.field private bO:Lkuu;

.field private bP:Lkyt;

.field private ba:Landroid/text/TextWatcher;

.field private bb:I

.field private bc:Z

.field private bd:Llcr;

.field private be:I

.field private bf:Z

.field private bg:[B

.field private bh:Z

.field private bi:Landroid/database/Cursor;

.field private bj:Z

.field private bk:I

.field private bl:Leid;

.field private bm:Z

.field private bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

.field private bo:Z

.field private bp:[I

.field private bq:[I

.field private br:I

.field private bs:I

.field private bt:Z

.field private bu:Leim;

.field private bv:Landroid/animation/ObjectAnimator;

.field private bw:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 378
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "circle_id"

    aput-object v1, v0, v2

    const-string v1, "circle_name"

    aput-object v1, v0, v3

    sput-object v0, Lehu;->N:[Ljava/lang/String;

    .line 384
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "is_member"

    aput-object v1, v0, v2

    const-string v1, "membership_status"

    aput-object v1, v0, v3

    const-string v1, "joinability"

    aput-object v1, v0, v4

    sput-object v0, Lehu;->O:[Ljava/lang/String;

    .line 521
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Lehu;->bx:Landroid/animation/TimeInterpolator;

    .line 524
    const/4 v0, 0x0

    sput-object v0, Lehu;->by:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 239
    invoke-direct {p0}, Llol;-><init>()V

    .line 394
    new-instance v0, Lhje;

    iget-object v1, p0, Lehu;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Lehu;->P:Lhje;

    .line 399
    new-instance v0, Lein;

    invoke-direct {v0, p0}, Lein;-><init>(Lehu;)V

    iput-object v0, p0, Lehu;->Q:Lein;

    .line 401
    new-instance v0, Llhd;

    invoke-direct {v0}, Llhd;-><init>()V

    iput-object v0, p0, Lehu;->S:Llhd;

    .line 414
    new-instance v0, Leig;

    invoke-direct {v0, p0}, Leig;-><init>(Lehu;)V

    iput-object v0, p0, Lehu;->ad:Lldh;

    .line 417
    iput v2, p0, Lehu;->af:I

    .line 428
    iput v2, p0, Lehu;->an:I

    .line 479
    const/4 v0, 0x1

    iput v0, p0, Lehu;->bb:I

    .line 506
    iput-boolean v2, p0, Lehu;->bm:Z

    .line 511
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lehu;->bp:[I

    .line 512
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lehu;->bq:[I

    .line 517
    iput-boolean v2, p0, Lehu;->bt:Z

    .line 532
    new-instance v0, Leil;

    invoke-direct {v0, p0}, Leil;-><init>(Lehu;)V

    iput-object v0, p0, Lehu;->bB:Leil;

    .line 534
    const/4 v0, -0x1

    iput v0, p0, Lehu;->bC:I

    .line 552
    new-instance v0, Lkuu;

    iget-object v1, p0, Lehu;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkuu;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lehu;->bO:Lkuu;

    .line 553
    new-instance v0, Lkyt;

    iget-object v1, p0, Lehu;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkyt;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lehu;->bP:Lkyt;

    .line 557
    new-instance v0, Lhnw;

    new-instance v1, Leie;

    invoke-direct {v1, p0}, Leie;-><init>(Lehu;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 559
    new-instance v0, Lhmf;

    iget-object v1, p0, Lehu;->av:Llqm;

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 561
    new-instance v0, Lhsx;

    iget-object v1, p0, Lehu;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lhsx;-><init>(Llqr;Lhsw;)V

    .line 3524
    return-void

    .line 511
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data

    .line 512
    :array_1
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method static synthetic A(Lehu;)Ljxq;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->bK:Ljxq;

    return-object v0
.end method

.method static synthetic B(Lehu;)Lkzw;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->ap:Lkzw;

    return-object v0
.end method

.method static synthetic C(Lehu;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 239
    iget-object v0, p0, Lehu;->ax:Llai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehu;->ax:Llai;

    invoke-virtual {v0}, Llai;->a()I

    move-result v0

    if-eqz v0, :cond_0

    new-array v0, v0, [Z

    iput-object v0, p0, Lehu;->ay:[Z

    iget-object v0, p0, Lehu;->ay:[Z

    invoke-static {v0, v5}, Ljava/util/Arrays;->fill([ZZ)V

    const v0, 0x7f0a0b29

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lehu;->ax:Llai;

    invoke-virtual {v1}, Llai;->b()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lehu;->ay:[Z

    const v3, 0x7f0a0579

    invoke-virtual {p0, v3}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0597

    invoke-virtual {p0, v4}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Llgr;->a(Ljava/lang/String;[Ljava/lang/String;[ZLjava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0, p0, v5}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "edit_tags"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic D(Lehu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->U:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic E(Lehu;)Lfdp;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->ac:Lfdp;

    return-object v0
.end method

.method static synthetic F(Lehu;)Lkzu;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->aH:Lkzu;

    return-object v0
.end method

.method static synthetic G(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic H(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->aM:Z

    return v0
.end method

.method static synthetic I(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->aA:Z

    return v0
.end method

.method static synthetic J(Lehu;)V
    .locals 4

    .prologue
    .line 239
    const-string v0, "extra_activity_id"

    iget-object v1, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->V:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    const v0, 0x7f0a0757

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0770

    invoke-virtual {p0, v1}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0596

    invoke-virtual {p0, v2}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "delete_activity"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic K(Lehu;)V
    .locals 4

    .prologue
    .line 239
    const v0, 0x7f0a0841

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0771

    invoke-virtual {p0, v1}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a07fa

    invoke-virtual {p0, v2}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a07fd

    invoke-virtual {p0, v3}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "remove_activity"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic L(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic M(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic N(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic O(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic P(Lehu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->ai:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic Q(Lehu;)Lkyt;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->bP:Lkyt;

    return-object v0
.end method

.method static synthetic R(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic S(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic T(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic U(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic V(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic W(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic X(Lehu;)V
    .locals 4

    .prologue
    .line 239
    const-string v0, "extra_activity_id"

    iget-object v1, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->ab:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    const v0, 0x7f0a0550

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0775

    invoke-virtual {p0, v1}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0596

    invoke-virtual {p0, v2}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "activity_id"

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "report_activity"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic Y()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 239
    sget-object v0, Lehu;->bz:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic Y(Lehu;)V
    .locals 4

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->bM:Lhjz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehu;->bN:Llep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehu;->bM:Lhjz;

    iget-object v0, p0, Lehu;->bN:Llep;

    iget-object v0, p0, Lehu;->U:Ljava/lang/String;

    iget-boolean v0, p0, Lehu;->aJ:Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "extra_activity_id"

    iget-object v1, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->T:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    iget-boolean v0, p0, Lehu;->aJ:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0a075c

    :goto_1
    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v1

    iget-boolean v0, p0, Lehu;->aJ:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0a0774

    :goto_2
    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a0596

    invoke-virtual {p0, v2}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "activity_id"

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "mute_activity"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0a075b

    goto :goto_1

    :cond_2
    const v0, 0x7f0a0773

    goto :goto_2
.end method

.method private Z()V
    .locals 2

    .prologue
    .line 1493
    invoke-virtual {p0}, Lehu;->r()Lu;

    move-result-object v0

    .line 1494
    instance-of v1, v0, Lehh;

    if-eqz v1, :cond_0

    .line 1495
    check-cast v0, Lehh;

    .line 1496
    invoke-virtual {v0}, Lehh;->at()V

    .line 1498
    :cond_0
    return-void
.end method

.method static synthetic Z(Lehu;)V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->aB:Ljava/lang/String;

    const v1, 0x7f0a075f

    invoke-direct {p0, v0, v1}, Lehu;->a(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method static synthetic a(Lehu;)I
    .locals 1

    .prologue
    .line 239
    invoke-direct {p0}, Lehu;->ak()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lehu;I)I
    .locals 0

    .prologue
    .line 239
    iput p1, p0, Lehu;->ak:I

    return p1
.end method

.method static synthetic a(Lehu;Lhgw;)Lhgw;
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lehu;->aR:Lhgw;

    return-object p1
.end method

.method static synthetic a(Lehu;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lehu;->ae:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Lehu;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lehu;->U:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lehu;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lehu;->aS:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lehu;Lkzp;)Lkzp;
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lehu;->ao:Lkzp;

    return-object p1
.end method

.method static synthetic a(Lehu;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lehu;->r()Lu;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lu;->a(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p2}, Lehu;->a(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method static synthetic a(Lehu;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 239
    invoke-direct/range {p0 .. p5}, Lehu;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Lhmv;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2752
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2754
    iget-object v1, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2755
    const-string v1, "extra_activity_id"

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757
    :cond_0
    iget-object v1, p0, Lehu;->ai:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2758
    const-string v1, "extra_square_id"

    iget-object v2, p0, Lehu;->ai:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2760
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2761
    const-string v1, "extra_gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2763
    :cond_2
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    move-object v1, v0

    .line 2764
    :goto_0
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 2765
    invoke-virtual {v2, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 2766
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2764
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2767
    return-void

    :cond_3
    move-object v1, v0

    .line 2763
    goto :goto_0
.end method

.method private a(Ljava/lang/CharSequence;I)V
    .locals 3

    .prologue
    .line 2550
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v0

    invoke-static {v0, p1}, Llsj;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 2551
    invoke-virtual {p0}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2552
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2553
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 3719
    const v0, 0x7f0a0b2f

    .line 3720
    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0b30

    .line 3721
    invoke-virtual {p0, v1}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a07fa

    .line 3722
    invoke-virtual {p0, v2}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a07fd

    .line 3723
    invoke-virtual {p0, v3}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 3719
    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 3725
    invoke-virtual {v0, v4}, Llgr;->b(Z)V

    .line 3726
    invoke-virtual {v0, p0, v4}, Llgr;->a(Lu;I)V

    .line 3727
    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 3728
    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->b()Z

    .line 3729
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2659
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2660
    sget-object v1, Lljd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 2661
    new-instance v1, Lljd;

    invoke-direct {v1, p1}, Lljd;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2662
    iget-object v1, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Landroid/text/SpannableStringBuilder;)V

    .line 2663
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 2664
    iget-object v1, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1, v0, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setSelection(II)V

    .line 2666
    invoke-direct {p0}, Lehu;->ad()V

    .line 2667
    return-void
.end method

.method private a(Ljava/lang/String;ZI)V
    .locals 7

    .prologue
    .line 2642
    const-string v0, "extra_comment_id"

    .line 2643
    invoke-static {v0, p1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 2644
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->ab:Lhmv;

    .line 2645
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 2646
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2644
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2648
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lehu;->d(I)V

    .line 2649
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v1, p0, Lehu;->R:Lhee;

    .line 2650
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    const/4 v4, 0x1

    move-object v3, p1

    move v5, p2

    move v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZZI)I

    move-result v0

    .line 2649
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    .line 2652
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 2027
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v1, p0, Lehu;->bi:Landroid/database/Cursor;

    invoke-static {v0, v1, v8}, Ldsm;->a(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v2

    .line 2031
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2032
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2033
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2034
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2038
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2039
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v7

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2040
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v1, v8

    .line 2043
    :cond_3
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2044
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2048
    :cond_4
    iget-boolean v0, p0, Lehu;->bh:Z

    if-eqz v0, :cond_c

    if-eqz v1, :cond_c

    .line 2049
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    move v2, v8

    .line 2050
    :goto_2
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_5
    move v1, v8

    .line 2051
    :goto_3
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_6
    move v0, v8

    .line 2052
    :goto_4
    new-instance v3, Ldpj;

    iget-object v6, p0, Lehu;->at:Llnl;

    invoke-direct {v3, v6}, Ldpj;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lehu;->R:Lhee;

    .line 2054
    invoke-interface {v6}, Lhee;->d()I

    move-result v6

    invoke-virtual {v3, v6}, Ldpj;->a(I)Ldpj;

    move-result-object v3

    .line 2055
    invoke-virtual {v3, p3}, Ldpj;->a(Ljava/lang/String;)Ldpj;

    move-result-object v3

    .line 2056
    invoke-virtual {v3, p4}, Ldpj;->b(Ljava/lang/String;)Ldpj;

    move-result-object v3

    .line 2057
    invoke-virtual {p0}, Lehu;->a()I

    move-result v6

    invoke-virtual {v3, v6}, Ldpj;->b(I)Ldpj;

    move-result-object v3

    .line 2058
    invoke-virtual {v3, v4}, Ldpj;->a(Ljava/util/ArrayList;)Ldpj;

    move-result-object v3

    .line 2059
    invoke-virtual {v3, v5}, Ldpj;->b(Ljava/util/ArrayList;)Ldpj;

    move-result-object v3

    .line 2060
    invoke-virtual {v3, v1}, Ldpj;->a(Z)Ldpj;

    move-result-object v1

    .line 2061
    invoke-virtual {v1, v0}, Ldpj;->b(Z)Ldpj;

    move-result-object v0

    .line 2062
    invoke-virtual {v0, v7}, Ldpj;->c(Z)Ldpj;

    move-result-object v0

    .line 2063
    iget-object v1, p0, Lehu;->at:Llnl;

    invoke-static {v1, v8}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    .line 2064
    if-eqz p5, :cond_7

    .line 2065
    const v3, 0x7f0a09d4

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v1, v6, v7

    .line 2067
    invoke-virtual {p0, v3, v6}, Lehu;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2066
    invoke-virtual {v0, v3}, Ldpj;->d(Ljava/lang/String;)Ldpj;

    move-result-object v3

    const v6, 0x7f0a09d5

    .line 2069
    invoke-virtual {p0, v6}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v6

    .line 2068
    invoke-virtual {v3, v6}, Ldpj;->e(Ljava/lang/String;)Ldpj;

    .line 2071
    :cond_7
    if-eqz v2, :cond_8

    .line 2072
    iget-object v2, p0, Lehu;->at:Llnl;

    const v3, 0x7f0a0525

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v1, v6, v7

    .line 2073
    invoke-virtual {v2, v3, v6}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2072
    invoke-virtual {v0, v1}, Ldpj;->e(Ljava/lang/String;)Ldpj;

    .line 2075
    :cond_8
    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v1

    invoke-virtual {v0}, Ldpj;->a()Ldpi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 2077
    new-instance v0, Ldib;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v2, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhms;

    move-object v3, p3

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    iput-object v0, p0, Lehu;->ab:Ldib;

    .line 2080
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    move v1, v8

    .line 2081
    :goto_5
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    move v2, v8

    .line 2082
    :goto_6
    invoke-static {p3}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2083
    if-eqz v1, :cond_11

    .line 2084
    iget-object v3, p0, Lehu;->at:Llnl;

    iget-object v4, p0, Lehu;->R:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-static {v3, v4, v0, v7}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Z)I

    .line 2088
    :cond_9
    :goto_7
    iget-object v0, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    if-eqz v0, :cond_b

    .line 2089
    iget-object v0, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_8
    if-ltz v3, :cond_b

    .line 2090
    iget-object v0, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2091
    instance-of v4, v0, Lgbz;

    if-eqz v4, :cond_14

    .line 2092
    check-cast v0, Lgbz;

    .line 2093
    invoke-virtual {v0}, Lgbz;->s()Lkzp;

    move-result-object v3

    if-eqz v3, :cond_12

    .line 2094
    invoke-virtual {v0}, Lgbz;->s()Lkzp;

    move-result-object v3

    invoke-virtual {v3}, Lkzp;->n()Llac;

    move-result-object v3

    if-eqz v3, :cond_12

    move v3, v8

    .line 2095
    :goto_9
    if-eqz v1, :cond_13

    .line 2096
    invoke-virtual {v0}, Lgbz;->i()V

    .line 2097
    if-eqz v3, :cond_a

    .line 2098
    new-instance v1, Ldpk;

    iget-object v2, p0, Lehu;->at:Llnl;

    iget-object v3, p0, Lehu;->R:Lhee;

    .line 2099
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 2100
    invoke-virtual {v0}, Lgbz;->C()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v8}, Ldpk;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 2101
    iget-object v2, p0, Lehu;->bJ:Lhoc;

    invoke-virtual {v2, v1}, Lhoc;->b(Lhny;)V

    .line 2113
    :cond_a
    :goto_a
    invoke-virtual {v0}, Lgbz;->k()V

    .line 2118
    :cond_b
    return-void

    :cond_c
    move v2, v7

    .line 2049
    goto/16 :goto_2

    :cond_d
    move v1, v7

    .line 2050
    goto/16 :goto_3

    :cond_e
    move v0, v7

    .line 2051
    goto/16 :goto_4

    :cond_f
    move v1, v7

    .line 2080
    goto :goto_5

    :cond_10
    move v2, v7

    .line 2081
    goto :goto_6

    .line 2085
    :cond_11
    if-eqz v2, :cond_9

    .line 2086
    iget-object v3, p0, Lehu;->at:Llnl;

    iget-object v4, p0, Lehu;->R:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-static {v3, v4, v0, v8}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Z)I

    goto :goto_7

    :cond_12
    move v3, v7

    .line 2094
    goto :goto_9

    .line 2104
    :cond_13
    if-eqz v2, :cond_a

    .line 2105
    invoke-virtual {v0}, Lgbz;->j()V

    .line 2106
    if-eqz v3, :cond_a

    .line 2107
    new-instance v1, Ldpk;

    iget-object v2, p0, Lehu;->at:Llnl;

    iget-object v3, p0, Lehu;->R:Lhee;

    .line 2108
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 2109
    invoke-virtual {v0}, Lgbz;->C()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v7}, Ldpk;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 2110
    iget-object v2, p0, Lehu;->bJ:Lhoc;

    invoke-virtual {v2, v1}, Lhoc;->b(Lhny;)V

    goto :goto_a

    .line 2089
    :cond_14
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_8
.end method

.method static synthetic a(Lehu;Z)Z
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lehu;->as:Z

    return p1
.end method

.method static synthetic a(Lehu;[B)[B
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lehu;->bg:[B

    return-object p1
.end method

.method static synthetic aA(Lehu;)I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lehu;->af:I

    return v0
.end method

.method static synthetic aB(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic aC(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic aD(Lehu;)Llnh;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->au:Llnh;

    return-object v0
.end method

.method static synthetic aE(Lehu;)V
    .locals 4

    .prologue
    .line 239
    new-instance v0, Lemz;

    invoke-direct {v0}, Lemz;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account_id"

    iget-object v3, p0, Lehu;->R:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "activity_id"

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lemz;->f(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "engagement"

    invoke-virtual {v0, v1, v2}, Lemz;->a(Lae;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic aF(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic aG(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic aH(Lehu;)Llnh;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->au:Llnh;

    return-object v0
.end method

.method static synthetic aI(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic aJ(Lehu;)Liwk;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->T:Liwk;

    return-object v0
.end method

.method static synthetic aK(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic aL(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->bh:Z

    return v0
.end method

.method static synthetic aM(Lehu;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->bi:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic aN(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic aO(Lehu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->aC:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic aP(Lehu;)Lfcu;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->bH:Lfcu;

    return-object v0
.end method

.method static synthetic aQ(Lehu;)Llah;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->aE:Llah;

    return-object v0
.end method

.method static synthetic aR(Lehu;)I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lehu;->aG:I

    return v0
.end method

.method private aa()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1501
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lehu;->ba:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1502
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1503
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1504
    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v0

    .line 1505
    if-eqz v0, :cond_0

    .line 1506
    invoke-virtual {v0}, Lae;->c()V

    .line 1508
    :cond_0
    return-void
.end method

.method static synthetic aa(Lehu;)V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->az:Landroid/text/Spanned;

    const v1, 0x7f0a0761

    invoke-direct {p0, v0, v1}, Lehu;->a(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private ab()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 1738
    iget-object v0, p0, Lehu;->aW:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1739
    iget-object v0, p0, Lehu;->aT:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1741
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-boolean v2, p0, Lehu;->aN:Z

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    .line 1742
    iget-object v2, p0, Lehu;->aY:Landroid/view/View;

    iget-boolean v0, p0, Lehu;->aN:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 1744
    const/4 v0, 0x0

    .line 1745
    iget-boolean v2, p0, Lehu;->aN:Z

    if-eqz v2, :cond_1

    .line 1746
    invoke-direct {p0}, Lehu;->ag()V

    .line 1747
    iget-object v0, p0, Lehu;->aH:Lkzu;

    .line 1748
    const v0, 0x7f0a077b

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 1766
    :goto_1
    iget-object v1, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1767
    return-void

    :cond_0
    move v0, v1

    .line 1742
    goto :goto_0

    .line 1753
    :cond_1
    iget-boolean v2, p0, Lehu;->aj:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lehu;->am:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lehu;->ai:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 1754
    iget-object v2, p0, Lehu;->aV:Lkvq;

    if-nez v2, :cond_2

    .line 1755
    new-instance v2, Lkvq;

    iget-object v3, p0, Lehu;->at:Llnl;

    iget-object v4, p0, Lehu;->R:Lhee;

    .line 1756
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v2, v3, p0, v4}, Lkvq;-><init>(Landroid/content/Context;Lu;I)V

    iput-object v2, p0, Lehu;->aV:Lkvq;

    .line 1758
    :cond_2
    iget-object v2, p0, Lehu;->aU:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    iget-object v3, p0, Lehu;->ai:Ljava/lang/String;

    iget v4, p0, Lehu;->ak:I

    iget v5, p0, Lehu;->al:I

    .line 1759
    invoke-static {v4, v5}, Lkvu;->a(II)I

    move-result v4

    .line 1758
    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a(Ljava/lang/String;I)V

    .line 1760
    iget-object v2, p0, Lehu;->aV:Lkvq;

    iget-object v3, p0, Lehu;->aU:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    invoke-virtual {v2, v3}, Lkvq;->a(Lkvv;)V

    .line 1761
    iget-object v2, p0, Lehu;->aW:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1762
    iget-object v2, p0, Lehu;->aT:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1764
    :cond_3
    const v0, 0x7f0a077d

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic ab(Lehu;)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Lehu;->ae()V

    return-void
.end method

.method private ac()V
    .locals 7

    .prologue
    .line 1980
    const/4 v0, 0x0

    iput-boolean v0, p0, Lehu;->Z:Z

    .line 1981
    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v6

    .line 1982
    const-string v0, "GetActivityTask"

    invoke-virtual {v6, v0}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1983
    new-instance v0, Ldox;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    iget-object v4, p0, Lehu;->ai:Ljava/lang/String;

    iget v5, p0, Lehu;->ak:I

    .line 1984
    invoke-static {v5}, Lkto;->a(I)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Ldox;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 1983
    invoke-virtual {v6, v0}, Lhoc;->b(Lhny;)V

    .line 1986
    :cond_0
    iget-object v0, p0, Lehu;->P:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 1987
    return-void
.end method

.method static synthetic ac(Lehu;)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Lehu;->af()V

    return-void
.end method

.method private ad()V
    .locals 1

    .prologue
    .line 2164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehu;->aZ:Z

    .line 2165
    invoke-direct {p0}, Lehu;->ag()V

    .line 2166
    return-void
.end method

.method static synthetic ad(Lehu;)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Lehu;->ac()V

    return-void
.end method

.method private ae()V
    .locals 3

    .prologue
    .line 2598
    iget-object v0, p0, Lehu;->X:Lfdr;

    invoke-virtual {v0}, Lfdr;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2599
    iget-object v0, p0, Lehu;->X:Lfdr;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfdr;->b(Z)V

    .line 2600
    iget-object v0, p0, Lehu;->P:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 2606
    :goto_0
    return-void

    .line 2602
    :cond_0
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v1, p0, Lehu;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->c(Landroid/content/Context;ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    .line 2604
    const/16 v0, 0x41

    invoke-direct {p0, v0}, Lehu;->d(I)V

    goto :goto_0
.end method

.method static synthetic ae(Lehu;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 239
    const/4 v0, 0x0

    const v1, 0x7f0a0b21

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lehu;->W:Ljava/lang/String;

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lehu;->W:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lehu;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0596

    invoke-virtual {p0, v2}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0, p0, v5}, Llgr;->a(Lu;I)V

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "activity_id"

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "reject_inferred_post"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic af(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method private af()V
    .locals 2

    .prologue
    .line 2609
    iget-object v0, p0, Lehu;->X:Lfdr;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfdr;->b(Z)V

    .line 2610
    iget-object v0, p0, Lehu;->P:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 2611
    return-void
.end method

.method static synthetic ag(Lehu;)Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method private ag()V
    .locals 4

    .prologue
    .line 2815
    iget-boolean v0, p0, Lehu;->aZ:Z

    if-nez v0, :cond_0

    .line 2833
    :goto_0
    return-void

    .line 2818
    :cond_0
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    new-instance v1, Leic;

    invoke-direct {v1, p0}, Leic;-><init>(Lehu;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private ah()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2836
    iput-boolean v7, p0, Lehu;->ar:Z

    .line 2837
    invoke-virtual {p0}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 2838
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v1

    const-string v2, "post_restrictions"

    const-string v3, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v1, v2, v3}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2839
    const v2, 0x7f0a098a

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lehu;->R:Lhee;

    .line 2841
    invoke-interface {v4}, Lhee;->g()Lhej;

    move-result-object v4

    const-string v5, "domain_name"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v1, v3, v7

    .line 2840
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2842
    const v2, 0x7f0a098b

    .line 2843
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0583

    .line 2844
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    .line 2842
    invoke-static {v2, v1, v0, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 2845
    invoke-virtual {v0, v6}, Llgr;->b(Z)V

    .line 2846
    invoke-virtual {v0, p0, v6}, Llgr;->a(Lu;I)V

    .line 2847
    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "public_dasher_warning"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 2848
    return-void
.end method

.method static synthetic ah(Lehu;)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Lehu;->ab()V

    return-void
.end method

.method static synthetic ai(Lehu;)Landroid/view/View;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->aY:Landroid/view/View;

    return-object v0
.end method

.method private ai()Z
    .locals 2

    .prologue
    .line 2851
    iget-boolean v0, p0, Lehu;->aN:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lehu;->ar:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lehu;->R:Lhee;

    .line 2853
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_default_restricted"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lehu;->aO:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lehu;->R:Lhee;

    .line 2856
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "domain_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2855
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aj()I
    .locals 2

    .prologue
    .line 3461
    iget v0, p0, Lehu;->be:I

    iget-object v1, p0, Lehu;->bd:Llcr;

    iget v1, v1, Llcr;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 3462
    iget-object v1, p0, Lehu;->bd:Llcr;

    invoke-virtual {v1, v0}, Llcr;->a(I)I

    move-result v0

    return v0
.end method

.method static synthetic aj(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->bm:Z

    return v0
.end method

.method private ak()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3672
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 3673
    if-ne v1, v0, :cond_0

    .line 3674
    const/4 v0, 0x0

    .line 3676
    :cond_0
    return v0
.end method

.method static synthetic ak(Lehu;)Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    return-object v0
.end method

.method static synthetic al(Lehu;)Leid;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->bl:Leid;

    return-object v0
.end method

.method private al()Lhoc;
    .locals 2

    .prologue
    .line 3732
    iget-object v0, p0, Lehu;->bJ:Lhoc;

    if-nez v0, :cond_0

    .line 3733
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lehu;->bJ:Lhoc;

    .line 3735
    :cond_0
    iget-object v0, p0, Lehu;->bJ:Lhoc;

    return-object v0
.end method

.method static synthetic am(Lehu;)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Lehu;->aa()V

    return-void
.end method

.method static synthetic an(Lehu;)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Lehu;->Z()V

    return-void
.end method

.method static synthetic ao(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic ap(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->aZ:Z

    return v0
.end method

.method static synthetic aq(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    invoke-direct {p0}, Lehu;->ai()Z

    move-result v0

    return v0
.end method

.method static synthetic ar(Lehu;)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Lehu;->ah()V

    return-void
.end method

.method static synthetic as(Lehu;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic at(Lehu;)V
    .locals 2

    .prologue
    .line 239
    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v0

    const-string v1, "pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lt;->a()V

    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method static synthetic au(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic av(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic aw(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic ax(Lehu;)Llnl;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->at:Llnl;

    return-object v0
.end method

.method static synthetic ay(Lehu;)V
    .locals 10

    .prologue
    const v9, 0x7f0a09b0

    const v8, 0x7f0a09ac

    const v7, 0x7f0a04a4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 239
    iget-object v0, p0, Lehu;->aR:Lhgw;

    if-nez v0, :cond_c

    iget-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    if-nez v0, :cond_1

    iget-object v0, p0, Lehu;->R:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lehu;->aj:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lehu;->aP:Z

    if-eqz v1, :cond_2

    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v1, p0, Lehu;->bI:Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-virtual {v0, v8, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_b

    iget-object v1, p0, Lehu;->aD:Ljava/lang/String;

    const v2, 0x7f0a0596

    invoke-virtual {p0, v2}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "audience"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->aD:Ljava/lang/String;

    iget-object v3, p0, Lehu;->V:Ljava/lang/String;

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    iget-object v3, p0, Lehu;->W:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    if-eqz v0, :cond_3

    const v0, 0x7f0a09ae

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v0, v6, [Ljava/lang/Object;

    const-string v3, "extended_circles"

    const-string v4, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v1, v3, v4}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v0, v5

    invoke-virtual {v1, v9, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const v0, 0x7f0a09af

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v3, v2, v5

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const v0, 0x7f0a04a2

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f0a09ab

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v3, v2, v5

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    const v3, 0x7f0a04a3

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    const v0, 0x7f0a09a9

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    const v3, 0x7f0a049f

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    const v0, 0x7f0a09aa

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    const v0, 0x7f0a09ad

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v0, v6, [Ljava/lang/Object;

    const-string v3, "extended_circles"

    const-string v4, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v1, v3, v4}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v0, v5

    invoke-virtual {v1, v9, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const v3, 0x7f0a04a0

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    new-array v0, v6, [Ljava/lang/Object;

    aput-object v2, v0, v5

    invoke-virtual {v1, v8, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v1, p0, Lehu;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lehu;->d(I)V

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lehu;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    const-string v1, "stream1upfrag"

    iget-object v2, p0, Lehu;->aD:Ljava/lang/String;

    iget-object v3, p0, Lehu;->aR:Lhgw;

    invoke-static {v0, v1, v2, v3}, Lfuk;->a(ILjava/lang/String;Ljava/lang/String;Lhgw;)Lekr;

    move-result-object v0

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "audience"

    invoke-virtual {v0, v1, v2}, Lekr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method static synthetic az(Lehu;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->aS:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lehu;I)I
    .locals 0

    .prologue
    .line 239
    iput p1, p0, Lehu;->al:I

    return p1
.end method

.method static synthetic b(Lehu;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lehu;->ai:Ljava/lang/String;

    return-object p1
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 3704
    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v0

    const-string v1, "comment_confirmation_tag"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3713
    :goto_0
    return-void

    .line 3708
    :cond_0
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3709
    invoke-virtual {p0, p1}, Lehu;->a(Z)V

    goto :goto_0

    .line 3712
    :cond_1
    const-string v0, "comment_confirmation_tag"

    invoke-direct {p0, v0}, Lehu;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lehu;Z)Z
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lehu;->aZ:Z

    return p1
.end method

.method static synthetic b(Lehu;)[I
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->bq:[I

    return-object v0
.end method

.method static synthetic c(Lehu;)I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lehu;->bE:I

    return v0
.end method

.method static synthetic c(Lehu;I)I
    .locals 0

    .prologue
    .line 239
    iput p1, p0, Lehu;->bb:I

    return p1
.end method

.method static synthetic c(Lehu;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->ai:Ljava/lang/String;

    iget-object v1, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lkxt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkxt;

    move-result-object v0

    invoke-virtual {p0}, Lehu;->q()Lae;

    move-result-object v1

    const-string v2, "ban_activity_author"

    invoke-virtual {v0, v1, v2}, Lkxt;->a(Lae;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lehu;Z)Z
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lehu;->bc:Z

    return p1
.end method

.method static synthetic d(Lehu;I)I
    .locals 0

    .prologue
    .line 239
    iput p1, p0, Lehu;->be:I

    return p1
.end method

.method static synthetic d(Lehu;)Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    return-object v0
.end method

.method private d(I)V
    .locals 4

    .prologue
    .line 2126
    iput p1, p0, Lehu;->af:I

    .line 2129
    sparse-switch p1, :sswitch_data_0

    .line 2151
    const v0, 0x7f0a058d

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 2156
    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2157
    invoke-static {v1, v0, v2, v3}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lepl;

    move-result-object v0

    .line 2159
    new-instance v1, Leik;

    invoke-direct {v1, p0}, Leik;-><init>(Lehu;)V

    invoke-virtual {v0, v1}, Lepl;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2160
    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 2161
    return-void

    .line 2131
    :sswitch_0
    const v0, 0x7f0a057b

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2136
    :sswitch_1
    const v0, 0x7f0a07ba

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2141
    :sswitch_2
    const v0, 0x7f0a07b8

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2146
    :sswitch_3
    const v0, 0x7f0a07b9

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2129
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_2
        0x21 -> :sswitch_3
        0x30 -> :sswitch_0
        0x41 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic d(Lehu;Z)Z
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lehu;->bf:Z

    return p1
.end method

.method private e(I)I
    .locals 2

    .prologue
    .line 3686
    iget-object v0, p0, Lehu;->bd:Llcr;

    iget v0, v0, Llcr;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 3687
    iget v0, p0, Lehu;->bD:I

    .line 3693
    :goto_0
    return v0

    .line 3690
    :cond_0
    shl-int/lit8 v0, p1, 0x1

    .line 3691
    iget v1, p0, Lehu;->bD:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic e(Lehu;I)I
    .locals 0

    .prologue
    .line 239
    iput p1, p0, Lehu;->bA:I

    return p1
.end method

.method static synthetic e(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->bj:Z

    return v0
.end method

.method static synthetic e(Lehu;Z)Z
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lehu;->bj:Z

    return p1
.end method

.method static synthetic f(Lehu;)I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lehu;->bA:I

    return v0
.end method

.method static synthetic f(Lehu;I)V
    .locals 6

    .prologue
    .line 239
    const-string v0, "extra_activity_id"

    iget-object v1, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->cz:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    :goto_0
    new-instance v0, Lkyd;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehu;->ai:Ljava/lang/String;

    iget-object v4, p0, Lehu;->U:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lkyd;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V

    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    return-void

    :cond_0
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->cA:Lhmv;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method static synthetic f(Lehu;Z)V
    .locals 5

    .prologue
    .line 239
    const-string v0, "extra_activity_id"

    iget-object v1, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz p1, :cond_0

    sget-object v0, Lhmv;->ao:Lhmv;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lehu;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v0, p0, Lehu;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/service/EsService;->c(Landroid/content/Context;ILjava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    if-eqz p1, :cond_2

    const/16 v0, 0x14

    :goto_2
    invoke-direct {p0, v0}, Lehu;->d(I)V

    return-void

    :cond_0
    sget-object v0, Lhmv;->an:Lhmv;

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/16 v0, 0x15

    goto :goto_2
.end method

.method static synthetic g(Lehu;I)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0, p1}, Lehu;->d(I)V

    return-void
.end method

.method static synthetic g(Lehu;Z)V
    .locals 5

    .prologue
    .line 239
    const-string v0, "extra_activity_id"

    iget-object v1, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz p1, :cond_0

    sget-object v0, Lhmv;->aq:Lhmv;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lehu;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v0, p0, Lehu;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/service/EsService;->d(Landroid/content/Context;ILjava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    if-eqz p1, :cond_2

    const/16 v0, 0x16

    :goto_2
    invoke-direct {p0, v0}, Lehu;->d(I)V

    return-void

    :cond_0
    sget-object v0, Lhmv;->ap:Lhmv;

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/16 v0, 0x17

    goto :goto_2
.end method

.method static synthetic g(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->Z:Z

    return v0
.end method

.method static synthetic h(Lehu;)Llae;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->aw:Llae;

    return-object v0
.end method

.method static synthetic h(Lehu;Z)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0, p1}, Lehu;->b(Z)V

    return-void
.end method

.method static synthetic i(Lehu;)Lfdr;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->X:Lfdr;

    return-object v0
.end method

.method static synthetic i(Lehu;Z)Z
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lehu;->ag:Z

    return p1
.end method

.method static synthetic j(Lehu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->V:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lehu;Z)Z
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lehu;->aN:Z

    return p1
.end method

.method static synthetic k(Lehu;)Lhee;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->R:Lhee;

    return-object v0
.end method

.method static synthetic k(Lehu;Z)Z
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lehu;->aL:Z

    return p1
.end method

.method static synthetic l(Lehu;)Lkzp;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->ao:Lkzp;

    return-object v0
.end method

.method static synthetic m(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->aJ:Z

    return v0
.end method

.method static synthetic n(Lehu;)Llai;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->ax:Llai;

    return-object v0
.end method

.method static synthetic o(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->aN:Z

    return v0
.end method

.method static synthetic p(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->aj:Z

    return v0
.end method

.method static synthetic q(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->ah:Z

    return v0
.end method

.method static synthetic r(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->aP:Z

    return v0
.end method

.method static synthetic s(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->aL:Z

    return v0
.end method

.method static synthetic t(Lehu;)I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lehu;->ak:I

    return v0
.end method

.method static synthetic u(Lehu;)I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lehu;->an:I

    return v0
.end method

.method static synthetic v(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->aK:Z

    return v0
.end method

.method static synthetic w(Lehu;)Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lehu;->am:Z

    return v0
.end method

.method static synthetic x(Lehu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->W:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y(Lehu;)Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->az:Landroid/text/Spanned;

    return-object v0
.end method

.method static synthetic z(Lehu;)Ljxs;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lehu;->bL:Ljxs;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 1991
    sget-object v0, Lhmw;->m:Lhmw;

    return-object v0
.end method

.method public U()V
    .locals 0

    .prologue
    .line 2169
    invoke-direct {p0}, Lehu;->ad()V

    .line 2170
    return-void
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 1512
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lehu;->b(Z)V

    .line 1513
    const/4 v0, 0x1

    return v0
.end method

.method public W()Lldh;
    .locals 1

    .prologue
    .line 3273
    iget-object v0, p0, Lehu;->ad:Lldh;

    return-object v0
.end method

.method public X()I
    .locals 1

    .prologue
    .line 3432
    iget v0, p0, Lehu;->bb:I

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 2183
    iget-object v0, p0, Lehu;->ao:Lkzp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehu;->ao:Lkzp;

    invoke-virtual {v0}, Lkzp;->n()Llac;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2184
    const/16 v0, 0xd8

    .line 2186
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x72

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 18

    .prologue
    .line 1065
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->at:Llnl;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    .line 1066
    invoke-virtual/range {p0 .. p0}, Lehu;->n()Lz;

    move-result-object v3

    .line 1068
    invoke-virtual/range {p0 .. p0}, Lehu;->n()Lz;

    move-result-object v2

    const-class v4, Ljgi;

    invoke-static {v2, v4}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljgi;

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljgi;->a()V

    .line 1069
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0d038c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lehu;->bC:I

    .line 1071
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    .line 1072
    const v2, 0x7f0400e6

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v11, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    .line 1079
    new-instance v2, Llcr;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lehu;->bc:Z

    invoke-direct {v2, v3, v4}, Llcr;-><init>(Landroid/content/Context;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->bd:Llcr;

    .line 1080
    new-instance v2, Lfdr;

    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lehu;->bd:Llcr;

    move-object/from16 v0, p0

    iget-object v5, v0, Lehu;->ao:Lkzp;

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0, v4, v5}, Lfdr;-><init>(Landroid/content/Context;Lehu;Llcr;Lkzp;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->X:Lfdr;

    .line 1082
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c()Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 1083
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b(Z)V

    .line 1085
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    new-instance v3, Lehv;

    invoke-direct {v3}, Lehv;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Lljl;)V

    .line 1094
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    const v3, 0x7f10032f

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->aT:Landroid/view/View;

    .line 1095
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->aT:Landroid/view/View;

    const v3, 0x7f1005cc

    .line 1096
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/squares/membership/JoinButton;

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->aU:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    .line 1098
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    const v3, 0x7f10032e

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->aW:Landroid/view/View;

    .line 1099
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a()Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    .line 1100
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->R:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lehu;->U:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Lu;ILjava/lang/String;Lkjm;)V

    .line 1101
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1102
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->b()Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->aY:Landroid/view/View;

    .line 1103
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->aY:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1104
    invoke-direct/range {p0 .. p0}, Lehu;->ab()V

    .line 1106
    new-instance v2, Leii;

    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->aY:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Leii;-><init>(Lehu;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->ba:Landroid/text/TextWatcher;

    .line 1107
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->ba:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1108
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    new-instance v3, Lehw;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lehw;-><init>(Lehu;)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1121
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->P:Lhje;

    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->bB:Leil;

    invoke-virtual {v2, v3}, Lhje;->a(Lhjj;)V

    .line 1123
    invoke-virtual/range {p0 .. p0}, Lehu;->k()Landroid/os/Bundle;

    move-result-object v2

    .line 1124
    const-string v3, "popup_start_x"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 1125
    const-string v4, "popup_start_y"

    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1130
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 1131
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lehu;->bk:I

    .line 1132
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    new-instance v3, Lljf;

    move-object/from16 v0, p0

    iget-object v4, v0, Lehu;->S:Llhd;

    invoke-direct {v3, v4}, Lljf;-><init>(Llhc;)V

    .line 1133
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Lljj;)V

    .line 1134
    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->X:Lfdr;

    move-object/from16 v0, p0

    iget v2, v0, Lehu;->bb:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v2}, Lfdr;->a(Z)V

    .line 1135
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    .line 1141
    :goto_1
    return-object v2

    .line 1134
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 1138
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->X:Lfdr;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lfdr;->a(Z)V

    .line 1139
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lehu;->bk:I

    .line 1140
    sget-object v2, Lehu;->by:Landroid/animation/TimeInterpolator;

    if-nez v2, :cond_4

    new-instance v2, Lhnh;

    const v3, 0x3ecccccd    # 0.4f

    const/4 v4, 0x0

    const v5, 0x3e4ccccd    # 0.2f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v2, v3, v4, v5, v6}, Lhnh;-><init>(FFFF)V

    sput-object v2, Lehu;->by:Landroid/animation/TimeInterpolator;

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lehu;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "popup_start_x"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "popup_start_y"

    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    const-string v4, "popup_start_width"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v4, "popup_start_height"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lehu;->e(I)I

    move-result v10

    invoke-direct/range {p0 .. p0}, Lehu;->aj()I

    move-result v8

    const-string v4, "popup_stream_start_height"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lehu;->bD:I

    const-string v4, "popup_stream_top"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lehu;->bE:I

    invoke-direct/range {p0 .. p0}, Lehu;->ak()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bp:[I

    aget v4, v2, v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bq:[I

    aget v6, v2, v12

    const/4 v2, -0x1

    if-eq v4, v2, :cond_5

    const/4 v2, -0x1

    if-ne v6, v2, :cond_7

    :cond_5
    const/4 v4, 0x0

    const/4 v2, 0x0

    invoke-direct/range {p0 .. p0}, Lehu;->ak()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lehu;->n()Lz;

    move-result-object v6

    invoke-static {v6}, Llsc;->b(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_a

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lehu;->bE:I

    :cond_6
    :goto_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lehu;->bp:[I

    aput v4, v6, v13

    move-object/from16 v0, p0

    iget-object v4, v0, Lehu;->bq:[I

    aput v2, v4, v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bp:[I

    aget v4, v2, v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bq:[I

    aget v6, v2, v12

    :cond_7
    new-instance v2, Leid;

    invoke-virtual {v11}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v11}, Leid;-><init>(Lehu;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->bl:Leid;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v11, -0x1

    const/4 v12, -0x1

    invoke-direct {v2, v11, v12}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lehu;->bl:Leid;

    invoke-virtual {v11, v2}, Leid;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bl:Leid;

    move-object/from16 v0, p0

    iget-object v11, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v2, v11}, Leid;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bl:Leid;

    new-instance v11, Lehx;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lehx;-><init>(Lehu;)V

    invoke-virtual {v2, v11}, Leid;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v2, Leim;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Leim;-><init>(Lehu;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->bu:Leim;

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bu:Leim;

    const-string v11, "values"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_0

    invoke-static {v2, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    sget-object v11, Lehu;->by:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v11}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    const-wide/16 v12, 0x190

    invoke-virtual {v2, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bd:Llcr;

    iget v2, v2, Llcr;->a:I

    const/4 v11, 0x1

    if-le v2, v11, :cond_c

    const/4 v2, 0x1

    :goto_3
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lehu;->bm:Z

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lehu;->bo:Z

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lehu;->bm:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bl:Leid;

    const v3, 0x7f02013f

    invoke-virtual {v2, v3}, Leid;->setBackgroundResource(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bl:Leid;

    invoke-virtual {v2}, Leid;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :cond_8
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iput v6, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iput v8, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v10, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lehu;->bm:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bl:Leid;

    const v3, 0x7f02013f

    invoke-virtual {v2, v3}, Leid;->setBackgroundResource(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bl:Leid;

    invoke-virtual {v2}, Leid;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/TransitionDrawable;

    const/16 v3, 0x96

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bl:Leid;

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lehu;->bp:[I

    aget v6, v6, v13

    const/4 v14, -0x1

    if-ne v6, v14, :cond_6

    invoke-direct/range {p0 .. p0}, Lehu;->aj()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lehu;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "popup_start_height"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lehu;->e(I)I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lehu;->n()Lz;

    move-result-object v2

    invoke-static {v2}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v15, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v16, v0

    div-int/lit8 v2, v16, 0x2

    div-int/lit8 v4, v6, 0x2

    sub-int/2addr v2, v4

    div-int/lit8 v4, v15, 0x2

    div-int/lit8 v17, v14, 0x2

    sub-int v4, v4, v17

    add-int v17, v2, v6

    move/from16 v0, v17

    move/from16 v1, v16

    if-le v0, v1, :cond_b

    add-int/2addr v6, v2

    move-object/from16 v0, p0

    iget v0, v0, Lehu;->bC:I

    move/from16 v17, v0

    sub-int v16, v16, v17

    sub-int v6, v6, v16

    sub-int/2addr v2, v6

    :cond_b
    const/4 v6, 0x0

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    add-int v2, v4, v14

    if-le v2, v15, :cond_f

    add-int v2, v4, v14

    sub-int/2addr v2, v15

    sub-int v2, v4, v2

    :goto_5
    move-object/from16 v0, p0

    iget v4, v0, Lehu;->bE:I

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v4, v6

    goto/16 :goto_2

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_d
    invoke-direct/range {p0 .. p0}, Lehu;->ak()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lehu;->bw:I

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v7, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bu:Leim;

    invoke-virtual/range {v2 .. v10}, Leim;->a(IIIIIIII)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lehu;->bm:Z

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c()Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setAlpha(F)V

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    new-instance v4, Lehy;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lehy;-><init>(Lehu;)V

    invoke-virtual {v2, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    move v10, v9

    move v8, v7

    move v6, v5

    move v4, v3

    goto/16 :goto_4

    :cond_f
    move v2, v4

    goto :goto_5

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1718
    packed-switch p1, :pswitch_data_0

    .line 1734
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1720
    :pswitch_0
    new-instance v0, Ldzg;

    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lehu;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/16 v3, 0x13

    sget-object v4, Lehu;->N:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Ldzg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    goto :goto_0

    .line 1725
    :pswitch_1
    new-instance v0, Ldwm;

    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lehu;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Ldwm;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 1729
    :pswitch_2
    new-instance v0, Lktn;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehu;->ai:Ljava/lang/String;

    sget-object v4, Lehu;->O:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lktn;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    .line 1718
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1996
    iget-object v0, p0, Lehu;->at:Llnl;

    const-class v1, Litj;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Litj;

    .line 1997
    invoke-interface {v0, p1, p2}, Litj;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2017
    :cond_0
    :goto_0
    return-void

    .line 2001
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2003
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2004
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2005
    const-string v1, "original_circle_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2007
    const-string v2, "selected_circle_ids"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2009
    const-string v3, "person_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2010
    const-string v4, "display_name"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2011
    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lehu;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2001
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 2278
    const-string v0, "comment_action"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2279
    if-nez v0, :cond_1

    .line 2384
    :cond_0
    :goto_0
    return-void

    .line 2283
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 2287
    iget-object v1, p0, Lehu;->T:Liwk;

    invoke-virtual {v1}, Liwk;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2288
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v1, p0, Lehu;->T:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Llnl;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2292
    :cond_2
    const-string v1, "comment_author_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2293
    const-string v1, "comment_author_name"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2294
    const-string v1, "comment_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2295
    const-string v1, "comment_content"

    .line 2296
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 2295
    invoke-static {v1}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v9

    .line 2297
    const-string v1, "plus_one_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2298
    const-string v1, "plus_one_by_me"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 2299
    const-string v1, "plus_one_count"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 2301
    iget-object v1, p0, Lehu;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 2303
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 2331
    :sswitch_0
    const-string v0, "extra_comment_id"

    invoke-static {v0, v3}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v4, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->W:Lhmv;

    invoke-virtual {v2, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    const v0, 0x7f0a0762

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a076c

    invoke-virtual {p0, v1}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0596

    invoke-virtual {p0, v2}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f0a0597

    invoke-virtual {p0, v4}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_id"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "delete_comment"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2305
    :sswitch_1
    if-eqz v4, :cond_3

    if-nez v6, :cond_4

    .line 2306
    :cond_3
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)I

    goto/16 :goto_0

    .line 2310
    :cond_4
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)I

    goto/16 :goto_0

    .line 2316
    :sswitch_2
    const-string v0, "extra_comment_id"

    invoke-static {v0, v3}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v4, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->U:Lhmv;

    invoke-virtual {v2, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    iget-object v0, p0, Lehu;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-boolean v8, p0, Lehu;->aj:Z

    move-object v4, v9

    invoke-static/range {v0 .. v8}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/text/Spanned;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lehu;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2319
    :sswitch_3
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, v3, v0, v1}, Lehu;->a(Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 2323
    :sswitch_4
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-direct {p0, v3, v0, v1}, Lehu;->a(Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 2327
    :sswitch_5
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, v3, v0, v1}, Lehu;->a(Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 2336
    :sswitch_6
    const v0, 0x7f0a0842

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0772

    invoke-virtual {p0, v1}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0a07fa

    invoke-virtual {p0, v3}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a07fd

    invoke-virtual {p0, v4}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v3, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "ban_user_id"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "ban_comment_author"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2339
    :sswitch_7
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0761

    invoke-direct {p0, v0, v1}, Lehu;->a(Ljava/lang/CharSequence;I)V

    goto/16 :goto_0

    .line 2342
    :sswitch_8
    new-instance v0, Lemv;

    invoke-direct {v0}, Lemv;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account_id"

    iget-object v3, p0, Lehu;->R:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "plus_one_id"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "total_plus_ones"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "restrict_to_domain"

    iget-boolean v3, p0, Lehu;->as:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lemv;->f(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "plus_ones"

    invoke-virtual {v0, v1, v2}, Lemv;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2345
    :sswitch_9
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2346
    const-string v0, "comment_discard_onreply_tag"

    invoke-direct {p0, v0}, Lehu;->a(Ljava/lang/String;)V

    .line 2347
    iput-object v2, p0, Lehu;->bF:Ljava/lang/String;

    .line 2348
    iput-object v5, p0, Lehu;->bG:Ljava/lang/String;

    goto/16 :goto_0

    .line 2350
    :cond_5
    invoke-direct {p0, v2, v5}, Lehu;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2354
    :sswitch_a
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "comment_author_id"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "comment_author_name"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ldyr;

    invoke-direct {v1, v5, v0}, Ldyr;-><init>(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, Ldyr;->a(Lu;I)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v0

    const-string v2, "block_person"

    invoke-virtual {v1, v0, v2}, Ldyr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2357
    :sswitch_b
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0981

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v2, 0x29

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0982

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0983

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0984

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v2, 0x2c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lehu;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0985

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v2, 0x2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const v0, 0x7f0a0974

    invoke-virtual {p0, v0}, Lehu;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Llgr;->a(Ljava/lang/String;[Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "comment_id"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "comment_action"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Lehu;->p()Lae;

    move-result-object v1

    const-string v2, "delete_comment"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2360
    :sswitch_c
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v3, v0, v1}, Lehu;->a(Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 2363
    :sswitch_d
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, v3, v0, v1}, Lehu;->a(Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 2366
    :sswitch_e
    const/4 v0, 0x0

    const/16 v1, 0x10

    invoke-direct {p0, v3, v0, v1}, Lehu;->a(Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 2370
    :sswitch_f
    iget-object v0, p0, Lehu;->at:Llnl;

    const-string v2, "https://support.google.com/legal/troubleshooter/1114905"

    .line 2371
    invoke-static {v2}, Litk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lehu;->aC:Ljava/lang/String;

    iget-object v4, p0, Lehu;->U:Ljava/lang/String;

    .line 2370
    invoke-static {v0, v1, v2, v3, v4}, Litm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2375
    :sswitch_10
    const/4 v0, 0x0

    const/16 v1, 0x11

    invoke-direct {p0, v3, v0, v1}, Lehu;->a(Ljava/lang/String;ZI)V

    goto/16 :goto_0

    .line 2378
    :sswitch_11
    invoke-direct {p0}, Lehu;->ae()V

    goto/16 :goto_0

    .line 2381
    :sswitch_12
    invoke-direct {p0}, Lehu;->af()V

    goto/16 :goto_0

    .line 2303
    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x22 -> :sswitch_3
        0x23 -> :sswitch_4
        0x24 -> :sswitch_5
        0x25 -> :sswitch_1
        0x26 -> :sswitch_2
        0x27 -> :sswitch_9
        0x28 -> :sswitch_b
        0x29 -> :sswitch_c
        0x2a -> :sswitch_d
        0x2b -> :sswitch_e
        0x2c -> :sswitch_f
        0x2d -> :sswitch_10
        0x31 -> :sswitch_a
        0x40 -> :sswitch_8
        0x41 -> :sswitch_11
        0x42 -> :sswitch_12
        0x52 -> :sswitch_6
        0x54 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2388
    const-string v0, "edit_tags"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389
    iget-object v0, p0, Lehu;->ay:[Z

    aput-boolean p2, v0, p1

    .line 2391
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 985
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 986
    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v0

    .line 987
    invoke-virtual {v0}, Lhoc;->d()Lhos;

    move-result-object v3

    .line 988
    invoke-virtual {v3, p0, v4, v1}, Lhos;->a(Lu;Ljava/lang/String;Z)V

    .line 989
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 991
    new-instance v0, Leih;

    invoke-direct {v0, p0}, Leih;-><init>(Lehu;)V

    iput-object v0, p0, Lehu;->ac:Lfdp;

    .line 993
    invoke-virtual {p0}, Lehu;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 994
    invoke-virtual {p0}, Lehu;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v1, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 996
    sget-object v0, Lehu;->bz:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 998
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f02003f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lehu;->bz:Landroid/graphics/drawable/Drawable;

    .line 1001
    :cond_0
    if-eqz p1, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lehu;->bo:Z

    .line 1002
    iget-boolean v0, p0, Lehu;->bo:Z

    if-eqz v0, :cond_5

    .line 1004
    const-string v0, "popup_end_x"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lehu;->bp:[I

    .line 1005
    const-string v0, "popup_end_y"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lehu;->bq:[I

    .line 1006
    const-string v0, "popup_start_orientation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lehu;->bw:I

    .line 1008
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1009
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    .line 1011
    :cond_1
    const-string v0, "db_context_specific_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1012
    const-string v0, "db_context_specific_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lkzp;->a([B)Lkzp;

    move-result-object v0

    iput-object v0, p0, Lehu;->ao:Lkzp;

    .line 1015
    :cond_2
    const-string v0, "operation_type"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lehu;->af:I

    .line 1017
    const-string v0, "get_activity_complete"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->Z:Z

    .line 1019
    iget-object v3, p0, Lehu;->at:Llnl;

    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v4, Lhms;

    .line 1020
    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 1019
    invoke-static {v3, v0, p1}, Ldib;->a(Landroid/content/Context;Lhms;Landroid/os/Bundle;)Ldib;

    move-result-object v0

    iput-object v0, p0, Lehu;->ab:Ldib;

    .line 1022
    const-string v0, "relateds_checked_states"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    iput-object v0, p0, Lehu;->ay:[Z

    .line 1025
    const-string v0, "seen_public_dasher_warning"

    .line 1026
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->ar:Z

    .line 1027
    const-string v0, "mute_processed"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->ag:Z

    .line 1028
    const-string v0, "read_processed"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->aQ:Z

    .line 1029
    const-string v0, "audience_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Lehu;->aR:Lhgw;

    .line 1030
    const-string v0, "blocked_gaia_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lehu;->aS:Ljava/util/ArrayList;

    .line 1032
    const-string v0, "show_keyboard"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->aZ:Z

    .line 1033
    const-string v0, "host_mode"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lehu;->bb:I

    .line 1035
    const-string v0, "force_full_bleed"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->bc:Z

    .line 1036
    const-string v0, "max_span"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lehu;->be:I

    .line 1037
    const-string v0, "can_comment"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->aN:Z

    .line 1038
    const-string v0, "is_post_restricted_to_domain"

    .line 1039
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->aO:Z

    .line 1040
    const-string v0, "scroll_to_last_comment_pending"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->bf:Z

    .line 1042
    const-string v0, "commentreply_authorname"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehu;->bG:Ljava/lang/String;

    .line 1043
    const-string v0, "commentreply_authorid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehu;->bF:Ljava/lang/String;

    .line 1045
    const-string v0, "scroll_position"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lehu;->br:I

    .line 1046
    const-string v0, "scroll_offset"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lehu;->bs:I

    .line 1050
    :cond_3
    :goto_1
    return-void

    :cond_4
    move v0, v2

    .line 1001
    goto/16 :goto_0

    .line 1047
    :cond_5
    invoke-virtual {p0}, Lehu;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1048
    invoke-direct {p0}, Lehu;->ac()V

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 2202
    iget-object v1, p0, Lehu;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 2203
    const-string v2, "delete_activity"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2204
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    .line 2205
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lehu;->d(I)V

    .line 2263
    :cond_0
    :goto_0
    return-void

    .line 2206
    :cond_1
    const-string v2, "delete_comment"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2207
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    const-string v3, "comment_id"

    .line 2208
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2207
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->e(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    .line 2209
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lehu;->d(I)V

    goto :goto_0

    .line 2210
    :cond_2
    const-string v2, "mute_activity"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2211
    iget-object v2, p0, Lehu;->at:Llnl;

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    iget-boolean v4, p0, Lehu;->aJ:Z

    if-nez v4, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-static {v2, v1, v3, v0}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    .line 2213
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lehu;->d(I)V

    goto :goto_0

    .line 2214
    :cond_4
    const-string v2, "report_activity"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2215
    iget v0, p0, Lehu;->ak:I

    invoke-static {v0}, Lkto;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lehu;->aC:Ljava/lang/String;

    .line 2217
    :goto_1
    iget-object v2, p0, Lehu;->at:Llnl;

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v2, v1, v3, v0}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    .line 2219
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lehu;->d(I)V

    goto :goto_0

    :cond_5
    move-object v0, v6

    .line 2215
    goto :goto_1

    .line 2220
    :cond_6
    const-string v2, "cancel_edits"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2221
    invoke-virtual {p0}, Lehu;->e()V

    goto :goto_0

    .line 2222
    :cond_7
    const-string v2, "remove_activity"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2223
    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v7

    new-instance v0, Lkyb;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    .line 2224
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehu;->ai:Ljava/lang/String;

    iget-object v4, p0, Lehu;->U:Ljava/lang/String;

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lkyb;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V

    .line 2223
    invoke-virtual {v7, v0}, Lhoc;->c(Lhny;)V

    .line 2227
    sget-object v0, Lhmv;->cB:Lhmv;

    invoke-direct {p0, v0, v6}, Lehu;->a(Lhmv;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2229
    :cond_8
    const-string v2, "ban_comment_author"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2230
    const-string v0, "ban_user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2231
    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v6

    new-instance v0, Lkvp;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    .line 2232
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehu;->ai:Ljava/lang/String;

    const/4 v5, 0x7

    invoke-direct/range {v0 .. v5}, Lkvp;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V

    .line 2231
    invoke-virtual {v6, v0}, Lhoc;->c(Lhny;)V

    .line 2233
    sget-object v0, Lhmv;->cC:Lhmv;

    invoke-direct {p0, v0, v4}, Lehu;->a(Lhmv;Ljava/lang/String;)V

    .line 2234
    iget-object v0, p0, Lehu;->at:Llnl;

    const/4 v1, 0x4

    new-instance v2, Lhml;

    invoke-direct {v2}, Lhml;-><init>()V

    new-instance v3, Lhmk;

    sget-object v4, Lomv;->g:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    .line 2235
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    new-instance v3, Lhmk;

    sget-object v4, Lonm;->c:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    .line 2236
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    iget-object v3, p0, Lehu;->at:Llnl;

    .line 2237
    invoke-virtual {v2, v3}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v2

    .line 2234
    invoke-static {v0, v1, v2}, Lhly;->a(Landroid/content/Context;ILhml;)V

    goto/16 :goto_0

    .line 2238
    :cond_9
    const-string v2, "edit_tags"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2239
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2240
    :goto_2
    iget-object v3, p0, Lehu;->ay:[Z

    array-length v3, v3

    if-ge v0, v3, :cond_b

    .line 2241
    iget-object v3, p0, Lehu;->ay:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_a

    .line 2242
    iget-object v3, p0, Lehu;->ax:Llai;

    invoke-virtual {v3, v0}, Llai;->c(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2240
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2245
    :cond_b
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_c

    .line 2246
    iget-object v0, p0, Lehu;->ax:Llai;

    iget-object v3, p0, Lehu;->ay:[Z

    .line 2247
    invoke-static {v0, v3}, Llai;->a(Llai;[Z)Llai;

    move-result-object v0

    .line 2248
    iget-object v3, p0, Lehu;->at:Llnl;

    iget-object v4, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v3, v1, v4, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;Llai;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    .line 2250
    const/16 v0, 0x80

    invoke-direct {p0, v0}, Lehu;->d(I)V

    .line 2252
    :cond_c
    iput-object v6, p0, Lehu;->ay:[Z

    goto/16 :goto_0

    .line 2253
    :cond_d
    const-string v0, "public_dasher_warning"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2254
    iget-object v0, p0, Lehu;->aY:Landroid/view/View;

    iget-object v1, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    .line 2255
    :cond_e
    const-string v0, "comment_confirmation_tag"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2256
    invoke-virtual {p0}, Lehu;->e()V

    goto/16 :goto_0

    .line 2257
    :cond_f
    const-string v0, "comment_discard_onreply_tag"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2258
    iget-object v0, p0, Lehu;->bF:Ljava/lang/String;

    iget-object v1, p0, Lehu;->bG:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lehu;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2259
    :cond_10
    const-string v0, "reject_inferred_post"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2260
    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v0

    new-instance v2, Ldpu;

    iget-object v3, p0, Lehu;->at:Llnl;

    iget-object v4, p0, Lehu;->U:Ljava/lang/String;

    iget-object v5, p0, Lehu;->V:Ljava/lang/String;

    invoke-direct {v2, v3, v1, v4, v5}, Ldpu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lhoc;->b(Lhny;)V

    goto/16 :goto_0
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 5

    .prologue
    .line 3488
    check-cast p1, Landroid/os/Bundle;

    .line 3489
    const-string v0, "comment_author_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3490
    const-string v1, "comment_author_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3491
    const/16 v2, 0x31

    invoke-direct {p0, v2}, Lehu;->d(I)V

    .line 3492
    const-string v2, "g:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3493
    :goto_0
    iget-object v2, p0, Lehu;->at:Llnl;

    iget-object v3, p0, Lehu;->R:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const/4 v4, 0x1

    invoke-static {v2, v3, v0, v1, v4}, Lcom/google/android/apps/plus/service/EsService;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    .line 3495
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lehu;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->bM:Lhmv;

    .line 3496
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 3495
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 3497
    return-void

    .line 3492
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1977
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v11, 0x2

    const/4 v10, -0x1

    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1778
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1957
    :cond_0
    :goto_0
    return-void

    .line 1780
    :pswitch_0
    iput-object p2, p0, Lehu;->bi:Landroid/database/Cursor;

    goto :goto_0

    .line 1785
    :pswitch_1
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1786
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1787
    :goto_1
    iget-boolean v2, p0, Lehu;->am:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    .line 1789
    invoke-direct {p0}, Lehu;->ac()V

    .line 1791
    :cond_1
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lehu;->ak:I

    .line 1792
    invoke-interface {p2, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lehu;->al:I

    .line 1793
    iput-boolean v0, p0, Lehu;->am:Z

    .line 1794
    invoke-direct {p0}, Lehu;->ab()V

    .line 1956
    :cond_2
    :goto_2
    iget-object v0, p0, Lehu;->P:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1786
    goto :goto_1

    .line 1801
    :pswitch_2
    iget-object v0, p0, Lehu;->X:Lfdr;

    invoke-virtual {v0, p2}, Lfdr;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 1802
    iget-boolean v0, p0, Lehu;->aa:Z

    if-nez v0, :cond_4

    .line 1804
    iput-boolean v1, p0, Lehu;->aa:Z

    .line 1805
    iget-object v0, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    iget-object v3, p0, Lehu;->X:Lfdr;

    iget v4, p0, Lehu;->br:I

    iget v5, p0, Lehu;->bs:I

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/widget/BaseAdapter;II)V

    .line 1808
    :cond_4
    iget-boolean v0, p0, Lehu;->Z:Z

    if-nez v0, :cond_11

    .line 1809
    invoke-direct {p0}, Lehu;->ac()V

    .line 1814
    :cond_5
    :goto_3
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1818
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v11, v0, :cond_6

    .line 1819
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824
    :cond_6
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehu;->V:Ljava/lang/String;

    .line 1825
    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehu;->W:Ljava/lang/String;

    .line 1827
    const/16 v0, 0x14

    .line 1828
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1829
    if-eqz v0, :cond_7

    .line 1831
    invoke-static {v0}, Llal;->a([B)Llal;

    move-result-object v0

    .line 1832
    invoke-virtual {v0}, Llal;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lehu;->ai:Ljava/lang/String;

    .line 1833
    invoke-virtual {v0}, Llal;->f()I

    move-result v3

    iput v3, p0, Lehu;->an:I

    .line 1834
    invoke-virtual {v0}, Llal;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehu;->bI:Ljava/lang/String;

    .line 1836
    :cond_7
    const/16 v0, 0xa

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1838
    const-wide/32 v6, 0x200000

    and-long/2addr v6, v4

    cmp-long v0, v6, v8

    if-eqz v0, :cond_12

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lehu;->ah:Z

    .line 1840
    const-wide/16 v6, 0x4000

    and-long/2addr v6, v4

    cmp-long v0, v6, v8

    if-eqz v0, :cond_13

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lehu;->aj:Z

    .line 1842
    const-wide/32 v6, 0x80000

    and-long/2addr v6, v4

    cmp-long v0, v6, v8

    if-eqz v0, :cond_8

    .line 1843
    const/16 v0, 0x17

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1844
    if-eqz v0, :cond_8

    .line 1845
    invoke-static {v0}, Lkzu;->a([B)Lkzu;

    move-result-object v0

    iput-object v0, p0, Lehu;->aH:Lkzu;

    .line 1847
    sget-object v0, Llap;->a:Lloz;

    .line 1848
    iput-boolean v2, p0, Lehu;->aI:Z

    .line 1852
    :cond_8
    const-wide/32 v6, 0x200000

    and-long/2addr v6, v4

    cmp-long v0, v6, v8

    if-eqz v0, :cond_9

    .line 1853
    const/16 v0, 0x17

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1854
    if-eqz v0, :cond_9

    .line 1855
    invoke-static {v0}, Lkzw;->a([B)Lkzw;

    move-result-object v0

    iput-object v0, p0, Lehu;->ap:Lkzw;

    .line 1859
    :cond_9
    const/16 v0, 0x8

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1860
    invoke-static {v0}, Llae;->a([B)Llae;

    move-result-object v0

    iput-object v0, p0, Lehu;->aw:Llae;

    .line 1862
    const/16 v0, 0x16

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_14

    .line 1863
    const/16 v0, 0x16

    .line 1864
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1863
    invoke-static {v0}, Llai;->a([B)Llai;

    move-result-object v0

    iput-object v0, p0, Lehu;->ax:Llai;

    .line 1869
    :goto_6
    const/4 v0, 0x6

    .line 1870
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1869
    invoke-static {v0}, Llah;->a([B)Llah;

    move-result-object v0

    iput-object v0, p0, Lehu;->aE:Llah;

    .line 1871
    const/4 v0, 0x7

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lehu;->aF:I

    .line 1872
    const/16 v0, 0x20

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lehu;->aG:I

    .line 1874
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v3

    invoke-virtual {v3}, Lz;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lz;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v3}, Lz;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7, v6}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_15

    move v0, v1

    :goto_7
    if-eqz v0, :cond_16

    if-eqz v6, :cond_16

    const-string v0, "com.google.android.apps.gmm."

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    move v0, v1

    :goto_8
    if-eqz v0, :cond_b

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v6, p0, Lehu;->aE:Llah;

    if-eqz v6, :cond_a

    const-string v6, "com.google.android.apps.gmm.plusCount"

    iget-object v7, p0, Lehu;->aE:Llah;

    invoke-virtual {v7}, Llah;->b()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v6, "com.google.android.apps.gmm.isPlussed"

    iget-object v7, p0, Lehu;->aE:Llah;

    invoke-virtual {v7}, Llah;->c()Z

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_a
    const-string v6, "com.google.android.apps.gmm.commentCount"

    iget v7, p0, Lehu;->aF:I

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v3, v10, v0}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 1877
    :cond_b
    const/16 v0, 0xe

    .line 1878
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1879
    const/16 v0, 0xd

    .line 1880
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1879
    invoke-static {v0}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lehu;->az:Landroid/text/Spanned;

    .line 1887
    :goto_9
    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_18

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lehu;->aA:Z

    .line 1889
    const/16 v0, 0x28

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehu;->aB:Ljava/lang/String;

    .line 1890
    const/16 v0, 0x1c

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1891
    if-eqz v3, :cond_19

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lehu;->aO:Z

    .line 1892
    iget-object v0, p0, Lehu;->X:Lfdr;

    invoke-virtual {v0, v3}, Lfdr;->a(Ljava/lang/String;)V

    .line 1893
    const/16 v0, 0x1a

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehu;->aC:Ljava/lang/String;

    .line 1895
    const/16 v0, 0x1f

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehu;->aD:Ljava/lang/String;

    .line 1897
    iget-boolean v0, p0, Lehu;->aQ:Z

    if-nez v0, :cond_d

    .line 1898
    const-string v0, "extra_activity_id"

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    .line 1899
    invoke-static {v0, v3}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1901
    iget-object v3, p0, Lehu;->aC:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_23

    .line 1902
    if-nez v0, :cond_c

    .line 1903
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1905
    :cond_c
    const-string v3, "extra_creation_source_id"

    iget-object v4, p0, Lehu;->aC:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v0

    .line 1908
    :goto_c
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lehu;->at:Llnl;

    invoke-direct {v4, v5}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v5, Lhmv;->ad:Lhmv;

    .line 1909
    invoke-virtual {v4, v5}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v4

    .line 1910
    invoke-virtual {v4, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v3

    .line 1908
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 1911
    iput-boolean v1, p0, Lehu;->aQ:Z

    .line 1914
    :cond_d
    const/16 v0, 0xb

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1915
    const-wide/16 v6, 0x40

    and-long/2addr v6, v4

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1a

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lehu;->aJ:Z

    .line 1916
    const-wide/16 v6, 0x200

    and-long/2addr v6, v4

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1b

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lehu;->aK:Z

    .line 1917
    const-wide/16 v6, 0x8

    and-long/2addr v6, v4

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1c

    move v0, v1

    :goto_f
    iput-boolean v0, p0, Lehu;->aL:Z

    .line 1918
    const/16 v0, 0xe

    .line 1919
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1918
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1d

    move v0, v1

    :goto_10
    iput-boolean v0, p0, Lehu;->aM:Z

    .line 1920
    const-wide/16 v6, 0x4

    and-long/2addr v6, v4

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1e

    move v0, v1

    :goto_11
    iput-boolean v0, p0, Lehu;->aN:Z

    .line 1921
    const-wide/16 v6, 0x1

    and-long/2addr v6, v4

    cmp-long v0, v6, v8

    if-nez v0, :cond_1f

    const-wide/16 v6, 0x400

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-nez v0, :cond_1f

    move v0, v1

    :goto_12
    iput-boolean v0, p0, Lehu;->aP:Z

    .line 1924
    invoke-direct {p0}, Lehu;->ab()V

    .line 1926
    invoke-virtual {p0}, Lehu;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 1927
    iget-boolean v3, p0, Lehu;->ag:Z

    if-nez v3, :cond_e

    const-string v3, "mute"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1928
    invoke-virtual {p0}, Lehu;->x()Landroid/view/View;

    move-result-object v0

    new-instance v3, Leib;

    invoke-direct {v3, p0}, Leib;-><init>(Lehu;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1942
    :cond_e
    iget-object v0, p0, Lehu;->aq:Leij;

    if-eqz v0, :cond_f

    .line 1943
    iget-object v0, p0, Lehu;->aq:Leij;

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    invoke-interface {v0, p2}, Leij;->a(Landroid/database/Cursor;)V

    .line 1946
    :cond_f
    iget-boolean v0, p0, Lehu;->aj:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lehu;->ai:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1947
    iget v0, p0, Lehu;->ak:I

    if-eq v0, v10, :cond_10

    iget v0, p0, Lehu;->al:I

    if-ne v0, v10, :cond_20

    .line 1949
    :cond_10
    invoke-virtual {p0}, Lehu;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v11, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto/16 :goto_2

    .line 1811
    :cond_11
    iget-boolean v0, p0, Lehu;->bf:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lehu;->X:Lfdr;

    invoke-virtual {v0}, Lfdr;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->c()V

    iput-boolean v2, p0, Lehu;->bf:Z

    goto/16 :goto_3

    :cond_12
    move v0, v2

    .line 1838
    goto/16 :goto_4

    :cond_13
    move v0, v2

    .line 1840
    goto/16 :goto_5

    .line 1866
    :cond_14
    const/4 v0, 0x0

    iput-object v0, p0, Lehu;->ax:Llai;

    goto/16 :goto_6

    :cond_15
    move v0, v2

    .line 1874
    goto/16 :goto_7

    :cond_16
    move v0, v2

    goto/16 :goto_8

    .line 1882
    :cond_17
    const/16 v0, 0xc

    .line 1883
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1882
    invoke-static {v0}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lehu;->az:Landroid/text/Spanned;

    goto/16 :goto_9

    :cond_18
    move v0, v2

    .line 1887
    goto/16 :goto_a

    :cond_19
    move v0, v2

    .line 1891
    goto/16 :goto_b

    :cond_1a
    move v0, v2

    .line 1915
    goto/16 :goto_d

    :cond_1b
    move v0, v2

    .line 1916
    goto/16 :goto_e

    :cond_1c
    move v0, v2

    .line 1917
    goto/16 :goto_f

    :cond_1d
    move v0, v2

    .line 1918
    goto/16 :goto_10

    :cond_1e
    move v0, v2

    .line 1920
    goto/16 :goto_11

    :cond_1f
    move v0, v2

    .line 1921
    goto/16 :goto_12

    .line 1951
    :cond_20
    iget v0, p0, Lehu;->ak:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_21

    iget v0, p0, Lehu;->ak:I

    .line 1952
    invoke-static {v0}, Lkto;->a(I)Z

    move-result v0

    if-eqz v0, :cond_22

    :cond_21
    move v2, v1

    :cond_22
    iput-boolean v2, p0, Lehu;->am:Z

    goto/16 :goto_2

    :cond_23
    move-object v3, v0

    goto/16 :goto_c

    .line 1778
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 239
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lehu;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Leij;)V
    .locals 3

    .prologue
    .line 3436
    iput-object p1, p0, Lehu;->aq:Leij;

    .line 3438
    invoke-virtual {p0}, Lehu;->x()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3439
    invoke-virtual {p0}, Lehu;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 3445
    :cond_0
    return-void
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 1153
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 3739
    return-void
.end method

.method public a(Ljava/lang/String;ILhoz;)V
    .locals 3

    .prologue
    .line 3743
    invoke-virtual {p0}, Lehu;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 3744
    invoke-direct {p0}, Lehu;->ac()V

    .line 3745
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 17

    .prologue
    .line 2887
    const-string v2, "EditModerationStateTask"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 2888
    invoke-virtual/range {p2 .. p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "moderation_state"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 2890
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 2891
    invoke-virtual/range {p0 .. p0}, Lehu;->e()V

    .line 2931
    :cond_0
    :goto_0
    return-void

    .line 2893
    :cond_1
    const-string v2, "RemoveReportBanTask"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    .line 2894
    invoke-virtual/range {p2 .. p2}, Lhoz;->f()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2895
    invoke-virtual/range {p2 .. p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "remove_post"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2897
    invoke-virtual/range {p0 .. p0}, Lehu;->e()V

    goto :goto_0

    .line 2899
    :cond_2
    const-string v2, "GetActivityTask"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2900
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->P:Lhje;

    invoke-virtual {v2}, Lhje;->a()V

    .line 2901
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lehu;->Z:Z

    .line 2903
    invoke-static/range {p2 .. p2}, Lhoz;->a(Lhoz;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2904
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->at:Llnl;

    const v3, 0x7f0a07d5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lehu;->i_(I)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 2905
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2907
    :cond_3
    const-string v2, "ModifyCircleMembershipsTask"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2908
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->ab:Ldib;

    if-eqz v2, :cond_0

    .line 2909
    invoke-static/range {p2 .. p2}, Lhoz;->a(Lhoz;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2910
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 2911
    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->ab:Ldib;

    invoke-virtual {v3, v2}, Ldib;->a(I)V

    .line 2915
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lehu;->ab:Ldib;

    goto :goto_0

    .line 2913
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lehu;->n()Lz;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lhoz;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 2917
    :cond_5
    const-string v2, "SetSquarePinnedStateTask"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual/range {p2 .. p2}, Lhoz;->f()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2918
    invoke-virtual/range {p2 .. p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "pinned_state"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 2919
    invoke-static {}, Llay;->a()Llay;

    move-result-object v3

    invoke-virtual {v3}, Llay;->b()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->ao:Lkzp;

    if-nez v3, :cond_6

    new-instance v3, Lkzp;

    invoke-direct {v3}, Lkzp;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lehu;->ao:Lkzp;

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->ao:Lkzp;

    new-instance v4, Llab;

    invoke-direct {v4, v2}, Llab;-><init>(I)V

    invoke-virtual {v3, v4}, Lkzp;->a(Llab;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->ao:Lkzp;

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->ai:Ljava/lang/String;

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v3, v2}, Lkzp;->a(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->X:Lfdr;

    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->ao:Lkzp;

    invoke-virtual {v2, v3}, Lfdr;->a(Lkzp;)V

    invoke-direct/range {p0 .. p0}, Lehu;->ac()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->at:Llnl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lehu;->R:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lehu;->ai:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Ldov;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Ljava/lang/String;[Ljava/lang/String;ZZJ[Ljava/lang/String;)Ldov;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lehu;->al()Lhoc;

    move-result-object v3

    invoke-virtual {v3, v2}, Lhoc;->b(Lhny;)V

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    .line 2920
    :cond_8
    const-string v2, "RejectInferredPostTask"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-static/range {p2 .. p2}, Lhoz;->a(Lhoz;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 2921
    invoke-virtual/range {p0 .. p0}, Lehu;->e()V

    .line 2922
    invoke-virtual/range {p0 .. p0}, Lehu;->n()Lz;

    move-result-object v2

    const v3, 0x7f0a0b22

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lehu;->W:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lehu;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 2923
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2924
    :cond_9
    const-string v2, "GetRedirectUrlTask"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2925
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bH:Lfcu;

    if-eqz v2, :cond_0

    .line 2926
    move-object/from16 v0, p0

    iget-object v2, v0, Lehu;->bH:Lfcu;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lfcu;->a(Lhoz;)V

    goto/16 :goto_0

    .line 2928
    :cond_a
    const-string v2, "EditSquareMembershipTask"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2929
    invoke-direct/range {p0 .. p0}, Lehu;->ac()V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 10

    .prologue
    .line 3480
    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v9

    new-instance v0, Ldpz;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    .line 3481
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v6, p0, Lehu;->aC:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v7, p4

    move v8, p5

    invoke-direct/range {v0 .. v8}, Ldpz;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 3480
    invoke-virtual {v9, v0}, Lhoc;->c(Lhny;)V

    .line 3483
    sget-object v0, Lhmv;->cs:Lhmv;

    invoke-direct {p0, v0, p1}, Lehu;->a(Lhmv;Ljava/lang/String;)V

    .line 3484
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 1171
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 1172
    return-void
.end method

.method public a(Z)V
    .locals 12

    .prologue
    .line 1192
    const/4 v0, 0x0

    iput-object v0, p0, Lehu;->aR:Lhgw;

    .line 1194
    iget v0, p0, Lehu;->bk:I

    if-nez v0, :cond_2

    .line 1195
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v1

    .line 1196
    if-eqz v1, :cond_1

    .line 1197
    if-eqz p1, :cond_0

    .line 1198
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Llln;

    invoke-virtual {v0, v2}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llln;

    .line 1199
    if-eqz v0, :cond_0

    .line 1200
    invoke-virtual {v0}, Llln;->a()Z

    .line 1203
    :cond_0
    invoke-virtual {v1}, Lz;->finish()V

    .line 1225
    :cond_1
    :goto_0
    return-void

    .line 1209
    :cond_2
    iget-boolean v0, p0, Lehu;->bt:Z

    if-nez v0, :cond_1

    .line 1213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehu;->bt:Z

    .line 1215
    invoke-direct {p0}, Lehu;->ak()I

    move-result v0

    .line 1219
    iget v1, p0, Lehu;->bw:I

    if-eq v0, v1, :cond_3

    .line 1220
    invoke-direct {p0}, Lehu;->Z()V

    .line 1221
    invoke-direct {p0}, Lehu;->aa()V

    goto :goto_0

    .line 1223
    :cond_3
    invoke-direct {p0}, Lehu;->ak()I

    move-result v0

    invoke-virtual {p0}, Lehu;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "popup_start_x"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "popup_start_y"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v3, "popup_start_width"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v3, "popup_start_height"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-direct {p0, v8}, Lehu;->e(I)I

    move-result v7

    invoke-direct {p0}, Lehu;->aj()I

    move-result v5

    iget-object v1, p0, Lehu;->bp:[I

    aget v1, v1, v0

    iget-object v3, p0, Lehu;->bq:[I

    aget v3, v3, v0

    const/4 v9, 0x0

    const/4 v0, 0x0

    iget-boolean v10, p0, Lehu;->bm:Z

    if-eqz v10, :cond_6

    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c()Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v0

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v0, v9}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setAlpha(F)V

    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c()Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v10, 0x96

    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v10, 0x96

    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    const/4 v0, 0x1

    move v9, v0

    move v10, v0

    :goto_1
    iget-object v0, p0, Lehu;->bu:Leim;

    invoke-virtual/range {v0 .. v8}, Leim;->a(IIIIIIII)V

    iget-object v0, p0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    iget-object v0, p0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    new-instance v1, Lehz;

    invoke-direct {v1, p0}, Lehz;-><init>(Lehu;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x140

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lehu;->bx:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Leia;

    invoke-direct {v1, p0}, Leia;-><init>(Lehu;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    if-eqz v10, :cond_4

    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c()Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_4
    if-eqz v9, :cond_5

    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_5
    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    iget-object v0, p0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iget-boolean v0, p0, Lehu;->bm:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lehu;->bl:Leid;

    const v1, 0x7f020140

    invoke-virtual {v0, v1}, Leid;->setBackgroundResource(I)V

    iget-object v0, p0, Lehu;->bl:Leid;

    invoke-virtual {v0}, Leid;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto/16 :goto_0

    :cond_6
    move v10, v9

    move v9, v0

    goto/16 :goto_1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1157
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1158
    const v1, 0x7f10067b

    if-ne v0, v1, :cond_0

    .line 1159
    invoke-direct {p0}, Lehu;->ac()V

    .line 1160
    const/4 v0, 0x1

    .line 1162
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 1518
    invoke-super {p0}, Llol;->aO_()V

    .line 1520
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v0

    .line 1521
    instance-of v1, v0, Los;

    if-eqz v1, :cond_1

    check-cast v0, Los;

    .line 1522
    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    .line 1523
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1524
    invoke-virtual {v0}, Loo;->e()V

    .line 1527
    :cond_0
    iget-object v0, p0, Lehu;->Q:Lein;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 1529
    invoke-virtual {p0}, Lehu;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lfdp;->b(Landroid/view/View;)V

    .line 1530
    return-void

    .line 1522
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 2197
    const/4 v0, 0x0

    return-object v0
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 3749
    new-instance v0, Lkre;

    sget-object v1, Lonm;->o:Lhmn;

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkre;-><init>(Lhmn;Ljava/lang/String;)V

    return-object v0
.end method

.method public ae_()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1564
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v0

    const-class v1, Ljgi;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgi;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljgi;->b()V

    .line 1566
    :cond_0
    iput-object v2, p0, Lehu;->aW:Landroid/view/View;

    .line 1568
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lehu;->ba:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1569
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1570
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->c()V

    .line 1571
    iput-object v2, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    .line 1573
    iget-object v0, p0, Lehu;->aY:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1574
    iput-object v2, p0, Lehu;->aY:Landroid/view/View;

    .line 1576
    iput-object v2, p0, Lehu;->aT:Landroid/view/View;

    .line 1578
    iget-object v0, p0, Lehu;->aU:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a(I)V

    .line 1579
    iput-object v2, p0, Lehu;->aU:Lcom/google/android/libraries/social/squares/membership/JoinButton;

    .line 1581
    iput-object v2, p0, Lehu;->aV:Lkvq;

    .line 1583
    iget-object v0, p0, Lehu;->bp:[I

    aput v4, v0, v3

    .line 1584
    iget-object v0, p0, Lehu;->bq:[I

    aput v4, v0, v3

    .line 1585
    iget-object v0, p0, Lehu;->bp:[I

    aput v4, v0, v5

    .line 1586
    iget-object v0, p0, Lehu;->bq:[I

    aput v4, v0, v5

    .line 1588
    iput-boolean v3, p0, Lehu;->bt:Z

    .line 1589
    iput-boolean v3, p0, Lehu;->bo:Z

    .line 1590
    iput-object v2, p0, Lehu;->X:Lfdr;

    .line 1592
    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->setVisibility(I)V

    .line 1594
    iget-object v0, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b(Z)V

    .line 1596
    iput-object v2, p0, Lehu;->bd:Llcr;

    .line 1597
    iput-object v2, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    .line 1598
    iput-object v2, p0, Lehu;->aY:Landroid/view/View;

    .line 1599
    iput-object v2, p0, Lehu;->ba:Landroid/text/TextWatcher;

    .line 1600
    iput-object v2, p0, Lehu;->bu:Leim;

    .line 1601
    iput-boolean v3, p0, Lehu;->Z:Z

    .line 1603
    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c()Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1604
    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c()Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a(Landroid/widget/BaseAdapter;)V

    .line 1608
    :cond_1
    iget-object v0, p0, Lehu;->bl:Leid;

    if-eqz v0, :cond_2

    .line 1609
    iget-object v0, p0, Lehu;->bl:Leid;

    invoke-virtual {v0, v2}, Leid;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1610
    iget-object v0, p0, Lehu;->bl:Leid;

    invoke-virtual {v0}, Leid;->removeAllViews()V

    .line 1613
    :cond_2
    iget-object v0, p0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_3

    .line 1614
    iget-object v0, p0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 1615
    iget-object v0, p0, Lehu;->bv:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1618
    :cond_3
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1619
    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    .line 1620
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 1621
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 1622
    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 1623
    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 1624
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1626
    iget-object v0, p0, Lehu;->bn:Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->setAlpha(F)V

    .line 1629
    :cond_4
    iget-object v0, p0, Lehu;->P:Lhje;

    iget-object v1, p0, Lehu;->bB:Leil;

    invoke-virtual {v0, v1}, Lhje;->b(Lhjj;)V

    .line 1631
    invoke-super {p0}, Llol;->ae_()V

    .line 1632
    return-void
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 2192
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1167
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2267
    const-string v0, "edit_tags"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2268
    const/4 v0, 0x0

    iput-object v0, p0, Lehu;->ay:[Z

    .line 2270
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 3467
    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v6

    new-instance v0, Lkyb;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    .line 3468
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v5, 0x2

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lkyb;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V

    .line 3467
    invoke-virtual {v6, v0}, Lhoc;->c(Lhny;)V

    .line 3469
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 1176
    return-void
.end method

.method public c(I)V
    .locals 3

    .prologue
    .line 3448
    iput p1, p0, Lehu;->be:I

    .line 3449
    invoke-direct {p0}, Lehu;->aj()I

    move-result v1

    .line 3450
    invoke-virtual {p0}, Lehu;->n()Lz;

    move-result-object v0

    .line 3451
    instance-of v2, v0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;

    if-eqz v2, :cond_0

    .line 3452
    check-cast v0, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->a(I)V

    .line 3458
    :goto_0
    return-void

    .line 3455
    :cond_0
    invoke-virtual {p0}, Lehu;->x()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f10032c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    .line 3456
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->a(I)V

    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 946
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 947
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 948
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lehu;->R:Lhee;

    .line 949
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Llhe;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llhe;

    .line 950
    if-eqz v0, :cond_0

    .line 951
    iget-object v1, p0, Lehu;->S:Llhd;

    invoke-virtual {v0}, Llhe;->a()Llhc;

    move-result-object v0

    invoke-virtual {v1, v0}, Llhd;->a(Llhc;)V

    .line 953
    :cond_0
    new-instance v0, Liwk;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v1, Lixj;

    .line 954
    invoke-virtual {v0, v1}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lehu;->T:Liwk;

    .line 955
    new-instance v0, Lfcu;

    iget-object v1, p0, Lehu;->at:Llnl;

    invoke-direct {v0, v1}, Lfcu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lehu;->bH:Lfcu;

    .line 956
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 957
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Ljxq;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxq;

    iput-object v0, p0, Lehu;->bK:Ljxq;

    .line 958
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Ljxs;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxs;

    iput-object v0, p0, Lehu;->bL:Ljxs;

    .line 959
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Lhjz;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjz;

    iput-object v0, p0, Lehu;->bM:Lhjz;

    .line 960
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Llep;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llep;

    iput-object v0, p0, Lehu;->bN:Llep;

    .line 961
    iget-object v0, p0, Lehu;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 963
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v2, Liaw;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liaw;

    .line 964
    invoke-interface {v0, v1}, Liaw;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lehu;->bh:Z

    .line 966
    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v1, Lhtp;

    new-instance v2, Lewk;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v3, p0}, Lewk;-><init>(Landroid/content/Context;Lu;)V

    .line 967
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Litj;

    new-instance v2, Lfup;

    iget-object v3, p0, Lehu;->at:Llnl;

    const/16 v4, 0xa

    const-string v5, "stream1upfrag"

    invoke-direct {v2, p0, v3, v4, v5}, Lfup;-><init>(Lu;Landroid/content/Context;ILjava/lang/String;)V

    .line 968
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkzh;

    iget-object v2, p0, Lehu;->bH:Lfcu;

    .line 970
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhtg;

    new-instance v2, Lfdq;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, v3}, Lfdq;-><init>(Landroid/content/Context;)V

    .line 971
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhti;

    new-instance v2, Leio;

    invoke-direct {v2, p0}, Leio;-><init>(Lehu;)V

    .line 972
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhtr;

    new-instance v2, Leip;

    invoke-direct {v2, p0}, Leip;-><init>(Lehu;)V

    .line 973
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhsn;

    new-instance v2, Leif;

    iget-object v3, p0, Lehu;->at:Llnl;

    invoke-direct {v2, p0, v3}, Leif;-><init>(Lehu;Landroid/content/Context;)V

    .line 974
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkuz;

    iget-object v2, p0, Lehu;->bO:Lkuu;

    .line 975
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkyw;

    iget-object v2, p0, Lehu;->bP:Lkyt;

    .line 976
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkxx;

    .line 977
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkvs;

    .line 978
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhmm;

    .line 979
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 981
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2274
    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 3473
    invoke-direct {p0}, Lehu;->al()Lhoc;

    move-result-object v6

    new-instance v0, Lkyb;

    iget-object v1, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    .line 3474
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v5, 0x1

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lkyb;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V

    .line 3473
    invoke-virtual {v6, v0}, Lhoc;->c(Lhny;)V

    .line 3475
    return-void
.end method

.method public d()Lfdp;
    .locals 1

    .prologue
    .line 1053
    iget-object v0, p0, Lehu;->ac:Lfdp;

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1058
    invoke-super {p0, p1}, Llol;->d(Landroid/os/Bundle;)V

    .line 1059
    iget v0, p0, Lehu;->be:I

    invoke-virtual {p0, v0}, Lehu;->c(I)V

    .line 1060
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1182
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lehu;->a(Z)V

    .line 1183
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1658
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 1660
    iget-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1661
    const-string v0, "pending_request_id"

    iget-object v1, p0, Lehu;->ae:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1664
    :cond_0
    iget-object v0, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    if-eqz v0, :cond_1

    .line 1665
    const-string v0, "scroll_position"

    iget-object v1, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1666
    const-string v0, "scroll_offset"

    iget-object v1, p0, Lehu;->Y:Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1668
    :cond_1
    const-string v0, "operation_type"

    iget v1, p0, Lehu;->af:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1669
    const-string v0, "get_activity_complete"

    iget-boolean v1, p0, Lehu;->Z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1670
    const-string v0, "relateds_checked_states"

    iget-object v1, p0, Lehu;->ay:[Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 1671
    const-string v0, "seen_public_dasher_warning"

    iget-boolean v1, p0, Lehu;->ar:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1672
    const-string v0, "mute_processed"

    iget-boolean v1, p0, Lehu;->ag:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1673
    const-string v0, "read_processed"

    iget-boolean v1, p0, Lehu;->aQ:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1674
    iget-object v0, p0, Lehu;->aR:Lhgw;

    if-eqz v0, :cond_2

    .line 1675
    const-string v0, "audience_data"

    iget-object v1, p0, Lehu;->aR:Lhgw;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1677
    :cond_2
    const-string v0, "blocked_gaia_ids"

    iget-object v1, p0, Lehu;->aS:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1678
    const-string v0, "show_keyboard"

    iget-boolean v1, p0, Lehu;->aZ:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1679
    const-string v0, "host_mode"

    iget v1, p0, Lehu;->bb:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1680
    const-string v0, "force_full_bleed"

    iget-boolean v1, p0, Lehu;->bc:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1681
    const-string v0, "max_span"

    iget v1, p0, Lehu;->be:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1682
    const-string v0, "can_comment"

    iget-boolean v1, p0, Lehu;->aN:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1683
    const-string v0, "is_post_restricted_to_domain"

    iget-boolean v1, p0, Lehu;->aO:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1684
    const-string v0, "scroll_to_last_comment_pending"

    iget-boolean v1, p0, Lehu;->bf:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1685
    iget-object v0, p0, Lehu;->ao:Lkzp;

    if-eqz v0, :cond_3

    .line 1687
    :try_start_0
    const-string v0, "db_context_specific_data"

    iget-object v1, p0, Lehu;->ao:Lkzp;

    .line 1688
    invoke-static {v1}, Lkzp;->a(Lkzp;)[B

    move-result-object v1

    .line 1687
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1694
    :cond_3
    :goto_0
    const-string v0, "popup_start_orientation"

    iget v1, p0, Lehu;->bw:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1696
    const-string v0, "popup_end_x"

    new-array v1, v5, [I

    iget-object v2, p0, Lehu;->bp:[I

    aget v2, v2, v3

    aput v2, v1, v3

    iget-object v2, p0, Lehu;->bp:[I

    aget v2, v2, v4

    aput v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1699
    const-string v0, "popup_end_y"

    new-array v1, v5, [I

    iget-object v2, p0, Lehu;->bq:[I

    aget v2, v2, v3

    aput v2, v1, v3

    iget-object v2, p0, Lehu;->bq:[I

    aget v2, v2, v4

    aput v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1702
    iget-object v0, p0, Lehu;->ab:Ldib;

    if-eqz v0, :cond_4

    .line 1703
    iget-object v0, p0, Lehu;->ab:Ldib;

    invoke-virtual {v0, p1}, Ldib;->a(Landroid/os/Bundle;)V

    .line 1706
    :cond_4
    iget-object v0, p0, Lehu;->bG:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1707
    const-string v0, "commentreply_authorname"

    iget-object v1, p0, Lehu;->bG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1708
    const-string v0, "commentreply_authorid"

    iget-object v1, p0, Lehu;->bF:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1710
    :cond_5
    return-void

    .line 1690
    :catch_0
    move-exception v0

    const-string v0, "stream1upfrag"

    const-string v1, "Failed to serialize mDbContextSpecificData"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public l(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2939
    iget-object v0, p0, Lehu;->bH:Lfcu;

    if-eqz v0, :cond_0

    .line 2940
    iget-object v0, p0, Lehu;->bH:Lfcu;

    invoke-virtual {v0, p1}, Lfcu;->l(Landroid/os/Bundle;)V

    .line 2942
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2771
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 2772
    const v1, 0x7f10029b

    if-ne v0, v1, :cond_3

    .line 2773
    iget-object v0, p0, Lehu;->T:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2774
    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v1, p0, Lehu;->T:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Llnl;->startActivity(Landroid/content/Intent;)V

    .line 2785
    :cond_0
    :goto_0
    return-void

    .line 2777
    :cond_1
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2778
    new-instance v0, Lkoe;

    const/16 v1, 0x5b

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehu;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lehu;->aH:Lkzu;

    const-string v0, "extra_activity_id"

    iget-object v2, p0, Lehu;->U:Ljava/lang/String;

    invoke-static {v0, v2}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iget-object v0, p0, Lehu;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lehu;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->S:Lhmv;

    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    invoke-virtual {v3, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v2

    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    iget-object v0, p0, Lehu;->at:Llnl;

    iget-object v2, p0, Lehu;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehu;->U:Ljava/lang/String;

    iget-object v4, p0, Lehu;->bg:[B

    invoke-static {v0, v2, v3, v1, v4}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehu;->ae:Ljava/lang/Integer;

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lehu;->d(I)V

    goto :goto_0

    .line 2780
    :cond_2
    iget-object v0, p0, Lehu;->aY:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 2782
    :cond_3
    const v1, 0x7f10029a

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lehu;->ai()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2783
    invoke-direct {p0}, Lehu;->ah()V

    goto :goto_0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 1534
    invoke-super {p0}, Llol;->z()V

    .line 1536
    iget-object v0, p0, Lehu;->Q:Lein;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 1538
    invoke-virtual {p0}, Lehu;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lfdp;->a(Landroid/view/View;)V

    .line 1541
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1542
    iget-object v0, p0, Lehu;->aX:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 1544
    :cond_0
    return-void
.end method
