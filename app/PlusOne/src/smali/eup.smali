.class public final Leup;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private N:Lhee;

.field private O:Ljmd;

.field private P:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Llol;-><init>()V

    .line 38
    new-instance v0, Lhmg;

    sget-object v1, Lona;->q:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Leup;->au:Llnh;

    .line 39
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 41
    new-instance v0, Lhmf;

    iget-object v1, p0, Leup;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 42
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 54
    const v0, 0x7f04012d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 56
    invoke-virtual {p0}, Leup;->q()Lae;

    move-result-object v0

    .line 57
    const-string v2, "onboarding_sul_content"

    invoke-virtual {v0, v2}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v2

    .line 59
    if-nez v2, :cond_0

    .line 60
    new-instance v2, Lejy;

    invoke-direct {v2}, Lejy;-><init>()V

    .line 61
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 62
    const-string v4, "ActionBarFragmentMixin.Enabled"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 63
    invoke-virtual {v2, v3}, Lu;->f(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    .line 66
    const v3, 0x7f1003bd

    const-string v4, "onboarding_sul_content"

    invoke-virtual {v0, v3, v2, v4}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    .line 67
    invoke-virtual {v0}, Lat;->b()I

    .line 70
    :cond_0
    const v0, 0x7f1003be

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leup;->P:Landroid/widget/TextView;

    .line 71
    iget-object v0, p0, Leup;->P:Landroid/widget/TextView;

    new-instance v2, Lhmi;

    invoke-direct {v2, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Leup;->P:Landroid/widget/TextView;

    new-instance v2, Lhmk;

    sget-object v3, Lona;->p:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 75
    return-object v1
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Leup;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Leup;->N:Lhee;

    .line 48
    iget-object v0, p0, Leup;->au:Llnh;

    const-class v1, Ljmd;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmd;

    iput-object v0, p0, Leup;->O:Ljmd;

    .line 49
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Leup;->P:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 81
    iget-object v0, p0, Leup;->at:Llnl;

    iget-object v1, p0, Leup;->N:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1}, Leuq;->a(Landroid/content/Context;I)V

    .line 82
    iget-object v0, p0, Leup;->O:Ljmd;

    invoke-interface {v0}, Ljmd;->i()V

    .line 84
    :cond_0
    return-void
.end method
