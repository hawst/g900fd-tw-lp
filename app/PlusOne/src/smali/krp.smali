.class public final Lkrp;
.super Landroid/os/Binder;
.source "PG"

# interfaces
.implements Lkrs;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/socialcast/impl/CastService;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    .locals 0

    .prologue
    .line 958
    iput-object p1, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 959
    return-void
.end method


# virtual methods
.method public a(Lkrl;)V
    .locals 1

    .prologue
    .line 1074
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->E(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1075
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 963
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->q(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z

    move-result v0

    return v0
.end method

.method public b(Lkrl;)V
    .locals 1

    .prologue
    .line 1079
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->E(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1080
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 968
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Liic;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->q(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    .line 969
    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->r(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 974
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->h(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z

    .line 975
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 979
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->s(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 984
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->t(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 989
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->u(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 994
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->v(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 999
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->w(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z

    move-result v0

    return v0
.end method

.method public i()Lizu;
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->x(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/google/android/libraries/social/media/MediaResource;
    .locals 1

    .prologue
    .line 1009
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->y(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1014
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->z(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1019
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->A(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1024
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->B(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->C(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 1034
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->D(Lcom/google/android/libraries/social/socialcast/impl/CastService;)I

    move-result v0

    return v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1039
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Liic;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Liic;

    move-result-object v0

    invoke-interface {v0}, Liic;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()V
    .locals 2

    .prologue
    .line 1044
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v1, "prev"

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)V

    .line 1045
    return-void
.end method

.method public r()V
    .locals 2

    .prologue
    .line 1049
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v1, "next"

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)V

    .line 1050
    return-void
.end method

.method public s()V
    .locals 2

    .prologue
    .line 1054
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v1, "play"

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)V

    .line 1055
    return-void
.end method

.method public t()V
    .locals 2

    .prologue
    .line 1059
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v1, "pause"

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)V

    .line 1060
    return-void
.end method

.method public u()V
    .locals 2

    .prologue
    .line 1064
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v1, "expand"

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)V

    .line 1065
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 1069
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-string v1, "collapse"

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)V

    .line 1070
    return-void
.end method

.method public w()Z
    .locals 3

    .prologue
    .line 1084
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    .line 1085
    invoke-virtual {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1086
    iget-object v1, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->D(Lcom/google/android/libraries/social/socialcast/impl/CastService;)I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 1087
    const-string v1, "socialcast_private_posts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public x()V
    .locals 1

    .prologue
    .line 1092
    iget-object v0, p0, Lkrp;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->p(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    .line 1093
    return-void
.end method
