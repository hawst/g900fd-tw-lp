.class public final Lptl;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lptk;

.field private b:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Loxq;-><init>()V

    .line 151
    sget-object v0, Lptk;->a:[Lptk;

    iput-object v0, p0, Lptl;->a:[Lptk;

    .line 148
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 174
    .line 175
    iget-object v1, p0, Lptl;->a:[Lptk;

    if-eqz v1, :cond_1

    .line 176
    iget-object v2, p0, Lptl;->a:[Lptk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 177
    if-eqz v4, :cond_0

    .line 178
    const/4 v5, 0x1

    .line 179
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 176
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 183
    :cond_1
    iget-object v1, p0, Lptl;->b:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 184
    const/4 v1, 0x2

    iget-object v2, p0, Lptl;->b:Ljava/lang/Float;

    .line 185
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 187
    :cond_2
    iget-object v1, p0, Lptl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    iput v0, p0, Lptl;->ai:I

    .line 189
    return v0
.end method

.method public a(Loxn;)Lptl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 197
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 198
    sparse-switch v0, :sswitch_data_0

    .line 202
    iget-object v2, p0, Lptl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 203
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lptl;->ah:Ljava/util/List;

    .line 206
    :cond_1
    iget-object v2, p0, Lptl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    :sswitch_0
    return-object p0

    .line 213
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 214
    iget-object v0, p0, Lptl;->a:[Lptk;

    if-nez v0, :cond_3

    move v0, v1

    .line 215
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lptk;

    .line 216
    iget-object v3, p0, Lptl;->a:[Lptk;

    if-eqz v3, :cond_2

    .line 217
    iget-object v3, p0, Lptl;->a:[Lptk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 219
    :cond_2
    iput-object v2, p0, Lptl;->a:[Lptk;

    .line 220
    :goto_2
    iget-object v2, p0, Lptl;->a:[Lptk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 221
    iget-object v2, p0, Lptl;->a:[Lptk;

    new-instance v3, Lptk;

    invoke-direct {v3}, Lptk;-><init>()V

    aput-object v3, v2, v0

    .line 222
    iget-object v2, p0, Lptl;->a:[Lptk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 223
    invoke-virtual {p1}, Loxn;->a()I

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 214
    :cond_3
    iget-object v0, p0, Lptl;->a:[Lptk;

    array-length v0, v0

    goto :goto_1

    .line 226
    :cond_4
    iget-object v2, p0, Lptl;->a:[Lptk;

    new-instance v3, Lptk;

    invoke-direct {v3}, Lptk;-><init>()V

    aput-object v3, v2, v0

    .line 227
    iget-object v2, p0, Lptl;->a:[Lptk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 231
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lptl;->b:Ljava/lang/Float;

    goto :goto_0

    .line 198
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 158
    iget-object v0, p0, Lptl;->a:[Lptk;

    if-eqz v0, :cond_1

    .line 159
    iget-object v1, p0, Lptl;->a:[Lptk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 160
    if-eqz v3, :cond_0

    .line 161
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 159
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_1
    iget-object v0, p0, Lptl;->b:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 166
    const/4 v0, 0x2

    iget-object v1, p0, Lptl;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 168
    :cond_2
    iget-object v0, p0, Lptl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 170
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0, p1}, Lptl;->a(Loxn;)Lptl;

    move-result-object v0

    return-object v0
.end method
