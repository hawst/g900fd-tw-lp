.class final Ldbe;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lnyi;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field private final b:Landroid/view/View$OnClickListener;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 190
    iput-object p2, p0, Ldbe;->a:Landroid/view/View$OnClickListener;

    .line 191
    iput-object p3, p0, Ldbe;->b:Landroid/view/View$OnClickListener;

    .line 192
    iput-object p1, p0, Ldbe;->c:Landroid/content/Context;

    .line 193
    return-void
.end method


# virtual methods
.method public a([Lnyi;)V
    .locals 4

    .prologue
    .line 197
    invoke-virtual {p0}, Ldbe;->clear()V

    .line 198
    if-eqz p1, :cond_1

    .line 199
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 200
    if-eqz v2, :cond_0

    iget-object v3, v2, Lnyi;->c:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 201
    invoke-virtual {p0, v2}, Ldbe;->add(Ljava/lang/Object;)V

    .line 199
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_1
    invoke-virtual {p0}, Ldbe;->notifyDataSetChanged()V

    .line 206
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 210
    if-nez p2, :cond_0

    .line 211
    iget-object v0, p0, Ldbe;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040185

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 213
    :cond_0
    invoke-virtual {p0, p1}, Ldbe;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnyi;

    .line 214
    const v1, 0x7f1004be

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 215
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 216
    iget-object v2, p0, Ldbe;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    const v1, 0x7f100138

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 218
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 219
    iget-object v0, v0, Lnyi;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v0, p0, Ldbe;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    return-object p2
.end method
