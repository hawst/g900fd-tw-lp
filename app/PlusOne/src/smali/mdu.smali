.class public final Lmdu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpeu;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3319
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3322
    iput-object v0, p0, Lmdu;->apiHeader:Llyq;

    .line 3325
    iput-object v0, p0, Lmdu;->a:Lpeu;

    .line 3319
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3342
    const/4 v0, 0x0

    .line 3343
    iget-object v1, p0, Lmdu;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 3344
    const/4 v0, 0x1

    iget-object v1, p0, Lmdu;->apiHeader:Llyq;

    .line 3345
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3347
    :cond_0
    iget-object v1, p0, Lmdu;->a:Lpeu;

    if-eqz v1, :cond_1

    .line 3348
    const/4 v1, 0x2

    iget-object v2, p0, Lmdu;->a:Lpeu;

    .line 3349
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3351
    :cond_1
    iget-object v1, p0, Lmdu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3352
    iput v0, p0, Lmdu;->ai:I

    .line 3353
    return v0
.end method

.method public a(Loxn;)Lmdu;
    .locals 2

    .prologue
    .line 3361
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3362
    sparse-switch v0, :sswitch_data_0

    .line 3366
    iget-object v1, p0, Lmdu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3367
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdu;->ah:Ljava/util/List;

    .line 3370
    :cond_1
    iget-object v1, p0, Lmdu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3372
    :sswitch_0
    return-object p0

    .line 3377
    :sswitch_1
    iget-object v0, p0, Lmdu;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 3378
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmdu;->apiHeader:Llyq;

    .line 3380
    :cond_2
    iget-object v0, p0, Lmdu;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3384
    :sswitch_2
    iget-object v0, p0, Lmdu;->a:Lpeu;

    if-nez v0, :cond_3

    .line 3385
    new-instance v0, Lpeu;

    invoke-direct {v0}, Lpeu;-><init>()V

    iput-object v0, p0, Lmdu;->a:Lpeu;

    .line 3387
    :cond_3
    iget-object v0, p0, Lmdu;->a:Lpeu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3362
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3330
    iget-object v0, p0, Lmdu;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 3331
    const/4 v0, 0x1

    iget-object v1, p0, Lmdu;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3333
    :cond_0
    iget-object v0, p0, Lmdu;->a:Lpeu;

    if-eqz v0, :cond_1

    .line 3334
    const/4 v0, 0x2

    iget-object v1, p0, Lmdu;->a:Lpeu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3336
    :cond_1
    iget-object v0, p0, Lmdu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3338
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3315
    invoke-virtual {p0, p1}, Lmdu;->a(Loxn;)Lmdu;

    move-result-object v0

    return-object v0
.end method
