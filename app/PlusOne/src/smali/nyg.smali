.class public final Lnyg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Float;

.field public e:Ljava/lang/Float;

.field public f:Ljava/lang/Float;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Long;

.field public i:Ljava/lang/Integer;

.field private j:Ljava/lang/Float;

.field private k:Ljava/lang/Long;

.field private l:Ljava/lang/Float;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/Integer;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6580
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 6686
    const/4 v0, 0x0

    .line 6687
    iget-object v1, p0, Lnyg;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6688
    const/4 v0, 0x1

    iget-object v1, p0, Lnyg;->a:Ljava/lang/String;

    .line 6689
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6691
    :cond_0
    iget-object v1, p0, Lnyg;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 6692
    const/4 v1, 0x2

    iget-object v2, p0, Lnyg;->b:Ljava/lang/String;

    .line 6693
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6695
    :cond_1
    iget-object v1, p0, Lnyg;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 6696
    const/4 v1, 0x3

    iget-object v2, p0, Lnyg;->c:Ljava/lang/Integer;

    .line 6697
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6699
    :cond_2
    iget-object v1, p0, Lnyg;->d:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 6700
    const/4 v1, 0x4

    iget-object v2, p0, Lnyg;->d:Ljava/lang/Float;

    .line 6701
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6703
    :cond_3
    iget-object v1, p0, Lnyg;->e:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 6704
    const/4 v1, 0x5

    iget-object v2, p0, Lnyg;->e:Ljava/lang/Float;

    .line 6705
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6707
    :cond_4
    iget-object v1, p0, Lnyg;->j:Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 6708
    const/4 v1, 0x6

    iget-object v2, p0, Lnyg;->j:Ljava/lang/Float;

    .line 6709
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6711
    :cond_5
    iget-object v1, p0, Lnyg;->k:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 6712
    const/4 v1, 0x7

    iget-object v2, p0, Lnyg;->k:Ljava/lang/Long;

    .line 6713
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6715
    :cond_6
    iget-object v1, p0, Lnyg;->f:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 6716
    const/16 v1, 0x8

    iget-object v2, p0, Lnyg;->f:Ljava/lang/Float;

    .line 6717
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6719
    :cond_7
    iget-object v1, p0, Lnyg;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 6720
    const/16 v1, 0x9

    iget-object v2, p0, Lnyg;->g:Ljava/lang/Boolean;

    .line 6721
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6723
    :cond_8
    iget-object v1, p0, Lnyg;->l:Ljava/lang/Float;

    if-eqz v1, :cond_9

    .line 6724
    const/16 v1, 0xa

    iget-object v2, p0, Lnyg;->l:Ljava/lang/Float;

    .line 6725
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6727
    :cond_9
    iget-object v1, p0, Lnyg;->m:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 6728
    const/16 v1, 0xb

    iget-object v2, p0, Lnyg;->m:Ljava/lang/String;

    .line 6729
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6731
    :cond_a
    iget-object v1, p0, Lnyg;->h:Ljava/lang/Long;

    if-eqz v1, :cond_b

    .line 6732
    const/16 v1, 0xc

    iget-object v2, p0, Lnyg;->h:Ljava/lang/Long;

    .line 6733
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6735
    :cond_b
    iget-object v1, p0, Lnyg;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 6736
    const/16 v1, 0xd

    iget-object v2, p0, Lnyg;->n:Ljava/lang/Integer;

    .line 6737
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6739
    :cond_c
    iget-object v1, p0, Lnyg;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 6740
    const/16 v1, 0xe

    iget-object v2, p0, Lnyg;->o:Ljava/lang/Integer;

    .line 6741
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6743
    :cond_d
    iget-object v1, p0, Lnyg;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 6744
    const/16 v1, 0xf

    iget-object v2, p0, Lnyg;->i:Ljava/lang/Integer;

    .line 6745
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6747
    :cond_e
    iget-object v1, p0, Lnyg;->p:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 6748
    const/16 v1, 0x10

    iget-object v2, p0, Lnyg;->p:Ljava/lang/String;

    .line 6749
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6751
    :cond_f
    iget-object v1, p0, Lnyg;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 6752
    const/16 v1, 0x11

    iget-object v2, p0, Lnyg;->q:Ljava/lang/String;

    .line 6753
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6755
    :cond_10
    iget-object v1, p0, Lnyg;->r:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 6756
    const/16 v1, 0x12

    iget-object v2, p0, Lnyg;->r:Ljava/lang/String;

    .line 6757
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6759
    :cond_11
    iget-object v1, p0, Lnyg;->s:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 6760
    const/16 v1, 0x13

    iget-object v2, p0, Lnyg;->s:Ljava/lang/String;

    .line 6761
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6763
    :cond_12
    iget-object v1, p0, Lnyg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6764
    iput v0, p0, Lnyg;->ai:I

    .line 6765
    return v0
.end method

.method public a(Loxn;)Lnyg;
    .locals 2

    .prologue
    .line 6773
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6774
    sparse-switch v0, :sswitch_data_0

    .line 6778
    iget-object v1, p0, Lnyg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6779
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyg;->ah:Ljava/util/List;

    .line 6782
    :cond_1
    iget-object v1, p0, Lnyg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6784
    :sswitch_0
    return-object p0

    .line 6789
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyg;->a:Ljava/lang/String;

    goto :goto_0

    .line 6793
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyg;->b:Ljava/lang/String;

    goto :goto_0

    .line 6797
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyg;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 6801
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lnyg;->d:Ljava/lang/Float;

    goto :goto_0

    .line 6805
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lnyg;->e:Ljava/lang/Float;

    goto :goto_0

    .line 6809
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lnyg;->j:Ljava/lang/Float;

    goto :goto_0

    .line 6813
    :sswitch_7
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnyg;->k:Ljava/lang/Long;

    goto :goto_0

    .line 6817
    :sswitch_8
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lnyg;->f:Ljava/lang/Float;

    goto :goto_0

    .line 6821
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnyg;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 6825
    :sswitch_a
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lnyg;->l:Ljava/lang/Float;

    goto :goto_0

    .line 6829
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyg;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 6833
    :sswitch_c
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnyg;->h:Ljava/lang/Long;

    goto/16 :goto_0

    .line 6837
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyg;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6841
    :sswitch_e
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyg;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6845
    :sswitch_f
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyg;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6849
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyg;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 6853
    :sswitch_11
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyg;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 6857
    :sswitch_12
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyg;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 6861
    :sswitch_13
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyg;->s:Ljava/lang/String;

    goto/16 :goto_0

    .line 6774
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x38 -> :sswitch_7
        0x45 -> :sswitch_8
        0x48 -> :sswitch_9
        0x55 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 6623
    iget-object v0, p0, Lnyg;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 6624
    const/4 v0, 0x1

    iget-object v1, p0, Lnyg;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6626
    :cond_0
    iget-object v0, p0, Lnyg;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 6627
    const/4 v0, 0x2

    iget-object v1, p0, Lnyg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6629
    :cond_1
    iget-object v0, p0, Lnyg;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 6630
    const/4 v0, 0x3

    iget-object v1, p0, Lnyg;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6632
    :cond_2
    iget-object v0, p0, Lnyg;->d:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 6633
    const/4 v0, 0x4

    iget-object v1, p0, Lnyg;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6635
    :cond_3
    iget-object v0, p0, Lnyg;->e:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 6636
    const/4 v0, 0x5

    iget-object v1, p0, Lnyg;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6638
    :cond_4
    iget-object v0, p0, Lnyg;->j:Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 6639
    const/4 v0, 0x6

    iget-object v1, p0, Lnyg;->j:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6641
    :cond_5
    iget-object v0, p0, Lnyg;->k:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 6642
    const/4 v0, 0x7

    iget-object v1, p0, Lnyg;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 6644
    :cond_6
    iget-object v0, p0, Lnyg;->f:Ljava/lang/Float;

    if-eqz v0, :cond_7

    .line 6645
    const/16 v0, 0x8

    iget-object v1, p0, Lnyg;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6647
    :cond_7
    iget-object v0, p0, Lnyg;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 6648
    const/16 v0, 0x9

    iget-object v1, p0, Lnyg;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 6650
    :cond_8
    iget-object v0, p0, Lnyg;->l:Ljava/lang/Float;

    if-eqz v0, :cond_9

    .line 6651
    const/16 v0, 0xa

    iget-object v1, p0, Lnyg;->l:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6653
    :cond_9
    iget-object v0, p0, Lnyg;->m:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 6654
    const/16 v0, 0xb

    iget-object v1, p0, Lnyg;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6656
    :cond_a
    iget-object v0, p0, Lnyg;->h:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 6657
    const/16 v0, 0xc

    iget-object v1, p0, Lnyg;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 6659
    :cond_b
    iget-object v0, p0, Lnyg;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 6660
    const/16 v0, 0xd

    iget-object v1, p0, Lnyg;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6662
    :cond_c
    iget-object v0, p0, Lnyg;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 6663
    const/16 v0, 0xe

    iget-object v1, p0, Lnyg;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6665
    :cond_d
    iget-object v0, p0, Lnyg;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 6666
    const/16 v0, 0xf

    iget-object v1, p0, Lnyg;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6668
    :cond_e
    iget-object v0, p0, Lnyg;->p:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 6669
    const/16 v0, 0x10

    iget-object v1, p0, Lnyg;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6671
    :cond_f
    iget-object v0, p0, Lnyg;->q:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 6672
    const/16 v0, 0x11

    iget-object v1, p0, Lnyg;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6674
    :cond_10
    iget-object v0, p0, Lnyg;->r:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 6675
    const/16 v0, 0x12

    iget-object v1, p0, Lnyg;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6677
    :cond_11
    iget-object v0, p0, Lnyg;->s:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 6678
    const/16 v0, 0x13

    iget-object v1, p0, Lnyg;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6680
    :cond_12
    iget-object v0, p0, Lnyg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6682
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6576
    invoke-virtual {p0, p1}, Lnyg;->a(Loxn;)Lnyg;

    move-result-object v0

    return-object v0
.end method
