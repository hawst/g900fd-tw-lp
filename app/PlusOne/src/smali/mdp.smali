.class public final Lmdp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlr;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1456
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1459
    iput-object v0, p0, Lmdp;->apiHeader:Llyr;

    .line 1462
    iput-object v0, p0, Lmdp;->a:Lnlr;

    .line 1456
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1479
    const/4 v0, 0x0

    .line 1480
    iget-object v1, p0, Lmdp;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 1481
    const/4 v0, 0x1

    iget-object v1, p0, Lmdp;->apiHeader:Llyr;

    .line 1482
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1484
    :cond_0
    iget-object v1, p0, Lmdp;->a:Lnlr;

    if-eqz v1, :cond_1

    .line 1485
    const/4 v1, 0x2

    iget-object v2, p0, Lmdp;->a:Lnlr;

    .line 1486
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1488
    :cond_1
    iget-object v1, p0, Lmdp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1489
    iput v0, p0, Lmdp;->ai:I

    .line 1490
    return v0
.end method

.method public a(Loxn;)Lmdp;
    .locals 2

    .prologue
    .line 1498
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1499
    sparse-switch v0, :sswitch_data_0

    .line 1503
    iget-object v1, p0, Lmdp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1504
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdp;->ah:Ljava/util/List;

    .line 1507
    :cond_1
    iget-object v1, p0, Lmdp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1509
    :sswitch_0
    return-object p0

    .line 1514
    :sswitch_1
    iget-object v0, p0, Lmdp;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 1515
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmdp;->apiHeader:Llyr;

    .line 1517
    :cond_2
    iget-object v0, p0, Lmdp;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1521
    :sswitch_2
    iget-object v0, p0, Lmdp;->a:Lnlr;

    if-nez v0, :cond_3

    .line 1522
    new-instance v0, Lnlr;

    invoke-direct {v0}, Lnlr;-><init>()V

    iput-object v0, p0, Lmdp;->a:Lnlr;

    .line 1524
    :cond_3
    iget-object v0, p0, Lmdp;->a:Lnlr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1499
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1467
    iget-object v0, p0, Lmdp;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 1468
    const/4 v0, 0x1

    iget-object v1, p0, Lmdp;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1470
    :cond_0
    iget-object v0, p0, Lmdp;->a:Lnlr;

    if-eqz v0, :cond_1

    .line 1471
    const/4 v0, 0x2

    iget-object v1, p0, Lmdp;->a:Lnlr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1473
    :cond_1
    iget-object v0, p0, Lmdp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1475
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1452
    invoke-virtual {p0, p1}, Lmdp;->a(Loxn;)Lmdp;

    move-result-object v0

    return-object v0
.end method
