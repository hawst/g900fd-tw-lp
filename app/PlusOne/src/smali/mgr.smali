.class public final Lmgr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfp;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25594
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25597
    iput-object v0, p0, Lmgr;->apiHeader:Llyr;

    .line 25600
    iput-object v0, p0, Lmgr;->a:Lnfp;

    .line 25594
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 25617
    const/4 v0, 0x0

    .line 25618
    iget-object v1, p0, Lmgr;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 25619
    const/4 v0, 0x1

    iget-object v1, p0, Lmgr;->apiHeader:Llyr;

    .line 25620
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25622
    :cond_0
    iget-object v1, p0, Lmgr;->a:Lnfp;

    if-eqz v1, :cond_1

    .line 25623
    const/4 v1, 0x2

    iget-object v2, p0, Lmgr;->a:Lnfp;

    .line 25624
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25626
    :cond_1
    iget-object v1, p0, Lmgr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25627
    iput v0, p0, Lmgr;->ai:I

    .line 25628
    return v0
.end method

.method public a(Loxn;)Lmgr;
    .locals 2

    .prologue
    .line 25636
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 25637
    sparse-switch v0, :sswitch_data_0

    .line 25641
    iget-object v1, p0, Lmgr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 25642
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgr;->ah:Ljava/util/List;

    .line 25645
    :cond_1
    iget-object v1, p0, Lmgr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25647
    :sswitch_0
    return-object p0

    .line 25652
    :sswitch_1
    iget-object v0, p0, Lmgr;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 25653
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgr;->apiHeader:Llyr;

    .line 25655
    :cond_2
    iget-object v0, p0, Lmgr;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25659
    :sswitch_2
    iget-object v0, p0, Lmgr;->a:Lnfp;

    if-nez v0, :cond_3

    .line 25660
    new-instance v0, Lnfp;

    invoke-direct {v0}, Lnfp;-><init>()V

    iput-object v0, p0, Lmgr;->a:Lnfp;

    .line 25662
    :cond_3
    iget-object v0, p0, Lmgr;->a:Lnfp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25637
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25605
    iget-object v0, p0, Lmgr;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 25606
    const/4 v0, 0x1

    iget-object v1, p0, Lmgr;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25608
    :cond_0
    iget-object v0, p0, Lmgr;->a:Lnfp;

    if-eqz v0, :cond_1

    .line 25609
    const/4 v0, 0x2

    iget-object v1, p0, Lmgr;->a:Lnfp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25611
    :cond_1
    iget-object v0, p0, Lmgr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 25613
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 25590
    invoke-virtual {p0, p1}, Lmgr;->a(Loxn;)Lmgr;

    move-result-object v0

    return-object v0
.end method
