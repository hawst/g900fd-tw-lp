.class public final Lpwk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpwk;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:I

.field private g:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    new-array v0, v0, [Lpwk;

    sput-object v0, Lpwk;->a:[Lpwk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33
    const/high16 v0, -0x80000000

    iput v0, p0, Lpwk;->f:I

    .line 16
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Lpwk;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 66
    const/4 v0, 0x1

    iget-object v1, p0, Lpwk;->b:Ljava/lang/String;

    .line 67
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 69
    :cond_0
    iget-object v1, p0, Lpwk;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 70
    const/4 v1, 0x2

    iget-object v2, p0, Lpwk;->c:Ljava/lang/String;

    .line 71
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_1
    iget-object v1, p0, Lpwk;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 74
    const/4 v1, 0x3

    iget-object v2, p0, Lpwk;->d:Ljava/lang/Boolean;

    .line 75
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 77
    :cond_2
    iget-object v1, p0, Lpwk;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 78
    const/4 v1, 0x4

    iget-object v2, p0, Lpwk;->e:Ljava/lang/Boolean;

    .line 79
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 81
    :cond_3
    iget v1, p0, Lpwk;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 82
    const/4 v1, 0x5

    iget v2, p0, Lpwk;->f:I

    .line 83
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_4
    iget-object v1, p0, Lpwk;->g:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 86
    const/4 v1, 0x6

    iget-object v2, p0, Lpwk;->g:Ljava/lang/Long;

    .line 87
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_5
    iget-object v1, p0, Lpwk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    iput v0, p0, Lpwk;->ai:I

    .line 91
    return v0
.end method

.method public a(Loxn;)Lpwk;
    .locals 2

    .prologue
    .line 99
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 100
    sparse-switch v0, :sswitch_data_0

    .line 104
    iget-object v1, p0, Lpwk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpwk;->ah:Ljava/util/List;

    .line 108
    :cond_1
    iget-object v1, p0, Lpwk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    :sswitch_0
    return-object p0

    .line 115
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpwk;->b:Ljava/lang/String;

    goto :goto_0

    .line 119
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpwk;->c:Ljava/lang/String;

    goto :goto_0

    .line 123
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpwk;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 127
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpwk;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 131
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 132
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 135
    :cond_2
    iput v0, p0, Lpwk;->f:I

    goto :goto_0

    .line 137
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpwk;->f:I

    goto :goto_0

    .line 142
    :sswitch_6
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpwk;->g:Ljava/lang/Long;

    goto :goto_0

    .line 100
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 40
    iget-object v0, p0, Lpwk;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x1

    iget-object v1, p0, Lpwk;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 43
    :cond_0
    iget-object v0, p0, Lpwk;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 44
    const/4 v0, 0x2

    iget-object v1, p0, Lpwk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    :cond_1
    iget-object v0, p0, Lpwk;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 47
    const/4 v0, 0x3

    iget-object v1, p0, Lpwk;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 49
    :cond_2
    iget-object v0, p0, Lpwk;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 50
    const/4 v0, 0x4

    iget-object v1, p0, Lpwk;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 52
    :cond_3
    iget v0, p0, Lpwk;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 53
    const/4 v0, 0x5

    iget v1, p0, Lpwk;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 55
    :cond_4
    iget-object v0, p0, Lpwk;->g:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 56
    const/4 v0, 0x6

    iget-object v1, p0, Lpwk;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 58
    :cond_5
    iget-object v0, p0, Lpwk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 60
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lpwk;->a(Loxn;)Lpwk;

    move-result-object v0

    return-object v0
.end method
