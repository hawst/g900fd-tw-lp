.class public final Lnic;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnid;

.field private b:Lnid;

.field private c:Lnid;

.field private d:Lnid;

.field private e:Lnid;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7687
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7690
    iput-object v0, p0, Lnic;->a:Lnid;

    .line 7693
    iput-object v0, p0, Lnic;->b:Lnid;

    .line 7696
    iput-object v0, p0, Lnic;->c:Lnid;

    .line 7699
    iput-object v0, p0, Lnic;->d:Lnid;

    .line 7702
    iput-object v0, p0, Lnic;->e:Lnid;

    .line 7687
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7728
    const/4 v0, 0x0

    .line 7729
    iget-object v1, p0, Lnic;->a:Lnid;

    if-eqz v1, :cond_0

    .line 7730
    const/4 v0, 0x1

    iget-object v1, p0, Lnic;->a:Lnid;

    .line 7731
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7733
    :cond_0
    iget-object v1, p0, Lnic;->b:Lnid;

    if-eqz v1, :cond_1

    .line 7734
    const/4 v1, 0x2

    iget-object v2, p0, Lnic;->b:Lnid;

    .line 7735
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7737
    :cond_1
    iget-object v1, p0, Lnic;->c:Lnid;

    if-eqz v1, :cond_2

    .line 7738
    const/4 v1, 0x3

    iget-object v2, p0, Lnic;->c:Lnid;

    .line 7739
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7741
    :cond_2
    iget-object v1, p0, Lnic;->d:Lnid;

    if-eqz v1, :cond_3

    .line 7742
    const/4 v1, 0x4

    iget-object v2, p0, Lnic;->d:Lnid;

    .line 7743
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7745
    :cond_3
    iget-object v1, p0, Lnic;->e:Lnid;

    if-eqz v1, :cond_4

    .line 7746
    const/4 v1, 0x5

    iget-object v2, p0, Lnic;->e:Lnid;

    .line 7747
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7749
    :cond_4
    iget-object v1, p0, Lnic;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7750
    iput v0, p0, Lnic;->ai:I

    .line 7751
    return v0
.end method

.method public a(Loxn;)Lnic;
    .locals 2

    .prologue
    .line 7759
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7760
    sparse-switch v0, :sswitch_data_0

    .line 7764
    iget-object v1, p0, Lnic;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7765
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnic;->ah:Ljava/util/List;

    .line 7768
    :cond_1
    iget-object v1, p0, Lnic;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7770
    :sswitch_0
    return-object p0

    .line 7775
    :sswitch_1
    iget-object v0, p0, Lnic;->a:Lnid;

    if-nez v0, :cond_2

    .line 7776
    new-instance v0, Lnid;

    invoke-direct {v0}, Lnid;-><init>()V

    iput-object v0, p0, Lnic;->a:Lnid;

    .line 7778
    :cond_2
    iget-object v0, p0, Lnic;->a:Lnid;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7782
    :sswitch_2
    iget-object v0, p0, Lnic;->b:Lnid;

    if-nez v0, :cond_3

    .line 7783
    new-instance v0, Lnid;

    invoke-direct {v0}, Lnid;-><init>()V

    iput-object v0, p0, Lnic;->b:Lnid;

    .line 7785
    :cond_3
    iget-object v0, p0, Lnic;->b:Lnid;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7789
    :sswitch_3
    iget-object v0, p0, Lnic;->c:Lnid;

    if-nez v0, :cond_4

    .line 7790
    new-instance v0, Lnid;

    invoke-direct {v0}, Lnid;-><init>()V

    iput-object v0, p0, Lnic;->c:Lnid;

    .line 7792
    :cond_4
    iget-object v0, p0, Lnic;->c:Lnid;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7796
    :sswitch_4
    iget-object v0, p0, Lnic;->d:Lnid;

    if-nez v0, :cond_5

    .line 7797
    new-instance v0, Lnid;

    invoke-direct {v0}, Lnid;-><init>()V

    iput-object v0, p0, Lnic;->d:Lnid;

    .line 7799
    :cond_5
    iget-object v0, p0, Lnic;->d:Lnid;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7803
    :sswitch_5
    iget-object v0, p0, Lnic;->e:Lnid;

    if-nez v0, :cond_6

    .line 7804
    new-instance v0, Lnid;

    invoke-direct {v0}, Lnid;-><init>()V

    iput-object v0, p0, Lnic;->e:Lnid;

    .line 7806
    :cond_6
    iget-object v0, p0, Lnic;->e:Lnid;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7760
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7707
    iget-object v0, p0, Lnic;->a:Lnid;

    if-eqz v0, :cond_0

    .line 7708
    const/4 v0, 0x1

    iget-object v1, p0, Lnic;->a:Lnid;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7710
    :cond_0
    iget-object v0, p0, Lnic;->b:Lnid;

    if-eqz v0, :cond_1

    .line 7711
    const/4 v0, 0x2

    iget-object v1, p0, Lnic;->b:Lnid;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7713
    :cond_1
    iget-object v0, p0, Lnic;->c:Lnid;

    if-eqz v0, :cond_2

    .line 7714
    const/4 v0, 0x3

    iget-object v1, p0, Lnic;->c:Lnid;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7716
    :cond_2
    iget-object v0, p0, Lnic;->d:Lnid;

    if-eqz v0, :cond_3

    .line 7717
    const/4 v0, 0x4

    iget-object v1, p0, Lnic;->d:Lnid;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7719
    :cond_3
    iget-object v0, p0, Lnic;->e:Lnid;

    if-eqz v0, :cond_4

    .line 7720
    const/4 v0, 0x5

    iget-object v1, p0, Lnic;->e:Lnid;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7722
    :cond_4
    iget-object v0, p0, Lnic;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7724
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7683
    invoke-virtual {p0, p1}, Lnic;->a(Loxn;)Lnic;

    move-result-object v0

    return-object v0
.end method
