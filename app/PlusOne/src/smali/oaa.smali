.class public final Loaa;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loaa;


# instance fields
.field private b:Ljava/lang/Double;

.field private c:Ljava/lang/String;

.field private d:[Load;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x0

    new-array v0, v0, [Loaa;

    sput-object v0, Loaa;->a:[Loaa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 422
    invoke-direct {p0}, Loxq;-><init>()V

    .line 429
    sget-object v0, Load;->a:[Load;

    iput-object v0, p0, Loaa;->d:[Load;

    .line 422
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    .line 451
    const/4 v0, 0x1

    iget-object v1, p0, Loaa;->b:Ljava/lang/Double;

    .line 453
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 454
    iget-object v1, p0, Loaa;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 455
    const/4 v1, 0x2

    iget-object v2, p0, Loaa;->c:Ljava/lang/String;

    .line 456
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 458
    :cond_0
    iget-object v1, p0, Loaa;->d:[Load;

    if-eqz v1, :cond_2

    .line 459
    iget-object v2, p0, Loaa;->d:[Load;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 460
    if-eqz v4, :cond_1

    .line 461
    const/4 v5, 0x3

    .line 462
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 459
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 466
    :cond_2
    iget-object v1, p0, Loaa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    iput v0, p0, Loaa;->ai:I

    .line 468
    return v0
.end method

.method public a(Loxn;)Loaa;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 476
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 477
    sparse-switch v0, :sswitch_data_0

    .line 481
    iget-object v2, p0, Loaa;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 482
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loaa;->ah:Ljava/util/List;

    .line 485
    :cond_1
    iget-object v2, p0, Loaa;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 487
    :sswitch_0
    return-object p0

    .line 492
    :sswitch_1
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Loaa;->b:Ljava/lang/Double;

    goto :goto_0

    .line 496
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loaa;->c:Ljava/lang/String;

    goto :goto_0

    .line 500
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 501
    iget-object v0, p0, Loaa;->d:[Load;

    if-nez v0, :cond_3

    move v0, v1

    .line 502
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Load;

    .line 503
    iget-object v3, p0, Loaa;->d:[Load;

    if-eqz v3, :cond_2

    .line 504
    iget-object v3, p0, Loaa;->d:[Load;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 506
    :cond_2
    iput-object v2, p0, Loaa;->d:[Load;

    .line 507
    :goto_2
    iget-object v2, p0, Loaa;->d:[Load;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 508
    iget-object v2, p0, Loaa;->d:[Load;

    new-instance v3, Load;

    invoke-direct {v3}, Load;-><init>()V

    aput-object v3, v2, v0

    .line 509
    iget-object v2, p0, Loaa;->d:[Load;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 510
    invoke-virtual {p1}, Loxn;->a()I

    .line 507
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 501
    :cond_3
    iget-object v0, p0, Loaa;->d:[Load;

    array-length v0, v0

    goto :goto_1

    .line 513
    :cond_4
    iget-object v2, p0, Loaa;->d:[Load;

    new-instance v3, Load;

    invoke-direct {v3}, Load;-><init>()V

    aput-object v3, v2, v0

    .line 514
    iget-object v2, p0, Loaa;->d:[Load;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 477
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 434
    const/4 v0, 0x1

    iget-object v1, p0, Loaa;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 435
    iget-object v0, p0, Loaa;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 436
    const/4 v0, 0x2

    iget-object v1, p0, Loaa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 438
    :cond_0
    iget-object v0, p0, Loaa;->d:[Load;

    if-eqz v0, :cond_2

    .line 439
    iget-object v1, p0, Loaa;->d:[Load;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 440
    if-eqz v3, :cond_1

    .line 441
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 439
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 445
    :cond_2
    iget-object v0, p0, Loaa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 447
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 418
    invoke-virtual {p0, p1}, Loaa;->a(Loxn;)Loaa;

    move-result-object v0

    return-object v0
.end method
