.class public final Llan;
.super Lllq;
.source "PG"

# interfaces
.implements Llja;


# instance fields
.field public a:Landroid/text/Spanned;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llag;",
            ">;"
        }
    .end annotation
.end field

.field private c:Llag;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lnrp;)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 42
    invoke-direct {p0}, Lllq;-><init>()V

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 44
    iget-object v0, p2, Lnrp;->c:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v3

    .line 47
    iget-object v6, p2, Lnrp;->e:Ljava/lang/String;

    .line 50
    iget-object v0, p2, Lnrp;->b:Lohv;

    if-eqz v0, :cond_d

    iget-object v0, p2, Lnrp;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    if-eqz v0, :cond_d

    .line 51
    iget-object v0, p2, Lnrp;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    .line 54
    :goto_0
    iget-object v2, p2, Lnrp;->b:Lohv;

    if-eqz v2, :cond_0

    iget-object v2, p2, Lnrp;->b:Lohv;

    iget-object v2, v2, Lohv;->c:Lohq;

    :goto_1
    invoke-static {v0, v2}, Llan;->a(Ljava/lang/String;Lohq;)Llag;

    move-result-object v2

    .line 57
    iget-object v0, p2, Lnrp;->b:Lohv;

    if-eqz v0, :cond_c

    .line 58
    iget-object v0, p2, Lnrp;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    if-eqz v0, :cond_6

    iget-object v5, v0, Lohp;->d:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "g:"

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v5, v0

    .line 61
    :goto_3
    iget-object v0, p2, Lnrp;->b:Lohv;

    if-eqz v0, :cond_7

    iget-object v0, p2, Lnrp;->b:Lohv;

    iget-object v0, v0, Lohv;->c:Lohq;

    :goto_4
    iget-object v8, p2, Lnrp;->d:[Lohv;

    .line 62
    invoke-static {p1, v3, v0, v8}, Llax;->a(Landroid/content/Context;ILohq;[Lohv;)Ljava/lang/String;

    move-result-object v3

    .line 66
    iget-object v0, p2, Lnrp;->d:[Lohv;

    if-eqz v0, :cond_8

    .line 67
    iget-object v9, p2, Lnrp;->d:[Lohv;

    array-length v10, v9

    move v8, v7

    :goto_5
    if-ge v8, v10, :cond_8

    aget-object v11, v9, v8

    .line 70
    iget-object v0, v11, Lohv;->b:Lohp;

    if-eqz v0, :cond_b

    .line 71
    iget-object v0, v11, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    .line 74
    :goto_6
    iget-object v11, v11, Lohv;->c:Lohq;

    invoke-static {v0, v11}, Llan;->a(Ljava/lang/String;Lohq;)Llag;

    move-result-object v0

    .line 75
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_5

    :cond_0
    move-object v2, v4

    .line 54
    goto :goto_1

    .line 58
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    iget-object v5, v0, Lohp;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "e:"

    iget-object v0, v0, Lohp;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    iget-object v5, v0, Lohp;->e:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "p:"

    iget-object v0, v0, Lohp;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move-object v0, v4

    goto :goto_2

    :cond_7
    move-object v0, v4

    .line 61
    goto :goto_4

    .line 79
    :cond_8
    iget-object v0, p2, Lnrp;->f:[Lpeo;

    .line 80
    if-eqz v0, :cond_9

    array-length v8, v0

    if-nez v8, :cond_a

    :cond_9
    :goto_7
    move-object v0, p0

    .line 79
    invoke-direct/range {v0 .. v7}, Llan;->a(Ljava/util/ArrayList;Llag;Ljava/lang/String;Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 82
    return-void

    .line 80
    :cond_a
    aget-object v4, v0, v7

    invoke-static {v4}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    array-length v8, v0

    if-le v8, v12, :cond_9

    const-string v8, "\n"

    invoke-virtual {v4, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v8

    aget-object v0, v0, v12

    invoke-static {v0}, Lhwr;->a(Lpeo;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_7

    :cond_b
    move-object v0, v4

    goto/16 :goto_6

    :cond_c
    move-object v5, v4

    goto/16 :goto_3

    :cond_d
    move-object v0, v4

    goto/16 :goto_0
.end method

.method private constructor <init>(Ljava/util/ArrayList;Llag;ILjava/lang/String;Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Llag;",
            ">;",
            "Llag;",
            "I",
            "Ljava/lang/String;",
            "Landroid/text/Spanned;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0}, Lllq;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    move/from16 v7, p8

    .line 87
    invoke-direct/range {v0 .. v7}, Llan;->a(Ljava/util/ArrayList;Llag;Ljava/lang/String;Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 89
    return-void
.end method

.method private static a(Ljava/lang/String;Lohq;)Llag;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 163
    .line 168
    if-eqz p1, :cond_2

    .line 169
    iget-object v2, p1, Lohq;->a:Ljava/lang/String;

    .line 170
    iget-object v4, p1, Lohq;->c:Ljava/lang/String;

    .line 171
    iget-object v1, p1, Lohq;->f:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v5

    .line 173
    iget-object v1, p1, Lohq;->p:[Loib;

    if-eqz v1, :cond_0

    .line 174
    :goto_0
    iget-object v1, p1, Lohq;->p:[Loib;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 175
    iget-object v1, p1, Lohq;->p:[Loib;

    aget-object v1, v1, v0

    .line 176
    iget-object v6, v1, Loib;->e:Ljava/lang/Boolean;

    invoke-static {v6}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 177
    iget-object v3, v1, Loib;->d:Ljava/lang/String;

    .line 184
    :cond_0
    :goto_1
    new-instance v0, Llag;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Llag;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    .line 174
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v5, v0

    move-object v4, v3

    move-object v2, v3

    goto :goto_1
.end method

.method public static a(Ljava/nio/ByteBuffer;)Llan;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 219
    invoke-static {p0}, Lllq;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    .line 220
    invoke-static {p0}, Lllq;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v7

    .line 225
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    if-ne v1, v8, :cond_0

    move v1, v8

    .line 226
    :goto_0
    if-eqz v1, :cond_1

    .line 227
    invoke-static {p0}, Lllq;->c(Ljava/nio/ByteBuffer;)[B

    move-result-object v1

    invoke-static {v1}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    move-object v4, v0

    .line 232
    :goto_1
    invoke-static {p0}, Llag;->a(Ljava/nio/ByteBuffer;)Llag;

    move-result-object v2

    .line 234
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    .line 235
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v9

    .line 236
    :goto_2
    if-ge v0, v3, :cond_2

    .line 237
    invoke-static {p0}, Llag;->a(Ljava/nio/ByteBuffer;)Llag;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    move v1, v9

    .line 225
    goto :goto_0

    .line 229
    :cond_1
    invoke-static {p0}, Lllq;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    move-object v5, v0

    goto :goto_1

    .line 240
    :cond_2
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    if-ne v0, v8, :cond_3

    .line 242
    :goto_3
    new-instance v0, Llan;

    invoke-direct/range {v0 .. v8}, Llan;-><init>(Ljava/util/ArrayList;Llag;ILjava/lang/String;Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    :cond_3
    move v8, v9

    .line 240
    goto :goto_3
.end method

.method private a(Ljava/util/ArrayList;Llag;Ljava/lang/String;Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Llag;",
            ">;",
            "Llag;",
            "Ljava/lang/String;",
            "Landroid/text/Spanned;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 93
    iput-object p1, p0, Llan;->b:Ljava/util/ArrayList;

    .line 94
    iput-object p2, p0, Llan;->c:Llag;

    .line 95
    iput-object p6, p0, Llan;->d:Ljava/lang/String;

    .line 96
    iput-object p3, p0, Llan;->f:Ljava/lang/String;

    .line 98
    iput-object p4, p0, Llan;->a:Landroid/text/Spanned;

    .line 99
    iput-object p6, p0, Llan;->d:Ljava/lang/String;

    .line 100
    iput-object p5, p0, Llan;->e:Ljava/lang/String;

    .line 102
    iput-boolean p7, p0, Llan;->g:Z

    .line 103
    return-void
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Llag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Llan;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 154
    iput-boolean p1, p0, Llan;->g:Z

    .line 155
    return-void
.end method

.method public b()Llag;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Llan;->c:Llag;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Llan;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Llan;->a:Landroid/text/Spanned;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Llan;->a:Landroid/text/Spanned;

    .line 125
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Llan;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Llan;->e:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Llan;->g:Z

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Llan;->e:Ljava/lang/String;

    return-object v0
.end method
