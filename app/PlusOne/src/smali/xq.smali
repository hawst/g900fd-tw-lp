.class public final Lxq;
.super Lrg;
.source "PG"

# interfaces
.implements Lhk;


# instance fields
.field e:Landroid/view/View;

.field f:Lxw;

.field g:Lxr;

.field h:Lxt;

.field final i:Lxx;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:I

.field private final q:Landroid/util/SparseBooleanArray;

.field private r:Landroid/view/View;

.field private s:Lxs;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 86
    const v0, 0x7f040004

    const v1, 0x7f040003

    invoke-direct {p0, p1, v0, v1}, Lrg;-><init>(Landroid/content/Context;II)V

    .line 72
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lxq;->q:Landroid/util/SparseBooleanArray;

    .line 82
    new-instance v0, Lxx;

    invoke-direct {v0, p0}, Lxx;-><init>(Lxq;)V

    iput-object v0, p0, Lxq;->i:Lxx;

    .line 87
    return-void
.end method


# virtual methods
.method public a(Lrp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p1}, Lrp;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lrp;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lrg;->a(Lrp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 172
    :cond_1
    invoke-virtual {p1}, Lrp;->isActionViewExpanded()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 174
    check-cast p3, Landroid/support/v7/widget/ActionMenuView;

    .line 175
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 176
    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 177
    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)Lxz;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    :cond_2
    return-object v0

    .line 172
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)Lsc;
    .locals 2

    .prologue
    .line 161
    invoke-super {p0, p1}, Lrg;->a(Landroid/view/ViewGroup;)Lsc;

    move-result-object v1

    move-object v0, v1

    .line 162
    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ActionMenuView;->a(Lxq;)V

    .line 163
    return-object v1
.end method

.method public a(Landroid/content/Context;Lrl;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 91
    invoke-super {p0, p1, p2}, Lrg;->a(Landroid/content/Context;Lrl;)V

    .line 93
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 95
    invoke-static {p1}, Lqu;->a(Landroid/content/Context;)Lqu;

    move-result-object v0

    .line 96
    iget-boolean v2, p0, Lxq;->k:Z

    if-nez v2, :cond_0

    .line 97
    invoke-virtual {v0}, Lqu;->b()Z

    move-result v2

    iput-boolean v2, p0, Lxq;->j:Z

    .line 100
    :cond_0
    invoke-virtual {v0}, Lqu;->c()I

    move-result v2

    iput v2, p0, Lxq;->l:I

    .line 105
    invoke-virtual {v0}, Lqu;->a()I

    move-result v0

    iput v0, p0, Lxq;->n:I

    .line 109
    iget v0, p0, Lxq;->l:I

    .line 110
    iget-boolean v2, p0, Lxq;->j:Z

    if-eqz v2, :cond_2

    .line 111
    iget-object v2, p0, Lxq;->e:Landroid/view/View;

    if-nez v2, :cond_1

    .line 112
    new-instance v2, Lxu;

    iget-object v3, p0, Lxq;->a:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lxu;-><init>(Lxq;Landroid/content/Context;)V

    iput-object v2, p0, Lxq;->e:Landroid/view/View;

    .line 113
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 114
    iget-object v3, p0, Lxq;->e:Landroid/view/View;

    invoke-virtual {v3, v2, v2}, Landroid/view/View;->measure(II)V

    .line 116
    :cond_1
    iget-object v2, p0, Lxq;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    .line 121
    :goto_0
    iput v0, p0, Lxq;->m:I

    .line 123
    const/high16 v0, 0x42600000    # 56.0f

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lxq;->p:I

    .line 126
    iput-object v5, p0, Lxq;->r:Landroid/view/View;

    .line 127
    return-void

    .line 118
    :cond_2
    iput-object v5, p0, Lxq;->e:Landroid/view/View;

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/ActionMenuView;)V
    .locals 1

    .prologue
    .line 548
    iput-object p1, p0, Lxq;->d:Lsc;

    .line 549
    iget-object v0, p0, Lxq;->c:Lrl;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ActionMenuView;->a(Lrl;)V

    .line 550
    return-void
.end method

.method public a(Lrl;Z)V
    .locals 0

    .prologue
    .line 514
    invoke-virtual {p0}, Lxq;->f()Z

    .line 515
    invoke-super {p0, p1, p2}, Lrg;->a(Lrl;Z)V

    .line 516
    return-void
.end method

.method public a(Lrp;Lsd;)V
    .locals 1

    .prologue
    .line 184
    invoke-interface {p2, p1}, Lsd;->a(Lrp;)V

    .line 186
    iget-object v0, p0, Lxq;->d:Lsc;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    .line 187
    check-cast p2, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    .line 188
    invoke-virtual {p2, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a(Lrn;)V

    .line 190
    iget-object v0, p0, Lxq;->s:Lxs;

    if-nez v0, :cond_0

    .line 191
    new-instance v0, Lxs;

    invoke-direct {v0, p0}, Lxs;-><init>(Lxq;)V

    iput-object v0, p0, Lxq;->s:Lxs;

    .line 193
    :cond_0
    iget-object v0, p0, Lxq;->s:Lxs;

    invoke-virtual {p2, v0}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->a(Lrf;)V

    .line 194
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 539
    if-eqz p1, :cond_0

    .line 541
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lrg;->a(Lsg;)Z

    .line 545
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-object v0, p0, Lxq;->c:Lrl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrl;->a(Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;I)Z
    .locals 2

    .prologue
    .line 256
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lxq;->e:Landroid/view/View;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 257
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lrg;->a(Landroid/view/ViewGroup;I)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lrp;)Z
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p1}, Lrp;->i()Z

    move-result v0

    return v0
.end method

.method public a(Lsg;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 261
    invoke-virtual {p1}, Lsg;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v3

    .line 278
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 264
    :goto_1
    invoke-virtual {v0}, Lsg;->u()Landroid/view/Menu;

    move-result-object v1

    iget-object v2, p0, Lxq;->c:Lrl;

    if-eq v1, v2, :cond_1

    .line 265
    invoke-virtual {v0}, Lsg;->u()Landroid/view/Menu;

    move-result-object v0

    check-cast v0, Lsg;

    goto :goto_1

    .line 267
    :cond_1
    invoke-virtual {v0}, Lsg;->getItem()Landroid/view/MenuItem;

    move-result-object v5

    iget-object v0, p0, Lxq;->d:Lsc;

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_3

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v1, v2, Lsd;

    if-eqz v1, :cond_2

    move-object v1, v2

    check-cast v1, Lsd;

    invoke-interface {v1}, Lsd;->a()Lrp;

    move-result-object v1

    if-ne v1, v5, :cond_2

    move-object v0, v2

    .line 268
    :goto_3
    if-nez v0, :cond_5

    .line 269
    iget-object v0, p0, Lxq;->e:Landroid/view/View;

    if-nez v0, :cond_4

    move v0, v3

    goto :goto_0

    .line 267
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 270
    :cond_4
    iget-object v0, p0, Lxq;->e:Landroid/view/View;

    .line 273
    :cond_5
    invoke-virtual {p1}, Lsg;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    .line 274
    new-instance v1, Lxr;

    iget-object v2, p0, Lxq;->b:Landroid/content/Context;

    invoke-direct {v1, p0, v2, p1}, Lxr;-><init>(Lxq;Landroid/content/Context;Lsg;)V

    iput-object v1, p0, Lxq;->g:Lxr;

    .line 275
    iget-object v1, p0, Lxq;->g:Lxr;

    invoke-virtual {v1, v0}, Lxr;->a(Landroid/view/View;)V

    .line 276
    iget-object v0, p0, Lxq;->g:Lxr;

    invoke-virtual {v0}, Lxr;->a()V

    .line 277
    invoke-super {p0, p1}, Lrg;->a(Lsg;)Z

    .line 278
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lxq;->d:Lsc;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 204
    invoke-super {p0, p1}, Lrg;->b(Z)V

    .line 209
    iget-object v0, p0, Lxq;->d:Lsc;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 211
    iget-object v0, p0, Lxq;->c:Lrl;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lxq;->c:Lrl;

    invoke-virtual {v0}, Lrl;->m()Ljava/util/ArrayList;

    move-result-object v4

    .line 213
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 214
    :goto_0
    if-ge v3, v5, :cond_1

    .line 215
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrp;

    invoke-virtual {v0}, Lrp;->a()Lhj;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {v0, p0}, Lhj;->a(Lhk;)V

    .line 214
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 222
    :cond_1
    iget-object v0, p0, Lxq;->c:Lrl;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lxq;->c:Lrl;

    invoke-virtual {v0}, Lrl;->n()Ljava/util/ArrayList;

    move-result-object v0

    .line 226
    :goto_1
    iget-boolean v3, p0, Lxq;->j:Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 227
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 228
    if-ne v3, v1, :cond_8

    .line 229
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrp;

    invoke-virtual {v0}, Lrp;->isActionViewExpanded()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    move v2, v0

    .line 235
    :cond_2
    :goto_3
    if-eqz v2, :cond_a

    .line 236
    iget-object v0, p0, Lxq;->e:Landroid/view/View;

    if-nez v0, :cond_3

    .line 237
    new-instance v0, Lxu;

    iget-object v1, p0, Lxq;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lxu;-><init>(Lxq;Landroid/content/Context;)V

    iput-object v0, p0, Lxq;->e:Landroid/view/View;

    .line 239
    :cond_3
    iget-object v0, p0, Lxq;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 240
    iget-object v1, p0, Lxq;->d:Lsc;

    if-eq v0, v1, :cond_5

    .line 241
    if-eqz v0, :cond_4

    .line 242
    iget-object v1, p0, Lxq;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 244
    :cond_4
    iget-object v0, p0, Lxq;->d:Lsc;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    .line 245
    iget-object v1, p0, Lxq;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->c()Lxz;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    :cond_5
    :goto_4
    iget-object v0, p0, Lxq;->d:Lsc;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iget-boolean v1, p0, Lxq;->j:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->a(Z)V

    .line 252
    return-void

    .line 222
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    move v0, v2

    .line 229
    goto :goto_2

    .line 231
    :cond_8
    if-lez v3, :cond_9

    :goto_5
    move v2, v1

    goto :goto_3

    :cond_9
    move v1, v2

    goto :goto_5

    .line 247
    :cond_a
    iget-object v0, p0, Lxq;->e:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lxq;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lxq;->d:Lsc;

    if-ne v0, v1, :cond_5

    .line 248
    iget-object v0, p0, Lxq;->d:Lsc;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lxq;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_4
.end method

.method public b()Z
    .locals 17

    .prologue
    .line 379
    move-object/from16 v0, p0

    iget-object v1, v0, Lxq;->c:Lrl;

    invoke-virtual {v1}, Lrl;->k()Ljava/util/ArrayList;

    move-result-object v10

    .line 380
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 381
    move-object/from16 v0, p0

    iget v6, v0, Lxq;->n:I

    .line 382
    move-object/from16 v0, p0

    iget v8, v0, Lxq;->m:I

    .line 383
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 384
    move-object/from16 v0, p0

    iget-object v1, v0, Lxq;->d:Lsc;

    check-cast v1, Landroid/view/ViewGroup;

    .line 386
    const/4 v5, 0x0

    .line 387
    const/4 v4, 0x0

    .line 388
    const/4 v7, 0x0

    .line 389
    const/4 v3, 0x0

    .line 390
    const/4 v2, 0x0

    move v9, v2

    :goto_0
    if-ge v9, v11, :cond_2

    .line 391
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lrp;

    .line 392
    invoke-virtual {v2}, Lrp;->k()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 393
    add-int/lit8 v5, v5, 0x1

    .line 399
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lxq;->o:Z

    if-eqz v13, :cond_17

    invoke-virtual {v2}, Lrp;->isActionViewExpanded()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 402
    const/4 v2, 0x0

    .line 390
    :goto_2
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    move v6, v2

    goto :goto_0

    .line 394
    :cond_0
    invoke-virtual {v2}, Lrp;->j()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 395
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 397
    :cond_1
    const/4 v3, 0x1

    goto :goto_1

    .line 407
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lxq;->j:Z

    if-eqz v2, :cond_4

    if-nez v3, :cond_3

    add-int v2, v5, v4

    if-le v2, v6, :cond_4

    .line 409
    :cond_3
    add-int/lit8 v6, v6, -0x1

    .line 411
    :cond_4
    sub-int v4, v6, v5

    .line 413
    move-object/from16 v0, p0

    iget-object v13, v0, Lxq;->q:Landroid/util/SparseBooleanArray;

    .line 414
    invoke-virtual {v13}, Landroid/util/SparseBooleanArray;->clear()V

    .line 416
    const/4 v2, 0x0

    move v9, v2

    move v5, v8

    move/from16 v16, v7

    move v7, v4

    move/from16 v4, v16

    :goto_3
    if-ge v9, v11, :cond_11

    .line 426
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lrp;

    .line 428
    invoke-virtual {v2}, Lrp;->k()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 429
    move-object/from16 v0, p0

    iget-object v3, v0, Lxq;->r:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v1}, Lxq;->a(Lrp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 430
    move-object/from16 v0, p0

    iget-object v6, v0, Lxq;->r:Landroid/view/View;

    if-nez v6, :cond_5

    .line 431
    move-object/from16 v0, p0

    iput-object v3, v0, Lxq;->r:Landroid/view/View;

    .line 433
    :cond_5
    invoke-virtual {v3, v12, v12}, Landroid/view/View;->measure(II)V

    .line 439
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 440
    sub-int/2addr v5, v3

    .line 441
    if-nez v4, :cond_16

    .line 444
    :goto_4
    invoke-virtual {v2}, Lrp;->getGroupId()I

    move-result v4

    .line 445
    if-eqz v4, :cond_6

    .line 446
    const/4 v6, 0x1

    invoke-virtual {v13, v4, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 448
    :cond_6
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lrp;->d(Z)V

    move v2, v5

    move v4, v7

    .line 425
    :goto_5
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    move v7, v4

    move v5, v2

    move v4, v3

    goto :goto_3

    .line 449
    :cond_7
    invoke-virtual {v2}, Lrp;->j()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 452
    invoke-virtual {v2}, Lrp;->getGroupId()I

    move-result v14

    .line 453
    invoke-virtual {v13, v14}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v15

    .line 454
    if-gtz v7, :cond_8

    if-eqz v15, :cond_b

    :cond_8
    if-lez v5, :cond_b

    const/4 v6, 0x1

    .line 457
    :goto_6
    if-eqz v6, :cond_15

    .line 458
    move-object/from16 v0, p0

    iget-object v3, v0, Lxq;->r:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v1}, Lxq;->a(Lrp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 459
    move-object/from16 v0, p0

    iget-object v8, v0, Lxq;->r:Landroid/view/View;

    if-nez v8, :cond_9

    .line 460
    move-object/from16 v0, p0

    iput-object v3, v0, Lxq;->r:Landroid/view/View;

    .line 462
    :cond_9
    invoke-virtual {v3, v12, v12}, Landroid/view/View;->measure(II)V

    .line 472
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 473
    sub-int v8, v5, v3

    .line 474
    if-nez v4, :cond_14

    move v5, v3

    .line 478
    :goto_7
    add-int v3, v8, v5

    if-lez v3, :cond_c

    const/4 v3, 0x1

    :goto_8
    and-int/2addr v3, v6

    move v6, v8

    move v8, v3

    .line 486
    :goto_9
    if-eqz v8, :cond_d

    if-eqz v14, :cond_d

    .line 487
    const/4 v3, 0x1

    invoke-virtual {v13, v14, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    move v3, v7

    .line 501
    :goto_a
    if-eqz v8, :cond_a

    add-int/lit8 v3, v3, -0x1

    .line 503
    :cond_a
    invoke-virtual {v2, v8}, Lrp;->d(Z)V

    move v2, v6

    move v4, v3

    move v3, v5

    .line 504
    goto :goto_5

    .line 454
    :cond_b
    const/4 v6, 0x0

    goto :goto_6

    .line 478
    :cond_c
    const/4 v3, 0x0

    goto :goto_8

    .line 488
    :cond_d
    if-eqz v15, :cond_13

    .line 490
    const/4 v3, 0x0

    invoke-virtual {v13, v14, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 491
    const/4 v3, 0x0

    move v4, v7

    move v7, v3

    :goto_b
    if-ge v7, v9, :cond_12

    .line 492
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lrp;

    .line 493
    invoke-virtual {v3}, Lrp;->getGroupId()I

    move-result v15

    if-ne v15, v14, :cond_f

    .line 495
    invoke-virtual {v3}, Lrp;->i()Z

    move-result v15

    if-eqz v15, :cond_e

    add-int/lit8 v4, v4, 0x1

    .line 496
    :cond_e
    const/4 v15, 0x0

    invoke-virtual {v3, v15}, Lrp;->d(Z)V

    .line 491
    :cond_f
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_b

    .line 506
    :cond_10
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lrp;->d(Z)V

    move v3, v4

    move v2, v5

    move v4, v7

    goto/16 :goto_5

    .line 509
    :cond_11
    const/4 v1, 0x1

    return v1

    :cond_12
    move v3, v4

    goto :goto_a

    :cond_13
    move v3, v7

    goto :goto_a

    :cond_14
    move v5, v4

    goto :goto_7

    :cond_15
    move v8, v6

    move v6, v5

    move v5, v4

    goto :goto_9

    :cond_16
    move v3, v4

    goto/16 :goto_4

    :cond_17
    move v2, v6

    goto/16 :goto_2
.end method

.method public c()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lxq;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c006d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lxq;->n:I

    .line 134
    iget-object v0, p0, Lxq;->c:Lrl;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lxq;->c:Lrl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrl;->b(Z)V

    .line 137
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 146
    iput-boolean p1, p0, Lxq;->j:Z

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lxq;->k:Z

    .line 148
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 156
    iput-boolean p1, p0, Lxq;->o:Z

    .line 157
    return-void
.end method

.method public d()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 301
    iget-boolean v0, p0, Lxq;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lxq;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lxq;->c:Lrl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxq;->d:Lsc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxq;->h:Lxt;

    if-nez v0, :cond_0

    iget-object v0, p0, Lxq;->c:Lrl;

    invoke-virtual {v0}, Lrl;->n()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    new-instance v0, Lxw;

    iget-object v2, p0, Lxq;->b:Landroid/content/Context;

    iget-object v3, p0, Lxq;->c:Lrl;

    iget-object v4, p0, Lxq;->e:Landroid/view/View;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lxw;-><init>(Lxq;Landroid/content/Context;Lrl;Landroid/view/View;Z)V

    .line 304
    new-instance v1, Lxt;

    invoke-direct {v1, p0, v0}, Lxt;-><init>(Lxq;Lxw;)V

    iput-object v1, p0, Lxq;->h:Lxt;

    .line 306
    iget-object v0, p0, Lxq;->d:Lsc;

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lxq;->h:Lxt;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 310
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lrg;->a(Lsg;)Z

    .line 314
    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 323
    iget-object v0, p0, Lxq;->h:Lxt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxq;->d:Lsc;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lxq;->d:Lsc;

    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lxq;->h:Lxt;

    invoke-virtual {v0, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 325
    const/4 v0, 0x0

    iput-object v0, p0, Lxq;->h:Lxt;

    move v0, v1

    .line 334
    :goto_0
    return v0

    .line 329
    :cond_0
    iget-object v0, p0, Lxq;->f:Lxw;

    .line 330
    if-eqz v0, :cond_1

    .line 331
    invoke-virtual {v0}, Lry;->e()V

    move v0, v1

    .line 332
    goto :goto_0

    .line 334
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 342
    invoke-virtual {p0}, Lxq;->e()Z

    move-result v0

    .line 343
    invoke-virtual {p0}, Lxq;->g()Z

    move-result v1

    or-int/2addr v0, v1

    .line 344
    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lxq;->g:Lxr;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lxq;->g:Lxr;

    invoke-virtual {v0}, Lxr;->e()V

    .line 355
    const/4 v0, 0x1

    .line 357
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lxq;->f:Lxw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxq;->f:Lxw;

    invoke-virtual {v0}, Lxw;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lxq;->h:Lxt;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lxq;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
