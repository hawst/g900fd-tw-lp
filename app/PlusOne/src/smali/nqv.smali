.class public final Lnqv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 467
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 503
    const/4 v0, 0x0

    .line 504
    iget-object v1, p0, Lnqv;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 505
    const/4 v0, 0x1

    iget-object v1, p0, Lnqv;->a:Ljava/lang/String;

    .line 506
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 508
    :cond_0
    iget-object v1, p0, Lnqv;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 509
    const/4 v1, 0x2

    iget-object v2, p0, Lnqv;->b:Ljava/lang/Integer;

    .line 510
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 512
    :cond_1
    iget-object v1, p0, Lnqv;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 513
    const/4 v1, 0x3

    iget-object v2, p0, Lnqv;->c:Ljava/lang/String;

    .line 514
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 516
    :cond_2
    iget-object v1, p0, Lnqv;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 517
    const/4 v1, 0x4

    iget-object v2, p0, Lnqv;->d:Ljava/lang/String;

    .line 518
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 520
    :cond_3
    iget-object v1, p0, Lnqv;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 521
    const/4 v1, 0x5

    iget-object v2, p0, Lnqv;->e:Ljava/lang/Boolean;

    .line 522
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 524
    :cond_4
    iget-object v1, p0, Lnqv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 525
    iput v0, p0, Lnqv;->ai:I

    .line 526
    return v0
.end method

.method public a(Loxn;)Lnqv;
    .locals 2

    .prologue
    .line 534
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 535
    sparse-switch v0, :sswitch_data_0

    .line 539
    iget-object v1, p0, Lnqv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 540
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnqv;->ah:Ljava/util/List;

    .line 543
    :cond_1
    iget-object v1, p0, Lnqv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 545
    :sswitch_0
    return-object p0

    .line 550
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqv;->a:Ljava/lang/String;

    goto :goto_0

    .line 554
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnqv;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 558
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqv;->c:Ljava/lang/String;

    goto :goto_0

    .line 562
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqv;->d:Ljava/lang/String;

    goto :goto_0

    .line 566
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqv;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 535
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lnqv;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 483
    const/4 v0, 0x1

    iget-object v1, p0, Lnqv;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 485
    :cond_0
    iget-object v0, p0, Lnqv;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 486
    const/4 v0, 0x2

    iget-object v1, p0, Lnqv;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 488
    :cond_1
    iget-object v0, p0, Lnqv;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 489
    const/4 v0, 0x3

    iget-object v1, p0, Lnqv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 491
    :cond_2
    iget-object v0, p0, Lnqv;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 492
    const/4 v0, 0x4

    iget-object v1, p0, Lnqv;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 494
    :cond_3
    iget-object v0, p0, Lnqv;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 495
    const/4 v0, 0x5

    iget-object v1, p0, Lnqv;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 497
    :cond_4
    iget-object v0, p0, Lnqv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 499
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 463
    invoke-virtual {p0, p1}, Lnqv;->a(Loxn;)Lnqv;

    move-result-object v0

    return-object v0
.end method
