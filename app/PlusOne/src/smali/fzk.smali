.class public final enum Lfzk;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lfzk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lfzk;

.field public static final enum b:Lfzk;

.field public static final enum c:Lfzk;

.field public static final enum d:Lfzk;

.field private static final synthetic g:[Lfzk;


# instance fields
.field private final e:Lfzj;

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 74
    new-instance v0, Lfzk;

    const-string v1, "COMMENT_TEXT"

    sget-object v2, Lfzj;->a:Lfzj;

    const v3, 0x7f100227

    invoke-direct {v0, v1, v4, v2, v3}, Lfzk;-><init>(Ljava/lang/String;ILfzj;I)V

    sput-object v0, Lfzk;->a:Lfzk;

    .line 75
    new-instance v0, Lfzk;

    const-string v1, "TAG_TEXT"

    sget-object v2, Lfzj;->b:Lfzj;

    const v3, 0x7f1004a4

    invoke-direct {v0, v1, v5, v2, v3}, Lfzk;-><init>(Ljava/lang/String;ILfzj;I)V

    sput-object v0, Lfzk;->b:Lfzk;

    .line 76
    new-instance v0, Lfzk;

    const-string v1, "PLUS_ONE_TEXT"

    sget-object v2, Lfzj;->c:Lfzj;

    const v3, 0x7f1004a1

    invoke-direct {v0, v1, v6, v2, v3}, Lfzk;-><init>(Ljava/lang/String;ILfzj;I)V

    sput-object v0, Lfzk;->c:Lfzk;

    .line 77
    new-instance v0, Lfzk;

    const-string v1, "HASHTAG_TEXT"

    sget-object v2, Lfzj;->g:Lfzj;

    const v3, 0x7f10049f

    invoke-direct {v0, v1, v7, v2, v3}, Lfzk;-><init>(Ljava/lang/String;ILfzj;I)V

    sput-object v0, Lfzk;->d:Lfzk;

    .line 73
    const/4 v0, 0x4

    new-array v0, v0, [Lfzk;

    sget-object v1, Lfzk;->a:Lfzk;

    aput-object v1, v0, v4

    sget-object v1, Lfzk;->b:Lfzk;

    aput-object v1, v0, v5

    sget-object v1, Lfzk;->c:Lfzk;

    aput-object v1, v0, v6

    sget-object v1, Lfzk;->d:Lfzk;

    aput-object v1, v0, v7

    sput-object v0, Lfzk;->g:[Lfzk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILfzj;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lfzj;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 83
    iput-object p3, p0, Lfzk;->e:Lfzj;

    .line 84
    iput p4, p0, Lfzk;->f:I

    .line 85
    return-void
.end method

.method public static synthetic a(Lfzk;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lfzk;->f:I

    return v0
.end method

.method public static synthetic b(Lfzk;)Lfzj;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lfzk;->e:Lfzj;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lfzk;
    .locals 1

    .prologue
    .line 73
    const-class v0, Lfzk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfzk;

    return-object v0
.end method

.method public static values()[Lfzk;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lfzk;->g:[Lfzk;

    invoke-virtual {v0}, [Lfzk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfzk;

    return-object v0
.end method
