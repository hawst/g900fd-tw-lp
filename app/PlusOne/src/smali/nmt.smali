.class public final Lnmt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lnmr;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Integer;

.field private e:Lofl;

.field private f:Logu;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1939
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1948
    iput-object v0, p0, Lnmt;->e:Lofl;

    .line 1951
    iput-object v0, p0, Lnmt;->f:Logu;

    .line 1954
    iput-object v0, p0, Lnmt;->b:Lnmr;

    .line 1939
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1988
    const/4 v0, 0x0

    .line 1989
    iget-object v1, p0, Lnmt;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1990
    const/4 v0, 0x1

    iget-object v1, p0, Lnmt;->a:Ljava/lang/String;

    .line 1991
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1993
    :cond_0
    iget-object v1, p0, Lnmt;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1994
    const/4 v1, 0x2

    iget-object v2, p0, Lnmt;->c:Ljava/lang/Boolean;

    .line 1995
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1997
    :cond_1
    iget-object v1, p0, Lnmt;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1998
    const/4 v1, 0x3

    iget-object v2, p0, Lnmt;->d:Ljava/lang/Integer;

    .line 1999
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2001
    :cond_2
    iget-object v1, p0, Lnmt;->e:Lofl;

    if-eqz v1, :cond_3

    .line 2002
    const/4 v1, 0x4

    iget-object v2, p0, Lnmt;->e:Lofl;

    .line 2003
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2005
    :cond_3
    iget-object v1, p0, Lnmt;->f:Logu;

    if-eqz v1, :cond_4

    .line 2006
    const/4 v1, 0x5

    iget-object v2, p0, Lnmt;->f:Logu;

    .line 2007
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2009
    :cond_4
    iget-object v1, p0, Lnmt;->b:Lnmr;

    if-eqz v1, :cond_5

    .line 2010
    const/4 v1, 0x6

    iget-object v2, p0, Lnmt;->b:Lnmr;

    .line 2011
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2013
    :cond_5
    iget-object v1, p0, Lnmt;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 2014
    const/4 v1, 0x7

    iget-object v2, p0, Lnmt;->g:Ljava/lang/String;

    .line 2015
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2017
    :cond_6
    iget-object v1, p0, Lnmt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2018
    iput v0, p0, Lnmt;->ai:I

    .line 2019
    return v0
.end method

.method public a(Loxn;)Lnmt;
    .locals 2

    .prologue
    .line 2027
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2028
    sparse-switch v0, :sswitch_data_0

    .line 2032
    iget-object v1, p0, Lnmt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2033
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnmt;->ah:Ljava/util/List;

    .line 2036
    :cond_1
    iget-object v1, p0, Lnmt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2038
    :sswitch_0
    return-object p0

    .line 2043
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmt;->a:Ljava/lang/String;

    goto :goto_0

    .line 2047
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnmt;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 2051
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnmt;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2055
    :sswitch_4
    iget-object v0, p0, Lnmt;->e:Lofl;

    if-nez v0, :cond_2

    .line 2056
    new-instance v0, Lofl;

    invoke-direct {v0}, Lofl;-><init>()V

    iput-object v0, p0, Lnmt;->e:Lofl;

    .line 2058
    :cond_2
    iget-object v0, p0, Lnmt;->e:Lofl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2062
    :sswitch_5
    iget-object v0, p0, Lnmt;->f:Logu;

    if-nez v0, :cond_3

    .line 2063
    new-instance v0, Logu;

    invoke-direct {v0}, Logu;-><init>()V

    iput-object v0, p0, Lnmt;->f:Logu;

    .line 2065
    :cond_3
    iget-object v0, p0, Lnmt;->f:Logu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2069
    :sswitch_6
    iget-object v0, p0, Lnmt;->b:Lnmr;

    if-nez v0, :cond_4

    .line 2070
    new-instance v0, Lnmr;

    invoke-direct {v0}, Lnmr;-><init>()V

    iput-object v0, p0, Lnmt;->b:Lnmr;

    .line 2072
    :cond_4
    iget-object v0, p0, Lnmt;->b:Lnmr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2076
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnmt;->g:Ljava/lang/String;

    goto :goto_0

    .line 2028
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1961
    iget-object v0, p0, Lnmt;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1962
    const/4 v0, 0x1

    iget-object v1, p0, Lnmt;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1964
    :cond_0
    iget-object v0, p0, Lnmt;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1965
    const/4 v0, 0x2

    iget-object v1, p0, Lnmt;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1967
    :cond_1
    iget-object v0, p0, Lnmt;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1968
    const/4 v0, 0x3

    iget-object v1, p0, Lnmt;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1970
    :cond_2
    iget-object v0, p0, Lnmt;->e:Lofl;

    if-eqz v0, :cond_3

    .line 1971
    const/4 v0, 0x4

    iget-object v1, p0, Lnmt;->e:Lofl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1973
    :cond_3
    iget-object v0, p0, Lnmt;->f:Logu;

    if-eqz v0, :cond_4

    .line 1974
    const/4 v0, 0x5

    iget-object v1, p0, Lnmt;->f:Logu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1976
    :cond_4
    iget-object v0, p0, Lnmt;->b:Lnmr;

    if-eqz v0, :cond_5

    .line 1977
    const/4 v0, 0x6

    iget-object v1, p0, Lnmt;->b:Lnmr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1979
    :cond_5
    iget-object v0, p0, Lnmt;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1980
    const/4 v0, 0x7

    iget-object v1, p0, Lnmt;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1982
    :cond_6
    iget-object v0, p0, Lnmt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1984
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1935
    invoke-virtual {p0, p1}, Lnmt;->a(Loxn;)Lnmt;

    move-result-object v0

    return-object v0
.end method
