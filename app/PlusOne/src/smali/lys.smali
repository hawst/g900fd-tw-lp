.class public final Llys;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lndz;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27133
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27136
    iput-object v0, p0, Llys;->apiHeader:Llyq;

    .line 27139
    iput-object v0, p0, Llys;->a:Lndz;

    .line 27133
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27156
    const/4 v0, 0x0

    .line 27157
    iget-object v1, p0, Llys;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 27158
    const/4 v0, 0x1

    iget-object v1, p0, Llys;->apiHeader:Llyq;

    .line 27159
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27161
    :cond_0
    iget-object v1, p0, Llys;->a:Lndz;

    if-eqz v1, :cond_1

    .line 27162
    const/4 v1, 0x2

    iget-object v2, p0, Llys;->a:Lndz;

    .line 27163
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27165
    :cond_1
    iget-object v1, p0, Llys;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27166
    iput v0, p0, Llys;->ai:I

    .line 27167
    return v0
.end method

.method public a(Loxn;)Llys;
    .locals 2

    .prologue
    .line 27175
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27176
    sparse-switch v0, :sswitch_data_0

    .line 27180
    iget-object v1, p0, Llys;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27181
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llys;->ah:Ljava/util/List;

    .line 27184
    :cond_1
    iget-object v1, p0, Llys;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27186
    :sswitch_0
    return-object p0

    .line 27191
    :sswitch_1
    iget-object v0, p0, Llys;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 27192
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llys;->apiHeader:Llyq;

    .line 27194
    :cond_2
    iget-object v0, p0, Llys;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27198
    :sswitch_2
    iget-object v0, p0, Llys;->a:Lndz;

    if-nez v0, :cond_3

    .line 27199
    new-instance v0, Lndz;

    invoke-direct {v0}, Lndz;-><init>()V

    iput-object v0, p0, Llys;->a:Lndz;

    .line 27201
    :cond_3
    iget-object v0, p0, Llys;->a:Lndz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27176
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27144
    iget-object v0, p0, Llys;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 27145
    const/4 v0, 0x1

    iget-object v1, p0, Llys;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27147
    :cond_0
    iget-object v0, p0, Llys;->a:Lndz;

    if-eqz v0, :cond_1

    .line 27148
    const/4 v0, 0x2

    iget-object v1, p0, Llys;->a:Lndz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27150
    :cond_1
    iget-object v0, p0, Llys;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27152
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 27129
    invoke-virtual {p0, p1}, Llys;->a(Loxn;)Llys;

    move-result-object v0

    return-object v0
.end method
