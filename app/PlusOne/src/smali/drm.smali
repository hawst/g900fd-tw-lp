.class public final Ldrm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lesn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lesn",
            "<",
            "Ldru;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lesn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lesn",
            "<",
            "Ldrt;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:[I

.field private static final d:Ljava/lang/Object;

.field private static final e:Ljava/lang/Object;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;

.field private static j:Ljava/io/File;

.field private static k:Lesn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lesn",
            "<",
            "Ldrv;",
            ">;"
        }
    .end annotation
.end field

.field private static l:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 149
    new-array v0, v6, [I

    fill-array-data v0, :array_0

    sput-object v0, Ldrm;->c:[I

    .line 201
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldrm;->d:Ljava/lang/Object;

    .line 284
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldrm;->e:Ljava/lang/Object;

    .line 286
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "event_id"

    aput-object v1, v0, v3

    const-string v1, "polling_token"

    aput-object v1, v0, v4

    const-string v1, "resume_token"

    aput-object v1, v0, v5

    const-string v1, "event_data"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "event_type"

    aput-object v2, v0, v1

    sput-object v0, Ldrm;->f:[Ljava/lang/String;

    .line 299
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "instant_share_end_time"

    aput-object v1, v0, v3

    sput-object v0, Ldrm;->g:[Ljava/lang/String;

    .line 302
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "resume_token"

    aput-object v1, v0, v3

    .line 306
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "event_data"

    aput-object v1, v0, v3

    const-string v1, "event_type"

    aput-object v1, v0, v4

    sput-object v0, Ldrm;->h:[Ljava/lang/String;

    .line 313
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "event_data"

    aput-object v1, v0, v3

    const-string v1, "event_type"

    aput-object v1, v0, v4

    const-string v1, "invitee_roster_proto"

    aput-object v1, v0, v5

    sput-object v0, Ldrm;->i:[Ljava/lang/String;

    .line 352
    const-class v0, Ldru;

    .line 353
    invoke-static {v0}, Lesn;->a(Ljava/lang/Class;)Lesn;

    move-result-object v0

    sput-object v0, Ldrm;->a:Lesn;

    .line 365
    const-class v0, Ldrv;

    .line 366
    invoke-static {v0}, Lesn;->a(Ljava/lang/Class;)Lesn;

    move-result-object v0

    sput-object v0, Ldrm;->k:Lesn;

    .line 375
    const-class v0, Ldrt;

    new-array v1, v5, [Ljava/lang/Object;

    sget-object v2, Ldrm;->k:Lesn;

    aput-object v2, v1, v3

    const-string v2, "people"

    aput-object v2, v1, v4

    .line 376
    invoke-static {v0, v1}, Lesn;->a(Ljava/lang/Class;[Ljava/lang/Object;)Lesn;

    move-result-object v0

    sput-object v0, Ldrm;->b:Lesn;

    .line 2650
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldrm;->l:Ljava/lang/Object;

    return-void

    .line 149
    nop

    :array_0
    .array-data 4
        0x19a
        0x194
        0x14f
    .end array-data
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 390
    int-to-float v0, p0

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 379
    invoke-static {p0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 380
    invoke-static {v1}, Llsc;->a(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 382
    invoke-static {v1}, Llsc;->b(Landroid/util/DisplayMetrics;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 383
    div-int/lit8 v0, v0, 0x2

    .line 386
    :cond_0
    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;I)I
    .locals 11

    .prologue
    .line 661
    const/4 v0, 0x3

    if-ne p3, v0, :cond_1

    const/4 v0, 0x1

    move v9, v0

    .line 663
    :goto_0
    if-eqz v9, :cond_2

    const/16 v0, 0x64

    move v8, v0

    .line 666
    :goto_1
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 667
    const-string v1, "events"

    sget-object v2, Ldrm;->h:[Ljava/lang/String;

    const-string v3, "event_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 669
    const/4 v1, 0x0

    .line 672
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 673
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    move-object v2, v1

    .line 677
    :goto_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 680
    const-class v1, Lhei;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    .line 681
    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v3, "gaia_id"

    .line 682
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 684
    if-eqz v2, :cond_c

    .line 686
    invoke-virtual {v2}, Lidh;->i()Lpbj;

    move-result-object v5

    .line 687
    iget-object v3, v5, Lpbj;->b:Lpbd;

    iget-object v3, v3, Lpbd;->b:Lpbc;

    if-eqz v3, :cond_0

    iget-object v3, v5, Lpbj;->b:Lpbd;

    iget-object v3, v3, Lpbd;->b:Lpbc;

    iget-object v3, v3, Lpbc;->a:Ljava/lang/Boolean;

    .line 688
    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 690
    :cond_0
    invoke-static {v2, v1}, Ldrm;->b(Lidh;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 691
    invoke-virtual {v2}, Lidh;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 692
    const/high16 v8, -0x80000000

    .line 772
    :goto_3
    return v8

    .line 661
    :cond_1
    const/4 v0, 0x0

    move v9, v0

    goto :goto_0

    .line 663
    :cond_2
    const/4 v0, 0x5

    move v8, v0

    goto :goto_1

    .line 677
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 695
    :cond_3
    iget-object v1, v5, Lpbj;->c:[Lpay;

    if-eqz v1, :cond_b

    .line 698
    const/4 v1, 0x0

    .line 700
    new-instance v6, Ljava/util/LinkedList;

    iget-object v3, v5, Lpbj;->c:[Lpay;

    .line 701
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 702
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v1

    .line 703
    :cond_4
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 704
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpay;

    .line 707
    iget v4, v1, Lpay;->b:I

    const/high16 v10, -0x80000000

    if-eq v4, v10, :cond_4

    .line 708
    iget-object v4, v1, Lpay;->e:Ljava/lang/Boolean;

    if-eqz v4, :cond_6

    iget-object v4, v1, Lpay;->e:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    .line 714
    :goto_5
    if-eqz v4, :cond_5

    if-nez v9, :cond_5

    .line 715
    iget v8, v1, Lpay;->b:I

    .line 718
    :cond_5
    iget v10, v1, Lpay;->b:I

    if-ne p3, v10, :cond_8

    .line 719
    const/4 v3, 0x1

    .line 720
    if-nez v4, :cond_9

    .line 721
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v1, Lpay;->e:Ljava/lang/Boolean;

    .line 722
    iget-object v4, v1, Lpay;->c:Ljava/lang/Integer;

    if-nez v4, :cond_7

    const/4 v4, 0x1

    :goto_6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v1, Lpay;->c:Ljava/lang/Integer;

    goto :goto_4

    .line 708
    :cond_6
    const/4 v4, 0x0

    goto :goto_5

    .line 722
    :cond_7
    iget-object v4, v1, Lpay;->c:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 725
    :cond_8
    if-eqz v4, :cond_9

    .line 729
    iget v4, v1, Lpay;->b:I

    const/4 v10, 0x3

    if-ne v4, v10, :cond_9

    .line 730
    const/high16 v4, -0x80000000

    if-ne p3, v4, :cond_9

    .line 731
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v1, Lpay;->e:Ljava/lang/Boolean;

    .line 732
    iget-object v4, v1, Lpay;->c:Ljava/lang/Integer;

    if-eqz v4, :cond_9

    .line 733
    iget-object v4, v1, Lpay;->c:Ljava/lang/Integer;

    iget-object v4, v1, Lpay;->c:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v1, Lpay;->c:Ljava/lang/Integer;

    .line 736
    iget-object v1, v1, Lpay;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_9

    .line 737
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    :cond_9
    move v1, v3

    move v3, v1

    .line 744
    goto :goto_4

    .line 748
    :cond_a
    if-nez v3, :cond_b

    .line 749
    new-instance v1, Lpay;

    invoke-direct {v1}, Lpay;-><init>()V

    .line 750
    iput p3, v1, Lpay;->b:I

    .line 751
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lpay;->c:Ljava/lang/Integer;

    .line 752
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v1, Lpay;->e:Ljava/lang/Boolean;

    .line 753
    const/4 v3, 0x0

    invoke-interface {v6, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 755
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lpay;

    .line 754
    invoke-interface {v6, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lpay;

    iput-object v1, v5, Lpbj;->c:[Lpay;

    .line 761
    :cond_b
    invoke-static {p0, v2, p1, p3}, Ldrm;->a(Landroid/content/Context;Lidh;II)V

    .line 763
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 764
    const-string v3, "event_data"

    invoke-virtual {v2}, Lidh;->f()[B

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 765
    const-string v2, "refresh_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 766
    const-string v2, "events"

    const-string v3, "event_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 770
    :cond_c
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lidg;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_3

    :cond_d
    move-object v2, v1

    goto/16 :goto_2
.end method

.method public static a(Lidh;)I
    .locals 1

    .prologue
    .line 2893
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lidh;->i()Lpbj;

    move-result-object v0

    iget-object v0, v0, Lpbj;->h:Lpax;

    if-eqz v0, :cond_0

    .line 2894
    invoke-virtual {p0}, Lidh;->i()Lpbj;

    move-result-object v0

    iget-object v0, v0, Lpbj;->h:Lpax;

    iget v0, v0, Lpax;->c:I

    invoke-static {v0}, Lidj;->a(I)I

    move-result v0

    .line 2897
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILidh;)J
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 900
    if-nez p2, :cond_0

    .line 920
    :goto_0
    return-wide v8

    .line 905
    :cond_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 906
    new-array v4, v2, [Ljava/lang/String;

    invoke-virtual {p2}, Lidh;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v6

    .line 910
    const-string v1, "events"

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "display_time"

    aput-object v3, v2, v6

    const-string v3, "event_id=?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 914
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 915
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 918
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-wide v8, v0

    .line 920
    goto :goto_0

    .line 918
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    move-wide v0, v8

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;II[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2603
    invoke-static {p0, p1, v4, v4}, Ldrm;->c(Landroid/content/Context;ILkfp;Lles;)V

    .line 2606
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2607
    const-string v1, "event_themes"

    const-string v2, "is_featured="

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-nez p2, :cond_0

    const/4 v2, 0x1

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "sort_order ASC"

    move-object v2, p3

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 435
    invoke-static {p0, p1, p2, p4}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 436
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 445
    :goto_0
    return-object v0

    .line 440
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 442
    invoke-static {p0, p1, p2, p3}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    invoke-static {p0, p1, p2, p4}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 445
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 552
    .line 553
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 554
    const-string v1, "events LEFT OUTER JOIN contacts ON (events.creator_gaia_id = contacts.gaia_id)"

    const-string v3, "event_id=?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v4, v2

    move-object v2, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;I[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 879
    .line 880
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 881
    const-string v1, "events LEFT OUTER JOIN contacts ON (events.creator_gaia_id = contacts.gaia_id)"

    const-string v3, "mine AND source = 1 AND deleted = 0"

    const-string v7, "end_time ASC"

    move-object v2, p2

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 887
    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)Lidh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 568
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 582
    :cond_0
    :goto_0
    return-object v0

    .line 572
    :cond_1
    sget-object v1, Ldrm;->h:[Ljava/lang/String;

    invoke-static {p0, p1, p2, v1}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 573
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 575
    const/4 v0, 0x0

    const/4 v2, 0x1

    :try_start_0
    invoke-static {v1, v0, v2}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 579
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/database/Cursor;II)Lidh;
    .locals 3

    .prologue
    .line 594
    :try_start_0
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 595
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 596
    if-eqz v2, :cond_1

    .line 597
    if-nez v0, :cond_0

    .line 598
    new-instance v1, Lidh;

    new-instance v0, Lpbl;

    invoke-direct {v0}, Lpbl;-><init>()V

    invoke-static {v0, v2}, Lpbl;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lpbl;

    invoke-direct {v1, v0}, Lidh;-><init>(Lpbl;)V

    move-object v0, v1

    .line 609
    :goto_0
    return-object v0

    .line 599
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 600
    new-instance v1, Lidh;

    new-instance v0, Lozp;

    invoke-direct {v0}, Lozp;-><init>()V

    .line 601
    invoke-static {v0, v2}, Lozp;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lozp;

    invoke-direct {v1, v0}, Lidh;-><init>(Lozp;)V
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 604
    :catch_0
    move-exception v0

    .line 605
    const-string v1, "EsEventData"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 606
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 609
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Loya;)Lidh;
    .locals 2

    .prologue
    .line 3238
    sget-object v0, Lpbl;->a:Loxr;

    invoke-virtual {p0, v0}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpbl;

    .line 3239
    if-eqz v0, :cond_0

    .line 3240
    new-instance v1, Lidh;

    invoke-direct {v1, v0}, Lidh;-><init>(Lpbl;)V

    move-object v0, v1

    .line 3243
    :goto_0
    return-object v0

    .line 3242
    :cond_0
    sget-object v0, Lozp;->a:Loxr;

    invoke-virtual {p0, v0}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lozp;

    .line 3243
    new-instance v1, Lidh;

    invoke-direct {v1, v0}, Lidh;-><init>(Lozp;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLkfp;)Lkff;
    .locals 12

    .prologue
    .line 491
    sget-object v11, Ldrm;->d:Ljava/lang/Object;

    monitor-enter v11

    .line 494
    :try_start_0
    new-instance v3, Lkfo;

    move-object/from16 v0, p9

    invoke-direct {v3, p0, p1, v0}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    .line 495
    if-eqz p8, :cond_1

    .line 496
    new-instance v1, Ldjn;

    move-object v2, p0

    move v4, p1

    move-object v5, p2

    move-object/from16 v6, p5

    move/from16 v7, p7

    invoke-direct/range {v1 .. v7}, Ldjn;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 502
    :goto_0
    invoke-virtual {v1}, Ldjn;->l()V

    .line 504
    invoke-virtual {v1}, Ldjn;->t()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 505
    const-string v2, "EsEventData"

    invoke-virtual {v1, v2}, Ldjn;->d(Ljava/lang/String;)V

    .line 508
    :cond_0
    monitor-exit v11

    return-object v1

    .line 499
    :cond_1
    new-instance v1, Ldjn;

    move-object v2, p0

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move/from16 v10, p7

    invoke-direct/range {v1 .. v10}, Ldjn;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 509
    :catchall_0
    move-exception v1

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Llah;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 3130
    const-string v1, "events"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "plus_one_data"

    aput-object v0, v2, v6

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3133
    if-nez v1, :cond_0

    .line 3147
    :goto_0
    return-object v5

    .line 3137
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3138
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3140
    new-instance v5, Llah;

    invoke-direct {v5}, Llah;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3147
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 3142
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Llah;->a([B)Llah;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 3147
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 3145
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Llto;)Lltp;
    .locals 5

    .prologue
    .line 2554
    const/4 v1, 0x0

    .line 2556
    if-eqz p0, :cond_4

    iget-object v0, p0, Llto;->c:[Lltp;

    if-eqz v0, :cond_4

    .line 2557
    const/4 v0, 0x0

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_0
    iget-object v2, p0, Llto;->c:[Lltp;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 2558
    iget-object v2, p0, Llto;->c:[Lltp;

    aget-object v2, v2, v1

    if-eqz v2, :cond_1

    .line 2559
    iget-object v2, p0, Llto;->c:[Lltp;

    aget-object v2, v2, v1

    iget v2, v2, Lltp;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2564
    iget-object v2, p0, Llto;->c:[Lltp;

    aget-object v2, v2, v1

    iget v2, v2, Lltp;->c:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Llto;->c:[Lltp;

    aget-object v2, v2, v1

    iget v2, v2, Lltp;->c:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    .line 2569
    :cond_0
    iget-object v0, p0, Llto;->c:[Lltp;

    aget-object v0, v0, v1

    .line 2557
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2570
    :cond_2
    if-nez v0, :cond_1

    .line 2572
    iget-object v0, p0, Llto;->c:[Lltp;

    aget-object v0, v0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    .line 2576
    :cond_4
    return-object v1
.end method

.method public static a([Lpay;I)Lpay;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2936
    if-nez p0, :cond_0

    .line 2958
    :goto_0
    return-object v2

    .line 2942
    :cond_0
    const/4 v0, 0x0

    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_3

    .line 2943
    aget-object v1, p0, v0

    .line 2944
    iget v3, v1, Lpay;->b:I

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_2

    .line 2947
    iget-object v3, v1, Lpay;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    iget-object v3, v1, Lpay;->e:Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    iget-object v3, v1, Lpay;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2948
    :cond_1
    iget v3, v1, Lpay;->b:I

    if-ne p1, v3, :cond_2

    move-object v0, v1

    :goto_2
    move-object v2, v0

    .line 2958
    goto :goto_0

    .line 2942
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;ILidh;Logr;)V
    .locals 11

    .prologue
    .line 1051
    .line 1052
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1053
    invoke-virtual {p2}, Lidh;->c()Ljava/lang/String;

    move-result-object v3

    .line 1055
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1056
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 1058
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1060
    :try_start_0
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p3, Logr;->i:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v5, p2

    move-object v6, p3

    move v10, p1

    invoke-static/range {v0 .. v10}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lidh;Logr;Ljava/lang/Long;Ljava/util/List;II)Z

    .line 1063
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1065
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1066
    return-void

    .line 1065
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILiuh;Llae;Llae;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Liuh;",
            "Llae;",
            "Llae;",
            "Ljava/util/ArrayList",
            "<",
            "Llae;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 3356
    .line 3357
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 3360
    invoke-virtual {p2}, Liuh;->d()Ljava/lang/String;

    move-result-object v1

    .line 3362
    const-string v3, "location_queries"

    const-string v4, "key=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v1, v5, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3366
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz p3, :cond_0

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p4, :cond_1

    invoke-virtual {v3, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p5, :cond_2

    invoke-virtual {v3, p5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3368
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 3369
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 3372
    const-string v5, "key"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3374
    const-string v1, "location_queries"

    const-string v5, "key"

    invoke-virtual {v2, v1, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v6

    .line 3376
    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-ltz v1, :cond_4

    .line 3377
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3379
    :try_start_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v0

    .line 3380
    :goto_0
    if-ge v1, v5, :cond_3

    .line 3381
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llae;

    .line 3382
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 3383
    const-string v8, "qrid"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3384
    const-string v8, "name"

    invoke-virtual {v0, p0}, Llae;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3385
    const-string v8, "location"

    invoke-static {v0}, Llae;->a(Llae;)[B

    move-result-object v0

    invoke-virtual {v4, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3387
    const-string v0, "locations"

    const-string v8, "qrid"

    invoke-virtual {v2, v0, v8, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 3380
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3391
    :cond_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3393
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3398
    :cond_4
    invoke-virtual {p2}, Liuh;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/EsProvider;->a(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3399
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3400
    return-void

    .line 3393
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lidh;Logr;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1019
    .line 1020
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1021
    invoke-virtual {p3}, Lidh;->c()Ljava/lang/String;

    move-result-object v3

    .line 1023
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1024
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 1026
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1028
    :try_start_0
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move v9, p1

    invoke-static/range {v0 .. v9}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lidh;Logr;Ljava/lang/Long;Ljava/util/List;I)Z

    .line 1031
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1033
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1037
    invoke-static {p0, v3, p1, p3}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;ILidh;)V

    .line 1039
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lidg;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1040
    return-void

    .line 1033
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Lidh;Logr;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZJLjava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Lidh;",
            "Logr;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ldrq;",
            ">;ZJ",
            "Ljava/util/List",
            "<",
            "Lpaf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1135
    .line 1136
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1137
    invoke-virtual/range {p3 .. p3}, Lidh;->c()Ljava/lang/String;

    move-result-object v5

    .line 1139
    const-class v2, Lhei;

    invoke-static {p0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhei;

    .line 1140
    move/from16 v0, p1

    invoke-interface {v2, v0}, Lhei;->a(I)Lhej;

    move-result-object v2

    .line 1142
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1144
    :try_start_0
    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1145
    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const/4 v10, 0x0

    move-object v2, p0

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v11, p1

    .line 1144
    invoke-static/range {v2 .. v11}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lidh;Logr;Ljava/lang/Long;Ljava/util/List;I)Z

    .line 1148
    if-eqz p11, :cond_0

    .line 1149
    invoke-interface/range {p11 .. p11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpaf;

    .line 1150
    iget-object v6, v2, Lpaf;->c:Ljava/lang/String;

    iget-object v7, v2, Lpaf;->b:Ljava/lang/String;

    iget-object v2, v2, Lpaf;->d:Ljava/lang/String;

    invoke-static {v5, v6, v7, v2, v4}, Ldrm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1165
    :catchall_0
    move-exception v2

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 1155
    :cond_0
    :try_start_1
    move-object/from16 v0, p5

    invoke-static {v4, v5, v0}, Ldrm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    move-object/from16 v0, p6

    invoke-static {v4, v5, v0}, Ldrm;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    const/4 v2, 0x1

    new-array v10, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v5, v10, v2

    const/4 v14, 0x1

    const-wide/16 v2, 0x0

    const-string v7, "events"

    const/4 v6, 0x1

    new-array v8, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v9, "display_time"

    aput-object v9, v8, v6

    const-string v9, "event_id=?"

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v6, v4

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-wide v2

    const/4 v6, 0x0

    :goto_1
    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    cmp-long v2, p9, v2

    if-eqz v2, :cond_1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "display_time"

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz v6, :cond_3

    const-string v3, "events"

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v6, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1158
    :cond_1
    :goto_2
    if-eqz p7, :cond_2

    .line 1159
    move-object/from16 v0, p7

    move/from16 v1, p8

    invoke-static {v4, v5, v0, v1}, Ldrm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Z)V

    .line 1163
    :cond_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1165
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1169
    move/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {p0, v5, v0, v1}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;ILidh;)V

    .line 1171
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lidg;->a:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1172
    return-void

    .line 1157
    :catchall_1
    move-exception v2

    :try_start_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_3
    const-string v3, "events"

    const-string v6, "event_id=?"

    invoke-virtual {v4, v3, v2, v6, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :cond_4
    move v6, v14

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/Long;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 955
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 956
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 979
    :cond_0
    :goto_0
    return-void

    .line 963
    :cond_1
    :try_start_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 969
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 970
    if-eqz p2, :cond_2

    .line 971
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    .line 973
    const-string v3, "instant_share_end_time"

    invoke-virtual {v1, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 974
    const-string v3, "events"

    const-string v4, "event_id=?"

    invoke-virtual {v0, v3, v1, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 976
    :cond_2
    const-string v2, "instant_share_end_time"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 977
    const-string v2, "events"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 966
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)V
    .locals 3

    .prologue
    .line 1523
    new-instance v1, Lhtd;

    invoke-direct {v1, p2, p3, p4, p5}, Lhtd;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 1525
    const-class v0, Liai;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liai;

    .line 1526
    const-class v2, Ldtv;

    invoke-interface {v0, p0, p1, v1, v2}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;Ljava/lang/Class;)V

    .line 1527
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const v5, 0x7f0a09ee

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2042
    const/4 v0, 0x0

    .line 2045
    invoke-static {p0, p1, p3, p4, v0}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2046
    const-string v1, "notif_type"

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2047
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2048
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2051
    invoke-static {}, Lfhu;->a()I

    move-result v1

    invoke-static {p0, v1, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2052
    new-instance v1, Lbs;

    invoke-direct {v1, p0}, Lbs;-><init>(Landroid/content/Context;)V

    .line 2053
    const v2, 0x7f02059f

    invoke-virtual {v1, v2}, Lbs;->a(I)Lbs;

    .line 2054
    invoke-virtual {v1, v4}, Lbs;->b(Z)Lbs;

    .line 2055
    const v2, 0x7f0a09ed

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    .line 2056
    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-virtual {p0, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbs;->e(Ljava/lang/CharSequence;)Lbs;

    .line 2057
    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-virtual {p0, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 2058
    invoke-virtual {v1, v0}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    .line 2060
    const-string v0, "notification"

    .line 2061
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 2064
    invoke-static {p0, p1}, Ljlp;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f1000a1

    .line 2065
    invoke-virtual {v1}, Lbs;->b()Landroid/app/Notification;

    move-result-object v1

    .line 2063
    invoke-virtual {v0, v2, v3, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2066
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ldrq;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1096
    .line 1097
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1099
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1101
    if-eqz p3, :cond_0

    .line 1102
    :try_start_0
    invoke-static {v1, p2, p3}, Ldrm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    :cond_0
    const/4 v0, 0x0

    invoke-static {v1, p2, v0}, Ldrm;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    if-eqz p4, :cond_1

    .line 1107
    invoke-static {v1, p2, p4, p5}, Ldrm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Z)V

    .line 1110
    :cond_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1112
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1115
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lidg;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1116
    return-void

    .line 1112
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 6

    .prologue
    .line 2390
    .line 2391
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2392
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2393
    const-string v2, "can_comment"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2394
    const-string v2, "events"

    const-string v3, "activity_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2397
    if-lez v0, :cond_0

    .line 2398
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lidg;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2400
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1688
    .line 1689
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1691
    const/4 v8, 0x0

    .line 1692
    const-string v1, "events"

    sget-object v2, Ldrm;->i:[Ljava/lang/String;

    const-string v3, "event_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1695
    const/4 v0, 0x0

    .line 1697
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1698
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v2, v0, v1}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;

    move-result-object v1

    .line 1701
    const/4 v0, 0x2

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1702
    if-eqz v0, :cond_8

    .line 1703
    const-class v3, Lpax;

    invoke-static {v0, v3}, Lfut;->a([BLjava/lang/Class;)[Loxu;

    move-result-object v0

    check-cast v0, [Lpax;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    .line 1707
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1710
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 1732
    :cond_0
    :goto_1
    return-void

    .line 1707
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1714
    :cond_1
    const/4 v3, 0x0

    .line 1715
    array-length v5, v1

    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, v5, :cond_7

    aget-object v2, v1, v4

    .line 1716
    iget-object v6, v2, Lpax;->b:Lpaf;

    if-eqz v6, :cond_4

    iget-object v6, v2, Lpax;->b:Lpaf;

    iget-object v6, v6, Lpaf;->c:Ljava/lang/String;

    .line 1717
    invoke-static {p4, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, v2, Lpax;->b:Lpaf;

    iget-object v6, v6, Lpaf;->e:Ljava/lang/String;

    .line 1718
    invoke-static {p5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1724
    :goto_3
    if-eqz v2, :cond_0

    iget-object v3, v2, Lpax;->f:Ljava/lang/Boolean;

    if-eqz v3, :cond_2

    iget-object v3, v2, Lpax;->f:Ljava/lang/Boolean;

    .line 1726
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v3, p3, :cond_0

    .line 1727
    :cond_2
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lpax;->f:Ljava/lang/Boolean;

    .line 1728
    invoke-static {p0, p1, p2, v1}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;[Lpax;)V

    .line 1729
    iget v3, v2, Lpax;->c:I

    iget-object v1, v2, Lpax;->d:Ljava/lang/Integer;

    .line 1730
    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    add-int/lit8 v4, v1, 0x1

    .line 1729
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lidh;->i()Lpbj;

    move-result-object v1

    iget-object v2, v1, Lpbj;->c:[Lpay;

    if-eqz v2, :cond_0

    iget-object v5, v1, Lpbj;->c:[Lpay;

    array-length v6, v5

    const/4 v1, 0x0

    move v2, v1

    :goto_4
    if-ge v2, v6, :cond_6

    aget-object v7, v5, v2

    if-eqz p3, :cond_5

    const/4 v1, -0x1

    :goto_5
    iget-object v8, v7, Lpay;->c:Ljava/lang/Integer;

    if-eqz v8, :cond_3

    iget v8, v7, Lpay;->b:I

    if-ne v8, v3, :cond_3

    iget-object v8, v7, Lpay;->c:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    mul-int/2addr v1, v4

    add-int/2addr v1, v8

    const/4 v8, 0x0

    invoke-static {v1, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v7, Lpay;->c:Ljava/lang/Integer;

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 1715
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 1729
    :cond_5
    const/4 v1, 0x1

    goto :goto_5

    :cond_6
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "event_data"

    invoke-virtual {v0}, Lidh;->f()[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v4, "event_type"

    invoke-virtual {v0}, Lidh;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "events"

    const-string v4, "event_id=?"

    invoke-virtual {v1, v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lidg;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_1

    :cond_7
    move-object v2, v3

    goto/16 :goto_3

    :cond_8
    move-object v0, v1

    move-object v1, v8

    goto/16 :goto_0

    :cond_9
    move-object v1, v8

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;[Lpax;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 1656
    const-string v0, "EsEventData"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1657
    array-length v2, p3

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p3, v0

    .line 1658
    invoke-virtual {v3}, Lpax;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x19

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "[INSERT_EVENT_INVITEE]; "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1664
    :cond_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1666
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object p2, v2, v1

    .line 1669
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1671
    const-string v3, "invitee_roster_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1672
    const-string v3, "invitee_roster_proto"

    .line 1673
    invoke-static {p3}, Lfut;->a([Loxu;)[B

    move-result-object v4

    .line 1672
    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1675
    invoke-static {p0, p2, v7, p3, v0}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;[Lpax;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1677
    const-string v3, "events"

    const-string v4, "event_id=?"

    invoke-virtual {v0, v3, v1, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1679
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lidg;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1680
    return-void
.end method

.method public static a(Landroid/content/Context;ILkfp;Lles;)V
    .locals 26

    .prologue
    .line 2992
    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3008
    :goto_0
    return-void

    .line 2996
    :cond_0
    const-string v4, "Event Themes"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lkfp;->c(Ljava/lang/String;)V

    .line 2998
    const-class v4, Ljgn;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljgn;

    .line 2999
    invoke-interface {v4}, Ljgn;->j()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v4

    if-nez v4, :cond_f

    .line 3001
    invoke-static/range {p0 .. p3}, Ldrm;->c(Landroid/content/Context;ILkfp;Lles;)V

    .line 3004
    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v4

    if-nez v4, :cond_f

    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v4

    invoke-virtual {v4}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v14, 0x0

    const/4 v12, 0x0

    :try_start_0
    const-string v5, "event_themes"

    sget-object v6, Ldrw;->a:[Ljava/lang/String;

    const-string v7, "placeholder_path IS NOT NULL"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "sort_order ASC"

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v17

    :try_start_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v16

    const-wide/16 v6, 0x0

    const-wide/16 v20, 0x0

    new-instance v8, Landroid/util/SparseIntArray;

    invoke-direct {v8}, Landroid/util/SparseIntArray;-><init>()V

    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    move-wide v12, v6

    :goto_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v5, Ljava/io/File;

    const/4 v6, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    const-wide/16 v10, 0x0

    cmp-long v7, v20, v10

    if-nez v7, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v10, 0x0

    cmp-long v5, v6, v10

    if-lez v5, :cond_3

    const-wide/32 v10, 0x100000

    div-long v20, v10, v6

    move-wide v12, v6

    goto :goto_1

    :cond_1
    if-nez v6, :cond_2

    const/4 v5, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v8, v5, v6}, Landroid/util/SparseIntArray;->append(II)V

    :cond_2
    move-wide v6, v12

    :cond_3
    move-wide v12, v6

    goto :goto_1

    :cond_4
    invoke-virtual {v8}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    if-lez v5, :cond_9

    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    const-string v5, "placeholder_path"

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuffer;

    const/16 v5, 0x100

    invoke-direct {v7, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v5, "theme_id IN("

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v6, 0x1

    invoke-virtual {v8}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    move/from16 v25, v5

    move v5, v6

    move/from16 v6, v25

    :goto_2
    if-ltz v6, :cond_8

    if-eqz v5, :cond_5

    const/4 v5, 0x0

    :goto_3
    invoke-virtual {v8, v6}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    :cond_5
    const/16 v9, 0x2c

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v4

    move-object/from16 v5, v17

    move-object v6, v14

    :goto_4
    if-eqz v5, :cond_6

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_6
    if-eqz v6, :cond_7

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v4

    :cond_8
    const/16 v5, 0x29

    :try_start_2
    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v5, "event_themes"

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_9
    const-string v5, "event_themes"

    sget-object v6, Ldrw;->a:[Ljava/lang/String;

    const-string v7, "placeholder_path IS NULL"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "sort_order ASC"

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v18

    :try_start_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_d

    invoke-static/range {p0 .. p0}, Ldrm;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v23

    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    const/high16 v5, 0x3e800000    # 0.25f

    invoke-static/range {p0 .. p0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v6}, Llsc;->a(Landroid/util/DisplayMetrics;)F

    move-result v6

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v8}, Ldrm;->a(I)I

    move-result v9

    const-class v5, Lizs;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lizs;

    move-wide v14, v12

    move-wide/from16 v12, v20

    :goto_5
    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v6

    if-nez v6, :cond_d

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_d

    const-wide/16 v6, 0x0

    cmp-long v6, v14, v6

    if-eqz v6, :cond_a

    move/from16 v0, v16

    int-to-long v6, v0

    cmp-long v6, v6, v12

    if-gez v6, :cond_d

    :cond_a
    const/4 v6, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/4 v6, 0x3

    move-object/from16 v0, v18

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    sget-object v6, Ljac;->a:Ljac;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-static {v0, v1, v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v6

    const/4 v11, 0x0

    const/4 v7, 0x0

    const/16 v10, 0x8

    :try_start_4
    invoke-virtual/range {v5 .. v10}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, [B

    move-object v6, v0
    :try_end_4
    .catch Lkdn; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lkde; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object/from16 v19, v6

    :goto_6
    if-eqz v19, :cond_11

    :try_start_5
    new-instance v24, Ljava/io/File;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-wide/16 v6, 0x0

    cmp-long v6, v14, v6

    if-nez v6, :cond_10

    move-object/from16 v0, v19

    array-length v6, v0

    int-to-long v10, v6

    const-wide/32 v6, 0x100000

    div-long/2addr v6, v10

    :goto_7
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result-object v14

    :try_start_6
    new-instance v12, Ljava/io/FileOutputStream;

    move-object/from16 v0, v24

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    add-int/lit8 v16, v16, 0x1

    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    const-string v12, "placeholder_path"

    move-object/from16 v0, v22

    invoke-virtual {v0, v12, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "event_themes"

    const-string v12, "theme_id = "

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v19

    if-eqz v19, :cond_c

    invoke-virtual {v15, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    :goto_8
    const/4 v15, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v4, v13, v0, v12, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_b

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :cond_b
    move-wide v12, v6

    move-wide v14, v10

    goto/16 :goto_5

    :catch_0
    move-exception v6

    :try_start_7
    const-string v7, "EsEventData"

    const-string v10, "Cannot download event theme"

    invoke-static {v7, v10, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v19, v11

    goto :goto_6

    :catch_1
    move-exception v7

    const-string v10, "EsEventData"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    add-int/lit8 v24, v24, 0x9

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v24, "Canceled "

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-object/from16 v19, v11

    goto/16 :goto_6

    :cond_c
    :try_start_8
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v15}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_8

    :catch_2
    move-exception v12

    move-object v13, v12

    move/from16 v12, v16

    :try_start_9
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v14, "EsEventData"

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, 0x26

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int v19, v19, v20

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v19, "Cannot write event placeholder file: "

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v19, " "

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :goto_9
    move-wide v14, v10

    move/from16 v16, v12

    move-wide v12, v6

    goto/16 :goto_5

    :cond_d
    if-eqz v17, :cond_e

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    :cond_e
    if-eqz v18, :cond_f

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 3007
    :cond_f
    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lkfp;->m(I)V

    goto/16 :goto_0

    .line 3004
    :catchall_1
    move-exception v4

    move-object v5, v12

    move-object v6, v14

    goto/16 :goto_4

    :catchall_2
    move-exception v4

    move-object/from16 v5, v17

    move-object/from16 v6, v18

    goto/16 :goto_4

    :cond_10
    move-wide v6, v12

    move-wide v10, v14

    goto/16 :goto_7

    :cond_11
    move-wide v6, v12

    move-wide v10, v14

    move/from16 v12, v16

    goto :goto_9
.end method

.method public static a(Landroid/content/Context;I[Llto;)V
    .locals 17
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    .line 2442
    .line 2443
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2444
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2446
    :try_start_0
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 2447
    const-string v3, "event_themes"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "theme_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "is_default"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "is_featured"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "image_url"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "sort_order"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "placeholder_path"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 2457
    :goto_0
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2458
    new-instance v5, Ldry;

    invoke-direct {v5}, Ldry;-><init>()V

    .line 2459
    const/4 v3, 0x0

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 2460
    const/4 v3, 0x1

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    iput-boolean v3, v5, Ldry;->a:Z

    .line 2461
    const/4 v3, 0x2

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_2
    iput-boolean v3, v5, Ldry;->b:Z

    .line 2462
    const/4 v3, 0x3

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ldry;->c:Ljava/lang/String;

    .line 2463
    const/4 v3, 0x4

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v5, Ldry;->d:I

    .line 2464
    const/4 v3, 0x5

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Ldry;->e:Ljava/lang/String;

    .line 2465
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2468
    :catchall_0
    move-exception v3

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2541
    :catchall_1
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 2460
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 2461
    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    .line 2468
    :cond_2
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 2471
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7, v10}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 2473
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 2475
    move-object/from16 v0, p2

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    move v6, v3

    :goto_3
    if-ltz v6, :cond_b

    .line 2476
    aget-object v9, p2, v6

    .line 2477
    iget-object v3, v9, Llto;->b:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    .line 2478
    invoke-static {v9}, Ldrm;->b(Llto;)Ljava/lang/String;

    move-result-object v11

    .line 2482
    if-eqz v11, :cond_7

    .line 2483
    iget-object v3, v9, Llto;->b:Ljava/lang/Integer;

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2488
    const/4 v4, 0x0

    .line 2489
    const/4 v3, 0x0

    .line 2490
    iget-object v5, v9, Llto;->d:[Lltj;

    if-eqz v5, :cond_5

    .line 2491
    iget-object v12, v9, Llto;->d:[Lltj;

    array-length v13, v12

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v13, :cond_5

    aget-object v14, v12, v5

    .line 2492
    iget v15, v14, Lltj;->b:I

    const/16 v16, 0x6

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 2493
    const/4 v3, 0x1

    .line 2491
    :cond_3
    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 2494
    :cond_4
    iget v14, v14, Lltj;->b:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_3

    .line 2496
    const/4 v4, 0x1

    goto :goto_5

    :cond_5
    move v5, v4

    move v4, v3

    .line 2501
    iget-object v3, v9, Llto;->b:Ljava/lang/Integer;

    invoke-virtual {v10, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldry;

    .line 2502
    if-eqz v3, :cond_6

    iget-boolean v12, v3, Ldry;->a:Z

    if-ne v12, v5, :cond_6

    iget-boolean v12, v3, Ldry;->b:Z

    if-ne v12, v4, :cond_6

    iget-object v12, v3, Ldry;->c:Ljava/lang/String;

    .line 2505
    invoke-static {v12, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_6

    iget v12, v3, Ldry;->d:I

    if-eq v12, v6, :cond_7

    .line 2507
    :cond_6
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    .line 2508
    const-string v12, "is_featured"

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    :goto_6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v8, v12, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2509
    const-string v12, "is_default"

    if-eqz v5, :cond_9

    const/4 v4, 0x1

    :goto_7
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v8, v12, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2510
    const-string v4, "image_url"

    invoke-virtual {v8, v4, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2511
    const-string v4, "sort_order"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2513
    if-eqz v3, :cond_a

    .line 2514
    const-string v3, "event_themes"

    const-string v4, "theme_id = "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v9, Llto;->b:Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x0

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v8, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2475
    :cond_7
    :goto_8
    add-int/lit8 v3, v6, -0x1

    move v6, v3

    goto/16 :goto_3

    .line 2508
    :cond_8
    const/4 v4, 0x0

    goto :goto_6

    .line 2509
    :cond_9
    const/4 v4, 0x0

    goto :goto_7

    .line 2517
    :cond_a
    const-string v3, "theme_id"

    iget-object v4, v9, Llto;->b:Ljava/lang/Integer;

    invoke-virtual {v8, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2518
    const-string v3, "event_themes"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_8

    .line 2523
    :cond_b
    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_c
    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 2524
    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ldry;

    .line 2526
    const-string v6, "event_themes"

    const-string v9, "theme_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    .line 2527
    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v11

    .line 2526
    invoke-virtual {v2, v6, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2529
    iget-object v3, v4, Ldry;->e:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 2530
    new-instance v3, Ljava/io/File;

    iget-object v4, v4, Ldry;->e:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_9

    .line 2534
    :cond_d
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    .line 2535
    const-string v3, "event_themes_sync_time"

    .line 2536
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2535
    invoke-virtual {v8, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2537
    const-string v3, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v8, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2539
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2541
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2542
    return-void
.end method

.method public static a(Landroid/content/Context;I[Loya;[Loya;[Loya;[Lpaf;)V
    .locals 8

    .prologue
    .line 2273
    .line 2274
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2276
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 2277
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "gaia_id"

    .line 2278
    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2279
    invoke-static {v1}, Ldrm;->b(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;

    move-result-object v4

    .line 2280
    const/4 v0, 0x5

    new-array v5, v0, [I

    .line 2282
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    move-object v0, p0

    move-object v3, p2

    move-object v6, p5

    move v7, p1

    .line 2285
    :try_start_0
    invoke-static/range {v0 .. v7}, Ldrm;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Loya;Ljava/util/Set;[I[Lpaf;I)V

    move-object v0, p0

    move-object v3, p3

    move-object v6, p5

    move v7, p1

    .line 2288
    invoke-static/range {v0 .. v7}, Ldrm;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Loya;Ljava/util/Set;[I[Lpaf;I)V

    move-object v0, p0

    move-object v3, p4

    move-object v6, p5

    move v7, p1

    .line 2291
    invoke-static/range {v0 .. v7}, Ldrm;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Loya;Ljava/util/Set;[I[Lpaf;I)V

    .line 2294
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2295
    const/4 v3, 0x0

    invoke-static {v1, p0, p1, v0, v3}, Ldrm;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;ILjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2308
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 2298
    :cond_0
    const/4 v0, 0x3

    :try_start_1
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v2

    aput v2, v5, v0

    .line 2301
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2302
    const-string v2, "event_list_sync_time"

    .line 2303
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2302
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2305
    const-string v2, "account_status"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2306
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2308
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2311
    const-string v0, "EsEventData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2312
    const/4 v0, 0x0

    aget v0, v5, v0

    const/4 v1, 0x1

    aget v1, v5, v1

    const/4 v2, 0x2

    aget v2, v5, v2

    const/4 v3, 0x3

    aget v3, v5, v3

    const/4 v4, 0x4

    aget v4, v5, v4

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x81

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "[INSERT_EVENT_LIST]; "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " inserted, "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " changed, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not changed, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " removed, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2322
    :cond_1
    invoke-static {p0}, Lhqd;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2321
    invoke-static {p0, p1, v0}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;)Lidh;

    move-result-object v0

    .line 2323
    if-eqz v0, :cond_2

    .line 2324
    invoke-virtual {v0}, Lidh;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, p1, v0}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;ILidh;)V

    .line 2327
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2328
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lidg;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2329
    return-void
.end method

.method static a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 3216
    new-instance v0, Llah;

    invoke-direct {v0, p3, p4, p5}, Llah;-><init>(Ljava/lang/String;IZ)V

    .line 3219
    :try_start_0
    invoke-static {v0}, Llah;->a(Llah;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3225
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 3226
    const-string v2, "plus_one_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 3227
    const-string v0, "events"

    const-string v2, "activity_id=?"

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3230
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lidg;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3231
    :goto_0
    return-void

    .line 3220
    :catch_0
    move-exception v0

    .line 3221
    const-string v1, "EsEventData"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not serialize DbPlusOneData "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Loya;Ljava/util/Set;[I[Lpaf;I)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "[",
            "Loya;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;[I[",
            "Lpaf;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2239
    if-nez p3, :cond_1

    .line 2259
    :cond_0
    return-void

    .line 2243
    :cond_1
    const/4 v1, 0x0

    move v11, v1

    :goto_0
    move-object/from16 v0, p3

    array-length v1, v0

    if-ge v11, v1, :cond_0

    .line 2244
    aget-object v1, p3, v11

    invoke-static {v1}, Ldrm;->a(Loya;)Lidh;

    move-result-object v6

    .line 2245
    invoke-static {v6, p2}, Ldrm;->b(Lidh;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2246
    const/4 v1, 0x4

    aget v2, p5, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, p5, v1

    .line 2243
    :goto_1
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_0

    .line 2250
    :cond_2
    invoke-virtual {v6}, Lidh;->c()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v12

    .line 2252
    invoke-virtual {v6}, Lidh;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 2253
    invoke-static/range {p6 .. p6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move/from16 v10, p7

    .line 2251
    invoke-static/range {v1 .. v10}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lidh;Logr;Ljava/lang/Long;Ljava/util/List;I)Z

    move-result v1

    .line 2254
    if-eqz v1, :cond_4

    if-eqz v12, :cond_3

    const/4 v1, 0x1

    .line 2257
    :goto_2
    aget v2, p5, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, p5, v1

    goto :goto_1

    .line 2254
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x2

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Lidh;I)V
    .locals 5

    .prologue
    .line 2361
    invoke-virtual {p1}, Lidh;->i()Lpbj;

    move-result-object v0

    iget-object v1, v0, Lpbj;->c:[Lpay;

    .line 2362
    if-eqz v1, :cond_0

    array-length v0, v1

    if-lez v0, :cond_0

    .line 2363
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 2364
    aget-object v2, v1, v0

    .line 2365
    iget-object v3, v2, Lpay;->e:Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    iget-object v3, v2, Lpay;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, v2, Lpay;->b:I

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_1

    iget v3, v2, Lpay;->b:I

    const/4 v4, 0x7

    if-eq v3, v4, :cond_1

    .line 2368
    iget v0, v2, Lpay;->b:I

    invoke-static {p0, p1, p2, v0}, Ldrm;->a(Landroid/content/Context;Lidh;II)V

    .line 2373
    :cond_0
    return-void

    .line 2363
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lidh;II)V
    .locals 4

    .prologue
    .line 784
    if-eqz p1, :cond_1

    .line 785
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 786
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 787
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 789
    invoke-virtual {p1}, Lidh;->i()Lpbj;

    move-result-object v1

    .line 790
    iget-object v2, v1, Lpbj;->h:Lpax;

    if-nez v2, :cond_0

    .line 791
    new-instance v2, Lpax;

    invoke-direct {v2}, Lpax;-><init>()V

    iput-object v2, v1, Lpbj;->h:Lpax;

    .line 792
    iget-object v2, v1, Lpbj;->h:Lpax;

    new-instance v3, Lpaf;

    invoke-direct {v3}, Lpaf;-><init>()V

    iput-object v3, v2, Lpax;->b:Lpaf;

    .line 793
    iget-object v2, v1, Lpbj;->h:Lpax;

    iget-object v2, v2, Lpax;->b:Lpaf;

    iput-object v0, v2, Lpaf;->c:Ljava/lang/String;

    .line 796
    :cond_0
    iget-object v0, v1, Lpbj;->h:Lpax;

    iput p3, v0, Lpax;->c:I

    .line 798
    :cond_1
    return-void
.end method

.method private static a(Landroid/content/Context;Lidh;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lidh;",
            "Ljava/util/List",
            "<",
            "Lpaf;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1200
    invoke-virtual {p1}, Lidh;->c()Ljava/lang/String;

    move-result-object v4

    .line 1202
    invoke-virtual {p1}, Lidh;->k()Lpai;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1204
    invoke-virtual {p1}, Lidh;->k()Lpai;

    move-result-object v5

    .line 1203
    if-eqz v5, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, v5, Lpai;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    if-nez v3, :cond_2

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpaf;

    iget-object v6, v0, Lpaf;->c:Ljava/lang/String;

    iget-object v7, v5, Lpai;->d:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v3, v0, Lpaf;->c:Ljava/lang/String;

    iget-object v6, v0, Lpaf;->b:Ljava/lang/String;

    iget-object v0, v0, Lpaf;->d:Ljava/lang/String;

    invoke-static {v4, v3, v6, v0, p3}, Ldrm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    const/4 v3, 0x1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v3, v2

    :cond_2
    if-nez v3, :cond_3

    if-eqz v5, :cond_3

    iget-object v0, v5, Lpai;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, v5, Lpai;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, v5, Lpai;->d:Ljava/lang/String;

    iget-object v1, v5, Lpai;->c:Ljava/lang/String;

    iget-object v3, v5, Lpai;->b:Ljava/lang/String;

    invoke-static {v4, v0, v1, v3, p3}, Ldrm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1206
    :cond_3
    invoke-virtual {p1}, Lidh;->i()Lpbj;

    move-result-object v0

    .line 1207
    iget-object v1, v0, Lpbj;->c:[Lpay;

    if-eqz v1, :cond_4

    .line 1209
    iget-object v0, v0, Lpbj;->c:[Lpay;

    array-length v1, v0

    :goto_1
    if-ge v2, v1, :cond_4

    aget-object v3, v0, v2

    .line 1210
    iget-object v3, v3, Lpay;->d:[Lpax;

    invoke-static {p0, v4, p2, v3, p3}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;[Lpax;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1209
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1213
    :cond_4
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;ILidh;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 1176
    const-class v0, Lhpu;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 1178
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1179
    invoke-static {p0}, Lhqd;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1180
    invoke-virtual {v0, p2}, Lhpu;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1181
    invoke-static {p0, p2, p3}, Ldrm;->c(Landroid/content/Context;ILidh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1182
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lidh;->n()J

    move-result-wide v2

    invoke-static {p0}, Lhqd;->g(Landroid/content/Context;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    cmp-long v0, v4, v2

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 1183
    const-class v0, Lhrt;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrt;

    new-instance v2, Ldwd;

    invoke-direct {v2, p3}, Ldwd;-><init>(Lidh;)V

    .line 1184
    invoke-virtual {v0, p0, p2, v1, v2}, Lhrt;->a(Landroid/content/Context;IZLhrv;)V

    .line 1187
    :cond_0
    return-void

    .line 1182
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;[Lpax;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lpaf;",
            ">;[",
            "Lpax;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1227
    if-eqz p3, :cond_0

    .line 1228
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    .line 1229
    aget-object v1, p3, v0

    .line 1230
    iget-object v2, v1, Lpax;->b:Lpaf;

    invoke-static {p1, v2, p2, p4}, Ldrm;->a(Ljava/lang/String;Lpaf;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1232
    iget-object v1, v1, Lpax;->e:Lpaf;

    invoke-static {p1, v1, p2, p4}, Ldrm;->a(Ljava/lang/String;Lpaf;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1228
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1236
    :cond_0
    return-void
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2969
    const-string v0, "events"

    const-string v1, "mine = 0"

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2972
    const-string v0, "events"

    const-string v1, "deleted = 1"

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2976
    const-string v0, "event_activities"

    invoke-virtual {p0, v0, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2979
    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 2980
    const-string v1, "resume_token"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2981
    const-string v1, "polling_token"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2982
    const-string v1, "events"

    invoke-virtual {p0, v1, v0, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2984
    invoke-static {p0}, Ldrm;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2985
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2404
    new-array v2, v1, [Ljava/lang/String;

    aput-object p3, v2, v0

    .line 2405
    const-string v3, "events"

    const-string v4, "event_id=?"

    invoke-virtual {p0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2410
    invoke-static {p1}, Lhqd;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2412
    const-class v0, Lhrt;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrt;

    invoke-virtual {v0, p1, p2}, Lhrt;->a(Landroid/content/Context;I)V

    move v0, v1

    .line 2415
    :cond_0
    if-lez v2, :cond_1

    if-eqz p4, :cond_1

    .line 2416
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lidg;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2419
    :cond_1
    const-string v1, "EsEventData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2420
    if-eqz v0, :cond_3

    const-string v0, "; disable IS"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "[DELETE_EVENT], id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2422
    :cond_2
    return-void

    .line 2420
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 1539
    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v8

    .line 1544
    const-string v1, "events"

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "polling_token"

    aput-object v0, v2, v8

    const-string v3, "event_id=?"

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1548
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1549
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    move v1, v8

    .line 1553
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1556
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1557
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1558
    const-string v2, "polling_token"

    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1560
    if-eqz v1, :cond_1

    .line 1561
    const-string v1, "events"

    invoke-virtual {p0, v1, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1566
    :cond_0
    :goto_1
    return-void

    .line 1553
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1563
    :cond_1
    const-string v1, "events"

    const-string v2, "event_id=?"

    invoke-virtual {p0, v1, v0, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    move-object v0, v5

    move v1, v9

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ldrq;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2099
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v6, v2

    .line 2102
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 2105
    const-string v3, "event_activities"

    const/4 v2, 0x5

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "type"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string v5, "owner_gaia_id"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "timestamp"

    aput-object v5, v4, v2

    const/4 v2, 0x4

    const-string v5, "fingerprint"

    aput-object v5, v4, v2

    const-string v5, "event_id=?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 2115
    :goto_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2116
    new-instance v2, Ldrr;

    invoke-direct {v2}, Ldrr;-><init>()V

    .line 2117
    new-instance v4, Ldrs;

    invoke-direct {v4}, Ldrs;-><init>()V

    .line 2119
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ldrs;->a:Ljava/lang/String;

    .line 2120
    const/4 v5, 0x1

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v2, Ldrr;->a:I

    .line 2121
    const/4 v5, 0x2

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Ldrr;->b:Ljava/lang/String;

    .line 2122
    const/4 v5, 0x3

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v2, Ldrr;->c:J

    .line 2123
    const/4 v5, 0x4

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v4, Ldrs;->b:I

    .line 2125
    invoke-interface {v11, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2128
    :catchall_0
    move-exception v2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2132
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 2133
    new-instance v13, Ldrr;

    invoke-direct {v13}, Ldrr;-><init>()V

    .line 2135
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldrq;

    .line 2136
    iget v3, v2, Ldrq;->a:I

    iput v3, v13, Ldrr;->a:I

    .line 2137
    iget-object v3, v2, Ldrq;->c:Ljava/lang/String;

    iput-object v3, v13, Ldrr;->b:Ljava/lang/String;

    .line 2138
    iget-wide v4, v2, Ldrq;->b:J

    iput-wide v4, v13, Ldrr;->c:J

    .line 2140
    invoke-interface {v11, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldrs;

    .line 2141
    iget-object v4, v2, Ldrq;->e:[B

    if-nez v4, :cond_1

    const/4 v4, 0x0

    move v5, v4

    .line 2142
    :goto_2
    const/4 v8, 0x0

    .line 2143
    const/4 v9, 0x0

    .line 2144
    const/4 v7, 0x0

    .line 2145
    iget v4, v2, Ldrq;->a:I

    const/16 v10, 0x64

    if-ne v4, v10, :cond_2

    .line 2146
    const/4 v10, 0x0

    .line 2149
    :try_start_1
    new-instance v4, Lnzx;

    invoke-direct {v4}, Lnzx;-><init>()V

    iget-object v15, v2, Ldrq;->e:[B

    invoke-static {v4, v15}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v4

    check-cast v4, Lnzx;

    .line 2150
    sget-object v15, Lnzu;->a:Loxr;

    invoke-virtual {v4, v15}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnzu;

    iget-object v4, v4, Lnzu;->b:Lnym;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_0

    .line 2155
    :goto_3
    if-eqz v4, :cond_8

    .line 2156
    iget-object v7, v4, Lnym;->b:Lnyl;

    iget-object v7, v7, Lnyl;->b:Ljava/lang/String;

    invoke-static {v7}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2157
    iget-object v4, v4, Lnym;->e:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    :goto_4
    move-object v8, v7

    move-object v7, v4

    move-object v4, v9

    .line 2166
    :goto_5
    if-nez v3, :cond_3

    .line 2167
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 2168
    const-string v3, "tile_id"

    iget-object v9, v2, Ldrq;->f:Ljava/lang/String;

    invoke-virtual {v12, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2169
    const-string v3, "event_id"

    move-object/from16 v0, p1

    invoke-virtual {v12, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2170
    const-string v3, "type"

    iget v9, v2, Ldrq;->a:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v12, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2171
    const-string v3, "timestamp"

    iget-wide v0, v2, Ldrq;->b:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v12, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2172
    const-string v3, "owner_gaia_id"

    iget-object v9, v2, Ldrq;->c:Ljava/lang/String;

    invoke-virtual {v12, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2173
    const-string v3, "owner_name"

    iget-object v9, v2, Ldrq;->d:Ljava/lang/String;

    invoke-virtual {v12, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2174
    const-string v3, "data"

    iget-object v2, v2, Ldrq;->e:[B

    invoke-virtual {v12, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2175
    const-string v2, "url"

    invoke-virtual {v12, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2176
    const-string v2, "comment"

    invoke-virtual {v12, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2177
    const-string v2, "fingerprint"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2178
    const-string v2, "photo_id"

    invoke-virtual {v12, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2179
    const-string v2, "event_activities"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v12}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/16 :goto_1

    .line 2141
    :cond_1
    iget-object v4, v2, Ldrq;->e:[B

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([B)I

    move-result v4

    move v5, v4

    goto/16 :goto_2

    .line 2151
    :catch_0
    move-exception v4

    .line 2152
    const-string v15, "EsEventData"

    const-string v16, "Unable to parse Tile from byte array."

    move-object/from16 v0, v16

    invoke-static {v15, v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v10

    goto/16 :goto_3

    .line 2159
    :cond_2
    iget v4, v2, Ldrq;->a:I

    const/4 v10, 0x5

    if-ne v4, v10, :cond_7

    .line 2160
    sget-object v4, Ldrm;->a:Lesn;

    iget-object v10, v2, Ldrq;->e:[B

    invoke-virtual {v4, v10}, Lesn;->a([B)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ldru;

    .line 2161
    if-eqz v4, :cond_7

    .line 2162
    iget-object v4, v4, Ldru;->text:Ljava/lang/String;

    goto/16 :goto_5

    .line 2181
    :cond_3
    iget v7, v3, Ldrs;->b:I

    if-eq v5, v7, :cond_4

    .line 2182
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 2183
    const-string v7, "tile_id"

    iget-object v9, v2, Ldrq;->f:Ljava/lang/String;

    invoke-virtual {v12, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2184
    const-string v7, "data"

    iget-object v2, v2, Ldrq;->e:[B

    invoke-virtual {v12, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2185
    const-string v2, "url"

    invoke-virtual {v12, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2186
    const-string v2, "comment"

    invoke-virtual {v12, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2187
    const-string v2, "fingerprint"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v12, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2188
    const-string v2, "event_activities"

    const-string v4, "_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v3, v3, Ldrs;->a:Ljava/lang/String;

    aput-object v3, v5, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2192
    :cond_4
    invoke-interface {v11, v13}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 2197
    :cond_5
    if-nez p3, :cond_6

    .line 2198
    invoke-interface {v11}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldrs;

    .line 2199
    const-string v4, "event_activities"

    const-string v5, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v2, v2, Ldrs;->a:Ljava/lang/String;

    aput-object v2, v7, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_6

    .line 2205
    :cond_6
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 2206
    const-string v2, "activity_refresh_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2207
    const-string v2, "events"

    const-string v3, "event_id=?"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2208
    return-void

    :cond_7
    move-object v4, v9

    goto/16 :goto_5

    :cond_8
    move-object v4, v7

    move-object v7, v8

    goto/16 :goto_4
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1323
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 1350
    :cond_0
    :goto_0
    return-void

    .line 1327
    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    aput-object p0, v2, v0

    aput-object p1, v2, v1

    .line 1332
    :try_start_0
    const-string v3, "SELECT event_id FROM event_people WHERE event_id=? AND gaia_id=?"

    invoke-static {p4, v3, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1341
    :goto_1
    if-eqz v0, :cond_2

    .line 1342
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1343
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1346
    const-string v1, "event_people"

    const/4 v2, 0x0

    invoke-virtual {p4, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1349
    :cond_2
    invoke-static {p4, p1, p2, p3}, Ldsm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Lpaf;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lpaf;",
            "Ljava/util/List",
            "<",
            "Lpaf;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1251
    .line 1253
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    iget-object v1, p1, Lpaf;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    move v1, v0

    move v2, v0

    .line 1254
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    if-nez v2, :cond_1

    .line 1255
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpaf;

    .line 1257
    iget-object v3, v0, Lpaf;->c:Ljava/lang/String;

    iget-object v4, p1, Lpaf;->c:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1258
    iget-object v2, v0, Lpaf;->c:Ljava/lang/String;

    iget-object v3, v0, Lpaf;->b:Ljava/lang/String;

    iget-object v0, v0, Lpaf;->d:Ljava/lang/String;

    invoke-static {p0, v2, v3, v0, p3}, Ldrm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1260
    const/4 v2, 0x1

    .line 1254
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1266
    :cond_2
    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    iget-object v0, p1, Lpaf;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lpaf;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1268
    iget-object v0, p1, Lpaf;->c:Ljava/lang/String;

    iget-object v1, p1, Lpaf;->b:Ljava/lang/String;

    iget-object v2, p1, Lpaf;->d:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2, p3}, Ldrm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1270
    :cond_3
    return-void
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 1

    .prologue
    .line 1916
    .line 1917
    invoke-static {p0}, Lhqd;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1916
    invoke-static {p0, p1, v0}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;)Lidh;

    move-result-object v0

    .line 1918
    if-eqz v0, :cond_0

    .line 1919
    invoke-static {p0, p1, v0}, Ldrm;->c(Landroid/content/Context;ILidh;)Z

    move-result v0

    .line 1921
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;[B)Z
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 524
    sget-object v9, Ldrm;->d:Ljava/lang/Object;

    monitor-enter v9

    .line 526
    :try_start_0
    invoke-static {p0, p1, p2}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;)Lidh;

    move-result-object v0

    .line 530
    invoke-static {p0, p1, p2, p3}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;I)I

    move-result v6

    .line 532
    if-eqz v0, :cond_0

    const/high16 v0, -0x80000000

    if-ne v6, v0, :cond_0

    .line 533
    monitor-exit v9

    move v0, v8

    .line 538
    :goto_0
    return v0

    .line 535
    :cond_0
    new-instance v0, Ldna;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p4

    move v5, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Ldna;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II[B)V

    .line 537
    invoke-virtual {v0}, Ldna;->l()V

    .line 538
    invoke-virtual {v0}, Ldna;->t()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    monitor-exit v9

    goto :goto_0

    .line 540
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v8

    .line 538
    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 460
    sget-object v1, Ldrm;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 461
    :try_start_0
    new-instance v0, Ldkb;

    invoke-direct {v0, p0, p1, p2, p3}, Ldkb;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 462
    invoke-virtual {v0}, Ldkb;->l()V

    .line 464
    invoke-virtual {v0}, Ldkb;->t()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 465
    const-string v2, "EsEventData"

    invoke-virtual {v0, v2}, Ldkb;->d(Ljava/lang/String;)V

    .line 468
    :cond_0
    invoke-virtual {v0}, Ldkb;->t()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 469
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lidh;Logr;Ljava/lang/Long;Ljava/util/List;I)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lidh;",
            "Logr;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lpaf;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 1367
    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v10, p9

    invoke-static/range {v0 .. v10}, Ldrm;->a(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lidh;Logr;Ljava/lang/Long;Ljava/util/List;II)Z

    move-result v0

    return v0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lidh;Logr;Ljava/lang/Long;Ljava/util/List;II)Z
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lidh;",
            "Logr;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lpaf;",
            ">;II)Z"
        }
    .end annotation

    .prologue
    .line 1387
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 1389
    const/4 v15, 0x0

    .line 1390
    const/4 v14, 0x1

    .line 1391
    const/4 v13, 0x0

    .line 1392
    const/4 v12, 0x0

    .line 1393
    const-string v5, "events"

    const/4 v4, 0x3

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "fingerprint"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "source"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "can_comment"

    aput-object v7, v6, v4

    const-string v7, "event_id=?"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p3, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1398
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1399
    const/4 v4, 0x0

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 1400
    const/4 v4, 0x1

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 1401
    const/4 v6, 0x0

    .line 1402
    const/4 v4, 0x2

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    .line 1406
    :goto_0
    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    if-nez p9, :cond_2

    .line 1408
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    const/4 v5, 0x0

    .line 1518
    :cond_0
    :goto_1
    return v5

    .line 1402
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    move v10, v4

    move v11, v5

    move v12, v6

    .line 1408
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1415
    invoke-virtual/range {p5 .. p5}, Lidh;->f()[B

    move-result-object v13

    .line 1417
    invoke-static {v13}, Ljava/util/Arrays;->hashCode([B)I

    move-result v14

    .line 1419
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 1420
    const-string v4, "source"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1423
    const/4 v4, 0x1

    move/from16 v0, p9

    if-ne v0, v4, :cond_3

    .line 1424
    const-string v4, "deleted"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1427
    :cond_3
    if-eqz p6, :cond_4

    move-object/from16 v0, p6

    iget-object v4, v0, Logr;->C:Loae;

    if-eqz v4, :cond_4

    .line 1429
    :try_start_1
    const-string v4, "plus_one_data"

    move-object/from16 v0, p6

    iget-object v5, v0, Logr;->C:Loae;

    invoke-static {v5}, Llah;->a(Loae;)[B

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1432
    const/4 v4, 0x1

    move/from16 v0, p9

    if-ne v0, v4, :cond_4

    .line 1433
    move-object/from16 v0, p6

    iget-object v4, v0, Logr;->C:Loae;

    .line 1434
    iget-object v7, v4, Loae;->a:Ljava/lang/String;

    iget-object v5, v4, Loae;->e:Ljava/lang/Integer;

    .line 1435
    invoke-static {v5}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v8

    iget-object v4, v4, Loae;->c:Ljava/lang/Boolean;

    .line 1436
    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v9

    move-object/from16 v4, p0

    move/from16 v5, p10

    move-object/from16 v6, p4

    .line 1434
    invoke-static/range {v4 .. v9}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1443
    :cond_4
    :goto_3
    if-nez v12, :cond_5

    if-eq v11, v14, :cond_11

    .line 1444
    :cond_5
    const-string v4, "refresh_timestamp"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1445
    const-string v4, "name"

    invoke-virtual/range {p5 .. p5}, Lidh;->d()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446
    invoke-virtual/range {p5 .. p5}, Lidh;->e()Ljava/lang/String;

    move-result-object v5

    .line 1447
    invoke-virtual/range {p5 .. p5}, Lidh;->i()Lpbj;

    move-result-object v6

    .line 1448
    const-string v4, "creator_gaia_id"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1449
    const-string v4, "event_data"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1450
    const-string v4, "event_type"

    invoke-virtual/range {p5 .. p5}, Lidh;->g()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1451
    const-string v4, "mine"

    move-object/from16 v0, p5

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ldrm;->b(Lidh;Ljava/lang/String;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1452
    const-string v7, "can_invite_people"

    .line 1453
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    if-eqz v6, :cond_d

    iget-object v4, v6, Lpbj;->a:Lpbe;

    if-eqz v4, :cond_d

    iget-object v4, v6, Lpbj;->a:Lpbe;

    iget-object v4, v4, Lpbe;->a:Ljava/lang/Boolean;

    .line 1456
    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_d

    :cond_6
    const/4 v4, 0x1

    .line 1453
    :goto_4
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 1452
    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1458
    const-string v7, "can_post_photos"

    .line 1459
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    if-eqz v6, :cond_e

    iget-object v4, v6, Lpbj;->a:Lpbe;

    if-eqz v4, :cond_e

    iget-object v4, v6, Lpbj;->a:Lpbe;

    iget-object v4, v4, Lpbe;->b:Ljava/lang/Boolean;

    .line 1462
    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-eqz v4, :cond_e

    :cond_7
    const/4 v4, 0x1

    .line 1459
    :goto_5
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 1458
    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1464
    const-string v4, "can_comment"

    if-eqz p6, :cond_8

    move-object/from16 v0, p6

    iget-object v5, v0, Logr;->q:Ljava/lang/Boolean;

    .line 1465
    invoke-static {v5}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v10

    .line 1464
    :cond_8
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1468
    invoke-virtual/range {p5 .. p5}, Lidh;->l()Loyy;

    move-result-object v4

    if-eqz v4, :cond_f

    .line 1469
    invoke-virtual/range {p5 .. p5}, Lidh;->l()Loyy;

    move-result-object v4

    iget-object v4, v4, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1474
    :goto_6
    const-string v6, "start_time"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1476
    invoke-virtual/range {p5 .. p5}, Lidh;->n()J

    move-result-wide v4

    .line 1477
    const-string v6, "end_time"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1479
    const-string v4, "fingerprint"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1480
    if-nez v12, :cond_9

    if-eqz p4, :cond_a

    .line 1481
    :cond_9
    const-string v4, "activity_id"

    move-object/from16 v0, v18

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1484
    :cond_a
    if-eqz p7, :cond_b

    .line 1485
    const-string v4, "display_time"

    move-object/from16 v0, v18

    move-object/from16 v1, p7

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1488
    :cond_b
    if-eqz v12, :cond_10

    .line 1489
    const-string v4, "event_id"

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1491
    const-string v4, "events"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1496
    :goto_7
    const/4 v4, 0x1

    move v5, v4

    .line 1503
    :goto_8
    const-string v4, "EsEventData"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1504
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v16

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v8, 0x30

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "[INSERT_EVENT], duration: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "ms"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1505
    const/4 v4, 0x3

    const-string v6, "EsEventData"

    invoke-virtual/range {p5 .. p5}, Lidh;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1508
    :cond_c
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p8

    move-object/from16 v3, p2

    invoke-static {v0, v1, v2, v3}, Ldrm;->a(Landroid/content/Context;Lidh;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1510
    const/4 v4, 0x1

    move/from16 v0, p9

    if-ne v0, v4, :cond_0

    .line 1512
    const-class v4, Liai;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Liai;

    .line 1513
    invoke-virtual/range {p5 .. p5}, Lidh;->g()I

    move-result v6

    if-nez v6, :cond_13

    .line 1514
    invoke-virtual/range {p5 .. p5}, Lidh;->a()Lpbl;

    move-result-object v6

    .line 1515
    :goto_9
    move-object/from16 v0, p0

    move/from16 v1, p10

    invoke-interface {v4, v0, v1, v6}, Liai;->a(Landroid/content/Context;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 1412
    :catchall_0
    move-exception v4

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v4

    .line 1456
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 1462
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_f
    move-wide/from16 v4, v16

    .line 1471
    goto/16 :goto_6

    .line 1493
    :cond_10
    const-string v4, "events"

    const-string v5, "event_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_7

    .line 1497
    :cond_11
    if-eqz p4, :cond_12

    .line 1498
    const-string v4, "activity_id"

    move-object/from16 v0, v18

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1499
    const-string v4, "events"

    const-string v5, "event_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_12
    move v5, v15

    goto/16 :goto_8

    .line 1514
    :cond_13
    invoke-virtual/range {p5 .. p5}, Lidh;->b()Lozp;

    move-result-object v6

    goto :goto_9

    :catch_0
    move-exception v4

    goto/16 :goto_3

    :cond_14
    move v10, v12

    move v11, v13

    move v12, v14

    goto/16 :goto_2
.end method

.method public static a(Lidh;J)Z
    .locals 3

    .prologue
    .line 1886
    invoke-virtual {p0}, Lidh;->n()J

    move-result-wide v0

    .line 1887
    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lidh;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1863
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lidh;->g()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1875
    :cond_0
    :goto_0
    return v0

    .line 1867
    :cond_1
    invoke-virtual {p0}, Lidh;->i()Lpbj;

    move-result-object v1

    .line 1870
    invoke-virtual {p0}, Lidh;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, Lpbj;->a:Lpbe;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lpbj;->a:Lpbe;

    iget-object v2, v2, Lpbe;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v1, v1, Lpbj;->a:Lpbe;

    iget-object v1, v1, Lpbe;->b:Ljava/lang/Boolean;

    .line 1873
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lidh;Ljava/lang/String;J)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1848
    invoke-static {p0, p1}, Ldrm;->a(Lidh;Ljava/lang/String;)Z

    move-result v3

    .line 1849
    invoke-virtual {p0}, Lidh;->i()Lpbj;

    move-result-object v0

    iget-object v0, v0, Lpbj;->h:Lpax;

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    .line 1850
    invoke-static {p0}, Ldrm;->a(Lidh;)I

    move-result v4

    if-eq v0, v4, :cond_0

    move v0, v1

    .line 1851
    :goto_0
    invoke-virtual {p0}, Lidh;->l()Loyy;

    move-result-object v4

    iget-object v4, v4, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0xa4cb80

    sub-long/2addr v4, v6

    .line 1852
    invoke-virtual {p0}, Lidh;->n()J

    move-result-wide v6

    const-wide/16 v8, 0x1388

    sub-long/2addr v6, v8

    .line 1854
    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    cmp-long v0, p2, v4

    if-lez v0, :cond_1

    cmp-long v0, p2, v6

    if-gez v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 1850
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1854
    goto :goto_1
.end method

.method public static a(Lozp;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1792
    if-eqz p0, :cond_0

    iget-object v0, p0, Lozp;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1798
    :goto_0
    return v0

    .line 1795
    :cond_1
    iget-object v0, p0, Lozp;->n:Loya;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lozp;->n:Loya;

    sget-object v2, Loyy;->a:Loxr;

    .line 1796
    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1798
    :goto_1
    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move-wide v2, v4

    .line 1796
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1798
    goto :goto_0
.end method

.method public static a()[I
    .locals 1

    .prologue
    .line 3234
    sget-object v0, Ldrm;->c:[I

    return-object v0
.end method

.method public static b(Landroid/content/Context;ILidh;)J
    .locals 3

    .prologue
    .line 991
    if-nez p2, :cond_0

    .line 992
    const-wide/16 v0, 0x0

    .line 1006
    :goto_0
    return-wide v0

    .line 994
    :cond_0
    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    invoke-virtual {p2}, Lidh;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 995
    invoke-virtual {p2}, Lidh;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ldrm;->g:[Ljava/lang/String;

    invoke-static {p0, p1, v0, v1}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 998
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 999
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1002
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1006
    :cond_2
    invoke-virtual {p2}, Lidh;->n()J

    move-result-wide v0

    goto :goto_0

    .line 1002
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static b(Lidh;Ljava/lang/String;J)J
    .locals 4

    .prologue
    .line 1896
    invoke-static {p0, p1, p2, p3}, Ldrm;->a(Lidh;Ljava/lang/String;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1897
    const-wide/16 v0, 0x0

    .line 1904
    :goto_0
    return-wide v0

    .line 1899
    :cond_0
    invoke-virtual {p0}, Lidh;->n()J

    move-result-wide v0

    .line 1901
    invoke-static {p0, p1}, Ldrm;->a(Lidh;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    .line 1902
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1904
    :cond_2
    invoke-virtual {p0}, Lidh;->l()Loyy;

    move-result-object v0

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb80

    sub-long/2addr v0, v2

    sub-long/2addr v0, p2

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;II[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2623
    .line 2624
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2627
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 2628
    const-string v3, "is_default!=0"

    move-object v4, v5

    .line 2635
    :goto_0
    const-string v1, "event_themes"

    const-string v7, "theme_id"

    move-object v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2638
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 2645
    :goto_1
    return-object v0

    .line 2631
    :cond_0
    const-string v3, "theme_id=?"

    .line 2632
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    goto :goto_0

    .line 2642
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2644
    invoke-static {p0, p1, v5, v5}, Ldrm;->c(Landroid/content/Context;ILkfp;Lles;)V

    .line 2645
    const-string v1, "event_themes"

    const-string v7, "theme_id"

    move-object v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 846
    .line 854
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 856
    const-wide/16 v2, 0x0

    .line 858
    :try_start_0
    const-string v1, "SELECT display_time FROM events WHERE event_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v4, v6

    invoke-static {v0, v1, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    move-wide v6, v2

    .line 864
    :goto_0
    const-string v1, "event_activities LEFT OUTER JOIN contacts ON (event_activities.owner_gaia_id = contacts.gaia_id)"

    const-string v3, "event_id = ? AND timestamp >= ?"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    aput-object p2, v4, v8

    .line 865
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v9

    const-string v7, "timestamp DESC"

    move-object v2, p3

    move-object v6, v5

    .line 864
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v1

    move-wide v6, v2

    goto :goto_0
.end method

.method public static b(Llto;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2586
    invoke-static {p0}, Ldrm;->a(Llto;)Lltp;

    move-result-object v0

    .line 2587
    if-eqz v0, :cond_0

    .line 2588
    iget-object v0, v0, Lltp;->d:Ljava/lang/String;

    .line 2590
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2338
    const-string v1, "events"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "event_id"

    aput-object v0, v2, v3

    const-string v3, "mine = 1"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2341
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2343
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2344
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2347
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2349
    return-object v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Z)Llah;
    .locals 7

    .prologue
    .line 3163
    const-string v0, "EsEventData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> plusOneEvent activity id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 3167
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3168
    invoke-static {v1, p2}, Ldrm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Llah;

    move-result-object v6

    .line 3170
    if-nez v6, :cond_1

    .line 3171
    const/4 v0, 0x0

    .line 3182
    :goto_0
    return-object v0

    .line 3174
    :cond_1
    invoke-virtual {v6, p3}, Llah;->a(Z)V

    .line 3176
    invoke-virtual {v6}, Llah;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Llah;->b()I

    move-result v4

    .line 3177
    invoke-virtual {v6}, Llah;->c()Z

    move-result v5

    move-object v0, p0

    move-object v2, p2

    .line 3176
    invoke-static/range {v0 .. v5}, Ldrm;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 3179
    invoke-virtual {v6}, Llah;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Llah;->b()I

    move-result v4

    .line 3180
    invoke-virtual {v6}, Llah;->c()Z

    move-result v5

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    .line 3179
    invoke-static/range {v0 .. v5}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)V

    move-object v0, v6

    .line 3182
    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 394
    invoke-static {p0}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v0

    new-instance v1, Ldrn;

    invoke-direct {v1}, Ldrn;-><init>()V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "action_instant_share_end_time_changed"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ldr;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 395
    return-void
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 642
    new-instance v0, Ldrp;

    invoke-direct {v0, p0, p1, p2}, Ldrp;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 648
    return-void
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 3187
    const-string v0, "EsEventData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x20

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, ">>>>> update event plusone id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3191
    :cond_0
    invoke-static {p0, p1}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3192
    invoke-static {v1, p2}, Ldrm;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Llah;

    move-result-object v6

    .line 3194
    if-nez v6, :cond_2

    .line 3208
    :cond_1
    :goto_0
    return-void

    .line 3198
    :cond_2
    invoke-virtual {v6}, Llah;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3202
    invoke-virtual {v6, p3}, Llah;->a(Ljava/lang/String;)V

    .line 3203
    invoke-virtual {v6}, Llah;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Llah;->b()I

    move-result v4

    .line 3204
    invoke-virtual {v6}, Llah;->c()Z

    move-result v5

    move-object v0, p0

    move-object v2, p2

    .line 3203
    invoke-static/range {v0 .. v5}, Ldrm;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 3206
    invoke-virtual {v6}, Llah;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Llah;->b()I

    move-result v4

    .line 3207
    invoke-virtual {v6}, Llah;->c()Z

    move-result v5

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    .line 3206
    invoke-static/range {v0 .. v5}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;ILkfp;Lles;)V
    .locals 15

    .prologue
    .line 3015
    sget-object v13, Ldrm;->e:Ljava/lang/Object;

    monitor-enter v13

    .line 3016
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3017
    monitor-exit v13

    .line 3067
    :goto_0
    return-void

    .line 3020
    :cond_0
    const-string v2, "Current events"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lkfp;->c(Ljava/lang/String;)V

    .line 3022
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 3023
    const-wide/32 v4, 0x112a880

    sub-long v8, v2, v4

    .line 3024
    const-wide/32 v4, 0xa4cb80

    add-long v10, v2, v4

    .line 3026
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 3028
    const-string v3, "events"

    sget-object v4, Ldrm;->f:[Ljava/lang/String;

    const-string v5, "end_time > ? AND start_time < ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 3033
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 3034
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 3028
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 3038
    const/4 v12, 0x0

    .line 3040
    :goto_1
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3041
    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v2

    if-nez v2, :cond_2

    .line 3042
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 3046
    const/4 v2, 0x1

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 3047
    const/4 v2, 0x2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 3048
    const/4 v2, 0x3

    const/4 v3, 0x4

    invoke-static {v14, v2, v3}, Ldrm;->a(Landroid/database/Cursor;II)Lidh;

    move-result-object v2

    .line 3052
    invoke-virtual {v2}, Lidh;->h()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v2, p0

    move/from16 v3, p1

    move-object/from16 v11, p2

    .line 3051
    invoke-static/range {v2 .. v11}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLkfp;)Lkff;

    move-result-object v2

    .line 3055
    if-eqz p3, :cond_1

    .line 3056
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lles;->a(Lkff;)V

    .line 3058
    :cond_1
    invoke-virtual {v2}, Lkff;->t()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_3

    .line 3059
    add-int/lit8 v2, v12, 0x1

    :goto_2
    move v12, v2

    .line 3061
    goto :goto_1

    .line 3063
    :cond_2
    :try_start_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 3066
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lkfp;->m(I)V

    .line 3067
    monitor-exit v13

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 3063
    :catchall_1
    move-exception v2

    :try_start_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    move v2, v12

    goto :goto_2
.end method

.method static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 1578
    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v8

    .line 1583
    const-string v1, "events"

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "resume_token"

    aput-object v0, v2, v8

    const-string v3, "event_id=?"

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1587
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1588
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    move v1, v8

    .line 1592
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1595
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1596
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1597
    const-string v2, "resume_token"

    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1599
    if-eqz v1, :cond_1

    .line 1600
    const-string v1, "events"

    invoke-virtual {p0, v1, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1605
    :cond_0
    :goto_1
    return-void

    .line 1592
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1602
    :cond_1
    const-string v1, "events"

    const-string v2, "event_id=?"

    invoke-virtual {p0, v1, v0, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    move-object v0, v5

    move v1, v9

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lidh;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2906
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    :cond_0
    move v1, v2

    .line 2926
    :cond_1
    :goto_0
    return v1

    .line 2910
    :cond_2
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 2911
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "gaia_id"

    .line 2912
    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2913
    invoke-virtual {p1}, Lidh;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2918
    invoke-virtual {p1}, Lidh;->i()Lpbj;

    move-result-object v4

    .line 2919
    iget-object v0, v4, Lpbj;->h:Lpax;

    if-eqz v0, :cond_5

    iget-object v0, v4, Lpbj;->h:Lpax;

    iget-object v0, v0, Lpax;->e:Lpaf;

    if-eqz v0, :cond_5

    move v0, v1

    .line 2921
    :goto_1
    iget-object v3, v4, Lpbj;->a:Lpbe;

    .line 2922
    if-eqz v3, :cond_3

    iget-object v5, v3, Lpbe;->a:Ljava/lang/Boolean;

    if-eqz v5, :cond_6

    iget-object v3, v3, Lpbe;->a:Ljava/lang/Boolean;

    .line 2923
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_3
    move v3, v1

    .line 2925
    :goto_2
    if-eqz v0, :cond_4

    if-nez v3, :cond_1

    :cond_4
    iget-object v0, v4, Lpbj;->b:Lpbd;

    iget-object v0, v0, Lpbd;->b:Lpbc;

    if-eqz v0, :cond_1

    iget-object v0, v4, Lpbj;->b:Lpbd;

    iget-object v0, v0, Lpbd;->b:Lpbc;

    iget-object v0, v0, Lpbc;->a:Ljava/lang/Boolean;

    .line 2926
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    goto :goto_0

    :cond_5
    move v0, v2

    .line 2919
    goto :goto_1

    :cond_6
    move v3, v2

    .line 2923
    goto :goto_2
.end method

.method public static b(Lidh;)Z
    .locals 2

    .prologue
    .line 3074
    const/4 v0, 0x3

    invoke-static {p0}, Ldrm;->a(Lidh;)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lidh;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2864
    invoke-virtual {p0}, Lidh;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    .line 2886
    :cond_0
    :goto_0
    return v1

    .line 2874
    :cond_1
    invoke-virtual {p0}, Lidh;->i()Lpbj;

    move-result-object v3

    .line 2875
    iget-object v0, v3, Lpbj;->h:Lpax;

    if-eqz v0, :cond_2

    iget-object v0, v3, Lpbj;->h:Lpax;

    iget v0, v0, Lpax;->c:I

    const/4 v4, -0x1

    if-eq v0, v4, :cond_2

    move v1, v2

    .line 2877
    goto :goto_0

    .line 2878
    :cond_2
    iget-object v0, v3, Lpbj;->c:[Lpay;

    if-eqz v0, :cond_0

    move v0, v1

    .line 2879
    :goto_1
    iget-object v4, v3, Lpbj;->c:[Lpay;

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 2880
    iget-object v4, v3, Lpbj;->c:[Lpay;

    aget-object v4, v4, v0

    iget-object v4, v4, Lpay;->e:Ljava/lang/Boolean;

    if-eqz v4, :cond_3

    iget-object v4, v3, Lpbj;->c:[Lpay;

    aget-object v4, v4, v0

    iget-object v4, v4, Lpay;->e:Ljava/lang/Boolean;

    .line 2881
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    move v1, v2

    .line 2882
    goto :goto_0

    .line 2879
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static b(Lozp;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1802
    invoke-static {p0}, Ldrm;->a(Lozp;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1808
    :goto_0
    return v0

    .line 1805
    :cond_0
    iget-object v0, p0, Lozp;->o:Loya;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lozp;->o:Loya;

    sget-object v2, Loyy;->a:Loxr;

    .line 1806
    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1808
    :goto_1
    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move-wide v2, v4

    .line 1806
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1808
    goto :goto_0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;)J
    .locals 6

    .prologue
    .line 804
    .line 808
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 810
    const-wide/16 v0, 0x0

    .line 812
    :try_start_0
    const-string v3, "SELECT COUNT(*) FROM event_activities WHERE event_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-static {v2, v3, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 818
    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 3113
    .line 3116
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3118
    const-string v1, "event_people_view"

    const-string v3, "event_id = ?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v4, v2

    move-object v2, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/content/Context;)Ljava/io/File;
    .locals 5

    .prologue
    .line 2698
    sget-object v0, Ldrm;->j:Ljava/io/File;

    if-nez v0, :cond_0

    .line 2699
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "event_themes"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v0, Ldrm;->j:Ljava/io/File;

    .line 2702
    :cond_0
    sget-object v0, Ldrm;->j:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2704
    :try_start_0
    sget-object v0, Ldrm;->j:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2712
    :cond_1
    :goto_0
    sget-object v0, Ldrm;->j:Ljava/io/File;

    return-object v0

    .line 2705
    :catch_0
    move-exception v0

    .line 2706
    const-string v1, "EsEventData"

    sget-object v2, Ldrm;->j:Ljava/io/File;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x31

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot create event theme placeholder directory: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2708
    const/4 v0, 0x0

    sput-object v0, Ldrm;->j:Ljava/io/File;

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;ILkfp;Lles;)V
    .locals 6

    .prologue
    .line 2658
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lkfp;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2662
    :cond_0
    sget-object v2, Ldrm;->l:Ljava/lang/Object;

    monitor-enter v2

    .line 2666
    :try_start_0
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 2667
    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2668
    const-string v1, "SELECT event_themes_sync_time  FROM account_status"

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2675
    :goto_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v0, v4, v0

    .line 2676
    const-wide/32 v4, 0x5265c00

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    .line 2677
    new-instance v0, Lkfo;

    invoke-direct {v0, p0, p1, p2}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    .line 2678
    new-instance v1, Ldkd;

    invoke-direct {v1, p0, v0, p1}, Ldkd;-><init>(Landroid/content/Context;Lkfo;I)V

    .line 2680
    invoke-virtual {v1}, Ldkd;->l()V

    .line 2681
    if-eqz p3, :cond_1

    .line 2682
    invoke-virtual {p3, v1}, Lles;->a(Lkff;)V

    .line 2684
    :cond_1
    invoke-virtual {v1}, Ldkd;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2687
    const-string v0, "EsEventData"

    invoke-virtual {v1, v0}, Ldkd;->d(Ljava/lang/String;)V

    .line 2690
    :cond_2
    monitor-exit v2

    .line 2692
    :cond_3
    return-void

    .line 2672
    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2690
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 3301
    const-string v1, "location_queries"

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v8

    const-string v7, "_id DESC"

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3305
    if-nez v1, :cond_0

    .line 3339
    :goto_0
    return-void

    .line 3309
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gt v0, v9, :cond_1

    .line 3310
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 3316
    :cond_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 3319
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 3320
    const-string v0, "_id IN("

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v9

    .line 3323
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3324
    if-eqz v0, :cond_2

    move v0, v8

    .line 3330
    :goto_2
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 3338
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 3327
    :cond_2
    const/16 v3, 0x2c

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 3333
    :cond_3
    const/16 v0, 0x29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3336
    const-string v0, "location_queries"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3338
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;ILidh;)Z
    .locals 16

    .prologue
    .line 1935
    const/4 v4, 0x0

    .line 1937
    const-string v2, "EsEventData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1938
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x30

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "#validateInstantShare; now: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1942
    :cond_0
    invoke-static/range {p0 .. p2}, Ldrm;->b(Landroid/content/Context;ILidh;)J

    move-result-wide v6

    .line 1943
    const-class v2, Lhsc;

    .line 1944
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhsc;

    invoke-virtual {v2}, Lhsc;->c()Z

    move-result v5

    .line 1948
    const/4 v2, -0x1

    move/from16 v0, p1

    if-eq v0, v2, :cond_4

    if-eqz p2, :cond_4

    if-nez v5, :cond_4

    .line 1950
    :try_start_0
    const-class v2, Lhei;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhei;

    .line 1951
    move/from16 v0, p1

    invoke-interface {v2, v0}, Lhei;->a(I)Lhej;

    move-result-object v2

    .line 1952
    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1953
    const-string v8, "is_plus_page"

    invoke-interface {v2, v8}, Lhej;->c(Ljava/lang/String;)Z

    move-result v8

    .line 1954
    invoke-virtual/range {p2 .. p2}, Lidh;->c()Ljava/lang/String;

    move-result-object v9

    .line 1956
    const-string v2, "alarm"

    .line 1957
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 1958
    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v10}, Leyq;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 1960
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1962
    const-string v12, "EsEventData"

    const/4 v13, 0x4

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1963
    const-string v12, "#validateInstantShare; cur event: "

    invoke-virtual/range {p2 .. p2}, Lidh;->c()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v14

    if-eqz v14, :cond_7

    invoke-virtual {v12, v13}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1966
    :cond_1
    :goto_0
    if-nez v8, :cond_4

    .line 1967
    move-object/from16 v0, p2

    invoke-static {v0, v3, v10, v11}, Ldrm;->a(Lidh;Ljava/lang/String;J)Z

    move-result v3

    if-eqz v3, :cond_4

    const-wide/16 v12, 0x1388

    sub-long v12, v6, v12

    cmp-long v3, v10, v12

    if-gez v3, :cond_4

    .line 1971
    invoke-virtual/range {p2 .. p2}, Lidh;->e()Ljava/lang/String;

    move-result-object v3

    .line 1969
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v9, v3}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v8

    .line 1972
    const-class v3, Lhrt;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhrt;

    .line 1973
    invoke-virtual/range {p2 .. p2}, Lidh;->d()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v3, v0, v12, v8, v13}, Lhrt;->a(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;Z)V

    .line 1977
    move-object/from16 v0, p0

    invoke-static {v0, v9}, Leyq;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    .line 1978
    const/4 v8, 0x0

    invoke-virtual {v2, v8, v6, v7, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1979
    const/4 v3, 0x1

    .line 1981
    :try_start_1
    const-string v2, "EsEventData"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1982
    sub-long v8, v6, v10

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x73

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "#validateInstantShare; keep IS; now: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", end: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", wake in: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1986
    :cond_2
    const-string v2, "EsEventData"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1987
    const-string v2, "MMM dd, yyyy h:mmaa"

    .line 1988
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 1989
    invoke-static {v2, v4}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 1991
    invoke-static {v2, v8}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x24

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Enable Instant Share; now: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ", alarm: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    move v4, v3

    .line 1988
    :cond_4
    if-nez v4, :cond_6

    .line 1999
    const/4 v2, -0x1

    move/from16 v0, p1

    if-eq v0, v2, :cond_5

    if-eqz p2, :cond_5

    if-nez v5, :cond_5

    .line 2001
    invoke-virtual/range {p2 .. p2}, Lidh;->n()J

    move-result-wide v2

    cmp-long v2, v6, v2

    if-gez v2, :cond_5

    .line 2004
    invoke-virtual/range {p2 .. p2}, Lidh;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lidh;->c()Ljava/lang/String;

    move-result-object v3

    .line 2005
    invoke-virtual/range {p2 .. p2}, Lidh;->e()Ljava/lang/String;

    move-result-object v5

    .line 2003
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v2, v3, v5}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2009
    :cond_5
    const-class v2, Lhrt;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhrt;

    .line 2010
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v2, v0, v1}, Lhrt;->a(Landroid/content/Context;I)V

    .line 2011
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lhrt;->a(Landroid/content/Context;)V

    .line 2018
    :cond_6
    return v4

    .line 1963
    :cond_7
    :try_start_2
    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v12}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1996
    :catchall_0
    move-exception v2

    move-object v3, v2

    move v2, v4

    :goto_1
    if-nez v2, :cond_9

    .line 1999
    const/4 v2, -0x1

    move/from16 v0, p1

    if-eq v0, v2, :cond_8

    if-eqz p2, :cond_8

    if-nez v5, :cond_8

    .line 2001
    invoke-virtual/range {p2 .. p2}, Lidh;->n()J

    move-result-wide v4

    cmp-long v2, v6, v4

    if-gez v2, :cond_8

    .line 2004
    invoke-virtual/range {p2 .. p2}, Lidh;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lidh;->c()Ljava/lang/String;

    move-result-object v4

    .line 2005
    invoke-virtual/range {p2 .. p2}, Lidh;->e()Ljava/lang/String;

    move-result-object v5

    .line 2003
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {v0, v1, v2, v4, v5}, Ldrm;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2009
    :cond_8
    const-class v2, Lhrt;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhrt;

    .line 2010
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v2, v0, v1}, Lhrt;->a(Landroid/content/Context;I)V

    .line 2011
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lhrt;->a(Landroid/content/Context;)V

    .line 2013
    :cond_9
    throw v3

    .line 1996
    :catchall_1
    move-exception v2

    move-object v15, v2

    move v2, v3

    move-object v3, v15

    goto :goto_1
.end method

.method public static c(Lidh;)Z
    .locals 2

    .prologue
    .line 3084
    invoke-virtual {p0}, Lidh;->i()Lpbj;

    move-result-object v0

    iget-object v0, v0, Lpbj;->a:Lpbe;

    .line 3085
    if-eqz v0, :cond_0

    iget-object v1, v0, Lpbe;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lpbe;->d:Ljava/lang/Boolean;

    .line 3087
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lozp;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1815
    if-eqz p0, :cond_0

    iget-object v0, p0, Lozp;->q:[Lozl;

    if-nez v0, :cond_1

    .line 1824
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 1818
    :goto_1
    iget-object v3, p0, Lozp;->q:[Lozl;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 1819
    iget-object v3, p0, Lozp;->q:[Lozl;

    aget-object v3, v3, v0

    iget v3, v3, Lozl;->b:I

    if-ne v3, v2, :cond_2

    move v1, v2

    .line 1821
    goto :goto_0

    .line 1818
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static d(Landroid/content/Context;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 826
    .line 829
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 830
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 831
    const-string v2, "resume_token"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 832
    const-string v2, "polling_token"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 833
    const-string v2, "events"

    const-string v3, "event_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 834
    return-void
.end method

.method public static d(Lidh;)Z
    .locals 2

    .prologue
    .line 3094
    if-eqz p0, :cond_0

    .line 3095
    invoke-virtual {p0}, Lidh;->i()Lpbj;

    move-result-object v0

    .line 3096
    :goto_0
    if-eqz p0, :cond_1

    iget-object v1, v0, Lpbj;->h:Lpax;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lpbj;->h:Lpax;

    iget-object v1, v1, Lpax;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lpbj;->h:Lpax;

    iget-object v0, v0, Lpax;->g:Ljava/lang/Boolean;

    .line 3099
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 3095
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3099
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static d(Lozp;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 3264
    if-eqz p0, :cond_0

    iget-object v0, p0, Lozp;->n:Loya;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 3271
    :goto_0
    return v0

    .line 3267
    :cond_1
    iget-object v0, p0, Lozp;->n:Loya;

    sget-object v2, Loyy;->a:Loxr;

    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 3268
    iget-object v0, p0, Lozp;->o:Loya;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lozp;->o:Loya;

    sget-object v2, Loyy;->a:Loxr;

    .line 3269
    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 3271
    :goto_1
    cmp-long v0, v6, v4

    if-lez v0, :cond_3

    cmp-long v0, v2, v4

    if-gtz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move-wide v2, v4

    .line 3269
    goto :goto_1

    :cond_3
    move v0, v1

    .line 3271
    goto :goto_0
.end method

.method public static e(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 2383
    .line 2384
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2385
    const/4 v1, 0x1

    invoke-static {v0, p0, p1, p2, v1}, Ldrm;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;ILjava/lang/String;Z)V

    .line 2386
    return-void
.end method

.method public static e(Lidh;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3248
    if-nez p0, :cond_1

    .line 3256
    :cond_0
    :goto_0
    return v0

    .line 3251
    :cond_1
    invoke-virtual {p0}, Lidh;->b()Lozp;

    move-result-object v1

    .line 3252
    if-eqz v1, :cond_0

    .line 3256
    iget-object v1, v1, Lozp;->m:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
