.class public final Lmel;
.super Loxq;
.source "PG"


# instance fields
.field public a:Loka;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11014
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11017
    iput-object v0, p0, Lmel;->apiHeader:Llyr;

    .line 11020
    iput-object v0, p0, Lmel;->a:Loka;

    .line 11014
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11037
    const/4 v0, 0x0

    .line 11038
    iget-object v1, p0, Lmel;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 11039
    const/4 v0, 0x1

    iget-object v1, p0, Lmel;->apiHeader:Llyr;

    .line 11040
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11042
    :cond_0
    iget-object v1, p0, Lmel;->a:Loka;

    if-eqz v1, :cond_1

    .line 11043
    const/4 v1, 0x2

    iget-object v2, p0, Lmel;->a:Loka;

    .line 11044
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11046
    :cond_1
    iget-object v1, p0, Lmel;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11047
    iput v0, p0, Lmel;->ai:I

    .line 11048
    return v0
.end method

.method public a(Loxn;)Lmel;
    .locals 2

    .prologue
    .line 11056
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11057
    sparse-switch v0, :sswitch_data_0

    .line 11061
    iget-object v1, p0, Lmel;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11062
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmel;->ah:Ljava/util/List;

    .line 11065
    :cond_1
    iget-object v1, p0, Lmel;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11067
    :sswitch_0
    return-object p0

    .line 11072
    :sswitch_1
    iget-object v0, p0, Lmel;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 11073
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmel;->apiHeader:Llyr;

    .line 11075
    :cond_2
    iget-object v0, p0, Lmel;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11079
    :sswitch_2
    iget-object v0, p0, Lmel;->a:Loka;

    if-nez v0, :cond_3

    .line 11080
    new-instance v0, Loka;

    invoke-direct {v0}, Loka;-><init>()V

    iput-object v0, p0, Lmel;->a:Loka;

    .line 11082
    :cond_3
    iget-object v0, p0, Lmel;->a:Loka;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11057
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11025
    iget-object v0, p0, Lmel;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 11026
    const/4 v0, 0x1

    iget-object v1, p0, Lmel;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11028
    :cond_0
    iget-object v0, p0, Lmel;->a:Loka;

    if-eqz v0, :cond_1

    .line 11029
    const/4 v0, 0x2

    iget-object v1, p0, Lmel;->a:Loka;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11031
    :cond_1
    iget-object v0, p0, Lmel;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11033
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11010
    invoke-virtual {p0, p1}, Lmel;->a(Loxn;)Lmel;

    move-result-object v0

    return-object v0
.end method
