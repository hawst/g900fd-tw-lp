.class public final Lote;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Loxq;-><init>()V

    .line 185
    const/high16 v0, -0x80000000

    iput v0, p0, Lote;->b:I

    .line 175
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 207
    const/4 v0, 0x0

    .line 208
    iget-object v1, p0, Lote;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 209
    const/4 v0, 0x1

    iget-object v1, p0, Lote;->a:Ljava/lang/String;

    .line 210
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 212
    :cond_0
    iget v1, p0, Lote;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 213
    const/4 v1, 0x2

    iget v2, p0, Lote;->b:I

    .line 214
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_1
    iget-object v1, p0, Lote;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 217
    const/4 v1, 0x3

    iget-object v2, p0, Lote;->c:Ljava/lang/String;

    .line 218
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_2
    iget-object v1, p0, Lote;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    iput v0, p0, Lote;->ai:I

    .line 222
    return v0
.end method

.method public a(Loxn;)Lote;
    .locals 2

    .prologue
    .line 230
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 231
    sparse-switch v0, :sswitch_data_0

    .line 235
    iget-object v1, p0, Lote;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 236
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lote;->ah:Ljava/util/List;

    .line 239
    :cond_1
    iget-object v1, p0, Lote;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    :sswitch_0
    return-object p0

    .line 246
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lote;->a:Ljava/lang/String;

    goto :goto_0

    .line 250
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 251
    if-eqz v0, :cond_2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 253
    :cond_2
    iput v0, p0, Lote;->b:I

    goto :goto_0

    .line 255
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lote;->b:I

    goto :goto_0

    .line 260
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lote;->c:Ljava/lang/String;

    goto :goto_0

    .line 231
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lote;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x1

    iget-object v1, p0, Lote;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 195
    :cond_0
    iget v0, p0, Lote;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 196
    const/4 v0, 0x2

    iget v1, p0, Lote;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 198
    :cond_1
    iget-object v0, p0, Lote;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 199
    const/4 v0, 0x3

    iget-object v1, p0, Lote;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 201
    :cond_2
    iget-object v0, p0, Lote;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 203
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lote;->a(Loxn;)Lote;

    move-result-object v0

    return-object v0
.end method
