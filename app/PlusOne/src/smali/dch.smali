.class final Ldch;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldcg;


# instance fields
.field private synthetic a:Ldbs;


# direct methods
.method constructor <init>(Ldbs;)V
    .locals 0

    .prologue
    .line 1142
    iput-object p1, p0, Ldch;->a:Ldbs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 1145
    iget-object v0, p0, Ldch;->a:Ldbs;

    invoke-static {v0}, Ldbs;->d(Ldbs;)Ldcm;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1146
    iget-object v0, p0, Ldch;->a:Ldbs;

    invoke-static {v0}, Ldbs;->d(Ldbs;)Ldcm;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ldcm;->b(Z)V

    .line 1148
    :cond_0
    return-void
.end method

.method public a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1160
    iget-object v0, p0, Ldch;->a:Ldbs;

    .line 1161
    invoke-virtual {v0}, Ldbs;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1164
    iget-object v1, p0, Ldch;->a:Ldbs;

    invoke-static {v1}, Ldbs;->m(Ldbs;)Ldeo;

    move-result-object v1

    invoke-virtual {v1}, Ldeo;->a()Lddl;

    move-result-object v1

    invoke-interface {v1}, Lddl;->J()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p3, :cond_2

    iget-object v1, p0, Ldch;->a:Ldbs;

    .line 1166
    invoke-static {v1}, Ldbs;->m(Ldbs;)Ldeo;

    move-result-object v1

    invoke-virtual {v1}, Ldeo;->a()Lddl;

    move-result-object v1

    invoke-interface {v1}, Lddl;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Ldch;->a:Ldbs;

    .line 1167
    invoke-static {v1}, Ldbs;->n(Ldbs;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1168
    iget-object v0, p0, Ldch;->a:Ldbs;

    const v1, 0x7f0a09f9

    .line 1169
    invoke-virtual {v0, v1}, Ldbs;->e_(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Ldch;->a:Ldbs;

    .line 1170
    invoke-static {v0}, Ldbs;->m(Ldbs;)Ldeo;

    move-result-object v0

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ldch;->a:Ldbs;

    const v2, 0x7f0a09fe

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p4, v3, v5

    iget-object v4, p0, Ldch;->a:Ldbs;

    .line 1172
    invoke-static {v4}, Ldbs;->m(Ldbs;)Ldeo;

    move-result-object v4

    invoke-virtual {v4}, Ldeo;->a()Lddl;

    move-result-object v4

    invoke-interface {v4}, Lddl;->m()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 1171
    invoke-virtual {v0, v2, v3}, Ldbs;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1173
    :goto_0
    iget-object v2, p0, Ldch;->a:Ldbs;

    const v3, 0x7f0a0596

    .line 1175
    invoke-virtual {v2, v3}, Ldbs;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldch;->a:Ldbs;

    const v4, 0x7f0a0597

    invoke-virtual {v3, v4}, Ldbs;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 1168
    invoke-static {v1, v0, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 1176
    iget-object v1, p0, Ldch;->a:Ldbs;

    invoke-virtual {v0, v1, v5}, Llgr;->a(Lu;I)V

    .line 1177
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "shape_id"

    invoke-virtual {v1, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1178
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "taggee_name"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1179
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "taggee_gaia_id"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    iget-object v1, p0, Ldch;->a:Ldbs;

    invoke-virtual {v1}, Ldbs;->p()Lae;

    move-result-object v1

    const-string v2, "pouf_accept_shape_and_share"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 1202
    :cond_0
    :goto_1
    return-void

    .line 1171
    :cond_1
    iget-object v0, p0, Ldch;->a:Ldbs;

    const v2, 0x7f0a09ff

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p4, v3, v5

    .line 1173
    invoke-virtual {v0, v2, v3}, Ldbs;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1184
    :cond_2
    const-string v1, "shape.show_create_confirm"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p3, :cond_3

    iget-object v0, p0, Ldch;->a:Ldbs;

    .line 1186
    invoke-static {v0}, Ldbs;->m(Ldbs;)Ldeo;

    move-result-object v0

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Ldch;->a:Ldbs;

    .line 1187
    invoke-static {v0}, Ldbs;->n(Ldbs;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1188
    :cond_3
    iget-object v0, p0, Ldch;->a:Ldbs;

    invoke-static {v0}, Ldbs;->o(Ldbs;)Ldcf;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ldcf;->a(JLjava/lang/String;)V

    goto :goto_1

    .line 1191
    :cond_4
    iget-object v0, p0, Ldch;->a:Ldbs;

    invoke-virtual {v0}, Ldbs;->p()Lae;

    move-result-object v0

    .line 1192
    const-string v1, "pouf_create_shape"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1194
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Leqb;->c:Leqb;

    const/4 v2, 0x0

    iget-object v3, p0, Ldch;->a:Ldbs;

    .line 1195
    invoke-static {v3}, Ldbs;->o(Ldbs;)Ldcf;

    move-result-object v3

    .line 1193
    invoke-static {v0, v1, v2, v3}, Leqa;->a(Ljava/lang/Long;Leqb;Landroid/graphics/RectF;Leqc;)Leqa;

    move-result-object v0

    .line 1196
    invoke-virtual {v0}, Lt;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 1197
    const-string v2, "taggee_gaia_id"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1198
    const-string v2, "taggee_name"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1199
    iget-object v1, p0, Ldch;->a:Ldbs;

    invoke-virtual {v1}, Ldbs;->p()Lae;

    move-result-object v1

    const-string v2, "pouf_create_shape"

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 1200
    iget-object v1, p0, Ldch;->a:Ldbs;

    invoke-virtual {v0, v1, v5}, Lt;->a(Lu;I)V

    goto :goto_1
.end method

.method public a(JZ)V
    .locals 15

    .prologue
    .line 1206
    iget-object v0, p0, Ldch;->a:Ldbs;

    invoke-virtual {v0}, Ldbs;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 1207
    const-string v1, "view_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1208
    iget-object v0, p0, Ldch;->a:Ldbs;

    iget-object v1, p0, Ldch;->a:Ldbs;

    invoke-virtual {v1}, Ldbs;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Ldch;->a:Ldbs;

    invoke-static {v2}, Ldbs;->p(Ldbs;)I

    move-result v2

    iget-object v3, p0, Ldch;->a:Ldbs;

    .line 1209
    invoke-static {v3}, Ldbs;->m(Ldbs;)Ldeo;

    move-result-object v3

    invoke-virtual {v3}, Ldeo;->a()Lddl;

    move-result-object v3

    invoke-interface {v3}, Lddl;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ldch;->a:Ldbs;

    invoke-static {v4}, Ldbs;->m(Ldbs;)Ldeo;

    move-result-object v4

    invoke-virtual {v4}, Ldeo;->a()Lddl;

    move-result-object v4

    invoke-interface {v4}, Lddl;->o()J

    move-result-wide v4

    iget-object v6, p0, Ldch;->a:Ldbs;

    .line 1210
    invoke-static {v6}, Ldbs;->m(Ldbs;)Ldeo;

    move-result-object v6

    invoke-virtual {v6}, Ldeo;->a()Lddl;

    move-result-object v6

    invoke-interface {v6}, Lddl;->k()Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    move-wide/from16 v6, p1

    move/from16 v12, p3

    .line 1208
    invoke-static/range {v1 .. v13}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;JJLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Ldbs;->a(Ldbs;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1212
    iget-object v0, p0, Ldch;->a:Ldbs;

    invoke-virtual {v0}, Ldbs;->p()Lae;

    move-result-object v0

    iget-object v1, p0, Ldch;->a:Ldbs;

    invoke-virtual {v1}, Ldbs;->n()Lz;

    move-result-object v1

    invoke-static {v0, v1}, Ldhh;->a(Lae;Landroid/content/Context;)V

    .line 1213
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1152
    iget-object v0, p0, Ldch;->a:Ldbs;

    invoke-static {v0}, Ldbs;->l(Ldbs;)V

    .line 1153
    iget-object v0, p0, Ldch;->a:Ldbs;

    invoke-static {v0}, Ldbs;->d(Ldbs;)Ldcm;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1154
    iget-object v0, p0, Ldch;->a:Ldbs;

    invoke-static {v0}, Ldbs;->d(Ldbs;)Ldcm;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ldcm;->b(Z)V

    .line 1156
    :cond_0
    return-void
.end method

.method public b(JZ)V
    .locals 5

    .prologue
    .line 1217
    iget-object v0, p0, Ldch;->a:Ldbs;

    const v1, 0x7f0a07b0

    .line 1218
    invoke-virtual {v0, v1}, Ldbs;->e_(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldch;->a:Ldbs;

    const v2, 0x7f0a07b1

    .line 1219
    invoke-virtual {v1, v2}, Ldbs;->e_(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldch;->a:Ldbs;

    const v3, 0x7f0a07fa

    .line 1220
    invoke-virtual {v2, v3}, Ldbs;->e_(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldch;->a:Ldbs;

    const v4, 0x7f0a0597

    invoke-virtual {v3, v4}, Ldbs;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 1217
    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 1221
    iget-object v1, p0, Ldch;->a:Ldbs;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lu;I)V

    .line 1222
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "shape_id"

    invoke-virtual {v1, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1223
    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "permanent_delete"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1224
    iget-object v1, p0, Ldch;->a:Ldbs;

    invoke-virtual {v1}, Ldbs;->p()Lae;

    move-result-object v1

    const-string v2, "pouf_delete_shape"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 1225
    return-void
.end method
