.class public final Lmqh;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmqh;


# instance fields
.field public b:Ljava/lang/Integer;

.field private c:I

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/String;

.field private g:[Ljava/lang/String;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x0

    new-array v0, v0, [Lmqh;

    sput-object v0, Lmqh;->a:[Lmqh;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 229
    invoke-direct {p0}, Loxq;-><init>()V

    .line 251
    iput v1, p0, Lmqh;->c:I

    .line 260
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lmqh;->g:[Ljava/lang/String;

    .line 263
    iput v1, p0, Lmqh;->h:I

    .line 229
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    const/4 v1, 0x0

    .line 297
    .line 298
    iget-object v0, p0, Lmqh;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 299
    const/4 v0, 0x1

    iget-object v2, p0, Lmqh;->b:Ljava/lang/Integer;

    .line 300
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 302
    :goto_0
    iget v2, p0, Lmqh;->c:I

    if-eq v2, v6, :cond_0

    .line 303
    const/4 v2, 0x2

    iget v3, p0, Lmqh;->c:I

    .line 304
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 306
    :cond_0
    iget-object v2, p0, Lmqh;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 307
    const/4 v2, 0x3

    iget-object v3, p0, Lmqh;->d:Ljava/lang/Boolean;

    .line 308
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 310
    :cond_1
    iget-object v2, p0, Lmqh;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 311
    const/4 v2, 0x4

    iget-object v3, p0, Lmqh;->e:Ljava/lang/Boolean;

    .line 312
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 314
    :cond_2
    iget-object v2, p0, Lmqh;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 315
    const/4 v2, 0x5

    iget-object v3, p0, Lmqh;->f:Ljava/lang/String;

    .line 316
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 318
    :cond_3
    iget-object v2, p0, Lmqh;->g:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lmqh;->g:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 320
    iget-object v3, p0, Lmqh;->g:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 322
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 320
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 324
    :cond_4
    add-int/2addr v0, v2

    .line 325
    iget-object v1, p0, Lmqh;->g:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 327
    :cond_5
    iget v1, p0, Lmqh;->h:I

    if-eq v1, v6, :cond_6

    .line 328
    const/4 v1, 0x7

    iget v2, p0, Lmqh;->h:I

    .line 329
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    :cond_6
    iget-object v1, p0, Lmqh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 332
    iput v0, p0, Lmqh;->ai:I

    .line 333
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmqh;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 341
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 342
    sparse-switch v0, :sswitch_data_0

    .line 346
    iget-object v1, p0, Lmqh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 347
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmqh;->ah:Ljava/util/List;

    .line 350
    :cond_1
    iget-object v1, p0, Lmqh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 352
    :sswitch_0
    return-object p0

    .line 357
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmqh;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 361
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 362
    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 370
    :cond_2
    iput v0, p0, Lmqh;->c:I

    goto :goto_0

    .line 372
    :cond_3
    iput v4, p0, Lmqh;->c:I

    goto :goto_0

    .line 377
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqh;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 381
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqh;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 385
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqh;->f:Ljava/lang/String;

    goto :goto_0

    .line 389
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 390
    iget-object v0, p0, Lmqh;->g:[Ljava/lang/String;

    array-length v0, v0

    .line 391
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 392
    iget-object v2, p0, Lmqh;->g:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 393
    iput-object v1, p0, Lmqh;->g:[Ljava/lang/String;

    .line 394
    :goto_1
    iget-object v1, p0, Lmqh;->g:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 395
    iget-object v1, p0, Lmqh;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 396
    invoke-virtual {p1}, Loxn;->a()I

    .line 394
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 399
    :cond_4
    iget-object v1, p0, Lmqh;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 403
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 404
    if-eqz v0, :cond_5

    if-eq v0, v4, :cond_5

    if-ne v0, v5, :cond_6

    .line 407
    :cond_5
    iput v0, p0, Lmqh;->h:I

    goto/16 :goto_0

    .line 409
    :cond_6
    iput v3, p0, Lmqh;->h:I

    goto/16 :goto_0

    .line 342
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 268
    iget-object v0, p0, Lmqh;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 269
    const/4 v0, 0x1

    iget-object v1, p0, Lmqh;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 271
    :cond_0
    iget v0, p0, Lmqh;->c:I

    if-eq v0, v5, :cond_1

    .line 272
    const/4 v0, 0x2

    iget v1, p0, Lmqh;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 274
    :cond_1
    iget-object v0, p0, Lmqh;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 275
    const/4 v0, 0x3

    iget-object v1, p0, Lmqh;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 277
    :cond_2
    iget-object v0, p0, Lmqh;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 278
    const/4 v0, 0x4

    iget-object v1, p0, Lmqh;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 280
    :cond_3
    iget-object v0, p0, Lmqh;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 281
    const/4 v0, 0x5

    iget-object v1, p0, Lmqh;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 283
    :cond_4
    iget-object v0, p0, Lmqh;->g:[Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 284
    iget-object v1, p0, Lmqh;->g:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 285
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 284
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 288
    :cond_5
    iget v0, p0, Lmqh;->h:I

    if-eq v0, v5, :cond_6

    .line 289
    const/4 v0, 0x7

    iget v1, p0, Lmqh;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 291
    :cond_6
    iget-object v0, p0, Lmqh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 293
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lmqh;->a(Loxn;)Lmqh;

    move-result-object v0

    return-object v0
.end method
