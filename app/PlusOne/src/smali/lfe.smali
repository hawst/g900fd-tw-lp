.class public final Llfe;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static d:Llfb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Llfb",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Llfg;

.field private B:Z

.field private C:I

.field private D:I

.field private E:Landroid/view/View;

.field public a:I

.field public b:I

.field public c:I

.field private e:I

.field private f:Llfi;

.field private g:Lifv;

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Z

.field private final m:Landroid/graphics/RectF;

.field private final n:Landroid/graphics/RectF;

.field private final o:Lgk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgk",
            "<",
            "Llff;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/lang/Object;

.field private final q:Llfh;

.field private final r:Llfh;

.field private final s:Llfh;

.field private t:I

.field private u:I

.field private v:F

.field private w:I

.field private x:Z

.field private final y:Landroid/graphics/Rect;

.field private final z:[Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Llfd;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Llfd;-><init>(I)V

    sput-object v0, Llfe;->d:Llfb;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput v2, p0, Llfe;->h:I

    .line 93
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Llfe;->m:Landroid/graphics/RectF;

    .line 94
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Llfe;->n:Landroid/graphics/RectF;

    .line 96
    new-instance v0, Lgk;

    invoke-direct {v0}, Lgk;-><init>()V

    iput-object v0, p0, Llfe;->o:Lgk;

    .line 99
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Llfe;->p:Ljava/lang/Object;

    .line 100
    new-instance v0, Llfh;

    invoke-direct {v0}, Llfh;-><init>()V

    iput-object v0, p0, Llfe;->q:Llfh;

    .line 101
    new-instance v0, Llfh;

    invoke-direct {v0}, Llfh;-><init>()V

    iput-object v0, p0, Llfe;->r:Llfh;

    .line 102
    new-instance v0, Llfh;

    invoke-direct {v0}, Llfh;-><init>()V

    iput-object v0, p0, Llfe;->s:Llfh;

    .line 105
    iput v1, p0, Llfe;->b:I

    .line 106
    iput v1, p0, Llfe;->c:I

    .line 116
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Llfe;->y:Landroid/graphics/Rect;

    .line 117
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Llfe;->z:[Landroid/graphics/Rect;

    .line 177
    iput-object p1, p0, Llfe;->E:Landroid/view/View;

    .line 178
    new-instance v0, Llfg;

    invoke-direct {v0, p0}, Llfg;-><init>(Llfe;)V

    iput-object v0, p0, Llfe;->A:Llfg;

    .line 179
    iget-object v0, p0, Llfe;->A:Llfg;

    invoke-virtual {v0}, Llfg;->start()V

    .line 180
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/16 v2, 0x800

    .line 165
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-gt v0, v2, :cond_0

    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    const/16 v0, 0x200

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/16 v0, 0x100

    goto :goto_1
.end method

.method static synthetic a(Llfe;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Llfe;->e:I

    return v0
.end method

.method private a(III)Llff;
    .locals 3

    .prologue
    .line 523
    iget-object v1, p0, Llfe;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 524
    :try_start_0
    iget-object v0, p0, Llfe;->q:Llfh;

    invoke-virtual {v0}, Llfh;->a()Llff;

    move-result-object v0

    .line 525
    if-eqz v0, :cond_0

    .line 526
    const/4 v2, 0x1

    iput v2, v0, Llff;->j:I

    .line 527
    invoke-virtual {v0, p1, p2, p3}, Llff;->a(III)V

    .line 528
    monitor-exit v1

    .line 530
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Llff;

    invoke-direct {v0, p0, p1, p2, p3}, Llff;-><init>(Llfe;III)V

    monitor-exit v1

    goto :goto_0

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Llfe;III)Llff;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Llfe;->b(III)Llff;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/Rect;IIIFI)V
    .locals 16

    .prologue
    .line 365
    move/from16 v0, p6

    neg-int v2, v0

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    .line 366
    move-object/from16 v0, p0

    iget v4, v0, Llfe;->C:I

    int-to-double v4, v4

    .line 367
    move-object/from16 v0, p0

    iget v6, v0, Llfe;->D:I

    int-to-double v6, v6

    .line 369
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    .line 370
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 371
    mul-double v10, v8, v4

    mul-double v12, v2, v6

    sub-double/2addr v10, v12

    .line 372
    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    mul-double v12, v8, v4

    mul-double v14, v2, v6

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    .line 371
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    .line 373
    mul-double v12, v2, v4

    mul-double v14, v8, v6

    add-double/2addr v12, v14

    .line 374
    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    mul-double/2addr v2, v4

    mul-double v4, v8, v6

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 373
    invoke-static {v12, v13, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 376
    move/from16 v0, p2

    int-to-float v3, v0

    int-to-float v4, v10

    const/high16 v5, 0x40000000    # 2.0f

    mul-float v5, v5, p5

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 377
    move/from16 v0, p3

    int-to-float v4, v0

    int-to-float v5, v2

    const/high16 v6, 0x40000000    # 2.0f

    mul-float v6, v6, p5

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 378
    int-to-float v5, v3

    int-to-float v6, v10

    div-float v6, v6, p5

    add-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    .line 379
    int-to-float v6, v4

    int-to-float v2, v2

    div-float v2, v2, p5

    add-float/2addr v2, v6

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    .line 382
    move-object/from16 v0, p0

    iget v6, v0, Llfe;->e:I

    shl-int v6, v6, p4

    .line 383
    const/4 v7, 0x0

    div-int/2addr v3, v6

    mul-int/2addr v3, v6

    invoke-static {v7, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 384
    const/4 v7, 0x0

    div-int/2addr v4, v6

    mul-int/2addr v4, v6

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 385
    move-object/from16 v0, p0

    iget v6, v0, Llfe;->b:I

    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 386
    move-object/from16 v0, p0

    iget v6, v0, Llfe;->c:I

    invoke-static {v6, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 388
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 389
    return-void
.end method

.method static synthetic a(Llfe;Llff;)V
    .locals 4

    .prologue
    .line 39
    iget-object v1, p0, Llfe;->p:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p1, Llff;->j:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    iput v0, p1, Llff;->j:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-virtual {p1}, Llff;->o()Z

    move-result v1

    iget-object v2, p0, Llfe;->p:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget v0, p1, Llff;->j:I

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    const/16 v0, 0x40

    iput v0, p1, Llff;->j:I

    iget-object v0, p1, Llff;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    sget-object v0, Llfe;->d:Llfb;

    iget-object v1, p1, Llff;->i:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Llfb;->a(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p1, Llff;->i:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Llfe;->q:Llfh;

    invoke-virtual {v0, p1}, Llfh;->a(Llff;)Z

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    if-eqz v1, :cond_3

    const/16 v0, 0x8

    :goto_1
    :try_start_3
    iput v0, p1, Llff;->j:I

    if-nez v1, :cond_4

    monitor-exit v2

    goto :goto_0

    :cond_3
    const/16 v0, 0x10

    goto :goto_1

    :cond_4
    iget-object v0, p0, Llfe;->r:Llfh;

    invoke-virtual {v0, p1}, Llfh;->a(Llff;)Z

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Llfe;->E:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    goto :goto_0
.end method

.method private a(Llff;)V
    .locals 3

    .prologue
    .line 485
    iget-object v1, p0, Llfe;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 486
    :try_start_0
    iget v0, p1, Llff;->j:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 487
    const/4 v0, 0x2

    iput v0, p1, Llff;->j:I

    .line 488
    iget-object v0, p0, Llfe;->s:Llfh;

    invoke-virtual {v0, p1}, Llfh;->a(Llff;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Llfe;->p:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 492
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Llff;Lifx;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 638
    :goto_0
    invoke-virtual {p1}, Llff;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    invoke-interface {p2, p1, p3, p4}, Lifx;->a(Lifv;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 640
    const/4 v0, 0x1

    .line 646
    :goto_1
    return v0

    .line 644
    :cond_0
    invoke-virtual {p1}, Llff;->p()Llff;

    move-result-object v0

    .line 645
    if-nez v0, :cond_1

    .line 646
    const/4 v0, 0x0

    goto :goto_1

    .line 648
    :cond_1
    iget v1, p1, Llff;->e:I

    iget v2, v0, Llff;->e:I

    if-ne v1, v2, :cond_2

    .line 649
    iget v1, p3, Landroid/graphics/RectF;->left:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->left:F

    .line 650
    iget v1, p3, Landroid/graphics/RectF;->right:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->right:F

    .line 655
    :goto_2
    iget v1, p1, Llff;->f:I

    iget v2, v0, Llff;->f:I

    if-ne v1, v2, :cond_3

    .line 656
    iget v1, p3, Landroid/graphics/RectF;->top:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->top:F

    .line 657
    iget v1, p3, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->bottom:F

    :goto_3
    move-object p1, v0

    .line 663
    goto :goto_0

    .line 652
    :cond_2
    iget v1, p0, Llfe;->e:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->left:F

    .line 653
    iget v1, p0, Llfe;->e:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 659
    :cond_3
    iget v1, p0, Llfe;->e:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->top:F

    .line 660
    iget v1, p0, Llfe;->e:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->bottom:F

    goto :goto_3
.end method

.method private b(III)Llff;
    .locals 4

    .prologue
    .line 563
    iget-object v0, p0, Llfe;->o:Lgk;

    invoke-static {p1, p2, p3}, Llfe;->c(III)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lgk;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llff;

    return-object v0
.end method

.method static synthetic b(Llfe;)Llfi;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Llfe;->f:Llfi;

    return-object v0
.end method

.method private b(Llff;)V
    .locals 3

    .prologue
    .line 535
    iget-object v1, p0, Llfe;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 536
    :try_start_0
    iget v0, p1, Llff;->j:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 537
    const/16 v0, 0x20

    iput v0, p1, Llff;->j:I

    .line 538
    monitor-exit v1

    .line 546
    :goto_0
    return-void

    .line 540
    :cond_0
    const/16 v0, 0x40

    iput v0, p1, Llff;->j:I

    .line 541
    iget-object v0, p1, Llff;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 542
    sget-object v0, Llfe;->d:Llfb;

    iget-object v2, p1, Llff;->i:Landroid/graphics/Bitmap;

    invoke-interface {v0, v2}, Llfb;->a(Ljava/lang/Object;)Z

    .line 543
    const/4 v0, 0x0

    iput-object v0, p1, Llff;->i:Landroid/graphics/Bitmap;

    .line 545
    :cond_1
    iget-object v0, p0, Llfe;->q:Llfh;

    invoke-virtual {v0, p1}, Llfh;->a(Llff;)Z

    .line 546
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic c(Llfe;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Llfe;->h:I

    return v0
.end method

.method private static c(III)J
    .locals 5

    .prologue
    const/16 v4, 0x10

    .line 567
    int-to-long v0, p0

    .line 568
    shl-long/2addr v0, v4

    int-to-long v2, p1

    or-long/2addr v0, v2

    .line 569
    shl-long/2addr v0, v4

    int-to-long v2, p2

    or-long/2addr v0, v2

    .line 570
    return-wide v0
.end method

.method static synthetic c()Llfb;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Llfe;->d:Llfb;

    return-object v0
.end method

.method static synthetic d(Llfe;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Llfe;->p:Ljava/lang/Object;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 474
    const/4 v0, 0x1

    iput-boolean v0, p0, Llfe;->B:Z

    .line 475
    iget-object v0, p0, Llfe;->o:Lgk;

    invoke-virtual {v0}, Lgk;->b()I

    move-result v2

    .line 476
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 477
    iget-object v0, p0, Llfe;->o:Lgk;

    invoke-virtual {v0, v1}, Lgk;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llff;

    .line 478
    invoke-virtual {v0}, Llff;->n()Z

    move-result v3

    if-nez v3, :cond_0

    .line 479
    invoke-direct {p0, v0}, Llfe;->a(Llff;)V

    .line 476
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 482
    :cond_1
    return-void
.end method

.method static synthetic e(Llfe;)Llfh;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Llfe;->s:Llfh;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 222
    iget-object v4, p0, Llfe;->p:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Llfe;->s:Llfh;

    invoke-virtual {v0}, Llfh;->b()V

    iget-object v0, p0, Llfe;->r:Llfh;

    invoke-virtual {v0}, Llfh;->b()V

    iget-object v0, p0, Llfe;->o:Lgk;

    invoke-virtual {v0}, Lgk;->b()I

    move-result v5

    move v1, v3

    :goto_0
    if-ge v1, v5, :cond_0

    iget-object v0, p0, Llfe;->o:Lgk;

    invoke-virtual {v0, v1}, Lgk;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llff;

    invoke-direct {p0, v0}, Llfe;->b(Llff;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Llfe;->o:Lgk;

    invoke-virtual {v0}, Lgk;->c()V

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    iget-object v0, p0, Llfe;->f:Llfi;

    if-nez v0, :cond_1

    .line 224
    iput v3, p0, Llfe;->b:I

    .line 225
    iput v3, p0, Llfe;->c:I

    .line 226
    iput v3, p0, Llfe;->a:I

    .line 227
    const/4 v0, 0x0

    iput-object v0, p0, Llfe;->g:Lifv;

    .line 235
    :goto_1
    iput-boolean v2, p0, Llfe;->x:Z

    .line 236
    return-void

    .line 222
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 229
    :cond_1
    iget-object v0, p0, Llfe;->f:Llfi;

    invoke-interface {v0}, Llfi;->b()I

    move-result v0

    iput v0, p0, Llfe;->b:I

    .line 230
    iget-object v0, p0, Llfe;->f:Llfi;

    invoke-interface {v0}, Llfi;->c()I

    move-result v0

    iput v0, p0, Llfe;->c:I

    .line 231
    iget-object v0, p0, Llfe;->f:Llfi;

    invoke-interface {v0}, Llfi;->d()Lifv;

    move-result-object v0

    iput-object v0, p0, Llfe;->g:Lifv;

    .line 232
    iget-object v0, p0, Llfe;->f:Llfi;

    invoke-interface {v0}, Llfi;->a()I

    move-result v0

    iput v0, p0, Llfe;->e:I

    .line 233
    iget-object v0, p0, Llfe;->g:Lifv;

    if-eqz v0, :cond_2

    iget v0, p0, Llfe;->b:I

    int-to-float v0, v0

    iget-object v1, p0, Llfe;->g:Lifv;

    invoke-virtual {v1}, Lifv;->c()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Lifu;->a(F)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Llfe;->a:I

    goto :goto_1

    :cond_2
    iget v0, p0, Llfe;->b:I

    iget v1, p0, Llfe;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v0, p0, Llfe;->e:I

    move v1, v2

    :goto_2
    if-ge v0, v3, :cond_3

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iput v1, p0, Llfe;->a:I

    goto :goto_1
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 239
    iput p1, p0, Llfe;->C:I

    .line 240
    iput p2, p0, Llfe;->D:I

    .line 241
    return-void
.end method

.method public a(IIF)V
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Llfe;->t:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Llfe;->u:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Llfe;->v:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 248
    :cond_0
    iput p1, p0, Llfe;->t:I

    .line 249
    iput p2, p0, Llfe;->u:I

    .line 250
    iput p3, p0, Llfe;->v:F

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Llfe;->x:Z

    goto :goto_0
.end method

.method public a(Llfi;I)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Llfe;->f:Llfi;

    if-eq v0, p1, :cond_0

    .line 196
    iput-object p1, p0, Llfe;->f:Llfi;

    .line 197
    invoke-virtual {p0}, Llfe;->a()V

    .line 199
    :cond_0
    iget v0, p0, Llfe;->w:I

    if-eq v0, p2, :cond_1

    .line 200
    iput p2, p0, Llfe;->w:I

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Llfe;->x:Z

    .line 203
    :cond_1
    return-void
.end method

.method public a(Lifx;)Z
    .locals 20

    .prologue
    .line 417
    move-object/from16 v0, p0

    iget v2, v0, Llfe;->C:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Llfe;->D:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Llfe;->x:Z

    if-nez v2, :cond_2

    .line 418
    :cond_0
    :goto_0
    const/4 v3, 0x1

    const/4 v2, 0x0

    :cond_1
    :goto_1
    if-lez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Llfe;->p:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->r:Llfh;

    invoke-virtual {v2}, Llfh;->a()Llff;

    move-result-object v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Llff;->n()Z

    move-result v4

    if-nez v4, :cond_1

    iget v4, v2, Llff;->j:I

    const/16 v5, 0x8

    if-ne v4, v5, :cond_e

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Llff;->c(Lifx;)V

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 417
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Llfe;->x:Z

    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v3, v0, Llfe;->v:F

    div-float/2addr v2, v3

    invoke-static {v2}, Lifu;->b(F)I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->a:I

    invoke-static {v2, v3, v4}, Lifu;->a(III)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Llfe;->h:I

    move-object/from16 v0, p0

    iget v2, v0, Llfe;->h:I

    move-object/from16 v0, p0

    iget v3, v0, Llfe;->a:I

    if-eq v2, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Llfe;->y:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->t:I

    move-object/from16 v0, p0

    iget v5, v0, Llfe;->u:I

    move-object/from16 v0, p0

    iget v6, v0, Llfe;->h:I

    move-object/from16 v0, p0

    iget v7, v0, Llfe;->v:F

    move-object/from16 v0, p0

    iget v8, v0, Llfe;->w:I

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Llfe;->a(Landroid/graphics/Rect;IIIFI)V

    move-object/from16 v0, p0

    iget v2, v0, Llfe;->C:I

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    iget v4, v3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v5, v0, Llfe;->t:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Llfe;->v:F

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Llfe;->i:I

    move-object/from16 v0, p0

    iget v2, v0, Llfe;->D:I

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    iget v3, v3, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->u:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->v:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Llfe;->j:I

    move-object/from16 v0, p0

    iget v2, v0, Llfe;->v:F

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->h:I

    shl-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f400000    # 0.75f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Llfe;->h:I

    add-int/lit8 v2, v2, -0x1

    :goto_2
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->a:I

    add-int/lit8 v4, v4, -0x2

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v9

    add-int/lit8 v2, v9, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Llfe;->a:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Llfe;->z:[Landroid/graphics/Rect;

    move v6, v9

    :goto_3
    if-ge v6, v10, :cond_5

    sub-int v2, v6, v9

    aget-object v3, v11, v2

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->t:I

    move-object/from16 v0, p0

    iget v5, v0, Llfe;->u:I

    move-object/from16 v0, p0

    iget v8, v0, Llfe;->w:I

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    add-int/lit8 v12, v6, 0x1

    shl-int/2addr v7, v12

    int-to-float v7, v7

    div-float v7, v2, v7

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Llfe;->a(Landroid/graphics/Rect;IIIFI)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Llfe;->h:I

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Llfe;->h:I

    add-int/lit8 v2, v2, -0x2

    move-object/from16 v0, p0

    iget v3, v0, Llfe;->C:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->t:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Llfe;->v:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Llfe;->i:I

    move-object/from16 v0, p0

    iget v3, v0, Llfe;->D:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->u:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Llfe;->v:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Llfe;->j:I

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Llfe;->w:I

    rem-int/lit8 v2, v2, 0x5a

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Llfe;->p:Ljava/lang/Object;

    monitor-enter v5

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->s:Llfh;

    invoke-virtual {v2}, Llfh;->b()V

    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->r:Llfh;

    invoke-virtual {v2}, Llfh;->b()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Llfe;->B:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->o:Lgk;

    invoke-virtual {v2}, Lgk;->b()I

    move-result v4

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->o:Lgk;

    invoke-virtual {v2, v3}, Lgk;->c(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llff;

    iget v6, v2, Llff;->g:I

    if-lt v6, v9, :cond_6

    if-ge v6, v10, :cond_6

    sub-int/2addr v6, v9

    aget-object v6, v11, v6

    iget v7, v2, Llff;->e:I

    iget v8, v2, Llff;->f:I

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Llfe;->o:Lgk;

    invoke-virtual {v6, v3}, Lgk;->a(I)V

    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Llfe;->b(Llff;)V

    :cond_7
    move v2, v3

    move v3, v4

    add-int/lit8 v2, v2, 0x1

    move v4, v3

    move v3, v2

    goto :goto_4

    :cond_8
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v9

    :goto_5
    if-ge v3, v10, :cond_d

    move-object/from16 v0, p0

    iget v2, v0, Llfe;->e:I

    shl-int v6, v2, v3

    sub-int v2, v3, v9

    aget-object v7, v11, v2

    iget v2, v7, Landroid/graphics/Rect;->top:I

    iget v8, v7, Landroid/graphics/Rect;->bottom:I

    move v5, v2

    :goto_6
    if-ge v5, v8, :cond_c

    iget v2, v7, Landroid/graphics/Rect;->left:I

    iget v12, v7, Landroid/graphics/Rect;->right:I

    move v4, v2

    :goto_7
    if-ge v4, v12, :cond_b

    invoke-static {v4, v5, v3}, Llfe;->c(III)J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->o:Lgk;

    invoke-virtual {v2, v14, v15}, Lgk;->a(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llff;

    if-eqz v2, :cond_a

    iget v13, v2, Llff;->j:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_9

    const/4 v13, 0x1

    iput v13, v2, Llff;->j:I

    :cond_9
    :goto_8
    add-int v2, v4, v6

    move v4, v2

    goto :goto_7

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_a
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Llfe;->a(III)Llff;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v13, v0, Llfe;->o:Lgk;

    invoke-virtual {v13, v14, v15, v2}, Lgk;->b(JLjava/lang/Object;)V

    goto :goto_8

    :cond_b
    add-int v2, v5, v6

    move v5, v2

    goto :goto_6

    :cond_c
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->E:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->postInvalidate()V

    goto/16 :goto_0

    .line 418
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    :cond_e
    iget v4, v2, Llff;->j:I

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x33

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Tile in upload queue has invalid state: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_f
    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->E:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->postInvalidate()V

    .line 420
    :cond_10
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Llfe;->k:I

    .line 421
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Llfe;->l:Z

    .line 423
    move-object/from16 v0, p0

    iget v6, v0, Llfe;->h:I

    .line 424
    move-object/from16 v0, p0

    iget v3, v0, Llfe;->w:I

    .line 425
    const/4 v2, 0x0

    .line 426
    if-eqz v3, :cond_20

    .line 427
    const/4 v2, 0x2

    move v8, v2

    .line 430
    :goto_9
    if-eqz v8, :cond_11

    .line 431
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lifx;->a(I)V

    .line 432
    if-eqz v3, :cond_11

    .line 433
    move-object/from16 v0, p0

    iget v2, v0, Llfe;->C:I

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->D:I

    div-int/lit8 v4, v4, 0x2

    .line 434
    int-to-float v5, v2

    int-to-float v7, v4

    move-object/from16 v0, p1

    invoke-interface {v0, v5, v7}, Lifx;->a(FF)V

    .line 435
    int-to-float v3, v3

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v5, v7, v9}, Lifx;->a(FFFF)V

    .line 436
    neg-int v2, v2

    int-to-float v2, v2

    neg-int v3, v4

    int-to-float v3, v3

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lifx;->a(FF)V

    .line 440
    :cond_11
    :try_start_4
    move-object/from16 v0, p0

    iget v2, v0, Llfe;->a:I

    if-eq v6, v2, :cond_19

    .line 441
    move-object/from16 v0, p0

    iget v2, v0, Llfe;->e:I

    shl-int v7, v2, v6

    .line 442
    int-to-float v2, v7

    move-object/from16 v0, p0

    iget v3, v0, Llfe;->v:F

    mul-float v9, v2, v3

    .line 443
    move-object/from16 v0, p0

    iget-object v10, v0, Llfe;->y:Landroid/graphics/Rect;

    .line 445
    iget v3, v10, Landroid/graphics/Rect;->top:I

    const/4 v2, 0x0

    :goto_a
    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    if-ge v3, v4, :cond_1a

    .line 446
    move-object/from16 v0, p0

    iget v4, v0, Llfe;->j:I

    int-to-float v4, v4

    int-to-float v5, v2

    mul-float/2addr v5, v9

    add-float v11, v4, v5

    .line 447
    iget v5, v10, Landroid/graphics/Rect;->left:I

    const/4 v4, 0x0

    :goto_b
    iget v12, v10, Landroid/graphics/Rect;->right:I

    if-ge v5, v12, :cond_18

    .line 448
    move-object/from16 v0, p0

    iget v12, v0, Llfe;->i:I

    int-to-float v12, v12

    int-to-float v13, v4

    mul-float/2addr v13, v9

    add-float/2addr v12, v13

    .line 449
    move-object/from16 v0, p0

    iget-object v13, v0, Llfe;->m:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v14, v0, Llfe;->n:Landroid/graphics/RectF;

    add-float v15, v12, v9

    add-float v16, v11, v9

    move/from16 v0, v16

    invoke-virtual {v14, v12, v11, v15, v0}, Landroid/graphics/RectF;->set(FFFF)V

    const/4 v12, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Llfe;->e:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Llfe;->e:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v13, v12, v15, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v3, v6}, Llfe;->b(III)Llff;

    move-result-object v12

    if-eqz v12, :cond_13

    invoke-virtual {v12}, Llff;->n()Z

    move-result v15

    if-nez v15, :cond_12

    iget v15, v12, Llff;->j:I

    const/16 v16, 0x8

    move/from16 v0, v16

    if-ne v15, v0, :cond_17

    move-object/from16 v0, p0

    iget v15, v0, Llfe;->k:I

    if-lez v15, :cond_15

    move-object/from16 v0, p0

    iget v15, v0, Llfe;->k:I

    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Llfe;->k:I

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Llff;->c(Lifx;)V

    :cond_12
    :goto_c
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v12, v1, v13, v14}, Llfe;->a(Llff;Lifx;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v12

    if-nez v12, :cond_14

    :cond_13
    move-object/from16 v0, p0

    iget-object v12, v0, Llfe;->g:Lifv;

    if-eqz v12, :cond_14

    move-object/from16 v0, p0

    iget v12, v0, Llfe;->e:I

    shl-int/2addr v12, v6

    move-object/from16 v0, p0

    iget-object v15, v0, Llfe;->g:Lifv;

    invoke-virtual {v15}, Lifv;->c()I

    move-result v15

    int-to-float v15, v15

    move-object/from16 v0, p0

    iget v0, v0, Llfe;->b:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    div-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Llfe;->g:Lifv;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lifv;->d()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Llfe;->c:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v16, v16, v17

    int-to-float v0, v5

    move/from16 v17, v0

    mul-float v17, v17, v15

    int-to-float v0, v3

    move/from16 v18, v0

    mul-float v18, v18, v16

    add-int v19, v5, v12

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v15, v15, v19

    add-int/2addr v12, v3

    int-to-float v12, v12

    mul-float v12, v12, v16

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1, v15, v12}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v12, v0, Llfe;->g:Lifv;

    move-object/from16 v0, p1

    invoke-interface {v0, v12, v13, v14}, Lifx;->a(Lifv;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 447
    :cond_14
    add-int/2addr v5, v7

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_b

    .line 449
    :cond_15
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Llfe;->l:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_c

    .line 458
    :catchall_2
    move-exception v2

    if-eqz v8, :cond_16

    .line 459
    invoke-interface/range {p1 .. p1}, Lifx;->b()V

    :cond_16
    throw v2

    .line 449
    :cond_17
    :try_start_5
    iget v15, v12, Llff;->j:I

    const/16 v16, 0x10

    move/from16 v0, v16

    if-eq v15, v0, :cond_12

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Llfe;->l:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Llfe;->a(Llff;)V

    goto/16 :goto_c

    .line 445
    :cond_18
    add-int/2addr v3, v7

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_a

    .line 452
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->g:Lifv;

    if-eqz v2, :cond_1a

    .line 453
    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->g:Lifv;

    move-object/from16 v0, p0

    iget v4, v0, Llfe;->i:I

    move-object/from16 v0, p0

    iget v5, v0, Llfe;->j:I

    move-object/from16 v0, p0

    iget v3, v0, Llfe;->b:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v6, v0, Llfe;->v:F

    mul-float/2addr v3, v6

    .line 454
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v6

    move-object/from16 v0, p0

    iget v3, v0, Llfe;->c:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v7, v0, Llfe;->v:F

    mul-float/2addr v3, v7

    .line 455
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v3, p1

    .line 453
    invoke-virtual/range {v2 .. v7}, Lifv;->a(Lifx;IIII)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 458
    :cond_1a
    if-eqz v8, :cond_1b

    .line 459
    invoke-interface/range {p1 .. p1}, Lifx;->b()V

    .line 463
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Llfe;->l:Z

    if-eqz v2, :cond_1e

    .line 464
    move-object/from16 v0, p0

    iget-boolean v2, v0, Llfe;->B:Z

    if-nez v2, :cond_1c

    .line 465
    invoke-direct/range {p0 .. p0}, Llfe;->d()V

    .line 470
    :cond_1c
    :goto_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Llfe;->l:Z

    if-nez v2, :cond_1d

    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->g:Lifv;

    if-eqz v2, :cond_1f

    :cond_1d
    const/4 v2, 0x1

    :goto_e
    return v2

    .line 468
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Llfe;->E:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->postInvalidate()V

    goto :goto_d

    .line 470
    :cond_1f
    const/4 v2, 0x0

    goto :goto_e

    :cond_20
    move v8, v2

    goto/16 :goto_9
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 392
    const/4 v0, 0x1

    iput-boolean v0, p0, Llfe;->x:Z

    .line 394
    iget-object v0, p0, Llfe;->A:Llfg;

    invoke-virtual {v0}, Llfg;->a()V

    .line 395
    iget-object v1, p0, Llfe;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 396
    :try_start_0
    iget-object v0, p0, Llfe;->r:Llfh;

    invoke-virtual {v0}, Llfh;->b()V

    .line 397
    iget-object v0, p0, Llfe;->s:Llfh;

    invoke-virtual {v0}, Llfh;->b()V

    .line 398
    iget-object v0, p0, Llfe;->q:Llfh;

    invoke-virtual {v0}, Llfh;->a()Llff;

    move-result-object v0

    .line 399
    :goto_0
    if-eqz v0, :cond_0

    .line 400
    invoke-virtual {v0}, Llff;->j()V

    .line 401
    iget-object v0, p0, Llfe;->q:Llfh;

    invoke-virtual {v0}, Llfh;->a()Llff;

    move-result-object v0

    goto :goto_0

    .line 403
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    iget-object v0, p0, Llfe;->o:Lgk;

    invoke-virtual {v0}, Lgk;->b()I

    move-result v3

    move v1, v2

    .line 406
    :goto_1
    if-ge v1, v3, :cond_1

    .line 407
    iget-object v0, p0, Llfe;->o:Lgk;

    invoke-virtual {v0, v1}, Lgk;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llff;

    .line 408
    invoke-virtual {v0}, Llff;->j()V

    .line 406
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 403
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 410
    :cond_1
    iget-object v0, p0, Llfe;->o:Lgk;

    invoke-virtual {v0}, Lgk;->c()V

    .line 411
    iget-object v0, p0, Llfe;->y:Landroid/graphics/Rect;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 413
    :cond_2
    sget-object v0, Llfe;->d:Llfb;

    invoke-interface {v0}, Llfb;->a()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 414
    return-void
.end method
