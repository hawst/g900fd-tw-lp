.class public final Lmcj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfk;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26242
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26245
    iput-object v0, p0, Lmcj;->apiHeader:Llyr;

    .line 26248
    iput-object v0, p0, Lmcj;->a:Lnfk;

    .line 26242
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26265
    const/4 v0, 0x0

    .line 26266
    iget-object v1, p0, Lmcj;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 26267
    const/4 v0, 0x1

    iget-object v1, p0, Lmcj;->apiHeader:Llyr;

    .line 26268
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26270
    :cond_0
    iget-object v1, p0, Lmcj;->a:Lnfk;

    if-eqz v1, :cond_1

    .line 26271
    const/4 v1, 0x2

    iget-object v2, p0, Lmcj;->a:Lnfk;

    .line 26272
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26274
    :cond_1
    iget-object v1, p0, Lmcj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26275
    iput v0, p0, Lmcj;->ai:I

    .line 26276
    return v0
.end method

.method public a(Loxn;)Lmcj;
    .locals 2

    .prologue
    .line 26284
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26285
    sparse-switch v0, :sswitch_data_0

    .line 26289
    iget-object v1, p0, Lmcj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26290
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcj;->ah:Ljava/util/List;

    .line 26293
    :cond_1
    iget-object v1, p0, Lmcj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26295
    :sswitch_0
    return-object p0

    .line 26300
    :sswitch_1
    iget-object v0, p0, Lmcj;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 26301
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmcj;->apiHeader:Llyr;

    .line 26303
    :cond_2
    iget-object v0, p0, Lmcj;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26307
    :sswitch_2
    iget-object v0, p0, Lmcj;->a:Lnfk;

    if-nez v0, :cond_3

    .line 26308
    new-instance v0, Lnfk;

    invoke-direct {v0}, Lnfk;-><init>()V

    iput-object v0, p0, Lmcj;->a:Lnfk;

    .line 26310
    :cond_3
    iget-object v0, p0, Lmcj;->a:Lnfk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26285
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26253
    iget-object v0, p0, Lmcj;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 26254
    const/4 v0, 0x1

    iget-object v1, p0, Lmcj;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26256
    :cond_0
    iget-object v0, p0, Lmcj;->a:Lnfk;

    if-eqz v0, :cond_1

    .line 26257
    const/4 v0, 0x2

    iget-object v1, p0, Lmcj;->a:Lnfk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26259
    :cond_1
    iget-object v0, p0, Lmcj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26261
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26238
    invoke-virtual {p0, p1}, Lmcj;->a(Loxn;)Lmcj;

    move-result-object v0

    return-object v0
.end method
