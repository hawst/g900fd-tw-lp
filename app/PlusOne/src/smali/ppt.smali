.class public final Lppt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lpgx;

.field private c:I

.field private d:Lpvy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 389
    invoke-direct {p0}, Loxq;-><init>()V

    .line 398
    const/high16 v0, -0x80000000

    iput v0, p0, Lppt;->c:I

    .line 403
    iput-object v1, p0, Lppt;->b:Lpgx;

    .line 406
    iput-object v1, p0, Lppt;->d:Lpvy;

    .line 389
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 429
    const/4 v0, 0x0

    .line 430
    iget v1, p0, Lppt;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 431
    const/4 v0, 0x1

    iget v1, p0, Lppt;->c:I

    .line 432
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 434
    :cond_0
    iget-object v1, p0, Lppt;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 435
    const/4 v1, 0x4

    iget-object v2, p0, Lppt;->a:Ljava/lang/String;

    .line 436
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    :cond_1
    iget-object v1, p0, Lppt;->d:Lpvy;

    if-eqz v1, :cond_2

    .line 439
    const/4 v1, 0x5

    iget-object v2, p0, Lppt;->d:Lpvy;

    .line 440
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 442
    :cond_2
    iget-object v1, p0, Lppt;->b:Lpgx;

    if-eqz v1, :cond_3

    .line 443
    const/4 v1, 0x6

    iget-object v2, p0, Lppt;->b:Lpgx;

    .line 444
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 446
    :cond_3
    iget-object v1, p0, Lppt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    iput v0, p0, Lppt;->ai:I

    .line 448
    return v0
.end method

.method public a(Loxn;)Lppt;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 456
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 457
    sparse-switch v0, :sswitch_data_0

    .line 461
    iget-object v1, p0, Lppt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 462
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lppt;->ah:Ljava/util/List;

    .line 465
    :cond_1
    iget-object v1, p0, Lppt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 467
    :sswitch_0
    return-object p0

    .line 472
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 473
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 476
    :cond_2
    iput v0, p0, Lppt;->c:I

    goto :goto_0

    .line 478
    :cond_3
    iput v2, p0, Lppt;->c:I

    goto :goto_0

    .line 483
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lppt;->a:Ljava/lang/String;

    goto :goto_0

    .line 487
    :sswitch_3
    iget-object v0, p0, Lppt;->d:Lpvy;

    if-nez v0, :cond_4

    .line 488
    new-instance v0, Lpvy;

    invoke-direct {v0}, Lpvy;-><init>()V

    iput-object v0, p0, Lppt;->d:Lpvy;

    .line 490
    :cond_4
    iget-object v0, p0, Lppt;->d:Lpvy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 494
    :sswitch_4
    iget-object v0, p0, Lppt;->b:Lpgx;

    if-nez v0, :cond_5

    .line 495
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lppt;->b:Lpgx;

    .line 497
    :cond_5
    iget-object v0, p0, Lppt;->b:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 457
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 411
    iget v0, p0, Lppt;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 412
    const/4 v0, 0x1

    iget v1, p0, Lppt;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 414
    :cond_0
    iget-object v0, p0, Lppt;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 415
    const/4 v0, 0x4

    iget-object v1, p0, Lppt;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 417
    :cond_1
    iget-object v0, p0, Lppt;->d:Lpvy;

    if-eqz v0, :cond_2

    .line 418
    const/4 v0, 0x5

    iget-object v1, p0, Lppt;->d:Lpvy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 420
    :cond_2
    iget-object v0, p0, Lppt;->b:Lpgx;

    if-eqz v0, :cond_3

    .line 421
    const/4 v0, 0x6

    iget-object v1, p0, Lppt;->b:Lpgx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 423
    :cond_3
    iget-object v0, p0, Lppt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 425
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0, p1}, Lppt;->a(Loxn;)Lppt;

    move-result-object v0

    return-object v0
.end method
