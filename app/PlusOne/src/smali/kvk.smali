.class public final Lkvk;
.super Lkxk;
.source "PG"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:[Ljava/lang/String;

.field private final i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lkxk;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 49
    iput-object p5, p0, Lkvk;->g:Ljava/lang/String;

    .line 50
    iput-object p6, p0, Lkvk;->h:[Ljava/lang/String;

    .line 51
    iput p7, p0, Lkvk;->i:I

    .line 52
    return-void
.end method


# virtual methods
.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lkvk;->l()Lkxl;

    move-result-object v0

    return-object v0
.end method

.method public l()Lkxl;
    .locals 17

    .prologue
    .line 56
    move-object/from16 v0, p0

    iget-object v1, v0, Lkvk;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lkvk;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lkvk;->i:I

    if-ge v1, v2, :cond_1

    .line 57
    :cond_0
    new-instance v1, Lkxl;

    invoke-direct {v1}, Lkxl;-><init>()V

    .line 84
    :goto_0
    return-object v1

    .line 60
    :cond_1
    new-instance v1, Lkvl;

    invoke-virtual/range {p0 .. p0}, Lkvk;->n()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lkfo;

    .line 61
    invoke-virtual/range {p0 .. p0}, Lkvk;->n()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lkvk;->c:I

    invoke-direct {v3, v4, v5}, Lkfo;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lkvk;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lkvk;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lkvk;->e:Ljava/lang/String;

    const/16 v7, 0x32

    invoke-direct/range {v1 .. v7}, Lkvl;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 63
    move-object/from16 v0, p0

    iput-object v1, v0, Lkvk;->f:Lkff;

    .line 65
    :try_start_0
    invoke-virtual {v1}, Lkvl;->l()V

    .line 66
    invoke-virtual {v1}, Lkvl;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 67
    sget-object v1, Lkvk;->b:Lkxl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lkvk;->f:Lkff;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lkvk;->f:Lkff;

    .line 73
    invoke-virtual {v1}, Lkvl;->t()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 74
    const-string v2, "SquareMemberSearch"

    invoke-virtual {v1, v2}, Lkvl;->d(Ljava/lang/String;)V

    .line 75
    iget-object v2, v1, Lkff;->k:Ljava/lang/Exception;

    if-eqz v2, :cond_3

    .line 76
    new-instance v2, Lkxl;

    iget-object v1, v1, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v2, v1}, Lkxl;-><init>(Ljava/lang/Exception;)V

    move-object v1, v2

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lkvk;->f:Lkff;

    throw v1

    .line 78
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 81
    :cond_4
    invoke-virtual {v1}, Lkvl;->i()Lnry;

    move-result-object v10

    .line 83
    move-object/from16 v0, p0

    iget-object v7, v0, Lkvk;->h:[Ljava/lang/String;

    iget-object v11, v10, Lnry;->e:[Lnsh;

    const/4 v8, 0x0

    const/4 v6, -0x1

    const/4 v5, -0x1

    const/4 v4, -0x1

    const/4 v3, -0x1

    const/4 v2, -0x1

    new-instance v12, Lhym;

    invoke-direct {v12, v7}, Lhym;-><init>([Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_1
    array-length v9, v7

    if-ge v1, v9, :cond_a

    aget-object v9, v7, v1

    const-string v13, "_id"

    invoke-virtual {v13, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    move v6, v1

    :cond_5
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    const-string v13, "qualified_id"

    invoke-virtual {v13, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    move v5, v1

    goto :goto_2

    :cond_7
    const-string v13, "name"

    invoke-virtual {v13, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    move v4, v1

    goto :goto_2

    :cond_8
    const-string v13, "avatar"

    invoke-virtual {v13, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    move v3, v1

    goto :goto_2

    :cond_9
    const-string v13, "membership_status"

    invoke-virtual {v13, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    move v2, v1

    goto :goto_2

    :cond_a
    if-eqz v11, :cond_10

    array-length v1, v11

    :goto_3
    array-length v7, v7

    new-array v13, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    move v9, v7

    :goto_4
    if-ge v9, v1, :cond_15

    const/4 v7, 0x0

    invoke-static {v13, v7}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    aget-object v14, v11, v9

    if-ltz v6, :cond_b

    add-int/lit8 v7, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v13, v6

    move v8, v7

    :cond_b
    if-ltz v5, :cond_c

    iget-object v7, v14, Lnsh;->f:Lnsa;

    if-eqz v7, :cond_12

    const-string v7, "t:"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    iget-object v7, v14, Lnsh;->f:Lnsa;

    iget-object v7, v7, Lnsa;->b:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v16

    if-eqz v16, :cond_11

    invoke-virtual {v15, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :goto_5
    aput-object v7, v13, v5

    :cond_c
    :goto_6
    if-ltz v4, :cond_d

    iget-object v7, v14, Lnsh;->f:Lnsa;

    if-eqz v7, :cond_14

    iget-object v7, v14, Lnsh;->f:Lnsa;

    iget-object v7, v7, Lnsa;->c:Ljava/lang/String;

    aput-object v7, v13, v4

    :cond_d
    :goto_7
    if-ltz v3, :cond_e

    iget-object v7, v14, Lnsh;->c:Ljava/lang/String;

    invoke-static {v7}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v13, v3

    :cond_e
    if-ltz v2, :cond_f

    iget v7, v14, Lnsh;->e:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v13, v2

    :cond_f
    invoke-virtual {v12, v13}, Lhym;->a([Ljava/lang/Object;)V

    add-int/lit8 v7, v9, 0x1

    move v9, v7

    goto :goto_4

    :cond_10
    const/4 v1, 0x0

    goto :goto_3

    :cond_11
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v15}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_12
    const-string v7, "g:"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    iget-object v7, v14, Lnsh;->b:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v16

    if-eqz v16, :cond_13

    invoke-virtual {v15, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :goto_8
    aput-object v7, v13, v5

    goto :goto_6

    :cond_13
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v15}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    :cond_14
    iget-object v7, v14, Lnsh;->d:Ljava/lang/String;

    aput-object v7, v13, v4

    goto :goto_7

    .line 84
    :cond_15
    new-instance v1, Lkxl;

    move-object/from16 v0, p0

    iget-object v2, v0, Lkvk;->e:Ljava/lang/String;

    iget-object v3, v10, Lnry;->d:Ljava/lang/String;

    invoke-direct {v1, v12, v2, v3}, Lkxl;-><init>(Lhym;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
