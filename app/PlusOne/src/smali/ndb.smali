.class public final Lndb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnde;

.field public b:Lndf;

.field public c:[Lndd;

.field public d:I

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 757
    iput-object v0, p0, Lndb;->a:Lnde;

    .line 760
    iput-object v0, p0, Lndb;->b:Lndf;

    .line 763
    sget-object v0, Lndd;->a:[Lndd;

    iput-object v0, p0, Lndb;->c:[Lndd;

    .line 766
    const/high16 v0, -0x80000000

    iput v0, p0, Lndb;->d:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 798
    .line 799
    iget-object v0, p0, Lndb;->a:Lnde;

    if-eqz v0, :cond_5

    .line 800
    const/4 v0, 0x1

    iget-object v2, p0, Lndb;->a:Lnde;

    .line 801
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 803
    :goto_0
    iget-object v2, p0, Lndb;->b:Lndf;

    if-eqz v2, :cond_0

    .line 804
    const/4 v2, 0x2

    iget-object v3, p0, Lndb;->b:Lndf;

    .line 805
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 807
    :cond_0
    iget-object v2, p0, Lndb;->c:[Lndd;

    if-eqz v2, :cond_2

    .line 808
    iget-object v2, p0, Lndb;->c:[Lndd;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 809
    if-eqz v4, :cond_1

    .line 810
    const/4 v5, 0x3

    .line 811
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 808
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 815
    :cond_2
    iget v1, p0, Lndb;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 816
    const/4 v1, 0x4

    iget v2, p0, Lndb;->d:I

    .line 817
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 819
    :cond_3
    iget-object v1, p0, Lndb;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 820
    const/4 v1, 0x5

    iget-object v2, p0, Lndb;->e:Ljava/lang/Integer;

    .line 821
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 823
    :cond_4
    iget-object v1, p0, Lndb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 824
    iput v0, p0, Lndb;->ai:I

    .line 825
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lndb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 833
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 834
    sparse-switch v0, :sswitch_data_0

    .line 838
    iget-object v2, p0, Lndb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 839
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lndb;->ah:Ljava/util/List;

    .line 842
    :cond_1
    iget-object v2, p0, Lndb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 844
    :sswitch_0
    return-object p0

    .line 849
    :sswitch_1
    iget-object v0, p0, Lndb;->a:Lnde;

    if-nez v0, :cond_2

    .line 850
    new-instance v0, Lnde;

    invoke-direct {v0}, Lnde;-><init>()V

    iput-object v0, p0, Lndb;->a:Lnde;

    .line 852
    :cond_2
    iget-object v0, p0, Lndb;->a:Lnde;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 856
    :sswitch_2
    iget-object v0, p0, Lndb;->b:Lndf;

    if-nez v0, :cond_3

    .line 857
    new-instance v0, Lndf;

    invoke-direct {v0}, Lndf;-><init>()V

    iput-object v0, p0, Lndb;->b:Lndf;

    .line 859
    :cond_3
    iget-object v0, p0, Lndb;->b:Lndf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 863
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 864
    iget-object v0, p0, Lndb;->c:[Lndd;

    if-nez v0, :cond_5

    move v0, v1

    .line 865
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lndd;

    .line 866
    iget-object v3, p0, Lndb;->c:[Lndd;

    if-eqz v3, :cond_4

    .line 867
    iget-object v3, p0, Lndb;->c:[Lndd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 869
    :cond_4
    iput-object v2, p0, Lndb;->c:[Lndd;

    .line 870
    :goto_2
    iget-object v2, p0, Lndb;->c:[Lndd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 871
    iget-object v2, p0, Lndb;->c:[Lndd;

    new-instance v3, Lndd;

    invoke-direct {v3}, Lndd;-><init>()V

    aput-object v3, v2, v0

    .line 872
    iget-object v2, p0, Lndb;->c:[Lndd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 873
    invoke-virtual {p1}, Loxn;->a()I

    .line 870
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 864
    :cond_5
    iget-object v0, p0, Lndb;->c:[Lndd;

    array-length v0, v0

    goto :goto_1

    .line 876
    :cond_6
    iget-object v2, p0, Lndb;->c:[Lndd;

    new-instance v3, Lndd;

    invoke-direct {v3}, Lndd;-><init>()V

    aput-object v3, v2, v0

    .line 877
    iget-object v2, p0, Lndb;->c:[Lndd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 881
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 882
    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    const/4 v2, 0x4

    if-ne v0, v2, :cond_8

    .line 887
    :cond_7
    iput v0, p0, Lndb;->d:I

    goto/16 :goto_0

    .line 889
    :cond_8
    iput v1, p0, Lndb;->d:I

    goto/16 :goto_0

    .line 894
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lndb;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 834
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 773
    iget-object v0, p0, Lndb;->a:Lnde;

    if-eqz v0, :cond_0

    .line 774
    const/4 v0, 0x1

    iget-object v1, p0, Lndb;->a:Lnde;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 776
    :cond_0
    iget-object v0, p0, Lndb;->b:Lndf;

    if-eqz v0, :cond_1

    .line 777
    const/4 v0, 0x2

    iget-object v1, p0, Lndb;->b:Lndf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 779
    :cond_1
    iget-object v0, p0, Lndb;->c:[Lndd;

    if-eqz v0, :cond_3

    .line 780
    iget-object v1, p0, Lndb;->c:[Lndd;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 781
    if-eqz v3, :cond_2

    .line 782
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 780
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 786
    :cond_3
    iget v0, p0, Lndb;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 787
    const/4 v0, 0x4

    iget v1, p0, Lndb;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 789
    :cond_4
    iget-object v0, p0, Lndb;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 790
    const/4 v0, 0x5

    iget-object v1, p0, Lndb;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 792
    :cond_5
    iget-object v0, p0, Lndb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 794
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lndb;->a(Loxn;)Lndb;

    move-result-object v0

    return-object v0
.end method
