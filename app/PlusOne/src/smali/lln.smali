.class public Llln;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llpv;
.implements Llpw;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Lhee;

.field private c:Los;

.field private d:Lkex;

.field private e:Z

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lllm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Llln;->e:Z

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llln;->f:Ljava/util/ArrayList;

    .line 86
    iput-object p1, p0, Llln;->a:Landroid/app/Activity;

    .line 87
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 88
    return-void
.end method

.method public constructor <init>(Los;Llqr;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Llln;-><init>(Landroid/app/Activity;Llqr;)V

    .line 92
    iput-object p1, p0, Llln;->c:Los;

    .line 93
    return-void
.end method

.method private a(Landroid/app/Activity;)Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 200
    invoke-virtual {p1}, Landroid/app/Activity;->getParentActivityIntent()Landroid/content/Intent;

    move-result-object v0

    .line 201
    if-nez v0, :cond_0

    iget-object v1, p0, Llln;->d:Lkex;

    if-eqz v1, :cond_0

    .line 202
    iget-object v1, p0, Llln;->d:Lkex;

    iget-object v0, p0, Llln;->b:Lhee;

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    invoke-interface {v1, p1, v0}, Lkex;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 206
    :cond_0
    if-eqz v0, :cond_2

    .line 207
    invoke-virtual {p1, v0}, Landroid/app/Activity;->shouldUpRecreateTask(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 208
    invoke-static {p1}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v0

    .line 209
    invoke-virtual {p1, v0}, Landroid/app/Activity;->onCreateNavigateUpTaskStack(Landroid/app/TaskStackBuilder;)V

    .line 210
    invoke-virtual {p1, v0}, Landroid/app/Activity;->onPrepareNavigateUpTaskStack(Landroid/app/TaskStackBuilder;)V

    .line 211
    invoke-virtual {v0}, Landroid/app/TaskStackBuilder;->startActivities()V

    .line 214
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->finishAffinity()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :goto_1
    const/4 v0, 0x1

    .line 221
    :goto_2
    return v0

    .line 202
    :cond_1
    iget-object v0, p0, Llln;->b:Lhee;

    .line 203
    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 221
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(Los;)Z
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p1}, Los;->a()Landroid/content/Intent;

    move-result-object v0

    .line 169
    if-nez v0, :cond_0

    iget-object v1, p0, Llln;->d:Lkex;

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Llln;->d:Lkex;

    iget-object v0, p0, Llln;->b:Lhee;

    if-nez v0, :cond_2

    const/4 v0, -0x1

    :goto_0
    invoke-interface {v1, p1, v0}, Lkex;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 174
    :cond_0
    if-eqz v0, :cond_3

    .line 175
    invoke-virtual {p1, v0}, Los;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 176
    invoke-static {p1}, Lda;->a(Landroid/content/Context;)Lda;

    move-result-object v1

    .line 177
    invoke-virtual {p1, v1}, Los;->a(Lda;)V

    .line 178
    invoke-virtual {p1, v1}, Los;->b(Lda;)V

    .line 181
    invoke-virtual {v1}, Lda;->a()I

    move-result v2

    if-nez v2, :cond_1

    .line 182
    invoke-virtual {v1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 185
    :cond_1
    invoke-virtual {v1}, Lda;->b()V

    .line 188
    :try_start_0
    invoke-static {p1}, Lh;->a(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :goto_1
    const/4 v0, 0x1

    .line 195
    :goto_2
    return v0

    .line 170
    :cond_2
    iget-object v0, p0, Llln;->b:Lhee;

    .line 171
    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    goto :goto_0

    .line 190
    :catch_0
    move-exception v0

    invoke-virtual {p1}, Los;->finish()V

    goto :goto_1

    .line 195
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public a(Lllm;)Llln;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Llln;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "UpNavigationHandler already on stack."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_0
    iget-object v0, p0, Llln;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    return-object p0
.end method

.method public a(Llnh;)Llln;
    .locals 1

    .prologue
    .line 96
    const-class v0, Llln;

    invoke-virtual {p1, v0, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 97
    return-object p0
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Llln;->a:Landroid/app/Activity;

    const-class v1, Lkex;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkex;

    iput-object v0, p0, Llln;->d:Lkex;

    .line 108
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Llln;->b:Lhee;

    .line 109
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 140
    iget-object v0, p0, Llln;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 141
    iget-object v0, p0, Llln;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllm;

    .line 142
    invoke-interface {v0}, Lllm;->a()Z

    move-result v0

    .line 143
    if-eqz v0, :cond_1

    .line 164
    :cond_0
    :goto_1
    return v2

    .line 140
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, p0, Llln;->a:Landroid/app/Activity;

    invoke-static {v0}, Lllk;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Llln;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_1

    .line 153
    :cond_3
    iget-object v0, p0, Llln;->c:Los;

    if-eqz v0, :cond_5

    .line 154
    iget-object v0, p0, Llln;->c:Los;

    invoke-direct {p0, v0}, Llln;->a(Los;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    :cond_4
    iget-object v0, p0, Llln;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_1

    .line 157
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_4

    .line 158
    iget-object v0, p0, Llln;->a:Landroid/app/Activity;

    invoke-direct {p0, v0}, Llln;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_1
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 130
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 131
    invoke-virtual {p0}, Llln;->a()Z

    move-result v0

    .line 133
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lllm;)Llln;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Llln;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 243
    return-object p0
.end method

.method public b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Llln;->c:Los;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Llln;->c:Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 116
    iget-boolean v1, p0, Llln;->e:Z

    invoke-virtual {v0, v1}, Loo;->c(Z)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 120
    iget-object v0, p0, Llln;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_0

    .line 122
    iget-boolean v1, p0, Llln;->e:Z

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0
.end method
