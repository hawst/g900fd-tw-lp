.class public final Lpjl;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1356
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1392
    const/4 v0, 0x0

    .line 1393
    iget-object v1, p0, Lpjl;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1394
    const/4 v0, 0x1

    iget-object v1, p0, Lpjl;->a:Ljava/lang/String;

    .line 1395
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1397
    :cond_0
    iget-object v1, p0, Lpjl;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1398
    const/4 v1, 0x2

    iget-object v2, p0, Lpjl;->b:Ljava/lang/String;

    .line 1399
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1401
    :cond_1
    iget-object v1, p0, Lpjl;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1402
    const/4 v1, 0x3

    iget-object v2, p0, Lpjl;->c:Ljava/lang/String;

    .line 1403
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1405
    :cond_2
    iget-object v1, p0, Lpjl;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1406
    const/4 v1, 0x4

    iget-object v2, p0, Lpjl;->d:Ljava/lang/String;

    .line 1407
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1409
    :cond_3
    iget-object v1, p0, Lpjl;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1410
    const/4 v1, 0x5

    iget-object v2, p0, Lpjl;->e:Ljava/lang/String;

    .line 1411
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1413
    :cond_4
    iget-object v1, p0, Lpjl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1414
    iput v0, p0, Lpjl;->ai:I

    .line 1415
    return v0
.end method

.method public a(Loxn;)Lpjl;
    .locals 2

    .prologue
    .line 1423
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1424
    sparse-switch v0, :sswitch_data_0

    .line 1428
    iget-object v1, p0, Lpjl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1429
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjl;->ah:Ljava/util/List;

    .line 1432
    :cond_1
    iget-object v1, p0, Lpjl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1434
    :sswitch_0
    return-object p0

    .line 1439
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjl;->a:Ljava/lang/String;

    goto :goto_0

    .line 1443
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjl;->b:Ljava/lang/String;

    goto :goto_0

    .line 1447
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjl;->c:Ljava/lang/String;

    goto :goto_0

    .line 1451
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjl;->d:Ljava/lang/String;

    goto :goto_0

    .line 1455
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjl;->e:Ljava/lang/String;

    goto :goto_0

    .line 1424
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1371
    iget-object v0, p0, Lpjl;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1372
    const/4 v0, 0x1

    iget-object v1, p0, Lpjl;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1374
    :cond_0
    iget-object v0, p0, Lpjl;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1375
    const/4 v0, 0x2

    iget-object v1, p0, Lpjl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1377
    :cond_1
    iget-object v0, p0, Lpjl;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1378
    const/4 v0, 0x3

    iget-object v1, p0, Lpjl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1380
    :cond_2
    iget-object v0, p0, Lpjl;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1381
    const/4 v0, 0x4

    iget-object v1, p0, Lpjl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1383
    :cond_3
    iget-object v0, p0, Lpjl;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1384
    const/4 v0, 0x5

    iget-object v1, p0, Lpjl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1386
    :cond_4
    iget-object v0, p0, Lpjl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1388
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1352
    invoke-virtual {p0, p1}, Lpjl;->a(Loxn;)Lpjl;

    move-result-object v0

    return-object v0
.end method
