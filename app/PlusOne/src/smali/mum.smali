.class public final Lmum;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11072
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11108
    const/4 v0, 0x0

    .line 11109
    iget-object v1, p0, Lmum;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11110
    const/4 v0, 0x1

    iget-object v1, p0, Lmum;->a:Ljava/lang/String;

    .line 11111
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11113
    :cond_0
    iget-object v1, p0, Lmum;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 11114
    const/4 v1, 0x2

    iget-object v2, p0, Lmum;->b:Ljava/lang/Integer;

    .line 11115
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11117
    :cond_1
    iget-object v1, p0, Lmum;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 11118
    const/4 v1, 0x3

    iget-object v2, p0, Lmum;->c:Ljava/lang/Boolean;

    .line 11119
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11121
    :cond_2
    iget-object v1, p0, Lmum;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 11122
    const/4 v1, 0x4

    iget-object v2, p0, Lmum;->d:Ljava/lang/Boolean;

    .line 11123
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11125
    :cond_3
    iget-object v1, p0, Lmum;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 11126
    const/4 v1, 0x5

    iget-object v2, p0, Lmum;->e:Ljava/lang/Integer;

    .line 11127
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11129
    :cond_4
    iget-object v1, p0, Lmum;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11130
    iput v0, p0, Lmum;->ai:I

    .line 11131
    return v0
.end method

.method public a(Loxn;)Lmum;
    .locals 2

    .prologue
    .line 11139
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11140
    sparse-switch v0, :sswitch_data_0

    .line 11144
    iget-object v1, p0, Lmum;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmum;->ah:Ljava/util/List;

    .line 11148
    :cond_1
    iget-object v1, p0, Lmum;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11150
    :sswitch_0
    return-object p0

    .line 11155
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmum;->a:Ljava/lang/String;

    goto :goto_0

    .line 11159
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmum;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 11163
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmum;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 11167
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmum;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 11171
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmum;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 11140
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11087
    iget-object v0, p0, Lmum;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 11088
    const/4 v0, 0x1

    iget-object v1, p0, Lmum;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 11090
    :cond_0
    iget-object v0, p0, Lmum;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 11091
    const/4 v0, 0x2

    iget-object v1, p0, Lmum;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11093
    :cond_1
    iget-object v0, p0, Lmum;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 11094
    const/4 v0, 0x3

    iget-object v1, p0, Lmum;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 11096
    :cond_2
    iget-object v0, p0, Lmum;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 11097
    const/4 v0, 0x4

    iget-object v1, p0, Lmum;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 11099
    :cond_3
    iget-object v0, p0, Lmum;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 11100
    const/4 v0, 0x5

    iget-object v1, p0, Lmum;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11102
    :cond_4
    iget-object v0, p0, Lmum;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11104
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11068
    invoke-virtual {p0, p1}, Lmum;->a(Loxn;)Lmum;

    move-result-object v0

    return-object v0
.end method
