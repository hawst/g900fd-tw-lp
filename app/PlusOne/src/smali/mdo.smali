.class public final Lmdo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlf;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1375
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1378
    iput-object v0, p0, Lmdo;->apiHeader:Llyq;

    .line 1381
    iput-object v0, p0, Lmdo;->a:Lnlf;

    .line 1375
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1398
    const/4 v0, 0x0

    .line 1399
    iget-object v1, p0, Lmdo;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 1400
    const/4 v0, 0x1

    iget-object v1, p0, Lmdo;->apiHeader:Llyq;

    .line 1401
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1403
    :cond_0
    iget-object v1, p0, Lmdo;->a:Lnlf;

    if-eqz v1, :cond_1

    .line 1404
    const/4 v1, 0x2

    iget-object v2, p0, Lmdo;->a:Lnlf;

    .line 1405
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1407
    :cond_1
    iget-object v1, p0, Lmdo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1408
    iput v0, p0, Lmdo;->ai:I

    .line 1409
    return v0
.end method

.method public a(Loxn;)Lmdo;
    .locals 2

    .prologue
    .line 1417
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1418
    sparse-switch v0, :sswitch_data_0

    .line 1422
    iget-object v1, p0, Lmdo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1423
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdo;->ah:Ljava/util/List;

    .line 1426
    :cond_1
    iget-object v1, p0, Lmdo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1428
    :sswitch_0
    return-object p0

    .line 1433
    :sswitch_1
    iget-object v0, p0, Lmdo;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 1434
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmdo;->apiHeader:Llyq;

    .line 1436
    :cond_2
    iget-object v0, p0, Lmdo;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1440
    :sswitch_2
    iget-object v0, p0, Lmdo;->a:Lnlf;

    if-nez v0, :cond_3

    .line 1441
    new-instance v0, Lnlf;

    invoke-direct {v0}, Lnlf;-><init>()V

    iput-object v0, p0, Lmdo;->a:Lnlf;

    .line 1443
    :cond_3
    iget-object v0, p0, Lmdo;->a:Lnlf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1418
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1386
    iget-object v0, p0, Lmdo;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 1387
    const/4 v0, 0x1

    iget-object v1, p0, Lmdo;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1389
    :cond_0
    iget-object v0, p0, Lmdo;->a:Lnlf;

    if-eqz v0, :cond_1

    .line 1390
    const/4 v0, 0x2

    iget-object v1, p0, Lmdo;->a:Lnlf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1392
    :cond_1
    iget-object v0, p0, Lmdo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1394
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1371
    invoke-virtual {p0, p1}, Lmdo;->a(Loxn;)Lmdo;

    move-result-object v0

    return-object v0
.end method
