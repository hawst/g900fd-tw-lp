.class public final Lgai;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/apps/plus/views/PhotoView;

.field private b:F

.field private c:F

.field private d:J

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/PhotoView;)V
    .locals 2

    .prologue
    .line 1880
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgai;->d:J

    .line 1882
    iput-object p1, p0, Lgai;->a:Lcom/google/android/apps/plus/views/PhotoView;

    .line 1883
    return-void
.end method

.method public static synthetic a(Lgai;)Z
    .locals 1

    .prologue
    .line 1866
    iget-boolean v0, p0, Lgai;->e:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1905
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgai;->e:Z

    .line 1906
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgai;->f:Z

    .line 1907
    return-void
.end method

.method public a(FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1889
    iget-boolean v2, p0, Lgai;->e:Z

    if-eqz v2, :cond_0

    .line 1898
    :goto_0
    return v0

    .line 1892
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lgai;->d:J

    .line 1893
    iput p1, p0, Lgai;->b:F

    .line 1894
    iput p2, p0, Lgai;->c:F

    .line 1895
    iput-boolean v0, p0, Lgai;->f:Z

    .line 1896
    iput-boolean v1, p0, Lgai;->e:Z

    .line 1897
    iget-object v0, p0, Lgai;->a:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    .line 1898
    goto :goto_0
.end method

.method public run()V
    .locals 9

    .prologue
    const/high16 v8, 0x447a0000    # 1000.0f

    const/4 v1, 0x0

    .line 1912
    iget-boolean v0, p0, Lgai;->f:Z

    if-eqz v0, :cond_1

    .line 1956
    :cond_0
    :goto_0
    return-void

    .line 1917
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1918
    iget-wide v4, p0, Lgai;->d:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    iget-wide v4, p0, Lgai;->d:J

    sub-long v4, v2, v4

    long-to-float v0, v4

    div-float/2addr v0, v8

    .line 1919
    :goto_1
    iget-object v4, p0, Lgai;->a:Lcom/google/android/apps/plus/views/PhotoView;

    const/4 v5, 0x0

    iget v6, p0, Lgai;->b:F

    mul-float/2addr v6, v0

    iget v7, p0, Lgai;->c:F

    mul-float/2addr v7, v0

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/PhotoView;->a(ZFF)Z

    .line 1920
    iput-wide v2, p0, Lgai;->d:J

    .line 1922
    mul-float/2addr v0, v8

    .line 1923
    iget v2, p0, Lgai;->b:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_6

    .line 1924
    iget v2, p0, Lgai;->b:F

    sub-float/2addr v2, v0

    iput v2, p0, Lgai;->b:F

    .line 1925
    iget v2, p0, Lgai;->b:F

    cmpg-float v2, v2, v1

    if-gez v2, :cond_2

    .line 1926
    iput v1, p0, Lgai;->b:F

    .line 1934
    :cond_2
    :goto_2
    iget v2, p0, Lgai;->c:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_7

    .line 1935
    iget v2, p0, Lgai;->c:F

    sub-float v0, v2, v0

    iput v0, p0, Lgai;->c:F

    .line 1936
    iget v0, p0, Lgai;->c:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 1937
    iput v1, p0, Lgai;->c:F

    .line 1947
    :cond_3
    :goto_3
    iget v0, p0, Lgai;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget v0, p0, Lgai;->c:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    .line 1948
    invoke-virtual {p0}, Lgai;->a()V

    .line 1952
    :cond_4
    iget-boolean v0, p0, Lgai;->f:Z

    if-nez v0, :cond_0

    .line 1955
    iget-object v0, p0, Lgai;->a:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1918
    goto :goto_1

    .line 1929
    :cond_6
    iget v2, p0, Lgai;->b:F

    add-float/2addr v2, v0

    iput v2, p0, Lgai;->b:F

    .line 1930
    iget v2, p0, Lgai;->b:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_2

    .line 1931
    iput v1, p0, Lgai;->b:F

    goto :goto_2

    .line 1940
    :cond_7
    iget v2, p0, Lgai;->c:F

    add-float/2addr v0, v2

    iput v0, p0, Lgai;->c:F

    .line 1941
    iget v0, p0, Lgai;->c:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 1942
    iput v1, p0, Lgai;->c:F

    goto :goto_3
.end method
