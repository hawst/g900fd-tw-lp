.class public final Lhqk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lhqk;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:J

.field public c:J

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lhqk;)I
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 821
    iget-wide v4, p0, Lhqk;->c:J

    iget-wide v6, p1, Lhqk;->c:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 822
    iget-wide v4, p1, Lhqk;->b:J

    iget-wide v6, p0, Lhqk;->b:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 829
    :cond_0
    :goto_0
    return v0

    .line 825
    :cond_1
    iget-wide v4, p1, Lhqk;->b:J

    iget-wide v6, p0, Lhqk;->b:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 828
    :cond_3
    iget-wide v4, p1, Lhqk;->c:J

    iget-wide v6, p0, Lhqk;->c:J

    sub-long/2addr v4, v6

    .line 829
    cmp-long v3, v4, v8

    if-eqz v3, :cond_0

    cmp-long v0, v4, v8

    if-lez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 812
    check-cast p1, Lhqk;

    invoke-virtual {p0, p1}, Lhqk;->a(Lhqk;)I

    move-result v0

    return v0
.end method
