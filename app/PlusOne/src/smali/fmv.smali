.class public final Lfmv;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;I[B)V
    .locals 1

    .prologue
    .line 25
    const-string v0, "UpdateSourceBackgroundTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lfmv;->a:Landroid/content/Context;

    .line 27
    iput p2, p0, Lfmv;->b:I

    .line 28
    iput-object p3, p0, Lfmv;->c:[B

    .line 29
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 5

    .prologue
    .line 33
    :try_start_0
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iget-object v1, p0, Lfmv;->c:[B

    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lock;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    new-instance v2, Ldof;

    iget-object v1, p0, Lfmv;->a:Landroid/content/Context;

    iget v3, p0, Lfmv;->b:I

    invoke-direct {v2, v1, v3, v0}, Ldof;-><init>(Landroid/content/Context;ILock;)V

    .line 41
    invoke-virtual {v2}, Ldof;->l()V

    .line 42
    new-instance v1, Lhoz;

    iget v3, v2, Lkff;->i:I

    iget-object v4, v2, Lkff;->k:Ljava/lang/Exception;

    .line 43
    invoke-virtual {v2}, Ldof;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfmv;->f()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0a06a7

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v3, v4, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    .line 44
    :goto_1
    return-object v0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    const-string v1, "UpdateSourceBackgroundTask"

    const-string v2, "Failed to parse binary proto data"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 38
    new-instance v0, Lhoz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lhoz;-><init>(Z)V

    goto :goto_1

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lfmv;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a057b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
