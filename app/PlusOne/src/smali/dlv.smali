.class public final Ldlv;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmfw;",
        "Lmfx;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;J)V
    .locals 6

    .prologue
    .line 34
    const-string v3, "notificationsreportabuse"

    new-instance v4, Lmfw;

    invoke-direct {v4}, Lmfw;-><init>()V

    new-instance v5, Lmfx;

    invoke-direct {v5}, Lmfx;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 37
    iput-object p3, p0, Ldlv;->a:Ljava/lang/String;

    .line 38
    iput-wide p4, p0, Ldlv;->b:J

    .line 39
    return-void
.end method


# virtual methods
.method protected a(Lmfw;)V
    .locals 6

    .prologue
    .line 43
    new-instance v0, Lmyj;

    invoke-direct {v0}, Lmyj;-><init>()V

    .line 46
    new-instance v1, Lmyk;

    invoke-direct {v1}, Lmyk;-><init>()V

    .line 47
    const-string v2, "android_gplus"

    iput-object v2, v1, Lmyk;->a:Ljava/lang/String;

    .line 48
    iput-object v1, v0, Lmyj;->a:Lmyk;

    .line 50
    new-instance v1, Lmyi;

    invoke-direct {v1}, Lmyi;-><init>()V

    .line 53
    new-instance v2, Llwr;

    invoke-direct {v2}, Llwr;-><init>()V

    .line 54
    const/4 v3, 0x1

    iput v3, v2, Llwr;->a:I

    .line 55
    const/4 v3, 0x0

    iput v3, v2, Llwr;->c:I

    .line 56
    iput-object v2, v1, Lmyi;->b:Llwr;

    .line 58
    new-instance v2, Lluv;

    invoke-direct {v2}, Lluv;-><init>()V

    .line 59
    iget-object v3, p0, Ldlv;->a:Ljava/lang/String;

    iput-object v3, v2, Lluv;->a:Ljava/lang/String;

    .line 60
    iget-wide v4, p0, Ldlv;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lluv;->b:Ljava/lang/Long;

    .line 61
    iput-object v2, v1, Lmyi;->a:Lluv;

    .line 63
    iput-object v1, v0, Lmyj;->b:Lmyi;

    .line 65
    iput-object v0, p1, Lmfw;->a:Lmyj;

    .line 66
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmfw;

    invoke-virtual {p0, p1}, Ldlv;->a(Lmfw;)V

    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 20
    return-void
.end method
