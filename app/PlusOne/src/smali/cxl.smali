.class public Lcxl;
.super Llol;
.source "PG"

# interfaces
.implements Ldde;


# instance fields
.field private N:Lddd;

.field private O:Lcxo;

.field private P:Ldgi;

.field private Q:Ldeo;

.field private R:J

.field private S:Lctz;

.field private final T:Lddk;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 38
    invoke-direct {p0}, Llol;-><init>()V

    .line 73
    new-instance v0, Lddk;

    invoke-direct {v0}, Lddk;-><init>()V

    iput-object v0, p0, Lcxl;->T:Lddk;

    .line 76
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcxl;->av:Llqm;

    sget-object v2, Ldgr;->a:Ldgu;

    new-instance v3, Lcxm;

    invoke-direct {v3, p0}, Lcxm;-><init>(Lcxl;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 256
    return-void
.end method

.method static synthetic a(Lcxl;)V
    .locals 6

    .prologue
    .line 38
    iget-object v0, p0, Lcxl;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->I()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcxl;->N:Lddd;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcxl;->N:Lddd;

    iget-boolean v1, v1, Ldct;->O:Z

    if-nez v1, :cond_0

    invoke-interface {v0}, Lddl;->o()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lddl;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcxl;->N:Lddd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcxl;->N:Lddd;

    iget-boolean v0, v0, Ldct;->O:Z

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcxl;->Q:Ldeo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldeo;->a(Z)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcxl;->R:J

    iget-object v0, p0, Lcxl;->N:Lddd;

    invoke-virtual {v0}, Lddd;->d()V

    iget-object v0, p0, Lcxl;->P:Ldgi;

    sget-object v1, Lhmv;->ek:Lhmv;

    invoke-interface {v0, v1}, Ldgi;->a(Lhmv;)V

    invoke-direct {p0}, Lcxl;->d()V

    goto :goto_0
.end method

.method static synthetic b(Lcxl;)Llnl;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcxl;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Lcxl;)Ldeo;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcxl;->Q:Ldeo;

    return-object v0
.end method

.method static synthetic d(Lcxl;)J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcxl;->R:J

    return-wide v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcxl;->O:Lcxo;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcxl;->O:Lcxo;

    invoke-virtual {p0}, Lcxl;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcxo;->a(Z)V

    .line 238
    :cond_0
    return-void
.end method

.method static synthetic e(Lcxl;)Lddk;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcxl;->T:Lddk;

    return-object v0
.end method

.method static synthetic f(Lcxl;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcxl;->d()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 148
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 150
    if-eqz p1, :cond_2

    const-string v0, "media_proxy"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "media_proxy"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lddl;

    :goto_0
    sget-object v3, Lcxn;->a:[I

    invoke-virtual {p0}, Lcxl;->k()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v4}, Lcxp;->a(Landroid/os/Bundle;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_1
    invoke-virtual {v1, v0}, Ldds;->a(Lddl;)Ldds;

    move-result-object v0

    invoke-virtual {p0}, Lcxl;->n()Lz;

    move-result-object v1

    invoke-virtual {p0}, Lcxl;->k()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ldds;->a(Landroid/content/Context;Landroid/os/Bundle;)Ldds;

    move-result-object v0

    iget-object v1, p0, Lcxl;->S:Lctz;

    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldds;->a(Ljcn;)Ldds;

    move-result-object v0

    invoke-virtual {v0, v2}, Ldds;->d(Z)Ldds;

    move-result-object v0

    invoke-virtual {v0}, Ldds;->a()Lddl;

    move-result-object v0

    iget-object v1, p0, Lcxl;->at:Llnl;

    invoke-interface {v0, v1}, Lddl;->a(Landroid/content/Context;)V

    iget-object v1, p0, Lcxl;->Q:Ldeo;

    invoke-virtual {v1, v0}, Ldeo;->a(Lddl;)V

    .line 152
    invoke-virtual {p0}, Lcxl;->q()Lae;

    move-result-object v0

    const-string v1, "load_fragment"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lddd;

    iput-object v0, p0, Lcxl;->N:Lddd;

    iget-object v0, p0, Lcxl;->N:Lddd;

    if-eqz v0, :cond_1

    move v0, v2

    :goto_2
    if-nez v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcxl;->k()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcxp;->a(Landroid/os/Bundle;)I

    move-result v0

    sget-object v1, Lcxn;->a:[I

    add-int/lit8 v0, v0, -0x1

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    :goto_3
    iget-object v0, p0, Lcxl;->N:Lddd;

    invoke-virtual {v0, v2}, Lddd;->d(Z)V

    iget-object v0, p0, Lcxl;->N:Lddd;

    invoke-virtual {p0}, Lcxl;->k()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lddd;->f(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcxl;->q()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    iget-object v1, p0, Lcxl;->N:Lddd;

    const-string v2, "load_fragment"

    invoke-virtual {v0, v1, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    invoke-virtual {v0}, Lat;->b()I

    .line 155
    :cond_0
    return-void

    .line 150
    :pswitch_0
    new-instance v1, Lddw;

    invoke-direct {v1}, Lddw;-><init>()V

    goto :goto_1

    :pswitch_1
    new-instance v1, Lddz;

    invoke-direct {v1}, Lddz;-><init>()V

    goto :goto_1

    :pswitch_2
    new-instance v1, Lddp;

    invoke-direct {v1}, Lddp;-><init>()V

    goto/16 :goto_1

    .line 152
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 153
    :pswitch_3
    new-instance v0, Lddf;

    invoke-direct {v0}, Lddf;-><init>()V

    iput-object v0, p0, Lcxl;->N:Lddd;

    goto :goto_3

    :pswitch_4
    new-instance v0, Ldcw;

    invoke-direct {v0}, Ldcw;-><init>()V

    iput-object v0, p0, Lcxl;->N:Lddd;

    goto :goto_3

    :pswitch_5
    new-instance v0, Ldda;

    invoke-direct {v0}, Ldda;-><init>()V

    iput-object v0, p0, Lcxl;->N:Lddd;

    goto :goto_3

    :cond_2
    move-object v0, v1

    goto/16 :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    .line 153
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lcxo;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcxl;->O:Lcxo;

    .line 231
    invoke-direct {p0}, Lcxl;->d()V

    .line 232
    return-void
.end method

.method public a(Lddd;)V
    .locals 1

    .prologue
    .line 212
    new-instance v0, Lcxq;

    invoke-direct {v0, p0}, Lcxq;-><init>(Lcxl;)V

    iput-object v0, p1, Ldct;->N:Ldcu;

    .line 213
    iget-object v0, p0, Lcxl;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-virtual {p1, v0}, Lddd;->a(Lddl;)V

    .line 214
    iput-object p1, p0, Lcxl;->N:Lddd;

    .line 215
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcxl;->N:Lddd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxl;->N:Lddd;

    iget-boolean v0, v0, Ldct;->O:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 88
    iget-object v0, p0, Lcxl;->au:Llnh;

    const-class v1, Ldde;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 89
    iget-object v0, p0, Lcxl;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Lcxl;->P:Ldgi;

    .line 90
    iget-object v0, p0, Lcxl;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcxl;->Q:Ldeo;

    .line 91
    iget-object v0, p0, Lcxl;->au:Llnh;

    const-class v1, Lctz;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcxl;->S:Lctz;

    .line 92
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 141
    iget-object v0, p0, Lcxl;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "media_proxy"

    iget-object v1, p0, Lcxl;->Q:Ldeo;

    invoke-virtual {v1}, Ldeo;->a()Lddl;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 144
    :cond_0
    return-void
.end method
