.class public final Llef;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Landroid/text/StaticLayout;

.field private c:I

.field private d:Z

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IZI)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 29
    sget-object v0, Llef;->a:Llct;

    if-nez v0, :cond_0

    .line 30
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Llef;->a:Llct;

    .line 33
    :cond_0
    iput p2, p0, Llef;->c:I

    .line 34
    iput-boolean p3, p0, Llef;->d:Z

    .line 35
    iput p4, p0, Llef;->e:I

    .line 36
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Llef;->b:Landroid/text/StaticLayout;

    .line 68
    iput v1, p0, Llef;->c:I

    .line 69
    iput-boolean v1, p0, Llef;->d:Z

    .line 70
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 57
    iget-boolean v0, p0, Llef;->d:Z

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0}, Llef;->getHeight()I

    move-result v0

    iget-object v1, p0, Llef;->b:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 59
    sget-object v1, Llef;->a:Llct;

    iget v1, v1, Llct;->m:I

    int-to-float v1, v1

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 60
    iget-object v1, p0, Llef;->b:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 61
    sget-object v1, Llef;->a:Llct;

    iget v1, v1, Llct;->m:I

    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 63
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    .line 40
    invoke-virtual {p0}, Llef;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 42
    iget-boolean v1, p0, Llef;->d:Z

    if-eqz v1, :cond_0

    .line 43
    const/16 v1, 0x10

    invoke-static {v0, v1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    .line 44
    iget v2, p0, Llef;->e:I

    .line 45
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 46
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sget-object v3, Llef;->a:Llct;

    iget v3, v3, Llct;->m:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    .line 47
    invoke-static {v1}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    iget v4, p0, Llef;->c:I

    if-le v0, v4, :cond_1

    const/4 v0, 0x1

    .line 44
    :goto_0
    invoke-static {v1, v2, v3, v0}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Llef;->b:Landroid/text/StaticLayout;

    .line 50
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Llef;->c:I

    invoke-virtual {p0, v0, v1}, Llef;->setMeasuredDimension(II)V

    .line 51
    return-void

    .line 47
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
