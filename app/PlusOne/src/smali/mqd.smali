.class public final Lmqd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmqd;


# instance fields
.field public b:[Lmqh;

.field public c:Lmqf;

.field public d:Lmqe;

.field public e:Lmqi;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:I

.field private i:Lmqg;

.field private j:I

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    new-array v0, v0, [Lmqd;

    sput-object v0, Lmqd;->a:[Lmqd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 213
    invoke-direct {p0}, Loxq;-><init>()V

    .line 699
    sget-object v0, Lmqh;->a:[Lmqh;

    iput-object v0, p0, Lmqd;->b:[Lmqh;

    .line 702
    iput-object v1, p0, Lmqd;->c:Lmqf;

    .line 705
    iput-object v1, p0, Lmqd;->d:Lmqe;

    .line 708
    iput-object v1, p0, Lmqd;->e:Lmqi;

    .line 711
    iput-object v1, p0, Lmqd;->i:Lmqg;

    .line 718
    iput v2, p0, Lmqd;->h:I

    .line 721
    iput v2, p0, Lmqd;->j:I

    .line 213
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v6, -0x80000000

    .line 773
    .line 774
    iget-object v1, p0, Lmqd;->b:[Lmqh;

    if-eqz v1, :cond_1

    .line 775
    iget-object v2, p0, Lmqd;->b:[Lmqh;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 776
    if-eqz v4, :cond_0

    .line 777
    const/4 v5, 0x1

    .line 778
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 775
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 782
    :cond_1
    iget-object v1, p0, Lmqd;->c:Lmqf;

    if-eqz v1, :cond_2

    .line 783
    const/4 v1, 0x2

    iget-object v2, p0, Lmqd;->c:Lmqf;

    .line 784
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 786
    :cond_2
    iget-object v1, p0, Lmqd;->d:Lmqe;

    if-eqz v1, :cond_3

    .line 787
    const/4 v1, 0x3

    iget-object v2, p0, Lmqd;->d:Lmqe;

    .line 788
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 790
    :cond_3
    iget-object v1, p0, Lmqd;->e:Lmqi;

    if-eqz v1, :cond_4

    .line 791
    const/4 v1, 0x4

    iget-object v2, p0, Lmqd;->e:Lmqi;

    .line 792
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 794
    :cond_4
    iget-object v1, p0, Lmqd;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 795
    const/4 v1, 0x5

    iget-object v2, p0, Lmqd;->f:Ljava/lang/Boolean;

    .line 796
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 798
    :cond_5
    iget-object v1, p0, Lmqd;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 799
    const/4 v1, 0x6

    iget-object v2, p0, Lmqd;->g:Ljava/lang/Boolean;

    .line 800
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 802
    :cond_6
    iget v1, p0, Lmqd;->h:I

    if-eq v1, v6, :cond_7

    .line 803
    const/4 v1, 0x7

    iget v2, p0, Lmqd;->h:I

    .line 804
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 806
    :cond_7
    iget v1, p0, Lmqd;->j:I

    if-eq v1, v6, :cond_8

    .line 807
    const/16 v1, 0x8

    iget v2, p0, Lmqd;->j:I

    .line 808
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 810
    :cond_8
    iget-object v1, p0, Lmqd;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 811
    const/16 v1, 0x9

    iget-object v2, p0, Lmqd;->k:Ljava/lang/Boolean;

    .line 812
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 814
    :cond_9
    iget-object v1, p0, Lmqd;->i:Lmqg;

    if-eqz v1, :cond_a

    .line 815
    const/16 v1, 0xa

    iget-object v2, p0, Lmqd;->i:Lmqg;

    .line 816
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 818
    :cond_a
    iget-object v1, p0, Lmqd;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 819
    const/16 v1, 0xb

    iget-object v2, p0, Lmqd;->l:Ljava/lang/Boolean;

    .line 820
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 822
    :cond_b
    iget-object v1, p0, Lmqd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 823
    iput v0, p0, Lmqd;->ai:I

    .line 824
    return v0
.end method

.method public a(Loxn;)Lmqd;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 832
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 833
    sparse-switch v0, :sswitch_data_0

    .line 837
    iget-object v2, p0, Lmqd;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 838
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmqd;->ah:Ljava/util/List;

    .line 841
    :cond_1
    iget-object v2, p0, Lmqd;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 843
    :sswitch_0
    return-object p0

    .line 848
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 849
    iget-object v0, p0, Lmqd;->b:[Lmqh;

    if-nez v0, :cond_3

    move v0, v1

    .line 850
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmqh;

    .line 851
    iget-object v3, p0, Lmqd;->b:[Lmqh;

    if-eqz v3, :cond_2

    .line 852
    iget-object v3, p0, Lmqd;->b:[Lmqh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 854
    :cond_2
    iput-object v2, p0, Lmqd;->b:[Lmqh;

    .line 855
    :goto_2
    iget-object v2, p0, Lmqd;->b:[Lmqh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 856
    iget-object v2, p0, Lmqd;->b:[Lmqh;

    new-instance v3, Lmqh;

    invoke-direct {v3}, Lmqh;-><init>()V

    aput-object v3, v2, v0

    .line 857
    iget-object v2, p0, Lmqd;->b:[Lmqh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 858
    invoke-virtual {p1}, Loxn;->a()I

    .line 855
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 849
    :cond_3
    iget-object v0, p0, Lmqd;->b:[Lmqh;

    array-length v0, v0

    goto :goto_1

    .line 861
    :cond_4
    iget-object v2, p0, Lmqd;->b:[Lmqh;

    new-instance v3, Lmqh;

    invoke-direct {v3}, Lmqh;-><init>()V

    aput-object v3, v2, v0

    .line 862
    iget-object v2, p0, Lmqd;->b:[Lmqh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 866
    :sswitch_2
    iget-object v0, p0, Lmqd;->c:Lmqf;

    if-nez v0, :cond_5

    .line 867
    new-instance v0, Lmqf;

    invoke-direct {v0}, Lmqf;-><init>()V

    iput-object v0, p0, Lmqd;->c:Lmqf;

    .line 869
    :cond_5
    iget-object v0, p0, Lmqd;->c:Lmqf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 873
    :sswitch_3
    iget-object v0, p0, Lmqd;->d:Lmqe;

    if-nez v0, :cond_6

    .line 874
    new-instance v0, Lmqe;

    invoke-direct {v0}, Lmqe;-><init>()V

    iput-object v0, p0, Lmqd;->d:Lmqe;

    .line 876
    :cond_6
    iget-object v0, p0, Lmqd;->d:Lmqe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 880
    :sswitch_4
    iget-object v0, p0, Lmqd;->e:Lmqi;

    if-nez v0, :cond_7

    .line 881
    new-instance v0, Lmqi;

    invoke-direct {v0}, Lmqi;-><init>()V

    iput-object v0, p0, Lmqd;->e:Lmqi;

    .line 883
    :cond_7
    iget-object v0, p0, Lmqd;->e:Lmqi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 887
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqd;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 891
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqd;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 895
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 896
    if-eq v0, v4, :cond_8

    if-ne v0, v5, :cond_9

    .line 898
    :cond_8
    iput v0, p0, Lmqd;->h:I

    goto/16 :goto_0

    .line 900
    :cond_9
    iput v4, p0, Lmqd;->h:I

    goto/16 :goto_0

    .line 905
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 906
    if-eq v0, v4, :cond_a

    if-ne v0, v5, :cond_b

    .line 908
    :cond_a
    iput v0, p0, Lmqd;->j:I

    goto/16 :goto_0

    .line 910
    :cond_b
    iput v4, p0, Lmqd;->j:I

    goto/16 :goto_0

    .line 915
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqd;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 919
    :sswitch_a
    iget-object v0, p0, Lmqd;->i:Lmqg;

    if-nez v0, :cond_c

    .line 920
    new-instance v0, Lmqg;

    invoke-direct {v0}, Lmqg;-><init>()V

    iput-object v0, p0, Lmqd;->i:Lmqg;

    .line 922
    :cond_c
    iget-object v0, p0, Lmqd;->i:Lmqg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 926
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqd;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 833
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 730
    iget-object v0, p0, Lmqd;->b:[Lmqh;

    if-eqz v0, :cond_1

    .line 731
    iget-object v1, p0, Lmqd;->b:[Lmqh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 732
    if-eqz v3, :cond_0

    .line 733
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 731
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 737
    :cond_1
    iget-object v0, p0, Lmqd;->c:Lmqf;

    if-eqz v0, :cond_2

    .line 738
    const/4 v0, 0x2

    iget-object v1, p0, Lmqd;->c:Lmqf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 740
    :cond_2
    iget-object v0, p0, Lmqd;->d:Lmqe;

    if-eqz v0, :cond_3

    .line 741
    const/4 v0, 0x3

    iget-object v1, p0, Lmqd;->d:Lmqe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 743
    :cond_3
    iget-object v0, p0, Lmqd;->e:Lmqi;

    if-eqz v0, :cond_4

    .line 744
    const/4 v0, 0x4

    iget-object v1, p0, Lmqd;->e:Lmqi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 746
    :cond_4
    iget-object v0, p0, Lmqd;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 747
    const/4 v0, 0x5

    iget-object v1, p0, Lmqd;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 749
    :cond_5
    iget-object v0, p0, Lmqd;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 750
    const/4 v0, 0x6

    iget-object v1, p0, Lmqd;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 752
    :cond_6
    iget v0, p0, Lmqd;->h:I

    if-eq v0, v5, :cond_7

    .line 753
    const/4 v0, 0x7

    iget v1, p0, Lmqd;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 755
    :cond_7
    iget v0, p0, Lmqd;->j:I

    if-eq v0, v5, :cond_8

    .line 756
    const/16 v0, 0x8

    iget v1, p0, Lmqd;->j:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 758
    :cond_8
    iget-object v0, p0, Lmqd;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 759
    const/16 v0, 0x9

    iget-object v1, p0, Lmqd;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 761
    :cond_9
    iget-object v0, p0, Lmqd;->i:Lmqg;

    if-eqz v0, :cond_a

    .line 762
    const/16 v0, 0xa

    iget-object v1, p0, Lmqd;->i:Lmqg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 764
    :cond_a
    iget-object v0, p0, Lmqd;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 765
    const/16 v0, 0xb

    iget-object v1, p0, Lmqd;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 767
    :cond_b
    iget-object v0, p0, Lmqd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 769
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0, p1}, Lmqd;->a(Loxn;)Lmqd;

    move-result-object v0

    return-object v0
.end method
