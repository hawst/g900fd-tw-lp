.class public final Llyo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lndy;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21787
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21790
    iput-object v0, p0, Llyo;->apiHeader:Llyq;

    .line 21793
    iput-object v0, p0, Llyo;->a:Lndy;

    .line 21787
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 21810
    const/4 v0, 0x0

    .line 21811
    iget-object v1, p0, Llyo;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 21812
    const/4 v0, 0x1

    iget-object v1, p0, Llyo;->apiHeader:Llyq;

    .line 21813
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21815
    :cond_0
    iget-object v1, p0, Llyo;->a:Lndy;

    if-eqz v1, :cond_1

    .line 21816
    const/4 v1, 0x2

    iget-object v2, p0, Llyo;->a:Lndy;

    .line 21817
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21819
    :cond_1
    iget-object v1, p0, Llyo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21820
    iput v0, p0, Llyo;->ai:I

    .line 21821
    return v0
.end method

.method public a(Loxn;)Llyo;
    .locals 2

    .prologue
    .line 21829
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 21830
    sparse-switch v0, :sswitch_data_0

    .line 21834
    iget-object v1, p0, Llyo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 21835
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyo;->ah:Ljava/util/List;

    .line 21838
    :cond_1
    iget-object v1, p0, Llyo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 21840
    :sswitch_0
    return-object p0

    .line 21845
    :sswitch_1
    iget-object v0, p0, Llyo;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 21846
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llyo;->apiHeader:Llyq;

    .line 21848
    :cond_2
    iget-object v0, p0, Llyo;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21852
    :sswitch_2
    iget-object v0, p0, Llyo;->a:Lndy;

    if-nez v0, :cond_3

    .line 21853
    new-instance v0, Lndy;

    invoke-direct {v0}, Lndy;-><init>()V

    iput-object v0, p0, Llyo;->a:Lndy;

    .line 21855
    :cond_3
    iget-object v0, p0, Llyo;->a:Lndy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 21830
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 21798
    iget-object v0, p0, Llyo;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 21799
    const/4 v0, 0x1

    iget-object v1, p0, Llyo;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21801
    :cond_0
    iget-object v0, p0, Llyo;->a:Lndy;

    if-eqz v0, :cond_1

    .line 21802
    const/4 v0, 0x2

    iget-object v1, p0, Llyo;->a:Lndy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 21804
    :cond_1
    iget-object v0, p0, Llyo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 21806
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 21783
    invoke-virtual {p0, p1}, Llyo;->a(Loxn;)Llyo;

    move-result-object v0

    return-object v0
.end method
