.class public final Lmyf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyk;

.field public b:Lmye;

.field public c:Lmyb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1125
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1128
    iput-object v0, p0, Lmyf;->a:Lmyk;

    .line 1131
    iput-object v0, p0, Lmyf;->b:Lmye;

    .line 1134
    iput-object v0, p0, Lmyf;->c:Lmyb;

    .line 1125
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1154
    const/4 v0, 0x0

    .line 1155
    iget-object v1, p0, Lmyf;->a:Lmyk;

    if-eqz v1, :cond_0

    .line 1156
    const/4 v0, 0x1

    iget-object v1, p0, Lmyf;->a:Lmyk;

    .line 1157
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1159
    :cond_0
    iget-object v1, p0, Lmyf;->b:Lmye;

    if-eqz v1, :cond_1

    .line 1160
    const/4 v1, 0x2

    iget-object v2, p0, Lmyf;->b:Lmye;

    .line 1161
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1163
    :cond_1
    iget-object v1, p0, Lmyf;->c:Lmyb;

    if-eqz v1, :cond_2

    .line 1164
    const/4 v1, 0x3

    iget-object v2, p0, Lmyf;->c:Lmyb;

    .line 1165
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1167
    :cond_2
    iget-object v1, p0, Lmyf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1168
    iput v0, p0, Lmyf;->ai:I

    .line 1169
    return v0
.end method

.method public a(Loxn;)Lmyf;
    .locals 2

    .prologue
    .line 1177
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1178
    sparse-switch v0, :sswitch_data_0

    .line 1182
    iget-object v1, p0, Lmyf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1183
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyf;->ah:Ljava/util/List;

    .line 1186
    :cond_1
    iget-object v1, p0, Lmyf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1188
    :sswitch_0
    return-object p0

    .line 1193
    :sswitch_1
    iget-object v0, p0, Lmyf;->a:Lmyk;

    if-nez v0, :cond_2

    .line 1194
    new-instance v0, Lmyk;

    invoke-direct {v0}, Lmyk;-><init>()V

    iput-object v0, p0, Lmyf;->a:Lmyk;

    .line 1196
    :cond_2
    iget-object v0, p0, Lmyf;->a:Lmyk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1200
    :sswitch_2
    iget-object v0, p0, Lmyf;->b:Lmye;

    if-nez v0, :cond_3

    .line 1201
    new-instance v0, Lmye;

    invoke-direct {v0}, Lmye;-><init>()V

    iput-object v0, p0, Lmyf;->b:Lmye;

    .line 1203
    :cond_3
    iget-object v0, p0, Lmyf;->b:Lmye;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1207
    :sswitch_3
    iget-object v0, p0, Lmyf;->c:Lmyb;

    if-nez v0, :cond_4

    .line 1208
    new-instance v0, Lmyb;

    invoke-direct {v0}, Lmyb;-><init>()V

    iput-object v0, p0, Lmyf;->c:Lmyb;

    .line 1210
    :cond_4
    iget-object v0, p0, Lmyf;->c:Lmyb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1178
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1139
    iget-object v0, p0, Lmyf;->a:Lmyk;

    if-eqz v0, :cond_0

    .line 1140
    const/4 v0, 0x1

    iget-object v1, p0, Lmyf;->a:Lmyk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1142
    :cond_0
    iget-object v0, p0, Lmyf;->b:Lmye;

    if-eqz v0, :cond_1

    .line 1143
    const/4 v0, 0x2

    iget-object v1, p0, Lmyf;->b:Lmye;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1145
    :cond_1
    iget-object v0, p0, Lmyf;->c:Lmyb;

    if-eqz v0, :cond_2

    .line 1146
    const/4 v0, 0x3

    iget-object v1, p0, Lmyf;->c:Lmyb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1148
    :cond_2
    iget-object v0, p0, Lmyf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1150
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1121
    invoke-virtual {p0, p1}, Lmyf;->a(Loxn;)Lmyf;

    move-result-object v0

    return-object v0
.end method
