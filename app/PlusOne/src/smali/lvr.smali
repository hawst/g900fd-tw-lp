.class public final Llvr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llvr;


# instance fields
.field private b:Llvd;

.field private c:Lluy;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Llvr;

    sput-object v0, Llvr;->a:[Llvr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12
    iput-object v0, p0, Llvr;->b:Llvd;

    .line 15
    iput-object v0, p0, Llvr;->c:Lluy;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Llvr;->b:Llvd;

    if-eqz v1, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-object v1, p0, Llvr;->b:Llvd;

    .line 40
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42
    :cond_0
    iget-object v1, p0, Llvr;->c:Lluy;

    if-eqz v1, :cond_1

    .line 43
    const/4 v1, 0x2

    iget-object v2, p0, Llvr;->c:Lluy;

    .line 44
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_1
    iget-object v1, p0, Llvr;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 47
    const/4 v1, 0x3

    iget-object v2, p0, Llvr;->d:Ljava/lang/String;

    .line 48
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    :cond_2
    iget-object v1, p0, Llvr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    iput v0, p0, Llvr;->ai:I

    .line 52
    return v0
.end method

.method public a(Loxn;)Llvr;
    .locals 2

    .prologue
    .line 60
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 61
    sparse-switch v0, :sswitch_data_0

    .line 65
    iget-object v1, p0, Llvr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llvr;->ah:Ljava/util/List;

    .line 69
    :cond_1
    iget-object v1, p0, Llvr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    :sswitch_0
    return-object p0

    .line 76
    :sswitch_1
    iget-object v0, p0, Llvr;->b:Llvd;

    if-nez v0, :cond_2

    .line 77
    new-instance v0, Llvd;

    invoke-direct {v0}, Llvd;-><init>()V

    iput-object v0, p0, Llvr;->b:Llvd;

    .line 79
    :cond_2
    iget-object v0, p0, Llvr;->b:Llvd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 83
    :sswitch_2
    iget-object v0, p0, Llvr;->c:Lluy;

    if-nez v0, :cond_3

    .line 84
    new-instance v0, Lluy;

    invoke-direct {v0}, Lluy;-><init>()V

    iput-object v0, p0, Llvr;->c:Lluy;

    .line 86
    :cond_3
    iget-object v0, p0, Llvr;->c:Lluy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 90
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llvr;->d:Ljava/lang/String;

    goto :goto_0

    .line 61
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Llvr;->b:Llvd;

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x1

    iget-object v1, p0, Llvr;->b:Llvd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25
    :cond_0
    iget-object v0, p0, Llvr;->c:Lluy;

    if-eqz v0, :cond_1

    .line 26
    const/4 v0, 0x2

    iget-object v1, p0, Llvr;->c:Lluy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28
    :cond_1
    iget-object v0, p0, Llvr;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 29
    const/4 v0, 0x3

    iget-object v1, p0, Llvr;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 31
    :cond_2
    iget-object v0, p0, Llvr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llvr;->a(Loxn;)Llvr;

    move-result-object v0

    return-object v0
.end method
