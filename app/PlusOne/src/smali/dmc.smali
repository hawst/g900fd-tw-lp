.class public final Ldmc;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmgk;",
        "Lmgl;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;JLjava/lang/String;)V
    .locals 6

    .prologue
    .line 25
    const-string v3, "photoseditcaption"

    new-instance v4, Lmgk;

    invoke-direct {v4}, Lmgk;-><init>()V

    new-instance v5, Lmgl;

    invoke-direct {v5}, Lmgl;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 27
    iput-object p3, p0, Ldmc;->a:Ljava/lang/String;

    .line 28
    iput-wide p4, p0, Ldmc;->b:J

    .line 29
    iput-object p6, p0, Ldmc;->c:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method protected a(Lmgk;)V
    .locals 4

    .prologue
    .line 33
    new-instance v0, Lnbe;

    invoke-direct {v0}, Lnbe;-><init>()V

    iput-object v0, p1, Lmgk;->a:Lnbe;

    .line 34
    iget-object v0, p1, Lmgk;->a:Lnbe;

    .line 35
    iget-object v1, p0, Ldmc;->a:Ljava/lang/String;

    iput-object v1, v0, Lnbe;->c:Ljava/lang/String;

    .line 36
    iget-wide v2, p0, Ldmc;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lnbe;->b:Ljava/lang/Long;

    .line 37
    iget-object v1, p0, Ldmc;->c:Ljava/lang/String;

    iput-object v1, v0, Lnbe;->a:Ljava/lang/String;

    .line 38
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lmgk;

    invoke-virtual {p0, p1}, Ldmc;->a(Lmgk;)V

    return-void
.end method
