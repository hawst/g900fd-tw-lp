.class public final Lovv;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:[Lovw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 73
    const/high16 v0, -0x80000000

    iput v0, p0, Lovv;->a:I

    .line 76
    sget-object v0, Lovw;->a:[Lovw;

    iput-object v0, p0, Lovv;->b:[Lovw;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 97
    .line 98
    iget v0, p0, Lovv;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_2

    .line 99
    const/4 v0, 0x1

    iget v2, p0, Lovv;->a:I

    .line 100
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 102
    :goto_0
    iget-object v2, p0, Lovv;->b:[Lovw;

    if-eqz v2, :cond_1

    .line 103
    iget-object v2, p0, Lovv;->b:[Lovw;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 104
    if-eqz v4, :cond_0

    .line 105
    const/4 v5, 0x2

    .line 106
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 103
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 110
    :cond_1
    iget-object v1, p0, Lovv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    iput v0, p0, Lovv;->ai:I

    .line 112
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lovv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 120
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 121
    sparse-switch v0, :sswitch_data_0

    .line 125
    iget-object v2, p0, Lovv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 126
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lovv;->ah:Ljava/util/List;

    .line 129
    :cond_1
    iget-object v2, p0, Lovv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    :sswitch_0
    return-object p0

    .line 136
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 137
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 139
    :cond_2
    iput v0, p0, Lovv;->a:I

    goto :goto_0

    .line 141
    :cond_3
    iput v1, p0, Lovv;->a:I

    goto :goto_0

    .line 146
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 147
    iget-object v0, p0, Lovv;->b:[Lovw;

    if-nez v0, :cond_5

    move v0, v1

    .line 148
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lovw;

    .line 149
    iget-object v3, p0, Lovv;->b:[Lovw;

    if-eqz v3, :cond_4

    .line 150
    iget-object v3, p0, Lovv;->b:[Lovw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 152
    :cond_4
    iput-object v2, p0, Lovv;->b:[Lovw;

    .line 153
    :goto_2
    iget-object v2, p0, Lovv;->b:[Lovw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 154
    iget-object v2, p0, Lovv;->b:[Lovw;

    new-instance v3, Lovw;

    invoke-direct {v3}, Lovw;-><init>()V

    aput-object v3, v2, v0

    .line 155
    iget-object v2, p0, Lovv;->b:[Lovw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 156
    invoke-virtual {p1}, Loxn;->a()I

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 147
    :cond_5
    iget-object v0, p0, Lovv;->b:[Lovw;

    array-length v0, v0

    goto :goto_1

    .line 159
    :cond_6
    iget-object v2, p0, Lovv;->b:[Lovw;

    new-instance v3, Lovw;

    invoke-direct {v3}, Lovw;-><init>()V

    aput-object v3, v2, v0

    .line 160
    iget-object v2, p0, Lovv;->b:[Lovw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 121
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 81
    iget v0, p0, Lovv;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 82
    const/4 v0, 0x1

    iget v1, p0, Lovv;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 84
    :cond_0
    iget-object v0, p0, Lovv;->b:[Lovw;

    if-eqz v0, :cond_2

    .line 85
    iget-object v1, p0, Lovv;->b:[Lovw;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 86
    if-eqz v3, :cond_1

    .line 87
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 85
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_2
    iget-object v0, p0, Lovv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 93
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lovv;->a(Loxn;)Lovv;

    move-result-object v0

    return-object v0
.end method
