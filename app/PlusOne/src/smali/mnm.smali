.class public final Lmnm;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lmms;

.field private c:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5777
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5789
    const/high16 v0, -0x80000000

    iput v0, p0, Lmnm;->a:I

    .line 5792
    const/4 v0, 0x0

    iput-object v0, p0, Lmnm;->b:Lmms;

    .line 5777
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5814
    const/4 v0, 0x0

    .line 5815
    iget v1, p0, Lmnm;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 5816
    const/4 v0, 0x1

    iget v1, p0, Lmnm;->a:I

    .line 5817
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5819
    :cond_0
    iget-object v1, p0, Lmnm;->b:Lmms;

    if-eqz v1, :cond_1

    .line 5820
    const/4 v1, 0x2

    iget-object v2, p0, Lmnm;->b:Lmms;

    .line 5821
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5823
    :cond_1
    iget-object v1, p0, Lmnm;->c:[B

    if-eqz v1, :cond_2

    .line 5824
    const/4 v1, 0x3

    iget-object v2, p0, Lmnm;->c:[B

    .line 5825
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 5827
    :cond_2
    iget-object v1, p0, Lmnm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5828
    iput v0, p0, Lmnm;->ai:I

    .line 5829
    return v0
.end method

.method public a(Loxn;)Lmnm;
    .locals 2

    .prologue
    .line 5837
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5838
    sparse-switch v0, :sswitch_data_0

    .line 5842
    iget-object v1, p0, Lmnm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5843
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnm;->ah:Ljava/util/List;

    .line 5846
    :cond_1
    iget-object v1, p0, Lmnm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5848
    :sswitch_0
    return-object p0

    .line 5853
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5854
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 5860
    :cond_2
    iput v0, p0, Lmnm;->a:I

    goto :goto_0

    .line 5862
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmnm;->a:I

    goto :goto_0

    .line 5867
    :sswitch_2
    iget-object v0, p0, Lmnm;->b:Lmms;

    if-nez v0, :cond_4

    .line 5868
    new-instance v0, Lmms;

    invoke-direct {v0}, Lmms;-><init>()V

    iput-object v0, p0, Lmnm;->b:Lmms;

    .line 5870
    :cond_4
    iget-object v0, p0, Lmnm;->b:Lmms;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5874
    :sswitch_3
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lmnm;->c:[B

    goto :goto_0

    .line 5838
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5799
    iget v0, p0, Lmnm;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 5800
    const/4 v0, 0x1

    iget v1, p0, Lmnm;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5802
    :cond_0
    iget-object v0, p0, Lmnm;->b:Lmms;

    if-eqz v0, :cond_1

    .line 5803
    const/4 v0, 0x2

    iget-object v1, p0, Lmnm;->b:Lmms;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5805
    :cond_1
    iget-object v0, p0, Lmnm;->c:[B

    if-eqz v0, :cond_2

    .line 5806
    const/4 v0, 0x3

    iget-object v1, p0, Lmnm;->c:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 5808
    :cond_2
    iget-object v0, p0, Lmnm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5810
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5773
    invoke-virtual {p0, p1}, Lmnm;->a(Loxn;)Lmnm;

    move-result-object v0

    return-object v0
.end method
