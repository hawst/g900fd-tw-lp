.class final Ljsv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhii;


# static fields
.field private static a:I


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljpv;

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, -0x1

    sput v0, Ljsv;->a:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljsw;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Ljsv;->b:Landroid/content/Context;

    .line 39
    invoke-virtual {p2}, Ljsw;->b()Ljpv;

    move-result-object v0

    iput-object v0, p0, Ljsv;->c:Ljpv;

    .line 40
    invoke-virtual {p2}, Ljsw;->c()Z

    move-result v0

    iput-boolean v0, p0, Ljsv;->d:Z

    .line 41
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 45
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v1, p0, Ljsv;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    .line 46
    iget-object v1, p0, Ljsv;->c:Ljpv;

    invoke-interface {v1}, Ljpv;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ljsv;->c:Ljpv;

    invoke-interface {v2}, Ljpv;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    .line 48
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 49
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 50
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 51
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Ljsv;->c:Ljpv;

    invoke-interface {v0}, Ljpv;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 2

    .prologue
    .line 61
    sget v0, Ljsv;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 62
    iget-object v0, p0, Ljsv;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b012a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Ljsv;->a:I

    .line 64
    :cond_0
    sget v0, Ljsv;->a:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 69
    const/16 v0, 0xb

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Ljsv;->d:Z

    return v0
.end method
