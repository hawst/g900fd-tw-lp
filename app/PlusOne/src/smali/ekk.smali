.class public final Lekk;
.super Leku;
.source "PG"


# instance fields
.field private N:Leky;

.field private final O:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Leku;-><init>()V

    .line 35
    new-instance v0, Lekl;

    invoke-direct {v0, p0}, Lekl;-><init>(Lekk;)V

    iput-object v0, p0, Lekk;->O:Lbc;

    .line 68
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lhmw;->r:Lhmw;

    return-object v0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x1

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 118
    const/16 v0, 0x42

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lekk;->N:Leky;

    invoke-virtual {p0, v1}, Lekk;->a(Landroid/widget/ListAdapter;)V

    .line 83
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekk;->X:Z

    .line 75
    new-instance v0, Leky;

    invoke-direct {v0, p0}, Leky;-><init>(Leku;)V

    iput-object v0, p0, Lekk;->N:Leky;

    .line 76
    return-void
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 96
    const v0, 0x7f0a09c7

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 97
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lekk;->N:Leky;

    const/4 v1, 0x5

    invoke-virtual {v0, p1, v1}, Leky;->a(Ljava/util/ArrayList;I)V

    .line 89
    iget-object v0, p0, Lekk;->N:Leky;

    invoke-virtual {v0}, Leky;->notifyDataSetChanged()V

    .line 90
    invoke-virtual {p0}, Lekk;->x()Landroid/view/View;

    invoke-virtual {p0}, Lekk;->aa()V

    .line 91
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 123
    const/16 v0, 0x75

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekk;->R:Z

    .line 107
    invoke-virtual {p0}, Lekk;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lekk;->O:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 108
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekk;->R:Z

    .line 113
    invoke-virtual {p0}, Lekk;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lekk;->O:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 114
    return-void
.end method
