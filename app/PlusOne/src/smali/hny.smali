.class public abstract Lhny;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:I

.field g:Lhnz;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x3

    iput v0, p0, Lhny;->h:I

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhny;->a:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lhny;->b:Ljava/lang/String;

    .line 63
    if-nez p2, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BackgroundTask tag cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    return-void
.end method


# virtual methods
.method public V_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(I)Lhny;
    .locals 0

    .prologue
    .line 137
    iput p1, p0, Lhny;->h:I

    .line 138
    return-object p0
.end method

.method public abstract a()Lhoz;
.end method

.method public a(Lhnz;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lhny;->g:Lhnz;

    .line 192
    return-void
.end method

.method public a_(Lhoz;)V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method public b(Ljava/lang/String;)Lhny;
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lhny;->b:Ljava/lang/String;

    .line 98
    return-object p0
.end method

.method public b(Z)Lhny;
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lhny;->d:Z

    .line 106
    return-object p0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    return-object v0
.end method

.method b(I)V
    .locals 0

    .prologue
    .line 183
    iput p1, p0, Lhny;->c:I

    .line 184
    return-void
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lhny;->a:Landroid/content/Context;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lhny;->d:Z

    return v0
.end method

.method public h()Lhny;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhny;->e:Z

    .line 123
    return-object p0
.end method

.method i()V
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lhny;->e:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lhny;->f:I

    if-nez v0, :cond_0

    .line 143
    invoke-virtual {p0}, Lhny;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhou;->a(Landroid/content/Context;)Lhou;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Lhou;->a()I

    move-result v0

    iput v0, p0, Lhny;->f:I

    .line 146
    :cond_0
    return-void
.end method

.method j()V
    .locals 2

    .prologue
    .line 149
    iget v0, p0, Lhny;->f:I

    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p0}, Lhny;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhou;->a(Landroid/content/Context;)Lhou;

    move-result-object v0

    iget v1, p0, Lhny;->f:I

    .line 151
    invoke-virtual {v0, v1}, Lhou;->a(I)V

    .line 153
    :cond_0
    return-void
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lhny;->b:Ljava/lang/String;

    return-object v0
.end method

.method l()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lhny;->c:I

    return v0
.end method

.method final m()Lhoz;
    .locals 4

    .prologue
    .line 195
    invoke-virtual {p0}, Lhny;->a()Lhoz;

    move-result-object v0

    .line 196
    if-nez v0, :cond_0

    .line 197
    new-instance v0, Ljava/lang/AssertionError;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Null result in BackgroundTask: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 199
    :cond_0
    iget v1, p0, Lhny;->h:I

    invoke-virtual {v0, v1}, Lhoz;->a(I)V

    .line 200
    return-object v0
.end method
