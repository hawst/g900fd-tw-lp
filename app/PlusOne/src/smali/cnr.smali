.class public final Lcnr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcnq;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Ljava/lang/String;

.field private final c:I

.field private d:Lctz;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcnr;->a:Landroid/app/Activity;

    .line 31
    if-eqz p3, :cond_0

    :goto_0
    iput-object p3, p0, Lcnr;->b:Ljava/lang/String;

    .line 32
    iput p4, p0, Lcnr;->c:I

    .line 34
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 35
    return-void

    .line 31
    :cond_0
    const-string p3, "android.intent.action.GET_CONTENT"

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lcnr;->b:Ljava/lang/String;

    iget v1, p0, Lcnr;->c:I

    iget-object v2, p0, Lcnr;->d:Lctz;

    .line 45
    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v2

    iget-object v3, p0, Lcnr;->a:Landroid/app/Activity;

    .line 44
    invoke-static {v0, v1, v2, v3}, Leys;->a(Ljava/lang/String;ILjcn;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcnr;->a:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 47
    iget-object v0, p0, Lcnr;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 48
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lctz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcnr;->d:Lctz;

    .line 40
    return-void
.end method
