.class public final Lply;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpgx;

.field public b:[Lppr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 394
    invoke-direct {p0}, Loxq;-><init>()V

    .line 397
    const/4 v0, 0x0

    iput-object v0, p0, Lply;->a:Lpgx;

    .line 400
    sget-object v0, Lppr;->a:[Lppr;

    iput-object v0, p0, Lply;->b:[Lppr;

    .line 394
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 421
    .line 422
    iget-object v0, p0, Lply;->a:Lpgx;

    if-eqz v0, :cond_2

    .line 423
    const/4 v0, 0x1

    iget-object v2, p0, Lply;->a:Lpgx;

    .line 424
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 426
    :goto_0
    iget-object v2, p0, Lply;->b:[Lppr;

    if-eqz v2, :cond_1

    .line 427
    iget-object v2, p0, Lply;->b:[Lppr;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 428
    if-eqz v4, :cond_0

    .line 429
    const/4 v5, 0x2

    .line 430
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 427
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 434
    :cond_1
    iget-object v1, p0, Lply;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    iput v0, p0, Lply;->ai:I

    .line 436
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lply;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 444
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 445
    sparse-switch v0, :sswitch_data_0

    .line 449
    iget-object v2, p0, Lply;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 450
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lply;->ah:Ljava/util/List;

    .line 453
    :cond_1
    iget-object v2, p0, Lply;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 455
    :sswitch_0
    return-object p0

    .line 460
    :sswitch_1
    iget-object v0, p0, Lply;->a:Lpgx;

    if-nez v0, :cond_2

    .line 461
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lply;->a:Lpgx;

    .line 463
    :cond_2
    iget-object v0, p0, Lply;->a:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 467
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 468
    iget-object v0, p0, Lply;->b:[Lppr;

    if-nez v0, :cond_4

    move v0, v1

    .line 469
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lppr;

    .line 470
    iget-object v3, p0, Lply;->b:[Lppr;

    if-eqz v3, :cond_3

    .line 471
    iget-object v3, p0, Lply;->b:[Lppr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 473
    :cond_3
    iput-object v2, p0, Lply;->b:[Lppr;

    .line 474
    :goto_2
    iget-object v2, p0, Lply;->b:[Lppr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 475
    iget-object v2, p0, Lply;->b:[Lppr;

    new-instance v3, Lppr;

    invoke-direct {v3}, Lppr;-><init>()V

    aput-object v3, v2, v0

    .line 476
    iget-object v2, p0, Lply;->b:[Lppr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 477
    invoke-virtual {p1}, Loxn;->a()I

    .line 474
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 468
    :cond_4
    iget-object v0, p0, Lply;->b:[Lppr;

    array-length v0, v0

    goto :goto_1

    .line 480
    :cond_5
    iget-object v2, p0, Lply;->b:[Lppr;

    new-instance v3, Lppr;

    invoke-direct {v3}, Lppr;-><init>()V

    aput-object v3, v2, v0

    .line 481
    iget-object v2, p0, Lply;->b:[Lppr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 445
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 405
    iget-object v0, p0, Lply;->a:Lpgx;

    if-eqz v0, :cond_0

    .line 406
    const/4 v0, 0x1

    iget-object v1, p0, Lply;->a:Lpgx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 408
    :cond_0
    iget-object v0, p0, Lply;->b:[Lppr;

    if-eqz v0, :cond_2

    .line 409
    iget-object v1, p0, Lply;->b:[Lppr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 410
    if-eqz v3, :cond_1

    .line 411
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 409
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 415
    :cond_2
    iget-object v0, p0, Lply;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 417
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 390
    invoke-virtual {p0, p1}, Lply;->a(Loxn;)Lply;

    move-result-object v0

    return-object v0
.end method
