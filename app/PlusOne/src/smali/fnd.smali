.class public final Lfnd;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmjs;",
        "Lmjt;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lnpd;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILnpd;)V
    .locals 6

    .prologue
    .line 35
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v3, "settingsset"

    new-instance v4, Lmjs;

    invoke-direct {v4}, Lmjs;-><init>()V

    new-instance v5, Lmjt;

    invoke-direct {v5}, Lmjt;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 41
    iput-object p3, p0, Lfnd;->a:Lnpd;

    .line 42
    return-void
.end method


# virtual methods
.method protected a(Lmjs;)V
    .locals 3

    .prologue
    .line 46
    new-instance v0, Lnpt;

    invoke-direct {v0}, Lnpt;-><init>()V

    iput-object v0, p1, Lmjs;->a:Lnpt;

    .line 47
    iget-object v0, p1, Lmjs;->a:Lnpt;

    .line 48
    new-instance v1, Lnpf;

    invoke-direct {v1}, Lnpf;-><init>()V

    .line 49
    iget-object v2, p0, Lfnd;->a:Lnpd;

    iput-object v2, v1, Lnpf;->d:Lnpd;

    .line 50
    iput-object v1, v0, Lnpt;->a:Lnpf;

    .line 51
    return-void
.end method

.method protected a(Lmjt;)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p1, Lmjt;->a:Lnpn;

    .line 56
    iget-object v1, v0, Lnpn;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lnpn;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lkfm;

    const-string v1, "SettingsSetRequest failed"

    invoke-direct {v0, v1}, Lkfm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmjs;

    invoke-virtual {p0, p1}, Lfnd;->a(Lmjs;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmjt;

    invoke-virtual {p0, p1}, Lfnd;->a(Lmjt;)V

    return-void
.end method
