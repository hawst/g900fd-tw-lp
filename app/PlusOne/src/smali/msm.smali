.class public final Lmsm;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3502
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3514
    const/high16 v0, -0x80000000

    iput v0, p0, Lmsm;->b:I

    .line 3502
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3531
    const/4 v0, 0x0

    .line 3532
    iget-object v1, p0, Lmsm;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3533
    const/4 v0, 0x1

    iget-object v1, p0, Lmsm;->a:Ljava/lang/String;

    .line 3534
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3536
    :cond_0
    iget v1, p0, Lmsm;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 3537
    const/4 v1, 0x2

    iget v2, p0, Lmsm;->b:I

    .line 3538
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3540
    :cond_1
    iget-object v1, p0, Lmsm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3541
    iput v0, p0, Lmsm;->ai:I

    .line 3542
    return v0
.end method

.method public a(Loxn;)Lmsm;
    .locals 2

    .prologue
    .line 3550
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3551
    sparse-switch v0, :sswitch_data_0

    .line 3555
    iget-object v1, p0, Lmsm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3556
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmsm;->ah:Ljava/util/List;

    .line 3559
    :cond_1
    iget-object v1, p0, Lmsm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3561
    :sswitch_0
    return-object p0

    .line 3566
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmsm;->a:Ljava/lang/String;

    goto :goto_0

    .line 3570
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3571
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 3575
    :cond_2
    iput v0, p0, Lmsm;->b:I

    goto :goto_0

    .line 3577
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmsm;->b:I

    goto :goto_0

    .line 3551
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3519
    iget-object v0, p0, Lmsm;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3520
    const/4 v0, 0x1

    iget-object v1, p0, Lmsm;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3522
    :cond_0
    iget v0, p0, Lmsm;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 3523
    const/4 v0, 0x2

    iget v1, p0, Lmsm;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3525
    :cond_1
    iget-object v0, p0, Lmsm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3527
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3498
    invoke-virtual {p0, p1}, Lmsm;->a(Loxn;)Lmsm;

    move-result-object v0

    return-object v0
.end method
