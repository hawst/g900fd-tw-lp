.class final Lfbt;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lfbs;

.field private final b:Landroid/content/Context;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;ILfbs;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 431
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 432
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfbt;->b:Landroid/content/Context;

    .line 433
    iput p2, p0, Lfbt;->c:I

    .line 434
    iput-object p3, p0, Lfbt;->a:Lfbs;

    .line 435
    iput-object p4, p0, Lfbt;->d:Ljava/lang/String;

    .line 436
    iput p5, p0, Lfbt;->e:I

    .line 437
    iput-object p6, p0, Lfbt;->f:Ljava/lang/String;

    .line 438
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Void;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 442
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 451
    :cond_0
    :goto_0
    return-object v8

    .line 446
    :cond_1
    aget-object v5, p1, v7

    .line 447
    new-instance v0, Ldmz;

    iget-object v1, p0, Lfbt;->b:Landroid/content/Context;

    iget v2, p0, Lfbt;->c:I

    iget-object v3, p0, Lfbt;->d:Ljava/lang/String;

    iget v4, p0, Lfbt;->e:I

    iget-object v6, p0, Lfbt;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Ldmz;-><init>(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 449
    invoke-virtual {v0}, Lkff;->l()V

    goto :goto_0
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lfbt;->a:Lfbs;

    invoke-static {v0}, Lfbs;->a(Lfbs;)V

    .line 457
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 422
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lfbt;->a([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 422
    invoke-virtual {p0}, Lfbt;->a()V

    return-void
.end method
