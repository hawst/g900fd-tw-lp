.class public final Lhrp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhrr;

.field private final c:Lhei;

.field private final d:I

.field private final e:Lhpu;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILhrr;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Llsk;->a(Z)V

    .line 40
    iput-object p1, p0, Lhrp;->a:Landroid/content/Context;

    .line 41
    iput p2, p0, Lhrp;->d:I

    .line 42
    iput-object p3, p0, Lhrp;->b:Lhrr;

    .line 43
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhrp;->c:Lhei;

    .line 44
    const-class v0, Lhpu;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    iput-object v0, p0, Lhrp;->e:Lhpu;

    .line 45
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()J
    .locals 4

    .prologue
    .line 48
    iget-object v0, p0, Lhrp;->c:Lhei;

    iget v1, p0, Lhrp;->d:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.autobackup.VideoReuploader"

    .line 49
    invoke-interface {v0, v1}, Lhej;->d(Ljava/lang/String;)Lhej;

    move-result-object v0

    const-string v1, "window_start_time_seconds"

    const-wide/16 v2, 0x0

    .line 50
    invoke-interface {v0, v1, v2, v3}, Lhej;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JJ)V
    .locals 11

    .prologue
    const-wide/16 v8, 0x3e8

    const-wide/16 v6, 0x0

    .line 72
    invoke-virtual {p0}, Lhrp;->a()J

    move-result-wide v0

    .line 73
    invoke-virtual {p0}, Lhrp;->b()J

    move-result-wide v2

    .line 74
    cmp-long v4, v0, v6

    if-nez v4, :cond_0

    cmp-long v4, v2, v6

    if-eqz v4, :cond_1

    :cond_0
    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    cmp-long v0, p3, v2

    if-eqz v0, :cond_2

    .line 78
    :cond_1
    iget-object v0, p0, Lhrp;->c:Lhei;

    iget v1, p0, Lhrp;->d:I

    invoke-interface {v0, v1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.autobackup.VideoReuploader"

    invoke-interface {v0, v1}, Lhek;->f(Ljava/lang/String;)Lhek;

    move-result-object v0

    const-string v1, "window_start_time_seconds"

    .line 79
    invoke-interface {v0, v1, p1, p2}, Lhek;->b(Ljava/lang/String;J)Lhek;

    move-result-object v0

    const-string v1, "window_end_time_seconds"

    .line 80
    invoke-interface {v0, v1, p3, p4}, Lhek;->b(Ljava/lang/String;J)Lhek;

    move-result-object v0

    .line 81
    invoke-interface {v0}, Lhek;->c()I

    .line 83
    iget-object v0, p0, Lhrp;->e:Lhpu;

    invoke-virtual {v0}, Lhpu;->d()I

    move-result v0

    .line 84
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget v1, p0, Lhrp;->d:I

    if-ne v0, v1, :cond_2

    .line 86
    iget-object v0, p0, Lhrp;->b:Lhrr;

    iget v1, p0, Lhrp;->d:I

    new-instance v2, Lhrc;

    mul-long v4, p1, v8

    mul-long v6, p3, v8

    invoke-direct {v2, v4, v5, v6, v7}, Lhrc;-><init>(JJ)V

    invoke-interface {v0, v1, v2}, Lhrr;->a(ILhrc;)V

    .line 90
    iget-object v0, p0, Lhrp;->c:Lhei;

    iget v1, p0, Lhrp;->d:I

    .line 91
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, v0, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lhrp;->a:Landroid/content/Context;

    invoke-static {v0}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v1, v0, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 97
    :cond_2
    return-void
.end method

.method b()J
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lhrp;->c:Lhei;

    iget v1, p0, Lhrp;->d:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.autobackup.VideoReuploader"

    .line 55
    invoke-interface {v0, v1}, Lhej;->d(Ljava/lang/String;)Lhej;

    move-result-object v0

    const-string v1, "window_end_time_seconds"

    const-wide/16 v2, 0x0

    .line 56
    invoke-interface {v0, v1, v2, v3}, Lhej;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method
