.class public final Lnir;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lnja;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Long;

.field private d:I

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 295
    invoke-direct {p0}, Loxq;-><init>()V

    .line 304
    const/4 v0, 0x0

    iput-object v0, p0, Lnir;->a:Lnja;

    .line 311
    const/high16 v0, -0x80000000

    iput v0, p0, Lnir;->d:I

    .line 295
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 339
    const/4 v0, 0x0

    .line 340
    iget-object v1, p0, Lnir;->a:Lnja;

    if-eqz v1, :cond_0

    .line 341
    const/4 v0, 0x1

    iget-object v1, p0, Lnir;->a:Lnja;

    .line 342
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 344
    :cond_0
    iget-object v1, p0, Lnir;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 345
    const/4 v1, 0x2

    iget-object v2, p0, Lnir;->b:Ljava/lang/Long;

    .line 346
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 348
    :cond_1
    iget-object v1, p0, Lnir;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 349
    const/4 v1, 0x3

    iget-object v2, p0, Lnir;->c:Ljava/lang/Long;

    .line 350
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 352
    :cond_2
    iget v1, p0, Lnir;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 353
    const/4 v1, 0x4

    iget v2, p0, Lnir;->d:I

    .line 354
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 356
    :cond_3
    iget-object v1, p0, Lnir;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 357
    const/4 v1, 0x5

    iget-object v2, p0, Lnir;->e:Ljava/lang/Boolean;

    .line 358
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 360
    :cond_4
    iget-object v1, p0, Lnir;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 361
    iput v0, p0, Lnir;->ai:I

    .line 362
    return v0
.end method

.method public a(Loxn;)Lnir;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 370
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 371
    sparse-switch v0, :sswitch_data_0

    .line 375
    iget-object v1, p0, Lnir;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 376
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnir;->ah:Ljava/util/List;

    .line 379
    :cond_1
    iget-object v1, p0, Lnir;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    :sswitch_0
    return-object p0

    .line 386
    :sswitch_1
    iget-object v0, p0, Lnir;->a:Lnja;

    if-nez v0, :cond_2

    .line 387
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnir;->a:Lnja;

    .line 389
    :cond_2
    iget-object v0, p0, Lnir;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 393
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnir;->b:Ljava/lang/Long;

    goto :goto_0

    .line 397
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnir;->c:Ljava/lang/Long;

    goto :goto_0

    .line 401
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 402
    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 405
    :cond_3
    iput v0, p0, Lnir;->d:I

    goto :goto_0

    .line 407
    :cond_4
    iput v2, p0, Lnir;->d:I

    goto :goto_0

    .line 412
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnir;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 371
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 318
    iget-object v0, p0, Lnir;->a:Lnja;

    if-eqz v0, :cond_0

    .line 319
    const/4 v0, 0x1

    iget-object v1, p0, Lnir;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 321
    :cond_0
    iget-object v0, p0, Lnir;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 322
    const/4 v0, 0x2

    iget-object v1, p0, Lnir;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 324
    :cond_1
    iget-object v0, p0, Lnir;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 325
    const/4 v0, 0x3

    iget-object v1, p0, Lnir;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 327
    :cond_2
    iget v0, p0, Lnir;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 328
    const/4 v0, 0x4

    iget v1, p0, Lnir;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 330
    :cond_3
    iget-object v0, p0, Lnir;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 331
    const/4 v0, 0x5

    iget-object v1, p0, Lnir;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 333
    :cond_4
    iget-object v0, p0, Lnir;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 335
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p0, p1}, Lnir;->a(Loxn;)Lnir;

    move-result-object v0

    return-object v0
.end method
