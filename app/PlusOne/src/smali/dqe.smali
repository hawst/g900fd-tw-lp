.class public final Ldqe;
.super Lhny;
.source "PG"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lhms;

.field private e:Ldsx;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "circle_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "for_sharing"

    aput-object v2, v0, v1

    sput-object v0, Ldqe;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    const-string v0, "ShowProfilePhotoPromptTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 60
    iput-object p3, p0, Ldqe;->b:Ljava/lang/String;

    .line 61
    iput-object p4, p0, Ldqe;->c:Ljava/lang/String;

    .line 62
    iput p2, p0, Ldqe;->f:I

    .line 63
    const-class v0, Lhms;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Ldqe;->d:Lhms;

    .line 64
    return-void
.end method

.method private c(Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 159
    const/4 v9, 0x0

    .line 161
    :try_start_0
    invoke-virtual {p0}, Ldqe;->f()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldqe;->f:I

    const/4 v2, 0x0

    sget-object v3, Ldqe;->a:[Ljava/lang/String;

    const-string v4, "(for_sharing > 0)"

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;IZ)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 163
    :try_start_1
    invoke-virtual {p0, p1}, Ldqe;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 164
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 165
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    if-eqz v1, :cond_1

    .line 171
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v7

    .line 174
    :goto_0
    return v0

    .line 170
    :cond_2
    if-eqz v1, :cond_3

    .line 171
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v8

    .line 174
    goto :goto_0

    .line 170
    :catchall_0
    move-exception v0

    move-object v1, v9

    :goto_1
    if-eqz v1, :cond_4

    .line 171
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 170
    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 68
    invoke-virtual {p0}, Ldqe;->f()Landroid/content/Context;

    move-result-object v9

    .line 69
    iget v0, p0, Ldqe;->f:I

    iget-object v1, p0, Ldqe;->b:Ljava/lang/String;

    invoke-static {v9, v0, v1}, Ldsm;->c(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v8, v6

    .line 74
    :goto_0
    if-eqz v8, :cond_b

    .line 75
    iget v3, p0, Ldqe;->f:I

    const-string v5, "g:"

    iget-object v0, p0, Ldqe;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v9, v3, v0, v6}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Z)Ldsx;

    move-result-object v0

    iput-object v0, p0, Ldqe;->e:Ldsx;

    .line 77
    invoke-direct {p0, v1}, Ldqe;->c(Ljava/lang/String;)Z

    move-result v1

    .line 78
    iget-object v0, p0, Ldqe;->e:Ldsx;

    iget-object v0, v0, Ldsx;->h:Lnjt;

    move v7, v1

    .line 83
    :goto_2
    if-nez v0, :cond_3

    move v5, v6

    .line 84
    :goto_3
    if-nez v0, :cond_5

    move-object v3, v4

    .line 85
    :goto_4
    if-eqz v3, :cond_0

    iget-object v0, v3, Lnkd;->d:Lnip;

    if-nez v0, :cond_6

    :cond_0
    move v1, v2

    .line 87
    :goto_5
    if-eqz v7, :cond_7

    if-eqz v3, :cond_7

    iget-object v0, v3, Lnkd;->a:Lnjb;

    if-eqz v0, :cond_7

    iget-object v0, v3, Lnkd;->a:Lnjb;

    iget-object v0, v0, Lnjb;->a:Ljava/lang/String;

    .line 89
    :goto_6
    iget v3, p0, Ldqe;->f:I

    if-eqz v8, :cond_a

    if-nez v5, :cond_a

    if-eqz v7, :cond_a

    if-ne v1, v6, :cond_8

    const v1, 0x7f0a09dd

    :goto_7
    new-array v5, v6, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {v9, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-class v0, Lhei;

    invoke-static {v9, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v7, "gaia_id"

    invoke-interface {v0, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x4

    invoke-static {v9, v3, v0, v4, v7}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "add_profile_photo_message_id"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lfhu;->a()I

    move-result v1

    invoke-static {v9, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Lbs;

    invoke-direct {v1, v9}, Lbs;-><init>(Landroid/content/Context;)V

    const v2, 0x7f020363

    invoke-virtual {v1, v2}, Lbs;->a(I)Lbs;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020155

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbs;->a(Landroid/graphics/Bitmap;)Lbs;

    invoke-virtual {v1, v6}, Lbs;->b(Z)Lbs;

    const v2, 0x7f0a09db

    invoke-virtual {v9, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    invoke-virtual {v1, v5}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    invoke-virtual {v1, v0}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    const-string v0, "notification"

    invoke-virtual {v9, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ":notifications:addprofilephoto:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f1000a3

    invoke-virtual {v1}, Lbs;->b()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 91
    iget-object v0, p0, Ldqe;->d:Lhms;

    new-instance v1, Lhmr;

    iget v2, p0, Ldqe;->f:I

    invoke-direct {v1, v9, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->L:Lhmv;

    .line 93
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->y:Lhmw;

    .line 94
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 91
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 96
    new-instance v0, Lhoz;

    invoke-direct {v0, v6}, Lhoz;-><init>(Z)V

    return-object v0

    :cond_1
    move v8, v2

    .line 70
    goto/16 :goto_0

    .line 75
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 83
    :cond_3
    iget v1, v0, Lnjt;->b:I

    if-ne v1, v10, :cond_4

    move v5, v6

    goto/16 :goto_3

    :cond_4
    move v5, v2

    goto/16 :goto_3

    .line 84
    :cond_5
    iget-object v0, v0, Lnjt;->e:Lnkd;

    move-object v3, v0

    goto/16 :goto_4

    .line 85
    :cond_6
    iget-object v0, v3, Lnkd;->d:Lnip;

    iget v0, v0, Lnip;->b:I

    .line 86
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    move v1, v0

    goto/16 :goto_5

    .line 87
    :cond_7
    iget-object v0, p0, Ldqe;->c:Ljava/lang/String;

    goto/16 :goto_6

    .line 89
    :cond_8
    if-ne v1, v10, :cond_9

    const v1, 0x7f0a09de

    goto/16 :goto_7

    :cond_9
    const v1, 0x7f0a09dc

    goto/16 :goto_7

    :cond_a
    const v1, 0x7f0a09df

    goto/16 :goto_7

    :cond_b
    move-object v0, v4

    move v7, v2

    goto/16 :goto_2
.end method

.method public a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    const/16 v0, 0x7c

    invoke-static {p1, v0}, Llsu;->a(Ljava/lang/String;C)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
