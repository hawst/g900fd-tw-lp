.class public final Llwc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llwc;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Llwb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    new-array v0, v0, [Llwc;

    sput-object v0, Llwc;->a:[Llwc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Loxq;-><init>()V

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Llwc;->c:Llwb;

    .line 231
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 253
    const/4 v0, 0x0

    .line 254
    iget-object v1, p0, Llwc;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 255
    const/4 v0, 0x1

    iget-object v1, p0, Llwc;->b:Ljava/lang/String;

    .line 256
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 258
    :cond_0
    iget-object v1, p0, Llwc;->c:Llwb;

    if-eqz v1, :cond_1

    .line 259
    const/4 v1, 0x2

    iget-object v2, p0, Llwc;->c:Llwb;

    .line 260
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    :cond_1
    iget-object v1, p0, Llwc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    iput v0, p0, Llwc;->ai:I

    .line 264
    return v0
.end method

.method public a(Loxn;)Llwc;
    .locals 2

    .prologue
    .line 272
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 273
    sparse-switch v0, :sswitch_data_0

    .line 277
    iget-object v1, p0, Llwc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 278
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llwc;->ah:Ljava/util/List;

    .line 281
    :cond_1
    iget-object v1, p0, Llwc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    :sswitch_0
    return-object p0

    .line 288
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llwc;->b:Ljava/lang/String;

    goto :goto_0

    .line 292
    :sswitch_2
    iget-object v0, p0, Llwc;->c:Llwb;

    if-nez v0, :cond_2

    .line 293
    new-instance v0, Llwb;

    invoke-direct {v0}, Llwb;-><init>()V

    iput-object v0, p0, Llwc;->c:Llwb;

    .line 295
    :cond_2
    iget-object v0, p0, Llwc;->c:Llwb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 273
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Llwc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 242
    const/4 v0, 0x1

    iget-object v1, p0, Llwc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 244
    :cond_0
    iget-object v0, p0, Llwc;->c:Llwb;

    if-eqz v0, :cond_1

    .line 245
    const/4 v0, 0x2

    iget-object v1, p0, Llwc;->c:Llwb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 247
    :cond_1
    iget-object v0, p0, Llwc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 249
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0, p1}, Llwc;->a(Loxn;)Llwc;

    move-result-object v0

    return-object v0
.end method
