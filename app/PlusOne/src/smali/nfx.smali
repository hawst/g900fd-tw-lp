.class public final Lnfx;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnzx;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2164
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2167
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lnfx;->a:[Lnzx;

    .line 2164
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2195
    .line 2196
    iget-object v1, p0, Lnfx;->a:[Lnzx;

    if-eqz v1, :cond_1

    .line 2197
    iget-object v2, p0, Lnfx;->a:[Lnzx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2198
    if-eqz v4, :cond_0

    .line 2199
    const/4 v5, 0x1

    .line 2200
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2197
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2204
    :cond_1
    iget-object v1, p0, Lnfx;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2205
    const/4 v1, 0x2

    iget-object v2, p0, Lnfx;->b:Ljava/lang/String;

    .line 2206
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2208
    :cond_2
    iget-object v1, p0, Lnfx;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2209
    const/4 v1, 0x3

    iget-object v2, p0, Lnfx;->c:Ljava/lang/Boolean;

    .line 2210
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2212
    :cond_3
    iget-object v1, p0, Lnfx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2213
    iput v0, p0, Lnfx;->ai:I

    .line 2214
    return v0
.end method

.method public a(Loxn;)Lnfx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2222
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2223
    sparse-switch v0, :sswitch_data_0

    .line 2227
    iget-object v2, p0, Lnfx;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2228
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnfx;->ah:Ljava/util/List;

    .line 2231
    :cond_1
    iget-object v2, p0, Lnfx;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2233
    :sswitch_0
    return-object p0

    .line 2238
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2239
    iget-object v0, p0, Lnfx;->a:[Lnzx;

    if-nez v0, :cond_3

    move v0, v1

    .line 2240
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 2241
    iget-object v3, p0, Lnfx;->a:[Lnzx;

    if-eqz v3, :cond_2

    .line 2242
    iget-object v3, p0, Lnfx;->a:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2244
    :cond_2
    iput-object v2, p0, Lnfx;->a:[Lnzx;

    .line 2245
    :goto_2
    iget-object v2, p0, Lnfx;->a:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2246
    iget-object v2, p0, Lnfx;->a:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 2247
    iget-object v2, p0, Lnfx;->a:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2248
    invoke-virtual {p1}, Loxn;->a()I

    .line 2245
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2239
    :cond_3
    iget-object v0, p0, Lnfx;->a:[Lnzx;

    array-length v0, v0

    goto :goto_1

    .line 2251
    :cond_4
    iget-object v2, p0, Lnfx;->a:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 2252
    iget-object v2, p0, Lnfx;->a:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2256
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfx;->b:Ljava/lang/String;

    goto :goto_0

    .line 2260
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnfx;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 2223
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2176
    iget-object v0, p0, Lnfx;->a:[Lnzx;

    if-eqz v0, :cond_1

    .line 2177
    iget-object v1, p0, Lnfx;->a:[Lnzx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2178
    if-eqz v3, :cond_0

    .line 2179
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2177
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2183
    :cond_1
    iget-object v0, p0, Lnfx;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2184
    const/4 v0, 0x2

    iget-object v1, p0, Lnfx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2186
    :cond_2
    iget-object v0, p0, Lnfx;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2187
    const/4 v0, 0x3

    iget-object v1, p0, Lnfx;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2189
    :cond_3
    iget-object v0, p0, Lnfx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2191
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2160
    invoke-virtual {p0, p1}, Lnfx;->a(Loxn;)Lnfx;

    move-result-object v0

    return-object v0
.end method
