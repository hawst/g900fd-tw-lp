.class public final Lpfy;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpfz;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;

.field private f:Lokn;

.field private g:Lokm;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/Long;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 258
    invoke-direct {p0}, Loxq;-><init>()V

    .line 261
    iput-object v0, p0, Lpfy;->a:Lpfz;

    .line 272
    iput-object v0, p0, Lpfy;->f:Lokn;

    .line 275
    iput-object v0, p0, Lpfy;->g:Lokm;

    .line 258
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 342
    const/4 v0, 0x0

    .line 343
    iget-object v1, p0, Lpfy;->a:Lpfz;

    if-eqz v1, :cond_0

    .line 344
    const/4 v0, 0x1

    iget-object v1, p0, Lpfy;->a:Lpfz;

    .line 345
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 347
    :cond_0
    iget-object v1, p0, Lpfy;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 348
    const/4 v1, 0x2

    iget-object v2, p0, Lpfy;->b:Ljava/lang/String;

    .line 349
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :cond_1
    iget-object v1, p0, Lpfy;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 352
    const/4 v1, 0x3

    iget-object v2, p0, Lpfy;->c:Ljava/lang/String;

    .line 353
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    :cond_2
    iget-object v1, p0, Lpfy;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 356
    const/4 v1, 0x4

    iget-object v2, p0, Lpfy;->d:Ljava/lang/Long;

    .line 357
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_3
    iget-object v1, p0, Lpfy;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 360
    const/4 v1, 0x5

    iget-object v2, p0, Lpfy;->e:Ljava/lang/String;

    .line 361
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_4
    iget-object v1, p0, Lpfy;->f:Lokn;

    if-eqz v1, :cond_5

    .line 364
    const/4 v1, 0x6

    iget-object v2, p0, Lpfy;->f:Lokn;

    .line 365
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_5
    iget-object v1, p0, Lpfy;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 368
    const/4 v1, 0x7

    iget-object v2, p0, Lpfy;->h:Ljava/lang/String;

    .line 369
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_6
    iget-object v1, p0, Lpfy;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 372
    const/16 v1, 0x8

    iget-object v2, p0, Lpfy;->i:Ljava/lang/String;

    .line 373
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_7
    iget-object v1, p0, Lpfy;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 376
    const/16 v1, 0x9

    iget-object v2, p0, Lpfy;->j:Ljava/lang/String;

    .line 377
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_8
    iget-object v1, p0, Lpfy;->k:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 380
    const/16 v1, 0xa

    iget-object v2, p0, Lpfy;->k:Ljava/lang/Long;

    .line 381
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 383
    :cond_9
    iget-object v1, p0, Lpfy;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 384
    const/16 v1, 0xb

    iget-object v2, p0, Lpfy;->l:Ljava/lang/String;

    .line 385
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 387
    :cond_a
    iget-object v1, p0, Lpfy;->g:Lokm;

    if-eqz v1, :cond_b

    .line 388
    const/16 v1, 0xc

    iget-object v2, p0, Lpfy;->g:Lokm;

    .line 389
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 391
    :cond_b
    iget-object v1, p0, Lpfy;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 392
    const/16 v1, 0xd

    iget-object v2, p0, Lpfy;->m:Ljava/lang/String;

    .line 393
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 395
    :cond_c
    iget-object v1, p0, Lpfy;->n:Ljava/lang/Long;

    if-eqz v1, :cond_d

    .line 396
    const/16 v1, 0xe

    iget-object v2, p0, Lpfy;->n:Ljava/lang/Long;

    .line 397
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 399
    :cond_d
    iget-object v1, p0, Lpfy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    iput v0, p0, Lpfy;->ai:I

    .line 401
    return v0
.end method

.method public a(Loxn;)Lpfy;
    .locals 2

    .prologue
    .line 409
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 410
    sparse-switch v0, :sswitch_data_0

    .line 414
    iget-object v1, p0, Lpfy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 415
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpfy;->ah:Ljava/util/List;

    .line 418
    :cond_1
    iget-object v1, p0, Lpfy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 420
    :sswitch_0
    return-object p0

    .line 425
    :sswitch_1
    iget-object v0, p0, Lpfy;->a:Lpfz;

    if-nez v0, :cond_2

    .line 426
    new-instance v0, Lpfz;

    invoke-direct {v0}, Lpfz;-><init>()V

    iput-object v0, p0, Lpfy;->a:Lpfz;

    .line 428
    :cond_2
    iget-object v0, p0, Lpfy;->a:Lpfz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 432
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfy;->b:Ljava/lang/String;

    goto :goto_0

    .line 436
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfy;->c:Ljava/lang/String;

    goto :goto_0

    .line 440
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpfy;->d:Ljava/lang/Long;

    goto :goto_0

    .line 444
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfy;->e:Ljava/lang/String;

    goto :goto_0

    .line 448
    :sswitch_6
    iget-object v0, p0, Lpfy;->f:Lokn;

    if-nez v0, :cond_3

    .line 449
    new-instance v0, Lokn;

    invoke-direct {v0}, Lokn;-><init>()V

    iput-object v0, p0, Lpfy;->f:Lokn;

    .line 451
    :cond_3
    iget-object v0, p0, Lpfy;->f:Lokn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 455
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfy;->h:Ljava/lang/String;

    goto :goto_0

    .line 459
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfy;->i:Ljava/lang/String;

    goto :goto_0

    .line 463
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfy;->j:Ljava/lang/String;

    goto :goto_0

    .line 467
    :sswitch_a
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpfy;->k:Ljava/lang/Long;

    goto :goto_0

    .line 471
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfy;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 475
    :sswitch_c
    iget-object v0, p0, Lpfy;->g:Lokm;

    if-nez v0, :cond_4

    .line 476
    new-instance v0, Lokm;

    invoke-direct {v0}, Lokm;-><init>()V

    iput-object v0, p0, Lpfy;->g:Lokm;

    .line 478
    :cond_4
    iget-object v0, p0, Lpfy;->g:Lokm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 482
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfy;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 486
    :sswitch_e
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpfy;->n:Ljava/lang/Long;

    goto/16 :goto_0

    .line 410
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 294
    iget-object v0, p0, Lpfy;->a:Lpfz;

    if-eqz v0, :cond_0

    .line 295
    const/4 v0, 0x1

    iget-object v1, p0, Lpfy;->a:Lpfz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 297
    :cond_0
    iget-object v0, p0, Lpfy;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 298
    const/4 v0, 0x2

    iget-object v1, p0, Lpfy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 300
    :cond_1
    iget-object v0, p0, Lpfy;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 301
    const/4 v0, 0x3

    iget-object v1, p0, Lpfy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 303
    :cond_2
    iget-object v0, p0, Lpfy;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 304
    const/4 v0, 0x4

    iget-object v1, p0, Lpfy;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 306
    :cond_3
    iget-object v0, p0, Lpfy;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 307
    const/4 v0, 0x5

    iget-object v1, p0, Lpfy;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 309
    :cond_4
    iget-object v0, p0, Lpfy;->f:Lokn;

    if-eqz v0, :cond_5

    .line 310
    const/4 v0, 0x6

    iget-object v1, p0, Lpfy;->f:Lokn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 312
    :cond_5
    iget-object v0, p0, Lpfy;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 313
    const/4 v0, 0x7

    iget-object v1, p0, Lpfy;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 315
    :cond_6
    iget-object v0, p0, Lpfy;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 316
    const/16 v0, 0x8

    iget-object v1, p0, Lpfy;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 318
    :cond_7
    iget-object v0, p0, Lpfy;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 319
    const/16 v0, 0x9

    iget-object v1, p0, Lpfy;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 321
    :cond_8
    iget-object v0, p0, Lpfy;->k:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 322
    const/16 v0, 0xa

    iget-object v1, p0, Lpfy;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 324
    :cond_9
    iget-object v0, p0, Lpfy;->l:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 325
    const/16 v0, 0xb

    iget-object v1, p0, Lpfy;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 327
    :cond_a
    iget-object v0, p0, Lpfy;->g:Lokm;

    if-eqz v0, :cond_b

    .line 328
    const/16 v0, 0xc

    iget-object v1, p0, Lpfy;->g:Lokm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 330
    :cond_b
    iget-object v0, p0, Lpfy;->m:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 331
    const/16 v0, 0xd

    iget-object v1, p0, Lpfy;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 333
    :cond_c
    iget-object v0, p0, Lpfy;->n:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 334
    const/16 v0, 0xe

    iget-object v1, p0, Lpfy;->n:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 336
    :cond_d
    iget-object v0, p0, Lpfy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 338
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 254
    invoke-virtual {p0, p1}, Lpfy;->a(Loxn;)Lpfy;

    move-result-object v0

    return-object v0
.end method
