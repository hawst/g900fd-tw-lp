.class public final Lhak;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 139
    invoke-static {}, Lhbr;->a()Ljava/util/List;

    move-result-object v0

    .line 140
    new-instance v1, Landroid/util/SparseArray;

    const/16 v2, 0x40

    invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v1, Lhak;->a:Landroid/util/SparseArray;

    .line 142
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhal;

    .line 143
    sget-object v2, Lhak;->a:Landroid/util/SparseArray;

    iget v3, v0, Lhal;->a:I

    iget-object v0, v0, Lhal;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0

    .line 146
    :cond_0
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12

    const-string v2, "AutoEnhance"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 147
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string v2, "UPoint"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 148
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12c

    const-string v2, "UPointItem"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 149
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/4 v1, 0x5

    const-string v2, "StraightenRotate"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 150
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/4 v1, 0x6

    const-string v2, "Crop"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 151
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x8

    const-string v2, "Vintage"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 152
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0xca

    const-string v2, "Vintage2"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 153
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0xa

    const-string v2, "Grunge"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 154
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x10

    const-string v2, "RetroLux"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 155
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x11

    const-string v2, "FixedFrames"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 156
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0xc8

    const-string v2, "Film"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 157
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x67

    const-string v2, "Healer"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 158
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12e

    const-string v2, "HealerPoint"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 159
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12f

    const-string v2, "HealerReplacement"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 160
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x66

    const-string v2, "SelectionControlPoint"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 161
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12d

    const-string v2, "SelectionControlPointItem"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 162
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x16

    const-string v2, "Brush"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 163
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x193

    const-string v2, "BiColorItem"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 164
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x194

    const-string v2, "GraduatedItem"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 165
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x192

    const-string v2, "BrushStroke"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 166
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x13

    const-string v2, "SelectiveGradientLastFilter"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 168
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x191

    const-string v2, "SelectiveGradientLastFilterItem"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 170
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x15

    const-string v2, "SelectiveGradientBcs"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 171
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x190

    const-string v2, "SelectiveGradientBcsItem"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 172
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0xd1

    const-string v2, "Newton"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 173
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const/16 v1, 0x384

    const-string v2, "RawFilter"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 174
    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 187
    sget-object v2, Lhak;->a:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 188
    :goto_1
    if-ltz v1, :cond_0

    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    :cond_0
    return v0

    .line 187
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    sget-object v0, Lhak;->a:Landroid/util/SparseArray;

    const-string v1, "unknown"

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
