.class public final Lmhf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnhb;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33532
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33535
    iput-object v0, p0, Lmhf;->apiHeader:Llyr;

    .line 33538
    iput-object v0, p0, Lmhf;->a:Lnhb;

    .line 33532
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33555
    const/4 v0, 0x0

    .line 33556
    iget-object v1, p0, Lmhf;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 33557
    const/4 v0, 0x1

    iget-object v1, p0, Lmhf;->apiHeader:Llyr;

    .line 33558
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 33560
    :cond_0
    iget-object v1, p0, Lmhf;->a:Lnhb;

    if-eqz v1, :cond_1

    .line 33561
    const/4 v1, 0x2

    iget-object v2, p0, Lmhf;->a:Lnhb;

    .line 33562
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33564
    :cond_1
    iget-object v1, p0, Lmhf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33565
    iput v0, p0, Lmhf;->ai:I

    .line 33566
    return v0
.end method

.method public a(Loxn;)Lmhf;
    .locals 2

    .prologue
    .line 33574
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 33575
    sparse-switch v0, :sswitch_data_0

    .line 33579
    iget-object v1, p0, Lmhf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 33580
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhf;->ah:Ljava/util/List;

    .line 33583
    :cond_1
    iget-object v1, p0, Lmhf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33585
    :sswitch_0
    return-object p0

    .line 33590
    :sswitch_1
    iget-object v0, p0, Lmhf;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 33591
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmhf;->apiHeader:Llyr;

    .line 33593
    :cond_2
    iget-object v0, p0, Lmhf;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33597
    :sswitch_2
    iget-object v0, p0, Lmhf;->a:Lnhb;

    if-nez v0, :cond_3

    .line 33598
    new-instance v0, Lnhb;

    invoke-direct {v0}, Lnhb;-><init>()V

    iput-object v0, p0, Lmhf;->a:Lnhb;

    .line 33600
    :cond_3
    iget-object v0, p0, Lmhf;->a:Lnhb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33575
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33543
    iget-object v0, p0, Lmhf;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 33544
    const/4 v0, 0x1

    iget-object v1, p0, Lmhf;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33546
    :cond_0
    iget-object v0, p0, Lmhf;->a:Lnhb;

    if-eqz v0, :cond_1

    .line 33547
    const/4 v0, 0x2

    iget-object v1, p0, Lmhf;->a:Lnhb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33549
    :cond_1
    iget-object v0, p0, Lmhf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33551
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33528
    invoke-virtual {p0, p1}, Lmhf;->a(Loxn;)Lmhf;

    move-result-object v0

    return-object v0
.end method
