.class public final Lpqh;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpqh;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lppf;

.field private e:[Ljava/lang/String;

.field private f:Lpil;

.field private g:Lpil;

.field private h:Lpil;

.field private i:[I

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    new-array v0, v0, [Lpqh;

    sput-object v0, Lpqh;->a:[Lpqh;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26
    iput-object v1, p0, Lpqh;->d:Lppf;

    .line 29
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpqh;->e:[Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lpqh;->f:Lpil;

    .line 35
    iput-object v1, p0, Lpqh;->g:Lpil;

    .line 38
    iput-object v1, p0, Lpqh;->h:Lpil;

    .line 41
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lpqh;->i:[I

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 125
    .line 126
    iget-object v0, p0, Lpqh;->b:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 127
    const/4 v0, 0x1

    iget-object v2, p0, Lpqh;->b:Ljava/lang/String;

    .line 128
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 130
    :goto_0
    iget-object v2, p0, Lpqh;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 131
    const/4 v2, 0x2

    iget-object v3, p0, Lpqh;->c:Ljava/lang/String;

    .line 132
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 134
    :cond_0
    iget-object v2, p0, Lpqh;->d:Lppf;

    if-eqz v2, :cond_1

    .line 135
    const/4 v2, 0x3

    iget-object v3, p0, Lpqh;->d:Lppf;

    .line 136
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 138
    :cond_1
    iget-object v2, p0, Lpqh;->e:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lpqh;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 140
    iget-object v4, p0, Lpqh;->e:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 142
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 140
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 144
    :cond_2
    add-int/2addr v0, v3

    .line 145
    iget-object v2, p0, Lpqh;->e:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 147
    :cond_3
    iget-object v2, p0, Lpqh;->f:Lpil;

    if-eqz v2, :cond_4

    .line 148
    const/4 v2, 0x5

    iget-object v3, p0, Lpqh;->f:Lpil;

    .line 149
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 151
    :cond_4
    iget-object v2, p0, Lpqh;->g:Lpil;

    if-eqz v2, :cond_5

    .line 152
    const/4 v2, 0x6

    iget-object v3, p0, Lpqh;->g:Lpil;

    .line 153
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 155
    :cond_5
    iget-object v2, p0, Lpqh;->h:Lpil;

    if-eqz v2, :cond_6

    .line 156
    const/4 v2, 0x7

    iget-object v3, p0, Lpqh;->h:Lpil;

    .line 157
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 159
    :cond_6
    iget-object v2, p0, Lpqh;->i:[I

    if-eqz v2, :cond_8

    iget-object v2, p0, Lpqh;->i:[I

    array-length v2, v2

    if-lez v2, :cond_8

    .line 161
    iget-object v3, p0, Lpqh;->i:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_7

    aget v5, v3, v1

    .line 163
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 161
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 165
    :cond_7
    add-int/2addr v0, v2

    .line 166
    iget-object v1, p0, Lpqh;->i:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 168
    :cond_8
    iget-object v1, p0, Lpqh;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 169
    const/16 v1, 0x9

    iget-object v2, p0, Lpqh;->j:Ljava/lang/Integer;

    .line 170
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_9
    iget-object v1, p0, Lpqh;->k:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 173
    const/16 v1, 0xa

    iget-object v2, p0, Lpqh;->k:Ljava/lang/String;

    .line 174
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_a
    iget-object v1, p0, Lpqh;->l:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 177
    const/16 v1, 0xb

    iget-object v2, p0, Lpqh;->l:Ljava/lang/String;

    .line 178
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_b
    iget-object v1, p0, Lpqh;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 181
    const/16 v1, 0xc

    iget-object v2, p0, Lpqh;->m:Ljava/lang/String;

    .line 182
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_c
    iget-object v1, p0, Lpqh;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 185
    const/16 v1, 0xd

    iget-object v2, p0, Lpqh;->n:Ljava/lang/Boolean;

    .line 186
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 188
    :cond_d
    iget-object v1, p0, Lpqh;->o:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 189
    const/16 v1, 0xe

    iget-object v2, p0, Lpqh;->o:Ljava/lang/String;

    .line 190
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_e
    iget-object v1, p0, Lpqh;->p:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 193
    const/16 v1, 0xf

    iget-object v2, p0, Lpqh;->p:Ljava/lang/String;

    .line 194
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_f
    iget-object v1, p0, Lpqh;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 197
    const/16 v1, 0x10

    iget-object v2, p0, Lpqh;->q:Ljava/lang/String;

    .line 198
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_10
    iget-object v1, p0, Lpqh;->r:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 201
    const/16 v1, 0x11

    iget-object v2, p0, Lpqh;->r:Ljava/lang/Boolean;

    .line 202
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 204
    :cond_11
    iget-object v1, p0, Lpqh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    iput v0, p0, Lpqh;->ai:I

    .line 206
    return v0

    :cond_12
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpqh;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 214
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 215
    sparse-switch v0, :sswitch_data_0

    .line 219
    iget-object v1, p0, Lpqh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 220
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpqh;->ah:Ljava/util/List;

    .line 223
    :cond_1
    iget-object v1, p0, Lpqh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    :sswitch_0
    return-object p0

    .line 230
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqh;->b:Ljava/lang/String;

    goto :goto_0

    .line 234
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqh;->c:Ljava/lang/String;

    goto :goto_0

    .line 238
    :sswitch_3
    iget-object v0, p0, Lpqh;->d:Lppf;

    if-nez v0, :cond_2

    .line 239
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpqh;->d:Lppf;

    .line 241
    :cond_2
    iget-object v0, p0, Lpqh;->d:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 245
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 246
    iget-object v0, p0, Lpqh;->e:[Ljava/lang/String;

    array-length v0, v0

    .line 247
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 248
    iget-object v2, p0, Lpqh;->e:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 249
    iput-object v1, p0, Lpqh;->e:[Ljava/lang/String;

    .line 250
    :goto_1
    iget-object v1, p0, Lpqh;->e:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 251
    iget-object v1, p0, Lpqh;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 252
    invoke-virtual {p1}, Loxn;->a()I

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 255
    :cond_3
    iget-object v1, p0, Lpqh;->e:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 259
    :sswitch_5
    iget-object v0, p0, Lpqh;->f:Lpil;

    if-nez v0, :cond_4

    .line 260
    new-instance v0, Lpil;

    invoke-direct {v0}, Lpil;-><init>()V

    iput-object v0, p0, Lpqh;->f:Lpil;

    .line 262
    :cond_4
    iget-object v0, p0, Lpqh;->f:Lpil;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 266
    :sswitch_6
    iget-object v0, p0, Lpqh;->g:Lpil;

    if-nez v0, :cond_5

    .line 267
    new-instance v0, Lpil;

    invoke-direct {v0}, Lpil;-><init>()V

    iput-object v0, p0, Lpqh;->g:Lpil;

    .line 269
    :cond_5
    iget-object v0, p0, Lpqh;->g:Lpil;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 273
    :sswitch_7
    iget-object v0, p0, Lpqh;->h:Lpil;

    if-nez v0, :cond_6

    .line 274
    new-instance v0, Lpil;

    invoke-direct {v0}, Lpil;-><init>()V

    iput-object v0, p0, Lpqh;->h:Lpil;

    .line 276
    :cond_6
    iget-object v0, p0, Lpqh;->h:Lpil;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 280
    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 281
    iget-object v0, p0, Lpqh;->i:[I

    array-length v0, v0

    .line 282
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 283
    iget-object v2, p0, Lpqh;->i:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 284
    iput-object v1, p0, Lpqh;->i:[I

    .line 285
    :goto_2
    iget-object v1, p0, Lpqh;->i:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    .line 286
    iget-object v1, p0, Lpqh;->i:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 287
    invoke-virtual {p1}, Loxn;->a()I

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 290
    :cond_7
    iget-object v1, p0, Lpqh;->i:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 294
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpqh;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 298
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqh;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 302
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqh;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 306
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqh;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 310
    :sswitch_d
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpqh;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 314
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqh;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 318
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqh;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 322
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpqh;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 326
    :sswitch_11
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpqh;->r:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 215
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lpqh;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 65
    const/4 v1, 0x1

    iget-object v2, p0, Lpqh;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 67
    :cond_0
    iget-object v1, p0, Lpqh;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 68
    const/4 v1, 0x2

    iget-object v2, p0, Lpqh;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 70
    :cond_1
    iget-object v1, p0, Lpqh;->d:Lppf;

    if-eqz v1, :cond_2

    .line 71
    const/4 v1, 0x3

    iget-object v2, p0, Lpqh;->d:Lppf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 73
    :cond_2
    iget-object v1, p0, Lpqh;->e:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 74
    iget-object v2, p0, Lpqh;->e:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 75
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 74
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 78
    :cond_3
    iget-object v1, p0, Lpqh;->f:Lpil;

    if-eqz v1, :cond_4

    .line 79
    const/4 v1, 0x5

    iget-object v2, p0, Lpqh;->f:Lpil;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 81
    :cond_4
    iget-object v1, p0, Lpqh;->g:Lpil;

    if-eqz v1, :cond_5

    .line 82
    const/4 v1, 0x6

    iget-object v2, p0, Lpqh;->g:Lpil;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 84
    :cond_5
    iget-object v1, p0, Lpqh;->h:Lpil;

    if-eqz v1, :cond_6

    .line 85
    const/4 v1, 0x7

    iget-object v2, p0, Lpqh;->h:Lpil;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 87
    :cond_6
    iget-object v1, p0, Lpqh;->i:[I

    if-eqz v1, :cond_7

    iget-object v1, p0, Lpqh;->i:[I

    array-length v1, v1

    if-lez v1, :cond_7

    .line 88
    iget-object v1, p0, Lpqh;->i:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_7

    aget v3, v1, v0

    .line 89
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 92
    :cond_7
    iget-object v0, p0, Lpqh;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 93
    const/16 v0, 0x9

    iget-object v1, p0, Lpqh;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 95
    :cond_8
    iget-object v0, p0, Lpqh;->k:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 96
    const/16 v0, 0xa

    iget-object v1, p0, Lpqh;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 98
    :cond_9
    iget-object v0, p0, Lpqh;->l:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 99
    const/16 v0, 0xb

    iget-object v1, p0, Lpqh;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 101
    :cond_a
    iget-object v0, p0, Lpqh;->m:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 102
    const/16 v0, 0xc

    iget-object v1, p0, Lpqh;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 104
    :cond_b
    iget-object v0, p0, Lpqh;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 105
    const/16 v0, 0xd

    iget-object v1, p0, Lpqh;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 107
    :cond_c
    iget-object v0, p0, Lpqh;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 108
    const/16 v0, 0xe

    iget-object v1, p0, Lpqh;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 110
    :cond_d
    iget-object v0, p0, Lpqh;->p:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 111
    const/16 v0, 0xf

    iget-object v1, p0, Lpqh;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 113
    :cond_e
    iget-object v0, p0, Lpqh;->q:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 114
    const/16 v0, 0x10

    iget-object v1, p0, Lpqh;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 116
    :cond_f
    iget-object v0, p0, Lpqh;->r:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 117
    const/16 v0, 0x11

    iget-object v1, p0, Lpqh;->r:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 119
    :cond_10
    iget-object v0, p0, Lpqh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 121
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpqh;->a(Loxn;)Lpqh;

    move-result-object v0

    return-object v0
.end method
