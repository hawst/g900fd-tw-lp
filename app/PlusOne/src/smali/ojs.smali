.class public final Lojs;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:[Lohh;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1522
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1536
    const/high16 v0, -0x80000000

    iput v0, p0, Lojs;->b:I

    .line 1539
    sget-object v0, Lohh;->a:[Lohh;

    iput-object v0, p0, Lojs;->c:[Lohh;

    .line 1522
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1566
    .line 1567
    iget-object v0, p0, Lojs;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1568
    const/4 v0, 0x1

    iget-object v2, p0, Lojs;->a:Ljava/lang/String;

    .line 1569
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1571
    :goto_0
    iget v2, p0, Lojs;->b:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_0

    .line 1572
    const/4 v2, 0x2

    iget v3, p0, Lojs;->b:I

    .line 1573
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1575
    :cond_0
    iget-object v2, p0, Lojs;->c:[Lohh;

    if-eqz v2, :cond_2

    .line 1576
    iget-object v2, p0, Lojs;->c:[Lohh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1577
    if-eqz v4, :cond_1

    .line 1578
    const/4 v5, 0x3

    .line 1579
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1576
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1583
    :cond_2
    iget-object v1, p0, Lojs;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1584
    const/4 v1, 0x4

    iget-object v2, p0, Lojs;->d:Ljava/lang/String;

    .line 1585
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1587
    :cond_3
    iget-object v1, p0, Lojs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1588
    iput v0, p0, Lojs;->ai:I

    .line 1589
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lojs;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1597
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1598
    sparse-switch v0, :sswitch_data_0

    .line 1602
    iget-object v2, p0, Lojs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1603
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lojs;->ah:Ljava/util/List;

    .line 1606
    :cond_1
    iget-object v2, p0, Lojs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1608
    :sswitch_0
    return-object p0

    .line 1613
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojs;->a:Ljava/lang/String;

    goto :goto_0

    .line 1617
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1618
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    .line 1622
    :cond_2
    iput v0, p0, Lojs;->b:I

    goto :goto_0

    .line 1624
    :cond_3
    iput v4, p0, Lojs;->b:I

    goto :goto_0

    .line 1629
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1630
    iget-object v0, p0, Lojs;->c:[Lohh;

    if-nez v0, :cond_5

    move v0, v1

    .line 1631
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohh;

    .line 1632
    iget-object v3, p0, Lojs;->c:[Lohh;

    if-eqz v3, :cond_4

    .line 1633
    iget-object v3, p0, Lojs;->c:[Lohh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1635
    :cond_4
    iput-object v2, p0, Lojs;->c:[Lohh;

    .line 1636
    :goto_2
    iget-object v2, p0, Lojs;->c:[Lohh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1637
    iget-object v2, p0, Lojs;->c:[Lohh;

    new-instance v3, Lohh;

    invoke-direct {v3}, Lohh;-><init>()V

    aput-object v3, v2, v0

    .line 1638
    iget-object v2, p0, Lojs;->c:[Lohh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1639
    invoke-virtual {p1}, Loxn;->a()I

    .line 1636
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1630
    :cond_5
    iget-object v0, p0, Lojs;->c:[Lohh;

    array-length v0, v0

    goto :goto_1

    .line 1642
    :cond_6
    iget-object v2, p0, Lojs;->c:[Lohh;

    new-instance v3, Lohh;

    invoke-direct {v3}, Lohh;-><init>()V

    aput-object v3, v2, v0

    .line 1643
    iget-object v2, p0, Lojs;->c:[Lohh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1647
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojs;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 1598
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1544
    iget-object v0, p0, Lojs;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1545
    const/4 v0, 0x1

    iget-object v1, p0, Lojs;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1547
    :cond_0
    iget v0, p0, Lojs;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 1548
    const/4 v0, 0x2

    iget v1, p0, Lojs;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1550
    :cond_1
    iget-object v0, p0, Lojs;->c:[Lohh;

    if-eqz v0, :cond_3

    .line 1551
    iget-object v1, p0, Lojs;->c:[Lohh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1552
    if-eqz v3, :cond_2

    .line 1553
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1551
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1557
    :cond_3
    iget-object v0, p0, Lojs;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1558
    const/4 v0, 0x4

    iget-object v1, p0, Lojs;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1560
    :cond_4
    iget-object v0, p0, Lojs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1562
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1518
    invoke-virtual {p0, p1}, Lojs;->a(Loxn;)Lojs;

    move-result-object v0

    return-object v0
.end method
