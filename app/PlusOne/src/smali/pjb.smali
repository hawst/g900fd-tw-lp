.class public final Lpjb;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lppr;

.field private b:Lpps;

.field private c:Lppf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    iput-object v0, p0, Lpjb;->a:Lppr;

    .line 20
    iput-object v0, p0, Lpjb;->b:Lpps;

    .line 23
    iput-object v0, p0, Lpjb;->c:Lppf;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, Lpjb;->a:Lppr;

    if-eqz v1, :cond_0

    .line 45
    const/4 v0, 0x1

    iget-object v1, p0, Lpjb;->a:Lppr;

    .line 46
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 48
    :cond_0
    iget-object v1, p0, Lpjb;->b:Lpps;

    if-eqz v1, :cond_1

    .line 49
    const/4 v1, 0x2

    iget-object v2, p0, Lpjb;->b:Lpps;

    .line 50
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_1
    iget-object v1, p0, Lpjb;->c:Lppf;

    if-eqz v1, :cond_2

    .line 53
    const/4 v1, 0x3

    iget-object v2, p0, Lpjb;->c:Lppf;

    .line 54
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_2
    iget-object v1, p0, Lpjb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    iput v0, p0, Lpjb;->ai:I

    .line 58
    return v0
.end method

.method public a(Loxn;)Lpjb;
    .locals 2

    .prologue
    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 67
    sparse-switch v0, :sswitch_data_0

    .line 71
    iget-object v1, p0, Lpjb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjb;->ah:Ljava/util/List;

    .line 75
    :cond_1
    iget-object v1, p0, Lpjb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    :sswitch_0
    return-object p0

    .line 82
    :sswitch_1
    iget-object v0, p0, Lpjb;->a:Lppr;

    if-nez v0, :cond_2

    .line 83
    new-instance v0, Lppr;

    invoke-direct {v0}, Lppr;-><init>()V

    iput-object v0, p0, Lpjb;->a:Lppr;

    .line 85
    :cond_2
    iget-object v0, p0, Lpjb;->a:Lppr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 89
    :sswitch_2
    iget-object v0, p0, Lpjb;->b:Lpps;

    if-nez v0, :cond_3

    .line 90
    new-instance v0, Lpps;

    invoke-direct {v0}, Lpps;-><init>()V

    iput-object v0, p0, Lpjb;->b:Lpps;

    .line 92
    :cond_3
    iget-object v0, p0, Lpjb;->b:Lpps;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 96
    :sswitch_3
    iget-object v0, p0, Lpjb;->c:Lppf;

    if-nez v0, :cond_4

    .line 97
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpjb;->c:Lppf;

    .line 99
    :cond_4
    iget-object v0, p0, Lpjb;->c:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lpjb;->a:Lppr;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v1, p0, Lpjb;->a:Lppr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_0
    iget-object v0, p0, Lpjb;->b:Lpps;

    if-eqz v0, :cond_1

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Lpjb;->b:Lpps;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 34
    :cond_1
    iget-object v0, p0, Lpjb;->c:Lppf;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x3

    iget-object v1, p0, Lpjb;->c:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_2
    iget-object v0, p0, Lpjb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 39
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpjb;->a(Loxn;)Lpjb;

    move-result-object v0

    return-object v0
.end method
