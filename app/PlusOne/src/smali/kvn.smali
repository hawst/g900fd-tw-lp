.class public final Lkvn;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Lkfo;

.field private final d:Lktq;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 30
    const-string v0, "DeclineSquareInvitationTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 32
    iput p2, p0, Lkvn;->a:I

    .line 33
    iput-object p3, p0, Lkvn;->b:Ljava/lang/String;

    .line 34
    new-instance v0, Lkfo;

    iget v1, p0, Lkvn;->a:I

    invoke-direct {v0, p1, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkvn;->c:Lkfo;

    .line 35
    const-class v0, Lktq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iput-object v0, p0, Lkvn;->d:Lktq;

    .line 36
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 5

    .prologue
    const/16 v4, 0x15

    .line 41
    iget-object v0, p0, Lkvn;->d:Lktq;

    iget v1, p0, Lkvn;->a:I

    iget-object v2, p0, Lkvn;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lktq;->a(ILjava/lang/String;)V

    .line 43
    new-instance v0, Lkvo;

    .line 44
    invoke-virtual {p0}, Lkvn;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lkvn;->c:Lkfo;

    iget-object v3, p0, Lkvn;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lkvo;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;I)V

    .line 45
    invoke-virtual {v0}, Lkvo;->l()V

    .line 47
    invoke-virtual {v0}, Lkvo;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    iget-object v1, p0, Lkvn;->d:Lktq;

    iget v2, p0, Lkvn;->a:I

    iget-object v3, p0, Lkvn;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4}, Lktq;->a(ILjava/lang/String;I)V

    .line 51
    :cond_0
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    .line 52
    invoke-virtual {v0}, Lkvo;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lkvn;->f()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a04c5

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
