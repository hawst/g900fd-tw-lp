.class public final Lmvs;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmvs;


# instance fields
.field private b:Ljava/lang/Double;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/String;

.field private e:Lmud;

.field private f:[Lmue;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:I

.field private j:Ljava/lang/Long;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/Integer;

.field private n:I

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/Integer;

.field private q:Lmtt;

.field private r:Ljava/lang/Long;

.field private s:[Lmwv;

.field private t:[Lmwr;

.field private u:Lmvt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5922
    const/4 v0, 0x0

    new-array v0, v0, [Lmvs;

    sput-object v0, Lmvs;->a:[Lmvs;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 5923
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5992
    iput-object v1, p0, Lmvs;->e:Lmud;

    .line 5995
    sget-object v0, Lmue;->a:[Lmue;

    iput-object v0, p0, Lmvs;->f:[Lmue;

    .line 6002
    iput v2, p0, Lmvs;->i:I

    .line 6013
    iput v2, p0, Lmvs;->n:I

    .line 6020
    iput-object v1, p0, Lmvs;->q:Lmtt;

    .line 6025
    sget-object v0, Lmwv;->a:[Lmwv;

    iput-object v0, p0, Lmvs;->s:[Lmwv;

    .line 6028
    sget-object v0, Lmwr;->a:[Lmwr;

    iput-object v0, p0, Lmvs;->t:[Lmwr;

    .line 6031
    iput-object v1, p0, Lmvs;->u:Lmvt;

    .line 5923
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v1, 0x0

    .line 6114
    .line 6115
    iget-object v0, p0, Lmvs;->b:Ljava/lang/Double;

    if-eqz v0, :cond_16

    .line 6116
    const/4 v0, 0x1

    iget-object v2, p0, Lmvs;->b:Ljava/lang/Double;

    .line 6117
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 6119
    :goto_0
    iget-object v2, p0, Lmvs;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 6120
    const/4 v2, 0x2

    iget-object v3, p0, Lmvs;->c:Ljava/lang/Integer;

    .line 6121
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6123
    :cond_0
    iget-object v2, p0, Lmvs;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 6124
    const/4 v2, 0x3

    iget-object v3, p0, Lmvs;->d:Ljava/lang/String;

    .line 6125
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6127
    :cond_1
    iget-object v2, p0, Lmvs;->e:Lmud;

    if-eqz v2, :cond_2

    .line 6128
    const/4 v2, 0x4

    iget-object v3, p0, Lmvs;->e:Lmud;

    .line 6129
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6131
    :cond_2
    iget-object v2, p0, Lmvs;->f:[Lmue;

    if-eqz v2, :cond_4

    .line 6132
    iget-object v3, p0, Lmvs;->f:[Lmue;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 6133
    if-eqz v5, :cond_3

    .line 6134
    const/4 v6, 0x5

    .line 6135
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 6132
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 6139
    :cond_4
    iget-object v2, p0, Lmvs;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 6140
    const/4 v2, 0x6

    iget-object v3, p0, Lmvs;->g:Ljava/lang/Integer;

    .line 6141
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6143
    :cond_5
    iget-object v2, p0, Lmvs;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 6144
    const/4 v2, 0x7

    iget-object v3, p0, Lmvs;->h:Ljava/lang/Integer;

    .line 6145
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6147
    :cond_6
    iget v2, p0, Lmvs;->i:I

    if-eq v2, v7, :cond_7

    .line 6148
    const/16 v2, 0x8

    iget v3, p0, Lmvs;->i:I

    .line 6149
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6151
    :cond_7
    iget-object v2, p0, Lmvs;->j:Ljava/lang/Long;

    if-eqz v2, :cond_8

    .line 6152
    const/16 v2, 0x9

    iget-object v3, p0, Lmvs;->j:Ljava/lang/Long;

    .line 6153
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 6155
    :cond_8
    iget-object v2, p0, Lmvs;->l:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 6156
    const/16 v2, 0xa

    iget-object v3, p0, Lmvs;->l:Ljava/lang/String;

    .line 6157
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6159
    :cond_9
    iget-object v2, p0, Lmvs;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    .line 6160
    const/16 v2, 0xb

    iget-object v3, p0, Lmvs;->k:Ljava/lang/Integer;

    .line 6161
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6163
    :cond_a
    iget-object v2, p0, Lmvs;->m:Ljava/lang/Integer;

    if-eqz v2, :cond_b

    .line 6164
    const/16 v2, 0xc

    iget-object v3, p0, Lmvs;->m:Ljava/lang/Integer;

    .line 6165
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6167
    :cond_b
    iget v2, p0, Lmvs;->n:I

    if-eq v2, v7, :cond_c

    .line 6168
    const/16 v2, 0xd

    iget v3, p0, Lmvs;->n:I

    .line 6169
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6171
    :cond_c
    iget-object v2, p0, Lmvs;->o:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    .line 6172
    const/16 v2, 0xe

    iget-object v3, p0, Lmvs;->o:Ljava/lang/Integer;

    .line 6173
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6175
    :cond_d
    iget-object v2, p0, Lmvs;->p:Ljava/lang/Integer;

    if-eqz v2, :cond_e

    .line 6176
    const/16 v2, 0xf

    iget-object v3, p0, Lmvs;->p:Ljava/lang/Integer;

    .line 6177
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6179
    :cond_e
    iget-object v2, p0, Lmvs;->q:Lmtt;

    if-eqz v2, :cond_f

    .line 6180
    const/16 v2, 0x10

    iget-object v3, p0, Lmvs;->q:Lmtt;

    .line 6181
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6183
    :cond_f
    iget-object v2, p0, Lmvs;->r:Ljava/lang/Long;

    if-eqz v2, :cond_10

    .line 6184
    const/16 v2, 0x11

    iget-object v3, p0, Lmvs;->r:Ljava/lang/Long;

    .line 6185
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 6187
    :cond_10
    iget-object v2, p0, Lmvs;->s:[Lmwv;

    if-eqz v2, :cond_12

    .line 6188
    iget-object v3, p0, Lmvs;->s:[Lmwv;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_12

    aget-object v5, v3, v2

    .line 6189
    if-eqz v5, :cond_11

    .line 6190
    const/16 v6, 0x12

    .line 6191
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 6188
    :cond_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 6195
    :cond_12
    iget-object v2, p0, Lmvs;->t:[Lmwr;

    if-eqz v2, :cond_14

    .line 6196
    iget-object v2, p0, Lmvs;->t:[Lmwr;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_14

    aget-object v4, v2, v1

    .line 6197
    if-eqz v4, :cond_13

    .line 6198
    const/16 v5, 0x13

    .line 6199
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6196
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 6203
    :cond_14
    iget-object v1, p0, Lmvs;->u:Lmvt;

    if-eqz v1, :cond_15

    .line 6204
    const/16 v1, 0x14

    iget-object v2, p0, Lmvs;->u:Lmvt;

    .line 6205
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6207
    :cond_15
    iget-object v1, p0, Lmvs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6208
    iput v0, p0, Lmvs;->ai:I

    .line 6209
    return v0

    :cond_16
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmvs;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 6217
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6218
    sparse-switch v0, :sswitch_data_0

    .line 6222
    iget-object v2, p0, Lmvs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 6223
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmvs;->ah:Ljava/util/List;

    .line 6226
    :cond_1
    iget-object v2, p0, Lmvs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6228
    :sswitch_0
    return-object p0

    .line 6233
    :sswitch_1
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lmvs;->b:Ljava/lang/Double;

    goto :goto_0

    .line 6237
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvs;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 6241
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmvs;->d:Ljava/lang/String;

    goto :goto_0

    .line 6245
    :sswitch_4
    iget-object v0, p0, Lmvs;->e:Lmud;

    if-nez v0, :cond_2

    .line 6246
    new-instance v0, Lmud;

    invoke-direct {v0}, Lmud;-><init>()V

    iput-object v0, p0, Lmvs;->e:Lmud;

    .line 6248
    :cond_2
    iget-object v0, p0, Lmvs;->e:Lmud;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6252
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6253
    iget-object v0, p0, Lmvs;->f:[Lmue;

    if-nez v0, :cond_4

    move v0, v1

    .line 6254
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmue;

    .line 6255
    iget-object v3, p0, Lmvs;->f:[Lmue;

    if-eqz v3, :cond_3

    .line 6256
    iget-object v3, p0, Lmvs;->f:[Lmue;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6258
    :cond_3
    iput-object v2, p0, Lmvs;->f:[Lmue;

    .line 6259
    :goto_2
    iget-object v2, p0, Lmvs;->f:[Lmue;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 6260
    iget-object v2, p0, Lmvs;->f:[Lmue;

    new-instance v3, Lmue;

    invoke-direct {v3}, Lmue;-><init>()V

    aput-object v3, v2, v0

    .line 6261
    iget-object v2, p0, Lmvs;->f:[Lmue;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6262
    invoke-virtual {p1}, Loxn;->a()I

    .line 6259
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6253
    :cond_4
    iget-object v0, p0, Lmvs;->f:[Lmue;

    array-length v0, v0

    goto :goto_1

    .line 6265
    :cond_5
    iget-object v2, p0, Lmvs;->f:[Lmue;

    new-instance v3, Lmue;

    invoke-direct {v3}, Lmue;-><init>()V

    aput-object v3, v2, v0

    .line 6266
    iget-object v2, p0, Lmvs;->f:[Lmue;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6270
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvs;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6274
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvs;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6278
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6279
    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-eq v0, v6, :cond_6

    if-eq v0, v7, :cond_6

    const/4 v2, 0x5

    if-eq v0, v2, :cond_6

    const/4 v2, 0x6

    if-eq v0, v2, :cond_6

    const/4 v2, 0x7

    if-eq v0, v2, :cond_6

    const/16 v2, 0x8

    if-eq v0, v2, :cond_6

    const/16 v2, 0x9

    if-eq v0, v2, :cond_6

    const/16 v2, 0xa

    if-eq v0, v2, :cond_6

    const/16 v2, 0xb

    if-eq v0, v2, :cond_6

    const/16 v2, 0xc

    if-eq v0, v2, :cond_6

    const/16 v2, 0xd

    if-eq v0, v2, :cond_6

    const/16 v2, 0xe

    if-eq v0, v2, :cond_6

    const/16 v2, 0xf

    if-eq v0, v2, :cond_6

    const/16 v2, 0x10

    if-ne v0, v2, :cond_7

    .line 6296
    :cond_6
    iput v0, p0, Lmvs;->i:I

    goto/16 :goto_0

    .line 6298
    :cond_7
    iput v1, p0, Lmvs;->i:I

    goto/16 :goto_0

    .line 6303
    :sswitch_9
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmvs;->j:Ljava/lang/Long;

    goto/16 :goto_0

    .line 6307
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmvs;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 6311
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvs;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6315
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvs;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6319
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6320
    if-eqz v0, :cond_8

    if-eq v0, v4, :cond_8

    if-eq v0, v5, :cond_8

    if-eq v0, v6, :cond_8

    if-eq v0, v7, :cond_8

    const/4 v2, 0x5

    if-eq v0, v2, :cond_8

    const/4 v2, 0x6

    if-ne v0, v2, :cond_9

    .line 6327
    :cond_8
    iput v0, p0, Lmvs;->n:I

    goto/16 :goto_0

    .line 6329
    :cond_9
    iput v1, p0, Lmvs;->n:I

    goto/16 :goto_0

    .line 6334
    :sswitch_e
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvs;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6338
    :sswitch_f
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvs;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6342
    :sswitch_10
    iget-object v0, p0, Lmvs;->q:Lmtt;

    if-nez v0, :cond_a

    .line 6343
    new-instance v0, Lmtt;

    invoke-direct {v0}, Lmtt;-><init>()V

    iput-object v0, p0, Lmvs;->q:Lmtt;

    .line 6345
    :cond_a
    iget-object v0, p0, Lmvs;->q:Lmtt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6349
    :sswitch_11
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmvs;->r:Ljava/lang/Long;

    goto/16 :goto_0

    .line 6353
    :sswitch_12
    const/16 v0, 0x92

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6354
    iget-object v0, p0, Lmvs;->s:[Lmwv;

    if-nez v0, :cond_c

    move v0, v1

    .line 6355
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmwv;

    .line 6356
    iget-object v3, p0, Lmvs;->s:[Lmwv;

    if-eqz v3, :cond_b

    .line 6357
    iget-object v3, p0, Lmvs;->s:[Lmwv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6359
    :cond_b
    iput-object v2, p0, Lmvs;->s:[Lmwv;

    .line 6360
    :goto_4
    iget-object v2, p0, Lmvs;->s:[Lmwv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 6361
    iget-object v2, p0, Lmvs;->s:[Lmwv;

    new-instance v3, Lmwv;

    invoke-direct {v3}, Lmwv;-><init>()V

    aput-object v3, v2, v0

    .line 6362
    iget-object v2, p0, Lmvs;->s:[Lmwv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6363
    invoke-virtual {p1}, Loxn;->a()I

    .line 6360
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 6354
    :cond_c
    iget-object v0, p0, Lmvs;->s:[Lmwv;

    array-length v0, v0

    goto :goto_3

    .line 6366
    :cond_d
    iget-object v2, p0, Lmvs;->s:[Lmwv;

    new-instance v3, Lmwv;

    invoke-direct {v3}, Lmwv;-><init>()V

    aput-object v3, v2, v0

    .line 6367
    iget-object v2, p0, Lmvs;->s:[Lmwv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6371
    :sswitch_13
    const/16 v0, 0x9a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6372
    iget-object v0, p0, Lmvs;->t:[Lmwr;

    if-nez v0, :cond_f

    move v0, v1

    .line 6373
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lmwr;

    .line 6374
    iget-object v3, p0, Lmvs;->t:[Lmwr;

    if-eqz v3, :cond_e

    .line 6375
    iget-object v3, p0, Lmvs;->t:[Lmwr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6377
    :cond_e
    iput-object v2, p0, Lmvs;->t:[Lmwr;

    .line 6378
    :goto_6
    iget-object v2, p0, Lmvs;->t:[Lmwr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    .line 6379
    iget-object v2, p0, Lmvs;->t:[Lmwr;

    new-instance v3, Lmwr;

    invoke-direct {v3}, Lmwr;-><init>()V

    aput-object v3, v2, v0

    .line 6380
    iget-object v2, p0, Lmvs;->t:[Lmwr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6381
    invoke-virtual {p1}, Loxn;->a()I

    .line 6378
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 6372
    :cond_f
    iget-object v0, p0, Lmvs;->t:[Lmwr;

    array-length v0, v0

    goto :goto_5

    .line 6384
    :cond_10
    iget-object v2, p0, Lmvs;->t:[Lmwr;

    new-instance v3, Lmwr;

    invoke-direct {v3}, Lmwr;-><init>()V

    aput-object v3, v2, v0

    .line 6385
    iget-object v2, p0, Lmvs;->t:[Lmwr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6389
    :sswitch_14
    iget-object v0, p0, Lmvs;->u:Lmvt;

    if-nez v0, :cond_11

    .line 6390
    new-instance v0, Lmvt;

    invoke-direct {v0}, Lmvt;-><init>()V

    iput-object v0, p0, Lmvs;->u:Lmvt;

    .line 6392
    :cond_11
    iget-object v0, p0, Lmvs;->u:Lmvt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6218
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    const/4 v0, 0x0

    .line 6036
    iget-object v1, p0, Lmvs;->b:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 6037
    const/4 v1, 0x1

    iget-object v2, p0, Lmvs;->b:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(ID)V

    .line 6039
    :cond_0
    iget-object v1, p0, Lmvs;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 6040
    const/4 v1, 0x2

    iget-object v2, p0, Lmvs;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6042
    :cond_1
    iget-object v1, p0, Lmvs;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6043
    const/4 v1, 0x3

    iget-object v2, p0, Lmvs;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 6045
    :cond_2
    iget-object v1, p0, Lmvs;->e:Lmud;

    if-eqz v1, :cond_3

    .line 6046
    const/4 v1, 0x4

    iget-object v2, p0, Lmvs;->e:Lmud;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 6048
    :cond_3
    iget-object v1, p0, Lmvs;->f:[Lmue;

    if-eqz v1, :cond_5

    .line 6049
    iget-object v2, p0, Lmvs;->f:[Lmue;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 6050
    if-eqz v4, :cond_4

    .line 6051
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 6049
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6055
    :cond_5
    iget-object v1, p0, Lmvs;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 6056
    const/4 v1, 0x6

    iget-object v2, p0, Lmvs;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6058
    :cond_6
    iget-object v1, p0, Lmvs;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 6059
    const/4 v1, 0x7

    iget-object v2, p0, Lmvs;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6061
    :cond_7
    iget v1, p0, Lmvs;->i:I

    if-eq v1, v6, :cond_8

    .line 6062
    const/16 v1, 0x8

    iget v2, p0, Lmvs;->i:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6064
    :cond_8
    iget-object v1, p0, Lmvs;->j:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 6065
    const/16 v1, 0x9

    iget-object v2, p0, Lmvs;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(IJ)V

    .line 6067
    :cond_9
    iget-object v1, p0, Lmvs;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 6068
    const/16 v1, 0xa

    iget-object v2, p0, Lmvs;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 6070
    :cond_a
    iget-object v1, p0, Lmvs;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 6071
    const/16 v1, 0xb

    iget-object v2, p0, Lmvs;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6073
    :cond_b
    iget-object v1, p0, Lmvs;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 6074
    const/16 v1, 0xc

    iget-object v2, p0, Lmvs;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6076
    :cond_c
    iget v1, p0, Lmvs;->n:I

    if-eq v1, v6, :cond_d

    .line 6077
    const/16 v1, 0xd

    iget v2, p0, Lmvs;->n:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6079
    :cond_d
    iget-object v1, p0, Lmvs;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 6080
    const/16 v1, 0xe

    iget-object v2, p0, Lmvs;->o:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6082
    :cond_e
    iget-object v1, p0, Lmvs;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 6083
    const/16 v1, 0xf

    iget-object v2, p0, Lmvs;->p:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6085
    :cond_f
    iget-object v1, p0, Lmvs;->q:Lmtt;

    if-eqz v1, :cond_10

    .line 6086
    const/16 v1, 0x10

    iget-object v2, p0, Lmvs;->q:Lmtt;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 6088
    :cond_10
    iget-object v1, p0, Lmvs;->r:Ljava/lang/Long;

    if-eqz v1, :cond_11

    .line 6089
    const/16 v1, 0x11

    iget-object v2, p0, Lmvs;->r:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 6091
    :cond_11
    iget-object v1, p0, Lmvs;->s:[Lmwv;

    if-eqz v1, :cond_13

    .line 6092
    iget-object v2, p0, Lmvs;->s:[Lmwv;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_13

    aget-object v4, v2, v1

    .line 6093
    if-eqz v4, :cond_12

    .line 6094
    const/16 v5, 0x12

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 6092
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6098
    :cond_13
    iget-object v1, p0, Lmvs;->t:[Lmwr;

    if-eqz v1, :cond_15

    .line 6099
    iget-object v1, p0, Lmvs;->t:[Lmwr;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_15

    aget-object v3, v1, v0

    .line 6100
    if-eqz v3, :cond_14

    .line 6101
    const/16 v4, 0x13

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 6099
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6105
    :cond_15
    iget-object v0, p0, Lmvs;->u:Lmvt;

    if-eqz v0, :cond_16

    .line 6106
    const/16 v0, 0x14

    iget-object v1, p0, Lmvs;->u:Lmvt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6108
    :cond_16
    iget-object v0, p0, Lmvs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6110
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5919
    invoke-virtual {p0, p1}, Lmvs;->a(Loxn;)Lmvs;

    move-result-object v0

    return-object v0
.end method
