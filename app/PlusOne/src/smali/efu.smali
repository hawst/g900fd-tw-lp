.class public final Lefu;
.super Lhya;
.source "PG"


# instance fields
.field final synthetic a:Lefj;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lefr;

.field private d:Landroid/database/Cursor;

.field private e:I

.field private f:Z


# direct methods
.method constructor <init>(Lefj;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 473
    iput-object p1, p0, Lefu;->a:Lefj;

    .line 474
    invoke-direct {p0, p2}, Lhya;-><init>(Landroid/content/Context;)V

    .line 476
    invoke-static {}, Lefj;->Z()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 477
    invoke-static {}, Lefj;->Z()Lhym;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    .line 480
    :cond_0
    invoke-static {p1}, Lefj;->f(Lefj;)Z

    move-result v0

    iput-boolean v0, p0, Lefu;->f:Z

    .line 481
    invoke-virtual {p0, v3, v3}, Lefu;->b(ZZ)V

    .line 482
    invoke-virtual {p0, v3, v3}, Lefu;->b(ZZ)V

    .line 483
    invoke-virtual {p0, v3, v4}, Lefu;->b(ZZ)V

    .line 484
    invoke-virtual {p0, v3, v3}, Lefu;->b(ZZ)V

    .line 485
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 743
    const/4 v0, 0x3

    return v0
.end method

.method protected a(II)I
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 728
    packed-switch p1, :pswitch_data_0

    .line 738
    :goto_0
    :pswitch_0
    return v0

    .line 732
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 734
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 736
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 728
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, -0x2

    .line 563
    packed-switch p2, :pswitch_data_0

    .line 618
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 565
    :pswitch_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 566
    new-instance v1, Llji;

    iget-object v2, p0, Lefu;->a:Lefj;

    invoke-static {v2}, Lefj;->i(Lefj;)Llcr;

    move-result-object v2

    iget v2, v2, Llcr;->b:I

    .line 567
    invoke-static {p1}, Ljgh;->a(Landroid/content/Context;)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Llji;-><init>(II)V

    .line 568
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 571
    :pswitch_1
    new-instance v0, Lfzg;

    invoke-direct {v0, p1}, Lfzg;-><init>(Landroid/content/Context;)V

    .line 572
    iget-object v1, p0, Lefu;->a:Lefj;

    invoke-static {v1}, Lefj;->g(Lefj;)Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfzg;->a(Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;)V

    .line 573
    new-instance v1, Llji;

    iget-object v2, p0, Lefu;->a:Lefj;

    invoke-static {v2}, Lefj;->i(Lefj;)Llcr;

    move-result-object v2

    iget v2, v2, Llcr;->b:I

    invoke-direct {v1, v2, v3}, Llji;-><init>(II)V

    .line 575
    invoke-virtual {v0, v1}, Lfzg;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 578
    :pswitch_2
    new-instance v0, Lfwr;

    invoke-direct {v0, p1}, Lfwr;-><init>(Landroid/content/Context;)V

    .line 580
    new-instance v1, Llji;

    iget-object v2, p0, Lefu;->a:Lefj;

    invoke-static {v2}, Lefj;->i(Lefj;)Llcr;

    move-result-object v2

    iget v2, v2, Llcr;->b:I

    invoke-direct {v1, v2, v3}, Llji;-><init>(II)V

    .line 582
    invoke-virtual {v0, v1}, Lfwr;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 585
    :pswitch_3
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 586
    const v0, 0x7f0a0870

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 587
    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 588
    const/4 v0, 0x0

    invoke-static {}, Lefj;->aa()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 590
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0205d8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 589
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 591
    new-instance v0, Lefv;

    invoke-direct {v0, p0, p1}, Lefv;-><init>(Lefu;Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 611
    iget-object v2, p0, Lefu;->a:Lefj;

    invoke-static {v2}, Lefj;->i(Lefj;)Llcr;

    move-result-object v2

    iget v2, v2, Llcr;->f:I

    invoke-static {}, Lefj;->ab()I

    move-result v3

    iget-object v4, p0, Lefu;->a:Lefj;

    .line 612
    invoke-static {v4}, Lefj;->i(Lefj;)Llcr;

    move-result-object v4

    iget v4, v4, Llcr;->f:I

    invoke-static {}, Lefj;->ab()I

    move-result v5

    .line 611
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 613
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, p0, Lefu;->e:I

    .line 614
    invoke-static {}, Lefj;->ac()I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 615
    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 563
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 555
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 557
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lhya;->a(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 623
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 624
    check-cast p1, Landroid/widget/TextView;

    .line 625
    new-instance v0, Llji;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Llji;-><init>(II)V

    .line 627
    iput-boolean v3, v0, Llji;->a:Z

    .line 628
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 629
    const v0, 0x7f0a09c9

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 630
    iget-object v0, p0, Lefu;->a:Lefj;

    invoke-static {v0}, Lefj;->i(Lefj;)Llcr;

    move-result-object v0

    iget v0, v0, Llcr;->f:I

    invoke-static {}, Lefj;->ad()I

    move-result v1

    add-int/2addr v0, v1

    .line 631
    invoke-static {}, Lefj;->ae()I

    move-result v1

    iget-object v2, p0, Lefu;->a:Lefj;

    invoke-static {v2}, Lefj;->i(Lefj;)Llcr;

    move-result-object v2

    iget v2, v2, Llcr;->f:I

    .line 630
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 632
    iget-object v0, p0, Lefu;->k:Landroid/content/Context;

    const/16 v1, 0x11

    invoke-static {v0, p1, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 634
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 9

    .prologue
    const/4 v0, 0x3

    const/4 v3, -0x1

    const/4 v4, 0x1

    const/4 v1, 0x2

    const/4 v6, 0x0

    .line 680
    packed-switch p2, :pswitch_data_0

    .line 714
    :goto_0
    :pswitch_0
    return-void

    .line 685
    :pswitch_1
    new-instance v3, Lfzh;

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 686
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-direct {v3, v2, v4}, Lfzh;-><init>(II)V

    .line 688
    invoke-interface {p3, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 689
    new-instance v4, Lfzh;

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 690
    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-direct {v4, v1, v0}, Lfzh;-><init>(II)V

    :goto_1
    move-object v0, p1

    .line 695
    check-cast v0, Lfzg;

    .line 696
    iget-object v1, p0, Lefu;->a:Lefj;

    invoke-static {v1}, Lefj;->i(Lefj;)Llcr;

    move-result-object v1

    iget v1, v1, Llcr;->d:I

    invoke-virtual {v0, v6, v1, v6, v6}, Lfzg;->setPadding(IIII)V

    .line 698
    iget-object v1, p0, Lefu;->b:Ljava/util/ArrayList;

    iget-object v2, p0, Lefu;->c:Lefr;

    iget-object v5, p0, Lefu;->a:Lefj;

    .line 699
    invoke-static {v5}, Lefj;->k(Lefj;)Lhxh;

    move-result-object v5

    iget-object v6, p0, Lefu;->a:Lefj;

    iget-object v7, p0, Lefu;->a:Lefj;

    invoke-static {v7}, Lefj;->l(Lefj;)Ljava/lang/String;

    move-result-object v7

    .line 698
    invoke-virtual/range {v0 .. v7}, Lfzg;->a(Ljava/util/ArrayList;Lefr;Lfzh;Lfzh;Lhxh;Lfzi;Ljava/lang/String;)V

    goto :goto_0

    .line 692
    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 703
    :pswitch_2
    check-cast p1, Lfwr;

    .line 705
    iget-object v2, p0, Lefu;->a:Lefj;

    invoke-static {v2}, Lefj;->i(Lefj;)Llcr;

    move-result-object v2

    iget v2, v2, Llcr;->d:I

    invoke-virtual {p1, v6, v2, v6, v6}, Lfwr;->setPadding(IIII)V

    .line 707
    invoke-interface {p3, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    move v2, v3

    :goto_2
    invoke-interface {p3, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v4

    :goto_3
    invoke-interface {p3, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    goto :goto_2

    :cond_2
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    goto :goto_3

    :cond_3
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    new-array v8, v0, [I

    move v5, v6

    :goto_4
    if-ge v5, v0, :cond_4

    packed-switch v5, :pswitch_data_1

    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :pswitch_3
    aput v7, v8, v6

    goto :goto_5

    :pswitch_4
    aput v3, v8, v4

    goto :goto_5

    :pswitch_5
    aput v2, v8, v1

    goto :goto_5

    .line 708
    :cond_4
    iget-object v0, p0, Lefu;->d:Landroid/database/Cursor;

    iget-object v1, p0, Lefu;->a:Lefj;

    invoke-virtual {p1, v0, v8, v1}, Lfwr;->a(Landroid/database/Cursor;[ILfws;)V

    goto/16 :goto_0

    .line 680
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    .line 707
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Left;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 495
    invoke-virtual {p0, v2}, Lefu;->l(Z)V

    .line 496
    iget-boolean v0, p0, Lefu;->f:Z

    if-eqz v0, :cond_0

    .line 498
    invoke-static {}, Lfdj;->ar()Lhym;

    move-result-object v0

    .line 497
    invoke-virtual {p0, v2, v0}, Lefu;->a(ILandroid/database/Cursor;)V

    .line 502
    :goto_0
    if-nez p1, :cond_1

    .line 503
    iput-object v1, p0, Lefu;->b:Ljava/util/ArrayList;

    .line 504
    iput-object v1, p0, Lefu;->c:Lefr;

    .line 505
    invoke-virtual {p0, v3, v1}, Lefu;->a(ILandroid/database/Cursor;)V

    .line 506
    iput-object v1, p0, Lefu;->d:Landroid/database/Cursor;

    .line 507
    iput v2, p0, Lefu;->e:I

    .line 508
    invoke-virtual {p0, v4, v1}, Lefu;->a(ILandroid/database/Cursor;)V

    .line 509
    invoke-virtual {p0, v5, v1}, Lefu;->a(ILandroid/database/Cursor;)V

    .line 521
    :goto_1
    invoke-virtual {p0, v3}, Lefu;->l(Z)V

    .line 522
    return-void

    .line 500
    :cond_0
    invoke-virtual {p0, v2, v1}, Lefu;->a(ILandroid/database/Cursor;)V

    goto :goto_0

    .line 512
    :cond_1
    iget-object v0, p1, Left;->c:Ljava/util/ArrayList;

    iput-object v0, p0, Lefu;->b:Ljava/util/ArrayList;

    .line 513
    iget-object v0, p1, Left;->d:Lefr;

    iput-object v0, p0, Lefu;->c:Lefr;

    .line 515
    iget-object v0, p1, Left;->a:Lhym;

    invoke-virtual {p0, v3, v0}, Lefu;->a(ILandroid/database/Cursor;)V

    .line 516
    iget-object v0, p1, Left;->e:Landroid/database/Cursor;

    iput-object v0, p0, Lefu;->d:Landroid/database/Cursor;

    .line 517
    iget v0, p1, Left;->f:I

    iput v0, p0, Lefu;->e:I

    .line 518
    iget-object v0, p1, Left;->b:Lhym;

    invoke-virtual {p0, v4, v0}, Lefu;->a(ILandroid/database/Cursor;)V

    .line 519
    invoke-static {}, Lefj;->Z()Lhym;

    move-result-object v0

    invoke-virtual {p0, v5, v0}, Lefu;->a(ILandroid/database/Cursor;)V

    goto :goto_1
.end method

.method public b()V
    .locals 4

    .prologue
    .line 525
    iget-object v0, p0, Lefu;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 526
    iget-object v0, p0, Lefu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 527
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 528
    iget-object v0, p0, Lefu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvw;

    .line 529
    invoke-virtual {v0}, Ldvw;->b()V

    .line 530
    invoke-static {v0}, Lemi;->a(Ldvw;)Z

    .line 533
    iget-object v0, v0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lefu;->a:Lefj;

    invoke-static {v0}, Lefj;->g(Lefj;)Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->setEnabled(Z)V

    .line 527
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 538
    :cond_1
    iget-object v0, p0, Lefu;->c:Lefr;

    if-eqz v0, :cond_3

    .line 539
    iget-object v0, p0, Lefu;->c:Lefr;

    iget-object v0, v0, Lefr;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 540
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 541
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loiu;

    .line 542
    iget-object v0, v0, Loiu;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    .line 543
    invoke-static {v0}, Lemi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 544
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 548
    :cond_3
    iget-object v0, p0, Lefu;->a:Lefj;

    invoke-static {v0}, Lefj;->h(Lefj;)V

    .line 549
    return-void
.end method

.method public b(II)Z
    .locals 1

    .prologue
    .line 723
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 718
    invoke-super {p0}, Lhya;->getCount()I

    move-result v0

    return v0
.end method
