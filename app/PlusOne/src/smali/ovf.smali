.class public final Lovf;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Louz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 437
    invoke-direct {p0}, Loxq;-><init>()V

    .line 440
    sget-object v0, Louz;->a:[Louz;

    iput-object v0, p0, Lovf;->a:[Louz;

    .line 437
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 458
    .line 459
    iget-object v1, p0, Lovf;->a:[Louz;

    if-eqz v1, :cond_1

    .line 460
    iget-object v2, p0, Lovf;->a:[Louz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 461
    if-eqz v4, :cond_0

    .line 462
    const/4 v5, 0x1

    .line 463
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 460
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 467
    :cond_1
    iget-object v1, p0, Lovf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 468
    iput v0, p0, Lovf;->ai:I

    .line 469
    return v0
.end method

.method public a(Loxn;)Lovf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 477
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 478
    sparse-switch v0, :sswitch_data_0

    .line 482
    iget-object v2, p0, Lovf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 483
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lovf;->ah:Ljava/util/List;

    .line 486
    :cond_1
    iget-object v2, p0, Lovf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    :sswitch_0
    return-object p0

    .line 493
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 494
    iget-object v0, p0, Lovf;->a:[Louz;

    if-nez v0, :cond_3

    move v0, v1

    .line 495
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Louz;

    .line 496
    iget-object v3, p0, Lovf;->a:[Louz;

    if-eqz v3, :cond_2

    .line 497
    iget-object v3, p0, Lovf;->a:[Louz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 499
    :cond_2
    iput-object v2, p0, Lovf;->a:[Louz;

    .line 500
    :goto_2
    iget-object v2, p0, Lovf;->a:[Louz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 501
    iget-object v2, p0, Lovf;->a:[Louz;

    new-instance v3, Louz;

    invoke-direct {v3}, Louz;-><init>()V

    aput-object v3, v2, v0

    .line 502
    iget-object v2, p0, Lovf;->a:[Louz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 503
    invoke-virtual {p1}, Loxn;->a()I

    .line 500
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 494
    :cond_3
    iget-object v0, p0, Lovf;->a:[Louz;

    array-length v0, v0

    goto :goto_1

    .line 506
    :cond_4
    iget-object v2, p0, Lovf;->a:[Louz;

    new-instance v3, Louz;

    invoke-direct {v3}, Louz;-><init>()V

    aput-object v3, v2, v0

    .line 507
    iget-object v2, p0, Lovf;->a:[Louz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 478
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 445
    iget-object v0, p0, Lovf;->a:[Louz;

    if-eqz v0, :cond_1

    .line 446
    iget-object v1, p0, Lovf;->a:[Louz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 447
    if-eqz v3, :cond_0

    .line 448
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 446
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 452
    :cond_1
    iget-object v0, p0, Lovf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 454
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0, p1}, Lovf;->a(Loxn;)Lovf;

    move-result-object v0

    return-object v0
.end method
