.class final Lebr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lebp;


# direct methods
.method constructor <init>(Lebp;)V
    .locals 0

    .prologue
    .line 413
    iput-object p1, p0, Lebr;->a:Lebp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 416
    iget-object v0, p0, Lebr;->a:Lebp;

    invoke-static {v0}, Lebp;->a(Lebp;)Landroid/content/Context;

    move-result-object v0

    .line 418
    new-instance v1, Lfdd;

    iget-object v2, p0, Lebr;->a:Lebp;

    invoke-static {v2}, Lebp;->b(Lebp;)I

    move-result v2

    iget-object v3, p0, Lebr;->a:Lebp;

    invoke-static {v3}, Lebp;->c(Lebp;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lfdd;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    return-object v1
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 423
    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 424
    iget-object v0, p0, Lebr;->a:Lebp;

    const/16 v1, 0xc

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Lebp;->a(Lebp;J)J

    .line 426
    const/16 v0, 0xe

    .line 427
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 426
    invoke-static {v0}, Ljux;->a([B)Ljux;

    move-result-object v0

    .line 429
    invoke-virtual {v0}, Ljux;->c()Lnzx;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 430
    invoke-virtual {v0}, Ljux;->c()Lnzx;

    move-result-object v0

    sget-object v1, Lnzr;->a:Loxr;

    invoke-virtual {v0, v1}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    .line 433
    iget-object v1, p0, Lebr;->a:Lebp;

    invoke-static {v1}, Lebp;->d(Lebp;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 434
    iget-object v1, p0, Lebr;->a:Lebp;

    invoke-static {v1}, Lebp;->e(Lebp;)Landroid/widget/CheckedTextView;

    move-result-object v1

    iget-object v4, v0, Lnzr;->e:Lnxo;

    iget-object v4, v4, Lnxo;->a:Lnxp;

    iget-object v4, v4, Lnxp;->a:Ljava/lang/Boolean;

    .line 435
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 434
    invoke-virtual {v1, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 436
    iget-object v1, p0, Lebr;->a:Lebp;

    invoke-static {v1}, Lebp;->f(Lebp;)Landroid/widget/CheckedTextView;

    move-result-object v4

    iget-object v1, v0, Lnzr;->e:Lnxo;

    iget-object v1, v1, Lnxo;->a:Lnxp;

    iget-object v1, v1, Lnxp;->b:Ljava/lang/Boolean;

    .line 437
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    .line 436
    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 439
    :cond_0
    iget-object v1, p0, Lebr;->a:Lebp;

    const-wide v4, 0x8000000000L

    invoke-static {v1, v4, v5}, Lebp;->b(Lebp;J)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 440
    iget-object v1, p0, Lebr;->a:Lebp;

    iget-object v4, v0, Lnzr;->b:Lnxr;

    iget-object v4, v4, Lnxr;->k:Ljava/lang/String;

    invoke-static {v1, v4}, Lebp;->a(Lebp;Ljava/lang/String;)Ljava/lang/String;

    .line 444
    :goto_1
    iget-object v1, p0, Lebr;->a:Lebp;

    iget-object v4, p0, Lebr;->a:Lebp;

    invoke-static {v4}, Lebp;->g(Lebp;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, v0, Lnzr;->b:Lnxr;

    iget-object v0, v0, Lnxr;->l:Lnxl;

    iget-object v0, v0, Lnxl;->b:Lnxm;

    if-eqz v0, :cond_1

    move v3, v2

    :cond_1
    invoke-static {v1, v3}, Lebp;->a(Lebp;Z)Z

    .line 447
    :cond_2
    iget-object v0, p0, Lebr;->a:Lebp;

    invoke-static {v0}, Lebp;->h(Lebp;)V

    .line 448
    iget-object v0, p0, Lebr;->a:Lebp;

    invoke-static {v0, v2}, Lebp;->b(Lebp;Z)Z

    .line 450
    :cond_3
    return-void

    :cond_4
    move v1, v3

    .line 437
    goto :goto_0

    .line 442
    :cond_5
    iget-object v1, p0, Lebr;->a:Lebp;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lebp;->a(Lebp;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 453
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 413
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lebr;->a(Landroid/database/Cursor;)V

    return-void
.end method
