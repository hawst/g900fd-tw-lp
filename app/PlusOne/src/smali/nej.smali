.class public final Lnej;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field private d:[Lnek;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1796
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1893
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnej;->b:[Ljava/lang/String;

    .line 1896
    sget-object v0, Lnek;->a:[Lnek;

    iput-object v0, p0, Lnej;->d:[Lnek;

    .line 1901
    const/high16 v0, -0x80000000

    iput v0, p0, Lnej;->e:I

    .line 1796
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1931
    const/4 v0, 0x1

    iget-object v2, p0, Lnej;->a:Ljava/lang/String;

    .line 1933
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1934
    iget-object v2, p0, Lnej;->b:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lnej;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1936
    iget-object v4, p0, Lnej;->b:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 1938
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1936
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1940
    :cond_0
    add-int/2addr v0, v3

    .line 1941
    iget-object v2, p0, Lnej;->b:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1943
    :cond_1
    iget-object v2, p0, Lnej;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 1944
    const/4 v2, 0x3

    iget-object v3, p0, Lnej;->c:Ljava/lang/Boolean;

    .line 1945
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1947
    :cond_2
    iget v2, p0, Lnej;->e:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    .line 1948
    const/4 v2, 0x4

    iget v3, p0, Lnej;->e:I

    .line 1949
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1951
    :cond_3
    iget-object v2, p0, Lnej;->d:[Lnek;

    if-eqz v2, :cond_5

    .line 1952
    iget-object v2, p0, Lnej;->d:[Lnek;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 1953
    if-eqz v4, :cond_4

    .line 1954
    const/4 v5, 0x5

    .line 1955
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1952
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1959
    :cond_5
    iget-object v1, p0, Lnej;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1960
    iput v0, p0, Lnej;->ai:I

    .line 1961
    return v0
.end method

.method public a(Loxn;)Lnej;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1969
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1970
    sparse-switch v0, :sswitch_data_0

    .line 1974
    iget-object v2, p0, Lnej;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1975
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnej;->ah:Ljava/util/List;

    .line 1978
    :cond_1
    iget-object v2, p0, Lnej;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1980
    :sswitch_0
    return-object p0

    .line 1985
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnej;->a:Ljava/lang/String;

    goto :goto_0

    .line 1989
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1990
    iget-object v0, p0, Lnej;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 1991
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1992
    iget-object v3, p0, Lnej;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1993
    iput-object v2, p0, Lnej;->b:[Ljava/lang/String;

    .line 1994
    :goto_1
    iget-object v2, p0, Lnej;->b:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 1995
    iget-object v2, p0, Lnej;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1996
    invoke-virtual {p1}, Loxn;->a()I

    .line 1994
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1999
    :cond_2
    iget-object v2, p0, Lnej;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 2003
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnej;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 2007
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2008
    if-eqz v0, :cond_3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_3

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 2011
    :cond_3
    iput v0, p0, Lnej;->e:I

    goto :goto_0

    .line 2013
    :cond_4
    iput v1, p0, Lnej;->e:I

    goto :goto_0

    .line 2018
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2019
    iget-object v0, p0, Lnej;->d:[Lnek;

    if-nez v0, :cond_6

    move v0, v1

    .line 2020
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lnek;

    .line 2021
    iget-object v3, p0, Lnej;->d:[Lnek;

    if-eqz v3, :cond_5

    .line 2022
    iget-object v3, p0, Lnej;->d:[Lnek;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2024
    :cond_5
    iput-object v2, p0, Lnej;->d:[Lnek;

    .line 2025
    :goto_3
    iget-object v2, p0, Lnej;->d:[Lnek;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 2026
    iget-object v2, p0, Lnej;->d:[Lnek;

    new-instance v3, Lnek;

    invoke-direct {v3}, Lnek;-><init>()V

    aput-object v3, v2, v0

    .line 2027
    iget-object v2, p0, Lnej;->d:[Lnek;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2028
    invoke-virtual {p1}, Loxn;->a()I

    .line 2025
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2019
    :cond_6
    iget-object v0, p0, Lnej;->d:[Lnek;

    array-length v0, v0

    goto :goto_2

    .line 2031
    :cond_7
    iget-object v2, p0, Lnej;->d:[Lnek;

    new-instance v3, Lnek;

    invoke-direct {v3}, Lnek;-><init>()V

    aput-object v3, v2, v0

    .line 2032
    iget-object v2, p0, Lnej;->d:[Lnek;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1970
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1906
    const/4 v1, 0x1

    iget-object v2, p0, Lnej;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1907
    iget-object v1, p0, Lnej;->b:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1908
    iget-object v2, p0, Lnej;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1909
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1908
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1912
    :cond_0
    iget-object v1, p0, Lnej;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1913
    const/4 v1, 0x3

    iget-object v2, p0, Lnej;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 1915
    :cond_1
    iget v1, p0, Lnej;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 1916
    const/4 v1, 0x4

    iget v2, p0, Lnej;->e:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1918
    :cond_2
    iget-object v1, p0, Lnej;->d:[Lnek;

    if-eqz v1, :cond_4

    .line 1919
    iget-object v1, p0, Lnej;->d:[Lnek;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 1920
    if-eqz v3, :cond_3

    .line 1921
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1919
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1925
    :cond_4
    iget-object v0, p0, Lnej;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1927
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1792
    invoke-virtual {p0, p1}, Lnej;->a(Loxn;)Lnej;

    move-result-object v0

    return-object v0
.end method
