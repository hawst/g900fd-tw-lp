.class public final Lnfo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lnzx;

.field private c:[Lnyb;

.field private d:[Lnyb;

.field private e:Ljava/lang/Integer;

.field private f:Lnyz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 866
    invoke-direct {p0}, Loxq;-><init>()V

    .line 869
    sget-object v0, Lnyb;->a:[Lnyb;

    iput-object v0, p0, Lnfo;->c:[Lnyb;

    .line 872
    sget-object v0, Lnyb;->a:[Lnyb;

    iput-object v0, p0, Lnfo;->d:[Lnyb;

    .line 879
    const/4 v0, 0x0

    iput-object v0, p0, Lnfo;->f:Lnyz;

    .line 882
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lnfo;->b:[Lnzx;

    .line 866
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 923
    .line 924
    iget-object v0, p0, Lnfo;->c:[Lnyb;

    if-eqz v0, :cond_1

    .line 925
    iget-object v3, p0, Lnfo;->c:[Lnyb;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 926
    if-eqz v5, :cond_0

    .line 927
    const/4 v6, 0x1

    .line 928
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 925
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 932
    :cond_2
    iget-object v2, p0, Lnfo;->d:[Lnyb;

    if-eqz v2, :cond_4

    .line 933
    iget-object v3, p0, Lnfo;->d:[Lnyb;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 934
    if-eqz v5, :cond_3

    .line 935
    const/4 v6, 0x2

    .line 936
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 933
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 940
    :cond_4
    iget-object v2, p0, Lnfo;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 941
    const/4 v2, 0x3

    iget-object v3, p0, Lnfo;->e:Ljava/lang/Integer;

    .line 942
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 944
    :cond_5
    iget-object v2, p0, Lnfo;->f:Lnyz;

    if-eqz v2, :cond_6

    .line 945
    const/4 v2, 0x4

    iget-object v3, p0, Lnfo;->f:Lnyz;

    .line 946
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 948
    :cond_6
    iget-object v2, p0, Lnfo;->a:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 949
    const/4 v2, 0x5

    iget-object v3, p0, Lnfo;->a:Ljava/lang/String;

    .line 950
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 952
    :cond_7
    iget-object v2, p0, Lnfo;->b:[Lnzx;

    if-eqz v2, :cond_9

    .line 953
    iget-object v2, p0, Lnfo;->b:[Lnzx;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 954
    if-eqz v4, :cond_8

    .line 955
    const/4 v5, 0x6

    .line 956
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 953
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 960
    :cond_9
    iget-object v1, p0, Lnfo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 961
    iput v0, p0, Lnfo;->ai:I

    .line 962
    return v0
.end method

.method public a(Loxn;)Lnfo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 970
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 971
    sparse-switch v0, :sswitch_data_0

    .line 975
    iget-object v2, p0, Lnfo;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 976
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnfo;->ah:Ljava/util/List;

    .line 979
    :cond_1
    iget-object v2, p0, Lnfo;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 981
    :sswitch_0
    return-object p0

    .line 986
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 987
    iget-object v0, p0, Lnfo;->c:[Lnyb;

    if-nez v0, :cond_3

    move v0, v1

    .line 988
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyb;

    .line 989
    iget-object v3, p0, Lnfo;->c:[Lnyb;

    if-eqz v3, :cond_2

    .line 990
    iget-object v3, p0, Lnfo;->c:[Lnyb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 992
    :cond_2
    iput-object v2, p0, Lnfo;->c:[Lnyb;

    .line 993
    :goto_2
    iget-object v2, p0, Lnfo;->c:[Lnyb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 994
    iget-object v2, p0, Lnfo;->c:[Lnyb;

    new-instance v3, Lnyb;

    invoke-direct {v3}, Lnyb;-><init>()V

    aput-object v3, v2, v0

    .line 995
    iget-object v2, p0, Lnfo;->c:[Lnyb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 996
    invoke-virtual {p1}, Loxn;->a()I

    .line 993
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 987
    :cond_3
    iget-object v0, p0, Lnfo;->c:[Lnyb;

    array-length v0, v0

    goto :goto_1

    .line 999
    :cond_4
    iget-object v2, p0, Lnfo;->c:[Lnyb;

    new-instance v3, Lnyb;

    invoke-direct {v3}, Lnyb;-><init>()V

    aput-object v3, v2, v0

    .line 1000
    iget-object v2, p0, Lnfo;->c:[Lnyb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1004
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1005
    iget-object v0, p0, Lnfo;->d:[Lnyb;

    if-nez v0, :cond_6

    move v0, v1

    .line 1006
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyb;

    .line 1007
    iget-object v3, p0, Lnfo;->d:[Lnyb;

    if-eqz v3, :cond_5

    .line 1008
    iget-object v3, p0, Lnfo;->d:[Lnyb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1010
    :cond_5
    iput-object v2, p0, Lnfo;->d:[Lnyb;

    .line 1011
    :goto_4
    iget-object v2, p0, Lnfo;->d:[Lnyb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 1012
    iget-object v2, p0, Lnfo;->d:[Lnyb;

    new-instance v3, Lnyb;

    invoke-direct {v3}, Lnyb;-><init>()V

    aput-object v3, v2, v0

    .line 1013
    iget-object v2, p0, Lnfo;->d:[Lnyb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1014
    invoke-virtual {p1}, Loxn;->a()I

    .line 1011
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1005
    :cond_6
    iget-object v0, p0, Lnfo;->d:[Lnyb;

    array-length v0, v0

    goto :goto_3

    .line 1017
    :cond_7
    iget-object v2, p0, Lnfo;->d:[Lnyb;

    new-instance v3, Lnyb;

    invoke-direct {v3}, Lnyb;-><init>()V

    aput-object v3, v2, v0

    .line 1018
    iget-object v2, p0, Lnfo;->d:[Lnyb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1022
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnfo;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1026
    :sswitch_4
    iget-object v0, p0, Lnfo;->f:Lnyz;

    if-nez v0, :cond_8

    .line 1027
    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    iput-object v0, p0, Lnfo;->f:Lnyz;

    .line 1029
    :cond_8
    iget-object v0, p0, Lnfo;->f:Lnyz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1033
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnfo;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 1037
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1038
    iget-object v0, p0, Lnfo;->b:[Lnzx;

    if-nez v0, :cond_a

    move v0, v1

    .line 1039
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 1040
    iget-object v3, p0, Lnfo;->b:[Lnzx;

    if-eqz v3, :cond_9

    .line 1041
    iget-object v3, p0, Lnfo;->b:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1043
    :cond_9
    iput-object v2, p0, Lnfo;->b:[Lnzx;

    .line 1044
    :goto_6
    iget-object v2, p0, Lnfo;->b:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 1045
    iget-object v2, p0, Lnfo;->b:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 1046
    iget-object v2, p0, Lnfo;->b:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1047
    invoke-virtual {p1}, Loxn;->a()I

    .line 1044
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1038
    :cond_a
    iget-object v0, p0, Lnfo;->b:[Lnzx;

    array-length v0, v0

    goto :goto_5

    .line 1050
    :cond_b
    iget-object v2, p0, Lnfo;->b:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 1051
    iget-object v2, p0, Lnfo;->b:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 971
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 887
    iget-object v1, p0, Lnfo;->c:[Lnyb;

    if-eqz v1, :cond_1

    .line 888
    iget-object v2, p0, Lnfo;->c:[Lnyb;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 889
    if-eqz v4, :cond_0

    .line 890
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 888
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 894
    :cond_1
    iget-object v1, p0, Lnfo;->d:[Lnyb;

    if-eqz v1, :cond_3

    .line 895
    iget-object v2, p0, Lnfo;->d:[Lnyb;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 896
    if-eqz v4, :cond_2

    .line 897
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 895
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 901
    :cond_3
    iget-object v1, p0, Lnfo;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 902
    const/4 v1, 0x3

    iget-object v2, p0, Lnfo;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 904
    :cond_4
    iget-object v1, p0, Lnfo;->f:Lnyz;

    if-eqz v1, :cond_5

    .line 905
    const/4 v1, 0x4

    iget-object v2, p0, Lnfo;->f:Lnyz;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 907
    :cond_5
    iget-object v1, p0, Lnfo;->a:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 908
    const/4 v1, 0x5

    iget-object v2, p0, Lnfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 910
    :cond_6
    iget-object v1, p0, Lnfo;->b:[Lnzx;

    if-eqz v1, :cond_8

    .line 911
    iget-object v1, p0, Lnfo;->b:[Lnzx;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 912
    if-eqz v3, :cond_7

    .line 913
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 911
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 917
    :cond_8
    iget-object v0, p0, Lnfo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 919
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 862
    invoke-virtual {p0, p1}, Lnfo;->a(Loxn;)Lnfo;

    move-result-object v0

    return-object v0
.end method
