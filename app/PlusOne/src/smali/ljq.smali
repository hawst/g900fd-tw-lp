.class public Lljq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/widget/Scroller;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lljq;->a:Landroid/widget/Scroller;

    .line 52
    return-void
.end method

.method public static a(Landroid/content/Context;)Lljq;
    .locals 2

    .prologue
    .line 44
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 45
    new-instance v0, Lljr;

    invoke-direct {v0, p0}, Lljr;-><init>(Landroid/content/Context;)V

    .line 47
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lljq;

    invoke-direct {v0, p0}, Lljq;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public a(IIIIIIII)V
    .locals 9

    .prologue
    .line 170
    iget-object v0, p0, Lljq;->a:Landroid/widget/Scroller;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 171
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lljq;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lljq;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lljq;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    return v0
.end method

.method public d()F
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lljq;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lljq;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 180
    return-void
.end method
