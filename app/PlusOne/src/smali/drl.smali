.class public final Ldrl;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ldrl;->a:Ljava/lang/Object;

    .line 42
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "generation"

    aput-object v2, v0, v1

    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/util/List;)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Licl;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 103
    const/4 v1, 0x0

    .line 104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 106
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 107
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 109
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Licl;

    .line 110
    invoke-static {v0}, Ldrl;->a(Licl;)Landroid/content/ContentValues;

    move-result-object v6

    .line 111
    if-eqz v6, :cond_0

    .line 112
    const-string v7, "emotishare_data"

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 113
    add-int/lit8 v1, v1, 0x1

    .line 115
    :cond_0
    const/4 v6, 0x3

    const-string v7, "EsEmotiShareData"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x8

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Insert: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v7, v0}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 118
    :cond_1
    :try_start_1
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 123
    invoke-static {p0, p1, v2, v3}, Ldrl;->a(Landroid/content/Context;IJ)V

    .line 125
    if-eqz v1, :cond_2

    .line 126
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->i:Landroid/net/Uri;

    invoke-virtual {v0, v2, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 129
    :cond_2
    return v1
.end method

.method private static a(Landroid/content/Context;I)J
    .locals 3

    .prologue
    .line 289
    .line 290
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 292
    :try_start_0
    const-string v1, "SELECT last_emotishare_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 296
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static a(Licl;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 65
    if-nez p0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-object v0

    .line 68
    :cond_1
    invoke-virtual {p0}, Licl;->b()Licj;

    move-result-object v1

    .line 69
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Licj;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    :try_start_0
    invoke-static {p0}, Licl;->a(Licl;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 79
    if-eqz v2, :cond_0

    .line 83
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 84
    const-string v3, "type"

    invoke-virtual {v1}, Licj;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 85
    const-string v1, "data"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 86
    const-string v1, "generation"

    invoke-virtual {p0}, Licl;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 77
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;IJ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 272
    .line 273
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 275
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 276
    const-string v2, "last_emotishare_sync_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 277
    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 279
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 280
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 307
    const-string v0, "emotishare_data"

    invoke-virtual {p0, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 308
    const/4 v1, 0x3

    const-string v2, "EsEmotiShareData"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "cleanupData deleted EmotiShares: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method private static a(Landroid/content/Context;ILkfp;)Z
    .locals 25

    .prologue
    .line 160
    invoke-virtual/range {p2 .. p2}, Lkfp;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 161
    const/4 v4, 0x0

    .line 181
    :goto_0
    return v4

    .line 164
    :cond_0
    const-string v4, "EmotiShare"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lkfp;->c(Ljava/lang/String;)V

    .line 168
    invoke-static/range {p0 .. p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v4

    invoke-virtual {v4}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 169
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 171
    :try_start_0
    invoke-static {v5}, Ldrl;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 172
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 176
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f001b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v12

    const v5, 0x7f0f001c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v13

    const v5, 0x7f0f001d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v14

    const v5, 0x7f0f001e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v15

    const v5, 0x7f0f001f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v16

    const v5, 0x7f0f0020

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    const v5, 0x7f0f0021

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v18

    const v5, 0x7f0f0022

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v19

    const v5, 0x7f0f0023

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v20

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    const/4 v4, 0x3

    new-array v0, v4, [J

    move-object/from16 v21, v0

    const/4 v4, 0x0

    sget-object v5, Lfvc;->c:Lfvc;

    invoke-virtual {v5}, Lfvc;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v21, v4

    const/4 v4, 0x1

    sget-object v5, Lfvc;->d:Lfvc;

    invoke-virtual {v5}, Lfvc;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v21, v4

    const/4 v4, 0x2

    sget-object v5, Lfvc;->e:Lfvc;

    invoke-virtual {v5}, Lfvc;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v21, v4

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    move v11, v4

    :goto_1
    array-length v4, v12

    if-ge v11, v4, :cond_2

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    aget-object v4, v16, v11

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v9, Licj;

    aget v4, v15, v11

    aget-object v5, v14, v11

    aget-object v7, v20, v11

    aget-object v8, v18, v11

    invoke-direct {v9, v4, v5, v7, v8}, Licj;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Licl;

    aget v5, v12, v11

    aget-object v7, v17, v11

    aget-object v8, v19, v11

    aget v10, v13, v11

    invoke-direct/range {v4 .. v10}, Licl;-><init>(ILjava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Licj;I)V

    aget v5, v13, v11

    add-int/lit8 v5, v5, -0x1

    if-ltz v5, :cond_1

    const/4 v6, 0x3

    if-ge v5, v6, :cond_1

    aget-wide v6, v21, v5

    cmp-long v5, v22, v6

    if-ltz v5, :cond_1

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v11, 0x1

    move v11, v4

    goto :goto_1

    .line 174
    :catchall_0
    move-exception v4

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4

    .line 178
    :cond_2
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Ldrl;->a(Landroid/content/Context;ILjava/util/List;)I

    move-result v4

    .line 179
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lkfp;->m(I)V

    .line 181
    const/4 v4, 0x1

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;ILkfp;Z)Z
    .locals 6

    .prologue
    .line 244
    sget-object v1, Ldrl;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 245
    if-nez p3, :cond_0

    .line 246
    :try_start_0
    invoke-static {p0, p1}, Ldrl;->a(Landroid/content/Context;I)J

    move-result-wide v2

    .line 247
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 248
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    const-wide/32 v4, 0xea60

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 250
    const/4 v0, 0x1

    monitor-exit v1

    .line 260
    :goto_0
    return v0

    .line 254
    :cond_0
    invoke-static {p0, p1, p2}, Ldrl;->a(Landroid/content/Context;ILkfp;)Z

    move-result v0

    .line 256
    if-eqz v0, :cond_1

    .line 257
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {p0, p1, v2, v3}, Ldrl;->a(Landroid/content/Context;IJ)V

    .line 260
    :cond_1
    monitor-exit v1

    goto :goto_0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
