.class public final Lmwt;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Long;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 9214
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9242
    iput v0, p0, Lmwt;->d:I

    .line 9245
    iput v0, p0, Lmwt;->e:I

    .line 9214
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 9271
    const/4 v0, 0x0

    .line 9272
    iget-object v1, p0, Lmwt;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 9273
    const/4 v0, 0x1

    iget-object v1, p0, Lmwt;->a:Ljava/lang/Long;

    .line 9274
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9276
    :cond_0
    iget-object v1, p0, Lmwt;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 9277
    const/4 v1, 0x2

    iget-object v2, p0, Lmwt;->b:Ljava/lang/Long;

    .line 9278
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9280
    :cond_1
    iget v1, p0, Lmwt;->d:I

    if-eq v1, v4, :cond_2

    .line 9281
    const/4 v1, 0x3

    iget v2, p0, Lmwt;->d:I

    .line 9282
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9284
    :cond_2
    iget v1, p0, Lmwt;->e:I

    if-eq v1, v4, :cond_3

    .line 9285
    const/4 v1, 0x4

    iget v2, p0, Lmwt;->e:I

    .line 9286
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9288
    :cond_3
    iget-object v1, p0, Lmwt;->c:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 9289
    const/4 v1, 0x5

    iget-object v2, p0, Lmwt;->c:Ljava/lang/Long;

    .line 9290
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9292
    :cond_4
    iget-object v1, p0, Lmwt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9293
    iput v0, p0, Lmwt;->ai:I

    .line 9294
    return v0
.end method

.method public a(Loxn;)Lmwt;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9302
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9303
    sparse-switch v0, :sswitch_data_0

    .line 9307
    iget-object v1, p0, Lmwt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9308
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwt;->ah:Ljava/util/List;

    .line 9311
    :cond_1
    iget-object v1, p0, Lmwt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9313
    :sswitch_0
    return-object p0

    .line 9318
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwt;->a:Ljava/lang/Long;

    goto :goto_0

    .line 9322
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwt;->b:Ljava/lang/Long;

    goto :goto_0

    .line 9326
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 9327
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-ne v0, v4, :cond_3

    .line 9330
    :cond_2
    iput v0, p0, Lmwt;->d:I

    goto :goto_0

    .line 9332
    :cond_3
    iput v2, p0, Lmwt;->d:I

    goto :goto_0

    .line 9337
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 9338
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-ne v0, v1, :cond_5

    .line 9348
    :cond_4
    iput v0, p0, Lmwt;->e:I

    goto :goto_0

    .line 9350
    :cond_5
    iput v2, p0, Lmwt;->e:I

    goto :goto_0

    .line 9355
    :sswitch_5
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwt;->c:Ljava/lang/Long;

    goto :goto_0

    .line 9303
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 9250
    iget-object v0, p0, Lmwt;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 9251
    const/4 v0, 0x1

    iget-object v1, p0, Lmwt;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 9253
    :cond_0
    iget-object v0, p0, Lmwt;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 9254
    const/4 v0, 0x2

    iget-object v1, p0, Lmwt;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 9256
    :cond_1
    iget v0, p0, Lmwt;->d:I

    if-eq v0, v4, :cond_2

    .line 9257
    const/4 v0, 0x3

    iget v1, p0, Lmwt;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9259
    :cond_2
    iget v0, p0, Lmwt;->e:I

    if-eq v0, v4, :cond_3

    .line 9260
    const/4 v0, 0x4

    iget v1, p0, Lmwt;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 9262
    :cond_3
    iget-object v0, p0, Lmwt;->c:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 9263
    const/4 v0, 0x5

    iget-object v1, p0, Lmwt;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 9265
    :cond_4
    iget-object v0, p0, Lmwt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9267
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9210
    invoke-virtual {p0, p1}, Lmwt;->a(Loxn;)Lmwt;

    move-result-object v0

    return-object v0
.end method
