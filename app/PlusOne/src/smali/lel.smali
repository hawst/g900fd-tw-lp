.class public final Llel;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Llcz;
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Llci;

.field private c:Lidf;

.field private d:Ljava/lang/String;

.field private e:Landroid/text/StaticLayout;

.field private f:Landroid/text/StaticLayout;

.field private g:Landroid/text/StaticLayout;

.field private h:Landroid/text/StaticLayout;

.field private i:Landroid/text/StaticLayout;

.field private j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private k:I

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Llel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Llel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 74
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    sget-object v0, Llel;->a:Llct;

    if-nez v0, :cond_0

    .line 77
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Llel;->a:Llct;

    .line 80
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Llel;->setWillNotDraw(Z)V

    .line 81
    invoke-virtual {p0, v1}, Llel;->setFocusable(Z)V

    .line 82
    invoke-virtual {p0, v1}, Llel;->setClickable(Z)V

    .line 83
    invoke-virtual {p0, p0}, Llel;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 86
    iget-object v0, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 87
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 133
    sget-object v0, Llel;->a:Llct;

    if-nez v0, :cond_0

    .line 134
    invoke-static {p0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Llel;->a:Llct;

    .line 144
    :cond_0
    const/16 v0, 0x14

    invoke-static {p0, v0}, Llib;->a(Landroid/content/Context;I)I

    move-result v0

    sget-object v1, Llel;->a:Llct;

    iget v1, v1, Llct;->l:I

    add-int/2addr v0, v1

    const/16 v1, 0xa

    .line 147
    invoke-static {p0, v1}, Llib;->a(Landroid/content/Context;I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    sget-object v1, Llel;->a:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    const/16 v1, 0x19

    .line 150
    invoke-static {p0, v1}, Llib;->a(Landroid/content/Context;I)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    sget-object v1, Llel;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 301
    iput-object v1, p0, Llel;->b:Llci;

    .line 303
    iget-object v0, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 305
    iput-object v1, p0, Llel;->e:Landroid/text/StaticLayout;

    .line 306
    iput-object v1, p0, Llel;->f:Landroid/text/StaticLayout;

    .line 307
    iput-object v1, p0, Llel;->g:Landroid/text/StaticLayout;

    .line 308
    iput-object v1, p0, Llel;->h:Landroid/text/StaticLayout;

    .line 309
    iput-object v1, p0, Llel;->i:Landroid/text/StaticLayout;

    .line 310
    iput v2, p0, Llel;->k:I

    .line 311
    iput v2, p0, Llel;->l:I

    .line 312
    iput-object v1, p0, Llel;->m:Ljava/lang/String;

    .line 313
    iput-object v1, p0, Llel;->n:Ljava/lang/String;

    .line 314
    iput-object v1, p0, Llel;->o:Ljava/lang/String;

    .line 315
    invoke-virtual {p0}, Llel;->clearAnimation()V

    .line 316
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    invoke-static {p0}, Llii;->h(Landroid/view/View;)V

    .line 318
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Llel;->setAlpha(F)V

    .line 320
    :cond_0
    return-void
.end method

.method public a(Llci;Lidf;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 91
    invoke-virtual {p0}, Llel;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 92
    iput-object p1, p0, Llel;->b:Llci;

    .line 93
    iput-object p2, p0, Llel;->c:Lidf;

    .line 94
    iget-object v0, p0, Llel;->c:Lidf;

    invoke-virtual {v0}, Lidf;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Llel;->m:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Llel;->c:Lidf;

    invoke-virtual {v0}, Lidf;->b()Ljava/lang/Long;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Llel;->c:Lidf;

    .line 99
    invoke-virtual {v0}, Lidf;->j()Ljava/lang/String;

    move-result-object v4

    .line 100
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    move v7, v5

    .line 98
    invoke-static/range {v1 .. v7}, Lidi;->a(Landroid/content/Context;JLjava/lang/String;ZLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llel;->n:Ljava/lang/String;

    .line 103
    :cond_0
    iget-object v0, p0, Llel;->c:Lidf;

    invoke-virtual {v0}, Lidf;->f()Ljava/lang/String;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_1

    .line 105
    const v2, 0x7f0a0490

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llel;->o:Ljava/lang/String;

    .line 108
    :cond_1
    iput-object p3, p0, Llel;->d:Ljava/lang/String;

    .line 109
    invoke-static {v1}, Llel;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Llel;->k:I

    .line 111
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 113
    iget-object v0, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p2}, Lidf;->g()Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-virtual {p2}, Lidf;->h()Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Llel;->addView(Landroid/view/View;)V

    .line 116
    return-void

    .line 94
    :pswitch_1
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0491

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0493

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0494

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0492

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x0

    return v0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 324
    invoke-virtual {p0}, Llel;->invalidate()V

    .line 325
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 326
    return-void
.end method

.method public e()V
    .locals 5

    .prologue
    .line 345
    iget-object v0, p0, Llel;->b:Llci;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Llel;->b:Llci;

    iget-object v1, p0, Llel;->c:Lidf;

    invoke-virtual {v1}, Lidf;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x9

    iget-object v4, p0, Llel;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Llci;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 349
    :cond_0
    return-void
.end method

.method public f()Llja;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Llel;->c:Lidf;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 5
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 121
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 122
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Llel;->c:Lidf;

    invoke-virtual {v2}, Lidf;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 123
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Llel;->n:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 124
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Llel;->c:Lidf;

    invoke-virtual {v2}, Lidf;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 125
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Llel;->o:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 126
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Llel;->m:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 127
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 330
    iget-object v0, p0, Llel;->b:Llci;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Llel;->b:Llci;

    iget-object v1, p0, Llel;->c:Lidf;

    invoke-virtual {v1}, Lidf;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Llel;->c:Lidf;

    .line 332
    invoke-virtual {v2}, Lidf;->i()Ljava/lang/String;

    move-result-object v2

    .line 331
    invoke-interface {v0, v1, v2}, Llci;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 239
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 241
    invoke-virtual {p0}, Llel;->getWidth()I

    move-result v6

    .line 242
    invoke-virtual {p0}, Llel;->getHeight()I

    move-result v7

    .line 244
    iget-object v0, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v0

    .line 248
    sget-object v1, Llel;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    .line 249
    iget v0, p0, Llel;->l:I

    .line 250
    iget-object v2, p0, Llel;->e:Landroid/text/StaticLayout;

    if-eqz v2, :cond_0

    .line 251
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 252
    iget-object v2, p0, Llel;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 253
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 254
    iget-object v2, p0, Llel;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Llel;->a:Llct;

    iget v3, v3, Llct;->l:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 257
    :cond_0
    iget-object v2, p0, Llel;->f:Landroid/text/StaticLayout;

    if-eqz v2, :cond_1

    .line 258
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 259
    iget-object v2, p0, Llel;->f:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 260
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 261
    iget-object v2, p0, Llel;->f:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 264
    :cond_1
    iget-object v2, p0, Llel;->g:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    .line 265
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 266
    iget-object v2, p0, Llel;->g:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 267
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 268
    iget-object v2, p0, Llel;->g:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 271
    :cond_2
    iget-object v2, p0, Llel;->h:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    .line 272
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 273
    iget-object v2, p0, Llel;->h:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 274
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 275
    iget-object v2, p0, Llel;->h:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 278
    :cond_3
    iget-object v2, p0, Llel;->i:Landroid/text/StaticLayout;

    if-eqz v2, :cond_4

    .line 279
    sget-object v2, Llel;->a:Llct;

    iget v2, v2, Llct;->m:I

    add-int/2addr v0, v2

    .line 280
    sget-object v2, Llel;->a:Llct;

    iget-object v2, v2, Llct;->av:Landroid/graphics/Bitmap;

    int-to-float v3, v1

    int-to-float v4, v0

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 281
    sget-object v2, Llel;->a:Llct;

    iget-object v2, v2, Llct;->av:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Llel;->a:Llct;

    iget v3, v3, Llct;->k:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 283
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 284
    iget-object v2, p0, Llel;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 285
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 287
    sget-object v0, Llel;->a:Llct;

    iget-object v0, v0, Llct;->av:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    sget-object v0, Llel;->a:Llct;

    iget v0, v0, Llct;->k:I

    .line 288
    iget-object v0, p0, Llel;->i:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    .line 291
    :cond_4
    const/4 v1, 0x0

    int-to-float v2, v7

    int-to-float v3, v6

    int-to-float v4, v7

    sget-object v0, Llel;->a:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 293
    invoke-virtual {p0}, Llel;->isPressed()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Llel;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 294
    :cond_5
    sget-object v0, Llel;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8, v8, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 295
    sget-object v0, Llel;->a:Llct;

    iget-object v0, v0, Llct;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 297
    :cond_6
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 232
    iget-object v0, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v0

    .line 233
    iget-object v1, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    sget-object v2, Llel;->a:Llct;

    iget v2, v2, Llct;->m:I

    iget v3, p0, Llel;->l:I

    sget-object v4, Llel;->a:Llct;

    iget v4, v4, Llct;->m:I

    add-int/2addr v4, v0

    iget v5, p0, Llel;->l:I

    add-int/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 235
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 158
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 159
    iget-object v0, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v0

    .line 160
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 161
    iget-object v3, p0, Llel;->j:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v3, v2, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 163
    sub-int v0, v1, v0

    sget-object v2, Llel;->a:Llct;

    iget v2, v2, Llct;->m:I

    mul-int/lit8 v2, v2, 0x3

    sub-int v2, v0, v2

    .line 165
    invoke-virtual {p0}, Llel;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 170
    iget-object v0, p0, Llel;->c:Lidf;

    invoke-virtual {v0}, Lidf;->a()Ljava/lang/String;

    move-result-object v0

    .line 171
    const/16 v4, 0x14

    .line 172
    invoke-static {v3, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 171
    invoke-static {v4, v0, v2, v7, v5}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Llel;->e:Landroid/text/StaticLayout;

    .line 174
    iget-object v0, p0, Llel;->e:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v4, Llel;->a:Llct;

    iget v4, v4, Llct;->l:I

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x0

    .line 176
    const/16 v4, 0xa

    invoke-static {v3, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    .line 178
    iget-object v5, p0, Llel;->n:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 179
    iget-object v5, p0, Llel;->n:Ljava/lang/String;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {v4, v5, v2, v7, v6}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v5

    iput-object v5, p0, Llel;->f:Landroid/text/StaticLayout;

    .line 181
    iget-object v5, p0, Llel;->f:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v0, v5

    .line 184
    :cond_0
    iget-object v5, p0, Llel;->c:Lidf;

    invoke-virtual {v5}, Lidf;->c()Ljava/lang/String;

    move-result-object v5

    .line 185
    if-eqz v5, :cond_1

    .line 186
    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {v4, v5, v2, v7, v6}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v5

    iput-object v5, p0, Llel;->g:Landroid/text/StaticLayout;

    .line 188
    iget-object v5, p0, Llel;->g:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v0, v5

    .line 191
    :cond_1
    iget-object v5, p0, Llel;->o:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 192
    iget-object v5, p0, Llel;->o:Ljava/lang/String;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {v4, v5, v2, v7, v6}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v4

    iput-object v4, p0, Llel;->h:Landroid/text/StaticLayout;

    .line 194
    iget-object v4, p0, Llel;->h:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v0, v4

    .line 197
    :cond_2
    sget-object v4, Llel;->a:Llct;

    iget-object v4, v4, Llct;->av:Landroid/graphics/Bitmap;

    .line 198
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v2, v4

    sget-object v4, Llel;->a:Llct;

    iget v4, v4, Llct;->k:I

    sub-int/2addr v2, v4

    .line 200
    iget-object v4, p0, Llel;->m:Ljava/lang/String;

    if-eqz v4, :cond_3

    if-lez v2, :cond_3

    .line 201
    sget-object v4, Llel;->a:Llct;

    iget v4, v4, Llct;->m:I

    add-int/2addr v0, v4

    .line 202
    const/16 v4, 0x19

    .line 203
    invoke-static {v3, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    iget-object v4, p0, Llel;->m:Ljava/lang/String;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 202
    invoke-static {v3, v4, v2, v7, v5}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Llel;->i:Landroid/text/StaticLayout;

    .line 205
    iget-object v2, p0, Llel;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 208
    :cond_3
    iget v2, p0, Llel;->k:I

    invoke-virtual {p0, v1, v2}, Llel;->setMeasuredDimension(II)V

    .line 210
    iget v1, p0, Llel;->k:I

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Llel;->l:I

    .line 211
    return-void
.end method
