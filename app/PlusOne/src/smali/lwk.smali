.class public final Llwk;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Llwm;

.field private c:Llwo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 277
    invoke-direct {p0}, Loxq;-><init>()V

    .line 286
    const/high16 v0, -0x80000000

    iput v0, p0, Llwk;->a:I

    .line 289
    iput-object v1, p0, Llwk;->b:Llwm;

    .line 292
    iput-object v1, p0, Llwk;->c:Llwo;

    .line 277
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 312
    const/4 v0, 0x0

    .line 313
    iget v1, p0, Llwk;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 314
    const/4 v0, 0x1

    iget v1, p0, Llwk;->a:I

    .line 315
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 317
    :cond_0
    iget-object v1, p0, Llwk;->b:Llwm;

    if-eqz v1, :cond_1

    .line 318
    const/4 v1, 0x2

    iget-object v2, p0, Llwk;->b:Llwm;

    .line 319
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    :cond_1
    iget-object v1, p0, Llwk;->c:Llwo;

    if-eqz v1, :cond_2

    .line 322
    const/4 v1, 0x3

    iget-object v2, p0, Llwk;->c:Llwo;

    .line 323
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 325
    :cond_2
    iget-object v1, p0, Llwk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    iput v0, p0, Llwk;->ai:I

    .line 327
    return v0
.end method

.method public a(Loxn;)Llwk;
    .locals 2

    .prologue
    .line 335
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 336
    sparse-switch v0, :sswitch_data_0

    .line 340
    iget-object v1, p0, Llwk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 341
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llwk;->ah:Ljava/util/List;

    .line 344
    :cond_1
    iget-object v1, p0, Llwk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    :sswitch_0
    return-object p0

    .line 351
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 352
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 355
    :cond_2
    iput v0, p0, Llwk;->a:I

    goto :goto_0

    .line 357
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Llwk;->a:I

    goto :goto_0

    .line 362
    :sswitch_2
    iget-object v0, p0, Llwk;->b:Llwm;

    if-nez v0, :cond_4

    .line 363
    new-instance v0, Llwm;

    invoke-direct {v0}, Llwm;-><init>()V

    iput-object v0, p0, Llwk;->b:Llwm;

    .line 365
    :cond_4
    iget-object v0, p0, Llwk;->b:Llwm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 369
    :sswitch_3
    iget-object v0, p0, Llwk;->c:Llwo;

    if-nez v0, :cond_5

    .line 370
    new-instance v0, Llwo;

    invoke-direct {v0}, Llwo;-><init>()V

    iput-object v0, p0, Llwk;->c:Llwo;

    .line 372
    :cond_5
    iget-object v0, p0, Llwk;->c:Llwo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 336
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 297
    iget v0, p0, Llwk;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 298
    const/4 v0, 0x1

    iget v1, p0, Llwk;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 300
    :cond_0
    iget-object v0, p0, Llwk;->b:Llwm;

    if-eqz v0, :cond_1

    .line 301
    const/4 v0, 0x2

    iget-object v1, p0, Llwk;->b:Llwm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 303
    :cond_1
    iget-object v0, p0, Llwk;->c:Llwo;

    if-eqz v0, :cond_2

    .line 304
    const/4 v0, 0x3

    iget-object v1, p0, Llwk;->c:Llwo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 306
    :cond_2
    iget-object v0, p0, Llwk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 308
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 273
    invoke-virtual {p0, p1}, Llwk;->a(Loxn;)Llwk;

    move-result-object v0

    return-object v0
.end method
