.class public final Lcpp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcsq;


# instance fields
.field private final a:Lcsp;

.field private final b:I

.field private final c:Lctz;

.field private final d:Landroid/view/LayoutInflater;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcsp;I)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcpp;->d:Landroid/view/LayoutInflater;

    .line 61
    const-class v0, Lctz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcpp;->c:Lctz;

    .line 62
    iput-object p2, p0, Lcpp;->a:Lcsp;

    .line 63
    iput p3, p0, Lcpp;->b:I

    .line 64
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcpp;->b:I

    return v0
.end method

.method public a(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f100085

    const/4 v4, 0x0

    .line 77
    if-nez p3, :cond_5

    .line 78
    iget-object v0, p0, Lcpp;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f04018e

    invoke-virtual {v0, v1, p4, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 81
    :goto_0
    iget-object v0, p0, Lcpp;->a:Lcsp;

    invoke-virtual {v0}, Lcsp;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    if-lt p1, v0, :cond_1

    .line 82
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 88
    :goto_2
    new-instance v0, Lfyf;

    invoke-direct {v0, p2, p2}, Lfyf;-><init>(II)V

    .line 89
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    return-object v2

    :cond_0
    move v0, v4

    .line 81
    goto :goto_1

    .line 84
    :cond_1
    iget-object v0, p0, Lcpp;->a:Lcsp;

    invoke-virtual {v0}, Lcsp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    move-object v1, v2

    check-cast v1, Lcom/google/android/apps/plus/views/PhotoTileView;

    iget v3, p0, Lcpp;->e:I

    if-eqz v3, :cond_2

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(I)V

    iget v3, p0, Lcpp;->e:I

    iget v5, p0, Lcpp;->f:I

    invoke-virtual {v1, v3, v5}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(II)V

    :cond_2
    const/high16 v3, 0x10000

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->i(I)V

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v5, Lcpi;

    invoke-static {v3, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcpi;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sget-object v5, Lcph;->a:Lcph;

    invoke-virtual {v3, v6, v7, v5}, Lcpi;->a(JLcph;)Lcpg;

    move-result-object v3

    const v5, 0x7f100084

    invoke-virtual {v1, v5, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->setTag(ILjava/lang/Object;)V

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0, v5, v1, v3}, Lcpp;->a(Landroid/content/Context;Lcom/google/android/apps/plus/views/PhotoTileView;Lcpg;)V

    :goto_3
    const v3, 0x7f10007a

    sget-object v5, Lcoq;->a:Lcoq;

    invoke-virtual {v1, v3, v5}, Lcom/google/android/apps/plus/views/PhotoTileView;->setTag(ILjava/lang/Object;)V

    new-instance v3, Lcpr;

    invoke-direct {v3, v1, v0}, Lcpr;-><init>(Lcom/google/android/apps/plus/views/PhotoTileView;Ljava/lang/Long;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Lcps;

    invoke-direct {v3, v1, v0}, Lcps;-><init>(Lcom/google/android/apps/plus/views/PhotoTileView;Ljava/lang/Long;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 85
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 84
    :cond_3
    invoke-virtual {v1, v9}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    invoke-virtual {v1, v9}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljcl;)V

    invoke-virtual {v1, v8}, Lcom/google/android/apps/plus/views/PhotoTileView;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Future;

    if-eqz v3, :cond_4

    const/4 v5, 0x1

    invoke-interface {v3, v5}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v5, Lcpj;

    invoke-static {v3, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcpj;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    new-instance v5, Lcpq;

    invoke-direct {v5, p0, v1, v0, v2}, Lcpq;-><init>(Lcpp;Lcom/google/android/apps/plus/views/PhotoTileView;Ljava/lang/Long;Landroid/view/View;)V

    invoke-virtual {v3, v6, v7, v5}, Lcpj;->a(JLcpo;)Ljava/util/concurrent/Future;

    move-result-object v3

    invoke-virtual {v1, v8, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->setTag(ILjava/lang/Object;)V

    goto :goto_3

    :cond_5
    move-object v2, p3

    goto/16 :goto_0
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lcpp;->e:I

    .line 72
    iput p2, p0, Lcpp;->f:I

    .line 73
    return-void
.end method

.method a(Landroid/content/Context;Lcom/google/android/apps/plus/views/PhotoTileView;Lcpg;)V
    .locals 12

    .prologue
    .line 176
    iget-wide v0, p3, Lcpg;->d:J

    const-wide/16 v2, 0x100

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v8, v0

    .line 177
    :goto_0
    iget-wide v0, p3, Lcpg;->d:J

    const-wide/16 v2, 0x4000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v9, v0

    .line 179
    :goto_1
    iget-object v0, p0, Lcpp;->c:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    .line 180
    sget-object v1, Ldqo;->a:Ldqo;

    new-instance v2, Ldqp;

    iget-wide v4, p3, Lcpg;->b:J

    invoke-direct {v2, v4, v5}, Ldqp;-><init>(J)V

    .line 181
    invoke-virtual {v0, v1, v2}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v0

    check-cast v0, Ldqm;

    .line 183
    const/4 v10, 0x0

    .line 184
    if-eqz v0, :cond_3

    .line 185
    invoke-virtual {v0}, Ldqm;->h()Lnzi;

    move-result-object v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    .line 192
    :goto_2
    iget-object v3, p3, Lcpg;->f:Lizu;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p2, v3, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;Lizo;)V

    .line 193
    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljcl;)V

    .line 194
    invoke-virtual {p2, v8}, Lcom/google/android/apps/plus/views/PhotoTileView;->d(Z)V

    .line 195
    invoke-virtual {p2, v9}, Lcom/google/android/apps/plus/views/PhotoTileView;->n(Z)V

    .line 197
    const-class v0, Leqt;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqt;

    iget-object v1, p3, Lcpg;->f:Lizu;

    .line 198
    invoke-interface {v0, v1, p2}, Leqt;->a(Lizu;Lcom/google/android/apps/plus/views/PhotoTileView;)V

    .line 200
    sget-object v0, Lfvc;->k:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(I)V

    .line 203
    const-class v0, Levy;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levy;

    iget-object v1, p3, Lcpg;->f:Lizu;

    .line 204
    invoke-virtual {v0, p2, v1}, Levy;->a(Lcom/google/android/apps/plus/views/PhotoTileView;Lizu;)V

    .line 206
    :cond_0
    return-void

    .line 176
    :cond_1
    const/4 v0, 0x0

    move v8, v0

    goto :goto_0

    .line 177
    :cond_2
    const/4 v0, 0x0

    move v9, v0

    goto :goto_1

    .line 187
    :cond_3
    new-instance v0, Ldqm;

    iget-wide v1, p3, Lcpg;->b:J

    iget-object v3, p3, Lcpg;->f:Lizu;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-wide v6, p3, Lcpg;->d:J

    invoke-direct/range {v0 .. v7}, Ldqm;-><init>(JLizu;Ljava/lang/String;Lnzi;J)V

    move-object v1, v0

    move-object v0, v10

    goto :goto_2

    .line 192
    :cond_4
    new-instance v2, Ljub;

    invoke-direct {v2, v0}, Ljub;-><init>(Lnzi;)V

    move-object v0, v2

    goto :goto_3
.end method

.method public a(Lctn;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcpp;->a:Lcsp;

    invoke-interface {p1, v0}, Lctn;->a(Lcsp;)V

    .line 102
    return-void
.end method
