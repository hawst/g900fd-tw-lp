.class public final Lfxt;
.super Lfye;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljv;


# static fields
.field private static a:Z

.field private static b:I

.field private static c:I

.field private static d:I

.field private static e:Landroid/graphics/Paint;


# instance fields
.field private f:Landroid/widget/TextView;

.field private g:Lfxy;

.field private h:Lfxp;

.field private i:Lfxo;

.field private j:Lfyd;

.field private k:Lfxn;

.field private l:Landroid/widget/TextView;

.field private m:Lfxf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lfye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    invoke-virtual {p0, p1, p2, p3}, Lfxt;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, -0x1

    const/4 v7, -0x2

    .line 73
    sget-boolean v0, Lfxt;->a:Z

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 76
    const v1, 0x7f0d02b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxt;->d:I

    .line 78
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 79
    sput-object v1, Lfxt;->e:Landroid/graphics/Paint;

    const v2, 0x7f0b030b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 80
    sget-object v1, Lfxt;->e:Landroid/graphics/Paint;

    const v2, 0x7f0d02b9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 83
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lfxt;->b:I

    .line 84
    const v1, 0x7f0d024c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfxt;->c:I

    .line 86
    sput-boolean v6, Lfxt;->a:Z

    .line 89
    :cond_0
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxt;->f:Landroid/widget/TextView;

    .line 91
    invoke-virtual {p0}, Lfxt;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfxt;->f:Landroid/widget/TextView;

    const/16 v2, 0x19

    .line 90
    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 92
    iget-object v0, p0, Lfxt;->f:Landroid/widget/TextView;

    invoke-static {}, Lljw;->a()Lljw;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 94
    iget-object v0, p0, Lfxt;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfxt;->addView(Landroid/view/View;)V

    .line 96
    new-instance v0, Lfxp;

    invoke-direct {v0, p1, p2, p3}, Lfxp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxt;->h:Lfxp;

    .line 97
    iget-object v0, p0, Lfxt;->h:Lfxp;

    invoke-virtual {p0, v0}, Lfxt;->addView(Landroid/view/View;)V

    .line 99
    new-instance v0, Lfxo;

    invoke-direct {v0, p1, p2, p3}, Lfxo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxt;->i:Lfxo;

    .line 100
    iget-object v0, p0, Lfxt;->i:Lfxo;

    invoke-virtual {p0, v0}, Lfxt;->addView(Landroid/view/View;)V

    .line 102
    new-instance v0, Lfyd;

    invoke-direct {v0, p1, p2, p3}, Lfyd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxt;->j:Lfyd;

    .line 103
    iget-object v0, p0, Lfxt;->j:Lfyd;

    invoke-virtual {v0, p0}, Lfyd;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lfxt;->j:Lfyd;

    invoke-virtual {p0, v0}, Lfxt;->addView(Landroid/view/View;)V

    .line 106
    new-instance v0, Lfxn;

    invoke-direct {v0, p1, p2, p3}, Lfxn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxt;->k:Lfxn;

    .line 107
    iget-object v0, p0, Lfxt;->k:Lfxn;

    const v1, 0x7f1000a6

    invoke-virtual {v0, v1}, Lfxn;->setId(I)V

    .line 108
    iget-object v0, p0, Lfxt;->k:Lfxn;

    invoke-virtual {p0, v0}, Lfxt;->addView(Landroid/view/View;)V

    .line 110
    new-instance v0, Lfxy;

    invoke-direct {v0, p1, p2, p3}, Lfxy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxt;->g:Lfxy;

    .line 111
    iget-object v0, p0, Lfxt;->g:Lfxy;

    invoke-virtual {p0, v0}, Lfxt;->addView(Landroid/view/View;)V

    .line 112
    iget-object v0, p0, Lfxt;->g:Lfxy;

    new-instance v1, Lfyf;

    invoke-direct {v1, v8, v7}, Lfyf;-><init>(II)V

    invoke-virtual {v0, v1}, Lfxy;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    iget-object v0, p0, Lfxt;->g:Lfxy;

    const v1, 0x7f1000a7

    invoke-virtual {v0, v1}, Lfxy;->setId(I)V

    .line 116
    sget v0, Lfxt;->c:I

    int-to-float v3, v0

    sget v4, Lfxt;->b:I

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Llif;->a(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lfxt;->l:Landroid/widget/TextView;

    .line 118
    iget-object v0, p0, Lfxt;->l:Landroid/widget/TextView;

    new-instance v1, Lfyf;

    invoke-direct {v1, v8, v7}, Lfyf;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 120
    iget-object v0, p0, Lfxt;->l:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfxt;->addView(Landroid/view/View;)V

    .line 121
    return-void
.end method

.method public a(Landroid/text/style/URLSpan;)V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lfxt;->m:Lfxf;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lfxt;->m:Lfxf;

    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lfxf;->c(Ljava/lang/String;)V

    .line 313
    :cond_0
    return-void
.end method

.method public a(Lidh;Leaq;Lfxf;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 220
    iput-object p3, p0, Lfxt;->m:Lfxf;

    .line 222
    invoke-virtual {p0}, Lfxt;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 225
    invoke-virtual {p1}, Lidh;->b()Lozp;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 226
    const v1, 0x7f0a0914

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 227
    const v4, 0x7f0a0915

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 233
    :goto_0
    invoke-virtual {p1}, Lidh;->i()Lpbj;

    move-result-object v4

    .line 234
    iget-object v5, v4, Lpbj;->b:Lpbd;

    .line 235
    invoke-virtual {p1}, Lidh;->p()Ljava/lang/String;

    move-result-object v6

    .line 236
    if-eqz v5, :cond_2

    iget-object v7, v5, Lpbd;->a:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v7, v5, Lpbd;->a:Ljava/lang/String;

    .line 237
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 238
    new-instance v6, Landroid/text/SpannableStringBuilder;

    iget-object v5, v5, Lpbd;->a:Ljava/lang/String;

    .line 239
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-direct {v6, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 240
    invoke-static {v6, p0}, Llju;->a(Landroid/text/Spannable;Lljv;)V

    .line 241
    iget-object v5, p0, Lfxt;->f:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 249
    iget-object v5, p0, Lfxt;->l:Landroid/widget/TextView;

    invoke-static {p1, v6, v7}, Ldrm;->a(Lidh;J)Z

    move-result v6

    if-eqz v6, :cond_4

    :goto_2
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v0, p0, Lfxt;->h:Lfxp;

    invoke-virtual {p1}, Lidh;->l()Loyy;

    move-result-object v1

    invoke-virtual {p1}, Lidh;->m()Loyy;

    move-result-object v5

    .line 252
    invoke-static {p1}, Ldrm;->c(Lidh;)Z

    move-result v6

    .line 251
    invoke-virtual {v0, v1, v5, v6}, Lfxp;->a(Loyy;Loyy;Z)V

    .line 254
    invoke-virtual {p1}, Lidh;->o()Lpao;

    move-result-object v1

    .line 255
    invoke-virtual {p1}, Lidh;->g()I

    move-result v0

    if-ne v0, v2, :cond_5

    move v0, v2

    .line 256
    :goto_3
    invoke-virtual {p1}, Lidh;->b()Lozp;

    move-result-object v5

    invoke-static {v5}, Ldrm;->b(Lozp;)Z

    move-result v5

    .line 258
    if-eqz v1, :cond_6

    move v0, v2

    .line 266
    :goto_4
    if-eqz v0, :cond_a

    .line 267
    iget-object v5, p0, Lfxt;->i:Lfxo;

    invoke-virtual {p1}, Lidh;->l()Loyy;

    move-result-object v6

    invoke-virtual {v5, v0, v1, v6, p3}, Lfxo;->a(ILpao;Loyy;Lfxf;)V

    .line 268
    iget-object v0, p0, Lfxt;->i:Lfxo;

    invoke-virtual {v0, v3}, Lfxo;->setVisibility(I)V

    .line 273
    :goto_5
    iget-object v0, v4, Lpbj;->f:Lltl;

    .line 274
    invoke-virtual {p1}, Lidh;->a()Lpbl;

    move-result-object v1

    .line 275
    if-eqz v1, :cond_b

    if-eqz v0, :cond_b

    iget-object v4, v0, Lltl;->a:Ljava/lang/String;

    .line 276
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 277
    iget-object v4, p0, Lfxt;->j:Lfyd;

    iget-object v0, v0, Lltl;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lfyd;->a(Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lfxt;->j:Lfyd;

    invoke-virtual {v0, v3}, Lfyd;->setVisibility(I)V

    .line 283
    :goto_6
    if-eqz v1, :cond_c

    iget-boolean v0, p2, Leaq;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p2, Leaq;->f:Z

    if-eqz v0, :cond_c

    .line 284
    :cond_0
    iget-object v0, p0, Lfxt;->k:Lfxn;

    invoke-virtual {v0, p3}, Lfxn;->a(Lfxf;)V

    .line 285
    iget-object v0, p0, Lfxt;->k:Lfxn;

    invoke-virtual {v0, p2}, Lfxn;->a(Leaq;)V

    .line 286
    iget-object v0, p0, Lfxt;->k:Lfxn;

    invoke-virtual {v0, v3}, Lfxn;->setVisibility(I)V

    .line 292
    :goto_7
    invoke-virtual {p1}, Lidh;->j()Z

    move-result v0

    if-eqz v0, :cond_d

    iget v0, p2, Leaq;->a:I

    if-ne v0, v2, :cond_d

    .line 293
    iget-object v0, p0, Lfxt;->g:Lfxy;

    invoke-virtual {v0, p1, p2, p3}, Lfxy;->a(Lidh;Leaq;Lfxf;)V

    .line 294
    iget-object v0, p0, Lfxt;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lfxt;->g:Lfxy;

    invoke-virtual {v0, v3}, Lfxy;->setVisibility(I)V

    .line 300
    :goto_8
    return-void

    .line 229
    :cond_1
    const v1, 0x7f0a090e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 230
    const v4, 0x7f0a090f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 242
    :cond_2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 243
    iget-object v5, p0, Lfxt;->f:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 245
    :cond_3
    iget-object v5, p0, Lfxt;->f:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_4
    move-object v0, v1

    .line 249
    goto/16 :goto_2

    :cond_5
    move v0, v3

    .line 255
    goto/16 :goto_3

    .line 260
    :cond_6
    iget-object v6, v4, Lpbj;->g:Lltn;

    if-eqz v6, :cond_7

    .line 261
    const/4 v0, 0x2

    goto/16 :goto_4

    .line 262
    :cond_7
    if-eqz v0, :cond_e

    iget-boolean v0, p2, Leaq;->k:Z

    if-eqz v0, :cond_e

    if-nez v5, :cond_e

    invoke-virtual {p0}, Lfxt;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class v0, Lhee;

    invoke-static {v5, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v6

    const-class v0, Lieh;

    invoke-static {v5, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v7, Ldxd;->r:Lief;

    invoke-interface {v0, v7, v6}, Lieh;->b(Lief;I)Z

    move-result v7

    const-string v0, "vnd.google.android.hangouts/vnd.google.android.hangout_on_air_whitelist"

    invoke-static {v5, v0, v6, v2}, Leyq;->a(Landroid/content/Context;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_8

    move v0, v2

    :goto_9
    if-eqz v7, :cond_9

    if-eqz v0, :cond_9

    move v0, v2

    :goto_a
    if-eqz v0, :cond_e

    .line 263
    const/4 v0, 0x3

    goto/16 :goto_4

    :cond_8
    move v0, v3

    .line 262
    goto :goto_9

    :cond_9
    move v0, v3

    goto :goto_a

    .line 270
    :cond_a
    iget-object v0, p0, Lfxt;->i:Lfxo;

    invoke-virtual {v0, v8}, Lfxo;->setVisibility(I)V

    goto/16 :goto_5

    .line 280
    :cond_b
    iget-object v0, p0, Lfxt;->j:Lfyd;

    invoke-virtual {v0, v8}, Lfyd;->setVisibility(I)V

    goto/16 :goto_6

    .line 288
    :cond_c
    iget-object v0, p0, Lfxt;->k:Lfxn;

    invoke-virtual {v0, v8}, Lfxn;->setVisibility(I)V

    goto/16 :goto_7

    .line 297
    :cond_d
    iget-object v0, p0, Lfxt;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lfxt;->g:Lfxy;

    invoke-virtual {v0, v8}, Lfxy;->setVisibility(I)V

    goto/16 :goto_8

    :cond_e
    move v0, v3

    goto/16 :goto_4
.end method

.method public b()V
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lfxt;->h:Lfxp;

    invoke-virtual {v0}, Lfxp;->b()V

    .line 304
    iget-object v0, p0, Lfxt;->i:Lfxo;

    invoke-virtual {v0}, Lfxo;->b()V

    .line 305
    iget-object v0, p0, Lfxt;->k:Lfxn;

    invoke-virtual {v0}, Lfxn;->b()V

    .line 306
    return-void
.end method

.method protected measureChildren(II)V
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x3

    const/4 v3, 0x1

    const/16 v9, 0x8

    const/4 v1, 0x0

    .line 125
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 126
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 130
    sget v2, Lfxt;->d:I

    add-int/lit8 v2, v2, 0x0

    .line 131
    sget v4, Lfxt;->d:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, v6, v4

    .line 133
    iget-object v5, p0, Lfxt;->f:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 134
    iget-object v5, p0, Lfxt;->f:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    iget-object v5, p0, Lfxt;->f:Landroid/widget/TextView;

    sget v7, Lfxt;->d:I

    sub-int v7, v4, v7

    const/high16 v8, -0x80000000

    invoke-static {v5, v7, v8, v0, v1}, Lfxt;->a(Landroid/view/View;IIII)V

    .line 139
    iget-object v0, p0, Lfxt;->f:Landroid/widget/TextView;

    sget v5, Lfxt;->d:I

    add-int/2addr v5, v2

    invoke-static {v0, v5, v1}, Lfxt;->a(Landroid/view/View;II)V

    .line 141
    iget-object v0, p0, Lfxt;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    sget v5, Lfxt;->d:I

    add-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x0

    .line 146
    :goto_0
    iget-object v5, p0, Lfxt;->l:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getVisibility()I

    move-result v5

    if-eq v5, v9, :cond_0

    .line 147
    iget-object v5, p0, Lfxt;->l:Landroid/widget/TextView;

    invoke-static {v5, v4, v11, v1, v1}, Lfxt;->a(Landroid/view/View;IIII)V

    .line 148
    iget-object v4, p0, Lfxt;->l:Landroid/widget/TextView;

    invoke-static {v4, v2, v0}, Lfxt;->a(Landroid/view/View;II)V

    .line 149
    iget-object v2, p0, Lfxt;->l:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    sget v4, Lfxt;->d:I

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 152
    :cond_0
    iget-object v2, p0, Lfxt;->g:Lfxy;

    invoke-virtual {v2}, Lfxy;->getVisibility()I

    move-result v2

    if-eq v2, v9, :cond_1

    .line 153
    iget-object v2, p0, Lfxt;->g:Lfxy;

    invoke-static {v2, v6, v11, v1, v1}, Lfxt;->a(Landroid/view/View;IIII)V

    .line 154
    iget-object v2, p0, Lfxt;->g:Lfxy;

    invoke-static {v2, v1, v0}, Lfxt;->a(Landroid/view/View;II)V

    .line 155
    iget-object v2, p0, Lfxt;->g:Lfxy;

    invoke-virtual {v2}, Lfxy;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 158
    :cond_1
    iget-object v2, p0, Lfxt;->j:Lfyd;

    invoke-virtual {v2}, Lfyd;->getVisibility()I

    move-result v2

    if-eq v2, v9, :cond_2

    .line 159
    iget-object v2, p0, Lfxt;->j:Lfyd;

    invoke-static {v2, v6, v11, v1, v1}, Lfxt;->a(Landroid/view/View;IIII)V

    .line 160
    iget-object v2, p0, Lfxt;->j:Lfyd;

    invoke-static {v2, v1, v0}, Lfxt;->a(Landroid/view/View;II)V

    .line 161
    iget-object v2, p0, Lfxt;->j:Lfyd;

    invoke-virtual {v2}, Lfyd;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 164
    :cond_2
    new-array v7, v10, [Lfxu;

    iget-object v2, p0, Lfxt;->k:Lfxn;

    aput-object v2, v7, v1

    iget-object v2, p0, Lfxt;->h:Lfxp;

    aput-object v2, v7, v3

    const/4 v2, 0x2

    iget-object v4, p0, Lfxt;->i:Lfxo;

    aput-object v4, v7, v2

    move v4, v1

    move v2, v1

    :goto_1
    if-ge v4, v10, :cond_6

    if-nez v2, :cond_3

    aget-object v2, v7, v4

    invoke-virtual {v2}, Lfxu;->getVisibility()I

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    move v2, v3

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 143
    :cond_4
    iget-object v0, p0, Lfxt;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v1

    .line 164
    goto :goto_2

    :cond_6
    move v2, v1

    move v4, v3

    move v5, v0

    :goto_3
    if-ge v2, v10, :cond_9

    aget-object v0, v7, v2

    invoke-virtual {v0}, Lfxu;->getVisibility()I

    move-result v8

    if-eq v8, v9, :cond_7

    invoke-virtual {v0, v4}, Lfxu;->a(Z)V

    const/high16 v8, -0x80000000

    invoke-static {v0, v6, v8, v1, v1}, Lfxt;->a(Landroid/view/View;IIII)V

    invoke-static {v0, v1, v5}, Lfxt;->a(Landroid/view/View;II)V

    invoke-virtual {v0}, Lfxu;->getMeasuredHeight()I

    move-result v0

    :goto_4
    add-int/2addr v5, v0

    if-eqz v4, :cond_8

    if-nez v0, :cond_8

    move v0, v3

    :goto_5
    add-int/lit8 v2, v2, 0x1

    move v4, v0

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_4

    :cond_8
    move v0, v1

    goto :goto_5

    .line 166
    :cond_9
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lfxt;->m:Lfxf;

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lfxt;->m:Lfxf;

    invoke-interface {v0}, Lfxf;->W()V

    .line 320
    :cond_0
    return-void
.end method
