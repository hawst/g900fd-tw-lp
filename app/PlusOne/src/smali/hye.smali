.class public Lhye;
.super Ldi;
.source "PG"


# instance fields
.field private b:Z

.field private final c:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final d:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Ldi;-><init>(Landroid/content/Context;)V

    .line 20
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lhye;->c:Ldp;

    .line 36
    iput-object p2, p0, Lhye;->d:Landroid/net/Uri;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 44
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct/range {p0 .. p6}, Ldi;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lhye;->c:Ldp;

    .line 53
    iput-object p7, p0, Lhye;->d:Landroid/net/Uri;

    .line 54
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Ldi;->f()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 104
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 106
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p1

    .line 109
    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 115
    :goto_1
    if-eqz v2, :cond_1

    .line 120
    :goto_2
    invoke-super {p0, v0}, Ldi;->a(Landroid/database/Cursor;)V

    .line 121
    return-void

    .line 108
    :catch_0
    move-exception v1

    move-object v1, v0

    goto :goto_0

    .line 109
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    :cond_2
    move-object v1, p1

    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lhye;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lhye;->f()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lhye;->C()Landroid/database/Cursor;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    .line 92
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 95
    :cond_0
    return-object v0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 58
    invoke-super {p0}, Ldi;->g()V

    .line 59
    iget-boolean v0, p0, Lhye;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lhye;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lhye;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lhye;->d:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lhye;->c:Ldp;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhye;->b:Z

    .line 64
    :cond_0
    return-void
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method protected i()V
    .locals 0

    .prologue
    .line 133
    invoke-virtual {p0}, Lhye;->b()Z

    .line 134
    invoke-super {p0}, Ldi;->i()V

    .line 135
    invoke-virtual {p0}, Lhye;->w()V

    .line 136
    return-void
.end method

.method public w()V
    .locals 2

    .prologue
    .line 125
    iget-boolean v0, p0, Lhye;->b:Z

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lhye;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lhye;->c:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhye;->b:Z

    .line 129
    :cond_0
    return-void
.end method
