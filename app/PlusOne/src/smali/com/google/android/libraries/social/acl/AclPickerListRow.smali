.class public Lcom/google/android/libraries/social/acl/AclPickerListRow;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:[Lcom/google/android/libraries/social/acl/AclPickerChip;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->setOrientation(I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->setOrientation(I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->setOrientation(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/acl/AclPickerChip;->setVisibility(I)V

    .line 83
    return-void
.end method

.method public a(ILandroid/view/View$OnClickListener;)V
    .locals 5

    .prologue
    .line 36
    new-array v0, p1, [Lcom/google/android/libraries/social/acl/AclPickerChip;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    .line 37
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 38
    iget-object v2, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f040021

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/acl/AclPickerChip;

    aput-object v0, v2, v1

    .line 39
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 41
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 42
    iget-object v2, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/acl/AclPickerChip;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v0, v0, v1

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/acl/AclPickerChip;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->addView(Landroid/view/View;)V

    .line 37
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 46
    :cond_0
    return-void
.end method

.method public a(ILhfv;Lhfw;Z)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 51
    const-string v0, ""

    .line 53
    invoke-virtual {p3}, Lhfw;->e()Ljqs;

    move-result-object v1

    .line 54
    invoke-virtual {p3}, Lhfw;->d()Lhxc;

    move-result-object v4

    .line 55
    invoke-virtual {p3}, Lhfw;->f()Lkxr;

    move-result-object v5

    .line 56
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljqs;->e()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    if-eqz v4, :cond_1

    .line 57
    invoke-virtual {v4}, Lhxc;->c()I

    move-result v1

    if-eq v1, v3, :cond_2

    :cond_1
    if-eqz v5, :cond_3

    .line 58
    invoke-virtual {v5}, Lkxr;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 59
    :cond_2
    const v1, 0x7f0201a7

    .line 65
    :goto_0
    iget-object v4, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v4, v4, p1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->getContext()Landroid/content/Context;

    invoke-virtual {p3}, Lhfw;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Lcom/google/android/libraries/social/acl/AclPickerChip;->a(Ljava/lang/String;I)V

    .line 66
    iget-object v1, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/acl/AclPickerChip;->a(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2, p3}, Lcom/google/android/libraries/social/acl/AclPickerChip;->a(Lhfv;Lhfw;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v0, v0, p1

    invoke-virtual {v0, p3}, Lcom/google/android/libraries/social/acl/AclPickerChip;->setTag(Ljava/lang/Object;)V

    .line 70
    if-eqz p4, :cond_4

    .line 71
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v0, v0, p1

    invoke-static {p2, p3, v0}, Lhfu;->a(Lhfv;Lhfw;Landroid/view/View;)V

    .line 76
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v0, v0, p1

    invoke-virtual {p3}, Lhfw;->h()Z

    move-result v1

    const v4, 0x7f100156

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v1, :cond_5

    move v0, v2

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 78
    invoke-virtual {p0, p1, v2}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a(II)V

    .line 79
    return-void

    .line 62
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p2, v0, p3}, Lhfv;->a(Landroid/content/Context;Lhfw;)Ljava/lang/String;

    move-result-object v0

    move v1, v2

    goto :goto_0

    .line 73
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a:[Lcom/google/android/libraries/social/acl/AclPickerChip;

    aget-object v0, v0, p1

    invoke-static {v0}, Lhfu;->a(Landroid/view/View;)V

    goto :goto_1

    :cond_5
    move v0, v3

    .line 76
    goto :goto_2
.end method
