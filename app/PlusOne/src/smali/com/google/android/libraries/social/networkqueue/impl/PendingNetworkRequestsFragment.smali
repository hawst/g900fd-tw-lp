.class public Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;
.super Llol;
.source "PG"

# interfaces
.implements Ljig;


# instance fields
.field private N:Landroid/widget/ListView;

.field private O:Ljie;

.field private P:I

.field private Q:Ljhi;

.field private R:Ldr;

.field private S:Ljik;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 37
    invoke-direct {p0}, Llol;-><init>()V

    .line 55
    new-instance v0, Lhmg;

    sget-object v1, Lonb;->b:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->au:Llnh;

    .line 56
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 57
    new-instance v0, Lhmf;

    iget-object v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 216
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->P:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljhw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->O:Ljie;

    invoke-virtual {v0, p1}, Ljie;->a(Ljava/util/List;)V

    .line 148
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 134
    const v0, 0x7f0401c9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 135
    const v0, 0x7f10054f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->N:Landroid/widget/ListView;

    .line 137
    const v0, 0x7f100550

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 138
    iget-object v2, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->N:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 140
    new-instance v0, Ljie;

    iget-object v2, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->at:Llnl;

    invoke-direct {v0, v2, p0}, Ljie;-><init>(Landroid/content/Context;Ljig;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->O:Ljie;

    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->N:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->O:Ljie;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 143
    return-object v1
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 188
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 189
    const-string v1, "account_id"

    iget v2, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->P:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 190
    const-string v1, "item_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 191
    new-instance v1, Ljih;

    invoke-direct {v1}, Ljih;-><init>()V

    .line 193
    invoke-virtual {v1, v0}, Ljih;->f(Landroid/os/Bundle;)V

    .line 194
    invoke-virtual {p0}, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    const-string v2, "pending_post_delete_confirmation"

    invoke-virtual {v1, v0, v2}, Ljih;->a(Lae;Ljava/lang/String;)V

    .line 195
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    .line 125
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->P:I

    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->at:Llnl;

    const-class v1, Ljhi;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhi;

    iput-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->Q:Ljhi;

    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->at:Llnl;

    invoke-static {v0}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->R:Ldr;

    .line 128
    new-instance v0, Ljik;

    invoke-direct {v0, p0}, Ljik;-><init>(Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->S:Ljik;

    .line 129
    return-void
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 158
    invoke-super {p0}, Llol;->aO_()V

    .line 159
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->Q:Ljhi;

    iget v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->P:I

    invoke-interface {v0, v1}, Ljhi;->a(I)Ljhh;

    move-result-object v0

    check-cast v0, Ljhy;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljhy;->e()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "networkqueue_change_displayitems"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->a(Ljava/util/List;)V

    .line 160
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->R:Ldr;

    iget-object v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->S:Ljik;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.plus.networkqueue_change"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ldr;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 161
    return-void

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->at:Llnl;

    new-instance v1, Ljil;

    iget-object v2, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->at:Llnl;

    iget v3, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->P:I

    invoke-direct {v1, v2, v3}, Ljil;-><init>(Landroid/content/Context;I)V

    invoke-static {v0, v1}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    goto :goto_0
.end method

.method public z()V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0}, Llol;->z()V

    .line 153
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->R:Ldr;

    iget-object v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingNetworkRequestsFragment;->S:Ljik;

    invoke-virtual {v0, v1}, Ldr;->a(Landroid/content/BroadcastReceiver;)V

    .line 154
    return-void
.end method
