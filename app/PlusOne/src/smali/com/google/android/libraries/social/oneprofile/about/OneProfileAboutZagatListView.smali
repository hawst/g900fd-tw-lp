.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;
.super Ljnl;
.source "PG"


# static fields
.field private static a:I

.field private static o:I


# instance fields
.field private p:Landroid/widget/LinearLayout;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/FrameLayout;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/view/View;

.field private u:Landroid/widget/LinearLayout;

.field private v:Ljns;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->a:I

    .line 64
    const v1, 0x7f0b00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->o:I

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->a:I

    .line 64
    const v1, 0x7f0b00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->o:I

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->a:I

    .line 64
    const v1, 0x7f0b00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->o:I

    .line 59
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;)Ljns;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->v:Ljns;

    return-object v0
.end method

.method public static b(Lnjt;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 189
    invoke-static {p0}, Ljnu;->h(Lnjt;)Lprs;

    move-result-object v2

    if-nez v2, :cond_0

    .line 190
    invoke-static {p0}, Ljnu;->f(Lnjt;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 191
    invoke-static {p0}, Ljnu;->e(Lnjt;)Lpxp;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, v2, Lpxp;->a:[Lpxo;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljns;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->v:Ljns;

    .line 186
    return-void
.end method

.method public a(Lnjt;)V
    .locals 13

    .prologue
    const/4 v11, -0x2

    const/4 v3, 0x1

    const/16 v12, 0x8

    const/4 v1, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 85
    invoke-static {p1}, Ljnu;->e(Lnjt;)Lpxp;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_4

    .line 88
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->p:Landroid/widget/LinearLayout;

    iget-object v4, v0, Lpxp;->a:[Lpxo;

    array-length v5, v4

    move v0, v1

    move v2, v3

    :goto_0
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    new-instance v7, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v8, v6, Lpxo;->b:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v8, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->a:I

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v11, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    if-eqz v2, :cond_1

    sget v9, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->g:I

    invoke-virtual {v8, v1, v1, v9, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    :goto_1
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v8, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v6, v6, Lpxo;->c:Ljava/lang/String;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->o:I

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    sget v9, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->f:I

    sget v10, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->g:I

    invoke-virtual {v8, v9, v1, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->r:Landroid/widget/FrameLayout;

    new-instance v2, Ljnq;

    invoke-direct {v2, p0}, Ljnq;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 94
    :goto_2
    invoke-static {p1}, Ljnu;->f(Lnjt;)Ljava/lang/String;

    move-result-object v0

    .line 95
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 96
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->s:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    :goto_3
    invoke-static {p1}, Ljnu;->h(Lnjt;)Lprs;

    move-result-object v0

    .line 105
    if-eqz v0, :cond_8

    .line 106
    iget-object v5, v0, Lprs;->a:[Lprr;

    array-length v6, v5

    move v4, v1

    move v0, v1

    :goto_4
    if-ge v4, v6, :cond_9

    aget-object v2, v5, v4

    .line 107
    iget-object v7, v2, Lprr;->b:[Lppg;

    array-length v8, v7

    move v2, v1

    :goto_5
    if-ge v2, v8, :cond_7

    aget-object v0, v7, v2

    .line 109
    new-instance v9, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v10, v0, Lppg;->b:Lppf;

    iget-object v10, v10, Lppf;->c:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v10, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->e:Landroid/content/res/ColorStateList;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f020416

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    sget v10, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->g:I

    sget v11, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->g:I

    invoke-virtual {v9, v1, v10, v1, v11}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v10, v0, Lppg;->b:Lppf;

    if-eqz v10, :cond_6

    iget-object v0, v0, Lppg;->b:Lppf;

    iget-object v0, v0, Lppf;->b:Ljava/lang/String;

    :goto_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    new-instance v10, Ljnr;

    invoke-direct {v10, p0, v0}, Ljnr;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 107
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5

    .line 91
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 99
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 109
    :cond_6
    const/4 v0, 0x0

    goto :goto_6

    .line 106
    :cond_7
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_4

    :cond_8
    move v0, v1

    .line 114
    :cond_9
    if-eqz v0, :cond_a

    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 121
    :goto_7
    return-void

    .line 118
    :cond_a
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->t:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 70
    const v0, 0x7f10044e

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->p:Landroid/widget/LinearLayout;

    .line 71
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->p:Landroid/widget/LinearLayout;

    const v1, 0x7f100548

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->q:Landroid/widget/LinearLayout;

    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->p:Landroid/widget/LinearLayout;

    const v1, 0x7f100549

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->r:Landroid/widget/FrameLayout;

    .line 73
    const v0, 0x7f10044f

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->s:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f100450

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->t:Landroid/view/View;

    .line 75
    const v0, 0x7f100451

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->u:Landroid/widget/LinearLayout;

    .line 76
    return-void
.end method
