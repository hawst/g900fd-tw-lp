.class public Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;
.super Lifj;
.source "PG"


# annotations
.annotation runtime Lifl;
    a = "files"
.end annotation


# static fields
.field public static final a:Lifm;


# instance fields
.field public contentUrl:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "content_url"
    .end annotation
.end field

.field public filename:Ljava/lang/String;
    .annotation runtime Lifk;
        a = "filename"
    .end annotation
.end field

.field public hashCode:J
    .annotation runtime Lifk;
        a = "hash_code"
        b = true
    .end annotation
.end field

.field public lastAccess:J
    .annotation runtime Lifk;
        a = "last_access"
        b = true
    .end annotation
.end field

.field public size:J
    .annotation runtime Lifk;
        a = "size"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 268
    new-instance v0, Lifm;

    const-class v1, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;

    invoke-direct {v0, v1}, Lifm;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->a:Lifm;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0}, Lifj;-><init>()V

    .line 270
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "hash_code: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 296
    iget-wide v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->hashCode:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", content_url"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 297
    iget-object v1, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->contentUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", last_access"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 298
    iget-wide v2, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->lastAccess:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", filename"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 299
    iget-object v1, p0, Lcom/google/android/libraries/social/gallery3d/common/FileCache$FileEntry;->filename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
