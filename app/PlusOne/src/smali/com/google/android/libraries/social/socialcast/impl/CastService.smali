.class public final Lcom/google/android/libraries/social/socialcast/impl/CastService;
.super Landroid/app/Service;
.source "PG"

# interfaces
.implements Lkdd;
.implements Llnk;


# static fields
.field private static P:Llnh;

.field public static a:Z

.field private static final b:Lhmk;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Ljava/lang/String;

.field private E:Lcom/google/android/libraries/social/media/MediaResource;

.field private F:Lizu;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Liig;

.field private K:Liic;

.field private L:Lihr;

.field private M:Lkfr;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private Q:Lkrk;

.field private c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lkrl;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/os/Handler;

.field private e:Lkrp;

.field private f:Lvp;

.field private g:Lvq;

.field private h:Lkrr;

.field private i:Ljava/lang/String;

.field private j:Lvn;

.field private k:Ljava/lang/Runnable;

.field private l:Lkro;

.field private m:Lvy;

.field private n:Lizs;

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:I

.field private v:Lkfs;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 91
    new-instance v0, Lhmk;

    sget-object v1, Lomz;->b:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    sput-object v0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->b:Lhmk;

    .line 159
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 132
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->D:Ljava/lang/String;

    .line 1100
    return-void
.end method

.method public static synthetic A(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->G:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic B(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->H:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic C(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->I:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic D(Lcom/google/android/libraries/social/socialcast/impl/CastService;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->u:I

    return v0
.end method

.method public static synthetic E(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->c:Ljava/util/HashSet;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Liic;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->K:Liic;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->k:Ljava/lang/Runnable;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->t:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/socialcast/impl/CastService;Lkro;)Lkro;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->l:Lkro;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/socialcast/impl/CastService;Lvy;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lvy;)V

    return-void
.end method

.method private a(Lhmn;)V
    .locals 4

    .prologue
    .line 465
    new-instance v0, Lhly;

    const/4 v1, 0x4

    new-instance v2, Lhml;

    invoke-direct {v2}, Lhml;-><init>()V

    new-instance v3, Lhmk;

    invoke-direct {v3, p1}, Lhmk;-><init>(Lhmn;)V

    .line 466
    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/social/socialcast/impl/CastService;->b:Lhmk;

    invoke-virtual {v2, v3}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lhly;-><init>(ILhml;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->r:Ljava/lang/String;

    .line 467
    invoke-virtual {v0, v1}, Lhly;->a(Ljava/lang/String;)Lhly;

    move-result-object v0

    .line 468
    invoke-virtual {v0, p0}, Lhly;->a(Landroid/content/Context;)V

    .line 469
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 924
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 926
    :try_start_0
    const-string v1, "command"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 927
    const-string v1, "id"

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 928
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 933
    return-void

    .line 929
    :catch_0
    move-exception v0

    .line 930
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 931
    :catch_1
    move-exception v0

    .line 932
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 937
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 938
    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    invoke-interface {v1}, Lihr;->c()Z

    move-result v1

    if-nez v1, :cond_2

    .line 939
    :cond_0
    const-string v1, "CastService"

    const-string v2, "Cannot send message because not connected to Chromecast. message: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    const v0, 0x7f0a024e

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 948
    :goto_1
    return-void

    .line 939
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 944
    :cond_2
    const-string v1, "CastService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 945
    const-string v1, "Sending message: (ns=urn:x-cast:com.google.android.apps.socialcast) "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 947
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    const-string v2, "urn:x-cast:com.google.android.apps.socialcast"

    invoke-interface {v1, v2, v0}, Lihr;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 945
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private a(Lvy;)V
    .locals 3

    .prologue
    .line 602
    const-string v0, "CastService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Starting cast session with route: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->m:Lvy;

    .line 607
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Liid;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liid;

    .line 608
    invoke-virtual {p1}, Lvy;->m()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Liid;->a(Landroid/os/Bundle;)Liic;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->K:Liic;

    .line 609
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lihu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lihu;

    .line 610
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lihu;->a(Landroid/content/Context;Ljava/lang/String;)Lihr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    .line 611
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    new-instance v1, Lkrm;

    invoke-direct {v1, p0}, Lkrm;-><init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    invoke-interface {v0, v1}, Lihr;->a(Lihs;)V

    .line 612
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->K:Liic;

    invoke-interface {v0, v1}, Lihr;->a(Liic;)V

    .line 616
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    invoke-interface {v0}, Lihr;->a()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 621
    :goto_0
    return-void

    .line 617
    :catch_0
    move-exception v0

    .line 618
    const-string v1, "CastService"

    const-string v2, "Error connecting to API client"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 619
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a()V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->w:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->O:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->D:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->y:Z

    return p1
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->G:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->z:Z

    return p1
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->H:Ljava/lang/String;

    return-object p1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkrl;

    .line 340
    invoke-interface {v0}, Lkrl;->b()V

    goto :goto_0

    .line 342
    :cond_0
    return-void
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->h()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->A:Z

    return p1
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->r:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->I:Ljava/lang/String;

    return-object p1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkrl;

    .line 350
    invoke-interface {v0}, Lkrl;->d()V

    goto :goto_0

    .line 352
    :cond_0
    return-void
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->B:Z

    return p1
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lkfs;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->v:Lkfs;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 355
    const-string v0, "CastService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    const-string v0, "APP ID: "

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->i:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 362
    :cond_0
    :goto_0
    new-instance v0, Lvo;

    invoke-direct {v0}, Lvo;-><init>()V

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->J:Liig;

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->i:Ljava/lang/String;

    .line 363
    invoke-interface {v1, v2}, Liig;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 362
    invoke-virtual {v0, v1}, Lvo;->a(Ljava/lang/String;)Lvo;

    move-result-object v0

    .line 363
    invoke-virtual {v0}, Lvo;->a()Lvn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->j:Lvn;

    .line 364
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f:Lvp;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->j:Lvn;

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g:Lvq;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lvp;->a(Lvn;Lvq;I)V

    .line 366
    return-void

    .line 360
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Ljac;->a:Ljac;

    invoke-static {v0, p1, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->F:Lizu;

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->E:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->E:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->E:Lcom/google/android/libraries/social/media/MediaResource;

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->n:Lizs;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->F:Lizu;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p0}, Lizs;->a(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->E:Lcom/google/android/libraries/social/media/MediaResource;

    return-void
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->C:Z

    return p1
.end method

.method public static synthetic g(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lihr;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->k:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 536
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->k:Ljava/lang/Runnable;

    .line 538
    :cond_0
    return-void
.end method

.method public static synthetic g(Lcom/google/android/libraries/social/socialcast/impl/CastService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->x:Z

    return p1
.end method

.method private h()V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    invoke-interface {v0}, Lihr;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 650
    const-string v0, "remove_user"

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Ljava/lang/String;)V

    .line 653
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g()V

    .line 656
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e()V

    .line 658
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->i()V

    .line 661
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->stopSelf()V

    .line 662
    return-void
.end method

.method public static synthetic h(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->w:Z

    return v0
.end method

.method public static synthetic h(Lcom/google/android/libraries/social/socialcast/impl/CastService;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->o:Z

    return p1
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 670
    iput-boolean v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->o:Z

    .line 672
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->K:Liic;

    if-nez v0, :cond_0

    .line 701
    :goto_0
    return-void

    .line 676
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->w:Z

    .line 681
    iput-boolean v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->x:Z

    .line 684
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->v:Lkfs;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->r:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lkfs;->b(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 689
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    if-eqz v0, :cond_1

    .line 690
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    const-string v1, "urn:x-cast:com.google.android.apps.socialcast"

    invoke-interface {v0, v1}, Lihr;->a(Ljava/lang/String;)V

    .line 691
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    invoke-interface {v0}, Lihr;->b()V

    .line 692
    iput-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->L:Lihr;

    .line 693
    iput-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->p:Ljava/lang/String;

    .line 694
    iput-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->q:Ljava/lang/String;

    .line 695
    iput-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->s:Ljava/lang/String;

    .line 696
    iput-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->t:Ljava/lang/String;

    .line 699
    :cond_1
    iput-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->m:Lvy;

    .line 700
    iput-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->K:Liic;

    goto :goto_0

    .line 686
    :catch_0
    move-exception v0

    const-string v0, "CastService"

    const-string v1, "error invalidating auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static synthetic i(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    .locals 3

    .prologue
    .line 59
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v1, "command"

    const-string v2, "add_user"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "auth_token"

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "display_name"

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "id"

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "image_url"

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "show_private_posts"

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    invoke-virtual {v2}, Lkrp;->w()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v1, "data_source"

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static synthetic j(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lkrr;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->h:Lkrr;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->o:Z

    return v0
.end method

.method public static synthetic l(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lkro;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->l:Lkro;

    return-object v0
.end method

.method public static synthetic m(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lizs;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->n:Lizs;

    return-object v0
.end method

.method public static synthetic n(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->d()V

    return-void
.end method

.method public static synthetic o(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->p:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic p(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->v:Lkfs;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->r:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lkfs;->b(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->M:Lkfr;

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    invoke-virtual {v0}, Lkrp;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "oauth2: https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.recentread"

    :goto_1
    invoke-virtual {v1, v0}, Lkfr;->a(Ljava/lang/String;)Lkfs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->v:Lkfs;

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->l:Lkro;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->l:Lkro;

    invoke-virtual {v0, v3}, Lkro;->cancel(Z)Z

    :cond_1
    new-instance v0, Lkro;

    invoke-direct {v0, p0}, Lkro;-><init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->l:Lkro;

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->l:Lkro;

    new-array v1, v3, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->r:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lkro;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :catch_0
    move-exception v0

    const-string v0, "CastService"

    const-string v1, "error invalidating auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "oauth2: https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.recentpublicread"

    goto :goto_1
.end method

.method public static synthetic q(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->x:Z

    return v0
.end method

.method public static synthetic r(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->k:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic s(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->y:Z

    return v0
.end method

.method public static synthetic t(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->z:Z

    return v0
.end method

.method public static synthetic u(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->A:Z

    return v0
.end method

.method public static synthetic v(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->B:Z

    return v0
.end method

.method public static synthetic w(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->C:Z

    return v0
.end method

.method public static synthetic x(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lizu;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->F:Lizu;

    return-object v0
.end method

.method public static synthetic y(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Lcom/google/android/libraries/social/media/MediaResource;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->E:Lcom/google/android/libraries/social/media/MediaResource;

    return-object v0
.end method

.method public static synthetic z(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->D:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->m:Lvy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->m:Lvy;

    invoke-virtual {v0}, Lvy;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f:Lvp;

    invoke-virtual {v0}, Lvp;->b()Lvy;

    move-result-object v0

    invoke-virtual {v0}, Lvy;->n()V

    .line 637
    :goto_0
    return-void

    .line 635
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->h()V

    goto :goto_0
.end method

.method public a(Lkda;)V
    .locals 2

    .prologue
    .line 481
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    .line 482
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 485
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 486
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->d()V

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 473
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 477
    return-void
.end method

.method public h_()Llnh;
    .locals 1

    .prologue
    .line 277
    sget-object v0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->P:Llnh;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->O:Ljava/lang/String;

    .line 284
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 285
    if-nez v1, :cond_0

    .line 287
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    .line 321
    :goto_0
    return-object v0

    .line 290
    :cond_0
    const-string v0, "data_source"

    const-string v2, "stream"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->N:Ljava/lang/String;

    .line 291
    const-string v0, "selected_route_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->O:Ljava/lang/String;

    .line 292
    const-string v0, "shutdown"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 294
    const-string v0, "CastService"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    const-string v3, "onBind. circleName:%s routeId:%s, clientShutdown:%s mediaRouteSelector:%s"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->N:Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v5, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->O:Ljava/lang/String;

    aput-object v5, v4, v0

    const/4 v0, 0x2

    .line 297
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->j:Lvn;

    if-nez v0, :cond_3

    const-string v0, "null"

    :goto_1
    aput-object v0, v4, v5

    .line 296
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 295
    :cond_1
    if-nez v2, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->O:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 302
    :cond_2
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e()V

    .line 303
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    goto :goto_0

    .line 297
    :cond_3
    const-string v0, "not null"

    goto :goto_1

    .line 306
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->j:Lvn;

    if-nez v0, :cond_5

    .line 308
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f()V

    .line 310
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 316
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 317
    const-string v1, "account_id"

    iget v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->u:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 318
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 321
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 239
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->c:Ljava/util/HashSet;

    .line 240
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->d:Landroid/os/Handler;

    .line 241
    new-instance v0, Lkrp;

    invoke-direct {v0, p0}, Lkrp;-><init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    .line 242
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lvp;->a(Landroid/content/Context;)Lvp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f:Lvp;

    .line 243
    new-instance v0, Lkrq;

    invoke-direct {v0, p0}, Lkrq;-><init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g:Lvq;

    .line 244
    new-instance v0, Lkrr;

    invoke-direct {v0, p0}, Lkrr;-><init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->h:Lkrr;

    .line 245
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Liig;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liig;

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->J:Liig;

    .line 248
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 249
    const-string v1, "active-plus-account"

    invoke-interface {v0, v1}, Lhei;->c(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->u:I

    .line 250
    iget v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->u:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 251
    const-string v0, "effective_gaia_id"

    invoke-interface {v1, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "gaia_id"

    .line 253
    invoke-interface {v1, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->p:Ljava/lang/String;

    .line 255
    const-string v0, "display_name"

    invoke-interface {v1, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->q:Ljava/lang/String;

    .line 256
    const-string v0, "account_name"

    invoke-interface {v1, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->r:Ljava/lang/String;

    .line 257
    const-string v0, "profile_photo_url"

    invoke-interface {v1, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->s:Ljava/lang/String;

    .line 258
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->u:I

    invoke-static {v0, v1}, Lksd;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->i:Ljava/lang/String;

    .line 259
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->n:Lizs;

    .line 261
    new-instance v0, Lkfr;

    invoke-direct {v0}, Lkfr;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->M:Lkfr;

    .line 262
    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->M:Lkfr;

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    .line 263
    invoke-virtual {v0}, Lkrp;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "oauth2: https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.recentread"

    .line 262
    :goto_0
    invoke-virtual {v1, v0}, Lkfr;->a(Ljava/lang/String;)Lkfs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->v:Lkfs;

    .line 267
    new-instance v0, Lkrk;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    invoke-direct {v0, p0, v1}, Lkrk;-><init>(Landroid/content/Context;Lkrp;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->Q:Lkrk;

    .line 269
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->Q:Lkrk;

    invoke-virtual {v0, v1}, Lkrp;->a(Lkrl;)V

    .line 271
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    .line 272
    new-instance v1, Llnh;

    invoke-direct {v1, p0, v0}, Llnh;-><init>(Landroid/content/Context;Llnh;)V

    sput-object v1, Lcom/google/android/libraries/social/socialcast/impl/CastService;->P:Llnh;

    .line 273
    return-void

    .line 263
    :cond_1
    const-string v0, "oauth2: https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.recentpublicread"

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 508
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a:Z

    .line 510
    new-instance v0, Lkoe;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->r:Ljava/lang/String;

    .line 511
    invoke-virtual {v0, v1}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    .line 512
    invoke-virtual {v0, p0}, Lkoe;->a(Landroid/content/Context;)V

    .line 514
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->l:Lkro;

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->l:Lkro;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkro;->cancel(Z)Z

    .line 516
    iput-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->l:Lkro;

    .line 519
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g()V

    .line 522
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f:Lvp;

    invoke-virtual {v0}, Lvp;->c()Lvy;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->m:Lvy;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 523
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f:Lvp;

    invoke-virtual {v0}, Lvp;->b()Lvy;

    move-result-object v0

    invoke-virtual {v0}, Lvy;->n()V

    .line 525
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->h()V

    .line 528
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f:Lvp;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->g:Lvq;

    invoke-virtual {v0, v1}, Lvp;->a(Lvq;)V

    .line 529
    iput-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->j:Lvn;

    .line 530
    iput-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->i:Ljava/lang/String;

    .line 531
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 373
    sput-boolean v1, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a:Z

    .line 383
    iget-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->j:Lvn;

    if-nez v3, :cond_0

    .line 387
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f()V

    .line 391
    :cond_0
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 392
    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->K:Liic;

    if-nez v2, :cond_1

    .line 393
    const-string v1, "CastService"

    const-string v2, "Sent action when not connected. This shouldn\'t be possible. Make sure there is an intent filter and .CastService exported is true in Android Manifest"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->stopSelf()V

    .line 461
    :goto_0
    return v0

    .line 400
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 401
    const-string v2, "com.google.android.libraries.social.socialcast.action.next"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 402
    sget-object v0, Lonk;->g:Lhmn;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lhmn;)V

    .line 403
    const-string v0, "next"

    .line 415
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Ljava/lang/String;)V

    :cond_2
    :goto_2
    move v0, v1

    .line 417
    goto :goto_0

    .line 404
    :cond_3
    const-string v2, "com.google.android.libraries.social.socialcast.action.prev"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 405
    sget-object v0, Lonk;->k:Lhmn;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lhmn;)V

    .line 406
    const-string v0, "prev"

    goto :goto_1

    .line 407
    :cond_4
    const-string v2, "com.google.android.libraries.social.socialcast.action.stop"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 408
    sget-object v0, Lonk;->e:Lhmn;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lhmn;)V

    .line 409
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->e:Lkrp;

    invoke-virtual {v0}, Lkrp;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 410
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a()V

    goto :goto_2

    .line 412
    :cond_5
    const-string v2, "com.google.android.libraries.social.socialcast.action.toggle_playpause"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->y:Z

    if-eqz v0, :cond_6

    sget-object v0, Lonk;->h:Lhmn;

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lhmn;)V

    .line 415
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->y:Z

    if-eqz v0, :cond_7

    const-string v0, "pause"

    goto :goto_1

    .line 413
    :cond_6
    sget-object v0, Lonk;->i:Lhmn;

    goto :goto_3

    .line 415
    :cond_7
    const-string v0, "play"

    goto :goto_1

    .line 420
    :cond_8
    iget-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->K:Liic;

    if-eqz v3, :cond_9

    .line 421
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->i()V

    .line 424
    :cond_9
    if-eqz p1, :cond_a

    .line 425
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 426
    if-eqz v3, :cond_a

    .line 427
    const-string v4, "selected_route_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->O:Ljava/lang/String;

    .line 431
    :cond_a
    iget-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->O:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 432
    const-string v1, "CastService"

    const-string v2, "Started with no selected route ID."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->stopSelf()V

    goto/16 :goto_0

    .line 438
    :cond_b
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkrl;

    invoke-interface {v0}, Lkrl;->c()V

    goto :goto_4

    .line 442
    :cond_c
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->f:Lvp;

    invoke-virtual {v0}, Lvp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvy;

    .line 443
    invoke-virtual {v0}, Lvy;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->O:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 444
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lvy;)V

    move v0, v1

    .line 452
    :goto_5
    if-nez v0, :cond_e

    .line 456
    new-instance v0, Lkrn;

    invoke-direct {v0, p0}, Lkrn;-><init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->k:Ljava/lang/Runnable;

    .line 457
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->d:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->k:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2710

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 460
    :cond_e
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/CastService;->Q:Lkrk;

    invoke-virtual {v0}, Lkrk;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->startForeground(ILandroid/app/Notification;)V

    move v0, v1

    .line 461
    goto/16 :goto_0

    :cond_f
    move v0, v2

    goto :goto_5
.end method
