.class public Lcom/google/android/libraries/social/activityresult/ActivityResultModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    const-class v0, Lloc;

    if-ne p2, v0, :cond_1

    .line 23
    const-class v0, Lloc;

    const/4 v1, 0x3

    new-array v1, v1, [Lloc;

    const/4 v2, 0x0

    new-instance v3, Lhkq;

    invoke-direct {v3}, Lhkq;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lhkl;

    invoke-direct {v3}, Lhkl;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lhkf;

    invoke-direct {v3}, Lhkf;-><init>()V

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 35
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    const-class v0, Llop;

    if-ne p2, v0, :cond_2

    .line 29
    const-class v0, Llop;

    new-instance v1, Lhkg;

    invoke-direct {v1}, Lhkg;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 32
    :cond_2
    const-class v0, Lhko;

    if-ne p2, v0, :cond_0

    .line 33
    const-class v0, Lhko;

    new-instance v1, Lhko;

    invoke-direct {v1, p1}, Lhko;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0
.end method
