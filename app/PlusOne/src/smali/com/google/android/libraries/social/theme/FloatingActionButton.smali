.class public Lcom/google/android/libraries/social/theme/FloatingActionButton;
.super Landroid/widget/ImageButton;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const v0, 0x7f0200ea

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/theme/FloatingActionButton;->setBackgroundResource(I)V

    .line 21
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    const v0, 0x7f050016

    invoke-static {p1, v0}, Landroid/animation/AnimatorInflater;->loadStateListAnimator(Landroid/content/Context;I)Landroid/animation/StateListAnimator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/theme/FloatingActionButton;->setStateListAnimator(Landroid/animation/StateListAnimator;)V

    .line 25
    :cond_0
    return-void
.end method
