.class public Lcom/google/android/libraries/social/ingest/IngestActivity;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Lisi;
.implements Lisp;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# instance fields
.field private a:Lcom/google/android/libraries/social/ingest/IngestService;

.field private b:Z

.field private c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

.field private d:Lisd;

.field private e:Landroid/os/Handler;

.field private f:Landroid/app/ProgressDialog;

.field private g:Landroid/view/ActionMode;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:I

.field private k:Landroid/support/v4/view/ViewPager;

.field private l:Lise;

.field private m:Z

.field private n:Landroid/view/MenuItem;

.field private o:Landroid/view/MenuItem;

.field private p:Landroid/widget/AdapterView$OnItemClickListener;

.field private q:Landroid/widget/AbsListView$MultiChoiceModeListener;

.field private r:Lirx;

.field private s:Landroid/database/DataSetObserver;

.field private t:Liry;

.field private u:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 67
    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->b:Z

    .line 76
    iput v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->j:I

    .line 80
    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->m:Z

    .line 114
    new-instance v0, Lirs;

    invoke-direct {v0, p0}, Lirs;-><init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->p:Landroid/widget/AdapterView$OnItemClickListener;

    .line 123
    new-instance v0, Lirt;

    invoke-direct {v0, p0}, Lirt;-><init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->q:Landroid/widget/AbsListView$MultiChoiceModeListener;

    .line 290
    new-instance v0, Lirx;

    invoke-direct {v0, p0}, Lirx;-><init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->r:Lirx;

    .line 337
    new-instance v0, Liru;

    invoke-direct {v0, p0}, Liru;-><init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->s:Landroid/database/DataSetObserver;

    .line 445
    new-instance v0, Liry;

    invoke-direct {v0}, Liry;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    .line 589
    new-instance v0, Lirv;

    invoke-direct {v0, p0}, Lirv;-><init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->u:Landroid/content/ServiceConnection;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/IngestActivity;I)I
    .locals 0

    .prologue
    .line 63
    iput p1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->j:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/IngestActivity;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->g:Landroid/view/ActionMode;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/IngestActivity;Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->o:Landroid/view/MenuItem;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/IngestActivity;Lcom/google/android/libraries/social/ingest/IngestService;)Lcom/google/android/libraries/social/ingest/IngestService;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->a:Lcom/google/android/libraries/social/ingest/IngestService;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/ui/IngestGridView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 270
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->h:Landroid/view/View;

    if-nez v0, :cond_0

    .line 271
    const v0, 0x7f100334

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->h:Landroid/view/View;

    .line 272
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->h:Landroid/view/View;

    const v1, 0x7f100336

    .line 273
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->i:Landroid/widget/TextView;

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 276
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 277
    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Z)V

    .line 278
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->setVisibility(I)V

    .line 279
    invoke-direct {p0, v2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->b(Z)V

    .line 280
    return-void
.end method

.method private a(Landroid/view/MenuItem;Z)V
    .locals 1

    .prologue
    .line 364
    if-nez p1, :cond_0

    .line 374
    :goto_0
    return-void

    .line 367
    :cond_0
    if-nez p2, :cond_1

    .line 368
    const v0, 0x1080059

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 369
    const v0, 0x7f0a040a

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 371
    :cond_1
    const v0, 0x1080028

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 372
    const v0, 0x7f0a0409

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ingest/IngestActivity;Landroid/view/MenuItem;Z)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Landroid/view/MenuItem;Z)V

    return-void
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 377
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->m:Z

    .line 378
    if-eqz p1, :cond_5

    .line 379
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->l:Lise;

    if-nez v0, :cond_0

    .line 380
    new-instance v0, Lise;

    iget-object v3, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->r:Lirx;

    invoke-direct {v0, p0, v3}, Lise;-><init>(Landroid/content/Context;Lisb;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->l:Lise;

    .line 381
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->l:Lise;

    iget-object v3, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    invoke-virtual {v3}, Lisd;->a()Liso;

    move-result-object v3

    invoke-virtual {v0, v3}, Lise;->a(Liso;)V

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->k:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->l:Lise;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->a(Lip;)V

    .line 384
    iget-object v3, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->k:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->l:Lise;

    .line 385
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->getFirstVisiblePosition()I

    move-result v0

    iget v5, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->j:I

    if-le v5, v0, :cond_1

    iget v5, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->j:I

    iget-object v6, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    invoke-virtual {v6}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->getLastVisiblePosition()I

    move-result v6

    if-le v5, v6, :cond_4

    .line 384
    :cond_1
    :goto_0
    invoke-virtual {v4, v0}, Lise;->a(I)I

    move-result v0

    invoke-virtual {v3, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 391
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    if-eqz p1, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->setVisibility(I)V

    .line 392
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->k:Landroid/support/v4/view/ViewPager;

    if-eqz p1, :cond_7

    :goto_3
    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 393
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->o:Landroid/view/MenuItem;

    if-eqz v0, :cond_3

    .line 394
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->o:Landroid/view/MenuItem;

    invoke-direct {p0, v0, p1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Landroid/view/MenuItem;Z)V

    .line 396
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->n:Landroid/view/MenuItem;

    invoke-direct {p0, v0, p1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Landroid/view/MenuItem;Z)V

    .line 397
    return-void

    .line 385
    :cond_4
    iget v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->j:I

    goto :goto_0

    .line 386
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->l:Lise;

    if-eqz v0, :cond_2

    .line 387
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    iget-object v3, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    iget-object v4, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->k:Landroid/support/v4/view/ViewPager;

    .line 388
    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v4

    .line 387
    invoke-virtual {v3, v4}, Lisd;->b(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->setSelection(I)V

    .line 389
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->k:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->a(Lip;)V

    goto :goto_1

    :cond_6
    move v0, v2

    .line 391
    goto :goto_2

    :cond_7
    move v2, v1

    .line 392
    goto :goto_3
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lisd;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    return-object v0
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->o:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->o:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->n:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->n:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 406
    :cond_1
    return-void
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lirx;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->r:Lirx;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/ingest/IngestActivity;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->o:Landroid/view/MenuItem;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    invoke-virtual {v0}, Lisd;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 410
    const v0, 0x7f0a0408

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(I)V

    .line 416
    :goto_0
    return-void

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    invoke-virtual {v0}, Lisd;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    invoke-virtual {v0}, Lisd;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 412
    const v0, 0x7f0a0407

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(I)V

    goto :goto_0

    .line 414
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->h:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Z)V

    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->b(Z)V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/ingest/IngestActivity;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->m:Z

    return v0
.end method

.method private f()Landroid/app/ProgressDialog;
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 507
    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->f:Landroid/app/ProgressDialog;

    .line 508
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->f:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 510
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->f:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/ingest/IngestActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lise;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->l:Lise;

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 542
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->f:Landroid/app/ProgressDialog;

    .line 544
    :cond_0
    return-void
.end method

.method public static synthetic h(Lcom/google/android/libraries/social/ingest/IngestActivity;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->b:Z

    return v0
.end method

.method public static synthetic i(Lcom/google/android/libraries/social/ingest/IngestActivity;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->g()V

    return-void
.end method

.method public static synthetic j(Lcom/google/android/libraries/social/ingest/IngestActivity;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    invoke-direct {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->f()Landroid/app/ProgressDialog;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iget v0, v0, Liry;->d:I

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    if-eqz v0, :cond_5

    :goto_1
    invoke-virtual {v3, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iget-object v1, v1, Liry;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iget-object v1, v1, Liry;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iget-object v1, v1, Liry;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iget-object v1, v1, Liry;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iget v0, v0, Liry;->c:I

    invoke-virtual {v3, v0}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iget v0, v0, Liry;->d:I

    invoke-virtual {v3, v0}, Landroid/app/ProgressDialog;->setMax(I)V

    :cond_2
    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_1
.end method

.method public static synthetic k(Lcom/google/android/libraries/social/ingest/IngestActivity;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    invoke-virtual {v0}, Lisd;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->g:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->g:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->g:Landroid/view/ActionMode;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->e()V

    return-void
.end method

.method public static synthetic l(Lcom/google/android/libraries/social/ingest/IngestActivity;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->f()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    return-void
.end method

.method public static synthetic m(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/IngestService;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->a:Lcom/google/android/libraries/social/ingest/IngestService;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 429
    return-void
.end method

.method public a(IILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 477
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    invoke-virtual {v0}, Liry;->a()V

    .line 478
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iput p2, v0, Liry;->d:I

    .line 479
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iput p1, v0, Liry;->c:I

    .line 480
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0406

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Liry;->b:Ljava/lang/String;

    .line 481
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 482
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 483
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 485
    return-void
.end method

.method public a(Lisj;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 450
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    invoke-virtual {v0}, Liry;->a()V

    .line 451
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iput v5, v0, Liry;->d:I

    .line 452
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110015

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 453
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 452
    invoke-virtual {v1, v2, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Liry;->a:Ljava/lang/String;

    .line 454
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 455
    return-void
.end method

.method public a(Ljava/util/Collection;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lisj;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 492
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 495
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 460
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    invoke-virtual {v0}, Liry;->a()V

    .line 461
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    iput v3, v0, Liry;->d:I

    .line 462
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->t:Liry;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0404

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Liry;->a:Ljava/lang/String;

    .line 463
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 464
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 470
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 471
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 500
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 501
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 503
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 265
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 266
    invoke-static {p0}, Lisk;->a(Landroid/content/Context;)V

    .line 267
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/libraries/social/ingest/IngestService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->u:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 98
    const v0, 0x7f0400e9

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->setContentView(I)V

    .line 99
    const v0, 0x7f100332

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    .line 100
    new-instance v0, Lisd;

    invoke-direct {v0, p0}, Lisd;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->s:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lisd;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->q:Landroid/widget/AbsListView$MultiChoiceModeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->p:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->r:Lirx;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->a(Lisv;)V

    .line 107
    const v0, 0x7f100333

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->k:Landroid/support/v4/view/ViewPager;

    .line 109
    new-instance v0, Lirw;

    invoke-direct {v0, p0}, Lirw;-><init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->e:Landroid/os/Handler;

    .line 111
    invoke-static {p0}, Lisk;->a(Landroid/content/Context;)V

    .line 112
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 229
    const v1, 0x7f12000a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 230
    const v0, 0x7f1006e8

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->n:Landroid/view/MenuItem;

    .line 231
    const v0, 0x7f1006e9

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 232
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->n:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->m:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Landroid/view/MenuItem;Z)V

    .line 233
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->a:Lcom/google/android/libraries/social/ingest/IngestService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->a:Lcom/google/android/libraries/social/ingest/IngestService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ingest/IngestService;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->u:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 239
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 240
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 210
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 211
    const v2, 0x7f1006e9

    if-ne v0, v2, :cond_1

    .line 212
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->g:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->a:Lcom/google/android/libraries/social/ingest/IngestService;

    iget-object v2, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->c:Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    .line 214
    invoke-virtual {v2}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->d:Lisd;

    .line 213
    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/ingest/IngestService;->a(Landroid/util/SparseBooleanArray;Landroid/widget/Adapter;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->g:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 223
    :cond_0
    :goto_0
    return v1

    .line 219
    :cond_1
    const v2, 0x7f1006e8

    if-ne v0, v2, :cond_3

    .line 220
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->m:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 223
    :cond_3
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->a:Lcom/google/android/libraries/social/ingest/IngestService;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->a:Lcom/google/android/libraries/social/ingest/IngestService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ingest/IngestService;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    .line 258
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->b:Z

    .line 259
    invoke-direct {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->g()V

    .line 260
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 261
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 244
    invoke-static {}, Lcom/google/android/libraries/social/ingest/ui/DateTileView;->a()Z

    .line 245
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->b:Z

    .line 246
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->a:Lcom/google/android/libraries/social/ingest/IngestService;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestActivity;->a:Lcom/google/android/libraries/social/ingest/IngestService;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ingest/IngestService;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    .line 249
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->e()V

    .line 250
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 251
    return-void
.end method
