.class public final Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;
.super Landroid/widget/LinearLayout;
.source "PG"


# static fields
.field private static a:I

.field private static b:I

.field private static c:I

.field private static d:I


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 19
    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    .line 20
    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b:I

    .line 21
    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->c:I

    .line 22
    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->d:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    iput v1, p0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->e:I

    .line 40
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->setOrientation(I)V

    .line 41
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->setGravity(I)V

    .line 43
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    if-ne v0, v2, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0234

    .line 45
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    .line 48
    :cond_0
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b:I

    if-ne v0, v2, :cond_1

    .line 49
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0235

    .line 50
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b:I

    .line 53
    :cond_1
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->c:I

    if-ne v0, v2, :cond_2

    .line 54
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0133

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->c:I

    .line 57
    :cond_2
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->d:I

    if-ne v0, v2, :cond_3

    .line 58
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0134

    .line 59
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->d:I

    .line 28
    :cond_3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 31
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    iput v0, p0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->e:I

    .line 40
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->setOrientation(I)V

    .line 41
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->setGravity(I)V

    .line 43
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    if-ne v0, v2, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0234

    .line 45
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    .line 48
    :cond_0
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b:I

    if-ne v0, v2, :cond_1

    .line 49
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0235

    .line 50
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b:I

    .line 53
    :cond_1
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->c:I

    if-ne v0, v2, :cond_2

    .line 54
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0133

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->c:I

    .line 57
    :cond_2
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->d:I

    if-ne v0, v2, :cond_3

    .line 58
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0134

    .line 59
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->d:I

    .line 32
    :cond_3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    iput v0, p0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->e:I

    .line 40
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->setOrientation(I)V

    .line 41
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->setGravity(I)V

    .line 43
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    if-ne v0, v2, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0234

    .line 45
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    .line 48
    :cond_0
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b:I

    if-ne v0, v2, :cond_1

    .line 49
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0235

    .line 50
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b:I

    .line 53
    :cond_1
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->c:I

    if-ne v0, v2, :cond_2

    .line 54
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0133

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->c:I

    .line 57
    :cond_2
    sget v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->d:I

    if-ne v0, v2, :cond_3

    .line 58
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0134

    .line 59
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->d:I

    .line 36
    :cond_3
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 68
    if-gez p1, :cond_0

    .line 69
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "size must be greater than zero."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    iput p1, p0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->e:I

    .line 72
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->removeAllViews()V

    move v0, v1

    .line 73
    :goto_0
    if-ge v0, p1, :cond_1

    .line 74
    sget v2, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->c:I

    new-instance v3, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v4, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v4}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v3}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget v2, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicWidth(I)V

    sget v2, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicHeight(I)V

    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget v3, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b:I

    div-int/lit8 v3, v3, 0x2

    sget v4, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->b:I

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v2, v3, v1, v4, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    sget v4, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    sget v5, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->a:I

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->addView(Landroid/view/View;)V

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_1
    return-void
.end method

.method public b(I)V
    .locals 5

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->e:I

    if-le p1, v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "selected index: %s larger than size: %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->e:I

    .line 81
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    const-string v0, "DOT_SELECTED_TAG"

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 84
    if-eqz v0, :cond_1

    .line 85
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    sget v2, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->c:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 86
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 87
    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 89
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 90
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    sget v2, Lcom/google/android/libraries/social/acl2/ShareouselDotMapView;->d:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    const-string v1, "DOT_SELECTED_TAG"

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 92
    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 93
    return-void
.end method
