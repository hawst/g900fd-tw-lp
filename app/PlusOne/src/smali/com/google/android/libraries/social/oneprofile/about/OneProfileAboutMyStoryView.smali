.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;
.super Ljnl;
.source "PG"


# instance fields
.field private a:Landroid/view/ViewGroup;

.field private o:Landroid/view/View;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/view/ViewGroup;

.field private u:Landroid/view/View;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public static a(Lnjt;ZZZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 254
    if-eqz p1, :cond_1

    if-nez p3, :cond_1

    .line 280
    :cond_0
    :goto_0
    return v0

    .line 257
    :cond_1
    if-eqz p0, :cond_4

    .line 258
    iget-object v1, p0, Lnjt;->d:Lnib;

    .line 259
    if-eqz v1, :cond_3

    .line 260
    iget-object v2, v1, Lnib;->d:Lnjv;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lnib;->d:Lnjv;

    iget-object v2, v2, Lnjv;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 265
    :cond_2
    iget-object v2, v1, Lnib;->e:Lnjv;

    if-eqz v2, :cond_3

    iget-object v1, v1, Lnib;->e:Lnjv;

    iget-object v1, v1, Lnjv;->b:Ljava/lang/String;

    .line 266
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 270
    :cond_3
    if-nez p2, :cond_4

    .line 271
    iget-object v1, p0, Lnjt;->e:Lnkd;

    .line 272
    if-eqz v1, :cond_4

    .line 273
    iget-object v2, v1, Lnkd;->j:Lnjv;

    if-eqz v2, :cond_4

    iget-object v1, v1, Lnkd;->j:Lnjv;

    iget-object v1, v1, Lnjv;->b:Ljava/lang/String;

    .line 274
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-super {p0}, Ljnl;->a()V

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 83
    return-void
.end method

.method public a(Lnjt;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 213
    .line 217
    if-eqz p1, :cond_5

    .line 218
    iget-object v2, p1, Lnjt;->d:Lnib;

    .line 219
    if-eqz v2, :cond_4

    .line 220
    iget-object v0, v2, Lnib;->d:Lnjv;

    if-eqz v0, :cond_3

    .line 221
    iget-object v0, v2, Lnib;->d:Lnjv;

    iget-object v0, v0, Lnjv;->b:Ljava/lang/String;

    .line 223
    :goto_0
    iget-object v3, v2, Lnib;->e:Lnjv;

    if-eqz v3, :cond_2

    .line 224
    iget-object v2, v2, Lnib;->e:Lnjv;

    iget-object v2, v2, Lnjv;->b:Ljava/lang/String;

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    .line 227
    :goto_1
    iget-object v3, p1, Lnjt;->e:Lnkd;

    .line 228
    if-eqz v3, :cond_1

    .line 229
    iget-object v4, v3, Lnkd;->j:Lnjv;

    if-eqz v4, :cond_1

    .line 230
    iget-object v1, v3, Lnkd;->j:Lnjv;

    iget-object v1, v1, Lnjv;->b:Ljava/lang/String;

    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    .line 235
    :goto_2
    invoke-virtual {p0, v0, v5}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 237
    invoke-virtual {p0, v2, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->b(Ljava/lang/String;Z)Z

    move-result v0

    .line 238
    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->c(Ljava/lang/String;Z)Z

    .line 240
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lldx;

    .line 241
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x200

    if-le v1, v2, :cond_0

    .line 242
    const/4 v1, 0x2

    iput v1, v0, Lldx;->a:I

    .line 246
    :goto_3
    return-void

    .line 244
    :cond_0
    iput v5, v0, Lldx;->a:I

    goto :goto_3

    :cond_1
    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    goto :goto_2

    :cond_2
    move-object v2, v0

    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v0, v1

    move-object v2, v1

    goto :goto_1

    :cond_5
    move-object v2, v1

    move-object v0, v1

    goto :goto_2
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 87
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->j:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    move v3, v1

    .line 88
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->j:Z

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->l:Z

    if-nez v4, :cond_3

    move v4, v1

    .line 89
    :goto_2
    if-eqz v3, :cond_7

    .line 90
    if-eqz v4, :cond_4

    .line 91
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020416

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 91
    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a(Landroid/view/View;)V

    .line 94
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    new-instance v6, Ljmz;

    invoke-direct {v6, p0}, Ljmz;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->o:Landroid/view/View;

    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 107
    :goto_3
    if-eqz v0, :cond_5

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a(Landroid/widget/TextView;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->q:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->d(Landroid/widget/TextView;)V

    .line 123
    :goto_4
    if-eqz p2, :cond_8

    if-nez v3, :cond_8

    :goto_5
    return v1

    :cond_1
    move v0, v2

    .line 86
    goto :goto_0

    :cond_2
    move v3, v2

    .line 87
    goto :goto_1

    :cond_3
    move v4, v2

    .line 88
    goto :goto_2

    .line 105
    :cond_4
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->o:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 112
    :cond_5
    if-eqz v4, :cond_6

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->q:Landroid/widget/TextView;

    const v4, 0x7f0a0302

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->e(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->b(Landroid/widget/TextView;Z)V

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->q:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->e(Landroid/widget/TextView;)V

    goto :goto_4

    .line 118
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_4

    .line 121
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_4

    :cond_8
    move v1, v2

    .line 123
    goto :goto_5
.end method

.method public b(Ljava/lang/String;Z)Z
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v1

    .line 130
    :goto_0
    if-eqz v4, :cond_6

    .line 131
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->r:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a(Landroid/widget/TextView;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    new-instance v0, Ljnb;

    invoke-direct {v0}, Ljnb;-><init>()V

    invoke-static {p1, v0}, Llir;->a(Ljava/lang/String;Landroid/text/Html$TagHandler;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 139
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    move v3, v2

    .line 141
    :goto_1
    if-eq v3, v0, :cond_1

    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 142
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    move v4, v2

    .line 129
    goto :goto_0

    .line 144
    :cond_1
    if-eqz v3, :cond_2

    .line 145
    invoke-virtual {v5, v2, v3}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 146
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 149
    :cond_2
    add-int/lit8 v3, v0, -0x1

    .line 150
    :goto_2
    if-ltz v3, :cond_3

    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 151
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .line 153
    :cond_3
    add-int/lit8 v6, v0, -0x1

    if-eq v3, v6, :cond_4

    .line 154
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v5, v3, v0}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->s:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->d(Landroid/widget/TextView;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->s:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    .line 162
    instance-of v0, v0, Landroid/text/method/LinkMovementMethod;

    if-nez v0, :cond_5

    .line 163
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->s:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 170
    :cond_5
    :goto_3
    if-eqz p2, :cond_7

    if-nez v4, :cond_7

    move v0, v1

    :goto_4
    return v0

    .line 167
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_7
    move v0, v2

    .line 170
    goto :goto_4
.end method

.method public c(Ljava/lang/String;Z)Z
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 174
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 175
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->j:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->k:Z

    if-eqz v3, :cond_1

    :cond_0
    if-eqz v0, :cond_3

    :cond_1
    move v3, v1

    .line 176
    :goto_1
    if-eqz v3, :cond_6

    .line 177
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 178
    iget-boolean v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->j:Z

    if-eqz v4, :cond_4

    .line 179
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->t:Landroid/view/ViewGroup;

    .line 180
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020416

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 179
    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 181
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->t:Landroid/view/ViewGroup;

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a(Landroid/view/View;)V

    .line 182
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->t:Landroid/view/ViewGroup;

    new-instance v5, Ljna;

    invoke-direct {v5, p0}, Ljna;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->u:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 195
    :goto_2
    if-eqz v0, :cond_5

    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->v:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a(Landroid/widget/TextView;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->w:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->d(Landroid/widget/TextView;)V

    .line 208
    :goto_3
    if-eqz p2, :cond_7

    if-nez v3, :cond_7

    :goto_4
    return v1

    :cond_2
    move v0, v2

    .line 174
    goto :goto_0

    :cond_3
    move v3, v2

    .line 175
    goto :goto_1

    .line 193
    :cond_4
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->u:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 200
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->w:Landroid/widget/TextView;

    const v4, 0x7f0a0305

    .line 201
    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->e(I)Ljava/lang/String;

    move-result-object v4

    .line 200
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->v:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->b(Landroid/widget/TextView;Z)V

    .line 203
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->w:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->e(Landroid/widget/TextView;)V

    goto :goto_3

    .line 206
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_3

    :cond_7
    move v1, v2

    .line 208
    goto :goto_4
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 62
    const v0, 0x7f100434

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a:Landroid/view/ViewGroup;

    .line 63
    const v0, 0x7f100435

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->o:Landroid/view/View;

    .line 64
    const v0, 0x7f100436

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->p:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f100437

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->q:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f100438

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->r:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f100439

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->s:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f10043a

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->t:Landroid/view/ViewGroup;

    .line 71
    const v0, 0x7f10043b

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->u:Landroid/view/View;

    .line 72
    const v0, 0x7f10043c

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->v:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f10043d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->w:Landroid/widget/TextView;

    .line 74
    return-void
.end method
