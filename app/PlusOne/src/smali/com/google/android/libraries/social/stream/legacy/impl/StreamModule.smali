.class public Lcom/google/android/libraries/social/stream/legacy/impl/StreamModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    const-class v0, Lkzl;

    if-ne p2, v0, :cond_1

    .line 23
    const-class v0, Lkzl;

    new-instance v1, Llbv;

    invoke-direct {v1, p1}, Llbv;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const-class v0, Lkzk;

    if-ne p2, v0, :cond_2

    .line 25
    const-class v0, Lkzk;

    new-instance v1, Llbl;

    invoke-direct {v1, p1}, Llbl;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 26
    const-class v0, Lkzk;

    new-instance v1, Lcom/google/android/libraries/social/stream/legacy/impl/PromoItemStoreExtension;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/stream/legacy/impl/PromoItemStoreExtension;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 27
    const-class v0, Lkzk;

    new-instance v1, Llbw;

    invoke-direct {v1}, Llbw;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 28
    :cond_2
    const-class v0, Lief;

    if-ne p2, v0, :cond_3

    .line 29
    const-class v0, Lief;

    const/16 v1, 0x8

    new-array v1, v1, [Lief;

    const/4 v2, 0x0

    sget-object v3, Llbq;->a:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Llbq;->c:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Llbq;->d:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Llbq;->e:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Llbq;->b:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Llbq;->f:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Llbq;->g:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Llbq;->h:Lief;

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37
    :cond_3
    const-class v0, Lhzs;

    if-ne p2, v0, :cond_4

    .line 38
    const-class v0, Lhzs;

    new-instance v1, Llbp;

    invoke-direct {v1}, Llbp;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 39
    :cond_4
    const-class v0, Ligx;

    if-ne p2, v0, :cond_5

    .line 40
    const-class v0, Ligx;

    new-instance v1, Llbr;

    invoke-direct {v1}, Llbr;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 41
    :cond_5
    const-class v0, Llbm;

    if-ne p2, v0, :cond_6

    .line 42
    const-class v0, Llbm;

    new-instance v1, Llbm;

    invoke-direct {v1, p1}, Llbm;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 43
    :cond_6
    const-class v0, Llcg;

    if-ne p2, v0, :cond_0

    .line 44
    const-class v0, Llcg;

    new-instance v1, Llcg;

    invoke-direct {v1, p1}, Llcg;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0
.end method
