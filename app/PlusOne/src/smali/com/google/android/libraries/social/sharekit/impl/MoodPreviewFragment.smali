.class public Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;
.super Llol;
.source "PG"

# interfaces
.implements Lklc;


# instance fields
.field private N:Lklb;

.field private O:Landroid/view/ViewGroup;

.field private P:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private Q:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Llol;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;)Lklb;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->N:Lklb;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->N:Lklb;

    invoke-virtual {v0}, Lklb;->b()Lkkz;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 74
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 77
    :goto_0
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->P:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->P:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->N:Lklb;

    invoke-virtual {v0}, Lklb;->b()Lkkz;

    move-result-object v0

    invoke-virtual {v0}, Lkkz;->d()Lkkp;

    move-result-object v0

    .line 81
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->P:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-nez v0, :cond_1

    move-object v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->Q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->Q:Landroid/view/View;

    new-instance v2, Lklf;

    invoke-direct {v2, p0}, Lklf;-><init>(Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->P:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->O:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 103
    :goto_2
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->n()Lz;

    move-result-object v2

    invoke-virtual {v0}, Lkkp;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Ljac;->d:Ljac;

    invoke-static {v2, v0, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    goto :goto_1

    .line 98
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->Q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->Q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->P:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->O:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 36
    const v0, 0x7f0401df

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 38
    const v0, 0x7f100585

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->O:Landroid/view/ViewGroup;

    .line 39
    const v0, 0x7f100586

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->P:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 40
    const v0, 0x7f100587

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->Q:Landroid/view/View;

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->O:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->au:Llnh;

    const-class v1, Lklb;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lklb;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->N:Lklb;

    .line 32
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Llol;->aO_()V

    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->N:Lklb;

    invoke-virtual {v0, p0}, Lklb;->a(Lklc;)V

    .line 55
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->a()V

    .line 56
    return-void
.end method

.method public ae_()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->P:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 61
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->Q:Landroid/view/View;

    .line 62
    invoke-super {p0}, Llol;->ae_()V

    .line 63
    return-void
.end method

.method public y()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->a()V

    .line 69
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/MoodPreviewFragment;->N:Lklb;

    invoke-virtual {v0, p0}, Lklb;->b(Lklc;)V

    .line 48
    invoke-super {p0}, Llol;->z()V

    .line 49
    return-void
.end method
