.class public Lcom/google/android/libraries/social/location/FriendLocationsListItemView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Litn;

.field private d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/ProgressBar;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 136
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 137
    invoke-virtual {v0, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 138
    const v1, 0x7f120001

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->inflate(I)V

    .line 139
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 140
    return-void
.end method

.method public a(Lnhm;Litn;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 61
    iput-object p4, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->b:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->c:Litn;

    .line 63
    iget-object v0, p1, Lnhm;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->a:Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v1, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->a:Ljava/lang/String;

    iget-object v2, p1, Lnhm;->e:Ljava/lang/String;

    .line 66
    invoke-static {v2}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 65
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->e:Landroid/widget/TextView;

    iget-object v1, p1, Lnhm;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p1, Lnhm;->c:[Lnij;

    .line 69
    invoke-static {v0}, Liuo;->a([Lnij;)Lnij;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_2

    iget-object v1, v0, Lnij;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 71
    iget-object v1, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->f:Landroid/widget/TextView;

    iget-object v0, v0, Lnij;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p1, Lnhm;->f:Lnhi;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lnhm;->f:Lnhi;

    iget v0, v0, Lnhi;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lnhm;->f:Lnhi;

    iget v0, v0, Lnhi;->a:I

    if-nez v0, :cond_3

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 92
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 93
    const-string v1, "active-plus-account"

    invoke-interface {v0, v1}, Lhei;->c(Ljava/lang/String;)I

    move-result v1

    .line 94
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->i:Z

    .line 97
    iget-boolean v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->i:Z

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 100
    const v0, 0x7f1002af

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 102
    :cond_1
    return-void

    .line 73
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->f:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 86
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 89
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->c:Litn;

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-ne p1, v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->c:Litn;

    iget-object v1, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Litn;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->c:Litn;

    iget-object v1, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Litn;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 114
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-nez v0, :cond_0

    .line 115
    const v0, 0x7f100174

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 116
    const v0, 0x7f10013d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->e:Landroid/widget/TextView;

    .line 117
    const v0, 0x7f1002b1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->f:Landroid/widget/TextView;

    .line 118
    const v0, 0x7f1002b0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->g:Landroid/widget/TextView;

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    .line 120
    const v0, 0x7f1002ae

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->h:Landroid/widget/ProgressBar;

    .line 121
    new-instance v0, Litt;

    invoke-direct {v0, p0}, Litt;-><init>(Lcom/google/android/libraries/social/location/FriendLocationsListItemView;)V

    .line 128
    iget-object v1, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->d:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v1, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    const v1, 0x7f1002ac

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    const v1, 0x7f1002af

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    :cond_0
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->c:Litn;

    if-nez v0, :cond_0

    .line 145
    const/4 v0, 0x0

    .line 154
    :goto_0
    return v0

    .line 147
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 148
    const v1, 0x7f10066e

    if-ne v0, v1, :cond_2

    .line 149
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->c:Litn;

    iget-object v1, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Litn;->c(Ljava/lang/String;)V

    .line 154
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 150
    :cond_2
    const v1, 0x7f10066f

    if-ne v0, v1, :cond_1

    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->c:Litn;

    iget-object v1, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/social/location/FriendLocationsListItemView;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Litn;->d(Ljava/lang/String;)V

    goto :goto_1
.end method
