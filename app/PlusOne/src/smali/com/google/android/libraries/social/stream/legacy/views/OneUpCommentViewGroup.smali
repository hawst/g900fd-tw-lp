.class public Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lkdd;
.implements Lljh;


# static fields
.field private static a:Z

.field private static b:I

.field private static c:I

.field private static d:I

.field private static e:I

.field private static f:Landroid/graphics/Paint;

.field private static g:I

.field private static h:Landroid/graphics/Paint;

.field private static i:Landroid/graphics/drawable/Drawable;

.field private static j:I

.field private static k:Landroid/graphics/drawable/Drawable;

.field private static l:F

.field private static m:I

.field private static n:I

.field private static o:Landroid/graphics/Paint;

.field private static p:Landroid/graphics/drawable/Drawable;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private D:Llip;

.field private E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private F:Lljg;

.field private G:Lljg;

.field private H:Lljg;

.field private I:Llir;

.field private J:Z

.field private K:Landroid/graphics/Rect;

.field private L:Landroid/graphics/Point;

.field private M:Lldh;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Landroid/text/Spanned;

.field private v:Ljava/lang/String;

.field private w:I

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 128
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 132
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 135
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 109
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->C:Ljava/util/Set;

    .line 119
    iput-boolean v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->J:Z

    .line 137
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->setClickable(Z)V

    .line 138
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->setFocusable(Z)V

    .line 139
    invoke-virtual {p0, p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->setWillNotDraw(Z)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 143
    sget-boolean v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a:Z

    if-nez v1, :cond_0

    .line 144
    const v1, 0x7f0d01c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->c:I

    .line 145
    const v1, 0x7f0d01c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->b:I

    .line 146
    const v1, 0x7f0d01c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->d:I

    .line 147
    const v1, 0x7f0d01c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->e:I

    .line 149
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 150
    sput-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->f:Landroid/graphics/Paint;

    const v2, 0x7f0b0119

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 152
    const v1, 0x7f0d01c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->g:I

    .line 155
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 156
    sput-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->h:Landroid/graphics/Paint;

    const v2, 0x7f0b011a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 159
    const v1, 0x7f0201bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->i:Landroid/graphics/drawable/Drawable;

    .line 160
    const v1, 0x7f0d01c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->j:I

    .line 163
    const v1, 0x7f0201d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->k:Landroid/graphics/drawable/Drawable;

    .line 164
    const v1, 0x7f0d01ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->l:F

    .line 166
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->m:I

    .line 167
    const v1, 0x7f0b011b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->n:I

    .line 169
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 170
    sput-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->o:Landroid/graphics/Paint;

    const v2, 0x7f0b010f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 171
    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->o:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 172
    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->o:Landroid/graphics/Paint;

    const v2, 0x7f0d019c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 174
    const v1, 0x7f020415

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->p:Landroid/graphics/drawable/Drawable;

    .line 177
    sput-boolean v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a:Z

    .line 180
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v4, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->L:Landroid/graphics/Point;

    .line 181
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 183
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->addView(Landroid/view/View;)V

    .line 186
    return-void
.end method


# virtual methods
.method public a(Lldh;)Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;
    .locals 0

    .prologue
    .line 749
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->M:Lldh;

    .line 750
    return-object p0
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 573
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->c()V

    .line 576
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    .line 577
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->G:Lljg;

    .line 578
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->H:Lljg;

    .line 579
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    .line 582
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 583
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    .line 584
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->L:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 586
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->C:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 587
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->D:Llip;

    .line 590
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->x:Ljava/lang/String;

    .line 591
    iput-boolean v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->y:Z

    .line 592
    iput v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->w:I

    .line 594
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->M:Lldh;

    .line 596
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->q:Ljava/lang/String;

    .line 597
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->r:Ljava/lang/String;

    .line 598
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->s:Ljava/lang/String;

    .line 599
    iput-boolean v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->B:Z

    .line 601
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->t:Ljava/lang/String;

    .line 602
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    .line 604
    iput-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->v:Ljava/lang/String;

    .line 605
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 658
    .line 659
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 658
    invoke-static {v0, p1, p2}, Llhu;->b(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 659
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->v:Ljava/lang/String;

    .line 661
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->H:Lljg;

    .line 662
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->J:Z

    .line 663
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/text/Spanned;ZZ)V
    .locals 1

    .prologue
    .line 646
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->t:Ljava/lang/String;

    .line 647
    invoke-static {p2}, Llir;->a(Landroid/text/Spanned;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    .line 648
    iput-boolean p3, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->z:Z

    .line 649
    iput-boolean p4, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->A:Z

    .line 650
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    .line 651
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->J:Z

    .line 652
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 609
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 610
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 614
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->q:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    :goto_0
    return-void

    .line 618
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->q:Ljava/lang/String;

    .line 619
    iput-object p2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->r:Ljava/lang/String;

    .line 620
    iput-object p3, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->s:Ljava/lang/String;

    .line 621
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->r:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 622
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->r:Ljava/lang/String;

    .line 623
    const-string v0, "===> Author name was null for gaia id: "

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->q:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 626
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_3

    .line 627
    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 628
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const-string v2, "comment_author_avatar_"

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->q:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setTransitionName(Ljava/lang/String;)V

    .line 630
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 631
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    :cond_3
    iput-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    .line 635
    iput-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->G:Lljg;

    .line 636
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->J:Z

    .line 638
    iput-boolean p4, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->B:Z

    goto :goto_0

    .line 623
    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 628
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 839
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->invalidate()V

    .line 840
    return-void
.end method

.method public a(ZILjava/lang/String;)V
    .locals 1

    .prologue
    .line 678
    iput-boolean p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->y:Z

    .line 679
    iput p2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->w:I

    .line 680
    iput-object p3, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->x:Ljava/lang/String;

    .line 681
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->J:Z

    .line 682
    return-void
.end method

.method public a([B)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 669
    if-eqz p1, :cond_0

    .line 670
    invoke-static {p1}, Llah;->a([B)Llah;

    move-result-object v0

    .line 671
    invoke-virtual {v0}, Llah;->c()Z

    move-result v1

    invoke-virtual {v0}, Llah;->b()I

    move-result v2

    invoke-virtual {v0}, Llah;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(ZILjava/lang/String;)V

    .line 675
    :goto_0
    return-void

    .line 673
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v1, v0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->a(ZILjava/lang/String;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 825
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b()V

    .line 828
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 833
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 835
    :cond_0
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->q:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->r:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->t:Ljava/lang/String;

    return-object v0
.end method

.method public g()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 4
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/16 v2, 0xa

    .line 538
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 539
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->r:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 540
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 542
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->z:Z

    if-eqz v1, :cond_2

    .line 543
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 544
    const v2, 0x7f0a048c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0a048d

    .line 545
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0a048e

    .line 546
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    :cond_1
    :goto_0
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 548
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    if-eqz v1, :cond_3

    .line 549
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 551
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->v:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 552
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 554
    :cond_4
    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->w:I

    if-lez v1, :cond_1

    .line 555
    const/16 v1, 0x2b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->w:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->x:Ljava/lang/String;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 723
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->y:Z

    return v0
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 528
    invoke-super {p0}, Landroid/view/ViewGroup;->invalidate()V

    .line 529
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->J:Z

    if-eqz v0, :cond_0

    .line 530
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 531
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->J:Z

    .line 533
    :cond_0
    return-void
.end method

.method public j()I
    .locals 1

    .prologue
    .line 730
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->w:I

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 737
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->z:Z

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 744
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->A:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 805
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 806
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->b()V

    .line 807
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-ne p1, v0, :cond_1

    .line 564
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhkr;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkr;

    invoke-virtual {v0, p1}, Lhkr;->a(Landroid/view/View;)V

    .line 565
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->M:Lldh;

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->q:Ljava/lang/String;

    invoke-interface {v0, v1}, Lldh;->a(Ljava/lang/String;)V

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 566
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->M:Lldh;

    if-eqz v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->M:Lldh;

    invoke-interface {v0, p0}, Lldh;->a(Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 811
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 812
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->c()V

    .line 813
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 215
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getWidth()I

    move-result v6

    .line 218
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getHeight()I

    move-result v7

    .line 220
    int-to-float v3, v6

    int-to-float v4, v7

    sget-object v5, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->f:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 222
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->z:Z

    if-eqz v0, :cond_3

    .line 223
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->k:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v2

    iget-object v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int v1, v5, v1

    iget-object v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v5, v2

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v0}, Llir;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v1}, Llir;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v2, p1}, Llir;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    sget-object v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->i:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->L:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->L:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->L:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->L:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 228
    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->o:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    float-to-int v0, v0

    .line 229
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->isPressed()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    :cond_1
    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->p:Landroid/graphics/drawable/Drawable;

    sub-int v2, v7, v0

    invoke-virtual {v1, v8, v8, v6, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 231
    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 234
    :cond_2
    sget v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->c:I

    int-to-float v1, v1

    sub-int v2, v7, v0

    int-to-float v2, v2

    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->d:I

    sub-int v3, v6, v3

    int-to-float v3, v3

    sub-int v0, v7, v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 237
    return-void

    .line 225
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->B:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->G:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->G:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->G:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->H:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->H:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->H:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v0}, Llir;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v1}, Llir;->c()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v2, p1}, Llir;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 817
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getPaddingLeft()I

    move-result v0

    sget v1, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->c:I

    add-int/2addr v0, v1

    .line 818
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getPaddingTop()I

    move-result v1

    sget v2, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->b:I

    add-int/2addr v1, v2

    .line 819
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v2

    .line 820
    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    add-int v4, v0, v2

    add-int/2addr v2, v1

    invoke-virtual {v3, v0, v1, v4, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 821
    return-void
.end method

.method protected onMeasure(II)V
    .locals 20

    .prologue
    .line 191
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->measure(II)V

    .line 194
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getPaddingLeft()I

    move-result v2

    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->c:I

    add-int v14, v2, v3

    .line 195
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getPaddingTop()I

    move-result v2

    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->b:I

    add-int v15, v2, v3

    .line 197
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getMeasuredWidth()I

    move-result v16

    .line 198
    sub-int v2, v16, v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->d:I

    sub-int v17, v2, v3

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->C:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 202
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->D:Llip;

    .line 204
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->z:Z

    if-eqz v2, :cond_0

    .line 205
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->c()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v11

    new-instance v2, Landroid/graphics/Rect;

    add-int v3, v14, v11

    add-int v4, v15, v11

    invoke-direct {v2, v14, v15, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    sget v2, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->g:I

    add-int/2addr v2, v11

    add-int v12, v14, v2

    sget-object v2, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    sget v4, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->j:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->L:Landroid/graphics/Point;

    add-int v5, v14, v17

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sub-int v2, v11, v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v15

    invoke-virtual {v4, v5, v2}, Landroid/graphics/Point;->set(II)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v4, 0xf

    invoke-static {v2, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sub-int v2, v17, v11

    sget v5, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->g:I

    sub-int/2addr v2, v5

    sub-int v5, v2, v3

    const v2, 0x7f0a048c

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v2, v4, v5, v3}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v2, Lljg;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v7, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->l:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v2, v12, v15}, Lljg;->a(II)V

    sub-int v2, v17, v11

    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->g:I

    sub-int v5, v2, v3

    add-int v2, v14, v11

    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->g:I

    add-int v11, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v2}, Lljg;->getHeight()I

    move-result v2

    add-int v12, v15, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->C:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v2, Llir;

    const v3, 0x7f0a048d

    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v6, 0x19

    invoke-static {v4, v6}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v7, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->l:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->M:Lldh;

    invoke-direct/range {v2 .. v10}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v2, v11, v12}, Llir;->a(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->C:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->b()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->K:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v3}, Llir;->getHeight()I

    move-result v3

    add-int/2addr v3, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 208
    :goto_0
    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->e:I

    add-int/2addr v2, v3

    sget-object v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->o:Landroid/graphics/Paint;

    .line 209
    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    .line 208
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->setMeasuredDimension(II)V

    .line 210
    return-void

    .line 206
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->c()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v2

    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->g:I

    add-int/2addr v2, v3

    add-int v19, v14, v2

    const/16 v2, 0xa

    move-object/from16 v0, v18

    invoke-static {v0, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->v:Ljava/lang/String;

    invoke-static {v4, v2}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v5

    new-instance v2, Lljg;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->v:Ljava/lang/String;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v7, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->l:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->H:Lljg;

    const/16 v2, 0xa

    move-object/from16 v0, v18

    invoke-static {v0, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v8

    const v2, 0x7f0a048f

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->B:Z

    if-eqz v2, :cond_4

    invoke-static {v8, v7}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v9

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->B:Z

    if-eqz v2, :cond_1

    new-instance v6, Lljg;

    sget-object v10, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v11, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->l:F

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v6 .. v13}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->G:Lljg;

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v3

    sget v13, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->g:I

    const/16 v2, 0xf

    move-object/from16 v0, v18

    invoke-static {v0, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v7

    sub-int v2, v17, v3

    sub-int/2addr v2, v13

    sub-int/2addr v2, v5

    sub-int/2addr v2, v9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->r:Ljava/lang/String;

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v5, v7, v2, v6}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v7, v6}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;)I

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v8

    new-instance v5, Lljg;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v10, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->l:F

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct/range {v5 .. v12}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    move/from16 v0, v19

    invoke-virtual {v2, v0, v15}, Lljg;->a(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v2}, Lljg;->getWidth()I

    move-result v2

    add-int v2, v2, v19

    invoke-virtual {v4}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    invoke-virtual {v7}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v4, v5

    add-int/2addr v4, v15

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->B:Z

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->G:Lljg;

    invoke-virtual {v5, v2, v4}, Lljg;->a(II)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->G:Lljg;

    invoke-virtual {v5}, Lljg;->getWidth()I

    move-result v5

    add-int/2addr v2, v5

    :cond_2
    add-int/2addr v2, v13

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->H:Lljg;

    invoke-virtual {v5, v2, v4}, Lljg;->a(II)V

    sub-int v2, v17, v3

    sub-int v11, v2, v13

    add-int v2, v14, v3

    add-int v12, v2, v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->F:Lljg;

    invoke-virtual {v2}, Lljg;->getHeight()I

    move-result v2

    add-int v13, v15, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->w:I

    if-lez v2, :cond_7

    new-instance v8, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    invoke-direct {v8, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {}, Lfo;->a()Lfo;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfo;->a(Ljava/lang/String;)Z

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->reverse()Ljava/lang/StringBuilder;

    invoke-static {}, Lfo;->a()Lfo;

    move-result-object v3

    invoke-static {v4}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lfo;->a(Ljava/lang/String;)Z

    move-result v3

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_5

    const/16 v2, 0x200f

    :goto_2
    invoke-virtual {v8, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_3
    const-string v2, "  "

    invoke-virtual {v8, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v2, "\u202d+"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->w:I

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x202c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 v2, 0xe

    move-object/from16 v0, v18

    invoke-static {v0, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->y:Z

    if-eqz v3, :cond_6

    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->n:I

    :goto_3
    invoke-direct {v4, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    invoke-interface {v3}, Landroid/text/Spanned;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v8, v2, v3, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const/16 v2, 0x21

    invoke-virtual {v8, v4, v3, v5, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v3, v8

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->C:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v2, Llir;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0x19

    invoke-static {v4, v5}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v7, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->l:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->M:Lldh;

    move v5, v11

    invoke-direct/range {v2 .. v10}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v2, v12, v13}, Llir;->a(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->C:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->I:Llir;

    invoke-virtual {v2}, Llir;->getHeight()I

    move-result v2

    add-int/2addr v2, v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->b()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->E:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v15

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/16 :goto_0

    :cond_4
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_5
    const/16 v2, 0x200e

    goto/16 :goto_2

    :cond_6
    sget v3, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->m:I

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->u:Landroid/text/Spanned;

    goto :goto_4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 755
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    .line 756
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    .line 758
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 789
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 760
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->C:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 761
    const/4 v5, 0x0

    invoke-interface {v0, v2, v3, v5}, Llip;->a(III)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 762
    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->D:Llip;

    .line 763
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->invalidate()V

    move v0, v1

    .line 764
    goto :goto_1

    .line 771
    :pswitch_2
    iput-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->D:Llip;

    .line 772
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->C:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 773
    invoke-interface {v0, v2, v3, v1}, Llip;->a(III)Z

    goto :goto_2

    .line 775
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->invalidate()V

    goto :goto_0

    .line 780
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->D:Llip;

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->D:Llip;

    const/4 v1, 0x3

    invoke-interface {v0, v2, v3, v1}, Llip;->a(III)Z

    .line 782
    iput-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->D:Llip;

    .line 783
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->invalidate()V

    goto :goto_0

    .line 758
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setPressed(Z)V
    .locals 1

    .prologue
    .line 794
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->isPressed()Z

    move-result v0

    .line 795
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setPressed(Z)V

    .line 796
    if-eq v0, p1, :cond_0

    .line 798
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;->invalidate()V

    .line 800
    :cond_0
    return-void
.end method
