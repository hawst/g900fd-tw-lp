.class public Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private a:Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

.field private b:Landroid/widget/ImageView;

.field private c:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method


# virtual methods
.method public a(Ljpv;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/16 v2, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 49
    new-instance v0, Ljpu;

    invoke-interface {p1}, Ljpv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljpu;-><init>(Ljava/lang/String;)V

    .line 50
    invoke-virtual {v0}, Ljpu;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->a:Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->setVisibility(I)V

    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->a:Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->a(Ljpv;)Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

    .line 62
    :cond_0
    :goto_0
    instance-of v0, p1, Ljtf;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 63
    check-cast v0, Ljtf;

    .line 64
    invoke-virtual {v0}, Ljtf;->i()Ljava/lang/String;

    move-result-object v0

    .line 68
    :goto_1
    invoke-interface {p1}, Ljpv;->f()Ljava/util/List;

    move-result-object v1

    .line 69
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 70
    if-ne v2, v6, :cond_2

    .line 71
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljod;

    invoke-interface {v0}, Ljod;->b()Ljava/lang/String;

    move-result-object v3

    .line 76
    :goto_2
    invoke-interface {p1}, Ljpv;->c()Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-interface {p1}, Ljpv;->h()Z

    move-result v2

    .line 78
    invoke-interface {p1}, Ljpv;->g()I

    move-result v0

    const/4 v7, 0x2

    if-ne v0, v7, :cond_3

    .line 80
    :goto_3
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->c:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Ljava/lang/String;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    .line 82
    return-void

    .line 55
    :cond_1
    invoke-virtual {v0}, Ljpu;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->a:Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->setVisibility(I)V

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020508

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 72
    :cond_2
    if-le v2, v6, :cond_4

    .line 73
    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a04ec

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_3
    move v6, v5

    .line 78
    goto :goto_3

    :cond_4
    move-object v3, v0

    goto :goto_2

    :cond_5
    move-object v0, v4

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 43
    const v0, 0x7f100174

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->a:Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;

    .line 44
    const v0, 0x7f100117

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->b:Landroid/widget/ImageView;

    .line 45
    const v0, 0x7f1001f8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/search/PersonSearchRowView;->c:Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;

    .line 46
    return-void
.end method
