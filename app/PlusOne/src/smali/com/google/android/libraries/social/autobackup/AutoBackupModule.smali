.class public Lcom/google/android/libraries/social/autobackup/AutoBackupModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lief;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    const/4 v0, 0x7

    new-array v0, v0, [Lief;

    const/4 v1, 0x0

    sget-object v2, Lhpi;->a:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lhpi;->b:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lhpi;->c:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lhpi;->d:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lhpi;->e:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lhpi;->g:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhpi;->f:Lief;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    const-class v0, Lief;

    if-ne p2, v0, :cond_1

    .line 27
    const-class v0, Lief;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/autobackup/AutoBackupModule;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/util/Collection;)V

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    const-class v0, Lheo;

    if-ne p2, v0, :cond_2

    .line 29
    const-class v1, Lheo;

    const/4 v0, 0x2

    new-array v2, v0, [Lheo;

    const/4 v3, 0x0

    const-class v0, Lhpu;

    .line 30
    invoke-virtual {p3, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lheo;

    aput-object v0, v2, v3

    const/4 v0, 0x1

    new-instance v3, Lhrs;

    invoke-direct {v3}, Lhrs;-><init>()V

    aput-object v3, v2, v0

    .line 29
    invoke-virtual {p3, v1, v2}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 32
    :cond_2
    const-class v0, Liwq;

    if-ne p2, v0, :cond_3

    .line 33
    const-class v0, Liwq;

    const-class v1, Lhpu;

    .line 34
    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .line 33
    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 35
    :cond_3
    const-class v0, Lhpu;

    if-ne p2, v0, :cond_4

    .line 36
    const-class v0, Lhpu;

    new-instance v1, Lhpu;

    invoke-direct {v1, p1}, Lhpu;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 38
    :cond_4
    const-class v0, Lhsc;

    if-ne p2, v0, :cond_5

    .line 39
    const-class v0, Lhsc;

    new-instance v1, Lhsc;

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lhsc;-><init>(Landroid/content/ContentResolver;)V

    .line 39
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 41
    :cond_5
    const-class v0, Lhqe;

    if-ne p2, v0, :cond_6

    .line 42
    const-class v0, Lhqe;

    invoke-static {p1}, Lhqe;->a(Landroid/content/Context;)Lhqe;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 43
    :cond_6
    const-class v0, Lhrj;

    if-ne p2, v0, :cond_7

    .line 44
    const-class v0, Lhrj;

    new-instance v1, Lhrj;

    invoke-direct {v1, p1}, Lhrj;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 45
    :cond_7
    const-class v0, Lhqz;

    if-ne p2, v0, :cond_8

    .line 46
    const-class v0, Lhqz;

    invoke-static {p1}, Lhqz;->a(Landroid/content/Context;)Lhqz;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 47
    :cond_8
    const-class v0, Lhrb;

    if-ne p2, v0, :cond_9

    .line 48
    const-class v0, Lhrb;

    new-instance v1, Lhrb;

    invoke-direct {v1}, Lhrb;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 49
    :cond_9
    const-class v0, Lhqo;

    if-ne p2, v0, :cond_a

    .line 50
    const-class v0, Lhqo;

    new-instance v1, Lhqp;

    invoke-direct {v1, p1}, Lhqp;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 51
    :cond_a
    const-class v0, Llow;

    if-ne p2, v0, :cond_0

    .line 52
    sget-object v0, Llow;->a:Lloy;

    goto/16 :goto_0
.end method
