.class public Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;
.super Llon;
.source "PG"

# interfaces
.implements Ljmd;


# instance fields
.field private final g:Lhee;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljmb;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:Ljme;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Llon;-><init>()V

    .line 26
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->e:Llnh;

    .line 27
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->g:Lhee;

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->i:I

    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->e:Llnh;

    const-class v1, Ljmd;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->e:Llnh;

    const-class v1, Ljmb;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->h:Ljava/util/List;

    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->e:Llnh;

    const-class v1, Ljme;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljme;

    iput-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->j:Ljme;

    .line 39
    return-void
.end method

.method public a(Ljmb;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 78
    invoke-virtual {p0}, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->f()Lae;

    move-result-object v2

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->g:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->j:Ljme;

    invoke-virtual {v0, p0, v3}, Ljme;->b(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 82
    :goto_0
    invoke-interface {p1, p0, v3, v0}, Ljmb;->a(Landroid/content/Context;IZ)I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 83
    const-string v0, "fragment_tag"

    .line 84
    invoke-interface {p1}, Ljmb;->a()Lu;

    move-result-object v1

    .line 85
    invoke-virtual {v2}, Lae;->a()Lat;

    move-result-object v2

    .line 86
    const v3, 0x7f10033c

    invoke-virtual {v2, v3, v1, v0}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    .line 87
    invoke-virtual {v2}, Lat;->b()I

    .line 91
    :goto_1
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->i()V

    goto :goto_1
.end method

.method public finish()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->g:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v5

    .line 101
    const/4 v0, -0x1

    if-eq v5, v0, :cond_2

    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v4, v2

    :goto_0
    if-ltz v3, :cond_1

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->h:Ljava/util/List;

    .line 108
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmb;

    invoke-interface {v0, p0, v5, v1}, Ljmb;->a(Landroid/content/Context;IZ)I

    move-result v0

    const/4 v6, 0x3

    if-ne v0, v6, :cond_0

    move v0, v1

    :goto_1
    or-int/2addr v4, v0

    .line 106
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 108
    goto :goto_1

    .line 110
    :cond_1
    if-nez v4, :cond_2

    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->j:Ljme;

    invoke-virtual {v0, p0, v5}, Ljme;->a(Landroid/content/Context;I)V

    .line 114
    :cond_2
    invoke-super {p0}, Llon;->finish()V

    .line 115
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->i:I

    .line 67
    iget v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->i:I

    iget-object v1, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->finish()V

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->i:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmb;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->a(Ljmb;)V

    goto :goto_0
.end method

.method public i()V
    .locals 0

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->h()V

    .line 96
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f0400ed

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->setContentView(I)V

    .line 47
    if-nez p1, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->h()V

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    const-string v0, "interstitial_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->i:I

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0, p1}, Llon;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 57
    const-string v0, "interstitial_index"

    iget v1, p0, Lcom/google/android/libraries/social/onboarding/InterstitialsActivity;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    return-void
.end method
