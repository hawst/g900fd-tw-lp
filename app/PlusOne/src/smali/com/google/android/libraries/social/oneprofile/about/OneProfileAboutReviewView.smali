.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;
.super Ljnl;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static a:I


# instance fields
.field private A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/view/LayoutInflater;

.field private D:Ljnk;

.field private o:Ljava/lang/String;

.field private p:Lpvi;

.field private q:[Lppr;

.field private r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/view/View;

.field private v:Landroid/view/View;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

.field private z:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 87
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a:I

    if-nez v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 89
    const v1, 0x7f0d012e

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a:I

    .line 76
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a:I

    if-nez v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 89
    const v1, 0x7f0d012e

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a:I

    .line 80
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a:I

    if-nez v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 89
    const v1, 0x7f0d012e

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a:I

    .line 84
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 316
    if-eqz p1, :cond_0

    .line 317
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->y:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a(I)V

    .line 318
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->y:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->setVisibility(I)V

    .line 322
    :goto_0
    return-void

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->y:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v5, -0x1000000

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 276
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->u:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->v:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 313
    :goto_0
    return-void

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->u:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->v:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 285
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->w:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->e:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->u:Landroid/view/View;

    invoke-virtual {v0, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->u:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->u:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020416

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->u:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Landroid/view/View;)V

    .line 300
    :goto_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 301
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 302
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 305
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->x:Landroid/widget/TextView;

    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 296
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 311
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lpgx;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 245
    if-eqz p1, :cond_1

    iget-object v1, p1, Lpgx;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 246
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v2, p1, Lpgx;->d:Ljava/lang/String;

    iget-object v3, p1, Lpgx;->c:Ljava/lang/String;

    .line 247
    invoke-static {v3}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 246
    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v2, p1, Lpgx;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setTag(Ljava/lang/Object;)V

    .line 249
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz p2, :cond_0

    move-object v0, p0

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    :goto_0
    if-eqz p1, :cond_2

    iget-object v0, p1, Lpgx;->b:Lppf;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lpgx;->b:Lppf;

    iget-object v0, v0, Lppf;->c:Ljava/lang/String;

    .line 256
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 257
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->s:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->s:Landroid/widget/TextView;

    iget-object v1, p1, Lpgx;->b:Lppf;

    iget-object v1, v1, Lppf;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->s:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Landroid/widget/TextView;)V

    .line 263
    :goto_1
    return-void

    .line 251
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v1, v0, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->s:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private a([Lppr;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 356
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->q:[Lppr;

    .line 357
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->q:[Lppr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->q:[Lppr;

    array-length v0, v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->z:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->setVisibility(I)V

    .line 359
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->f:I

    invoke-virtual {p0, v2, v0, v2, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->setPadding(IIII)V

    .line 360
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->z:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->q:[Lppr;

    int-to-float v2, p2

    const v3, 0x3f19999a    # 0.6f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a([Lppr;II)V

    .line 366
    :goto_0
    return-void

    .line 363
    :cond_0
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->f:I

    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->f:I

    invoke-virtual {p0, v2, v0, v2, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->setPadding(IIII)V

    .line 364
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->z:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private a([Lpxo;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->removeAllViews()V

    .line 327
    if-eqz p1, :cond_1

    array-length v0, p1

    if-eqz v0, :cond_1

    move v1, v2

    .line 328
    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_1

    .line 329
    aget-object v3, p1, v1

    .line 330
    iget-object v0, v3, Lpxo;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v3, Lpxo;->d:Ljava/lang/String;

    .line 331
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->C:Landroid/view/LayoutInflater;

    const v4, 0x7f04012f

    const/4 v5, 0x0

    .line 333
    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 334
    const v0, 0x7f100118

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 335
    iget-object v5, v3, Lpxo;->b:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    const v0, 0x7f1001e9

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 337
    iget-object v3, v3, Lpxo;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->addView(Landroid/view/View;)V

    .line 328
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 343
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getChildCount()I

    move-result v1

    const/4 v3, 0x1

    if-le v1, v3, :cond_2

    :goto_1
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->setVisibility(I)V

    .line 344
    return-void

    .line 343
    :cond_2
    const/16 v2, 0x8

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 266
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->t:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->t:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->t:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->e(Landroid/widget/TextView;)V

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->t:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b(Lnjt;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 379
    iget-object v1, p0, Lnjt;->f:Lnjg;

    iget-object v1, v1, Lnjg;->b:Lnix;

    iget-object v1, v1, Lnix;->d:Lpmm;

    iget-object v1, v1, Lpmm;->k:Lptg;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lptg;->a:Lpgg;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lpgg;->b:Ljava/lang/Integer;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    :goto_0
    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 347
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->B:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->B:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    :goto_0
    return-void

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->B:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 230
    invoke-super {p0}, Ljnl;->a()V

    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->z:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a()V

    .line 232
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->o:Ljava/lang/String;

    .line 238
    return-void
.end method

.method public a(Ljnk;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->D:Ljnk;

    .line 242
    return-void
.end method

.method public a(Lnjt;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 131
    .line 138
    invoke-static {p1}, Ljnu;->g(Lnjt;)Ljava/util/List;

    move-result-object v4

    move v1, v2

    .line 139
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 140
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpmx;

    .line 141
    iget-object v5, v0, Lpmx;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->o:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v1, v0

    .line 147
    :goto_1
    if-eqz v1, :cond_2

    .line 148
    iget-object v6, v1, Lpmx;->b:Lpgx;

    .line 149
    iget-object v5, v1, Lpmx;->e:Ljava/lang/String;

    .line 150
    iget-object v4, v1, Lpmx;->c:Ljava/lang/Integer;

    .line 151
    iget-object v0, v1, Lpmx;->g:Lpxp;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lpmx;->g:Lpxp;

    iget-object v0, v0, Lpxp;->a:[Lpxo;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lpmx;->g:Lpxp;

    iget-object v0, v0, Lpxp;->a:[Lpxo;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, v1, Lpmx;->g:Lpxp;

    iget-object v0, v0, Lpxp;->a:[Lpxo;

    .line 152
    :goto_2
    iget-object v1, v1, Lpmx;->d:Ljava/lang/String;

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    .line 155
    :goto_3
    const/4 v7, 0x1

    invoke-direct {p0, v6, v7}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Lpgx;Z)V

    .line 156
    invoke-direct {p0, v5}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->b(Ljava/lang/String;)V

    .line 157
    invoke-direct {p0, v3, v3, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Ljava/lang/Integer;)V

    .line 159
    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a([Lpxo;)V

    .line 160
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->c(Ljava/lang/String;)V

    .line 161
    invoke-direct {p0, v3, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a([Lppr;I)V

    .line 162
    return-void

    .line 139
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 151
    goto :goto_2

    :cond_2
    move-object v0, v3

    move-object v1, v3

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    goto :goto_3

    :cond_3
    move-object v1, v3

    goto :goto_1
.end method

.method public a(Lpvl;II)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 165
    .line 176
    iget-object v0, p1, Lpvl;->b:[Lpvt;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lpvl;->b:[Lpvt;

    array-length v0, v0

    if-eqz v0, :cond_9

    .line 177
    iget-object v0, p1, Lpvl;->b:[Lpvt;

    aget-object v0, v0, v9

    .line 178
    iget-object v1, v0, Lpvt;->d:[Lpvi;

    if-eqz v1, :cond_9

    .line 179
    if-ltz p2, :cond_9

    iget-object v1, v0, Lpvt;->d:[Lpvi;

    array-length v1, v1

    if-ge p2, v1, :cond_9

    .line 180
    iget-object v0, v0, Lpvt;->d:[Lpvi;

    aget-object v0, v0, p2

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->p:Lpvi;

    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->p:Lpvi;

    iget-object v5, v0, Lpvi;->e:[Lpvf;

    array-length v6, v5

    move v4, v9

    move-object v1, v3

    move-object v2, v3

    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v0, v5, v4

    .line 182
    iget v7, v0, Lpvf;->b:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 183
    sget-object v2, Lpvg;->a:Loxr;

    invoke-virtual {v0, v2}, Lpvf;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpmx;

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    .line 181
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 184
    :cond_0
    iget v7, v0, Lpvf;->b:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_8

    sget-object v7, Lpvd;->a:Loxr;

    .line 185
    invoke-virtual {v0, v7}, Lpvf;->a(Loxr;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 186
    sget-object v1, Lpvd;->a:Loxr;

    invoke-virtual {v0, v1}, Lpvf;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lply;

    iget-object v0, v0, Lply;->b:[Lppr;

    move-object v1, v2

    goto :goto_1

    :cond_1
    move-object v8, v1

    move-object v4, v2

    .line 193
    :goto_2
    if-eqz v4, :cond_7

    .line 194
    iget-object v7, p1, Lpvl;->a:Lpgx;

    .line 195
    iget-object v6, v4, Lpmx;->e:Ljava/lang/String;

    .line 197
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->p:Lpvi;

    iget-object v2, v0, Lpvi;->d:Lpmm;

    .line 198
    if-eqz v2, :cond_6

    .line 199
    iget-object v0, v2, Lpmm;->n:Lpwb;

    if-eqz v0, :cond_5

    iget-object v0, v2, Lpmm;->n:Lpwb;

    iget-object v0, v0, Lpwb;->a:Lppf;

    if-eqz v0, :cond_5

    .line 200
    iget-object v0, v2, Lpmm;->n:Lpwb;

    iget-object v0, v0, Lpwb;->a:Lppf;

    iget-object v0, v0, Lppf;->c:Ljava/lang/String;

    .line 202
    :goto_3
    iget-object v1, v2, Lpmm;->a:Lpqx;

    if-eqz v1, :cond_4

    .line 203
    iget-object v1, v2, Lpmm;->a:Lpqx;

    iget-object v1, v1, Lpqx;->i:Ljava/lang/String;

    .line 205
    :goto_4
    iget-object v5, v2, Lpmm;->b:Lpge;

    if-eqz v5, :cond_3

    .line 206
    iget-object v2, v2, Lpmm;->b:Lpge;

    iget-object v2, v2, Lpge;->a:Ljava/lang/String;

    move-object v11, v2

    move-object v2, v0

    move-object v0, v11

    .line 210
    :goto_5
    iget-object v5, v4, Lpmx;->c:Ljava/lang/Integer;

    .line 212
    iget-object v10, v4, Lpmx;->g:Lpxp;

    if-eqz v10, :cond_2

    .line 213
    iget-object v3, v4, Lpmx;->g:Lpxp;

    iget-object v3, v3, Lpxp;->a:[Lpxo;

    .line 216
    :cond_2
    iget-object v4, v4, Lpmx;->d:Ljava/lang/String;

    move-object v11, v4

    move-object v4, v1

    move-object v1, v5

    move-object v5, v2

    move-object v2, v0

    move-object v0, v3

    move-object v3, v11

    .line 219
    :goto_6
    invoke-direct {p0, v7, v9}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Lpgx;Z)V

    .line 220
    invoke-direct {p0, v6}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->b(Ljava/lang/String;)V

    .line 221
    invoke-direct {p0, v5, v2, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Ljava/lang/Integer;)V

    .line 223
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a([Lpxo;)V

    .line 224
    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->c(Ljava/lang/String;)V

    .line 225
    invoke-direct {p0, v8, p3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a([Lppr;I)V

    .line 226
    return-void

    :cond_3
    move-object v2, v0

    move-object v0, v3

    goto :goto_5

    :cond_4
    move-object v1, v3

    goto :goto_4

    :cond_5
    move-object v0, v3

    goto :goto_3

    :cond_6
    move-object v0, v3

    move-object v1, v3

    move-object v2, v3

    goto :goto_5

    :cond_7
    move-object v0, v3

    move-object v1, v3

    move-object v2, v3

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    goto :goto_6

    :cond_8
    move-object v0, v1

    move-object v1, v2

    goto/16 :goto_1

    :cond_9
    move-object v8, v3

    move-object v4, v3

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->D:Ljnk;

    if-eqz v0, :cond_1

    .line 371
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 372
    const v1, 0x7f100412

    if-eq v0, v1, :cond_0

    const v1, 0x7f100415

    if-ne v0, v1, :cond_1

    .line 373
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->D:Ljnk;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljnk;->f(Ljava/lang/String;)V

    .line 376
    :cond_1
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 98
    const v0, 0x7f100412

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 99
    const v0, 0x7f100413

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->s:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f100414

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->t:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f100415

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->u:Landroid/view/View;

    .line 102
    const v0, 0x7f100418

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->v:Landroid/view/View;

    .line 103
    const v0, 0x7f100416

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->w:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f100417

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->x:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f100419

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->y:Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;

    .line 106
    const v0, 0x7f10041c

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->z:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    .line 107
    const v0, 0x7f10041b

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a(I)V

    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->A:Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a(Z)V

    .line 112
    const v0, 0x7f10041a

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->B:Landroid/widget/TextView;

    .line 114
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->C:Landroid/view/LayoutInflater;

    .line 116
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->f:I

    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->f:I

    invoke-virtual {p0, v2, v0, v2, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->setPadding(IIII)V

    .line 117
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 121
    const v0, 0x7fffffff

    invoke-static {v0, p1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->resolveSize(II)I

    move-result v0

    .line 122
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->z:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 123
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 124
    int-to-float v0, v0

    const v2, 0x3f19999a    # 0.6f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 126
    invoke-super {p0, p1, p2}, Ljnl;->onMeasure(II)V

    .line 127
    return-void
.end method
