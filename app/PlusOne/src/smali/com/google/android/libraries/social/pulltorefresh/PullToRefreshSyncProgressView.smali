.class public Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;
.super Landroid/view/View;
.source "PG"


# static fields
.field private static final a:[I

.field private static final b:I

.field private static c:[Landroid/graphics/Paint;

.field private static d:F

.field private static e:I

.field private static f:I


# instance fields
.field private g:J

.field private h:I

.field private i:[Landroid/graphics/Rect;

.field private final j:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 20
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    .line 25
    sput-object v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->a:[I

    sput v1, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->b:I

    return-void

    .line 20
    nop

    :array_0
    .array-data 4
        -0xf062a8
        -0xbd7a0c
        -0xd5a00
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Lkcq;

    invoke-direct {v0, p0}, Lkcq;-><init>(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->j:Ljava/lang/Runnable;

    .line 71
    sget-object v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c:[Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 72
    new-array v0, v3, [Landroid/graphics/Paint;

    sput-object v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c:[Landroid/graphics/Paint;

    .line 73
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 74
    sget-object v1, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v1, v0

    .line 75
    sget-object v1, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c:[Landroid/graphics/Paint;

    aget-object v1, v1, v0

    sget-object v2, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->a:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->d:F

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d013a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->f:I

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d013b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->e:I

    .line 84
    :cond_1
    return-void
.end method

.method public static synthetic a()F
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->d:F

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)J
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->g:J

    return-wide v0
.end method

.method public static synthetic b()I
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->e:I

    return v0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->h:I

    return v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;)[Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->i:[Landroid/graphics/Rect;

    return-object v0
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 103
    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->i:[Landroid/graphics/Rect;

    if-nez v1, :cond_0

    .line 122
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->i:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    invoke-virtual {p0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->getWidth()I

    move-result v2

    .line 108
    invoke-virtual {p0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->getHeight()I

    move-result v3

    .line 111
    iget v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->h:I

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_1

    .line 112
    iget-object v4, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->i:[Landroid/graphics/Rect;

    aget-object v4, v4, v1

    sget v5, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->f:I

    sub-int v5, v2, v5

    invoke-virtual {v4, v5, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 113
    sget v4, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->f:I

    sget v5, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->e:I

    add-int/2addr v4, v5

    sub-int/2addr v2, v4

    .line 111
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 117
    :cond_1
    :goto_2
    iget v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->h:I

    if-ge v0, v1, :cond_2

    .line 118
    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->i:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    sget-object v2, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->c:[Landroid/graphics/Paint;

    rem-int/lit8 v3, v0, 0x3

    aget-object v2, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 120
    :cond_2
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->g:J

    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->j:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Liu;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->i:[Landroid/graphics/Rect;

    if-nez v0, :cond_1

    .line 92
    invoke-virtual {p0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    sget v1, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->e:I

    sget v2, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->f:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->h:I

    .line 93
    iget v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->h:I

    new-array v0, v0, [Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->i:[Landroid/graphics/Rect;

    .line 94
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->h:I

    if-ge v0, v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->i:[Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aput-object v2, v1, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;->g:J

    .line 99
    :cond_1
    return-void
.end method
