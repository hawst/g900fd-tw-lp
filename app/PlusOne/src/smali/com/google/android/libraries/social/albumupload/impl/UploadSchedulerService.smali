.class public Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static final a:J


# instance fields
.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lhlo;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lhku;

.field private d:Lhei;

.field private e:Lhlh;

.field private f:Lhlq;

.field private g:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 36
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    const-string v0, "AlbumUploadService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 57
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->b:Landroid/util/SparseArray;

    .line 231
    new-instance v0, Lhlt;

    invoke-direct {v0, p0}, Lhlt;-><init>(Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->g:Landroid/content/BroadcastReceiver;

    .line 67
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 43
    invoke-static {p0, p1}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 44
    return-void
.end method

.method private static b(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 49
    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 123
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 125
    return-void
.end method

.method protected a(IJ)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 174
    invoke-virtual {p0}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 175
    invoke-static {v1, p1}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v2

    .line 177
    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 180
    const/4 v3, 0x3

    .line 181
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v4, p2

    .line 182
    invoke-static {v1, v6, v2, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 180
    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 183
    return-void
.end method

.method protected a(J)V
    .locals 1

    .prologue
    .line 188
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_0
    return-void

    .line 190
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected a(I)Z
    .locals 10

    .prologue
    .line 137
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->c(I)Lhlo;

    move-result-object v2

    .line 138
    invoke-virtual {v2}, Lhlo;->a()J

    move-result-wide v4

    .line 139
    invoke-virtual {v2, v4, v5}, Lhlo;->b(J)Lhla;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lhla;->b()J

    move-result-wide v6

    .line 142
    const/4 v1, 0x0

    .line 143
    const-wide/16 v8, -0x1

    cmp-long v3, v4, v8

    if-eqz v3, :cond_2

    .line 144
    invoke-virtual {v2, v6, v7}, Lhlo;->a(J)Ljava/lang/String;

    move-result-object v3

    .line 145
    invoke-virtual {v0}, Lhla;->a()Ljava/lang/String;

    move-result-object v8

    .line 146
    invoke-static {p1, v3, v6, v7}, Lhkv;->a(ILjava/lang/String;J)Lhkv;

    move-result-object v6

    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->e:Lhlh;

    invoke-virtual {v0, v6}, Lhlh;->a(Lhkv;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->f:Lhlq;

    .line 152
    invoke-virtual {v0, p1, v3, v8}, Lhlq;->a(ILjava/lang/String;Ljava/lang/String;)Lhls;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lhls;->a()Z

    move-result v7

    if-nez v7, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->f:Lhlq;

    invoke-virtual {v0, p1, v3, v8}, Lhlq;->b(ILjava/lang/String;Ljava/lang/String;)Lhls;

    move-result-object v0

    .line 158
    :cond_0
    invoke-virtual {v0}, Lhls;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 159
    invoke-virtual {v0}, Lhls;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v5, v0}, Lhlo;->a(JLjava/lang/String;)V

    .line 160
    const/4 v0, 0x1

    .line 165
    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->e:Lhlh;

    invoke-virtual {v1, v6}, Lhlh;->a(Lhkv;)V

    .line 168
    :goto_1
    return v0

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->c:Lhku;

    invoke-interface {v0}, Lhku;->a()I

    move-result v0

    .line 163
    invoke-virtual {v2, v4, v5, v0}, Lhlo;->a(JI)Lhld;

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected b(I)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, -0x1

    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 203
    iget-object v2, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->d:Lhei;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "logged_in"

    aput-object v4, v3, v5

    invoke-interface {v2, v3}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 204
    if-eq p1, v1, :cond_0

    .line 206
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 207
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v5, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 210
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 211
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->c(I)Lhlo;

    move-result-object v3

    .line 212
    invoke-virtual {v3}, Lhlo;->b()Z

    move-result v3

    if-nez v3, :cond_1

    .line 216
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsa;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected c(I)Lhlo;
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->b:Landroid/util/SparseArray;

    new-instance v1, Lhlo;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lhlo;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlo;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 76
    const-class v0, Lhku;

    invoke-static {v1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhku;

    iput-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->c:Lhku;

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->c:Lhku;

    if-nez v0, :cond_0

    .line 78
    sget-object v0, Lhll;->a:Lhku;

    iput-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->c:Lhku;

    .line 80
    :cond_0
    const-class v0, Lhei;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->d:Lhei;

    .line 81
    const-class v0, Lhlh;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlh;

    iput-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->e:Lhlh;

    .line 83
    new-instance v0, Lhlq;

    invoke-direct {v0, v1}, Lhlq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->f:Lhlq;

    .line 84
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v1, -0x1

    .line 96
    if-eqz p1, :cond_2

    const-string v0, "account_id"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 99
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->b(I)I

    move-result v2

    if-eq v2, v1, :cond_1

    .line 100
    invoke-virtual {p0}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->b()Z

    move-result v3

    if-nez v3, :cond_3

    .line 101
    invoke-virtual {p0}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->a()V

    .line 119
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 96
    goto :goto_0

    .line 106
    :cond_3
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->a(I)Z

    move-result v3

    .line 109
    if-nez v3, :cond_0

    .line 110
    iget-object v3, p0, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->c:Lhku;

    invoke-interface {v3}, Lhku;->b()J

    move-result-wide v4

    .line 111
    sget-wide v6, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->a:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_4

    .line 112
    invoke-virtual {p0, v4, v5}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->a(J)V

    goto :goto_0

    .line 114
    :cond_4
    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->a(IJ)V

    goto :goto_1
.end method
