.class public final Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;
.super Landroid/app/Service;
.source "PG"

# interfaces
.implements Llnk;


# static fields
.field private static a:Lhps;

.field private static b:Llnh;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 63
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 238
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 239
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "logged_in"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 240
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 241
    const-string v3, "is_managed_account"

    invoke-interface {v1, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 242
    new-instance v3, Landroid/accounts/Account;

    const-string v4, "account_name"

    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "com.google"

    invoke-direct {v3, v1, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v3}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;Landroid/accounts/Account;)V

    goto :goto_0

    .line 246
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 209
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 210
    invoke-interface {v0, p1}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 212
    :goto_0
    invoke-static {p0}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 213
    new-instance v2, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v2, v0, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const/4 v0, 0x0

    invoke-static {v2, v1, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 215
    invoke-static {v2, v1}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 217
    invoke-static {p0}, Lhpo;->a(Landroid/content/Context;)Lhpo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lhpo;->a(I)V

    .line 218
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x5460

    const/4 v2, 0x1

    .line 249
    invoke-static {p0}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 250
    const/4 v0, 0x0

    .line 251
    invoke-static {p1, v3}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 252
    if-eqz v1, :cond_1

    .line 253
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/PeriodicSync;

    .line 254
    iget-wide v6, v0, Landroid/content/PeriodicSync;->period:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    move v1, v2

    .line 255
    goto :goto_0

    .line 257
    :cond_0
    iget-object v0, v0, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    invoke-static {p1, v3, v0}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    move v1, v0

    .line 262
    :cond_2
    if-nez v1, :cond_3

    .line 263
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 264
    const-string v1, "sync_periodic"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 265
    invoke-static {p1, v3, v0, v8, v9}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 267
    :cond_3
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 199
    invoke-static {p0}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 200
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, p1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 202
    invoke-static {p0, v1}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 203
    return-void
.end method

.method private static declared-synchronized b(Landroid/content/Context;)Lhps;
    .locals 3

    .prologue
    .line 42
    const-class v1, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a:Lhps;

    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    .line 44
    new-instance v2, Llnh;

    invoke-direct {v2, p0, v0}, Llnh;-><init>(Landroid/content/Context;Llnh;)V

    sput-object v2, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->b:Llnh;

    .line 45
    new-instance v0, Lhps;

    invoke-direct {v0, p0}, Lhps;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a:Lhps;

    .line 47
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a:Lhps;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 222
    invoke-static {p0}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 223
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, p1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 225
    return-void
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 229
    invoke-static {p0}, Lhqv;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 230
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, p1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-static {v1, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static synthetic d(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 28
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p1}, Lhei;->a(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public h_()Llnh;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->b:Llnh;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 52
    invoke-static {p0}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->b(Landroid/content/Context;)Lhps;

    move-result-object v0

    invoke-virtual {v0}, Lhps;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
