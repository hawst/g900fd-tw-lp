.class public final Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmq;
.implements Lhob;


# instance fields
.field private final e:Lhee;

.field private f:Lhoc;

.field private g:Lkbw;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Lloa;-><init>()V

    .line 45
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->x:Llnh;

    .line 46
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->e:Lhee;

    .line 156
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;)Lhee;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->e:Lhee;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;)Lkbw;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->g:Lkbw;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;)Lhoc;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->f:Lhoc;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lhmw;->K:Lhmw;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->x:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 140
    iget-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->x:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->f:Lhoc;

    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->x:Llnh;

    const-class v1, Lkbw;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkbw;

    iput-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->g:Lkbw;

    .line 142
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lhos;->a(Z)V

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->g:Lkbw;

    iget-object v1, p0, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->e:Lhee;

    .line 120
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 119
    invoke-interface {v0, p2, p0, v1}, Lkbw;->a(Lhoz;Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 121
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 122
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 126
    if-ne p1, v3, :cond_0

    if-ne p2, v2, :cond_0

    .line 127
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 128
    const-string v1, "name_violation"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 129
    invoke-virtual {p0, v2, v0}, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->setResult(ILandroid/content/Intent;)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->finish()V

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lloa;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 53
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f0401c2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->setContentView(I)V

    .line 56
    const v0, 0x7f100118

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 57
    const v1, 0x7f100139

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 58
    const v2, 0x7f10054a

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 60
    invoke-virtual {p0}, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 61
    const-string v4, "extra_title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    const-string v0, "extra_message"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    const-string v0, "extra_profile_suspension_info"

    .line 65
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [B

    .line 66
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 68
    :try_start_0
    new-instance v1, Lnkq;

    invoke-direct {v1}, Lnkq;-><init>()V

    .line 69
    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnkq;

    .line 70
    const/4 v1, 0x0

    :goto_0
    iget-object v4, v0, Lnkq;->a:[Lnkp;

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 71
    iget-object v4, v0, Lnkq;->a:[Lnkp;

    aget-object v4, v4, v1

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Loxt;->printStackTrace()V

    .line 77
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnkp;

    .line 78
    invoke-virtual {p0}, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f0401c3

    invoke-virtual {v1, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 79
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 80
    iget-object v4, v0, Lnkp;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 82
    new-instance v4, Lkcc;

    invoke-direct {v4, p0, v0}, Lkcc;-><init>(Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;Lnkp;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 114
    :cond_1
    return-void
.end method
