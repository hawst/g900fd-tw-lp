.class public Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;
.super Landroid/view/ViewGroup;
.source "PG"


# static fields
.field private static a:Z

.field private static b:Landroid/graphics/drawable/Drawable;

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:Landroid/graphics/drawable/Drawable;

.field private static e:Landroid/graphics/drawable/Drawable;

.field private static f:Landroid/graphics/drawable/Drawable;

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:I


# instance fields
.field private k:Z

.field private l:I

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Z

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/TextView;

.field private r:Landroid/widget/ProgressBar;

.field private s:Landroid/graphics/Rect;

.field private t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ljava/lang/StringBuilder;

.field private v:Z

.field private w:Z

.field private x:Landroid/graphics/Rect;

.field private y:Z

.field private z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 81
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->u:Ljava/lang/StringBuilder;

    .line 65
    iput-boolean v2, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    .line 67
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->x:Landroid/graphics/Rect;

    .line 70
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->z:Landroid/graphics/Rect;

    .line 83
    sget-boolean v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a:Z

    if-nez v0, :cond_0

    .line 84
    sput-boolean v2, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a:Z

    .line 86
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 88
    const v1, 0x7f0d0181

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->g:I

    .line 89
    const v1, 0x7f0d0182

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->h:I

    .line 90
    const v1, 0x7f0d0183

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->i:I

    .line 91
    const v1, 0x7f0d0184

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->j:I

    .line 92
    const v1, 0x7f0203df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->b:Landroid/graphics/drawable/Drawable;

    .line 93
    const v1, 0x7f0203e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->c:Landroid/graphics/drawable/Drawable;

    .line 94
    const v1, 0x7f0203da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->d:Landroid/graphics/drawable/Drawable;

    .line 95
    const v1, 0x7f0203db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->e:Landroid/graphics/drawable/Drawable;

    .line 96
    const v1, 0x7f0203dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->f:Landroid/graphics/drawable/Drawable;

    .line 98
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 99
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 100
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 101
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 102
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 110
    :cond_0
    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 111
    const-class v1, Liaw;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liaw;

    .line 112
    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-interface {v1, v0}, Liaw;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->k:Z

    .line 114
    const/4 v0, 0x0

    const/16 v1, 0x1f

    invoke-static {p1, v0, v3, v1}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->addView(Landroid/view/View;)V

    .line 121
    const/4 v0, 0x0

    const/16 v1, 0x19

    invoke-static {p1, v0, v3, v1}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->addView(Landroid/view/View;)V

    .line 130
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 131
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 386
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    if-nez v0, :cond_1

    move v6, v2

    .line 388
    :goto_0
    if-ne p2, v6, :cond_2

    .line 389
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    move-object v1, v0

    .line 410
    :goto_1
    if-ge v2, p2, :cond_5

    .line 411
    if-lez v2, :cond_0

    .line 412
    const-string v0, ", "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    :cond_0
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v6, v0

    goto :goto_0

    .line 396
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 397
    :goto_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p2, :cond_7

    .line 399
    const/4 v0, -0x1

    move v1, v2

    move v3, v0

    move v4, v2

    .line 400
    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 401
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 402
    if-lt v0, v4, :cond_3

    move v3, v1

    move v4, v0

    .line 400
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 407
    :cond_4
    invoke-interface {v5, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 417
    :cond_5
    if-ge p2, v6, :cond_6

    .line 418
    const-string v0, ",\u2026"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    :cond_6
    return-void

    :cond_7
    move-object v1, v5

    goto :goto_1
.end method


# virtual methods
.method public a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v1, 0x7f02055f

    const/16 v2, 0x1f

    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 167
    iput-boolean v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    .line 168
    sget v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->g:I

    iput v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->l:I

    .line 169
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    packed-switch p1, :pswitch_data_0

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 172
    :pswitch_0
    const v0, 0x7f0205da

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setBackgroundResource(I)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    const/16 v2, 0xf

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 174
    iput-boolean v3, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    .line 175
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->c:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    .line 176
    iget-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->k:Z

    iput-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    .line 177
    iget-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    if-eqz v0, :cond_0

    .line 178
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->f:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->n:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 184
    :pswitch_1
    const v0, 0x7f0205da

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setBackgroundResource(I)V

    .line 185
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    const/16 v2, 0xf

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 186
    iput-boolean v3, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    .line 187
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->b:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    .line 188
    iget-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->k:Z

    iput-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    .line 189
    iget-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    if-eqz v0, :cond_1

    .line 190
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->f:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->n:Landroid/graphics/drawable/Drawable;

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 199
    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setBackgroundResource(I)V

    .line 200
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 201
    iput-boolean v3, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    .line 202
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->e:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 207
    :pswitch_3
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setBackgroundResource(I)V

    .line 208
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 209
    iput-boolean v3, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    .line 210
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->e:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    .line 211
    sget v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->h:I

    iput v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->l:I

    goto :goto_0

    .line 216
    :pswitch_4
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setBackgroundResource(I)V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 218
    iput-boolean v3, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    .line 219
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->e:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 224
    :pswitch_5
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setBackgroundResource(I)V

    .line 225
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 226
    iput-boolean v3, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    .line 227
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->e:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    .line 228
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 234
    :pswitch_6
    const v0, 0x7f0205da

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setBackgroundResource(I)V

    .line 235
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0430

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/String;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    const/16 v2, 0xf

    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 237
    iput-boolean v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    goto/16 :goto_0

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_6
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->o:Z

    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->requestLayout()V

    .line 139
    :cond_0
    return-void

    .line 135
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/String;)V

    .line 245
    iput-object p1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    .line 246
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 248
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    .line 253
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->requestLayout()V

    .line 254
    return-void

    .line 250
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(I)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    if-eq v0, p1, :cond_0

    .line 143
    iput-boolean p1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    .line 144
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->requestLayout()V

    .line 146
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->w:Z

    if-eq v0, p1, :cond_2

    .line 150
    iput-boolean p1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->w:Z

    .line 151
    if-eqz p1, :cond_3

    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 155
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->addView(Landroid/view/View;)V

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 162
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->invalidate()V

    .line 164
    :cond_2
    return-void

    .line 159
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 513
    iget-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    if-eqz v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 515
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 517
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    if-eqz v0, :cond_1

    .line 518
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->n:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->z:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 519
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 521
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 522
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 424
    sub-int v1, p4, p2

    .line 425
    sub-int v6, p5, p3

    .line 426
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v1, v0

    iget-object v2, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int v2, v0, v2

    .line 427
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v6, v0

    iget-object v3, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v7, v0, v3

    .line 428
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sub-int v3, v1, v0

    .line 429
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v4, v6, v0

    .line 431
    const/4 v0, 0x0

    .line 432
    iget-object v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    .line 434
    iget-boolean v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->w:Z

    if-eqz v5, :cond_0

    .line 435
    iget-object v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v5

    .line 436
    iget-object v9, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v9}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v9

    .line 437
    sub-int/2addr v1, v5

    div-int/lit8 v1, v1, 0x2

    .line 438
    sub-int v9, v6, v9

    div-int/lit8 v9, v9, 0x2

    .line 439
    iget-object v10, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    add-int v11, v1, v5

    add-int/2addr v5, v9

    invoke-virtual {v10, v1, v9, v11, v5}, Landroid/widget/ProgressBar;->layout(IIII)V

    .line 443
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    if-eqz v1, :cond_1

    .line 444
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 445
    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 446
    iget v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->l:I

    add-int/2addr v0, v1

    .line 450
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 451
    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    sget v5, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->i:I

    add-int/2addr v1, v5

    add-int/2addr v0, v1

    .line 454
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v0

    .line 456
    const/4 v0, 0x0

    .line 457
    iget-boolean v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    if-eqz v5, :cond_3

    .line 458
    sget-object v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 459
    iget v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->l:I

    add-int/2addr v5, v0

    add-int/2addr v1, v5

    .line 462
    :cond_3
    iget-object v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v5

    .line 463
    iget-object v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-ge v2, v5, :cond_4

    .line 464
    iget-object v2, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    .line 466
    :cond_4
    iget-object v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int v9, v7, v8

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v5, v9

    .line 467
    iget-object v9, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    if-ge v5, v9, :cond_5

    .line 468
    iget-object v5, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    .line 470
    :cond_5
    add-int/2addr v1, v2

    .line 471
    if-le v1, v3, :cond_6

    move v1, v3

    .line 474
    :cond_6
    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v3

    add-int/2addr v3, v5

    .line 475
    if-le v3, v4, :cond_7

    move v3, v4

    .line 479
    :cond_7
    iget-boolean v4, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    if-eqz v4, :cond_8

    .line 480
    iget-object v4, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    .line 481
    iget-object v6, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 482
    iget-object v8, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    sub-int v9, v7, v4

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    .line 483
    iget-object v9, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->x:Landroid/graphics/Rect;

    add-int v10, v2, v6

    add-int/2addr v4, v8

    invoke-virtual {v9, v2, v8, v10, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 484
    iget v4, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->l:I

    add-int/2addr v4, v6

    add-int/2addr v2, v4

    .line 487
    :cond_8
    iget-boolean v4, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    if-eqz v4, :cond_9

    .line 488
    iget-object v4, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    .line 489
    iget-object v6, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v4

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    .line 490
    sub-int/2addr v1, v0

    .line 491
    iget-object v7, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->z:Landroid/graphics/Rect;

    add-int/2addr v0, v1

    add-int/2addr v4, v6

    invoke-virtual {v7, v1, v6, v0, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 492
    iget v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->l:I

    sub-int/2addr v1, v0

    .line 495
    :cond_9
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    .line 496
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_c

    .line 497
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 498
    sub-int/2addr v1, v0

    .line 499
    iget-object v4, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    add-int/2addr v0, v1

    invoke-virtual {v4, v1, v5, v0, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 500
    sget v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->i:I

    sub-int/2addr v1, v0

    .line 506
    :cond_a
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_b

    .line 507
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v5, v1, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 509
    :cond_b
    return-void

    .line 502
    :cond_c
    iget-object v0, p0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v5, v1, v3}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 16

    .prologue
    .line 258
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 259
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v8

    .line 260
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v9

    .line 261
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_9

    const/4 v1, 0x1

    move v7, v1

    .line 263
    :goto_0
    if-eqz v2, :cond_0

    .line 264
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 267
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    sget v3, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->j:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iput v3, v1, Landroid/graphics/Rect;->left:I

    .line 269
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 270
    if-nez v10, :cond_a

    const v1, 0x7fffffff

    .line 273
    :goto_1
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v11

    .line 274
    if-nez v11, :cond_b

    const v2, 0x7fffffff

    .line 277
    :goto_2
    const/4 v4, 0x0

    .line 278
    const/4 v3, 0x0

    .line 279
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->v:Z

    if-eqz v5, :cond_2

    .line 280
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 281
    if-eqz v7, :cond_1

    .line 282
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->l:I

    add-int/2addr v3, v4

    .line 284
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    move v15, v4

    move v4, v3

    move v3, v15

    .line 286
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    if-eqz v5, :cond_c

    sget-object v5, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->l:I

    add-int/2addr v5, v6

    move v6, v5

    .line 289
    :goto_3
    if-eqz v7, :cond_d

    sub-int v5, v1, v4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v12

    .line 292
    :goto_4
    const/high16 v12, -0x80000000

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 294
    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->o:Z

    if-eqz v13, :cond_3

    .line 295
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 296
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    const/high16 v14, -0x80000000

    invoke-static {v5, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v13, v5, v12}, Landroid/widget/TextView;->measure(II)V

    .line 298
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    .line 305
    :cond_3
    const/high16 v13, 0x40000000    # 2.0f

    if-eq v10, v13, :cond_4

    .line 306
    add-int v1, v5, v4

    add-int/2addr v1, v6

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v5

    .line 309
    :cond_4
    const/high16 v5, 0x40000000    # 2.0f

    if-eq v11, v5, :cond_5

    .line 310
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v9, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    .line 314
    :cond_5
    move/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->resolveSize(II)I

    move-result v5

    .line 315
    move/from16 v0, p2

    invoke-static {v2, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->resolveSize(II)I

    move-result v8

    .line 318
    if-eqz v7, :cond_7

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->o:Z

    if-nez v1, :cond_7

    .line 319
    sub-int v1, v5, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 320
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->y:Z

    if-eqz v2, :cond_6

    .line 321
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->l:I

    add-int/2addr v2, v6

    sub-int/2addr v1, v2

    .line 324
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->u:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    if-nez v2, :cond_e

    const/4 v2, 0x0

    .line 326
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->u:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/StringBuilder;I)V

    .line 327
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->u:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v12}, Landroid/widget/TextView;->measure(II)V

    .line 329
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    .line 330
    if-gt v3, v1, :cond_f

    .line 331
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 375
    :cond_7
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->w:Z

    if-eqz v1, :cond_8

    .line 376
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v8, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->s:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1, v1}, Landroid/widget/ProgressBar;->measure(II)V

    .line 380
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setMeasuredDimension(II)V

    .line 381
    return-void

    .line 261
    :cond_9
    const/4 v1, 0x0

    move v7, v1

    goto/16 :goto_0

    .line 271
    :cond_a
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    goto/16 :goto_1

    .line 275
    :cond_b
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    goto/16 :goto_2

    .line 286
    :cond_c
    const/4 v5, 0x0

    move v6, v5

    goto/16 :goto_3

    .line 289
    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 325
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_5

    .line 332
    :cond_f
    const/4 v3, 0x1

    if-ne v2, v3, :cond_10

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    const/high16 v3, -0x80000000

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v1, v12}, Landroid/widget/TextView;->measure(II)V

    .line 336
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6

    .line 340
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 341
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 342
    const v4, 0x7fffffff

    .line 344
    add-int/lit8 v3, v2, -0x1

    move v15, v3

    move v3, v4

    move v4, v15

    :goto_7
    if-lez v4, :cond_11

    .line 345
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->u:Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 346
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->u:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/StringBuilder;I)V

    .line 347
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->u:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v3, v7, v12}, Landroid/widget/TextView;->measure(II)V

    .line 349
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    .line 351
    sub-int v7, v2, v4

    .line 352
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    const v10, 0x7f110017

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v13, 0x0

    .line 353
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v11, v13

    .line 352
    invoke-virtual {v6, v10, v7, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v7, v9, v12}, Landroid/widget/TextView;->measure(II)V

    .line 355
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    .line 357
    sget v9, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->i:I

    add-int/2addr v3, v9

    add-int/2addr v3, v7

    .line 358
    if-le v3, v1, :cond_11

    .line 359
    add-int/lit8 v4, v4, -0x1

    goto :goto_7

    .line 363
    :cond_11
    if-le v3, v1, :cond_7

    .line 365
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->q:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 366
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    .line 367
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v4

    const v6, 0x7f0a042f

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v7, v9

    invoke-virtual {v4, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 366
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->p:Landroid/widget/TextView;

    const/high16 v3, -0x80000000

    .line 369
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 368
    invoke-virtual {v2, v1, v12}, Landroid/widget/TextView;->measure(II)V

    goto/16 :goto_6
.end method
