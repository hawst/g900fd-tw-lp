.class public Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;
.super Llol;
.source "PG"

# interfaces
.implements Lbc;
.implements Lhob;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhob;"
    }
.end annotation


# instance fields
.field private N:Lhee;

.field private O:Lhoc;

.field private P:Llfw;

.field private Q:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Llol;-><init>()V

    .line 125
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 54
    const v0, 0x7f040222

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 55
    const v0, 0x7f100605

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->Q:Landroid/widget/ListView;

    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->Q:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->P:Llfw;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 57
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    packed-switch p1, :pswitch_data_0

    .line 111
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 109
    :pswitch_0
    new-instance v0, Llfx;

    iget-object v1, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->at:Llnl;

    iget-object v2, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->N:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Llfx;-><init>(Landroid/content/Context;I)V

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->P:Llfw;

    invoke-virtual {v0, p1}, Llfw;->a(Landroid/database/Cursor;)V

    .line 118
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 49
    new-instance v0, Llfw;

    iget-object v1, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->at:Llnl;

    invoke-direct {v0, v1}, Llfw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->P:Llfw;

    .line 50
    return-void
.end method

.method public a(Ldo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->P:Llfw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Llfw;->a(Landroid/database/Cursor;)V

    .line 123
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 31
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 86
    const-string v0, "ResetTimingBreakdown"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->P:Llfw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Llfw;->a(Landroid/database/Cursor;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->P:Llfw;

    invoke-virtual {v0}, Llfw;->notifyDataSetChanged()V

    .line 90
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Llol;->aO_()V

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->O:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 64
    invoke-virtual {p0}, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 65
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->N:Lhee;

    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->O:Lhoc;

    .line 44
    return-void
.end method

.method public e(Landroid/view/MenuItem;)V
    .locals 5

    .prologue
    .line 74
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 75
    const v1, 0x7f10016a

    if-ne v0, v1, :cond_1

    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->O:Lhoc;

    new-instance v1, Llfy;

    iget-object v2, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->at:Llnl;

    iget-object v3, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->N:Lhee;

    .line 77
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v1, v2, v3}, Llfy;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    const v1, 0x7f1004a2

    if-ne v0, v1, :cond_0

    .line 79
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SENDTO"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.SUBJECT"

    const-string v3, "Timing Breakdown Metrics for :"

    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->N:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v4, "account_name"

    invoke-interface {v0, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->P:Llfw;

    invoke-virtual {v2}, Llfw;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "text/plain"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public z()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Llol;->z()V

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->O:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->b(Lhob;)Lhoc;

    .line 71
    return-void
.end method
