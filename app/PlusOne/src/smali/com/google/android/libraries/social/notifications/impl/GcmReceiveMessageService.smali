.class public Lcom/google/android/libraries/social/notifications/impl/GcmReceiveMessageService;
.super Landroid/app/IntentService;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "GcmReceiveMessageService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method private static a([B)Llvx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 117
    if-nez p0, :cond_0

    move-object v0, v1

    .line 126
    :goto_0
    return-object v0

    .line 122
    :cond_0
    :try_start_0
    new-instance v0, Llvx;

    invoke-direct {v0}, Llvx;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Llvx;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    const-string v2, "GcmReceiveMsgService"

    const-string v3, "Failed to parse payload to it\'s proto form."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 126
    goto :goto_0
.end method

.method private a(Llvx;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiveMessageService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p1, Llvx;->a:Ljava/lang/String;

    iget-object v3, p1, Llvx;->b:[Llub;

    invoke-static {v0, p2, v2, v3, v7}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Llub;Z)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiveMessageService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p1, Llvx;->a:Ljava/lang/String;

    iget-object v5, p1, Llvx;->b:[Llub;

    new-array v6, v7, [I

    aput v7, v6, v1

    .line 65
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    :goto_0
    array-length v2, v5

    if-ge v0, v2, :cond_2

    move v2, v1

    :goto_1
    if-gtz v2, :cond_0

    aget-object v8, v5, v0

    iget v8, v8, Llub;->d:I

    aget v9, v6, v2

    if-ne v8, v9, :cond_1

    aget-object v2, v5, v0

    iget-object v2, v2, Llub;->b:Ljava/lang/String;

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 63
    invoke-static {v3, p2, v4, v0}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)[Ljiu;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiveMessageService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Ljis;

    invoke-static {v2, v3}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 70
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 71
    new-instance v4, Ljiq;

    sget-object v5, Ljir;->a:Ljir;

    invoke-direct {v4}, Ljiq;-><init>()V

    .line 73
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 74
    :goto_2
    if-ge v1, v3, :cond_3

    .line 75
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 74
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 77
    :cond_3
    return-void
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 95
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    const-string v1, "GcmReceiveMsgService"

    const-string v2, "Failed to parse payload string into bytes."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 98
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiveMessageService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 105
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "logged_in"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 106
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 107
    const-string v3, "effective_gaia_id"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 108
    const-string v0, "account_name"

    invoke-interface {v1, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    :goto_0
    return-object v0

    .line 111
    :cond_1
    const-string v0, "GcmReceiveMsgService"

    const-string v1, "Invalid recipientOid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 41
    :try_start_0
    const-string v0, "ht"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    const-string v0, "GcmReceiveMsgService"

    const-string v1, "Intent did not contain the payload key."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiver;->a(Landroid/content/Intent;)Z

    .line 55
    return-void

    .line 44
    :cond_1
    :try_start_1
    const-string v0, "ht"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiveMessageService;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiveMessageService;->a([B)Llvx;

    move-result-object v0

    const-string v1, "GcmReceiveMsgService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Received payload: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    if-eqz v0, :cond_4

    iget-object v1, v0, Llvx;->a:Ljava/lang/String;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_3

    const-string v0, "GcmReceiveMsgService"

    const-string v1, "Intent did not contain a valid payload."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    .line 45
    :cond_3
    if-eqz v0, :cond_0

    .line 47
    iget-object v1, v0, Llvx;->a:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiveMessageService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 49
    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiveMessageService;->a(Llvx;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 54
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GcmReceiver;->a(Landroid/content/Intent;)Z

    throw v0

    .line 44
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
