.class public Lcom/google/android/libraries/social/avatars/AvatarResource;
.super Lcom/google/android/libraries/social/resources/images/ImageResource;
.source "PG"


# instance fields
.field private mDownloadUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lkdv;Lhsr;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/resources/images/ImageResource;-><init>(Lkdv;Lkds;)V

    .line 120
    return-void
.end method

.method private getAvatarUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mId:Lkdc;

    check-cast v0, Lhsr;

    iget-object v0, v0, Lhsr;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected decodeBitmap(Ljava/nio/ByteBuffer;Z)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 166
    const/4 v0, 0x0

    invoke-super {p0, p1, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->decodeBitmap(Ljava/nio/ByteBuffer;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 168
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mId:Lkdc;

    check-cast v1, Lhsr;

    iget v1, v1, Lhsr;->b:I

    if-eqz v1, :cond_2

    .line 169
    iget-object v2, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mManager:Lkdv;

    .line 170
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 171
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 170
    invoke-interface {v2, v1, v3}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 173
    iget-object v1, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mManager:Lkdv;

    invoke-interface {v1}, Lkdv;->a()Landroid/content/Context;

    move-result-object v4

    .line 174
    iget-object v1, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mId:Lkdc;

    check-cast v1, Lhsr;

    iget v1, v1, Lhsr;->b:I

    const/4 v5, 0x1

    if-ne v1, v5, :cond_3

    .line 175
    iget-object v1, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mId:Lkdc;

    check-cast v1, Lhsr;

    iget v1, v1, Lhsr;->a:I

    invoke-static {v4, v1}, Lhss;->a(Landroid/content/Context;I)I

    move-result v1

    .line 177
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-lt v3, v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ge v3, v1, :cond_1

    .line 178
    :cond_0
    invoke-static {v0, v1, v1, v6}, Llrw;->a(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 179
    invoke-interface {v2, v0}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    move-object v0, v1

    .line 182
    :cond_1
    invoke-static {v4, v0, v6}, Llho;->a(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    .line 188
    :goto_0
    invoke-interface {v2, v1}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 191
    :cond_2
    return-object v0

    .line 184
    :cond_3
    invoke-static {v4}, Lhss;->i(Landroid/content/Context;)F

    move-result v1

    .line 185
    iget-object v4, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mManager:Lkdv;

    .line 186
    invoke-interface {v4}, Lkdv;->a()Landroid/content/Context;

    invoke-static {v0, v1, v3}, Llho;->a(Landroid/graphics/Bitmap;FLandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_0
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mDownloadUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mId:Lkdc;

    check-cast v0, Lhsr;

    .line 157
    iget-object v1, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mManager:Lkdv;

    invoke-interface {v1}, Lkdv;->a()Landroid/content/Context;

    move-result-object v1

    iget v0, v0, Lhsr;->a:I

    invoke-static {v1, v0}, Lhss;->a(Landroid/content/Context;I)I

    move-result v0

    .line 158
    invoke-direct {p0}, Lcom/google/android/libraries/social/avatars/AvatarResource;->getAvatarUrl()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3c

    invoke-static {v1, v2, v0, v0}, Ljbd;->a(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mDownloadUrl:Ljava/lang/String;

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mDownloadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getShortFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mId:Lkdc;

    check-cast v0, Lhsr;

    .line 126
    invoke-direct {p0}, Lcom/google/android/libraries/social/avatars/AvatarResource;->getAvatarUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Llsf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    iget-object v2, p0, Lcom/google/android/libraries/social/avatars/AvatarResource;->mManager:Lkdv;

    invoke-interface {v2}, Lkdv;->a()Landroid/content/Context;

    move-result-object v2

    iget v3, v0, Lhsr;->a:I

    invoke-static {v2, v3}, Lhss;->a(Landroid/content/Context;I)I

    move-result v2

    .line 128
    const-string v3, "-a"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {v0}, Lhsr;->aJ_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    const-string v0, "-o"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public load()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/google/android/libraries/social/avatars/AvatarResource;->getAvatarUrl()Ljava/lang/String;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_0

    .line 139
    invoke-super {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->load()V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/avatars/AvatarResource;->deliverDownloadError(I)V

    goto :goto_0
.end method
