.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;
.super Ljnl;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static a:Landroid/content/res/ColorStateList;


# instance fields
.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:Ljmy;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->s:Z

    .line 60
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a:Landroid/content/res/ColorStateList;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->s:Z

    .line 60
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a:Landroid/content/res/ColorStateList;

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->s:Z

    .line 60
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a:Landroid/content/res/ColorStateList;

    .line 57
    return-void
.end method

.method public static b(Lnjt;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 150
    invoke-static {p0}, Ljnu;->c(Lnjt;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 170
    :cond_0
    :goto_0
    return v0

    .line 152
    :cond_1
    invoke-static {p0}, Ljnu;->b(Lnjt;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 155
    invoke-static {p0}, Ljnu;->j(Lnjt;)Lppf;

    move-result-object v2

    .line 156
    if-eqz v2, :cond_2

    iget-object v2, v2, Lppf;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 159
    :cond_2
    invoke-static {p0}, Ljnu;->l(Lnjt;)[Lpgq;

    move-result-object v2

    .line 160
    if-eqz v2, :cond_3

    array-length v3, v2

    if-eqz v3, :cond_3

    .line 161
    aget-object v2, v2, v1

    .line 162
    iget-object v3, v2, Lpgq;->b:Lpgr;

    if-eqz v3, :cond_3

    iget-object v3, v2, Lpgq;->b:Lpgr;

    iget-object v3, v3, Lpgr;->b:Lppf;

    if-eqz v3, :cond_3

    iget-object v2, v2, Lpgq;->b:Lpgr;

    iget-object v2, v2, Lpgr;->b:Lppf;

    iget-object v2, v2, Lppf;->b:Ljava/lang/String;

    .line 164
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 170
    goto :goto_0
.end method


# virtual methods
.method public a(Ljmy;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->t:Ljmy;

    .line 147
    return-void
.end method

.method public a(Lnjt;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 65
    invoke-static {p1}, Ljnu;->c(Lnjt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->o:Ljava/lang/String;

    .line 66
    invoke-static {p1}, Ljnu;->b(Lnjt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->p:Ljava/lang/String;

    .line 67
    invoke-static {p1}, Ljnu;->j(Lnjt;)Lppf;

    move-result-object v4

    .line 68
    if-eqz v4, :cond_1

    iget-object v0, v4, Lppf;->b:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->q:Ljava/lang/String;

    .line 70
    invoke-static {p1}, Ljnu;->l(Lnjt;)[Lpgq;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_2

    array-length v2, v0

    if-eqz v2, :cond_2

    aget-object v0, v0, v7

    move-object v3, v0

    .line 72
    :goto_1
    if-eqz v3, :cond_3

    iget-object v0, v3, Lpgq;->b:Lpgr;

    if-eqz v0, :cond_3

    iget-object v0, v3, Lpgq;->b:Lpgr;

    iget-object v0, v0, Lpgr;->b:Lppf;

    if-eqz v0, :cond_3

    iget-object v0, v3, Lpgq;->b:Lpgr;

    iget-object v0, v0, Lpgr;->b:Lppf;

    iget-object v0, v0, Lppf;->b:Ljava/lang/String;

    :goto_2
    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->r:Ljava/lang/String;

    .line 75
    const v0, 0x7f1003f7

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;

    .line 76
    invoke-static {p1}, Ljnu;->a(Lnjt;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    move-object v2, v1

    .line 77
    :goto_3
    iget-boolean v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->s:Z

    if-eqz v5, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 78
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Ljac;->a:Ljac;

    invoke-static {v5, v2, v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v5}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->a(Lizu;Z)V

    .line 79
    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->setVisibility(I)V

    .line 84
    :goto_4
    const v0, 0x7f1003f8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 85
    const v0, 0x7f1003fa

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->o:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 87
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 88
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    sget-object v2, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 98
    :goto_5
    const v0, 0x7f1003fb

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 99
    const v0, 0x7f1003fd

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 100
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->p:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 101
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 102
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    sget-object v2, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 112
    :goto_6
    const v0, 0x7f1003fe

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 113
    const v0, 0x7f100400

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 114
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->q:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 115
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 116
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v2, v4, Lppf;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    sget-object v2, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 126
    :goto_7
    const v0, 0x7f100401

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 127
    const v0, 0x7f100403

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 128
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->r:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 129
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 130
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v1, v3, Lpgq;->b:Lpgr;

    iget-object v1, v1, Lpgr;->b:Lppf;

    iget-object v1, v1, Lppf;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 133
    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 139
    :goto_8
    return-void

    :cond_1
    move-object v0, v1

    .line 68
    goto/16 :goto_0

    :cond_2
    move-object v3, v1

    .line 71
    goto/16 :goto_1

    :cond_3
    move-object v0, v1

    .line 72
    goto/16 :goto_2

    .line 76
    :pswitch_0
    iget-object v2, p1, Lnjt;->f:Lnjg;

    iget-object v2, v2, Lnjg;->b:Lnix;

    iget-object v2, v2, Lnix;->d:Lpmm;

    iget-object v2, v2, Lpmm;->l:Lpui;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lnjt;->f:Lnjg;

    iget-object v2, v2, Lnjg;->b:Lnix;

    iget-object v2, v2, Lnix;->d:Lpmm;

    iget-object v2, v2, Lpmm;->l:Lpui;

    iget-object v2, v2, Lpui;->a:Lpuj;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lnjt;->f:Lnjg;

    iget-object v2, v2, Lnjg;->b:Lnix;

    iget-object v2, v2, Lnix;->d:Lpmm;

    iget-object v2, v2, Lpmm;->l:Lpui;

    iget-object v2, v2, Lpui;->a:Lpuj;

    iget-object v2, v2, Lpuj;->b:Ljava/lang/String;

    goto/16 :goto_3

    .line 81
    :cond_4
    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/oneprofile/about/PlacesMapView;->setVisibility(I)V

    goto/16 :goto_4

    .line 93
    :cond_5
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 94
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 107
    :cond_6
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 108
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 121
    :cond_7
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 122
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 135
    :cond_8
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 136
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    .line 76
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->s:Z

    .line 143
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 176
    const v1, 0x7f1003f8

    if-ne v0, v1, :cond_1

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->t:Ljmy;

    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->o:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljmy;->c(Ljava/lang/String;)V

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    const v1, 0x7f1003fb

    if-ne v0, v1, :cond_2

    .line 179
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->t:Ljmy;

    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->p:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljmy;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :cond_2
    const v1, 0x7f1003fe

    if-ne v0, v1, :cond_3

    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->t:Ljmy;

    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->q:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljmy;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :cond_3
    const v1, 0x7f100401

    if-ne v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->t:Ljmy;

    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->r:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljmy;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
