.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;
.super Ljnl;
.source "PG"


# instance fields
.field private a:Landroid/view/ViewGroup;

.field private o:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public static b(Lnjt;)Z
    .locals 1

    .prologue
    .line 157
    if-eqz p0, :cond_0

    .line 158
    iget-object v0, p0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->g:Lnim;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->g:Lnim;

    iget-object v0, v0, Lnim;->b:[Lnil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->g:Lnim;

    iget-object v0, v0, Lnim;->b:[Lnil;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 161
    const/4 v0, 0x1

    .line 165
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lnjt;)V
    .locals 3

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 144
    if-eqz p1, :cond_0

    .line 145
    iget-object v1, p1, Lnjt;->e:Lnkd;

    .line 146
    if-eqz v1, :cond_0

    .line 147
    iget-object v2, v1, Lnkd;->g:Lnim;

    if-eqz v2, :cond_0

    .line 148
    iget-object v0, v1, Lnkd;->g:Lnim;

    iget-object v0, v0, Lnim;->b:[Lnil;

    .line 153
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->a([Lnil;)V

    .line 154
    return-void
.end method

.method public a([Lnil;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/16 v12, 0x8

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 54
    if-eqz p1, :cond_c

    array-length v0, p1

    if-lez v0, :cond_c

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 57
    array-length v7, p1

    move v5, v4

    move v0, v6

    :goto_0
    if-ge v5, v7, :cond_b

    aget-object v8, p1, v5

    .line 58
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->getContext()Landroid/content/Context;

    move-result-object v9

    new-instance v10, Landroid/widget/LinearLayout;

    invoke-direct {v10, v9}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {v10, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v2, v8, Lnil;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v3, v8, Lnil;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v2, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->a(Landroid/widget/TextView;Z)V

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v3, v8, Lnil;->d:Lnih;

    if-eqz v3, :cond_10

    iget-object v0, v3, Lnih;->a:Lnhz;

    if-eqz v0, :cond_f

    iget-object v0, v3, Lnih;->a:Lnhz;

    iget-object v0, v0, Lnhz;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    iget-object v0, v3, Lnih;->a:Lnhz;

    iget-object v0, v0, Lnhz;->a:Ljava/lang/Integer;

    :goto_1
    iget-object v2, v3, Lnih;->b:Lnhz;

    if-eqz v2, :cond_e

    iget-object v2, v3, Lnih;->b:Lnhz;

    iget-object v2, v2, Lnhz;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_e

    iget-object v2, v3, Lnih;->b:Lnhz;

    iget-object v2, v2, Lnhz;->a:Ljava/lang/Integer;

    :goto_2
    iget-object v11, v3, Lnih;->c:Ljava/lang/Boolean;

    if-eqz v11, :cond_d

    iget-object v3, v3, Lnih;->c:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move v13, v3

    move-object v3, v0

    move v0, v13

    :goto_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v3, :cond_1

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz v0, :cond_9

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    const-string v0, " - "

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->i:Ljava/lang/String;

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v8, Lnil;->c:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, v8, Lnil;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_6

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_5

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_7

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->c(Landroid/widget/TextView;)V

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_7
    iget-object v0, v8, Lnil;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v2, v8, Lnil;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->d(Landroid/widget/TextView;)V

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 57
    :cond_8
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v4

    goto/16 :goto_0

    .line 58
    :cond_9
    if-eqz v2, :cond_3

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_a

    const-string v0, " - "

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 61
    :cond_b
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    :goto_5
    return-void

    .line 63
    :cond_c
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v12}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    :cond_d
    move-object v3, v0

    move v0, v4

    goto/16 :goto_3

    :cond_e
    move-object v2, v1

    goto/16 :goto_2

    :cond_f
    move-object v0, v1

    goto/16 :goto_1

    :cond_10
    move v0, v4

    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_3
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x7

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 42
    const v0, 0x7f1003e6

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->a:Landroid/view/ViewGroup;

    .line 43
    const v0, 0x7f1003e5

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->o:Landroid/widget/TextView;

    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->o:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->e(Landroid/widget/TextView;)V

    .line 45
    return-void
.end method
