.class public Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownActivity;
.super Lloa;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Lloa;-><init>()V

    .line 17
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownActivity;->x:Llnh;

    invoke-virtual {v0, v1}, Llln;->a(Llnh;)Llln;

    .line 18
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownActivity;->x:Llnh;

    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 19
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f040221

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownActivity;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownActivity;->h()Loo;

    move-result-object v0

    .line 44
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Loo;->c(Z)V

    .line 45
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120017

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 24
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 29
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 30
    const v1, 0x7f10016a

    if-eq v0, v1, :cond_0

    const v1, 0x7f1004a2

    if-ne v0, v1, :cond_1

    .line 31
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownActivity;->f()Lae;

    move-result-object v0

    const v1, 0x7f100604

    invoke-virtual {v0, v1}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;

    .line 32
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/timingbreakdown/ui/TimingBreakdownFragment;->e(Landroid/view/MenuItem;)V

    .line 33
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lloa;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
