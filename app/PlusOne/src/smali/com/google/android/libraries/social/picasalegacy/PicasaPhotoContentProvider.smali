.class public Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;
.super Landroid/content/ContentProvider;
.source "PG"


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private b:Ljava/lang/String;

.field private final c:Landroid/content/UriMatcher;

.field private d:Ljwh;

.field private e:I

.field private f:Z

.field private g:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    const-string v0, "content://media/external/fs_id"

    .line 87
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->a:Landroid/net/Uri;

    .line 86
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 92
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->f:Z

    return-void
.end method

.method private static a(Landroid/content/Context;)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 150
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 151
    sget-object v1, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->a:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 153
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 160
    invoke-static {v1}, Lifu;->a(Landroid/database/Cursor;)V

    :goto_0
    return v0

    .line 156
    :cond_0
    invoke-static {v1}, Lifu;->a(Landroid/database/Cursor;)V

    const/4 v0, -0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lifu;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method private a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 239
    const-string v0, "1"

    const-string v3, "force_recalculate"

    invoke-virtual {p1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 240
    if-nez v3, :cond_0

    const-string v0, "1"

    const-string v4, "cache_only"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 241
    :goto_0
    new-instance v4, Ljwq;

    invoke-direct {v4, p2}, Ljwq;-><init>([Ljava/lang/String;)V

    .line 242
    iget-object v5, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->d:Ljwh;

    .line 243
    array-length v6, p2

    new-array v6, v6, [Ljava/lang/Object;

    .line 244
    if-eqz v0, :cond_2

    .line 245
    array-length v3, p2

    :goto_1
    if-ge v1, v3, :cond_4

    .line 246
    aget-object v0, p2, v1

    invoke-virtual {v5, v0, v2}, Ljwh;->a(Ljava/lang/String;Ljava/lang/String;)Lifq;

    move-result-object v0

    .line 248
    if-nez v0, :cond_1

    move-object v0, v2

    :goto_2
    aput-object v0, v6, v1

    .line 245
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 240
    goto :goto_0

    .line 248
    :cond_1
    invoke-virtual {v0}, Lifq;->a()[B

    move-result-object v0

    goto :goto_2

    .line 251
    :cond_2
    array-length v7, p2

    :goto_3
    if-ge v1, v7, :cond_4

    .line 252
    aget-object v0, p2, v1

    invoke-virtual {v5, v0, v2, v3}, Ljwh;->a(Ljava/lang/String;Ljava/lang/String;Z)Lifq;

    move-result-object v0

    .line 254
    if-nez v0, :cond_3

    move-object v0, v2

    :goto_4
    aput-object v0, v6, v1

    .line 251
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 254
    :cond_3
    invoke-virtual {v0}, Lifq;->a()[B

    move-result-object v0

    goto :goto_4

    .line 257
    :cond_4
    invoke-virtual {v4, v6}, Ljwq;->a([Ljava/lang/Object;)V

    .line 258
    return-object v4
.end method

.method private declared-synchronized a()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 165
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounted"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "mounted_ro"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 188
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 165
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 169
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 170
    invoke-static {v0}, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->a(Landroid/content/Context;)I

    move-result v1

    .line 171
    iget-boolean v2, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->f:Z

    if-nez v2, :cond_4

    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x1f

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "set fsid first time:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->f:Z

    .line 175
    iput v1, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->e:I

    .line 176
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "external_storage_fsid"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 177
    :cond_4
    :try_start_2
    iget v2, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->e:I

    if-eq v2, v1, :cond_1

    .line 179
    iget v2, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->e:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x28

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "fsid changed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 180
    iput v1, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->e:I

    .line 181
    iget-object v2, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->g:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "external_storage_fsid"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 184
    invoke-static {v0}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->d:Ljwh;

    invoke-virtual {v0}, Ljwh;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->a()V

    return-void
.end method


# virtual methods
.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .locals 4

    .prologue
    .line 192
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    .line 193
    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->b:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->b:Ljava/lang/String;

    const-string v2, "photos"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 195
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->b:Ljava/lang/String;

    const-string v2, "fingerprint"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->b:Ljava/lang/String;

    const-string v2, "photos/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->b:Ljava/lang/String;

    const-string v2, "albumcovers/#"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 198
    return-void
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 302
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unsupported uri:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->d:Ljwh;

    invoke-virtual {v0, p3}, Ljwh;->a([Ljava/lang/String;)I

    move-result v0

    return v0

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 211
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :pswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.picasasync.item"

    .line 209
    :goto_0
    return-object v0

    .line 207
    :pswitch_2
    const-string v0, "vnd.android.cursor.item/vnd.google.android.picasasync.item"

    goto :goto_0

    .line 209
    :pswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.google.android.picasasync.album_cover"

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 285
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x7

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "INSERT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljwn;->a(Ljava/lang/String;)I

    move-result v1

    .line 287
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    .line 289
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "unsupported uri:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    :catchall_0
    move-exception v0

    invoke-static {v1}, Ljwn;->a(I)V

    throw v0
.end method

.method public onCreate()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 103
    invoke-virtual {p0}, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 107
    invoke-static {v0}, Ljvv;->a(Landroid/content/Context;)V

    .line 109
    invoke-static {v0}, Ljwh;->a(Landroid/content/Context;)Ljwh;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->d:Ljwh;

    .line 110
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->g:Landroid/content/SharedPreferences;

    .line 112
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "picasa-photo-provider"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 114
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 115
    new-instance v2, Ljws;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Ljws;-><init>(Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;Landroid/os/Looper;)V

    .line 117
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    new-instance v4, Ljwr;

    invoke-direct {v4, p0, v2}, Ljwr;-><init>(Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;Landroid/os/Handler;)V

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 124
    invoke-static {v2, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 125
    return v5
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 5

    .prologue
    .line 263
    const-string v1, "OPEN "

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljwn;->a(Ljava/lang/String;)I

    move-result v1

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 276
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "unsupported uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    :catchall_0
    move-exception v0

    invoke-static {v1}, Ljwn;->a(I)V

    throw v0

    .line 263
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 267
    :pswitch_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->e()Ljwt;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljwt;->b(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 279
    invoke-static {v1}, Ljwn;->a(I)V

    :goto_1
    return-object v0

    .line 272
    :pswitch_2
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;

    move-result-object v0

    .line 273
    invoke-virtual {v0}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->e()Ljwt;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljwt;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 279
    invoke-static {v1}, Ljwn;->a(I)V

    goto :goto_1

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 218
    const-string v1, "QUERY "

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljwn;->a(Ljava/lang/String;)I

    move-result v1

    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :catchall_0
    move-exception v0

    invoke-static {v1}, Ljwn;->a(I)V

    throw v0

    .line 218
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 220
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 221
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljwn;->b(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224
    invoke-static {v1}, Ljwn;->a(I)V

    return-object v0

    .line 220
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/libraries/social/picasalegacy/PicasaPhotoContentProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    .line 314
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unsupported uri:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
