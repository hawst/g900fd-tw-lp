.class public Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;
.super Llol;
.source "PG"

# interfaces
.implements Lbc;
.implements Ljby;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ljby;",
        "Llgs;"
    }
.end annotation


# static fields
.field private static final N:[Ljava/lang/String;


# instance fields
.field private O:Landroid/net/Uri;

.field private P:I

.field private Q:Ljbt;

.field private R:Ljce;

.field private S:Ljbx;

.field private T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

.field private U:Ljbp;

.field private V:Z

.field private W:I

.field private final X:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 90
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "date_added"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->N:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Llol;-><init>()V

    .line 98
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->P:I

    .line 523
    new-instance v0, Ljcd;

    invoke-direct {v0, p0}, Ljcd;-><init>(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->X:Landroid/view/View$OnClickListener;

    .line 537
    return-void
.end method

.method private V()Ljava/lang/String;
    .locals 3

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 398
    const-string v1, "file:/"

    .line 399
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 400
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 402
    :cond_0
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;I)I
    .locals 0

    .prologue
    .line 66
    iput p1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->P:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Ljbp;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->U:Ljbp;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;Ljbp;)Ljbp;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->U:Ljbp;

    return-object p1
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 361
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 380
    :goto_0
    return-void

    .line 370
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 371
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-le v0, v2, :cond_1

    .line 372
    invoke-virtual {p2}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 373
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p2}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 374
    invoke-virtual {p2}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Ljava/util/ArrayList;Landroid/net/Uri;)V

    .line 373
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 378
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Ljava/util/ArrayList;Landroid/net/Uri;)V

    .line 379
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->S:Ljbx;

    invoke-virtual {v0, v1, p0}, Ljbx;->a(Ljava/util/List;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;Ljac;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Ljac;)V

    return-void
.end method

.method private a(Ljac;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 452
    new-instance v2, Landroid/content/Intent;

    sget-object v0, Ljac;->a:Ljac;

    if-ne p1, v0, :cond_1

    const-string v0, "android.media.action.IMAGE_CAPTURE"

    :goto_0
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 459
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd_HHmmss"

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljac;->a:Ljac;

    if-ne p1, v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x8

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "IMG_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".jpg"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v3, Ljava/io/File;

    sget-object v4, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    .line 460
    const-string v0, "output"

    iget-object v3, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 462
    sget-object v0, Ljac;->b:Ljac;

    if-ne p1, v0, :cond_0

    .line 463
    const-string v0, "android.intent.extra.videoQuality"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 466
    :cond_0
    sget-object v0, Ljac;->a:Ljac;

    if-ne p1, v0, :cond_4

    move v0, v1

    .line 469
    :goto_3
    :try_start_0
    invoke-virtual {p0, v2, v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    :goto_4
    return-void

    .line 452
    :cond_1
    const-string v0, "android.media.action.VIDEO_CAPTURE"

    goto :goto_0

    .line 459
    :cond_2
    sget-object v3, Ljac;->b:Ljac;

    if-ne p1, v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x8

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "VID_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".mp4"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 466
    :cond_4
    const/4 v0, 0x2

    goto :goto_3

    .line 471
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->at:Llnl;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a03ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 472
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4
.end method

.method private a(Ljava/util/ArrayList;Landroid/net/Uri;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lizr;",
            ">;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 349
    if-nez p2, :cond_1

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->at:Llnl;

    const/4 v1, 0x0

    invoke-static {v0, p2, v1}, Ljbv;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Ljac;

    move-result-object v0

    .line 353
    if-eqz v0, :cond_0

    .line 356
    new-instance v1, Ljbv;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->at:Llnl;

    invoke-direct {v1, v2, p2, v0}, Ljbv;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljac;)V

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(J)[Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 289
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v3, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    .line 290
    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Ljbx;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->S:Ljbx;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Llnl;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->at:Llnl;

    return-object v0
.end method

.method private c(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 383
    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 393
    :goto_0
    return v0

    .line 387
    :cond_0
    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    .line 389
    new-instance v1, Ljava/io/File;

    invoke-direct {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->V()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 390
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    goto :goto_0

    .line 393
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Ljbt;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->Q:Ljbt;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Llnl;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->at:Llnl;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->W:I

    return v0
.end method

.method public static synthetic g(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)V
    .locals 4

    .prologue
    .line 66
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a03ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a03ac

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Llgr;->a(Ljava/lang/String;[Ljava/lang/String;)Llgr;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->p()Lae;

    move-result-object v1

    const-string v2, "PhotoOrVideo"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 201
    invoke-super {p0}, Llol;->A()V

    .line 202
    return-void
.end method

.method public U()V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->U:Ljbp;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->U:Ljbp;

    invoke-virtual {v0}, Ljbp;->a()V

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->U:Ljbp;

    .line 234
    :cond_0
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 140
    const v0, 0x7f040109

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 141
    const v0, 0x7f10036f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    .line 142
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    const v1, 0x7f020416

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->setSelector(I)V

    .line 144
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->k()Landroid/os/Bundle;

    move-result-object v3

    .line 145
    if-eqz v3, :cond_1

    .line 146
    const-string v0, "header_text"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    const v0, 0x7f10036e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 149
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    const v0, 0x7f10036c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 152
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 153
    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->X:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    const v0, 0x7f10036d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 156
    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->at:Llnl;

    const-class v4, Lhee;

    invoke-static {v1, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhee;

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    .line 157
    const-string v4, "gaia_id"

    .line 158
    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "profile_photo_url"

    invoke-interface {v1, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-virtual {v0, v4, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    const-string v0, "media_picker_mode"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->W:I

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    new-instance v1, Ljcc;

    invoke-direct {v1, p0}, Ljcc;-><init>(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->R:Ljce;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 181
    return-object v2
.end method

.method public a()Lcom/google/android/libraries/social/ui/views/ObservableGridView;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 269
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v3, v0

    const-string v4, "_id"

    const-string v6, "date_added"

    new-instance v0, Ldi;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v7, " DESC"

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Ldi;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :goto_0
    return-object v0

    .line 272
    :cond_0
    const-string v4, "media_type=1 OR media_type=3"

    .line 277
    const-string v0, "content://media/external/file"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 278
    const-string v6, "date_added DESC"

    .line 279
    new-instance v0, Ldi;

    .line 280
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v1

    sget-object v3, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->N:[Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Ldi;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 339
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    .line 340
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->V()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0, v1, v1}, Landroid/provider/MediaStore$Images$Media;->insertImage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->S:Ljbx;

    new-instance v2, Ljbv;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v3

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v4, Ljac;->a:Ljac;

    invoke-direct {v2, v3, v0, v4}, Ljbv;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljac;)V

    invoke-virtual {v1, v2, p0}, Ljbx;->a(Lizr;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v5, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 340
    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "PhotoPickerFragment"

    const-string v2, "Could not find taken photo"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v5, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v5, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    throw v0

    .line 341
    :cond_1
    const/4 v0, 0x2

    if-ne v0, p1, :cond_2

    .line 342
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "_data"

    invoke-direct {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->V()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->at:Llnl;

    invoke-virtual {v1}, Llnl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->S:Ljbx;

    new-instance v2, Ljbv;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v3

    sget-object v4, Ljac;->b:Ljac;

    invoke-direct {v2, v3, v0, v4}, Ljbv;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljac;)V

    invoke-virtual {v1, v2, p0}, Ljbx;->a(Lizr;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iput-object v5, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v1, "PhotoPickerFragment"

    const-string v2, "Could not find taken video"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iput-object v5, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    goto :goto_0

    :catchall_1
    move-exception v0

    iput-object v5, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    throw v0

    .line 343
    :cond_2
    const/4 v0, 0x3

    if-ne v0, p1, :cond_0

    .line 344
    invoke-direct {p0, p2, p3}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 497
    if-nez p1, :cond_0

    .line 498
    sget-object v0, Ljac;->a:Ljac;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Ljac;)V

    .line 502
    :goto_0
    return-void

    .line 500
    :cond_0
    sget-object v0, Ljac;->b:Ljac;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Ljac;)V

    goto :goto_0
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 521
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 295
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 296
    iget-boolean v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->V:Z

    if-eqz v1, :cond_0

    .line 297
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v2, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->N:[Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 298
    const-wide/16 v2, -0x65

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(J)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 299
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    :cond_0
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v2, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->N:[Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 303
    const-wide/16 v2, -0x66

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(J)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 304
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/database/Cursor;

    .line 309
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 311
    new-instance v0, Landroid/database/MergeCursor;

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 312
    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->R:Ljce;

    invoke-virtual {v1, v0}, Ljce;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 313
    iget v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->P:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 314
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    iget v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->P:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->setSelection(I)V

    .line 316
    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 113
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "android.hardware.camera"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->at:Llnl;

    .line 115
    invoke-virtual {v0}, Llnl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "android.hardware.camera.front"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->V:Z

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->au:Llnh;

    const-class v2, Ljbx;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbx;

    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->S:Ljbx;

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->au:Llnh;

    const-class v2, Ljbt;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbt;

    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->Q:Ljbt;

    .line 118
    new-instance v0, Ljce;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2, v3}, Ljce;-><init>(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->R:Ljce;

    .line 119
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v1, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 120
    if-eqz p1, :cond_1

    .line 121
    const-string v0, "STATE_CURRENT_PHOTO_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    .line 122
    const-string v0, "STATE_SCROLL_POSITION"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->P:I

    .line 124
    :cond_1
    return-void

    .line 115
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 512
    return-void
.end method

.method public a(Ldo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->R:Ljce;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljce;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 321
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 66
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lizr;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 327
    if-eq p0, p2, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Ljce;

    invoke-virtual {v0}, Ljce;->notifyDataSetChanged()V

    .line 330
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 186
    invoke-super {p0}, Llol;->aO_()V

    .line 187
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->S:Ljbx;

    invoke-virtual {v0, p0}, Ljbx;->a(Ljby;)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->U:Ljbp;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100371

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    if-eqz v0, :cond_0

    new-instance v1, Ljbp;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljbp;-><init>(Landroid/app/Activity;Landroid/view/TextureView;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->U:Ljbp;

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 509
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 515
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 518
    return-void
.end method

.method public d()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 221
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->k()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "options"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-le v2, v3, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 128
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 129
    const-string v0, "STATE_CURRENT_PHOTO_PATH"

    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->O:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 130
    const-string v1, "STATE_SCROLL_POSITION"

    iget v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->P:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->T:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    .line 131
    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->getFirstVisiblePosition()I

    move-result v0

    .line 130
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    return-void

    .line 131
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->P:I

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 226
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Nexus 4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 193
    invoke-super {p0}, Llol;->z()V

    .line 194
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->S:Ljbx;

    invoke-virtual {v0, p0}, Ljbx;->b(Ljby;)V

    .line 195
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->U()V

    .line 196
    return-void
.end method
