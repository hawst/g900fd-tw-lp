.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;
.super Lcom/google/android/libraries/social/media/ui/MediaView;
.source "PG"


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 36
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->a:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    .line 37
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->l()Lizu;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_1

    .line 39
    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    const-string v0, "size="

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 43
    if-eq v0, v4, :cond_1

    .line 44
    add-int/lit8 v2, v0, 0x5

    .line 45
    const/16 v0, 0x26

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 46
    if-ne v0, v4, :cond_0

    .line 47
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 49
    :cond_0
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 50
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 51
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 52
    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 53
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->a:F

    .line 60
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->a:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->a:F

    .line 61
    :goto_0
    const v1, 0x7fffffff

    invoke-static {v1, p1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    .line 62
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 63
    int-to-float v1, v1

    div-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryMapView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 65
    invoke-super {p0, p1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->onMeasure(II)V

    .line 66
    return-void

    .line 60
    :cond_2
    const v0, 0x3fb72c23

    goto :goto_0
.end method
