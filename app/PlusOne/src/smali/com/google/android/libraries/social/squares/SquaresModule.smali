.class public Lcom/google/android/libraries/social/squares/SquaresModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    const-class v0, Lktq;

    if-ne p2, v0, :cond_1

    .line 31
    const-class v0, Lktq;

    new-instance v1, Lktr;

    invoke-direct {v1, p1}, Lktr;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 48
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    const-class v0, Lhzs;

    if-ne p2, v0, :cond_2

    .line 33
    const-class v0, Lhzs;

    new-instance v1, Lktt;

    invoke-direct {v1}, Lktt;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 34
    :cond_2
    const-class v0, Lkxp;

    if-ne p2, v0, :cond_3

    .line 35
    const-class v0, Lkxp;

    new-instance v1, Lkxp;

    invoke-direct {v1, p3}, Lkxp;-><init>(Llnh;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 36
    :cond_3
    const-class v0, Lheo;

    if-ne p2, v0, :cond_4

    .line 37
    const-class v0, Lheo;

    const-class v1, Lkxp;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 38
    :cond_4
    const-class v0, Lktw;

    if-ne p2, v0, :cond_5

    .line 39
    const-class v0, Lktw;

    new-instance v1, Lktw;

    invoke-direct {v1, p1}, Lktw;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 40
    :cond_5
    const-class v0, Licv;

    if-ne p2, v0, :cond_6

    .line 41
    const-class v0, Licv;

    new-instance v1, Lktd;

    invoke-direct {v1}, Lktd;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :cond_6
    const-class v0, Lkwk;

    if-ne p2, v0, :cond_7

    .line 43
    const-class v0, Lkwk;

    new-instance v1, Lkvm;

    invoke-direct {v1}, Lkvm;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 44
    :cond_7
    const-class v0, Ligx;

    if-ne p2, v0, :cond_0

    .line 45
    const-class v0, Ligx;

    new-instance v1, Lkym;

    invoke-direct {v1}, Lkym;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 46
    const-class v0, Ligx;

    new-instance v1, Lkvf;

    invoke-direct {v1}, Lkvf;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
