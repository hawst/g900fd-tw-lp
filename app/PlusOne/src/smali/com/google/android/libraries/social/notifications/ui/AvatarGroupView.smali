.class public Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;
.super Landroid/view/ViewGroup;
.source "PG"


# static fields
.field private static a:[I

.field private static b:Ljava/lang/Integer;

.field private static c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->b:Ljava/lang/Integer;

    .line 24
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->c:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method private a()I
    .locals 2

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->getChildCount()I

    move-result v0

    sget-object v1, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a:[I

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private a(IIIII)V
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 172
    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 175
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 126
    sget-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a:[I

    if-nez v0, :cond_0

    .line 127
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 128
    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f0d0149

    .line 129
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0d014a

    .line 130
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0d014b

    .line 131
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x7f0d014c

    .line 132
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    aput v3, v1, v2

    sput-object v1, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a:[I

    .line 134
    const v1, 0x7f0d014d

    .line 135
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->b:Ljava/lang/Integer;

    .line 136
    const v1, 0x7f0d014e

    .line 137
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->c:Ljava/lang/Integer;

    .line 139
    :cond_0
    invoke-static {p0}, Llii;->g(Landroid/view/View;)V

    .line 140
    return-void
.end method

.method private a(Llvm;IZZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 143
    new-instance v3, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    .line 144
    invoke-virtual {v3, p2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 145
    invoke-virtual {v3, p3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c(Z)V

    .line 146
    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(Z)V

    .line 147
    iget-object v0, p1, Llvm;->c:Ljava/lang/String;

    iget-object v4, p1, Llvm;->b:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    if-eqz p4, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 149
    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 150
    invoke-virtual {v3, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 151
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->addView(Landroid/view/View;)V

    .line 152
    return-void

    :cond_0
    move v0, v2

    .line 148
    goto :goto_0
.end method


# virtual methods
.method public a([Llvm;ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->removeAllViews()V

    .line 50
    if-eqz p1, :cond_0

    array-length v1, p1

    if-lez v1, :cond_0

    .line 51
    array-length v1, p1

    .line 53
    if-ne v1, v3, :cond_1

    .line 54
    aget-object v0, p1, v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(Llvm;IZZ)V

    .line 61
    :cond_0
    return-void

    .line 56
    :cond_1
    :goto_0
    if-ge v0, v1, :cond_0

    .line 57
    aget-object v2, p1, v0

    invoke-direct {p0, v2, v3, p2, p3}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(Llvm;IZZ)V

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 13

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->getMeasuredWidth()I

    move-result v11

    .line 82
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->getMeasuredHeight()I

    move-result v12

    .line 84
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->getChildCount()I

    move-result v0

    .line 85
    sget-object v1, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a:[I

    invoke-direct {p0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a()I

    move-result v2

    aget v4, v1, v2

    .line 87
    packed-switch v0, :pswitch_data_0

    .line 114
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    .line 116
    const/4 v6, 0x1

    sub-int v7, v11, v4

    const/4 v8, 0x0

    move-object v5, p0

    move v9, v11

    move v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    .line 118
    const/4 v1, 0x2

    const/4 v2, 0x0

    sub-int v3, v12, v4

    move-object v0, p0

    move v5, v12

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    .line 120
    const/4 v1, 0x3

    sub-int v2, v11, v4

    sub-int v3, v12, v4

    move-object v0, p0

    move v4, v11

    move v5, v12

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    .line 123
    :goto_0
    return-void

    .line 89
    :pswitch_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    goto :goto_0

    .line 94
    :pswitch_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    .line 96
    const/4 v1, 0x1

    sub-int v2, v11, v4

    sub-int v3, v12, v4

    move-object v0, p0

    move v4, v11

    move v5, v12

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    goto :goto_0

    .line 101
    :pswitch_2
    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v3, v4, v0

    sget-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->c:Ljava/lang/Integer;

    .line 102
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v5, v12, v0

    move-object v0, p0

    .line 101
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    .line 104
    const/4 v6, 0x1

    sub-int v0, v11, v4

    div-int/lit8 v7, v0, 0x2

    sget-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int v0, v11, v4

    div-int/lit8 v9, v0, 0x2

    sget-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->b:Ljava/lang/Integer;

    .line 105
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int v10, v4, v0

    move-object v5, p0

    .line 104
    invoke-direct/range {v5 .. v10}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    .line 107
    const/4 v3, 0x2

    sget-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->c:Ljava/lang/Integer;

    .line 108
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v5, v4, v0

    sget-object v0, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->c:Ljava/lang/Integer;

    .line 109
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v7, v12, v0

    move-object v2, p0

    move v6, v11

    .line 107
    invoke-direct/range {v2 .. v7}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a(IIIII)V

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 65
    invoke-static {v0, p1}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->resolveSize(II)I

    move-result v1

    .line 66
    invoke-static {v0, p2}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->resolveSize(II)I

    move-result v2

    .line 68
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->getChildCount()I

    move-result v3

    .line 69
    if-lez v3, :cond_1

    .line 70
    invoke-direct {p0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a()I

    move-result v3

    .line 71
    :goto_0
    if-gt v0, v3, :cond_1

    .line 72
    sget-object v4, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a:[I

    aget v4, v4, v3

    sget-object v5, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->a:[I

    aget v5, v5, v3

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v6, v4, v5}, Landroid/view/View;->measure(II)V

    .line 71
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {p0, v1, v2}, Lcom/google/android/libraries/social/notifications/ui/AvatarGroupView;->setMeasuredDimension(II)V

    .line 77
    return-void
.end method
