.class public Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;
.super Landroid/app/IntentService;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    const-string v0, "GunsIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lkfo;
    .locals 4

    .prologue
    .line 73
    const-string v0, "account_name"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 74
    const-string v0, "effective_gaia_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "GunsIntentService"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot handle action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " either accountName or effectiveGaiaId must be present."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lkfo;

    invoke-direct {v0, v1, v2}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Intent;)Ljix;
    .locals 1

    .prologue
    .line 94
    const-string v0, "trigger"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_0

    .line 96
    check-cast v0, Ljix;

    .line 99
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljix;->a:Ljix;

    goto :goto_0
.end method

.method public static c(Landroid/content/Intent;)Ljin;
    .locals 1

    .prologue
    .line 106
    const-string v0, "fetch_category"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    check-cast v0, Ljin;

    .line 112
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljin;->a:Ljin;

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljjn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjn;

    .line 88
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljjn;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Ljjm;

    .line 89
    invoke-virtual {p0}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljjm;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 90
    return-void
.end method
