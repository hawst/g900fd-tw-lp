.class public final Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmm;


# instance fields
.field private e:Lksh;

.field private f:Lksg;

.field private g:Lkrp;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 75
    invoke-direct {p0}, Lloa;-><init>()V

    .line 54
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->y:Llqc;

    const v2, 0x7f120016

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->x:Llnh;

    .line 55
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 57
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    .line 77
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->x:Llnh;

    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 78
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;Lkrp;)Lkrp;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    return-object p1
.end method

.method public static synthetic a(Landroid/content/Context;ZLkrs;)V
    .locals 2

    .prologue
    .line 50
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {p2}, Lkrs;->o()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "socialcast_private_posts"

    invoke-interface {v0, v1, p1}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->l()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)Lksg;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->f:Lksg;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)Lkrp;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V
    .locals 5

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->j:Z

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    invoke-virtual {v1}, Lkrp;->w()Z

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    invoke-virtual {v0}, Lkrp;->w()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->j:Z

    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->invalidateOptionsMenu()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->f()Lae;

    move-result-object v0

    const-string v1, "StreamCastUiFragment"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v1

    const v2, 0x1020002

    new-instance v3, Lksj;

    invoke-direct {v3}, Lksj;-><init>()V

    const-string v4, "StreamCastUiFragment"

    invoke-virtual {v1, v2, v3, v4}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    move-result-object v1

    invoke-virtual {v1}, Lat;->b()I

    :cond_1
    const-string v1, "CastActionBarIconFragment"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    new-instance v2, Lkri;

    invoke-direct {v2}, Lkri;-><init>()V

    const-string v3, "account_id"

    iget-object v4, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    invoke-virtual {v4}, Lkrp;->o()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v1}, Lkri;->f(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    const-string v1, "CastActionBarIconFragment"

    invoke-virtual {v0, v2, v1}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    :cond_2
    return-void
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V
    .locals 4

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->f()Lae;

    move-result-object v0

    const-string v1, "ConnectingFragment"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lkrt;

    invoke-direct {v2}, Lkrt;-><init>()V

    const-string v3, "ConnectingFragment"

    invoke-virtual {v0, v1, v2, v3}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    :cond_0
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 157
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    const-string v1, "data_source"

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string v1, "selected_route_id"

    iget-object v2, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->e:Lksh;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 162
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 146
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->x:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->x:Llnh;

    const-string v1, "com.google.android.libraries.social.appid"

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/String;I)Llnh;

    .line 149
    return-void
.end method

.method public a(Lhjk;)V
    .locals 5

    .prologue
    const v4, 0x7f1006fd

    .line 189
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 190
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 191
    new-instance v0, Lksi;

    .line 192
    invoke-interface {p1, v4}, Lhjk;->c(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->j:Z

    iget-object v3, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    invoke-direct {v0, v1, v2, v3}, Lksi;-><init>(Landroid/view/MenuItem;ZLkrs;)V

    .line 191
    invoke-interface {p1, v4, v0}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 194
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 177
    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 178
    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 179
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 180
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return v0
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 153
    new-instance v0, Lhmk;

    sget-object v1, Lonk;->l:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public k()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 348
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    invoke-virtual {v0}, Lkrp;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    invoke-virtual {v0}, Lkrp;->c()V

    .line 355
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->finish()V

    .line 356
    return-void

    .line 353
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "shutdown"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->e:Lksh;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 84
    new-instance v0, Lksh;

    invoke-direct {v0, p0}, Lksh;-><init>(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->e:Lksh;

    .line 85
    new-instance v0, Lksg;

    invoke-direct {v0, p0}, Lksg;-><init>(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->f:Lksg;

    .line 87
    if-eqz p1, :cond_1

    .line 88
    const-string v0, "data_source"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->h:Ljava/lang/String;

    .line 89
    const-string v0, "selected_route_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->i:Ljava/lang/String;

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    .line 94
    const-string v1, "data_source"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->h:Ljava/lang/String;

    .line 95
    const-string v1, "selected_route_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 102
    const-string v0, "data_source"

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v0, "selected_route_id"

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-super {p0, p1}, Lloa;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 105
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 109
    invoke-super {p0}, Lloa;->onStart()V

    .line 111
    new-instance v0, Lkse;

    invoke-direct {v0, p0}, Lkse;-><init>(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V

    .line 119
    new-instance v1, Lksf;

    invoke-direct {v1, p0}, Lksf;-><init>(Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;)V

    .line 127
    invoke-static {p0}, Lkso;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    invoke-static {p0, v0, v1}, Lkso;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->l()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    iget-object v1, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->f:Lksg;

    invoke-virtual {v0, v1}, Lkrp;->b(Lkrl;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->e:Lksh;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/socialcast/impl/StreamCastActivity;->g:Lkrp;

    .line 141
    :cond_0
    invoke-super {p0}, Lloa;->onStop()V

    .line 142
    return-void
.end method
