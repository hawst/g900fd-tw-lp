.class public Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;
.super Landroid/widget/FrameLayout;
.source "PG"


# static fields
.field private static final a:Z

.field private static final b:Z


# instance fields
.field private c:Landroid/opengl/GLSurfaceView;

.field private d:Z

.field private e:Landroid/view/Choreographer$FrameCallback;

.field private f:[F

.field private final g:Ljava/lang/Object;

.field private h:Llfk;

.field private i:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b:Z

    return-void

    :cond_0
    move v0, v2

    .line 50
    goto :goto_0

    :cond_1
    move v1, v2

    .line 52
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 88
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    iput-boolean v3, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->d:Z

    .line 71
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->f:[F

    .line 76
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->g:Ljava/lang/Object;

    .line 229
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->i:Landroid/graphics/RectF;

    .line 89
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 93
    :cond_0
    new-instance v0, Llfk;

    invoke-direct {v0}, Llfk;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    new-instance v1, Llfe;

    invoke-direct {v1, p0}, Llfe;-><init>(Landroid/view/View;)V

    iput-object v1, v0, Llfk;->g:Llfe;

    .line 96
    new-instance v0, Landroid/opengl/GLSurfaceView;

    invoke-direct {v0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->setEGLContextClientVersion(I)V

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    new-instance v1, Llfl;

    invoke-direct {v1, p0}, Llfl;-><init>(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)V

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v3}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, -0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Landroid/opengl/GLSurfaceView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    return-object v0
.end method

.method private a(Llfk;)V
    .locals 3

    .prologue
    .line 173
    if-eqz p1, :cond_0

    iget-object v0, p1, Llfk;->e:Llfi;

    if-eqz v0, :cond_0

    iget v0, p1, Llfk;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->getWidth()I

    move-result v0

    if-nez v0, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p1, Llfk;->e:Llfi;

    invoke-interface {v1}, Llfi;->b()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 179
    invoke-virtual {p0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p1, Llfk;->e:Llfi;

    invoke-interface {v2}, Llfi;->c()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 177
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p1, Llfk;->a:F

    goto :goto_0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 80
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->d:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Llfk;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->g:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/graphics/Matrix;)V
    .locals 11

    .prologue
    const/16 v10, 0xb4

    const/4 v2, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 232
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iget-object v0, v0, Llfk;->e:Llfi;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iget-object v0, v0, Llfk;->e:Llfi;

    invoke-interface {v0}, Llfi;->e()I

    move-result v4

    .line 237
    rem-int/lit16 v0, v4, 0xb4

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    move v3, v0

    .line 238
    :goto_1
    if-eqz v3, :cond_5

    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iget-object v0, v0, Llfk;->e:Llfi;

    invoke-interface {v0}, Llfi;->c()I

    move-result v0

    move v1, v0

    .line 240
    :goto_2
    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iget-object v0, v0, Llfk;->e:Llfi;

    invoke-interface {v0}, Llfi;->b()I

    move-result v0

    .line 242
    :goto_3
    iget-object v5, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->i:Landroid/graphics/RectF;

    int-to-float v6, v1

    int-to-float v7, v0

    invoke-virtual {v5, v8, v8, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 243
    iget-object v5, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->i:Landroid/graphics/RectF;

    invoke-virtual {p1, v5}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 244
    iget-object v5, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->f:[F

    invoke-virtual {p1, v5}, Landroid/graphics/Matrix;->getValues([F)V

    .line 245
    div-int/lit8 v1, v1, 0x2

    .line 246
    div-int/lit8 v0, v0, 0x2

    .line 247
    iget-object v5, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->f:[F

    aget v5, v5, v2

    .line 248
    invoke-virtual {p0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v6, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->i:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    sub-float/2addr v2, v6

    div-float/2addr v2, v9

    div-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 249
    invoke-virtual {p0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->getHeight()I

    move-result v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->i:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    sub-float/2addr v6, v7

    div-float/2addr v6, v9

    div-float/2addr v6, v5

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 250
    const/16 v7, 0x5a

    if-eq v4, v7, :cond_2

    if-ne v4, v10, :cond_7

    .line 251
    :cond_2
    int-to-float v1, v1

    iget-object v7, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->i:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    div-float/2addr v7, v5

    int-to-float v2, v2

    sub-float v2, v7, v2

    add-float/2addr v1, v2

    float-to-int v2, v1

    .line 255
    :goto_4
    if-eq v4, v10, :cond_3

    const/16 v1, 0x10e

    if-ne v4, v1, :cond_8

    .line 256
    :cond_3
    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->i:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    div-float/2addr v1, v5

    int-to-float v4, v6

    sub-float/2addr v1, v4

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 260
    :goto_5
    iget-object v1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iput v5, v1, Llfk;->a:F

    .line 261
    iget-object v4, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    if-eqz v3, :cond_9

    move v1, v0

    :goto_6
    iput v1, v4, Llfk;->b:I

    .line 262
    iget-object v1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    if-eqz v3, :cond_a

    :goto_7
    iput v2, v1, Llfk;->c:I

    .line 263
    invoke-virtual {p0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->invalidate()V

    goto/16 :goto_0

    :cond_4
    move v3, v2

    .line 237
    goto/16 :goto_1

    .line 238
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iget-object v0, v0, Llfk;->e:Llfi;

    .line 239
    invoke-interface {v0}, Llfi;->b()I

    move-result v0

    move v1, v0

    goto/16 :goto_2

    .line 240
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iget-object v0, v0, Llfk;->e:Llfi;

    .line 241
    invoke-interface {v0}, Llfi;->c()I

    move-result v0

    goto/16 :goto_3

    .line 253
    :cond_7
    int-to-float v1, v1

    iget-object v7, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->i:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    div-float/2addr v7, v5

    int-to-float v2, v2

    sub-float v2, v7, v2

    sub-float/2addr v1, v2

    float-to-int v2, v1

    goto :goto_4

    .line 258
    :cond_8
    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->i:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    div-float/2addr v1, v5

    int-to-float v4, v6

    sub-float/2addr v1, v4

    sub-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_5

    :cond_9
    move v1, v2

    .line 261
    goto :goto_6

    :cond_a
    move v2, v0

    .line 262
    goto :goto_7
.end method

.method public a(Llfi;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 137
    sget-boolean v1, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v1, :cond_0

    .line 150
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 141
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iput-object p1, v1, Llfk;->e:Llfi;

    .line 142
    iget-object v1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iput-object p2, v1, Llfk;->f:Ljava/lang/Runnable;

    .line 143
    iget-object v3, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Llfi;->b()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    :goto_1
    iput v1, v3, Llfk;->b:I

    .line 144
    iget-object v3, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Llfi;->c()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    :goto_2
    iput v1, v3, Llfk;->c:I

    .line 145
    iget-object v1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Llfi;->e()I

    move-result v0

    :cond_1
    iput v0, v1, Llfk;->d:I

    .line 146
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    const/4 v1, 0x0

    iput v1, v0, Llfk;->a:F

    .line 147
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a(Llfk;)V

    .line 148
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    invoke-virtual {p0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->invalidate()V

    goto :goto_0

    :cond_2
    move v1, v0

    .line 143
    goto :goto_1

    :cond_3
    move v1, v0

    .line 144
    goto :goto_2

    .line 148
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 116
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    iget-object v0, v0, Llfk;->g:Llfe;

    invoke-virtual {v0}, Llfe;->b()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 123
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onPause()V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 130
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onResume()V

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 184
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    .line 201
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v0, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->b:Z

    if-eqz v0, :cond_3

    .line 206
    iget-boolean v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->d:Z

    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->e:Landroid/view/Choreographer$FrameCallback;

    if-nez v0, :cond_2

    new-instance v0, Llfj;

    invoke-direct {v0, p0}, Llfj;-><init>(Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->e:Landroid/view/Choreographer$FrameCallback;

    :cond_2
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->e:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0

    .line 208
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 163
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 164
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v0, :cond_0

    .line 169
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->h:Llfk;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a(Llfk;)V

    .line 169
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setTranslationX(F)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 193
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-nez v0, :cond_0

    .line 197
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setTranslationX(F)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 109
    sget-boolean v0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->a:Z

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/social/tiledimage/views/TiledImageView;->c:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, p1}, Landroid/opengl/GLSurfaceView;->setVisibility(I)V

    .line 112
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 113
    return-void
.end method
