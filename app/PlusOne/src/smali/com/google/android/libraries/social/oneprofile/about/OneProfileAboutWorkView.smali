.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;
.super Ljnl;
.source "PG"


# instance fields
.field private a:Landroid/view/ViewGroup;

.field private o:Landroid/view/View;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/view/ViewGroup;

.field private s:Landroid/view/View;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/ViewGroup;

.field private w:Landroid/view/View;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/view/View;

.field private z:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public static b(Lnjt;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 309
    if-eqz p0, :cond_3

    .line 310
    iget-object v1, p0, Lnjt;->e:Lnkd;

    .line 311
    if-eqz v1, :cond_3

    .line 312
    iget-object v2, v1, Lnkd;->e:Lnjv;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lnkd;->e:Lnjv;

    iget-object v2, v2, Lnjv;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 324
    :cond_0
    :goto_0
    return v0

    .line 315
    :cond_1
    iget-object v2, v1, Lnkd;->n:Lnjv;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lnkd;->n:Lnjv;

    iget-object v2, v2, Lnjv;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 318
    :cond_2
    iget-object v2, v1, Lnkd;->f:Lnio;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lnkd;->f:Lnio;

    iget-object v2, v2, Lnio;->b:[Lnin;

    if-eqz v2, :cond_3

    iget-object v1, v1, Lnkd;->f:Lnio;

    iget-object v1, v1, Lnio;->b:[Lnin;

    array-length v1, v1

    if-gtz v1, :cond_0

    .line 324
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-super {p0}, Ljnl;->a()V

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 80
    return-void
.end method

.method public a(Lnjt;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 283
    .line 287
    if-eqz p1, :cond_3

    .line 288
    iget-object v3, p1, Lnjt;->e:Lnkd;

    .line 289
    if-eqz v3, :cond_3

    .line 290
    iget-object v0, v3, Lnkd;->e:Lnjv;

    if-eqz v0, :cond_2

    .line 291
    iget-object v0, v3, Lnkd;->e:Lnjv;

    iget-object v0, v0, Lnjv;->b:Ljava/lang/String;

    .line 293
    :goto_0
    iget-object v2, v3, Lnkd;->n:Lnjv;

    if-eqz v2, :cond_1

    .line 294
    iget-object v2, v3, Lnkd;->n:Lnjv;

    iget-object v2, v2, Lnjv;->b:Ljava/lang/String;

    .line 296
    :goto_1
    iget-object v4, v3, Lnkd;->f:Lnio;

    if-eqz v4, :cond_0

    .line 297
    iget-object v1, v3, Lnkd;->f:Lnio;

    iget-object v1, v1, Lnio;->b:[Lnin;

    .line 302
    :cond_0
    :goto_2
    const/4 v3, 0x1

    invoke-virtual {p0, v0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 304
    invoke-virtual {p0, v2, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->b(Ljava/lang/String;Z)Z

    .line 305
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a([Lnin;)V

    .line 306
    return-void

    :cond_1
    move-object v2, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v2, v1

    move-object v0, v1

    goto :goto_2
.end method

.method public a([Lnin;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 160
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->z:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 161
    if-eqz p1, :cond_a

    array-length v0, p1

    if-lez v0, :cond_a

    move v4, v6

    .line 162
    :goto_0
    if-nez v4, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->j:Z

    if-eqz v0, :cond_12

    .line 163
    :cond_0
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->v:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->k:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 164
    iget-object v5, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->y:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->k:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_2
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 166
    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->j:Z

    if-eqz v0, :cond_d

    .line 167
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->v:Landroid/view/ViewGroup;

    .line 168
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f020416

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 167
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->v:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a(Landroid/view/View;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->v:Landroid/view/ViewGroup;

    new-instance v5, Ljnp;

    invoke-direct {v5, p0}, Ljnp;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;)V

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 185
    :goto_3
    if-eqz v4, :cond_10

    .line 186
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->z:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 190
    array-length v7, p1

    move v5, v2

    move v0, v6

    :goto_4
    if-ge v5, v7, :cond_11

    aget-object v8, p1, v5

    .line 191
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->getContext()Landroid/content/Context;

    move-result-object v9

    new-instance v10, Landroid/widget/LinearLayout;

    invoke-direct {v10, v9}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {v10, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->z:Landroid/view/ViewGroup;

    invoke-virtual {v1, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, v8, Lnin;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v4, v8, Lnin;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a(Landroid/widget/TextView;Z)V

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v4, v8, Lnin;->d:Lnih;

    if-eqz v4, :cond_16

    iget-object v0, v4, Lnih;->a:Lnhz;

    if-eqz v0, :cond_15

    iget-object v0, v4, Lnih;->a:Lnhz;

    iget-object v0, v0, Lnhz;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_15

    iget-object v0, v4, Lnih;->a:Lnhz;

    iget-object v0, v0, Lnhz;->a:Ljava/lang/Integer;

    :goto_5
    iget-object v1, v4, Lnih;->b:Lnhz;

    if-eqz v1, :cond_14

    iget-object v1, v4, Lnih;->b:Lnhz;

    iget-object v1, v1, Lnhz;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    iget-object v1, v4, Lnih;->b:Lnhz;

    iget-object v1, v1, Lnhz;->a:Ljava/lang/Integer;

    :goto_6
    iget-object v11, v4, Lnih;->c:Ljava/lang/Boolean;

    if-eqz v11, :cond_13

    iget-object v4, v4, Lnih;->c:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move v12, v4

    move-object v4, v0

    move v0, v12

    :goto_7
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v4, :cond_2

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    if-eqz v0, :cond_e

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    const-string v0, " - "

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->i:Ljava/lang/String;

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    :goto_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v8, Lnin;->c:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, v8, Lnin;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_7

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_6

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_8

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->c(Landroid/widget/TextView;)V

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_8
    iget-object v0, v8, Lnin;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iget-object v1, v8, Lnin;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->d(Landroid/widget/TextView;)V

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 190
    :cond_9
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v2

    goto/16 :goto_4

    :cond_a
    move v4, v2

    .line 161
    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 163
    goto/16 :goto_1

    :cond_c
    move v0, v2

    .line 164
    goto/16 :goto_2

    .line 182
    :cond_d
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->w:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 191
    :cond_e
    if-eqz v1, :cond_4

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_f

    const-string v0, " - "

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 196
    :cond_10
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->x:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->e(Landroid/widget/TextView;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->z:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 206
    :cond_11
    :goto_9
    return-void

    .line 202
    :cond_12
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->z:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_9

    :cond_13
    move-object v4, v0

    move v0, v2

    goto/16 :goto_7

    :cond_14
    move-object v1, v3

    goto/16 :goto_6

    :cond_15
    move-object v0, v3

    goto/16 :goto_5

    :cond_16
    move v0, v2

    move-object v1, v3

    move-object v4, v3

    goto/16 :goto_7
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 84
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->j:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    move v3, v1

    .line 85
    :goto_1
    if-eqz v3, :cond_5

    .line 86
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 87
    iget-boolean v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->j:Z

    if-eqz v4, :cond_3

    .line 88
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a:Landroid/view/ViewGroup;

    .line 89
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020416

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 88
    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 90
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a(Landroid/view/View;)V

    .line 91
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a:Landroid/view/ViewGroup;

    new-instance v5, Ljnn;

    invoke-direct {v5, p0}, Ljnn;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->o:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 104
    :goto_2
    if-eqz v0, :cond_4

    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a(Landroid/widget/TextView;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->q:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->d(Landroid/widget/TextView;)V

    .line 117
    :goto_3
    if-eqz p2, :cond_6

    if-nez v3, :cond_6

    :goto_4
    return v1

    :cond_1
    move v0, v2

    .line 83
    goto :goto_0

    :cond_2
    move v3, v2

    .line 84
    goto :goto_1

    .line 102
    :cond_3
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->o:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 109
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->q:Landroid/widget/TextView;

    const v4, 0x7f0a0309

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->e(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->b(Landroid/widget/TextView;Z)V

    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->q:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->e(Landroid/widget/TextView;)V

    goto :goto_3

    .line 115
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_3

    :cond_6
    move v1, v2

    .line 117
    goto :goto_4
.end method

.method public b(Ljava/lang/String;Z)Z
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 122
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->j:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    move v3, v1

    .line 123
    :goto_1
    if-eqz v3, :cond_5

    .line 124
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 125
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->j:Z

    if-eqz v3, :cond_3

    .line 126
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->r:Landroid/view/ViewGroup;

    .line 127
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020416

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 126
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 128
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->r:Landroid/view/ViewGroup;

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a(Landroid/view/View;)V

    .line 129
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->r:Landroid/view/ViewGroup;

    new-instance v4, Ljno;

    invoke-direct {v4, p0}, Ljno;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->s:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 143
    :goto_2
    if-eqz v0, :cond_4

    .line 144
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->u:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->t:Landroid/widget/TextView;

    invoke-virtual {p0, v3, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a(Landroid/widget/TextView;Z)V

    .line 146
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->u:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->d(Landroid/widget/TextView;)V

    .line 156
    :goto_3
    if-eqz p2, :cond_6

    if-nez v0, :cond_6

    :goto_4
    return v1

    :cond_1
    move v0, v2

    .line 121
    goto :goto_0

    :cond_2
    move v3, v2

    .line 122
    goto :goto_1

    .line 140
    :cond_3
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->s:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 148
    :cond_4
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->u:Landroid/widget/TextView;

    const v4, 0x7f0a030c

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->e(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->t:Landroid/widget/TextView;

    invoke-virtual {p0, v3, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->b(Landroid/widget/TextView;Z)V

    .line 150
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->u:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->e(Landroid/widget/TextView;)V

    goto :goto_3

    .line 154
    :cond_5
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_3

    :cond_6
    move v1, v2

    .line 156
    goto :goto_4
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 54
    const v0, 0x7f10043f

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->a:Landroid/view/ViewGroup;

    .line 55
    const v0, 0x7f100440

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->o:Landroid/view/View;

    .line 56
    const v0, 0x7f100441

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->p:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f100442

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->q:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f100443

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->r:Landroid/view/ViewGroup;

    .line 60
    const v0, 0x7f100444

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->s:Landroid/view/View;

    .line 61
    const v0, 0x7f100445

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->t:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f100446

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->u:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f100447

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->v:Landroid/view/ViewGroup;

    .line 65
    const v0, 0x7f100449

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->w:Landroid/view/View;

    .line 66
    const v0, 0x7f10044b

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->x:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f10044a

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->y:Landroid/view/View;

    .line 68
    const v0, 0x7f10044c

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->z:Landroid/view/ViewGroup;

    .line 69
    return-void
.end method
