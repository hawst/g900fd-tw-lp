.class public Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Llcs;


# instance fields
.field private final A:Lgu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgu",
            "<",
            "Lldy;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lldv;

.field public b:Landroid/widget/ListAdapter;

.field private c:Llhc;

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:F

.field private k:F

.field private l:I

.field private final m:Landroid/view/VelocityTracker;

.field private final n:Lljq;

.field private final o:Lnh;

.field private final p:Lnh;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I

.field private v:[I

.field private w:[I

.field private x:[Lldt;

.field private final y:Lldz;

.field private final z:Lleb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 379
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 383
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 386
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m:Landroid/view/VelocityTracker;

    .line 84
    new-instance v0, Lldu;

    invoke-direct {v0, p0}, Lldu;-><init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V

    .line 91
    new-instance v0, Lldz;

    invoke-direct {v0, p0}, Lldz;-><init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    .line 93
    new-instance v0, Lleb;

    invoke-direct {v0, p0}, Lleb;-><init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->z:Lleb;

    .line 95
    new-instance v0, Lgu;

    invoke-direct {v0}, Lgu;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    .line 388
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 389
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->f:I

    .line 390
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->g:I

    .line 391
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->h:I

    .line 392
    invoke-static {p1}, Lljq;->a(Landroid/content/Context;)Lljq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    .line 394
    new-instance v0, Lnh;

    invoke-direct {v0, p1}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o:Lnh;

    .line 395
    new-instance v0, Lnh;

    invoke-direct {v0, p1}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p:Lnh;

    .line 396
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->setWillNotDraw(Z)V

    .line 397
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->setClipToPadding(Z)V

    .line 416
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;I)I
    .locals 0

    .prologue
    .line 45
    iput p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    return p1
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1592
    if-nez p1, :cond_4

    .line 1593
    const-string v0, "StreamGridView"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v1, v2

    .line 1594
    :goto_0
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    invoke-virtual {v0, v1}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lldy;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lldy;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, " -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v0, v0, Lldv;->a:I

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v0, v0, v2

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x23

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, " mItemTops["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "]="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1595
    :cond_2
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->q()V

    .line 1597
    :cond_3
    new-instance v0, Lldw;

    invoke-direct {v0}, Lldw;-><init>()V

    throw v0

    .line 1599
    :cond_4
    return-void
.end method

.method private a(IZ)Z
    .locals 17

    .prologue
    .line 778
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j()Z

    move-result v7

    .line 779
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 783
    if-nez v7, :cond_13

    .line 786
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->r:Z

    .line 792
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v2

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v5}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(II)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v2, v2, Lldv;->b:I

    add-int/2addr v1, v2

    .line 794
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(II)I

    move-result v2

    .line 796
    if-lez p1, :cond_0

    .line 798
    const/4 v1, 0x1

    .line 810
    :goto_0
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 811
    if-eqz v3, :cond_e

    .line 812
    if-eqz v1, :cond_2

    .line 813
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v4

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v4, :cond_3

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v8

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v9

    add-int/2addr v9, v3

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v10

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v11

    add-int/2addr v11, v3

    invoke-virtual {v6, v8, v9, v10, v11}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 801
    :cond_0
    if-gez v1, :cond_1

    .line 805
    const/4 v1, 0x0

    .line 807
    :cond_1
    const/4 v2, 0x0

    move/from16 v16, v2

    move v2, v1

    move/from16 v1, v16

    goto :goto_0

    .line 812
    :cond_2
    neg-int v1, v3

    move v3, v1

    goto :goto_1

    .line 813
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v4, v1, Lldv;->a:I

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v4, :cond_4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v8, v6, v1

    add-int/2addr v8, v3

    aput v8, v6, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v8, v6, v1

    add-int/2addr v8, v3

    aput v8, v6, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 814
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v1, v1, Lldv;->b:I

    neg-int v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getHeight()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v6, v6, Lldv;->b:I

    add-int/2addr v6, v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_4
    if-ltz v1, :cond_6

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v9

    if-le v9, v6, :cond_6

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->q:Z

    if-eqz v9, :cond_5

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v9}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->removeViewsInLayout(II)V

    :goto_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    invoke-virtual {v9, v8}, Lldz;->a(Landroid/view/View;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->removeViewAt(I)V

    goto :goto_5

    :cond_6
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_8

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v6

    if-ge v6, v4, :cond_8

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->q:Z

    if-eqz v6, :cond_7

    const/4 v6, 0x0

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v8}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->removeViewsInLayout(II)V

    :goto_7
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    invoke-virtual {v6, v1}, Lldz;->a(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    goto :goto_6

    :cond_7
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->removeViewAt(I)V

    goto :goto_7

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v8

    if-lez v8, :cond_d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    const v4, 0x7fffffff

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    const/high16 v4, -0x80000000

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([II)V

    const/4 v1, 0x0

    move v6, v1

    :goto_8
    if-ge v6, v8, :cond_b

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lldx;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v10, v10, Lldv;->b:I

    sub-int/2addr v9, v10

    iget v1, v1, Lldx;->topMargin:I

    sub-int/2addr v9, v1

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    add-int/2addr v4, v6

    invoke-virtual {v1, v4}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lldy;

    if-eqz v1, :cond_9

    const/4 v4, 0x1

    :goto_9
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Z)V

    iget v11, v1, Lldy;->c:I

    const/4 v4, 0x0

    :goto_a
    if-ge v4, v11, :cond_a

    iget v12, v1, Lldy;->a:I

    add-int/2addr v12, v4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v14, v14, v12

    iget-object v15, v1, Lldy;->d:[I

    aget v15, v15, v12

    sub-int v15, v9, v15

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v14

    aput v14, v13, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v14, v14, v12

    invoke-static {v14, v10}, Ljava/lang/Math;->max(II)I

    move-result v14

    aput v14, v13, v12

    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    :cond_9
    const/4 v4, 0x0

    goto :goto_9

    :cond_a
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_8

    :cond_b
    const/4 v1, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v4, v4, Lldv;->a:I

    if-ge v1, v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v4, v4, v1

    const v6, 0x7fffffff

    if-ne v4, v6, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingTop()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aput v4, v6, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aput v4, v6, v1

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n()V

    .line 816
    :cond_e
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->r:Z

    .line 817
    sub-int v1, v5, v2

    .line 823
    :goto_c
    if-eqz p2, :cond_10

    .line 824
    invoke-static/range {p0 .. p0}, Liu;->a(Landroid/view/View;)I

    move-result v2

    .line 826
    if-eqz v2, :cond_f

    const/4 v4, 0x1

    if-ne v2, v4, :cond_10

    if-nez v7, :cond_10

    .line 829
    :cond_f
    if-lez v1, :cond_10

    .line 830
    if-lez p1, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o:Lnh;

    .line 831
    :goto_d
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    invoke-virtual {v1, v2}, Lnh;->a(F)Z

    .line 832
    invoke-static/range {p0 .. p0}, Liu;->c(Landroid/view/View;)V

    .line 837
    :cond_10
    if-eqz v3, :cond_11

    .line 838
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d(I)V

    .line 841
    :cond_11
    if-eqz p1, :cond_12

    if-eqz v3, :cond_15

    :cond_12
    const/4 v1, 0x1

    :goto_e
    return v1

    .line 820
    :cond_13
    const/4 v3, 0x0

    move v1, v5

    goto :goto_c

    .line 830
    :cond_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p:Lnh;

    goto :goto_d

    .line 841
    :cond_15
    const/4 v1, 0x0

    goto :goto_e
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->s:Z

    return p1
.end method

.method private b(II)I
    .locals 18

    .prologue
    .line 987
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingLeft()I

    move-result v6

    .line 988
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingRight()I

    move-result v2

    .line 989
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v7, v3, Lldv;->b:I

    .line 990
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v8, v3, Lldv;->a:I

    .line 992
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getWidth()I

    move-result v3

    sub-int/2addr v3, v6

    sub-int v2, v3, v2

    add-int/lit8 v3, v8, -0x1

    mul-int/2addr v3, v7

    sub-int/2addr v2, v3

    div-int v9, v2, v8

    .line 994
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingTop()I

    move-result v10

    .line 995
    move/from16 v0, p2

    neg-int v11, v0

    .line 996
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->l()I

    move-result v2

    .line 999
    :goto_0
    if-ltz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v2, v3, v2

    if-le v2, v11, :cond_9

    if-ltz p1, :cond_9

    .line 1000
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(I)Landroid/view/View;

    move-result-object v12

    .line 1001
    if-nez v12, :cond_0

    .line 1002
    const/4 v2, 0x0

    .line 1065
    :goto_1
    return v2

    .line 1005
    :cond_0
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lldx;

    .line 1007
    invoke-virtual {v12}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    move-object/from16 v0, p0

    if-eq v3, v0, :cond_1

    .line 1008
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->q:Z

    if-eqz v3, :cond_3

    .line 1009
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v3, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1015
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lldy;

    .line 1016
    if-eqz v3, :cond_4

    const/4 v4, 0x1

    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Z)V

    .line 1018
    iget v4, v3, Lldy;->c:I

    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v13

    .line 1019
    mul-int v4, v9, v13

    add-int/lit8 v5, v13, -0x1

    mul-int/2addr v5, v7

    add-int/2addr v4, v5

    iget v5, v2, Lldx;->leftMargin:I

    iget v14, v2, Lldx;->rightMargin:I

    add-int/2addr v5, v14

    sub-int/2addr v4, v5

    .line 1021
    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 1024
    iget v4, v2, Lldx;->height:I

    const/4 v14, -0x2

    if-ne v4, v14, :cond_5

    .line 1025
    const/4 v4, 0x0

    const/4 v14, 0x0

    invoke-static {v4, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1029
    :goto_4
    invoke-virtual {v12, v5, v4}, Landroid/view/View;->measure(II)V

    .line 1031
    const v5, 0x7fffffff

    .line 1032
    const/4 v4, 0x0

    move/from16 v17, v4

    move v4, v5

    move/from16 v5, v17

    :goto_5
    if-ge v5, v13, :cond_6

    .line 1033
    iget v14, v3, Lldy;->a:I

    add-int/2addr v14, v5

    .line 1034
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v15, v15, v14

    if-ge v15, v4, :cond_2

    .line 1035
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v4, v4, v14

    .line 1032
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 1011
    :cond_3
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->addView(Landroid/view/View;I)V

    goto :goto_2

    .line 1016
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 1027
    :cond_5
    iget v4, v2, Lldx;->height:I

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v4, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_4

    .line 1039
    :cond_6
    const/4 v5, 0x0

    :goto_6
    if-ge v5, v13, :cond_7

    .line 1040
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    iget v15, v3, Lldy;->a:I

    add-int/2addr v15, v5

    aput v4, v14, v15

    .line 1039
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 1043
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    iget v5, v3, Lldy;->a:I

    aget v4, v4, v5

    .line 1044
    iget v5, v2, Lldx;->bottomMargin:I

    sub-int/2addr v4, v5

    .line 1045
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v4, v5

    .line 1046
    iget v14, v3, Lldy;->a:I

    add-int v15, v9, v7

    mul-int/2addr v14, v15

    add-int/2addr v14, v6

    iget v15, v2, Lldx;->leftMargin:I

    add-int/2addr v14, v15

    .line 1047
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    add-int/2addr v15, v14

    .line 1048
    invoke-virtual {v12, v14, v5, v15, v4}, Landroid/view/View;->layout(IIII)V

    .line 1050
    const/4 v4, 0x0

    :goto_7
    if-ge v4, v13, :cond_8

    .line 1051
    iget v12, v3, Lldy;->a:I

    add-int/2addr v12, v4

    .line 1052
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    sub-int v15, v5, v7

    iget v0, v2, Lldx;->topMargin:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    iget-object v0, v3, Lldy;->d:[I

    move-object/from16 v16, v0

    aget v16, v16, v12

    sub-int v15, v15, v16

    aput v15, v14, v12

    .line 1050
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 1055
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->l()I

    move-result v3

    .line 1056
    add-int/lit8 v2, p1, -0x1

    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    move/from16 p1, v2

    move v2, v3

    .line 1057
    goto/16 :goto_0

    .line 1059
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getHeight()I

    move-result v3

    .line 1060
    const/4 v2, 0x0

    move/from16 v17, v2

    move v2, v3

    move/from16 v3, v17

    :goto_8
    if-ge v3, v8, :cond_b

    .line 1061
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v4, v4, v3

    if-ge v4, v2, :cond_a

    .line 1062
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v2, v2, v3

    .line 1060
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 1065
    :cond_b
    sub-int v2, v10, v2

    goto/16 :goto_1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;I)I
    .locals 0

    .prologue
    .line 45
    iput p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    return p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->r:Z

    return p1
.end method

.method private c(II)I
    .locals 19

    .prologue
    .line 1076
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingLeft()I

    move-result v8

    .line 1077
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingRight()I

    move-result v1

    .line 1078
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v9, v2, Lldv;->b:I

    .line 1079
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v10, v2, Lldv;->a:I

    .line 1081
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getWidth()I

    move-result v2

    sub-int/2addr v2, v8

    sub-int v1, v2, v1

    add-int/lit8 v2, v10, -0x1

    mul-int/2addr v2, v9

    sub-int/2addr v1, v2

    div-int v11, v1, v10

    .line 1083
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getHeight()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingBottom()I

    move-result v2

    sub-int v12, v1, v2

    .line 1084
    add-int v13, v12, p2

    .line 1085
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m()I

    move-result v1

    .line 1088
    :goto_0
    if-ltz v1, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v1, v2, v1

    if-ge v1, v13, :cond_f

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    move/from16 v0, p1

    if-ge v0, v1, :cond_f

    .line 1089
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(I)Landroid/view/View;

    move-result-object v14

    .line 1090
    if-nez v14, :cond_0

    .line 1091
    const/4 v1, 0x0

    .line 1178
    :goto_1
    return v1

    .line 1094
    :cond_0
    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lldx;

    .line 1096
    invoke-virtual {v14}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    move-object/from16 v0, p0

    if-eq v2, v0, :cond_1

    .line 1097
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->q:Z

    if-eqz v2, :cond_4

    .line 1098
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1104
    :cond_1
    :goto_2
    iget v2, v1, Lldx;->a:I

    invoke-static {v10, v2}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 1105
    mul-int v2, v11, v15

    add-int/lit8 v3, v15, -0x1

    mul-int/2addr v3, v9

    add-int/2addr v2, v3

    iget v3, v1, Lldx;->leftMargin:I

    iget v4, v1, Lldx;->rightMargin:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    .line 1107
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    .line 1109
    const/4 v3, 0x0

    .line 1111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lldy;

    .line 1112
    if-nez v2, :cond_5

    .line 1113
    new-instance v2, Lldy;

    invoke-direct {v2, v10}, Lldy;-><init>(I)V

    .line 1114
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    move/from16 v0, p1

    invoke-virtual {v4, v0, v2}, Lgu;->b(ILjava/lang/Object;)V

    .line 1119
    :cond_2
    :goto_3
    iput v15, v2, Lldy;->c:I

    .line 1121
    const v5, 0x7fffffff

    .line 1122
    const/4 v7, 0x0

    :goto_4
    sub-int v4, v10, v15

    if-gt v7, v4, :cond_7

    .line 1123
    const/high16 v4, -0x80000000

    move v6, v7

    .line 1124
    :goto_5
    add-int v17, v7, v15

    move/from16 v0, v17

    if-ge v6, v0, :cond_6

    .line 1125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    move-object/from16 v17, v0

    aget v17, v17, v6

    move/from16 v0, v17

    if-le v0, v4, :cond_3

    .line 1126
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v4, v4, v6

    .line 1124
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 1100
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 1115
    :cond_5
    iget v4, v2, Lldy;->c:I

    if-eq v4, v15, :cond_2

    .line 1116
    const/4 v3, 0x1

    goto :goto_3

    .line 1129
    :cond_6
    if-ge v4, v5, :cond_12

    .line 1131
    iput v7, v2, Lldy;->a:I

    .line 1122
    :goto_6
    add-int/lit8 v7, v7, 0x1

    move v5, v4

    goto :goto_4

    .line 1136
    :cond_7
    iget v4, v1, Lldx;->height:I

    const/4 v6, -0x2

    if-ne v4, v6, :cond_a

    .line 1137
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1141
    :goto_7
    move/from16 v0, v16

    invoke-virtual {v14, v0, v4}, Landroid/view/View;->measure(II)V

    .line 1143
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 1144
    if-nez v3, :cond_8

    iget v3, v2, Lldy;->b:I

    if-eq v4, v3, :cond_c

    iget v3, v2, Lldy;->b:I

    if-lez v3, :cond_c

    .line 1145
    :cond_8
    const-string v3, "StreamGridView"

    const/4 v6, 0x4

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_9

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v6, 0x36

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "invalidating layout records after position="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    invoke-virtual {v3}, Lgu;->b()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    :goto_8
    if-ltz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    invoke-virtual {v6, v3}, Lgu;->e(I)I

    move-result v6

    move/from16 v0, p1

    if-le v6, v0, :cond_b

    add-int/lit8 v3, v3, -0x1

    goto :goto_8

    .line 1139
    :cond_a
    iget v4, v1, Lldx;->height:I

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_7

    .line 1145
    :cond_b
    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    add-int/lit8 v7, v3, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lgu;->b()I

    move-result v16

    sub-int v3, v16, v3

    invoke-virtual {v6, v7, v3}, Lgu;->a(II)V

    .line 1147
    :cond_c
    iput v4, v2, Lldy;->b:I

    .line 1149
    iget-object v3, v2, Lldy;->d:[I

    const/4 v6, 0x0

    invoke-static {v3, v6}, Ljava/util/Arrays;->fill([II)V

    .line 1150
    const/4 v3, 0x0

    :goto_9
    if-ge v3, v15, :cond_d

    .line 1151
    iget v6, v2, Lldy;->a:I

    add-int/2addr v6, v3

    .line 1152
    iget-object v7, v2, Lldy;->d:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    move-object/from16 v16, v0

    aget v16, v16, v6

    sub-int v16, v5, v16

    aput v16, v7, v6

    .line 1153
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v16, v7, v6

    iget-object v0, v2, Lldy;->d:[I

    move-object/from16 v17, v0

    aget v17, v17, v6

    add-int v16, v16, v17

    aput v16, v7, v6

    .line 1150
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 1156
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    iget v5, v2, Lldy;->a:I

    aget v3, v3, v5

    .line 1157
    add-int/2addr v3, v9

    iget v5, v1, Lldx;->topMargin:I

    add-int/2addr v3, v5

    .line 1158
    add-int/2addr v4, v3

    .line 1159
    iget v5, v2, Lldy;->a:I

    add-int v6, v11, v9

    mul-int/2addr v5, v6

    add-int/2addr v5, v8

    iget v6, v1, Lldx;->leftMargin:I

    add-int/2addr v5, v6

    .line 1160
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v5

    .line 1161
    invoke-virtual {v14, v5, v3, v6, v4}, Landroid/view/View;->layout(IIII)V

    .line 1163
    const/4 v3, 0x0

    :goto_a
    if-ge v3, v15, :cond_e

    .line 1164
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    iget v6, v2, Lldy;->a:I

    add-int/2addr v6, v3

    iget v7, v1, Lldx;->bottomMargin:I

    add-int/2addr v7, v4

    aput v7, v5, v6

    .line 1163
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 1167
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m()I

    move-result v1

    .line 1168
    add-int/lit8 p1, p1, 0x1

    .line 1169
    goto/16 :goto_0

    .line 1171
    :cond_f
    const/4 v2, 0x0

    .line 1172
    const/4 v1, 0x0

    move/from16 v18, v1

    move v1, v2

    move/from16 v2, v18

    :goto_b
    if-ge v2, v10, :cond_11

    .line 1173
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v3, v3, v2

    if-le v3, v1, :cond_10

    .line 1174
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v1, v1, v2

    .line 1172
    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 1178
    :cond_11
    sub-int/2addr v1, v12

    goto/16 :goto_1

    :cond_12
    move v4, v5

    goto/16 :goto_6
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)Lljq;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    return-object v0
.end method

.method private c(I)V
    .locals 1

    .prologue
    .line 903
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->e:I

    if-eq p1, v0, :cond_0

    .line 904
    iput p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->e:I

    .line 905
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c:Llhc;

    if-eqz v0, :cond_0

    .line 906
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c:Llhc;

    invoke-interface {v0, p0, p1}, Llhc;->a(Landroid/view/View;I)V

    .line 909
    :cond_0
    return-void
.end method

.method private d(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 917
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c:Llhc;

    if-eqz v0, :cond_0

    .line 918
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c:Llhc;

    iget v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    if-nez v0, :cond_1

    .line 920
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->r()I

    move-result v0

    .line 918
    :goto_0
    invoke-interface {v2, p0, v3, p1, v0}, Llhc;->a(Landroid/view/View;III)V

    .line 922
    :cond_0
    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->onScrollChanged(IIII)V

    .line 923
    return-void

    :cond_1
    move v0, v1

    .line 920
    goto :goto_0
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->s:Z

    return v0
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n()V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o()V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)Lgu;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    return-object v0
.end method

.method public static synthetic h()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public static synthetic h(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)[I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    return-object v0
.end method

.method public static synthetic i()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)[I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->k()V

    return-void
.end method

.method private j()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 848
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 860
    :cond_0
    :goto_0
    return v1

    .line 852
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingTop()I

    move-result v2

    .line 853
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingBottom()I

    move-result v3

    sub-int v3, v0, v3

    move v0, v1

    .line 854
    :goto_1
    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v4, v4, Lldv;->a:I

    if-ge v0, v4, :cond_2

    .line 855
    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v4, v4, v0

    if-lt v4, v2, :cond_0

    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v4, v4, v0

    if-gt v4, v3, :cond_0

    .line 854
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 860
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private k()V
    .locals 18

    .prologue
    .line 1272
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingLeft()I

    move-result v6

    .line 1273
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingRight()I

    move-result v1

    .line 1274
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v7, v2, Lldv;->b:I

    .line 1275
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v8, v2, Lldv;->a:I

    .line 1276
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getWidth()I

    move-result v2

    sub-int/2addr v2, v6

    sub-int v1, v2, v1

    add-int/lit8 v2, v8, -0x1

    mul-int/2addr v2, v7

    sub-int/2addr v1, v2

    div-int v9, v1, v8

    .line 1278
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v10

    .line 1280
    const/4 v1, 0x0

    move v5, v1

    :goto_0
    if-ge v5, v10, :cond_6

    .line 1281
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 1282
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lldx;

    .line 1283
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    add-int/2addr v2, v5

    .line 1284
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    invoke-virtual {v3, v2}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lldy;

    .line 1286
    iget v3, v1, Lldx;->a:I

    invoke-static {v8, v3}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 1288
    const/high16 v4, -0x80000000

    .line 1289
    const/4 v3, 0x0

    move/from16 v17, v3

    move v3, v4

    move/from16 v4, v17

    :goto_1
    if-ge v4, v12, :cond_1

    .line 1290
    iget v13, v2, Lldy;->a:I

    add-int/2addr v13, v4

    .line 1291
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v14, v14, v13

    if-le v14, v3, :cond_0

    .line 1292
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v3, v3, v13

    .line 1289
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1296
    :cond_1
    invoke-virtual {v11}, Landroid/view/View;->isLayoutRequested()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1297
    mul-int v4, v9, v12

    add-int/lit8 v13, v12, -0x1

    mul-int/2addr v13, v7

    add-int/2addr v4, v13

    iget v13, v1, Lldx;->leftMargin:I

    iget v14, v1, Lldx;->rightMargin:I

    add-int/2addr v13, v14

    sub-int/2addr v4, v13

    .line 1299
    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v4, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    .line 1302
    iget v4, v1, Lldx;->height:I

    const/4 v14, -0x2

    if-ne v4, v14, :cond_3

    .line 1303
    const/4 v4, 0x0

    const/4 v14, 0x0

    invoke-static {v4, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1308
    :goto_2
    invoke-virtual {v11, v13, v4}, Landroid/view/View;->measure(II)V

    .line 1310
    :cond_2
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iput v4, v2, Lldy;->b:I

    .line 1312
    iget-object v4, v2, Lldy;->d:[I

    const/4 v13, 0x0

    invoke-static {v4, v13}, Ljava/util/Arrays;->fill([II)V

    .line 1313
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v12, :cond_4

    .line 1314
    iget v13, v2, Lldy;->a:I

    add-int/2addr v13, v4

    .line 1315
    iget-object v14, v2, Lldy;->d:[I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v15, v15, v13

    sub-int v15, v3, v15

    aput v15, v14, v13

    .line 1316
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v15, v14, v13

    iget-object v0, v2, Lldy;->d:[I

    move-object/from16 v16, v0

    aget v16, v16, v13

    add-int v15, v15, v16

    aput v15, v14, v13

    .line 1313
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1305
    :cond_3
    iget v4, v1, Lldx;->height:I

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v4, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_2

    .line 1319
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    iget v4, v2, Lldy;->a:I

    aget v3, v3, v4

    .line 1320
    add-int/2addr v3, v7

    iget v4, v1, Lldx;->topMargin:I

    add-int/2addr v3, v4

    .line 1321
    iget v4, v2, Lldy;->b:I

    add-int/2addr v4, v3

    .line 1322
    iget v13, v2, Lldy;->a:I

    add-int v14, v9, v7

    mul-int/2addr v13, v14

    add-int/2addr v13, v6

    iget v14, v1, Lldx;->leftMargin:I

    add-int/2addr v13, v14

    .line 1323
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    add-int/2addr v14, v13

    .line 1324
    invoke-virtual {v11, v13, v3, v14, v4}, Landroid/view/View;->layout(IIII)V

    .line 1326
    const/4 v3, 0x0

    :goto_4
    if-ge v3, v12, :cond_5

    .line 1327
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    iget v13, v2, Lldy;->a:I

    add-int/2addr v13, v3

    iget v14, v1, Lldx;->bottomMargin:I

    add-int/2addr v14, v4

    aput v14, v11, v13

    .line 1326
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1280
    :cond_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_0

    .line 1330
    :cond_6
    return-void
.end method

.method private l()I
    .locals 4

    .prologue
    .line 1379
    const/high16 v2, -0x80000000

    .line 1380
    const/4 v1, -0x1

    .line 1381
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v3, v3, Lldv;->a:I

    if-ge v0, v3, :cond_1

    .line 1382
    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v3, v3, v0

    if-le v3, v2, :cond_0

    .line 1383
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v2, v1, v0

    move v1, v0

    .line 1381
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1387
    :cond_1
    return v1
.end method

.method private m()I
    .locals 4

    .prologue
    .line 1391
    const v2, 0x7fffffff

    .line 1392
    const/4 v1, -0x1

    .line 1393
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v3, v3, Lldv;->a:I

    if-ge v0, v3, :cond_1

    .line 1394
    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v3, v3, v0

    if-ge v3, v2, :cond_0

    .line 1395
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v2, v1, v0

    move v1, v0

    .line 1393
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1399
    :cond_1
    return v1
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v0, v0, Lldv;->a:I

    .line 1404
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    array-length v1, v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    array-length v1, v1

    if-eq v1, v0, :cond_1

    .line 1406
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v0, v0, Lldv;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    .line 1407
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v0, v0, Lldv;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    .line 1410
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingTop()I

    move-result v0

    .line 1411
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 1412
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 1413
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1495
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1496
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Lldz;->a(Landroid/view/View;)V

    .line 1495
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1499
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->q:Z

    if-eqz v0, :cond_2

    .line 1500
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->removeAllViewsInLayout()V

    .line 1505
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    array-length v0, v0

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v2, v2, Lldv;->a:I

    if-eq v0, v2, :cond_3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v2, v2, Lldv;->a:I

    new-array v2, v2, [Lldt;

    iput-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    :goto_2
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v2, v2, Lldv;->a:I

    if-ge v1, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    new-instance v3, Lldt;

    invoke-direct {v3, v0}, Lldt;-><init>(Landroid/content/Context;)V

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1502
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->removeAllViews()V

    goto :goto_1

    .line 1506
    :cond_3
    return-void
.end method

.method private p()V
    .locals 4

    .prologue
    .line 1525
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    if-eqz v0, :cond_1

    .line 1526
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1527
    iget-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->q:Z

    if-eqz v1, :cond_0

    .line 1528
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->removeViewInLayout(Landroid/view/View;)V

    .line 1532
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    array-length v3, v3

    sub-int/2addr v2, v3

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lgu;->c(I)V

    .line 1526
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1530
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 1543
    :cond_1
    return-void
.end method

.method private q()V
    .locals 9

    .prologue
    .line 1616
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1618
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1619
    iget v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    add-int/2addr v3, v0

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 1620
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x4a

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, " -> "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " l="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", t="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", r="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", b="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1619
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1622
    :cond_0
    return-void
.end method

.method private r()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1643
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 1654
    :cond_0
    :goto_0
    return v0

    .line 1646
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c()I

    move-result v1

    if-nez v1, :cond_0

    .line 1650
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingTop()I

    move-result v1

    .line 1651
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1653
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lldx;

    .line 1654
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a()I

    move-result v3

    add-int/2addr v1, v3

    iget v0, v0, Lldx;->topMargin:I

    add-int/2addr v0, v1

    sub-int v0, v2, v0

    goto :goto_0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 1658
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o:Lnh;

    .line 1660
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o:Lnh;

    invoke-virtual {v0}, Lnh;->c()Z

    move-result v0

    .line 1662
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p:Lnh;

    .line 1663
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p:Lnh;

    invoke-virtual {v1}, Lnh;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1665
    if-eqz v0, :cond_0

    .line 1666
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 1668
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v0, v0, Lldv;->b:I

    return v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 360
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p()V

    .line 362
    iput p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    .line 364
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    if-lez v0, :cond_0

    .line 365
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v1, v1, Lldv;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    .line 370
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 18

    .prologue
    .line 508
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    .line 509
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o()V

    .line 510
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    invoke-virtual {v2}, Lgu;->c()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v8, v2, Lldv;->b:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v9, v2, Lldv;->a:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v3, v9, -0x1

    mul-int/2addr v3, v8

    sub-int/2addr v2, v3

    div-int v10, v2, v9

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    if-ge v3, v2, :cond_6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(I)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_5

    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lldx;

    new-instance v12, Lldy;

    invoke-direct {v12, v9}, Lldy;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    invoke-virtual {v4, v3, v12}, Lgu;->b(ILjava/lang/Object;)V

    iget v4, v2, Lldx;->a:I

    invoke-static {v9, v4}, Ljava/lang/Math;->min(II)I

    move-result v13

    iput v13, v12, Lldy;->c:I

    const v5, 0x7fffffff

    const/4 v7, 0x0

    :goto_1
    sub-int v4, v9, v13

    if-gt v7, v4, :cond_2

    const/high16 v4, -0x80000000

    move v6, v7

    :goto_2
    add-int v14, v7, v13

    if-ge v6, v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v14, v14, v6

    if-le v14, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v4, v4, v6

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_1
    if-ge v4, v5, :cond_a

    iput v7, v12, Lldy;->a:I

    :goto_3
    add-int/lit8 v7, v7, 0x1

    move v5, v4

    goto :goto_1

    :cond_2
    mul-int v4, v10, v13

    add-int/lit8 v6, v13, -0x1

    mul-int/2addr v6, v8

    add-int/2addr v4, v6

    iget v6, v2, Lldx;->leftMargin:I

    iget v7, v2, Lldx;->rightMargin:I

    add-int/2addr v6, v7

    sub-int/2addr v4, v6

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    iget v4, v2, Lldx;->height:I

    const/4 v7, -0x2

    if-ne v4, v7, :cond_3

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    :goto_4
    invoke-virtual {v11, v6, v4}, Landroid/view/View;->measure(II)V

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iput v4, v12, Lldy;->b:I

    iget-object v4, v12, Lldy;->d:[I

    const/4 v6, 0x0

    invoke-static {v4, v6}, Ljava/util/Arrays;->fill([II)V

    const/4 v4, 0x0

    :goto_5
    if-ge v4, v13, :cond_4

    iget v6, v12, Lldy;->a:I

    add-int/2addr v6, v4

    iget-object v7, v12, Lldy;->d:[I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v14, v14, v6

    sub-int v14, v5, v14

    aput v14, v7, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v14, v7, v6

    iget-object v15, v12, Lldy;->d:[I

    aget v15, v15, v6

    iget v0, v12, Lldy;->b:I

    move/from16 v16, v0

    add-int v15, v15, v16

    add-int/2addr v15, v8

    iget v0, v2, Lldx;->bottomMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    add-int/2addr v14, v15

    aput v14, v7, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_3
    iget v4, v2, Lldx;->height:I

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_4

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    invoke-virtual {v2, v11}, Lldz;->a(Landroid/view/View;)V

    :cond_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_0

    :cond_6
    const v3, 0x7fffffff

    const/4 v2, 0x0

    move/from16 v17, v2

    move v2, v3

    move/from16 v3, v17

    :goto_6
    if-ge v3, v9, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v4, v4, v3

    if-ge v4, v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v2, v2, v3

    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_8
    const/4 v3, 0x0

    :goto_7
    if-ge v3, v9, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v5, v5, v3

    sub-int/2addr v5, v2

    add-int v5, v5, p2

    aput v5, v4, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v5, v5, v3

    aput v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 511
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->requestLayout()V

    .line 512
    return-void

    :cond_a
    move v4, v5

    goto/16 :goto_3
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 445
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    .line 446
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->z:Lleb;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 449
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->A:Lgu;

    invoke-virtual {v1}, Lgu;->c()V

    .line 450
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o()V

    .line 451
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n()V

    .line 452
    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    .line 453
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    invoke-virtual {v1}, Lldz;->a()V

    .line 454
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j:F

    .line 456
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    .line 457
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->s:Z

    .line 458
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    :cond_1
    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    .line 460
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    if-lez v0, :cond_2

    .line 461
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v1, v1, Lldv;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->u:I

    .line 466
    :cond_2
    if-eqz p1, :cond_3

    .line 467
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->z:Lleb;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 468
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lldz;->a(I)V

    .line 470
    :cond_3
    return-void
.end method

.method public a(Lldv;)V
    .locals 1

    .prologue
    .line 419
    new-instance v0, Lldv;

    invoke-direct {v0, p1}, Lldv;-><init>(Lldv;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    .line 420
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n()V

    .line 421
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o()V

    .line 422
    return-void
.end method

.method public a(Llea;)V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    iput-object p1, v0, Lldz;->a:Llea;

    .line 479
    return-void
.end method

.method public a(Llhc;)V
    .locals 1

    .prologue
    .line 491
    iput-object p1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c:Llhc;

    .line 492
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d(I)V

    .line 493
    return-void
.end method

.method public aE_()V
    .locals 1

    .prologue
    .line 426
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d:Z

    .line 427
    return-void
.end method

.method public aF_()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 432
    iput-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d:Z

    .line 433
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(I)V

    .line 434
    return-void
.end method

.method final b(I)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1339
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_3

    .line 1340
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    sub-int v2, p1, v0

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v3, v0, Lldv;->a:I

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    array-length v0, v0

    if-ge v2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v4, v4, Lldv;->b:I

    add-int/lit8 v5, v3, -0x1

    mul-int/2addr v4, v5

    sub-int/2addr v0, v4

    div-int v4, v0, v3

    const/high16 v0, -0x80000000

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v5, v5, v1

    if-ge v0, v5, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v0, v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    aget-object v1, v1, v2

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v3, v3, v2

    sub-int/2addr v0, v3

    invoke-virtual {v1, v4, v0}, Lldt;->a(II)V

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->x:[Lldt;

    aget-object v0, v0, v2

    .line 1374
    :goto_2
    return-object v0

    .line 1344
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 1345
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    invoke-virtual {v1, v0}, Lldz;->b(I)Landroid/view/View;

    move-result-object v0

    .line 1347
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1348
    if-eq v1, v0, :cond_4

    if-eqz v0, :cond_4

    .line 1350
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    invoke-virtual {v2, v0}, Lldz;->a(Landroid/view/View;)V

    .line 1353
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1355
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eq v2, p0, :cond_6

    .line 1356
    if-nez v0, :cond_7

    .line 1357
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1362
    :cond_5
    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1365
    :cond_6
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lldx;

    .line 1366
    iput p1, v0, Lldx;->b:I

    .line 1367
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v2

    iput v2, v0, Lldx;->c:I

    .line 1368
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, v0, Lldx;->d:J

    move-object v0, v1

    .line 1369
    goto :goto_2

    .line 1358
    :cond_7
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1359
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_3

    .line 1370
    :catch_0
    move-exception v0

    .line 1371
    const-string v1, "StreamGridView"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1372
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    .line 1373
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x5e

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Exception while trying to obtain a view at position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in view: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " activity: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " adapter: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1371
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1374
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public b()Llea;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->y:Lldz;

    iget-object v0, v0, Lldz;->a:Llea;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 504
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    return v0
.end method

.method public computeScroll()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 738
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    invoke-virtual {v1}, Lljq;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 739
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    invoke-virtual {v1}, Lljq;->c()I

    move-result v1

    .line 740
    int-to-float v2, v1

    iget v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 741
    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j:F

    .line 742
    const/4 v1, 0x0

    invoke-direct {p0, v2, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 744
    :cond_0
    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    invoke-virtual {v1}, Lljq;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 745
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 769
    :cond_1
    :goto_0
    return-void

    .line 747
    :cond_2
    if-eqz v0, :cond_4

    .line 748
    invoke-static {p0}, Liu;->a(Landroid/view/View;)I

    move-result v0

    .line 749
    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    .line 751
    if-lez v2, :cond_5

    .line 752
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o:Lnh;

    .line 756
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    invoke-virtual {v1}, Lljq;->d()F

    move-result v1

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lnh;->a(I)Z

    .line 757
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 759
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    invoke-virtual {v0}, Lljq;->f()V

    .line 761
    :cond_4
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(I)V
    :try_end_0
    .catch Lldw; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 764
    :catch_0
    move-exception v0

    .line 765
    const-string v1, "StreamGridView"

    const-string v2, "StreamGridView state got corrupted in computeScroll, resetting to top of stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 767
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 754
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p:Lnh;
    :try_end_1
    .catch Lldw; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public d()I
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 519
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v4, v0, Lldv;->a:I

    .line 520
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v0, v0, Lldv;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    if-nez v0, :cond_1

    :cond_0
    move v0, v4

    .line 562
    :goto_0
    return v0

    .line 524
    :cond_1
    if-ne v4, v2, :cond_2

    move v0, v2

    .line 525
    goto :goto_0

    .line 528
    :cond_2
    const v0, 0x7fffffff

    move v1, v7

    .line 532
    :goto_1
    if-ge v1, v4, :cond_4

    .line 533
    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v3, v3, v1

    if-ge v3, v0, :cond_3

    .line 534
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v0, v0, v1

    .line 532
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v3, v4

    .line 541
    :goto_2
    const/4 v1, 0x2

    if-lt v3, v1, :cond_9

    move v6, v7

    .line 544
    :goto_3
    sub-int v1, v4, v3

    if-gt v6, v1, :cond_8

    .line 545
    const/high16 v1, -0x80000000

    move v5, v6

    .line 547
    :goto_4
    add-int v8, v6, v3

    if-ge v5, v8, :cond_6

    .line 548
    iget-object v8, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v8, v8, v5

    if-le v8, v1, :cond_5

    .line 549
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    aget v1, v1, v5

    .line 547
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 556
    :cond_6
    sub-int/2addr v1, v0

    iget-object v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v5, v5, Lldv;->c:I

    if-gt v1, v5, :cond_7

    move v0, v3

    .line 557
    goto :goto_0

    .line 544
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 541
    :cond_8
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    :cond_9
    move v0, v2

    .line 562
    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 865
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 867
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o:Lnh;

    .line 868
    const/4 v0, 0x0

    .line 869
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o:Lnh;

    invoke-virtual {v2}, Lnh;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 870
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o:Lnh;

    invoke-virtual {v0, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    move v0, v1

    .line 873
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p:Lnh;

    invoke-virtual {v2}, Lnh;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 874
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 875
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getWidth()I

    move-result v2

    .line 876
    neg-int v3, v2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 877
    const/high16 v3, 0x43340000    # 180.0f

    int-to-float v2, v2

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v2, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 878
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p:Lnh;

    invoke-virtual {v2, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    .line 879
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 883
    :goto_0
    if-eqz v1, :cond_1

    .line 884
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 887
    :cond_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 1639
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->r()I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 692
    iget-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d:Z

    if-nez v1, :cond_1

    .line 721
    :cond_0
    :goto_0
    return v0

    .line 697
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xc

    if-lt v1, v3, :cond_2

    .line 698
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j()Z

    move-result v1

    if-nez v1, :cond_2

    .line 699
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 700
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 721
    :cond_2
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 703
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    move v1, v2

    .line 709
    :goto_1
    cmpl-float v2, v1, v2

    if-eqz v2, :cond_2

    .line 710
    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->i:I

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Llii;->a(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->i:I

    :cond_3
    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->i:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 711
    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 713
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0

    .line 706
    :cond_4
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    goto :goto_1

    .line 700
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 567
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d:Z

    if-nez v0, :cond_0

    move v0, v2

    .line 610
    :goto_0
    return v0

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 572
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 573
    packed-switch v0, :pswitch_data_0

    :cond_1
    :pswitch_0
    move v0, v2

    .line 610
    goto :goto_0

    .line 575
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 576
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    invoke-virtual {v0}, Lljq;->f()V

    .line 577
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j:F

    .line 578
    invoke-static {p1, v2}, Lik;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->l:I

    .line 579
    iput v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->k:F

    .line 580
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 582
    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(I)V

    move v0, v3

    .line 583
    goto :goto_0

    .line 588
    :pswitch_2
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->l:I

    invoke-static {p1, v0}, Lik;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 589
    if-gez v0, :cond_2

    .line 590
    const-string v0, "StreamGridView"

    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->l:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x7b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " - did StreamGridView receive an inconsistent event stream?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 593
    goto :goto_0

    .line 595
    :cond_2
    invoke-static {p1, v0}, Lik;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 596
    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->k:F

    add-float/2addr v1, v0

    .line 597
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->f:I

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-lez v0, :cond_3

    move v4, v3

    .line 598
    :goto_1
    if-eqz v4, :cond_5

    .line 599
    cmpl-float v0, v1, v5

    if-lez v0, :cond_4

    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->f:I

    neg-int v0, v0

    int-to-float v0, v0

    :goto_2
    add-float/2addr v0, v1

    .line 601
    :goto_3
    float-to-int v1, v0

    .line 602
    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->k:F

    .line 604
    if-eqz v4, :cond_1

    move v0, v3

    .line 605
    goto/16 :goto_0

    :cond_3
    move v4, v2

    .line 597
    goto :goto_1

    .line 599
    :cond_4
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->f:I

    int-to-float v0, v0

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3

    .line 573
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 927
    iput-boolean v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->q:Z

    .line 928
    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 929
    :cond_0
    :goto_0
    iput-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->q:Z

    .line 931
    sub-int v0, p4, p2

    .line 932
    sub-int v1, p5, p3

    .line 933
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o:Lnh;

    invoke-virtual {v2, v0, v1}, Lnh;->a(II)V

    .line 934
    iget-object v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->p:Lnh;

    invoke-virtual {v2, v0, v1}, Lnh;->a(II)V

    .line 935
    return-void

    .line 928
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v2, v0, Lldv;->a:I

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_2

    iget-object v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->w:[I

    iget-object v4, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->v:[I

    aget v4, v4, v0

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iput-boolean v5, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->r:Z

    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->s:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->o()V

    :goto_2
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v2

    add-int/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(II)I

    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->t:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getPaddingTop()I

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(II)I

    iput-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->r:Z

    iput-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->s:Z

    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d(I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->k()V

    goto :goto_2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 616
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d:Z

    if-nez v1, :cond_0

    move v0, v9

    .line 686
    :goto_0
    return v0

    .line 620
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 621
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 622
    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_1
    move v0, v9

    .line 686
    goto :goto_0

    .line 624
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 625
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    invoke-virtual {v0}, Lljq;->f()V

    .line 626
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j:F

    .line 627
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lik;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->l:I

    .line 628
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->k:F
    :try_end_0
    .catch Lldw; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 681
    :catch_0
    move-exception v0

    .line 682
    const-string v1, "StreamGridView"

    const-string v2, "StreamGridView state got corrupted in onTouchEvent, resetting to top of stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 684
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Landroid/widget/ListAdapter;)V

    goto :goto_1

    .line 632
    :pswitch_1
    :try_start_1
    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->l:I

    invoke-static {p1, v1}, Lik;->a(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 633
    if-gez v1, :cond_2

    .line 634
    const-string v1, "StreamGridView"

    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->l:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x7b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - did StreamGridView receive an inconsistent event stream?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 639
    :cond_2
    invoke-static {p1, v1}, Lik;->d(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 640
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j:F

    sub-float v0, v2, v0

    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->k:F

    add-float/2addr v0, v1

    .line 641
    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->e:I

    if-nez v1, :cond_3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->f:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_3

    .line 642
    cmpl-float v1, v0, v4

    if-lez v1, :cond_4

    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->f:I

    neg-int v1, v1

    int-to-float v1, v1

    :goto_2
    add-float/2addr v0, v1

    .line 643
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(I)V

    .line 645
    :cond_3
    float-to-int v1, v0

    .line 646
    int-to-float v3, v1

    sub-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->k:F

    .line 648
    iget v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->e:I

    if-ne v0, v9, :cond_1

    .line 649
    iput v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j:F

    .line 651
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 653
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_1

    .line 642
    :cond_4
    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->f:I

    int-to-float v1, v1

    goto :goto_2

    .line 660
    :pswitch_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(I)V

    .line 661
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->s()V

    goto/16 :goto_1

    .line 665
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->g:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 666
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->m:Landroid/view/VelocityTracker;

    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->l:I

    invoke-static {v0, v1}, Liq;->b(Landroid/view/VelocityTracker;I)F

    move-result v4

    .line 668
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->h:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 669
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(I)V

    .line 670
    iget-object v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->n:Lljq;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/high16 v7, -0x80000000

    const v8, 0x7fffffff

    invoke-virtual/range {v0 .. v8}, Lljq;->a(IIIIIIII)V

    .line 672
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j:F

    .line 673
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 677
    :goto_3
    invoke-direct {p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->s()V

    goto/16 :goto_1

    .line 675
    :cond_5
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(I)V
    :try_end_1
    .catch Lldw; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 622
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 891
    iget-boolean v0, p0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->r:Z

    if-nez v0, :cond_0

    .line 892
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 894
    :cond_0
    return-void
.end method
