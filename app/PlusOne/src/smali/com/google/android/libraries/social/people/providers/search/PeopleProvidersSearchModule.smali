.class public Lcom/google/android/libraries/social/people/providers/search/PeopleProvidersSearchModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    const-class v0, Ljtr;

    if-ne p2, v0, :cond_1

    .line 79
    const-class v0, Ljtr;

    new-instance v1, Ljtr;

    invoke-direct {v1}, Ljtr;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    const-class v0, Lhzd;

    if-ne p2, v0, :cond_0

    .line 82
    const-class v0, Lhzd;

    new-instance v1, Ljtl;

    const-string v2, "CirclesAndPeople.search"

    invoke-direct {v1, v2}, Ljtl;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 90
    const-class v0, Lhzd;

    new-instance v1, Ljtm;

    const-string v2, "Circles.search"

    invoke-direct {v1, v2}, Ljtm;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 97
    const-class v0, Lhzd;

    new-instance v1, Ljtn;

    const-string v2, "People.search"

    invoke-direct {v1, v2}, Ljtn;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
