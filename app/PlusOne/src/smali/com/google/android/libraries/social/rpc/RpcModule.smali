.class public Lcom/google/android/libraries/social/rpc/RpcModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    const-class v0, Lkfd;

    if-ne p2, v0, :cond_1

    .line 21
    const-class v0, Lkfd;

    new-instance v1, Lkfb;

    invoke-direct {v1, p1}, Lkfb;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 33
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    const-class v0, Lkfe;

    if-ne p2, v0, :cond_2

    .line 23
    sget-object v0, Lkfc;->a:Lloy;

    .line 24
    const-class v0, Ljhc;

    invoke-virtual {p3, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhc;

    .line 28
    if-eqz v0, :cond_0

    .line 29
    const-class v1, Lkfe;

    new-instance v2, Lkfl;

    invoke-direct {v2, v0}, Lkfl;-><init>(Ljhc;)V

    invoke-virtual {p3, v1, v2}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 31
    :cond_2
    const-class v0, Llow;

    if-ne p2, v0, :cond_0

    .line 32
    sget-object v0, Llow;->a:Lloy;

    goto :goto_0
.end method
