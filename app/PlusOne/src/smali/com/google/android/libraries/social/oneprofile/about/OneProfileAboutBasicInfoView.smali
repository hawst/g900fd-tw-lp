.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;
.super Ljnl;
.source "PG"


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/view/View;

.field private C:Landroid/view/ViewGroup;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/TextView;

.field private F:Landroid/view/View;

.field private G:Z

.field private a:Landroid/view/ViewGroup;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/view/View;

.field private r:Landroid/view/ViewGroup;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/view/View;

.field private v:Landroid/view/ViewGroup;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/view/ViewGroup;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method public static b(Lnjt;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 382
    if-eqz p0, :cond_4

    .line 383
    iget-object v3, p0, Lnjt;->e:Lnkd;

    .line 384
    if-eqz v3, :cond_4

    .line 385
    iget-object v2, v3, Lnkd;->d:Lnip;

    .line 386
    iget v2, v2, Lnip;->b:I

    packed-switch v2, :pswitch_data_0

    .line 393
    iget-object v2, v3, Lnkd;->l:Lnjk;

    .line 394
    if-eqz v2, :cond_0

    iget-object v4, v2, Lnjk;->b:[Lnjj;

    if-eqz v4, :cond_0

    iget-object v4, v2, Lnjk;->b:[Lnjj;

    array-length v4, v4

    if-lez v4, :cond_0

    .line 396
    iget-object v4, v2, Lnjk;->b:[Lnjj;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 397
    iget v6, v6, Lnjj;->b:I

    packed-switch v6, :pswitch_data_1

    .line 396
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 407
    :cond_0
    iget-object v2, v3, Lnkd;->i:Lnhu;

    if-eqz v2, :cond_2

    iget-object v2, v3, Lnkd;->i:Lnhu;

    iget-object v2, v2, Lnhu;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 431
    :cond_1
    :goto_1
    :pswitch_0
    return v0

    .line 410
    :cond_2
    iget-object v2, v3, Lnkd;->k:Lnjl;

    if-eqz v2, :cond_3

    .line 411
    iget-object v2, v3, Lnkd;->k:Lnjl;

    iget v2, v2, Lnjl;->b:I

    packed-switch v2, :pswitch_data_2

    .line 425
    :cond_3
    iget-object v2, v3, Lnkd;->b:Lnjf;

    .line 426
    if-eqz v2, :cond_4

    iget-object v3, v2, Lnjf;->b:[Lnje;

    if-eqz v3, :cond_4

    iget-object v2, v2, Lnjf;->b:[Lnje;

    array-length v2, v2

    if-gtz v2, :cond_1

    :cond_4
    move v0, v1

    .line 431
    goto :goto_1

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 397
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 411
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    packed-switch p1, :pswitch_data_0

    .line 112
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 106
    :pswitch_0
    const v0, 0x7f0a031f

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 108
    :pswitch_1
    const v0, 0x7f0a0320

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 110
    :pswitch_2
    const v0, 0x7f0a0321

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-super {p0}, Ljnl;->a()V

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 101
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->G:Z

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    :cond_0
    return-void
.end method

.method public a(Lnip;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 117
    iget-object v0, p1, Lnip;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p1, Lnip;->b:I

    .line 118
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    .line 120
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->j:Z

    if-nez v4, :cond_0

    if-eqz v1, :cond_4

    :cond_0
    move v1, v2

    .line 121
    :goto_2
    if-eqz v1, :cond_6

    .line 122
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 123
    iget-boolean v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->j:Z

    if-eqz v1, :cond_5

    .line 124
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a:Landroid/view/ViewGroup;

    .line 125
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020416

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 124
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 126
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Landroid/view/View;)V

    .line 127
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a:Landroid/view/ViewGroup;

    new-instance v4, Ljmj;

    invoke-direct {v4, p0}, Ljmj;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;)V

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->q:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 140
    :goto_3
    iget-object v1, p1, Lnip;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p1, Lnip;->b:I

    const/4 v4, 0x3

    if-ne v1, v4, :cond_1

    move v3, v2

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->G:Z

    .line 142
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->o:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Landroid/widget/TextView;Z)V

    .line 144
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->d(Landroid/widget/TextView;)V

    .line 148
    :goto_4
    return-void

    .line 118
    :cond_2
    iget-object v0, p1, Lnip;->c:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v1, v3

    .line 119
    goto :goto_1

    :cond_4
    move v1, v3

    .line 120
    goto :goto_2

    .line 138
    :cond_5
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->q:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 146
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_4
.end method

.method public a(Lnjt;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 345
    iput-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->G:Z

    .line 352
    if-eqz p1, :cond_4

    .line 353
    iget-object v5, p1, Lnjt;->e:Lnkd;

    .line 354
    if-eqz v5, :cond_4

    .line 355
    iget-object v4, v5, Lnkd;->d:Lnip;

    .line 356
    iget-object v0, v5, Lnkd;->l:Lnjk;

    .line 357
    if-eqz v0, :cond_3

    .line 358
    iget-object v0, v0, Lnjk;->b:[Lnjj;

    .line 360
    :goto_0
    iget-object v2, v5, Lnkd;->i:Lnhu;

    if-eqz v2, :cond_2

    .line 361
    iget-object v2, v5, Lnkd;->i:Lnhu;

    iget-object v2, v2, Lnhu;->a:Ljava/lang/String;

    .line 363
    :goto_1
    iget-object v6, v5, Lnkd;->k:Lnjl;

    .line 364
    if-eqz v6, :cond_0

    .line 365
    iget v3, v6, Lnjl;->b:I

    .line 367
    :cond_0
    iget-object v5, v5, Lnkd;->b:Lnjf;

    .line 368
    if-eqz v5, :cond_1

    .line 369
    iget-object v1, v5, Lnjf;->b:[Lnje;

    .line 374
    :cond_1
    :goto_2
    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Lnip;)V

    .line 375
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a([Lnjj;)V

    .line 376
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->b(Ljava/lang/String;)V

    .line 377
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->d(I)V

    .line 378
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a([Lnje;)V

    .line 379
    return-void

    :cond_2
    move-object v2, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v0, v1

    move-object v2, v1

    move-object v4, v1

    goto :goto_2
.end method

.method public a([Lnje;)V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 301
    if-eqz p1, :cond_2

    array-length v0, p1

    if-lez v0, :cond_2

    move v0, v1

    .line 302
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->j:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_3

    :cond_0
    move v3, v1

    .line 303
    :goto_1
    if-eqz v3, :cond_7

    .line 304
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->C:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 305
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->j:Z

    if-eqz v3, :cond_4

    .line 306
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->C:Landroid/view/ViewGroup;

    .line 307
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020416

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 306
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 308
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->C:Landroid/view/ViewGroup;

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Landroid/view/View;)V

    .line 309
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->C:Landroid/view/ViewGroup;

    new-instance v4, Ljmm;

    invoke-direct {v4, p0}, Ljmm;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 318
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->F:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 322
    :goto_2
    if-eqz v0, :cond_6

    .line 323
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 324
    array-length v3, p1

    :goto_3
    if-ge v2, v3, :cond_5

    aget-object v4, p1, v2

    .line 325
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 326
    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    :cond_1
    iget-object v4, v4, Lnje;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_2
    move v0, v2

    .line 301
    goto :goto_0

    :cond_3
    move v3, v2

    .line 302
    goto :goto_1

    .line 320
    :cond_4
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->F:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 330
    :cond_5
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->E:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->D:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Landroid/widget/TextView;Z)V

    .line 332
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->E:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->d(Landroid/widget/TextView;)V

    .line 341
    :goto_4
    return-void

    .line 334
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->E:Landroid/widget/TextView;

    const v2, 0x7f0a0339

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->D:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->b(Landroid/widget/TextView;Z)V

    .line 336
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->E:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(Landroid/widget/TextView;)V

    goto :goto_4

    .line 339
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_4
.end method

.method public a([Lnjj;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 173
    if-eqz p1, :cond_4

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    array-length v4, p1

    move v0, v3

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, p1, v0

    .line 176
    iget v5, v5, Lnjj;->b:I

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->b(I)Ljava/lang/String;

    move-result-object v5

    .line 177
    if-eqz v5, :cond_1

    .line 178
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 181
    const-string v6, ", "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    :cond_0
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    .line 190
    :goto_2
    iget-boolean v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->j:Z

    if-nez v4, :cond_3

    if-eqz v1, :cond_6

    :cond_3
    move v4, v2

    .line 191
    :goto_3
    if-eqz v4, :cond_9

    .line 192
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 193
    iget-boolean v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->j:Z

    if-eqz v4, :cond_7

    .line 194
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->r:Landroid/view/ViewGroup;

    .line 195
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020416

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 194
    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 196
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->r:Landroid/view/ViewGroup;

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Landroid/view/View;)V

    .line 197
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->r:Landroid/view/ViewGroup;

    new-instance v5, Ljmk;

    invoke-direct {v5, p0}, Ljmk;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->u:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 210
    :goto_4
    if-eqz v1, :cond_8

    .line 211
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->s:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Landroid/widget/TextView;Z)V

    .line 213
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->t:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->d(Landroid/widget/TextView;)V

    .line 222
    :goto_5
    return-void

    .line 187
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move v1, v3

    .line 189
    goto :goto_2

    :cond_6
    move v4, v3

    .line 190
    goto :goto_3

    .line 208
    :cond_7
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->u:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 215
    :cond_8
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->t:Landroid/widget/TextView;

    const v1, 0x7f0a0324

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->s:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->b(Landroid/widget/TextView;Z)V

    .line 217
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->t:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(Landroid/widget/TextView;)V

    goto :goto_5

    .line 220
    :cond_9
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_5
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    packed-switch p1, :pswitch_data_0

    .line 167
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 159
    :pswitch_0
    const v0, 0x7f0a0325

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 161
    :pswitch_1
    const v0, 0x7f0a0326

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 163
    :pswitch_2
    const v0, 0x7f0a0327

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 165
    :pswitch_3
    const v0, 0x7f0a0328

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 225
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 226
    :goto_0
    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->w:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Landroid/widget/TextView;Z)V

    .line 230
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->x:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->d(Landroid/widget/TextView;)V

    .line 236
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 225
    goto :goto_0

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->v:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method public c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    packed-switch p1, :pswitch_data_0

    .line 259
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 241
    :pswitch_0
    const v0, 0x7f0a032e

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 243
    :pswitch_1
    const v0, 0x7f0a032f

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 245
    :pswitch_2
    const v0, 0x7f0a0330

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 247
    :pswitch_3
    const v0, 0x7f0a0331

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 249
    :pswitch_4
    const v0, 0x7f0a0332

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 251
    :pswitch_5
    const v0, 0x7f0a0333

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 253
    :pswitch_6
    const v0, 0x7f0a0334

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 255
    :pswitch_7
    const v0, 0x7f0a0335

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 257
    :pswitch_8
    const v0, 0x7f0a0336

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public d(I)V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 264
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->c(I)Ljava/lang/String;

    move-result-object v4

    .line 265
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 266
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->j:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    move v3, v1

    .line 267
    :goto_1
    if-eqz v3, :cond_5

    .line 268
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 269
    iget-boolean v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->j:Z

    if-eqz v3, :cond_3

    .line 270
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->y:Landroid/view/ViewGroup;

    .line 271
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020416

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 270
    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 272
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Landroid/view/View;)V

    .line 273
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->y:Landroid/view/ViewGroup;

    new-instance v5, Ljml;

    invoke-direct {v5, p0}, Ljml;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;)V

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->B:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 286
    :goto_2
    if-eqz v0, :cond_4

    .line 287
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->z:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Landroid/widget/TextView;Z)V

    .line 289
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->A:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->d(Landroid/widget/TextView;)V

    .line 298
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 265
    goto :goto_0

    :cond_2
    move v3, v2

    .line 266
    goto :goto_1

    .line 284
    :cond_3
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->B:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 291
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->A:Landroid/widget/TextView;

    const v2, 0x7f0a032c

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->z:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->b(Landroid/widget/TextView;Z)V

    .line 293
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->A:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->e(Landroid/widget/TextView;)V

    goto :goto_3

    .line 296
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_3
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 69
    const v0, 0x7f1003c1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a:Landroid/view/ViewGroup;

    .line 70
    const v0, 0x7f1003c3

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->o:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f1003c4

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->p:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f1003c2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->q:Landroid/view/View;

    .line 73
    const v0, 0x7f1003c5

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->r:Landroid/view/ViewGroup;

    .line 74
    const v0, 0x7f1003c7

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->s:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f1003c8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->t:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f1003c6

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->u:Landroid/view/View;

    .line 77
    const v0, 0x7f1003c9

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->v:Landroid/view/ViewGroup;

    .line 78
    const v0, 0x7f1003ca

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->w:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f1003cb

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->x:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f1003cc

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->y:Landroid/view/ViewGroup;

    .line 81
    const v0, 0x7f1003ce

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->z:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f1003cf

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->A:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f1003cd

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->B:Landroid/view/View;

    .line 84
    const v0, 0x7f1003d0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->C:Landroid/view/ViewGroup;

    .line 85
    const v0, 0x7f1003d2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->D:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f1003d3

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->E:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f1003d1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->F:Landroid/view/View;

    .line 88
    return-void
.end method
