.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;
.super Landroid/widget/LinearLayout;
.source "PG"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static d:I


# instance fields
.field private e:I

.field private f:I

.field private g:[F

.field private h:[Landroid/view/View;

.field private i:[Landroid/view/View;

.field private j:[Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 18
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->a:[I

    .line 20
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->b:[I

    .line 22
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->c:[I

    return-void

    .line 18
    nop

    :array_0
    .array-data 4
        0x7f100423
        0x7f100426
        0x7f100429
        0x7f10042c
        0x7f10042f
    .end array-data

    .line 20
    :array_1
    .array-data 4
        0x7f100425
        0x7f100428
        0x7f10042b
        0x7f10042e
        0x7f100431
    .end array-data

    .line 22
    :array_2
    .array-data 4
        0x7f100424
        0x7f100427
        0x7f10042a
        0x7f10042d
        0x7f100430
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->g:[F

    .line 29
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->h:[Landroid/view/View;

    .line 30
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->i:[Landroid/view/View;

    .line 31
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->j:[Landroid/view/View;

    .line 47
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->d:I

    if-nez v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 49
    const v1, 0x7f0d0133

    .line 50
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->d:I

    .line 35
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->g:[F

    .line 29
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->h:[Landroid/view/View;

    .line 30
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->i:[Landroid/view/View;

    .line 31
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->j:[Landroid/view/View;

    .line 47
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->d:I

    if-nez v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 49
    const v1, 0x7f0d0133

    .line 50
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->d:I

    .line 39
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->g:[F

    .line 29
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->h:[Landroid/view/View;

    .line 30
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->i:[Landroid/view/View;

    .line 31
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->j:[Landroid/view/View;

    .line 47
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->d:I

    if-nez v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 49
    const v1, 0x7f0d0133

    .line 50
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->d:I

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method public a(IIIII)V
    .locals 4

    .prologue
    .line 70
    .line 71
    invoke-static {p4, p5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 70
    invoke-static {p3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    .line 72
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->g:[F

    const/4 v2, 0x0

    int-to-float v3, p1

    div-float/2addr v3, v0

    aput v3, v1, v2

    .line 73
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->g:[F

    const/4 v2, 0x1

    int-to-float v3, p2

    div-float/2addr v3, v0

    aput v3, v1, v2

    .line 74
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->g:[F

    const/4 v2, 0x2

    int-to-float v3, p3

    div-float/2addr v3, v0

    aput v3, v1, v2

    .line 75
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->g:[F

    const/4 v2, 0x3

    int-to-float v3, p4

    div-float/2addr v3, v0

    aput v3, v1, v2

    .line 76
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->g:[F

    const/4 v2, 0x4

    int-to-float v3, p5

    div-float v0, v3, v0

    aput v0, v1, v2

    .line 77
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->requestLayout()V

    .line 78
    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x0

    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    move v1, v0

    .line 57
    :goto_0
    if-ge v1, v4, :cond_0

    .line 58
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->h:[Landroid/view/View;

    sget-object v3, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->a:[I

    aget v3, v3, v1

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v1

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 60
    :goto_1
    if-ge v1, v4, :cond_1

    .line 61
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->j:[Landroid/view/View;

    sget-object v3, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->b:[I

    aget v3, v3, v1

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v1

    .line 60
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 63
    :cond_1
    :goto_2
    if-ge v0, v4, :cond_2

    .line 64
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->i:[Landroid/view/View;

    sget-object v2, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->c:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v0

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 66
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v0, 0x0

    .line 88
    iget v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->f:I

    if-nez v1, :cond_1

    .line 89
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    move v1, v0

    .line 90
    :goto_0
    if-ge v1, v6, :cond_0

    .line 91
    iget v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->f:I

    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->h:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->f:I

    .line 90
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 93
    :goto_1
    if-ge v1, v6, :cond_1

    .line 94
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->i:[Landroid/view/View;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->f:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 93
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 98
    :cond_1
    if-eqz p1, :cond_3

    .line 99
    const v1, 0x7fffffff

    invoke-static {v1, p1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->resolveSize(II)I

    move-result v1

    .line 100
    iget v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->e:I

    if-eq v2, v1, :cond_2

    .line 101
    iget v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->f:I

    sub-int v2, v1, v2

    sget v3, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->d:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->e:I

    .line 102
    :goto_2
    if-ge v0, v6, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->j:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->e:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryHistogramView;->g:[F

    aget v4, v4, v0

    mul-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 105
    :cond_2
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 110
    :goto_3
    return-void

    .line 108
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_3
.end method
