.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;
.super Ljnl;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Ljmi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public static b(Lnjt;)Z
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lnjt;->g:Ljava/lang/String;

    .line 101
    invoke-static {p0}, Ljnu;->d(Lnjt;)Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-static {p0}, Ljnu;->b(Lnjt;)Ljava/lang/String;

    move-result-object v2

    .line 104
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljmi;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->a:Ljmi;

    .line 85
    return-void
.end method

.method public a(Lnjt;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 49
    iget-object v0, p1, Lnjt;->g:Ljava/lang/String;

    .line 51
    invoke-static {p1}, Ljnu;->d(Lnjt;)Ljava/lang/String;

    move-result-object v1

    .line 52
    if-eqz v1, :cond_1

    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://maps.google.com/maps?cid="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v0, :cond_0

    const-string v2, "&q="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 55
    const v2, 0x7f100543

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 56
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 57
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    :cond_1
    invoke-static {p1}, Ljnu;->b(Lnjt;)Ljava/lang/String;

    move-result-object v1

    .line 61
    if-eqz v1, :cond_3

    .line 62
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://maps.google.com/maps?daddr="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_2

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    const v1, 0x7f100545

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 65
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 66
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    :cond_3
    invoke-static {p1}, Ljnu;->c(Lnjt;)Ljava/lang/String;

    move-result-object v0

    .line 70
    const v1, 0x7f100546

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 71
    const v2, 0x7f100547

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 72
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 73
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 74
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 75
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 76
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    :goto_0
    return-void

    .line 78
    :cond_4
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 79
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 89
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 90
    const v1, 0x7f100543

    if-ne v0, v1, :cond_1

    .line 91
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->a:Ljmi;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljmi;->k(Ljava/lang/String;)V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    const v1, 0x7f100545

    if-ne v0, v1, :cond_2

    .line 93
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->a:Ljmi;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljmi;->j(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    const v1, 0x7f100547

    if-ne v0, v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->a:Ljmi;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljmi;->i(Ljava/lang/String;)V

    goto :goto_0
.end method
