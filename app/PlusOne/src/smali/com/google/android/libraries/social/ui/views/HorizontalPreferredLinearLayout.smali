.class public Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private a:Lljc;

.field private b:Z

.field private c:Z

.field private d:I

.field private e:[I

.field private f:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method private a([I)I
    .locals 3

    .prologue
    .line 61
    const/high16 v1, -0x80000000

    .line 62
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 63
    aget v2, p1, v0

    if-le v2, v1, :cond_0

    .line 64
    aget v1, p1, v0

    .line 62
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_1
    return v1
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->d:I

    .line 54
    return-void
.end method

.method public a(Lljc;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a:Lljc;

    .line 50
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->c:Z

    .line 58
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 11

    .prologue
    .line 150
    sub-int v6, p4, p2

    .line 153
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingLeft()I

    move-result v3

    .line 154
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingTop()I

    move-result v2

    .line 155
    const/4 v1, 0x0

    .line 156
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getChildCount()I

    move-result v7

    .line 157
    const/4 v0, 0x0

    move v5, v0

    move v0, v3

    :goto_0
    if-ge v5, v7, :cond_3

    .line 158
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 159
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_4

    .line 160
    if-eqz v1, :cond_0

    .line 161
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->d:I

    add-int/2addr v0, v3

    .line 163
    :cond_0
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 164
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 165
    iget-boolean v4, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->b:Z

    if-eqz v4, :cond_1

    .line 167
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingTop()I

    move-result v2

    .line 168
    const/4 v1, 0x1

    move v10, v1

    move v1, v2

    move v2, v0

    move v0, v10

    .line 177
    :goto_1
    add-int v4, v2, v3

    .line 178
    add-int v3, v1, v9

    .line 179
    invoke-virtual {v8, v2, v1, v4, v3}, Landroid/view/View;->layout(IIII)V

    move v2, v3

    move v1, v4

    .line 157
    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v10, v0

    move v0, v1

    move v1, v10

    goto :goto_0

    .line 170
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v0, v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getLayoutDirection()I

    move-result v0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_2

    .line 171
    sub-int v0, v6, v3

    :goto_3
    move v10, v1

    move v1, v2

    move v2, v0

    move v0, v10

    .line 175
    goto :goto_1

    .line 173
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingLeft()I

    move-result v0

    goto :goto_3

    .line 182
    :cond_3
    return-void

    :cond_4
    move v10, v1

    move v1, v0

    move v0, v10

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x0

    .line 80
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 83
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getChildCount()I

    move-result v5

    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->e:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->e:[I

    array-length v0, v0

    if-ge v0, v5, :cond_1

    .line 86
    :cond_0
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->e:[I

    .line 87
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->f:[I

    :cond_1
    move v3, v1

    move v2, v1

    .line 90
    :goto_0
    if-ge v3, v5, :cond_4

    .line 91
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-eq v6, v8, :cond_2

    .line 93
    invoke-virtual {v0, v4, v4}, Landroid/view/View;->measure(II)V

    .line 94
    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->e:[I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    aput v7, v6, v3

    .line 95
    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->f:[I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    aput v0, v6, v3

    .line 101
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->e:[I

    aget v0, v0, v3

    if-lez v0, :cond_d

    .line 102
    if-lez v2, :cond_3

    iget v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->d:I

    :goto_2
    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->e:[I

    aget v6, v6, v3

    add-int/2addr v0, v6

    add-int/2addr v0, v2

    .line 90
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->e:[I

    aput v1, v0, v3

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->f:[I

    aput v1, v0, v3

    goto :goto_1

    :cond_3
    move v0, v1

    .line 102
    goto :goto_2

    .line 106
    :cond_4
    if-lez v2, :cond_5

    .line 107
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingRight()I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 110
    :cond_5
    invoke-static {v2, p1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->resolveSize(II)I

    move-result v0

    .line 111
    iget-boolean v3, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->c:Z

    if-nez v3, :cond_7

    if-ne v2, v0, :cond_7

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->b:Z

    .line 115
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->b:Z

    if-eqz v0, :cond_8

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a:Lljc;

    if-eqz v0, :cond_6

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a:Lljc;

    invoke-interface {v0}, Lljc;->a()V

    .line 121
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->f:[I

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a([I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :goto_5
    invoke-virtual {p0, v2, v0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->setMeasuredDimension(II)V

    .line 146
    return-void

    :cond_7
    move v0, v1

    .line 111
    goto :goto_4

    .line 124
    :cond_8
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a:Lljc;

    if-eqz v0, :cond_9

    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a:Lljc;

    invoke-interface {v0}, Lljc;->b()V

    .line 128
    :cond_9
    const v0, 0x7fffffff

    invoke-static {v0, p1}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->resolveSize(II)I

    move-result v0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingRight()I

    move-result v2

    sub-int v2, v0, v2

    move v0, v1

    .line 131
    :goto_6
    if-ge v0, v5, :cond_b

    .line 132
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 133
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-eq v6, v8, :cond_a

    .line 134
    const/high16 v6, -0x80000000

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v3, v6, v4}, Landroid/view/View;->measure(II)V

    .line 136
    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->e:[I

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    aput v7, v6, v0

    .line 137
    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->f:[I

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    aput v3, v6, v0

    .line 131
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 141
    :cond_b
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->e:[I

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->a([I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingLeft()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, v0

    .line 142
    iget-object v3, p0, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->f:[I

    move v0, v1

    :goto_7
    array-length v4, v3

    if-ge v1, v4, :cond_c

    aget v4, v3, v1

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/HorizontalPreferredLinearLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_5

    :cond_d
    move v0, v2

    goto/16 :goto_3
.end method
