.class public Lcom/google/android/libraries/social/media/MediaResource;
.super Lcom/google/android/libraries/social/resources/images/ImageResource;
.source "PG"

# interfaces
.implements Ljvt;


# static fields
.field private static final DESIRED_QUALITY:[I

.field private static final SIZE_A_HIGH_RES:Lizz;

.field private static final SIZE_A_PREVIEW:Lizz;

.field private static final SIZE_A_SIZES_AND_SCALES:[Lizz;

.field private static final SIZE_A_STANDARD_RES:Lizz;

.field private static final SIZE_A_STANDARD_THEN_HIGH_RES:Lizz;

.field private static final SIZE_B_HIGH_RES:Lizz;

.field private static final SIZE_B_PREVIEW:Lizz;

.field private static final SIZE_B_SIZES_AND_SCALES:[Lizz;

.field private static final SIZE_B_STANDARD_RES:Lizz;

.field private static final SIZE_B_STANDARD_THEN_HIGH_RES:Lizz;

.field private static final SIZE_C_HIGH_RES:Lizz;

.field private static final SIZE_C_PREVIEW:Lizz;

.field private static final SIZE_C_SIZES_AND_SCALES:[Lizz;

.field private static final SIZE_C_STANDARD_RES:Lizz;

.field private static final SIZE_C_STANDARD_THEN_HIGH_RES:Lizz;

.field private static final SIZE_LARGE:Lizz;

.field private static mDefaultLargeSize:I

.field private static mDefaultThumbnailSize:I

.field private static sAutoSizeInitialized:Z

.field private static sCustomDimensionSlop:I

.field private static sDownscaleStandardImages:Z

.field private static sSizeCategoryAPixels:I

.field private static sSizeCategoryBPixels:I

.field private static sSizeCategoryCPixels:I

.field private static sStandardScaleMultiplier:I


# instance fields
.field private downloadQualityLevel:I

.field private downloadQualityType:I

.field private fifeQualityModel:Lizx;

.field private mCachedFile:Ljava/io/File;

.field private mCachedFileHeight:I

.field private mCachedFileQualityLevel:I

.field private mCachedFileQualityType:I

.field private mCachedFileWidth:I

.field private mCachedSizeAndScale:Lizz;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mDownloadSizeAndScale:Lizz;

.field private mLocalRawFile:Ljava/io/File;

.field private mMimeType:Ljava/lang/String;

.field private mShortFileName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x6

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x3

    .line 458
    new-instance v0, Lizz;

    const/4 v1, 0x0

    invoke-direct {v0, v4, v1}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_LARGE:Lizz;

    .line 460
    new-instance v0, Lizz;

    invoke-direct {v0, v7, v4}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_HIGH_RES:Lizz;

    .line 462
    new-instance v0, Lizz;

    invoke-direct {v0, v7, v6}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_RES:Lizz;

    .line 464
    new-instance v0, Lizz;

    invoke-direct {v0, v7, v5}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_PREVIEW:Lizz;

    .line 466
    new-instance v0, Lizz;

    invoke-direct {v0, v8, v4}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_HIGH_RES:Lizz;

    .line 468
    new-instance v0, Lizz;

    invoke-direct {v0, v8, v6}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_RES:Lizz;

    .line 470
    new-instance v0, Lizz;

    invoke-direct {v0, v8, v5}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_PREVIEW:Lizz;

    .line 472
    new-instance v0, Lizz;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v4}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_HIGH_RES:Lizz;

    .line 474
    new-instance v0, Lizz;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v6}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_STANDARD_RES:Lizz;

    .line 476
    new-instance v0, Lizz;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v5}, Lizz;-><init>(II)V

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_PREVIEW:Lizz;

    .line 479
    sget-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_RES:Lizz;

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_HIGH_RES:Lizz;

    .line 480
    invoke-virtual {v0, v1}, Lizz;->a(Lizz;)Lizz;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_THEN_HIGH_RES:Lizz;

    .line 481
    sget-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_RES:Lizz;

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_HIGH_RES:Lizz;

    .line 482
    invoke-virtual {v0, v1}, Lizz;->a(Lizz;)Lizz;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_THEN_HIGH_RES:Lizz;

    .line 483
    sget-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_STANDARD_RES:Lizz;

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_HIGH_RES:Lizz;

    .line 484
    invoke-virtual {v0, v1}, Lizz;->a(Lizz;)Lizz;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_STANDARD_THEN_HIGH_RES:Lizz;

    .line 493
    const/16 v0, 0x8

    new-array v0, v0, [Lizz;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_HIGH_RES:Lizz;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_LARGE:Lizz;

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_HIGH_RES:Lizz;

    .line 495
    invoke-virtual {v1, v2}, Lizz;->a(Lizz;)Lizz;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_THEN_HIGH_RES:Lizz;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_HIGH_RES:Lizz;

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_HIGH_RES:Lizz;

    .line 497
    invoke-virtual {v1, v2}, Lizz;->a(Lizz;)Lizz;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_RES:Lizz;

    sget-object v3, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_RES:Lizz;

    .line 498
    invoke-virtual {v2, v3}, Lizz;->a(Lizz;)Lizz;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_PREVIEW:Lizz;

    sget-object v3, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_RES:Lizz;

    .line 499
    invoke-virtual {v2, v3}, Lizz;->a(Lizz;)Lizz;

    move-result-object v2

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_PREVIEW:Lizz;

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_RES:Lizz;

    .line 500
    invoke-virtual {v1, v2}, Lizz;->a(Lizz;)Lizz;

    move-result-object v1

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_HIGH_RES:Lizz;

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_RES:Lizz;

    .line 501
    invoke-virtual {v1, v2}, Lizz;->a(Lizz;)Lizz;

    move-result-object v1

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_SIZES_AND_SCALES:[Lizz;

    .line 504
    const/16 v0, 0x8

    new-array v0, v0, [Lizz;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_LARGE:Lizz;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_HIGH_RES:Lizz;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_THEN_HIGH_RES:Lizz;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_HIGH_RES:Lizz;

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_RES:Lizz;

    .line 508
    invoke-virtual {v1, v2}, Lizz;->a(Lizz;)Lizz;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_RES:Lizz;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_STANDARD_RES:Lizz;

    sget-object v3, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_RES:Lizz;

    .line 510
    invoke-virtual {v2, v3}, Lizz;->a(Lizz;)Lizz;

    move-result-object v2

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_PREVIEW:Lizz;

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_RES:Lizz;

    .line 511
    invoke-virtual {v1, v2}, Lizz;->a(Lizz;)Lizz;

    move-result-object v1

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_PREVIEW:Lizz;

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_RES:Lizz;

    .line 512
    invoke-virtual {v1, v2}, Lizz;->a(Lizz;)Lizz;

    move-result-object v1

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_SIZES_AND_SCALES:[Lizz;

    .line 515
    new-array v0, v8, [Lizz;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_LARGE:Lizz;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_HIGH_RES:Lizz;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_STANDARD_THEN_HIGH_RES:Lizz;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_RES:Lizz;

    aput-object v1, v0, v4

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_PREVIEW:Lizz;

    sget-object v3, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_STANDARD_RES:Lizz;

    .line 520
    invoke-virtual {v2, v3}, Lizz;->a(Lizz;)Lizz;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_PREVIEW:Lizz;

    sget-object v3, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_STANDARD_RES:Lizz;

    .line 521
    invoke-virtual {v2, v3}, Lizz;->a(Lizz;)Lizz;

    move-result-object v2

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_PREVIEW:Lizz;

    sget-object v2, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_STANDARD_RES:Lizz;

    .line 522
    invoke-virtual {v1, v2}, Lizz;->a(Lizz;)Lizz;

    move-result-object v1

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_SIZES_AND_SCALES:[Lizz;

    .line 525
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/libraries/social/media/MediaResource;->DESIRED_QUALITY:[I

    return-void

    :array_0
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data
.end method

.method public constructor <init>(Lkdv;Lizy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 561
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/resources/images/ImageResource;-><init>(Lkdv;Lkds;)V

    .line 553
    iput v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityType:I

    .line 554
    iput v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityLevel:I

    .line 555
    iput v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileWidth:I

    .line 556
    iput v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileHeight:I

    .line 557
    iput v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityType:I

    .line 558
    iput v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    .line 562
    sget-boolean v0, Lcom/google/android/libraries/social/media/MediaResource;->sAutoSizeInitialized:Z

    if-nez v0, :cond_0

    .line 563
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/media/MediaResource;->initializeAutoSizeSupport(Lkdv;)V

    .line 565
    invoke-interface {p1}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 566
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 567
    div-int/lit8 v0, v0, 0x4

    sput v0, Lcom/google/android/libraries/social/media/MediaResource;->sCustomDimensionSlop:I

    .line 569
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/libraries/social/media/MediaResource;->sAutoSizeInitialized:Z

    .line 572
    :cond_0
    iget v0, p2, Lizy;->c:I

    if-eq v0, v2, :cond_1

    .line 573
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizx;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizx;

    iput-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->fifeQualityModel:Lizx;

    .line 574
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->fifeQualityModel:Lizx;

    iget v1, p2, Lizy;->c:I

    invoke-interface {v0, v1}, Lizx;->a(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityType:I

    .line 575
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->fifeQualityModel:Lizx;

    iget v1, p2, Lizy;->c:I

    invoke-interface {v0, v1}, Lizx;->b(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityLevel:I

    .line 577
    :cond_1
    return-void
.end method

.method public static synthetic access$000()I
    .locals 1

    .prologue
    .line 57
    sget v0, Lcom/google/android/libraries/social/media/MediaResource;->sSizeCategoryBPixels:I

    return v0
.end method

.method public static synthetic access$100()I
    .locals 1

    .prologue
    .line 57
    sget v0, Lcom/google/android/libraries/social/media/MediaResource;->sSizeCategoryCPixels:I

    return v0
.end method

.method private appendCustomWithQualityDirName(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 2

    .prologue
    .line 698
    invoke-static {p2}, Llsf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2b

    .line 699
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 700
    return-object p1
.end method

.method private doUpgradeIfNecessary(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1171
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getResourceConsumerCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1200
    :cond_0
    :goto_0
    return-void

    .line 1175
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1176
    invoke-virtual {v0}, Lizy;->a()I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-nez v1, :cond_0

    .line 1180
    iget v1, v0, Lizy;->b:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 1189
    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->isOnFastAndFreeNetwork()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0, p1}, Lcom/google/android/libraries/social/media/MediaResource;->isCustomCachedFileUpgradeNeeded(Lizy;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1190
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->downloadResource()V

    goto :goto_0

    .line 1182
    :sswitch_1
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/media/MediaResource;->isAutomaticCacheFileUpgradeNeeded(Lizy;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1184
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->downloadResource()V

    goto :goto_0

    .line 1180
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method private findAutoSizedCachedFile()V
    .locals 6

    .prologue
    .line 986
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    invoke-virtual {v0}, Lizy;->b()I

    move-result v1

    .line 987
    const/4 v0, 0x0

    .line 988
    packed-switch v1, :pswitch_data_0

    .line 1000
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileName()Ljava/lang/String;

    move-result-object v4

    .line 1001
    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 1002
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aget-object v2, v0, v1

    iget-object v2, v2, Lizz;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1003
    :goto_2
    iget-object v3, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v3}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 1004
    if-nez v3, :cond_3

    .line 1005
    iget-object v3, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v3}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1007
    :goto_3
    if-eqz v2, :cond_2

    .line 1008
    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedSizeAndScale:Lizz;

    .line 1009
    iput-object v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFile:Ljava/io/File;

    .line 1013
    :cond_0
    return-void

    .line 990
    :pswitch_0
    sget-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_SIZES_AND_SCALES:[Lizz;

    goto :goto_0

    .line 993
    :pswitch_1
    sget-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_SIZES_AND_SCALES:[Lizz;

    goto :goto_0

    .line 996
    :pswitch_2
    sget-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_SIZES_AND_SCALES:[Lizz;

    goto :goto_0

    .line 1002
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 1001
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move-object v2, v3

    goto :goto_3

    .line 988
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private findCustomSizedCachedFile()V
    .locals 21

    .prologue
    .line 790
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v2, Lizy;

    .line 791
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v3}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v3

    .line 794
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFile:Ljava/io/File;

    .line 795
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileWidth:I

    .line 796
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileHeight:I

    .line 797
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityType:I

    .line 798
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    .line 807
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v4

    .line 808
    iget-object v5, v2, Lizy;->a:Lizu;

    invoke-virtual {v5}, Lizu;->d()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/libraries/social/media/MediaResource;->appendCustomWithQualityDirName(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 809
    invoke-static {v4}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v4

    .line 811
    new-instance v8, Ljava/io/File;

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v8, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 812
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 939
    :cond_0
    :goto_0
    return-void

    .line 817
    :cond_1
    iget v3, v2, Lizy;->d:I

    if-eqz v3, :cond_2

    iget v3, v2, Lizy;->e:I

    if-nez v3, :cond_4

    :cond_2
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    .line 818
    :goto_1
    const-wide v6, 0x3fb999999999999aL    # 0.1

    sub-double v10, v4, v6

    .line 819
    const-wide v6, 0x3fb999999999999aL    # 0.1

    add-double v12, v4, v6

    .line 821
    invoke-virtual {v2}, Lizy;->a()I

    move-result v3

    .line 822
    and-int/lit8 v9, v3, -0x21

    .line 824
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    .line 825
    invoke-static {v3, v9}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileNameAppendFlags(Ljava/lang/StringBuilder;I)V

    .line 826
    invoke-static {v3}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v14

    .line 828
    const/4 v4, 0x0

    .line 830
    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v15

    .line 831
    if-eqz v15, :cond_0

    array-length v3, v15

    if-eqz v3, :cond_0

    .line 835
    const/4 v3, 0x0

    move v5, v3

    move-object v3, v4

    :goto_2
    array-length v4, v15

    if-ge v5, v4, :cond_f

    .line 836
    aget-object v4, v15, v5

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 838
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 839
    array-length v6, v7

    const/16 v16, 0x4

    move/from16 v0, v16

    if-ge v6, v0, :cond_5

    .line 841
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v8, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 835
    :cond_3
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 817
    :cond_4
    iget v3, v2, Lizy;->d:I

    iget v4, v2, Lizy;->e:I

    div-int/2addr v3, v4

    int-to-double v4, v3

    goto :goto_1

    .line 845
    :cond_5
    array-length v6, v7

    const/16 v16, 0x6

    move/from16 v0, v16

    if-ge v6, v0, :cond_3

    .line 847
    if-eqz v9, :cond_6

    .line 852
    array-length v6, v7

    const/16 v16, 0x5

    move/from16 v0, v16

    if-lt v6, v0, :cond_c

    const/4 v6, 0x4

    aget-object v6, v7, v6

    .line 853
    :goto_4
    invoke-virtual {v14, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 854
    :cond_6
    const/4 v6, 0x0

    :try_start_0
    aget-object v6, v7, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 864
    const/4 v6, 0x1

    aget-object v6, v7, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 865
    const/4 v6, 0x2

    aget-object v6, v7, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    .line 866
    const/4 v6, 0x3

    aget-object v6, v7, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v19

    .line 873
    if-eqz v16, :cond_7

    if-nez v17, :cond_d

    :cond_7
    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 874
    :goto_5
    cmpg-double v20, v6, v10

    if-ltz v20, :cond_3

    cmpl-double v6, v6, v12

    if-gtz v6, :cond_3

    .line 875
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityType:I

    move/from16 v0, v18

    if-ne v0, v6, :cond_3

    .line 881
    if-eqz v3, :cond_b

    .line 886
    if-nez v18, :cond_e

    .line 897
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    move/from16 v0, v19

    if-lt v6, v0, :cond_3

    .line 903
    :goto_6
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileWidth:I

    if-eqz v6, :cond_8

    if-eqz v16, :cond_8

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileWidth:I

    move/from16 v0, v16

    if-gt v6, v0, :cond_3

    .line 909
    :cond_8
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileHeight:I

    if-eqz v6, :cond_9

    if-eqz v17, :cond_9

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileHeight:I

    move/from16 v0, v17

    if-gt v6, v0, :cond_3

    .line 914
    :cond_9
    iget v6, v2, Lizy;->d:I

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileWidth:I

    if-eqz v6, :cond_a

    if-eqz v16, :cond_3

    .line 920
    :cond_a
    iget v6, v2, Lizy;->e:I

    if-eqz v6, :cond_b

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileHeight:I

    if-eqz v6, :cond_b

    if-eqz v17, :cond_3

    .line 923
    :cond_b
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileWidth:I

    .line 927
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileHeight:I

    .line 928
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityType:I

    .line 929
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    move-object v3, v4

    .line 930
    goto/16 :goto_3

    .line 852
    :cond_c
    const-string v6, ""

    goto/16 :goto_4

    .line 868
    :catch_0
    move-exception v4

    const-string v4, "ImageResource"

    const-string v6, "NumberFormatException parsing cached file\'s filename."

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 873
    :cond_d
    div-int v6, v16, v17

    int-to-double v6, v6

    goto :goto_5

    .line 902
    :cond_e
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    move/from16 v0, v19

    if-gt v6, v0, :cond_3

    goto :goto_6

    .line 934
    :cond_f
    if-eqz v3, :cond_0

    .line 938
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v8, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFile:Ljava/io/File;

    goto/16 :goto_0
.end method

.method private getAutomaticSize()I
    .locals 3

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1099
    iget v1, v0, Lizy;->d:I

    iget v2, v0, Lizy;->e:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1100
    if-nez v1, :cond_0

    .line 1102
    sget v1, Lcom/google/android/libraries/social/media/MediaResource;->sSizeCategoryAPixels:I

    .line 1105
    :cond_0
    invoke-virtual {v0}, Lizy;->a()I

    move-result v0

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_2

    .line 1127
    :cond_1
    :goto_0
    :pswitch_0
    return v1

    .line 1109
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    if-nez v0, :cond_3

    .line 1110
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getInitialDownloadSizeAndScale()Lizz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    .line 1113
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    iget v0, v0, Lizz;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1118
    :pswitch_1
    sget-boolean v0, Lcom/google/android/libraries/social/media/MediaResource;->sDownscaleStandardImages:Z

    if-eqz v0, :cond_1

    .line 1120
    sget v0, Lcom/google/android/libraries/social/media/MediaResource;->sStandardScaleMultiplier:I

    mul-int/2addr v0, v1

    div-int/lit16 v1, v0, 0x2000

    goto :goto_0

    .line 1113
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getCachedPhotoFile(Landroid/content/Context;Lizu;)Ljava/io/File;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 720
    const/4 v2, 0x1

    move-object v0, p1

    move v3, v1

    move v4, v1

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileName(Lizu;IIIILandroid/graphics/RectF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 723
    const-class v0, Lkdv;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    .line 724
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getCachedVideoFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 731
    const-class v0, Lkdv;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v7

    .line 732
    sget-object v0, Ljac;->b:Ljac;

    invoke-static {p0, p1, v0}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    .line 733
    const/4 v2, 0x4

    move v3, v1

    move v4, v1

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileName(Lizu;IIIILandroid/graphics/RectF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 735
    invoke-virtual {v7, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 736
    if-eqz v0, :cond_0

    .line 739
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method private getConnectivityManager()Landroid/net/ConnectivityManager;
    .locals 2

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 1064
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    .line 1065
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 1067
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method private getCustomWithQualityShortFileName(Lizu;IIIII)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x2c

    .line 705
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 706
    invoke-virtual {p1}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/media/MediaResource;->appendCustomWithQualityDirName(Ljava/lang/StringBuilder;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-char v2, Ljava/io/File;->separatorChar:C

    .line 707
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 708
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 709
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 710
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 711
    invoke-static {v0, p2}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileNameAppendFlags(Ljava/lang/StringBuilder;I)V

    .line 712
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 713
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDownsampledLocalBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;I)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 1515
    invoke-static {p1, p2}, Ljcq;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Point;

    move-result-object v1

    .line 1516
    iget v0, v1, Landroid/graphics/Point;->x:I

    div-int/2addr v0, p3

    iget v2, v1, Landroid/graphics/Point;->y:I

    div-int/2addr v2, p3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1518
    const/4 v0, 0x0

    .line 1519
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1520
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-interface {v0, v3, v1}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1523
    :cond_0
    invoke-static {p1, p2, v2, v0}, Ljcq;->a(Landroid/content/ContentResolver;Landroid/net/Uri;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1524
    if-eq v1, v0, :cond_1

    .line 1525
    iget-object v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v2, v0}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 1528
    :cond_1
    return-object v1
.end method

.method private getFifeOptions()I
    .locals 4

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1299
    const/4 v1, 0x6

    .line 1300
    invoke-virtual {v0}, Lizy;->a()I

    move-result v2

    .line 1301
    and-int/lit8 v3, v2, 0x4

    if-nez v3, :cond_0

    .line 1302
    const/16 v1, 0x16

    .line 1304
    :cond_0
    and-int/lit8 v3, v2, 0x4

    if-nez v3, :cond_1

    and-int/lit8 v3, v2, 0x20

    if-nez v3, :cond_1

    .line 1307
    or-int/lit8 v1, v1, 0x20

    .line 1310
    :cond_1
    and-int/lit16 v2, v2, 0x4000

    if-eqz v2, :cond_2

    .line 1311
    or-int/lit16 v1, v1, 0x80

    .line 1313
    :cond_2
    iget-object v0, v0, Lizy;->f:Landroid/graphics/RectF;

    if-eqz v0, :cond_3

    .line 1314
    or-int/lit16 v1, v1, 0x400

    .line 1316
    :cond_3
    return v1
.end method

.method public static getFileThumb(Landroid/content/Context;Ljava/io/File;)Ljava/io/File;
    .locals 4

    .prologue
    .line 193
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "-thumb"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getInitialDownloadSizeAndScale()Lizz;
    .locals 1

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    invoke-virtual {v0}, Lizy;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1093
    sget-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_C_STANDARD_THEN_HIGH_RES:Lizz;

    :goto_0
    return-object v0

    .line 1088
    :pswitch_0
    sget-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_A_STANDARD_THEN_HIGH_RES:Lizz;

    goto :goto_0

    .line 1090
    :pswitch_1
    sget-object v0, Lcom/google/android/libraries/social/media/MediaResource;->SIZE_B_STANDARD_THEN_HIGH_RES:Lizz;

    goto :goto_0

    .line 1086
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getLocalBitmap(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 1568
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1570
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/libraries/social/media/MediaResource;->getDownsampledLocalBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1572
    invoke-direct {p0, p3, v0}, Lcom/google/android/libraries/social/media/MediaResource;->getScaledLocalBitmap(ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1573
    if-eq v1, v0, :cond_0

    .line 1574
    iget-object v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v2, v0}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 1577
    :cond_0
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/libraries/social/media/MediaResource;->getRotatedBitmap(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1578
    if-eq v0, v1, :cond_1

    .line 1579
    iget-object v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v2, v1}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 1582
    :cond_1
    return-object v0
.end method

.method private getLocalUri()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 1320
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1321
    iget-object v0, v0, Lizy;->a:Lizu;

    .line 1322
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1323
    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v1

    .line 1324
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 1327
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    .line 1328
    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lizn;

    invoke-static {v0, v3}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizn;

    .line 1329
    if-eqz v0, :cond_0

    .line 1330
    invoke-interface {v0, v1}, Lizn;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1333
    :goto_0
    if-eqz v2, :cond_1

    const-string v3, "http"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    if-nez v0, :cond_1

    move-object v0, v1

    .line 1338
    :goto_1
    return-object v0

    .line 1330
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1338
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getMediaStoreBitmap(Landroid/content/Context;Landroid/net/Uri;IIZ)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 1589
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1590
    iget v1, v0, Lizy;->b:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    if-eqz p5, :cond_1

    .line 1591
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/MediaResource;->getMediaStoreThumb(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    .line 1592
    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/media/MediaResource;->getRotatedAndCroppedBitmap(Landroid/graphics/Bitmap;Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1600
    :goto_0
    return-object v0

    .line 1593
    :cond_1
    iget v0, v0, Lizy;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1595
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    .line 1596
    invoke-interface {v1}, Lkdv;->aM_()I

    move-result v1

    .line 1594
    invoke-static {v0, p2, v1}, Ljcq;->a(Landroid/content/ContentResolver;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1598
    :cond_2
    invoke-static {p3, p4}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/media/MediaResource;->getLocalBitmap(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private getMediaStoreThumb(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 1460
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    const/16 v1, 0x200

    const/16 v2, 0x180

    invoke-interface {v0, v1, v2}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1463
    invoke-static {p1, p2, v0}, Ljcq;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1466
    if-eq v1, v0, :cond_0

    .line 1467
    iget-object v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v2, v0}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 1469
    :cond_0
    return-object v1
.end method

.method private getRotatedAndCroppedBitmap(Landroid/graphics/Bitmap;Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    .line 1549
    invoke-direct {p0, p2, p3, p1}, Lcom/google/android/libraries/social/media/MediaResource;->getRotatedBitmap(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1550
    if-eq v4, p1, :cond_0

    .line 1551
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v1, p1}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 1554
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v1, p4, p5}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1555
    if-nez v4, :cond_3

    const/4 v0, 0x0

    .line 1556
    :goto_0
    if-eq v4, v0, :cond_1

    .line 1557
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v1, v4}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 1559
    :cond_1
    if-eq v3, v0, :cond_2

    .line 1560
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v1, v3}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 1563
    :cond_2
    return-object v0

    .line 1555
    :cond_3
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v1, p4, :cond_4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ne v1, p5, :cond_4

    move-object v0, v4

    goto :goto_0

    :cond_4
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    mul-int/2addr v1, p5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int/2addr v2, p4

    if-le v1, v2, :cond_5

    int-to-float v1, p5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v1, v2

    int-to-float v1, p4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    sub-float/2addr v1, v6

    mul-float/2addr v1, v7

    :goto_1
    invoke-virtual {v5, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    float-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v1, v7

    float-to-int v0, v0

    int-to-float v0, v0

    add-float/2addr v0, v7

    invoke-virtual {v5, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    if-eqz v3, :cond_6

    move-object v0, v3

    :goto_2
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/Paint;

    const/4 v6, 0x3

    invoke-direct {v2, v6}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v1, v4, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_5
    int-to-float v1, p4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v1, v2

    int-to-float v1, p5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    sub-float/2addr v1, v6

    mul-float/2addr v1, v7

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_1

    :cond_6
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p4, p5, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2
.end method

.method private getRotatedBitmap(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 1473
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p2}, Ljcq;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)I

    move-result v0

    .line 1477
    if-eqz v0, :cond_0

    .line 1478
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 1479
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 1478
    invoke-interface {v1, v2, v3}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1481
    invoke-direct {p0, p3, v0}, Lcom/google/android/libraries/social/media/MediaResource;->rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object p3

    .line 1482
    if-eq p3, v1, :cond_0

    .line 1483
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v0, v1}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 1489
    :cond_0
    return-object p3
.end method

.method private getScaledLocalBitmap(ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1532
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 1533
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 1534
    int-to-float v2, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1536
    cmpg-float v3, v2, v4

    if-gez v3, :cond_0

    .line 1537
    iget-object v3, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v1, v1

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-interface {v3, v0, v1}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1538
    cmpl-float v0, v2, v4

    if-ltz v0, :cond_1

    .line 1539
    :goto_0
    if-eq p2, v1, :cond_0

    .line 1540
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v0, v1}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 1543
    :cond_0
    return-object p2

    .line 1538
    :cond_1
    if-nez v1, :cond_2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_1
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v4, Landroid/graphics/Paint;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v2, p2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    move-object p2, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private static getShortFileName(Lizu;IIIILandroid/graphics/RectF;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 642
    invoke-virtual {p0}, Lizu;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 643
    invoke-virtual {p0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v1, Ljac;->b:Ljac;

    if-ne v0, v1, :cond_2

    .line 644
    invoke-virtual {p0}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljbg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 655
    :goto_0
    if-eqz p6, :cond_0

    .line 656
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 658
    :cond_0
    :goto_1
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 659
    invoke-static {v0}, Llsf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 661
    packed-switch p2, :pswitch_data_0

    .line 673
    :goto_2
    :pswitch_0
    invoke-static {v1, p1}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileNameAppendFlags(Ljava/lang/StringBuilder;I)V

    .line 679
    if-eqz p5, :cond_1

    .line 680
    const/4 v0, 0x0

    invoke-static {p5, v0}, Ljbd;->a(Landroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    :cond_1
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 646
    :cond_2
    invoke-virtual {p0}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 648
    :cond_3
    invoke-virtual {p0}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 649
    invoke-virtual {p0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lizu;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 650
    :cond_5
    invoke-virtual {p0}, Lizu;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 651
    invoke-virtual {p0}, Lizu;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 653
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "A media ref should have a URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 656
    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 663
    :pswitch_1
    const/16 v0, 0x2d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x78

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 666
    :pswitch_2
    const-string v0, "-t"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 669
    :pswitch_3
    const-string v0, "-l"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 672
    :pswitch_4
    const-string v0, "-z"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 661
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static getShortFileNameAppendFlags(Ljava/lang/StringBuilder;I)V
    .locals 1

    .prologue
    .line 686
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 687
    const-string v0, "-a"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    :cond_0
    and-int/lit8 v0, p1, 0x20

    if-eqz v0, :cond_1

    .line 690
    const-string v0, "-nw"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 692
    :cond_1
    and-int/lit16 v0, p1, 0x100

    if-eqz v0, :cond_2

    .line 693
    const-string v0, "-p"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    :cond_2
    return-void
.end method

.method private initializeAutoSizeSupport(Lkdv;)V
    .locals 6

    .prologue
    .line 580
    invoke-interface {p1}, Lkdv;->f()I

    move-result v0

    .line 581
    sput v0, Lcom/google/android/libraries/social/media/MediaResource;->sSizeCategoryAPixels:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/google/android/libraries/social/media/MediaResource;->sSizeCategoryBPixels:I

    .line 582
    sget v0, Lcom/google/android/libraries/social/media/MediaResource;->sSizeCategoryAPixels:I

    int-to-float v0, v0

    const v1, 0x3e99999a    # 0.3f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/google/android/libraries/social/media/MediaResource;->sSizeCategoryCPixels:I

    .line 584
    invoke-interface {p1}, Lkdv;->m()F

    move-result v0

    .line 585
    float-to-double v2, v0

    const-wide/high16 v4, 0x3fe8000000000000L    # 0.75

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 588
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/libraries/social/media/MediaResource;->sDownscaleStandardImages:Z

    .line 590
    const/high16 v1, 0x46000000    # 8192.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/google/android/libraries/social/media/MediaResource;->sStandardScaleMultiplier:I

    .line 593
    :cond_0
    invoke-interface {p1}, Lkdv;->g()I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/media/MediaResource;->mDefaultThumbnailSize:I

    .line 594
    invoke-interface {p1}, Lkdv;->h()I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/media/MediaResource;->mDefaultLargeSize:I

    .line 595
    return-void
.end method

.method private isAutomaticCacheFileUpgradeNeeded(Lizy;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1016
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedSizeAndScale:Lizz;

    if-nez v1, :cond_1

    .line 1034
    :cond_0
    :goto_0
    return v0

    .line 1020
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedSizeAndScale:Lizz;

    iget-object v1, v1, Lizz;->c:Lizz;

    .line 1021
    if-eqz v1, :cond_0

    .line 1025
    iget v2, v1, Lizz;->a:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 1026
    iput-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    .line 1034
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 1027
    :cond_3
    iget v2, v1, Lizz;->a:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    sget-boolean v2, Lcom/google/android/libraries/social/media/MediaResource;->sDownscaleStandardImages:Z

    if-eqz v2, :cond_2

    .line 1029
    invoke-virtual {p1}, Lizy;->a()I

    move-result v2

    and-int/lit16 v2, v2, 0x1000

    if-nez v2, :cond_2

    .line 1030
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->isOnFastAndFreeNetwork()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1031
    iput-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    goto :goto_1
.end method

.method private isCustomCachedFileUpgradeNeeded(Lizy;Ljava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 943
    iget v0, p1, Lizy;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    move v0, v2

    .line 982
    :goto_0
    return v0

    .line 948
    :cond_0
    invoke-virtual {p1}, Lizy;->a()I

    move-result v0

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_1

    move v0, v2

    .line 949
    goto :goto_0

    .line 953
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityType:I

    if-nez v0, :cond_2

    .line 955
    iget v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    iget v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityLevel:I

    if-le v0, v1, :cond_3

    move v0, v3

    .line 956
    goto :goto_0

    .line 960
    :cond_2
    iget v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    iget v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityLevel:I

    if-ge v0, v1, :cond_3

    move v0, v3

    .line 961
    goto :goto_0

    .line 965
    :cond_3
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/social/media/MediaResource;->getWidth(Ljava/lang/Object;)I

    move-result v4

    .line 966
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/social/media/MediaResource;->getHeight(Ljava/lang/Object;)I

    move-result v0

    .line 967
    int-to-double v6, v4

    int-to-double v0, v0

    div-double/2addr v6, v0

    .line 970
    iget v0, p1, Lizy;->d:I

    if-nez v0, :cond_4

    iget v0, p1, Lizy;->e:I

    int-to-double v0, v0

    mul-double/2addr v0, v6

    double-to-int v0, v0

    .line 971
    :goto_1
    iget v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileWidth:I

    if-nez v1, :cond_5

    iget v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileHeight:I

    int-to-double v8, v1

    mul-double/2addr v6, v8

    double-to-int v1, v6

    .line 975
    :goto_2
    sget v5, Lcom/google/android/libraries/social/media/MediaResource;->sCustomDimensionSlop:I

    sub-int/2addr v0, v5

    if-le v0, v1, :cond_8

    .line 978
    if-le v1, v4, :cond_6

    move v0, v3

    .line 979
    :goto_3
    if-nez v0, :cond_7

    move v0, v3

    goto :goto_0

    .line 970
    :cond_4
    iget v0, p1, Lizy;->d:I

    goto :goto_1

    .line 971
    :cond_5
    iget v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileWidth:I

    goto :goto_2

    :cond_6
    move v0, v2

    .line 978
    goto :goto_3

    :cond_7
    move v0, v2

    .line 979
    goto :goto_0

    :cond_8
    move v0, v2

    .line 982
    goto :goto_0
.end method

.method private isOnFastAndFreeNetwork()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1071
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getConnectivityManager()Landroid/net/ConnectivityManager;

    move-result-object v2

    .line 1072
    invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1073
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1081
    :cond_0
    :goto_0
    return v0

    .line 1077
    :cond_1
    invoke-static {v2}, Ler;->a(Landroid/net/ConnectivityManager;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1081
    goto :goto_0
.end method

.method private obtainLocalRawFileName()Z
    .locals 3

    .prologue
    .line 1223
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1224
    iget-object v0, v0, Lizy;->a:Lizu;

    .line 1225
    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    .line 1226
    invoke-static {v0}, Llsb;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1227
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mLocalRawFile:Ljava/io/File;

    .line 1237
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1229
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v1}, Lkdv;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1230
    invoke-static {v1, v0}, Llsb;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1231
    if-nez v0, :cond_1

    .line 1232
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Couldn\'t compute raw file name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/media/MediaResource;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1233
    const/4 v0, 0x0

    goto :goto_1

    .line 1235
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mLocalRawFile:Ljava/io/File;

    goto :goto_0
.end method

.method private rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1493
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1494
    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 1496
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1497
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1499
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1500
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 1502
    iget-object v4, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v4, v2, v3}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1504
    iget v3, v1, Landroid/graphics/RectF;->left:F

    neg-float v3, v3

    iget v1, v1, Landroid/graphics/RectF;->top:F

    neg-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1506
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1507
    new-instance v3, Landroid/graphics/Paint;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 1508
    invoke-virtual {v1, p1, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1510
    return-object v2
.end method

.method private useCustomWithQualityCache(Lizy;)Z
    .locals 2

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    .line 609
    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljaa;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljaa;

    invoke-interface {v0}, Ljaa;->a()Z

    move-result v0

    .line 611
    if-eqz v0, :cond_0

    iget v0, p1, Lizy;->b:I

    if-nez v0, :cond_0

    iget v0, p1, Lizy;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lizy;->g:Lizo;

    if-nez v0, :cond_0

    iget-object v0, p1, Lizy;->f:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    iget-object v0, p1, Lizy;->a:Lizu;

    .line 616
    invoke-virtual {v0}, Lizu;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lizy;->a:Lizu;

    .line 617
    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v1, Ljac;->a:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public deliverData(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1149
    iget v1, v0, Lizy;->b:I

    sparse-switch v1, :sswitch_data_0

    .line 1167
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliverData(Ljava/lang/Object;)V

    .line 1168
    return-void

    .line 1151
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    if-eqz v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    iput-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedSizeAndScale:Lizz;

    .line 1153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    goto :goto_0

    .line 1159
    :sswitch_1
    iget v1, v0, Lizy;->d:I

    iput v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileWidth:I

    .line 1160
    iget v0, v0, Lizy;->e:I

    iput v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileHeight:I

    .line 1161
    iget v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityType:I

    iput v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityType:I

    .line 1162
    iget v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityLevel:I

    iput v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    goto :goto_0

    .line 1149
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method public deliverResource(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1141
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliverResource(Ljava/lang/Object;)V

    .line 1142
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/media/MediaResource;->doUpgradeIfNecessary(Ljava/lang/Object;)V

    .line 1143
    return-void
.end method

.method protected downloadResource()V
    .locals 3

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1043
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    if-nez v1, :cond_0

    iget v1, v0, Lizy;->b:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 1045
    invoke-virtual {v0}, Lizy;->a()I

    move-result v1

    and-int/lit16 v1, v1, 0x400

    if-nez v1, :cond_0

    .line 1046
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getInitialDownloadSizeAndScale()Lizz;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    .line 1048
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    iget-object v1, v1, Lizz;->c:Lizz;

    if-eqz v1, :cond_0

    .line 1049
    invoke-virtual {v0}, Lizy;->a()I

    move-result v0

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 1050
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->isOnFastAndFreeNetwork()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1052
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    iget-object v0, v0, Lizz;->c:Lizz;

    iput-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    .line 1055
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1056
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->loadLocalResource()V

    .line 1060
    :goto_0
    return-void

    .line 1058
    :cond_1
    invoke-super {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->downloadResource()V

    goto :goto_0
.end method

.method protected generateDebugContentType()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1644
    invoke-super {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->generateDebugContentType()Ljava/lang/String;

    move-result-object v1

    .line 1645
    if-nez v1, :cond_0

    .line 1646
    const/4 v0, 0x0

    .line 1671
    :goto_0
    return-object v0

    .line 1649
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1650
    iget v0, v0, Lizy;->c:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    move-object v0, v1

    .line 1651
    goto :goto_0

    .line 1654
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityType:I

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 1671
    goto :goto_0

    .line 1656
    :pswitch_0
    iget v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    packed-switch v0, :pswitch_data_1

    move-object v0, v1

    .line 1666
    goto :goto_0

    .line 1658
    :pswitch_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " (High)"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1660
    :pswitch_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " (Standard)"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1662
    :pswitch_3
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " (Low)"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1664
    :pswitch_4
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " (Very Low)"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1669
    :pswitch_5
    iget v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFileQualityLevel:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (L"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1654
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 1656
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getCacheFileName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1132
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    if-eqz v0, :cond_1

    .line 1133
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mDownloadSizeAndScale:Lizz;

    iget-object v0, v0, Lizz;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1135
    :goto_0
    return-object v0

    .line 1133
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1135
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCachedFile()Ljava/io/File;
    .locals 5

    .prologue
    const/4 v2, 0x3

    .line 752
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mLocalRawFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mLocalRawFile:Ljava/io/File;

    .line 786
    :goto_0
    return-object v0

    .line 756
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 757
    invoke-virtual {v0}, Lizy;->a()I

    move-result v1

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_1

    .line 758
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 761
    :cond_1
    iget v1, v0, Lizy;->b:I

    sparse-switch v1, :sswitch_data_0

    .line 786
    :cond_2
    :goto_1
    invoke-super {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getCachedFile()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 763
    :sswitch_0
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/media/MediaResource;->useCustomWithQualityCache(Lizy;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 764
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->findCustomSizedCachedFile()V

    .line 765
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFile:Ljava/io/File;

    if-eqz v0, :cond_4

    .line 766
    const-string v0, "ImageResource"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 767
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFile:Ljava/io/File;

    .line 768
    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cached resource found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 767
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFile:Ljava/io/File;

    goto :goto_0

    .line 773
    :cond_4
    const-string v0, "ImageResource"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 781
    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->findAutoSizedCachedFile()V

    .line 782
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mCachedFile:Ljava/io/File;

    goto/16 :goto_0

    .line 761
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .locals 7

    .prologue
    .line 1242
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    move-object v6, v0

    check-cast v6, Lizy;

    .line 1243
    iget-object v1, v6, Lizy;->a:Lizu;

    .line 1244
    invoke-virtual {v1}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    .line 1248
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1249
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    .line 1250
    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lizn;

    invoke-static {v0, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizn;

    .line 1251
    invoke-virtual {v1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v1

    .line 1252
    if-eqz v0, :cond_1

    invoke-interface {v0, v1}, Lizn;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1255
    :cond_0
    :goto_0
    invoke-static {v0}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1256
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getFifeOptions()I

    move-result v1

    .line 1257
    iget v2, v6, Lizy;->b:I

    packed-switch v2, :pswitch_data_0

    .line 1292
    :goto_1
    :pswitch_0
    return-object v0

    .line 1252
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1259
    :pswitch_1
    iget v2, v6, Lizy;->d:I

    if-eqz v2, :cond_2

    iget v2, v6, Lizy;->e:I

    if-eqz v2, :cond_2

    .line 1260
    invoke-virtual {v6}, Lizy;->a()I

    move-result v2

    and-int/lit16 v2, v2, 0x100

    if-nez v2, :cond_4

    .line 1262
    or-int/lit8 v1, v1, 0x48

    .line 1268
    :cond_2
    :goto_2
    iget v2, v6, Lizy;->c:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 1269
    iget-object v2, p0, Lcom/google/android/libraries/social/media/MediaResource;->fifeQualityModel:Lizx;

    iget v3, v6, Lizy;->c:I

    invoke-interface {v2, v3, v1}, Lizx;->a(II)I

    move-result v1

    .line 1272
    :cond_3
    iget v2, v6, Lizy;->d:I

    iget v3, v6, Lizy;->e:I

    iget v4, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityType:I

    iget v5, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityLevel:I

    iget-object v6, v6, Lizy;->f:Landroid/graphics/RectF;

    invoke-static/range {v0 .. v6}, Ljbd;->a(Ljava/lang/String;IIIIILandroid/graphics/RectF;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1263
    :cond_4
    invoke-virtual {v6}, Lizy;->a()I

    move-result v2

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_2

    .line 1264
    or-int/lit8 v1, v1, 0x40

    goto :goto_2

    .line 1276
    :pswitch_2
    invoke-virtual {v6}, Lizy;->a()I

    move-result v2

    and-int/lit16 v2, v2, 0x100

    if-nez v2, :cond_5

    .line 1277
    or-int/lit8 v1, v1, 0x8

    .line 1279
    :cond_5
    sget v2, Lcom/google/android/libraries/social/media/MediaResource;->mDefaultThumbnailSize:I

    iget-object v3, v6, Lizy;->f:Landroid/graphics/RectF;

    invoke-static {v0, v1, v2, v3}, Ljbd;->a(Ljava/lang/String;IILandroid/graphics/RectF;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1282
    :pswitch_3
    sget v2, Lcom/google/android/libraries/social/media/MediaResource;->mDefaultLargeSize:I

    iget-object v3, v6, Lizy;->f:Landroid/graphics/RectF;

    invoke-static {v0, v1, v2, v3}, Ljbd;->a(Ljava/lang/String;IILandroid/graphics/RectF;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1285
    :pswitch_4
    iget-object v2, v6, Lizy;->f:Landroid/graphics/RectF;

    invoke-static {v0, v1, v2}, Ljbd;->a(Ljava/lang/String;ILandroid/graphics/RectF;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1288
    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getAutomaticSize()I

    move-result v2

    iget-object v3, v6, Lizy;->f:Landroid/graphics/RectF;

    invoke-static {v0, v1, v2, v3}, Ljbd;->a(Ljava/lang/String;IILandroid/graphics/RectF;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1257
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public getRawFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mLocalRawFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mLocalRawFile:Ljava/io/File;

    .line 747
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getRawFile()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public getShortFileName()Ljava/lang/String;
    .locals 8

    .prologue
    .line 622
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mShortFileName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 623
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    move-object v6, v0

    check-cast v6, Lizy;

    .line 624
    invoke-direct {p0, v6}, Lcom/google/android/libraries/social/media/MediaResource;->useCustomWithQualityCache(Lizy;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 625
    iget-object v1, v6, Lizy;->a:Lizu;

    invoke-virtual {v6}, Lizy;->a()I

    move-result v2

    iget v3, v6, Lizy;->d:I

    iget v4, v6, Lizy;->e:I

    iget v5, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityType:I

    iget v6, p0, Lcom/google/android/libraries/social/media/MediaResource;->downloadQualityLevel:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/social/media/MediaResource;->getCustomWithQualityShortFileName(Lizu;IIIII)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mShortFileName:Ljava/lang/String;

    .line 633
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mShortFileName:Ljava/lang/String;

    return-object v0

    .line 629
    :cond_1
    iget-object v0, v6, Lizy;->a:Lizu;

    invoke-virtual {v6}, Lizy;->a()I

    move-result v1

    iget v2, v6, Lizy;->b:I

    iget v3, v6, Lizy;->d:I

    iget v4, v6, Lizy;->e:I

    iget-object v5, v6, Lizy;->f:Landroid/graphics/RectF;

    iget-object v7, v6, Lizy;->g:Lizo;

    if-nez v7, :cond_2

    const/4 v6, 0x0

    :goto_1
    invoke-static/range {v0 .. v6}, Lcom/google/android/libraries/social/media/MediaResource;->getShortFileName(Lizu;IIIILandroid/graphics/RectF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mShortFileName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v6, v6, Lizy;->g:Lizo;

    .line 630
    invoke-interface {v6}, Lizo;->a()Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method public load()V
    .locals 5

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    check-cast v0, Lizy;

    .line 1205
    iget-object v1, v0, Lizy;->a:Lizu;

    .line 1206
    invoke-virtual {v0}, Lizy;->a()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1207
    invoke-direct {p0}, Lcom/google/android/libraries/social/media/MediaResource;->obtainLocalRawFileName()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1208
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1209
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1210
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getRawFile()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2e

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Returning file name to consumers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " file name: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1209
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/MediaResource;->logDebug(Ljava/lang/String;)V

    .line 1212
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->getRawFile()Ljava/io/File;

    move-result-object v2

    invoke-interface {v0, p0, v1, v2}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    .line 1220
    :goto_0
    return-void

    .line 1214
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    goto :goto_0

    .line 1219
    :cond_2
    invoke-super {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->load()V

    goto :goto_0
.end method

.method public loadLocalResource()V
    .locals 18

    .prologue
    .line 1345
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v14

    .line 1346
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v2}, Lkdv;->a()Landroid/content/Context;

    move-result-object v3

    .line 1347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    move-object v11, v2

    check-cast v11, Lizy;

    .line 1348
    iget-object v2, v11, Lizy;->a:Lizu;

    invoke-virtual {v2}, Lizu;->e()Landroid/net/Uri;

    move-result-object v4

    .line 1349
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/media/MediaResource;->isDebugLogEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1350
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x17

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Loading local resource "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/MediaResource;->logDebug(Ljava/lang/String;)V

    .line 1353
    :cond_0
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v4}, Llsb;->b(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mMimeType:Ljava/lang/String;

    .line 1354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mMimeType:Ljava/lang/String;

    invoke-static {v2}, Llsb;->c(Ljava/lang/String;)Z

    move-result v7

    .line 1355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mMimeType:Ljava/lang/String;

    invoke-static {v2}, Llsb;->a(Ljava/lang/String;)Z

    move-result v9

    .line 1356
    invoke-static {v4}, Llsb;->b(Landroid/net/Uri;)Z

    move-result v13

    .line 1357
    invoke-static {v4}, Llsb;->a(Landroid/net/Uri;)Z

    move-result v8

    .line 1359
    if-nez v7, :cond_5

    if-nez v9, :cond_5

    .line 1361
    invoke-static {v3}, Ljvo;->a(Landroid/content/Context;)Ljvo;

    move-result-object v2

    .line 1362
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/social/media/MediaResource;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v2, v4, v5, v0}, Ljvo;->a(Landroid/net/Uri;Ljava/lang/String;Ljvt;)V

    .line 1369
    :cond_1
    :goto_0
    const/4 v10, 0x0

    .line 1370
    const/4 v12, 0x0

    .line 1372
    :try_start_0
    iget v5, v11, Lizy;->d:I

    .line 1373
    iget v6, v11, Lizy;->e:I

    .line 1374
    iget v2, v11, Lizy;->b:I

    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v2, v0, :cond_6

    .line 1375
    sget v6, Lcom/google/android/libraries/social/media/MediaResource;->mDefaultThumbnailSize:I

    move v5, v6

    .line 1395
    :cond_2
    :goto_1
    :pswitch_0
    const/4 v2, 0x0

    .line 1397
    if-eqz v8, :cond_3

    .line 1398
    new-instance v2, Ljava/io/File;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    const-string v8, "-thumb"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v17

    if-eqz v17, :cond_7

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_2
    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1401
    :cond_3
    iget v8, v11, Lizy;->b:I

    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v8, v0, :cond_8

    if-eqz v2, :cond_8

    .line 1402
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1403
    const/4 v3, 0x0

    invoke-static {v2, v3}, Llrx;->a(Ljava/io/File;Z)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1429
    :goto_3
    if-eqz v10, :cond_e

    .line 1430
    invoke-virtual {v11}, Lizy;->a()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_4

    .line 1431
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v4

    .line 1432
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1433
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x55

    invoke-virtual {v10, v2, v6, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1435
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v2}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/media/MediaResource;->getCacheFileName()Ljava/lang/String;

    move-result-object v7

    .line 1436
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    array-length v8, v2

    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-result-object v2

    check-cast v2, Ljava/nio/ByteBuffer;

    .line 1435
    invoke-virtual {v6, v7, v2}, Lcom/google/android/libraries/social/filecache/FileCache;->write(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    .line 1437
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 1438
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/media/MediaResource;->isDebugLogEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1440
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/media/MediaResource;->getRawFile()Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1441
    invoke-static {v4, v5}, Llse;->b(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x3e

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Saved local thumbnail in file cache: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " file name: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " time spent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1439
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/MediaResource;->logDebug(Ljava/lang/String;)V

    .line 1444
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/libraries/social/media/MediaResource;->deliverResource(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1457
    :goto_4
    return-void

    .line 1363
    :cond_5
    if-eqz v9, :cond_1

    .line 1364
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mResourceType:I

    .line 1365
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-interface {v2, v0, v5}, Lkdv;->b(Lkda;I)V

    goto/16 :goto_0

    .line 1377
    :cond_6
    :try_start_1
    iget v2, v11, Lizy;->b:I

    packed-switch v2, :pswitch_data_0

    .line 1391
    :pswitch_1
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 1451
    :catch_0
    move-exception v2

    const/4 v2, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/MediaResource;->deliverDownloadError(I)V

    goto :goto_4

    .line 1379
    :pswitch_2
    :try_start_2
    sget v6, Lcom/google/android/libraries/social/media/MediaResource;->mDefaultLargeSize:I

    move v5, v6

    .line 1380
    goto/16 :goto_1

    .line 1386
    :pswitch_3
    if-nez v5, :cond_2

    if-nez v6, :cond_2

    .line 1387
    sget v6, Lcom/google/android/libraries/social/media/MediaResource;->mDefaultLargeSize:I

    move v5, v6

    goto/16 :goto_1

    .line 1398
    :cond_7
    new-instance v8, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v8, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_2

    .line 1453
    :catch_1
    move-exception v2

    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/MediaResource;->deliverDownloadError(I)V

    goto :goto_4

    .line 1404
    :cond_8
    if-eqz v9, :cond_9

    :try_start_3
    invoke-virtual {v11}, Lizy;->a()I

    move-result v2

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_9

    .line 1405
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/media/MediaResource;->obtainLocalRawFileName()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/media/MediaResource;->mLocalRawFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Llrx;->a(Ljava/io/File;Z)Ljava/nio/ByteBuffer;

    move-result-object v2

    goto/16 :goto_3

    .line 1408
    :cond_9
    if-eqz v13, :cond_a

    move-object/from16 v2, p0

    .line 1409
    invoke-direct/range {v2 .. v7}, Lcom/google/android/libraries/social/media/MediaResource;->getMediaStoreBitmap(Landroid/content/Context;Landroid/net/Uri;IIZ)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1410
    const-string v6, "mediastore"

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    move-object/from16 v3, p0

    move-wide v4, v14

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/libraries/social/media/MediaResource;->logDecodeTime(JLjava/lang/String;IIILandroid/graphics/Bitmap;)V

    move-object v2, v12

    goto/16 :goto_3

    .line 1412
    :cond_a
    if-eqz v7, :cond_b

    .line 1413
    invoke-static {v3, v4, v5, v6}, Ljcq;->a(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1414
    const-string v6, "video"

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    move-object/from16 v3, p0

    move-wide v4, v14

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/libraries/social/media/MediaResource;->logDecodeTime(JLjava/lang/String;IIILandroid/graphics/Bitmap;)V

    move-object v2, v12

    goto/16 :goto_3

    .line 1417
    :cond_b
    iget v2, v11, Lizy;->b:I

    const/4 v7, 0x1

    if-ne v2, v7, :cond_d

    .line 1419
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    .line 1420
    invoke-interface {v3}, Lkdv;->aM_()I

    move-result v3

    .line 1418
    invoke-static {v2, v4, v3}, Ljcq;->a(Landroid/content/ContentResolver;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1425
    :goto_5
    invoke-virtual {v4}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v6

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v9, -0x1

    move-object/from16 v3, p0

    move-wide v4, v14

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/libraries/social/media/MediaResource;->logDecodeTime(JLjava/lang/String;IIILandroid/graphics/Bitmap;)V

    :cond_c
    move-object v2, v12

    goto/16 :goto_3

    .line 1422
    :cond_d
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v2}, Lcom/google/android/libraries/social/media/MediaResource;->getLocalBitmap(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v10

    goto :goto_5

    .line 1445
    :cond_e
    if-eqz v2, :cond_f

    .line 1446
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/MediaResource;->deliverData(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_4

    .line 1455
    :catch_2
    move-exception v2

    const/4 v2, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/MediaResource;->deliverDownloadError(I)V

    goto/16 :goto_4

    .line 1448
    :cond_f
    const/4 v2, 0x3

    :try_start_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/MediaResource;->deliverDownloadError(I)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_4

    .line 1377
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onPanoramaTypeDetected(I)V
    .locals 4

    .prologue
    .line 1605
    const/4 v0, 0x0

    .line 1606
    packed-switch p1, :pswitch_data_0

    .line 1625
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1626
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x42

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Delivering resource type to consumers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resource type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/media/MediaResource;->logDebug(Ljava/lang/String;)V

    .line 1630
    :cond_0
    if-eqz v0, :cond_1

    .line 1631
    iget-object v1, p0, Lcom/google/android/libraries/social/media/MediaResource;->mManager:Lkdv;

    invoke-interface {v1, p0, v0}, Lkdv;->b(Lkda;I)V

    .line 1633
    :cond_1
    return-void

    .line 1608
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mMimeType:Ljava/lang/String;

    invoke-static {v0}, Llsb;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1609
    const/4 v0, 0x2

    goto :goto_0

    .line 1610
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mMimeType:Ljava/lang/String;

    invoke-static {v0}, Llsb;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1611
    const/4 v0, 0x1

    goto :goto_0

    .line 1613
    :cond_3
    const/4 v0, -0x1

    .line 1615
    goto :goto_0

    .line 1617
    :pswitch_1
    const/4 v0, 0x4

    .line 1618
    goto :goto_0

    .line 1621
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 1606
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onPanoramaTypeDetectionFailure()V
    .locals 3

    .prologue
    .line 1637
    invoke-virtual {p0}, Lcom/google/android/libraries/social/media/MediaResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1638
    iget-object v0, p0, Lcom/google/android/libraries/social/media/MediaResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x30

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed to determine if the image is a panorama: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/media/MediaResource;->logDebug(Ljava/lang/String;)V

    .line 1640
    :cond_0
    return-void
.end method
