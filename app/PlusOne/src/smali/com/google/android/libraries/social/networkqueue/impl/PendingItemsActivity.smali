.class public Lcom/google/android/libraries/social/networkqueue/impl/PendingItemsActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhjj;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 22
    invoke-direct {p0}, Lloa;-><init>()V

    .line 24
    new-instance v0, Lhjg;

    iget-object v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingItemsActivity;->y:Llqc;

    const v2, 0x7f12000f

    invoke-direct {v0, p0, v1, v2}, Lhjg;-><init>(Los;Llqr;I)V

    iget-object v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingItemsActivity;->x:Llnh;

    .line 25
    invoke-virtual {v0, v1}, Lhjg;->a(Llnh;)Lhjg;

    move-result-object v0

    .line 26
    invoke-virtual {v0, p0}, Lhjg;->e(Lhjj;)Lhjg;

    .line 27
    new-instance v0, Llln;

    iget-object v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/PendingItemsActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Llln;-><init>(Los;Llqr;)V

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 48
    const v0, 0x7f1006e5

    new-instance v1, Lifa;

    invoke-direct {v1}, Lifa;-><init>()V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 49
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_stream"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 50
    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 39
    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 40
    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 41
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f0a019c

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/networkqueue/impl/PendingItemsActivity;->setTitle(I)V

    .line 34
    const v0, 0x7f0401c8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/networkqueue/impl/PendingItemsActivity;->setContentView(I)V

    .line 35
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0, p1}, Lloa;->onNewIntent(Landroid/content/Intent;)V

    .line 62
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/networkqueue/impl/PendingItemsActivity;->setIntent(Landroid/content/Intent;)V

    .line 63
    return-void
.end method
