.class public Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private b:Z

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->a:Landroid/view/LayoutInflater;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->b:Z

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->a:Landroid/view/LayoutInflater;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->b:Z

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->a:Landroid/view/LayoutInflater;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->b:Z

    .line 44
    return-void
.end method


# virtual methods
.method public a(Lnse;)V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 50
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->removeAllViews()V

    .line 51
    if-eqz p1, :cond_5

    iget-object v0, p1, Lnse;->a:[Lnsd;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 52
    iget-object v4, p1, Lnse;->a:[Lnsd;

    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_5

    aget-object v6, v4, v2

    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f0401ee

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 54
    const v0, 0x7f1003f6

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v1, v6, Lnsd;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v1, "https://s2.googleusercontent.com/s2/favicons?domain="

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v1}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v1, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->c:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    float-to-double v10, v1

    const-wide v12, 0x3ffccccccccccccdL    # 1.8

    cmpl-double v1, v10, v12

    if-ltz v1, :cond_3

    const-string v1, "32"

    :goto_1
    iput-object v1, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->c:Ljava/lang/String;

    :cond_1
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v9, "sz"

    iget-object v10, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->c:Ljava/lang/String;

    invoke-virtual {v1, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v9, Ljac;->a:Ljac;

    invoke-static {v8, v1, v9}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->e(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f100138

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Landroid/text/SpannableString;

    iget-object v8, v6, Lnsd;->c:Ljava/lang/String;

    invoke-direct {v1, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iget-boolean v8, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->b:Z

    if-eqz v8, :cond_2

    iget-object v8, v6, Lnsd;->b:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    new-instance v8, Llju;

    iget-object v9, v6, Lnsd;->b:Ljava/lang/String;

    invoke-direct {v8, v9, v3}, Llju;-><init>(Ljava/lang/String;Z)V

    iget-object v6, v6, Lnsd;->c:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v9, 0x21

    invoke-interface {v1, v8, v3, v6, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    invoke-static {}, Lljw;->a()Lljw;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    invoke-virtual {p0, v7}, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->addView(Landroid/view/View;)V

    .line 52
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 54
    :cond_3
    const-wide v12, 0x4006666666666666L    # 2.8

    cmpl-double v1, v10, v12

    if-ltz v1, :cond_4

    const-string v1, "64"

    goto :goto_1

    :cond_4
    const-string v1, "16"

    goto :goto_1

    .line 58
    :cond_5
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/google/android/libraries/social/squares/stream/RelatedLinksView;->b:Z

    .line 106
    return-void
.end method
