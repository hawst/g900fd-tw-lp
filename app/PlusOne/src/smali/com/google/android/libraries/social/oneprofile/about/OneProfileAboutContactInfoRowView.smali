.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static c:I

.field private static d:Landroid/content/res/ColorStateList;


# instance fields
.field public a:I

.field public b:Ljms;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 42
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->c:I

    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d012d

    .line 44
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->c:I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->d:Landroid/content/res/ColorStateList;

    .line 31
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->c:I

    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d012d

    .line 44
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->c:I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->d:Landroid/content/res/ColorStateList;

    .line 35
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->c:I

    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d012d

    .line 44
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->c:I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b034a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->d:Landroid/content/res/ColorStateList;

    .line 39
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->a(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public a(ILjms;)V
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->a:I

    .line 58
    if-eqz p1, :cond_0

    .line 59
    iput-object p2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->b:Ljms;

    .line 61
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->a:I

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->f:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->f:Landroid/widget/TextView;

    .line 90
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020416

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 89
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->f:Landroid/widget/TextView;

    new-instance v1, Ljmn;

    invoke-direct {v1, p0}, Ljmn;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->f:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 52
    const v0, 0x7f100138

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->e:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f1001e9

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoRowView;->f:Landroid/widget/TextView;

    .line 54
    return-void
.end method
