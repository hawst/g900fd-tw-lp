.class public Lcom/google/android/libraries/social/ingest/IngestService;
.super Landroid/app/Service;
.source "PG"

# interfaces
.implements Lisi;
.implements Lisn;
.implements Lisp;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# instance fields
.field private a:Lisl;

.field private final b:Landroid/os/IBinder;

.field private c:Lisa;

.field private d:Landroid/mtp/MtpDevice;

.field private e:Ljava/lang/String;

.field private f:Liso;

.field private g:Lcom/google/android/libraries/social/ingest/IngestActivity;

.field private h:Z

.field private i:I

.field private j:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lisj;",
            ">;"
        }
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Landroid/app/NotificationManager;

.field private n:Lbs;

.field private o:J

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 66
    new-instance v0, Lirz;

    invoke-direct {v0, p0}, Lirz;-><init>(Lcom/google/android/libraries/social/ingest/IngestService;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->b:Landroid/os/IBinder;

    .line 72
    iput-boolean v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->h:Z

    .line 73
    iput v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->i:I

    .line 75
    iput-boolean v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->k:Z

    .line 79
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->o:J

    .line 80
    iput-boolean v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->p:Z

    .line 308
    return-void
.end method

.method private c(Landroid/mtp/MtpDevice;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 116
    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->d:Landroid/mtp/MtpDevice;

    if-ne v1, p1, :cond_0

    .line 143
    :goto_1
    return-void

    .line 119
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/libraries/social/ingest/IngestService;->h:Z

    .line 120
    iput-boolean v3, p0, Lcom/google/android/libraries/social/ingest/IngestService;->l:Z

    .line 121
    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->j:Ljava/util/Collection;

    .line 122
    iput-boolean v3, p0, Lcom/google/android/libraries/social/ingest/IngestService;->k:Z

    .line 123
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->d:Landroid/mtp/MtpDevice;

    .line 124
    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->f:Liso;

    iget-object v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->d:Landroid/mtp/MtpDevice;

    invoke-virtual {v1, v2}, Liso;->a(Landroid/mtp/MtpDevice;)V

    .line 125
    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->d:Landroid/mtp/MtpDevice;

    if-eqz v1, :cond_2

    .line 126
    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->d:Landroid/mtp/MtpDevice;

    invoke-virtual {v1}, Landroid/mtp/MtpDevice;->getDeviceInfo()Landroid/mtp/MtpDeviceInfo;

    move-result-object v1

    .line 127
    if-nez v1, :cond_1

    move-object p1, v0

    .line 128
    goto :goto_0

    .line 129
    :cond_1
    invoke-virtual {v1}, Landroid/mtp/MtpDeviceInfo;->getModel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->e:Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    .line 133
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->f:Liso;

    invoke-virtual {v1}, Liso;->d()Ljava/lang/Runnable;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 138
    :goto_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    if-eqz v0, :cond_3

    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a()V

    goto :goto_1

    .line 136
    :cond_2
    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->e:Ljava/lang/String;

    goto :goto_2

    .line 141
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->k:Z

    goto :goto_1
.end method


# virtual methods
.method public a()Liso;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->f:Liso;

    return-object v0
.end method

.method public a(IILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 228
    if-eqz p3, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->c:Lisa;

    invoke-virtual {v0, p3}, Lisa;->a(Ljava/lang/String;)V

    .line 231
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->p:Z

    .line 232
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(IILjava/lang/String;)V

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    invoke-virtual {v0, p2, p1, v1}, Lbs;->a(IIZ)Lbs;

    move-result-object v0

    .line 236
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0406

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 237
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    const v1, 0x7f100053

    iget-object v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    .line 238
    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    .line 237
    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 239
    return-void
.end method

.method public a(Landroid/mtp/MtpDevice;)V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->d:Landroid/mtp/MtpDevice;

    if-nez v0, :cond_0

    .line 210
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/ingest/IngestService;->c(Landroid/mtp/MtpDevice;)V

    .line 212
    :cond_0
    return-void
.end method

.method protected a(Landroid/util/SparseBooleanArray;Landroid/widget/Adapter;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 189
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 190
    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 191
    invoke-virtual {p1, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p1, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 193
    instance-of v4, v0, Lisj;

    if-eqz v4, :cond_0

    .line 194
    check-cast v0, Lisj;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 198
    :cond_1
    new-instance v0, Lish;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->d:Landroid/mtp/MtpDevice;

    iget-object v4, p0, Lcom/google/android/libraries/social/ingest/IngestService;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v3, v4, p0}, Lish;-><init>(Landroid/mtp/MtpDevice;Ljava/util/Collection;Ljava/lang/String;Landroid/content/Context;)V

    .line 199
    invoke-virtual {v0, p0}, Lish;->a(Lisi;)V

    .line 200
    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v2, v3}, Lbs;->a(IIZ)Lbs;

    move-result-object v1

    .line 201
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0406

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 202
    const v1, 0x7f100053

    iget-object v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    .line 203
    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    .line 202
    invoke-virtual {p0, v1, v2}, Lcom/google/android/libraries/social/ingest/IngestService;->startForeground(ILandroid/app/Notification;)V

    .line 204
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 205
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/ingest/IngestActivity;)V
    .locals 5

    .prologue
    const v4, 0x7f100052

    const/4 v3, 0x0

    .line 150
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    if-ne v0, p1, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    if-nez v0, :cond_2

    .line 155
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->p:Z

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    invoke-virtual {v0, v3, v3, v3}, Lbs;->a(IIZ)Lbs;

    move-result-object v0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0405

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    .line 159
    invoke-virtual {v1}, Lbs;->c()Landroid/app/Notification;

    move-result-object v1

    .line 158
    invoke-virtual {v0, v4, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    const v1, 0x7f100053

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 164
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    invoke-virtual {v0, v4}, Landroid/app/NotificationManager;->cancel(I)V

    .line 165
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->h:Z

    if-eqz v0, :cond_3

    .line 166
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    iget-object v1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->j:Ljava/util/Collection;

    iget v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Ljava/util/Collection;I)V

    .line 168
    iput-boolean v3, p0, Lcom/google/android/libraries/social/ingest/IngestService;->h:Z

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->j:Ljava/util/Collection;

    .line 171
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->k:Z

    if-eqz v0, :cond_4

    .line 172
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a()V

    .line 173
    iput-boolean v3, p0, Lcom/google/android/libraries/social/ingest/IngestService;->k:Z

    .line 176
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->f:Liso;

    invoke-virtual {v0}, Liso;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->c()V

    .line 179
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->l:Z

    if-eqz v0, :cond_6

    .line 180
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->d()V

    .line 181
    iput-boolean v3, p0, Lcom/google/android/libraries/social/ingest/IngestService;->l:Z

    .line 183
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->d:Landroid/mtp/MtpDevice;

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->p:Z

    goto :goto_0
.end method

.method public a(Lisj;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 271
    iput-boolean v6, p0, Lcom/google/android/libraries/social/ingest/IngestService;->p:Z

    .line 272
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lisj;I)V

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 277
    iget-wide v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->o:J

    const-wide/16 v4, 0xb4

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 278
    iput-wide v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->o:J

    .line 279
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    const/4 v1, 0x1

    invoke-virtual {v0, v6, p2, v1}, Lbs;->a(IIZ)Lbs;

    move-result-object v0

    .line 280
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0403

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 281
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    const v1, 0x7f100052

    iget-object v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    .line 282
    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    .line 281
    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public a(Ljava/util/Collection;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lisj;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 244
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/ingest/IngestService;->stopForeground(Z)V

    .line 245
    iput-boolean v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->p:Z

    .line 246
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Ljava/util/Collection;I)V

    .line 257
    :goto_0
    return-void

    .line 249
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->h:Z

    .line 250
    iput-object p1, p0, Lcom/google/android/libraries/social/ingest/IngestService;->j:Ljava/util/Collection;

    .line 251
    iput p2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->i:I

    .line 252
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    invoke-virtual {v0, v1, v1, v1}, Lbs;->a(IIZ)Lbs;

    move-result-object v0

    .line 253
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0402

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 254
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    const v1, 0x7f100053

    iget-object v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    .line 255
    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    .line 254
    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->b()V

    .line 292
    :cond_0
    return-void
.end method

.method public b(Landroid/mtp/MtpDevice;)V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->d:Landroid/mtp/MtpDevice;

    if-ne p1, v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    const v1, 0x7f100052

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 218
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    const v1, 0x7f100053

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 219
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestService;->c(Landroid/mtp/MtpDevice;)V

    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->p:Z

    .line 223
    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 296
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->p:Z

    .line 297
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->c()V

    .line 305
    :goto_0
    return-void

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    invoke-virtual {v0, v1, v1, v1}, Lbs;->a(IIZ)Lbs;

    move-result-object v0

    .line 301
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0405

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 302
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    const v1, 0x7f100052

    iget-object v2, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    .line 303
    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    .line 302
    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->g:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->d()V

    .line 267
    :goto_0
    return-void

    .line 264
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->l:Z

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->b:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 84
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 85
    new-instance v0, Lisa;

    invoke-direct {v0, p0}, Lisa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->c:Lisa;

    .line 86
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->m:Landroid/app/NotificationManager;

    .line 87
    new-instance v0, Lbs;

    invoke-direct {v0, p0}, Lbs;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->n:Lbs;

    const v1, 0x108007c

    invoke-virtual {v0, v1}, Lbs;->a(I)Lbs;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    invoke-static {p0, v3, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    .line 92
    invoke-static {}, Liso;->a()Liso;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->f:Liso;

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->f:Liso;

    invoke-virtual {v0, p0}, Liso;->a(Lisp;)V

    .line 95
    new-instance v0, Lisl;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ingest/IngestService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lisl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->a:Lisl;

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->a:Lisl;

    invoke-virtual {v0}, Lisl;->b()Ljava/util/List;

    move-result-object v0

    .line 97
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 98
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/mtp/MtpDevice;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ingest/IngestService;->c(Landroid/mtp/MtpDevice;)V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->a:Lisl;

    invoke-virtual {v0, p0}, Lisl;->a(Lisn;)V

    .line 101
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->a:Lisl;

    invoke-virtual {v0}, Lisl;->a()V

    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/social/ingest/IngestService;->f:Liso;

    invoke-virtual {v0, p0}, Liso;->b(Lisp;)V

    .line 107
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 108
    return-void
.end method
