.class public Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private final A:I

.field private final B:Landroid/view/VelocityTracker;

.field private final C:Landroid/widget/Scroller;

.field private final D:Lnh;

.field private final E:Lnh;

.field private F:F

.field private G:Z

.field private final H:Landroid/util/SparseBooleanArray;

.field private final I:Landroid/graphics/Point;

.field private final J:Landroid/graphics/Point;

.field private K:Landroid/graphics/drawable/Drawable;

.field private L:Z

.field private M:[I

.field private N:Z

.field private O:Ljava/lang/Runnable;

.field private P:Llkc;

.field private a:Z

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private final f:Lgu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgu",
            "<",
            "Llkb;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Llkf;

.field private h:Z

.field private i:Z

.field private j:[I

.field private k:[I

.field private l:I

.field private m:I

.field private n:I

.field private o:Landroid/widget/ListAdapter;

.field private p:Z

.field private q:Z

.field private r:I

.field private final s:Lljz;

.field private t:I

.field private u:I

.field private v:I

.field private w:F

.field private x:F

.field private final y:I

.field private final z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 192
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 196
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x2

    .line 199
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    iput v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b:I

    .line 103
    iput v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    .line 105
    new-instance v0, Lgu;

    invoke-direct {v0}, Lgu;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    .line 115
    new-instance v0, Llkf;

    invoke-direct {v0}, Llkf;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    .line 139
    new-instance v0, Lljz;

    invoke-direct {v0, p0}, Lljz;-><init>(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->s:Lljz;

    .line 158
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->B:Landroid/view/VelocityTracker;

    .line 168
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    .line 174
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    .line 175
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->I:Landroid/graphics/Point;

    .line 176
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v2, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    .line 179
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    .line 182
    new-instance v0, Lljy;

    invoke-direct {v0, p0}, Lljy;-><init>(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->O:Ljava/lang/Runnable;

    .line 201
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->y:I

    .line 203
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->z:I

    .line 204
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->A:I

    .line 205
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    .line 207
    new-instance v0, Lnh;

    invoke-direct {v0, p1}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    .line 208
    new-instance v0, Lnh;

    invoke-direct {v0, p1}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    .line 209
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setWillNotDraw(Z)V

    .line 210
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setClipToPadding(Z)V

    .line 211
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;I)I
    .locals 0

    .prologue
    .line 51
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r:I

    return p1
.end method

.method private a(IZ)Z
    .locals 15

    .prologue
    .line 571
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    move v2, v0

    .line 572
    :goto_0
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 576
    if-nez v2, :cond_2e

    .line 579
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h:Z

    .line 580
    if-lez p1, :cond_7

    .line 581
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, v6}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(II)I

    move-result v1

    .line 582
    const/4 v0, 0x1

    move v3, v1

    .line 587
    :goto_1
    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 588
    if-eqz v0, :cond_8

    move v0, v4

    :goto_2
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v1, :cond_9

    move v1, v0

    :goto_3
    iget-boolean v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v5, :cond_a

    const/4 v5, 0x0

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v8

    const/4 v7, 0x0

    :goto_5
    if-ge v7, v8, :cond_b

    invoke-virtual {p0, v7}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v10

    add-int/2addr v10, v1

    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v11

    add-int/2addr v11, v5

    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v12

    add-int/2addr v12, v1

    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v13

    add-int/2addr v13, v5

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 571
    :cond_1
    const v2, 0x7fffffff

    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    :goto_6
    iget v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-ge v0, v3, :cond_4

    iget-object v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v3, v3, v0

    if-ge v3, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v2, v2, v0

    :cond_2
    iget-object v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v3, v3, v0

    if-le v3, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v1, v1, v0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v0

    :goto_7
    if-lt v2, v4, :cond_6

    sub-int/2addr v0, v3

    if-gt v1, v0, :cond_6

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v0

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_0

    .line 584
    :cond_7
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0, v6}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    add-int/2addr v1, v0

    .line 585
    const/4 v0, 0x0

    move v3, v1

    goto/16 :goto_1

    .line 588
    :cond_8
    neg-int v0, v4

    goto/16 :goto_2

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_a
    move v5, v0

    goto/16 :goto_4

    :cond_b
    iget v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    const/4 v1, 0x0

    :goto_8
    if-ge v1, v5, :cond_c

    iget-object v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v8, v7, v1

    add-int/2addr v8, v0

    aput v8, v7, v1

    iget-object v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v8, v7, v1

    add-int/2addr v8, v0

    aput v8, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 589
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v0

    :goto_9
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f()I

    move-result v5

    int-to-float v5, v5

    iget v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    mul-float/2addr v5, v7

    float-to-int v5, v5

    iget v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    mul-int/2addr v7, v1

    add-int v9, v5, v7

    neg-int v5, v1

    add-int v10, v0, v1

    neg-int v1, v9

    add-int v11, v0, v9

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v8, v0

    :goto_a
    if-ltz v8, :cond_13

    invoke-virtual {p0, v8}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_f

    invoke-virtual {v12}, Landroid/view/View;->getRight()I

    move-result v0

    :goto_b
    iget-boolean v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v7, :cond_10

    invoke-virtual {v12}, Landroid/view/View;->getLeft()I

    move-result v7

    :goto_c
    if-le v7, v5, :cond_d

    if-lt v0, v10, :cond_37

    :cond_d
    if-lt v0, v1, :cond_37

    if-gt v7, v11, :cond_37

    if-ge v0, v5, :cond_37

    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_11

    invoke-virtual {v12}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_d
    if-le v0, v9, :cond_12

    move v0, v5

    :goto_e
    add-int/lit8 v1, v8, -0x1

    move v8, v1

    move v1, v0

    goto :goto_a

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v0

    goto :goto_9

    :cond_f
    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    move-result v0

    goto :goto_b

    :cond_10
    invoke-virtual {v12}, Landroid/view/View;->getTop()I

    move-result v7

    goto :goto_c

    :cond_11
    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_d

    :cond_12
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llka;

    iget-boolean v0, v0, Llka;->g:Z

    if-nez v0, :cond_37

    move v0, v5

    goto :goto_e

    :cond_13
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    :goto_f
    if-ltz v5, :cond_16

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_14

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v0

    :goto_10
    if-le v0, v11, :cond_16

    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i:Z

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    invoke-virtual {p0, v5, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeViewsInLayout(II)V

    :goto_11
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Llkf;->a(Landroid/view/View;I)V

    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_f

    :cond_14
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_10

    :cond_15
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeViewAt(I)V

    goto :goto_11

    :cond_16
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    :goto_12
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_17

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_1a

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v0

    :goto_13
    if-lt v0, v1, :cond_1b

    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->n()V

    :cond_17
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v9

    if-lez v9, :cond_25

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    const v1, 0x7fffffff

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    const/4 v0, 0x0

    move v8, v0

    :goto_14
    if-ge v8, v9, :cond_20

    invoke-virtual {p0, v8}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llka;

    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v1, :cond_1d

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v1

    :goto_15
    iget v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    sub-int v10, v1, v7

    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v1, :cond_1e

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v1

    move v5, v1

    :goto_16
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    iget v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    add-int/2addr v7, v8

    invoke-virtual {v1, v7}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llkb;

    iget v7, v0, Llka;->e:I

    iget v11, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    iget v12, v0, Llka;->b:I

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v11

    add-int/2addr v11, v7

    iget v7, v0, Llka;->e:I

    :goto_17
    if-ge v7, v11, :cond_1f

    iget v12, v0, Llka;->e:I

    sub-int v12, v7, v12

    invoke-virtual {v1, v12}, Llkb;->a(I)I

    move-result v12

    sub-int v12, v10, v12

    iget v13, v0, Llka;->e:I

    sub-int v13, v7, v13

    invoke-virtual {v1, v13}, Llkb;->b(I)I

    move-result v13

    add-int/2addr v13, v5

    iget-object v14, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v14, v14, v7

    if-ge v12, v14, :cond_18

    iget-object v14, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aput v12, v14, v7

    :cond_18
    iget-object v12, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v12, v12, v7

    if-le v13, v12, :cond_19

    iget-object v12, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aput v13, v12, v7

    :cond_19
    add-int/lit8 v7, v7, 0x1

    goto :goto_17

    :cond_1a
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v0

    goto/16 :goto_13

    :cond_1b
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i:Z

    if-eqz v0, :cond_1c

    const/4 v0, 0x0

    const/4 v7, 0x1

    invoke-virtual {p0, v0, v7}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeViewsInLayout(II)V

    :goto_18
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v7

    invoke-virtual {v0, v5, v7}, Llkf;->a(Landroid/view/View;I)V

    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    goto/16 :goto_12

    :cond_1c
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeViewAt(I)V

    goto :goto_18

    :cond_1d
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_15

    :cond_1e
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v1

    move v5, v1

    goto :goto_16

    :cond_1f
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_14

    :cond_20
    const v1, 0x7fffffff

    const/4 v0, 0x0

    :goto_19
    iget v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-ge v0, v5, :cond_22

    iget-object v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v5, v5, v0

    if-ge v5, v1, :cond_21

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v1, v1, v0

    :cond_21
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    :cond_22
    const v0, 0x7fffffff

    if-ne v1, v0, :cond_23

    const/4 v1, 0x0

    :cond_23
    const/4 v0, 0x0

    :goto_1a
    iget v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-ge v0, v5, :cond_25

    iget-object v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v5, v5, v0

    const v7, 0x7fffffff

    if-ne v5, v7, :cond_24

    iget-object v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aput v1, v5, v0

    iget-object v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aput v1, v5, v0

    :cond_24
    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 590
    :cond_25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h:Z

    .line 591
    sub-int v0, v6, v3

    .line 597
    :goto_1b
    if-eqz p2, :cond_27

    .line 598
    invoke-static {p0}, Liu;->a(Landroid/view/View;)I

    move-result v1

    .line 600
    if-eqz v1, :cond_26

    const/4 v3, 0x1

    if-ne v1, v3, :cond_27

    if-nez v2, :cond_27

    .line 603
    :cond_26
    if-lez v0, :cond_27

    .line 604
    if-lez p1, :cond_2f

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    .line 606
    :goto_1c
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v1, :cond_30

    .line 607
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v1

    .line 611
    :goto_1d
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    invoke-virtual {v0, v1}, Lnh;->a(F)Z

    .line 612
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 617
    :cond_27
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v2

    .line 618
    if-lez v2, :cond_31

    const/4 v0, 0x1

    .line 619
    :goto_1e
    if-eqz v0, :cond_29

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    if-nez v1, :cond_29

    .line 620
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 624
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v1, :cond_32

    .line 625
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 626
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v0

    .line 632
    :goto_1f
    if-lt v1, v0, :cond_28

    if-gez p1, :cond_33

    :cond_28
    const/4 v0, 0x1

    .line 634
    :cond_29
    :goto_20
    if-eqz v0, :cond_2b

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    add-int/2addr v1, v2

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r:I

    if-ne v1, v3, :cond_2b

    .line 635
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 640
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v1, :cond_34

    .line 641
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    .line 642
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingRight()I

    move-result v1

    .line 643
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v0

    .line 649
    :goto_21
    sub-int/2addr v0, v1

    if-gt v2, v0, :cond_2a

    if-lez p1, :cond_35

    :cond_2a
    const/4 v0, 0x1

    .line 652
    :cond_2b
    :goto_22
    if-eqz v0, :cond_2c

    .line 653
    invoke-direct/range {p0 .. p1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k(I)V

    .line 656
    :cond_2c
    if-eqz p1, :cond_2d

    if-eqz v4, :cond_36

    :cond_2d
    const/4 v0, 0x1

    :goto_23
    return v0

    .line 594
    :cond_2e
    const/4 v4, 0x0

    move v0, v6

    goto :goto_1b

    .line 604
    :cond_2f
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    goto :goto_1c

    .line 609
    :cond_30
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v1

    goto :goto_1d

    .line 618
    :cond_31
    const/4 v0, 0x0

    goto :goto_1e

    .line 628
    :cond_32
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    .line 629
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v0

    goto :goto_1f

    .line 632
    :cond_33
    const/4 v0, 0x0

    goto :goto_20

    .line 645
    :cond_34
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 646
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingBottom()I

    move-result v1

    .line 647
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v0

    goto :goto_21

    .line 649
    :cond_35
    const/4 v0, 0x0

    goto :goto_22

    .line 656
    :cond_36
    const/4 v0, 0x0

    goto :goto_23

    :cond_37
    move v0, v1

    goto/16 :goto_e
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->N:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q:Z

    return p1
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Landroid/util/SparseBooleanArray;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Llkf;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    return-object v0
.end method

.method private d(II)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2336
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->L:Z

    if-nez v0, :cond_1

    .line 2373
    :cond_0
    :goto_0
    return-void

    .line 2340
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->I:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, p1

    .line 2341
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->I:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v2, p2

    .line 2342
    mul-int/2addr v0, v0

    mul-int/2addr v2, v2

    add-int/2addr v0, v2

    .line 2343
    iget v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->y:I

    iget v4, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->y:I

    mul-int/2addr v2, v4

    if-ge v0, v2, :cond_0

    .line 2347
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d()I

    move-result v4

    .line 2353
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v0, v3

    :goto_1
    if-ltz v2, :cond_4

    .line 2354
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2355
    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    invoke-virtual {v5, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2356
    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    aget v6, v6, v3

    if-lt p1, v6, :cond_2

    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    aget v6, v6, v3

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    if-gt p1, v6, :cond_2

    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    aget v6, v6, v1

    if-lt p2, v6, :cond_2

    iget-object v6, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    aget v6, v6, v1

    .line 2357
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v5, v6

    if-gt p2, v5, :cond_2

    .line 2358
    add-int v0, v2, v4

    .line 2362
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2363
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j(I)V

    :goto_2
    move v0, v1

    .line 2353
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 2365
    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i(I)V

    goto :goto_2

    .line 2370
    :cond_4
    if-eqz v0, :cond_0

    .line 2371
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->invalidate()V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o()V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->p:Z

    return v0
.end method

.method public static synthetic g(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)Lgu;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)V
    .locals 4

    .prologue
    .line 51
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Llkf;->a(Landroid/view/View;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeAllViewsInLayout()V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeAllViews()V

    goto :goto_1
.end method

.method public static synthetic i(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)[I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    return v0
.end method

.method private k(I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2056
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->P:Llkc;

    if-eqz v0, :cond_0

    .line 2057
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->P:Llkc;

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    .line 2058
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v4

    iget v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r:I

    move-object v1, p0

    move v6, p1

    .line 2057
    invoke-interface/range {v0 .. v6}, Llkc;->a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;IIIII)V

    .line 2060
    :cond_0
    invoke-virtual {p0, v7, v7, v7, v7}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->onScrollChanged(IIII)V

    .line 2061
    return-void
.end method

.method public static synthetic k(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;)[I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    return-object v0
.end method

.method private l(I)V
    .locals 1

    .prologue
    .line 2071
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->u:I

    if-eq p1, v0, :cond_0

    .line 2072
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->u:I

    .line 2073
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->P:Llkc;

    if-eqz v0, :cond_0

    .line 2074
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->P:Llkc;

    invoke-interface {v0, p0, p1}, Llkc;->a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;I)V

    .line 2077
    :cond_0
    return-void
.end method

.method private m()V
    .locals 21

    .prologue
    .line 1054
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v2

    if-nez v2, :cond_1

    .line 1096
    :cond_0
    :goto_0
    return-void

    .line 1058
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 1060
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_8

    .line 1061
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x0

    .line 1066
    :goto_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-eq v2, v3, :cond_2

    .line 1067
    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    .line 1071
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    .line 1072
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    array-length v3, v3

    if-eq v3, v2, :cond_4

    .line 1073
    :cond_3
    new-array v3, v2, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    .line 1074
    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    .line 1075
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v2

    .line 1076
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->n:I

    add-int/2addr v2, v3

    .line 1077
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    invoke-static {v3, v2}, Ljava/util/Arrays;->fill([II)V

    .line 1078
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    invoke-static {v3, v2}, Ljava/util/Arrays;->fill([II)V

    .line 1079
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v2}, Lgu;->c()V

    .line 1080
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i:Z

    if-eqz v2, :cond_a

    .line 1081
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeAllViewsInLayout()V

    .line 1085
    :goto_3
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->n:I

    .line 1088
    :cond_4
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h:Z

    .line 1089
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q:Z

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v2

    move v3, v2

    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    move/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f()I

    move-result v17

    const/4 v12, -0x1

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    const/high16 v4, -0x80000000

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v18

    const/4 v2, 0x0

    move v14, v2

    :goto_5
    move/from16 v0, v18

    if-ge v14, v0, :cond_18

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Llka;

    iget v13, v2, Llka;->e:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    add-int v10, v4, v14

    if-nez v15, :cond_5

    invoke-virtual {v5}, Landroid/view/View;->isLayoutRequested()Z

    move-result v4

    if-eqz v4, :cond_c

    :cond_5
    const/4 v4, 0x1

    move v6, v4

    :goto_6
    if-eqz v15, :cond_37

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v5}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v4

    if-eq v4, v5, :cond_36

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeViewAt(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v14}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->addView(Landroid/view/View;I)V

    :goto_7
    iget v5, v2, Llka;->b:I

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Llka;

    iget v7, v2, Llka;->b:I

    if-eq v7, v5, :cond_6

    const-string v5, "ColumnGridView"

    const-string v7, "Span changed!"

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iput v13, v2, Llka;->e:I

    :goto_8
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    iget v7, v2, Llka;->b:I

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v19

    mul-int v5, v17, v19

    add-int/lit8 v7, v19, -0x1

    mul-int v7, v7, v16

    add-int/2addr v5, v7

    if-eqz v6, :cond_7

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v6, :cond_f

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget v6, v2, Llka;->width:I

    const/4 v7, -0x2

    if-ne v6, v7, :cond_d

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v20, v5

    move v5, v2

    move/from16 v2, v20

    :goto_9
    invoke-virtual {v4, v5, v2}, Landroid/view/View;->measure(II)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v2, v2, v13

    const/high16 v5, -0x80000000

    if-le v2, v5, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v2, v2, v13

    add-int v5, v2, v16

    :goto_a
    const/4 v2, 0x1

    move/from16 v0, v19

    if-le v0, v2, :cond_14

    add-int/lit8 v2, v13, 0x1

    move v6, v2

    :goto_b
    add-int v2, v13, v19

    if-ge v6, v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v2, v2, v6

    add-int v2, v2, v16

    if-le v2, v5, :cond_35

    :goto_c
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v2

    goto :goto_b

    .line 1063
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x0

    goto/16 :goto_1

    .line 1075
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v2

    goto/16 :goto_2

    .line 1083
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeAllViews()V

    goto/16 :goto_3

    .line 1089
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v2

    move v3, v2

    goto/16 :goto_4

    :cond_c
    const/4 v4, 0x0

    move v6, v4

    goto/16 :goto_6

    :cond_d
    iget v6, v2, Llka;->width:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_e

    iget v6, v2, Llka;->a:I

    add-int/lit8 v6, v6, -0x1

    mul-int v6, v6, v16

    iget v2, v2, Llka;->a:I

    mul-int v2, v2, v17

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    mul-float/2addr v2, v7

    float-to-int v2, v2

    add-int/2addr v2, v6

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v20, v5

    move v5, v2

    move/from16 v2, v20

    goto :goto_9

    :cond_e
    iget v2, v2, Llka;->width:I

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v20, v5

    move v5, v2

    move/from16 v2, v20

    goto :goto_9

    :cond_f
    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget v6, v2, Llka;->height:I

    const/4 v7, -0x2

    if-ne v6, v7, :cond_10

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_9

    :cond_10
    iget v6, v2, Llka;->height:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_11

    iget v6, v2, Llka;->a:I

    add-int/lit8 v6, v6, -0x1

    mul-int v6, v6, v16

    iget v2, v2, Llka;->a:I

    mul-int v2, v2, v17

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    mul-float/2addr v2, v7

    float-to-int v2, v2

    add-int/2addr v2, v6

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_9

    :cond_11
    iget v2, v2, Llka;->height:I

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_9

    :cond_12
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_13

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v5

    goto/16 :goto_a

    :cond_13
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v5

    goto/16 :goto_a

    :cond_14
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_15

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    move v6, v2

    :goto_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_16

    add-int v2, v5, v6

    add-int v7, v17, v16

    mul-int/2addr v7, v13

    add-int/2addr v7, v3

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v7

    move v9, v2

    :goto_e
    invoke-virtual {v4, v5, v7, v2, v8}, Landroid/view/View;->layout(IIII)V

    move v2, v13

    :goto_f
    add-int v4, v13, v19

    if-ge v2, v4, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aput v9, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    :cond_15
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    move v6, v2

    goto :goto_d

    :cond_16
    add-int v2, v17, v16

    mul-int/2addr v2, v13

    add-int v7, v3, v2

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v7

    add-int v8, v5, v6

    move v9, v8

    move/from16 v20, v5

    move v5, v7

    move/from16 v7, v20

    goto :goto_e

    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v2, v10}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Llkb;

    if-eqz v2, :cond_34

    iget v4, v2, Llkb;->c:I

    if-eq v4, v6, :cond_34

    iput v6, v2, Llkb;->c:I

    move v4, v10

    :goto_10
    if-eqz v2, :cond_33

    iget v5, v2, Llkb;->d:I

    move/from16 v0, v19

    if-eq v5, v0, :cond_33

    move/from16 v0, v19

    iput v0, v2, Llkb;->d:I

    move v2, v10

    :goto_11
    add-int/lit8 v5, v14, 0x1

    move v14, v5

    move v11, v2

    move v12, v4

    goto/16 :goto_5

    :cond_18
    const/4 v2, 0x0

    :goto_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-ge v2, v3, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v3, v3, v2

    const/high16 v4, -0x80000000

    if-ne v3, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v4, v4, v2

    aput v4, v3, v2

    :cond_19
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    :cond_1a
    if-gez v12, :cond_1b

    if-ltz v11, :cond_20

    :cond_1b
    if-ltz v12, :cond_1c

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d(I)V

    :cond_1c
    if-ltz v11, :cond_1d

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->e(I)V

    :cond_1d
    const/4 v2, 0x0

    move v5, v2

    :goto_13
    move/from16 v0, v18

    if-ge v5, v0, :cond_20

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    add-int v4, v2, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Llka;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v3, v4}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llkb;

    if-nez v3, :cond_1e

    new-instance v3, Llkb;

    invoke-direct {v3}, Llkb;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v7, v4, v3}, Lgu;->b(ILjava/lang/Object;)V

    :cond_1e
    iget v4, v2, Llka;->e:I

    iput v4, v3, Llkb;->a:I

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v4, :cond_1f

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v4

    :goto_14
    iput v4, v3, Llkb;->c:I

    iget-wide v6, v2, Llka;->f:J

    iput-wide v6, v3, Llkb;->b:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    iget v2, v2, Llka;->b:I

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, v3, Llkb;->d:I

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_13

    :cond_1f
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v4

    goto :goto_14

    .line 1090
    :cond_20
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i:Z

    if-eqz v2, :cond_30

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    new-array v10, v2, [I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f()I

    move-result v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a([I)I

    move-result v5

    const/4 v2, 0x0

    move v8, v2

    :goto_15
    if-ge v8, v9, :cond_30

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r:I

    if-ge v8, v2, :cond_30

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Llka;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    iget v4, v2, Llka;->b:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v13

    mul-int v3, v12, v13

    add-int/lit8 v4, v13, -0x1

    mul-int/2addr v4, v11

    add-int v6, v3, v4

    const/4 v3, 0x1

    if-le v13, v3, :cond_24

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v13, v10}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(II[I)Llkb;

    move-result-object v4

    iget v5, v4, Llkb;->a:I

    :goto_16
    const/4 v3, 0x0

    if-nez v4, :cond_25

    new-instance v4, Llkb;

    invoke-direct {v4}, Llkb;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v14, v8, v4}, Lgu;->b(ILjava/lang/Object;)V

    iput v5, v4, Llkb;->a:I

    iput v13, v4, Llkb;->d:I

    :goto_17
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->p:Z

    if-eqz v14, :cond_21

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    invoke-interface {v14, v8}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v14

    iput-wide v14, v4, Llkb;->b:J

    iput-wide v14, v2, Llka;->f:J

    :cond_21
    iput v5, v2, Llka;->e:I

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v14, :cond_29

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v6, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    iget v14, v2, Llka;->width:I

    const/4 v15, -0x2

    if-ne v14, v15, :cond_27

    const/4 v2, 0x0

    const/4 v14, 0x0

    invoke-static {v2, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v20, v6

    move v6, v2

    move/from16 v2, v20

    :goto_18
    invoke-virtual {v7, v6, v2}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_2c

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    move v7, v2

    :goto_19
    if-nez v3, :cond_22

    iget v2, v4, Llkb;->c:I

    if-eq v7, v2, :cond_23

    iget v2, v4, Llkb;->c:I

    if-lez v2, :cond_23

    :cond_22
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->e(I)V

    :cond_23
    iput v7, v4, Llkb;->c:I

    const/4 v2, 0x1

    if-le v13, v2, :cond_2d

    aget v3, v10, v5

    add-int/lit8 v2, v5, 0x1

    move v6, v2

    :goto_1a
    add-int v2, v5, v13

    if-ge v6, v2, :cond_2e

    aget v2, v10, v6

    if-le v2, v3, :cond_32

    :goto_1b
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v3, v2

    goto :goto_1a

    :cond_24
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v3, v8}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llkb;

    move-object v4, v3

    goto/16 :goto_16

    :cond_25
    iget v14, v4, Llkb;->d:I

    if-eq v13, v14, :cond_26

    iput v13, v4, Llkb;->d:I

    iput v5, v4, Llkb;->a:I

    const/4 v3, 0x1

    goto :goto_17

    :cond_26
    iget v5, v4, Llkb;->a:I

    goto :goto_17

    :cond_27
    iget v14, v2, Llka;->width:I

    const/4 v15, -0x1

    if-ne v14, v15, :cond_28

    iget v14, v2, Llka;->a:I

    add-int/lit8 v14, v14, -0x1

    mul-int/2addr v14, v11

    iget v2, v2, Llka;->a:I

    mul-int/2addr v2, v12

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    mul-float/2addr v2, v15

    float-to-int v2, v2

    add-int/2addr v2, v14

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v2, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v20, v6

    move v6, v2

    move/from16 v2, v20

    goto :goto_18

    :cond_28
    iget v2, v2, Llka;->width:I

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v2, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v20, v6

    move v6, v2

    move/from16 v2, v20

    goto :goto_18

    :cond_29
    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v6, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    iget v14, v2, Llka;->height:I

    const/4 v15, -0x2

    if-ne v14, v15, :cond_2a

    const/4 v2, 0x0

    const/4 v14, 0x0

    invoke-static {v2, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_18

    :cond_2a
    iget v14, v2, Llka;->height:I

    const/4 v15, -0x1

    if-ne v14, v15, :cond_2b

    iget v14, v2, Llka;->a:I

    add-int/lit8 v14, v14, -0x1

    mul-int/2addr v14, v11

    iget v2, v2, Llka;->a:I

    mul-int/2addr v2, v12

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    mul-float/2addr v2, v15

    float-to-int v2, v2

    add-int/2addr v2, v14

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v2, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_18

    :cond_2b
    iget v2, v2, Llka;->height:I

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v2, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_18

    :cond_2c
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    move v7, v2

    goto/16 :goto_19

    :cond_2d
    aget v3, v10, v5

    :cond_2e
    add-int v2, v3, v7

    add-int v3, v2, v11

    move v2, v5

    :goto_1c
    add-int v6, v5, v13

    if-ge v2, v6, :cond_2f

    sub-int v6, v2, v5

    invoke-virtual {v4, v6}, Llkb;->b(I)I

    move-result v6

    add-int/2addr v6, v3

    aput v6, v10, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1c

    :cond_2f
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a([I)I

    move-result v5

    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto/16 :goto_15

    .line 1091
    :cond_30
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v3

    add-int/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(II)I

    .line 1092
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    add-int/lit8 v3, v2, -0x1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->n:I

    if-lez v2, :cond_31

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->n:I

    :goto_1d
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(II)I

    .line 1093
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->n()V

    .line 1094
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h:Z

    .line 1095
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q:Z

    goto/16 :goto_0

    .line 1092
    :cond_31
    const/4 v2, 0x0

    goto :goto_1d

    :cond_32
    move v2, v3

    goto/16 :goto_1b

    :cond_33
    move v2, v11

    goto/16 :goto_11

    :cond_34
    move v4, v12

    goto/16 :goto_10

    :cond_35
    move v2, v5

    goto/16 :goto_c

    :cond_36
    move-object v4, v5

    goto/16 :goto_7

    :cond_37
    move-object v4, v5

    goto/16 :goto_8
.end method

.method private n()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1124
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    neg-int v2, v1

    .line 1127
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    .line 1128
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 1129
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1130
    iget-boolean v4, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 1131
    :goto_1
    if-ge v0, v2, :cond_1

    .line 1132
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    .line 1128
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1130
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    goto :goto_1

    .line 1136
    :cond_1
    return-void
.end method

.method private o()V
    .locals 1

    .prologue
    .line 1955
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v0}, Lgu;->c()V

    .line 1956
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeAllViews()V

    .line 1959
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->p()V

    .line 1962
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    invoke-virtual {v0}, Llkf;->a()V

    .line 1963
    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1970
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    .line 1971
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1972
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    array-length v1, v1

    if-eq v1, v0, :cond_1

    .line 1973
    :cond_0
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    .line 1974
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    .line 1977
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v0

    .line 1978
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 1979
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 1982
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->setFinalX(I)V

    .line 1983
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->setFinalY(I)V

    .line 1984
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1985
    invoke-virtual {p0, v2, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->scrollTo(II)V

    .line 1988
    iput v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    .line 1989
    iput v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    .line 1990
    iput v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->n:I

    .line 1991
    return-void
.end method

.method private q()V
    .locals 1

    .prologue
    .line 2376
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->N:Z

    if-eqz v0, :cond_0

    .line 2377
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->invalidate()V

    .line 2379
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->N:Z

    .line 2380
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->O:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2381
    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    .line 2878
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    .line 2880
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    invoke-virtual {v0}, Lnh;->c()Z

    move-result v0

    .line 2882
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    .line 2883
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    invoke-virtual {v1}, Lnh;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 2885
    if-eqz v0, :cond_0

    .line 2886
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 2888
    :cond_0
    return-void
.end method


# virtual methods
.method final a(II)I
    .locals 19

    .prologue
    .line 1336
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v2

    move v4, v2

    .line 1337
    :goto_0
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    .line 1338
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f()I

    move-result v13

    .line 1339
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v2

    move v5, v2

    .line 1340
    :goto_1
    sub-int v14, v5, p2

    .line 1341
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g()I

    move-result v7

    .line 1343
    const/4 v3, 0x1

    .line 1345
    :goto_2
    if-ltz v7, :cond_1e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v2, v2, v7

    if-gt v2, v14, :cond_0

    if-nez v3, :cond_1e

    :cond_0
    if-ltz p1, :cond_1e

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r:I

    move/from16 v0, p1

    if-ge v0, v2, :cond_1e

    .line 1347
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v15

    .line 1348
    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Llka;

    .line 1350
    invoke-virtual {v15}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    move-object/from16 v0, p0

    if-eq v3, v0, :cond_1

    .line 1351
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i:Z

    if-eqz v3, :cond_5

    .line 1352
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v3, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1358
    :cond_1
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    iget v6, v2, Llka;->b:I

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v16

    .line 1359
    mul-int v3, v13, v16

    add-int/lit8 v6, v16, -0x1

    mul-int/2addr v6, v12

    add-int v17, v3, v6

    .line 1362
    const/4 v3, 0x1

    move/from16 v0, v16

    if-le v0, v3, :cond_f

    .line 1363
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llkb;

    if-nez v3, :cond_6

    new-instance v3, Llkb;

    invoke-direct {v3}, Llkb;-><init>()V

    move/from16 v0, v16

    iput v0, v3, Llkb;->d:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    move/from16 v0, p1

    invoke-virtual {v6, v0, v3}, Lgu;->b(ILjava/lang/Object;)V

    :cond_2
    const/4 v10, -0x1

    const/high16 v8, -0x80000000

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    sub-int v9, v6, v16

    :goto_4
    if-ltz v9, :cond_8

    const v7, 0x7fffffff

    move v11, v9

    :goto_5
    add-int v6, v9, v16

    if-ge v11, v6, :cond_7

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v6, v6, v11

    if-ge v6, v7, :cond_23

    :goto_6
    add-int/lit8 v7, v11, 0x1

    move v11, v7

    move v7, v6

    goto :goto_5

    .line 1336
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v2

    move v4, v2

    goto/16 :goto_0

    .line 1339
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v2

    move v5, v2

    goto/16 :goto_1

    .line 1354
    :cond_5
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->addView(Landroid/view/View;I)V

    goto :goto_3

    .line 1363
    :cond_6
    iget v6, v3, Llkb;->d:I

    move/from16 v0, v16

    if-eq v6, v0, :cond_2

    new-instance v2, Ljava/lang/IllegalStateException;

    iget v3, v3, Llkb;->d:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x70

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Invalid LayoutRecord! Record had span="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " but caller requested span="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_7
    if-le v7, v8, :cond_22

    move v6, v9

    :goto_7
    add-int/lit8 v9, v9, -0x1

    move v8, v7

    move v10, v6

    goto :goto_4

    :cond_8
    iput v10, v3, Llkb;->a:I

    const/4 v6, 0x0

    :goto_8
    move/from16 v0, v16

    if-ge v6, v0, :cond_b

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    add-int v9, v6, v10

    aget v7, v7, v9

    sub-int/2addr v7, v8

    iget-object v9, v3, Llkb;->e:[I

    if-nez v9, :cond_9

    if-eqz v7, :cond_a

    :cond_9
    invoke-virtual {v3}, Llkb;->a()V

    iget-object v9, v3, Llkb;->e:[I

    shl-int/lit8 v11, v6, 0x1

    add-int/lit8 v11, v11, 0x1

    aput v7, v9, v11

    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    .line 1364
    :cond_b
    iget v7, v3, Llkb;->a:I

    move-object v6, v3

    .line 1369
    :goto_9
    const/4 v3, 0x0

    .line 1370
    if-nez v6, :cond_10

    .line 1371
    new-instance v6, Llkb;

    invoke-direct {v6}, Llkb;-><init>()V

    .line 1372
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    move/from16 v0, p1

    invoke-virtual {v8, v0, v6}, Lgu;->b(ILjava/lang/Object;)V

    .line 1373
    iput v7, v6, Llkb;->a:I

    .line 1374
    move/from16 v0, v16

    iput v0, v6, Llkb;->d:I

    .line 1383
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->p:Z

    if-eqz v8, :cond_c

    .line 1384
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    move/from16 v0, p1

    invoke-interface {v8, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v8

    .line 1385
    iput-wide v8, v6, Llkb;->b:J

    .line 1386
    iput-wide v8, v2, Llka;->f:J

    .line 1389
    :cond_c
    iput v7, v2, Llka;->e:I

    .line 1393
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v8, :cond_14

    .line 1394
    const/high16 v8, 0x40000000    # 2.0f

    move/from16 v0, v17

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 1396
    iget v9, v2, Llka;->width:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_12

    .line 1397
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 1418
    :goto_b
    invoke-virtual {v15, v9, v8}, Landroid/view/View;->measure(II)V

    .line 1420
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v8, :cond_17

    .line 1421
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    move v10, v8

    .line 1422
    :goto_c
    if-nez v3, :cond_d

    iget v3, v6, Llkb;->c:I

    if-eq v10, v3, :cond_e

    iget v3, v6, Llkb;->c:I

    if-lez v3, :cond_e

    .line 1423
    :cond_d
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d(I)V

    .line 1425
    :cond_e
    iput v10, v6, Llkb;->c:I

    .line 1428
    const/4 v3, 0x1

    move/from16 v0, v16

    if-le v0, v3, :cond_18

    .line 1429
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v8, v3, v7

    .line 1430
    add-int/lit8 v3, v7, 0x1

    move v9, v3

    :goto_d
    add-int v3, v7, v16

    if-ge v9, v3, :cond_19

    .line 1431
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v3, v3, v9

    .line 1432
    if-ge v3, v8, :cond_21

    .line 1430
    :goto_e
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move v8, v3

    goto :goto_d

    .line 1366
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llkb;

    move-object v6, v3

    goto/16 :goto_9

    .line 1375
    :cond_10
    iget v8, v6, Llkb;->d:I

    move/from16 v0, v16

    if-eq v0, v8, :cond_11

    .line 1376
    move/from16 v0, v16

    iput v0, v6, Llkb;->d:I

    .line 1377
    iput v7, v6, Llkb;->a:I

    .line 1378
    const/4 v3, 0x1

    goto/16 :goto_a

    .line 1380
    :cond_11
    iget v7, v6, Llkb;->a:I

    goto/16 :goto_a

    .line 1398
    :cond_12
    iget v9, v2, Llka;->width:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_13

    .line 1399
    iget v9, v2, Llka;->a:I

    add-int/lit8 v9, v9, -0x1

    mul-int/2addr v9, v12

    .line 1400
    iget v10, v2, Llka;->a:I

    mul-int/2addr v10, v13

    int-to-float v10, v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    mul-float/2addr v10, v11

    float-to-int v10, v10

    add-int/2addr v9, v10

    .line 1401
    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    goto :goto_b

    .line 1403
    :cond_13
    iget v9, v2, Llka;->width:I

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    goto/16 :goto_b

    .line 1406
    :cond_14
    const/high16 v8, 0x40000000    # 2.0f

    move/from16 v0, v17

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 1408
    iget v8, v2, Llka;->height:I

    const/4 v10, -0x2

    if-ne v8, v10, :cond_15

    .line 1409
    const/4 v8, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    goto/16 :goto_b

    .line 1410
    :cond_15
    iget v8, v2, Llka;->height:I

    const/4 v10, -0x1

    if-ne v8, v10, :cond_16

    .line 1411
    iget v8, v2, Llka;->a:I

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v8, v12

    .line 1412
    iget v10, v2, Llka;->a:I

    mul-int/2addr v10, v13

    int-to-float v10, v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    mul-float/2addr v10, v11

    float-to-int v10, v10

    add-int/2addr v8, v10

    .line 1413
    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    goto/16 :goto_b

    .line 1415
    :cond_16
    iget v8, v2, Llka;->height:I

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    goto/16 :goto_b

    .line 1421
    :cond_17
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    move v10, v8

    goto/16 :goto_c

    .line 1438
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v8, v3, v7

    .line 1445
    :cond_19
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v3, :cond_1a

    .line 1447
    sub-int v3, v8, v10

    .line 1448
    add-int v9, v13, v12

    mul-int/2addr v9, v7

    add-int/2addr v9, v4

    .line 1449
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v10, v9

    move v11, v3

    .line 1458
    :goto_f
    invoke-virtual {v15, v3, v9, v8, v10}, Landroid/view/View;->layout(IIII)V

    move v3, v7

    .line 1460
    :goto_10
    add-int v8, v7, v16

    if-ge v3, v8, :cond_1b

    .line 1461
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    sub-int v9, v3, v7

    invoke-virtual {v6, v9}, Llkb;->a(I)I

    move-result v9

    sub-int v9, v11, v9

    sub-int/2addr v9, v12

    aput v9, v8, v3

    .line 1460
    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    .line 1453
    :cond_1a
    sub-int v10, v8, v10

    .line 1454
    add-int v3, v13, v12

    mul-int/2addr v3, v7

    add-int v9, v4, v3

    .line 1455
    invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v9

    move v11, v10

    move/from16 v18, v9

    move v9, v10

    move v10, v8

    move v8, v3

    move/from16 v3, v18

    .line 1456
    goto :goto_f

    .line 1464
    :cond_1b
    iget-boolean v3, v2, Llka;->g:Z

    .line 1465
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    const/4 v6, 0x0

    aget v6, v2, v6

    .line 1466
    const/4 v2, 0x1

    :goto_11
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-ge v2, v7, :cond_1d

    if-eqz v3, :cond_1d

    .line 1467
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v7, v7, v2

    if-eq v7, v6, :cond_1c

    .line 1468
    const/4 v3, 0x0

    .line 1466
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    goto :goto_11

    .line 1472
    :cond_1d
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g()I

    move-result v7

    .line 1473
    add-int/lit8 v2, p1, -0x1

    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    move/from16 p1, v2

    .line 1474
    goto/16 :goto_2

    .line 1476
    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v3

    .line 1477
    const/4 v2, 0x0

    :goto_12
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-ge v2, v4, :cond_20

    .line 1478
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v4, v4, v2

    if-ge v4, v3, :cond_1f

    .line 1479
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v3, v3, v2

    .line 1477
    :cond_1f
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 1482
    :cond_20
    sub-int v2, v5, v3

    return v2

    :cond_21
    move v3, v8

    goto/16 :goto_e

    :cond_22
    move v7, v8

    move v6, v10

    goto/16 :goto_7

    :cond_23
    move v6, v7

    goto/16 :goto_6
.end method

.method final a([I)I
    .locals 5

    .prologue
    .line 1817
    const/4 v3, -0x1

    .line 1818
    const v1, 0x7fffffff

    .line 1820
    iget v4, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    .line 1821
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    .line 1822
    aget v0, p1, v2

    .line 1823
    if-ge v0, v1, :cond_1

    move v1, v2

    .line 1821
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 1828
    :cond_0
    return v3

    :cond_1
    move v0, v1

    move v1, v3

    goto :goto_1
.end method

.method final a(ILandroid/view/View;)Landroid/view/View;
    .locals 6

    .prologue
    .line 1877
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    invoke-virtual {v0, p1}, Llkf;->b(I)Landroid/view/View;

    move-result-object v0

    .line 1878
    if-eqz v0, :cond_0

    .line 1915
    :goto_0
    return-object v0

    .line 1883
    :cond_0
    if-eqz p2, :cond_4

    .line 1884
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llka;

    iget v0, v0, Llka;->d:I

    .line 1885
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v2

    .line 1886
    if-ne v0, v2, :cond_5

    .line 1889
    :goto_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1, p2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1891
    if-eq v1, p2, :cond_1

    if-eqz p2, :cond_1

    .line 1893
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v3

    invoke-virtual {v0, p2, v3}, Llkf;->a(Landroid/view/View;I)V

    .line 1896
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1898
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eq v3, p0, :cond_3

    .line 1899
    if-nez v0, :cond_6

    .line 1900
    const-string v0, "ColumnGridView"

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x5a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "view at position "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " doesn\'t have layout parameters;using default layout paramters"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1902
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i()Llka;

    move-result-object v0

    .line 1908
    :cond_2
    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1911
    :cond_3
    check-cast v0, Llka;

    .line 1912
    iput p1, v0, Llka;->c:I

    .line 1913
    iput v2, v0, Llka;->d:I

    move-object v0, v1

    .line 1915
    goto :goto_0

    .line 1884
    :cond_4
    const/4 v0, -0x1

    goto :goto_1

    .line 1886
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    .line 1887
    invoke-virtual {v0, v2}, Llkf;->c(I)Landroid/view/View;

    move-result-object p2

    goto :goto_2

    .line 1903
    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1904
    const-string v3, "ColumnGridView"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x74

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "view at position "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " doesn\'t have layout parameters of type ColumnGridView.LayoutParams; wrapping parameters"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1906
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/view/ViewGroup$LayoutParams;)Llka;

    move-result-object v0

    goto :goto_3
.end method

.method protected a(Landroid/view/ViewGroup$LayoutParams;)Llka;
    .locals 2

    .prologue
    .line 2088
    new-instance v1, Llka;

    invoke-direct {v1, p1}, Llka;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2089
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v1, Llka;->h:I

    .line 2090
    return-object v1

    .line 2089
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method final a(II[I)Llkb;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 1832
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v0, p1}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llkb;

    .line 1833
    if-nez v0, :cond_1

    .line 1834
    new-instance v0, Llkb;

    invoke-direct {v0}, Llkb;-><init>()V

    .line 1835
    iput p2, v0, Llkb;->d:I

    .line 1836
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v1, p1, v0}, Lgu;->b(ILjava/lang/Object;)V

    .line 1841
    :cond_0
    const/4 v5, -0x1

    .line 1842
    const v3, 0x7fffffff

    .line 1844
    iget v8, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    move v4, v6

    .line 1845
    :goto_0
    sub-int v1, v8, p2

    if-gt v4, v1, :cond_3

    .line 1846
    const/high16 v2, -0x80000000

    move v7, v4

    .line 1847
    :goto_1
    add-int v1, v4, p2

    if-ge v7, v1, :cond_2

    .line 1848
    aget v1, p3, v7

    .line 1849
    if-le v1, v2, :cond_8

    .line 1847
    :goto_2
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v2, v1

    goto :goto_1

    .line 1837
    :cond_1
    iget v1, v0, Llkb;->d:I

    if-eq v1, p2, :cond_0

    .line 1838
    new-instance v1, Ljava/lang/IllegalStateException;

    iget v0, v0, Llkb;->d:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x70

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid LayoutRecord! Record had span="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " but caller requested span="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for position="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1853
    :cond_2
    if-ge v2, v3, :cond_7

    move v1, v4

    .line 1845
    :goto_3
    add-int/lit8 v4, v4, 0x1

    move v3, v2

    move v5, v1

    goto :goto_0

    .line 1859
    :cond_3
    iput v5, v0, Llkb;->a:I

    move v1, v6

    .line 1861
    :goto_4
    if-ge v1, p2, :cond_6

    .line 1862
    add-int v2, v1, v5

    aget v2, p3, v2

    sub-int v2, v3, v2

    iget-object v4, v0, Llkb;->e:[I

    if-nez v4, :cond_4

    if-eqz v2, :cond_5

    :cond_4
    invoke-virtual {v0}, Llkb;->a()V

    iget-object v4, v0, Llkb;->e:[I

    shl-int/lit8 v6, v1, 0x1

    aput v2, v4, v6

    .line 1861
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1865
    :cond_6
    return-object v0

    :cond_7
    move v2, v3

    move v1, v5

    goto :goto_3

    :cond_8
    move v1, v2

    goto :goto_2
.end method

.method public a()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q()V

    .line 215
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 236
    if-gtz p1, :cond_0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 237
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "colCount must be at least 1 - received "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-eq p1, v0, :cond_2

    const/4 v0, 0x1

    .line 241
    :goto_0
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b:I

    iput p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    .line 242
    if-eqz v0, :cond_1

    .line 243
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m()V

    .line 245
    :cond_1
    return-void

    .line 240
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1923
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 1924
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->s:Lljz;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1929
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o()V

    .line 1932
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    .line 1933
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q:Z

    .line 1934
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r:I

    .line 1935
    if-eqz p1, :cond_3

    .line 1936
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->s:Lljz;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1937
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Llkf;->a(I)V

    .line 1938
    invoke-interface {p1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->p:Z

    .line 1943
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->L:Z

    if-eqz v0, :cond_1

    .line 1944
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j()V

    .line 1947
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m()V

    .line 1948
    return-void

    :cond_2
    move v0, v1

    .line 1934
    goto :goto_0

    .line 1940
    :cond_3
    iput-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->p:Z

    goto :goto_1
.end method

.method public a(Llkc;)V
    .locals 1

    .prologue
    .line 2046
    iput-object p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->P:Llkc;

    .line 2047
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k(I)V

    .line 2048
    return-void
.end method

.method public a(Llkg;)V
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g:Llkf;

    iput-object p1, v0, Llkf;->a:Llkg;

    .line 700
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 251
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->G:Z

    .line 252
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    return v0
.end method

.method final b(II)I
    .locals 19

    .prologue
    .line 1496
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v2

    move v4, v2

    .line 1497
    :goto_0
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    .line 1498
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f()I

    move-result v12

    .line 1499
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_5

    .line 1500
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    move v5, v2

    .line 1501
    :goto_1
    add-int v13, v5, p2

    .line 1502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a([I)I

    move-result v7

    .line 1505
    :goto_2
    if-ltz v7, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v2, v2, v7

    if-ge v2, v13, :cond_14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r:I

    move/from16 v0, p1

    if-ge v0, v2, :cond_14

    .line 1506
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v14

    .line 1507
    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Llka;

    .line 1509
    invoke-virtual {v14}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    move-object/from16 v0, p0

    if-eq v3, v0, :cond_0

    .line 1510
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i:Z

    if-eqz v3, :cond_6

    .line 1511
    const/4 v3, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v3, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1517
    :cond_0
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    iget v6, v2, Llka;->b:I

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 1518
    mul-int v3, v12, v15

    add-int/lit8 v6, v15, -0x1

    mul-int/2addr v6, v11

    add-int v8, v3, v6

    .line 1521
    const/4 v3, 0x1

    if-le v15, v3, :cond_7

    .line 1522
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v15, v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(II[I)Llkb;

    move-result-object v6

    .line 1523
    iget v7, v6, Llkb;->a:I

    .line 1528
    :goto_4
    const/4 v3, 0x0

    .line 1529
    if-nez v6, :cond_8

    .line 1530
    new-instance v6, Llkb;

    invoke-direct {v6}, Llkb;-><init>()V

    .line 1531
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    move/from16 v0, p1

    invoke-virtual {v9, v0, v6}, Lgu;->b(ILjava/lang/Object;)V

    .line 1532
    iput v7, v6, Llkb;->a:I

    .line 1533
    iput v15, v6, Llkb;->d:I

    .line 1542
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->p:Z

    if-eqz v9, :cond_1

    .line 1543
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    move/from16 v0, p1

    invoke-interface {v9, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v16

    .line 1544
    move-wide/from16 v0, v16

    iput-wide v0, v6, Llkb;->b:J

    .line 1545
    move-wide/from16 v0, v16

    iput-wide v0, v2, Llka;->f:J

    .line 1548
    :cond_1
    iput v7, v2, Llka;->e:I

    .line 1552
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v9, :cond_c

    .line 1553
    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 1555
    iget v9, v2, Llka;->width:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_a

    .line 1556
    const/4 v2, 0x0

    const/4 v9, 0x0

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v18, v8

    move v8, v2

    move/from16 v2, v18

    .line 1577
    :goto_6
    invoke-virtual {v14, v8, v2}, Landroid/view/View;->measure(II)V

    .line 1579
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_f

    .line 1580
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    move v9, v2

    .line 1581
    :goto_7
    if-nez v3, :cond_2

    iget v2, v6, Llkb;->c:I

    if-eq v9, v2, :cond_3

    iget v2, v6, Llkb;->c:I

    if-lez v2, :cond_3

    .line 1582
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->e(I)V

    .line 1584
    :cond_3
    iput v9, v6, Llkb;->c:I

    .line 1587
    const/4 v2, 0x1

    if-le v15, v2, :cond_10

    .line 1588
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v3, v2, v7

    .line 1589
    add-int/lit8 v2, v7, 0x1

    move v8, v2

    :goto_8
    add-int v2, v7, v15

    if-ge v8, v2, :cond_11

    .line 1590
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v2, v2, v8

    .line 1591
    if-le v2, v3, :cond_17

    .line 1589
    :goto_9
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    move v3, v2

    goto :goto_8

    .line 1496
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v2

    move v4, v2

    goto/16 :goto_0

    .line 1500
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    move v5, v2

    goto/16 :goto_1

    .line 1513
    :cond_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 1525
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lgu;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llkb;

    move-object v6, v3

    goto/16 :goto_4

    .line 1534
    :cond_8
    iget v9, v6, Llkb;->d:I

    if-eq v15, v9, :cond_9

    .line 1535
    iput v15, v6, Llkb;->d:I

    .line 1536
    iput v7, v6, Llkb;->a:I

    .line 1537
    const/4 v3, 0x1

    goto/16 :goto_5

    .line 1539
    :cond_9
    iget v7, v6, Llkb;->a:I

    goto/16 :goto_5

    .line 1557
    :cond_a
    iget v9, v2, Llka;->width:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_b

    .line 1558
    iget v9, v2, Llka;->a:I

    add-int/lit8 v9, v9, -0x1

    mul-int/2addr v9, v11

    .line 1559
    iget v2, v2, Llka;->a:I

    mul-int/2addr v2, v12

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    mul-float/2addr v2, v10

    float-to-int v2, v2

    add-int/2addr v2, v9

    .line 1560
    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v18, v8

    move v8, v2

    move/from16 v2, v18

    .line 1561
    goto/16 :goto_6

    .line 1562
    :cond_b
    iget v2, v2, Llka;->width:I

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move/from16 v18, v8

    move v8, v2

    move/from16 v2, v18

    goto/16 :goto_6

    .line 1565
    :cond_c
    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 1567
    iget v9, v2, Llka;->height:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_d

    .line 1568
    const/4 v2, 0x0

    const/4 v9, 0x0

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_6

    .line 1569
    :cond_d
    iget v9, v2, Llka;->height:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_e

    .line 1570
    iget v9, v2, Llka;->a:I

    add-int/lit8 v9, v9, -0x1

    mul-int/2addr v9, v11

    .line 1571
    iget v2, v2, Llka;->a:I

    mul-int/2addr v2, v12

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->F:F

    mul-float/2addr v2, v10

    float-to-int v2, v2

    add-int/2addr v2, v9

    .line 1572
    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_6

    .line 1574
    :cond_e
    iget v2, v2, Llka;->height:I

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/16 :goto_6

    .line 1580
    :cond_f
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    move v9, v2

    goto/16 :goto_7

    .line 1597
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v3, v2, v7

    .line 1604
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_12

    .line 1605
    add-int/2addr v3, v11

    .line 1606
    add-int v2, v3, v9

    .line 1607
    add-int v8, v12, v11

    mul-int/2addr v8, v7

    add-int/2addr v8, v4

    .line 1608
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v8

    move v10, v2

    .line 1617
    :goto_a
    invoke-virtual {v14, v3, v8, v2, v9}, Landroid/view/View;->layout(IIII)V

    move v2, v7

    .line 1619
    :goto_b
    add-int v3, v7, v15

    if-ge v2, v3, :cond_13

    .line 1620
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    sub-int v8, v2, v7

    invoke-virtual {v6, v8}, Llkb;->b(I)I

    move-result v8

    add-int/2addr v8, v10

    aput v8, v3, v2

    .line 1619
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 1611
    :cond_12
    add-int v2, v12, v11

    mul-int/2addr v2, v7

    add-int v8, v4, v2

    .line 1612
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v8

    .line 1613
    add-int/2addr v3, v11

    .line 1614
    add-int/2addr v9, v3

    move v10, v9

    move/from16 v18, v3

    move v3, v8

    move/from16 v8, v18

    .line 1615
    goto :goto_a

    .line 1623
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a([I)I

    move-result v7

    .line 1624
    add-int/lit8 p1, p1, 0x1

    .line 1625
    goto/16 :goto_2

    .line 1627
    :cond_14
    const/4 v3, 0x0

    .line 1628
    const/4 v2, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-ge v2, v4, :cond_16

    .line 1629
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v4, v4, v2

    if-le v4, v3, :cond_15

    .line 1630
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k:[I

    aget v3, v3, v2

    .line 1628
    :cond_15
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 1633
    :cond_16
    sub-int v2, v3, v5

    return v2

    :cond_17
    move v2, v3

    goto/16 :goto_9
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    .line 291
    :goto_0
    iput p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    .line 292
    if-eqz v0, :cond_0

    .line 293
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m()V

    .line 295
    :cond_0
    return-void

    .line 290
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 260
    iput-boolean p1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->e:Z

    .line 261
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    return v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 308
    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    .line 309
    return-void

    .line 308
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2028
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    if-nez v0, :cond_0

    .line 2036
    :goto_0
    return-void

    .line 2032
    :cond_0
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    .line 2033
    iput v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    .line 2034
    iput p2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->n:I

    .line 2035
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->requestLayout()V

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 2095
    instance-of v0, p1, Llka;

    return v0
.end method

.method public computeScroll()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 859
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 861
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_1

    .line 862
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    .line 866
    :goto_0
    int-to-float v1, v0

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    sub-float/2addr v1, v3

    float-to-int v1, v1

    .line 867
    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    .line 868
    invoke-direct {p0, v1, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 870
    :goto_1
    if-nez v0, :cond_3

    iget-object v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_3

    .line 871
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 894
    :cond_0
    :goto_2
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l(I)V

    .line 895
    return-void

    .line 864
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 868
    goto :goto_1

    .line 873
    :cond_3
    if-eqz v0, :cond_5

    .line 874
    invoke-static {p0}, Liu;->a(Landroid/view/View;)I

    move-result v0

    .line 875
    const/4 v3, 0x2

    if-eq v0, v3, :cond_4

    .line 877
    if-lez v1, :cond_6

    .line 878
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    .line 883
    :goto_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v1, v3, :cond_7

    .line 884
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v1

    float-to-int v1, v1

    .line 886
    :goto_4
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lnh;->a(I)Z

    .line 887
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 889
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 891
    :cond_5
    iput v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    goto :goto_2

    .line 880
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_4
.end method

.method public d()I
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    return v0
.end method

.method final d(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1312
    move v0, v1

    .line 1313
    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v2}, Lgu;->b()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v2, v0}, Lgu;->e(I)I

    move-result v2

    if-ge v2, p1, :cond_0

    .line 1314
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1316
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v2, v1, v0}, Lgu;->a(II)V

    .line 1317
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 947
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 949
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->K:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 995
    :cond_0
    return-void

    .line 953
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v3

    .line 954
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingRight()I

    move-result v1

    sub-int v4, v0, v1

    .line 955
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v5

    .line 956
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingBottom()I

    move-result v1

    sub-int v6, v0, v1

    .line 960
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_0

    .line 961
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 963
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    add-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 964
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->N:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    if-ltz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    if-ltz v0, :cond_4

    .line 965
    instance-of v0, v1, Llke;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 969
    check-cast v0, Llke;

    invoke-interface {v0}, Llke;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 970
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 975
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    aget v7, v7, v10

    if-lt v0, v7, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    aget v7, v7, v10

    .line 976
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    if-gt v0, v7, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    aget v7, v7, v11

    if-lt v0, v7, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->M:[I

    aget v7, v7, v11

    .line 978
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    if-gt v0, v7, :cond_4

    .line 979
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 984
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v7

    .line 985
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v8

    .line 986
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 988
    if-gt v0, v4, :cond_4

    if-lt v7, v3, :cond_4

    if-gt v8, v6, :cond_4

    if-lt v1, v5, :cond_4

    .line 989
    iget-object v9, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->K:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9, v0, v8, v7, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 993
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->K:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 960
    :cond_4
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto/16 :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 905
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 907
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    .line 908
    const/4 v0, 0x0

    .line 909
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    invoke-virtual {v2}, Lnh;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 910
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_2

    .line 911
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 912
    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 913
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v2, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 914
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    invoke-virtual {v2, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    .line 915
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :goto_0
    move v0, v1

    .line 921
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    invoke-virtual {v2}, Lnh;->a()Z

    move-result v2

    if-nez v2, :cond_4

    .line 922
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_3

    .line 923
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 924
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->rotate(F)V

    .line 925
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v5, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 926
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    invoke-virtual {v2, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    .line 927
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 939
    :goto_1
    if-eqz v1, :cond_1

    .line 940
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 943
    :cond_1
    return-void

    .line 917
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    invoke-virtual {v0, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    goto :goto_0

    .line 929
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 930
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v2

    .line 931
    neg-int v3, v2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 932
    const/high16 v3, 0x43340000    # 180.0f

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 933
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    invoke-virtual {v2, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    .line 934
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method public e()I
    .locals 1

    .prologue
    .line 334
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    return v0
.end method

.method final e(I)V
    .locals 4

    .prologue
    .line 1320
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v0}, Lgu;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1321
    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v1, v0}, Lgu;->e(I)I

    move-result v1

    if-le v1, p1, :cond_0

    .line 1322
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1324
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 1325
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->f:Lgu;

    invoke-virtual {v3}, Lgu;->b()I

    move-result v3

    sub-int v0, v3, v0

    invoke-virtual {v1, v2, v0}, Lgu;->a(II)V

    .line 1326
    return-void
.end method

.method public f()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1142
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v0

    .line 1143
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingBottom()I

    move-result v1

    .line 1144
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getHeight()I

    move-result v2

    .line 1145
    :goto_2
    sub-int v0, v2, v0

    sub-int v1, v0, v1

    .line 1146
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v2, v0

    .line 1147
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->e:Z

    if-eqz v0, :cond_3

    sub-int v0, v1, v2

    iget v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    rem-int/2addr v0, v5

    if-eqz v0, :cond_3

    move v0, v3

    .line 1149
    :goto_3
    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    div-int/2addr v1, v2

    if-eqz v0, :cond_4

    :goto_4
    add-int v0, v1, v3

    return v0

    .line 1142
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v0

    goto :goto_0

    .line 1143
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingRight()I

    move-result v1

    goto :goto_1

    .line 1144
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getWidth()I

    move-result v2

    goto :goto_2

    :cond_3
    move v0, v4

    .line 1147
    goto :goto_3

    :cond_4
    move v3, v4

    .line 1149
    goto :goto_4
.end method

.method public f(I)V
    .locals 1

    .prologue
    .line 2015
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(II)V

    .line 2016
    return-void
.end method

.method final g()I
    .locals 4

    .prologue
    .line 1756
    const/4 v3, -0x1

    .line 1757
    const/high16 v1, -0x80000000

    .line 1759
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    .line 1760
    add-int/lit8 v2, v0, -0x1

    :goto_0
    if-ltz v2, :cond_0

    .line 1761
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->j:[I

    aget v0, v0, v2

    .line 1762
    if-le v0, v1, :cond_1

    move v1, v2

    .line 1760
    :goto_1
    add-int/lit8 v2, v2, -0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 1767
    :cond_0
    return v3

    :cond_1
    move v0, v1

    move v1, v3

    goto :goto_1
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 2212
    if-nez p1, :cond_0

    .line 2213
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->K:Landroid/graphics/drawable/Drawable;

    .line 2217
    :goto_0
    return-void

    .line 2215
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->K:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i()Llka;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 2100
    new-instance v0, Llka;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Llka;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/view/ViewGroup$LayoutParams;)Llka;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 1999
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeAllViews()V

    .line 2002
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->p()V

    .line 2005
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m()V

    .line 2006
    return-void
.end method

.method public h(I)Z
    .locals 2

    .prologue
    .line 2300
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v0

    return v0
.end method

.method protected i()Llka;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2081
    new-instance v2, Llka;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const/4 v3, -0x2

    invoke-direct {v2, v0, v3, v1, v1}, Llka;-><init>(IIII)V

    return-object v2

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public i(I)V
    .locals 2

    .prologue
    .line 2307
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->L:Z

    if-eqz v0, :cond_0

    .line 2308
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    .line 2309
    if-nez v0, :cond_0

    .line 2310
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 2311
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    .line 2312
    :cond_0
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 2261
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->L:Z

    if-nez v0, :cond_0

    .line 2262
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not in selection mode!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2265
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->L:Z

    .line 2266
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2267
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->invalidate()V

    .line 2269
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 2270
    return-void
.end method

.method public j(I)V
    .locals 2

    .prologue
    .line 2323
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->L:Z

    if-eqz v0, :cond_0

    .line 2324
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    .line 2325
    if-eqz v0, :cond_0

    .line 2326
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 2327
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    .line 2328
    :cond_0
    return-void
.end method

.method public k()Landroid/util/SparseBooleanArray;
    .locals 4

    .prologue
    .line 2287
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    .line 2288
    new-instance v1, Landroid/util/SparseBooleanArray;

    invoke-direct {v1, v0}, Landroid/util/SparseBooleanArray;-><init>(I)V

    .line 2289
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 2290
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 2289
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2292
    :cond_0
    return-object v1
.end method

.method public l()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2866
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 2874
    :cond_0
    :goto_0
    return v0

    .line 2869
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->e()I

    move-result v3

    add-int/2addr v2, v3

    if-eqz v2, :cond_2

    move v0, v1

    .line 2870
    goto :goto_0

    .line 2873
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v2

    .line 2874
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c()I

    move-result v4

    add-int/2addr v2, v4

    if-ge v3, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 899
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 900
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->O:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 901
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, -0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 369
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 370
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 372
    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v3

    .line 443
    :goto_1
    return v0

    .line 374
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Point;->set(II)V

    .line 375
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->O:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {p0, v0, v4, v5}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 377
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 378
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 379
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_1

    .line 380
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    .line 385
    :goto_2
    invoke-static {p1, v3}, Lik;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->v:I

    .line 386
    iput v6, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->x:F

    .line 387
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 389
    iput v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    move v0, v2

    .line 390
    goto :goto_1

    .line 382
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    goto :goto_2

    .line 393
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->L:Z

    if-eqz v0, :cond_0

    move v0, v2

    .line 394
    goto :goto_1

    .line 400
    :pswitch_1
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->v:I

    invoke-static {p1, v0}, Lik;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 401
    if-gez v0, :cond_3

    .line 402
    const-string v0, "ColumnGridView"

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->v:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x6f

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - did we receive an inconsistent event stream?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 405
    goto :goto_1

    .line 412
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v1, :cond_5

    .line 413
    invoke-static {p1, v0}, Lik;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 417
    :goto_3
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->x:F

    add-float/2addr v1, v0

    .line 418
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->y:I

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-lez v0, :cond_6

    move v4, v2

    .line 419
    :goto_4
    if-eqz v4, :cond_8

    .line 420
    cmpl-float v0, v1, v6

    if-lez v0, :cond_7

    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->y:I

    neg-int v0, v0

    int-to-float v0, v0

    :goto_5
    add-float/2addr v0, v1

    .line 422
    :goto_6
    float-to-int v1, v0

    .line 423
    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->x:F

    .line 425
    if-eqz v4, :cond_0

    .line 426
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 427
    if-eqz v0, :cond_4

    .line 428
    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_4
    move v0, v2

    .line 430
    goto/16 :goto_1

    .line 415
    :cond_5
    invoke-static {p1, v0}, Lik;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    goto :goto_3

    :cond_6
    move v4, v3

    .line 418
    goto :goto_4

    .line 420
    :cond_7
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->y:I

    int-to-float v0, v0

    goto :goto_5

    .line 437
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 438
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q()V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_6

    .line 372
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1033
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i:Z

    .line 1034
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m()V

    .line 1035
    iput-boolean v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->i:Z

    .line 1037
    sub-int v0, p4, p2

    .line 1038
    sub-int v1, p5, p3

    .line 1039
    iget-boolean v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_0

    .line 1040
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    invoke-virtual {v2, v1, v0}, Lnh;->a(II)V

    .line 1041
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    invoke-virtual {v2, v1, v0}, Lnh;->a(II)V

    .line 1047
    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k(I)V

    .line 1048
    return-void

    .line 1043
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->D:Lnh;

    invoke-virtual {v2, v0, v1}, Lnh;->a(II)V

    .line 1044
    iget-object v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->E:Lnh;

    invoke-virtual {v2, v0, v1}, Lnh;->a(II)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    .line 1006
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1007
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1009
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getMeasuredWidth()I

    move-result v0

    if-ne v1, v0, :cond_0

    .line 1010
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getMeasuredHeight()I

    move-result v0

    if-eq v2, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1012
    :goto_0
    invoke-virtual {p0, v1, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setMeasuredDimension(II)V

    .line 1016
    if-eqz v0, :cond_3

    .line 1017
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_3

    .line 1018
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1019
    if-eqz v3, :cond_1

    .line 1020
    invoke-virtual {v3}, Landroid/view/View;->requestLayout()V

    .line 1017
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1010
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1025
    :cond_3
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_4

    if-lez v2, :cond_4

    if-lez v1, :cond_4

    .line 1026
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_5

    div-int/lit8 v0, v2, 0x0

    :goto_2
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    .line 1029
    :cond_4
    return-void

    .line 1026
    :cond_5
    div-int/lit8 v0, v1, 0x0

    goto :goto_2
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    .line 2151
    check-cast p1, Llkh;

    .line 2152
    invoke-virtual {p1}, Llkh;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q:Z

    .line 2154
    iget v0, p1, Llkh;->c:I

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    .line 2155
    iget v0, p1, Llkh;->d:I

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->n:I

    .line 2156
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 2157
    iget-boolean v0, p1, Llkh;->f:Z

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->L:Z

    .line 2158
    iget-boolean v0, p1, Llkh;->g:Z

    iput-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->G:Z

    .line 2160
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    if-nez v0, :cond_0

    .line 2161
    iget v0, p1, Llkh;->b:I

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    .line 2168
    :goto_0
    iget-object v0, p1, Llkh;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_2

    .line 2169
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->H:Landroid/util/SparseBooleanArray;

    iget-object v2, p1, Llkh;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v2

    iget-object v3, p1, Llkh;->e:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 2168
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 2162
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->G:Z

    if-eqz v0, :cond_1

    iget v0, p1, Llkh;->b:I

    if-lez v0, :cond_1

    .line 2163
    iget v0, p1, Llkh;->b:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    div-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    mul-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    goto :goto_0

    .line 2165
    :cond_1
    iget v0, p1, Llkh;->b:I

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    div-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    goto :goto_0

    .line 2171
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->requestLayout()V

    .line 2172
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    .prologue
    .line 2105
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2106
    new-instance v3, Llkh;

    invoke-direct {v3, v0}, Llkh;-><init>(Landroid/os/Parcelable;)V

    .line 2107
    iget v4, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l:I

    .line 2108
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->m:I

    .line 2109
    iput v4, v3, Llkh;->b:I

    .line 2110
    iput v0, v3, Llkh;->c:I

    .line 2111
    if-ltz v4, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v4, v0, :cond_0

    .line 2112
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, v3, Llkh;->a:J

    .line 2114
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->k()Landroid/util/SparseBooleanArray;

    move-result-object v0

    iput-object v0, v3, Llkh;->e:Landroid/util/SparseBooleanArray;

    .line 2115
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->L:Z

    iput-boolean v0, v3, Llkh;->f:Z

    .line 2116
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->G:Z

    iput-boolean v0, v3, Llkh;->g:Z

    .line 2120
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildCount()I

    move-result v1

    .line 2121
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 2122
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2123
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Llka;

    .line 2124
    iget-boolean v0, v0, Llka;->g:Z

    if-eqz v0, :cond_3

    .line 2127
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_2

    .line 2128
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingLeft()I

    move-result v1

    .line 2129
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 2134
    :goto_1
    iget v5, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d:I

    sub-int/2addr v0, v5

    sub-int/2addr v0, v1

    iput v0, v3, Llkh;->d:I

    .line 2136
    if-eqz v2, :cond_1

    .line 2138
    add-int v0, v4, v2

    iput v0, v3, Llkh;->b:I

    .line 2139
    iget v0, v3, Llkh;->b:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    iget v0, v3, Llkh;->b:I

    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2140
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->o:Landroid/widget/ListAdapter;

    iget v1, v3, Llkh;->b:I

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, v3, Llkh;->a:J

    .line 2146
    :cond_1
    return-object v3

    .line 2131
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getPaddingTop()I

    move-result v1

    .line 2132
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_1

    .line 2121
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/high16 v5, -0x80000000

    const/4 v10, 0x0

    const/4 v2, -0x1

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 448
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 449
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 451
    packed-switch v0, :pswitch_data_0

    .line 555
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l(I)V

    move v1, v9

    .line 561
    :goto_1
    return v1

    .line 453
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 454
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->I:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 455
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->O:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 457
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 458
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 459
    if-eqz v0, :cond_1

    .line 460
    invoke-interface {v0, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 464
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 465
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 466
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_2

    .line 467
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    .line 471
    :goto_2
    invoke-static {p1, v1}, Lik;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->v:I

    .line 472
    iput v10, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->x:F

    goto :goto_0

    .line 469
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    goto :goto_2

    .line 477
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 478
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q()V

    .line 480
    iget v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->v:I

    invoke-static {p1, v0}, Lik;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 481
    if-gez v0, :cond_3

    .line 482
    const-string v0, "ColumnGridView"

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->v:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x6f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onInterceptTouchEvent could not find pointer with id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - did we receive an inconsistent event stream?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 491
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v1, :cond_5

    .line 492
    invoke-static {p1, v0}, Lik;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 496
    :goto_3
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    sub-float v1, v0, v1

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->x:F

    add-float/2addr v2, v1

    .line 498
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    if-nez v1, :cond_b

    .line 499
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->y:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_b

    .line 500
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 501
    if-eqz v1, :cond_4

    .line 502
    invoke-interface {v1, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 504
    :cond_4
    cmpl-float v1, v2, v10

    if-lez v1, :cond_6

    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->y:I

    neg-int v1, v1

    int-to-float v1, v1

    :goto_4
    add-float/2addr v1, v2

    .line 505
    iput v9, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    .line 508
    :goto_5
    float-to-int v2, v1

    .line 509
    int-to-float v3, v2

    sub-float/2addr v1, v3

    iput v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->x:F

    .line 511
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    if-ne v1, v9, :cond_0

    .line 512
    iput v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    .line 514
    invoke-direct {p0, v2, v9}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_0

    .line 494
    :cond_5
    invoke-static {p1, v0}, Lik;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    goto :goto_3

    .line 504
    :cond_6
    iget v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->y:I

    int-to-float v1, v1

    goto :goto_4

    .line 523
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 524
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q()V

    .line 526
    iput v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    .line 527
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r()V

    goto/16 :goto_0

    .line 532
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 533
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q()V

    .line 535
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->B:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->z:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 536
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->B:Landroid/view/VelocityTracker;

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->v:I

    .line 537
    invoke-static {v0, v2}, Liq;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    .line 540
    :goto_6
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->A:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_a

    .line 541
    const/4 v2, 0x2

    iput v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    .line 542
    iget-boolean v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_8

    float-to-int v3, v0

    .line 543
    :goto_7
    iget-boolean v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a:Z

    if-eqz v2, :cond_9

    move v4, v1

    .line 544
    :goto_8
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->C:Landroid/widget/Scroller;

    const v6, 0x7fffffff

    const v8, 0x7fffffff

    move v2, v1

    move v7, v5

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 547
    iput v10, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->w:F

    .line 548
    invoke-static {p0}, Liu;->c(Landroid/view/View;)V

    .line 553
    :goto_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->d(II)V

    .line 554
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->r()V

    goto/16 :goto_0

    .line 537
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->B:Landroid/view/VelocityTracker;

    iget v2, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->v:I

    .line 538
    invoke-static {v0, v2}, Liq;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    goto :goto_6

    :cond_8
    move v3, v1

    .line 542
    goto :goto_7

    .line 543
    :cond_9
    float-to-int v4, v0

    goto :goto_8

    .line 550
    :cond_a
    iput v1, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->t:I

    goto :goto_9

    :cond_b
    move v1, v2

    goto/16 :goto_5

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 360
    if-eqz p1, :cond_0

    .line 361
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->J:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 362
    invoke-direct {p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->q()V

    .line 364
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 365
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 999
    iget-boolean v0, p0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h:Z

    if-nez v0, :cond_0

    .line 1000
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1002
    :cond_0
    return-void
.end method
