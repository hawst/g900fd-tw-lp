.class public Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;
.super Llol;
.source "PG"

# interfaces
.implements Lkkj;


# instance fields
.field private N:Landroid/view/ViewGroup;

.field private O:Landroid/view/ViewGroup;

.field private P:Landroid/view/View;

.field private Q:Landroid/view/View;

.field private R:Landroid/view/View;

.field private S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

.field private T:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

.field private U:Lkjj;

.field private V:Lkki;

.field private W:Lkkr;

.field private X:Ljava/lang/String;

.field private Y:Landroid/os/Bundle;

.field private Z:Z

.field private final aa:Lhoc;

.field private final ab:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkkl;",
            ">;"
        }
    .end annotation
.end field

.field private ac:Lkkh;

.field private final ad:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Llol;-><init>()V

    .line 81
    new-instance v0, Lhoc;

    .line 82
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->z_()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->aa:Lhoc;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ab:Ljava/util/List;

    .line 88
    new-instance v0, Lkjz;

    invoke-direct {v0, p0}, Lkjz;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ad:Landroid/view/View$OnClickListener;

    .line 102
    return-void
.end method

.method private W()V
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ab:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkkl;

    .line 269
    invoke-interface {v0}, Lkkl;->a()V

    goto :goto_0

    .line 271
    :cond_0
    return-void
.end method

.method private X()V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ab:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkkl;

    .line 275
    invoke-interface {v0}, Lkkl;->b()V

    goto :goto_0

    .line 277
    :cond_0
    return-void
.end method

.method private Y()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 316
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->x()Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->U:Lkjj;

    invoke-interface {v0}, Lkjj;->b()Lkjf;

    move-result-object v0

    iget-boolean v0, v0, Lkjf;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->T:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->x()Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->U:Lkjj;

    invoke-interface {v3}, Lkjj;->b()Lkjf;

    move-result-object v3

    iget-boolean v3, v3, Lkjf;->a:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 320
    return-void

    :cond_0
    move v0, v2

    .line 316
    goto :goto_0

    :cond_1
    move v1, v2

    .line 318
    goto :goto_1
.end method

.method private Z()I
    .locals 1

    .prologue
    .line 424
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    return v0
.end method

.method private a(Lkzs;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 519
    invoke-virtual {p1}, Lkzs;->e()Ljava/lang/String;

    move-result-object v0

    .line 520
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 537
    :goto_0
    return-object v0

    .line 525
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 527
    :try_start_0
    const-string v1, "com.google.android.apps.social"

    const/4 v2, 0x0

    .line 528
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 527
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 529
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 530
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 537
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)Lkki;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;Lklq;Llec;Lled;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a(Lklq;Llec;Lled;)V

    return-void
.end method

.method private a(Lklq;Llec;Lled;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 409
    iget-object v0, p1, Lklq;->a:Lkko;

    invoke-virtual {v0}, Lkko;->e()Lkzv;

    move-result-object v1

    iget-object v2, p1, Lklq;->c:Lkzs;

    iget-object v0, p1, Lklq;->c:Lkzs;

    if-nez v0, :cond_0

    move-object v5, v3

    .line 413
    :goto_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Z()I

    move-result v6

    const/4 v7, -0x1

    move-object v0, p2

    move-object v4, v3

    move-object v9, p3

    .line 409
    invoke-virtual/range {v0 .. v9}, Llec;->a(Lkzv;Lkzs;Lkzs;Ljava/lang/String;Ljava/lang/String;IIZLled;)V

    .line 415
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->R:Landroid/view/View;

    iget-object v1, p1, Lklq;->a:Lkko;

    .line 416
    invoke-virtual {v1}, Lkko;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p1, Lklq;->a:Lkko;

    invoke-virtual {v1}, Lkko;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->W:Lkkr;

    .line 417
    invoke-virtual {v1}, Lkkr;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 418
    invoke-virtual {p2}, Llec;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 415
    :goto_1
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 421
    return-void

    .line 409
    :cond_0
    iget-object v0, p1, Lklq;->c:Lkzs;

    .line 412
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a(Lkzs;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 418
    :cond_1
    const/16 v8, 0x8

    goto :goto_1
.end method

.method private aa()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v1, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 428
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->O:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 430
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->p()Lklq;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lklq;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 431
    :goto_0
    if-nez v0, :cond_1

    .line 432
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v2}, Lkki;->j()Lkzz;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 433
    new-instance v0, Lkxq;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->at:Llnl;

    invoke-direct {v0, v2}, Lkxq;-><init>(Landroid/content/Context;)V

    .line 434
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v2}, Lkki;->j()Lkzz;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkxq;->a(Lkzz;)V

    .line 436
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 438
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0113

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v2, v8, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 436
    invoke-virtual {v0, v2}, Lkxq;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 449
    :cond_1
    :goto_1
    if-eqz v0, :cond_8

    .line 450
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->O:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 451
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->P:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 452
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->O:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 453
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->N:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 463
    :goto_2
    return-void

    .line 430
    :cond_2
    iget-object v0, v3, Lklq;->b:Lkzy;

    if-eqz v0, :cond_3

    new-instance v0, Llec;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->at:Llnl;

    invoke-direct {v0, v2}, Llec;-><init>(Landroid/content/Context;)V

    iget-object v2, v3, Lklq;->b:Lkzy;

    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Z()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Llec;->a(Lkzy;I)V

    goto :goto_0

    :cond_3
    iget-object v0, v3, Lklq;->a:Lkko;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->W:Lkkr;

    invoke-virtual {v0}, Lkkr;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Q:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Q:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ad:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    new-instance v2, Llec;

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->at:Llnl;

    invoke-direct {v2, v0}, Llec;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->W:Lkkr;

    invoke-virtual {v0}, Lkkr;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Lkkf;

    invoke-direct {v0, p0, v3, v2}, Lkkf;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;Lklq;Llec;)V

    :goto_3
    invoke-direct {p0, v3, v2, v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a(Lklq;Llec;Lled;)V

    move-object v0, v2

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    .line 439
    :cond_7
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v2}, Lkki;->q()Lkjd;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 440
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->r()Loya;

    move-result-object v0

    .line 441
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v2}, Lkki;->q()Lkjd;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->at:Llnl;

    .line 442
    invoke-virtual {v3}, Llnl;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, Loya;->c:Ljava/lang/String;

    .line 441
    invoke-virtual {v2, v3, v4, v0}, Lkjd;->a(Landroid/content/Context;Ljava/lang/String;Loya;)Landroid/view/View;

    move-result-object v0

    .line 443
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->N:Landroid/view/ViewGroup;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 445
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0112

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v3, v8, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 443
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 454
    :cond_8
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->s()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 455
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->N:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 456
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->P:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 458
    :cond_9
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Q:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 459
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 460
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->O:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 461
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->N:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method private ab()V
    .locals 7

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->l()Lklb;

    move-result-object v0

    invoke-virtual {v0}, Lklb;->b()Lkkz;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 542
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v1}, Lkki;->l()Lklb;

    move-result-object v1

    invoke-virtual {v1}, Lklb;->b()Lkkz;

    move-result-object v1

    invoke-virtual {v1}, Lkkz;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->b(Ljava/lang/CharSequence;)V

    .line 549
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->n()Lklo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    .line 550
    invoke-virtual {v0}, Lkki;->m()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->V()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a(Ljava/lang/CharSequence;)V

    .line 553
    :cond_0
    return-void

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->w()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 544
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->i()Lkzm;

    move-result-object v2

    sget-object v0, Lljd;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lkzm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2}, Lkzm;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lljd;

    invoke-direct {v3, v2}, Lljd;-><init>(Ljava/lang/String;)V

    new-instance v2, Landroid/text/SpannableString;

    const v4, 0x7f0a02ca

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v4

    const/16 v5, 0x21

    invoke-interface {v2, v3, v4, v0, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 546
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->d()V

    goto/16 :goto_0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Y()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->p()Lae;

    move-result-object v4

    const-string v0, "sharelet_content_fragment"

    invoke-virtual {v4, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->U:Lkjj;

    invoke-interface {v0}, Lkjj;->a()Lkji;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v3, v2

    :goto_0
    if-nez v3, :cond_0

    if-eqz v5, :cond_3

    :cond_0
    invoke-virtual {v4}, Lae;->a()Lat;

    move-result-object v4

    if-eqz v3, :cond_4

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->e()Landroid/text/SpannableStringBuilder;

    move-result-object v6

    const-string v0, "editable_post_text"

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v7, Landroid/text/style/URLSpan;

    invoke-virtual {v6, v1, v0, v7}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    if-eqz v0, :cond_5

    array-length v2, v0

    new-array v2, v2, [Lkiz;

    :goto_1
    array-length v7, v0

    if-ge v1, v7, :cond_2

    new-instance v7, Lkiz;

    aget-object v8, v0, v1

    aget-object v9, v0, v1

    invoke-virtual {v6, v9}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v9

    aget-object v10, v0, v1

    invoke-virtual {v6, v10}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v10

    invoke-direct {v7, v8, v9, v10}, Lkiz;-><init>(Landroid/text/style/URLSpan;II)V

    aput-object v7, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->U:Lkjj;

    invoke-interface {v0}, Lkjj;->a()Lkji;

    move-result-object v0

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Lkji;->a(I)Lu;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    :goto_2
    const-string v1, "editable_post_text_url_spans"

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    invoke-virtual {v3, v5}, Lu;->f(Landroid/os/Bundle;)V

    const v0, 0x7f100564

    const-string v1, "sharelet_content_fragment"

    invoke-virtual {v4, v0, v3, v1}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    :goto_3
    invoke-virtual {v4}, Lat;->b()I

    :cond_3
    return-void

    :cond_4
    invoke-virtual {v4, v5}, Lat;->a(Lu;)Lat;

    goto :goto_3

    :cond_5
    move-object v0, v2

    goto :goto_2
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->X()V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->W()V

    return-void
.end method


# virtual methods
.method public U()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->U()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->V()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 169
    const v0, 0x7f0401d7

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 171
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->p()Lae;

    move-result-object v0

    const v2, 0x7f100563

    invoke-virtual {v0, v2}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    .line 174
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    new-instance v2, Lkkb;

    invoke-direct {v2, p0}, Lkkb;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a(Landroid/view/View$OnClickListener;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    new-instance v2, Lkkc;

    invoke-direct {v2, p0}, Lkkc;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a(Lkjx;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    new-instance v2, Lkkd;

    invoke-direct {v2, p0}, Lkkd;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a(Landroid/text/TextWatcher;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a()V

    .line 205
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->f()Lae;

    move-result-object v0

    const v2, 0x7f100565

    .line 206
    invoke-virtual {v0, v2}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->T:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    .line 207
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->T:Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;

    new-instance v2, Lkke;

    invoke-direct {v2, p0}, Lkke;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/LocationSelectorFragment;->a(Lkkv;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->a()Ljava/lang/String;

    move-result-object v0

    .line 215
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 216
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a(Ljava/lang/CharSequence;)V

    .line 219
    :cond_0
    const v0, 0x7f100569

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->N:Landroid/view/ViewGroup;

    .line 220
    const v0, 0x7f10056a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->O:Landroid/view/ViewGroup;

    .line 221
    const v0, 0x7f100260

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->P:Landroid/view/View;

    .line 222
    const v0, 0x7f10056c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Q:Landroid/view/View;

    .line 223
    const v0, 0x7f10056b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->R:Landroid/view/View;

    .line 227
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0123

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 229
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Z()I

    move-result v2

    div-int/lit8 v2, v2, 0x4

    .line 230
    sub-int v0, v2, v0

    .line 231
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 233
    invoke-virtual {v2, v0, v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 234
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->R:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 236
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Y()V

    .line 238
    return-object v1
.end method

.method public a()Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 112
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->au:Llnh;

    const-class v1, Lkki;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkki;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->au:Llnh;

    const-class v1, Lkkr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkkr;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->W:Lkkr;

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->aa:Lhoc;

    invoke-virtual {v0}, Lhoc;->d()Lhos;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 118
    invoke-virtual {v0, p0, v1, v2}, Lhos;->a(Lu;Ljava/lang/String;Z)V

    .line 120
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->at:Llnl;

    const-class v1, Lkjj;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjj;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->U:Lkjj;

    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->U:Lkjj;

    new-instance v1, Lkka;

    invoke-direct {v1, p0}, Lkka;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;)V

    invoke-interface {v0, v1}, Lkjj;->a(Lkjk;)V

    .line 129
    if-eqz p1, :cond_2

    .line 130
    const-string v0, "content_deep_link_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const-string v0, "content_deep_link_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->X:Ljava/lang/String;

    .line 134
    :cond_0
    const-string v0, "content_deep_link_metadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    const-string v0, "content_deep_link_metadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Y:Landroid/os/Bundle;

    .line 139
    :cond_1
    const-string v0, "domain_restrict"

    iget-boolean v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Z:Z

    .line 140
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Z:Z

    .line 143
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->W()V

    .line 147
    :cond_2
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0, p1}, Lkki;->a(Ljava/lang/String;)V

    .line 160
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->X()V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->d()V

    .line 162
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->aa()V

    .line 164
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ac:Lkkh;

    invoke-interface {v0}, Lkkh;->e()V

    goto :goto_0
.end method

.method public a(Lkkh;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ac:Lkkh;

    .line 108
    return-void
.end method

.method public a(Lkkl;)V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ab:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 492
    invoke-super {p0}, Llol;->aO_()V

    .line 494
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0, p0}, Lkki;->a(Lkkj;)V

    .line 496
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ab()V

    .line 497
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->aa()V

    .line 500
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0}, Lkki;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ac:Lkkh;

    invoke-interface {v0}, Lkkh;->e()V

    .line 503
    :cond_0
    return-void
.end method

.method public ae_()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 243
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    .line 244
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->N:Landroid/view/ViewGroup;

    .line 245
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->O:Landroid/view/ViewGroup;

    .line 246
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->P:Landroid/view/View;

    .line 247
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Q:Landroid/view/View;

    .line 248
    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->R:Landroid/view/View;

    .line 250
    invoke-super {p0}, Llol;->ae_()V

    .line 251
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->X()V

    .line 336
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->V()Ljava/lang/String;

    move-result-object v0

    .line 338
    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "http://"

    const-string v2, ""

    .line 339
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 340
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    invoke-virtual {v1, p1}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a(Ljava/lang/CharSequence;)V

    .line 343
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->at:Llnl;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a02c9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lkkg;

    invoke-direct {v2}, Lkkg;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 346
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->aa()V

    .line 347
    return-void

    .line 340
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->S:Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->W()V

    .line 261
    :cond_0
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->ab()V

    .line 330
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->aa()V

    .line 331
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 480
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 481
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->X:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 482
    const-string v0, "content_deep_link_id"

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->X:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Y:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 486
    const-string v0, "content_deep_link_metadata"

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->Y:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 488
    :cond_1
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V:Lkki;

    invoke-virtual {v0, p0}, Lkki;->b(Lkkj;)V

    .line 508
    invoke-super {p0}, Llol;->z()V

    .line 509
    return-void
.end method
