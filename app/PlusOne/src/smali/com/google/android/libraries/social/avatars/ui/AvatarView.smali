.class public Lcom/google/android/libraries/social/avatars/ui/AvatarView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;


# static fields
.field private static a:Z

.field private static b:Landroid/graphics/Paint;

.field private static c:Landroid/graphics/RectF;

.field private static d:Lhso;


# instance fields
.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lkda;

.field private i:I

.field private j:Z

.field private k:Landroid/graphics/Paint;

.field private l:Landroid/graphics/Rect;

.field private m:Landroid/graphics/Rect;

.field private n:Z

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Landroid/graphics/drawable/Drawable;

.field private t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    iput-boolean v4, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->r:Z

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 74
    sget-boolean v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a:Z

    if-nez v0, :cond_0

    .line 75
    const-class v0, Lhso;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    sput-object v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d:Lhso;

    .line 76
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 77
    sput-object v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 78
    sget-object v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 79
    sget-object v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b:Landroid/graphics/Paint;

    const v2, 0x7f0b0139

    .line 80
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 79
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    sget-object v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    sput-boolean v4, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a:Z

    .line 85
    :cond_0
    const v0, 0x7f020416

    .line 86
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->s:Landroid/graphics/drawable/Drawable;

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 89
    iput v6, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e:I

    .line 90
    iput v6, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    .line 91
    if-eqz p2, :cond_5

    .line 92
    const-string v0, "size"

    invoke-interface {p2, v3, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_1

    .line 94
    invoke-static {v0}, Lhss;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e:I

    .line 97
    :cond_1
    const-string v0, "shape"

    invoke-interface {p2, v3, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_2

    .line 99
    invoke-static {v0}, Lhss;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    .line 102
    :cond_2
    const-string v0, "scale"

    invoke-interface {p2, v3, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_3

    .line 104
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->p:Z

    .line 107
    :cond_3
    const-string v0, "selectable"

    invoke-interface {p2, v3, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_4

    .line 109
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->r:Z

    .line 112
    :cond_4
    const-string v0, "allowNonSquare"

    invoke-interface {p2, v3, v0, v5}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->n:Z

    .line 116
    :cond_5
    iget v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 117
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_6

    .line 118
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setLayoutDirection(I)V

    .line 120
    :cond_6
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 123
    iput p1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    .line 124
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->g:Ljava/lang/String;

    .line 185
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->f:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->t:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 199
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 200
    iput-object p1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->f:Ljava/lang/String;

    .line 201
    iput-object p2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->t:Ljava/lang/String;

    .line 202
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b()V

    .line 204
    :cond_1
    return-void
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->invalidate()V

    .line 253
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->p:Z

    .line 128
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 228
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->t:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 230
    sget-object v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d:Lhso;

    iget-object v1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->t:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e:I

    iget v3, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    .line 231
    invoke-interface {v0, v1, v2, v3, p0}, Lhso;->a(Ljava/lang/String;IILkdd;)Lkda;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->h:Lkda;

    .line 236
    :cond_0
    :goto_1
    return-void

    .line 228
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 233
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->invalidate()V

    goto :goto_1
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 136
    iput p1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e:I

    .line 137
    iget v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e:I

    packed-switch v0, :pswitch_data_0

    .line 152
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhss;->e(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    .line 155
    :goto_0
    return-void

    .line 139
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhss;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    goto :goto_0

    .line 143
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhss;->c(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    goto :goto_0

    .line 147
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhss;->g(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    goto :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->n:Z

    .line 163
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->h:Lkda;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->h:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->h:Lkda;

    .line 248
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 210
    iput-boolean p1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->q:Z

    .line 211
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->invalidate()V

    .line 212
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 166
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 167
    iput-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->f:Ljava/lang/String;

    .line 168
    iput-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->t:Ljava/lang/String;

    .line 169
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 401
    iput-boolean p1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->r:Z

    .line 402
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 331
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->invalidate()V

    .line 333
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 334
    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->f:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 177
    const/4 v0, 0x0

    .line 180
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "g:"

    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a052a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 216
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b()V

    .line 218
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 222
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 223
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 224
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 338
    .line 339
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->h:Lkda;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->h:Lkda;

    invoke-virtual {v0}, Lkda;->getStatus()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    .line 340
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->h:Lkda;

    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 343
    :goto_0
    if-nez v0, :cond_0

    .line 344
    iget v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e:I

    packed-switch v2, :pswitch_data_0

    .line 362
    :cond_0
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->q:Z

    if-eqz v2, :cond_1

    .line 363
    sget-object v2, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 364
    sget-object v2, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c:Landroid/graphics/RectF;

    const/16 v3, 0x69

    const/16 v4, 0x1f

    invoke-virtual {p1, v2, v3, v4}, Landroid/graphics/Canvas;->saveLayerAlpha(Landroid/graphics/RectF;II)I

    .line 366
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->j:Z

    if-eqz v2, :cond_5

    .line 367
    iget-object v1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->m:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->l:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 371
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->q:Z

    if-eqz v0, :cond_2

    .line 372
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 375
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->r:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->isPressed()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->q:Z

    if-nez v0, :cond_4

    .line 376
    iget v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    packed-switch v0, :pswitch_data_1

    .line 393
    :cond_4
    :goto_3
    return-void

    .line 346
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    invoke-static {v0, v2}, Lhss;->b(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 350
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    invoke-static {v0, v2}, Lhss;->c(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 354
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    invoke-static {v0, v2}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 358
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    invoke-static {v0, v2}, Lhss;->e(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 369
    :cond_5
    invoke-virtual {p1, v0, v5, v5, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 378
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3

    .line 381
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 382
    int-to-float v1, v0

    int-to-float v2, v0

    add-int/lit8 v0, v0, -0x2

    int-to-float v0, v0

    sget-object v3, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 386
    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhss;->i(Landroid/content/Context;)F

    move-result v0

    .line 387
    sget-object v1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 388
    sget-object v1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c:Landroid/graphics/RectF;

    sget-object v2, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v0, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto/16 :goto_0

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 376
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 314
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 315
    iget v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->o:I

    if-nez v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->s:Landroid/graphics/drawable/Drawable;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 318
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v5, -0x80000000

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 257
    iget v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    .line 258
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 260
    if-ne v1, v4, :cond_5

    .line 261
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 266
    :cond_0
    :goto_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 268
    iget-boolean v1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->n:Z

    if-eqz v1, :cond_6

    .line 269
    iget v1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    .line 270
    if-eq v2, v4, :cond_1

    if-ne v2, v5, :cond_b

    .line 271
    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v7, v1

    move v1, v0

    move v0, v7

    .line 283
    :goto_1
    iget v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    if-eq v1, v2, :cond_8

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->j:Z

    .line 284
    iget-boolean v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->j:Z

    if-eqz v2, :cond_4

    .line 285
    iget-object v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->k:Landroid/graphics/Paint;

    if-nez v2, :cond_2

    .line 286
    new-instance v2, Landroid/graphics/Paint;

    const/4 v4, 0x2

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->k:Landroid/graphics/Paint;

    .line 287
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->l:Landroid/graphics/Rect;

    .line 290
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->l:Landroid/graphics/Rect;

    invoke-virtual {v2, v3, v3, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 291
    iget v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    if-le v2, v1, :cond_a

    .line 292
    iget-object v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->m:Landroid/graphics/Rect;

    if-nez v2, :cond_3

    .line 293
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->m:Landroid/graphics/Rect;

    .line 295
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->p:Z

    if-eqz v2, :cond_9

    .line 296
    iget-object v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->m:Landroid/graphics/Rect;

    iget v4, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    iget v5, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    invoke-virtual {v2, v3, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 309
    :cond_4
    :goto_3
    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setMeasuredDimension(II)V

    .line 310
    return-void

    .line 262
    :cond_5
    if-ne v1, v5, :cond_0

    .line 263
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 275
    :cond_6
    if-ne v2, v4, :cond_7

    .line 276
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_4
    move v1, v0

    .line 280
    goto :goto_1

    .line 278
    :cond_7
    iget v1, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_4

    :cond_8
    move v2, v3

    .line 283
    goto :goto_2

    .line 298
    :cond_9
    iget v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    .line 299
    iget v3, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    add-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    .line 300
    iget v4, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    sub-int/2addr v4, v0

    div-int/lit8 v4, v4, 0x2

    .line 301
    iget v5, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->i:I

    add-int/2addr v5, v0

    div-int/lit8 v5, v5, 0x2

    .line 302
    iget-object v6, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->m:Landroid/graphics/Rect;

    invoke-virtual {v6, v2, v4, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_3

    .line 305
    :cond_a
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->m:Landroid/graphics/Rect;

    goto :goto_3

    :cond_b
    move v7, v1

    move v1, v0

    move v0, v7

    goto/16 :goto_1
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->s:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    .line 323
    const/4 v0, 0x1

    .line 325
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
