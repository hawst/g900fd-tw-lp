.class public Lcom/google/android/libraries/social/sharekit/impl/SharekitModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    const-class v0, Lief;

    if-ne p2, v0, :cond_1

    .line 24
    const-class v0, Lief;

    const/4 v1, 0x7

    new-array v1, v1, [Lief;

    const/4 v2, 0x0

    sget-object v3, Lkmx;->e:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lkmx;->c:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lkmx;->d:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lkmx;->a:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lkmx;->b:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Lkmx;->g:Lief;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lkmx;->f:Lief;

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    const-class v0, Lknk;

    if-ne p2, v0, :cond_2

    .line 34
    const-class v0, Lknk;

    new-instance v1, Lknk;

    invoke-direct {v1, p1}, Lknk;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 35
    :cond_2
    const-class v0, Lkmv;

    if-ne p2, v0, :cond_3

    .line 36
    const-class v0, Lkmv;

    new-instance v1, Lkmv;

    invoke-direct {v1, p1}, Lkmv;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 37
    :cond_3
    const-class v0, Lheo;

    if-ne p2, v0, :cond_4

    .line 38
    const-class v0, Lheo;

    const-class v1, Lkmv;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 39
    :cond_4
    const-class v0, Lkly;

    if-ne p2, v0, :cond_5

    .line 40
    const-class v0, Lkly;

    new-instance v1, Lkly;

    invoke-direct {v1, p1}, Lkly;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 41
    :cond_5
    const-class v0, Lkjh;

    if-ne p2, v0, :cond_6

    .line 42
    const-class v0, Lkjh;

    new-instance v1, Lklx;

    invoke-direct {v1}, Lklx;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 43
    :cond_6
    const-class v0, Ljhk;

    if-ne p2, v0, :cond_7

    .line 44
    const-class v0, Ljhk;

    new-instance v1, Lklj;

    invoke-direct {v1, p1}, Lklj;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :cond_7
    const-class v0, Liwq;

    if-ne p2, v0, :cond_8

    .line 46
    const-class v0, Liwq;

    const-class v1, Lkmv;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :cond_8
    const-class v0, Lixl;

    if-ne p2, v0, :cond_0

    .line 48
    const-class v0, Lixl;

    const-class v1, Lkmv;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
