.class public Lcom/google/android/libraries/social/async/BackgroundTaskService;
.super Landroid/app/Service;
.source "PG"

# interfaces
.implements Lhnz;


# static fields
.field private static final a:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private b:Lhny;

.field private final c:Ljava/lang/Runnable;

.field private d:Lhor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lhol;

    invoke-direct {v0}, Lhol;-><init>()V

    .line 55
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->a:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 60
    new-instance v0, Lhom;

    invoke-direct {v0, p0}, Lhom;-><init>(Lcom/google/android/libraries/social/async/BackgroundTaskService;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->c:Ljava/lang/Runnable;

    .line 151
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/async/BackgroundTaskService;)Lhor;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->d:Lhor;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/async/BackgroundTaskService;)Lhny;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->b:Lhny;

    if-nez v0, :cond_0

    new-instance v0, Lhop;

    invoke-direct {v0, p0}, Lhop;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->b:Lhny;

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->b:Lhny;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/async/BackgroundTaskService;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->c:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/async/BackgroundTaskService;)Lhny;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->b:Lhny;

    return-object v0
.end method


# virtual methods
.method public a(Lhny;Lhoz;)V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->d:Lhor;

    invoke-virtual {v0}, Lhor;->d()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lhoo;

    invoke-direct {v1, p0, p1, p2}, Lhoo;-><init>(Lcom/google/android/libraries/social/async/BackgroundTaskService;Lhny;Lhoz;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 136
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 79
    const-class v0, Lhor;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhor;

    iput-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->d:Lhor;

    .line 80
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 86
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/async/BackgroundTaskService;->d:Lhor;

    invoke-virtual {v0}, Lhor;->c()Lhny;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0, p0}, Lhny;->a(Lhnz;)V

    .line 89
    sget-object v1, Lcom/google/android/libraries/social/async/BackgroundTaskService;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lhon;

    invoke-direct {v2, v0}, Lhon;-><init>(Lhny;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 96
    :cond_0
    const/4 v0, 0x2

    return v0
.end method
