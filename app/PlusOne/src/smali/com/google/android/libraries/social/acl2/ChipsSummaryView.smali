.class public Lcom/google/android/libraries/social/acl2/ChipsSummaryView;
.super Landroid/widget/ScrollView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhzn;


# instance fields
.field private final a:Lhhx;

.field private b:Lhzl;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhij;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

.field private e:I

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    iput v3, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->e:I

    .line 38
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0227

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 39
    invoke-virtual {p0, p0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    new-instance v1, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-direct {v1, p1, v4}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    .line 41
    iget-object v1, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v1, v0, v0, v0, v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->setPadding(IIII)V

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v3, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 46
    if-eqz p2, :cond_0

    .line 47
    const-string v0, "maxLines"

    invoke-interface {p2, v4, v0, v3}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->e:I

    .line 52
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 53
    iget v1, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->e:I

    if-eq v1, v3, :cond_1

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 54
    iget v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->e:I

    .line 57
    :cond_1
    const-class v0, Lhij;

    invoke-static {p1, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->c:Ljava/util/List;

    .line 58
    const-class v0, Lhhx;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhx;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->a:Lhhx;

    .line 60
    const-class v0, Lhzl;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->b:Lhzl;

    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->b:Lhzl;

    if-eqz v0, :cond_3

    .line 62
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->b:Lhzl;

    invoke-interface {v1}, Lhzl;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->a(ILjava/util/Collection;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->b:Lhzl;

    instance-of v0, v0, Lhzp;

    if-eqz v0, :cond_2

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->b:Lhzl;

    check-cast v0, Lhzp;

    invoke-interface {v0, p0}, Lhzp;->a(Lhzn;)V

    .line 70
    :cond_2
    :goto_0
    return-void

    .line 68
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 80
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhih;

    .line 82
    invoke-virtual {v0}, Lhih;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 83
    invoke-virtual {v0}, Lhih;->c()V

    .line 80
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 86
    :cond_1
    return-void
.end method

.method public a(ILandroid/os/Parcelable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->f:Z

    .line 118
    sget-object v0, Lhik;->a:[I

    add-int/lit8 v2, p1, -0x1

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 120
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->f:Z

    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhij;

    invoke-interface {v0, p2}, Lhij;->a(Landroid/os/Parcelable;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lhij;->a(Landroid/content/Context;Landroid/os/Parcelable;)Lhii;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    new-instance v1, Lhih;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lhih;-><init>(Landroid/content/Context;Lhii;)V

    invoke-virtual {v1, p2}, Lhih;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1, p0}, Lhih;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1

    .line 124
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {p0, p2}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 127
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->removeAllViews()V

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(ILjava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 135
    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->a(ILandroid/os/Parcelable;)V

    goto :goto_0

    .line 137
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Landroid/widget/ScrollView;->onAttachedToWindow()V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 76
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->f:Z

    .line 142
    instance-of v0, p1, Lhih;

    if-eqz v0, :cond_2

    .line 143
    check-cast p1, Lhih;

    .line 145
    invoke-virtual {p1}, Lhih;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    iget-object v1, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->b:Lhzl;

    invoke-virtual {p1}, Lhih;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-interface {v1, v0}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->a()V

    .line 149
    invoke-virtual {p1}, Lhih;->b()V

    goto :goto_0

    .line 152
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->a()V

    .line 153
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->a:Lhhx;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->a:Lhhx;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lhhx;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 109
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 110
    iget-boolean v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->g:Z

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->smoothScrollTo(II)V

    .line 113
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->onMeasure(II)V

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->getMeasuredHeight()I

    move-result v2

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->d:Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;

    iget v1, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->e:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/multilinelayout/MultiLineLayout;->a(I)I

    move-result v1

    .line 101
    if-lt v2, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->g:Z

    .line 102
    iget-boolean v0, p0, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->g:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 103
    :goto_1
    const v1, 0x7fffffff

    invoke-static {v1, p1}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->resolveSize(II)I

    move-result v1

    .line 104
    invoke-static {v0, p2}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->resolveSize(II)I

    move-result v0

    .line 103
    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/acl2/ChipsSummaryView;->setMeasuredDimension(II)V

    .line 105
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 102
    goto :goto_1
.end method
