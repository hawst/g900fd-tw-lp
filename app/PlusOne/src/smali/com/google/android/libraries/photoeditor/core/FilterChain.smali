.class public Lcom/google/android/libraries/photoeditor/core/FilterChain;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# instance fields
.field private final a:Landroid/graphics/RectF;

.field private b:F

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/photoeditor/core/FilterChainNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    .line 38
    iput v2, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b:F

    .line 39
    iput v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->c:I

    .line 40
    iput v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->d:I

    .line 41
    iput v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->e:I

    .line 42
    iput v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->f:I

    .line 48
    const/16 v0, 0x12

    .line 49
    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 562
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    .line 38
    iput v2, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b:F

    .line 39
    iput v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->c:I

    .line 40
    iput v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->d:I

    .line 41
    iput v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->e:I

    .line 42
    iput v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->f:I

    .line 48
    const/16 v0, 0x12

    .line 49
    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 562
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    .line 60
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 61
    return-void
.end method

.method private g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 158
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getFilterNodeAt(I)Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    move-result-object v1

    .line 159
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;->getFilterParameter()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v1

    const/16 v2, 0x12

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    iget-object v3, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;-><init>(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 135
    :cond_0
    return-void
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 326
    const/high16 v0, -0x3dcc0000    # -45.0f

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x42340000    # 45.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 327
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal value of RotationAngle."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_1
    iput p1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b:F

    .line 330
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 339
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-le p1, v0, :cond_1

    .line 340
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal value of aspectRatio."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :cond_1
    iput p1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->c:I

    .line 343
    return-void
.end method

.method public a(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 314
    invoke-virtual {p1}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal value of width or height."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 318
    return-void
.end method

.method public a(Landroid/graphics/RectF;FIIII)V
    .locals 2

    .prologue
    .line 292
    invoke-virtual {p1}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-ltz p3, :cond_0

    if-ltz p5, :cond_0

    if-ltz p6, :cond_0

    const/high16 v0, -0x3dcc0000    # -45.0f

    cmpg-float v0, p2, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x42340000    # 45.0f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    .line 294
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal value of width or height or aspectRatio or rotationAngle."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 298
    iput p2, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b:F

    .line 299
    iput p3, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->c:I

    .line 300
    iput p4, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->d:I

    .line 301
    iput p5, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->e:I

    .line 302
    iput p6, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->f:I

    .line 304
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->e()V

    .line 305
    return-void
.end method

.method public a(Lcom/google/android/libraries/photoeditor/core/FilterChainNode;)V
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p1}, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;->getFilterParameter()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 250
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 251
    return-void
.end method

.method public a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    invoke-virtual {p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 93
    :pswitch_0
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;-><init>(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 94
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Lcom/google/android/libraries/photoeditor/core/FilterChainNode;)V

    .line 98
    :goto_0
    :pswitch_1
    return-void

    .line 76
    :pswitch_2
    iput-object p1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 77
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;-><init>(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 79
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 82
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a([BI)V
    .locals 2

    .prologue
    .line 112
    packed-switch p2, :pswitch_data_0

    .line 119
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 126
    return-void

    .line 114
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    const/16 v1, 0xc9

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterBuffer(I[B)Z

    goto :goto_0

    .line 118
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    const/16 v1, 0xca

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterBuffer(I[B)Z

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    return-object v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 352
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    .line 353
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal value of postRotation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 355
    :cond_1
    iput p1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->d:I

    .line 356
    return-void
.end method

.method public b(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V
    .locals 2

    .prologue
    .line 225
    const/16 v0, 0x262

    .line 226
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectX()F

    move-result v1

    .line 225
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 227
    const/16 v0, 0x263

    .line 228
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectY()F

    move-result v1

    .line 227
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 229
    const/16 v0, 0x264

    .line 230
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectWidth()F

    move-result v1

    .line 229
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 231
    const/16 v0, 0x265

    .line 232
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectHeight()F

    move-result v1

    .line 231
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 233
    const/16 v0, 0x266

    .line 234
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getRotationAngle()F

    move-result v1

    .line 233
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 235
    const/16 v0, 0x267

    .line 236
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getAspectRatio()I

    move-result v1

    .line 235
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 237
    const/16 v0, 0x268

    .line 238
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v1

    .line 237
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 239
    const/16 v0, 0x269

    .line 240
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getImageWidth()I

    move-result v1

    .line 239
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 241
    const/16 v0, 0x26a

    .line 242
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getImageHeight()I

    move-result v1

    .line 241
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 243
    return-void
.end method

.method public c()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;->getFilterParameter()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 364
    if-gez p1, :cond_0

    .line 365
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ImageWidth must be larger than 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_0
    iput p1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->e:I

    .line 368
    return-void
.end method

.method public d()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 205
    const/4 v0, 0x0

    .line 206
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 207
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getFilterNodeAt(I)Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    move-result-object v0

    move-object v1, v0

    .line 210
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 211
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    .line 212
    if-eq v0, v1, :cond_0

    .line 213
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 218
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, v4, v4, v6, v6}, Landroid/graphics/RectF;->set(FFFF)V

    iput v4, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b:F

    iput v5, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->c:I

    iput v5, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->d:I

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->e()V

    .line 219
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 375
    if-gez p1, :cond_0

    .line 376
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ImageHeight must be larger than 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378
    :cond_0
    iput p1, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->f:I

    .line 379
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 257
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Lcom/google/android/libraries/photoeditor/core/FilterChainNode;)V

    .line 257
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 260
    :cond_0
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 266
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    .line 267
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->d:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAspectRatio()I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 426
    iget v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->c:I

    return v0
.end method

.method public getCropRectHeight()F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    return v0
.end method

.method public getCropRectWidth()F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    return v0
.end method

.method public getCropRectX()F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    return v0
.end method

.method public getCropRectY()F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    return v0
.end method

.method public getFilterNodeAt(I)Lcom/google/android/libraries/photoeditor/core/FilterChainNode;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 176
    const/4 v0, 0x0

    .line 178
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    goto :goto_0
.end method

.method public getFilterNodes()Ljava/util/List;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/photoeditor/core/FilterChainNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getImageHeight()I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 450
    iget v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->f:I

    return v0
.end method

.method public getImageWidth()I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 442
    iget v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->e:I

    return v0
.end method

.method public getPostRotation()I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 434
    iget v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->d:I

    return v0
.end method

.method public getRotationAngle()F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 418
    iget v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b:F

    return v0
.end method

.method public size()I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/FilterChain;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
