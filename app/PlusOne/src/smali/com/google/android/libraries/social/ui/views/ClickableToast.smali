.class public Lcom/google/android/libraries/social/ui/views/ClickableToast;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Ljava/lang/Runnable;

.field private b:J

.field private c:Landroid/widget/TextView;

.field private d:Lliu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Llis;

    invoke-direct {v0, p0}, Llis;-><init>(Lcom/google/android/libraries/social/ui/views/ClickableToast;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a:Ljava/lang/Runnable;

    .line 46
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->b:J

    .line 68
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setVisibility(I)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance v0, Llis;

    invoke-direct {v0, p0}, Llis;-><init>(Lcom/google/android/libraries/social/ui/views/ClickableToast;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a:Ljava/lang/Runnable;

    .line 46
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->b:J

    .line 68
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setVisibility(I)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a()V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    new-instance v0, Llis;

    invoke-direct {v0, p0}, Llis;-><init>(Lcom/google/android/libraries/social/ui/views/ClickableToast;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a:Ljava/lang/Runnable;

    .line 46
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->b:J

    .line 68
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setVisibility(I)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a()V

    .line 65
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 124
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->c:Landroid/widget/TextView;

    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->addView(Landroid/view/View;)V

    .line 126
    invoke-virtual {p0, p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    const v0, 0x7f020086

    const v1, 0x7f0b013a

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a(II)V

    .line 128
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 73
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setBackgroundResource(I)V

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 94
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->b()V

    .line 113
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a:Ljava/lang/Runnable;

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 121
    :goto_0
    return-void

    .line 116
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 117
    iget-wide p1, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->b:J

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->a:Ljava/lang/Runnable;

    invoke-static {v0, p1, p2}, Llsx;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public a(Lliu;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->d:Lliu;

    .line 85
    return-void
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setVisibility(I)V

    .line 132
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setClickable(Z)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->invalidate()V

    .line 134
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x437f0000    # 255.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 137
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setAlpha(F)V

    .line 138
    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 141
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 143
    :cond_1
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setClickable(Z)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->invalidate()V

    .line 148
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 150
    invoke-static {}, Llsj;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 153
    :cond_0
    new-instance v1, Llit;

    invoke-direct {v1, p0}, Llit;-><init>(Lcom/google/android/libraries/social/ui/views/ClickableToast;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->d:Lliu;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/ClickableToast;->d:Lliu;

    invoke-interface {v0}, Lliu;->a()V

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/ClickableToast;->c()V

    .line 185
    return-void
.end method
