.class public abstract Lcom/google/android/libraries/social/resources/images/ImageResource;
.super Lkda;
.source "PG"

# interfaces
.implements Lorg/chromium/net/HttpUrlRequestListener;


# static fields
.field private static sDebugDecimalFormat:Ljava/text/DecimalFormat;


# instance fields
.field private mContentLength:J

.field private mDebugContentType:Ljava/lang/String;

.field private mDebugFileSize:Ljava/lang/String;

.field private mDebugPixelSize:Ljava/lang/String;

.field private mDebugTimingInfo:Ljava/lang/String;

.field private mDecodeTime:J

.field private mDecodeTimeString:Ljava/lang/String;

.field private mDownloadEndTimestamp:J

.field private mDownloadStartTimestamp:J

.field private mDownloadTimeString:Ljava/lang/String;

.field private mDownloadUrl:Ljava/lang/String;

.field private mFileSize:I

.field public final mManager:Lkdv;

.field private mMimeType:Ljava/lang/String;

.field private mNegotiatedProtocol:Ljava/lang/String;

.field private mNetworkRequest:Lorg/chromium/net/HttpUrlRequest;

.field private mPartialDataFile:Ljava/io/File;

.field private mPartialResponse:Z

.field private mResponseStartedTime:J

.field private mSizeInBytes:I


# direct methods
.method public constructor <init>(Lkdv;Lkds;)V
    .locals 1

    .prologue
    .line 193
    invoke-direct {p0, p1, p2}, Lkda;-><init>(Lkdg;Lkdc;)V

    .line 156
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mSizeInBytes:I

    .line 194
    iput-object p1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    .line 195
    return-void
.end method

.method public static synthetic access$000(Lcom/google/android/libraries/social/resources/images/ImageResource;)Lkdc;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    return-object v0
.end method

.method private deliver(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 728
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    .line 729
    iget v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    if-eq v2, v3, :cond_1

    iget v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    if-eq v2, v1, :cond_1

    .line 730
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 732
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getStatusAsString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x35

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Resource no longer needed, not delivering: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", status: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 731
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 801
    :cond_0
    :goto_0
    return-void

    .line 737
    :cond_1
    iget v2, v0, Lkds;->h:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_3

    .line 738
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 740
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getRawFile()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x30

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Completing a download-only request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " file name: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 739
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 742
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getRawFile()Ljava/io/File;

    move-result-object v2

    invoke-interface {v0, p0, v1, v2}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    goto :goto_0

    .line 746
    :cond_3
    iget v2, v0, Lkds;->h:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_5

    .line 747
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 748
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x38

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Image decoding disabled. Delivering bytes to consumers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 750
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    .line 751
    invoke-static {p1}, Llrx;->a(Ljava/nio/ByteBuffer;)[B

    move-result-object v2

    .line 750
    invoke-interface {v0, p0, v1, v2}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 756
    :cond_5
    :try_start_0
    invoke-static {p1}, Lirp;->a(Ljava/nio/ByteBuffer;)Z

    move-result v2

    .line 757
    if-eqz v2, :cond_6

    .line 758
    const/4 v3, 0x2

    iput v3, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResourceType:I

    .line 761
    :cond_6
    iget v0, v0, Lkds;->h:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    if-eqz v2, :cond_8

    .line 762
    new-instance v0, Lirp;

    invoke-static {p1}, Llrx;->a(Ljava/nio/ByteBuffer;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lirp;-><init>([B)V

    .line 763
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliverResource(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 788
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 789
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Out of memory while decoding image: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 791
    :cond_7
    new-instance v0, Lkdr;

    invoke-direct {v0, p0}, Lkdr;-><init>(Lcom/google/android/libraries/social/resources/images/ImageResource;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 799
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 766
    :cond_8
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    iget v0, v0, Lkds;->h:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_a

    move v0, v1

    .line 768
    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->decodeBitmap(Ljava/nio/ByteBuffer;Z)Ljava/lang/Object;

    move-result-object v0

    .line 769
    if-eqz v0, :cond_b

    .line 770
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 771
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Delivering image to consumers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 773
    :cond_9
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliverResource(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 766
    :cond_a
    const/4 v0, 0x0

    goto :goto_1

    .line 776
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 777
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Bad image; cannot decode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 780
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getRawFile()Ljava/io/File;

    move-result-object v0

    .line 781
    if-eqz v0, :cond_d

    .line 782
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 784
    :cond_d
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    const/4 v1, 0x5

    invoke-interface {v0, p0, v1}, Lkdv;->a(Lkda;I)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private getBitmapSize(Landroid/graphics/Bitmap;)I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 667
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 668
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v0

    .line 672
    :goto_0
    return v0

    .line 670
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    goto :goto_0
.end method

.method private getDownloadUrlForPartialDataFile(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4

    .prologue
    .line 387
    :try_start_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "u"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Llsu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 396
    :goto_1
    return-object v0

    .line 387
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 389
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getDownloadUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 390
    :catch_1
    move-exception v0

    .line 391
    const-string v1, "Cannot obtain download URL for partial file"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 392
    if-eqz p2, :cond_1

    .line 393
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 394
    new-instance v1, Ljava/io/File;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "u"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 396
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getDownloadUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 394
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private isBitmapPackingEnabled()Z
    .locals 2

    .prologue
    .line 970
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPartialDownloadFileName(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 235
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x7e

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private saveDownloadUrl(Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    .line 870
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    .line 874
    :goto_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "u"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 876
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Llsu;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 877
    const/4 v0, 0x1

    .line 882
    :goto_2
    return v0

    .line 870
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    goto :goto_0

    .line 874
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 878
    :catch_0
    move-exception v1

    .line 879
    const-string v2, "Cannot save download URL"

    invoke-virtual {p0, v2, v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 881
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 882
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private saveToCache(Ljava/lang/String;Ljava/nio/ByteBuffer;ZZ)V
    .locals 1

    .prologue
    .line 858
    if-eqz p3, :cond_0

    .line 859
    invoke-direct {p0, p1, p4}, Lcom/google/android/libraries/social/resources/images/ImageResource;->saveDownloadUrl(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 867
    :goto_0
    return-void

    .line 864
    :cond_0
    if-eqz p4, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    .line 865
    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    .line 866
    :goto_1
    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/social/filecache/FileCache;->write(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 865
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    goto :goto_1
.end method

.method private startDirectToDiskDownload()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 282
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    move-object v6, v0

    check-cast v6, Lkds;

    .line 284
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getPartialDownloadFileName()Ljava/lang/String;

    move-result-object v1

    .line 285
    iget v0, v6, Lkds;->h:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    .line 286
    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    .line 287
    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 288
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    .line 289
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 290
    invoke-direct {p0, v0, v2}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getDownloadUrlForPartialDataFile(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    .line 293
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getIdentifier()Lkdc;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x22

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Downloading using URL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " resource: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 297
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 298
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 299
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 303
    :cond_2
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 310
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 311
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Download URL is null: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    goto/16 :goto_0

    .line 304
    :catch_0
    move-exception v0

    .line 305
    const-string v1, "Cannot open cache file"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 306
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliverDownloadError(I)V

    .line 328
    :goto_1
    return-void

    .line 314
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    const/4 v3, 0x0

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Ljgm;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v3

    .line 317
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-interface {v3, v0, v1}, Lorg/chromium/net/HttpUrlRequest;->a(J)V

    .line 318
    iget v0, v6, Lkds;->h:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    .line 320
    invoke-interface {v0}, Lkdv;->j()J

    move-result-wide v0

    .line 322
    :goto_2
    iget v4, v6, Lkds;->h:I

    and-int/lit16 v4, v4, 0x800

    if-nez v4, :cond_5

    move v2, v7

    :cond_5
    invoke-interface {v3, v0, v1, v2}, Lorg/chromium/net/HttpUrlRequest;->a(JZ)V

    .line 324
    monitor-enter p0

    .line 325
    :try_start_1
    iput-object v3, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mNetworkRequest:Lorg/chromium/net/HttpUrlRequest;

    .line 326
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    invoke-interface {v3}, Lorg/chromium/net/HttpUrlRequest;->g()V

    goto :goto_1

    .line 320
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    .line 321
    invoke-interface {v0}, Lkdv;->k()J

    move-result-wide v0

    goto :goto_2

    .line 326
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method private startDownload()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v2, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 331
    iput-wide v10, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    .line 332
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getPartialDownloadFileName()Ljava/lang/String;

    move-result-object v0

    .line 333
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v1}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    .line 334
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    if-eqz v1, :cond_0

    .line 335
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v9}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getDownloadUrlForPartialDataFile(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    .line 337
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    iput-object v3, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    .line 342
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 343
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getIdentifier()Lkdc;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x22

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Downloading using URL: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " resource: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 347
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    if-eqz v1, :cond_3

    .line 350
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 351
    iget-wide v4, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    .line 352
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getIdentifier()Lkdc;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x44

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Continuing download to file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " ("

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " bytes) resource: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 351
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 354
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    const/4 v1, 0x1

    .line 355
    invoke-static {v0, v1}, Llrx;->a(Ljava/io/File;Z)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 356
    new-instance v4, Lorg/chromium/net/ChunkedWritableByteChannel;

    invoke-direct {v4}, Lorg/chromium/net/ChunkedWritableByteChannel;-><init>()V

    .line 357
    invoke-interface {v4, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 358
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    :goto_0
    if-eqz v4, :cond_4

    .line 367
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Ljgm;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    .line 370
    iget-wide v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    invoke-interface {v0, v2, v3}, Lorg/chromium/net/HttpUrlRequest;->a(J)V

    .line 377
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v1}, Lkdv;->k()J

    move-result-wide v2

    .line 378
    invoke-interface {v0, v2, v3, v9}, Lorg/chromium/net/HttpUrlRequest;->a(JZ)V

    .line 379
    monitor-enter p0

    .line 380
    :try_start_1
    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mNetworkRequest:Lorg/chromium/net/HttpUrlRequest;

    .line 381
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382
    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->g()V

    .line 383
    return-void

    .line 360
    :catch_0
    move-exception v0

    .line 361
    iput-wide v10, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    :cond_3
    move-object v4, v3

    goto :goto_0

    .line 372
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, p0}, Ljgm;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    goto :goto_1

    .line 381
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method


# virtual methods
.method protected appendDescription(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 987
    const-string v0, "\n  Size:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getSizeInBytes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 988
    return-void
.end method

.method public cancelDownload()V
    .locals 1

    .prologue
    .line 538
    iget-boolean v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloading:Z

    if-nez v0, :cond_1

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 546
    :cond_1
    monitor-enter p0

    .line 547
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mNetworkRequest:Lorg/chromium/net/HttpUrlRequest;

    .line 548
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    if-eqz v0, :cond_0

    .line 550
    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->h()V

    goto :goto_0

    .line 548
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public decodeBitmap(Ljava/nio/ByteBuffer;Z)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 887
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0, p0, p1, p2}, Lkdv;->a(Lcom/google/android/libraries/social/resources/images/ImageResource;Ljava/nio/ByteBuffer;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public deliverAndCacheData(Ljava/nio/ByteBuffer;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 804
    if-nez p1, :cond_1

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 808
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    .line 809
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->onDownloadEnd()V

    .line 811
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    if-eqz v1, :cond_2

    .line 812
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 815
    :cond_2
    iget v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    if-eq v1, v8, :cond_3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-eqz v1, :cond_0

    .line 820
    :cond_3
    iget-wide v4, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    if-eq v1, v8, :cond_6

    move v4, v3

    .line 826
    :goto_1
    if-eqz v4, :cond_7

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getPartialDownloadFileName()Ljava/lang/String;

    move-result-object v1

    .line 827
    :goto_2
    iget v5, v0, Lkds;->h:I

    and-int/lit16 v5, v5, 0x400

    if-eqz v5, :cond_4

    move v2, v3

    .line 829
    :cond_4
    iget v5, v0, Lkds;->h:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_9

    .line 830
    invoke-direct {p0, v1, p1, v4, v2}, Lcom/google/android/libraries/social/resources/images/ImageResource;->saveToCache(Ljava/lang/String;Ljava/nio/ByteBuffer;ZZ)V

    .line 831
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 832
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 833
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getRawFile()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-eqz v2, :cond_8

    const-string v0, "; long-term cache"

    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x30

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Completing a download-only request: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " file name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 832
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 836
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getRawFile()Ljava/io/File;

    move-result-object v1

    invoke-interface {v0, p0, v3, v1}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 822
    :cond_6
    iget-wide v4, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_d

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    int-to-long v4, v1

    iget-wide v6, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    cmp-long v1, v4, v6

    if-gez v1, :cond_d

    move v4, v3

    .line 823
    goto/16 :goto_1

    .line 826
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getCacheFileName()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 833
    :cond_8
    const-string v0, ""

    goto :goto_3

    .line 840
    :cond_9
    if-nez v4, :cond_a

    .line 841
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliver(Ljava/nio/ByteBuffer;)V

    .line 844
    :cond_a
    iget v0, v0, Lkds;->h:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 848
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 849
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_c

    const-string v0, "; long-term cache"

    :goto_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x28

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Saving image in file cache: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " file name: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 853
    :cond_b
    invoke-direct {p0, v1, p1, v4, v2}, Lcom/google/android/libraries/social/resources/images/ImageResource;->saveToCache(Ljava/lang/String;Ljava/nio/ByteBuffer;ZZ)V

    goto/16 :goto_0

    .line 849
    :cond_c
    const-string v0, ""

    goto :goto_4

    :cond_d
    move v4, v2

    goto/16 :goto_1
.end method

.method public deliverData(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 503
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    .line 506
    iget v2, v0, Lkds;->h:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_3

    .line 507
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->onDownloadEnd()V

    .line 512
    iget-boolean v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialResponse:Z

    if-eqz v2, :cond_2

    .line 513
    iget-object v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    .line 514
    iget-object v3, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    iget v0, v0, Lkds;->h:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, v3, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->saveDownloadUrl(Ljava/lang/String;Z)Z

    move-object v0, v2

    .line 520
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 521
    iget-object v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x30

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Completing a download-only request: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " file name: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 524
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v2, p0, v1, v0}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    .line 535
    :goto_2
    return-void

    .line 514
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 517
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getRawFile()Ljava/io/File;

    move-result-object v0

    .line 518
    iget-object v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto :goto_1

    .line 528
    :cond_3
    check-cast p1, Ljava/nio/ByteBuffer;

    .line 529
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 530
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 531
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Delivering data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; buffer has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 534
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliverAndCacheData(Ljava/nio/ByteBuffer;)V

    goto :goto_2
.end method

.method public deliverDownloadError(I)V
    .locals 0

    .prologue
    .line 975
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->onDownloadEnd()V

    .line 976
    invoke-super {p0, p1}, Lkda;->deliverDownloadError(I)V

    .line 977
    return-void
.end method

.method public deliverHttpError(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 981
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->onDownloadEnd()V

    .line 982
    invoke-super {p0, p1, p2}, Lkda;->deliverHttpError(ILjava/lang/String;)V

    .line 983
    return-void
.end method

.method public downloadResource()V
    .locals 4

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->onDownloadStart()V

    .line 270
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getDownloadUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    .line 271
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 272
    new-instance v0, Ljava/lang/NullPointerException;

    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to download null image url: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    iget v0, v0, Lkds;->h:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 275
    invoke-direct {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->startDirectToDiskDownload()V

    .line 279
    :goto_0
    return-void

    .line 277
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->startDownload()V

    goto :goto_0
.end method

.method public generateDebugContentType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1036
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getMimeType()Ljava/lang/String;

    move-result-object v0

    .line 1037
    if-eqz v0, :cond_1

    .line 1038
    const-string v1, "image/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1039
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1044
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected generateDebugFileSize()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1061
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getFileSize()I

    move-result v0

    .line 1063
    if-gtz v0, :cond_0

    .line 1064
    const/4 v0, 0x0

    .line 1089
    :goto_0
    return-object v0

    .line 1070
    :cond_0
    const/high16 v1, 0x100000

    if-le v0, v1, :cond_2

    .line 1071
    int-to-double v0, v0

    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    div-double v2, v0, v2

    .line 1072
    const-string v0, "MB"

    .line 1081
    :goto_1
    sget-object v1, Lcom/google/android/libraries/social/resources/images/ImageResource;->sDebugDecimalFormat:Ljava/text/DecimalFormat;

    if-nez v1, :cond_1

    .line 1082
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v4, "@@@"

    invoke-direct {v1, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/libraries/social/resources/images/ImageResource;->sDebugDecimalFormat:Ljava/text/DecimalFormat;

    .line 1085
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/libraries/social/resources/images/ImageResource;->sDebugDecimalFormat:Ljava/text/DecimalFormat;

    .line 1086
    invoke-virtual {v4, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x20

    .line 1087
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1088
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1089
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1073
    :cond_2
    const/16 v1, 0x400

    if-le v0, v1, :cond_3

    .line 1074
    int-to-double v0, v0

    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    div-double v2, v0, v2

    .line 1075
    const-string v0, "kB"

    goto :goto_1

    .line 1077
    :cond_3
    int-to-double v2, v0

    .line 1078
    const-string v0, "B"

    goto :goto_1
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 576
    :goto_0
    return-object v0

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Lkdo;

    if-eqz v0, :cond_1

    .line 574
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Lkdo;

    iget-object v0, v0, Lkdo;->a:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 576
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCacheFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getShortFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCachedFile()Ljava/io/File;
    .locals 2

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getCacheFileName()Ljava/lang/String;

    move-result-object v1

    .line 257
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 258
    if-eqz v0, :cond_0

    .line 262
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public getDebugContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugContentType:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1049
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugContentType:Ljava/lang/String;

    .line 1057
    :goto_0
    return-object v0

    .line 1052
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->generateDebugContentType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugContentType:Ljava/lang/String;

    .line 1053
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugContentType:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1054
    const-string v0, "unknown"

    goto :goto_0

    .line 1057
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugContentType:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDebugFileSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugFileSize:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1094
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugFileSize:Ljava/lang/String;

    .line 1102
    :goto_0
    return-object v0

    .line 1097
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->generateDebugFileSize()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugFileSize:Ljava/lang/String;

    .line 1098
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugFileSize:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1099
    const-string v0, "unknown"

    goto :goto_0

    .line 1102
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugFileSize:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDebugPixelSize()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1106
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugPixelSize:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1107
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugPixelSize:Ljava/lang/String;

    .line 1117
    :goto_0
    return-object v0

    .line 1110
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getWidth()I

    move-result v0

    .line 1111
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getHeight()I

    move-result v1

    .line 1112
    add-int v2, v0, v1

    if-nez v2, :cond_1

    .line 1113
    const/4 v0, 0x0

    goto :goto_0

    .line 1116
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugPixelSize:Ljava/lang/String;

    .line 1117
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugPixelSize:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDebugTimingInfo()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1121
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugTimingInfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1122
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugTimingInfo:Ljava/lang/String;

    .line 1135
    :goto_0
    return-object v0

    .line 1125
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadTimeString:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDecodeTimeString:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1126
    const/4 v0, 0x0

    goto :goto_0

    .line 1129
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadTimeString:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDecodeTimeString:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1130
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadTimeString:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDecodeTimeString:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugTimingInfo:Ljava/lang/String;

    .line 1135
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugTimingInfo:Ljava/lang/String;

    goto :goto_0

    .line 1131
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDecodeTimeString:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1132
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDecodeTimeString:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugTimingInfo:Ljava/lang/String;

    goto :goto_1
.end method

.method public abstract getDownloadUrl()Ljava/lang/String;
.end method

.method public getFileSize()I
    .locals 1

    .prologue
    .line 1032
    iget v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mFileSize:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getHeight(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getHeight(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 602
    :goto_0
    return v0

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Lkdo;

    if-eqz v0, :cond_1

    .line 600
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Lkdo;

    iget v0, v0, Lkdo;->c:I

    goto :goto_0

    .line 602
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getManager()Lkdv;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getPartialDownloadFileName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getCacheFileName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRawFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    .line 244
    iget v1, v0, Lkds;->h:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 245
    const/4 v0, 0x0

    .line 250
    :goto_0
    return-object v0

    .line 248
    :cond_0
    iget v0, v0, Lkds;->h:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    .line 249
    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    .line 250
    :goto_1
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getCacheFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->d()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    goto :goto_1
.end method

.method public getResourceType()I
    .locals 1

    .prologue
    .line 609
    iget v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResourceType:I

    return v0
.end method

.method public abstract getShortFileName()Ljava/lang/String;
.end method

.method public getSizeInBytes()I
    .locals 2

    .prologue
    .line 639
    iget v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mSizeInBytes:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 640
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 641
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 643
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 644
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getBitmapSize(Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mSizeInBytes:I

    .line 661
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mSizeInBytes:I

    return v0

    .line 645
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Lkdo;

    if-eqz v0, :cond_2

    .line 646
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Lkdo;

    iget-object v0, v0, Lkdo;->a:Landroid/graphics/Bitmap;

    .line 647
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getBitmapSize(Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mSizeInBytes:I

    goto :goto_0

    .line 648
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Lirp;

    if-eqz v0, :cond_3

    .line 649
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Lirp;

    .line 650
    invoke-virtual {v0}, Lirp;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mSizeInBytes:I

    goto :goto_0

    .line 651
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Lkdt;

    if-eqz v0, :cond_4

    .line 652
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Lkdt;

    .line 653
    iget v0, v0, Lkdt;->f:I

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mSizeInBytes:I

    goto :goto_0

    .line 654
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_5

    .line 655
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, [B

    array-length v0, v0

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mSizeInBytes:I

    goto :goto_0

    .line 657
    :cond_5
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mSizeInBytes:I

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getWidth(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getWidth(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 584
    instance-of v0, p1, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 585
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 589
    :goto_0
    return v0

    .line 586
    :cond_0
    instance-of v0, p1, Lkdo;

    if-eqz v0, :cond_1

    .line 587
    check-cast p1, Lkdo;

    iget v0, p1, Lkdo;->b:I

    goto :goto_0

    .line 589
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCacheEnabled()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 614
    iget v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 631
    :goto_0
    return v0

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    iget v0, v0, Lkds;->h:I

    and-int/lit8 v0, v0, 0xa

    if-eqz v0, :cond_1

    move v0, v1

    .line 620
    goto :goto_0

    .line 623
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 624
    invoke-direct {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isBitmapPackingEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 625
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 626
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Lkdo;

    if-eqz v0, :cond_7

    .line 627
    invoke-direct {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isBitmapPackingEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Lkdo;

    iget-object v0, v0, Lkdo;->a:Landroid/graphics/Bitmap;

    .line 628
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-eqz v0, :cond_6

    :cond_5
    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move v0, v2

    .line 631
    goto :goto_0
.end method

.method public load()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 677
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    .line 679
    iget v2, v0, Lkds;->h:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_1

    .line 680
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getRawFile()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x22

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Loading disabled for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " file name: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 683
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    const/4 v2, 0x3

    invoke-interface {v0, p0, v2, v1}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    .line 725
    :goto_0
    return-void

    .line 687
    :cond_1
    iget v2, v0, Lkds;->h:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_3

    .line 688
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getCachedFile()Ljava/io/File;

    move-result-object v2

    .line 689
    if-eqz v2, :cond_3

    .line 691
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 692
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2e

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Returning file name to consumers: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " file name: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 695
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0, p0, v6, v2}, Lkdv;->a(Lkda;ILjava/lang/Object;)V

    goto :goto_0

    .line 701
    :cond_3
    iget v0, v0, Lkds;->h:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_5

    .line 702
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getCachedFile()Ljava/io/File;

    move-result-object v0

    .line 703
    if-eqz v0, :cond_5

    .line 704
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 705
    iget-object v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x25

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Loading image from file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " file name: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 708
    :cond_4
    const/4 v2, 0x1

    :try_start_0
    invoke-static {v0, v2}, Llrx;->a(Ljava/io/File;Z)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 717
    :goto_1
    if-eqz v0, :cond_6

    .line 718
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliver(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    .line 713
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1

    .line 711
    :catch_1
    move-exception v2

    .line 712
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x12

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot load file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    move-object v0, v1

    goto :goto_1

    .line 720
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 721
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Requesting download: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 723
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->downloadResource()V

    goto/16 :goto_0
.end method

.method public logDecodeTime(JLjava/lang/String;IIILandroid/graphics/Bitmap;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 992
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDecodeTime:J

    .line 993
    iget-wide v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDecodeTime:J

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDecodeTimeString:Ljava/lang/String;

    .line 994
    iput-object p3, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mMimeType:Ljava/lang/String;

    .line 995
    iput p4, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mFileSize:I

    .line 996
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 997
    if-nez p7, :cond_1

    const-string v0, "null"

    .line 999
    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDecodeTimeString:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x50

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Decoded "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " byte "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from source ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] into bitmap "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " in "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 1004
    :cond_0
    iput-object v5, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugContentType:Ljava/lang/String;

    .line 1005
    iput-object v5, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugFileSize:Ljava/lang/String;

    .line 1006
    iput-object v5, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugPixelSize:Ljava/lang/String;

    .line 1007
    iput-object v5, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDebugTimingInfo:Ljava/lang/String;

    .line 1008
    return-void

    .line 997
    :cond_1
    invoke-virtual {p7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 998
    invoke-virtual {p7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected onDownloadEnd()V
    .locals 5

    .prologue
    .line 407
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloading:Z

    .line 408
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadEndTimestamp:J

    .line 409
    iget-wide v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadEndTimestamp:J

    iget-wide v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadStartTimestamp:J

    sub-long/2addr v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadTimeString:Ljava/lang/String;

    .line 410
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0, p0}, Lkdv;->b(Lcom/google/android/libraries/social/resources/images/ImageResource;)V

    .line 411
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadTimeString:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Download completed in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 414
    :cond_0
    return-void
.end method

.method protected onDownloadStart()V
    .locals 2

    .prologue
    .line 401
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloading:Z

    .line 402
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadStartTimestamp:J

    .line 403
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0, p0}, Lkdv;->a(Lcom/google/android/libraries/social/resources/images/ImageResource;)V

    .line 404
    return-void
.end method

.method public onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 14

    .prologue
    .line 418
    monitor-enter p0

    .line 419
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mNetworkRequest:Lorg/chromium/net/HttpUrlRequest;

    if-eq p1, v0, :cond_0

    .line 420
    monitor-exit p0

    .line 493
    :goto_0
    return-void

    .line 423
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mNetworkRequest:Lorg/chromium/net/HttpUrlRequest;

    .line 424
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    invoke-virtual {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->onDownloadEnd()V

    .line 428
    const/4 v0, 0x0

    .line 429
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->c()Ljava/io/IOException;

    move-result-object v1

    .line 430
    instance-of v2, v1, Lorg/chromium/net/ResponseTooLargeException;

    if-eqz v2, :cond_c

    .line 431
    const/4 v2, 0x1

    .line 432
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x14

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Response too large: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    .line 433
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    .line 434
    iget v0, v0, Lkds;->h:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_1

    .line 435
    const/4 v0, 0x0

    .line 436
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialResponse:Z

    move-object v1, v0

    .line 440
    :cond_1
    :goto_1
    if-eqz v1, :cond_5

    .line 442
    const-string v0, "ImageResource"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 443
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Network Exception: Id is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    :cond_2
    const-string v3, "Network exception: "

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 446
    if-eqz v2, :cond_4

    const/4 v0, 0x5

    :goto_3
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliverDownloadError(I)V

    goto/16 :goto_0

    .line 424
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 445
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 446
    :cond_4
    const/4 v0, 0x4

    goto :goto_3

    .line 451
    :cond_5
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->b()I

    move-result v12

    .line 452
    const/16 v0, 0xc8

    if-eq v12, v0, :cond_7

    .line 453
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    if-eqz v0, :cond_6

    .line 454
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialDataFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 456
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p0, v12, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->deliverHttpError(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 458
    :cond_7
    iget v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    .line 463
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mPartialResponse:Z

    .line 466
    :cond_8
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->a()J

    move-result-wide v0

    .line 467
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_a

    .line 468
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    .line 473
    :goto_4
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "logged_in"

    aput-object v3, v1, v2

    .line 474
    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 475
    const/4 v1, 0x0

    .line 476
    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_9

    .line 477
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 478
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 479
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 482
    :cond_9
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->k()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadStartTimestamp:J

    iget-wide v6, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResponseStartedTime:J

    iget-wide v8, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mDownloadEndTimestamp:J

    iget-wide v10, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    iget-object v13, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mNegotiatedProtocol:Ljava/lang/String;

    invoke-interface/range {v0 .. v13}, Lkdv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJJILjava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    .line 487
    iget v0, v0, Lkds;->h:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_b

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->d()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 492
    :goto_5
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v1, p0, v0}, Lkdv;->a(Lkda;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 470
    :cond_a
    iget-wide v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mContentLength:J

    goto :goto_4

    .line 487
    :cond_b
    const/4 v0, 0x0

    goto :goto_5

    :cond_c
    move v2, v0

    goto/16 :goto_1
.end method

.method public onResponseStarted(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 2

    .prologue
    .line 497
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResponseStartedTime:J

    .line 498
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mNegotiatedProtocol:Ljava/lang/String;

    .line 499
    return-void
.end method

.method public pack()V
    .locals 6

    .prologue
    .line 891
    iget v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    iget v0, v0, Lkds;->h:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Lkdo;

    if-eqz v0, :cond_1

    .line 894
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isBitmapPackingEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 936
    :cond_1
    :goto_0
    return-void

    .line 901
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 902
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 903
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 904
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    move v5, v1

    move v1, v2

    move-object v2, v0

    move v0, v5

    .line 912
    :goto_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    .line 913
    if-eqz v3, :cond_1

    .line 921
    :try_start_0
    new-instance v4, Lkdt;

    invoke-direct {v4}, Lkdt;-><init>()V

    .line 922
    iput-object v3, v4, Lkdt;->a:Landroid/graphics/Bitmap$Config;

    .line 923
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iput v3, v4, Lkdt;->d:I

    .line 924
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iput v3, v4, Lkdt;->e:I

    .line 925
    iput v1, v4, Lkdt;->b:I

    .line 926
    iput v0, v4, Lkdt;->c:I

    .line 927
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, v4, Lkdt;->f:I

    .line 928
    iget v0, v4, Lkdt;->f:I

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, v4, Lkdt;->g:Ljava/nio/ByteBuffer;

    .line 929
    iget-object v0, v4, Lkdt;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 930
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v0, v2}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 931
    iput-object v4, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    .line 932
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 934
    :catch_0
    move-exception v0

    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    goto :goto_0

    .line 906
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Lkdo;

    .line 907
    iget-object v2, v0, Lkdo;->a:Landroid/graphics/Bitmap;

    .line 908
    iget v1, v0, Lkdo;->b:I

    .line 909
    iget v0, v0, Lkdo;->c:I

    goto :goto_1
.end method

.method public recycle()V
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mId:Lkdc;

    check-cast v0, Lkds;

    iget v0, v0, Lkds;->h:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_1

    .line 557
    const/4 v0, 0x0

    .line 558
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v1, v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 559
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 563
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 564
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mManager:Lkdv;

    invoke-interface {v1, v0}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 567
    :cond_1
    invoke-super {p0}, Lkda;->recycle()V

    .line 568
    return-void

    .line 560
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v1, v1, Lkdo;

    if-eqz v1, :cond_0

    .line 561
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Lkdo;

    iget-object v0, v0, Lkdo;->a:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public unpack()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 939
    iget v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Lkdt;

    if-nez v0, :cond_1

    .line 964
    :cond_0
    :goto_0
    return-void

    .line 943
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isBitmapPackingEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 944
    iput v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    goto :goto_0

    .line 948
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Lkdt;

    .line 950
    :try_start_0
    iget v1, v0, Lkdt;->d:I

    iget v2, v0, Lkdt;->e:I

    iget-object v3, v0, Lkdt;->a:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 952
    iget-object v2, v0, Lkdt;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 953
    iget-object v2, v0, Lkdt;->g:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 954
    iget v2, v0, Lkdt;->b:I

    iget v3, v0, Lkdt;->d:I

    if-ne v2, v3, :cond_3

    iget v2, v0, Lkdt;->c:I

    iget v3, v0, Lkdt;->e:I

    if-eq v2, v3, :cond_4

    .line 955
    :cond_3
    new-instance v2, Lkdo;

    iget v3, v0, Lkdt;->b:I

    iget v0, v0, Lkdt;->c:I

    invoke-direct {v2, v1, v3, v0}, Lkdo;-><init>(Landroid/graphics/Bitmap;II)V

    iput-object v2, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    .line 959
    :goto_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 961
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;

    .line 962
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mStatus:I

    goto :goto_0

    .line 957
    :cond_4
    :try_start_1
    iput-object v1, p0, Lcom/google/android/libraries/social/resources/images/ImageResource;->mResource:Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
