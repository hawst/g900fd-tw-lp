.class public Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private a:Z

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:Z

.field private g:Lkch;

.field private h:Lkcp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/high16 v3, 0x43160000    # 150.0f

    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    iput v3, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->e:F

    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 68
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    const v2, 0x3a83126f    # 0.001f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->d:F

    .line 72
    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->d:F

    div-float/2addr v0, v1

    const/high16 v1, 0x40200000    # 2.5f

    div-float/2addr v0, v1

    .line 73
    const/high16 v1, 0x43960000    # 300.0f

    .line 74
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 73
    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->e:F

    .line 76
    return-void

    .line 68
    :cond_0
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    goto :goto_0
.end method

.method public static a(Landroid/widget/AbsListView;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 85
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 88
    :cond_1
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 89
    goto :goto_0

    .line 91
    :cond_2
    invoke-virtual {p0}, Landroid/widget/AbsListView;->getPaddingTop()I

    move-result v2

    .line 92
    invoke-virtual {p0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    if-ge v3, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lkch;Lkcp;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->g:Lkch;

    .line 81
    iput-object p2, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->h:Lkcp;

    .line 82
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 210
    iput-boolean p1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->f:Z

    .line 211
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 121
    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->g:Lkch;

    if-nez v1, :cond_0

    .line 122
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 188
    :goto_0
    return v0

    .line 125
    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    .line 126
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 188
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 128
    :pswitch_0
    iget-boolean v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->g:Lkch;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->g:Lkch;

    invoke-interface {v1}, Lkch;->Z()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->g:Lkch;

    invoke-interface {v1}, Lkch;->Y()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->g:Lkch;

    invoke-interface {v1}, Lkch;->aQ_()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->f:Z

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    if-eqz v1, :cond_1

    .line 129
    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->h:Lkcp;

    invoke-interface {v1, v0}, Lkcp;->a(F)V

    .line 130
    iput-boolean v2, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a:Z

    iput v4, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->b:F

    iget v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->b:F

    iput v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->c:F

    goto :goto_1

    :cond_2
    move v1, v3

    .line 128
    goto :goto_2

    .line 136
    :pswitch_1
    iget-boolean v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->f:Z

    if-eqz v1, :cond_3

    .line 137
    iput-boolean v3, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a:Z

    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->h:Lkcp;

    invoke-interface {v0}, Lkcp;->c()V

    goto :goto_1

    .line 141
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    iget v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->b:F

    sub-float v1, v4, v1

    .line 144
    iget v2, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->d:F

    div-float/2addr v1, v2

    .line 145
    iget v2, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->e:F

    cmpl-float v2, v1, v2

    if-lez v2, :cond_4

    .line 146
    iput-boolean v3, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a:Z

    .line 147
    iget-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->g:Lkch;

    invoke-interface {v0}, Lkch;->K_()V

    goto :goto_1

    .line 152
    :cond_4
    iget v2, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->c:F

    sub-float/2addr v2, v4

    .line 153
    iget v5, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->d:F

    div-float/2addr v2, v5

    .line 154
    const/high16 v5, 0x41200000    # 10.0f

    cmpl-float v2, v2, v5

    if-lez v2, :cond_5

    .line 155
    iput-boolean v3, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a:Z

    .line 156
    iget-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->h:Lkcp;

    invoke-interface {v0}, Lkcp;->c()V

    goto :goto_1

    .line 162
    :cond_5
    const/high16 v2, 0x41700000    # 15.0f

    cmpg-float v2, v1, v2

    if-gez v2, :cond_6

    .line 169
    :goto_3
    iget-object v1, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->h:Lkcp;

    iget v2, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->e:F

    div-float/2addr v0, v2

    invoke-interface {v1, v0}, Lkcp;->a(F)V

    .line 172
    iget v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->c:F

    cmpl-float v0, v4, v0

    if-lez v0, :cond_1

    .line 173
    iput v4, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->c:F

    goto/16 :goto_1

    .line 166
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->h:Lkcp;

    invoke-interface {v0}, Lkcp;->b()V

    move v0, v1

    goto :goto_3

    .line 181
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a:Z

    if-eqz v0, :cond_1

    .line 182
    iput-boolean v3, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a:Z

    .line 183
    iget-object v0, p0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->h:Lkcp;

    invoke-interface {v0}, Lkcp;->c()V

    goto/16 :goto_1

    .line 126
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
