.class public Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;
.super Llon;
.source "PG"

# interfaces
.implements Ljby;


# instance fields
.field private final g:Ljbx;

.field private final h:Lhkd;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Llon;-><init>()V

    .line 29
    new-instance v0, Ljbx;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Ljbx;-><init>(Landroid/app/Activity;Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->g:Ljbx;

    .line 30
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->e:Llnh;

    .line 31
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    .line 39
    new-instance v0, Ljbz;

    invoke-direct {v0, p0}, Ljbz;-><init>(Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->h:Lhkd;

    .line 91
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 35
    invoke-super {p0, p1}, Llon;->a(Landroid/os/Bundle;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->e:Llnh;

    const-class v1, Ljbx;

    iget-object v2, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->g:Ljbx;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 37
    return-void
.end method

.method public a(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lizr;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 79
    const-string v1, "shareables"

    iget-object v2, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->g:Ljbx;

    .line 80
    iget-object v2, v2, Ljbx;->a:Ljava/util/ArrayList;

    .line 79
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 81
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->finish()V

    .line 83
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 52
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 54
    const-string v0, "media_picker_mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 55
    packed-switch v0, :pswitch_data_0

    .line 70
    :goto_0
    new-instance v0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-direct {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;-><init>()V

    .line 71
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->f(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->f()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2, v0}, Lat;->a(ILu;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->g:Ljbx;

    invoke-virtual {v0, p0}, Ljbx;->a(Ljby;)V

    .line 74
    return-void

    .line 57
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->e:Llnh;

    const-class v2, Ljbt;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbt;

    const v2, 0x7f10004f

    iget-object v3, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->h:Lhkd;

    invoke-interface {v0, v2, v3}, Ljbt;->a(ILhkd;)V

    goto :goto_0

    .line 62
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->e:Llnh;

    const-class v2, Ljbt;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbt;

    const v2, 0x7f10004e

    iget-object v3, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->h:Lhkd;

    invoke-interface {v0, v2, v3}, Ljbt;->a(ILhkd;)V

    goto :goto_0

    .line 66
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->e:Llnh;

    const-class v2, Ljbt;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbt;

    const v2, 0x7f100050

    iget-object v3, p0, Lcom/google/android/libraries/social/mediapicker/MediaPickerActivity;->h:Lhkd;

    invoke-interface {v0, v2, v3}, Ljbt;->a(ILhkd;)V

    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
