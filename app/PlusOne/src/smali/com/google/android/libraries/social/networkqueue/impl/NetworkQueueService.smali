.class public Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;
.super Landroid/app/Service;
.source "PG"


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljhz;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljia;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->a:Ljava/lang/Object;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 182
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 183
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "logged_in"

    aput-object v3, v2, v1

    invoke-interface {v0, v2}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 184
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_0

    .line 185
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-wide/16 v4, 0x0

    invoke-direct {p0, v0, v4, v5, v6}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->a(IJZ)V

    .line 184
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 187
    :cond_0
    return-void
.end method

.method private a(IJZ)V
    .locals 6

    .prologue
    .line 201
    const-string v0, "NetworkQueueService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const-wide/16 v0, 0x3e8

    div-long v0, p2, v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x4a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Schedule processing: accountId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", delay="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sec"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhz;

    .line 208
    if-nez v0, :cond_3

    .line 209
    const-class v0, Ljhi;

    .line 210
    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhi;

    invoke-interface {v0, p1}, Ljhi;->a(I)Ljhh;

    move-result-object v2

    .line 211
    if-nez v2, :cond_1

    .line 212
    monitor-exit v1

    .line 228
    :goto_0
    return-void

    .line 217
    :cond_1
    new-instance v0, Ljhz;

    .line 218
    invoke-virtual {p0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->c:Ljia;

    invoke-direct {v0, v3, v2, v4}, Ljhz;-><init>(Landroid/content/Context;Ljhh;Ljia;)V

    .line 219
    invoke-virtual {v0}, Ljhz;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 220
    monitor-exit v1

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 222
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 227
    :cond_3
    invoke-virtual {v0, p2, p3, p4}, Ljhz;->a(JZ)V

    .line 228
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->b:Landroid/util/SparseArray;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 78
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->b:Landroid/util/SparseArray;

    .line 79
    new-instance v0, Ljid;

    invoke-direct {v0, p0}, Ljid;-><init>(Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->c:Ljia;

    .line 102
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 106
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 107
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v4, 0x4

    const/4 v6, 0x3

    const/4 v2, -0x1

    .line 114
    const-string v0, "NetworkQueueService"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x36

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onStartCommand with id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " flags: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 118
    :cond_0
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_2

    .line 119
    invoke-direct {p0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->a()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->stopSelf()V

    .line 171
    :cond_1
    :goto_0
    return v6

    .line 131
    :cond_2
    const-string v0, "network_queue_scheduler"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 132
    const-string v1, "account_id"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 134
    packed-switch v0, :pswitch_data_0

    .line 168
    :pswitch_0
    const-string v1, "NetworkQueueService"

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-virtual {p0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->stopSelf()V

    goto :goto_0

    .line 136
    :pswitch_1
    if-ne v1, v2, :cond_3

    .line 137
    const-string v0, "NetworkQueueService"

    const-string v2, "Invalid account id for process."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_3
    const-string v0, "extra_delay_millis"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 140
    const-string v0, "extra_retry_ioexceptions"

    const/4 v4, 0x1

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 141
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->a(IJZ)V

    goto :goto_0

    .line 145
    :pswitch_2
    if-ne v1, v2, :cond_4

    .line 146
    const-string v0, "NetworkQueueService"

    const-string v2, "Invalid account id for cancel."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_4
    const-class v0, Ljhi;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhi;

    invoke-interface {v0, v1}, Ljhi;->a(I)Ljhh;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhz;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljhz;->b(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->c:Ljia;

    invoke-interface {v1, v0, v2}, Ljia;->a(Ljhz;Ljhh;)V

    :cond_5
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 149
    :pswitch_3
    const-string v0, "item_id"

    invoke-virtual {p1, v0, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 153
    cmp-long v0, v2, v8

    if-eqz v0, :cond_6

    .line 154
    const-string v0, "NetworkQueueService"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x48

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Queue item timed out for account "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " itemId:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 156
    :cond_6
    const-string v0, "NetworkQueueService"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_0

    .line 163
    :pswitch_4
    const-string v0, "NetworkQueueService"

    const-string v1, "No command specified in intent."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-virtual {p0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->stopSelf()V

    goto/16 :goto_0

    .line 134
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
