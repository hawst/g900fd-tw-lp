.class public Lcom/google/android/libraries/social/squares/membership/JoinButton;
.super Landroid/widget/Button;
.source "PG"

# interfaces
.implements Lhmm;
.implements Lkvv;


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a:Ljava/lang/String;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->b:I

    .line 25
    iput-boolean v1, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->c:Z

    .line 37
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setGravity(I)V

    .line 38
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a(I)V

    .line 40
    sget-object v0, Lkwa;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 41
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->c:Z

    .line 42
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 43
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 11

    .prologue
    const/4 v9, 0x2

    const v1, -0x8c8c8d

    const v5, 0x7f0205d8

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 63
    iget v0, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->b:I

    if-ne v0, p1, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iput p1, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->b:I

    .line 73
    iget-boolean v0, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, -0x1

    .line 74
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->c:Z

    if-eqz v4, :cond_3

    const v4, 0x7f02055d

    .line 77
    :goto_2
    packed-switch p1, :pswitch_data_0

    .line 128
    :pswitch_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 73
    goto :goto_1

    :cond_3
    move v4, v5

    .line 74
    goto :goto_2

    .line 79
    :pswitch_1
    const v1, 0x7f0a04c6

    move v5, v1

    move v1, v4

    move v4, v0

    move v0, v2

    .line 133
    :goto_3
    const/4 v6, 0x4

    new-array v6, v6, [I

    .line 134
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getPaddingLeft()I

    move-result v7

    aput v7, v6, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getPaddingTop()I

    move-result v7

    aput v7, v6, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getPaddingRight()I

    move-result v7

    aput v7, v6, v9

    const/4 v7, 0x3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->getPaddingBottom()I

    move-result v8

    aput v8, v6, v7

    .line 136
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setText(I)V

    .line 137
    invoke-virtual {p0, v4}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setTextColor(I)V

    .line 138
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setBackgroundResource(I)V

    .line 139
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setEnabled(Z)V

    .line 140
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setVisibility(I)V

    .line 143
    aget v0, v6, v3

    aget v1, v6, v2

    aget v2, v6, v9

    const/4 v4, 0x3

    aget v4, v6, v4

    invoke-virtual {p0, v0, v1, v2, v4}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setPadding(IIII)V

    .line 145
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 148
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setVisibility(I)V

    .line 149
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->setVisibility(I)V

    goto :goto_0

    .line 87
    :pswitch_2
    const v1, 0x7f0a04c7

    move v5, v1

    move v1, v4

    move v4, v0

    move v0, v2

    .line 91
    goto :goto_3

    .line 95
    :pswitch_3
    const v1, 0x7f0a04c8

    move v5, v1

    move v1, v4

    move v4, v0

    move v0, v2

    .line 99
    goto :goto_3

    .line 103
    :pswitch_4
    const v0, 0x7f0a04c9

    move v4, v1

    move v1, v5

    move v5, v0

    move v0, v2

    .line 107
    goto :goto_3

    .line 111
    :pswitch_5
    const v1, 0x7f0a04ca

    .line 112
    const v0, -0x333334

    move v4, v0

    move v0, v3

    move v10, v5

    move v5, v1

    move v1, v10

    .line 115
    goto :goto_3

    .line 119
    :pswitch_6
    const v1, 0x7f0a04cb

    .line 120
    const v0, -0x777778

    move v4, v0

    move v0, v2

    move v10, v5

    move v5, v1

    move v1, v10

    .line 123
    goto :goto_3

    .line 77
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a:Ljava/lang/String;

    .line 47
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a(I)V

    .line 48
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->c:Z

    .line 52
    return-void
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/google/android/libraries/social/squares/membership/JoinButton;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 188
    :pswitch_0
    const/4 v0, 0x0

    .line 192
    :goto_0
    return-object v0

    .line 158
    :pswitch_1
    sget-object v0, Lomv;->a:Lhmn;

    .line 192
    :goto_1
    new-instance v1, Lkqw;

    iget-object v2, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lkqw;-><init>(Lhmn;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 163
    :pswitch_2
    sget-object v0, Lomv;->h:Lhmn;

    goto :goto_1

    .line 168
    :pswitch_3
    sget-object v0, Lomv;->P:Lhmn;

    goto :goto_1

    .line 173
    :pswitch_4
    sget-object v0, Lomv;->R:Lhmn;

    goto :goto_1

    .line 178
    :pswitch_5
    sget-object v0, Lomv;->e:Lhmn;

    goto :goto_1

    .line 182
    :pswitch_6
    sget-object v0, Lomv;->ah:Lhmn;

    goto :goto_1

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/libraries/social/squares/membership/JoinButton;->b:I

    return v0
.end method
