.class public Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# static fields
.field private static a:F

.field private static b:F

.field private static c:F

.field private static d:F

.field private static e:I

.field private static f:Landroid/graphics/drawable/Drawable;

.field private static g:Landroid/graphics/drawable/Drawable;


# instance fields
.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/ImageView;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const v4, 0x7f10005e

    const/4 v2, -0x2

    const/4 v3, 0x0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    sget-object v0, Ljtw;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->m:Z

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 60
    sget v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Landroid/content/res/Resources;)V

    .line 65
    :cond_0
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    .line 66
    iget-object v1, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->m:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->b:F

    :goto_0
    invoke-virtual {v1, v3, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 71
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Landroid/view/View;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->addView(Landroid/view/View;)V

    .line 74
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->j:Landroid/widget/ImageView;

    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setId(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->j:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->j:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->j:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->addView(Landroid/view/View;)V

    .line 82
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    .line 83
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 85
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_2

    .line 86
    const/16 v1, 0x10

    invoke-virtual {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 90
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v1, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->m:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->d:F

    :goto_2
    invoke-virtual {v1, v3, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b013d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Landroid/view/View;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->addView(Landroid/view/View;)V

    .line 96
    return-void

    .line 66
    :cond_1
    sget v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a:F

    goto/16 :goto_0

    .line 88
    :cond_2
    invoke-virtual {v0, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_1

    .line 91
    :cond_3
    sget v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->c:F

    goto :goto_2
.end method

.method private a(Landroid/view/View;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 100
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 101
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/view/View;->setTextAlignment(I)V

    .line 103
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;IIII)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 236
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, p4

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v1, p2

    invoke-virtual {p1, v0, p3, v1, p5}, Landroid/view/View;->layout(IIII)V

    .line 241
    :goto_0
    return-void

    .line 239
    :cond_0
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    const v1, 0x7f0d024b

    .line 106
    const v0, 0x7f0d024c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a:F

    .line 107
    const v0, 0x7f0d024d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->b:F

    .line 108
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->c:F

    .line 109
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->d:F

    .line 110
    const v0, 0x7f0d0202

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->e:I

    .line 111
    const v0, 0x7f0203b2

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->f:Landroid/graphics/drawable/Drawable;

    .line 112
    const v0, 0x7f0201a7

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->g:Landroid/graphics/drawable/Drawable;

    .line 113
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 133
    iput-object p3, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->n:Ljava/lang/CharSequence;

    .line 134
    iput-boolean p6, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->l:Z

    .line 136
    if-eqz p2, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    sget-object v3, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4, v4, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    sget v3, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->e:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 142
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->k:Z

    .line 145
    iget-boolean v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->k:Z

    if-eqz v0, :cond_2

    if-eqz p5, :cond_2

    .line 146
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Llhv;->a(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :goto_2
    iget-object v3, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->k:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->j:Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->l:Z

    if-eqz v3, :cond_5

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 144
    goto :goto_1

    .line 147
    :cond_2
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 150
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    move v0, v2

    .line 152
    goto :goto_3

    :cond_5
    move v1, v2

    .line 154
    goto :goto_4
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 215
    sub-int v8, p4, p2

    .line 216
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    .line 217
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    .line 219
    add-int v7, v5, v0

    .line 221
    iget-object v1, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Landroid/view/View;IIII)V

    .line 223
    const/4 v4, 0x0

    .line 225
    iget-boolean v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->l:Z

    if-eqz v0, :cond_0

    .line 226
    sget-object v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 227
    iget-object v3, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->j:Landroid/widget/ImageView;

    const/4 v4, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Landroid/view/View;IIII)V

    .line 228
    sget v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->e:I

    add-int v4, v6, v0

    .line 231
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    move-object v2, p0

    move v6, v8

    invoke-direct/range {v2 .. v7}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->a(Landroid/view/View;IIII)V

    .line 232
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 159
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 160
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 163
    iget-object v3, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-static {v5, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    if-nez v6, :cond_0

    move v0, v1

    .line 164
    :goto_0
    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 163
    invoke-virtual {v3, v7, v0}, Landroid/widget/TextView;->measure(II)V

    .line 166
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    .line 169
    iget-boolean v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->l:Z

    if-eqz v0, :cond_8

    .line 170
    sget-object v0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 171
    iget-object v7, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->j:Landroid/widget/ImageView;

    .line 172
    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    sget-object v9, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->g:Landroid/graphics/drawable/Drawable;

    .line 173
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 171
    invoke-virtual {v7, v8, v9}, Landroid/widget/ImageView;->measure(II)V

    .line 174
    sget v7, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->e:I

    add-int/2addr v0, v7

    .line 177
    :goto_1
    iget-boolean v7, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->k:Z

    if-eqz v7, :cond_7

    .line 178
    sub-int/2addr v6, v3

    .line 181
    iget-object v7, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->h:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLineCount()I

    move-result v7

    .line 182
    const/4 v8, 0x2

    if-lt v7, v8, :cond_1

    .line 183
    iget-object v4, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    sub-int v0, v5, v0

    .line 184
    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 185
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 183
    invoke-virtual {v4, v0, v1}, Landroid/widget/TextView;->measure(II)V

    move v0, v3

    .line 210
    :goto_2
    invoke-virtual {p0, v5, v0}, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->setMeasuredDimension(II)V

    .line 211
    return-void

    :cond_0
    move v0, v2

    .line 163
    goto :goto_0

    .line 188
    :cond_1
    rsub-int/lit8 v7, v7, 0x3

    .line 189
    if-eq v7, v4, :cond_2

    iget-boolean v8, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->l:Z

    if-eqz v8, :cond_4

    .line 192
    :cond_2
    :goto_3
    if-eqz v4, :cond_3

    .line 193
    if-lez v7, :cond_5

    iget-object v8, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->n:Ljava/lang/CharSequence;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 194
    iget-object v8, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->n:Ljava/lang/CharSequence;

    sget-object v10, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 199
    :cond_3
    :goto_4
    iget-object v8, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 200
    iget-object v4, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 201
    iget-object v4, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    sub-int v0, v5, v0

    .line 202
    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    if-nez v6, :cond_6

    .line 203
    :goto_5
    invoke-static {v6, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 201
    invoke-virtual {v4, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 206
    iget-object v0, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v3

    goto :goto_2

    :cond_4
    move v4, v1

    .line 189
    goto :goto_3

    .line 196
    :cond_5
    iget-object v8, p0, Lcom/google/android/libraries/social/people/ui/PeopleListRowNameView;->i:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_6
    move v1, v2

    .line 202
    goto :goto_5

    :cond_7
    move v0, v3

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_1
.end method
