.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;
.super Ljnl;
.source "PG"


# static fields
.field private static o:Landroid/content/res/ColorStateList;

.field private static p:I


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/view/ViewGroup;

.field private C:Landroid/view/LayoutInflater;

.field public a:Ljmx;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/view/View;

.field private v:Landroid/view/ViewGroup;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/view/View;

.field private y:Landroid/view/ViewGroup;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->C:Landroid/view/LayoutInflater;

    .line 79
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->o:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 81
    const v1, 0x7f0b034b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->o:Landroid/content/res/ColorStateList;

    .line 82
    const v1, 0x7f0d012c

    .line 83
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->p:I

    .line 67
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->C:Landroid/view/LayoutInflater;

    .line 79
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->o:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 81
    const v1, 0x7f0b034b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->o:Landroid/content/res/ColorStateList;

    .line 82
    const v1, 0x7f0d012c

    .line 83
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->p:I

    .line 71
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->C:Landroid/view/LayoutInflater;

    .line 79
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->o:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 81
    const v1, 0x7f0b034b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->o:Landroid/content/res/ColorStateList;

    .line 82
    const v1, 0x7f0d012c

    .line 83
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->p:I

    .line 75
    :cond_0
    return-void
.end method

.method private a(Lnit;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->C:Landroid/view/LayoutInflater;

    const v1, 0x7f040138

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 110
    invoke-direct {p0, v0, p1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->a(Landroid/view/View;Lnit;)V

    .line 111
    return-object v0
.end method

.method private a(Landroid/view/View;Lnit;)V
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/4 v2, 0x0

    .line 115
    .line 116
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020416

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 115
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 117
    sget v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->f:I

    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->p:I

    sget v3, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->f:I

    sget v4, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->p:I

    invoke-virtual {p1, v0, v1, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 120
    const v0, 0x7f1003f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 121
    iget-object v1, p2, Lnit;->c:Ljava/lang/String;

    .line 122
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "http:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 123
    const-string v3, "http:"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 125
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Ljac;->a:Ljac;

    invoke-static {v3, v1, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 126
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->e(Landroid/graphics/drawable/Drawable;)V

    .line 128
    const v0, 0x7f100138

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 130
    iget-object v7, p2, Lnit;->d:Ljava/lang/String;

    .line 131
    new-instance v8, Landroid/text/SpannableString;

    invoke-direct {v8, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 132
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const-string v1, "sans-serif"

    sget v3, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->c:I

    sget-object v4, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->e:Landroid/content/res/ColorStateList;

    sget-object v5, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->e:Landroid/content/res/ColorStateList;

    invoke-direct/range {v0 .. v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    .line 133
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    .line 132
    invoke-interface {v8, v0, v2, v1, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 135
    iget-object v0, p2, Lnit;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 136
    iget-object v0, p2, Lnit;->e:Ljava/lang/String;

    const-string v1, "past-contributor-to"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0350

    .line 138
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 139
    new-instance v9, Landroid/text/SpannableString;

    invoke-direct {v9, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 140
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const-string v1, "sans-serif"

    sget v3, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->c:I

    sget-object v4, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->o:Landroid/content/res/ColorStateList;

    sget-object v5, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->o:Landroid/content/res/ColorStateList;

    invoke-direct/range {v0 .. v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    .line 142
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    .line 140
    invoke-interface {v9, v0, v2, v1, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 143
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    aput-object v8, v0, v2

    const/4 v1, 0x1

    const-string v2, " "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v9, v0, v1

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    :goto_1
    iget-object v0, p2, Lnit;->b:Ljava/lang/String;

    .line 152
    new-instance v1, Ljmw;

    invoke-direct {v1, p0, v0}, Ljmw;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    return-void

    .line 123
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 145
    :cond_2
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 148
    :cond_3
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static b(Lnjt;)Z
    .locals 1

    .prologue
    .line 253
    if-eqz p0, :cond_0

    .line 254
    iget-object v0, p0, Lnjt;->d:Lnib;

    .line 255
    if-eqz v0, :cond_0

    .line 256
    iget-object v0, v0, Lnib;->g:Lniu;

    .line 257
    if-eqz v0, :cond_0

    .line 258
    iget-object v0, v0, Lniu;->a:[Lnit;

    .line 259
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 260
    const/4 v0, 0x1

    .line 265
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljmx;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->a:Ljmx;

    .line 106
    return-void
.end method

.method public a(Lnjt;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 164
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->r:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->s:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 168
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 169
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->B:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 171
    const/4 v0, 0x0

    .line 173
    if-eqz p1, :cond_b

    .line 174
    iget-object v2, p1, Lnjt;->d:Lnib;

    .line 175
    if-eqz v2, :cond_b

    .line 176
    iget-object v2, v2, Lnib;->g:Lniu;

    .line 177
    if-eqz v2, :cond_b

    .line 178
    iget-object v0, v2, Lniu;->a:[Lnit;

    move-object v2, v0

    .line 183
    :goto_0
    if-eqz v2, :cond_7

    array-length v0, v2

    if-lez v0, :cond_7

    move v0, v1

    .line 185
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 186
    aget-object v3, v2, v0

    .line 187
    iget v4, v3, Lnit;->f:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->s:Landroid/view/View;

    invoke-direct {p0, v0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->a(Landroid/view/View;Lnit;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    move v0, v1

    .line 196
    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_3

    .line 197
    aget-object v3, v2, v0

    .line 198
    iget v4, v3, Lnit;->f:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 199
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->v:Landroid/view/ViewGroup;

    invoke-direct {p0, v3, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->a(Lnit;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 200
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 196
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 185
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 204
    :goto_3
    array-length v3, v2

    if-ge v0, v3, :cond_5

    .line 205
    aget-object v3, v2, v0

    .line 206
    iget v4, v3, Lnit;->f:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 207
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->y:Landroid/view/ViewGroup;

    invoke-direct {p0, v3, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->a(Lnit;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 208
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 204
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    move v0, v1

    .line 212
    :goto_4
    array-length v3, v2

    if-ge v0, v3, :cond_7

    .line 213
    aget-object v3, v2, v0

    .line 214
    iget v4, v3, Lnit;->f:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_6

    .line 215
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->B:Landroid/view/ViewGroup;

    invoke-direct {p0, v3, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->a(Lnit;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 216
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->B:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 212
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 221
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 222
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 231
    :goto_5
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_9

    .line 232
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 241
    :goto_6
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->B:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_a

    .line 242
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->B:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 250
    :goto_7
    return-void

    .line 226
    :cond_8
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->u:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->v:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_5

    .line 236
    :cond_9
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->x:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->y:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_6

    .line 246
    :cond_a
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->A:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->B:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_7

    :cond_b
    move-object v2, v0

    goto/16 :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 90
    const v0, 0x7f1003e9

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->q:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f1003ea

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->r:Landroid/view/View;

    .line 92
    const v0, 0x7f1003eb

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->s:Landroid/view/View;

    .line 93
    const v0, 0x7f1003ec

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->t:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f1003ed

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->u:Landroid/view/View;

    .line 95
    const v0, 0x7f1003ee

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->v:Landroid/view/ViewGroup;

    .line 96
    const v0, 0x7f1003ef

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->w:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f1003f0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->x:Landroid/view/View;

    .line 98
    const v0, 0x7f1003f1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->y:Landroid/view/ViewGroup;

    .line 99
    const v0, 0x7f1003f2

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->z:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f1003f3

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->A:Landroid/view/View;

    .line 101
    const v0, 0x7f1003f4

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->B:Landroid/view/ViewGroup;

    .line 102
    return-void
.end method
