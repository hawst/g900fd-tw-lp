.class public Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# static fields
.field private static a:F


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/support/v7/widget/SearchView;

.field private g:Landroid/widget/TextView;

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 66
    sget v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    const v1, 0x7f0d0233

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a:F

    .line 50
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    sget v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    const v1, 0x7f0d0233

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a:F

    .line 54
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    sget v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    const v1, 0x7f0d0233

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a:F

    .line 58
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 66
    sget v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    const v1, 0x7f0d0233

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a:F

    .line 63
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;)Landroid/support/v7/widget/SearchView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->b()V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    const-class v1, Lhid;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhid;

    .line 148
    if-eqz v0, :cond_0

    .line 149
    invoke-interface {v0}, Lhid;->d()V

    .line 151
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 230
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->b(Ljava/lang/CharSequence;)V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    const-class v1, Lhid;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhid;

    .line 138
    if-eqz v0, :cond_0

    .line 139
    invoke-interface {v0}, Lhid;->c()V

    .line 141
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a(ZZ)V

    .line 201
    return-void
.end method

.method public a(ZZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 204
    iget-boolean v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->i:Z

    if-ne v0, p1, :cond_0

    .line 221
    :goto_0
    return-void

    .line 207
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->i:Z

    .line 209
    if-eqz p1, :cond_2

    const/4 v0, 0x0

    .line 210
    :goto_1
    invoke-static {}, Llsm;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_3

    :goto_2
    if-eqz v1, :cond_1

    .line 211
    neg-float v0, v0

    .line 214
    :cond_1
    if-eqz p2, :cond_4

    .line 215
    iget-object v1, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 216
    iget-object v1, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0

    .line 209
    :cond_2
    sget v0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->a:F

    goto :goto_1

    .line 210
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 218
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 219
    iget-object v1, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 178
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 188
    iput-boolean v1, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->h:Z

    .line 189
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SearchView;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, p1, v2}, Landroid/support/v7/widget/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 163
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->requestFocus()Z

    .line 164
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    new-instance v1, Lhic;

    invoke-direct {v1, p0}, Lhic;-><init>(Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/SearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->h:Z

    .line 175
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 225
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 226
    return-void

    .line 225
    :cond_0
    const v0, 0x3eb33333    # 0.35f

    goto :goto_0
.end method

.method public c()Landroid/support/v7/widget/SearchView;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->h:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 74
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 76
    const v0, 0x7f100144

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->b:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->b:Landroid/view/View;

    new-instance v1, Lhhy;

    invoke-direct {v1, p0}, Lhhy;-><init>(Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const v0, 0x7f100146

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c:Landroid/view/View;

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c:Landroid/view/View;

    new-instance v1, Lhhz;

    invoke-direct {v1, p0}, Lhhz;-><init>(Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->c:Landroid/view/View;

    const v1, 0x3eb33333    # 0.35f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->i:Z

    .line 102
    const v0, 0x7f100147

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->d:Landroid/view/View;

    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->d:Landroid/view/View;

    new-instance v1, Lhia;

    invoke-direct {v1, p0}, Lhia;-><init>(Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    const v0, 0x7f100145

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->e:Landroid/view/View;

    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->e:Landroid/view/View;

    new-instance v1, Lhib;

    invoke-direct {v1, p0}, Lhib;-><init>(Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    const v0, 0x7f100149

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->a(Z)V

    .line 120
    iget-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->f:Landroid/support/v7/widget/SearchView;

    const v1, 0x7f10012b

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 122
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x1a

    .line 121
    invoke-static {v1, v0, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 123
    const v0, 0x7f100148

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->g:Landroid/widget/TextView;

    .line 125
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/acl2/AclPickerActionBarView;->g:Landroid/widget/TextView;

    const/16 v2, 0x2c

    .line 124
    invoke-static {v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 126
    return-void
.end method
