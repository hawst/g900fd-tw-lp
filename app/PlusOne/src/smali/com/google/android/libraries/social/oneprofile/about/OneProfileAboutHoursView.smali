.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;
.super Ljnl;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static final a:Ljava/lang/CharSequence;

.field private static o:I

.field private static p:I


# instance fields
.field private q:Landroid/view/LayoutInflater;

.field private r:Landroid/widget/LinearLayout;

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, " \u2013 "

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->a:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->q:Landroid/view/LayoutInflater;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->o:I

    if-nez v1, :cond_0

    .line 58
    const v1, 0x7f0b00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->o:I

    .line 60
    const v1, 0x7f0b00d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->p:I

    .line 44
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->q:Landroid/view/LayoutInflater;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->o:I

    if-nez v1, :cond_0

    .line 58
    const v1, 0x7f0b00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->o:I

    .line 60
    const v1, 0x7f0b00d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->p:I

    .line 48
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->q:Landroid/view/LayoutInflater;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->o:I

    if-nez v1, :cond_0

    .line 58
    const v1, 0x7f0b00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->o:I

    .line 60
    const v1, 0x7f0b00d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->p:I

    .line 52
    :cond_0
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 188
    packed-switch p1, :pswitch_data_0

    .line 204
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 190
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 192
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 194
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 196
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 198
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 200
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 202
    :pswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(Lnkh;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->q:Landroid/view/LayoutInflater;

    const v1, 0x7f040136

    .line 137
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;

    .line 138
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    const/4 v1, 0x1

    .line 140
    iget-object v7, p1, Lnkh;->c:[Lnkc;

    array-length v8, v7

    move v5, v4

    :goto_0
    if-ge v5, v8, :cond_4

    aget-object v9, v7, v5

    .line 141
    iget-object v3, v9, Lnkc;->b:Lnkb;

    if-eqz v3, :cond_1

    iget-object v3, v9, Lnkc;->c:Lnkb;

    if-eqz v3, :cond_1

    .line 142
    if-eqz v1, :cond_2

    move v3, v4

    .line 147
    :goto_1
    if-eqz v9, :cond_0

    iget-object v1, v9, Lnkc;->b:Lnkb;

    if-eqz v1, :cond_0

    iget-object v1, v9, Lnkc;->c:Lnkb;

    if-nez v1, :cond_3

    :cond_0
    move-object v1, v2

    :goto_2
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move v1, v3

    .line 140
    :cond_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 145
    :cond_2
    const-string v3, "\n"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v3, v1

    goto :goto_1

    .line 147
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v9, Lnkc;->b:Lnkb;

    iget-object v10, v10, Lnkb;->a:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v10, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    iget-object v9, v9, Lnkc;->c:Lnkb;

    iget-object v9, v9, Lnkb;->a:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 151
    :cond_4
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 152
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 154
    iget v3, p1, Lnkh;->b:I

    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->a(I)I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_5

    .line 156
    const/4 v2, 0x7

    iget v3, p1, Lnkh;->b:I

    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->a(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    move-object v2, v1

    .line 162
    :cond_5
    if-eqz v2, :cond_6

    .line 163
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "cccccc"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 164
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 169
    :goto_3
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->a(Ljava/lang/String;)V

    .line 170
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->b(Ljava/lang/String;)V

    .line 171
    iget-object v1, p1, Lnkh;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    iget-object v1, p1, Lnkh;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 172
    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->o:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->setBackgroundColor(I)V

    .line 178
    :goto_4
    return-object v0

    .line 166
    :cond_6
    const-string v1, ""

    goto :goto_3

    .line 174
    :cond_7
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->setBackgroundColor(I)V

    goto :goto_4

    :cond_8
    move-object v0, v2

    .line 178
    goto :goto_4
.end method

.method public static b(Lnjt;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 221
    invoke-static {p0}, Ljnu;->i(Lnjt;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 246
    :cond_0
    :goto_0
    return v0

    .line 226
    :cond_1
    iget-object v2, p0, Lnjt;->f:Lnjg;

    iget-object v2, v2, Lnjg;->b:Lnix;

    .line 228
    invoke-static {v2}, Ljnu;->a(Lnix;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 229
    iget-object v2, v2, Lnix;->d:Lpmm;

    iget-object v2, v2, Lpmm;->i:Lpqm;

    iget-object v4, v2, Lpqm;->a:[Lpqn;

    array-length v5, v4

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v2, v4, v3

    .line 230
    iget-object v6, v2, Lpqn;->c:[Lpqo;

    array-length v7, v6

    move v2, v0

    :goto_2
    if-ge v2, v7, :cond_3

    aget-object v8, v6, v2

    .line 231
    iget-object v8, v8, Lpqo;->b:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    move v0, v1

    .line 232
    goto :goto_0

    .line 230
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 229
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 236
    :cond_4
    invoke-static {v2}, Ljnu;->b(Lnix;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 237
    iget-object v2, v2, Lnix;->b:Lnhw;

    iget-object v4, v2, Lnhw;->a:[Lnkh;

    array-length v5, v4

    move v3, v0

    :goto_3
    if-ge v3, v5, :cond_0

    aget-object v2, v4, v3

    .line 238
    iget-object v6, v2, Lnkh;->c:[Lnkc;

    array-length v7, v6

    move v2, v0

    :goto_4
    if-ge v2, v7, :cond_6

    aget-object v8, v6, v2

    .line 239
    iget-object v9, v8, Lnkc;->b:Lnkb;

    if-eqz v9, :cond_5

    iget-object v8, v8, Lnkc;->c:Lnkb;

    if-eqz v8, :cond_5

    move v0, v1

    .line 240
    goto :goto_0

    .line 238
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 237
    :cond_6
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Ljnl;->a()V

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 101
    return-void
.end method

.method public a(Lnjt;)V
    .locals 14

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 72
    invoke-static {p1}, Ljnu;->i(Lnjt;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    :cond_0
    return-void

    .line 76
    :cond_1
    iget-object v0, p1, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    .line 77
    invoke-static {v0}, Ljnu;->a(Lnix;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 78
    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->i:Lpqm;

    iget-object v6, v0, Lpqm;->a:[Lpqn;

    array-length v7, v6

    move v5, v2

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v8, v6, v5

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->q:Landroid/view/LayoutInflater;

    const v1, 0x7f040136

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    iget-object v10, v8, Lpqn;->c:[Lpqo;

    array-length v11, v10

    move v4, v2

    :goto_1
    if-ge v4, v11, :cond_4

    aget-object v12, v10, v4

    iget-object v13, v12, Lpqo;->b:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_2

    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    iget-object v12, v12, Lpqo;->b:Ljava/lang/String;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    const-string v13, "\n"

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, v8, Lpqn;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->a(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->b(Ljava/lang/String;)V

    iget-object v1, v8, Lpqn;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    iget-object v1, v8, Lpqn;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->p:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->a(I)V

    sget v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->o:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->setBackgroundColor(I)V

    .line 80
    :goto_3
    if-eqz v0, :cond_5

    .line 81
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 78
    :cond_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 79
    :cond_6
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->a(I)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->setBackgroundColor(I)V

    goto :goto_3

    :cond_7
    move-object v0, v3

    goto :goto_3

    .line 85
    :cond_8
    invoke-static {v0}, Ljnu;->b(Lnix;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    iget-object v0, v0, Lnix;->b:Lnhw;

    iget-object v0, v0, Lnhw;->a:[Lnkh;

    array-length v1, v0

    :goto_4
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 87
    invoke-direct {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->a(Lnkh;)Landroid/view/View;

    move-result-object v3

    .line 88
    if-eqz v3, :cond_9

    .line 89
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 86
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 67
    const v0, 0x7f1003e7

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->r:Landroid/widget/LinearLayout;

    .line 68
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 251
    invoke-super {p0, p1, p2}, Ljnl;->onMeasure(II)V

    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    .line 254
    if-lez v4, :cond_2

    move v2, v1

    move v3, v1

    .line 257
    :goto_0
    if-ge v2, v4, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;

    .line 259
    invoke-virtual {v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->b()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 257
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 261
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->s:I

    if-eq v0, v3, :cond_2

    .line 262
    :goto_1
    if-ge v1, v4, :cond_1

    .line 263
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;

    .line 264
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursRowView;->b(I)V

    .line 262
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 266
    :cond_1
    iput v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->s:I

    .line 267
    invoke-super {p0, p1, p2}, Ljnl;->onMeasure(II)V

    .line 270
    :cond_2
    return-void
.end method
