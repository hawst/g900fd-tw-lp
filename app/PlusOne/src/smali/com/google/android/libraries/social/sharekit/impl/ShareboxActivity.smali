.class public Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;
.super Lloa;
.source "PG"

# interfaces
.implements Lhmm;
.implements Lhmq;
.implements Lhob;
.implements Ljby;
.implements Lkjg;
.implements Lkjk;
.implements Lkkh;
.implements Lklc;
.implements Lkmw;
.implements Llgs;
.implements Llha;
.implements Llks;


# instance fields
.field private A:Z

.field private final B:Lkmu;

.field private final C:Lkmy;

.field private D:Landroid/widget/ImageButton;

.field private E:Landroid/widget/ImageButton;

.field private F:Landroid/widget/ImageButton;

.field private G:Landroid/widget/ImageButton;

.field private H:Landroid/widget/LinearLayout;

.field private I:Landroid/widget/TextView;

.field private J:Landroid/widget/TextView;

.field private K:Landroid/widget/ScrollView;

.field private L:Landroid/view/View;

.field private M:Landroid/view/ViewGroup;

.field private N:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

.field private O:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

.field private P:Lkll;

.field private Q:Ljava/lang/Boolean;

.field private R:Z

.field private S:Lhgw;

.field private T:Z

.field private U:Z

.field private V:I

.field private W:Landroid/view/ViewGroup;

.field private X:Landroid/view/ViewGroup;

.field private final Y:Lklb;

.field private final Z:Lkky;

.field private aa:Z

.field private ab:I

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:F

.field private ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

.field private ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

.field private ai:I

.field private aj:I

.field private final ak:Lkjm;

.field private final al:Landroid/view/View$OnClickListener;

.field public final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lhxc;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lhee;

.field public g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

.field public final h:Lhoc;

.field public i:Lhgw;

.field public j:Z

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:Landroid/view/View;

.field public p:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field public q:Landroid/widget/TextView;

.field public r:Landroid/view/View;

.field public final s:Ljbx;

.field public final t:Lkki;

.field public final u:Lkkr;

.field public v:[B

.field public w:Lkmv;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 117
    invoke-direct {p0}, Lloa;-><init>()V

    .line 166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->e:Ljava/util/HashMap;

    .line 167
    new-instance v0, Lkmu;

    invoke-direct {v0, p0}, Lkmu;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->B:Lkmu;

    .line 169
    new-instance v0, Lhet;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhet;-><init>(Landroid/app/Activity;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x:Llnh;

    .line 170
    invoke-virtual {v0, v1}, Lhet;->a(Llnh;)Lhet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    .line 173
    new-instance v0, Lhmf;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->y:Llqc;

    invoke-direct {v0, v1}, Lhmf;-><init>(Llqr;)V

    .line 176
    new-instance v0, Lkmy;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkmy;-><init>(Landroid/app/Activity;Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    .line 201
    new-instance v0, Lhoc;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Landroid/app/Activity;Llqr;)V

    .line 202
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->h:Lhoc;

    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->R:Z

    .line 215
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->V:I

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->m:Z

    .line 225
    new-instance v0, Ljbx;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Ljbx;-><init>(Landroid/app/Activity;Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    .line 226
    new-instance v0, Lklb;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->y:Llqc;

    invoke-direct {v0, v1}, Lklb;-><init>(Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y:Lklb;

    .line 227
    new-instance v0, Lkky;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->y:Llqc;

    invoke-direct {v0, v1}, Lkky;-><init>(Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Z:Lkky;

    .line 228
    new-instance v0, Lkki;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkki;-><init>(Landroid/app/Activity;Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 230
    new-instance v0, Lkkr;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->y:Llqc;

    invoke-direct {v0, p0, v1}, Lkkr;-><init>(Landroid/app/Activity;Llqr;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->u:Lkkr;

    .line 248
    new-instance v0, Lkms;

    invoke-direct {v0, p0}, Lkms;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ak:Lkjm;

    .line 344
    new-instance v0, Lkmh;

    invoke-direct {v0, p0}, Lkmh;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->al:Landroid/view/View$OnClickListener;

    .line 2002
    return-void
.end method

.method private P()V
    .locals 8

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 550
    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ab:I

    if-ne v0, v1, :cond_3

    move v0, v1

    .line 551
    :goto_0
    iget v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ab:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    move v3, v1

    .line 553
    :goto_1
    iget-object v4, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->D:Landroid/widget/ImageButton;

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 554
    iget-object v4, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->E:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 556
    if-nez v0, :cond_0

    if-eqz v3, :cond_5

    :cond_0
    move v6, v1

    .line 557
    :goto_2
    if-eqz v6, :cond_6

    sget-object v4, Llkt;->c:Llkt;

    .line 558
    :goto_3
    iget-object v7, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v7}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->h()Llkt;

    move-result-object v7

    if-eq v7, v4, :cond_1

    .line 559
    iget-object v7, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v7, v4, v1}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llkt;Z)V

    .line 562
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v1

    const v4, 0x7f10057c

    invoke-virtual {v1, v4}, Lae;->a(I)Lu;

    move-result-object v1

    .line 563
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v4

    const v7, 0x7f10057d

    invoke-virtual {v4, v7}, Lae;->a(I)Lu;

    move-result-object v4

    .line 567
    if-eqz v6, :cond_a

    .line 570
    invoke-virtual {v1}, Lu;->x()Landroid/view/View;

    move-result-object v6

    if-eqz v0, :cond_7

    move v1, v2

    :goto_4
    invoke-virtual {v6, v1}, Landroid/view/View;->setVisibility(I)V

    .line 571
    invoke-virtual {v4}, Lu;->x()Landroid/view/View;

    move-result-object v1

    if-eqz v3, :cond_8

    :goto_5
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 573
    if-eqz v0, :cond_9

    .line 574
    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ac:I

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(I)V

    .line 595
    :cond_2
    :goto_6
    return-void

    :cond_3
    move v0, v2

    .line 550
    goto :goto_0

    :cond_4
    move v3, v2

    .line 551
    goto :goto_1

    :cond_5
    move v6, v2

    .line 556
    goto :goto_2

    .line 557
    :cond_6
    sget-object v4, Llkt;->b:Llkt;

    goto :goto_3

    :cond_7
    move v1, v5

    .line 570
    goto :goto_4

    :cond_8
    move v2, v5

    .line 571
    goto :goto_5

    .line 575
    :cond_9
    if-eqz v3, :cond_2

    .line 576
    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ad:I

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(I)V

    goto :goto_6

    .line 582
    :cond_a
    invoke-virtual {v1}, Lu;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 583
    invoke-virtual {v4}, Lu;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 587
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    new-instance v1, Lkmk;

    invoke-direct {v1, p0}, Lkmk;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->post(Ljava/lang/Runnable;)Z

    goto :goto_6
.end method

.method private Q()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 668
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v1}, Lkki;->w()I

    move-result v1

    .line 669
    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    if-eq v1, v0, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private R()V
    .locals 3

    .prologue
    .line 739
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->J:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 740
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a02cb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 739
    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 742
    return-void

    .line 740
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    .line 741
    invoke-virtual {v0}, Ljbx;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private S()V
    .locals 5

    .prologue
    .line 1100
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0}, Lkmy;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1101
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0, v1}, Lkmy;->a(I)Lkji;

    move-result-object v2

    .line 1102
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0401e6

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 1104
    invoke-interface {v2}, Lkji;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1105
    invoke-interface {v2}, Lkji;->g()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1106
    const v3, 0x7f10004a

    invoke-virtual {v0, v3, v2}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    .line 1108
    new-instance v3, Lkmi;

    invoke-direct {v3, p0, v2}, Lkmi;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;Lkji;)V

    .line 1116
    invoke-interface {v2, v0}, Lkji;->a(Landroid/widget/ImageButton;)Z

    move-result v2

    .line 1117
    if-eqz v2, :cond_0

    new-instance v2, Lhmi;

    invoke-direct {v2, v3}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1120
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->H:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1100
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v2, v3

    .line 1117
    goto :goto_1

    .line 1122
    :cond_1
    return-void
.end method

.method private T()V
    .locals 7

    .prologue
    const v5, 0x7f1005a1

    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1267
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    if-eqz v0, :cond_2

    .line 1268
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->A:Z

    if-eqz v0, :cond_3

    .line 1269
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->c(Z)V

    .line 1270
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    :cond_0
    move-object v3, v0

    move v0, v2

    .line 1273
    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->b(Z)V

    .line 1276
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iget-boolean v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Z)V

    .line 1278
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->A:Z

    if-eqz v0, :cond_4

    .line 1280
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    move v0, v4

    .line 1282
    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1287
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->W:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    if-eqz v0, :cond_6

    .line 1288
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->w:Lkmv;

    iget-object v5, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    invoke-virtual {v0, v5}, Lkmv;->c(I)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    .line 1287
    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1292
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    move-result-object v0

    .line 1294
    invoke-virtual {v0}, Lhgw;->h()I

    move-result v3

    if-ne v3, v1, :cond_7

    .line 1295
    invoke-virtual {v0, v2}, Lhgw;->b(I)Lhxc;

    move-result-object v0

    invoke-virtual {v0}, Lhxc;->c()I

    move-result v0

    const/16 v3, 0x9

    if-ne v0, v3, :cond_7

    move v0, v1

    .line 1297
    :goto_3
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->X:Landroid/view/ViewGroup;

    iget-boolean v5, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->A:Z

    if-eqz v5, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->F()Z

    move-result v0

    if-nez v0, :cond_1

    move v4, v2

    :cond_1
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1300
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    .line 1301
    iget-boolean v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    if-eqz v3, :cond_2

    .line 1302
    const-string v3, "domain_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1303
    const v0, 0x7f100571

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1304
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a02b5

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v3, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1303
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1305
    const v0, 0x7f100572

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1306
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a02b6

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-virtual {v4, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1305
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1309
    :cond_2
    return-void

    .line 1273
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iget-boolean v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    if-nez v3, :cond_0

    move-object v3, v0

    move v0, v1

    goto/16 :goto_0

    .line 1282
    :cond_4
    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    if-eqz v3, :cond_5

    move-object v3, v0

    move v0, v4

    goto/16 :goto_1

    :cond_5
    move-object v3, v0

    move v0, v2

    goto/16 :goto_1

    :cond_6
    move v0, v4

    .line 1288
    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 1295
    goto :goto_3
.end method

.method private U()Z
    .locals 1

    .prologue
    .line 1362
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 1363
    invoke-virtual {v0}, Lkki;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private V()V
    .locals 4

    .prologue
    .line 1870
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    invoke-virtual {v0}, Lkll;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1883
    :cond_0
    :goto_0
    return-void

    .line 1874
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 1875
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 1877
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V()Ljava/lang/String;

    move-result-object v1

    .line 1879
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    iget-object v2, v2, Ljbx;->a:Ljava/util/ArrayList;

    .line 1881
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    invoke-virtual {v3, v0, v1, v2}, Lkll;->a(Lhej;Ljava/lang/String;Ljava/util/List;)V

    .line 1882
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    invoke-virtual {v0}, Lkll;->a()V

    goto :goto_0
.end method

.method private W()V
    .locals 1

    .prologue
    .line 1886
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    if-eqz v0, :cond_0

    .line 1887
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    invoke-virtual {v0}, Lkll;->b()V

    .line 1889
    :cond_0
    return-void
.end method

.method private X()F
    .locals 2

    .prologue
    .line 1906
    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->af:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 1908
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->af:F

    .line 1910
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->af:F

    return v0
.end method

.method private Y()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1921
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Q:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 1923
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 1924
    :goto_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_4

    move v3, v1

    .line 1925
    :goto_1
    if-eqz v3, :cond_6

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_5

    :cond_0
    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    :cond_1
    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Q:Ljava/lang/Boolean;

    .line 1927
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Q:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_3
    move v0, v2

    .line 1923
    goto :goto_0

    :cond_4
    move v3, v2

    .line 1924
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1925
    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_3
.end method

.method private a(ILhmn;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I",
            "Lhmn;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 751
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 752
    new-instance v1, Lhmk;

    invoke-direct {v1, p2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 753
    new-instance v1, Lhmi;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->al:Landroid/view/View$OnClickListener;

    invoke-direct {v1, v2}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 754
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Llnh;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x:Llnh;

    return-object v0
.end method

.method private a(I)V
    .locals 0

    .prologue
    .line 542
    iput p1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ab:I

    .line 543
    if-eqz p1, :cond_0

    .line 544
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->J()V

    .line 546
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P()V

    .line 547
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;I)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;Landroid/widget/GridView;I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 117
    invoke-virtual {p1}, Landroid/widget/GridView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/GridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    if-ne p2, v2, :cond_1

    iget v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ac:I

    if-eq v1, v0, :cond_0

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ac:I

    iget v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ab:I

    if-ne v1, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p2, v3, :cond_0

    iget v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ad:I

    if-eq v1, v0, :cond_0

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ad:I

    iget v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ab:I

    if-ne v1, v3, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(I)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lhgw;ZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1248
    iput-boolean p3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->R:Z

    .line 1249
    iput-boolean p2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    .line 1250
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    if-eqz v0, :cond_2

    .line 1251
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Lhgw;)V

    .line 1252
    if-eqz p1, :cond_1

    .line 1253
    invoke-virtual {p1}, Lhgw;->i()I

    move-result v0

    if-lez v0, :cond_0

    .line 1254
    invoke-virtual {p1, v1}, Lhgw;->c(I)Lkxr;

    move-result-object v0

    invoke-virtual {v0}, Lkxr;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    .line 1256
    :cond_0
    invoke-virtual {p1}, Lhgw;->j()I

    move-result v0

    if-lez v0, :cond_1

    .line 1257
    invoke-virtual {p1, v1}, Lhgw;->d(I)Lhxs;

    move-result-object v0

    invoke-virtual {v0}, Lhxs;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    .line 1263
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->T()V

    .line 1264
    return-void

    .line 1261
    :cond_2
    iput-object p1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->S:Lhgw;

    goto :goto_0
.end method

.method private a(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 426
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aj:I

    .line 427
    invoke-static {p1}, Lkjp;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 429
    const/4 v0, 0x0

    .line 430
    if-nez v1, :cond_0

    .line 431
    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 432
    :goto_0
    const/4 v1, 0x1

    move v4, v1

    move-object v1, v0

    move v0, v4

    .line 434
    :cond_0
    new-instance v2, Llgz;

    invoke-direct {v2}, Llgz;-><init>()V

    .line 435
    invoke-virtual {v2, v1}, Llgz;->a(Ljava/lang/String;)Llgz;

    move-result-object v1

    .line 436
    invoke-virtual {v1, p2}, Llgz;->e(Ljava/lang/String;)Llgz;

    move-result-object v1

    .line 437
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0271

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Llgz;->c(Ljava/lang/String;)Llgz;

    move-result-object v1

    const v2, 0x7f0a0597

    .line 438
    invoke-virtual {v1, v2}, Llgz;->a(I)Llgz;

    move-result-object v1

    .line 440
    if-eqz v0, :cond_1

    .line 441
    invoke-virtual {v1}, Llgz;->a()Llgz;

    .line 444
    :cond_1
    invoke-virtual {v1}, Llgz;->b()Llgw;

    move-result-object v0

    .line 445
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v1

    const-string v2, "SharekitLinkDialog"

    invoke-virtual {v0, v1, v2}, Llgw;->a(Lae;Ljava/lang/String;)V

    .line 446
    return-void

    .line 431
    :cond_2
    const-string v0, "http://"

    goto :goto_0
.end method

.method private a(Llkt;)V
    .locals 1

    .prologue
    .line 1892
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Llkt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1893
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->V()V

    .line 1897
    :goto_0
    return-void

    .line 1895
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->W()V

    goto :goto_0
.end method

.method private b(I)V
    .locals 3

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    .line 677
    if-lez p1, :cond_0

    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ae:I

    if-eq p1, v0, :cond_0

    .line 678
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y()Z

    move-result v0

    if-nez v0, :cond_1

    .line 698
    :cond_0
    :goto_0
    return-void

    .line 681
    :cond_1
    iput p1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ae:I

    .line 682
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->X()F

    move-result v0

    .line 685
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->i()I

    move-result v1

    .line 687
    int-to-float v1, v1

    mul-float/2addr v1, v0

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 689
    if-ge p1, v1, :cond_2

    .line 692
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v2, Llkt;->d:Llkt;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llkt;F)V

    goto :goto_0

    .line 696
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v1, Llkt;->d:Llkt;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llkt;F)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V
    .locals 4

    .prologue
    const v3, 0x7f0a02cd

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aj:I

    new-instance v1, Llgz;

    invoke-direct {v1}, Llgz;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Llgz;->b(Ljava/lang/String;)Llgz;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v2}, Ljbx;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Llgz;->a(Ljava/lang/String;)Llgz;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Llgz;->c(Ljava/lang/String;)Llgz;

    move-result-object v1

    if-eqz v0, :cond_0

    const v0, 0x7f0a0597

    :goto_0
    invoke-virtual {v1, v0}, Llgz;->a(I)Llgz;

    move-result-object v0

    invoke-virtual {v0}, Llgz;->b()Llgw;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v1

    const-string v2, "SharekitAlbumName"

    invoke-virtual {v0, v1, v2}, Llgw;->a(Lae;Ljava/lang/String;)V

    return-void

    :cond_0
    const v0, 0x7f0a0598

    goto :goto_0
.end method

.method private b(Llkt;)Z
    .locals 3

    .prologue
    .line 1900
    sget-object v0, Llkt;->d:Llkt;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v1, Llkt;->d:Llkt;

    .line 1901
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->b(Llkt;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v2, Llkt;->c:Llkt;

    .line 1902
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->b(Llkt;)I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 745
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 746
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->al:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 747
    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->W:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->w:Lkmv;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lkmv;->d(I)V

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->W:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->X:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->X:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->w:Lkmv;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lkmv;->f(I)V

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->X:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public static synthetic e(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V
    .locals 4

    .prologue
    const v3, 0x7f1006f9

    .line 117
    const v0, 0x7f10059d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lyo;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lyo;-><init>(Landroid/content/Context;Landroid/view/View;)V

    const v0, 0x7f120013

    invoke-virtual {v1, v0}, Lyo;->a(I)V

    invoke-virtual {v1}, Lyo;->a()Landroid/view/Menu;

    move-result-object v0

    const v2, 0x7f1006f7

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Z:Lkky;

    invoke-virtual {v2}, Lkky;->c()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    invoke-virtual {v1}, Lyo;->a()Landroid/view/Menu;

    move-result-object v0

    const v2, 0x7f1006f8

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Z:Lkky;

    invoke-virtual {v2}, Lkky;->a()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    invoke-virtual {v1}, Lyo;->a()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Z:Lkky;

    invoke-virtual {v2}, Lkky;->e()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    invoke-virtual {v1}, Lyo;->a()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    new-instance v0, Lkmj;

    invoke-direct {v0, p0}, Lkmj;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    invoke-virtual {v1, v0}, Lyo;->a(Lyp;)V

    invoke-virtual {v1}, Lyo;->c()V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lkky;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Z:Lkky;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lcom/google/android/libraries/social/ui/views/ObservableGridView;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->N:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lcom/google/android/libraries/social/ui/views/ObservableGridView;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->O:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->K:Landroid/widget/ScrollView;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->F:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lkll;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lkmy;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    return-object v0
.end method

.method public static synthetic m(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->w()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->T:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected B()V
    .locals 1

    .prologue
    .line 1139
    sget-object v0, Lhmv;->dv:Lhmv;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lhmv;)V

    .line 1140
    return-void
.end method

.method public C()V
    .locals 4

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v1

    .line 1174
    const v0, 0x7f1005a0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1175
    const-string v2, "display_name"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1176
    const v0, 0x7f10059e

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 1177
    const-string v2, "gaia_id"

    .line 1178
    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "profile_photo_url"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1177
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->T()V

    .line 1181
    return-void
.end method

.method public D()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1225
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1226
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->S:Lhgw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->S:Lhgw;

    invoke-virtual {v0, v2}, Lhgw;->b(I)Lhxc;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1232
    :cond_0
    :goto_0
    return-void

    .line 1226
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->g()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0596

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v0, v1, v6}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v1

    const-string v2, "acl_modification_disabled"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 1229
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->U:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v3}, Lkki;->j()Lkzz;

    move-result-object v3

    if-eqz v3, :cond_c

    invoke-virtual {v3}, Lkzz;->e()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    move v3, v2

    :goto_3
    iget-boolean v4, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->z:Z

    if-eqz v4, :cond_9

    new-instance v4, Lhhr;

    invoke-direct {v4, p0}, Lhhr;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lhgw;->i()I

    move-result v5

    if-gtz v5, :cond_3

    invoke-virtual {v0}, Lhgw;->j()I

    move-result v5

    if-lez v5, :cond_7

    :cond_3
    invoke-static {v0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->a(Lhgw;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Lhhr;->a(Ljava/util/ArrayList;Ljava/lang/String;)Lhhr;

    :cond_4
    :goto_4
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    invoke-virtual {v4, v0}, Lhhr;->a(Z)Lhhr;

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-virtual {v4, v0}, Lhhr;->a(I)Lhhr;

    if-nez v3, :cond_8

    move v0, v1

    :goto_5
    invoke-virtual {v4, v0}, Lhhr;->b(Z)Lhhr;

    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ai:I

    invoke-virtual {v4, v0}, Lhhr;->b(I)V

    invoke-virtual {v4}, Lhhr;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1230
    :goto_6
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1231
    const v0, 0x7f050023

    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->overridePendingTransition(II)V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 1229
    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Lhgw;->p()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v0}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->a(Lhgw;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lhhr;->a(Ljava/util/ArrayList;Ljava/lang/String;)Lhhr;

    goto :goto_4

    :cond_8
    move v0, v2

    goto :goto_5

    :cond_9
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/google/android/libraries/social/acl/AclPickerActivity;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->R:Z

    if-nez v6, :cond_a

    const-string v6, "extra_acl"

    invoke-virtual {v4, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_a
    const-string v5, "restrict_to_domain"

    iget-boolean v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v5, "extra_account_id"

    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v6}, Lhee;->d()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v5, "circle_usage_type"

    iget v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ai:I

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v5, "domain_name"

    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v6}, Lhee;->g()Lhej;

    move-result-object v6

    const-string v7, "domain_name"

    invoke-interface {v6, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "is_dasher_account"

    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v6}, Lhee;->g()Lhej;

    move-result-object v6

    const-string v7, "is_dasher_account"

    invoke-interface {v6, v7}, Lhej;->c(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v5, "enable_domain_restrict_toggle"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "hide_domain_restrict_toggle"

    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->V:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_b

    const-string v0, "category_display_mode"

    iget v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->V:I

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_b
    move-object v0, v4

    goto/16 :goto_6

    :cond_c
    move v3, v0

    move v0, v2

    goto/16 :goto_3
.end method

.method public E()Lhgw;
    .locals 1

    .prologue
    .line 1312
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    move-result-object v0

    return-object v0
.end method

.method protected F()Z
    .locals 2

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->w:Lkmv;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lkmv;->e(I)Z

    move-result v0

    return v0
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 1161
    sget-object v0, Lhmw;->Y:Lhmw;

    return-object v0
.end method

.method public G()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1367
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    .line 1383
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v1

    .line 1367
    goto :goto_0

    .line 1371
    :cond_2
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1375
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0}, Lkmy;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1376
    :goto_2
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v3}, Lkmy;->c()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 1377
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v3, v0}, Lkmy;->a(I)Lkji;

    move-result-object v3

    invoke-interface {v3}, Lkji;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1376
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v1, v2

    .line 1383
    goto :goto_1
.end method

.method public H()V
    .locals 2

    .prologue
    .line 1433
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->J()V

    .line 1435
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1445
    :goto_0
    return-void

    .line 1439
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->w()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 1440
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->n()V

    goto :goto_0

    .line 1442
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->r()V

    .line 1443
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->o()V

    goto :goto_0
.end method

.method public I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1579
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->U()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public J()V
    .locals 1

    .prologue
    .line 1587
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->d()V

    .line 1588
    return-void
.end method

.method protected K()Lklo;
    .locals 1

    .prologue
    .line 1595
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->n()Lklo;

    move-result-object v0

    return-object v0
.end method

.method protected L()Z
    .locals 2

    .prologue
    .line 1599
    const-string v0, "com.google.android.apps.plus.GOOGLE_BIRTHDAY_POST"

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public M()Lhgw;
    .locals 1

    .prologue
    .line 1942
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h()Lhgw;

    move-result-object v0

    return-object v0
.end method

.method public N()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1951
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 1952
    invoke-virtual {v0}, Lkki;->j()Lkzz;

    move-result-object v0

    .line 1953
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    move-result-object v1

    .line 1955
    if-eqz v0, :cond_1

    .line 1956
    invoke-virtual {v0}, Lkzz;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    .line 1981
    :cond_0
    :goto_0
    return-void

    .line 1957
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lhgw;->i()I

    move-result v0

    if-lez v0, :cond_2

    .line 1959
    invoke-virtual {v1, v2}, Lhgw;->c(I)Lkxr;

    move-result-object v0

    invoke-virtual {v0}, Lkxr;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    goto :goto_0

    .line 1960
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lhgw;->j()I

    move-result v0

    if-lez v0, :cond_3

    .line 1963
    invoke-virtual {v1, v2}, Lhgw;->d(I)Lhxs;

    move-result-object v0

    invoke-virtual {v0}, Lhxs;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    goto :goto_0

    .line 1965
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    .line 1966
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_dasher_account"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1967
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_default_restricted"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1968
    iput-boolean v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    .line 1973
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "restrict_to_domain"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 1974
    invoke-virtual {v0}, Lkki;->w()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 1975
    iput-boolean v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->U:Z

    .line 1978
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "restrict_to_domain"

    iget-boolean v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    goto :goto_0

    .line 1970
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->w:Lkmv;

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    .line 1971
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 1970
    invoke-virtual {v0, v1}, Lkmv;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    goto :goto_1
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1554
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1558
    return-void
.end method

.method public a(Landroid/app/Activity;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 1729
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 1731
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 1733
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v1}, Ljbx;->h()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1734
    new-instance v1, Lhmr;

    invoke-direct {v1, p1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->dx:Lhmv;

    .line 1736
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1734
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1739
    :cond_0
    if-eqz p2, :cond_1

    .line 1740
    new-instance v1, Lhmr;

    invoke-direct {v1, p1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->dF:Lhmv;

    .line 1742
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1740
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1745
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v1}, Lkki;->h()Llae;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1746
    new-instance v1, Lhmr;

    invoke-direct {v1, p1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->dz:Lhmv;

    .line 1748
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1746
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1751
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->K()Lklo;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1752
    new-instance v1, Lhmr;

    invoke-direct {v1, p1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->dw:Lhmv;

    .line 1754
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1752
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1757
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->u:Lkkr;

    invoke-virtual {v1}, Lkkr;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1758
    new-instance v1, Lhmr;

    invoke-direct {v1, p1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->dO:Lhmv;

    .line 1760
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1758
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1763
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v1}, Lkki;->l()Lklb;

    move-result-object v1

    invoke-virtual {v1}, Lklb;->b()Lkkz;

    move-result-object v1

    .line 1764
    if-eqz v1, :cond_5

    .line 1766
    if-eqz p2, :cond_6

    invoke-virtual {v1}, Lkkz;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1767
    sget-object v1, Lhmv;->dN:Lhmv;

    .line 1771
    :goto_0
    new-instance v3, Lhmr;

    invoke-direct {v3, p1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 1773
    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1771
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1774
    new-instance v1, Lhmr;

    invoke-direct {v1, p1, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->dB:Lhmv;

    .line 1776
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1774
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1778
    :cond_5
    return-void

    .line 1769
    :cond_6
    sget-object v1, Lhmv;->dM:Lhmv;

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 724
    invoke-super {p0, p1}, Lloa;->a(Landroid/os/Bundle;)V

    .line 725
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x:Llnh;

    const-class v1, Ljbx;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lklb;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y:Lklb;

    .line 726
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkky;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Z:Lkky;

    .line 727
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkki;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 728
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkkr;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->u:Lkkr;

    .line 729
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhoc;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->h:Lhoc;

    .line 730
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkjg;

    .line 731
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkjj;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    .line 732
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lkjm;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ak:Lkjm;

    .line 733
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lhmm;

    .line 734
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 735
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x:Llnh;

    const-class v1, Lkmv;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkmv;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->w:Lkmv;

    .line 736
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1518
    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1519
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0}, Lkmy;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1520
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkmy;->a(Lkji;)V

    .line 1521
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c()V

    .line 1526
    :cond_0
    :goto_0
    return-void

    .line 1524
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->finish()V

    goto :goto_0
.end method

.method public a(Lhgw;)V
    .locals 2

    .prologue
    .line 1235
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lhgw;ZZ)V

    .line 1236
    return-void
.end method

.method protected a(Lhgw;Z)V
    .locals 1

    .prologue
    .line 1243
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lhgw;ZZ)V

    .line 1244
    return-void
.end method

.method public final a(Lhmv;)V
    .locals 3

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1151
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    invoke-direct {v2, p0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 1154
    invoke-virtual {v2, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1152
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1157
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 521
    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aj:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 522
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    if-ne p2, v2, :cond_1

    :goto_0
    invoke-virtual {v0, p1, p0}, Ljbx;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 524
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->R()V

    .line 530
    :cond_0
    :goto_1
    return-void

    .line 522
    :cond_1
    const/4 p1, 0x0

    goto :goto_0

    .line 525
    :cond_2
    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aj:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 526
    if-ne p2, v2, :cond_0

    .line 527
    invoke-static {p1}, Lkjp;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0272

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lhgw;)V
    .locals 4

    .prologue
    .line 1781
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1782
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 1783
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1784
    new-instance v2, Lhmr;

    invoke-direct {v2, p0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->dK:Lhmv;

    .line 1786
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 1784
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 1788
    :cond_0
    invoke-virtual {p2}, Lhgw;->h()I

    move-result v2

    if-lez v2, :cond_1

    .line 1789
    new-instance v2, Lhmr;

    invoke-direct {v2, p0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->dD:Lhmv;

    .line 1791
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 1789
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 1793
    :cond_1
    invoke-virtual {p2}, Lhgw;->g()I

    move-result v2

    if-lez v2, :cond_2

    .line 1794
    new-instance v2, Lhmr;

    invoke-direct {v2, p0, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->dC:Lhmv;

    .line 1796
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1794
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1799
    :cond_2
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 1623
    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1624
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    invoke-virtual {p2}, Lhoz;->b()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v0, :cond_3

    const-string v0, "INVALID_ACL_EXPANSION"

    invoke-static {v1, v0}, Lkgf;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    move-result-object v0

    invoke-virtual {v0}, Lhgw;->i()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const v1, 0x7f0a02c8

    if-eqz v0, :cond_2

    const v0, 0x7f0a02c7

    :goto_1
    invoke-virtual {p3, v1, v0}, Lhos;->a(II)V

    .line 1626
    :cond_0
    :goto_2
    return-void

    .line 1624
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const v0, 0x7f0a02c6

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->p()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0}, Lkmy;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkmy;->a(Lkji;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s()V

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->finish()V

    goto :goto_2
.end method

.method public a(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lizr;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->z()V

    .line 403
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->d()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 404
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->v()V

    .line 407
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizr;

    .line 408
    invoke-interface {v0}, Lizr;->f()Lizu;

    move-result-object v0

    .line 409
    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v2

    sget-object v3, Ljac;->b:Ljac;

    if-ne v2, v3, :cond_1

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 410
    new-instance v2, Lkls;

    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    .line 411
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v2, p0, v3, v0}, Lkls;-><init>(Landroid/content/Context;ILizu;)V

    .line 410
    invoke-static {p0, v2}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    goto :goto_0

    .line 414
    :cond_2
    return-void
.end method

.method protected a(Lklo;)V
    .locals 1

    .prologue
    .line 1591
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v0, p1}, Lkki;->a(Lklo;)V

    .line 1592
    return-void
.end method

.method public a(Lkmz;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1204
    if-nez p1, :cond_0

    .line 1205
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lklo;)V

    .line 1214
    :goto_0
    return-void

    .line 1212
    :cond_0
    new-instance v3, Lklo;

    invoke-virtual {p1}, Lkmz;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p1, Lkmz;->a:Locz;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lkmz;->a:Locz;

    iget-object v0, v0, Locz;->c:[Loya;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lkmz;->a:Locz;

    iget-object v0, v0, Locz;->c:[Loya;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    new-instance v1, Loyp;

    invoke-direct {v1}, Loyp;-><init>()V

    iput-object v1, v0, Loya;->d:Loyp;

    iget-object v1, v0, Loya;->d:Loyp;

    iput-object v2, v1, Loyp;->b:Ljava/lang/String;

    :cond_1
    invoke-direct {v3, v4, v0}, Lklo;-><init>(Ljava/lang/String;Loya;)V

    .line 1213
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lklo;)V

    goto :goto_0

    .line 1212
    :cond_2
    iget-object v0, p1, Lkmz;->b:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    new-instance v1, Loya;

    invoke-direct {v1}, Loya;-><init>()V

    new-instance v0, Lpdd;

    invoke-direct {v0}, Lpdd;-><init>()V

    iget-object v5, p1, Lkmz;->b:Landroid/os/Bundle;

    const-string v6, "title"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lpdd;->d:Ljava/lang/String;

    iget-object v5, p1, Lkmz;->b:Landroid/os/Bundle;

    const-string v6, "description"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lpdd;->e:Ljava/lang/String;

    iget-object v5, p1, Lkmz;->b:Landroid/os/Bundle;

    const-string v6, "thumbnailUrl"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lpdd;->c:Ljava/lang/String;

    sget-object v5, Lpdd;->a:Loxr;

    invoke-virtual {v1, v5, v0}, Loya;->a(Loxr;Ljava/lang/Object;)V

    sget-object v0, Lkmz;->d:[I

    iput-object v0, v1, Loya;->b:[I

    iget-object v0, p1, Lkmz;->c:Lkng;

    if-eqz v0, :cond_3

    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    new-instance v5, Loyj;

    invoke-direct {v5}, Loyj;-><init>()V

    iput-object v1, v5, Loyj;->b:Loya;

    new-instance v1, Loyo;

    invoke-direct {v1}, Loyo;-><init>()V

    iput-object v1, v5, Loyj;->c:Loyo;

    iget-object v1, v5, Loyj;->c:Loyo;

    const/high16 v6, -0x80000000

    iput v6, v1, Loyo;->c:I

    iget-object v1, v5, Loyj;->c:Loyo;

    new-instance v6, Loyp;

    invoke-direct {v6}, Loyp;-><init>()V

    iput-object v6, v1, Loyo;->a:Loyp;

    iget-object v1, v5, Loyj;->c:Loyo;

    iget-object v1, v1, Loyo;->a:Loyp;

    iget-object v6, p1, Lkmz;->c:Lkng;

    iget-object v6, v6, Lkng;->c:Ljava/lang/String;

    iput-object v6, v1, Loyp;->b:Ljava/lang/String;

    iget-object v1, v5, Loyj;->c:Loyo;

    iget-object v1, v1, Loyo;->a:Loyp;

    iget-object v6, p1, Lkmz;->c:Lkng;

    iget-object v6, v6, Lkng;->b:Ljava/lang/String;

    iput-object v6, v1, Loyp;->d:Ljava/lang/String;

    sget-object v1, Lkmz;->e:[I

    iput-object v1, v0, Loya;->b:[I

    sget-object v1, Loyj;->a:Loxr;

    invoke-virtual {v0, v1, v5}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_3
    move-object v0, v1

    goto/16 :goto_1

    :cond_4
    move-object v0, v2

    goto/16 :goto_1
.end method

.method public a(Llae;)V
    .locals 1

    .prologue
    .line 1608
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v0, p1}, Lkki;->a(Llae;)V

    .line 1609
    return-void
.end method

.method public a(Llkt;F)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1825
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Llkt;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1826
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->L:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Llsj;->a(Landroid/view/View;F)V

    .line 1827
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->V()V

    .line 1844
    :cond_0
    :goto_0
    return-void

    .line 1831
    :cond_1
    invoke-virtual {p1}, Llkt;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    move v2, v0

    .line 1832
    :goto_1
    if-eqz v2, :cond_6

    move v0, p2

    .line 1833
    :goto_2
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->L:Landroid/view/View;

    invoke-static {v3, v0}, Llsj;->a(Landroid/view/View;F)V

    .line 1836
    if-eqz v2, :cond_7

    const v0, 0x3f666666    # 0.9f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_7

    .line 1837
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->V()V

    .line 1843
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v2, Llkt;->b:Llkt;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->b(Llkt;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v3, Llkt;->c:Llkt;

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->b(Llkt;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->e()Z

    move-result v3

    if-eqz v3, :cond_9

    :goto_4
    const-string v0, "ShareboxActivity"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x24

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "setShareboxBottomMargin: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-eq v2, v1, :cond_0

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_5
    move v2, v1

    .line 1831
    goto :goto_1

    .line 1832
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 1838
    :cond_7
    if-eqz v2, :cond_8

    const v0, 0x3f4ccccd    # 0.8f

    cmpg-float v0, p2, v0

    if-gez v0, :cond_3

    .line 1839
    :cond_8
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->W()V

    goto :goto_3

    .line 1843
    :cond_9
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y()Z

    move-result v1

    if-nez v1, :cond_a

    move v1, v0

    goto :goto_4

    :cond_a
    invoke-virtual {p1}, Llkt;->a()Z

    move-result v1

    if-eqz v1, :cond_b

    move v1, v2

    goto :goto_4

    :cond_b
    sub-int v1, v2, v0

    int-to-float v1, v1

    mul-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_4
.end method

.method public a(Llkt;Llkt;)V
    .locals 5

    .prologue
    .line 1808
    const-string v0, "ShareboxActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1809
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onExpandingStateChanged: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " --> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1812
    :cond_0
    invoke-virtual {p2}, Llkt;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1813
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(I)V

    .line 1816
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Llkt;)V

    .line 1818
    invoke-virtual {p2}, Llkt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ab:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1819
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljbx;->a(I)V

    .line 1821
    :cond_2
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 1947
    new-instance v0, Lhmk;

    sget-object v1, Lonj;->h:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 418
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->R()V

    .line 419
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1166
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1546
    return-void
.end method

.method public b(Lhgw;)V
    .locals 2

    .prologue
    .line 1239
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lhgw;ZZ)V

    .line 1240
    return-void
.end method

.method protected b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1613
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 1614
    invoke-virtual {v0}, Lkki;->q()Lkjd;

    move-result-object v0

    .line 1615
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkjd;->a()Ljava/lang/String;

    move-result-object v0

    .line 1616
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "CreatePostTask"

    .line 1617
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ReshareTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1615
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1617
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c(Landroid/content/Intent;)Lhgw;
    .locals 1

    .prologue
    .line 1218
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Lhgw;)Lkjc;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1387
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 1388
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->e()Ljava/lang/String;

    move-result-object v4

    .line 1389
    new-instance v0, Lhmr;

    invoke-direct {v0, p0, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmw;->Y:Lhmw;

    .line 1390
    invoke-virtual {v0, v3}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v0

    sget-object v3, Lhmw;->z:Lhmw;

    .line 1391
    invoke-virtual {v0, v3}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v0

    .line 1392
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhmr;->a(Ljava/lang/Long;)Lhmr;

    move-result-object v5

    .line 1393
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    iget-object v3, v0, Ljbx;->a:Ljava/util/ArrayList;

    .line 1395
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->n()Lklo;

    move-result-object v6

    .line 1397
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v0}, Lkki;->l()Lklb;

    move-result-object v0

    .line 1398
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lklb;->b()Lkkz;

    move-result-object v0

    .line 1399
    :goto_0
    iget-object v7, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v7}, Ljbx;->b()Ljava/lang/String;

    move-result-object v7

    .line 1400
    iget-object v8, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v8}, Ljbx;->a()Ljava/lang/String;

    move-result-object v8

    .line 1402
    new-instance v9, Lkjc;

    invoke-direct {v9}, Lkjc;-><init>()V

    .line 1403
    invoke-virtual {v9, p0}, Lkjc;->a(Landroid/content/Context;)Lkjc;

    move-result-object v9

    .line 1404
    invoke-virtual {v9, v2}, Lkjc;->a(I)Lkjc;

    move-result-object v2

    .line 1405
    invoke-virtual {v2, v5}, Lkjc;->a(Lhmr;)Lkjc;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->u:Lkkr;

    .line 1406
    invoke-virtual {v5}, Lkkr;->b()Lkey;

    move-result-object v5

    invoke-virtual {v2, v5}, Lkjc;->a(Lkey;)Lkjc;

    move-result-object v5

    if-eqz v6, :cond_1

    .line 1407
    invoke-virtual {v6}, Lklo;->a()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v5, v2}, Lkjc;->a(Ljava/lang/String;)Lkjc;

    move-result-object v5

    if-eqz v6, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->u:Lkkr;

    .line 1409
    invoke-virtual {v2}, Lkkr;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lklo;->a(Ljava/lang/String;)Loya;

    move-result-object v2

    .line 1408
    :goto_2
    invoke-virtual {v5, v2}, Lkjc;->a(Loya;)Lkjc;

    move-result-object v2

    .line 1411
    invoke-virtual {v2, p1}, Lkjc;->a(Lhgw;)Lkjc;

    move-result-object v5

    iget-boolean v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->i:Lhgw;

    .line 1412
    :goto_3
    invoke-virtual {v5, v2}, Lkjc;->b(Lhgw;)Lkjc;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->k:Ljava/lang/String;

    .line 1413
    invoke-virtual {v2, v5}, Lkjc;->b(Ljava/lang/String;)Lkjc;

    move-result-object v2

    .line 1414
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->I()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lkjc;->d(Ljava/lang/String;)Lkjc;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    .line 1415
    invoke-virtual {v5}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->V()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lkjc;->e(Ljava/lang/String;)Lkjc;

    move-result-object v5

    if-nez v7, :cond_4

    if-nez v8, :cond_4

    move-object v2, v3

    .line 1416
    :goto_4
    invoke-virtual {v5, v2}, Lkjc;->a(Ljava/util/ArrayList;)Lkjc;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 1417
    invoke-virtual {v3}, Lkki;->h()Llae;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkjc;->a(Llae;)Lkjc;

    move-result-object v2

    .line 1418
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->L()Z

    move-result v3

    invoke-virtual {v2, v3}, Lkjc;->d(Z)Lkjc;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 1419
    invoke-virtual {v3}, Lkki;->i()Lkzm;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkjc;->a(Lkzm;)Lkjc;

    move-result-object v2

    if-nez v0, :cond_5

    move-object v0, v1

    .line 1420
    :goto_5
    invoke-virtual {v2, v0}, Lkjc;->b(Loya;)Lkjc;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    .line 1421
    invoke-virtual {v2}, Lkki;->j()Lkzz;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkjc;->a(Lkzz;)Lkjc;

    move-result-object v0

    .line 1422
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    :goto_6
    invoke-virtual {v0, v1}, Lkjc;->f(Ljava/lang/String;)Lkjc;

    move-result-object v0

    .line 1423
    invoke-virtual {v0}, Lkjc;->a()Lkjc;

    move-result-object v0

    .line 1424
    invoke-virtual {v0, v8}, Lkjc;->g(Ljava/lang/String;)Lkjc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    .line 1425
    invoke-virtual {v1}, Ljbx;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkjc;->h(Ljava/lang/String;)Lkjc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Z:Lkky;

    .line 1426
    invoke-virtual {v1}, Lkky;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lkjc;->a(Z)Lkjc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Z:Lkky;

    .line 1427
    invoke-virtual {v1}, Lkky;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Lkjc;->b(Z)Lkjc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Z:Lkky;

    .line 1428
    invoke-virtual {v1}, Lkky;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lkjc;->c(Z)Lkjc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    .line 1429
    invoke-virtual {v1}, Lkmy;->a()Lkji;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkjc;->a(Lkji;)Lkjc;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    .line 1398
    goto/16 :goto_0

    :cond_1
    move-object v2, v1

    .line 1407
    goto/16 :goto_1

    :cond_2
    move-object v2, v1

    .line 1409
    goto/16 :goto_2

    :cond_3
    move-object v2, v1

    .line 1411
    goto/16 :goto_3

    :cond_4
    move-object v2, v1

    .line 1415
    goto/16 :goto_4

    .line 1420
    :cond_5
    invoke-virtual {v0}, Lkkz;->d()Lkkp;

    move-result-object v0

    invoke-virtual {v0}, Lkkp;->a()Loya;

    move-result-object v0

    goto :goto_5

    .line 1422
    :cond_6
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_6
.end method

.method public c()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->F:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->G()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 338
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1550
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 1130
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x()V

    .line 1131
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->v()V

    .line 1132
    return-void
.end method

.method public d(Lhgw;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1566
    invoke-virtual {p1}, Lhgw;->i()I

    move-result v1

    if-nez v1, :cond_1

    .line 1570
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, v0}, Lhgw;->c(I)Lkxr;

    move-result-object v1

    invoke-virtual {v1}, Lkxr;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 1482
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g()Lbb;

    move-result-object v0

    const/4 v1, 0x3

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->B:Lkmu;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1484
    return-void
.end method

.method public finish()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1530
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v0

    const v2, 0x7f10057c

    .line 1531
    invoke-virtual {v0, v2}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->U()V

    move v0, v1

    .line 1533
    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v2}, Lkmy;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1534
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v2, v0}, Lkmy;->a(I)Lkji;

    .line 1533
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1537
    :cond_0
    invoke-super {p0}, Lloa;->finish()V

    .line 1539
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_1

    .line 1540
    const v0, 0x7f050020

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->overridePendingTransition(II)V

    .line 1542
    :cond_1
    return-void
.end method

.method public l()Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 1478
    const/4 v0, 0x0

    return-object v0
.end method

.method public m()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1453
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0}, Lkmy;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1454
    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v2}, Lkmy;->c()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1455
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v2, v0}, Lkmy;->a(I)Lkji;

    move-result-object v2

    invoke-interface {v2}, Lkji;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1460
    :goto_1
    return v1

    .line 1454
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1460
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public n()V
    .locals 0

    .prologue
    .line 1467
    return-void
.end method

.method public o()V
    .locals 0

    .prologue
    .line 1473
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 706
    invoke-super {p0, p1, p2, p3}, Lloa;->onActivityResult(IILandroid/content/Intent;)V

    .line 710
    packed-switch p1, :pswitch_data_0

    .line 715
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C()V

    .line 716
    return-void

    .line 712
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v0, "restrict_to_domain"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "extra_acl"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    const-string v2, "restrict_to_domain"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Lhgw;Z)V

    goto :goto_0

    :cond_1
    const-string v0, "extra_acl"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Lhgw;)V

    goto :goto_0

    .line 710
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1488
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->h()Llkt;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Llkt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1489
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->v()V

    .line 1507
    :goto_0
    return-void

    .line 1493
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->U()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1496
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0}, Lkmy;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0}, Lkmy;->a()Lkji;

    move-result-object v0

    invoke-interface {v0}, Lkji;->i()Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    :goto_2
    const v2, 0x7f0a02c4

    .line 1497
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a02c5

    .line 1498
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1494
    invoke-static {v1, v0, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 1499
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 1496
    goto :goto_1

    :cond_2
    const v0, 0x7f0a02c3

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1501
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0}, Lkmy;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1502
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0, v1}, Lkmy;->a(Lkji;)V

    goto :goto_0

    .line 1505
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 759
    invoke-super {p0, p1}, Lloa;->onCreate(Landroid/os/Bundle;)V

    .line 762
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->x:Llnh;

    const-class v0, Lieh;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 763
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 765
    sget-object v4, Lkmx;->c:Lief;

    invoke-interface {v0, v4, v3}, Lieh;->b(Lief;I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->z:Z

    .line 768
    sget-object v4, Lkmx;->e:Lief;

    invoke-interface {v0, v4, v3}, Lieh;->b(Lief;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->A:Z

    .line 771
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 772
    const-string v0, "clear_acl"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->l:Z

    .line 773
    const-string v0, "category_display_mode"

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->V:I

    .line 774
    const-string v0, "promoted_post_data"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->v:[B

    .line 775
    const-string v0, "circle_usage_type"

    const/4 v4, 0x5

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ai:I

    .line 776
    const-string v0, "is_limited_sharing"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->T:Z

    .line 777
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 779
    const/16 v0, 0x9

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ai:I

    .line 782
    :cond_0
    if-nez p1, :cond_b

    .line 784
    const-string v0, "external_id"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 785
    const-string v0, "external_id"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->k:Ljava/lang/String;

    .line 787
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->k:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 796
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/16 v0, 0x20

    invoke-static {v0}, Llsu;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x15

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->k:Ljava/lang/String;

    .line 799
    :cond_2
    const-string v0, "extra_acl"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->S:Lhgw;

    .line 801
    const-string v0, "com.google.android.apps.plus.GOOGLE_BIRTHDAY_POST"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 802
    new-instance v0, Lhgw;

    new-instance v4, Ljqs;

    const-string v5, "com.google.android.apps.plus.RECIPIENT_ID"

    .line 803
    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.google.android.apps.plus.RECIPIENT_NAME"

    .line 804
    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-direct {v4, v5, v3, v6}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v4}, Lhgw;-><init>(Ljqs;)V

    .line 802
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Lhgw;)V

    .line 815
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->f()Z

    move-result v0

    if-nez v0, :cond_3

    .line 816
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y()Z

    move-result v0

    if-nez v0, :cond_9

    :cond_3
    move v0, v2

    .line 817
    :goto_1
    if-eqz v0, :cond_a

    move v0, v1

    :goto_2
    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ab:I

    .line 831
    :goto_3
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0, p0}, Ljbx;->a(Ljby;)V

    .line 832
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y:Lklb;

    invoke-virtual {v0, p0}, Lklb;->a(Lklc;)V

    .line 834
    if-nez p1, :cond_4

    .line 835
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->B()V

    .line 838
    :cond_4
    const v0, 0x7f0401da

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->setContentView(I)V

    .line 840
    const v0, 0x7f10058b

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 841
    if-eqz v0, :cond_5

    .line 842
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->al:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 843
    if-eqz p1, :cond_c

    const-string v3, "PICKER_HEADER_SHOWING"

    .line 844
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 845
    :goto_4
    new-instance v3, Lkll;

    invoke-direct {v3, p0, v0, v2}, Lkll;-><init>(Landroid/content/Context;Landroid/view/View;Z)V

    iput-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    .line 848
    :cond_5
    const v0, 0x7f100576

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M:Landroid/view/ViewGroup;

    .line 850
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M:Landroid/view/ViewGroup;

    const v2, 0x7f100577

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->K:Landroid/widget/ScrollView;

    .line 852
    const v0, 0x7f100573

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(I)Landroid/view/View;

    .line 853
    const v0, 0x7f10056f

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->W:Landroid/view/ViewGroup;

    .line 855
    const v0, 0x7f100594

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(I)Landroid/view/View;

    .line 856
    const v0, 0x7f100591

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->X:Landroid/view/ViewGroup;

    .line 858
    const v0, 0x7f1005a2

    sget-object v2, Lonj;->f:Lhmn;

    invoke-direct {p0, v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(ILhmn;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->F:Landroid/widget/ImageButton;

    .line 859
    const v0, 0x7f100598

    sget-object v2, Lonj;->d:Lhmn;

    invoke-direct {p0, v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(ILhmn;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->D:Landroid/widget/ImageButton;

    .line 860
    const v0, 0x7f10059a

    sget-object v2, Lonj;->a:Lhmn;

    invoke-direct {p0, v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(ILhmn;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->G:Landroid/widget/ImageButton;

    .line 861
    const v0, 0x7f100599

    sget-object v2, Lonj;->c:Lhmn;

    invoke-direct {p0, v0, v2}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(ILhmn;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->E:Landroid/widget/ImageButton;

    .line 862
    const v0, 0x7f100597

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->H:Landroid/widget/LinearLayout;

    .line 863
    const v0, 0x7f10059d

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 865
    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v2}, Lkki;->w()I

    move-result v2

    if-ne v2, v8, :cond_d

    .line 866
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 871
    :goto_5
    const v0, 0x7f100562

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(I)Landroid/view/View;

    .line 873
    const v0, 0x7f100148

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    .line 874
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->c(I)V

    .line 875
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ah:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->S:Lhgw;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Lhgw;)V

    .line 877
    const v0, 0x7f10059b

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->I:Landroid/widget/TextView;

    .line 878
    const v0, 0x7f10059c

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->J:Landroid/widget/TextView;

    .line 880
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->R()V

    .line 882
    const v0, 0x7f10059f

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(I)Landroid/view/View;

    .line 883
    const v0, 0x7f100566

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->o:Landroid/view/View;

    .line 884
    const v0, 0x7f100567

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->p:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 885
    const v0, 0x7f100568

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->q:Landroid/widget/TextView;

    .line 887
    const v0, 0x7f10057a

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->L:Landroid/view/View;

    invoke-static {}, Llsj;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->L:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->L:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 889
    :goto_6
    const v0, 0x7f10015e

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    .line 890
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    .line 891
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020588

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 890
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 892
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Z)V

    .line 893
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llks;)V

    .line 894
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v1, Llkt;->d:Llkt;

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llkt;F)V

    .line 895
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v1, Llkt;->c:Llkt;

    .line 896
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->X()F

    move-result v2

    .line 895
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llkt;F)V

    .line 897
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v1, Llkt;->b:Llkt;

    invoke-virtual {v0, v1, v9}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llkt;F)V

    .line 899
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 900
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v1, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a:Llku;

    sget-object v2, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a:Llku;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llku;Llku;)V

    .line 909
    :goto_7
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v0

    const v1, 0x7f10057c

    .line 910
    invoke-virtual {v0, v1}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a()Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->N:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    .line 911
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->N:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    new-instance v1, Lkmm;

    invoke-direct {v1, p0}, Lkmm;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->a(Llje;)V

    .line 918
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    const v1, 0x7f100584

    .line 919
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->O:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    .line 920
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->O:Lcom/google/android/libraries/social/ui/views/ObservableGridView;

    new-instance v1, Lkmn;

    invoke-direct {v1, p0}, Lkmn;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/ObservableGridView;->a(Llje;)V

    .line 927
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v0

    const v1, 0x7f100578

    .line 928
    invoke-virtual {v0, v1}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    .line 929
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    new-instance v1, Lkmo;

    invoke-direct {v1, p0}, Lkmo;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a(Lkkl;)V

    .line 942
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    new-instance v1, Lkmp;

    invoke-direct {v1, p0}, Lkmp;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    invoke-virtual {v0, v1}, Lkki;->a(Lkkk;)V

    .line 948
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    new-instance v1, Lkmq;

    invoke-direct {v1, p0}, Lkmq;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    invoke-virtual {v0, v1}, Lkki;->a(Lkkj;)V

    .line 969
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a(Lkkh;)V

    .line 971
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    move-result-object v0

    if-nez v0, :cond_6

    .line 973
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->S:Lhgw;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Lhgw;)V

    .line 975
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ag:Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/impl/ComposedContentFragment;->a()Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ak:Lkjm;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/sharekit/impl/CommentBoxFragment;->a(ILkjm;)V

    .line 977
    const v0, 0x7f100165

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->r:Landroid/view/View;

    .line 979
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0, p0}, Lkmy;->a(Lkjk;)V

    .line 981
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->S()V

    .line 982
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->z()V

    .line 983
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C()V

    .line 984
    return-void

    .line 806
    :cond_7
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->c(Landroid/content/Intent;)Lhgw;

    move-result-object v0

    .line 807
    if-eqz v0, :cond_8

    .line 808
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Lhgw;)V

    goto/16 :goto_0

    .line 811
    :cond_8
    const-string v0, "extra_acl"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->b(Lhgw;)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 816
    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 817
    goto/16 :goto_2

    .line 819
    :cond_b
    const-string v0, "AUDIENCE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->S:Lhgw;

    .line 820
    const-string v0, "DEFAULT_AUDIENCE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->R:Z

    .line 821
    const-string v0, "PICKER_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ab:I

    .line 822
    const-string v0, "ACTIVITY_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->k:Ljava/lang/String;

    .line 823
    const-string v0, "ACTIVE_DIALOG_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aj:I

    .line 824
    const-string v0, "AUDIENCE_LOADED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->m:Z

    .line 825
    const-string v0, "restrict_audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->i:Lhgw;

    .line 826
    const-string v0, "restrict_to_domain"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    .line 827
    const-string v0, "is_restricted_reshare"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->U:Z

    .line 828
    const-string v0, "using_custom_buttonbar"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aa:Z

    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 844
    goto/16 :goto_4

    .line 868
    :cond_d
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 887
    :cond_e
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->L:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 904
    :cond_f
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    sget-object v1, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->b:Llku;

    sget-object v2, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->b:Llku;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->a(Llku;Llku;)V

    goto/16 :goto_7
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1005
    invoke-super {p0, p1}, Lloa;->onPostCreate(Landroid/os/Bundle;)V

    .line 1008
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1009
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->v()V

    .line 1018
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P()V

    .line 1014
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1015
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->v()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 1022
    invoke-super {p0}, Lloa;->onResume()V

    .line 1023
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->F:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->G()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1025
    iget-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->m:Z

    if-eqz v0, :cond_0

    .line 1026
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1029
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->g:Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/expandingscrollview/ExpandingScrollView;->h()Llkt;

    move-result-object v0

    .line 1030
    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(Llkt;)V

    .line 1031
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 988
    invoke-super {p0, p1}, Lloa;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 989
    const-string v0, "AUDIENCE"

    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->M()Lhgw;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 990
    const-string v0, "DEFAULT_AUDIENCE"

    iget-boolean v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->R:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 991
    const-string v0, "PICKER_TYPE"

    iget v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->ab:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 992
    const-string v0, "ACTIVITY_ID"

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    const-string v0, "ACTIVE_DIALOG_NAME"

    iget v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aj:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 994
    const-string v1, "PICKER_HEADER_SHOWING"

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->P:Lkll;

    .line 995
    invoke-virtual {v0}, Lkll;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 994
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 996
    const-string v0, "AUDIENCE_LOADED"

    iget-boolean v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 997
    const-string v0, "restrict_audience"

    iget-object v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->i:Lhgw;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 998
    const-string v0, "restrict_to_domain"

    iget-boolean v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 999
    const-string v0, "is_restricted_reshare"

    iget-boolean v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->U:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1000
    const-string v0, "using_custom_buttonbar"

    iget-boolean v1, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aa:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1001
    return-void

    .line 995
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()V
    .locals 0

    .prologue
    .line 1721
    return-void
.end method

.method public q()V
    .locals 0

    .prologue
    .line 333
    return-void
.end method

.method public r()V
    .locals 0

    .prologue
    .line 1655
    return-void
.end method

.method public s()V
    .locals 0

    .prologue
    .line 1661
    return-void
.end method

.method public t()V
    .locals 0

    .prologue
    .line 393
    invoke-static {p0}, Liew;->a(Landroid/app/Activity;)V

    .line 394
    return-void
.end method

.method public u()V
    .locals 2

    .prologue
    .line 397
    const v0, 0x7f0a02ba

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 398
    return-void
.end method

.method public v()V
    .locals 1

    .prologue
    .line 538
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->a(I)V

    .line 539
    return-void
.end method

.method public w()V
    .locals 2

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->K:Landroid/widget/ScrollView;

    new-instance v1, Lkml;

    invoke-direct {v1, p0}, Lkml;-><init>(Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 605
    return-void
.end method

.method public x()V
    .locals 4

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    invoke-virtual {v0}, Lkmy;->a()Lkji;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 611
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->f()Lae;

    move-result-object v1

    .line 612
    const-string v2, "SHARELET_BUTTON_BAR"

    invoke-virtual {v1, v2}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v2

    .line 613
    if-nez v0, :cond_2

    if-eqz v2, :cond_2

    .line 614
    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    invoke-virtual {v1, v2}, Lat;->a(Lu;)Lat;

    move-result-object v1

    invoke-virtual {v1}, Lat;->b()I

    .line 619
    :cond_0
    :goto_1
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aa:Z

    .line 620
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->z()V

    .line 621
    return-void

    .line 609
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->C:Lkmy;

    .line 610
    invoke-virtual {v0}, Lkmy;->a()Lkji;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lkji;->a(I)Lu;

    move-result-object v0

    goto :goto_0

    .line 615
    :cond_2
    if-eqz v0, :cond_0

    .line 616
    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const v2, 0x7f100596

    const-string v3, "SHARELET_BUTTON_BAR"

    invoke-virtual {v1, v2, v0, v3}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    move-result-object v1

    .line 617
    invoke-virtual {v1}, Lat;->b()I

    goto :goto_1

    .line 619
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public y()V
    .locals 1

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Y:Lklb;

    invoke-virtual {v0}, Lklb;->b()Lkkz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1041
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->v()V

    .line 1043
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->z()V

    .line 1044
    return-void
.end method

.method public z()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 624
    iget-object v0, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v0}, Ljbx;->h()Z

    move-result v0

    if-nez v0, :cond_2

    move v5, v1

    .line 625
    :goto_0
    if-nez v5, :cond_3

    move v0, v1

    .line 629
    :goto_1
    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->H:Landroid/widget/LinearLayout;

    iget-boolean v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->aa:Z

    if-eqz v3, :cond_4

    move v3, v4

    :goto_2
    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 631
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->s:Ljbx;

    invoke-virtual {v3}, Ljbx;->f()Z

    move-result v3

    if-eqz v3, :cond_d

    move v1, v2

    move v3, v2

    .line 636
    :goto_3
    invoke-direct {p0}, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->Q()Z

    move-result v6

    if-eqz v6, :cond_0

    move v3, v2

    move v0, v2

    move v5, v2

    .line 642
    :cond_0
    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->t:Lkki;

    invoke-virtual {v6}, Lkki;->w()I

    move-result v6

    if-ne v6, v4, :cond_5

    .line 643
    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->p:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v6, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 644
    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 650
    :goto_4
    iget-object v7, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->G:Landroid/widget/ImageButton;

    if-eqz v0, :cond_6

    move v6, v2

    :goto_5
    invoke-virtual {v7, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 651
    iget-object v7, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->D:Landroid/widget/ImageButton;

    if-eqz v0, :cond_7

    move v6, v2

    :goto_6
    invoke-virtual {v7, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 652
    iget-object v7, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->E:Landroid/widget/ImageButton;

    if-eqz v0, :cond_8

    move v6, v2

    :goto_7
    invoke-virtual {v7, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 653
    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->I:Landroid/widget/TextView;

    if-eqz v3, :cond_9

    move v3, v2

    :goto_8
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 654
    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->J:Landroid/widget/TextView;

    if-eqz v5, :cond_a

    move v3, v2

    :goto_9
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 655
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->J:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    move v1, v2

    .line 657
    :goto_a
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->H:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_c

    .line 658
    iget-object v3, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->H:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 659
    const v3, 0x7f10004a

    invoke-virtual {v5, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    .line 660
    if-eqz v3, :cond_1

    .line 661
    if-eqz v0, :cond_b

    move v3, v2

    :goto_b
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 657
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_2
    move v5, v2

    .line 624
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 625
    goto/16 :goto_1

    :cond_4
    move v3, v2

    .line 629
    goto :goto_2

    .line 646
    :cond_5
    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->p:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v6, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 647
    iget-object v6, p0, Lcom/google/android/libraries/social/sharekit/impl/ShareboxActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_6
    move v6, v4

    .line 650
    goto :goto_5

    :cond_7
    move v6, v4

    .line 651
    goto :goto_6

    :cond_8
    move v6, v4

    .line 652
    goto :goto_7

    :cond_9
    move v3, v4

    .line 653
    goto :goto_8

    :cond_a
    move v3, v4

    .line 654
    goto :goto_9

    :cond_b
    move v3, v4

    .line 661
    goto :goto_b

    .line 664
    :cond_c
    return-void

    :cond_d
    move v3, v5

    goto/16 :goto_3
.end method
