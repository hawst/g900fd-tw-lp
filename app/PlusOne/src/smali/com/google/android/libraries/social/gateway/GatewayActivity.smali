.class public Lcom/google/android/libraries/social/gateway/GatewayActivity;
.super Llon;
.source "PG"

# interfaces
.implements Ligv;


# instance fields
.field private g:Livx;

.field private h:Ligy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Llon;-><init>()V

    .line 31
    new-instance v0, Livx;

    iget-object v1, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->f:Llqc;

    invoke-direct {v0, p0, v1}, Livx;-><init>(Lz;Llqr;)V

    iget-object v1, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->e:Llnh;

    .line 32
    invoke-virtual {v0, v1}, Livx;->a(Llnh;)Livx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->g:Livx;

    .line 186
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "viewerid"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "viewerid"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "effectiveid"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "effectiveid"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v2, v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v0, 0x80000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, v2, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v1, Landroid/content/ComponentName;

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-object v0, v2

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->a(Landroid/content/Intent;)V

    .line 126
    return-void

    .line 125
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->h()V

    .line 122
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 169
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->setResult(I)V

    .line 170
    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->finish()V

    .line 171
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 175
    if-eqz p2, :cond_0

    .line 176
    invoke-static {p1, p2}, Ligw;->a(ILjava/lang/String;)Ligw;

    move-result-object v0

    .line 177
    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->f()Lae;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v0, v1, v2}, Ligw;->a(Lae;Ljava/lang/String;)V

    .line 181
    :goto_0
    return-void

    .line 179
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->a(I)V

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 71
    if-eqz p1, :cond_4

    .line 72
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const v1, 0x10080

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_2

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v4, :cond_1

    const-string v5, "native_handler"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 74
    :goto_1
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 81
    :cond_0
    const/high16 v0, 0x2010000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->e:Llnh;

    const-class v1, Liha;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liha;

    .line 84
    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-interface {v0, v3, p1}, Liha;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    goto :goto_2

    .line 73
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 87
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->startActivity(Landroid/content/Intent;)V

    .line 88
    invoke-virtual {p0, v2, v2}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->overridePendingTransition(II)V

    .line 90
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->finish()V

    .line 91
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 38
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->e:Llnh;

    const-class v1, Ligx;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ligx;

    invoke-interface {v0, v2}, Ligx;->a(Landroid/content/Intent;)Ligy;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    iput-object v0, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->h:Ligy;

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->h:Ligy;

    if-nez v0, :cond_3

    .line 43
    invoke-direct {p0}, Lcom/google/android/libraries/social/gateway/GatewayActivity;->h()V

    .line 51
    :cond_0
    :goto_2
    return-void

    .line 40
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 47
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->h:Ligy;

    iget-object v1, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->f:Llqc;

    iget-object v2, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->g:Livx;

    invoke-interface {v0, p0, v1, p0, v2}, Ligy;->a(Landroid/app/Activity;Llqr;Ligv;Livx;)V

    .line 48
    if-nez p1, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/social/gateway/GatewayActivity;->h:Ligy;

    invoke-interface {v0}, Ligy;->a()V

    goto :goto_2
.end method
