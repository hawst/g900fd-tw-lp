.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;
.super Ljnl;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:Landroid/graphics/drawable/Drawable;


# instance fields
.field private o:Ljmv;

.field private p:Landroid/widget/TextView;

.field private q:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 60
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    const v1, 0x7f02027c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a:Landroid/graphics/drawable/Drawable;

    .line 49
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    const v1, 0x7f02027c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a:Landroid/graphics/drawable/Drawable;

    .line 53
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    const v1, 0x7f02027c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a:Landroid/graphics/drawable/Drawable;

    .line 57
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;)Ljmv;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->o:Ljmv;

    return-object v0
.end method

.method public static b(Lnjt;)Z
    .locals 1

    .prologue
    .line 171
    if-eqz p0, :cond_0

    .line 172
    iget-object v0, p0, Lnjt;->e:Lnkd;

    .line 173
    if-eqz v0, :cond_0

    .line 174
    iget-object v0, v0, Lnkd;->m:Lnik;

    .line 175
    if-eqz v0, :cond_0

    .line 176
    iget-object v0, v0, Lnik;->a:[Lnij;

    .line 177
    if-eqz v0, :cond_0

    array-length v0, v0

    if-eqz v0, :cond_0

    .line 178
    const/4 v0, 0x1

    .line 184
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-super {p0}, Ljnl;->a()V

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->q:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 104
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    invoke-static {p1}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->q:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v2, v0, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->q:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->q:Lcom/google/android/libraries/social/media/ui/MediaView;

    new-instance v1, Ljmt;

    invoke-direct {v1, p0}, Ljmt;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->q:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 87
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    if-nez p1, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 101
    :goto_1
    return-void

    .line 91
    :cond_0
    sget-object v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 93
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->j:Z

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    const v2, 0x7f0a0314

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->e(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Ljmv;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->o:Ljmv;

    .line 84
    return-void
.end method

.method public a(Lnjt;)V
    .locals 8

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 123
    .line 126
    if-eqz p1, :cond_3

    .line 130
    iget-object v0, p1, Lnjt;->e:Lnkd;

    .line 131
    if-eqz v0, :cond_3

    .line 132
    iget-object v0, v0, Lnkd;->m:Lnik;

    .line 133
    if-eqz v0, :cond_3

    .line 134
    iget-object v2, v0, Lnik;->b:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    .line 135
    iget-object v0, v0, Lnik;->a:[Lnij;

    .line 136
    if-eqz v0, :cond_2

    array-length v2, v0

    if-eqz v2, :cond_2

    .line 137
    aget-object v6, v0, v4

    .line 138
    iget-object v2, v6, Lnij;->g:Ljava/lang/String;

    .line 139
    iget-object v0, v6, Lnij;->h:Ljava/lang/String;

    .line 140
    iget-object v7, v6, Lnij;->c:Ljava/lang/Double;

    .line 141
    iget-object v6, v6, Lnij;->d:Ljava/lang/Double;

    .line 147
    :goto_0
    invoke-virtual {p0, v2, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a(Ljava/lang/String;Z)V

    .line 148
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a(Ljava/lang/String;)V

    .line 150
    iget-boolean v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->j:Z

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->r:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->r:Landroid/view/View;

    new-instance v1, Ljmu;

    invoke-direct {v1, p0}, Ljmu;-><init>(Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->s:Landroid/view/View;

    if-nez v3, :cond_0

    :goto_1
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 168
    :goto_2
    return-void

    :cond_0
    move v4, v5

    .line 162
    goto :goto_1

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->r:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_2
    move-object v0, v1

    move-object v2, v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    move-object v2, v1

    move v3, v4

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 69
    const v0, 0x7f100404

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->p:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f100288

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->q:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 71
    const v0, 0x7f100406

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->r:Landroid/view/View;

    .line 72
    const v0, 0x7f100240

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->s:Landroid/view/View;

    .line 73
    return-void
.end method
