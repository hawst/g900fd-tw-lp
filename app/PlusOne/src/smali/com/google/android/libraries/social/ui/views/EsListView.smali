.class public Lcom/google/android/libraries/social/ui/views/EsListView;
.super Landroid/widget/ListView;
.source "PG"


# instance fields
.field private final a:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v0, Lljb;

    invoke-direct {v0, p0}, Lljb;-><init>(Lcom/google/android/libraries/social/ui/views/EsListView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/EsListView;->a:Landroid/database/DataSetObserver;

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 25
    invoke-static {p1, p2}, Lcom/google/android/libraries/social/ui/views/EsListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    new-instance v0, Lljb;

    invoke-direct {v0, p0}, Lljb;-><init>(Lcom/google/android/libraries/social/ui/views/EsListView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/EsListView;->a:Landroid/database/DataSetObserver;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 29
    invoke-static {p1, p2}, Lcom/google/android/libraries/social/ui/views/EsListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    new-instance v0, Lljb;

    invoke-direct {v0, p0}, Lljb;-><init>(Lcom/google/android/libraries/social/ui/views/EsListView;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/ui/views/EsListView;->a:Landroid/database/DataSetObserver;

    .line 30
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;
    .locals 3

    .prologue
    .line 33
    if-nez p1, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-object p0

    .line 42
    :cond_1
    const/4 v0, 0x0

    const-string v1, "theme"

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    .line 43
    if-eqz v1, :cond_0

    .line 44
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/EsListView;->isFastScrollEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    :goto_0
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/EsListView;->setFastScrollEnabled(Z)V

    .line 88
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/ui/views/EsListView;->setFastScrollEnabled(Z)V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/EsListView;->getWidth()I

    move-result v0

    .line 92
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/EsListView;->getHeight()I

    move-result v1

    .line 93
    invoke-virtual {p0, v0, v1, v0, v1}, Lcom/google/android/libraries/social/ui/views/EsListView;->onSizeChanged(IIII)V

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/social/ui/views/EsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 64
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 65
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 79
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/ui/views/EsListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_1

    .line 71
    iget-object v1, p0, Lcom/google/android/libraries/social/ui/views/EsListView;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 74
    :cond_1
    if-eqz p1, :cond_2

    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/social/ui/views/EsListView;->a:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 78
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method
