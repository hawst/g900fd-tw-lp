.class public Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# static fields
.field public static final a:Ljava/lang/Integer;

.field public static final b:Ljava/lang/Integer;

.field public static final c:Ljava/lang/Integer;

.field private static final g:[I


# instance fields
.field public d:[I

.field public e:[Ljava/lang/Object;

.field public f:I

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final i:I

.field private final j:Z

.field private k:I

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhbq;",
            ">;"
        }
    .end annotation
.end field

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a:Ljava/lang/Integer;

    .line 43
    const/16 v0, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b:Ljava/lang/Integer;

    .line 44
    const/16 v0, -0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->c:Ljava/lang/Integer;

    .line 46
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x262
        0x263
        0x264
        0x265
        0x266
        0x267
        0x268
        0x269
        0x26a
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->k:I

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->m:Ljava/util/List;

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->i:I

    .line 78
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a()[I

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a([I)V

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->h:Ljava/util/List;

    .line 80
    iput-boolean v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->j:Z

    .line 81
    return-void
.end method

.method public constructor <init>(I[I[IZ)V
    .locals 4

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->k:I

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->m:Ljava/util/List;

    .line 88
    iput p1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->i:I

    .line 89
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a([I)V

    .line 91
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 92
    if-eqz p3, :cond_0

    .line 93
    array-length v2, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p3, v0

    .line 94
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->h:Ljava/util/List;

    .line 99
    iput-boolean p4, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->j:Z

    .line 100
    return-void
.end method

.method public static h(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 262
    sget-object v2, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g:[I

    move v1, v0

    :goto_0
    const/16 v3, 0x9

    if-ge v1, v3, :cond_0

    aget v3, v2, v1

    .line 263
    if-ne v3, p0, :cond_1

    .line 264
    const/4 v0, 0x1

    .line 268
    :cond_0
    return v0

    .line 262
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 456
    sget-object v0, Lhan;->a:Lhao;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lhao;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V
    .locals 2

    .prologue
    .line 368
    iget-object v0, p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    .line 369
    iget-object v0, p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e:[Ljava/lang/Object;

    .line 370
    iget v0, p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->f:I

    iput v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->f:I

    .line 371
    iget v0, p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->k:I

    iput v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->k:I

    .line 374
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    .line 375
    iget-object v0, p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 376
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->addSubParameters(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    goto :goto_0

    .line 378
    :cond_0
    return-void
.end method

.method public a(Lhbq;)V
    .locals 2

    .prologue
    .line 530
    if-nez p1, :cond_0

    .line 531
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 544
    :cond_1
    return-void

    .line 538
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 539
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 540
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 541
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(Lhbq;)V

    goto :goto_0
.end method

.method protected a([I)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 106
    array-length v0, p1

    sget-object v2, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g:[I

    add-int/lit8 v0, v0, 0x9

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    array-length v2, p1

    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 108
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g:[I

    iget-object v2, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    array-length v3, p1

    sget-object v4, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g:[I

    const/16 v4, 0x9

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 111
    array-length v0, p1

    sget-object v2, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g:[I

    add-int/lit8 v0, v0, 0x9

    sget-object v2, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g:[I

    add-int/lit8 v0, v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e:[Ljava/lang/Object;

    move v0, v1

    .line 114
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 115
    iget-object v2, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e:[Ljava/lang/Object;

    aget v3, p1, v0

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->c(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->f:I

    .line 122
    const/16 v0, 0x262

    invoke-virtual {p0, v0, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 123
    const/16 v0, 0x263

    invoke-virtual {p0, v0, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 124
    const/16 v0, 0x264

    invoke-virtual {p0, v0, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 125
    const/16 v0, 0x265

    invoke-virtual {p0, v0, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 126
    const/16 v0, 0x266

    invoke-virtual {p0, v0, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 127
    const/16 v0, 0x267

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 128
    const/16 v0, 0x268

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 129
    const/16 v0, 0x269

    invoke-virtual {p0, v0, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 130
    const/16 v0, 0x26a

    invoke-virtual {p0, v0, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 131
    return-void
.end method

.method public a(IF)Z
    .locals 4

    .prologue
    .line 235
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 236
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v1

    .line 238
    instance-of v2, v0, Ljava/lang/Number;

    if-eqz v2, :cond_0

    instance-of v2, v1, Ljava/lang/Number;

    if-nez v2, :cond_1

    .line 239
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 243
    :cond_1
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v2

    move-object v0, v1

    .line 244
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 246
    cmpg-float v1, p2, v2

    if-gez v1, :cond_3

    move v0, v2

    .line 252
    :cond_2
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g(I)I

    move-result v1

    .line 253
    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    .line 254
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 248
    :cond_3
    cmpl-float v1, p2, v0

    if-gtz v1, :cond_2

    move v0, p2

    goto :goto_0

    .line 256
    :cond_4
    iget-object v2, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e:[Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v1

    .line 258
    cmpl-float v0, v0, p2

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public declared-synchronized a(ILjava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 203
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g(I)I

    move-result v4

    .line 205
    const/4 v1, -0x1

    if-eq v4, v1, :cond_6

    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e:[Ljava/lang/Object;

    aget-object v1, v1, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e:[Ljava/lang/Object;

    aget-object v1, v1, v4

    .line 206
    if-nez v1, :cond_3

    if-nez p2, :cond_2

    move v1, v2

    :goto_0
    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    .line 211
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e:[Ljava/lang/Object;

    aput-object p2, v1, v4

    .line 213
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->h:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->h:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->n:Z

    if-nez v1, :cond_1

    .line 219
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->n:Z

    .line 220
    check-cast p2, Ljava/lang/Number;

    .line 221
    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {p0, p1, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->activateStyleOrPreset(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;II)Z

    .line 222
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->n:Z

    .line 225
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v2

    .line 227
    :goto_1
    monitor-exit p0

    return v1

    :cond_2
    move v1, v3

    .line 206
    goto :goto_0

    :cond_3
    :try_start_1
    instance-of v5, v1, Ljava/lang/Number;

    if-eqz v5, :cond_5

    instance-of v5, p2, Ljava/lang/Number;

    if-eqz v5, :cond_5

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v5

    move-object v0, p2

    check-cast v0, Ljava/lang/Number;

    move-object v1, v0

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    cmpl-float v1, v5, v1

    if-nez v1, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_0

    :cond_5
    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_0

    :cond_6
    move v1, v3

    .line 208
    goto :goto_1

    .line 203
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected a()[I
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0
.end method

.method public addSubParameters(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V
    .locals 2
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbq;

    .line 425
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(Lhbq;)V

    goto :goto_0

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 428
    return-void
.end method

.method public affectsPanorama()Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 344
    iget-boolean v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->j:Z

    return v0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3e8

    goto :goto_0
.end method

.method public b(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 452
    sget-object v0, Lhan;->a:Lhao;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lhao;->b(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)Z
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 460
    sget-object v0, Lhan;->a:Lhao;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lhao;->c(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->h:Ljava/util/List;

    return-object v0
.end method

.method public d(I)[Ljava/lang/Object;
    .locals 6

    .prologue
    .line 482
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 483
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 485
    sub-int v0, v2, v1

    add-int/lit8 v0, v0, 0x1

    new-array v3, v0, [Ljava/lang/Integer;

    move v0, v1

    .line 486
    :goto_0
    if-gt v0, v2, :cond_0

    .line 487
    sub-int v4, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 486
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 490
    :cond_0
    return-object v3
.end method

.method public declared-synchronized e()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 2

    .prologue
    .line 355
    monitor-enter p0

    const/4 v1, 0x0

    .line 357
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 362
    :goto_0
    :try_start_1
    invoke-virtual {v0, p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364
    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    const/4 v0, 0x0

    .line 439
    :goto_0
    return v0

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 439
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected g(I)I
    .locals 2

    .prologue
    .line 144
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 145
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 150
    :goto_1
    return v0

    .line 144
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public g()V
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 572
    :cond_0
    return-void
.end method

.method public declared-synchronized getActiveParameterKey()I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 402
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getFilterType()I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 444
    iget v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->i:I

    return v0
.end method

.method public declared-synchronized getParameterBuffer(I)[B
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterValue(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getParameterFloat(I)F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterValue(I)Ljava/lang/Object;

    move-result-object v0

    .line 173
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getParameterInteger(I)I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterValue(I)Ljava/lang/Object;

    move-result-object v0

    .line 167
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getParameterKeys()[I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d:[I

    return-object v0
.end method

.method public declared-synchronized getParameterPointX(I)F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterValue(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 179
    iget v0, v0, Landroid/graphics/PointF;->x:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getParameterPointY(I)F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterValue(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    .line 185
    iget v0, v0, Landroid/graphics/PointF;->y:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getParameterString(I)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterValue(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getParameterValue(I)Ljava/lang/Object;
    .locals 2
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g(I)I

    move-result v0

    .line 157
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 158
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "Wrong parameterkey used."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 161
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e:[Ljava/lang/Object;

    aget-object v0, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public getSubParameters()Ljava/util/List;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->l:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 581
    :cond_0
    return-void
.end method

.method public i(I)F
    .locals 3

    .prologue
    .line 500
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 501
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 502
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v2

    .line 504
    sub-float/2addr v2, v1

    sub-float/2addr v0, v1

    div-float v0, v2, v0

    return v0
.end method

.method public isDefaultParameter(I)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 473
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized setActiveParameterKey(I)V
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 407
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    .line 413
    :goto_0
    monitor-exit p0

    return-void

    .line 411
    :cond_0
    :try_start_1
    iput p1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->f:I

    .line 412
    iget v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->f:I

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 407
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setParameterBuffer(I[B)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 303
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setParameterFloat(IF)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 288
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setParameterInteger(II)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 283
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setParameterPoint(IFF)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 293
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p2, p3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setParameterString(ILjava/lang/String;)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 298
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v0

    return v0
.end method
