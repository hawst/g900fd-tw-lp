.class public Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;
.super Ljnl;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private a:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/view/View;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/view/ViewGroup;

.field private t:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Ljnl;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Ljnl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public static b(Lnjt;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 171
    if-eqz p0, :cond_2

    .line 172
    iget-object v1, p0, Lnjt;->e:Lnkd;

    .line 173
    if-eqz v1, :cond_2

    .line 174
    iget-object v1, v1, Lnkd;->h:Lniz;

    .line 175
    if-eqz v1, :cond_2

    .line 176
    iget-object v2, v1, Lniz;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 185
    :cond_0
    :goto_0
    return v0

    .line 179
    :cond_1
    iget-object v2, v1, Lniz;->c:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v1, v1, Lniz;->c:[Ljava/lang/String;

    array-length v1, v1

    if-gtz v1, :cond_0

    .line 185
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Ljnl;->a()V

    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 73
    return-void
.end method

.method public a(Lnjt;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 138
    .line 142
    if-eqz p1, :cond_5

    .line 143
    iget-object v1, p1, Lnjt;->e:Lnkd;

    .line 144
    if-eqz v1, :cond_5

    .line 145
    iget-object v5, v1, Lnkd;->h:Lniz;

    .line 146
    if-eqz v5, :cond_5

    .line 147
    iget-object v2, v5, Lniz;->d:Ljava/lang/String;

    .line 148
    iget-object v1, v5, Lniz;->b:Ljava/lang/String;

    .line 149
    iget-object v6, v5, Lniz;->c:[Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, v5, Lniz;->c:[Ljava/lang/String;

    array-length v6, v6

    if-lez v6, :cond_0

    .line 151
    iget-object v0, v5, Lniz;->c:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 157
    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a(Ljava/lang/String;)Z

    move-result v2

    .line 158
    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    invoke-virtual {p0, v1, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 159
    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a(Ljava/util/List;Z)Z

    .line 161
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_3

    .line 162
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 163
    :cond_1
    :goto_2
    if-eqz v3, :cond_4

    .line 164
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->t:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    :goto_3
    return-void

    :cond_2
    move v2, v4

    .line 158
    goto :goto_1

    :cond_3
    move v3, v4

    .line 162
    goto :goto_2

    .line 166
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    move-object v1, v0

    move-object v2, v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    invoke-static {p1}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 78
    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Ljac;->a:Ljac;

    invoke-static {v3, v1, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 79
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    .line 80
    const/4 v0, 0x1

    .line 83
    :goto_0
    return v0

    .line 82
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 88
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    .line 89
    :goto_0
    if-eqz v2, :cond_1

    .line 90
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->o:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->p:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->p:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->o:Landroid/widget/TextView;

    invoke-virtual {p0, v3, p2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a(Landroid/widget/TextView;Z)V

    .line 94
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->p:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->d(Landroid/widget/TextView;)V

    .line 100
    :goto_1
    if-nez v2, :cond_2

    :goto_2
    return v0

    :cond_0
    move v2, v1

    .line 88
    goto :goto_0

    .line 96
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->o:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->p:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    .line 100
    goto :goto_2
.end method

.method public a(Ljava/util/List;Z)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 106
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    move v3, v1

    .line 107
    :goto_0
    if-eqz v3, :cond_4

    .line 108
    if-eqz p2, :cond_2

    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->q:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 113
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114
    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->r:Landroid/widget/TextView;

    if-nez p2, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p0, v4, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a(Landroid/widget/TextView;Z)V

    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 116
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 117
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v5}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->d(Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_3

    :cond_1
    move v3, v2

    .line 106
    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 114
    goto :goto_2

    .line 120
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->q:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 124
    :cond_5
    if-nez v3, :cond_6

    if-eqz p2, :cond_6

    :goto_4
    return v1

    :cond_6
    move v1, v2

    goto :goto_4
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 66
    const/16 v0, 0x9

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Ljnl;->onFinishInflate()V

    .line 54
    const v0, 0x7f100288

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->a:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 55
    const v0, 0x7f10040b

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->o:Landroid/widget/TextView;

    .line 56
    const v0, 0x7f100404

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->p:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f10040c

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->q:Landroid/view/View;

    .line 58
    const v0, 0x7f10040e

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->r:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f10040f

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->s:Landroid/view/ViewGroup;

    .line 60
    const v0, 0x7f1003e5

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->t:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->t:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->e(Landroid/widget/TextView;)V

    .line 62
    return-void
.end method
