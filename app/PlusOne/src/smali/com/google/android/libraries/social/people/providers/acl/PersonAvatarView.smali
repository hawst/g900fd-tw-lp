.class public final Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# static fields
.field private static a:I


# instance fields
.field private final b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private final c:Lhis;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 30
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 42
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 46
    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c()V

    .line 47
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 48
    const/16 v1, 0xd

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 49
    iget-object v1, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    new-instance v0, Lhis;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhis;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c:Lhis;

    .line 53
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 55
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 56
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 57
    iget-object v1, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c:Lhis;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 46
    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c()V

    .line 47
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 48
    const/16 v1, 0xd

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 49
    iget-object v1, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    new-instance v0, Lhis;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhis;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c:Lhis;

    .line 53
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 55
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 56
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 57
    iget-object v1, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c:Lhis;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 46
    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c()V

    .line 47
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 48
    const/16 v1, 0xd

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 49
    iget-object v1, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    new-instance v0, Lhis;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhis;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c:Lhis;

    .line 53
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 55
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 56
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 57
    iget-object v1, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c:Lhis;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 39
    return-void
.end method

.method private b()I
    .locals 2

    .prologue
    .line 82
    sget v0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->a:I

    if-nez v0, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01fe

    .line 84
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->a:I

    .line 86
    :cond_0
    sget v0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->a:I

    return v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    .line 97
    return-void
.end method


# virtual methods
.method public a(Ljpv;)Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;
    .locals 3

    .prologue
    .line 65
    if-eqz p1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->b:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-interface {p1}, Ljpv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Ljpv;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    return-object p0
.end method

.method public a()Lhis;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/social/people/providers/acl/PersonAvatarView;->c:Lhis;

    return-object v0
.end method
