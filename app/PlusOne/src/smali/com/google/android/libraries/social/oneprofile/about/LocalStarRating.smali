.class public Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;
.super Landroid/widget/LinearLayout;
.source "PG"


# static fields
.field private static b:Landroid/graphics/drawable/Drawable;

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:Landroid/graphics/drawable/Drawable;

.field private static e:Landroid/graphics/drawable/Drawable;

.field private static f:Landroid/graphics/drawable/Drawable;

.field private static g:Landroid/graphics/drawable/Drawable;


# instance fields
.field private a:[Landroid/widget/ImageView;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a(Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a(Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-direct {p0, p2}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a(Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x0

    .line 47
    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 49
    const v2, 0x7f0203f4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->b:Landroid/graphics/drawable/Drawable;

    .line 50
    const v2, 0x7f0203f2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->c:Landroid/graphics/drawable/Drawable;

    .line 51
    const v2, 0x7f0203f0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->d:Landroid/graphics/drawable/Drawable;

    .line 52
    const v2, 0x7f0203f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->e:Landroid/graphics/drawable/Drawable;

    .line 53
    const v2, 0x7f0203f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->f:Landroid/graphics/drawable/Drawable;

    .line 54
    const v2, 0x7f0203f1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->g:Landroid/graphics/drawable/Drawable;

    .line 57
    :cond_0
    if-eqz p1, :cond_5

    .line 58
    const/4 v1, 0x0

    const-string v2, "size"

    invoke-interface {p1, v1, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    if-eqz v1, :cond_1

    .line 60
    const-string v2, "12"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 61
    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->b:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->h:Landroid/graphics/drawable/Drawable;

    .line 62
    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->c:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->i:Landroid/graphics/drawable/Drawable;

    .line 63
    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->d:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->j:Landroid/graphics/drawable/Drawable;

    .line 76
    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->setOrientation(I)V

    .line 77
    new-array v1, v4, [Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a:[Landroid/widget/ImageView;

    .line 78
    :goto_1
    if-ge v0, v4, :cond_6

    .line 79
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a:[Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v0

    .line 80
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 81
    iget-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->addView(Landroid/view/View;)V

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_2
    const-string v2, "16"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 65
    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->e:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->h:Landroid/graphics/drawable/Drawable;

    .line 66
    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->f:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->i:Landroid/graphics/drawable/Drawable;

    .line 67
    sget-object v1, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->g:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 69
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid size value: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 73
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing size value!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_6
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 92
    const/16 v1, 0x1f4

    move v2, v1

    move v1, v0

    .line 94
    :goto_0
    const/4 v3, 0x5

    if-ge v0, v3, :cond_2

    .line 95
    if-le p1, v2, :cond_0

    .line 96
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 94
    :goto_1
    add-int/lit8 v0, v0, 0x1

    add-int/lit16 v2, v2, 0x3e8

    add-int/lit16 v1, v1, 0x3e8

    goto :goto_0

    .line 97
    :cond_0
    if-le p1, v1, :cond_1

    .line 98
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 100
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->a:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/libraries/social/oneprofile/about/LocalStarRating;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 103
    :cond_2
    return-void
.end method
