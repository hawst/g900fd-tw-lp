.class public Lcom/google/android/libraries/social/acl/AclPickerActivity;
.super Llon;
.source "PG"

# interfaces
.implements Lhgk;


# instance fields
.field private g:Lhfy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Llon;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lhgw;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 39
    const-string v1, "extra_acl"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 40
    const-string v1, "extra_acl_label"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string v1, "restrict_to_domain"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 42
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/social/acl/AclPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/AclPickerActivity;->finish()V

    .line 44
    const/4 v0, 0x0

    const v1, 0x7f050021

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/acl/AclPickerActivity;->overridePendingTransition(II)V

    .line 46
    return-void
.end method

.method protected a(Lu;ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 59
    .line 60
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/AclPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Lu;->k()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 61
    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Lu;->f(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/AclPickerActivity;->f()Lae;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v1

    .line 65
    invoke-virtual {v1, p2, p1, p3}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    .line 66
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lat;->a(I)Lat;

    .line 67
    invoke-virtual {v1}, Lat;->c()I

    .line 69
    invoke-virtual {v0}, Lae;->b()Z

    .line 70
    return-void

    .line 60
    :cond_1
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v3

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    move-object v0, v2

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerActivity;->g:Lhfy;

    invoke-virtual {v0}, Lhfy;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    invoke-super {p0}, Llon;->onBackPressed()V

    .line 97
    const/4 v0, 0x0

    const v1, 0x7f050021

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/social/acl/AclPickerActivity;->overridePendingTransition(II)V

    .line 99
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f100143

    .line 23
    invoke-super {p0, p1}, Llon;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v0, 0x7f04001e

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/social/acl/AclPickerActivity;->setContentView(I)V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/libraries/social/acl/AclPickerActivity;->f()Lae;

    move-result-object v0

    invoke-virtual {v0, v2}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lhfy;

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerActivity;->g:Lhfy;

    .line 28
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerActivity;->g:Lhfy;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lhfy;

    invoke-direct {v0}, Lhfy;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerActivity;->g:Lhfy;

    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerActivity;->g:Lhfy;

    const-string v1, "AclPickerActivity"

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/android/libraries/social/acl/AclPickerActivity;->a(Lu;ILjava/lang/String;)V

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/acl/AclPickerActivity;->g:Lhfy;

    invoke-virtual {v0, p0}, Lhfy;->a(Lhgk;)V

    .line 33
    return-void
.end method
