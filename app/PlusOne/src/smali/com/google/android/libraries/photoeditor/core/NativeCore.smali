.class public final enum Lcom/google/android/libraries/photoeditor/core/NativeCore;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhao;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/libraries/photoeditor/core/NativeCore;",
        ">;",
        "Lhao;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

.field private static final b:Landroid/graphics/BitmapFactory$Options;

.field private static final synthetic h:[Lcom/google/android/libraries/photoeditor/core/NativeCore;


# instance fields
.field private c:Landroid/content/ContextWrapper;

.field private d:Lhbf;

.field private e:Lhbn;

.field private f:Z

.field private g:Lhbm;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/photoeditor/core/NativeCore;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    .line 45
    new-array v0, v3, [Lcom/google/android/libraries/photoeditor/core/NativeCore;

    sget-object v1, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->h:[Lcom/google/android/libraries/photoeditor/core/NativeCore;

    .line 69
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 70
    sput-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->b:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 71
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->b:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 72
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->b:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 73
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->b:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    .line 74
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->b:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 75
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->b:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 76
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 80
    const-string v0, "photoeditor_native"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getAssets()Landroid/content/res/AssetManager;

    .line 144
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "filter_resources"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 146
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 147
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 151
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 719
    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 721
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 722
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 724
    :cond_0
    return-void
.end method

.method private native activateOnScreenFilter(IZ)I
.end method

.method public static native activateStyleOrPreset(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;II)Z
.end method

.method private b()Z
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 104
    iget-object v2, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    .line 105
    new-instance v5, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "filter_resources"

    invoke-direct {v5, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 106
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 107
    invoke-virtual {v5}, Ljava/io/File;->mkdir()Z

    .line 110
    :cond_0
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    .line 139
    :goto_0
    return v0

    .line 117
    :cond_1
    :try_start_0
    const-string v2, "filter_resources"

    invoke-virtual {v4, v2}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 118
    array-length v7, v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move v3, v0

    move-object v0, v1

    :goto_1
    if-ge v3, v7, :cond_5

    :try_start_1
    aget-object v2, v6, v3

    .line 119
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v5, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 120
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_a

    .line 121
    const-string v9, "filter_resources/"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v9, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v4, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 122
    :try_start_2
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 124
    :try_start_3
    invoke-static {v2, v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    move-object v1, v2

    .line 118
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_1

    .line 121
    :cond_2
    :try_start_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 128
    :catchall_0
    move-exception v2

    move-object v11, v2

    move-object v2, v0

    move-object v0, v11

    .line 129
    :goto_4
    if-eqz v2, :cond_3

    .line 130
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 133
    :cond_3
    if-eqz v1, :cond_4

    .line 134
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_4
    throw v0

    .line 129
    :cond_5
    if-eqz v0, :cond_6

    .line 130
    :try_start_6
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 133
    :cond_6
    if-eqz v1, :cond_7

    .line 134
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 139
    :cond_7
    const/4 v0, 0x1

    goto :goto_0

    .line 133
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_8

    .line 134
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_8
    throw v0

    .line 133
    :catchall_2
    move-exception v0

    if-eqz v1, :cond_9

    .line 134
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_9
    throw v0

    .line 128
    :catchall_3
    move-exception v0

    move-object v2, v1

    goto :goto_4

    :catchall_4
    move-exception v0

    goto :goto_4

    :catchall_5
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    goto :goto_4

    :cond_a
    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    goto :goto_3
.end method

.method private c()V
    .locals 2

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    if-nez v0, :cond_0

    .line 616
    const-string v0, "NativeCore"

    const-string v1, "ContextWrapper is not ready!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Context has not been initialized (use initContext())"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 619
    :cond_0
    return-void
.end method

.method public static native cleanupJNI()V
.end method

.method public static native contextAction(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;I)I
.end method

.method public static createRGBX8TextureFromBitmap(IIILandroid/graphics/Bitmap;)I
    .locals 8
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 214
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 219
    :cond_1
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v5, v4

    .line 218
    invoke-static/range {v0 .. v7}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->createRGBX8TextureFromBitmap(IIILandroid/graphics/Bitmap;IIII)I

    move-result v0

    return v0
.end method

.method public static native createRGBX8TextureFromBitmap(IIILandroid/graphics/Bitmap;IIII)I
.end method

.method public static native deactivateOffScreenFilter()V
.end method

.method public static native deleteOffscreenContext()V
.end method

.method public static native deleteTexture(I)V
.end method

.method public static native frameShouldShuffle(I)Z
.end method

.method public static native getDefaultValue(II)I
.end method

.method public static native getMaxValue(II)I
.end method

.method public static native getMinValue(II)I
.end method

.method public static native initOffscreenContext()I
.end method

.method public static native offscreenContextMakeCurrent()V
.end method

.method public static native offscreenFilterHundredPercentRegion(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
.end method

.method public static native offscreenFilterPreviewToBitmap(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Lcom/google/android/libraries/photoeditor/core/TilesProvider;II)Landroid/graphics/Bitmap;
.end method

.method public static native offscreenPrepareToApplyImage()V
.end method

.method public static native onSurfaceChanged()V
.end method

.method public static native onscreenFilterTileToScreen(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Lcom/google/android/libraries/photoeditor/core/TilesProvider;Lcom/google/android/libraries/photoeditor/core/Tile;IIZZIIII)I
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end method

.method public static native scaleImage(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
.end method

.method public static native setRenderScaleMode(I)V
.end method

.method private static native transformFilterParameterToImageSpace(Lcom/google/android/libraries/photoeditor/core/FilterChain;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/libraries/photoeditor/core/NativeCore;
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;

    return-object v0
.end method

.method public static values()[Lcom/google/android/libraries/photoeditor/core/NativeCore;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->h:[Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0}, [Lcom/google/android/libraries/photoeditor/core/NativeCore;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/photoeditor/core/NativeCore;

    return-object v0
.end method


# virtual methods
.method public a(II)I
    .locals 1

    .prologue
    .line 296
    invoke-static {p1, p2}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->getMinValue(II)I

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/libraries/photoeditor/core/FilterChain;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V
    .locals 0

    .prologue
    .line 338
    invoke-static {p1, p2}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->transformFilterParameterToImageSpace(Lcom/google/android/libraries/photoeditor/core/FilterChain;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 339
    return-void
.end method

.method public a(Lhbf;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->d:Lhbf;

    .line 161
    return-void
.end method

.method public a(Lhbm;)V
    .locals 0

    .prologue
    .line 711
    iput-object p1, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->g:Lhbm;

    .line 712
    return-void
.end method

.method public a(Lhbn;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->e:Lhbn;

    .line 165
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->g:Lhbm;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public native activateOffScreenFilter(I)I
.end method

.method public activateOnScreenFilterChecked(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Z)Z
    .locals 2
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 242
    invoke-virtual {p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    .line 243
    invoke-direct {p0, v0, p2}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->activateOnScreenFilter(IZ)I

    move-result v0

    .line 244
    if-gez v0, :cond_0

    .line 245
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 248
    :cond_0
    if-nez v0, :cond_1

    .line 254
    const/4 v1, 0x7

    invoke-static {p1, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->contextAction(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;I)I

    .line 257
    :cond_1
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(II)I
    .locals 1

    .prologue
    .line 301
    invoke-static {p1, p2}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->getMaxValue(II)I

    move-result v0

    return v0
.end method

.method public c(II)I
    .locals 1

    .prologue
    .line 306
    invoke-static {p1, p2}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->getDefaultValue(II)I

    move-result v0

    return v0
.end method

.method public cleanUpContext()V
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    .line 101
    return-void
.end method

.method public createAutorotatedTexture(Ljava/lang/String;IF)Landroid/graphics/Bitmap;
    .locals 8
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 627
    invoke-direct {p0, p1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 629
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 630
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 632
    int-to-float v5, v3

    int-to-float v6, v4

    div-float/2addr v5, v6

    .line 633
    cmpl-float v6, v5, v7

    if-eqz v6, :cond_0

    cmpl-float v6, p3, v7

    if-eqz v6, :cond_0

    cmpl-float v5, v5, v7

    if-lez v5, :cond_1

    move v5, v2

    :goto_0
    cmpl-float v6, p3, v7

    if-lez v6, :cond_2

    :goto_1
    if-eq v5, v2, :cond_0

    .line 637
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 638
    const/high16 v2, -0x3d4c0000    # -90.0f

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    move v2, v1

    move v6, v1

    .line 640
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 642
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    .line 646
    :cond_0
    return-object v0

    :cond_1
    move v5, v1

    .line 633
    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public declared-synchronized getCompare()Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 386
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPreviewSrcImage(II)Landroid/graphics/Bitmap;
    .locals 2
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->d:Lhbf;

    if-nez v0, :cond_1

    .line 397
    const/4 v0, 0x0

    .line 406
    :cond_0
    :goto_0
    return-object v0

    .line 400
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->d:Lhbf;

    invoke-virtual {v0}, Lhbf;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 402
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 403
    const/4 v1, 0x1

    invoke-static {v0, p1, p2, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 404
    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getScaledSrcImage(II)Landroid/graphics/Bitmap;
    .locals 4
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 412
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->d:Lhbf;

    if-nez v0, :cond_1

    .line 413
    const/4 v0, 0x0

    .line 426
    :cond_0
    :goto_0
    return-object v0

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->d:Lhbf;

    invoke-virtual {v0}, Lhbf;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 417
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 418
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 420
    if-gt p1, v1, :cond_0

    if-gt p2, v2, :cond_0

    if-ne p1, v1, :cond_2

    if-eq p2, v2, :cond_0

    :cond_2
    if-ne p1, v3, :cond_3

    if-eq p2, v3, :cond_0

    .line 425
    :cond_3
    const/4 v1, 0x1

    invoke-static {v0, p1, p2, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 426
    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public loadBackgroundTexture(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 7
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, 0x0

    .line 580
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->loadBitmapResource(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 582
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 583
    packed-switch p2, :pswitch_data_0

    .line 605
    :goto_0
    invoke-virtual {v5}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v2

    if-nez v2, :cond_0

    .line 607
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    .line 606
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 608
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    .line 611
    :cond_0
    return-object v0

    .line 585
    :pswitch_0
    invoke-virtual {v5, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_0

    .line 589
    :pswitch_1
    invoke-virtual {v5, v3, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_0

    .line 593
    :pswitch_2
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_0

    .line 597
    :pswitch_3
    const/high16 v2, -0x3d4c0000    # -90.0f

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_0

    .line 583
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public loadBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 443
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c()V

    .line 448
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    .line 449
    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 450
    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/libraries/photoeditor/core/NativeCore;->b:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 452
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 459
    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method public loadBitmapResource(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 476
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c()V

    .line 478
    invoke-direct {p0, p1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 479
    if-nez v0, :cond_0

    .line 480
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 483
    :cond_0
    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public loadFilterResource(Ljava/lang/String;)[B
    .locals 6
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 498
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c()V

    .line 499
    new-array v0, v0, [B

    .line 502
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "filter_resources"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 504
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 531
    :cond_0
    :goto_0
    return-object v0

    .line 508
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 509
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 514
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v1, v4

    new-array v1, v1, [B

    .line 515
    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 516
    const/4 v2, 0x0

    array-length v4, v1

    invoke-virtual {v3, v1, v2, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v2

    .line 517
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 519
    array-length v3, v1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-ne v2, v3, :cond_0

    move-object v0, v1

    .line 520
    goto :goto_0

    .line 524
    :catch_0
    move-exception v1

    const-string v1, "NativeCore"

    const-string v2, "loadFilterResource FileNotFoundException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 526
    :catch_1
    move-exception v1

    const-string v1, "NativeCore"

    const-string v2, "loadFilterResource IllegalStateException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 528
    :catch_2
    move-exception v1

    const-string v1, "NativeCore"

    const-string v2, "loadFilterResource IOException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadRawResource(Ljava/lang/String;)[B
    .locals 6
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 543
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c()V

    .line 545
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 546
    const-string v1, "raw"

    iget-object v2, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    .line 547
    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 548
    if-nez v1, :cond_0

    .line 549
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->loadFilterResource(Ljava/lang/String;)[B

    move-result-object v0

    .line 567
    :goto_0
    return-object v0

    .line 553
    :cond_0
    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 555
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 556
    const/16 v2, 0x4000

    new-array v2, v2, [B

    .line 558
    :goto_1
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 559
    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 565
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 567
    new-array v0, v5, [B

    goto :goto_0

    .line 562
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 564
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0
.end method

.method public onFilterChainProgressChanged(IFF)V
    .locals 0
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 685
    return-void
.end method

.method public onPreviewProgressChanged(FF)V
    .locals 2
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 698
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->g:Lhbm;

    if-eqz v0, :cond_0

    .line 699
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 700
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->g:Lhbm;

    invoke-interface {v0}, Lhbm;->Y()V

    .line 708
    :cond_0
    :goto_0
    return-void

    .line 701
    :cond_1
    cmpg-float v0, p1, p2

    if-gez v0, :cond_2

    .line 702
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->g:Lhbm;

    .line 703
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    .line 702
    invoke-interface {v0}, Lhbm;->x_()V

    goto :goto_0

    .line 705
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->g:Lhbm;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lhbm;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public recycleBitmap(Landroid/graphics/Bitmap;)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 655
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->d:Lhbf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->d:Lhbf;

    .line 656
    invoke-virtual {v0}, Lhbf;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->d:Lhbf;

    .line 657
    invoke-virtual {v0}, Lhbf;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eq p1, v0, :cond_2

    .line 658
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 659
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 661
    :cond_1
    const/4 v0, 0x1

    .line 663
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public native renderFilterChain(Landroid/graphics/Bitmap;Lcom/google/android/libraries/photoeditor/core/FilterChain;IZ)Landroid/graphics/Bitmap;
.end method

.method public requestRerender()V
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->e:Lhbn;

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->e:Lhbn;

    invoke-interface {v0}, Lhbn;->a()Z

    .line 672
    :cond_0
    return-void
.end method

.method public native rotateImage(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
.end method

.method public declared-synchronized setCompare(Z)V
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 391
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    monitor-exit p0

    return-void

    .line 391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setUpContext(Landroid/content/ContextWrapper;)V
    .locals 3
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->c:Landroid/content/ContextWrapper;

    .line 89
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    const-string v0, "NativeCore"

    const-string v1, "couldn\'t set up filter resources correctly"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 93
    const-string v1, "NativeCore"

    const-string v2, "couldn\'t set up filter resources correctly"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
